<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<dl class="Dl3Style" id="divHO_2x_to_3x">	
	<dt>
		<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
<!-- 		<input type="text" id="shop" class="InputTextStyle InputText1Style" /> -->
		<input type="text" id="shop" style="width:250px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>

	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="fromDate" type="text" class="InputTextStyle InputText3Style" value="<s:property value="fromDate"/>" />
	</dd>
	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="toDate" type="text" class="InputTextStyle InputText3Style" value="<s:property value="fromDate"/>" />
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button id="btnSearch" class="BtnGeneralStyle" onclick="SupperviseLogistics.exportVT13();">Xuất báo cáo</button>
</div>	
<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){			
	setDateTimePicker('fromDate');
	setDateTimePicker('toDate');
// 	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),null,null,null,null,true);
	ReportUtils.loadKendoTreeMultipleChoice("shop", $('#shopId').val(), $('#curShopId').val());
	//fix loi ngay gio hien tai
	$('#fromDate').datepicker({ dateFormat: 'dd/mm/yyyy' });
	$('#fromDate').datepicker('setDate', new Date());
	$('#toDate').datepicker({ dateFormat: 'dd/mm/yyyy' });
	$('#toDate').datepicker('setDate', new Date());
});
</script>
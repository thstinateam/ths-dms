<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="selectShopForCurrentPageDiv" style="display:none;">
<div id="selectShopForHeader" style="width:670px; height:auto;">
	<div class="PopupContentMid2">
       	<div class="GeneralForm Search1Form">
  			<div class="Clear" style="height: 10px;"></div>
  			<label class="LabelStyle Label2Style" style="width: 237px;"><span class="ReqiureStyle">Chọn lại đơn vị tương tác</span></label>
  			<div class="Clear" style="height: 10px;"></div>
            <label class="LabelStyle Label2Style">Đơn vị<span class="ReqiureStyle">(*)</span></label>
            <div class="BoxSelect BoxSelect11" id="lstShopChangeByRoleDiv">
				<select id="lstShopVOIsChange"> 
				</select>
			</div>
			<div class="Clear"></div>
            <div class="BtnCenterSection">
            	<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnSetCurrentPage" onclick="return General.setCurrentPageIsChange();">
            		<span class="Sprite2">Đồng ý</span>
            	</button>
            	&nbsp;
            	<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnGoingHome" onclick="return General.goinghome();">
            		<span class="Sprite2">Trang chủ</span>
            	</button>
            </div>
            <p id="errMsgDlIsChange" style="display: none;" class="ErrorMsgStyle"></p>
            <p id="successMsgDlIsChange" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear"></div>
		</div>
      </div>
		<script type="text/javascript">
			$(function(){
				$('#lstShopByRoleDiv').bind('keyup',function(event){
					if(event.keyCode == keyCodes.ENTER){
						return General.setCurrentPageIsChange();
					}
				});
			});
		</script>
</div>
</div>

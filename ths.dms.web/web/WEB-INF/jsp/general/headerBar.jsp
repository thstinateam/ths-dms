<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<a class="menutoggle"><i class="fa fa-bars"></i></a>
<div class="HeaderBgSection">
	<span id="logo">
		<a href="#">
			<span class="HideText DmsOneLogo">DMS One</span> 
			<%-- <span class="HideText Sprite1 VinamilkLogo"></span> --%>
			<span class="HideText Sprite1"></span>
		</a>
	</span>
<!-- 	<div class="AccountFuncSection"> -->
<!-- 		<ul class="ResetList AccountFuncList"> -->
<%-- 			<li><span id="shopLockDate">Ngày làm việc: <s:property value="lockDate"/></span></li> --%>
<%-- 			<li><span style="margin-left: 10px;"><s:property value="getInfoHeader()"/></span></li> --%>
<%-- 			<li><span style="margin-left: 10px;" id = "spHeaderShopInf">Đơn vị: <s:property value="getCurrentShopInSession().shopCode"/> - <s:property value="getCurrentShopInSession().shopName"/></span></li> --%>
<!-- 			<li class="LogoutItem"><a href="/commons/logout" class="Sprite1">Thoát</a></li> -->
<!-- 		</ul> -->
<!-- 	</div> -->
	<div class="AccountFuncSection">
		<ul class="ResetList AccountFuncList">
<!-- 		trungtm6 comment -->
<%-- 			<li><span id="shopLockDate">Ngày làm việc: <s:property value="lockDate"/></span></li> --%>

			<s:if test="checkInheritUserPrivByUserLogin () == 1">
				<li><span style="margin-left: 10px;"><s:property value="getInfoHeader()"/></span></li>
			</s:if>
			<s:else>
				<li><span style="margin-left: 10px; color: #ffd184;" title="Bạn đang sử dụng quyền thay thế"><s:property value="getInfoHeader()"/></span></li>
			</s:else>
			<s:if test="checkShopRootSingle () == 0">
				<li><span style="margin-left: 10px;" id ="spHeaderShopInf" title="Click chuột để thay đổi Đơn vị">Đơn vị:<a href="javascript:void(0);" style="color: #ffd184;" onclick="General.openDialogSelectShopIsChange();"> <s:property value="getCurrentShopInSession().shopCode"/> - <s:property value="getCurrentShopInSession().shopName"/></a></span></li>
			</s:if>
			<s:else>
				<li><span style="margin-left: 10px;" id ="spHeaderShopInf">Đơn vị: <s:property value="getCurrentShopInSession().shopCode"/> - <s:property value="getCurrentShopInSession().shopName"/></span></li>
			</s:else>
			<!-- <li class="LogoutItem"><a href="/commons/logout" class="Sprite1">Thoát</a></li> -->
			<li class="LogoutItem"><a href="javascript:void(0);" onclick="setTimeout(function(){isCloseInfoUser=1;$('.AccOption').toggle();},200);" class="Sprite1"><span class="HideText">Menu</span></a></li>
		</ul>
		<div class="AccOption" style="display:none;" onclick="isCloseInfoUser=1;">
	        <ul class="ResetList ChildMenuSectionList" >
				<li class="Item1"><a href="#" class="Sprite1 Item1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                 <li>
                 	<div class="InfoShop FixFloat">
                        <img width="50" height="50" src="<s:property value="getImageCurrentUser()"/>" class="Avatar"/>
                        <p class="Text1Style"><s:property value="currentUser.userName"/></p>
	                    <%-- <p class="Text1Style"><s:property value="getCodeCurrentUser()"/></p> --%>
                     </div>
                 </li>
                 <li><a href="javascript:void(0);" onclick="changePassPopup();" class="">Đổi mật khẩu</a></li>
                 <li class="EndItem"><a href="/commons/logout" onclick="$('.AccOption').hide();return true;">Thoát</a></li>
             </ul>
	    </div>
	</div>
	<div class="Clear"></div>
</div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="ths.dms.helper.Configuration"%>
<%@page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> --%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%-- <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> --%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="java.util.Date"%>
<%@page import="ths.dms.web.servlet.CaptchaServlet"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:i18n name="message">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>DMS</title>
	<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/iconselect/iconselect.js")%>"></script>
	<link rel="stylesheet" type="text/css" href="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/iconselect/iconselect.css")%>" ></link>
    <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/plugins/iconselect/iscroll.js")%>"></script>
    
<style type="text/css">
/*- [ General -*/
html { height:100%; overflow:auto; }
body { font-family:Arial; background-color:#fff; font-size:100%; color:#333; }
body, div, h1, h2, h3, h4, h5, h6, p, form, dl, dl dt, dl dd, pre { margin:0; padding:0; text-align:left; }
pre { font-family:Arial, Geneva, sans-serif; white-space:pre-wrap; }
ul, li { text-align:left;list-style:none; }
img { border:none; vertical-align:middle; }
fieldset { border:none; background:none; margin:0; padding:0; }
fieldset legend { display: none; }
a { text-decoration:none; text-align:left; color:#0066cc;outline:none !important; }
a:focus { outline:#0000FF dotted thin; }
a:hover { text-decoration: none; }
button, input { font-family:Arial; margin:0; padding:0; }
textarea { font-family:Arial; padding:0px; margin:0px; overflow:hidden; resize:none; vertical-align:bottom; outline:none; overflow-y:auto; }
select { font-family:Arial; }
select option { padding:0 4px; }
.FixFloat { overflow:hidden; height:auto; }
.Clear { clear:both; padding:0 !important; }
.Wordwrap { word-wrap:break-word; }
.Wordbreak { display: inline-block; }
.HideText { text-indent:-1000em; display:block; overflow:hidden; height:auto; }
.ResetList, .ResetList li { margin:0; padding:0; list-style:none; }
object, embed { outline:none; }
input[disabled="disabled"] { background-color:#ededed; color:#333333; border:1px solid #D0DBEA; }
/*- ] General -*/

/*- [ Common -*/
.Sprite1 { background-image:none; background-repeat:no-repeat; }
#logo, .AccountFuncList li, #logo a span, .LabelLStyle, .LoginFormSection .BgInputText1Style, .CapcharEditSection, .RefeshStyle, .CopyRightSection .CopyRightTextStyle { float:left; }
.BoxFuncLogin, .CopyRightSection .ViettelLink { float:right; }
/*- ] Common -*/

/*- [ Header -*/
#header { position:fixed; top:0px; left:0px; width:100%; }
.LoginTheme { 
	background-color:#FFFFD4; /* tunglh 23/04/16 */
	background-image:none;
	background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center;
    background-size:cover;
	height:100%;
	width: 100%;
	margin:0;
    padding:0;
    height:100%;
    background-size:100% 100%;
    -webkit-background-size:100% 100%;        /*  Safari  */
    -khtml-background-size:100% 100%;         /*  Konqueror  */
     -moz-background-size:100% 100%;          /*  Firefox  */
    background-repeat:no-repeat;
    background-attachment:fixed; 
}
.LoginTheme #container { background-color:#eee; height:100%; width:100%; }
.LoginTheme #content { z-index:10; }
.LoginFormSection { width:397px; margin:0 auto; }
.LoginFormTopS { 
	background-color:#fff; 
	padding:15px 0 0; 
	border-radius: 10px;
	-moz-box-shadow: 0 0 5px #888;
	-webkit-box-shadow: 0 0 5px#888;
	box-shadow: 0 0 5px #888;	
}
.LoginFormBtmS { background-color:#fff; padding:0 0 23px; border-radius: 10px;}
.LoginFormLoopS { background-color:#fff; }
.LoginFormPenuS { background-color:#fff; font-size:0.75em; padding:16px 29px 0 30px; }
.LoginFormSection .LabelStyle { font-weight:bold; padding:0 0 6px; display:block; }
.LoginFormSection .ErrorMsgStyle { color:#ff0000; font-size:0.9167em; }
.LoginFormSection .BgInputTextStyle { margin:0 0 11px; background-position:0px -560px; width:338px; height:40px; }
.LoginFormSection .BgInputTextStyle span { display:inline-block; width:100%;}
.LoginFormSection .UserTempStyle { background-position:10px -711px; }
.LoginFormSection .PwdTempStyle { background-position:10px -747px; }
.LoginFormSection .InputTextStyle { 
	background-color: #ffffb4;
    border: 1px solid #ddd;
    border-radius: 3px;
    height: 35px;
    line-height: 35px;
    padding-left: 10px;
    width: 100%; }
.LoginFormSection .BgInputText1Style { width:112px; height:38px; background-position:0px -660px; padding:1px 5px; margin:0 10px 0 0; }
.LoginFormSection .CapcharEditSection { margin:0 12px 0 0; }
.LoginFormSection .RefeshStyle { margin:12px 0 0; }
.LoginFormSection .Field { margin:0 0 9px; }
.FieldLogin { padding:15px 0; }
.FieldLogin .LabelLStyle input { vertical-align:middle; margin:0 7px 0 0; }
.FieldLogin .BoxFuncLogin { color:#0343bb; }
.ButtonLogin { text-align:center; padding:0 0 20px; }
.ButtonLogin .InputLoginStyle { 
		width:348px; font-weight:bold; 
		color:#333; text-transform:uppercase; text-align:center; 
		padding:10px 0; 
		border:none; 
		cursor:pointer; 
}
.CopyRightSection .CopyRightTextStyle { padding:16px 0 12px 88px; background-position:0px -447px; }
.CopyRightSection .ViettelLink { padding:16px 0 0; color:#0099cc; }
.LoginSection h3 { height:0px; }
/* Middle */
.BoxFrame { position:relative; overflow:hidden; }
.BoxFrame[class] { display:table; position:static; }
.BoxMiddle { position:absolute; top:50%; }
.BoxMiddle[class] { display:table-cell; vertical-align:middle; position:static; }
.LoginTheme .BoxMiddle > #content { position:relative; top:-50%; display:block; margin:0 auto; }
.logo_salesone_company {
	float: left; width: 120px; padding-top: 15px; height: 48px; padding-left: 32px;
}
.logoCompany {
	background:url(resources/images/ths.png) center bottom no-repeat; padding:0 0 64px;
}
/*-]- Content*/

/*-[-#footer*/
.LoginTheme #footer { background:url(resources/images/bg_footer.jpg) left top repeat-x; position:fixed; bottom:0px; left:0px; width:100%; }
#footer .Text1Style { font-size:0.75em; color:#04528e; padding:7px 0; text-align:center; }
/*-]-#footer*/

/*- [ Hack Chrome -*/
@media screen and (-webkit-min-device-pixel-ratio:0) {
 }
/*- ] Hack Chrome -*/
</style>
</head>  
<%
	int attemptLogin = 0;
	try {
		attemptLogin = Integer.parseInt(request.getSession(false)
				.getAttribute("attemptLogin").toString());
		System.out.println(attemptLogin);
		if (attemptLogin >= 5) {
			CaptchaServlet.generateToken(session);	
		}		
	} catch (Exception e) {
	}
%>
<body class="LoginTheme">
	<div id="container" class="BoxFrame" style="background:none;">
		<div class="BoxMiddle">
			<div id="header">
                <div class="HeaderBgSection" style="background:none;" > <!--  Hien background header -->
                    <h1 id="logo"><a href="javascript:void(0)"><span class="HideText Sprite1 DmsOneLogo">SALES ONE</span> <span class="HideText Sprite1">IDP</span></a></h1>
                    <!-- <div id="my-icon-select"></div> -->
                </div>
            </div>
            <div id="content"  >
                <div class="LoginSection">
                    <h3 class="HideText"> <s:text name="page.button.title.login"></s:text> </h3>
                    <div class="LoginFormSection">
						<div class="LoginFormTopS">
                            <div class="LoginFormBtmS">
                                <div class="LoginFormLoopS">
                                    <div class="LoginFormPenuS">                        		
                                        <form:form method="post" id="fm1" cssClass="fm-v clearfix" commandName="${commandName}" htmlEscape="true" autocomplete="off">
                                            <fieldset>
                                                <legend> <s:text name="page.button.title.login"></s:text> </legend>
                                                <label class="LabelStyle"><s:text name="page.username" /></label>
                                                <p class="BgInputTextStyle Sprite1">
                                                	<span class="UserTempStyle Sprite1">
                                                		<input id="username"  name="username" type="text" class="InputTextStyle"/>                                                	
                                                	</span>
                                                </p>
                                                <label class="LabelStyle"><s:text name="page.password" /></label>
                                                <p class="BgInputTextStyle Sprite1">
                                                	<span class="PwdTempStyle Sprite1">
                                                		<input type="password" class="InputTextStyle" style="display: none;"/>
                                                		<input id="password" name="password" type="password" class="InputTextStyle"/>
                                                	</span>
                                                </p>
                                                <%
													if (attemptLogin >= 5) {
												%>
                                                <label class="LabelStyle">Mã bảo vệ:</label>
                                                <div class="FixFloat Field">
                                                    <p class="BgInputTextStyle BgInputText1Style Sprite1"><input id="capcha" name="capcha" class="InputTextStyle" /></p>                                                       
                                                    <div class="CapcharEditSection" style="margin:0px">
					                                    <img id="imgCaptcha" src="/captcha" width="180" height="39" />
					                                </div>                                                 
                                                    <a onclick="refreshCapchaLogin();" class="RefeshStyle" href="javaScript:void(0);"><img src="resources/images/icon-refesh.png?v=1001" width="20px" height="20px"/></a>
                                                </div>
                                                <%
 													} 
 												%>
<%--                                                 <form:errors path="*" cssClass="ErrorMsgStyle" id="errMsg" element="div"/> --%>
                                                <div class="ErrorMsgStyle" id="status">${errMsg}</div>
                                               <div class="FixFloat FieldLogin" style="display:none;"> 
                                                   <a href="mailto:dmsone_support@dmsone.com.vn" title="<s:text name="page.login.contact.change.pwd" />" class="BoxFuncLogin"> <s:text name="page.login.contact.change.pwd" /></a>
                                                </div>
                                                
                                                <div class="ButtonLogin FixFloat" style="display:inline-block;">                                                    
                                                    <input type="hidden" name="lt" value="${flowExecutionKey}" />
													<input type="hidden" name="_eventId" value="submit" />
											 		<input class="InputLoginStyle" name="submit" accesskey="l" 
											 			value="<s:text name="page.button.title.login" />" tabindex="4" type="submit" />
                                                </div>
                                                <input type="hidden" id="loginCount" name="loginCount" value="${requestScope.loginCount}" />
                                                <div class="FixFloat CopyRightSection">
                                                    <p class="logoCompany"></p>
                                                </div>
                                            </fieldset>
                                        </form:form>
                                    </div>
                                </div>            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="footer">
                <p class="Text1Style"><s:text name="page.login.copyright" /></p>
            </div>           				
		</div>
	</div>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.js"></script>
	<script>
			document.getElementById('username').focus();
			/**
			* function thay doi pass trang login
			* @author vuongmq
			* @since 24/08/2015
			*/
			function changePass() {
				params = 'width=' + screen.width;
				params += ', height=' + screen.height;
				params += ', top=0, left=0';
				params += ', fullscreen=yes';
	
				//var newwin = window.open('/changePassword',
				var newwin = window.open('/changePasswordPopup',
						'', params);
				if (window.focus) {
					newwin.focus();
				}
				return false;
			}			
			function refreshCapchaLogin() { 
				$("#imgCaptcha").attr("src", "/captcha?v=" + new Date().getMilliseconds());		
				return false;
			}
			var iconSelect;
			var firstLoad = true;
			/* var lang = '<s:property value="lang" />';
            

	        window.onload = function() {	        	
	            document.getElementById('my-icon-select').addEventListener('changed', function(e) {	 
	            	if (firstLoad) {
	            		return;
	            	}
	              	window.location.href= '/login?lang=' + iconSelect.getSelectedValue();
	            });
	        	iconSelect = new IconSelect("my-icon-select", 
	                     {'selectedIconWidth':35,
	                     'selectedIconHeight':22,
	                     'selectedBoxPadding':0,
	                     'iconsWidth':35,
	                     'iconsHeight':30,
	                     'boxIconSpace':0,
	                     'vectoralIconNumber':2,
	                     'horizontalIconNumber':1});
	        	var icons = [];
	            icons.push({'iconFilePath':'/resources/i18n/vi.png', 'iconValue':'vi'});
	            icons.push({'iconFilePath':'/resources/i18n/en.png', 'iconValue':'en'});	           
	            iconSelect.refresh(icons);
				for (var i = 0; i < icons.length; ++i) {
					var icon = icons[i];
					if (lang == icon.iconValue) {
						iconSelect.setSelectedIndex(i);
					}
				}
			firstLoad = false;
		}; */
	</script>
</body>
</html>
</s:i18n>

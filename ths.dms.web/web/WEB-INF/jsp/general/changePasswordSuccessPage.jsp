<%@ page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page import="java.util.Date"%>
<%@page import="viettel.passport.util.CaptchaServlet"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
	    <title>Nutifood Single Sign On</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	    
	    <!--[if gte IE 6]><style type="text/css" media="screen">@import 'css/ie_cas.css';</style><![endif]-->
	    <script type="text/javascript" src="js/common_rosters.js"></script>
	    <link rel="icon" href="<%=request.getContextPath()%>/favicon.ico" type="image/x-icon" />
	    <style type="text/css">
/*- [ General -*/
html { height:100%; overflow:auto; }
body { font-family:Arial; background-color:#fff; font-size:100%; color:#333; }
body, div, h1, h2, h3, h4, h5, h6, p, form, dl, dl dt, dl dd, pre { margin:0; padding:0; text-align:left; }
pre { font-family:Arial, Geneva, sans-serif; white-space:pre-wrap; }
ul, li { text-align:left;list-style:none; }
img { border:none; vertical-align:middle; }
fieldset { border:none; background:none; margin:0; padding:0; }
fieldset legend { display: none; }
a { text-decoration:none; text-align:left; color:#0066cc;outline:none !important; }
a:focus { outline:#0000FF dotted thin; }
a:hover { text-decoration: none; }
button, input { font-family:Arial; margin:0; padding:0; }
textarea { font-family:Arial; padding:0px; margin:0px; overflow:hidden; resize:none; vertical-align:bottom; outline:none; overflow-y:auto; }
select { font-family:Arial; }
select option { padding:0 4px; }
.FixFloat { overflow:hidden; height:auto; }
.Clear { clear:both; padding:0 !important; }
.Wordwrap { word-wrap:break-word; }
.Wordbreak { display: inline-block; }
.HideText { text-indent:-1000em; display:block; overflow:hidden; height:auto; }
.ResetList, .ResetList li { margin:0; padding:0; list-style:none; }
object, embed { outline:none; }
input[disabled="disabled"] { background-color:#ededed; color:#333333; border:1px solid #D0DBEA; }
/*- ] General -*/

/*- [ Common -*/
.Sprite1 { background-image:url(resources/images/bg_sprites1.png); background-repeat:no-repeat; }
#logo, .AccountFuncList li, #logo a span, .LabelLStyle, .LoginFormSection .BgInputText1Style, .CapcharEditSection, .RefeshStyle, .CopyRightSection .CopyRightTextStyle { float:left; }
.BoxFuncLogin, .CopyRightSection .ViettelLink { float:right; }
/*- ] Common -*/

/*- [ Header -*/
#header { position:fixed; top:0px; left:0px; width:100%; }
.HeaderBgSection { background:url(resources/images/bg_header.jpg) left top repeat-x; padding:12px 12px 12px; }
.DmsOneLogo { background-position:0px 0px; width:94px; height:23px; margin:0 8px 0 0; }
.VinamilkLogo { background-position:right 0px; width:125px; height:20px; }
/*- ] Menu -*/
/*- ] Header -*/

/*-[- Content*/
.LoginTheme { background:#fff url(resources/images/bg_loginloop.jpg) left top repeat-x; height:100%; }
.LoginTheme #container { background:url(resources/images/bg_login.jpg) center top no-repeat; height:100%; width:100%; }
.LoginTheme #content { z-index:10; }
.LoginFormSection { width:397px; margin:0 auto; }
.LoginFormTopS { background:url(resources/images/bg_formChangePasswordTop.png) center top no-repeat; padding:65px 0 0; }
.LoginFormBtmS { background:url(resources/images/bg_formLoginBtm.png) center bottom no-repeat; padding:0 0 23px; }
.LoginFormLoopS { background:url(resources/images/bg_formLoginLoop.png) center top repeat-y; }
.LoginFormPenuS { background:url(resources/images/bg_formLoginPenu.png) center bottom no-repeat; font-size:0.75em; padding:16px 29px 0 30px; }
.LoginFormSection .LabelStyle { font-weight:bold; padding:0 0 6px; display:block; }
.LoginFormSection .ErrorMsgStyle { color:#ff0000; font-size:0.9167em; }
.LoginFormSection .BgInputTextStyle { margin:0 0 11px; background-position:0px -560px; width:338px; height:40px; }
.LoginFormSection .BgInputTextStyle span { padding:1px 10px 0 30px; display:inline-block; width:220px; }
.LoginFormSection .UserTempStyle { background-position:10px -711px; }
.LoginFormSection .PwdTempStyle { background-position:10px -747px; }
.LoginFormSection .InputTextStyle { width:100%; height:35px; border:none; background:none; line-height:35px; }
.LoginFormSection .BgInputText1Style { width:112px; height:38px; background-position:0px -660px; padding:1px 5px; margin:0 10px 0 0; }
.LoginFormSection .CapcharEditSection { margin:0 12px 0 0; }
.LoginFormSection .RefeshStyle { margin:12px 0 0; }
.LoginFormSection .Field { margin:0 0 9px; }
.FieldLogin { padding:15px 0; }
.FieldLogin .LabelLStyle input { vertical-align:middle; margin:0 7px 0 0; }
.FieldLogin .BoxFuncLogin { color:#0343bb; }
.ButtonLogin { text-align:center; padding:0 0 20px; }
.ButtonLogin .InputLoginStyle { background-position:0px -511px; width:336px; font-weight:bold; color:#fff; text-transform:uppercase; text-align:center; padding:10px 0; border:none; background-color:transparent; cursor:pointer; }
.CopyRightSection .CopyRightTextStyle { padding:16px 0 12px 88px; background-position:0px -447px; }
.CopyRightSection .ViettelLink { padding:16px 0 0; color:#0099cc; }
.LoginSection h3 { height:0px; }
/* Middle */
.BoxFrame { position:relative; overflow:hidden; }
.BoxFrame[class] { display:table; position:static; }
.BoxMiddle { position:absolute; top:50%; }
.BoxMiddle[class] { display:table-cell; vertical-align:middle; position:static; }
.LoginTheme .BoxMiddle > #content { position:relative; top:-50%; display:block; margin:0 auto; }
/*-]- Content*/

/*-[-#footer*/
.LoginTheme #footer { background:url(resources/images/bg_footer.jpg) left top repeat-x; position:fixed; bottom:0px; left:0px; width:100%; }
#footer .Text1Style { font-size:0.75em; color:#04528e; padding:16px 0; text-align:center; }
/*-]-#footer*/

/*- [ Hack Chrome -*/
@media screen and (-webkit-min-device-pixel-ratio:0) {
 }
/*- ] Hack Chrome -*/
</style>
			<!--[if IE 8]>
				<style type="text/css">
				.BgInputTextStyle .InputTextStyle { padding:2px 0 0; }
			    </style>
			<![endif]-->
	</head>
	<body class="LoginTheme">
	<div id="container" class="BoxFrame">
		<div class="BoxMiddle">
			<div id="header">
                <div class="HeaderBgSection">
                    <h1 id="logo"><a href="javascript:void(0)"><span class="HideText Sprite1 DmsOneLogo">DMS One</span> <span class="HideText Sprite1 VinamilkLogo">Nutifood</span></a></h1>
                    <div class="Clear"></div>
                </div>
            </div>
            <div id="content">
                <div class="LoginSection">
                    <h3 class="HideText">Thay đổi mật khẩu</h3>
                    <div class="LoginFormSection">
                        <div class="LoginFormTopS">
                            <div class="LoginFormBtmS">
                                <div class="LoginFormLoopS">
                                    <div class="LoginFormPenuS">      
                                    	<div id="msg" class="SuccessMsgStyle">
									    Bạn đã thay đổi mật khẩu thành công
									    </div>
                                    </div>
                                </div>            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="footer">
                <p class="Text1Style">&copy; Copyright ThienHaiSoft</p>
            </div>           				
		</div>
	</div>		
	</body>
</html>

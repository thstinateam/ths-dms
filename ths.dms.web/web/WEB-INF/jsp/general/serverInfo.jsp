<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.struts2.ServletActionContext" %>
<%@page import="ths.dms.helper.Configuration"%>
<input type="hidden" id="cur_focus" value=""/>
<div id="responseDiv" style="display: none;"></div>
<%-- <div id ="searchStyle1EasyUIDialogDiv" style="display: none" >
	<div id="searchStyle1EasyUIDialog" class="easyui-dialog" style="width: 600px; height: auto;" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="seachStyle1CodeLabel" class="LabelStyle Label1Style" style=" width: 100px;">Mã</label>
				<input id="seachStyle1Code" maxlength="50" tabindex="1" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style" style=" width: 100px;">Tên</label> 
				<input id="seachStyle1Name" type="text" maxlength="250" tabindex="2" style="width: 145px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>
                <label id="seachStyle1AddressLabel" style="display: none;width: 100px;" class="LabelStyle Label1Style">Địa chỉ</label>
                <input id="seachStyle1Address" type="text" style="display: none;width: 409px;"  maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" tabindex="3" id="btnSearchStyle1">Tìm kiếm</button>
				<input type="hidden" name="" id="searchStyle1Url"/>
				<input type="hidden" name="" id="searchStyle1CodeText"/>
				<input type="hidden" name="" id="searchStyle1NameText"/>
				<input type="hidden" name="" id="searchStyle1IdText"/>
				<s:hidden id="searchStyle1AddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="GridSection" id="searchStyle1ContainerGrid">					
					<table id="searchStyle1Grid" class="easyui-datagrid"></table>
					<div id="searchStyle1Pager"></div>
				</div>
				<div class="BtnCenterSection">
					<button id="__btnSave" style="display: none;" class="BtnGeneralStyle">Chọn</button>
					<button id="__btnClose" class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgCopy" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
</div>
<div id ="searchCustomerForCopyDiv" style="display: none" >
	<div id="searchCustomerForCopyDialog" class="easyui-dialog" style="width: 600px; height: auto;" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="seachStyle1CodeLabel" class="LabelStyle Label1Style" style=" width: 100px;">Mã</label>
				<input id="seachStyle1Code" maxlength="50" tabindex="1" type="text" style=" width: 145px;" class="InputTextStyle InputText1Style" /> 
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style" style=" width: 100px;">Tên</label> 
				<input id="seachStyle1Name" type="text" maxlength="250" tabindex="2" style="width: 145px;"  class="InputTextStyle InputText1Style" />
				<div class="Clear"></div>
                <label id="seachStyle1AddressLabel" style="display: none;width: 100px;" class="LabelStyle Label1Style">Địa chỉ</label>
                <input id="seachStyle1Address" type="text" style="display: none;width: 409px;"  maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
				<button class="BtnGeneralStyle BtnSearchStyle BtnSearchOnDialog" tabindex="3" id="btnSearchStyle1">Tìm kiếm</button>
				<input type="hidden" name="" id="searchStyle1Url"/>
				<input type="hidden" name="" id="searchStyle1CodeText"/>
				<input type="hidden" name="" id="searchStyle1NameText"/>
				<input type="hidden" name="" id="searchStyle1IdText"/>
				<s:hidden id="searchStyle1AddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="GridSection" id="searchStyle1ContainerGrid">					
					<table id="searchStyle1Grid" class="easyui-datagrid"></table>
					<div id="searchStyle1Pager"></div>
				</div>
				<div class="BtnCenterSection">
					<button id="btnCopy" onclick="SPAdjustmentOrder.initCopyOrder();" class="BtnGeneralStyle">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearch" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgCopy" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
</div>
<div id="searchStyle1" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralMilkBox">
                  <div class="GeneralMilkTopBox">
                      <div class="GeneralMilkBtmBox">
                          <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox ResearchSection">
                         	<div class="SearchSales4Form">                                 
                                 <label id="seachStyle1CodeLabel" class="LabelStyle Label1Style">Mã</label>
                                 <input id="seachStyle1Code" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText1Style" />
                                 <label id="seachStyle1NameLabel" class="LabelStyle Label2Style">Tên</label>
                                 <input id="seachStyle1Name" type="text" maxlength="250" tabindex="2" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <label id="seachStyle1AddressLabel" style="display: none;" class="LabelStyle Label1Style">Địa chỉ</label>
                                 <input id="seachStyle1Address" type="text" style="display: none;" maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <div class="ButtonSection">
                                     <button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" tabindex="4" id="btnSearchStyle1"><span class="Sprite2">Tìm kiếm</span></button>
                                 </div>
                                 <s:hidden id="searchStyle1Url"></s:hidden>
                                 <s:hidden id="searchStyle1CodeText"></s:hidden>
                                 <s:hidden id="searchStyle1NameText"></s:hidden>
                                 <s:hidden id="searchStyle1AddressText"></s:hidden>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="GeneralMilkBox">
                 <div class="GeneralMilkTopBox">
                     <div class="GeneralMilkBtmBox">
                         <h3 class="Sprite2"><span class="Sprite2">Kết quả tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox">
	                         <div id="searchStyle1ScrollBody" class="">
	                         	<div class="ResultSection" id="searchStyle1ContainerGrid">                                 
	                                <table id="searchStyle1Grid"></table>
									<div id="searchStyle1Pager"></div>
	                             </div>
	                         </div>                             
                         </div>
                     </div>
                 </div>
             </div>
             <div class="BoxDialogBtm">
                 <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
             </div>
     	</div>
     </div>
 </div> 
 <div id="searchStyle4" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralMilkBox">
                  <div class="GeneralMilkTopBox">
                      <div class="GeneralMilkBtmBox">
                          <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox ResearchSection">
                         	<div class="SearchSales4Form">
                         		 <label id="seachStyle4ShopCodeLabel" class="LabelStyle Label1Style">Mã đơn vị</label>
                                 <input id="seachStyle4ShopCode" maxlength="50" tabindex="1" disabled="disabled" type="text" class="InputTextStyle InputText1Style" />
                                 <label id="seachStyle4ShopNameLabel" class="LabelStyle Label2Style">Tên đơn vị</label>
                                 <input id="seachStyle4ShopName" type="text" maxlength="250" disabled="disabled" tabindex="2" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>                                  
                                 <label id="seachStyle4CodeLabel" class="LabelStyle Label1Style">Mã</label>
                                 <input id="seachStyle4Code" maxlength="50" tabindex="3" type="text" class="InputTextStyle InputText1Style" />
                                 <label id="seachStyle4NameLabel" class="LabelStyle Label2Style">Tên</label>
                                 <input id="seachStyle4Name" type="text" maxlength="250" tabindex="4" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <div class="ButtonSection">
                                     <button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" tabindex="5" id="btnSearchStyle4"><span class="Sprite2">Tìm kiếm</span></button>
                                 </div>
                                 <s:hidden id="searchStyle4Url"></s:hidden>
                                 <s:hidden id="searchStyle4CodeText"></s:hidden>
                                 <s:hidden id="searchStyle4NameText"></s:hidden>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="GeneralMilkBox">
                 <div class="GeneralMilkTopBox">
                     <div class="GeneralMilkBtmBox">
                         <h3 class="Sprite2"><span class="Sprite2">Kết quả tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox">
	                         <div id="searchStyle1ScrollBody" class="">
	                         	<div class="ResultSection" id="searchStyle4ContainerGrid">                                 
	                                <table id="searchStyle4Grid"></table>
									<div id="searchStyle4Pager"></div>
	                             </div>
	                         </div>                             
                         </div>
                     </div>
                 </div>
             </div>
             <div class="BoxDialogBtm">
                 <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
             </div>
     	</div>
     </div>
 </div>
 <div id="searchStyle1EasyUI" class="easyui-window" data-options="closed:true,modal:true" style="width:540px;height:392px;display: none">
 	<input type="hidden" id="needReloadGrid" value="false">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralMilkBox">
                  <div class="GeneralMilkTopBox">
                      <div class="GeneralMilkBtmBox">
                          <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox ResearchSection">
                         	<div class="Warehouse84Form">                                 
                                 <label id="seachStyle1CodeLabel" class="LabelStyle Label1Style">Mã</label>
                                 <input id="seachStyle1Code" type="text" class="InputTextStyle InputText1Style" />
                                 <label id="seachStyle1NameLabel" class="LabelStyle Label2Style">Tên</label>
                                 <input id="seachStyle1Name" type="text" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <label style="display:none;" id="seachStyle4AddressLabel" class="LabelStyle Label1Style">Địa chỉ</label>
                                 <input style="display:none;" id="seachStyle4Address" type="text" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <div class="ButtonSection">
                                     <button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnSearchStyle1"><span class="Sprite2">Tìm kiếm</span></button>
                                 </div>
                                 <s:hidden id="searchStyle1Url"></s:hidden>
                                 <s:hidden id="searchStyle1CodeText"></s:hidden>
                                 <s:hidden id="searchStyle1NameText"></s:hidden>
                                 <s:hidden id="searchStyle1AddressText"></s:hidden>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="GeneralMilkBox">
                 <div class="GeneralMilkTopBox">
                     <div class="GeneralMilkBtmBox">
                         <h3 class="Sprite2"><span class="Sprite2">Kết quả tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox">
	                         <div id="searchStyle1ScrollBody">
	                         	<div class="ResultSection" id="searchStyle1ContainerGrid">                                 
	                                <table id="searchStyle5Grid"></table>
									<div id="searchStyle5Pager"></div>
	                             </div>
	                         </div>                             
                         </div>
                     </div>
                 </div>
             </div>
     	</div>
     </div>
</div>
<div id="searchStyle3" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralMilkBox">
                  <div class="GeneralMilkTopBox">
                      <div class="GeneralMilkBtmBox">
                          <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox ResearchSection">
                         	<div class="Warehouse84Form">                                 
                                 <label id="searchStyle3CodeLabel" class="LabelStyle Label1Style">Mã SP</label>
                                 <input id="searchStyle3Code" type="text" class="InputTextStyle InputText1Style" />
                                 <label id="searchStyle3NameLabel" class="LabelStyle Label2Style">Tên SP</label>
                                 <input id="searchStyle3Name" type="text" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <label id="searchStyle3FromLabel" class="LabelStyle">Số lượng chưa phân bổ của cấp cha:</label>
                                 <div class="Clear"></div>
                                 <label id="searchStyle3FromLabel" class="LabelStyle Label1Style">Từ</label>
                                 <input id="searchStyle3From" type="text" class="InputTextStyle InputText1Style" onkeypress="return numbersonly(event,false);" maxlength="10"/>
                                 <label id="searchStyle3ToLabel" class="LabelStyle Label2Style">Đến</label>
                                 <input id="searchStyle3To" type="text" class="InputTextStyle InputText1Style" onkeypress="return numbersonly(event,false);" maxlength="10"/>
                                 <div class="Clear"></div>
                                 <div class="ButtonSection">
                                     <button class="BtnGeneralStyle BtnSearchOnDialog Sprite2" id="btnSearchStyle3"><span class="Sprite2">Tìm kiếm</span></button>
                                 </div>
                                 <p class="ErrorMsgStyle Sprite1" id="errDialogMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
                                 <s:hidden id="searchStyle3Url"></s:hidden>
                                 <s:hidden id="searchStyle3CodeText"></s:hidden>
                                 <s:hidden id="searchStyle3NameText"></s:hidden>
                                 <s:hidden id="searchStyle3IdText"></s:hidden>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="GeneralMilkBox">
                 <div class="GeneralMilkTopBox">
                     <div class="GeneralMilkBtmBox">
                         <h3 class="Sprite2"><span class="Sprite2">Kết quả tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox">
                             <div class="ResultSection" id="searchStyle3ContainerGrid">                                 
                                <table id="searchStyle3Grid"></table>
								<div id="searchStyle3Pager"></div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="BoxDialogBtm">
                 <div class="ButtonSection">
                 	<button id="btnProductAdd" class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"><span class="Sprite2">Chọn</span></button>
                 	<button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button>
                 	<div class="BoxDialogAlert">
						<p style="display: none" id="errMsgUpdateDiv" class="ErrorMsgStyle Sprite1">Vui lòng nhập đúng kết quả</p>
					</div>
                 </div>
             </div>
     	</div>
     </div>
</div>
<div id="searchStyle5PPDialog" style="display: none;">
	<div id="searchStyle5" class="easyui-dialog" title="Danh sách sản phẩm khuyến mãi" style="width:616px;height:auto;" data-options="closed:true,modal:true">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
                <div class="GridSection">
                    <table id="listPromotionProduct"></table>
                </div>
                <p style="display: none" id="errMultiChooseProduct" class="ErrorMsgStyle"></p>
                <div class="BtnCenterSection">
                	<button class="BtnGeneralStyle BtnGeneralMStyle" id="btnMultiChoose" style="display: none;"><span class="Sprite2">Chọn</span></button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#searchStyle5').dialog('close');"><span class="Sprite2">Đóng</span></button>
				</div>
            </div>            
		</div>
    </div>
</div>
<div id="searchStyleDistrict" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralMilkBox">
                  <div class="GeneralMilkTopBox">
                      <div class="GeneralMilkBtmBox">
                          <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox ResearchSection">
                         	<div class="Warehouse84Form">                                 
                                 <label id="seachStyleDistrictCodeLabel" class="LabelStyle Label1Style" style="width:90px;">Mã</label>
                                 <input id="seachStyleDistrictCode" maxlength="50" type="text" class="InputTextStyle InputText1Style" style="width:120px;" />
                                 <label id="seachStyleDistrictNameLabel" class="LabelStyle Label2Style" style="width:90px;padding-right:10px;">Tên</label>
                                 <input id="seachStyleDistrictName" type="text" maxlength="250" class="InputTextStyle InputText1Style" style="width:120px;" />
                                 <div class="Clear"></div>
                                 <div class="ButtonSection">
                                     <button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnSearchStyleDistrict"><span class="Sprite2">Tìm kiếm</span></button>
                                 </div>
                                 <s:hidden id="searchStyleDistrictUrl"></s:hidden>
                                 <s:hidden id="searchStyleDistrictCodeText"></s:hidden>
                                 <s:hidden id="searchStyleDistrictNameText"></s:hidden>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="GeneralMilkBox">
                 <div class="GeneralMilkTopBox">
                     <div class="GeneralMilkBtmBox">
                         <h3 class="Sprite2"><span class="Sprite2">Kết quả tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox">
	                         <div id="searchStyleDistrictScrollBody" class="">
	                         	<div class="ResultSection" id="searchStyleDistrictContainerGrid">                                 
	                                <table id="searchStyleDistrictGrid"></table>
									<div id="searchStyleDistrictPager"></div>
	                             </div>
	                         </div>                             
                         </div>
                     </div>
                 </div>
             </div>
             <div class="BoxDialogBtm">
                 <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
             </div>
     	</div>
     </div>
 </div>
<div id="payrollTableEasyUIDiv" class="easyui-window" data-options="closed:true,modal:true" style="width:540px;height:392px;display: none">
	<input type="hidden" id="tReloadGrid" value="false">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralMilkBox">
                  <div class="GeneralMilkTopBox">
                      <div class="GeneralMilkBtmBox">
                          <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox ResearchSection">
                         	<div class="Warehouse84Form">                                 
                                 <label class="LabelStyle Label1Style">Mã bảng lương</label>
                                 <input id="payrollTableCodeEasy" type="text" class="InputTextStyle InputText1Style" />
                                 <label class="LabelStyle Label2Style">Tên bảng lương</label>
                                 <input id="payrollTableNameEasy" type="text" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <label class="LabelStyle Label1Style">Tháng/Năm</label>
                                 <input id="dateSearchEasy" type="text" class="InputTextStyle InputText2Style" />
                                 <div class="Clear"></div>
                                 <div class="ButtonSection">
                                     <button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnPayrollSearch"><span class="Sprite2">Tìm kiếm</span></button>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="GeneralMilkBox">
                 <div class="GeneralMilkTopBox">
                     <div class="GeneralMilkBtmBox">
                         <h3 class="Sprite2"><span class="Sprite2">Kết quả tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox">
	                         <div id="divScrollBody">
	                         	<div class="ResultSection" id="payrollTableEasyGrid">                                 
	                                <table id="easyTGrid"></table>
									<div id="easyTPager"></div>
	                             </div>
	                         </div>                             
                         </div>
                     </div>
                 </div>
             </div>
     	</div>
     </div>
 </div>
 <div id ="soReportContainer" style="display: none" >
	<div id="soReportDialog" class="easyui-dialog" title="Kết quả xuất báo cáo" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >				
				<div id="reportDataScroll" class="ScrollBodySection" style="height: 300px;overflow: scroll;">
              		<div class="ResultSection" id="reportData"> </div>
              	</div>
              	<div class="BtnCenterSection" style=" margin-top: 5px;">
					<div class="BoxSelect BoxSelect5">
	                    <s:select id="formatType" headerKey="-2" headerValue="-- Chọn định dạng xuất --" cssClass="" list="#{'XLS':'XLS','PDF':'PDF','CSV':'CSV'}"></s:select>
	             	</div>
	             	<button id="btnExport" class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="ReportUtils.getReportByFormat();"><span class="Sprite2">Xuất</span></button>
                 	<button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2"  onclick="$('.easyui-dialog').dialog('close');"><span class="Sprite2">Đóng</span></button>
				</div>
				<div id="msgRowDisplay" class="BoxDialogAlert" style="display: none"><p class="ErrorMsgStyle"></p></div>
				 <div id="msgErrDlg" class="BoxDialogAlert" style="display: none"><p class="ErrorMsgStyle"></p></div>				
             <s:hidden id="rptUrl" value="" />
     		<s:hidden id="typeReport" value="" />						
				
			</div>
		</div>
	</div>
</div>

 <div id="payrollPeriodSearch" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralMilkBox">
                  <div class="GeneralMilkTopBox">
                      <div class="GeneralMilkBtmBox">
                          <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox ResearchSection">
                         	<div class="SearchSales4Form">                                 
                                 <label class="LabelStyle Label1Style">Mã kỳ lương</label>
                                 <input id="periodCodeSearch" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText1Style" />
                                 <label class="LabelStyle Label2Style">Tên kỳ lương</label>
                                 <input id="periodNameSearch" type="text" maxlength="250" tabindex="2" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <label class="LabelStyle Label1Style">Từ tháng</label>
                                 <input id="periodFromDateSearch" type="text" maxlength="10" tabindex="3" style="width: 100px;" class="InputTextStyle InputText1Style" />
                                 <label class="LabelStyle Label2Style">Đến tháng</label>
                                 <input id="periodToDateSearch" type="text" maxlength="10" style="width: 100px;" tabindex="4" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <div class="ButtonSection">
                                     <button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" tabindex="5" id="btnSearchPeriod"><span class="Sprite2">Tìm kiếm</span></button>
                                 </div>
                                 <p class="ErrorMsgStyle Sprite1" id="errPeriodMsg" style="display: none;"></p>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="GeneralMilkBox">
                 <div class="GeneralMilkTopBox">
                     <div class="GeneralMilkBtmBox">
                         <h3 class="Sprite2"><span class="Sprite2">Kết quả tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox">
	                         <div id="searchStyle1ScrollBody" class="">
	                         	<div class="ResultSection" id="searchPeriodContainerGrid">                                 
	                                <table id="searchPeriodGrid"></table>
									<div id="searchPeriodPager"></div>
	                             </div>
	                         </div>                             
                         </div>
                     </div>
                 </div>
             </div>
             <div class="BoxDialogBtm">
                 <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
             </div>
     	</div>
     </div>
 </div>
 <div id="fancybox-nvbh-1" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralMilkBox">
                  <div class="GeneralMilkTopBox">
                      <div class="GeneralMilkBtmBox">
                          <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox ResearchSection">
                         	<div class="SearchSales4Form">                                 
                                 <label id="seachStyle1CodeLabel" class="LabelStyle Label1Style">Mã</label>
                                 <input id="seachStyle1Code" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText1Style" />
                                 <label id="seachStyle1NameLabel" class="LabelStyle Label2Style">Tên</label>
                                 <input id="seachStyle1Name" type="text" maxlength="250" tabindex="2" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <div class="ButtonSection">
                                     <button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" tabindex="4" id="btnSearchStyle1"><span class="Sprite2">Tìm kiếm</span></button>
                                 </div>
                                 <s:hidden id="my-searchStyle1Url"></s:hidden>
                                 <s:hidden id="my-searchStyle1CodeText"></s:hidden>
                                 <s:hidden id="my-searchStyle1NameText"></s:hidden>
                                 <s:hidden id="my-searchStyle1AddressText"></s:hidden>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="GeneralMilkBox">
                 <div class="GeneralMilkTopBox">
                     <div class="GeneralMilkBtmBox">
                         <h3 class="Sprite2"><span class="Sprite2">Kết quả tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox">
	                         <div id="searchStyle1ScrollBody" class="">
	                         	<div class="ResultSection" id="my-searchStyle1ContainerGrid">                                 
	                                <table id="my-searchStyle1Grid"></table>
									<div id="my-searchStyle1Pager"></div>
	                             </div>
	                         </div>                             
                         </div>
                     </div>
                 </div>
             </div>
             <div class="BoxDialogBtm">
             	 <div class="ButtonSection">
             	 	<button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" id="myFancyboxSelect"><span class="Sprite2">Chọn</span></button>
                 	<button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" id="myFancyboxClose" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button>
                 </div>
             </div>
     	</div>
     </div>
     <s:hidden id="loai"></s:hidden>
 </div>
 <div id="searchProduct" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralMilkBox">
                  <div class="GeneralMilkTopBox">
                      <div class="GeneralMilkBtmBox">
                          <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox ResearchSection">
                         	<div class="SearchSales4Form">                                 
                                 <label id="searchProductCodeLabel" class="LabelStyle Label1Style">Mã</label>
                                 <input id="searchProductCode" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText1Style" />
                                 <label id="searchProductNameLabel" class="LabelStyle Label2Style">Tên</label>
                                 <input id="searchProductName" type="text" maxlength="250" tabindex="2" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <label id="searchProductAddressLabel" style="display: none;" class="LabelStyle Label1Style">Địa chỉ</label>
                                 <input id="searchProductAddress" type="text" style="display: none;" maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                                 <div class="Clear"></div>
                                 <div class="ButtonSection">
                                     <button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" tabindex="4" id="btnSearchProduct"><span class="Sprite2">Tìm kiếm</span></button>
                                 </div>
                                 <s:hidden id="searchProductUrl"></s:hidden>
                                 <s:hidden id="searchProductCodeText"></s:hidden>
                                 <s:hidden id="searchProductNameText"></s:hidden>
                                 <s:hidden id="searchProductAddressText"></s:hidden>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="GeneralMilkBox">
                 <div class="GeneralMilkTopBox">
                     <div class="GeneralMilkBtmBox">
                         <h3 class="Sprite2"><span class="Sprite2">Kết quả tìm kiếm</span></h3>
                         <div class="GeneralMilkInBox">
	                         <div id="searchProductScrollBody" class="">
	                         	<div class="ResultSection" id="searchProductContainerGrid">                                 
	                                <table id="searchProductGrid"></table>
									<div id="searchProductPager"></div>
	                             </div>
	                         </div>                             
                         </div>
                     </div>
                 </div>
             </div>
             <div class="BoxDialogBtm">
                 <div class="ButtonSection">
                 <button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" id="productSelect"><span class="Sprite2">Chọn</span></button>
                 <button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
             </div>
             <p class="ErrorMsgStyle Sprite1" id="errProDiaMsg" style="display: none;"></p>
     	</div>
     </div>
 </div>
 
 
<!-- Thong NM  -->
<div id ="searchStyle2EasyUIDialogDiv" style="display:none; width:630px; height: auto;" >
	<div id="searchStyle2EasyUIDialog" data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">				
				<label id="searchStyle2EasyUICodeLabel" class="LabelStyle Label2Style">Mã</label>
				<input id="searchStyle2EasyUICode" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText5Style" /> 
				<label id="searchStyle2EasyUINameLabel" class="LabelStyle Label2Style">Tên</label> 
				<input id="searchStyle2EasyUIName" type="text" maxlength="250" tabindex="2"  class="InputTextStyle InputText5Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnSearchOnDialog" tabindex="3" id="btnSearchStyle2EasyUI">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
				<s:hidden id="searchStyle2EasyUIUrl"></s:hidden>
				<s:hidden id="searchStyle2EasyUICodeText"></s:hidden>
				<s:hidden id="searchStyle2EasyUINameText"></s:hidden>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle" id="searchStyle2EasyUIContainerTitle">Kết quả tìm kiếm </h2>
				<div class="GridSection" id="searchStyle2EasyUIContainerGrid">
					<table id="searchStyle2EasyUIGrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" style="display: none;" id="btnSearchStyle2EasyUIUpdate">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" id="btnSearchStyle2EasyUIClose" onclick="$('#searchStyle2EasyUIDialog').dialog('destroy');">Đóng</button>
				</div>
			</div>
			<p id="errMsgSearchStyle2EasyUI" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
		</div>
	</div>
</div> --%>

<!-- Begin Vuongmq; 24/08/2015; popup change pass  -->
<div id ="searchStyle1EasyUIDialogChangePassDivChangePass" style="visibility: hidden;width:600px" >
	<div id="searchStyle1EasyUIDialogChangePass" style="overflow:hidden" class="easyui-dialog" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid" style="overflow:hidden">
			<div class="GeneralForm Search1Form">
				<!-- <h3 class="Title2Style">Change Pass</h3> -->
				<div class="Clear" style="margin-top:8px"></div>
				<label id="seachStyle1CodeLabel" style="float: left; width: 135px; color: #215ea2; padding: 2px 0px 2px 20px">Mật khẩu cũ <span class="ReqiureStyle"> *</span></label>
				<input id="seachStyle1PassOld" maxlength="100" tabindex="1" type="password" class="InputTextStyle InputText1Style" style="width:200px; margin-left:10px" />
				<div class="Clear"></div>
				<label id="seachStyle1NameLabel" style="float: left; width: 135px; color: #215ea2; padding: 2px 0px 2px 20px" >Mật khẩu mới <span class="ReqiureStyle"> *</span></label> 
				<input id="seachStyle1PassNew" maxlength="100" tabindex="2" type="password"  class="InputTextStyle InputText1Style" style="width:200px; margin-left:10px" />
				<div class="Clear"></div>
				<label id="seachStyle1NameLabel"  style="float: left; width: 135px; color: #215ea2; padding: 2px 0px 2px 20px" >Nhập lại mật khẩu mới <span class="ReqiureStyle"> *</span></label> 
				<input id="seachStyle1PassRetype" maxlength="100" tabindex="3" type="password" class="InputTextStyle InputText1Style" style="width:200px; margin-left: 10px" />
				<div class="Clear"></div>
				<div class="FixFloat FieldLogin" style="padding:10px 18px">
                    <label class="LabelRStyle"><font size="2" color="red">Để an toàn, mật khẩu mới bắt buộc phải có ít nhất 6 ký tự, có chữ hoa và chữ thường.</font>
                    </label>	
                </div>
				<div class="BtnCenterSection">
					<button id="__btnSaveChangePass" class="BtnGeneralStyle" tabindex="4" onclick="return UnitTreeCatalog.changePassPopupUser();">Cập nhật</button>
					<button id="__btnCancelChangePass" class="BtnGeneralStyle BtnGeneralMStyle" tabindex="5" onclick="$('.easyui-dialog').dialog('close');">Bỏ qua</button>
				</div>
				<p id="errMsgChangePass" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				<p id="successMsgChangePass" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
</div>
<!-- End Vuongmq; 24/08/2015; popup change pass  -->
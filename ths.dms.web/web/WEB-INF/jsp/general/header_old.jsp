<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<style type="text/css">
#header .MenuSection ul{
	list-style: none;
	font-family: Arial, san-serif;
	font-size: 12px;
	padding: 0;
}
#header .MenuSection ul li{
	float:left;
	position:relative;
	/*font-size: 12px;*/
}
#header .MenuSection ul a{
	text-decoration:none;
	/*color:#fff;*/
	display:block;
	/*font-weight:bold;*/
	/*padding:6px 10px;*/
	padding-left:10px;
	padding-right:10px;
	background-color:transparent;
	font-size: 12.5px;
}
#header .MenuSection ul a:hover, #header .MenuSection li:hover > a {
	background-color:#71B7ED;
	color:#fff;
}
#header .MenuSection li ul{
	position:absolute;
	display:none;
	width:200px;
}
#header .MenuSection li li {
	width: 100%;
}
#header .MenuSection, #header .MenuSection li ul a{
	/*background:#2882BC;
	color:#fff;*/
}
#header .MenuSection li:hover ul {
	display:block;
}
#header .MenuSection li:hover ul li ul{
	display:none;
}
#header .MenuSection li:hover ul li:hover ul {
	display:block;
	position:absolute;
	left:200px;
	top:0;
}
#header .MenuList li a {
	text-shadow:none;
}
/*#header .MenuList ul ul li {
	border-top: none;
	border-left:1px solid #45b5db;
	border-right:1px solid #45b5db;
	border-bottom:1px solid #45b5db;
}*/
#header .MenuList ul ul {
	border-top:none;
	border-bottom:1px solid #45b5db;
	border-left:1px solid #45b5db;
	border-right:1px solid #45b5db;
}
#header .MenuList .SubMenuList li a.HasSubMenuItem {
	background-image:url(/resources/images/icon_submenu2.png);
	background-position:right 5px center;
	background-repeat:no-repeat;
}
#header ul.MenuList > li > a {
	font-size: 13px;
}
</style>

<div class="HeaderBgSection">
	<span id="logo">
		<a href="#">
			<span class="HideText DmsOneLogo">DMS One</span> 
			<%-- <span class="HideText Sprite1 VinamilkLogo"></span> --%>
			<span class="HideText Sprite1"></span>
		</a>
	</span>
<!-- 	<div class="AccountFuncSection"> -->
<!-- 		<ul class="ResetList AccountFuncList"> -->
<%-- 			<li><span id="shopLockDate">Ngày làm việc: <s:property value="lockDate"/></span></li> --%>
<%-- 			<li><span style="margin-left: 10px;"><s:property value="getInfoHeader()"/></span></li> --%>
<%-- 			<li><span style="margin-left: 10px;" id = "spHeaderShopInf">Đơn vị: <s:property value="getCurrentShopInSession().shopCode"/> - <s:property value="getCurrentShopInSession().shopName"/></span></li> --%>
<!-- 			<li class="LogoutItem"><a href="/commons/logout" class="Sprite1">Thoát</a></li> -->
<!-- 		</ul> -->
<!-- 	</div> -->
	<div class="AccountFuncSection">
		<ul class="ResetList AccountFuncList">
<!-- 		trungtm6 comment -->
<%-- 			<li><span id="shopLockDate">Ngày làm việc: <s:property value="lockDate"/></span></li> --%>

			<s:if test="checkInheritUserPrivByUserLogin () == 1">
				<li><span style="margin-left: 10px;"><s:property value="getInfoHeader()"/></span></li>
			</s:if>
			<s:else>
				<li><span style="margin-left: 10px; color: #ffd184;" title="Bạn đang sử dụng quyền thay thế"><s:property value="getInfoHeader()"/></span></li>
			</s:else>
			<s:if test="checkShopRootSingle () == 0">
				<li><span style="margin-left: 10px;" id ="spHeaderShopInf" title="Click chuột để thay đổi Đơn vị">Đơn vị:<a href="javascript:void(0);" style="color: #ffd184;" onclick="General.openDialogSelectShopIsChange();"> <s:property value="getCurrentShopInSession().shopCode"/> - <s:property value="getCurrentShopInSession().shopName"/></a></span></li>
			</s:if>
			<s:else>
				<li><span style="margin-left: 10px;" id ="spHeaderShopInf">Đơn vị: <s:property value="getCurrentShopInSession().shopCode"/> - <s:property value="getCurrentShopInSession().shopName"/></span></li>
			</s:else>
			<!-- <li class="LogoutItem"><a href="/commons/logout" class="Sprite1">Thoát</a></li> -->
			<li class="LogoutItem"><a href="javascript:void(0);" onclick="setTimeout(function(){isCloseInfoUser=1;$('.AccOption').toggle();},200);" class="Sprite1"><span class="HideText">Menu</span></a></li>
		</ul>
		<div class="AccOption" style="display:none;" onclick="isCloseInfoUser=1;">
	        <ul class="ResetList ChildMenuSectionList" >
				<li class="Item1"><a href="#" class="Sprite1 Item1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                 <li>
                 	<div class="InfoShop FixFloat">
                        <img width="50" height="50" src="<s:property value="getImageCurrentUser()"/>" class="Avatar"/>
                        <p class="Text1Style"><s:property value="currentUser.userName"/></p>
	                    <%-- <p class="Text1Style"><s:property value="getCodeCurrentUser()"/></p> --%>
                     </div>
                 </li>
                 <li><a href="javascript:void(0);" onclick="changePassPopup();" class="">Đổi mật khẩu</a></li>
                 <li class="EndItem"><a href="/commons/logout" onclick="$('.AccOption').hide();return true;">Thoát</a></li>
             </ul>
	    </div>
	</div>
	<div class="Clear"></div>
</div>
<div class="MenuSection">
	<ul class="ResetList MenuList">
		<s:iterator id="permission" value=" #session.cmsUserToken.listMenu">
<%--         	<s:if test='!#permission.objectName.equals("SUB_FUN")'> --%>
       		<li id="<s:property value="#permission.formId"/>">        		
       		<s:if test='#permission.url!= null && #permission.url.length()>0'>
       			<a  href="<s:property value="#permission.url"/>"><s:property value="#permission.formName"/></a>
       		</s:if>
       		<s:else>
       			<a  href="javascript:void(0);"><s:property value="#permission.formName"/></a>
       		</s:else>					
				<s:if test="#permission.listForm != null && #permission.listForm.size() > 0">
					<ul class="ResetList SubMenuList" id="sub_<s:property value="#permission.formId"/>">
			   			<s:iterator id="childFunction" value="#permission.listForm" status="u">
			   				<s:if test="#childFunction.listForm !=null && #childFunction.listForm.size()>0">
			   					<li>
									<a class="HasSubMenuItem" id="child_<s:property value="#childFunction.formId"/>" href="javascript:void(0);" ><s:property value="#childFunction.formName"/></a>									
									<ul class="ResetList SubMenuList" id="sub_Sub_<s:property value="#childFunction.formId"/>">										
										<s:iterator id="childChildFunction" value="#childFunction.listForm" status="v">
											<li>
												<a id="child_Child_<s:property value="#childChildFunction.formId"/>" href="<s:property value="#childChildFunction.url"/>" ><s:property value="#childChildFunction.formName"/></a>
											</li>
										</s:iterator>
									</ul>																				    		
								</li>
			   				</s:if>
			   				<s:else>
			   					<li>
									<a id="child_<s:property value="#childFunction.formId"/>" href="<s:property value="#childFunction.url"/>" ><s:property value="#childFunction.formName"/></a>								
								</li>
			   				</s:else>			   				
		               </s:iterator>
	               </ul>
	            </s:if>
			</li>
<%--         	</s:if>			 --%>
	   	</s:iterator>
	</ul>
	<div class="Clear"></div>
</div>
<script type="text/javascript">
	/**
	* function thay doi pass cua CurrentUser khi dang nhap thanh cong
	* @author vuongmq
	* @since 24/08/2015
	*/
	function changePassPopup() {
		$('.ErrorMsgStyle').html('').hide();
		$('#searchStyle1EasyUIDialogChangePassDivChangePass').css('visibility','visible');
		$('#searchStyle1EasyUIDialogChangePass').dialog({
			title: 'Đổi mật khẩu',
			width: 426,
			height: 280,
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassOld').focus();
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassOld').val('');
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassNew').val('');
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassRetype').val('');
				//$('.easyui-dialog #seachStyle1Code')
				$(window).bind('keyup', function(event) {
					if (event.keyCode == 13) {
						$('#__btnSaveChangePass').click();
					}
				});
			},
			onClose : function() {
				$('#searchStyle1EasyUIDialogChangePassDivChangePass').css("visibility", "hidden");
			}
		});
	}
	$(window).bind('click', function() {
		if (isCloseInfoUser != undefined && isCloseInfoUser != null && isCloseInfoUser == 0) {
			$('.AccOption').hide();
		} else {
			isCloseInfoUser = 0;
		}
	});
</script>
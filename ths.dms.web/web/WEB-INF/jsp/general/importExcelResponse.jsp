<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div id="errorExcel" style="display: none"><s:property value="isError"/></div>
<div id="errorExcelMsg" style="display: none"><s:property value="errMsg"/></div>
<div id="totalRow" style="display: none"><s:property value="totalItem"/></div>
<div id="numFail" style="display: none"><s:property value="numFail"/></div>
<div id="numFinish" style="display: none"><s:property value="numFinish"/></div>
<div id="fileNameFail" style="display: none"><s:property value="fileNameFail"/></div>
<div id="typeView" style="display: none"><s:property value="typeView"/></div>
<div id="lstSize" style="display: none"><s:property value="lstView.size()"/></div>
<div id="totalItemSuccess" style="display: none"><s:property value="totalItemSuccess"/></div>
<input type="hidden" id="newToken" value='<s:property value="token"/>' name="newToken">
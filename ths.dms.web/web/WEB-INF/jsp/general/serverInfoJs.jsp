<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.struts2.ServletActionContext" %>
<%@page import="ths.dms.helper.Configuration"%>
<s:hidden name="token" id="token"></s:hidden>
<s:hidden value="0" id="valueIndex"></s:hidden>

<s:hidden id="permissionUser" name="headOfficePermission"></s:hidden>

<s:hidden id="locked" name="shopLocked"></s:hidden>

<input type="hidden" id="typeAttribute" value=""/>
<input type="hidden" id="tabAttribute" value=""/>
<script type="text/javascript">
var _MapControl = new Map();
var _isFullPrivilege = false;
var yearPeriodChange = null;
var excel_template_path ='<%=ServletActionContext.getServletContext().getContextPath() +Configuration.getExcelTemplatePath()%>';
var DEBUG = false;
var activeStatus = 'RUNNING';
var stoppedStatus = 'STOPPED';
var waitingStatus = 'WAITING';
var activeStatusText = 'Hoạt động';
var inActiveStatusText = 'Tạm ngưng';
var DA_DUYET = 'Đã duyệt';
var DU_THAO = 'Dự thảo';
var ShopDecentralizationSTT = {VNM:1, KENH:2, MIEN:3, VUNG:4, NPP:5, UNALL:6, ALL:-2};
var ShopTreeNode = {ROOT: 1, IDP: 3, KENH4: 4, KENH21: 21, SUBMIEN18: 18, SUBMIEN22: 22, MIEN: 5, VUNG: 6, NPP: 7};
var keyCodes = {F1: 112, F2: 113, F3: 114, F4: 115, F6: 117, F8: 119, ENTER: 13, Alt: 18, BACK_SPACE: 8, DELETE: 46, TAB: 9, F5: 116, F9: 120, ESCAPE: 27, CTRL:17, SHIFT:16, HOME:36, END:35, ARROW_LEFT:37, ARROW_RIGHT:39, ARROW_UP:38, ARROW_DOWN:40};
var keyups = {AltPlus: 61, Alt_n: 78, Alt_c: 67, Alt_d: 68, Alt_r: 82, Alt_x: 88};
var characterValue = {A: 65, B: 66, C: 67, D: 68, E: 69, F: 70, G:71, H:72, I:73, J:74, K:75, L:76, M:77, N:78,O:79,P:80,Q:81,R:82,S:83,T:84,U:85,V:86,W:87,X:88,Y:89,Z:90};
var keyCombine = [65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90];
var finalFixed = {
	ALL: -2	
};
var SpecificType = {
	NPP: 1, 
	parseValue: function (value) {
		if (value.trim() == 'NPP') {
			return SpecificType.NPP;
		}
		return null;
	}
};
var StatusType = {ALL: -2, ACTIVE: 1, INACTIVE: 0};
var activeType = {ALL: -2, DELETE: -1, STOPPED: 0, RUNNING: 1, WAITING: 2,
		parseValue: function(value){
			if(value.trim() == 'STOPPED'){
				return activeType.STOPPED;
			} else if(value.trim() == 'RUNNING'){
				return activeType.RUNNING;
			} else if(value.trim() == 'WAITING'){
				return activeType.WAITING;
			}			
			return -2;
		},
		parseNametoText: function(name) {
			if (!name) {
				"";
			}
			name = name.trim();
			if (stoppedStatus == name) {
				return inActiveStatusText;
			}
			if (activeStatus == name) {
				return activeStatusText;
			}
			if (waitingStatus = name) {
				return waitingStatusText;
			}
			return "";
		},
		checkIsRecode: function(isEvent){
			if(isEvent == undefined || isEvent == null){
				return false;
			}
			if(isEvent!= activeType.DELETE && isEvent != activeType.STOPPED && isEvent != activeType.RUNNINGisEvent != activeType.WAITING){
				return false;
			}
			return true;
		},
		parseText: function(value) {
			if (value == undefined || value == null) {
				return '';
			} else if (value == activeType.RUNNING) {
				return 'Hoạt động';
			} else if (value == activeType.WAITING) {
				return 'Dự thảo';
			} else if (value == activeType.STOPPED) {
				return 'Tạm ngưng';
			}
			return '';
		}
	};
// vuongmq; 28/12/2015
var ActionType = {INSERT: 0, UPDATE: 1, DELETE: 2};
var ApprovedText = {ALL: -2, STOPPED: 0, RUNNING: 1, WAITING: 2,
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			}else if(value == ApprovedText.ALL){
				return 'Tất cả';
			} else if(value == ApprovedText.RUNNING){
				return 'Đã duyệt';
			} else if(value == ApprovedText.WAITING){
				return 'Dự thảo';
			} else if(value == ApprovedText.STOPPED){
				return 'Chưa duyệt';
			}
			return '';
		}};
var statusTypeText = {ALL: -2, STOPPED: 0, RUNNING: 1, WAITING: 2,
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			}else if(value == statusTypeText.ALL){
				return '<s:text name="price_combo_all"/>';
			} else if(value == statusTypeText.RUNNING){
				return '<s:text name="common.status.active"/>';
			} else if(value == statusTypeText.WAITING){
				return '<s:text name="action.status.waiting"/>';
			} else if(value == statusTypeText.STOPPED){
				return '<s:text name="pause.status.name"/>';
			}
			return '';
		}};
var equipStatusText = {ALL: -2, INACTIVE: 0, ACTIVE: 1,
		parseValue: function(value){
			if(value == undefined || value == null){
				return '';
			}else if(value == equipStatusText.ALL){
				return 'Tất cả';
			} else if(value == equipStatusText.ACTIVE){
				return 'ACTIVE';
			} else if(value == equipStatusText.INACTIVE){
				return 'INACTIVE';
			}
			return '';
		}};
var defaultType = {DEFAULT: 1, NOTDEFAULT: 0, 
		parseValue: function(value){
			if(value.trim() == 'DEFAULT'){
				return activeType.DEFAULT;
			} else if(value.trim() == 'NOTDEFAULT'){
				return activeType.NOTDEFAULT;
			}			
			return -2;
		}};
var nodeType = {SHOP: 1, STAFF: 2, 
		parseValue: function(value){
			if(value.trim() == 'SHOP'){
				return nodeType.SHOP;
			} else if(value.trim() == 'STAFF'){
				return nodeType.STAFF;
			}			
			return -2;
		}};
var cycleCountType = {ONGOING: 0, COMPLETED: 1, CANCELLED: 2, REJECTED: 3, WAIT_APPROVED: 4, NOT_COUNTING: 5,
		parseValue: function(value) {
			if (value.trim() == 'ONGOING') {
				return cycleCountType.ONGOING;
			} else if (value.trim() == 'COMPLETED') {
				return cycleCountType.COMPLETED;
			} else if (value.trim() == 'CANCELLED') {
				return cycleCountType.CANCELLED;
			} else if (value.trim() == 'REJECTED') {
				return cycleCountType.REJECTED;
			} else if (value.trim() == 'WAIT_APPROVED') {
				return cycleCountType.WAIT_APPROVED;
			} else if (value.trim() == 'NOT_COUNTING') {
				return cycleCountType.NOT_COUNTING;
			}		
			return -2;
		}};
var warehouseType = {SALES: 0, PROMOTION: 1,
		parseValue: function(value){
			if(value == 'SALES' || value == warehouseType.SALES){
				return '<s:text name="stock_manage_kho_ban"/>';
			} else if(value == 'PROMOTION' || value == warehouseType.PROMOTION){
				return '<s:text name="stock_manage_kho_km"/>';
			}
			return '';
		}};
var ProgramType = {AUTO_PROM: 0, MANUAL_PROM: 1, DISPLAY_SCORE:2, PRODUCT_EXCHANGE:3, DESTROY:4, RETURN:5, KEY_SHOP:6,
		parseValue: function(value){
			if(value == 'SALES' || value == warehouseType.SALES){
				return '<s:text name="stock_manage_kho_ban"/>';
			} else if(value == 'PROMOTION' || value == warehouseType.PROMOTION){
				return '<s:text name="stock_manage_kho_km"/>';
			}
			return '';
		}};
var ProductType = {
		CAT: 1,
	    SUB_CAT: 2,
	    BRAND: 3,
	    FLAVOUR: 4,
	    PACKING: 5,
	    PRODUCT: 6,
	    SECOND_SUB_CAT: 7
};
// vuongmq, 15/11/2015; them trang thai cho feedbackStaff
var FeedbackStatus = {NEW: 0, COMPLETE: 1, APPROVED: 2,
		parseValue: function(value) {
			if (value == undefined || value == null) {
				return '';
			} else if (value == FeedbackStatus.NEW) {
				return 'Chưa thực hiện';
			} else if (value == FeedbackStatus.COMPLETE) {
				return 'Đã thực hiện';
			} else if (value == FeedbackStatus.APPROVED) {
				return 'Đã duyệt';
			}
			return '';
		}};
// vuongmq, 10/08/2015; them cac loai type, status cho po_vnm
var ProductTypeVNM = {ALL: -2, SALE: 1, PROMOTION: 2, POSM: 3,
		parseValue: function(value){
			if (value == undefined || value == null) {
				return '';
			} else if (value == ProductTypeVNM.SALE) {
				return product_type_ban;
			} else if (value == ProductTypeVNM.PROMOTION) {
				return product_type_km;
			} else if (value == ProductTypeVNM.POSM) {
				return product_type_posm;
			}
			return '';
		}};
// PO_CUSTOMER_SERVICE(1) PO DV nhap; PO_CONFIRM(2) PO confirm nhap; RETURNED_SALES_ORDER(3) PO confirm tra ; PO_CUSTOMER_SERVICE_RETURN(4) PO DV tra
var PoType = {PO_CUSTOMER_SERVICE: 1, PO_CONFIRM: 2, RETURNED_SALES_ORDER: 3, PO_CUSTOMER_SERVICE_RETURN: 4,
		parseValue: function(value){
			if (value == undefined || value == null) {
				return -1;
			} else if (value == 'PO_CUSTOMER_SERVICE') {
				return PoType.PO_CUSTOMER_SERVICE;
			} else if (value == 'PO_CONFIRM') {
				return PoType.PO_CONFIRM;
			} else if (value == 'RETURNED_SALES_ORDER') {
				return PoType.RETURNED_SALES_ORDER;
			} else if (value == 'PO_CUSTOMER_SERVICE_RETURN') {
				return PoType.PO_CUSTOMER_SERVICE_RETURN;
			}
			return -1;
		}};
var PoTypeText = {PO_CUSTOMER_SERVICE: 1, PO_CONFIRM: 2, RETURNED_SALES_ORDER: 3, PO_CUSTOMER_SERVICE_RETURN: 4,
		parseValue: function(value){
			if (value == undefined || value == null) {
				return '';
			} else if (value == PoTypeText.PO_CUSTOMER_SERVICE || value == PoTypeText.PO_CONFIRM) {
				return povnm_type_input;  // đơn nhập
			} else if (value == PoTypeText.RETURNED_SALES_ORDER || value == PoTypeText.PO_CUSTOMER_SERVICE_RETURN) {
				return povnm_type_output; // đơn trả
			}
			return '';
		}};
// // Chua nhap NOT_IMPORT(0), // Dang nhap IMPORTING(1), // Da nhap/ tra xong 	IMPORTED(2), // Co the treo PENDING(3), // Da treo SUSPEND(4);
var PoStatus = {NOT_IMPORT: 0, IMPORTING: 1, IMPORTED: 2, PENDING: 3, SUSPEND: 4,
		parseValue: function(value){
			if (value == undefined || value == null) {
				return -1;
			} else if (value == 'NOT_IMPORT') {
				return PoStatus.NOT_IMPORT;
			} else if (value == 'IMPORTING') {
				return PoStatus.IMPORTING;
			} else if (value == 'IMPORTED') {
				return PoStatus.IMPORTED;
			} else if (value == 'PENDING') {
				return PoStatus.PENDING;
			} else if (value == 'SUSPEND') {
				return PoStatus.SUSPEND;
			}
			return -1;
		}};
// vuongmq; 10/12/2015
var PoManualType = {PO_IMPORT: 1, PO_RETURN: 2,
		parseValue: function(value) {
			if (value == undefined || value == null) {
				return '';
			} else if (value == PoManualType.PO_IMPORT) {
				return 'Đơn đặt hàng'
			} else if (value == PoManualType.PO_RETURN) {
				return 'Đơn trả hàng';
			}
			return '';
		}};
// vuongmq; 18/12/2015
var ApprovalStepPo = {NEW: 1, WAITING: 2, APPROVED: 3, COMPLETED: 4, REJECTED: 5, DESTROY: 6,
		parseValue: function(value) {
			if (value == undefined || value == null) {
				return -1;
			} else if (value == 'NEW') {
				return ApprovalStepPo.NEW;
			} else if (value == 'WAITING') {
				return ApprovalStepPo.WAITING;
			} else if (value == 'APPROVED') {
				return ApprovalStepPo.APPROVED;
			} else if (value == 'COMPLETED') {
				return ApprovalStepPo.COMPLETED;
			} else if (value == 'REJECTED') {
				return ApprovalStepPo.REJECTED;
			} else if (value == 'DESTROY') {
				return ApprovalStepPo.DESTROY;
			}
			return -1;
		}};
var PoStatusTextOfType = {NOT_IMPORT: 0, IMPORTING: 1, IMPORTED: 2, PENDING: 3, SUSPEND: 4,
		parseValue: function(value, type){
			if (value == undefined || value == null || type == undefined || type == null) {
				return '';
			} else if (value == PoStatusTextOfType.NOT_IMPORT) {
				if (type == PoTypeText.PO_CUSTOMER_SERVICE || type == PoTypeText.PO_CONFIRM) {
					return po_vnm_status_not_import;	//return 'Chưa nhập';
				} else if (type == PoTypeText.RETURNED_SALES_ORDER || type == PoTypeText.PO_CUSTOMER_SERVICE_RETURN) {
					return po_vnm_status_not_import_output; //return 'Chưa trả';
				}
			} else if (value == PoStatusTextOfType.IMPORTING) {
				if (type == PoTypeText.PO_CUSTOMER_SERVICE || type == PoTypeText.PO_CONFIRM) {
					return po_vnm_status_importing; //return 'Đã nhập một phần';
				} else if (type == PoTypeText.RETURNED_SALES_ORDER || type == PoTypeText.PO_CUSTOMER_SERVICE_RETURN) {
					return po_vnm_status_importing_output; //return 'Đã trả một phần';
				}
			} else if (value == PoStatusTextOfType.IMPORTED) {
				if (type == PoTypeText.PO_CUSTOMER_SERVICE || type == PoTypeText.PO_CONFIRM) {
					return po_vnm_status_imported; //return 'Đã nhập';
				} else if (type == PoTypeText.RETURNED_SALES_ORDER || type == PoTypeText.PO_CUSTOMER_SERVICE_RETURN) {
					return po_vnm_status_imported_output; //return 'Đã trả';
				}
			} else if (value == PoStatusTextOfType.PENDING) {
				if (type == PoTypeText.PO_CUSTOMER_SERVICE || type == PoTypeText.PO_CONFIRM) {
					if (type == PoTypeText.PO_CUSTOMER_SERVICE) {
						return po_vnm_status_pending; //return 'Có thể treo';
					} else {
						return confirm_order_huy;// huy
					}
				} else if (type == PoTypeText.RETURNED_SALES_ORDER || type == PoTypeText.PO_CUSTOMER_SERVICE_RETURN) {
					if (type == PoTypeText.PO_CUSTOMER_SERVICE_RETURN) {
						return po_vnm_status_pending; //return 'Có thể treo';
					} else {
						return confirm_order_huy;// huy
					}
				}
			} else if (value == PoStatusTextOfType.SUSPEND) {
				if (type == PoTypeText.PO_CUSTOMER_SERVICE || type == PoTypeText.PO_CONFIRM) {
					return po_vnm_status_suspend; // return 'Đã treo';
				} else if (type == PoTypeText.RETURNED_SALES_ORDER || type == PoTypeText.PO_CUSTOMER_SERVICE_RETURN) {
					return po_vnm_status_suspend;
				}
			}
			return '';
		}};
//vuongmq; 19/06/2015 thanh toan sua chua
var PaymentStatusRepair = {NOT_PAID_YET: 0, PAID_TRANSACTION: 1, PAID: 2,
		parseValue: function(value) {
			if (value == undefined || value == null) {
				return '';
			} else if(value == PaymentStatusRepair.NOT_PAID_YET) {
				return 'Chưa thanh toán';
			} else if(value == PaymentStatusRepair.PAID_TRANSACTION) {
				return 'Đang tham gia thanh toán';
			} else if(value == PaymentStatusRepair.PAID) {
				return 'Đã thanh toán';
			}
			return '';
		}};		
//vuongmq; 19/06/2015 trang thai sua chua; dang dung thanh toan sua chua
var StatusRecordsEquip = {DRAFT: 0, WAITING_APPROVAL: 1, APPROVED: 2, NO_APPROVAL: 3, CANCELLATION: 4, ARE_CORRECTED: 5, COMPLETION_REPAIRS: 6, LIQUIDATED: 7,
		parseValue: function(value) {
			if (value == undefined || value == null) {
				return '';
			} else if(value == StatusRecordsEquip.DRAFT) {
				return 'Dự thảo';
			} else if(value == StatusRecordsEquip.WAITING_APPROVAL) {
				return 'Chờ duyệt';
			} else if(value == StatusRecordsEquip.APPROVED) {
				return 'Đã duyệt';
			} else if(value == StatusRecordsEquip.NO_APPROVAL) {
				return 'Không duyệt';
			} else if(value == StatusRecordsEquip.CANCELLATION) {
				return 'Hủy';
			} else if(value == StatusRecordsEquip.ARE_CORRECTED) {
				return 'Đang sửa chữa';
			} else if(value == StatusRecordsEquip.COMPLETION_REPAIRS) {
				return 'Hoàn tất';
			} else if(value == StatusRecordsEquip.LIQUIDATED) {
				return 'Thanh lý';
			}
			return '';
		}};	
//nhutnn; 02/07/2015 trang thai giao nhan
var StatusDeliveryEquip = {
	NOT_EXPORT: 0,
	EXPORTED: 1,
	parseValue: function(value) {
		if (value == undefined || value == null) {
			return '';
		} else if (value == StatusDeliveryEquip.NOT_EXPORT) {
			return 'Chưa xuất';
		} else if (value == StatusDeliveryEquip.EXPORTED) {
			return 'Đã xuất';
		} 
		return '';
	}
};
//nhutnn; 02/07/2015 trang thai giao nhan
var TypeStockEquipLend = {
	NCC: 1,
	COMPANY: 2,
	parseValue: function(value) {
		if (value == undefined || value == null) {
			return '';
		} else if (value == TypeStockEquipLend.NCC) {
			return 'Kho NCC';
		} else if (value == TypeStockEquipLend.COMPANY) {
			return 'Kho công ty';
		} 
		return '';
	},
	parseValueNPP: function(value) {
		if (value == undefined || value == null) {
			return '';
		} else if (value == TypeStockEquipLend.NCC) {
			return 'Kho NPP';
		} else if (value == TypeStockEquipLend.COMPANY) {
			return 'Kho công ty';
		} 
		return '';
	}
};
var svn_revision_number = '<%=Configuration.FILE_VERSION_NUMBER%>';
var StaffRoleType = {NVBH:1, NVVS:2, NVTT:3, NVGH:4, NVGS:5, TBHM:6, TBHV:7, NHVNM:8, KTNPP:9, NVBHANDNVVS:1012};
var StaffSpecType = {VIETTEL_ADMIN: 0, STAFF:1, SUPERVISOR:2, MANAGER:3, NVGH:4, NVTT:5, RSM:6};
var focusProductType = {
	MHTT1: 1,
	MHTT2: 2,
	parseValue: function(value){
		if(value.trim() == 'M,HTT1'){
			return focusProductType.MHTT1;
		} else if(value.trim() == 'MHTT2'){
			return focusProductType.MHTT2;
		}
		return -2;
	}
};
var PayPeriodStatus = {
		deleted:-1,
		create_new:0,
		applied_value:1,
		caculated:2,
		approved:3,
		stop:4,
		destroyed:5		
};

var RoleType = {NOROLE: 0, HO: 1, GS: 2, DISTRIBUTOR:3,
	parseValue: function(value){
		if(value.trim() == 'HO'){
			return roleType.HO;
		} else if(value.trim() == 'GS'){
			return roleType.GS;
		} else if(value.trim() == 'DISTRIBUTOR'){
			return roleType.DISTRIBUTOR;
		}			
		return roleType.NOROLE;
}};
var MapType = {
	VIETTEL_MAP : 1,
	GOOGLE_MAP : 2
};
var mapShopVale = '<s:property value="#session.mapType" />';
//trungtm6 bo sung thong tin cau hinh
var apConfig = {
	sysDigitDecimal: '<s:property value="sysDigitDecimal" />',
	sysDecimalPoint: '<s:property value="sysDecimalPoint" />',
	sysCurrency: '<s:property value="sysCurrency" />',
	sysSaleRoute: '<s:property value="sysSaleRoute" />'
};


var orderType = {IN: 'IN',SO: 'SO',CM: 'CM',CO: 'CO',GO: 'GO',DP: 'DP',DC: 'DC', DCT: 'DCT', DCG: 'DCG'};
var orderStatus = {NOT_APPROVED: 0, APPROVED: 1, RETURNED: 2};
var IsShowPrice = {SHOW: 1, NOT_SHOW: 2};
var keyCode_F9 = 120;
var keyCode_TAB = 9;
var keyCode_DELETE= 46;
var msgErr_required_field = '<s:text name="common.required.field"/>';
var msgErr_possitive_integer = '<s:text name="common.possitive.integer"/>';
var msgErr_invalid_format_date = '<s:text name="common.invalid.format.date"/>';
var msgErr_invalid_format_date_new = '<s:text name="common.invalid.format.date.new"/>';
var msgErr_invalid_format_month = '<s:text name="common.invalid.format.month"/>';
var msgErr_invalid_format_email = '<s:text name="common.invalid.format.email"/>';
var msgErr_number = '<s:text name="common.number"/>';
var msgErr_invalid_format_code = '<s:text name="common.invalid.format.code"/>';
var msgErr_invalid_format_name = '<s:text name="common.invalid.format.name"/>';
var msgErr_invalid_format_address = '<s:text name="common.invalid.format.address"/>';
var msgErr_invalid_format_special = '<s:text name="common.invalid.format.special"/>';
var msgErr_cross_value = '<s:text name="common.cross.value"/>';
var msgErr_cross_value_section = '<s:text name="common.cross.value.section"/>';
var msgErr_no_result = '<s:text name="common.no.result"/>';
var msgErr_has_result = '<s:text name="common.has.result"/>';
var msgErr_no_select_data = '<s:text name="common.no.select.data"/>';
var msgErr_fromdate_greater_todate = '<s:text name="common.fromdate.greater.todate"/>';
var msgErr_frommonth_greater_tomonth = '<s:text name="common.frommonth.greater.tomonth"/>';
var msgErr_fromdate_greater_currentdate = '<s:text name="common.fromdate.greater.currentdate"/>';
var msgErr_empty_date = '<s:text name="common.empty.date"/>';
var msgErr_exist_code = '<s:text name="common.exist.code"/>';
var msgErr_not_exist_in_db = '<s:text name="common.not.exist.in.db"/>';
var msgErr_file_not_exist = '<s:text name="common.file.not.exist"/>';
var msgErr_file_invalid_type = '<s:text name="common.file.invalid.type"/>';
var msgErr_no_data_report = '<s:text name="common.no.data.report"/>';
var msgErr_result_import_excel = '<s:text name="common.result.import.excel"/>';
var msgErr_result_import_sale_order_excel = '<s:text name="common.result.import.sale.order.excel"/>';
var msgErr_result_import_excel_fail = '<s:text name="common.result.import.excel.fail"/>';
var msgErr_excel_file_invalid_type = '<s:text name="common.excel.file.invalid.type"/>';
var msgErr_pdf_file_invalid_type = '<s:text name="common.pdf.file.invalid.type"/>';
var msgErr_zip_file_invalid_type = '<s:text name="common.zip.file.invalid.type"/>';
var msgErr_report_file_invalid_type = '<s:text name="common.report.file.invalid.type"/>';
var msgErr_invalid_maxlength = '<s:text name="common.invalid.maxlength"/>';
var currentUser = '<s:property value="currentUser.userName"/>';
var msgErr_invalid_format_az='<s:text name="common.invalid.format.tf.az"/>';
var msgErr_invalid_format_num='<s:text name="common.invalid.format.tf.num"/>';
var msgErr_invalid_format_num_dot='<s:text name="common.invalid.format.tf.num.dot"/>';
var msgErr_invalid_format_comma='<s:text name="common.invalid.format.tf.num.comma"/>';
var msgErr_invalid_format_sign='<s:text name="common.invalid.format.tf.num.sign"/>';
var msgErr_invalid_format_confact='<s:text name="common.invalid.format.tf.num.confact"/>';
var msgErr_invalid_format_comma_dot='<s:text name="common.invalid.format.tf.num.comma.dot"/>';
var msgErr_required_choose_format='<s:text name="common.required.choose"/>';
var msgErr_invalid_format_car_number='<s:text name="common.invalid.format.tf.car.number"/>';
var msgErr_invalid_format_payroll = '<s:text name="common.invalid.format.payroll"/>';
var msgErr_invalid_format_tf_id_number = '<s:text name="common.invalid.format.tf.id.number"/>';
var videoServerPath = '<%=Configuration.getServerVideoPath()%>';
var imgServerPath = '<%=Configuration.getImgServerPath()%>';
var imgServerSOPath = '<%=Configuration.getImgServerSOPath()%>';
var imgServerDocPath = '<%=Configuration.getImgServerDocPath()%>';
var imgServerProdcutPath = '<%=Configuration.getImgServerProductPath()%>';
var staticServerPath = '<%=Configuration.getStaticServerPath()%>';
var ReportServerPath = '<%=Configuration.getReportRealPath()%>';
var imgRealPath  = '<%=Configuration.getImageRealPath()%>';
var vbd_url = '<%=Configuration.getVietbandoKey()%>';
var vtm_url = 'http://viettelmap.vn/VTMapService/VTMapAPI?api=VTMap&type=main&k=<%=Configuration.getViettelMapKey()%>';
var google_map_url = 'https://maps.googleapis.com/maps/api/js?key=<%=Configuration.getGoogleMapsKey()%>';
<%-- version 02: update 2015
var vtm_url = 'http://125.212.226.40:8080/VTMapService/VTMapAPI?api=VTMap&type=main&k=<%=Configuration.getViettelMapKey()%>';--%>
var isCloseInfoUser = 0; //1: cho phép đóng div thông tin user đăng nhập
var activePermission = '<s:property value="activePermission"/>';
var developCustomer = '<s:property value="developCustomer"/>';
var isShopEnable = '<s:property value="shopEnable"/>';
var isBoth = '<s:property value="shopEnableBoth"/>';
var sys_separation_auto='<s:property value="separationLot"/>';
var commercialSupportPermission = '<s:property value="commercialSupportPermission"/>';
var isFromBKPTHNVGH = false; 
var isFromBKXNKNVBH = false; 
var isFromDCPOCTDVKH = false;
var isOpenJAlert = false;
var TOP=0,LEFT=0;
var isAuthorize = !isNaN('<s:property value="isAuthorize"/>') && Number('<s:property value="isAuthorize"/>')==1? true:false;
var isAllowEditPromotion = true;//!isNaN('<s:property value="allowEditPromotion"/>') && Number('<s:property value="allowEditPromotion"/>')==1? true:false;
var editOrderTablet = !isNaN('<s:property value="editOrderTablet"/>') && Number('<s:property value="editOrderTablet"/>')==1? true:false;
var vnm_ho = '<s:property value="vnm_ho"/>';
var characterVI = new Array();
var characterReplace = new Array("A","D","E","I","O","U","Y","a","d","e","i","o","u","y");
characterVI[0]=new Array("À","Á","Ả","Ã","Ạ","Â","Ấ","Ầ","Ẩ","Ẫ","Ậ","Ắ","Ằ","Ă");
characterVI[1]=new Array("Đ");
characterVI[2]=new Array("È","É","Ẻ","Ẽ","Ẹ","Ê","Ề","Ế","Ể","Ễ","Ệ");
characterVI[3]=new Array("Ì","Í","Ỉ","Ĩ","Ị");
characterVI[4]=new Array("Ò","Ó","Ỏ","Õ","Ọ","Ô","Ồ","Ố","Ổ","Ỗ","Ộ","Ơ","Ờ","Ớ","Ở","Ỡ","Ợ");
characterVI[5]=new Array("Ù","Ú","Ủ","Ũ","Ụ","Ư","Ừ","Ứ","Ử","Ữ","Ự");
characterVI[6]=new Array("Ỳ","Ý","Ỷ","Ỹ","Ỵ");
characterVI[7]=new Array("à","á","ả","ã","ạ","â","ấ","ầ","ẩ","ẫ","ậ","ắ","ằ","ă");
characterVI[8]=new Array("đ");
characterVI[9]=new Array("è","é","ẻ","ẽ","ẹ","ê","ề","ế","ể","ễ","ệ");
characterVI[10]=new Array("ì","í","ỉ","ĩ","ị");
characterVI[11]=new Array("ò","ó","ỏ","õ","ọ","ô","ồ","ố","ổ","ỗ","ộ","ơ","ờ","ớ","ở","ỡ","ợ");
characterVI[12]=new Array("ù","ú","ủ","ũ","ụ","ư","ừ","ứ","ử","ữ","ự");
characterVI[13]=new Array("ỳ","ý","ỷ","ỹ","ỵ");

function unicodeToEnglish(str){
	if(str == null){
		return "";
	}
	for (i=0;i<=characterVI.length-1;i++){
		for (i1=0;i1<=characterVI[i].length-1;i1++){
			str=str.replace(new RegExp(characterVI[i][i1],'g'),characterReplace[i]);
		}
	}
	return Utils.XSSEncode(str);
}

function selectedDropdowlist(id, value){
	if(id!=undefined && id!=null && value!=undefined && value!=null){
		$('#'+id+' option[value='+value+']').attr('selected','selected').change();
	}
}


$(document).ready(function(){	
	Utils.bindAutoCombineKey();
	Utils.bindAutoSearch();	
	Utils.bindingBanCharacter();
	if($('#fakefilepc').html()!= null){
		$('#fakefilepc').bind('focus',function(e){
			$('#excelFile').click();
			readonly('fakefilepc');
		});
	}	
	var isCheck = false;
	$('#currentMonth .InputTextStyle').each(function(){
	    if(!$(this).is(':hidden') && !isCheck){
	        isCheck = true;
	        $(this).focus();
	    }
	});	
	$(window).bind('keyup',function(event){
		if(event.keyCode == keyCodes.ESCAPE){		   				
			$('.easyui-dialog').each(function(){
				if($(this).parent().css('display')!='none'){
					$('#'+$(this).attr('id')).dialog('close');
				}
			});
 		}
		return true;
   	});	
	$(window).resize(function() {
		var tmBanChar = setTimeout(function(){
			 $('.window').each(function(){
	  			  if(!$(this).is(':hidden')){
	  				  var top = ($(window).height() / 2) - ($(this).outerHeight() / 2);
	  				  var left = ($(window).width() / 2) - ($(this).outerWidth() / 2);
	  				  if(Number(top)<0){
	  					  top = 50;
	  				  }
	  				  $(this).css({ top: top, left: left});
	  			  }
			 });	
			 clearTimeout(tmBanChar);
		 },20); 
	});
	$('.FuncBtmSection form .DivInputFileSection button').bind('click', function(){
		if($('.FuncBtmSection form .DivInputFileSection #fakefilepc').val().trim().length > 0){
			$.messager.confirm('Xác nhận', 'Bạn có muốn nhập từ file?', function(r){
				if (r){
					$('.FuncBtmSection form').submit();
				}
			});	
		} else {
			return true;
		}		
		return false;
	});
	$('#btnSearch,#btnAdd,#btnCancel,#btnCreate,#btnDismiss,.DivInputFileSection button, #btnSearchProduct').bind('focus', function(){
		$('.ErrorMsgStyle').each(function(){
			if(!$(this).is(':hidden')){
				$(this).html('').hide();
			}
		});
	});
	$('.InputTextStyle , button').each(function(){
		$(this).bind('focus',function(){
			$('#cur_focus').val($(this).attr('id'));
		});
	});
	$('.MySelectBoxClass').live('focus',function(){
		$(this).next().addClass('FocusStyle');
	});
	$('.MySelectBoxClass').live('blur',function(){
		$(this).next().removeClass('FocusStyle');
	});	
});
function showLoadingIcon(){
	$('#divOverlay').show();
}
function hideLoadingIcon(){
	$('#divOverlay').hide();
}
String.format = function( text ){
    if ( arguments.length <= 1 ){return text;}
    var tokenCount = arguments.length - 2;
    for( var token = 0; token <= tokenCount; token++ ){
        text = text.replace( new RegExp( "\\{" + token + "\\}", "gi" ), arguments[ token + 1 ] );
    }
    return text;
};
String.prototype.endsWith = function (a) {
    return this.substr(this.length - a.length) === a;
};
String.prototype.startsWith = function (a) {
    return this.substr(0, a.length) === a;
};
String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
    };
String.prototype.count=function(s1) { 
    return (s1 == undefined || s1 == null || s1 == '') ? 0 : (this.length - this.replace(new RegExp(s1,"g"), '').length) / s1.length;
};
String.prototype.bool = function() {
    return (/^true$/i).test(this);
};
</script>
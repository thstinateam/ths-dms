<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
//******************************************
var msgTuyen7 = '<s:text name="expire.type.0"></s:text>';
//******************************************
var js_fromdate_todate_validate = '<s:text name="common.fromdate.greater.todate"/>';
//******************************************
var jsp_common_xacnhan = '<s:text name="jsp.common.xacnhan"/>';
var jsp_common_xacnhan_xoa = '<s:text name="msg.confirm.delete"/>';
var jsp_common_dongy = '<s:text name="jsp.common.dongy"/>';
var jsp_common_huybo = '<s:text name="jsp.common.huybo"/>';
var jsp_common_luu = '<s:text name="jsp.common.luu"/>';
var jsp_common_dong = '<s:text name="jsp.common.dong"/>';
var jsp_common_sao_chep = '<s:text name="jsp.common.saochep"/>';
var jsp_common_save_success = '<s:text name="jsp.common.save.success"/>';
var jsp_common_save_fail = '<s:text name="jsp.common.save.fail"/>';
var jsp_common_delete_success = '<s:text name="jsp.common.delete.success"/>';
var jsp_common_du_lieu_thanh_cong = '<s:text name="jsp.common.du.lieu.thanh.cong"/>';
var jsp_common_ban_luu_thong_tin_nay = '<s:text name="jsp.common.ban.luu.thong.tin.nay"/>';
var jsp_common_err_data_update = '<s:text name="jsp.common.err.data.update"/>';
var jsp_common_chon_tuyen = '<s:text name="jsp.common.chon.tuyen"/>';
var jsp_common_khong_co_tuyen = '<s:text name="jsp.common.khong.co.tuyen"/>';
//****************************************** BEGIN GRID
var common_trang = '<s:text name="common.trang"/>';
var common_trang_cua = '<s:text name="common.trang.cua"/>';
var common_trang_xem = '<s:text name="common.trang.xem"/>';
var common_trang_den = '<s:text name="common.trang.den"/>';
var common_trang_dong = '<s:text name="common.trang.dong"/>';
//****************************************** END GRID
var msgCommon1 = '<s:text name="jsp.common.msg.1"/>';
var msgCommon2 = '<s:text name="jsp.common.msg.2"/>';
var msgCommon3 = '<s:text name="jsp.common.msg.3"/>';
var msgCommon4 = '<s:text name="jsp.common.msg.4"/>';
var msgCommon5 = '<s:text name="jsp.common.msg.5"/>';
var msgCommon6 = '<s:text name="jsp.common.msg.6"/>';
var msgCommon7 = '<s:text name="jsp.common.msg.7"/>';
var msgCommon8 = '<s:text name="jsp.common.msg.8"/>';
var msgCommonShop1 = '<s:text name="msg.common.shop.err.1"/>';
var jsp_common_numerical_order = '<s:text name="jsp.common.numerical.order"/>';
var jsp_common_status = '<s:text name="jsp.common.status"/>';
var jsp_common_status_all = '<s:text name="jsp.common.status.all"/>';
var jsp_common_status_active = '<s:text name="jsp.common.status.active"/>';
var jsp_common_status_stoped = '<s:text name="jsp.common.status.stoped"/>';
var jsp_common_status_draft = '<s:text name="jsp.common.status.draft"/>';
var jsp_common_ngay_chot = '<s:text name="jsp.common.ngay.chot"/>';
var jsp_common_ngay_lam_viec = '<s:text name="chot.ngay.title.ngay.lam.viec"/>';
var jsp_common_loai = '<s:text name="jsp.common.loai"/>';
var jsp_common_s_mo_ta = '<s:text name="jsp.common.s.mo.ta"/>';
var jsp_common_s_gia_tri = '<s:text name="jsp.common.s.gia.tri"/>';
var jsp_common_taomoi = '<s:text name="jsp.common.taomoi"/>';
var jsp_common_capnhat = '<s:text name="jsp.common.capnhat"/>';
var jsp_common_xoa = '<s:text name="jsp.common.xoa"/>';
var jsp_common_sua = '<s:text name="jsp.common.sua"/>';
var jsp_common_chon_nhieu_gia_tri = '<s:text name="jsp.common.chon.nhieu.gia.tri"/>';
var jsp_common_chon_tat_ca = '<s:text name="jsp.common.chon.tat.ca"/>';
var jsp_common_bo_tat_ca = '<s:text name="jsp.common.bo.tat.ca"/>';
var jsp_common_bo_qua = '<s:text name="jsp.common.bo.qua"/>';
var jsp_common_shop_code = '<s:text name="jsp.common.shop.code"/>';
var jsp_common_shop_name = '<s:text name="jsp.common.shop.name"/>';
var jsp_common_cust_code = '<s:text name="jsp.common.customer.code"/>';
var jsp_common_cust_name = '<s:text name="jsp.common.customer.name"/>';
var jsp_common_address = '<s:text name="jsp.common.address"/>';
var jsp_common_staff_code = '<s:text name="jsp.common.staff.code"/>';
var jsp_common_staff_name = '<s:text name="jsp.common.staff.name"/>';
var jsp_common_thong_bao= '<s:text name="jsp.common.thong.bao"/>';
var jsp_common_from_date = '<s:text name="jsp.common.from.date"/>';
var jsp_common_to_date = '<s:text name="jsp.common.to.date"/>';
var jsp_common_product_code = '<s:text name="jsp.common.product.code"/>';
var jsp_common_product_name = '<s:text name="jsp.common.product.name"/>';
var jsp_common_route_code = '<s:text name="jsp.common.route.code"/>';
var jsp_common_route_name = '<s:text name="jsp.common.route.name"/>';
var jsp_common_warehouse = '<s:text name="jsp.common.warehouse"/>';
var jsp_common_ton_kho = '<s:text name="jsp.common.ton.kho"/>';
var jsp_common_stock_quantity = '<s:text name="jsp.common.stock.quantity"/>';
var jsp_common_stock_available_quantity = '<s:text name="jsp.common.stock.available.quantity"/>';
var jsp_common_so_luong = '<s:text name="jsp.common.so.luong"/>';
var jsp_common_gia = '<s:text name="jsp.common.gia"/>';
var jsp_common_price = '<s:text name="jsp.common.price"/>';
var jsp_common_pk_price = '<s:text name="jsp.common.pk.price"/>';
var jsp_common_so_lo = '<s:text name="jsp.common.so.lo"/>';
var jsp_common_status_reject = '<s:text name="common.status.reject"/>';
var jsp_common_msg_deltete_kh = '<s:text name="jsp.common.msg.deltete.kh"/>';
var jsp_common_msg_deltete_nv = '<s:text name="jsp.common.msg.deltete.nv"/>';
var jsp_common_shop_name_lable = '<s:text name="common.shop.name.lable"/>';
//******************************************
var msgText1 = '<s:text name="jsp.common.dangtaidulieu"/>';
var msgText2 = '<s:text name="jsp.common.taomoi"/>';
var msgText3 = '<s:text name="jsp.common.capnhat"/>';
var msgText4 = '<s:text name="jsp.common.xoa"/>';
var msgText5 = '<s:text name="jsp.common.chon"/>';
var msgText6 = '<s:text name="jsp.common.xem.chi.tiet"/>';
var msgThang1 = '<s:text name="jsp.common.thang.1"/>';
var msgThang2 = '<s:text name="jsp.common.thang.2"/>';
var msgThang3 = '<s:text name="jsp.common.thang.3"/>';
var msgThang4 = '<s:text name="jsp.common.thang.4"/>';
var msgThang5 = '<s:text name="jsp.common.thang.5"/>';
var msgThang6 = '<s:text name="jsp.common.thang.6"/>';
var msgThang7 = '<s:text name="jsp.common.thang.7"/>';
var msgThang8 = '<s:text name="jsp.common.thang.8"/>';
var msgThang9 = '<s:text name="jsp.common.thang.9"/>';
var msgThang10 = '<s:text name="jsp.common.thang.10"/>';
var msgThang11 = '<s:text name="jsp.common.thang.11"/>';
var msgThang12 = '<s:text name="jsp.common.thang.12"/>';
var msgThu2 = '<s:text name="jsp.common.thu.2"/>';
var msgThu3 = '<s:text name="jsp.common.thu.3"/>';
var msgThu4 = '<s:text name="jsp.common.thu.4"/>';
var msgThu5 = '<s:text name="jsp.common.thu.5"/>';
var msgThu6 = '<s:text name="jsp.common.thu.6"/>';
var msgThu7 = '<s:text name="jsp.common.thu.7"/>';
var msgCN = '<s:text name="jsp.common.thu.cn"/>';
//****************************************************
var msgCommonErr1 = '<s:text name="msg.common.err.1"/>';
var msgCommonErr2 = '<s:text name="msg.common.err.2"/>';
var msgCommonErr3 = '<s:text name="msg.common.err.3"/>';
var msgCommonErr4 = '<s:text name="msg.common.err.4"/>';
var msgCommonErr5 = '<s:text name="msg.common.err.5"/>';
var msgCommonErr6 = '<s:text name="msg.common.err.6"/>';
var msgCommonErr7 = '<s:text name="msg.common.err.7"/>';
var msgCommonErr8 = '<s:text name="msg.common.err.8"/>';
var msgCommonErr9 = '<s:text name="msg.common.err.9"/>';
var msgCommonErr10 = '<s:text name="msg.common.err.10"/>';
var msgCommonErr11 = '<s:text name="msg.common.err.11"/>';
//******************************************

/*** Begin Quản lý cấu hình tham số */
var jsp_apparam_tao_moi_tham_so = '<s:text name="jsp.apparam.tao.moi.tham.so"/>';
var jsp_apparam_tao_tham_so = '<s:text name="jsp.apparam.tao.tham.so"/>';
var jsp_apparam_ma_tham_so = '<s:text name="jsp.apparam.ma.tham.so" />';
var jsp_apparam_ten_tham_so = '<s:text name="jsp.apparam.ten.tham.so" />';
var jsp_apparam_ban_tao_moi_tham_so = '<s:text name="jsp.apparam.ban.tao.moi.tham.so"/>';
var jsp_apparam_cap_nhat_tham_so = '<s:text name="jsp.apparam.cap.nhat.tham.so"/>';
var jsp_apparam_khong_doi_cap_nhat = '<s:text name="jsp.apparam.khong.doi.cap.nhat"/>';
var jsp_apparam_ban_cap_nhat_tham_so = '<s:text name="jsp.apparam.ban.cap.nhat.tham.so"/>';

/*** End Quản lý cấu hình tham số */
 
/*** Begin Quản lý ngay lam viec */
var work_date_chon_thang = '<s:text name="work.date.chon.thang"/>';
var jsp_common_thu_hai = '<s:text name="jsp.common.thu.hai"/>';
var jsp_common_thu_ba = '<s:text name="jsp.common.thu.ba"/>';
var jsp_common_thu_bon = '<s:text name="jsp.common.thu.bon"/>';
var jsp_common_thu_nam = '<s:text name="jsp.common.thu.nam"/>';
var jsp_common_thu_sau = '<s:text name="jsp.common.thu.sau"/>';
var jsp_common_thu_bay = '<s:text name="jsp.common.thu.bay"/>';
var jsp_common_thu_chunhat = '<s:text name="jsp.common.thu.chunhat"/>';
var work_date_chon_ngay = '<s:text name="work.date.chon.ngay"/>';
var jsp_common_ngay_n = '<s:text name="jsp.common.ngay.n"/>';
var jsp_common_ngay_1 = '<s:text name="jsp.common.ngay.1"/>';
var jsp_common_ngay_2 = '<s:text name="jsp.common.ngay.2"/>';
var jsp_common_ngay_3 = '<s:text name="jsp.common.ngay.3"/>';
var jsp_common_ngay_4 = '<s:text name="jsp.common.ngay.4"/>';
var jsp_common_ngay_5 = '<s:text name="jsp.common.ngay.5"/>';
var jsp_common_ngay_6 = '<s:text name="jsp.common.ngay.6"/>';
var jsp_common_ngay_7 = '<s:text name="jsp.common.ngay.7"/>';
var jsp_common_ngay_8 = '<s:text name="jsp.common.ngay.8"/>';
var jsp_common_ngay_9 = '<s:text name="jsp.common.ngay.9"/>';
var jsp_common_ngay_10 = '<s:text name="jsp.common.ngay.10"/>';
var jsp_common_ngay_11 = '<s:text name="jsp.common.ngay.11"/>';
var jsp_common_ngay_12 = '<s:text name="jsp.common.ngay.12"/>';
var jsp_common_ngay_13 = '<s:text name="jsp.common.ngay.13"/>';
var jsp_common_ngay_14 = '<s:text name="jsp.common.ngay.14"/>';
var jsp_common_ngay_15 = '<s:text name="jsp.common.ngay.15"/>';
var jsp_common_ngay_16 = '<s:text name="jsp.common.ngay.16"/>';
var jsp_common_ngay_17 = '<s:text name="jsp.common.ngay.17"/>';
var jsp_common_ngay_18 = '<s:text name="jsp.common.ngay.18"/>';
var jsp_common_ngay_19 = '<s:text name="jsp.common.ngay.19"/>';
var jsp_common_ngay_20 = '<s:text name="jsp.common.ngay.20"/>';
var jsp_common_ngay_21 = '<s:text name="jsp.common.ngay.21"/>';
var jsp_common_ngay_22 = '<s:text name="jsp.common.ngay.22"/>';
var jsp_common_ngay_23 = '<s:text name="jsp.common.ngay.23"/>';
var jsp_common_ngay_24 = '<s:text name="jsp.common.ngay.24"/>';
var jsp_common_ngay_25 = '<s:text name="jsp.common.ngay.25"/>';
var jsp_common_ngay_26 = '<s:text name="jsp.common.ngay.26"/>';
var jsp_common_ngay_27 = '<s:text name="jsp.common.ngay.27"/>';
var jsp_common_ngay_28 = '<s:text name="jsp.common.ngay.28"/>';
var jsp_common_ngay_29 = '<s:text name="jsp.common.ngay.29"/>';
var jsp_common_ngay_30 = '<s:text name="jsp.common.ngay.30"/>';
var jsp_common_ngay_31 = '<s:text name="jsp.common.ngay.31"/>';
var work_date_chon_thu = '<s:text name="work.date.chon.thu"/>';
var work_date_copy_lich_tu_cha = '<s:text name="work.date.copy.lich.tu.cha"/>';
var work_date_thiet_lap_ngay_lam_viec = '<s:text name="work.date.thiet.lap.ngay.lam.viec"/>';
var jsp_common_thang_mot = '<s:text name="jsp.common.thang.mot"/>';
var jsp_common_thang_hai = '<s:text name="jsp.common.thang.hai"/>';
var jsp_common_thang_ba = '<s:text name="jsp.common.thang.ba"/>';
var jsp_common_thang_bon = '<s:text name="jsp.common.thang.bon"/>';
var jsp_common_thang_nam = '<s:text name="jsp.common.thang.nam"/>';
var jsp_common_thang_sau = '<s:text name="jsp.common.thang.sau"/>';
var jsp_common_thang_bay = '<s:text name="jsp.common.thang.bay"/>';
var jsp_common_thang_tam = '<s:text name="jsp.common.thang.tam"/>';
var jsp_common_thang_chin = '<s:text name="jsp.common.thang.chin"/>';
var jsp_common_thang_muoi = '<s:text name="jsp.common.thang.muoi"/>';
var jsp_common_thang_muoimot = '<s:text name="jsp.common.thang.muoimot"/>';
var jsp_common_thang_muoihai = '<s:text name="jsp.common.thang.muoihai"/>';
var work_date_chon_tu_thang_thiet_lap_view = '<s:text name="work.date.chon.tu.thang.thiet.lap.view"/>';
var work_date_chon_den_thang_thiet_lap_view = '<s:text name="work.date.chon.den.thang.thiet.lap.view"/>';
var work_date_thang_thiet_lap_sau_thang_hien_tai = '<s:text name="work.date.thang.thiet.lap.sau.thang.hien.tai"/>';
var work_date_thang_thiet_lap_nho_bang_den_thang = '<s:text name="work.date.thang.thiet.lap.nho.bang.den.thang"/>';
var work_date_chon_tu_den_thang_thiet_lap_khong_qua_10_nam = '<s:text name="work.date.chon.tu.den.thang.thiet.lap.khong.qua.10.nam"/>';
var work_date_vui_long_chon_ngay = '<s:text name="work.date.vui.long.chon.ngay"/>';
var work_date_vui_long_chon_thang = '<s:text name="work.date.vui.long.chon.thang"/>';
var work_date_chon_thiet_lap = '<s:text name="work.date.chon.thiet.lap"/>';
var work_date_ban_co_muon_thiet_lap_ngay_lam_viec = '<s:text name="work.date.ban.co.muon.thiet.lap.ngay.lam.viec"/>';
var work_date_khong_thay_don_vi_cha_lap_lich = '<s:text name="work.date.khong.thay.don.vi.cha.lap.lich"/>';
var ss_traningplan_date_code = '<s:text name="ss.traningplan.date.code"/>';
var work_date_ly_do_maxlength = '<s:text name="work.date.ly.do.maxlength"/>';
var catalog_reason = '<s:text name="catalog.reason"/>';
var work_date_ly_do_nhap = '<s:text name="work.date.ly.do.nhap"/>';
/*** End Quản lý ngay lam viec*/

/*** Begin Quản lý sản phẩm */
var catalog_display_product_code = '<s:text name="catalog.display.product.code"/>'; // Mã sản phẩm
var catalog_product_name = '<s:text name="catalog.product.name"/>'; // Tên sản phẩm
var catalog_product_code = '<s:text name="catalog.product.code"/>'; //Mã SP
var catalog_product_name_sp = '<s:text name="catalog.product.name.sp"/>'; // Tên SP
var catalog_categoryvn = '<s:text name="catalog.categoryvn"/>';
var catalog_categoryvn_child = '<s:text name="catalog.categoryvn.child"/>';
var catalog_product_brand = '<s:text name="catalog.product.brand"/>';
var catalog_product_flavour = '<s:text name="catalog.product.flavour"/>';
var catalog_product_packing = '<s:text name="catalog.product.packing"/>';
var catalog_product_order_index = '<s:text name="catalog.product.order.index"/>';
var catalog_product_order_index_duplicate = '<s:text name="catalog.product.order.index.duplicate"/>';
var catalog_product_order_index_more_than_zero = '<s:text name="catalog.product.order.index.more.than.zero"/>';
var jsp_product_han_su_dung = '<s:text name="jsp.product.han.su.dung"/>';
var catalog_uselot_code = '<s:text name="catalog.uselot.code"/>';
var jsp_common_ngay = '<s:text name="jsp.common.ngay"/>';
var jsp_common_thang = '<s:text name="jsp.common.thang"/>';
var jsp_common_co = '<s:text name="jsp.common.co"/>';
var jsp_common_khong = '<s:text name="jsp.common.khong"/>';
var catalog_product_them_san_pham = '<s:text name="catalog.product.them.san.pham"/>';
var catalog_product_ten_viet_tat = '<s:text name="catalog.product.ten.viet.tat"/>';
var catalog_product_dvt = '<s:text name="catalog.product.dvt"/>';
var catalog_product_quy_cach = '<s:text name="catalog.product.quy.cach"/>';
var jsp_product_han_su_dung = '<s:text name="jsp.product.han.su.dung"/>';
var catalog_product_chon_nganh_hang = '<s:text name="catalog.product.chon.nganh.hang"/>';
var catalog_product_chon_dvt = '<s:text name="catalog.product.chon.dvt"/>';
var catalog_product_chon_nhan_hieu = '<s:text name="catalog.product.chon.nhan.hieu"/>';
var catalog_product_chon_huong_vi = '<s:text name="catalog.product.chon.huong.vi"/>';
var catalog_product_chon_bao_bi = '<s:text name="catalog.product.chon.bao.bi"/>';
var catalog_product_chon_nganh_hang_con = '<s:text name="catalog.product.chon.nganh.hang.con"/>';
var jsp_common_s_xem_lai = '<s:text name="jsp.common.s.xem.lai"/>';
var jsp_common_s_tai_hinh_anh_tc = '<s:text name="jsp.common.s.tai.hinh.anh.tc"/>';
var jsp_common_s_tai_hinh_anh_tb = '<s:text name="jsp.common.s.tai.hinh.anh.tb"/>';
var jsp_common_s_size_too_large = '<s:text name="jsp.common.s.size.too.large"/>';
var catalog_product_upload_toi_da_hinh_sp = '<s:text name="catalog.product.upload.toi.da.hinh.sp"/>';
var catalog_product_ko_chon_hinh_sp = '<s:text name="catalog.product.ko.chon.hinh.sp"/>';
var catalog_product_ten_hinh_anh = '<s:text name="catalog.product.ten.hinh.anh"/>';
var catalog_product_ten_video = '<s:text name="catalog.product.ten.video"/>';
var catalog_product_sp_mot_video = '<s:text name="catalog.product.sp.mot.video"/>';
var catalog_product_video_sp_vuot_qua_dung_luong = '<s:text name="catalog.product.video.sp.vuot.qua.dung.luong"/>';
var catalog_product_video_tai_tc = '<s:text name="catalog.product.video.tai.tc"/>';
var catalog_product_video_hinh_anh_va = '<s:text name="catalog.product.video.hinh.anh.va"/>';
var catalog_product_video_mot = '<s:text name="catalog.product.video.mot"/>';
var catalog_product_video_tai_tc_ha_video = '<s:text name="catalog.product.video.tai.tc.ha.video"/>';
var catalog_product_video_tai_tc_ha_phim = '<s:text name="catalog.product.video.tai.tc.ha.phim"/>';
var catalog_product_video_khong_hop_le = '<s:text name="catalog.product.video.khong.hop.le"/>';
var catalog_product_video_khong_file_video_chon = '<s:text name="catalog.product.video.khong.file.video.chon"/>';
var catalog_product_video_khong_dung_dinh_dang = '<s:text name="catalog.product.video.khong.dung.dinh.dang"/>';
var catalog_product_video_khong_file_chon = '<s:text name="catalog.product.video.khong.file.chon"/>';
var catalog_product_video_ban_co_muon_xoa = '<s:text name="catalog.product.video.ban.co.muon.xoa"/>';
var catalog_product_ma_vach_phai_so_nguyen = '<s:text name="catalog.product.ma.vach.phai.so.nguyen"/>';
var catalog_product_thsd_nam_trong_khong_chin = '<s:text name="catalog.product.thsd.nam.trong.khong.chin"/>';
var catalog_product_thsd_vuot_qua_ba_kytu = '<s:text name="catalog.product.thsd.vuot.qua.ba.kytu"/>';
var catalog_product_thsd_chon_loai_hsd = '<s:text name="catalog.product.thsd.chon.loai.hsd"/>';
var catalog_product_thsd_nhap_hsd = '<s:text name="catalog.product.thsd.nhap.hsd"/>';
var catalog_product_brand = '<s:text name="catalog.product.brand"/>';
var catalog_product_volumn_lit = '<s:text name="catalog.product.volumn.lit"/>';
var catalog_product_uom1 = '<s:text name="catalog.product.uom1"/>';
var catalog_product_uom2 = '<s:text name="catalog.product.uom2"/>';
var catalog_proudct_convfact = '<s:text name="catalog.proudct.convfact"/>';
var catalog_proudct_convfact_sl = '<s:text name="catalog.proudct.convfact.sl"/>';
var catalog_product_dong_goi_nam_trong_khong_chin = '<s:text name="catalog.product.dong.goi.nam.trong.khong.chin"/>';
var catalog_product_the_tich_gioi_han_chin_kytu = '<s:text name="catalog.product.the.tich.gioi.han.chin.kytu"/>';
var catalog_product_the_tich_khong_dung_dinh_dang = '<s:text name="catalog.product.the.tich.khong.dung.dinh.dang"/>';
var catalog_product_khoi_luong_tinh_gioi_han_haimuoi_kytu = '<s:text name="catalog.product.khoi.luong.tinh.gioi.han.haimuoi.kytu"/>';
var catalog_product_khoi_luong_tinh_khong_dung_dinh_dang = '<s:text name="catalog.product.khoi.luong.tinh.khong.dung.dinh.dang"/>';
var catalog_product_net_weight_minus = '<s:text name="catalog.product.net.weight.minus"/>';
var catalog_product_tong_kl_gioi_han_haimuoi_kytu = '<s:text name="catalog.product.tong.kl.gioi.han.haimuoi.kytu"/>';
var catalog_product_tong_kl_khong_dung_dinh_dang = '<s:text name="catalog.product.tong.kl.khong.dung.dinh.dang"/>';
var catalog_product_tong_kl_phai_so_nguyen = '<s:text name="catalog.product.tong.kl.phai.so.nguyen"/>';
var catalog_product_netgross = '<s:text name="catalog.product.netgross"/>';
var catalog_product_volumn_minus = '<s:text name="catalog.product.volumn.minus"/>';
var jsp_common_chon = '<s:text name="jsp.common.chon"/>';
var jsp_common_s_product_please_upload_image_video = '<s:text name="jsp.common.s.product.please.upload.image.video"/>';
var jsp_common_s_product_upload_maximum = '<s:text name="jsp.common.s.product.upload.maximum"/>';
var catalog_product_video_hinh_anh_lon = '<s:text name="catalog.product.video.hinh.anh.lon"/>';
var catalog_product_video_hinh_anh_kt_lon = '<s:text name="catalog.product.video.hinh.anh.kt.lon"/>';
var catalog_product_video_hinh_anh_kt_toi_da = '<s:text name="catalog.product.video.hinh.anh.kt.toi.da"/>';
var catalog_product_export_file = '<s:text name="catalog.product.export.file"/>';
var catalog_product_upload_file_not_format = '<s:text name="catalog.product.upload.file.not.format"/>';
var xem_ban_do='<s:text name="catalog.product.view.map" />';
 /*End Quản lý sản phẩm*/
 

var qltt_chuc_nang = '<s:text name="qltt_chuc_nang" />';
var qltt_ma_thuoc_tinh  = '<s:text name="qltt_ma_thuoc_tinh" />';
var qltt_ten_thuoc_tinh = '<s:text name="qltt_ten_thuoc_tinh" />';
var qltt_doi_tuong_ap_dung = '<s:text name="qltt_doi_tuong_ap_dung" />';
var qltt_loai_gia_tri = '<s:text name="qltt_loai_gia_tri" />';
var qltt_trang_thai = '<s:text name="qltt_trang_thai" />';
var qltt_danh_sach_thuoc_tinh = '<s:text name="qltt_danh_sach_thuoc_tinh" />';
var qltt_bat_buoc = '<s:text name="qltt_bat_buoc" />';
var qltt_thuoc_tinh_bat_buoc = '<s:text name="qltt_thuoc_tinh_bat_buoc" />';
var qltt_thu_tu_hien_thi = '<s:text name="qltt_thu_tu_hien_thi" />';
var qltt_danh_sach_gia_tri_cua_tt = '<s:text name="qltt_danh_sach_gia_tri_cua_tt" />';
var qltt_rang_buoc_gia_tri_nhap = '<s:text name="qltt_rang_buoc_gia_tri_nhap" />';
var qltt_do_dai_toi_da = '<s:text name="qltt_do_dai_toi_da" />';
var qltt_gia_tri_hop_le_tu  = '<s:text name="qltt_gia_tri_hop_le_tu" />';
var qltt_gia_tri_hop_le_den = '<s:text name="qltt_gia_tri_hop_le_den" />';
var qltt_bo_qua = '<s:text name="qltt_bo_qua" />';
var qltt_ma_gia_tri = '<s:text name="qltt_ma_gia_tri" />';
var qltt_ten_gia_tri = '<s:text name="qltt_ten_gia_tri" />';
var qltt_js_msg_tu_den ='<s:text name="qltt_js_msg_tu_den" />';
var qltt_gia_tri = '<s:text name="qltt_gia_tri" />';
var qltt_doi_tuong_nhanvien = '<s:text name="qltt_doi_tuong_nhanvien" />';
var qltt_doi_tuong_khachhang = '<s:text name="qltt_doi_tuong_khachhang" />';
var qltt_doi_tuong_sanpham = '<s:text name="qltt_doi_tuong_sanpham" />';
var hoat_dong= '<s:text name="action.status.name" />';
var tam_ngung ='<s:text name="pause.status.name" />';

var qltt_kiem_tra_maxvalue = '<s:text name="qltt_kiem_tra_maxvalue" />';
var qltt_kiem_tra_minvalue = '<s:text name="qltt_kiem_tra_minvalue" />';
var qltt_do_dai_toi_da_chu ='<s:text name="qltt_do_dai_toi_da_chu" />';
var qltt_do_dai_toi_da_so ='<s:text name="qltt_do_dai_toi_da_so" />';

/*** Begin Quan ly ngay lam viec*/
var work_date_menu ='<s:text name="work.date.menu" />';
var work_date_don_vi ='<s:text name="work.date.don.vi" />';
var work_date_first_mess ='<s:text name="work.date.first.mess" />';
var work_date_for_year ='<s:text name="work.date.for.year" />';
var work_date_for_month ='<s:text name="work.date.for.month" />';
var work_date_inherit ='<s:text name="work.date.inherit" />';
var work_date_institute ='<s:text name="work.date.institute" />';
var work_date_month_title ='<s:text name="work.date.month.title" />';
var work_date_month_title1 ='<s:text name="work.date.month.title1" />';
var work_date_month_title2 ='<s:text name="work.date.month.title2" />';
var work_date_month_title3 ='<s:text name="work.date.month.title3" />';
var work_date_month_title4 ='<s:text name="work.date.month.title4" />';
var work_date_month_title5 ='<s:text name="work.date.month.title5" />';
var work_date_month_title6 ='<s:text name="work.date.month.title6" />';
var work_date_month_title7 ='<s:text name="work.date.month.title7" />';
var work_date_month_title8 ='<s:text name="work.date.month.title8" />';
var work_date_month_title9 ='<s:text name="work.date.month.title9" />';
var work_date_month_title10 ='<s:text name="work.date.month.title10" />';
var work_date_month_title11 ='<s:text name="work.date.month.title11" />';
var work_date_month_title12 ='<s:text name="work.date.month.title12" />';
var work_date_working_date ='<s:text name="work.date.working.date" />';
var work_date_off_date ='<s:text name="work.date.off.date" />';
var work_date_reason ='<s:text name="work.date.reason" />';
var work_date_update ='<s:text name="work.date.update" />';
var work_date_type0 ='<s:text name="work.date.type0" />';
var work_date_type1 ='<s:text name="work.date.type1" />';
var work_date_co_so_ngay_lam_viec = '<s:text name="work_date_co_so_ngay_lam_viec" />';
var work_date_mess_confirm_inharit = '<s:text name="work.date.mess.confirm.inharit" />';
var work_date_mess_success = '<s:text name="work.date.mess.success" />';
var work_date_mess_note_not_exist_parent = '<s:text name="work_date_mess_note_not_exist_parent" />';

var work_date_chon = '<s:text name="work_date_chon" />';
var work_date_T2 ='<s:text name="work.date.T2" />';
var work_date_T3 ='<s:text name="work.date.T3" />';
var work_date_T4 ='<s:text name="work.date.T4" />';
var work_date_T5 ='<s:text name="work.date.T5" />';
var work_date_T6 ='<s:text name="work.date.T6" />';
var work_date_T7 ='<s:text name="work.date.T7" />';
var work_date_CN ='<s:text name="work.date.CN" />';
var work_date_th2 ='<s:text name="work.date.th2" />';
var work_date_th3 ='<s:text name="work.date.th3" />';
var work_date_th4 ='<s:text name="work.date.th4" />';
var work_date_th5 ='<s:text name="work.date.th5" />';
var work_date_th6 ='<s:text name="work.date.th6" />';
var work_date_th7 ='<s:text name="work.date.th7" />';
var common_unit_name_err = '<s:text name="common.unit.name.err" />';
var work_date_day = '<s:text name="work.date.day" />';
var jsp_common_timkiem = '<s:text name="jsp.common.timkiem" />';
var work_date_result_search = '<s:text name="work_date_result_search" />';


var work_date_unit_code = '<s:text name="work_date_unit_code" />';
var work_date_unit_name = '<s:text name="work_date_unit_name" />';
var work_date_day = '<s:text name="work.date.day" />';
/*** End Quan ly ngay lam viec*/
 
/*** Begin Quan ly cay don vi; thong tin nhan vien*/
var catalog_staff_code = '<s:text name="catalog.staff.code"/>';
var catalog_staff_name = '<s:text name="catalog.staff.name"/>';
var catalog_unit_tree = '<s:text name="catalog.unit.tree"/>';
var catalog_staff_type = '<s:text name="catalog.staff.type"/>';
var catalog_gender_value = '<s:text name="catalog.gender.value"/>';
var catalog_unit_ngay_bat_dau_lam_viec_khong_hop_le = '<s:text name="catalog_unit_ngay_bat_dau_lam_viec_khong_hop_le"/>';
var catalog_mobilephone = '<s:text name="catalog.mobilephone"/>';
var catalog_telephone = '<s:text name="catalog.telephone"/>';
var catalog_email_code = '<s:text name="catalog.email.code"/>';
var catalog_unit_view_thong_tin_nhan_vien_back = '<s:text name="catalog_unit_view_thong_tin_nhan_vien_back"/>';
var catalog_unit_view_ban_do_don_vi = '<s:text name="catalog_unit_view_ban_do_don_vi"/>';
var organization_ban_co_muon_danh_dau_quan_ly_don_vi = '<s:text name="organization_ban_co_muon_danh_dau_quan_ly_don_vi"/>'; 
var organization_ban_co_muon_bo_danh_dau_quan_ly_don_vi = '<s:text name="organization_ban_co_muon_bo_danh_dau_quan_ly_don_vi"/>';
var organization_title_popup_is_Manage = '<s:text name="organization_title_popup_is_Manage"/>';

/*** End Quan ly cay don vi; thong tin nhan vien*/

/*** Begin Cây tổ chức*/
var organization_ban_xoa_loai_don_vi = '<s:text name="organization_ban_xoa_loai_don_vi"/>';
var organization_ban_xoa_chuc_vu = '<s:text name="organization_ban_xoa_chuc_vu"/>';
var organization_title_popup_them_don_vi = '<s:text name="organization_title_popup_them_don_vi" />';
var organization_title_popup_them_chuc_vu = '<s:text name="organization_title_popup_them_chuc_vu" />';
var organization_title_popup_loai_don_vi = '<s:text name="organization_title_popup_loai_don_vi" />';
var organization_title_popup_loai_chuc_vu = '<s:text name="organization_title_popup_loai_chuc_vu" />';
var organization_title_delete_loai_don_vi = '<s:text name="organization_title_delete_loai_don_vi" />';
var organization_title_delete_loai_chuc_vu = '<s:text name="organization_title_delete_loai_chuc_vu" />';
var organization_prefix = '<s:text name="organization_prefix" />';
var organization_err_delete_shop_type_exist_node_type_id ='<s:text name="organization_err_delete_shop_type_exist_node_type_id" />'; 
var organization_err_choose_shop_type_exist_node_type_id = '<s:text name="organization_err_choose_shop_type_exist_node_type_id" />';
var organization_err_delete_staff_type_exist_node_type_id = '<s:text name="organization_err_delete_staff_type_exist_node_type_id" />';
var organization_err_choose_staff_type_exist_node_type_id = '<s:text name="organization_err_choose_staff_type_exist_node_type_id" />';
var organization_title_popup_choose_image = '<s:text name="organization_title_popup_choose_image"/>';

var organization_title_popup_information_organization = '<s:text name="organization_title_popup_information_organization" />';
var organization_title_popup_information_position = '<s:text name="organization_title_popup_information_position" />';
var organization_title_popup_description = '<s:text name="organization_title_popup_description" />';
var organization_err_prefix_not_unicode = '<s:text name="organization_err_prefix_not_unicode" />';
var organization_err_prefix_exist = '<s:text name="organization_err_prefix_exist"/>';
/*** End Cây tổ chức*/

/*** Begin nhap hang **/
var povnm_type_input = '<s:text name="povnm_type_input"/>';
var povnm_type_output = '<s:text name="povnm_type_output"/>';
var po_vnm_status_not_import = '<s:text name="po.vnm.status.not.import"/>'; // chua nhap
var po_vnm_status_importing = '<s:text name="po.vnm.status.importing"/>'; // da nhap mot phan
var po_vnm_status_imported = '<s:text name="po.vnm.status.imported"/>'; // da nhap
var po_vnm_status_not_import_output = '<s:text name="po.vnm.status.not.import.output"/>'; // chua tra
var po_vnm_status_importing_output = '<s:text name="po.vnm.status.importing.output"/>'; // da tra mot phan
var po_vnm_status_imported_output = '<s:text name="po.vnm.status.imported.output"/>'; // da tra

var confirm_order_huy = '<s:text name="confirm_order_huy"/>'; // huy
var po_vnm_status_pending = '<s:text name="po.vnm.status.pending"/>'; // co the treo
var po_vnm_status_suspend = '<s:text name="po.vnm.status.suspend"/>'; // da treo
/*** End nhap hang **/

/*** Begin giam sat ban hang **/
var msgNetSignWeak = '<s:text name="jsp.supervise.batt.weak" />'; // = Yếu
var msgNetSignStrong = '<s:text name="jsp.supervise.batt.strong" />'; // = Manh
var msgNetSignVeryStrong = '<s:text name="jsp.supervise.batt.very.strong" />'; // = Cuc Manh
/*** End giam sat ban hang **/

/*** Begin Quan ly muc doanh so nganh*/
var catalog_sales_brand_update_seasonal_by_industry = '<s:text name="catalog_sales_brand_update_seasonal_by_industry"/>';
var catalog_sales_brand_NH_Code = '<s:text name="catalog_sales_brand_NH_Code"/>';
var catalog_sales_brand_standard_code = '<s:text name="catalog_sales_brand_standard_code"/>';
var catalog_sales_brand_standard_name = '<s:text name="catalog_sales_brand_standard_name"/>';
var catalog_sales_brand_from_amount = '<s:text name="catalog_sales_brand_from_amount"/>';
var catalog_sales_brand_to_amount = '<s:text name="catalog_sales_brand_to_amount"/>';
var unit_tree_search_unit_search_grid_no = '<s:text name="unit_tree.search_unit.search_grid.no"/>';
var catalog_sales_brand_from_amount_to_amount = '<s:text name="catalog_sales_brand_from_amount_to_amount"/>';
var catalog_sales_brand_add_level_income =  '<s:text name ="catalog_sales_brand_add_level_income"/>';
var catalog_sales_brand_target = '<s:text name="catalog_sales_brand_target"/>';
var catalog_sales_brand_chose_standard = '<s:text name="catalog_sales_brand_chose_standard"/>';
var catalog_sales_brand_all = '<s:text name="catalog_sales_brand_all"/>';
var catalog_customer_add_level_income = '<s:text name="catalog_customer_add_level_income"/>';
/*** End Quan ly muc doanh so nganh*/

/*** Begin loai khach hang*/
var catalog_customer_type_parent_customer_type_code = '<s:text name="catalog_customer_type_parent_customer_type_code"/>';
var catalog_customer_type_customer_type_code = '<s:text name="catalog_customer_type_customer_type_code"/>';
var catalog_customer_type_customer_type_name = '<s:text name="catalog_customer_type_customer_type_name"/>';
var catalog_customer_type_standard_sku = '<s:text name="catalog_customer_type_standard_sku"/>';
var	catalog_customer_type_target_ds = '<s:text name="catalog_customer_type_target_ds"/>';
var catalog_customer_type_edit = '<s:text name="catalog_customer_type_edit"/>';
var catalog_customer_type_add = '<s:text name="catalog_customer_type_add"/>';
var unit_tree_search_unit_shop_status =  '<s:text name="unit_tree.search_unit.shop_status"/>';
var catalog_sales_brand_search_information = '<s:text name="catalog_sales_brand_search_information"/>';
var catalog_search_information = '<s:text name="catalog_search_information"/>';
var unit_tree_search_unit_search_header = '<s:text name="unit_tree.search_unit.search_header"/>';
 /*** End loai khach hang*/
 
/*** Begin danh sach loai khach hang*/
var catalog_customer_type_title = '<s:text name="catalog_customer_type_title"/>';
var catalog_customer_type_customer_type_code = '<s:text name="catalog_customer_type_customer_type_code"/>';
var catalog_customer_type_customer_type_name = '<s:text name="catalog_customer_type_customer_type_name"/>';
var catalog_customer_type_standard_sku = '<s:text name="catalog_customer_type_standard_sku"/>';
var catalog_customer_type_target_ds = '<s:text name="catalog_customer_type_target_ds"/>';

var catalog_customer_type_standard_sku_is_not_number = '<s:text name="catalog_customer_type_standard_sku_is_not_number"/>'; 
var catalog_customer_type_target_ds_is_not_number ='<s:text name="catalog_customer_type_target_ds_is_not_number"/>';
var catalog_customer_type_chose_status = '<s:text name="catalog_customer_type_chose_status"/>'; 
var catalog_customer_type_add_information='<s:text name="catalog_customer_type_add_information"/>'; 
var catalog_customer_type_add_information ='<s:text name="catalog_customer_type_add_information"/>'; 
var catalog_customer_type_chose_customer_type='<s:text name="catalog_customer_type_chose_customer_type"/>'; 
var report_status_label ='<s:text name="report.status.label"/>';
var catalog_customer_type_information = '<s:text name="catalog_customer_type_information"/>';
var catalog_customer_type_add_information = '<s:text name="catalog_customer_type_add_information"/>';
var catalog_customer_type_update_information = '<s:text name="catalog_customer_type_update_information"/>';
var common_information_detail = '<s:text name="common_information_detail"/>';
var catalog_customer_staff_type_infomation = '<s:text name="catalog_customer_staff_type_infomation"/>';
var catalog_customer_type_chose_customer_type = '<s:text name="catalog_customer_type_chose_customer_type"/>';
var action_type_status_delete='<s:text name="action.type.status.delete"/>';
/*** End danh sach loai khach hang*/
/*** Begin cay dia ban ***/
var unit_tree_search_unit_search_grid_import_download_file= '<s:text name="unit_tree.search_unit.search_grid.import.download_file"/>';
var unit_tree_search_unit_search_grid_import_button_text= '<s:text name="unit_tree.search_unit.search_grid.import_button_text"/>';
var unit_tree_search_unit_search_grid_export_button_text= '<s:text name="unit_tree.search_unit.search_grid.export_button_text"/>';
var area_tree_area = '<s:text name="area_tree_area"/>';
var area_tree_title = '<s:text name="area_tree_title"/>';
var area_tree_information_search = '<s:text name="area_tree_information_search"/>';
var area_tree_parent_code_id = '<s:text name="area_tree_parent_code_id"/>';
var area_tree_parent_name = '<s:text name="area_tree_parent_name"/>';
var area_tree_code = '<s:text name="area_tree_code"/>';
var area_tree_name='<s:text name="area_tree_name"/>';
var area_tree_list = '<s:text name="area_tree_list"/>';
var area_tree_no_result='<s:text name="area_tree_no_result"/>';
var area_tree_stt = '<s:text name="area_tree_stt"/>';
var area_tree_information_area = '<s:text name="area_tree_information_area"/>';
var area_tree_area_update = '<s:text name="area_tree_area_update"/>';
/** import cay dia ban  #Ham Utils **/

var common_result_import_excel_chose_file = '<s:text name="common_result_import_excel_chose_file"/>';
var common_result_import_excel_form_not_exist_in_file = '<s:text name="common_result_import_excel_form_not_exist_in_file"/>';
var common_result_import_excel_message_conform = '<s:text name="common_result_import_excel_message_conform"/>';
var common_result_import_excel_detail_eror = '<s:text name="area_tree_area_update"/>';
var common_result_import_excel_not_file_exist =  '<s:text name="common_result_import_excel_not_file_exist"/>';
var catalog_unit_tao_don_vi_con = '<s:text name="catalog_unit_tao_don_vi_con"/>';
var catalog_unit_tao_chuc_vu = '<s:text name="catalog_unit_tao_chuc_vu"/>';
var catalog_unit_tree_title = '<s:text name="catalog_unit_tree_title"/>';
var catalog_unit_thong_tin_nhan_vien = '<s:text name="catalog_unit_thong_tin_nhan_vien"/>';
var catalog_unit_tree_chuyen_don_vi = '<s:text name="catalog_unit_tree_chuyen_don_vi"/>';
var catalog_unit_hien_thi_trang_thai_tam_ngung =  '<s:text name="catalog_unit_hien_thi_trang_thai_tam_ngung"/>';
var catalog_unit_tree_tam_ngung_doi_tuong_nay = '<s:text name="catalog_unit_tree_tam_ngung_doi_tuong_nay"/>';
var catalog_unit_tree_change_unit_phan_quyen = '<s:text name = "catalog_unit_tree_change_unit_phan_quyen"/>';
var catalog_unit_tree_chang_unit_don_vi_dich_khong_cho_tao = '<s:text name="catalog_unit_tree_chang_unit_don_vi_dich_khong_cho_tao"/>';
var catalog_unit_tree_change_unit_don_vi_dich_la_don_vi_hien_tai  ='<s:text name ="catalog_unit_tree_change_unit_don_vi_dich_la_don_vi_hien_tai"/>';

/** **/ 
/** #BEGIN QUAN LY KHO **/
var stock_manage_title = '<s:text name="stock_manage_title"/>';
var stock_manage_title_list_stock = '<s:text name="stock_manage_title_list_stock"/>';
var stock_manage_quan_ly_kho = '<s:text name="stock_manage_quan_ly_kho"/>';
var stock_manage_don_vi   = '<s:text name="stock_manage_don_vi"/>'; 
var stock_manage_ma_kho   = '<s:text name="stock_manage_ma_kho"/>';
var stock_manage_ten_kho  = '<s:text name="stock_manage_ten_kho"/>';
var stock_manage_loai_kho  = '<s:text name="stock_manage_loai_kho"/>';
var stock_manage_kho_ban  = '<s:text name="stock_manage_kho_ban"/>';
var stock_manage_kho_km  = '<s:text name="stock_manage_kho_km"/>';
var stock_manage_tu_ngay  = '<s:text name="stock_manage_tu_ngay"/>';
var stock_manage_den_ngay = '<s:text name="stock_manage_den_ngay"/>';
var stock_manage_ngay_tao = '<s:text name="stock_manage_ngay_tao"/>';
var stock_manage_mien     = '<s:text name="stock_manage_mien"/>';
var stock_manage_vung     = '<s:text name="stock_manage_vung"/>';
var stock_manage_thu_tu   = '<s:text name="stock_manage_thu_tu"/>';
var stock_manage_maNPP    = '<s:text name="stock_manage_maNPP"/>';
var stock_manage_tenNPP   = '<s:text name="stock_manage_tenNPP"/>';
var stock_manage_status   = '<s:text name="stock_manage_status"/>';
var stock_manage_button_tim_kiem   = '<s:text name= "stock_manage_button_tim_kiem"/>';
var stock_manage_button_sao_chep = '<s:text name= "stock_manage_button_sao_chep"/>';
var stock_manage_tooltip_them  = '<s:text name= "stock_manage_tooltip_them"/>';
var stock_manage_tooltip_chinh_sua = '<s:text name= "stock_manage_tooltip_chinh_sua"/>';
var stock_manage_tooltip_tam_ngung = '<s:text name= "stock_manage_tooltip_tam_ngung"/>';
var stock_manage_tooltip_xoa       = '<s:text name= "stock_manage_tooltip_xoa"/>';
var stock_manage_tooltip_ghi_chu = '<s:text name="stock_manage_tooltip_ghi_chu"/>';
var stock_manage_tooltip_thu_tu = '<s:text name="stock_manage_tooltip_thu_tu"/>';
var stock_manage_tooltip_luu       = '<s:text name="stock_manage_tooltip_luu"/>';
var stock_manage_tooltip_dong  = '<s:text name="stock_manage_tooltip_dong"/>';
var stock_manage_ma_don_vi = '<s:text name="stock_manage_ma_don_vi"/>';
var stock_manage_ten_don_vi = '<s:text name="stock_manage_ten_don_vi"/>';
var stock_manage_tungay_denngay ='<s:text name="stock_manage_tu_ngay_denngay"/>';
var stock_manage_them_moi_kho = '<s:text name="stock_manage_them_moi_kho"/>';
var stock_manage_chinh_sua_kho = '<s:text name="stock_manage_chinh_sua_kho"/>';
stock_manage_tooltip_ghi_chu
var stock_manage_khong_xac_dinh_vi_tri = '<s:text name="stock_manage_khong_xac_dinh_vi_tri"/>';
var stock_manage_khong_xac_dinh_tinh_trang = '<s:text name="stock_manage_khong_xac_dinh_tinh_trang"/>';
var stock_manage_thu_tu_lon_hon_mot = '<s:text name="stock_manage_thu_tu_lon_hon_mot"/>';
var stock_manage_ban_cap_nhat_kho = '<s:text name="stock_manage_ban_cap_nhat_kho"/>'; 
var stock_manage_them_kho = '<s:text name="stock_manage_them_kho"/>';
var stock_manage_chua_chon_don_vi = '<s:text name="stock_manage_chua_chon_don_vi"/>';
var stock_manage_sao_chep_kho = '<s:text name="stock_manage_sao_chep_kho"/>';
var stock_manage_xoa_kho = '<s:text name="stock_manage_xoa_kho"/>';
var stock_manage_co_muon = '<s:text name="stock_manage_co_muon"/>';
var stock_manage_sao_chep_kho = '<s:text name="stock_manage_sao_chep_kho"/>';
var stock_manage_tim_kiem_don_vi = '<s:text name="stock_manage_tim_kiem_don_vi"/>';
var stock_manage_chon = '<s:text name="stock_manage_chon"/>';
/** #END QUAN LY KHO **/

/** Begin product Type */
var product_type_ban = '<s:text name="product_type_ban"/>';
var product_type_km = '<s:text name="product_type_km"/>';
var product_type_posm = '<s:text name="product_type_posm"/>';
/** End product Type */

/** end cay dia ban */

/** #BEGIN thong tin khach hang **/
var sale_product_customer_stt = '<s:text name="sale_product_customer_stt"/>';
var sale_product_customer_ngay = '<s:text name="sale_product_customer_ngay"/>'; 
var sale_product_customer_so_don_hang = '<s:text name="sale_product_customer_so_don_hang"/>';
var sale_product_customer_thanh_tien = '<s:text name="sale_product_customer_thanh_tien"/>';
/* #END thong tin khach hang*/
/** TẠO �?ƠN HÀNG **/
var create_order_tt_tim_kiem = '<s:text name="create_order_tt_tim_kiem"/>';
var create_order_ma_kh = '<s:text name="create_order_ma_kh"/>'; 
var create_order_ten_kh = '<s:text name="create_order_ten_kh"/>';
var create_order_dia_chi = '<s:text name="create_order_dia_chi"/>';
var create_order_tim_kiem = '<s:text name="create_order_tim_kiem"/>';
var create_order_dong = '<s:text name="create_order_dong"/>'; 
var create_order_ma_san_pham = '<s:text name="create_order_ma_san_pham"/>';
var create_order_ten_san_pham = '<s:text name="create_order_ten_san_pham"/>'; 
var create_order_don_gia = '<s:text name="create_order_don_gia"/>';
var create_order_kho = '<s:text name="create_order_kho"/>';
var create_order_ton_kho_dap_ung = '<s:text name="create_order_ton_kho_dap_ung"/>';
var create_order_thuc_dat= '<s:text name="create_order_thuc_dat"/>'; 
var create_order_thanh_tien = '<s:text name="create_order_thanh_tien"/>';
var create_order_CKMH = '<s:text name="create_order_CKMH"/>';
var create_order_CTHTTM = '<s:text name="create_order_CTHTTM"/>';
var create_order_order_date_null = '<s:text name="create_order_order_date_null"/>';
var create_order_order_date_lock_date = '<s:text name="create_order_order_date_lock_date"/>';
var create_order_order_date_current_date = '<s:text name="create_order_order_date_current_date"/>';
var create_order_order_date_delivery_date = '<s:text name="create_order_order_date_delivery_date"/>';
var create_order_tt_dh= '<s:text name="create_order_tt_dh"/>';
var create_order_chon = '<s:text name="create_order_chon"/>'; 
var create_order_so_dh = '<s:text name="create_order_so_dh"/>';
var create_order_ngay = '<s:text name="create_order_ngay"/>';
var create_order_khach_hang = '<s:text name="create_order_khach_hang"/>';
var create_order_dia_chi = '<s:text name="create_order_dia_chi"/>'; 
var create_order_nvbh = '<s:text name="create_order_nvbh"/>';
var create_order_nvgh = '<s:text name="create_order_nvgh"/>';
var create_order_total_sale_quan = '<s:text name="create_order_total_sale_quan"/>';
var create_order_nvtt = '<s:text name="create_order_nvtt"/>'; 
var create_order_thanh_tien = '<s:text name="create_order_thanh_tien"/>';
/** END TẠO �?ƠN HÀNG */

/** X�?C NHẬN �?ƠN HÀNG*/
var confirm_order_STT = '<s:text name="confirm_order_STT"/>';
var confirm_order_so_ref = '<s:text name="confirm_order_so_ref"/>'; 
var confirm_order_so_don_hang = '<s:text name="confirm_order_so_don_hang"/>';
var confirm_order_so_chung_tu = '<s:text name="confirm_order_so_chung_tu"/>';
var confirm_order_nvbh = '<s:text name="confirm_order_nvbh"/>';
var confirm_order_khach_hang = '<s:text name="confirm_order_khach_hang"/>'; 
var confirm_order_tong_tien = '<s:text name="confirm_order_tong_tien"/>';
var confirm_order_chiet_khau = '<s:text name="confirm_order_chiet_khau"/>'; 
var confirm_order_thanh_toan = '<s:text name="confirm_order_thanh_toan"/>';
var confirm_order_ngay = '<s:text name="confirm_order_ngay"/>';
var confirm_order_dia_chi = '<s:text name="confirm_order_dia_chi"/>';
var confirm_order_nvgh= '<s:text name="confirm_order_nvgh"/>'; 
var sale_product_customer_sl= '<s:text name="sale_product_customer_sl"/>';
/** END X�?C NHẬN �?ƠN HÀNG*/

/*** Begin gia san pham*/
var price_grid_column1 = '<s:text name="price_grid_column1"/>';
var price_grid_column2 = '<s:text name="price_grid_column2"/>';
var price_grid_column3 = '<s:text name="price_grid_column3"/>';
var price_grid_column4 = '<s:text name="price_grid_column4"/>';
var	price_grid_column5 = '<s:text name="price_grid_column5"/>';
var price_grid_column6 = '<s:text name="price_grid_column6"/>';
var price_grid_column7 = '<s:text name="price_grid_column7"/>';
var price_grid_column8 = '<s:text name="price_grid_column8"/>';
var price_grid_column9 = '<s:text name="price_grid_column9"/>';
var price_grid_column10 = '<s:text name="price_grid_column10"/>';
var price_grid_column11 = '<s:text name="price_grid_column11"/>';
var price_grid_column12 = '<s:text name="price_grid_column12"/>';
var price_grid_column13 = '<s:text name="price_grid_column13"/>';
var price_grid_column14 = '<s:text name="price_grid_column14"/>';
var price_grid_column15 = '<s:text name="price_grid_column15"/>';
var price_grid_column16 = '<s:text name="price_grid_column16"/>';
var price_grid_column17 = '<s:text name="price_grid_column17"/>';
var price_grid_column18 = '<s:text name="price_grid_column18"/>';
var price_search_error_01='<s:text name="price_search_error_01"/>';
var price_search_error_02='<s:text name="price_search_error_02"/>';
var price_search_error_03='<s:text name="price_search_error_03"/>';
var price_search_error_04='<s:text name="price_search_error_04"/>';
var price_search_error_05='<s:text name="price_search_error_05"/>';
var price_search_error_06='<s:text name="price_search_error_06"/>';
var price_search_error_07='<s:text name="price_search_error_07"/>';
var price_search_error_08='<s:text name="price_search_error_08"/>';
var price_search_error_09='<s:text name="price_search_error_09"/>';
var price_search_error_10='<s:text name="price_search_error_10"/>';
var price_search_error_11='<s:text name="price_search_error_11"/>';
var price_search_error_12='<s:text name="price_search_error_12"/>';
var price_search_error_13='<s:text name="price_search_error_13"/>';
var price_search_error_14='<s:text name="price_search_error_14"/>';
var common_status_stopped='<s:text name="common.status.stopped"/>';
var price_stt='<s:text name="price_stt"/>';
var price_unit_code='<s:text name="price_unit_code"/>';
var price_unit_name='<s:text name="price_unit_name"/>';
var price_change='<s:text name="price_change"/>';
var catalog_product_com='<s:text name="catalog.product.com"/>';
var sale_in_price_do_you_want_stop='<s:text name="sale.in.price.do.you.want.stop"/>';
 /*** End gia san pham*/
 
 /*** Begin nhap xuat kho dieu chinh*/
 var stock_update_pxk_kiem_vcnb = '<s:text name="stock.update.title.phieu.xuat.kho.kiem.van.chuyen.noi.bo"/>';
 var stock_update_ten_lenh_dieu_dong = '<s:text name="stock.update.title.ten.lenh.dieu.dong"/>';
 var stock_update_ngay_lenh_dieu_dong = '<s:text name="stock.update.title.ngay.lenh.dieu.dong"/>';
 var stock_update_noi_dung = '<s:text name="stock.update.title.noi.dung"/>';
 var stock_update_gia_tri_so_luong_err = '<s:text name="stock.update.title.gia.tri.so.luong.error"/>';
 var stock_update_vuot_qua_gia_tri_ton_kho = '<s:text name="stock.update.title.vuot.qua.gia.tri.ton.kho"/>';
 var stock_update_chon_sp_xuat_kho = '<s:text name="stock.update.title.msg.chon.san.pham.xuat.kho"/>';
 var stock_update_chon_loai_don_dieu_chinh = '<s:text name="stock.update.title.msg.chon.loai.don.dieu.chinh"/>';
 var stock_update_gia_tri_sl_khong_hop_le = '<s:text name="stock.update.title.gia.tri.so.luong.khong.hop.le"/>';
 var stock_update_confirm_delete_product = '<s:text name="stock.update.title.confirm.delete.product"/>';
 var stock_update_confirm_delete_lot = '<s:text name="stock.update.title.confirm.delete.lot"/>';
 var stock_update_khong_co_sp_duoc_dieu_chinh = '<s:text name="stock.update.title.khong.co.sp.duoc.dieu.chinh"/>';
 var stock_update_chua_chon_kho_sp_can_nhap_xuat = '<s:text name="stock.update.title.chua.chon.kho.sp.can.nhap.xuat"/>';
 var stock_update_quantity_error = '<s:text name="stock.update.title.quantity.error"/>';
 var stock_update_quantity_more_than_avai_quantity = '<s:text name="stock.update.title.quantity.more.than.avai.quantity"/>';
 var stock_update_product_code_invalid = '<s:text name="stock.update.title.product.code.invalid"/>';
 var stock_update_yeu_cau_nhap_lo = '<s:text name="stock.update.title.yeu.cau.nhap.lo"/>';
 var stock_update_tong_sl_nhap_lo_err = '<s:text name="stock.update.title.tong.sl.nhap.lo.err"/>';
 var stock_update_ma_sp_cung_kho_hon_mot_dong = '<s:text name="stock.update.title.ma.sp.cung.kho.hon.mot.dong"/>';
 var stock_update_confirm_adjust = '<s:text name="stock.update.title.confirm.adjust"/>';
 var stock_update_lot_quantity_invalid = '<s:text name="stock.update.title.lot.quantity.invalid"/>';
 var stock_update_lot_quantity_invalid_ex = '<s:text name="stock.update.title.lot.quantity.invalid.ex"/>';
 var stock_update_lot_quantity_more_than_stock = '<s:text name="stock.update.title.lot.quantity.more.than.stock"/>';
 var stock_update_lot_quantity_more_than_stock_ex = '<s:text name="stock.update.title.lot.quantity.more.than.stock.ex"/>';
 var stock_update_exists_lot = '<s:text name="stock.update.title.exists.lot"/>';
 /*** End nhap xuat kho dieu chinh*/
  
 /*** Begin sale plan */
 
 var sale_plan_error_search_01='<s:text name="sale_plan_error_search_01"/>';
 var sale_plan_error_search_02='<s:text name="sale_plan_error_search_02"/>';
 var sale_plan_error_search_03='<s:text name="sale_plan_error_search_03"/>';
 var sale_plan_error_search_04='<s:text name="sale_plan_error_search_04"/>';
 var sale_plan_error_search_05='<s:text name="sale_plan_error_search_05"/>';
 var sale_plan_error_search_06='<s:text name="sale_plan_error_search_06"/>';
 var sale_plan_error_search_07='<s:text name="sale_plan_error_search_07"/>';
 var sale_plan_error_search_08='<s:text name="sale_plan_error_search_08"/>';
 var sale_plan_error_search_09='<s:text name="sale_plan_error_search_09"/>';
 var sale_plan_error_search_10='<s:text name="sale_plan_error_search_10"/>';
 var sale_plan_error_search_11='<s:text name="sale_plan_error_search_11"/>';
 var sale_plan_error_search_12='<s:text name="sale_plan_error_search_12"/>';
 var sale_plan_error_search_13='<s:text name="sale_plan_error_search_13"/>';
 var sale_plan_error_search_14='<s:text name="sale_plan_error_search_14"/>';
 var sale_plan_error_search_15='<s:text name="sale_plan_error_search_15"/>';
 var sale_plan_error_search_16='<s:text name="sale_plan_error_search_16"/>';
 var sale_plan_nvbh_code='<s:text name="sale_plan_nvbh_code"/>';
 var sale_plan_month_allocate='<s:text name="sale_plan_month_allocate"/>';
 var sale_plan_error_input_01='<s:text name="sale_plan_error_input_01"/>';
 var sale_plan_tongcong='<s:text name="sale_plan_tongcong"/>';
 var sale_plan_error_input_02='<s:text name="sale_plan_error_input_02"/>';
 var sale_plan_error_input_03='<s:text name="sale_plan_error_input_03"/>';
 var sale_plan_confirm_01='<s:text name="sale_plan_confirm_01"/>';
 var sale_plan_confirm='<s:text name="sale_plan_confirm"/>';
 var sale_plan_save_success='<s:text name="sale_plan_save_success"/>';
 var sale_plan_monthplan='<s:text name="sale_plan_monthplan"/>';
 var sale_plan_excel_export='<s:text name="sale_plan_excel_export"/>';
 var sale_plan_unit_code='<s:text name="sale_plan_unit_code"/>';
 var sale_plan_unit_name='<s:text name="sale_plan_unit_name"/>';
 var sale_plan_search_unit='<s:text name="sale_plan_search_unit"/>';
 var sale_plan_unit_allocate='<s:text name="sale_plan_unit_allocate"/>';
 var sale_plan_product='<s:text name="sale_plan_product"/>';
 var sale_plan_productname='<s:text name="sale_plan_productname"/>';
 var sale_plan_search_product='<s:text name="sale_plan_search_product"/>';
 var sale_plan_view_error='<s:text name="sale_plan_view_error"/>';
 var sale_plan_search_info='<s:text name="sale_plan_search_info"/>';
 var sale_plan_search='<s:text name="sale_plan_search"/>';
 var jsp_common_stt='<s:text name="jsp_common_stt"/>';

 /*** End sale plan */
 
  /*** Begin route */
  var create_route_tuan_0_54 = '<s:text name="ss.create.route.tuan.0.54"/>';
  var create_route_tan_suat_0_54 = '<s:text name="ss.create.route.tan.suat.0.54"/>';
  var create_route_add_cust_route = '<s:text name="ss.create.route.title.add.cust.route"/>';
  var create_route_confirm_delete_cust = '<s:text name="ss.create.route.confirm.delete.cust"/>';
  var create_route_confirm_delete_route = '<s:text name="ss.create.route.confirm.delete.route"/>';
  var create_route_confirm_update_route = '<s:text name="ss.create.route.confirm.update.route"/>';
  var create_route_confirm_create_route = '<s:text name="ss.create.route.confirm.create.route"/>';
  var create_route_chua_chon_kh = '<s:text name="ss.create.route.chua.chon.kh"/>';
  var create_route_find_no_resul = '<s:text name="ss.create.route.find.no.result"/>';
  var create_route_from_date_null = '<s:text name="imp.tuyen.clmn.error.tuNgay.null"/>';
  var create_route_to_date_null = '<s:text name="imp.tuyen.clmn.error.denNgay.null"/>';
  var create_route_ckgt_bat_buoc = '<s:text name="ss.create.route.ckgt.bat.buoc"/>';
  var create_route_tan_suat_is_not_number = '<s:text name="ss.create.route.tan.suat.is.not.number"/>';
  var create_route_tan_suat_0_100 = '<s:text name="ss.create.route.tan.suat.0.100"/>';
  var create_route_time_line_and_line = '<s:text name="ss.create.route.time.line.and.line"/>';
  var create_route_line_is_not_require = '<s:text name="ss.create.route.from.date.line.is.not.require"/>';
  var create_route_cust_not_ckgt = '<s:text name="ss.create.route.cust.not.ckgt.in.line"/>';
  var create_route_have_not_cust_add = '<s:text name="ss.create.route.have.not.cust.add"/>';
  var create_route_no_route_is_change = '<s:text name="ss.create.route.no.route.is.change"/>';
  var create_route_cust_route_todate = '<s:text name="ss.create.route.cust.route.todate"/>';
  var create_route_cust_route_todate_null = '<s:text name="ss.create.route.cust.route.todate.null"/>';
  var create_route_confirm_direct_add_page = '<s:text name="ss.create.route.confirm.direct.add.route.page"/>';
  var create_route_no_choose_staff_code = '<s:text name="ss.create.route.no.choose.staff.code"/>';
  var create_route_no_data_to_update = '<s:text name="ss.assign.route.set.order.error.data.null"/>';
  var create_route_stt_ma_khong_lon_hon_sl_kh = '<s:text name="ss.create.route.stt.ma.khong.lon.hon.sl.kh"/>';
  var create_route_stt_lon_hon_0 = '<s:text name="ss.create.route.stt.lon.hon.0"/>';
  var create_route_stt_code_duplicate_code = '<s:text name="ss.create.route.stt.code.duplicate.code"/>';
  var create_route_sale_day_chua_chon = '<s:text name="ss.create.route.sale.day.chua.chon"/>';
  var create_route_confirm_update_stt = '<s:text name="ss.create.route.confirm.update.stt"/>';
  var create_route_tt = '<s:text name="ss.create.route.tt"/>';
  var create_route_cust_have_not_ttgt = '<s:text name="ss.create.route.cust.have.not.ttgt"/>';
  var create_route_assign_staff_route= '<s:text name="ss.assign.staff.route.title"/>';
  var create_route_set_order_permission= '<s:text name="ss.create.route.set.order.perimission"/>';
  var sale_plan_choose_staff= '<s:text name="sale_plan_choose_staff"/>';
  var sale_plan_no_staff= '<s:text name="sale_plan_no_staff"/>';
 /*** End route */

 /*** Begin image manager */
 var image_manager_fromdate = '<s:text name="image_manager_fromdate"/>';
 var image_manager_todate = '<s:text name="image_manager_todate"/>';
 var image_manager_custcode = '<s:text name="image_manager_custcode"/>';
 var image_manager_nameoraddress = '<s:text name="image_manager_nameoraddress"/>';
 var image_manager_search_image = '<s:text name="image_manager_search_image"/>';
 var image_manager_imageofprogram = '<s:text name="image_manager_imageofprogram"/>';
 var image_manager_confirm = '<s:text name="image_manager_confirm"/>';
 var image_manager_search_error_01 = '<s:text name="image_manager_search_error_01"/>';
 var image_manager_search_error_02 = '<s:text name="image_manager_search_error_02"/>';
 var image_manager_search_error_03 = '<s:text name="image_manager_search_error_03"/>';
 var image_manager_search_error_04 = '<s:text name="image_manager_search_error_04"/>';
 var image_manager_search_error_05 = '<s:text name="image_manager_search_error_05"/>';
 var image_manager_search_error_06 = '<s:text name="image_manager_search_error_06"/>';
 var image_manager_search_error_07 = '<s:text name="image_manager_search_error_07"/>';
 var image_manager_search_error_08 = '<s:text name="image_manager_search_error_08"/>';
 var image_manager_search_error_09 = '<s:text name="image_manager_search_error_09"/>';
 var image_manager_search_error_10 = '<s:text name="image_manager_search_error_10"/>';
 var image_manager_search_error_11 = '<s:text name="image_manager_search_error_11"/>';
 var image_manager_search_error_12 = '<s:text name="image_manager_search_error_12"/>';
 var image_manager_search_error_13 = '<s:text name="image_manager_search_error_13"/>';
 var image_manager_search_error_14 = '<s:text name="image_manager_search_error_14"/>';
 var image_manager_search_error_15 = '<s:text name="image_manager_search_error_15"/>';
 var image_manager_search_error_16 = '<s:text name="image_manager_search_error_16"/>';
 
 
 
 /**CUSTOMER*/
var catalog_customer_require ='<s:text name="catalog.customer.require"/>';
var catalog_customer_active ='<s:text name="catalog.customer.active"/>';
var catalog_customer_pausing ='<s:text name="catalog.customer.pausing"/>';
var catalog_customer_data_not_found ='<s:text name="catalog.customer.data.not.found"/>';
var catalog_customer_pause ='<s:text name="catalog.customer.pause"/>';
var catalog_customer_draf ='<s:text name="catalog.customer.draf"/>';
var catalog_customer_reject ='<s:text name="catalog.customer.reject"/>';
var catalog_customer_delete='<s:text name="catalog.customer.delete"/>';
var catalog_customer_is_vat ='<s:text name="catalog.customer.is.vat"/>';
var catalog_customer_not_vat ='<s:text name="catalog.customer.not.vat"/>';
var catalog_customer_amount_revenue_limit='<s:text name="catalog.customer.amount.revenue.limit"/>';
var catalog_customer_amount_revenue_from='<s:text name="catalog.customer.amount.revenue.from"/>';
var catalog_customer_amount_revenue_to='<s:text name="catalog.customer.amount.revenue.to"/>';
var catalog_customer_create_new ='<s:text name="catalog.customer.create.new"/>';
var catalog_customer_update ='<s:text name="catalog.customer.update"/>';
var catalog_customer_info ='<s:text name="catalog.customer.info"/>';
var catalog_customer_cat_info ='<s:text name="catalog.customer.cat.info"/>';
var catalog_customer_org ='<s:text name="catalog.customer.org"/>';
var catalog_customer_provices ='<s:text name="catalog.customer.provices"/>';
var catalog_customer_districts ='<s:text name="catalog.customer.districts"/>';
var catalog_customer_areas ='<s:text name="catalog.customer.areas"/>';
var catalog_customer_infos ='<s:text name="catalog.customer.infos"/>';
var catalog_customer_type ='<s:text name="catalog.customer.type"/>';
var catalog_customer_aply_debit_limit ='<s:text name="catalog.customer.aply.debit.limit"/>';
var catalog_customer_debit_limit ='<s:text name="catalog.customer.debit.limit"/>';
var catalog_customer_debit_date_limit ='<s:text name="catalog.customer.debit.date.limit"/>';
var catalog_customer_debit_limit_now ='<s:text name="catalog.customer.debit.limit.now"/>';
var catalog_customer_amount_debit_limit ='<s:text name="catalog.customer.amount.debit.limit"/>';
var catalog_customer_vat_info ='<s:text name="catalog.customer.vat.info"/>';
var catalog_customer_print_vat ='<s:text name="catalog.customer.print.vat"/>';
var catalog_customer_org_name ='<s:text name="catalog.customer.org.name"/>';
var catalog_customer_resentation ='<s:text name="catalog.customer.resentation"/>';
var catalog_customer_tax_no ='<s:text name="catalog.customer.tax.no"/>';
var catalog_customer_account_no ='<s:text name="catalog.customer.account.no"/>';
var catalog_customer_bank='<s:text name="catalog.customer.bank"/>';
var catalog_customer_mobile ='<s:text name="catalog.customer.mobile"/>';
var catalog_customer_telephone ='<s:text name="catalog.customer.telephone"/>';
var catalog_customer_email ='<s:text name="catalog.customer.email"/>';
var catalog_customer_status ='<s:text name="catalog.customer.status"/>';
var catalog_customer_full_view ='<s:text name="catalog.customer.full.view"/>';
var catalog_customer_ignore ='<s:text name="catalog.customer.ignore"/>';
var catalog_customer_create ='<s:text name="catalog.customer.create"/>';
var catalog_customer_update ='<s:text name="catalog.customer.update"/>';
var catalog_customer_cat ='<s:text name="catalog.customer.cat"/>';
var catalog_customer_cat_select ='<s:text name="catalog.customer.cat.select"/>';
var catalog_customer_amount ='<s:text name="catalog.customer.amount"/>';
var catalog_customer_amount_select ='<s:text name="catalog.customer.amount.select"/>';
var catalog_customer_add ='<s:text name="catalog.customer.add"/>';
var catalog_customer_no_result ='<s:text name="catalog.customer.no.result"/>';
var catalog_customer_select ='<s:text name="catalog.customer.select"/>';
var catalog_customer_search_info ='<s:text name="catalog.customer.search.info"/>';
var catalog_customer_phone ='<s:text name="catalog.customer.phone"/>';
var catalog_customer_search ='<s:text name="catalog.customer.search"/>';
var catalog_customer_list ='<s:text name="catalog.customer.list"/>';
var catalog_customer_approve ='<s:text name="catalog.customer.approve"/>';
var catalog_customer_org_code='<s:text name="catalog.customer.org.code"/>';
var catalog_customer_no='<s:text name="catalog.customer.no"/>';
var catalog_customer_account='<s:text name="catalog.customer.account"/>';
var catalog_customer_full_name='<s:text name="catalog.customer.full.name"/>';
var catalog_customer_phone_number_start_with_zero='<s:text name="catalog.customer.phone.number.start.with.zero"/>';
var catalog_customer_mobi_phone_number_start_with_zero='<s:text name="catalog.customer.mobi.phone.number.start.with.zero"/>';
var catalog_customer_phone_number_from_9_to_12='<s:text name="catalog.customer.phone.number.from.9.to.12"/>';
var catalog_customer_mobi_phone_number_from_9_to_12='<s:text name="catalog.customer.mobi.phone.number.from.9.to.12"/>';
var catalog_customer_province_require='<s:text name="catalog.customer.province.require"/>';
var catalog_customer_district_require='<s:text name="catalog.customer.district.require"/>';
var catalog_customer_area_require='<s:text name="catalog.customer.area.require"/>';
var catalog_customer_code_duplicate ='<s:text name="catalog.customer.code.duplicate"/>';
var catalog_customer_debit_amount_must_number='<s:text name="catalog.customer.debit.amount.must.number"/>';
var catalog_customer_debit_limit_must_number='<s:text name="catalog.customer.debit.limit.must.number"/>';
var catalog_customer_shop_code ='<s:text name="catalog.customer.shop.code"/>';
var catalog_customer ='<s:text name="catalog.customer"/>';
var catalog_customer_code ='<s:text name="catalog.customer.code"/>';
var catalog_customer_name ='<s:text name="catalog.customer.name"/>';
var catalog_customer_date_of_birth ='<s:text name="catalog.customer.date.of.birth"/>';
var catalog_customer_first_name ='<s:text name="catalog.customer.first.name"/>';
var catalog_customer_country ='<s:text name="catalog.customer.country"/>';
var catalog_customer_province ='<s:text name="catalog.customer.province"/>';
var catalog_customer_province_only ='<s:text name="catalog.customer.province.only"/>';
var catalog_customer_shop_code ='<s:text name="catalog.customer.shop.code"/>';
var catalog_customer_shop_name ='<s:text name="catalog.customer.shop.name"/>';
var catalog_customer_district ='<s:text name="catalog.customer.district"/>';
var catalog_customer_precinct ='<s:text name="catalog.customer.precinct"/>';
var catalog_customer_street ='<s:text name="catalog.customer.street"/>';
var catalog_customer_mobiphone ='<s:text name="catalog.customer.mobiphone"/>';
var catalog_customer_on_order_name ='<s:text name="catalog.customer.on.order.name"/>';
var catalog_customer_receive_address ='<s:text name="catalog.customer.receive.address"/>';
var catalog_customer_cashier ='<s:text name="catalog.customer.cashier"/>';
var catalog_customer_delivery ='<s:text name="catalog.customer.delivery"/>';
var catalog_customer_group_transfer ='<s:text name="catalog.customer.group.transfer"/>';
var catalog_customer_group_transfer_code ='<s:text name="catalog.customer.group.transfer.code"/>';
var catalog_customer_debit_amount ='<s:text name="catalog.customer.debit.amount"/>';
var catalog_customer_debit_date ='<s:text name="catalog.customer.debit.date"/>';
var catalog_customer_confirm ='<s:text name="common.catalog.customer.confirm"/>';
var catalog_customer_tansuat ='<s:text name="catalog.customer.tansuat"/>';
var catalog_year_code ='<s:text name="catalog.year.code"/>';
var catalog_address ='<s:text name="catalog.address"/>';
var catalog_position='<s:text name="catalog.position"/>';
var catalog_education='<s:text name="catalog.education"/>';
var catalog_day_month_code ='<s:text name="catalog.day.month.code"/>';
var catalog_customer_level_cat_exist ='<s:text name="catalog.customer.level.cat.exist"/>';
var catalog_customer_cat_exist ='<s:text name="catalog.customer.cat.exist"/>';
var catalog_customer_loyalty ='<s:text name="catalog.customer.loyalty"/>';
var catalog_customer_cat ='<s:text name="catalog.customer.cat"/>';
var catalog_customer_view_image_title='<s:text name="catalog.customer.view.image.title"/>';
var catalog_customer_region_type ='<s:text name="catalog.customer.region.type"/>';
var catalog_customer_region_display ='<s:text name="catalog.customer.region.display"/>';
var catalog_customer_contact ='<s:text name="catalog.customer.contact"/>';
var catalog_customer_house_number ='<s:text name="catalog.customer.house.number"/>';
var catalog_customer_area_code ='<s:text name="catalog.customer.area.code"/>';
var catalog_product_month ='<s:text name="catalog.product.month"/>';
var catalog_customer_street_label ='<s:text name="catalog.customer.street.label"/>';
var catalog_customer_house_number_label ='<s:text name="catalog.customer.house.number.label"/>';
var catalog_customer_house_number_cmnd ='<s:text name="catalog.customer.house.number.cmnd"/>';
var catalog_customer_mobile_phone ='<s:text name="catalog.customer.mobile.phone"/>';
var catalog_customer_account_number ='<s:text name="catalog.customer.account.number"/>';
var catalog_customer_name_in_bill ='<s:text name="catalog.customer.name.in.bill"/>';
var catalog_customer_nguoi_dai_dien ='<s:text name="catalog.customer.nguoi.dai.dien"/>';
var catalog_customer_ten_ngan_hang ='<s:text name="catalog.customer.ten.ngan.hang"/>';
var catalog_customer_dia_chi_hoa_don ='<s:text name="catalog.customer.dia.chi.hoa.don"/>';
var catalog_customer_shop_code_NPP ='<s:text name="catalog.customer.shop.code.NPP"/>';
var catalog_customer_delivery_code ='<s:text name="catalog.customer.delivery.code"/>';
var catalog_customer_cashier_code ='<s:text name="catalog.customer.cashier.code"/>';
var catalog_customer_name_customer ='<s:text name="catalog.customer.name.customer"/>';
var common_bank_name = '<s:text name="common.bank.name"/>';
var catalog_customer_sale_cat = '<s:text name="catalog.customer.sale.cat"/>'; 
var catalog_customer_import_cat_info = '<s:text name="catalog.customer.import.cat.info"/>';
 /*** End image manager */
 
 /**
 * sale supervise
 */
var jsSaleSuperviseStaffCodeText = '<s:text name="js.sale.supervise.constant.staff.code.text"></s:text>';
var jsSaleSuperviseRouteCodeText = '<s:text name="js.sale.supervise.constant.route.code.text"></s:text>';
var jsSaleSuperviseContextMenuDaySalesAndQuantity = '<s:text name="js.sale.supervise.context.menu.day.sales.and.quantity"></s:text>';
var jsSaleSuperviseContextMenuDaySales = '<s:text name="js.sale.supervise.context.menu.day.sales"></s:text>';
var jsSaleSuperviseControlRefreshStaffPositionText = '<s:text name="js.sale.supervise.content.header.region.control.refresh.staff.position"></s:text>';
var jsSaleSuperviseControlCalcDistanceText = '<s:text name="js.sale.supervise.content.header.region.control.calc.distance"></s:text>';
var jsSaleSuperviseControlDrawPath = '<s:text name="js.sale.supervise.content.footer.control.draw.path"></s:text>';
var jsSaleSuperviseDlgDaySalesSalerTitle = '<s:text name="js.sale.supervise.dialog.day.sales.saler.title"></s:text>';
var jsSaleSuperviseDlgDaySalesSupervisorTitle = '<s:text name="js.sale.supervise.dialog.day.sales.supervisor.title"></s:text>';
var jsSaleSuperviseDlgMonthAccumulateSupervisorTitle = '<s:text name="js.sale.supervise.dialog.month.accumulate.supervisor.title"></s:text>';
var jsSaleSuperviseSupervisorMarkerGroupAllText = '<s:text name="js.sale.supervise.supervisor.marker.supervise.group.all.text"></s:text>';
var jsSaleSuperviseSalerMarkerNoCustomerVisited = '<s:text name="js.sale.supervise.saler.marker.no.customer.visited"></s:text>';
var jsSaleSuperviseSalerPathDeletePathText = '<s:text name="js.sale.supervise.saler.delete.path"></s:text>';
var jsSaleSuperviseSalerPathPointPopupText = '<s:text name="js.sale.supervise.path.point.popup.position.update.time"></s:text>';
var jsSaleSuperviseTreeGridColumnHeader1st = '<s:text name="js.sale.supervise.tree.grid.header.1st"></s:text>';
var jsSaleSuperviseTreeGridColumnHeader2nd = '<s:text name="js.sale.supervise.tree.grid.header.2nd"></s:text>';
var jsSaleSuperviseTreeGridColumnHeader3rd = '<s:text name="js.sale.supervise.tree.grid.header.3rd"></s:text>';
// END

//clockpicker
var msgChonGio = '<s:text name="clockpicker.chon.gio" />';

var msgKhongTheLuu = '<s:text name="jsp.lock.danhmuc" />';
var msgTenDanhMucKTDB = '<s:text name="jsp.dm.name.ky.tu.dac.biet"/>';
/******************************************************************
 * promotion
 */
// Begin Feedback
var feedback_upload_success = '<s:text name="feedback.upload.success"></s:text>';
var jsp_common_s_upload_maximum = '<s:text name="jsp.common.s.upload.maximum"></s:text>';
var jsp_common_s_please_upload_select = '<s:text name="jsp.common.s.please.upload.select"></s:text>';

// End Feedback

var promotionMultiLanguage = {
		code: '<s:text name="catalog.promotion.code"/>',
		name: '<s:text name="catalog.promotion.name"/>',
		description: '<s:text name="jsp.common.s.mo.ta"/>'
}

var reportMultiLanguage = {
		fromDate: '<s:text name="common.date.fromdate"/>',
		toDate: '<s:text name="common.date.todate"/>',
		currentSysDate: '<s:text name="common.date.sysdate"/>',
		cycle: '<s:text name="common.cycle.name"/>',
		shop: '<s:text name="common.shop.name.lable"/>',
		description: '<s:text name="jsp.common.s.mo.ta"/>'
}

//settings
var ss_settings_system_confirm_update_data = '<s:text name="ss.settings.system.confirm.update.data"></s:text>';
var jsp_settings_system_hour_auto_close_date = '<s:text name="jsp.settings.system.hour.auto.close.date"></s:text>';
var jsp_settings_system_distance_check_customer = '<s:text name="jsp.settings.system.distance.check.customer"></s:text>';
var jsp_settings_system_problem_attach_size_max = '<s:text name="jsp.settings.system.problem.attach.size.max"></s:text>';
var jsp_settings_system_problem_file_type_upload = '<s:text name="jsp.settings.system.problem.file.type.upload"></s:text>';
var jsp_settings_system_tablet_time_request_position = '<s:text name="jsp.settings.system.tablet.time.request.position"></s:text>';
var jsp_settings_system_tablet_time_request_sync_data = '<s:text name="jsp.settings.system.tablet.time.request.sync.data"></s:text>';
</script>
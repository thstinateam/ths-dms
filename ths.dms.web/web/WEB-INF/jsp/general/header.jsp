<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<style type="text/css">
#header .MenuSection ul{
	list-style: none; 
	font-family: Arial, san-serif;
	font-size: 12px;
	padding: 0;
}
#header .MenuSection ul li{
	float:left;
	position:relative;
	/*font-size: 12px;*/
}
#header .MenuSection ul a{
	text-decoration:none;
	/*color:#fff;*/
	display:block;
	/*font-weight:bold;*/
	/*padding:6px 10px;*/
	padding-left:10px;
	padding-right:10px;
	background-color:transparent;
	font-size: 12.5px;
}
#header .MenuSection ul a:hover, #header .MenuSection li:hover > a {
	background-color:#71B7ED;
	color:#fff;
}
#header .MenuSection li ul{
	position:absolute;
	display:none;
	width:200px;
}
#header .MenuSection li li {
	width: 100%;
}
#header .MenuSection, #header .MenuSection li ul a{
	/*background:#2882BC;
	color:#fff;*/
}
#header .MenuSection li:hover ul {
	display:block;
}
#header .MenuSection li:hover ul li ul{
	display:none;
}
#header .MenuSection li:hover ul li:hover ul {
	display:block;
	position:absolute;
	left:200px;
	top:0;
}
#header .MenuList li a {
	text-shadow:none;
}
/*#header .MenuList ul ul li {
	border-top: none;
	border-left:1px solid #45b5db;
	border-right:1px solid #45b5db;
	border-bottom:1px solid #45b5db;
}*/
#header .MenuList ul ul {
	border-top:none;
	border-bottom:1px solid #45b5db;
	border-left:1px solid #45b5db;
	border-right:1px solid #45b5db;
}
#header .MenuList .SubMenuList li a.HasSubMenuItem {
	background-image:url(/resources/images/icon_submenu2.png);
	background-position:right 5px center;
	background-repeat:no-repeat;
}
#header ul.MenuList > li > a {
	font-size: 13px;
}
</style>


<!-- <div class="MenuSection"> -->
<div class="leftpanelinner">  
	<ul class="nav nav-pills nav-stacked nav-bracket">
		<s:iterator id="permission" value=" #session.cmsUserToken.listMenu">
       		    
       		<s:if test='#permission.url!= null && #permission.url.length()>0'>
	       		<li id="<s:property value="#permission.formId"/>">
	       			<a  href="<s:property value="#permission.url"/>"><i class="fa fa-th-list"></i><span><s:property value="#permission.formName"/></span></a>
	       		</li>
       		</s:if>
       		<s:else>
       			<li class="nav-parent">

       				<a  href="javascript:void(0);">
					<s:if test='#permission.formId == 570'>
       						<i class="fa fa-cog"></i>
       					</s:if>
       					<s:if test='#permission.formId == 398'>
       						<i class="fa fa-book"></i>
       					</s:if>
       					<s:if test='#permission.formId == 1259'>
       						<i class="fa fa-map-marker"></i>
       					</s:if>
       					<s:if test='#permission.formId == 1258'>
       						<i class="fa fa-gift"></i>
       					</s:if>
       					<s:if test='#permission.formId == 424'>
       						<i class="fa fa-shopping-cart"></i>
       					</s:if>
       					<s:if test='#permission.formId == 1257'>
       						<i class="fa fa-cubes"></i>
       					</s:if>
       					<s:if test='#permission.formId == 1308'>
       						<i class="fa fa-truck"></i>
       					</s:if><span><s:property value="#permission.formName"/></span></a>
       				<s:if test="#permission.listForm != null && #permission.listForm.size() > 0">
					<ul class="children" id="sub_<s:property value="#permission.formId"/>">
			   			<s:iterator id="childFunction" value="#permission.listForm" status="u">
			   				<s:if test="#childFunction.listForm !=null && #childFunction.listForm.size()>0">
								<s:iterator id="childChildFunction" value="#childFunction.listForm" status="v">
									<li>
										<a id="child_Child_<s:property value="#childChildFunction.formId"/>" href="<s:property value="#childChildFunction.url"/>" ><s:property value="#childChildFunction.formName"/></a>
									</li>
								</s:iterator>
			   				</s:if>
			   				<s:else>
			   					<li>
									<a id="child_<s:property value="#childFunction.formId"/>" href="<s:property value="#childFunction.url"/>" ><s:property value="#childFunction.formName"/></a>								
								</li>
			   				</s:else>			   				
		               </s:iterator>
	               </ul>
	            </s:if>
       				
       			</li>
       		</s:else>					
				
	   	</s:iterator>
	</ul>
</div>
<script type="text/javascript">
	/**
	* function thay doi pass cua CurrentUser khi dang nhap thanh cong
	* @author vuongmq
	* @since 24/08/2015
	*/
	function changePassPopup() {
		$('.ErrorMsgStyle').html('').hide();
		$('#searchStyle1EasyUIDialogChangePassDivChangePass').css('visibility','visible');
		$('#searchStyle1EasyUIDialogChangePass').dialog({
			title: 'Đổi mật khẩu',
			width: 426,
			height: 280,
			closed: false,
			cache: false,
			modal: true,
			onOpen: function() {
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassOld').focus();
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassOld').val('');
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassNew').val('');
				$('#searchStyle1EasyUIDialogChangePass #seachStyle1PassRetype').val('');
				//$('.easyui-dialog #seachStyle1Code')
				$(window).bind('keyup', function(event) {
					if (event.keyCode == 13) {
						$('#__btnSaveChangePass').click();
					}
				});
			},
			onClose : function() {
				$('#searchStyle1EasyUIDialogChangePassDivChangePass').css("visibility", "hidden");
			}
		});
	}
	$(window).bind('click', function() {
		if (isCloseInfoUser != undefined && isCloseInfoUser != null && isCloseInfoUser == 0) {
			$('.AccOption').hide();
		} else {
			isCloseInfoUser = 0;
		}
	});
</script>
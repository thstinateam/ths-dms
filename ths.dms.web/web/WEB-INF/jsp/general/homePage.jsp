<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="LoginTheme">
	<div id="LoginContainer" style="background: none;" >
		<div class="LoginBoxMiddle">
			<div id="LoginContent">
				<div style="height: 774px"></div>
			</div>				
		</div>
	</div>   
</div>

<div style="display:none">
	<div id="warningPopupByLstRole" class="easyui-dialog" title="Cảnh báo" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm" >
                <div class="icon-warning" style="width: 48px; height: 48px; float: left; margin-right: 20px;"></div>
                <p style="font-weight: bold; padding: 2px 0px;">Người dùng (<span id="date" style="color: #CC0000;"></span>) chưa được gán vai trò (<span id="date1" style="color: #CC0000;"></span>).</p>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="btnCancelRole">Đóng</button>
                </div>
					<p id="errMsgSelectRole" class="ErrorMsgStyle" style="display: none"></p> 
			</div>
             </div>
	</div>
</div>

<s:if test="lstRoleToken!=null && roleToken==null" >
<div id="selectRoleAndShopDefault" title="" data-options="closed:true, modal:true" style="width:670px;height:auto;">
	<div class="PopupContentMid2">
       	<div class="GeneralForm Search1Form">
  			<div class="Clear" style="height: 20px;"></div>
            <label class="LabelStyle Label2Style">Vai trò<span class="ReqiureStyle">(*)</span></label>
            <div class="BoxSelect BoxSelect11" id="lstRoleDiv">
				<select class="MySelectBoxClass" id="lstRoleToken" onchange="return General.getListShopByRole(this);">
					<option value="0" selected="selected">Chọn vai trò</option>
					<s:iterator value="lstRoleToken" var="obj">
						<option value="<s:property value="roleId" />"><s:property value="roleCode" /> - <s:property value="roleName" /></option>		                			
	                </s:iterator>
				</select>
			</div>
            <div class="Clear"></div>
            <label class="LabelStyle Label2Style">Đơn vị<span class="ReqiureStyle">(*)</span></label>
            <div class="BoxSelect BoxSelect11" id="lstShopByRoleDiv">
				<select class="easyui-combobox" id="lstShopByRole">
					<option value="" code="" name=""></option>
				</select>
			</div>
			<div class="Clear"></div>
            <div class="BtnCenterSection">
            	<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog cmsdefault" id="btnNextByRole" onclick="return General.nextByRole();">
            		<span class="Sprite2">Đồng ý</span>
            	</button>
            	&nbsp;
            	<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnNextByRole" onclick="return General.logoutSystem();">
            		<span class="Sprite2">Thoát</span>
            	</button>
            </div>
            <p id="errMsgDialogNextRole" style="display: none;" class="ErrorMsgStyle"></p>
            <p id="successMsgDialogNextRole" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear"></div>
		</div>
      </div>
</div>
<script type="text/javascript">
	var flagChangePass = '<s:property value="viewChangePassDefault"/>';
	$(function(){
		if (!isNullOrEmpty(flagChangePass) && flagChangePass == StatusType.ACTIVE) {
			UnitTreeCatalog.openDialogChangePassDefault();
		} else {
			$('#lstShopByRoleDiv, #lstShopByRoleDiv').bind('keyup',function(event){
				if(event.keyCode == keyCodes.ENTER){
					return General.nextByRole();
				}
			});
			General.bindComboboxEasyUI("lstShopByRole", 452);		
			$("#lstRoleDiv .MySelectBoxClass").width(452);
			$("#lstRoleDiv .CustomStyleSelectBoxInner").width(420);
			General.openDialogSelectNextRole();
		}
	});
</script>
</s:if>
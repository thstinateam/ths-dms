<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="common-dialog-search-tree-in-grid-choose-single" class="easyui-dialog" title="" data-options="closed:true, modal:true" style="width:670px;height:auto;">
	<div class="PopupContentMid2">
       	<div class="GeneralForm Search1Form">
  			<div class="Clear" style="height: 20px;"></div>
			<label class="LabelStyle Label2Style"><s:text name="work_date_unit_code" /></label>
            <input id="txtShopCodeDlg" type="text" class="InputTextStyle InputText1Style" style="width: 196px;" maxlength="100"/>
            <label class="LabelStyle Label2Style"><s:text name="work_date_unit_name" /></label>
            <input id="txtShopNameDlg" type="text" class="InputTextStyle InputText1Style" style="width: 196px;" maxlength="250"/>
			<div class="Clear"></div>
          	<div class="BtnCenterSection" style="pading:0px; margin: 0px;">
          		<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnComonSearchTreeInGrid" onclick="$('#commonDialogTreeShopG').treegrid('load', {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()});">
          			<span class="Sprite2"><s:text name="jsp.common.timkiem" /></span>
          		</button>
          	</div>
			<h2 class="Title2Style" style="margin-top: 10px;"><s:text name="work_date_result_search" /></h2>
			<div class="Clear"></div>
			<div class="GridSection" id="common-dialog-dgTreeShopGContainerGS" style="padding-top: 0px;">
					 <table id="commonDialogTreeShopG"></table>
			</div>
			<div class="Clear"></div>
		</div>
     </div>
</div>
<script type="text/javascript">
	$(function(){
		$('#txtShopCodeDlg, #txtShopNameDlg, #btnComonSearchTreeInGrid').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#btnComonSearchTreeInGrid').click();
			}
		});
	});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<select class="MySelectBoxClass" id="yearPeriod" onchange="changeYear();">
	<s:iterator value="lstYear">
		<option value='<s:property/>'><s:property/></option>
	</s:iterator>
</select>
<script type="text/javascript">
function changeYear(){
	if (yearPeriodChange != undefined && yearPeriodChange != null) {
		yearPeriodChange.call(this,$('#yearPeriod').val());
	}
}
</script>
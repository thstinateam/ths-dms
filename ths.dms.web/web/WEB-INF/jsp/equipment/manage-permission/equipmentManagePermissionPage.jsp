<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!-- begin vuongmq; 11/05/2015; xuat Excel ATTT -->
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<!-- end vuongmq; 11/05/2015; xuat Excel ATTT -->
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Thiết bị</a>
		</li>
		<li><span>Gán quyền cho người dùng</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
<!-- 				<h2 class="Title2Style">Thông tin tìm kiếm</h2> -->
				<div class="SearchInSection SProduct1Form" style="padding-left: 0px;">
				<div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin tìm kiếm" style="width:1000px;height:150px;"data-options="collapsible:true,closable:false,border:false">
					<label class="LabelStyle Label1Style" style="text-align:left;padding-left: 10px;height:auto;">Đơn vị</label>
					<!-- <input id="shop" type="text" data-placeholder="Tất cả" style="height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" /> -->
					<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="height: auto;" class="InputTextStyle InputText1Style" />
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 20px; height:auto;">Mã người dùng</label>
					<input id="personalCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="1"/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Tên người dùng</label>
					<input id="personalName" type="text" class="InputTextStyle InputText1Style" maxlength="250" tabindex="3"/>
					<label class="LabelStyle Label1Style" style="text-align:left;height:auto;padding-left: 20px">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="personalStatus" tabindex="4">
							<option value="-1" >Tất cả</option>
							<option value="1" selected="selected" >Hoạt động</option>
							<option value="0" >Tạm ngưng</option>
							
						</select>
					</div>
					<div class="Clear"></div>
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnSearch" class="BtnGeneralStyle" onclick="EquipManagePermission.searchStaff();" tabindex="5">Tìm kiếm</button>
						</div>
						<div class="Clear"></div>
					</div>	
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
				</div>		
				</div>	
				<!-- danh sach nguoi dung -->	
				<h2 class="Title2Style" style="height:15px;">
					<span style="float:left;">Danh sách người dùng</span>
					<div style="float:right; margin-top: -5px;margin-right: 27px;">
						<a href="#" id="downloadTemplate"><img src="/resources/images/icon_download_32.png" height="20" width="20" title="Tải mẫu Nhập Excel" tabindex="6"></a>
						<a href="javascript:void(0);" onclick="EquipManagePermission.showDlgImportExcel();"><img src="/resources/images/icon-import.png" height="25" width="25" title="Nhập Excel" tabindex="7"></a>
						<a href="javascript:void(0);" onclick="EquipManagePermission.exportExcel();"><img src="/resources/images/icon-export.png" height="25" width="25" title="Xuất Excel" tabindex="8"></a>					
					</div>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="gridPersonal"></table>
					</div>
				</div>		

				<!-- danh sach quyen -->	
				<div id="panelPermission" style="display: none">
					<h2 class="Title2Style" style="height:15px;">
						<span style="float:left;">Danh sách quyền: <span style="color:red;" id="titleStaff"></span></span>
					</h2>	
						<div class="SearchInSection SProduct1Form">	
							<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Mã quyền</label>
							<input id="permissionCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="9"/>
							<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Tên quyền</label>
							<input id="permissionName" type="text" class="InputTextStyle InputText1Style" maxlength="250" tabindex="10"/>
							<label class="LabelStyle Label1Style" style="text-align:left;height:auto;padding-left: 20px">Trạng thái</label>
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="permissionStatus" tabindex="11">
									<option value="-1" selected="selected">Tất cả</option>
									<option value="1" >Hoạt động</option>
									<option value="0" >Tạm ngưng</option>
									
								</select>
							</div>
							<div class="Clear"></div>
						</div>
						<div class="Clear"></div>
						<div class="SearchInSection SProduct1Form">
							<div class="BtnCenterSection">
								<button id="btnSearchPermission" class="BtnGeneralStyle" onclick="EquipManagePermission.viewStaffPermission();" tabindex="12">Tìm kiếm</button>
							</div>
							<div class="Clear"></div>
						</div>	
						<p id="successMsg" class="LabelStyle Label1Style" style="display: none; margin-left: 10px;"></p>	
						<p id="errMsgPermission" class="ErrorMsgStyle" style="display: none;"></p>
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id="gridPermissionContainer">
							<table id="gridPermission"></table>
						</div>
						<div class="Clear"></div>
						<div class="BtnCenterSection">
							<button id="btnAddPermisson" class="BtnGeneralStyle" onclick="EquipManagePermission.showPopupRole();" tabindex="13">Thêm quyền</button>
							<button id="btnLock" class="BtnGeneralStyle" onclick="EquipManagePermission.updateRoleUser(0)" tabindex="14">Khóa</button>
							<button id="btnUnLock" class="BtnGeneralStyle" onclick="EquipManagePermission.updateRoleUser(1)" tabindex="15">Mở khóa</button>
							<button id="btnDelete" class="BtnGeneralStyle" onclick="EquipManagePermission.updateRoleUser(-1)" tabindex="16">Xóa</button>
						</div>
					</div>							
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup them quyen -->
<div style="display: none">
	<div id="easyuiPopupHistory" class="easyui-dialog" title="Thêm quyền" data-options="closed:true,modal:true"  style="width:400px;height:215px;">
		<div class="PopupContentMid">
	   		<div class="SearchInSection SProduct1Form">
	   			<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;color: #990099;">Mã quyền</label>
				<input id="roleCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="4"/>
				<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;color: #990099;">Tên quyền</label>
				<input id="proleName" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="4"/>
				<div class="GridSection" id="gridRoleDiv">
					<table id="gridRole"></table>
				</div>
			</div>        
		</div>
	</div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Nhập excel gán quyền cho người dùng" data-options="closed:true,modal:true" style="width:500px;height:250px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipManagePermission/importEquipStaffPermission" name="importFrmName" id="importFrmStaffPermission" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1" style="width:368px">
						<input id="fakefilepcStaffPermission" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileStaffPermission" onchange="previewImportExcelFile(this,'importFrmName','fakefilepcStaffPermission', 'errExcelMsg');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a onclick="" id="downloadTemplate" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="return EquipManagePermission.importExcelStaffPermission();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<!-- <p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgBBGN"/> -->
		<p id="successExcelMsg" class="SuccessMsgStyle" style="display:none;"></p>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsg" />
	</div>	
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {	
	$('#panelDeliveryRecord').width($('#panelDeliveryRecord').parent().width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 5px;");
	},1000);
	/** tai file mau temple*/
	$('#downloadTemplate').attr('href',excel_template_path + 'equipment/Bieu_mau_gan_quyen_nguoi_dung.xlsx');
	$('.easyui-dialog #downloadTemplate').attr('href',excel_template_path + 'equipment/Bieu_mau_gan_quyen_nguoi_dung.xlsx');

	var params = new Object();
	params.personalCode = $('#personalCode').val().trim();
	params.personalName = $('#personalName').val().trim();
	params.personalStatus = $('#personalStatus').val();
	
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [EquipmentListEquipment._curShopId]
    });

	 /**dung cho dieu kien xuat excel theo dk search*/
	EquipManagePermission._params = new Object();
	EquipManagePermission._params.personalCode = $('#personalCode').val().trim();
	EquipManagePermission._params.personalName = $('#personalName').val().trim();
	EquipManagePermission._params.personalStatus = $('#personalStatus').val();
	EquipManagePermission._mapRole = new Map();
	$('#gridPersonal').datagrid({
		url:'/equipManagePermission/search-staff',
		method : 'GET',
		height: 'auto',
		width : $(window).width() - 55,
		scrollbarSize:0,
		rownumbers:true,
		fitColumns:true,
		autoRowHeight:true, 
		pagination:true, 
		pageSize:20,
		queryParams:params,
		singleSelect:true,
		pageList  : [20],
		columns:[[		  
			//{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left'},
			{field: 'staffCode',title:'Mã người dùng', width: 60, sortable:false,resizable:false, align: 'left', formatter: function(value,row,index){
				return Utils.XSSEncode(value);
			}  },
			{field: 'staffName',title:'Tên người dùng',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value,row,index){
				return Utils.XSSEncode(value);
			} },
			{field: 'shopCode',title:'Đơn vị',width: 200,sortable:false,resizable:false, align: 'left' , formatter: function(value,row,index){
				return Utils.XSSEncode(row.shopCode + ' - ' + row.shopName);
			} },
			{field: 'status', title: 'Trạng thái', width: 50, sortable:false,resizable:false,align: 'left', formatter: function(value,row,index){
				var html = '';
				if(row.status == 0) {
					html = 'Tạm ngưng';
				}else if(row.status == 1) {
					html = 'Hoạt động';
				}
				return html;
			} },
			{field:'view', title:'', width: 30, align: 'center', formatter: function (value, row, index) {
				var html = '<a href="javascript:void(0)" onclick="EquipManagePermission.viewStaffPermission(\'' + Utils.XSSEncode(row.id) + '\',\'' + Utils.XSSEncode(row.staffCode) +  '\')"><span style="cursor:pointer"><img title="Xem quyền nhân viên" src="/resources/images/icon-view.png"/></span></a>'; 
				return html;
		    }}
		]],
		onBeforeLoad:function(param){											 
		},
		onLoadSuccess :function(data){  	 		    	
		},
	});
	var mapId = new Map();
	$('#gridPermission').datagrid({
			url:'/equipManagePermission/search-permission',
			method : 'GET',
			height: 'auto',
			width : $(window).width() - 55,
			scrollbarSize:0,
			rownumbers:true,
			fitColumns: true,
			autoRowHeight:true, 
			pagination:true, 
			pageSize:10,
			queryParams:params,
			singleSelect:true,
			pageList  : [10],
			columns: [[		  
				{field: 'check', checkbox: true, width: 50, align: 'left'},
				{field: 'roleCode', title: 'Mã quyền', resizable: false, width: 150, align: 'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value); 
				}}, 
				{field: 'roleName', title: 'Tên quyền', resizable: false, width: 250, align: 'left', formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field: 'discription', title: 'Mô tả', resizable: false, width: 300, align: 'left' , formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field: 'status', title: 'Trạng thái', resizable: false, width: 100, align: 'left', formatter: function(value, row, index) {
					var html = '';
					if(row.status == 0) {
						html = 'Tạm ngưng';
					}else if(row.status == 1) {
						html = 'Hoạt động';
					}
					return html;
				}}
			]],
			onBeforeLoad:function(param){											 
			},
			onLoadSuccess :function(data){  	 		    	
		    	$(window).resize();
		    	if (data == null || data.total == 0) {
					
				}else{
					$('.datagrid-header-rownumber').html('STT');		    	
				}		
		    	$('.datagrid-header-row td div').css('text-align','center');
		    	updateRownumWidthForDataGrid('');
				var rows = $('#gridPermission').datagrid('getRows');
				if(rows != undefined && rows != null) {
					$('.datagrid-header-check input').removeAttr('checked');
			    	for(var i = 0; i < rows.length; i++) {
			    		if(EquipManagePermission._mapRole.get(rows[i].id)!=null) {
			    			$('#gridPermission').datagrid('checkRow', i);
			    		}
			    	}
				}
		    	
    		 	$(window).resize();
			},
			onCheck:function(index,row){
				EquipManagePermission._mapRole.put(row.id,row);  
		    },
		    onUncheck:function(index,row){
		    	EquipManagePermission._mapRole.remove(row.id);
		    },
		    onCheckAll:function(rows){
			 	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		EquipManagePermission._mapRole.put(row.id,row);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		EquipManagePermission._mapRole.remove(row.id);	
		    	}
		    }
		});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<style type="text/css">
#player_api {
	width: 678px !important;
}
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">Thiết bị</a></li>
		<li><span>Thiết lập định mức hạng mục</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<!-- <div id="panelEqipItemConfig" class="easyui-panel" title="Thông tin tìm kiếm" style="width: 1000px; height: 200px; padding-top: 20px;" data-options="collapsible:true,closable:false,border:false"> -->
				<h2 class="Title2Style">Thông tin tìm kiếm
					<span style="float:right;margin-right:15px;">
              			<a onclick="Utils.toggleSearchInput(this);" class="searchShow" id="searchHiddenLink" href="javascript:void(0);"></a>
       				</span>
				</h2>
					<div class="SearchInSection SProduct1Form">
						<label class="LabelStyle Label1Style" style="text-align:left; height:auto; width: 66px;" >Dung tích từ</label>
						<input type="text" class="InputTextStyle InputText1Style vinput-money" id="txtFromCapacity" onkeypress="EquipItemConfig.onchangeCapacity(this);" maxlength="12" /> 
						<label class="LabelStyle Label1Style">Đến</label>
						<input type="text" class="InputTextStyle InputText1Style vinput-money" id="txtToCapacity" onkeypress="EquipItemConfig.onchangeCapacity(this);" maxlength="12" /> 
						<label	class="LabelStyle Label1Style">Trạng thái</label>
						<div class="BoxSelect BoxSelect2" id="divCbxStatus">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="cbxStatus">
							 	<option value="-1" selected="selected">Tất cả</option>
								<option value="1">Hoạt động</option>
						   		<option value="0">Tạm ngưng</option>						   		
							</select>
						</div>	
						<div class="Clear"></div>
					</div>
					<div class="SearchInSection SProduct1Form">
						<div class="Clear"></div>
						<div class="BtnCenterSection">
		                    <button class="BtnGeneralStyle" id="btnSearchEqpItem" onclick="return EquipItemConfig.search();">Tìm kiếm</button>
		                </div>
						<div class="Clear"></div>
						<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
						<p class="ErrorMsgStyle" id="errMsgSearch" style="display: none;"></p>
					</div>
				<!-- </div> -->
				<h2 class="Title2Style">Danh sách thiết lập
					<a onclick = "EquipItemConfig.exportBySearchEquipItemConfig();" style = "float: right;	padding: 0px 25px 0px 0px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_export.png" title="Xuất Excel"></a>
					<a onclick = "EquipItemConfig.openDialogImportEquipItemConfig();" style = "float: right;	padding-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_import.png" title="Nhập Excel"></a>
					<a onclick = "EquipItemConfig.downloadTemplateImportEquipItemConfig();" style = "float: right; padding-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_download_32.png" title="Tải mẫu Nhập Excel"></a>
				</h2>
				<div class="SearchInSection SProduct1Form" id ="idDivDg">
					<div class="GridSection" id="gridContainer">
						<table id="grid"></table>
					</div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="eqItem_import_easyuiPopupImportExcel" class="easyui-dialog" title="Nhập Excel Biên bản báo mất" data-options="closed:true,modal:true" style="width:465px;height:250px;">
		<div class="PopupContentMid">
			<div class="SearchInSection SProduct1Form" style="border: none;">
				<form action="/equip-item-config/importEquipItemConfig" name="importExcelFrm" id="importExcelFrm" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style" style="padding-right: 10px; text-align: right; width: 80px;">Tập tin Excel</label>
				    <div class="UploadFileSection Sprite1" style="width: 270px;">
						<input id="fakefileExcelpc" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" style="width: 270px;" size="1" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importExcelFrm','fakefileExcelpc', 'errImpExcelMsg');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a onclick="EquipItemConfig.downloadTemplateImportEquipItemConfig();" href="javascript:void(0)" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="EquipItemConfig.importEquipItemConfig();">Nhập Excel</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#eqItem_import_easyuiPopupImportExcel').dialog('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p id="successImpExcelMsg" class="SuccessMsgStyle" style="display:none;"></p>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errImpExcelMsg" />
	</div>
  	
</div>
<div class="ContentSection" style="visibility: hidden;" id="dialogEquipItemDiv">
	<div id="dialogEquipItemConfig" class="easyui-dialog" title="Thêm mới" data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">
				<label class="LabelStyle Label4Style"><span	style="margin-left: 30px;">Dung tích từ</span></label> 
				<input type="text" id="fromCapacity" class="InputTextStyle vinput-money" onkeypress="EquipItemConfig.onchangeCapacity(this);" maxlength="12" />
				<div class="Clear"></div>
				<label class="LabelStyle Label4Style"><span	style="margin-left: 30px;">Đến</span></label> 
				<input type="text" id="toCapacity" class="InputTextStyle vinput-money" onkeypress="EquipItemConfig.onchangeCapacity(this);" maxlength="12" />
<!-- 				<div class="Clear"></div> -->
<%-- 				<label class="LabelStyle Label4Style"><span	style="margin-left: 30px;">Trạng thái</span></label> --%>
<!-- 				<div class="BoxSelect BoxSelect2"> -->
<%-- 					<select id="cbxStatusDl" class="MySelectBoxClass"> --%>
<!-- 						<option value="1" selected="selected">Hoạt động</option> -->
<!-- 						<option value="0">Tạm ngưng</option> -->
<%-- 					</select> --%>
<!-- 				</div> -->
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSaveEquipItemConfig" class="BtnGeneralStyle">Cập nhật</button>
					<button class="BtnGeneralStyle"	onclick="$('#dialogEquipItemConfig').dialog('close');">Bỏ qua</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
			<p id="errorMsgDl" class="ErrorMsgStyle" style="display: none;"></p>
			<p id="successMsgDl" class="SuccessMsgStyle" style="display: none"></p>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {	
		$('#divCbxStatus, #txtFromCapacity, #txtToCapacity').bind('keyup', function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#btnSearchEqpItem').click(); 
			}
		}); 
		$('#panelEqipItemConfig input').bind('keyup', function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#btnSearchEqpItem').click(); 
			}
		}); 
		$('#dialogEquipItemConfig input').bind('keyup', function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#btnSaveEquipItemConfig').click(); 
			}
		}); 
		$("#txtFromCapacity, #txtToCapacity, #fromCapacity, #toCapacity").keydown(function (e) {
	       // Ensure that it is a number and stop the keypress
	       if (e.shiftKey) {
	           e.preventDefault();
	       }
	    });
		$('#panelEqipItemConfig').width($(window).width()- 18);
		$('#panelEqipItemConfig').height(175);
		
		var params = new Object();	
		params.fromCapacity = $('#txtFromCapacity').val().trim();
		params.toCapacity = $('#txtToCapacity').val().trim();
		params.status = $('#cbxStatus').val().trim();
		$('#grid').datagrid({
			url : "/equip-item-config/search",
			autoRowHeight : true,
			height: 378,
			rownumbers : true, 		
			singleSelect: true,
			pagination:true,
			rowNum : 20,
			fitColumns:true,
			pageList  : [20],
			scrollbarSize:0,
			width: $('#gridContainer').width(),
			autoWidth: true,
			queryParams:params,	
		    columns:[[	        
			    {field:'equipItemConfigCode', title: 'Mã', width: 100, sortable: false, resizable: true , align: 'left', formatter: function(value, row, index) {
			    	if (value != undefined && value != null) {
			    		return Utils.XSSEncode(value);
			    	}
			    	return "";
			    }},
			        
			    {field:'capacity',title: 'Dung tích(lít)', width: 50, align: 'left',sortable:false, resizable:true, formatter:function(value, row, index){
			    	if (value != undefined && value != null) {
			    		return Utils.XSSEncode(value);
			    	}
			    	return "";
			    }},
			    {field:'status', title: 'Trạng thái', width: 50, align: 'left', sortable:false, resizable:true, editor:{type:'text'}, formatter:function(value, row, index) {
			    	if (value != undefined && value != null) {
			    		return statusTypeText.parseValue(value);
			    	}
			    	return "";
			    }},
			    {field:'edit', title:'<a href="javascript:void(0)" class="cmsiscontrol" onclick="return EquipItemConfig.dialogAddOrUpdateEquipmentItem();" id="dg_parent_insert" title="Thêm mới"><img src="/resources/images/icon_add.png"/></a>', width: 30, align: 'center', sortable: false, resizable: true
				    , formatter: function(value,row,index){
				    	var divEdit = '';
				    	var divIdRemove = "dg_parent_remove_" + index;
					    var divIdEdit = "dg_parent_edit_" + index;
				    	if (!isNaN(row.status) && Number(row.status) == activeType.RUNNING) {
					    	divEdit = divEdit + '<a href="javascript:void(0)" class="cmsiscontrol" id="'+divIdEdit+'" onclick="return EquipItemConfig.dialogAddOrUpdateEquipmentItem('+row.id+','+index+');"><img width="15" height="15" src="/resources/images/icon-edit.png" title="Chỉnh sửa"></a> ';
					    	divEdit = divEdit + '<a href="javascript:void(0)" class="cmsiscontrol" id="'+divIdEdit+'" onclick="return EquipItemConfig.stopEquipmentItemConfig('+row.id+',\''+row.equipItemConfigCode+'\');"><img width="15" height="15" src="/resources/images/icon-stop.png" title="Tạm ngưng"></a> ';
				    	} else {
				    		divEdit = divEdit + '<a href="javascript:void(0)" class="cmsiscontrol" id="dg_parent_edit_" onclick="return false;"><img width="15" height="15" src="/resources/images/icon-edit_disable.png" title="Chỉnh sửa"></a> ';
					    	divEdit = divEdit + '<a href="javascript:void(0)" class="cmsiscontrol" id="dg_parent_remove_" onclick="return false;"><img width="15" height="15" src="/resources/images/icon-stop_disable.png" title="Tạm ngưng"></a> ';
				    	}
				    	divEdit = divEdit + '<a href="javascript:void(0)" class="cmsiscontrol" id="'+divIdRemove+'" onclick="return EquipItemConfig.removedEquipmentItemConfig('+row.id+',\''+row.equipItemConfigCode+'\')"><img width="15" height="15" src="/resources/images/icon_delete.png" title="Xóa"></a>';
				    	return divEdit;
					}
			    },
			    {field: 'id', index: 'id', hidden: true},
		    ]],
		    view:detailview,
			detailFormatter:function(index, row){
				return '<div style="padding:2px"><table class="ddv"></table></div>';			
			},
			onExpandRow:function(indexRows, row){		
				$('#divOverlay').show();
				var ddv = $(this).datagrid('getRowDetail', indexRows).find('table.ddv');
				var idEditAll = "dg_chidrent_edit_all_" + row.id;
				var idSaveAll = "dg_chidrent_save_all_" + row.id;
				var idBackAll = "dg_chidrent_back_all_" + row.id;
				var editAll = '<a href="javascript:void(0)" class="child_edit_all" id="'+idEditAll+'" onclick="return EquipItemConfig.editAllEquipItemConfigDtl('+indexRows+')"><img width="15" height="15" src="/resources/images/icon-edit.png" title="Chỉnh sửa"></a>';
				var saveAll = ' <a href="javascript:void(0)" class="child_save_all" id="'+idSaveAll+'" onclick="return EquipItemConfig.saveAllEquipItemConfigDtl('+row.id+','+indexRows+')"><img width="15" height="15" src="/resources/images/icon-save.png" title="Cập nhật"></a>';
				var backAll = ' <a href="javascript:void(0)" class="child_back_all" id="'+idBackAll+'" onclick="return EquipItemConfig.backAllEquipItemConfigDtl('+indexRows+')"><img width="15" height="15" src="/resources/images/icon_esc.png" title="Bỏ qua"></a>';
				params = new Object();
				params.id = row.id;
				ddv.datagrid({
					url:'/equip-item-config/get-list-item-config-detail',
					rownumbers:true,
					fitColumns:true,
					singleSelect:true,
					height:'auto',
					width:$('#gridContainer').width() - 70,
					queryParams:params,	
					loadMsg:'',
					columns:[[
					          {field:'equipItemCode', title:'Mã hạng mục', width:50, align:'left', sortable:false, resizable:false, formatter:function(val,r,idx){
					        	  return Utils.XSSEncode(val);
					          }},
					          {field:'equipItemName', title:'Tên hạng mục', width:100, align:'left', sortable:false, resizable:false, formatter:function(val, r, idx){
					        	  return Utils.XSSEncode(val);				        	 	 
					          }},
							  {field:'fromMaterialPrice', title:'Đơn giá vật tư từ', width:60, align:'right', sortable:false, resizable:false, editor:{type:'text'},formatter:function(val, r, idx){
								  if (val != undefined && val != null) {
					        	  	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(val.toString().trim().replace(/,/g,'')));
					        	  }
					        	  return '';
					          }},
					          {field:'toMaterialPrice', title:'Đơn giá vật tư đến', width:60, align:'right', sortable:false, resizable:false, editor:{type:'text'},formatter:function(val, r, idx){
					        	  if (val != undefined && val != null) {
					        	  	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(val.toString().trim().replace(/,/g,'')));
					        	  }
					        	  return '';
					          }},
					          {field:'fromWorkerPrice', title:'Đơn giá nhân công từ', width:60, align:'right', sortable:false, resizable:false, editor:{type:'text'},formatter:function(val, r, idx){
					        	  if (val != undefined && val != null) {
					        	  	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(val.toString().trim().replace(/,/g,'')));
					        	  }
					        	  return '';
					          }},
					          {field:'toWorkerPrice', title:'Đơn giá nhân công đến', width:60, align:'right', sortable:false, resizable:false, editor:{type:'text'},formatter:function(val, r, idx){
					        	  if (val != undefined && val != null) {
					        	  	return VTUtilJS.formatCurrency(Utils.formatDoubleValue(val.toString().trim().replace(/,/g,'')));
					        	  }
					        	  return '';
					          }},
					          {field:'editDetail', title:editAll+saveAll+backAll, width:30, align:'center', sortable:false, resizable:false
					        	  ,formatter:function(val, r, idx){
					        		var equipItemConfigDtId = row.id;
					        		if (isNaN(equipItemConfigDtId) || Number(equipItemConfigDtId) <= 0) {
					        			equipItemConfigDtId = -1;
					        		}
					        		var key = idx + '_' + indexRows;
					        	  	var edit = '<a href="javascript:void(0)" class="cmsiscontrol child_edit" id="dg_chidrent_edit_'+key+'" onclick="return EquipItemConfig.editEquipItemConfigDtl('+ equipItemConfigDtId +','+idx+','+ indexRows+')"><img width="15" height="15" src="/resources/images/icon-edit.png" title="Chỉnh sửa"></a> ';
							    	var save = ' <a href="javascript:void(0)" class="cmsiscontrol child_save" id="dg_chidrent_save_'+key+'" onclick="return EquipItemConfig.saveEquipItemConfigDtl('+ equipItemConfigDtId +','+ idx+','+ indexRows+')"><img width="15" height="15" src="/resources/images/icon-save.png" title="Cập nhật"></a>';
									var back = ' <a href="javascript:void(0)" class="cmsiscontrol child_back" id="dg_chidrent_back_'+key+'" onclick="return EquipItemConfig.backEquipItemConfigDtl('+ equipItemConfigDtId +','+ idx+','+ indexRows+')"><img width="15" height="15" src="/resources/images/icon_esc.png" title="Bỏ qua"></a>';
							    	return edit + save + back; 
					         	 }
					          }
					]],
					
					onResize:function(){
						$('#grid').datagrid('fixDetailRowHeight',indexRows);
					},
					onLoadSuccess:function(){
						$('#divOverlay').hide();
						$('.child_edit_all').show();
						$('.child_save_all').hide();
						$('.child_back_all').hide();
						$('.child_edit').each(function(i,e){
							$(this).show();
						});
						
						var tm1 = setTimeout(function(){
							$('.child_save').each(function(i,e){
								$(this).hide();
							});
							$('.child_back').each(function(i,e){
								$(this).hide();
							});
							clearTimeout(tm1);
						}, 50);
						var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
						if (row == undefined || row == null || isNaN(row.status) || Number(row.status) != activeType.RUNNING) {
							ddv.datagrid('hideColumn', 'editDetail');
						} else {
							ddv.datagrid('showColumn', 'editDetail');
						}
						Utils.functionAccessFillControl('gridContainer');
						var tm2 = setTimeout(function(){
							$('#grid').datagrid('fixDetailRowHeight', indexRows);
							clearTimeout(tm2);
					 	}, 2);
						var rowsDt = $(ddv).datagrid('getRows');
						if (rowsDt != null && rowsDt != undefined) {
							for (var j = 0, size = rowsDt.length; j < size; j++) {
								var mapKeyId = indexRows.toString() + '_' + j.toString();
								EquipItemConfig._mapRowDetailChange.remove(mapKeyId);
							}
						}
					},
					onBeforeEdit:function(rowIndex, rowData){
						var ddv = $('#grid').datagrid('getRowDetail', indexRows).find('table.ddv');
						var rowsDtl = $(ddv).datagrid('getRows');
					    for(var i=0;i<rowsDtl.length;++i){
					    	$(grid).datagrid('endEdit', i);	
					    }
					}		
				});
			},
		    onLoadSuccess :function(data){	    	
				$(window).resize();
				Utils.functionAccessFillControl('gridContainer');
 				if($('#gridContainer .cmsiscontrol[id^="dg_parent_"]').length == 0){
 					$('#grid').datagrid("hideColumn", "edit");
				} else {
					$('#grid').datagrid("showColumn", "edit");
				}
 				EquipItemConfig._mapRowDetailChange = new Map();
			}
		});
	});
</script>
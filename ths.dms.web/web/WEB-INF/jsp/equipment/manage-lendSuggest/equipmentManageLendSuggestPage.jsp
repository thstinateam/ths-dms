<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Thiết bị</a>
		</li>
		<li><span id="titleHeader"></span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm
					<span style="float:right;margin-right:15px;">
              			<a href="javascript:void(0);" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
       				</span>
				</h2>
				<div class="SearchInSection SProduct1Form" style="padding-left: 0px;">				
					<!-- <label class="LabelLeftStyleM2 LabelLeft5Style">Đơn vị</label> -->
					<label class="LabelStyle Label1Style">Đơn vị</label>
					<div class="BoxSelect BoxSelect2">				
						<div class="Field2">
							<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:200px;height: auto;" class="InputTextStyle InputText1Style" />
						</div>
					</div>			
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Mã biên bản</label>
					<input id="code" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					<label class="LabelStyle Label1Style">Mã giám sát</label>
					<input id="superviseCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					<label class="LabelStyle Label19Style">Trạng thái biên bản</label>
					<div class="BoxSelect BoxSelect2 cmsiscontrol" id="statusCreate">
						<select class="MySelectBoxClass" id="status">
							<option value="" >Tất cả</option>
							<option value="0" selected="selected">Dự thảo</option>
							<option value="1" >Chờ duyệt</option>
							<option value="2" >Duyệt</option>
							<option value="3" >Không duyệt</option>
							<option value="6" >Hoàn tất</option>
							<option value="4" >Hủy</option>
						</select>
					</div>
					<div class="BoxSelect BoxSelect2 cmsiscontrol" id="statusApproved">
						<select class="MySelectBoxClass" id="status">
							<option value="" >Tất cả</option>
							<option value="1" selected="selected">Chờ duyệt</option>
							<option value="2" >Duyệt</option>
							<option value="6" >Hoàn tất</option>
						</select>
					</div>
					<input type="hidden" id="statusApproved_flag" class="cmsiscontrol" value="1" />													
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Ngày biên bản từ</label>
					<div id="fromDateDiv">
						<input type="text" id="fDate" class="InputTextStyle InputText6Style"/>
					</div>
					<label class="LabelStyle Label1Style">Đến </label>
					<div id="toDateDiv">
						<input type="text" id="tDate" class="InputTextStyle InputText6Style"/>
					</div>
					<label class="LabelStyle Label19Style">Xuất hợp đồng/Giao nhận</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="statusDelivery">
							<option value="" selected="selected">Tất cả</option>
							<option value="0" >Chưa xuất</option>
							<option value="1" >Đã xuất</option>
						</select>
					</div>
					<div class="Clear"></div>
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnSearch" class="BtnGeneralStyle" onclick="EquipmentProposalBorrow.searchProposalBorrow();" >Tìm kiếm</button>
						</div>
						<div class="Clear"></div>
					</div>	
					<p id="errLendMsg" class="ErrorMsgStyle" style="display: none;"></p>	
					<p id="successMsgLend" class="SuccessMsgStyle" style="display: none;margin-top:10px;margin-left: 10px"></p>
				</div>	
				<!-- danh sach bien ban de nghi muon thiet bi -->
				<div class="Title2Style" style="height:15px;font-weight: bold;">
					<span style="float:left;">Danh sách đề nghị mượn thiết bị</span>
					<div style="float:right; margin-top: -5px;margin-right: 27px;">
						<!-- <a href="javascript:void(0);" id="btnPrint" onclick="EquipmentProposalBorrow.printRecodeProposalBorrow();" ><img src="/resources/images/icon-printer.png" height="20" width="20" title="In"></a> -->
						<a href="javascript:void(0);" id="exportDelivery_btnExportDelivery" class="cmsiscontrol" style="display:none" onclick="EquipmentProposalBorrow.exportDeliveryRecord();" ><img src="/resources/images/icon-export-delivery.png" height="20" width="20" title="Xuất hợp đồng\Giao nhận"></a>
						<a href="javascript:void(0);" id="importRecord_btnTemplate" class="cmsiscontrol" style="display:none" onclick="EquipmentProposalBorrow.downloadTemplate();" ><img src="/resources/images/icon_download_32.png" height="20" width="20" title="Tải tập tin mẫu"></a>
						<a href="javascript:void(0);" id="importRecord_btnImport" class="cmsiscontrol" style="display:none" onclick="EquipmentProposalBorrow.showDlgImportExcel();"><img src="/resources/images/icon_table_import.png" height="20" width="20" title="Nhập excel"></a>
						<a href="javascript:void(0);" id="btnExport" onclick="EquipmentProposalBorrow.exportEquipLend();" style="margin-right: 5px;"><img src="/resources/images/icon_table_export.png" height="20" width="20" title="Xuất excel"></a>					
						<div style="float:right;">
							<div style="margin-right: 5px;" class="BoxSelect BoxSelect11 cmsiscontrol" id = "statusLabelsCreate">
								<select class="MySelectBoxClass" id="statusRecordLabel" style="width:160px;">
									<option value="" selected="selected">Chọn</option>
									<option value="1" >Chờ Duyệt</option>
									<option value="4" >Hủy</option>
								</select>
							</div>
							<div style="margin-right: 5px;" class="BoxSelect BoxSelect11 cmsiscontrol" id = "statusLabelsApproved">
								<select class="MySelectBoxClass" id="statusRecordLabel" style="width:160px;">
									<option value="" selected="selected">Chọn</option>
									<option value="2" >Duyệt</option>
									<option value="3" >Không Duyệt</option>
								</select>
							</div>
							<button id="btnSaveLst" class="BtnGeneralStyle"  onclick="EquipmentProposalBorrow.saveRecordChange();" >Lưu</button>
						</div>
					</div>
				</div>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridLendEquipContainer">
						<table id="gridLendEquip"></table>
					</div>
				</div>							
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup import -->
<div id="importRecord" class="cmsiscontrol">
  	<div style="display: none" id="easyuiPopupImportExcel" title="Import - Biên bản đề nghị mượn thiết bị" data-options="closed:true,modal:true" style="width:475px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-management-proposal-borrow/import-excel-record" name="importFrmBBDNM" id="importFrmBBDNM" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">
						<input id="fakefilepcBBDNM" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileBBDNM" onchange="previewImportExcelFile(this,'importFrmBBDNM','fakefilepcBBDNM', 'errExcelMsgBBDNM');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a onclick="EquipmentProposalBorrow.downloadTemplate();" class="Sprite1" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="return EquipmentProposalBorrow.importExcelBBDNM();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgBBDNM"/>
	</div>	
</div>
<!-- popup bien ban giao nhan -->
<div style="display: none">
	<div id="easyuiPopupLstDelivery" class="easyui-dialog" title="Danh sách biên bản hợp đồng/giao nhận" data-options="closed:true,modal:true" >
		<div class="PopupContentMid">			
	   		<div class="GridSection" id="gridDeliveryDiv">
				<table id="gridDelivery"></table>
			</div>
			<div class="GeneralForm ImportExcel1Form">												
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadExcelFile" onclick="return EquipmentProposalBorrow.exportDeliveryRecordAgain();">Xuất</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupLstDelivery').window('close');">Bỏ qua</button>
				</div>
			</div>
			<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errMsgPopupLstDelivery"/>
			<p id="successMsgPopupLstDelivery" class="SuccessMsgStyle" style="display: none;margin-top:10px;margin-left: 10px"></p>
		</div>
		<input type="hidden" id="idRecordPopup"/>
	</div>
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="curShopCode" name="curShopCode"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	if(window.location.pathname == "/equipment-management-proposal-borrow/info"){
		$("#titleHeader").html("Quản lý đề nghị mượn thiết bị");		
	} else if(window.location.pathname == "/equipment-management-proposal-borrow/approve"){
		$("#titleHeader").html("Phê duyệt đề nghị mượn thiết bị");	
	}

	$("#statusLabelsApproved .CustomStyleSelectBox .CustomStyleSelectBoxInner").width(125);
	$("#statusLabelsCreate .CustomStyleSelectBox .CustomStyleSelectBoxInner").width(125);
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar("fDate");
	ReportUtils.setCurrentDateForCalendar("tDate");	
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });
	Utils._functionCallback = function() {
		var params=new Object(); 
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if(dataShop!=null && dataShop.length>0) {
			var lstShop = new Array();
			var curShopCode = $('#curShopCode').val();
			for(var i=0;i<dataShop.length;i++){
				if(dataShop[i].shopCode != curShopCode) {
					lstShop.push(dataShop[i].shopCode);
				}
			}
			params.lstShop = lstShop.join(",");
		}
		params.code = $('#code').val().trim();
		params.superviseCode = $('#superviseCode').val().trim();
		params.statusRecord = $('#status').val().trim();
		params.flagRecordStatus = $('#statusApproved_flag').val();
		params.fromDate = $('#fDate').val().trim();
		params.toDate = $('#tDate').val().trim();	
		params.statusDelivery = $('#statusDelivery').val().trim();	
		EquipmentProposalBorrow._mapRecored = new Map();
		
		$('#gridLendEquip').datagrid({
			url:'/equipment-management-proposal-borrow/search',
			method : 'GET',
			height: 'auto',
			width : $('#gridLendEquipContainer').width(),
			scrollbarSize:0,
			rownumbers:true,
			fitColumns:true,
			autoRowHeight:true, 
			pagination:true, 
			pageSize:10,
			queryParams:params,
			pageList  : [50],
			columns:[[		  
				{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'center'},
				{field: 'shopCode',title:'Đơn vị', width: 100, sortable:false,resizable:false, align: 'left', formatter: function(value,row,index){
					return Utils.XSSEncode(row.shopCode + ' - ' + row.shopName);
				}},
				{field: 'code',title:'Mã biên bản', width: 50, sortable:false,resizable:false, align: 'center', formatter: function(value,row,index){
					return Utils.XSSEncode(value);
				}},
				{field: 'createFormDate',title:'Ngày biên bản',width: 60,sortable:false,resizable:false, align: 'center', formatter: function(value,row,index){
					return Utils.XSSEncode(value);
				}},
				{field: 'staffCode',title:'Giám sát',width: 100,sortable:false,resizable:false, align: 'left' , formatter: function(value,row,index){
					return Utils.XSSEncode(row.staffCode + ' - ' + row.staffName);
				}},
				{field: 'status', title: 'Trạng thái biên bản', width: 80,resizable:false,align: 'left', formatter: function(value,row,index){
					return StatusRecordsEquip.parseValue(Utils.XSSEncode(value));
				}},
			    {field: 'statusDelivery',title:'Hợp đồng/Giao nhận', width:100, resizable:false, align:'left', formatter: function(value, row, index){
			    	return StatusDeliveryEquip.parseValue(Utils.XSSEncode(value));
			    }},
			    {field: 'note', title: 'Ghi chú', width: 100, align: 'left', sortable: false, resizable: true, formatter: function(value, row, index) {
					return Utils.XSSEncode(value);
				}},
				{field:'edit', title:'<a id="createRecord" class="cmsiscontrol" href="/equipment-management-proposal-borrow/edit"><span style="cursor:pointer"><img title="Thêm mới" src="/resources/images/icon_add.png"/></span></a>', width: 50, align: 'center', formatter: function (value, row, index) {
					var html = '<a onclick="EquipmentProposalBorrow.viewPrintEquipLend(' + row.id + ')"><span style="cursor:pointer;margin-right: 3px;margin-left: 3px;"><img width="20" height="20" title="Xem biên bản" src="/resources/images/icon_print_view_34.png"/></span></a>';
					if (row.status == StatusRecordsEquip.DRAFT || row.status == StatusRecordsEquip.NO_APPROVAL) {
						html += '<a id="editRecordCreate" class="cmsiscontrol" href="/equipment-management-proposal-borrow/edit?id=' + row.id +'"><span style="cursor:pointer;margin-right: 3px;margin-left: 3px;"><img title="Chỉnh sửa" src="/resources/images/icon_edit.png"/></span></a>';
					} else {
						html += '<a id="editRecordCreate" class="cmsiscontrol" href="/equipment-management-proposal-borrow/edit?id=' + row.id +'"><span style="cursor:pointer;margin-right: 3px;margin-left: 3px;"><img title="Xem chi tiết" src="/resources/images/icon-view.png"/></span></a>';
					}	
					if (row.status == StatusRecordsEquip.WAITING_APPROVAL) {
						html += '<a id="editRecordApproved" class="cmsiscontrol" href="/equipment-management-proposal-borrow/change?id=' + row.id +'"><span style="cursor:pointer;margin-right: 3px;margin-left: 3px;"><img title="Chỉnh sửa" src="/resources/images/icon_edit.png"/></span></a>';
					} else {
						html += '<a id="editRecordApproved" class="cmsiscontrol" href="/equipment-management-proposal-borrow/change?id=' + row.id +'"><span style="cursor:pointer;margin-right: 3px;margin-left: 3px;"><img title="Xem chi tiết" src="/resources/images/icon-view.png"/></span></a>';
					}
					if (row.status == StatusRecordsEquip.APPROVED && row.statusDelivery == StatusDeliveryEquip.NOT_EXPORT) {
						html += '<a id="exportDelivery" class="cmsiscontrol" onclick="EquipmentProposalBorrow.exportDeliveryRecord('+row.id+')"><span style="cursor:pointer;margin-right: 3px;margin-left: 3px;"><img width="20" height="20" title="Xuất hợp đồng giao nhận" src="/resources/images/icon-export-delivery.png"></span></a>'; 
					} else if (row.status == StatusRecordsEquip.APPROVED && row.statusDelivery == StatusDeliveryEquip.EXPORTED) {
						html+= '<a id="exportDelivery" class="cmsiscontrol" onclick="EquipmentProposalBorrow.getDeliveryRecordAgain('+row.id+')"><span style="cursor:pointer;margin-right: 3px;margin-left: 3px;"><img width="20" height="20" title="Xuất lại hợp đồng/Giao nhận" src="/resources/images/icon-export-again.png"/></span></a>'; 
					} else {
						html+= '<a id="exportDelivery" class="cmsiscontrol"><span style="opacity: 0;cursor:pointer;margin-right: 3px;margin-left: 3px;"><img width="20" height="20" src="/resources/images/icon-view.png"/></span></a>'; 
					}
					return html;
			    }}
			]],
			onBeforeLoad:function(param){	
				$(".datagrid-header-check input[type='checkbox']").removeAttr("checked");										 
			},
			onLoadSuccess :function(data){  	 
				if(data.error){
					$("#errLendMsg").html(data.errMsg).show();
				} else {
					Utils.functionAccessFillControl();
					$('.datagrid-header-rownumber').html('STT');				

					var rows = $('#gridLendEquip').datagrid('getRows');
			    	for(var i = 0; i < rows.length; i++) {
			    		if(EquipmentProposalBorrow._mapRecored.get(rows[i].id)!=null) {
			    			$('#gridLendEquip').datagrid('checkRow', i);
			    		}
			    	}	
				}					
			},
			onCheck:function(index,row){
				EquipmentProposalBorrow._mapRecored.put(row.id,row);   
		    },
		    onUncheck:function(index,row){
		    	EquipmentProposalBorrow._mapRecored.remove(row.id);
		    },
		    onCheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		EquipmentProposalBorrow._mapRecored.put(row.id,row);
		    	}
		    },
		    onUncheckAll:function(rows){
		    	for(var i = 0;i<rows.length;i++){
		    		var row = rows[i];
		    		EquipmentProposalBorrow._mapRecored.remove(row.id);	
		    	}
		    }
		});
	};
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a id="titleUrl" href="/equipment-management-proposal-borrow/info"></a>
		</li>
		<li>
			<s:if test="id == null">
				<span>Lập đề nghị mượn thiết bị</span>
			</s:if>
			<s:else>
				<span id="titleEditPage"></span>
			</s:else>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung
					<span style="float:right;margin-right:15px;">
              			<a href="javascript:void(0);" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
       				</span>
				</h2>
				<div class="SearchInSection SProduct1Form" style="padding-left: 0px;">	
					<label class="LabelLeftStyleM5 LabelLeft4Style">Mã biên bản</label>
					<input id="code" type="text" class="InputTextStyle InputText1Style" value="<s:property value="code"/>" maxlength="50" disabled="disabled"/>
					<label class="LabelStyle Label1Style">Mã giám sát</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="staffCode">
							<option value="0"> --- Chọn giám sát ---</option> <!-- xu ly XSS option dau tien -->
							<s:iterator value="lstStaffVOs" var="obj">
				    			<option value="<s:property value="#obj.staffCode" />">
				    			<s:if test="staffCode == ''">
				    				<s:property value="#obj.staffCode" /><s:property value="#obj.staffName"/>
				    			</s:if>
				    			<s:else>
				    				<s:property value="#obj.staffCode" /> - <s:property value="#obj.staffName"/>
				    			</s:else>
				    			</option>
				    		</s:iterator>
						</select>
					</div>
					<label class="LabelStyle Label1Style">Ngày biên bản</label>
					<s:if test="statusRecord == 0">
						<div id="createFormDateDiv">
							<input type="text" id="createFormDate" class="InputTextStyle InputText6Style" value="<s:property value="createFormDate"/>" />
						</div>
					</s:if>
					<s:else>
						<div id="createFormDateDiv">
							<input type="text" id="createFormDate" class="InputTextStyle InputText6Style" value="<s:property value="createFormDate"/>" disabled="disabled"/>
						</div>
					</s:else>
					<div class="Clear"></div>
					<label class="LabelLeftStyleM5 LabelLeft4Style">Trạng thái biên bản</label>
					<s:if test="id == null">
						<div class="BoxSelect BoxSelect2 BoxDisSelect">
							<select class="MySelectBoxClass" id="status" disabled="disabled">
								<option value="0" selected="selected">Dự thảo</option>
							</select>
						</div>		
					</s:if>
					<s:else>
						<s:if test="statusRecord == 0">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="status">
									<option value="0" selected="selected">Dự thảo</option>
									<option value="1" >Chờ duyệt</option>
									<option value="4" >Hủy</option>
								</select>
							</div>		
						</s:if>
						<s:elseif test="statusRecord == 3">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="status">
									<option value="3" selected="selected">Không duyệt</option>
									<option value="1" >Chờ duyệt</option>
									<option value="4" >Hủy</option>
								</select>
							</div>		
						</s:elseif>
						<s:elseif test="statusRecord == 1">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="status">
									<option value="1" selected="selected">Chờ duyệt</option>
									<option value="2" >Duyệt</option>
									<option value="3" >Không duyệt</option>
								</select>
							</div>		
						</s:elseif>
						<s:elseif test="statusRecord == 2">
							<div class="BoxSelect BoxSelect2 BoxDisSelect">
								<select class="MySelectBoxClass" id="status" disabled="disabled">
									<option value="2" selected="selected">Duyệt</option>
								</select>
							</div>
						</s:elseif>
						<s:elseif test="statusRecord == 4">
							<div class="BoxSelect BoxSelect2 BoxDisSelect">
								<select class="MySelectBoxClass" id="status" disabled="disabled">
									<option value="4" selected="selected">Hủy</option>
								</select>
							</div>
						</s:elseif>
						<s:elseif test="statusRecord == 6">
							<div class="BoxSelect BoxSelect2 BoxDisSelect">
								<select class="MySelectBoxClass" id="status" disabled="disabled">
									<option value="6" selected="selected">Hoàn tất</option>
								</select>
							</div>
						</s:elseif>
					</s:else>
					<label class="LabelStyle Label1Style">Ghi chú</label>
					<s:if test="statusRecord == 0">
						<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 41%; height: 45px; font-size: 13px;" maxlength="500"><s:property value="note"/></textarea>
					</s:if>
					<s:else>
						<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 41%; height: 45px; font-size: 13px;" maxlength="500" disabled="disabled"><s:property value="note"/></textarea>
					</s:else>
					<div class="Clear"></div>								
				</div>		
				<!-- danh sach bien ban -->	
				<h2 class="Title2Style" style="height:15px;">
					<span style="float:left;">Danh sách đề nghị</span>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridEquipLendChangeDiv">
						<table class="easyui-datagrid" id="gridEquipLendChange" >
							<thead frozen="true">
								<tr>
									<s:if test="id == null || statusRecord == 0 || statusRecord == 3">
										<th data-options="field:'delete',width:35,
											formatter: equipCreateLendSuggest.datagridOption.column0.formatter"rowspan="2">											
											<a href="javascript:void(0);" onclick="EquipmentProposalBorrow.insertEquipmentInGrid();"><img src="/resources/images/icon_add.png" title="Thêm"/></a>											
										</th>
									</s:if>
									<th data-options="field:'equipCategory',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column1.formatter,
										editor: equipCreateLendSuggest.datagridOption.column1.editor" rowspan="2">Loại thiết bị</th>
									<th data-options="field:'capacity',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column2.formatter,
										editor: equipCreateLendSuggest.datagridOption.column2.editor" rowspan="2">Dung tích (lít)</th>
									<th data-options="field:'quantity',width:100, align:'right', 
										formatter: equipCreateLendSuggest.datagridOption.column3.formatter,
										editor: equipCreateLendSuggest.datagridOption.column3.editor" rowspan="2">Số lượng</th>
									<th data-options="field:'shopCode',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column4.formatter,
										editor: equipCreateLendSuggest.datagridOption.column4.editor" rowspan="2">Mã đơn vị</th>
									<th data-options="field:'customerCode',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column5.formatter,
										editor: equipCreateLendSuggest.datagridOption.column5.editor" rowspan="2">Khách hàng (F9)</th>
									<th data-options="field:'customerName',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column6.formatter,
										editor: equipCreateLendSuggest.datagridOption.column6.editor" rowspan="2">Tên khách hàng</th>
								</tr>
							</thead>
							<thead>
								<tr>
									<th data-options="field:'representative',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column7.formatter,
										editor: equipCreateLendSuggest.datagridOption.column7.editor" rowspan="2">Người đứng tên mượn thiết bị</th>
									<th data-options="field:'relation',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column8.formatter,
										editor: equipCreateLendSuggest.datagridOption.column8.editor" rowspan="2">Quan hệ người đứng tên mượn thiết bị với chủ cửa hàng/Doanh nghiệp</th>
									<th data-options="field:null,width:100," colspan="6"><span style="font-size: 12px; font-weight: bold"> Địa chỉ đặt thiết bị </span></th>
									<th data-options="field:'idNo',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column15.formatter,
										editor: equipCreateLendSuggest.datagridOption.column15.editor" rowspan="2">Số CMND</th>
									<th data-options="field:'idNoPlace',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column16.formatter,
										editor: equipCreateLendSuggest.datagridOption.column16.editor" rowspan="2">Nơi cấp CMND</th>
									<th data-options="field:'idNoDate',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column17.formatter,
										editor: equipCreateLendSuggest.datagridOption.column17.editor" rowspan="2">Ngày cấp CMND</th>
									<th data-options="field:'businessNo',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column18.formatter,
										editor: equipCreateLendSuggest.datagridOption.column18.editor" rowspan="2">Số giấy phép ĐKKD</th>
									<th data-options="field:'businessNoPlace',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column19.formatter,
										editor: equipCreateLendSuggest.datagridOption.column19.editor" rowspan="2">Nơi cấp ĐKKD</th>
									<th data-options="field:'businessNoDate',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column20.formatter,
										editor: equipCreateLendSuggest.datagridOption.column20.editor" rowspan="2">Ngày cấp ĐKKD</th>
									<th data-options="field:'timeLend',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column21.formatter,
										editor: equipCreateLendSuggest.datagridOption.column21.editor" rowspan="2">Thời gian mong muốn nhận thiết bị</th>
									<th data-options="field:'healthStatus',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column22.formatter,
										editor: equipCreateLendSuggest.datagridOption.column22.editor" rowspan="2">Tình trạng thiết bị</th>
									<th data-options="field:'stockType',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column23.formatter,
										editor: equipCreateLendSuggest.datagridOption.column23.editor" rowspan="2">Kho xuất</th>
									<%-- <th data-options="field:'amount',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column24.formatter,
										editor: equipCreateLendSuggest.datagridOption.column24.editor" rowspan="2">Doanh số trung bình 3 tháng gần nhất <s:property value = "nganhHangStr"/></th> --%>
								</tr>
								<tr>
									<th data-options="field:'phone',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column9.formatter,
										editor: equipCreateLendSuggest.datagridOption.column9.editor">Điện thoại</th>
									<th data-options="field:'address',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column10.formatter,
										editor: equipCreateLendSuggest.datagridOption.column10.editor">Số nhà</th>
									<th data-options="field:'province',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column11.formatter,
										editor: equipCreateLendSuggest.datagridOption.column11.editor">Tỉnh/TP</th>
									<th data-options="field:'district',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column12.formatter,
										editor: equipCreateLendSuggest.datagridOption.column12.editor">Quận/Huyện</th>
									<th data-options="field:'ward',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column13.formatter,
										editor: equipCreateLendSuggest.datagridOption.column13.editor">Phường/Xã</th>
									<th data-options="field:'street',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column14.formatter,
										editor: equipCreateLendSuggest.datagridOption.column14.editor">Tên đường/Phố/Thôn/Ấp</th>								
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="Clear"></div>     
				<div class="SearchInSection SProduct1Form">                       
	               	<div class="BtnCenterSection" style="padding-bottom: 10px;" id="divBtnEdit">
	               		<s:if test="statusRecord == 0 || statusRecord == 3">
				       		<button id="editRecord_btnCapNhat" class="BtnGeneralStyle cmsiscontrol" onclick="return EquipmentProposalBorrow.updateRecord();">Cập nhật</button> &nbsp;&nbsp;				       		
				       	</s:if>
				       	<s:elseif test="statusRecord == 1">
				       		<button id="editApproved_btnCapNhat" class="BtnGeneralStyle cmsiscontrol" onclick="return EquipmentProposalBorrow.updateRecord();">Cập nhật</button> &nbsp;&nbsp;
				       	</s:elseif>
				       	<button class="BtnGeneralStyle" onclick="EquipmentProposalBorrow.returnSearchPage();">Bỏ qua</button>
		           	</div>
		           	<div class="Clear"></div> 
		           	<p id="errMsgChangeLend" class="ErrorMsgStyle" style="display: none;"></p>
		           	<p id="successMsgChangeLend" class="SuccessMsgStyle" style="display: none"></p>
	           	</div>
		    </div>	
		</div>
	</div>
</div>
<s:hidden id="curShopCode" name="shopCode"></s:hidden>
<s:hidden id="idEquipLend" name="id"></s:hidden>
<s:hidden id="isLevelShop" name="isLevelShop"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {	
	setDateTimePicker('createFormDate');
	var statusRecordEquipLend = '<s:property value="statusRecord"/>';
	if(statusRecordEquipLend != StatusRecordsEquip.DRAFT){
		$('#createFormDate').next().unbind('click');
		$("#staffCode").attr("disabled","disabled")
		$($("#staffCode").parent()).addClass("BoxDisSelect");
	}

	var superviseCode = '<s:property value="superviseCode"/>';
	if (superviseCode != null && superviseCode != "") {
		$("#staffCode").val(superviseCode).change();
	}

	if(window.location.pathname == "/equipment-management-proposal-borrow/edit"){
		$("#titleUrl").attr("href",'/equipment-management-proposal-borrow/info');
		$("#titleUrl").html("Quản lý đề nghị mượn thiết bị");	
		if(statusRecordEquipLend == StatusRecordsEquip.WAITING_APPROVAL){
			$("#status").attr("disabled","disabled")
			$($("#status").parent()).addClass("BoxDisSelect");
		}	
	} else if(window.location.pathname == "/equipment-management-proposal-borrow/change"){
		$("#titleUrl").attr("href",'/equipment-management-proposal-borrow/approve');	
		$("#titleUrl").html("Phê duyệt đề nghị mượn thiết bị");	
	}

	Utils._functionCallback = function() {
		if ($("#divBtnEdit button").length < 2) {
			$("#titleEditPage").html("Xem chi tiết biên bản đề nghị mượn thiết bị");
		} else if ($("#titleEditPage").html() == "") {
			$("#titleEditPage").html("Chỉnh sửa đề nghị mượn thiết bị");
		}
	};

	$('#gridEquipLendChange').datagrid({
		url: '/equipment-management-proposal-borrow/get-list-detail-in-record',
		queryParams: {
			id: $("#idEquipLend").val() > 0 ? $("#idEquipLend").val() : ""
		},
		width: $('#gridEquipLendChangeDiv').width(),		
		height: 'auto',
		scrollbarSize:0,
		rownumbers:true,
		autoRowHeight:true, 
		singleSelect: true,
		// pagination:true, 
		// pageSize:20,		
		onDblClickRow: function(index, row) {
			if (statusRecordEquipLend == StatusRecordsEquip.DRAFT || statusRecordEquipLend == StatusRecordsEquip.NO_APPROVAL) {
				$('.ErrorMsgStyle').html('').hide();
				if (EquipmentProposalBorrow._editIndex != null) {
					var msg = EquipmentProposalBorrow.validateEquipmentInGrid();
					if (msg.length > 0) {
						$('#errMsgChangeLend').html(msg).show();
						return false;
					}
					// them dong moi
					var row = EquipmentProposalBorrow.getDetailEdit();
					$('#gridEquipLendChange').datagrid("updateRow", {
						index: EquipmentProposalBorrow._editIndex,
						row: row
					});
				}

				EquipmentProposalBorrow._editIndex = index;
				$('#gridEquipLendChange').datagrid('beginEdit', EquipmentProposalBorrow._editIndex);
				var edx = $('#gridEquipLendChange').datagrid('getEditors', EquipmentProposalBorrow._editIndex);
				$(edx[9].target).combobox("clear");
				$(edx[10].target).combobox("clear");
				EquipmentProposalBorrow.addEditorInGridChangeLend();
			}
		},
		onLoadSuccess :function(data){ 	 
	    	$('.datagrid-header-rownumber').html('STT');		    		
	    	$('.datagrid-header-row td div').css('text-align','center');
	    	// updateRownumWidthForDataGrid('');	    	
		}
	});	
	EquipmentProposalBorrow.getLstInfo();
	EquipmentProposalBorrow._editIndex = null;
});
var equipCreateLendSuggest = {
	datagridOption: {
		column0: {			
			formatter: function (value,row,index) {
				return '<a href="javascript:void(0);" onclick="EquipmentProposalBorrow.deleteRowEquipLendDetail('+index+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
			}
		},
		column1: {
			title: 'Loại thiết bị',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox',
				options:{
					valueField: 'code',
				    textField: 'name',
				    formatter: function(r) {
						return Utils.XSSEncode(r['name'].trim());
					},
				    filter: function(q, r){
						q = q.toString().trim().toUpperCase();
						q = unicodeToEnglish(q);
						return (r['searchText'].indexOf(q)>=0);
					},
				}
			}
		},
		column2: {
			title: 'Dung Tích',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column3: {
			title: 'Số lượng',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'numberbox',
				options:{
					width: 10,
					min:0,
			    	max: 9999999999
				}
			}
		},
		column4: {
			title: 'Mã đơn vị',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column5: {
			title: 'Khách hàng (F9)',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column6: {
			title: 'Tên khách hàng',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			}
		},
		column7: {
			title: 'Người đứng tên mượn thiết bị',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column8: {
			title: 'Quan hệ người đứng tên mượn thiết bị với chủ cửa hàng/Doanh nghiệp',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column9: {
			title: 'Điện thoại',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			}
		},
		column10: {
			title: 'Số nhà',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column11: {
			title: 'Tỉnh/TP',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox',
				options:{
					editable: false,
					valueField : 'areaCode',
					textField : 'areaName',
					formatter: function(r) {
						return Utils.XSSEncode(r['areaName'].trim());
					},
					filter: function(q, r){
						q = q.toString().trim().toUpperCase();
						q = unicodeToEnglish(q);
						return (r['searchText'].indexOf(q)>=0);
					},
					onSelect: function(r) {
						if(r != null){
							var data = {};
							data.objectType = 2; // district
							data.areaId = r.id;
							var ed = $('#gridEquipLendChange').datagrid('getEditors', EquipmentProposalBorrow._editIndex);
							$(ed[9].target).combobox('clear');
							$(ed[10].target).combobox('clear');
							Utils.getJSONDataByAjax(data, '/equipment-management-proposal-borrow/get-list-sub-area',function(result) {
								if(result == undefined || result == null){
									result = [];
								}			
								var obj;
								for (var i = 0, sz = result.length; i < sz; i++) {
									obj = result[i];
									obj.searchText = unicodeToEnglish(obj.areaCode + obj.areaName);
									obj.searchText = obj.searchText.toUpperCase();
								}			
								$(ed[9].target).combobox('loadData', result);
								var row = $('#gridEquipLendChange').datagrid('getRows')[EquipmentProposalBorrow._editIndex];
								if(row.districtCode != undefined && row.districtCode != null && row.districtCode != "" ){
									var dt = $(ed[9].target).combobox("getData");
									for(var i=0, sz = dt.length; i < sz; i++){
										if(dt[i].areaCode == row.districtCode){
											$(ed[9].target).combobox("select", row.districtCode);
										}
									}									
								}
							});
						}
					}
				}
			}
		},
		column12: {
			title: 'Quận/Huyện',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox',
				options:{
					editable: false,
					valueField : 'areaCode',
					textField : 'areaName',
					formatter: function(r) {
						return Utils.XSSEncode(r['areaName'].trim());
					},
					filter: function(q, r){
						q = q.toString().trim().toUpperCase();
						q = unicodeToEnglish(q);
						return (r['searchText'].indexOf(q)>=0);
					},
					onSelect: function(r) {
						if(r != null) {
							var data = {};
							data.objectType = 3; // ward
							data.areaId = r.id;
							var ed = $('#gridEquipLendChange').datagrid('getEditors', EquipmentProposalBorrow._editIndex);
							$(ed[10].target).combobox('clear');
							Utils.getJSONDataByAjax(data, '/equipment-management-proposal-borrow/get-list-sub-area',function(result) {
								if(result == undefined || result == null){
									result = [];
								}
								var obj;
								for (var i = 0, sz = result.length; i < sz; i++) {
									obj = result[i];
									obj.searchText = unicodeToEnglish(obj.areaCode + obj.areaName);
									obj.searchText = obj.searchText.toUpperCase();
								}
								$(ed[10].target).combobox('loadData', result);
								var row = $('#gridEquipLendChange').datagrid('getRows')[EquipmentProposalBorrow._editIndex];
								if(row.wardCode != undefined && row.wardCode != null && row.wardCode != "" ){
									var dt = $(ed[10].target).combobox("getData");
									for(var i=0, sz = dt.length; i < sz; i++){
										if(dt[i].areaCode == row.wardCode){
											$(ed[10].target).combobox("select", row.wardCode);
										}
									}
								}
							});
						}
					}
				}
			}
		},
		column13: {
			title: 'Phường/Xã',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox',
				options:{
					editable: false,
					valueField : 'areaCode',
					textField : 'areaName',
					formatter: function(r) {
						return Utils.XSSEncode(r['areaName'].trim());
					},
					filter: function(q, r){
						q = q.toString().trim().toUpperCase();
						q = unicodeToEnglish(q);
						return (r['searchText'].indexOf(q)>=0);
					}
				}
			}
		},
		column14: {
			title: 'Tên đường/Phố/Thôn/Ấp',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column15: {
			title: 'Số CMND',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column16: {
			title: 'Nơi cấp CMND',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column17: {
			title: 'Ngày cấp CMND',
			formatter: function (value,row,index) {
				var html = "";
				if(row.idNoDate != null) {
					html = Utils.XSSEncode(value);
				} else {
					//html = '<input type="text" id="idNoDate"'+ index +' style="width:100%" />';
				}
				return html;
			},
			editor: {
				type:'datebox'
			}
		},
		column18: {
			title: 'Số giấy phép ĐKKD',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column19: {
			title: 'Nơi cấp ĐKKD',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column20: {
			title: 'Ngày cấp ĐKKD',
			formatter: function (value,row,index) {
				var html = "";
				if(row.businessNoDate != null) {
					html = Utils.XSSEncode(value);
				} else {
					//html = '<input type="text" id="businessNoDate"'+ index +' style="width:100%" />';     
				}
				return html;
			},
			editor: {
				type:'datebox'
			}
		},
		column21: {
			title: 'Thời gian mong muốn nhận thiết bị',
			formatter: function (value,row,index) {
				var html = "";
				if(row.timeLend != null) {
					html = Utils.XSSEncode(value);
				} else {
					// html = '<input type="text" id="timeLend"'+ index +' style="width:100%" />';     
				}
				return html;
			},
			editor: {
				type:'datebox'
			}
		},
		column22: {
			title: 'Tình trạng thiết bị',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox',
				options:{
					editable: false,
					valueField: 'apParamCode',
				    textField: 'apParamName',
				    formatter: function(r) {
						return Utils.XSSEncode(r['apParamName'].trim());
					},
				    filter: function(q, r){
						q = q.toString().trim().toUpperCase();
						q = unicodeToEnglish(q);
						return (r['searchText'].indexOf(q)>=0);
					}
				}
			}
		},
		column23: {
			title: 'Kho xuất',
			formatter: function (value,row,index) {
				var html = ""
				if (value != null && value != "") {
					html = value;
				} else if (row.stockTypeValue != null) {
					html = TypeStockEquipLend.parseValue(row.stockTypeValue);
				}
				return Utils.XSSEncode(html);
			},
			editor: {
				type:'combobox',
				options:{
					editable: false,
					data: [{"value":1, "text":"Kho NCC"},{"value":2, "text":"Kho công ty"}],
				    valueField: 'value',
				    textField: 'text'
				}
			}
		},
		/* column24: {
			title: 'Doanh số trung bình 3 tháng gần nhất <s:property value = "nganhHangStr"/>',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			}
		} */
	}
}
</script>
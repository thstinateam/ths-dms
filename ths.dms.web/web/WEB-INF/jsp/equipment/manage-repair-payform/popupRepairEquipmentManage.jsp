<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchRepair" class="easyui-dialog"
		title="Chọn phiếu sửa chữa"
		data-options="closed:true,modal:true" >
			<!-- <div class="SearchSection GeneralSSection"> -->
				<!-- <div id="pnlListEquipment" class="easyui-panel" title="Thông tin tìm kiếm" style="width:auto; height:auto; padding-top: 20px;"data-options="collapsible:true,closable:false,border:false"> -->
				
					<!-- <div class="SearchInSection SProduct1Form"> -->
				<div class="GeneralForm Search1Form">
					<h2 class="Title2Style">Thông tin tìm kiếm <span style="float:right;margin-right:15px;">
						<a href="javascript:void(0);" title="Ẩn/ hiện" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
					</h2>
					<!-- <div class="SearchInSection SProduct1Form"> -->
					<div class="SProduct1Form">
						<label class="LabelStyle Label6Style">Mã đơn vị (F9)</label>						
						<input id="shop" type="text" style="height: auto;" class="InputTextStyle InputText8Style" maxlength="50" tabindex="1"/>
						<label	class="LabelStyle Label6Style">Mã phiếu</label>
						<input	type="text" class="InputTextStyle InputText8Style" id="txtRepairCodePopup" maxlength="50" tabindex="2"/>	
						<label class="LabelStyle Label6Style">Trạng thái</label>
						<div class="BoxSelect BoxSelect3 BoxDisSelect">
							<select class="MySelectBoxClass" id="statusPopup" onchange="" tabindex="3">
								<option value="6">Hoàn thành</option>
							</select>
						</div>
						<div class="Clear"></div>
						<label class="LabelStyle Label6Style">Mã KH</label>
						<input id="customerCode" type="text" class="InputTextStyle InputText8Style" maxlength="50" tabindex="4" />
						<label class="LabelStyle Label6Style">Tên KH</label>
						<input id="customerName" type="text" class="InputTextStyle InputText8Style" maxlength="250" tabindex="5"/>
						<label class="LabelStyle Label6Style">Thanh toán</label>
						<div class="BoxSelect BoxSelect3 BoxDisSelect">
							<select class="MySelectBoxClass" id="statusPayment" onchange="" tabindex="6">
								<option value="0">Chưa thanh toán</option>
							</select>
						</div>
						<div class="Clear"></div>
						<label	class="LabelStyle Label6Style">Số serial</label> 
						<input type="text" class="InputTextStyle InputText8Style" id="txtSerial" maxlength="100" tabindex="7"/>		
						<label class="LabelStyle Label6Style">Mã thiết bị</label> 
						<input	type="text" class="InputTextStyle InputText8Style" id="txtCode" maxlength="50" tabindex="8"/> 
						<div class="Clear"></div>
						<!-- <div style="margin-top: 10px"></div> -->
						<label class="LabelStyle Label6Style">Ngày tạo từ</label>
						<!-- <div id="fromDateDiv"> -->
							<input type="text" id="fromDate" value='<s:property value="fromDate"/>' class="InputTextStyle InputText6Style" tabindex="9"/>
						<!-- </div> -->
						<label class="LabelStyle Label6Style">Đến</label>
						<!-- <div id="toDateDiv"> -->
							<input type="text" id="toDate" value='<s:property value="toDate"/>' class="InputTextStyle InputText6Style" tabindex="10"/>
						<!-- </div> -->
						<div class="Clear"></div>		
						<!-- <div class="SearchInSection SProduct1Form"> -->
						<div class="SProduct1Form">
							<p id="successMsgPopup" class="SuccessMsgStyle" style="display: none"></p>
							<p id="errMsgPopup" class="ErrorMsgStyle" style="display: none;"></p>										
							<div class="BtnCenterSection">
								<div style="margin-top: 10px"></div>
								<button id="btnSearchRepairPopup" class="BtnGeneralStyle" onclick="RepairEquipmentPayment.searchPopupRepair();">Tìm kiếm</button>
							</div>
							<div class="Clear"></div>
						</div>
					</div>
				</div>			
				
				<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Danh sách phiếu sửa chữa thiết bị</h2>			
					<div class="GridSection" id="gridContainer">
						<table id="gridRepairPopup"></table>
					</div>
				</div>		
				<div class="Clear"></div>
				<div class="GeneralForm SProduct1Form">
					<div class="BtnCenterSection">
						<button id="btnPopupRepairChoose" class="BtnGeneralStyle"	onclick="RepairEquipmentPayment.chooseRepairPopup();">Chọn</button>
						<button id="btnPopupRepairClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchRepair').dialog('close');">Đóng</button>
					</div>
					<div class="Clear"></div>
					<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
				</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {	
	setDateTimePicker('fromDate');
	//ReportUtils.setCurrentDateForCalendar("fromDate");
	$("#fromDate").val(getLastWeek());//Lay truoc 7 ngay
	setDateTimePicker('toDate');
	ReportUtils.setCurrentDateForCalendar("toDate");
});
</script>
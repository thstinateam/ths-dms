<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="/equipment-repair-payment/info">Thiết bị</a>
		</li>
		<li>
			<s:if test='id != null'>
				<span>Cập nhật phiếu thanh toán sửa chữa</span>
			</s:if>
			<s:else>
				<span>Lập phiếu thanh toán sửa chữa</span>
			</s:else>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<!-- <h2 class="Title2Style">Thông tin chung</h2> -->
				<h2 class="Title2Style">Thông tin chung <span style="float:right;margin-right:15px;">
						<a href="javascript:void(0);" title="Ẩn/ hiện" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
				<div class="SearchInSection SProduct1Form">
					<label	class="LabelStyle Label1Style">Mã phiếu</label>
						<input	type="text" class="InputTextStyle InputText1Style" id="txtRepairCode" maxlength="50" value='<s:property value="equipmentRepairPayment.code"/>' />	
						<!-- <label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Ngày tạo từ</label> -->
						<label class="LabelStyle Label1Style" >Ngày thanh toán</label>
						<div id="fromDateDiv">
							<input type="text" id="fDate" class="InputTextStyle InputText6Style" value='<s:property value="equipmentRepairPayment.paymentDateStr"/>' />
						</div>
						<label class="LabelStyle Label1Style" >Trạng thái duyệt</label>
						<div class="BoxSelect BoxSelect2" id="ddlUsageStatusDiv">
							<select class="MySelectBoxClass InputTextStyle" id="cbxStatus">
							    <option value="0">Dự thảo</option>
							    <option value="1">Chờ duyệt</option>
							    <!-- <option value="4">Hủy</option> -->
							</select>
						</div>						
						<div class="Clear"></div>
						<p id="successMsgDetail" class="SuccessMsgStyle" style="display: none"></p>
						<p id="errMsgDetail" class="ErrorMsgStyle" style="display: none;"></p>
						<!-- <div class="SearchInSection SProduct1Form">
							<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
							<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
							<div class="BtnCenterSection">
								<button id="btnSearch" class="BtnGeneralStyle" onclick="RepairEquipmentPayment.searchRepairPayForm();">Tìm kiếm</button>
							</div>
							<div class="Clear"></div>
						</div>	 -->
				</div>				
				<h2 class="Title2Style"><span>Danh sách phiếu sửa chữa</span></h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridRepairItemContainer">
						<table id="gridRepairItem"></table>
					</div>
				</div>	
				<div class="SearchInSection SProduct1Form">
					<!-- <p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>	
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p> -->	
					<div class="BtnCenterSection">
						<button id="btnUpdate" class="BtnGeneralStyle cmsiscontrol" onclick="RepairEquipmentPayment.updateEquipmentRepairPaymentRecord(<s:property value="id"/>);">Cập nhật</button>
						<button id="btnClose" class="BtnGeneralStyle" onclick="location.href = '/equipment-repair-payment/info';">Bỏ qua</button>
					</div>
					<div class="Clear"></div>
				</div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup khi them moi danh sách thiet bi -->
<tiles:insertTemplate template="/WEB-INF/jsp/equipment/manage-repair-payform/popupRepairEquipmentManage.jsp" />
<script type="text/javascript">
	$(document).ready(function() {
		$('#fDate').focus();
		disabled('txtRepairCode');
		setDateTimePicker('fDate');

		var idPayment = '<s:property value="id"/>';	
		var status = '<s:property value="equipmentRepairPayment.status.value"/>';
		var statusPayment = '<s:property value="statusPayment"/>'; // gia tri nay khac null, !='' la file detail da o quyen duyet
		var htmlCombo = '';
		if (statusPayment == '' || statusPayment != StatusRecordsEquip.APPROVED) {
			// quyen Ql tao moi, chinh sua
			RepairEquipmentPayment.initializeRepairItemsGrid(idPayment, status);
			// truong hop tao moi phieu thanh toan: chua co status
			htmlCombo = '<option value="0">Dự thảo</option> <option value="1">Chờ duyệt</option>';
			if (status != '' && status == StatusRecordsEquip.DRAFT) {
				//htmlCombo = '<option value="1">Chờ duyệt</option> <option value="4">Hủy</option>';
				htmlCombo = '<option value="0">Dự thảo</option> <option value="1">Chờ duyệt</option> <option value="4">Hủy</option>';
			} else if (status == StatusRecordsEquip.WAITING_APPROVAL) {
				//htmlCombo = '<option value="4">Hủy</option>';
				htmlCombo = '<option value="1">Chờ duyệt</option> <option value="4">Hủy</option>';
				disableDateTimePicker('fDate');
			} else if (status == StatusRecordsEquip.NO_APPROVAL) {
				//htmlCombo = '<option value="1">Chờ duyệt</option> <option value="4">Hủy</option>';
				htmlCombo = '<option value="3">Không duyệt</option> <option value="1">Chờ duyệt</option> <option value="4">Hủy</option>';
			} else if (status == StatusRecordsEquip.APPROVED) {
				htmlCombo = '<option value="2">Duyệt</option>';
				disableDateTimePicker('fDate');
				disableSelectbox('cbxStatus');
// 				$('#btnUpdate').remove();
			} else if (status == StatusRecordsEquip.CANCELLATION) {
				htmlCombo = '<option value="4">Hủy</option>';
				disableDateTimePicker('fDate');
				disableSelectbox('cbxStatus');
// 				$('#btnUpdate').remove();
			}
		} else {
			$('#btnClose').attr('onclick', 'location.href = "/equipment-repair-payment/approved/info";');
			// quyen duyet
			RepairEquipmentPayment.initializeRepairItemsGrid(idPayment, status, statusPayment);
			if (status == StatusRecordsEquip.WAITING_APPROVAL) {
				htmlCombo = '<option value="1">Chờ duyệt</option> <option value="2">Duyệt</option> <option value="3">Không duyệt</option>';
				disableDateTimePicker('fDate');
			} else if (status == StatusRecordsEquip.APPROVED) {
				htmlCombo = '<option value="2">Duyệt</option>';
				disableDateTimePicker('fDate');
				disableSelectbox('cbxStatus');
				$('#btnUpdate').remove();
			} else if (status == StatusRecordsEquip.NO_APPROVAL) {
				htmlCombo = '<option value="3">Không duyệt</option>';
				disableSelectbox('cbxStatus');
				disableDateTimePicker('fDate');
				$('#btnUpdate').remove();
			}
		}
		$('#cbxStatus').html(htmlCombo).change();
		$('#cbxStatus').val(status).change();
	});
</script>
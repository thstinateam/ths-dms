<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="javascript:void(0);">Thiết bị</a>
		</li>
		<li>
			<span>Phê duyệt thanh toán sửa chữa</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm <span style="float:right;margin-right:15px;">
						<a href="javascript:void(0);" title="Ẩn/ hiện" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
					<div class="SearchInSection SProduct1Form">
						<label	class="LabelStyle Label1Style">Mã phiếu</label>
						<input	type="text" class="InputTextStyle InputText1Style" id="txtRepairCode" maxlength="50" autocomplete="off" />	
						<!-- <label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Ngày tạo từ</label> -->
						<label class="LabelStyle Label1Style" >Ngày thanh toán</label>
						<div id="fromDateDiv">
							<input type="text" id="fDate" class="InputTextStyle InputText6Style" autocomplete="off"/>
						</div>
						<label class="LabelStyle Label1Style" >Trạng thái duyệt</label>
						<div class="BoxSelect BoxSelect2" id="ddlUsageStatusDiv">
							<select class="MySelectBoxClass InputTextStyle" id="cbxStatus" autocomplete="off">
							    <option value="1" selected="selected">Chờ duyệt</option>
							    <option value="2">Đã Duyệt</option>
							    <option value="3">Không duyệt</option>
							</select>
						</div>						
						<div class="Clear"></div>
						<div class="SearchInSection SProduct1Form">
							<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
							<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
							<div class="BtnCenterSection">
								<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol" onclick="RepairEquipmentPayment.searchRepairPayForm();">Tìm kiếm</button>
							</div>
							<div class="Clear"></div>
						</div>	
					</div>	
				<h2 class="Title2Style">Danh sách phiếu thanh toán
					<div style="float:right; margin-top: -5px;margin-right: 27px;">
					<button style="float:right;" id="grpSave_btnSaveLst" class="BtnGeneralStyle cmsiscontrol" onclick="RepairEquipmentPayment.updateStatus();" >Lưu</button>
					<div id='grpSave_cbxStatusUpdateDiv' style="float:right;margin-right: 5px;" class="BoxSelect BoxSelect2 cmsiscontrol">							
						<select class="MySelectBoxClass InputTextStyle" id="cbxStatusUpdate">
							<option value="-1" selected="selected">Chọn</option>
						    <option value="2">Duyệt</option>
						    <option value="3">Không duyệt</option>
						</select>
					</div>
					<!-- <a  onclick = "RepairEquipmentPayment.exportBySearchListRepair();" style = "float: right;	margin: 0px 25px 0px 0px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_export.png" title="Xuất Excel"></a> -->
					<!-- <a  onclick = "RepairEquipmentPayment.openDialogImportRepair();" style = "float: right;	margin-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_import.png" title="Nhập Excel"></a>
					<a class="downloadTemplateReport"  style = "float: right; margin-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_download_32.png" title="Tải mẫu Nhập Excel"></a>
					<a href="javascript:void(0);" onclick="RepairEquipmentPayment.printRepair();" style="margin-right:7px;"><img src="/resources/images/icon-printer.png" height="20" width="20" title="In"></a> -->
					</div>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="grid"></table>
					</div>
				</div>		
				<div class="Clear"></div>
				<!-- </div> -->
			</div> <!-- end class="ToolBarSection" -->
		</div>
	</div>
	<div class="Clear"></div>
	<!-- Chi tiet chuc nang Form -->
	<div class="ContentSection" style="visibility:hidden; display: none;" id="equipHistoryTemplateDiv">
		<div class="BreadcrumbSection">
		    <ul class="ResetList FixFloat BreadcrumbList">
		        <li><span>Danh sách phiếu sửa chữa </span><span id="equipmentCodeFromSpan" style="color:#199700"></span></li>
		    </ul>
		</div>
		<div class="GeneralCntSection">
			<div class="GridSection" id="equipHistoryDgContainerGrid">
				 <table id="equipHistoryDg"></table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {	
	$('#txtRepairCode').focus();
	setDateTimePicker('fDate');
	//ReportUtils.setCurrentDateForCalendar("fDate");
	
	/** vuongmq; 20/04/2015; put params SearchPayForm vao de dung export Excel theo tim kiem */
	RepairEquipmentPayment.setParamSearchPayForm();
	
	RepairEquipmentPayment._mapRepair = new Map();
	$('#grid').datagrid({
		url : "/equipment-repair-payment/searchRepairPayForm",
		pagination : true,
        rownumbers : true,
        pageNumber : 1,
        scrollbarSize: 0,
        autoWidth: true,
        pageSize: 20,
        pageList: [20, 30, 50],
        autoRowHeight : true,
        fitColumns : true,
		queryParams: RepairEquipmentPayment._paramsSearch,
		width: $('#gridContainer').width() - 15,
		checkOnSelect: false, // vuongmq; 19/05/2015; chi cho check checkox
	    //height: 400,        
			/*{field: 'check',checkbox:true, width: 40, sortable:false,resizable:false, align: 'left'},
			{field:'edit', title:'<a href="/equipment-repair-manage/change-record" title="Tạo mới"><img src="/resources/images/icon_add.png"/></a>', width: 70, align: 'center', sortable: false, resizable: true, formatter: function(value,row,index){
		    	return '<a onclick="RepairEquipmentPayment.viewDetail('+row.id+',\''+row.maPhieu+'\')" href="javascript:void(0)"><img width="18" height="18" src="/resources/images/icon-view.png" title="Xem hạng mục sửa chữa '+row.maPhieu+'"></a><a onclick="" href="/equipment-repair-manage/change-record?id='+row.id+'"><img width="18" style="padding-left: 5px;" height="18" src="/resources/images/icon-edit.png" title="Chỉnh sửa '+row.maPhieu+'"></a>';
			}},*/
		columns:[[
			{field: 'check',checkbox:true, width: 40, sortable:false,resizable:false, align: 'left'},
			{field:'maPhieu', title: 'Mã phiếu', width:200, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'ngayLap', title: 'Ngày thanh toán', width: 100, sortable: false, resizable: false , align: 'center', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},	
		    {field:'trangThai',title: 'Trạng thái', width: 150, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	var status = '';
		    	if (value == StatusRecordsEquip.DRAFT) {
		    		status = StatusRecordsEquip.parseValue(StatusRecordsEquip.DRAFT);
		    	} else if (value == StatusRecordsEquip.WAITING_APPROVAL) {
		    		status = StatusRecordsEquip.parseValue(StatusRecordsEquip.WAITING_APPROVAL);
		    	} else if (value == StatusRecordsEquip.APPROVED) {
		    		status = StatusRecordsEquip.parseValue(StatusRecordsEquip.APPROVED);
		    	} else if (value == StatusRecordsEquip.NO_APPROVAL) {
		    		status = StatusRecordsEquip.parseValue(StatusRecordsEquip.NO_APPROVAL);
		    	} else if (value == StatusRecordsEquip.CANCELLATION) {
		    		status = StatusRecordsEquip.parseValue(StatusRecordsEquip.CANCELLATION);
		    	}
		    	return Utils.XSSEncode(status);
		    }},
			{field:'tongTien', title: 'Tổng tiền', width: 150, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
				if (value != undefined && value != null) {
		    		return formatCurrency(value);
		    	}	
		    	return '';	
		    }},
		    {field:'edit', title:'', width: 70, align: 'center', sortable: false, resizable: true, formatter: function(value,row,index){
		    	return '<a id="btnView' + row.id + '" class="cmsiscontrol" onclick="RepairEquipmentPayment.viewDetail('+row.id+',\''+row.maPhieu+'\')" href="javascript:void(0)"><img width="18" height="18" src="/resources/images/icon-view.png" title="Xem phiếu sửa chữa '+row.maPhieu+'"></a> <a id="btnEdit' + row.id + '" class="cmsiscontrol" onclick="" href="/equipment-repair-payment/change-record?id='+row.id+'&statusPayment=2"><img width="18" style="padding-left: 5px;" height="18" src="/resources/images/icon-edit.png" title="Chỉnh sửa '+row.maPhieu+'"></a>';
			}},
	    ]],
	    onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');
	    	$('.datagrid-header-check input').removeAttr('checked');
        	var rows = $('#grid').datagrid('getRows');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(RepairEquipmentPayment._mapRepair.get(rows[i].id)!=null) {
	    			$('#grid').datagrid('checkRow', i);
	    		}
	    	}
	    	Utils.functionAccessFillControl('gridContainer', function(data) {
	    		var arrViewLength =  $('#gridContainer a[id^="btnView"]').length;
				var invisibleViewLenght = $('.isCMSInvisible[id^="btnView"]').length;
				var arrEditLength =  $('#gridContainer a[id^="btnEdit"]').length;
				var invisibleEditLenght = $('.isCMSInvisible[id^="btnEdit"]').length;
				if ((arrViewLength > 0 && arrViewLength != invisibleViewLenght) || (arrEditLength > 0 && arrEditLength != invisibleEditLenght)) {
					$('#grid').datagrid("showColumn", "edit");
				} else {
					$('#grid').datagrid("hideColumn", "edit");
				}
	    	});
   		 	$(window).resize();    		 

	    },
	    onCheck:function(index,row){
			RepairEquipmentPayment._mapRepair.put(row.id,row);   
	    },
	    onUncheck:function(index,row){
	    	RepairEquipmentPayment._mapRepair.remove(row.id);
	    },
	    onCheckAll:function(rows){
	    	for (var i = 0; i < rows.length; i++) {
	    		var row = rows[i];
	    		RepairEquipmentPayment._mapRepair.put(row.id,row);
	    	}
	    },
	    onUncheckAll:function(rows){
	    	for(var i = 0; i < rows.length; i++) {
	    		var row = rows[i];
	    		RepairEquipmentPayment._mapRepair.remove(row.id);		    		
	    	}
	    }
	});
	
	$('#txtRepairCode, #fDate, #cbxStatus').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});
});
</script>
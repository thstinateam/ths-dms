<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="javascript:void(0);">Thiết bị</a>
		</li>
		<li>
			<span>Quản lý danh mục</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin tìm kiếm" style="width:1000px;height:200px;padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
				<div class="SearchInSection SProduct1Form">
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style" style="margin-left: 18%;">Loại</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="type">
							<option value="-1">Tất cả</option>
							<option value="2" >NCC</option>
						<!-- 	<option value="3" >Hiệu</option>	 -->						
							<option value="4" >Hạng mục sửa chữa</option>
							<option value="1" > Loại thiết bị</option>
						</select>
					</div>
					
					<label	class="LabelStyle Label1Style" style="">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status">
							<option value="-1" selected="selected">Tất cả</option>
							<option value="1">Hoạt động</option>
						    <option value="0">Tạm ngưng</option>						    
						</select>
					</div>	
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style" style="margin-left: 18%;">Mã</label> 
					<input	type="text" class="InputTextStyle InputText1Style" id="code" maxlength="50"/> 
					<label	class="LabelStyle Label1Style" style="">Tên</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="name" maxlength="250"/>	
				</div>			
				<div class="SearchInSection SProduct1Form">
				<div class="Clear"></div>
					<div class="BtnCenterSection">
                        <button class="BtnGeneralStyle" id="btnSearch" onclick="return EquipmentManageCatalog.searchinfo();">Tìm kiếm</button>
                    </div>
					<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
					<p class="ErrorMsgStyle" id="errMsgSearch" style="display: none;"></p>
				</div>
			</div>	
				<h2 class="Title2Style" style="height:15px;">
					<span style="float:left;">Danh sách hạng mục</span>
					<div style="float:right; margin-top: -5px;margin-right: 27px;">
						<a href="#" class="downloadTemplateReport"><img src="/resources/images/icon_download_32.png" height="20" width="20" title="Tải mẫu Nhập Excel"></a>
						<a href="javascript:void(0);" onclick="EquipmentManageCatalog.showDlgCatalogImportExcel();"><img src="/resources/images/icon-import.png" height="25" width="25" title="Nhập Excel"></a>
						<a href="javascript:void(0);" onclick="EquipmentManageCatalog.exportExcel();"><img src="/resources/images/icon-export.png" height="25" width="25" title="Xuất Excel"></a>					
					</div>
				</h2>	
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id=gridContainer>
							<table id="grid"></table>
						</div>
					</div>						
			  </div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Import - Danh mục thiết bị" data-options="closed:true,modal:true" style="width:470px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-manage-catalog/import-excel" name="importFrmCatalog" id="importFrmCatalog" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">
						<input id="fakefilepcCatalog" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileCatalog" onchange="previewImportExcelFile(this,'importFrmCatalog','fakefilepcCatalog', 'errExcelMsgCatalog');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a onclick="" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadExcelFile" onclick="return EquipmentManageCatalog.importExcelCatalog();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgCatalog"/>
	</div>	
</div>
<%-- <s:hidden id="" value="0"></s:hidden> --%>
<script type="text/javascript">
$(document).ready(function() {	
	 $('#code, #name').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	}); 
	$('#panelDeliveryRecord').width($(window).width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
	},1000);
	function selectedDropdowlist(id, value){
		if(id!=undefined && id!=null && value!=undefined && value!=null){
			$('#'+id+' option[value='+value+']').attr('selected','selected').change();
		}
	}
	var params = new Object();	
	params.code = $('#code').val().trim();
	params.name = encodeChar($('#name').val().trim());
	params.status = $('#status').val();
	$('#grid').datagrid({
		url : "/equipment-manage-catalog/search",
		autoRowHeight : true,
		rownumbers : true, 
		checkOnSelect :true,
		singleSelect: true,
		pagination:true,
		rowNum : 50,
		fitColumns:true,
		pageList  : [50,100,150],
		scrollbarSize:0,
		width: $('#gridContainer').width(),
		autoWidth: true,
		queryParams:params,	
	    columns:[[	        
		    {field:'code', title: 'Mã', width: 100, sortable: false, resizable: true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field:'name', title: 'Tên', width:200,sortable:false,resizable:true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},	    
		    {field:'type',title: 'Loại', width: 80, align: 'left',sortable:false, resizable:true, formatter:function(value, row, index){
		    	if (value == undefined || value == null || value.toString().trim().length == 0 || isNaN(value)){
					return "";
				}
				if(value == 1){
					return "Loại thiết bị";
				}else if(value == 2){
					return "NCC";
				}else if(value == 3){
					return "Hiệu";
				}else if(value == 4){
						return "Hạng mục sửa chữa";
					}
		    }},
		    {field:'status', title: 'Trạng thái', width: 80, align: 'left',sortable:false,resizable:true,formatter:function(value, row, index){
		    	if (value != undefined && value != null) {
		    		return statusTypeText.parseValue(value);
		    	}
		    	return "";
		    }},
		    {field:'edit',  title:'<a href="/equipment-manage-catalog/change?id=0"><img src="/resources/images/icon_add.png"/></a>',
		    	width: 20, align: 'center',sortable:false,resizable:true, formatter: function (value, row, index) {
		    		if (row.id != undefined && row.id != null && !isNaN(row.id)) {			
		    			return '<a href="/equipment-manage-catalog/change?id=' + row.id +'&type='+ row.type +'"><img width="16" style="" heigh="16" src="/resources/images/icon-edit.png" title="Chỉnh sửa"></a>';		
		    		} 
		    		return '<a><img width="16" style="" heigh="16" src="/resources/images/icon-edit_disable.png" title="Chỉnh sửa"></a>';
		    }},
		    {field: 'id', index: 'id', hidden: true},
	    ]],	    
	    onLoadSuccess :function(data){	 
	    	 if (data == null || data.total == 0) {
			//	 $("#errMsgSearch").html("Không tìm thấy dữ liệu").show();
			 }else{
				 $('.datagrid-header-rownumber').html('STT');			
				 Utils.updateRownumWidthAndHeightForDataGrid('grid');
				 $(window).resize();
			 }			
		}
	});	
	$('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Import_DanhMuc.xlsx');
});
</script>
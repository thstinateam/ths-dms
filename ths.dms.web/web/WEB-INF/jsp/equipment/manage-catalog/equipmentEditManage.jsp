<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="/equipment-manage-catalog/info">Thiết bị</a>
		</li>
		<li>
			<span>Thiết lập danh mục</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
			<div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin chung" style="width:1000px;height:600px;padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
				<div class="SearchInSection SProduct1Form">	
				
				<label class="LabelStyle Label2Style" style=";text-align:left;;height:auto;">Loại</label>
					<s:if test="id==null || id ==0">
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="type"  tabindex="2" onchange="changeCombo();">		
								<option value="2" selected="selected"  >NCC</option>					
								<option value="1" >Loại thiết bị</option>
								<option value="4" >Hạng mục sửa chữa</option>
							</select>
						</div>
					</s:if>	
					<s:else>
					  <div class="BoxSelect BoxSelect2 BoxDisSelect">	
						<select class="MySelectBoxClass" id="type" disabled="disabled"  tabindex="2" >	
						<s:if test="type == 1 ">
							<option value="1" selected="selected" >Loại thiết bị</option>
						</s:if>	
						<s:elseif test="type == 2">
							<option value="2" selected="selected">NCC</option>
						</s:elseif>
						<s:elseif test="type == 3">
							<option value="3" selected="selected">Hiệu</option>
						</s:elseif>
						<s:elseif test="type == 4">
							<option value="4" selected="selected">Hạng mục sửa chữa</option>
						</s:elseif>
						</select>
					  </div>	
					</s:else>
					
					<div class="Clear"></div>								
					<label class="LabelStyle Label2Style" style=";text-align:left;;height:auto;">Mã <span class="RequireStyle">(*)</span></label> 
					<s:if test="id==null || id ==0">
						<input id="code" type="text" class="InputTextStyle InputText1Style"  tabindex="2" autocomplete="off" maxlength="50" /> 
					</s:if>	
					<s:else>
						<input id="code" type="text" class="InputTextStyle InputText1Style" value="<s:property value="equipmentVO.code" />"  tabindex="2" autocomplete="off" maxlength="50" disabled="disabled"/> 
					</s:else>
					<label class="LabelStyle Label2Style" style=";text-align:left;margin-left: 60px;height:auto;">Tên <span class="RequireStyle">(*)</span></label> 
					<s:if test="id==null || id ==0">
						<input id="name" type="text" class="InputTextStyle InputText1Style"  tabindex="2" autocomplete="off" maxlength="250"/> 
					</s:if>	
					<s:else>
						<input id="name" type="text" class="InputTextStyle InputText1Style" value="<s:property value="equipmentVO.name" />"  tabindex="2" autocomplete="off" maxlength="250"/> 
					</s:else>
					
					<label	class="LabelStyle Label2Style" style=";text-align:left;margin-left: 60px;height:auto;">Trạng thái</label>
						<div class="BoxSelect BoxSelect2">
						<s:if test="id==null || id ==0">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status" disabled="disabled"  tabindex="2" >							  
							    <option value="1">Hoạt động</option>
							</select>
						</s:if>		
						<s:else>
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status"  tabindex="2" >	
								<s:if test="equipmentVO.status == 1">
								    <option value="1">Hoạt động</option>
								    <option value="0">Tạm ngưng</option>
								</s:if>
								<s:elseif test="equipmentVO.status == 0">					
								    <option value="0">Tạm ngưng</option>
								    <option value="1">Hoạt động</option>
								 </s:elseif>	
							</select>	
						</s:else>							
						</div>														
					<div class="Clear"></div>
					<div id="typeItemDiv" style="display:none;">
						<label class="LabelStyle Label2Style" style=";text-align:left;;height:auto;">Loại hạng mục</label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="typeItem"  tabindex="2" >		
								<option value="0" selected="selected"  >Không cho thay đổi giá trị</option>					
								<option value="1" >Cho thay đổi giá trị</option>
							</select>
						</div>
						<label class="LabelStyle Label2Style" style=";text-align:left;margin-left: 60px;height:auto;">Số tháng bảo hành </label> 
						<input id="warranty" type="text" class="InputTextStyle InputText1Style"  tabindex="2" autocomplete="off" maxlength="6"/> 
					</div>
					<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>	
				</div>	
					<div class="SearchInSection SProduct1Form">                                
	                   <div class="BtnCenterSection" style="padding-bottom: 10px;">
	                    	   <button class="BtnGeneralStyle" id="btnCapNhat" onclick="return EquipmentManageCatalog.save();"  tabindex="2" >Cập nhật</button>						   
						       <button class="BtnGeneralStyle" onclick="window.location.href='/equipment-manage-catalog/info'"  tabindex="2" >Bỏ qua</button>
			           </div>
		            </div>	

		         </div>	
			  </div>
				<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	<div class="Clear"></div>
</div>
<s:hidden id="idChange" name="id"></s:hidden>
<s:hidden id="typeChange" name="type"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	$('#panelDeliveryRecord').width($(window).width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
	},1000);
	$('#code, #name, #type').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnCapNhat').click(); 
		}
	});	
	Utils.bindFormatOnTextfield('warranty',Utils._TF_NUMBER);
	Utils.formatCurrencyFor('warranty');
	var statusEq = '<s:property value="equipmentVO.status" />';
	if(statusEq != undefined && statusEq != "" && statusEq == equipStatusText.ACTIVE){
		enableSelectbox('status');
	} else {
		disableSelectbox('status');
	}
	var type = $('#typeChange').val();
	if(type!=null && type == '4'){
		if($('#idChange').val() != undefined && $('#idChange').val() != '' && $('#idChange').val() != '0'){
			var typeItem = '<s:property value="equipmentVO.type" />';
			var warranty = '<s:property value="equipmentVO.warranty" />';
			$('#typeItemDiv').show();
			if(typeItem!=null){
				$('#typeItem').val(typeItem).change();
			}
			if(warranty!=null && warranty!=''){
				$('#warranty').val(formatCurrency(warranty));
			}
		}
	}
});
function changeCombo() {
	var val = $("#type").val();
	if (val == '4') {
		$('#typeItemDiv').show();
		$('#typeItem').val(0).change();
		$('#warranty').val('');
	} else {
		$('#typeItemDiv').css('display','none');
	}
	//tamvnm: gioi han ma loai 2 ky tu
	if (val == EquipmentManageCatalog._categoryType) {
		$('#code').attr('maxlength','2');
	} else {
		$('#code').attr('maxlength','50');
	}
}
</script>
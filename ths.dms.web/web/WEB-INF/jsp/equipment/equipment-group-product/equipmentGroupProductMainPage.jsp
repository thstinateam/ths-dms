<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>


<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Thiết bị</a></li>
		<li><span>Danh sách sản phẩm cho thiết bị</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div class="SearchInSection SProduct1Form" style="padding-left: 0px;">
				<div id="panelSearch" class="easyui-panel" title="Thông tin tìm kiếm" style="width:1000px;height: auto;padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
					<label class="LabelStyle Label1Style">Mã nhóm thiết bị</label>
					<input id="equipmentGroupCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="1" />
					<label class="LabelStyle Label1Style">Tên nhóm thiết bị</label>
					<input id="equipmentGroupName" type="text" class="InputTextStyle InputText1Style" maxlength="250" tabindex="2"/>
					<label class="LabelStyle Label1Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
<%-- 						<s:select cssStyle="opacity: 0; height: 20px;" tabindex="3" cssClass="MySelectBoxClass" list="statusRecords" id="equipmentGroupStatus" --%>
<!-- 								value="%{statusRecords.get(1).apParamCode}" listKey="apParamCode" listValue="apParamName" disabled="disabledButton" > -->
<%-- 						</s:select> --%>
						<select style="opacity: 0; height: 20px;" class="MySelectBoxClass" id="equipmentGroupStatus">
							<option value="-2">Tất cả</option>
						    <option selected="selected" value="1">Hoạt động</option>
						    <option value="0">Tạm ngưng</option>
						</select>
					</div>
					<div class="Clear"></div>
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnSearch" class="BtnGeneralStyle" onclick="EquipmentGroupProduct.searchEquipmentGroupHandler();" tabindex="4">Tìm kiếm</button>
						</div>
						<div class="Clear"></div>
					</div>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
				</div>		
				</div>		
				<h2 class="Title2Style" style="height:15px;">
					<span style="float:left;">Danh sách nhóm thiết bị</span>
					<div style="float:right; margin-top: -5px;margin-right: 27px;">
						<a href="/resources/templates/equipment/Bieu_mau_import_danh_sach_san_pham_thiet_bi.xlsx" class="downloadTemplateReport" id="btnTemplate"><img src="/resources/images/icon_download_32.png" height="20" width="20" style="border-left-color: blue; border-left-style: solid; border-left-width: 2px;" title="Tải tập tin mẫu"></a>
						<a href="javascript:void(0);"  id="btnImport" onclick="EquipmentGroupProduct.showDlgImportExcel();"><img src="/resources/images/icon_table_import.png" height="20" width="20" title="Nhập excel"></a>
						<a href="javascript:void(0);"  id="btnExport" onclick="EquipmentGroupProduct.exportExcelGroupProduct();" style="margin-right: 5px;"><img src="/resources/images/icon_table_export.png" height="20" width="20" title="Xuất excel"></a>
					</div>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="equipmentGroupGrid"></table>
					</div>
				</div>							
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
 <div style="display: none;" id="popupProductSearchDiv">
	<div id="popupProductSearch" class="easyui-dialog" title="Thêm sản phẩm" style="width:890px; height:auto;" data-options="closed:true,modal:true">
	    <div class="PopupContentMid2">
	    	<div class="GeneralForm Search1Form">
	        	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
	            <label class="LabelStyle Label2Style">Mã sản phẩm</label>
	            <input id="productCodeDl" type="text" class="InputTextStyle InputText5Style" maxlength="20" autofocus="autofocus" />
	            <label class="LabelStyle Label2Style">Tên sản phẩm</label>
	            <input id="productNameDl" type="text" class="InputTextStyle InputText5Style" maxlength="250"  />
	            <label class="LabelStyle Label2Style">Trạng thái</label>
	            <div class="BoxSelect BoxSelect5" id="productStatusDlDiv">
					<select style="opacity: 0; height: 20px;" class="MySelectBoxClass" id="productStatusDl">
					    <option value="-2">Tất cả</option>
					    <option selected="selected" value="1">Hoạt động</option>
					    <option value="0">Tạm ngưng</option>
					</select>
				</div>
	            <div class="Clear"></div>
				<label class="LabelStyle Label2Style">Ngành hàng</label>
				<div class="BoxSelect BoxSelect5" id="productInforDlDiv">
					 <s:select cssStyle="opacity: 0; height: 20px;" tabindex="3" cssClass="MySelectBoxClass" list="lstProductInfo" id="productInforDl"
							value="%{lstProductInfo.get(1).productInfoCode}" listKey="id" listValue="productInfoName" disabled="disabledButton" >
					</s:select>
				</div>
	            <div class="Clear"></div>
	            <div class="BtnCenterSection">
	                <button id="btnSeachProductTree" class="BtnGeneralStyle"  onclick="return EquipmentGroupProduct.searchProductOnTree();">Tìm kiếm</button>
	            </div>
	            <div class="Clear"></div>
	        </div>
	        <div class="GeneralForm Search1Form">
	        	<h2 class="Title2Style Title2MTStyle">Danh sách sản phẩm</h2>
	        	<div class="GridSection" id="gridProductInsertContainer">
	        		<table id="gridProductInsert"></table>
	            </div>
	            <div class="Clear"></div>
	            <div class="BtnCenterSection">
	                <button id="btnSaveProduct" class="BtnGeneralStyle" onclick="return EquipmentGroupProduct.addProductsToEquipmentGroup();">Cập nhật</button>
	            </div>
	            <p id="successMsgPopup3" class="SuccessMsgStyle" style="display: none"></p>
	            <p id="errMsgPopup3" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
	        </div>
		</div>
	</div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Import - Danh sách sản phẩm cho thiết bị" data-options="closed:true,modal:true" style="width:470px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-group-product/import-excel" name="importFrmBBGN" id="importFrmBBGN" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">
						<input id="fakefilepcBBGN" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileBBGN" onchange="previewImportExcelFile(this,'importFrmBBGN','fakefilepcBBGN', 'errExcelMsgBBGN');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="">
					<a onclick="" class="downloadTemplateReport" >Tải file excel mẫu</a>
					
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadExcelFile" onclick="return EquipmentGroupProduct.importExcelGroupProduct();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgBBGN"/>
	</div>	
</div>
<script type="text/javascript">
$(document).ready(function() {	
	$('#productInforDl').prepend('<option selected="selected" value="-2">Tất cả</option>').change();
	/*
	 * custom giao dien
	 */
	$('#panelSearch').width($('#panelSearch').parent().width());
	$('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_import_danh_sach_san_pham_thiet_bi.xlsx');
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 5px;");
		$('#equipmentGroupCode').focus();
	}, 1000);
	// khoi tao cac control tren trang: grid, ...
	EquipmentGroupProduct.initializePageControl({
		gridId: 'equipmentGroupGrid',
		url: '/equipment-group-product/search-equipment-group'
	});
	EquipmentGroupProduct.searchEquipmentGroupHandler();
	$('#productCodeDl, #productNameDl, #productStatusDlDiv, #productInforDlDiv').bind('keyup', function(event) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if (event.keyCode == keyCodes.ENTER) {
			$('#btnSeachProductTree').click();
		}
	});	
});
</script>
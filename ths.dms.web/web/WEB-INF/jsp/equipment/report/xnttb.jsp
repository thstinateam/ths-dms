<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<dl class="Dl3Style">
	<dt>
		<label  class="LabelStyle Label1Style">Từ ngày</label>
	</dt>
	<dd>
		<input id="fDate" type="text" class="InputTextStyle InputText2Style" style="width:218px;" maxlength="10" /> 
	</dd>
	<dt>
		<label  class="LabelStyle Label1Style">Đến ngày</label>
	</dt>
	<dd>
		<input id="tDate" type="text" class="InputTextStyle InputText2Style" style="width:218px;" maxlength="10" /> 
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="EquipmentReport.exportXnttb();">Xuất báo cáo</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<script type="text/javascript">
	$(document).ready( function() {

		setDateTimePicker('fDate');
		ReportUtils.setCurrentDateForCalendar("fDate");
		setDateTimePicker('tDate');
		ReportUtils.setCurrentDateForCalendar("tDate");
		
		$('.MySelectBoxClass').customStyle();
		//disableDateTimePicker('fromDate');
		//disableDateTimePicker('toDate');
	});
	
	function changeInfoExportXnttb() {
		//var info = $('#infoExport').val();
		//if (activeType.STOPPED == info) {
		//	disableDateTimePicker('fromDate');
		//	disableDateTimePicker('toDate');
		//} else {
			enableDateTimePicker('fDate');
			enableDateTimePicker('tDate');
		//}
	}
</script>
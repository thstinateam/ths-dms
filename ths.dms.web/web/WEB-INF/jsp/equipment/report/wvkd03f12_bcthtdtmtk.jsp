<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị <span class="ReqiureStyle">*</span></label>
	</dt>
	<dd>
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
		</s:if><s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
	</dd>
	
	<dt class="ClearLeft LabelTMStyle" style="margin-top: 2px;">
		<label class="LabelStyle Label1Style" style=";text-align:right;;height:auto;">Từ ngày</label>
	</dt>
	<dd>
		<div id="fromDate">
			<input type="text" id="fDate" class="InputTextStyle InputText6Style" tabindex="2" />
		</div>
	</dd>
	<dt class="LabelTMStyle" style="margin-top: 2px;">
	<label class="LabelStyle Label1Style">Đến ngày </label>
	
	</dt>
	<dd>
		<div id="toDate">
			<input type="text" id="tDate" class="InputTextStyle InputText6Style" tabindex="2"/>
		</div>	
	</dd>
	<dt class="ClearLeft LabelTMStyle" style="margin-top: 2px;">
		<label class="LabelStyle Label1Style">Loại thiết bị<span class="ReqiureStyle"> *</span><br /></label>
	</dt>
	<dd style="height: auto; margin-top:-3px;">
		<input id="categoryEquip" type="text" data-placeholder="Tất cả loại thiết bị" style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="LabelTMStyle" style="margin-top:2px;">
		<label class="LabelStyle Label1Style" >Nhóm thiết bị<span class="ReqiureStyle"> *</span><br /></label>
	</dt>
	<dd style="height:auto;">		
		<div class="BoxSelect BoxSelect2"  id="divEquipGroup" style="margin-top:-3px">
			<div class="Field2">
				<input id="equipGroup" type="text" data-placeholder="Tất cả nhóm thiết bị"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>	
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="EquipmentReport.exportEQ_3_1_1_10();">Xuất báo cáo</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready( function() {
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	$("#fDate").val('<s:property value="fromDate"/>');
	ReportUtils.setCurrentDateForCalendar("tDate");
	
	ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId) {
	});
    // EquipmentReport.loadComboEquipGroup("/report/equipment/getListCustormeType");
    $("#equipGroup").kendoMultiSelect({
        dataTextField: "code",
        dataValueField: "id",
        filter: "contains",
		itemTemplate: function (data, e, s, h, q) {
			return '<div node-id="' + Utils.XSSEncode(data.id) + '" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.code) + ' - ' + Utils.XSSEncode(data.name)+'</span></div>';
		},
		tagTemplate: '#: data.code #',
        change: function(e) {
        	var lstGroupEquip = this.value();
        	var param = '';
        	var param1 = '';
        	for (var i = 0, size = lstGroupEquip.length; i < size; i++) {
        		if (lstGroupEquip[i] != null && lstGroupEquip[i] != undefined && lstGroupEquip[i].length == 0) {
	        		if (i == 0) {
	        			param += '?lstGroupEquip=' + lstGroupEquip[i];
	        			param1 += '?lstGroupEquipId=' + lstGroupEquip[i];
	        		} else {
	        			param += '&lstGroupEquip='+lstGroupEquip[i];
	        			param1 += '&lstGroupEquipId='+lstGroupEquip[i];
	        		}
	        	}
        	}
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/stock-equip-group/kendo-ui-combobox-ho.json?arrStatus=0,1"
                }
            }
        }
		//,value: [$('#curShopId').val()]
    });
    $(window).bind('keypress',function(event) {
		if ($('.fancybox-inner').length != 0) {
			if (event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP) {
				Images.showImageNextPre(0);
			} else if (event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN) {
				Images.showImageNextPre(1);
			}
			return false;
		}
		return true;
	});		
    
	// setTimeout(function() {
	// 	$('.k-input').focus();
	// }, 1000);		
	
	
	// $('#year').bind('change',function(){
	// 	var year = $('#year').val().trim();
	// 	var msg = Utils.getMessageOfRequireCheck('year', 'Năm');
	// 	if (msg.length == 0) {
	// 		msg = Utils.getMessageOfSpecialCharactersValidateEx('year', 'Năm',Utils._TF_NUMBER);
	// 	}
	// 	if(msg.length >0){
	// 		$('#errMsg').html(msg).show();
	// 		changeValuePeriodCombobox(null,"period");
	// 		return false;
	// 	}
		
	// 	var params = new Object();
	// 	params.year = year;
	// 	Utils.getJSONDataByAjaxNotOverlay(params, '/report/equipment/getPeriod', function(data){						
	// 		changeValuePeriodCombobox(data,"period");
	// 	});		
	// }).change();
	$("#categoryEquip").kendoMultiSelect({
        dataTextField: "code",
        dataValueField: "id",
        filter: "contains",
		itemTemplate: function (data, e, s, h, q) {
			return '<div node-id="' + Utils.XSSEncode(data.id) + '" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.code) + ' - ' + Utils.XSSEncode(data.name)+'</span></div>';
		},
		tagTemplate: '#: data.code #',
        change: function(e) {
        	var lstCategoryEquip = this.value();
        	var param = '';
        	var param1 = '';
        	for (var i = 0, size = lstCategoryEquip.length; i < size; i++) {
        		if (lstCategoryEquip[i] != null && lstCategoryEquip[i] != undefined && lstCategoryEquip[i].length == 0) {
	        		if (i == 0) {
	        			param += '?lstCategoryEquip=' + lstCategoryEquip[i];
	        			param1 += '?lstCategoryEquipId=' + lstCategoryEquip[i];
	        		} else {
	        			param += '&lstCategoryEquip='+lstCategoryEquip[i];
	        			param1 += '&lstCategoryEquipId='+lstCategoryEquip[i];
	        		}
	        	}
        	}
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/stock-equip-category/kendo-ui-combobox-ho.json?arrStatus=0,1"
                }
            }
        }
		//,value: [$('#curShopId').val()]
    });
	// $('#equipGroupId').dropdownchecklist({ 
	// 	forceMultiple: true,
	// 	firstItemChecksAll: true,
	// 	maxDropHeight: 350,
	// 	width: 226,
	// 	onItemClick:function(checkbox, selector){				
	// 	}
	// });	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<style>
	#selectImageType .ui-dropdownchecklist-text,
	#selectImageType .ui-dropdownchecklist-selector {
    	width: 196px !important;
	}	
</style>
<dl class="Dl3Style">
	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Kho</label>
	</dt>
	<dd style="height: auto; margin-top:-10px;">
		<input id="stockEquip" type="text" data-placeholder="Tất cả kho"  style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="LabelTMStyle" style="height: auto; margin-top: -7px;">
		<label class="LabelStyle Label1Style">Trạng thái</label>
	</dt>
	<dd style="margin-top: -10px;">
		<select id="status" class="MySelectBoxClass">
			<option value="-2">Tất cả</option>
			<option value="1">Tủ tốt</option>
			<option value="2">Sửa chữa</option>
			<option value="3">Thanh lý</option>
			<option value="4">Đã mất</option>
		</select>
	</dd>
	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Loại thiết bị</label>
	</dt>
	<dd style="height: auto; margin-top:-10px;">
		<input id="categoryEquip" type="text" data-placeholder="Tất cả loại thiết bị" style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="LabelTMStyle">
		<label class="LabelStyle Label1Style" >Nhóm thiết bị</label>
	</dt>
	<dd style="height: auto; margin-top:-10px;">
		<input id="groupEquip" type="text" data-placeholder="Tất cả nhóm thiết bị" style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="ClearLeft LabelTMStyle" style="height: auto; margin-top: 0;">
		<label  class="LabelStyle Label1Style">Năm<span class="ReqiureStyle">(*)</span></label>
	</dt>
	<dd>
		<input id="year" type="text" maxlength="4" class="InputTextStyle InputText1Style" /> 
	</dd>
	<dt class="LabelTMStyle" style="height: auto; margin-top: 0;">
		<label class="LabelStyle Label1Style">Kỳ<span class="ReqiureStyle">(*)</span></label>
	</dt>
	<dd>
		<div class="BoxSelect BoxSelect2">							
			<select class="MySelectBoxClass" id="period"></select>
		</div>	
	</dd>
	
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" id = "btnReportXNT1D1" onclick="EquipmentReport.exportXNT1D2();">Xuất báo cáo</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<script type="text/javascript">
	$(document).ready( function() {
		$('.MySelectBoxClass').customStyle();
		$('#status').css('width', '225');
		$('#period').css('width', '225');
		$('.CustomStyleSelectBox').css('width', '192');
		$('.CustomStyleSelectBox').css('height', '21');
		var date = new Date();
		var y = date.getFullYear();
		$('#year').val(y);
		$('#year').bind('change',function(){
			var year = $('#year').val().trim();
			var msg = Utils.getMessageOfRequireCheck('year', 'Năm');
			if (msg.length > 0) {
				$('#errMsg').html(msg).show();
				return false;
			}
			
			var params = new Object();
			params.year = year;
			Utils.getJSONDataByAjaxNotOverlay(params, '/report/equipment/getPeriod', function(data){						
				if (data != undefined && data != null && data.rows != undefined && data.rows.length != 0) {
				    var html = "";	
				    var CHOSE = data.rows[0].id;
					for(var i = 0,size = data.rows.length; i < size; i++){
						html += ('<option value="'+data.rows[i].id+'">'+(data.rows[i].name)+'</option>');
						if(data.rows[i].status==2){
							CHOSE = data.rows[i].id;
						}
					}
					$('#period').html(html);
					$('#period').val(CHOSE).change();
					if (data.equipPeriodCurrent != null && data.equipPeriodCurrent != undefined) {
						selectedDropdowlist('period', data.equipPeriodCurrent.id);
					}
				} else {
					var html =  ('<option value="-1" selected="selected"></option>'); 
					$('#period').html(html);
					$('#period').val('-1').change();	
				}
			});		
		}).change();
		
		$("#stockEquip").kendoMultiSelect({
	        dataTextField: "equipStockCode",
	        dataValueField: "equipStockId",
	        filter: "contains",
			itemTemplate: function (data, e, s, h, q) {
				return '<div node-id="'+Utils.XSSEncode(data.equipStockId)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.equipStockCode) + ' - ' + Utils.XSSEncode(data.equipStockName)+'</span></div>';
			},
			tagTemplate: '#: data.equipStockCode #',
	        change: function(e) {
	        	var lstStock = this.value();
	        	var param = '';
	        	var param1 = '';
	        	for (var i = 0, size = lstStock.length; i < size; i++) {
	        		if (lstStock[i] != null && lstStock[i] != undefined && lstStock[i].length == 0) {
		        		if (i == 0) {
		        			param += '?lstStock=' + lstStock[i];
		        			param1 += '?lstStockId=' + lstStock[i];
		        		} else {
		        			param += '&lstStock='+lstStock[i];
		        			param1 += '&lstStockId='+lstStock[i];
		        		}
		        	}
	        	}
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/stock-equip-stock-not-permission-stock/kendo-ui-combobox-ho-ex.json"
	                }
	            }
	        }
	        //,value: [$('#curShopId').val()]
	    });
		$("#groupEquip").kendoMultiSelect({
	        dataTextField: "code",
	        dataValueField: "id",
	        filter: "contains",
			itemTemplate: function (data, e, s, h, q) {
				return '<div node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.code) + ' - ' + Utils.XSSEncode(data.name)+'</span></div>';
			},
			tagTemplate: '#: data.code #',
	        change: function(e) {
	        	var lstGroupEquip = this.value();
	        	var param = '';
	        	var param1 = '';
	        	for (var i = 0, size = lstGroupEquip.length; i < size; i++) {
	        		if (lstGroupEquip[i] != null && lstGroupEquip[i] != undefined && lstGroupEquip[i].length == 0) {
		        		if (i == 0) {
		        			param += '?lstGroupEquip=' + lstGroupEquip[i];
		        			param1 += '?lstGroupEquipId=' + lstGroupEquip[i];
		        		} else {
		        			param += '&lstGroupEquip='+lstGroupEquip[i];
		        			param1 += '&lstGroupEquipId='+lstGroupEquip[i];
		        		}
		        	}
	        	}
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/stock-equip-group/kendo-ui-combobox-ho.json?arrStatus=0,1"
	                }
	            }
	        }
			//,value: [$('#curShopId').val()]
	    });
		$("#categoryEquip").kendoMultiSelect({
	        dataTextField: "code",
	        dataValueField: "id",
	        filter: "contains",
			itemTemplate: function (data, e, s, h, q) {
				return '<div node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.code) + ' - ' + Utils.XSSEncode(data.name)+'</span></div>';
			},
			tagTemplate: '#: data.code #',
	        change: function(e) {
	        	var lstCategoryEquip = this.value();
	        	var param = '';
	        	var param1 = '';
	        	for (var i = 0, size = lstCategoryEquip.length; i < size; i++) {
	        		if (lstCategoryEquip[i] != null && lstCategoryEquip[i] != undefined && lstCategoryEquip[i].length == 0) {
		        		if (i == 0) {
		        			param += '?lstCategoryEquip=' + lstCategoryEquip[i];
		        			param1 += '?lstCategoryEquipId=' + lstCategoryEquip[i];
		        		} else {
		        			param += '&lstCategoryEquip='+lstCategoryEquip[i];
		        			param1 += '&lstCategoryEquipId='+lstCategoryEquip[i];
		        		}
		        	}
	        	}
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/stock-equip-category/kendo-ui-combobox-ho.json?arrStatus=0,1"
	                }
	            }
	        }
			//,value: [$('#curShopId').val()]
	    });
	});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>


<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style" style="text-align:left;padding-left:40px;width:70px;">Đơn vị <span class="RequireStyle">(*)</span></label>
		</dt>
		<dd style="height:auto;">
			<!-- <input id="shop" style="width: 226px;" /> -->
			<div class="BoxSelect BoxSelect2">
			<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:216px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>
		</dd>

		<dt class="ClearLeft" style="margin-top:-20px;">
			<label class="LabelStyle Label1Style" style="padding-left:40px;width:70px;text-align:left;">Đến Ngày <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd style="margin-top:-20px;">
			<input type="text" id="tDate" class="InputTextStyle InputText2Style" value="<s:property value="yesterdayStr" />"/>
		</dd>
		
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="EquipmentReport.exportDS1_1();">Xuất báo cáo</button>
	</div>
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>

<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){	
	$("dt").css("width", "auto");
	setDateTimePicker('tDate');
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if (level == ShopDecentralizationSTT.VNM) {//VNM
				return '<div class="tree-vnm" node-id="' + Utils.XSSEncode(data.shopId) + '" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">' + Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName) + '</span></div>';
			} else if (level == ShopDecentralizationSTT.KENH) {
				return '<div class="tree-vnm" node-id="' + Utils.XSSEncode(data.shopId) + '" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">' + Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName) + '</span></div>';
			} else if (level == ShopDecentralizationSTT.MIEN) {
				return '<div class="tree-mien" node-id="' + Utils.XSSEncode(data.shopId) + '" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">' + Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName) + '</span></div>';
			} else if (level == ShopDecentralizationSTT.VUNG) {
				return '<div class="tree-vung" node-id="' + Utils.XSSEncode(data.shopId) + '" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">' + Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName) + '</span></div>';
			} else if (level == ShopDecentralizationSTT.NPP) {
				return '<div class="tree-npp" node-id="' + Utils.XSSEncode(data.shopId) + '" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">' + Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName) + '</span></div>';
			} else {
				return '<div class="tree-npp" node-id="' + Utils.XSSEncode(data.shopId) + '" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">' + Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName) + '</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop = this.value();
        	var param = '';
        	var param1 = '';
        	for (var i = 0, size = lstShop.length; i < size; i++) {
        		if (i == 0) {
        			param += '?lstShop=' + lstShop[i];
        			param1 += '?lstShopId=' + lstShop[i];
        		} else {
        			param += '&lstShop=' + lstShop[i];
        			param1 += '&lstShopId=' + lstShop[i];
        		}
        	}
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    //url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                    url: "/rest/report/shop/kendo-ui-combobox-ho-have-shop-off/1.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");
	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt><label class="LabelStyle Label1Style">Mã NPP (F9)<span class="ReqiureStyle">(*)</span></label></dt>
        <dd style="height:auto;">
        	<s:if test="NPP=='NPP'">
        		<input id="shop" style="width:216px; height:auto;" disabled="disabled" class="InputTextStyle InputText1Style">
        	</s:if>
        	<s:else>
        		<input id="shop" style="width:216px; height:auto;" class="InputTextStyle InputText1Style">
        	</s:else>
        </dd>
        
        <dt></dt>
		<dd>
			<input type="checkbox" id="chkIsStop" onclick="chkIsStopCheckEx(this);" />
			<span style="padding-left:5px;color:#215EA2;">Lấy NPP ngưng hoạt động</span>
		</dd>

		<dt class="ClearLeft" style="margin-top:-15px;">
			<label class="LabelStyle Label1Style">NVBH</label>
		</dt>
		<dd style="margin-top:-15px;">
			<input type="text" class="InputTextStyle InputText1Style" id="staffCode" maxlength="500"/>
		</dd>
		<dt style="margin-top:-15px;"><label class="LabelStyle Label1Style">Mã kiểm kê</label></dt>
	   	<dd style="margin-top:-15px;">
	   		<input type="text" class="InputTextStyle InputText1Style" id="statisticCode" maxlength="500"/>
	   	</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input type="text" id="fDate" class="InputTextStyle InputText2Style" />
		</dd>
		
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input type="text" id="tDate" class="InputTextStyle InputText2Style" />
		</dd>
        <dt></dt>
	    <dd></dd>
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnExport" class="BtnGeneralStyle" onclick="return EquipmentReport.exportKK1_3();">Xuất báo cáo</button>
	</div>	
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>

<s:hidden id="curShopId" name="shopId"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$('.MySelectBoxClass').customStyle();
	$('#promotionType').css('width', '225');
	$('.CustomStyleSelectBox').css('width', '192');
	$('.CustomStyleSelectBox').css('height', '21');
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.level;
			if (level == ShopDecentralizationSTT.VNM) {
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px;margin-left: -10px;"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if (level == ShopDecentralizationSTT.KENH) {
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if (level == ShopDecentralizationSTT.MIEN) {
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if (level == ShopDecentralizationSTT.VUNG) {
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if (level == ShopDecentralizationSTT.NPP) {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate: '#: data.shopCode #',
        change: function(e) {
        	var lstShop = this.value();
        	var param = '';
        	var param1 = '';
        	for (var i = 0, size = lstShop.length; i < size; i++) {
        		if (i == 0) {
        			param += '?lstShop=' + lstShop[i];
        			param1 += '?lstShopId=' + lstShop[i];
        		} else {
        			param += '&lstShop=' + lstShop[i];
        			param1 += '&lstShopId=' + lstShop[i];
        		}
        	}
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    //url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                    url: "/rest/report/shop/kendo-ui-combobox-ho-have-shop-off-ex/1.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });
	$('#staffCode').bind('keyup', function(event) {
		if(event.keyCode == keyCode_F9){
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			var lstShopId = '';
			if (dataShop != null && dataShop.length > 0) {
				lstShopId = dataShop[0].id;
				for (var i = 1; i < dataShop.length; i++) {
					lstShopId += "," + dataShop[i].id;
				}
			}
			VCommonJS.showDialogSearch2WithCheckbox({
				params : {
					arrIdStr: lstShopId,
					objectType : StaffRoleType.NVBHANDNVVS,
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Nhân viên'},
			        {id:'name', maxlength:250, label:'Tên Nhân viên'},
			    ],
			    chooseCallback : function(listObj) {
			        if (listObj != undefined && listObj != null && listObj.length > 0) {
			        	var staffCode = listObj[0].staffCode.trim();
			        	for (var i = 1; i < listObj.length; i++) {
			        		staffCode = staffCode.trim() + ',' + listObj[i].staffCode.trim();
			        	}
			        	$('#staffCode').val(staffCode.trim());
			        } else {
			        	$('#staffCode').val("");
			        }
			        $('#common-dialog-search-2-textbox').dialog("close");
			    },
			    url : '/commons/search-staff-in-permission',
			    columns : [[
			        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false},
			        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false},
			        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false}
			    ]]
			});			
		}
	});
	$('#displayProgramCode').bind('keyup', function(event) {
		if(event.keyCode == keyCode_F9){
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2WithCheckbox({
					params : {
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã CTTB'},
				        {id:'name', maxlength:250, label:'Tên CTTB'},
				    ],
				    chooseCallback : function(listObj) {
				        if (listObj != undefined && listObj != null && listObj.length > 0) {
				        	var code = listObj[0].code.trim();
				        	for (var i = 1; i < listObj.length; i++) {
				        		code = code.trim() + ',' + listObj[i].code.trim();
				        	}
				        	$('#displayProgramCode').val(code.trim());
				        } else {
				        	$('#displayProgramCode').val("");
				        }
				        $('#common-dialog-search-2-textbox').dialog("close");
				    },
				    url : '/commons/search-display-program-by-shop',
				    columns : [[
				        {field:'code', title:'Mã CTTB', align:'left', width: 110, sortable:false, resizable:false},
				        {field:'name', title:'Tên CTTB', align:'left', width: 200, sortable:false, resizable:false},
				        {field:'cb', checkbox:true, align:'center', width:80, sortable : false, resizable : false}
				    ]]
				}); 
			}
		}
	});
    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");
});
</script>
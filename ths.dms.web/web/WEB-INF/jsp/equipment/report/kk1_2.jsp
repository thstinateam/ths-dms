<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<style>
	#selectImageType .ui-dropdownchecklist-text,
	#selectImageType .ui-dropdownchecklist-selector {
    	width: 196px !important;
	}	
</style>

<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle">(*)</span></label>
	</dt>
	<dd style="height:auto;">
		<div class="BoxSelect BoxSelect2" style="margin-top:-3px">
			<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>	
	</dd>
	
	<dt class="ClearLeft" style="margin-top:-20px;">
		<label class="LabelStyle Label1Style">Nhân viên (F9)</label>
	</dt>
	<dd style="margin-top:-20px;">
		<input id="staffCode" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	
	<dt class="ClearLeft LabelTMStyle">
		<label  class="LabelStyle Label1Style">Ngày chụp từ<br />(dd/MM/yyyy)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	
<!-- 	<dd> -->
<%-- 		<input id="fromDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="fromDate"/>"/>  --%>
<!-- 	</dd> -->
        <dd><input id="fDate" style="width: 216px;" class="InputTextStyle InputText1Style" onchange="onTextboxchange();"> </dd>
        
	<dt class="LabelTMStyle">
		<label  class="LabelStyle Label1Style">Ngày chụp đến<br />(dd/MM/yyyy)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
<!-- 	<dd> -->
<%-- 		<input id="toDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="fromDate"/>"/>  --%>
<!-- 	</dd> -->
    <dd><input id="tDate" style="width: 216px;" class="InputTextStyle InputText1Style" onchange="onTextboxchange();"></dd>
	
	<dt class="ClearLeft LabelTMStyle"><label class="LabelStyle Label1Style"  style="padding: 10px 0 0;">Mã kiểm kê</label></dt>
	<dd style="height:auto;">		
		<div class="BoxSelect BoxSelect2" style="margin-top:-3px">
			<div class="Field2">
				<input id="cttb" type="text" data-placeholder="Chọn Mã kiểm kê"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>	
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="EquipmentReport.exportKK1_2();">Xuất Excel</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="shopId" name="currentShopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
	$(document).ready(function() {
		$('#fDate').datetimepicker({
	 		lang: 'vi',
	 		format: 'd/m/Y H:i'
		});
		$('#tDate').datetimepicker({
	 		lang: 'vi',
	 		format: 'd/m/Y H:i'
		});
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		var hour = currentTime.getHours();
		var minute = currentTime.getMinutes();
		if (month < 10) {
			month = '0' + month;
		}
		if (day < 10) {
			day = '0' + day;
		}
		if (hour < 10) {
			hour = '0' + hour;
		}
		if (minute < 10) {
			minute = '0' + minute;
		}
		$('#fDate').val(day + '/' + month + '/' + year + ' 00:00');
		$('#tDate').val(day + '/' + month + '/' + year + ' 23:59');
		
		$("#shop").kendoMultiSelect({
	        dataTextField: "shopCode",
	        dataValueField: "shopId",
	        filter: "contains",
			itemTemplate: function(data, e, s, h, q) {
				var level = data.isLevel;
				if (level == ShopDecentralizationSTT.VNM) {//VNM
					return '<div class="tree-vnm" node-id="' + Utils.XSSEncode(data.shopId) + '" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">' + Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName) + '</span></div>';
				} else if (level == ShopDecentralizationSTT.KENH) {
					return '<div class="tree-vnm" node-id="' + Utils.XSSEncode(data.shopId) + '" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">' + Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName) + '</span></div>';
				} else if (level == ShopDecentralizationSTT.MIEN) {
					return '<div class="tree-mien" node-id="' + Utils.XSSEncode(data.shopId) + '" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">' + Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName) + '</span></div>';
				} else if (level == ShopDecentralizationSTT.VUNG) {
					return '<div class="tree-vung" node-id="' + Utils.XSSEncode(data.shopId) + '" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">' + Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName) + '</span></div>';
				} else if (level == ShopDecentralizationSTT.NPP) {
					return '<div class="tree-npp" node-id="' + Utils.XSSEncode(data.shopId) + '" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">' + Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName) + '</span></div>';
				} else {
					return '<div class="tree-npp" node-id="' + Utils.XSSEncode(data.shopId) + '" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">' + Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName) + '</span></div>';
				}
			},
	        tagTemplate: '#: data.shopCode #',
	        change: function(e) {
	        	var lstShop=this.value();
	        	var param='';
	        	var param1='';
	        	for (var i=0; i < lstShop.length; i++) {
	        		if (i == 0) {
	        			param += '?lstShop=' + lstShop[i];
	        			param1 += '?lstShopId=' + lstShop[i];
	        		} else {
	        			param += '&lstShop=' + lstShop[i];
	        			param1 += '&lstShopId=' + lstShop[i];
	        		}
	        	}
	        	$('#staffCode').val('');
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
// 	                    url: "/rest/report/shop/kendo-ui-combobox-ho-have-shop-off/1.json"
	                }
	            }
	        },
	        value: [$('#curShopId').val()]
	    });

		$("#cttb").kendoMultiSelect({
	        dataTextField: "code",
	        dataValueField: "id",
	        filter: "contains",
			itemTemplate: function(data, e, s, h, q) {			
					return '<div class="tree-vnm" node-id="' + Utils.XSSEncode(data.id)+'" style="width:230px"><span class="tree-vnm-text">'+Utils.XSSEncode(data.code) +'</span></div>';			
			},
	        tagTemplate:  '#: data.code #',	       
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",	                  
	                    url: "/commons/equip/list-checking" + "?fromDate=" + $('#fDate').val().trim() + "&toDate=" + $('#tDate').val().trim()
	                }
	            }
	        }
	    });
		
	    var shopKendo = $("#shop").data("kendoMultiSelect");
	    shopKendo.wrapper.attr("id", "shop-wrapper");

		setTimeout(function() {
			$('.k-input').focus();
		}, 1000);
		var lstObjectType = StaffRoleType.GSKA + "," + StaffRoleType.GSMT + "," + StaffRoleType.GSKA + "," + StaffRoleType.NVBH ;
		$('#staffCode').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				var multiShop = $("#shop").data("kendoMultiSelect");
				var dataShop = multiShop.dataItems();
				var lstShopId = '';
				if (dataShop != null && dataShop.length > 0) {
					lstShopId = dataShop[0].id;
					for (var i = 1, size1 = dataShop.length; i < size1; i++) {
						lstShopId += "," + dataShop[i].id;
					}
				}				
				VCommonJS.showDialogSearch2WithCheckbox({
					params : {						
						lstObjectType: lstObjectType,
						arrIdStr: lstShopId					
					},
					inputs : [
				        {id:'code', maxlength:50, label: 'Mã Nhân viên'},
				        {id:'name', maxlength:250, label: 'Tên Nhân viên'},
				    ],
				    chooseCallback : function(listObj) {				        
				        if (listObj != undefined && listObj != null && listObj.length > 0) {
				        	var staffCode = listObj[0].staffCode.trim();
				        	for (var i = 1; i < listObj.length; i++) {
				        		staffCode = staffCode.trim() + ', ' + listObj[i].staffCode.trim();
				        	}
				        	$('#staffCode').val(staffCode.trim());
				        } else {
				        	$('#staffCode').val("");
				        }
				        $('#common-dialog-search-2-textbox').dialog("close");
				    },
				    url : '/commons/search-staff-show-list',
				    columns : [[
				        {field: 'staffCode', title: 'Mã Nhân viên', align: 'left', width: 110, sortable: false, resizable: false},
				        {field: 'staffName', title: 'Tên Nhân viên', align: 'left', width: 200, sortable: false, resizable: false},
				        {field: 'cb', checkbox: true, align: 'center', width: 80, sortable: false, resizable: false},
				    ]]
				});
			}
		});
		
	});
	onTextboxchange = function() {
		var kk = $("#cttb").data("kendoMultiSelect");
		kk.value([]);
		var dataSource = new kendo.data.DataSource({
		            transport: {
			                read: {
			                    dataType: "json",	                  
			                    url: "/commons/equip/list-checking" + "?fromDate=" + $('#fDate').val().trim() + "&toDate=" + $('#tDate').val().trim()
			                }
			            }
		        });
		kk.setDataSource(dataSource);
	};
</script>
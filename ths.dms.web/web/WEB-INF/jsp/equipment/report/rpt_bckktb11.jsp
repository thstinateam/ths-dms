<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Đơn vị <span class="ReqiureStyle">*</span></label>
		</dt>
		<dd>
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
		</s:if><s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
		</dd>
		
		<dt>
			<label class="LabelStyle Label1Style">Chương trình <span class="ReqiureStyle">*</span></label>
		</dt>
		<dd>
			<div class="BoxSelect BoxSelect12">
				<input  type="text" style="width:226px;" class="easyui-combobox" id="cbxProgramKK" maxlength="50" />
			</div>
            <!-- <div class="BoxSelect BoxSelect1" id="cat"> 
            	<select id="category" class="MySelectBoxClass" >  
           			 <option value="0">Chọn chương trình</option>
         	   </select> 
            </div> -->
		</dd>

		<%-- <dt class="ClearLeft"><label class="LabelStyle Label1Style">Từ ngày <span class="ReqiureStyle"> *</span></label></dt>
		<dd><input id="fDate" class="InputTextStyle InputText2Style" value="<s:property value='date' />"></dd>
		
		<dt><label class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle"></span></label></dt>
		<dd><input id="tDate" class="InputTextStyle InputText2Style" value="<s:property value='date' />"></dd> --%>
	</dl>
	
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return EquipmentReport.exportKKTB11();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="curKKId"></s:hidden> <!-- key shop Id hien tai select-->
<script type="text/javascript">
$(document).ready(function(){	
	$('.MySelectBoxClass').customStyle();
	ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId) {
		var param = {};
		param.shopId = $('#shopId').val().trim();
		Utils.initProgramStatistic('cbxProgramKK', '/commons/loadProgramStatistic', param, 230, function (rec) {
			if (rec != null && rec.id != null && rec.id != activeType.DELETE) {
				/* $('#fDate').val(rec.fromDate);
				$('#tDate').val(rec.toDate); */
	    	}
	    	$('#curKKId').val(rec.id);
		});
	});
	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<style>
	#selectImageType .ui-dropdownchecklist-text,
	#selectImageType .ui-dropdownchecklist-selector {
    	width: 196px !important;
	}	
</style>

<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle"> *</span></label>
	</dt>
	<dd >
		<!-- <input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:226px;" class="InputTextStyle InputText1Style" /> -->
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" data-placeholder="Chọn đơn vị" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
		</s:if><s:else>
			<input type="text" id="shop" data-placeholder="Chọn đơn vị" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
	</dd>		
	<!-- <div class="Clear"></div> -->		
	<dt class="ClearLeft">
		<label  class="LabelStyle Label1Style">Từ ngày<span class="ReqiureStyle"> *</span></label>
	</dt>
	<dd>
		<input id="fDate" type="text" class="InputTextStyle InputText2Style" value='<s:property value="fromDate" />' maxlength="10" /> 
	</dd>
	<dt>
		<label  class="LabelStyle Label1Style">Đến ngày<span class="ReqiureStyle"> *</span></label>
	</dt>
	<dd>
		<input id="tDate" type="text" class="InputTextStyle InputText2Style" value='<s:property value="toDate" />' maxlength="10" /> 
	</dd>
	<dt class="ClearLeft LabelTMStyle" style="margin-top: 2px;">
		<label class="LabelStyle Label1Style">Loại thiết bị<span class="ReqiureStyle"> *</span><br /></label>
	</dt>
	<dd style="height: auto; margin-top:-3px;">
		<input id="categoryEquip" type="text" data-placeholder="Tất cả loại thiết bị" style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="LabelTMStyle" style="margin-top: 2px;">
		<label class="LabelStyle Label1Style" >Nhóm thiết bị<span class="ReqiureStyle"> *</span><br /></label>
	</dt>
	<dd style="height:auto;">		
		<div class="BoxSelect BoxSelect2"  id="divEquipGroup" style="margin-top:-3px">
			<div class="Field2">
				<input id="equipGroup" type="text" data-placeholder="Tất cả nhóm thiết bị"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>	
	</dd>
	<!-- <dt>&nbsp;</dt>
	<dd>&nbsp;</dd> -->
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="EquipmentReport.exportEQ_3_1_1_8();">Xuất báo cáo</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
	$(document).ready( function() {
		$('.MySelectBoxClass').customStyle();
		$('#promotionType').css('width', '225');
		$('.CustomStyleSelectBox').css('width','192');
		$('.CustomStyleSelectBox').css('height','21');
		setDateTimePicker('fDate');
		setDateTimePicker('tDate');
		ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId) {
			/* var param = {};
			param.shopId = $('#shopId').val().trim();
			Utils.initProgramStatistic('cbxProgramKK', '/commons/loadProgramStatistic', param, 230, function (rec) {
				if (rec != null && rec.id != null && rec.id != activeType.DELETE) {
// 					$('#fDate').val(rec.fromDate);
// 					$('#tDate').val(rec.toDate); 
		    	}
		    	$('#curKKId').val(rec.id);
			}); */
		});
	    $("#equipGroup").kendoMultiSelect({
	        dataTextField: "code",
	        dataValueField: "id",
	        filter: "contains",
			itemTemplate: function (data, e, s, h, q) {
				return '<div node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.code) + ' - ' + Utils.XSSEncode(data.name)+'</span></div>';
			},
			tagTemplate: '#: data.code #',
	        change: function(e) {
	        	var lstGroupEquip = this.value();
	        	var param = '';
	        	var param1 = '';
	        	for (var i = 0, size = lstGroupEquip.length; i < size; i++) {
	        		if (lstGroupEquip[i] != null && lstGroupEquip[i] != undefined && lstGroupEquip[i].length == 0) {
		        		if (i == 0) {
		        			param += '?lstGroupEquip=' + lstGroupEquip[i];
		        			param1 += '?lstGroupEquipId=' + lstGroupEquip[i];
		        		} else {
		        			param += '&lstGroupEquip='+lstGroupEquip[i];
		        			param1 += '&lstGroupEquipId='+lstGroupEquip[i];
		        		}
		        	}
	        	}
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/stock-equip-group/kendo-ui-combobox-ho.json?arrStatus=0,1"
	                }
	            }
	        }
			//,value: [$('#curShopId').val()]
	    });
		$("#categoryEquip").kendoMultiSelect({
	        dataTextField: "code",
	        dataValueField: "id",
	        filter: "contains",
			itemTemplate: function (data, e, s, h, q) {
				return '<div node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.code) + ' - ' + Utils.XSSEncode(data.name)+'</span></div>';
			},
			tagTemplate: '#: data.code #',
	        change: function(e) {
	        	var lstCategoryEquip = this.value();
	        	var param = '';
	        	var param1 = '';
	        	for (var i = 0, size = lstCategoryEquip.length; i < size; i++) {
	        		if (lstCategoryEquip[i] != null && lstCategoryEquip[i] != undefined && lstCategoryEquip[i].length == 0) {
		        		if (i == 0) {
		        			param += '?lstCategoryEquip=' + lstCategoryEquip[i];
		        			param1 += '?lstCategoryEquipId=' + lstCategoryEquip[i];
		        		} else {
		        			param += '&lstCategoryEquip='+lstCategoryEquip[i];
		        			param1 += '&lstCategoryEquipId='+lstCategoryEquip[i];
		        		}
		        	}
	        	}
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/stock-equip-category/kendo-ui-combobox-ho.json?arrStatus=0,1"
	                }
	            }
	        }
			//,value: [$('#curShopId').val()]
	    });
	});
</script>
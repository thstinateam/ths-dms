<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle">(*)</span></label>
	</dt>
	<dd style="height:auto;">
		<div class="BoxSelect BoxSelect2" style="margin-top:-3px">
			<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>	
	</dd>		
	<dt class="ClearLeft LabelTMStyle" style="margin-top:-20px;">
		<label  class="LabelStyle Label1Style">Năm
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd style="margin-top:-20px;">
		<input id="year1"  maxlength="4" type="text" class="InputTextStyle InputText2Style" style="width:218px;"/> 
	</dd>
	<dt class="LabelTMStyle" style="margin-top:-20px;">
		<label  class="LabelStyle Label1Style">Từ kỳ
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd style="margin-top:-20px;">
		<div class="BoxSelect BoxSelect1">							
			<select class="MySelectBoxClass" id="fromPeriod">								
				
			</select>
		</div>	
	</dd>
	
	
	<dt class="ClearLeft LabelTMStyle" >
		<label  class="LabelStyle Label1Style">Năm
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd style="margin-top:-5px;">
		<input id="year2"  maxlength="4" type="text" class="InputTextStyle InputText2Style" style="width:218px;"/> 
	</dd>
	<dt class="LabelTMStyle" style="margin-top:-5px;">
		<label  class="LabelStyle Label1Style">Đến kỳ
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd style="margin-top:-5px;">
		<div class="BoxSelect BoxSelect1">							
			<select class="MySelectBoxClass" id="toPeriod">								
				
			</select>
		</div>	
	</dd>
	<dt class="ClearLeft LabelTMStyle" style="margin-top: 2px;">
		<label class="LabelStyle Label1Style">Loại thiết bị<span class="ReqiureStyle">(*)</span><br /></label>
	</dt>
	<dd style="height: auto; margin-top:-3px;">
		<input id="categoryEquip" type="text" data-placeholder="Tất cả loại thiết bị" style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="LabelTMStyle" style="margin-top:2px;">
		<label class="LabelStyle Label1Style" >Nhóm thiết bị<span class="ReqiureStyle">(*)</span><br /></label>
	</dt>
	<dd style="height:auto;">		
		<div class="BoxSelect BoxSelect2"  id="divEquipGroup" style="margin-top:-3px">
			<div class="Field2">
				<input id="equipGroup" type="text" data-placeholder="Tất cả nhóm thiết bị"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>	
	</dd>
    <dt class="ClearLeft LabelTMStyle" style="margin-top:-20px;"><label class="LabelStyle Label1Style">Kiểu File</label></dt>
        <dd style="margin-top:-20px;">
           <!-- <input type="radio" name="formatType" id="formatTypePDF" value="PDF" ><label for="formatTypePdf">PDF</label> -->
           <input type="radio" name="formatType" id="formatTypeExcel" value="XLS" checked="checked"><label for="formatTypeExcel">XLS</label>
                      <input type="radio" name="formatType" id="formatTypeExcel" value="PDF"><label for="formatTypeExcel">PDF</label>
           
        </dd>
	<dt>&nbsp;</dt>
	<dd>&nbsp;</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="EquipmentReport.exportEQ_3_1_1_4();">Xuất báo cáo</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="shopId" name="currentShopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
	$(document).ready( function() {
		$('.MySelectBoxClass').customStyle();
		$('.CustomStyleSelectBox').css('width','193');
		$('.CustomStyleSelectBox').css('height','21');
		var date = new Date();
		var y = date.getFullYear();
		$('#year1').val(y);
		$('#year2').val(y);
		 $("#shop").kendoMultiSelect({
		        dataTextField: "shopCode",
		        dataValueField: "shopId",
		        filter: "contains",
				itemTemplate: function(data, e, s, h, q) {
					var level = data.isLevel;
					if(level == ShopDecentralizationSTT.VNM) {//VNM
						return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
					} else if(level == ShopDecentralizationSTT.KENH) {
							return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
					}else if(level == ShopDecentralizationSTT.MIEN){
						return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
					}else if(level == ShopDecentralizationSTT.VUNG){
						return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
					}else if(level == ShopDecentralizationSTT.NPP){
						return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
					} else {
						return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
					}
				},
				tagTemplate:  '#: data.shopCode #',
		        change: function(e) {
		        	var lstShop=this.value();
		        	var param='';
		        	var param1='';
		        	for(var i=0;i<lstShop.length;i++){
		        		if(lstShop[i] != null && lstShop[i] != ""){
			        		if(i==0){
			        			param+='?lstShop='+lstShop[i];
			        			param1+='?lstShopId='+lstShop[i];
			        		}else{
			        			param+='&lstShop='+lstShop[i];
			        			param1+='&lstShopId='+lstShop[i];
			        		}
			        	}
		        	}
		        	if(param != "" && param1 != ""){
						$('#divEquipGroup').html('<input id="equipGroup" type="text" data-placeholder="Tất cả"  style="width:218px;height: auto;margin-left:0px" class="InputTextStyle InputText1Style" />');
						EquipmentReport.loadComboEquipGroup("/report/equipment/getListCustormeType"+param1);
			        }
		        },
		        dataSource: {
		            transport: {
		                read: {
		                    dataType: "json",
		                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
		                }
		            }
		        },
		        value: [$('#curShopId').val()]
		    });
	    var shopKendo = $("#shop").data("kendoMultiSelect");
	    shopKendo.wrapper.attr("id", "shop-wrapper");
	    // EquipmentReport.loadComboEquipGroup("/report/equipment/getListCustormeType");
	    $("#equipGroup").kendoMultiSelect({
	        dataTextField: "code",
	        dataValueField: "id",
	        filter: "contains",
			itemTemplate: function (data, e, s, h, q) {
				return '<div node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.code) + ' - ' + Utils.XSSEncode(data.name)+'</span></div>';
			},
			tagTemplate: '#: data.code #',
	        change: function(e) {
	        	var lstGroupEquip = this.value();
	        	var param = '';
	        	var param1 = '';
	        	for (var i = 0, size = lstGroupEquip.length; i < size; i++) {
	        		if (lstGroupEquip[i] != null && lstGroupEquip[i] != undefined && lstGroupEquip[i].length == 0) {
		        		if (i == 0) {
		        			param += '?lstGroupEquip=' + lstGroupEquip[i];
		        			param1 += '?lstGroupEquipId=' + lstGroupEquip[i];
		        		} else {
		        			param += '&lstGroupEquip='+lstGroupEquip[i];
		        			param1 += '&lstGroupEquipId='+lstGroupEquip[i];
		        		}
		        	}
	        	}
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/stock-equip-group/kendo-ui-combobox-ho.json?arrStatus=0,1"
	                }
	            }
	        }
			//,value: [$('#curShopId').val()]
	    });
	    $(window).bind('keypress',function(event){
			if($('.fancybox-inner').length!=0){
				if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
					Images.showImageNextPre(0);
				}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
					Images.showImageNextPre(1);
				}
				return false;
			}
			return true;
		});		
	    
		// setTimeout(function() {
		// 	$('.k-input').focus();
		// }, 1000);		
		
		
		$('#year1').bind('change',function(){
			var year = $('#year1').val().trim();
			var msg = Utils.getMessageOfRequireCheck('year1', 'Năm');
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidateEx('year1', 'Năm',Utils._TF_NUMBER);
			}
			if(msg.length >0){
				$('#errMsg').html(msg).show();
				changeValuePeriodCombobox(null,"fromPeriod");
				return false;
			}
			
			var params = new Object();
			params.year = year;
			Utils.getJSONDataByAjaxNotOverlay(params, '/report/equipment/getPeriod', function(data){						
				changeValuePeriodCombobox(data,"fromPeriod");
			});		
		}).change();
		
		$('#year2').bind('change',function(){
			var year = $('#year2').val().trim();
			var msg = Utils.getMessageOfRequireCheck('year2', 'Năm');
			if (msg.length == 0) {
				msg = Utils.getMessageOfSpecialCharactersValidateEx('year2', 'Năm',Utils._TF_NUMBER);
			}
			if(msg.length >0){
				$('#errMsg').html(msg).show();
				changeValuePeriodCombobox(null,"toPeriod");
				return false;
			}
			var params = new Object();
			params.year = year;
			Utils.getJSONDataByAjaxNotOverlay(params, '/report/equipment/getPeriod', function(data){						
				changeValuePeriodCombobox(data,"toPeriod");
			});		
		}).change();

		$("#categoryEquip").kendoMultiSelect({
	        dataTextField: "code",
	        dataValueField: "id",
	        filter: "contains",
			itemTemplate: function (data, e, s, h, q) {
				return '<div node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span><div style="width: 15px; display: inline-block;"></div></span><span>'+Utils.XSSEncode(data.code) + ' - ' + Utils.XSSEncode(data.name)+'</span></div>';
			},
			tagTemplate: '#: data.code #',
	        change: function(e) {
	        	var lstCategoryEquip = this.value();
	        	var param = '';
	        	var param1 = '';
	        	for (var i = 0, size = lstCategoryEquip.length; i < size; i++) {
	        		if (lstCategoryEquip[i] != null && lstCategoryEquip[i] != undefined && lstCategoryEquip[i].length == 0) {
		        		if (i == 0) {
		        			param += '?lstCategoryEquip=' + lstCategoryEquip[i];
		        			param1 += '?lstCategoryEquipId=' + lstCategoryEquip[i];
		        		} else {
		        			param += '&lstCategoryEquip='+lstCategoryEquip[i];
		        			param1 += '&lstCategoryEquipId='+lstCategoryEquip[i];
		        		}
		        	}
	        	}
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/stock-equip-category/kendo-ui-combobox-ho.json?arrStatus=0,1"
	                }
	            }
	        }
			//,value: [$('#curShopId').val()]
	    });
		
		function changeValuePeriodCombobox(data,id){
			if (data != undefined && data != null && data.rows != undefined && data.rows.length != 0) {
			    var html = "";	
			    var CHOSE = data.rows[0].id;
				for(var i = 0,size = data.rows.length; i < size; i++){
					html += ('<option value="'+data.rows[i].id+'">'+(data.rows[i].name)+'</option>');
					if(data.rows[i].status==2 && data.rows[0].id==CHOSE) {//always set firstValue when have alot of period_status=2
						CHOSE = data.rows[i].id;
					}
					if(data.equipPeriodCurrent.id==data.rows[i].id ){
						CHOSE = data.rows[i].id;
					}
				}
				
				$('#'+id).html(html);
				$('#'+id).val(CHOSE).change();
			}else{
				var html =  ('<option value="-1" selected="selected"></option>'); 
				$('#'+id).html(html);
				$('#'+id).val('-1').change();	
			}
		}
		
	});
</script>
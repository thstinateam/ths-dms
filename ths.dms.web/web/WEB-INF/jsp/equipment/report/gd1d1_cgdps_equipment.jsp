<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<style>
	#selectImageType .ui-dropdownchecklist-text,
	#selectImageType .ui-dropdownchecklist-selector {
    	width: 196px !important;
	}	
</style>
<dl class="Dl3Style">
	<dt class="ClearLeft LabelTMStyle" style="margin:0">
		<label class="LabelStyle Label1Style">Đơn vị </label>
	</dt>
	<dd style="height: auto; margin:0">
		<div class="BoxSelect BoxSelect2" style="margin-top:-3px">
			<div class="Field2">
				<input id="shop" type="text" data-placeholder="Tất cả"  style="width:215px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>	
	</dd>
	<dt class="LabelTMStyle" style="margin:0">
		<label class="LabelStyle Label1Style" >Loại</label>
	</dt>
	<dd style="height: auto; margin:0">
		<div class="BoxSelect BoxSelect2">							
			<select class="MySelectBoxClass" id="typeGG">
				<option value="-2">Tất cả</option>
				<option value="3">Báo mất</option>
				<option value="1">Chuyển kho</option>
				<option value="11">Đề nghị mượn thiết bị</option>
				<option value="12">Đề nghị thu hồi thiết bị</option>
				<option value="0">Giao nhận và hợp đồng</option>
				<option value="10">Nhập kho điều chỉnh</option>
				<option value="2">Sửa chữa</option>
				<option value="5">Thu hồi và thanh lý</option>
				<option value="6">Thanh lý tài sản</option>	
			</select>
		</div>	
	</dd>
	<dt class="ClearLeft LabelTMStyle" style="margin: 0;">
		<label class="LabelStyle Label1Style">Từ ngày<span class="ReqiureStyle">(*)</span></label>
	</dt>
	<dd style="height: auto; margin:0">
		<input id="fromDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="reportDate"/>"/> 
	</dd>
	<dt class="LabelTMStyle" style="margin: 0;">
		<label  class="LabelStyle Label1Style">Đến ngày<span class="ReqiureStyle">(*)</span></label>
	</dt>
	<dd style="height: auto; margin:0">
		<input id="toDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="reportDate"/>"/> 
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" id = "btnReportXNT1D1" onclick="EquipmentReport.reportGD1D1BCCGDPS();">Xuất báo cáo</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
	$(document).ready( function() {
		$('.MySelectBoxClass').customStyle();
		$('#typeGG').css('width', '225');
		$('.CustomStyleSelectBox').css('width','192');
		$('.CustomStyleSelectBox').css('height','21');
		setDateTimePicker('fromDate');
		setDateTimePicker('toDate');
		$("#shop").kendoMultiSelect({
	        dataTextField: "shopCode",
	        dataValueField: "shopId",
	        filter: "contains",
			itemTemplate: function(data, e, s, h, q) {
				var level = data.level;
				if (level == ShopDecentralizationSTT.VNM) {
					return '<div class="tree-vnm" node-id="' + Utils.XSSEncode(data.shopId)+'" style="width:230px;margin-left: -10px;"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else if (level == ShopDecentralizationSTT.KENH) {
						return '<div class="tree-vnm" node-id="'+ Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else if (level == ShopDecentralizationSTT.MIEN) {
					return '<div class="tree-mien" node-id="' + Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else if (level == ShopDecentralizationSTT.VUNG) {
					return '<div class="tree-vung" node-id="' + Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else if (level == ShopDecentralizationSTT.NPP) {
					return '<div class="tree-npp" node-id="' + Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else {
					return '<div class="tree-npp" node-id="' + Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
	        tagTemplate:  '#: data.shopCode #',
	        change: function(e) {},
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    // url: "/rest/report/shop/kendo-ui-combobox-ho-have-shop-off/1.json"
	                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
	                }
	            }
	        },
	        value: [$('#curShopId').val()]
	    });
	    var shopKendo = $("#shop").data("kendoMultiSelect");
	    shopKendo.wrapper.attr("id", "shop-wrapper");
	});
</script>
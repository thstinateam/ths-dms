<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Thiết bị</a>
		</li>
		<li><span>Lập đề nghị thu thiết bị</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
<!-- 				<h2 class="Title2Style">Thông tin tìm kiếm</h2> -->
				<div class="SearchInSection SProduct1Form" style="padding-left: 0px;">
				<div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin tìm kiếm" style="width:1000px;height:120px;padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Mã biên bản</label>
					<input id="lendSuggestCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="4"/>
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 60px;height:auto;">Trạng thái biên bản</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="status" tabindex="9">
							<option value="-1" selected="selected">Tất cả</option>
							<option value="0" >Dự thảo</option>
							<option value="2" >Đã duyệt</option>
							<option value="4" >Hủy</option>
						</select>
					</div>
					<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>			
				</div>		
				</div>	
				<!-- danh sach bien ban giao nhan -->	
				<h2 class="Title2Style" style="height:15px;">
					<span style="float:left;">Danh sách đề nghị</span>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table class="easyui-datagrid" id="grid">
							<thead>
							<tr>
								<th data-options="field:'delete',width:35,
									title: equipCreateLendSuggest.datagridOption.column0.title,
									formatter: equipCreateLendSuggest.datagridOption.column0.formatter"rowspan="2">
									<a href="javascript:void(0);" onclick="EquipmentManageCatalog.insertEquipmentGroupInGrid();"><img src="/resources/images/icon_add.png" title="Thêm"/></a>
								</th>
								<th data-options="field:'mien',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Miền</th>
								<th data-options="field:'kenh',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Kênh</th>
								<th data-options="field:'NPP',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">NPP</th>
								<th data-options="field:'maKhachHang',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Mã KH (F9)</th>
								<th data-options="field:'tenKhachHang',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Tên khách hàng</th>
								<th data-options="field:'maThietBi',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalCombobox.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalCombobox.editor" rowspan="2">Mã thiết bị (F9)</th>
								<th data-options="field:'soSeri',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Số serial</th>
								<th data-options="field:'thoiGiang',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalDatebox.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalDatebox.editor" rowspan="2">Thời gian đề nghị thu hồi</th>
								<th data-options="field:'nguoiDungTen',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Người đứng tên mượn thiết bị</th>
								<th data-options="field:'dienThoai',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Điện thoại</th>
								<th data-options="field:null,width:100," colspan="5">Địa chỉ đặt tủ</th>	
								<th data-options="field:'soLuong',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Số lượng</th>
								<th data-options="field:'nhomThietBi',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Nhóm thiết bị</th>
								<th data-options="field:'tinhTrang',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Trình trạng thiết bị</th>
								<th data-options="field:'giamSat',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Giám sát</th>
								<th data-options="field:'dtGiamSat',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor" rowspan="2">Điện thoại</th>
							</tr>
							<tr>
								<!-- <th data-options="field:'dienThoai',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column7.formatter,
									editor: equipCreateLendSuggest.datagridOption.column7.editor">Điện thoại</th> -->
								<th data-options="field:'soNha',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor">Số nhà</th>
								<th data-options="field:'tinh',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor">Tỉnh/TP</th>
								<th data-options="field:'huyen',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor">Quận/Huyện</th>
								<th data-options="field:'xa',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor">Phường/Xã</th>
								<th data-options="field:'duong',width:100,
									formatter: equipCreateLendSuggest.datagridOption.columnNormalText.formatter,
									editor: equipCreateLendSuggest.datagridOption.columnNormalText.editor">Tên đường/Phố/Thôn/Ấp</th>
								
							</tr>

							</thead>
						</table>
					</div>
				</div>
				<div class="Clear"></div>
				<div class="SearchInSection SProduct1Form">                                
               <div class="BtnCenterSection" style="padding-bottom: 10px;">
				       <button id="btnCapNhat" class="BtnGeneralStyle" onclick="return EquipmentManageCatalog.saveEquipGroup();">Cập nhật</button> &nbsp;&nbsp;
				       <button class="BtnGeneralStyle" onclick="window.location.href='/equipment-group-manage/info'">Bỏ qua</button>
	           </div>
		    </div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {	
	$('#panelDeliveryRecord').width($('#panelDeliveryRecord').parent().width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 5px;");
	},1000);
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar("fDate");
	ReportUtils.setCurrentDateForCalendar("tDate");	
	$('#shopCode').focus();
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

 //    var shopKendo = $("#shop").data("kendoMultiSelect");
 //    shopKendo.wrapper.attr("id", "shop-wrapper"); 
	// $('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_import_bien_ban_thu_hoi_thanh_ly.xlsx');
	var params = new Object();
	EquipmentManageCatalog._mapEviction = new Map();
	EquipmentManageCatalog._lstRowID = new Array();
	params.fromDate = $('#fDate').val();
	params.toDate = $('#tDate').val();	

	$('#grid').datagrid({
		url:'/equipCreateCollectionSuggest/search',
		method : 'GET',
		height: 'auto',
		width : $(window).width() - 35,
		scrollbarSize:0,
		rownumbers:true,
		//fitColumns:true,
		autoRowHeight:true, 
		pagination:true, 
		pageSize:10,
		queryParams:params,
		pageList  : [20],
// 		width: $('#gridContainer').width(),
		onSelect: function(i) {
			// if (EquipmentManageCatalog._editIndex != i) {
			// 	if (EquipmentManageCatalog.endEditing()) {
			 $('#grid').datagrid('beginEdit', i);
					// EquipmentManageCatalog._editIndex = i;
					// $(".datagrid-editable-input.numberbox-f").attr("maxlength", "3");
					// $("#gridContainer td[field=amount] .datagrid-editable-input.numberbox-f").attr("maxlength", "17");
					// var edts = $("#grid").datagrid("getEditors", EquipmentManageCatalog._editIndex);
					// $(edts[2].target).combobox("loadData", [{customerType:"0", customerTypeName: "Chưa xác định"},
					//                                         {customerType:"1", customerTypeName: "Thành thị"}, {customerType:"2", customerTypeName: "Nông thôn"}]);
				// }
			// }
		},	
		onUnselect:function(i){
			$('#grid').datagrid('endEdit', i);
		},
		onBeforeLoad:function(param){											 
		},
		onLoadSuccess :function(data){  	 		    	
	    	$(window).resize();
	    	if (data == null || data.total == 0) {
				
			}else{
				$('.datagrid-header-rownumber').html('STT');		    	
			}		
	    	$('.datagrid-header-row td div').css('text-align','center');
	    	updateRownumWidthForDataGrid('');
			var rows = $('#grid').datagrid('getRows');
	    	$('.datagrid-header-check input').removeAttr('checked');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(EquipmentManageCatalog._mapEviction.get(rows[i].id)!=null) {
	    			$('#grid').datagrid('checkRow', i);
	    		}
	    	}
    		 $(window).resize();
		},
		// onCheck:function(index,row){
		// 	EquipmentManageCatalog._mapEviction.put(row.id,row);  
		// 	if(row.trangThaiBienBan == "Đã duyệt"){
		// 		EquipmentManageCatalog._lstRowID.push(row.id);
		// 	}
			
	 //    },
	 //    onUncheck:function(index,row){
	 //    	EquipmentManageCatalog._mapEviction.remove(row.id);
		// 	var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
	 //    	if(position != null){
	 //    		EquipmentManageCatalog._lstRowID.splice(position,1);
	 //    	}
	 //    },
	 //    onCheckAll:function(rows){
		//  	for(var i = 0;i<rows.length;i++){
	 //    		var row = rows[i];
	 //    		EquipmentManageCatalog._mapEviction.put(row.id,row);
	 //    		var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
		// 		if(position == -1 && row.trangThaiBienBan == "Đã duyệt"){
		// 			EquipmentManageCatalog._lstRowID.push(row.id);
		// 		}
	 //    	}
	 //    },
	 //    onUncheckAll:function(rows){
	 //    	for(var i = 0;i<rows.length;i++){
	 //    		var row = rows[i];
	 //    		EquipmentManageCatalog._mapEviction.remove(row.id);	
	 //    		var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
		// 		if(position != null){
		// 			EquipmentManageCatalog._lstRowID.splice(position,1);
		// 		}
	 //    	}
	 //    }

	});
	
});

function changeCombo() {
    var status = $('#statusRecordLabel').val().trim();
    var html = '';
    if(status == '2'||status == '-1'){
    	html = 
    	'<option value="0" selected="selected">Chọn</option> <option value="1" >Đã gửi</option> <option value="2" >Đã nhận</option> ';
    }else{
    	html = '<option value="0" selected="selected">Chọn</option>';
    }
    $('#statusDeliveryLabel').html(html);
    $('#statusDeliveryLabel').change();
};
var equipCreateLendSuggest = {
	deleteRow: function(i){
		$('#grid').datagrid("deleteRow", i);
	},
	datagridOption: {
		column0: {
			title: '<a href="javascript:void(0);" onclick="EquipmentManageCatalog.insertEquipmentGroupInGrid();"><img src="/resources/images/icon_add.png" title="Thêm"/></a>',
			formatter: function (value,row,index) {
				return '<a href="javascript:void(0);" onclick="equipCreateLendSuggest.deleteRow('+index+');"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
			}
		},
		columnNormalText: {
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		columnNormalCombobox: {
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox'
			}
		},
		columnNormalDatebox: {
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'datebox'
			}
		}
	}
}
</script>
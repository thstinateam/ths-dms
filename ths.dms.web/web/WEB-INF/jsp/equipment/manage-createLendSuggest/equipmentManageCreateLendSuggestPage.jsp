<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Thiết bị</a>
		</li>
		<li><span>Lập đề nghị mượn thiết bị</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
<!-- 				<h2 class="Title2Style">Thông tin tìm kiếm</h2> -->
				<div class="SearchInSection SProduct1Form" style="padding-left: 0px;">
				<div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin tìm kiếm" style="width:1000px;height:120px;padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Mã biên bản</label>
					<input id="lendSuggestCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="4"/>
					<label class="LabelStyle Label1Style">Mã giám sát</label>
					<input id="superviseCode" type="text" class="InputTextStyle InputText1Style" maxlength="100" tabindex="5"/>
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 60px;height:auto;">Trạng thái biên bản</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="status" tabindex="9">
							<option value="-1" selected="selected">Tất cả</option>
							<option value="0" >Dự thảo</option>
							<option value="2" >Đã duyệt</option>
							<option value="4" >Hủy</option>
						</select>
					</div>
					<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>			
				</div>		
				</div>	
				<!-- danh sach bien ban giao nhan -->	
				<h2 class="Title2Style" style="height:15px;">
					<span style="float:left;">Danh sách đề nghị</span>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table class="easyui-datagrid" id="grid">
							<thead>
							<tr>
								<th data-options="field:'delete',width:35,
									title: equipCreateLendSuggest.datagridOption.column0.title,
									formatter: equipCreateLendSuggest.datagridOption.column0.formatter"rowspan="2">
									<a href="javascript:void(0);" onclick="EquipmentManageCatalog.insertEquipmentGroupInGrid();"><img src="/resources/images/icon_add.png" title="Thêm"/></a>
								</th>
								<th data-options="field:'loaiThietBi',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column1.formatter,
									editor: equipCreateLendSuggest.datagridOption.column1.editor" rowspan="2">Loại thiết bị</th>
								<th data-options="field:'dungTich',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column2.formatter,
									editor: equipCreateLendSuggest.datagridOption.column2.editor" rowspan="2">Dung tích (lít)</th>
								<th data-options="field:'khachHang',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column3.formatter,
									editor: equipCreateLendSuggest.datagridOption.column3.editor" rowspan="2">Khách hàng (F9)</th>
								<th data-options="field:'tenKhachHang',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column4.formatter,
									editor: equipCreateLendSuggest.datagridOption.column4.editor" rowspan="2">Tên khách hàng</th>
								<th data-options="field:'ngDungTenMuonTu',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column5.formatter,
									editor: equipCreateLendSuggest.datagridOption.column5.editor" rowspan="2">Người đứng tên mượn tủ</th>
								<th data-options="field:'quanHe',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column6.formatter,
									editor: equipCreateLendSuggest.datagridOption.column6.editor" rowspan="2">Quan hệ người đứng tên mượn tủ với chủ cửa hàng/Doanh nghiệp</th>
								<th data-options="field:null,width:100," colspan="6">Địa chỉ đặt tủ</th>
								<th data-options="field:'soCMND',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column13.formatter,
									editor: equipCreateLendSuggest.datagridOption.column13.editor" rowspan="2">Số CMND</th>
								<th data-options="field:'noiCapCMND',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column14.formatter,
									editor: equipCreateLendSuggest.datagridOption.column14.editor" rowspan="2">Nơi cấp CMND</th>
								<th data-options="field:'ngayCapCMND',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column15.formatter,
									editor: equipCreateLendSuggest.datagridOption.column15.editor" rowspan="2">Ngày cấp CMND</th>
								<th data-options="field:'giayPhepDKKD',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column16.formatter,
									editor: equipCreateLendSuggest.datagridOption.column16.editor" rowspan="2">Số giấy phép ĐKKD</th>
								<th data-options="field:'noiCapDKKD',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column17.formatter,
									editor: equipCreateLendSuggest.datagridOption.column17.editor" rowspan="2">Nơi cấp ĐKKD</th>
								<th data-options="field:'ngayCapDKKD',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column18.formatter,
									editor: equipCreateLendSuggest.datagridOption.column18.editor" rowspan="2">Ngày cấp ĐKKD</th>
								<th data-options="field:'thoiGianNhanTu',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column19.formatter,
									editor: equipCreateLendSuggest.datagridOption.column19.editor" rowspan="2">Thời gian mong muốn nhận tủ</th>
								<th data-options="field:'tinhTrang',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column20.formatter,
									editor: equipCreateLendSuggest.datagridOption.column20.editor" rowspan="2">Tình trạng tủ</th>
								<th data-options="field:'khoXuat',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column21.formatter,
									editor: equipCreateLendSuggest.datagridOption.column21.editor" rowspan="2">Kho xuất</th>
								<th data-options="field:'doanhSoTB',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column22.formatter,
									editor: equipCreateLendSuggest.datagridOption.column22.editor" rowspan="2">Doanh số trung bình 3 tháng gần nhất (D)</th>
							</tr>
							<tr>
								<th data-options="field:'dienThoai',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column7.formatter,
									editor: equipCreateLendSuggest.datagridOption.column7.editor">Điện thoại</th>
								<th data-options="field:'soNha',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column8.formatter,
									editor: equipCreateLendSuggest.datagridOption.column8.editor">Số nhà</th>
								<th data-options="field:'tinh',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column9.formatter,
									editor: equipCreateLendSuggest.datagridOption.column9.editor">Tỉnh/TP</th>
								<th data-options="field:'huyen',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column10.formatter,
									editor: equipCreateLendSuggest.datagridOption.column10.editor">Quận/Huyện</th>
								<th data-options="field:'xa',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column11.formatter,
									editor: equipCreateLendSuggest.datagridOption.column11.editor">Phường/Xã</th>
								<th data-options="field:'duong',width:100,
									formatter: equipCreateLendSuggest.datagridOption.column12.formatter,
									editor: equipCreateLendSuggest.datagridOption.column12.editor">Tên đường/Phố/Thôn/Ấp</th>
								
							</tr>

							</thead>
						</table>
					</div>
				</div>
				<div class="Clear"></div>
				<div class="SearchInSection SProduct1Form">                                
               <div class="BtnCenterSection" style="padding-bottom: 10px;">
				       <button id="btnCapNhat" class="BtnGeneralStyle" onclick="return EquipmentManageCatalog.saveEquipGroup();">Cập nhật</button> &nbsp;&nbsp;
				       <button class="BtnGeneralStyle" onclick="window.location.href='/equipment-group-manage/info'">Bỏ qua</button>
	           </div>
		    </div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {	
	$('#panelDeliveryRecord').width($('#panelDeliveryRecord').parent().width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 5px;");
	},1000);
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar("fDate");
	ReportUtils.setCurrentDateForCalendar("tDate");	
	$('#shopCode').focus();
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

 //    var shopKendo = $("#shop").data("kendoMultiSelect");
 //    shopKendo.wrapper.attr("id", "shop-wrapper"); 
	// $('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_import_bien_ban_thu_hoi_thanh_ly.xlsx');
	var params = new Object();
	EquipmentManageCatalog._mapEviction = new Map();
	EquipmentManageCatalog._lstRowID = new Array();
	params.fromDate = $('#fDate').val();
	params.toDate = $('#tDate').val();	

	$('#grid').datagrid({
		url:'/equipCreateLendSuggest/search',
		method : 'GET',
		height: 'auto',
		width : $(window).width() - 35,
		scrollbarSize:0,
		rownumbers:true,
		//fitColumns:true,
		autoRowHeight:true, 
		pagination:true, 
		pageSize:10,
		queryParams:params,
		pageList  : [20],
// 		width: $('#gridContainer').width(),
		onSelect: function(i) {
			// if (EquipmentManageCatalog._editIndex != i) {
			// 	if (EquipmentManageCatalog.endEditing()) {
			 $('#grid').datagrid('beginEdit', i);
					// EquipmentManageCatalog._editIndex = i;
					// $(".datagrid-editable-input.numberbox-f").attr("maxlength", "3");
					// $("#gridContainer td[field=amount] .datagrid-editable-input.numberbox-f").attr("maxlength", "17");
					// var edts = $("#grid").datagrid("getEditors", EquipmentManageCatalog._editIndex);
					// $(edts[2].target).combobox("loadData", [{customerType:"0", customerTypeName: "Chưa xác định"},
					//                                         {customerType:"1", customerTypeName: "Thành thị"}, {customerType:"2", customerTypeName: "Nông thôn"}]);
				// }
			// }
		},	
		onUnselect:function(i){
			$('#grid').datagrid('endEdit', i);
		},
		// columns:[[	
		// 	{field:'delete', title:'<a href="javascript:void(0);" onclick="EquipmentManageCatalog.insertEquipmentGroupInGrid();"><img src="/resources/images/icon_add.png" title="Thêm"/></a>', width:25, align:'center',rowspan: 2, formatter: function(value, row, index) {
		//     	return '<a href="javascript:void(0);" onclick="equipCreateLendSuggest.deleteRow('+index+');"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
		// 	}},
		// 	{field: 'loaiThietBi',title:'Loại thiết bị', width: 100, sortable:false,resizable:false, align: 'left',rowspan: 2, formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 	},
		// 		 editor:{
		// 			type:'text',
		// 		}
		// 	},
		// 	{field: 'dungTich',title:'Dung tích (Lít)', width: 100, sortable:false,resizable:false, align: 'center',rowspan: 2, formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 		},
		// 		 editor:{
		// 			type:'text',
		// 		}
		// 	},

		// 	{field: 'khachHang',title:'Khách hàng (F9)',width: 70,sortable:false,resizable:false, align: 'center',rowspan: 2, formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 	},
		// 		editor:{
		// 			type:'text',
		// 		}
		// 	},
		// 	{field: 'tenKhachHang',title:'Tên khách hàng',width: 100,sortable:false,resizable:false, align: 'left' ,rowspan: 2, formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 	},
		// 		 editor:{
		// 			type:'text',
		// 		}
		// 	},
		// 	{field: 'ngDungTenMuonTu', title: 'Người đứng tên mượn tủ', width: 100, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 	},
		// 		editor:{
		// 			type:'text',
		// 		}
		// 	},
		// 	{field: 'quanHe', title: 'Quan hệ người đứng tên mượn tủ với chủ cửa hàng/Doanh nghiệp', width: 200, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 	},
		// 		editor:{
		// 			type:'text',
		// 		}
		// 	},
		// 	{field: null, title: 'Địa chỉ đặt tủ', width: 600, colspan: 6
		// 		// align: 'center', formatter: function(value,row,index){
		// 		// return Utils.XSSEncode(value);
		// 		// }
		// 	},
		// 	{field: 'dienThoai', title: 'Điện thoại', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 	},
		// 		 editor:{
		// 			type:'text',
		// 		}
		// 	},
		// 	{field: 'soNha', title: 'Số nhà', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 	},
		// 		 editor:{
		// 			type:'text',
		// 		}
		// 	},
		// 	{field: 'tinh', title: 'Tỉnh/TP', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 	},
		// 		 editor:{
		// 			type:'combobox',
		// 		}
		// 	},
		// 	{field: 'huyen', title: 'Quận/Huyện', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 	},
		// 		 editor:{
		// 			type:'combobox',
		// 		}
		// 	},
		// 	{field: 'xa', title: 'Phường/Xã', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 	},
		// 		 editor:{
		// 			type:'combobox',
		// 		}
		// 	},
		// 	{field: 'duong', title: 'Tên đường/Phố/Thôn/Ấp', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(value,row,index){
		// 		return Utils.XSSEncode(value);
		// 	},
		// 		 editor:{
		// 			type:'combobox',
		// 		}
		// 	}
			// ,
			// {field: 'soCMND', title: 'Số CMND', width: 100, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
			// 	return Utils.XSSEncode(value);
			// },
			// 	 editor:{
			// 		type:'text',
			// 	}
			// },
			// {field: 'noiCapCMND', title: 'Nơi cấp CMND', width: 100, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
			// 	return Utils.XSSEncode(value);
			// },
			// 	 editor:{
			// 		type:'text',
			// 	}
			// },
			// {field: 'ngayCapCMND', title: 'Ngày cấp CMND', width: 100, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
			// 	return Utils.XSSEncode(value);
			// 	},
			//  	editor:{
			// 		type:'datebox',
			// 	}
			// },
			// {field: 'giayPhepDKKD', title: 'Số giấy phép ĐKKD', width: 100, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
			// 	return Utils.XSSEncode(value);
			// },
			// 	 editor:{
			// 		type:'text',
			// 	}
			// },
			// {field: 'noiCapDKKD', title: 'Nơi cấp ĐKKD', width: 100, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
			// 	return Utils.XSSEncode(value);
			// },
			// 	 editor:{
			// 		type:'text',
			// 	}
			// },
			// {field: 'ngayCapDKKD', title: 'Ngày cấp ĐKKD', width: 100, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
			// 	return Utils.XSSEncode(value);
			// 	},
			// 	editor:{
			// 		type:'datebox',
			// 	}
			// },
			// {field: 'thoiGianNhanTu', title: 'Thời gian mong muốn nhận tủ', width: 100, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
			// 	return Utils.XSSEncode(value);
			// },
			// 	 editor:{
			// 		type:'datebox',
			// 	}
			// },
			// {field: 'tinhTrang', title: 'Trình trạng tủ', width: 100, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
			// 	return Utils.XSSEncode(value);
			// },
			// 	 editor:{
			// 		type:'combobox',
			// 	}
			// },
			// {field: 'khoXuat', title: 'Kho xuất', width: 100, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
			// 	return Utils.XSSEncode(value);
			// },
			// 	 editor:{
			// 		type:'combobox',
			// 	}
			// },
			// {field: 'doanhSoTB', title: 'Doanh số trung bình 3 tháng gần nhất (D)', width: 100, sortable:false,resizable:false,align: 'left',rowspan: 2, formatter: function(value,row,index){
			// 	return Utils.XSSEncode(value);
			// },
			// 	 editor:{
			// 		type:'text',
			// 	}
			// }
		// ]],
		onBeforeLoad:function(param){											 
		},
		onLoadSuccess :function(data){  	 		    	
	    	$(window).resize();
	    	if (data == null || data.total == 0) {
				
			}else{
				$('.datagrid-header-rownumber').html('STT');		    	
			}		
	    	$('.datagrid-header-row td div').css('text-align','center');
	    	updateRownumWidthForDataGrid('');
			var rows = $('#grid').datagrid('getRows');
	    	$('.datagrid-header-check input').removeAttr('checked');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(EquipmentManageCatalog._mapEviction.get(rows[i].id)!=null) {
	    			$('#grid').datagrid('checkRow', i);
	    		}
	    	}
    		 $(window).resize();
		},
		// onCheck:function(index,row){
		// 	EquipmentManageCatalog._mapEviction.put(row.id,row);  
		// 	if(row.trangThaiBienBan == "Đã duyệt"){
		// 		EquipmentManageCatalog._lstRowID.push(row.id);
		// 	}
			
	 //    },
	 //    onUncheck:function(index,row){
	 //    	EquipmentManageCatalog._mapEviction.remove(row.id);
		// 	var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
	 //    	if(position != null){
	 //    		EquipmentManageCatalog._lstRowID.splice(position,1);
	 //    	}
	 //    },
	 //    onCheckAll:function(rows){
		//  	for(var i = 0;i<rows.length;i++){
	 //    		var row = rows[i];
	 //    		EquipmentManageCatalog._mapEviction.put(row.id,row);
	 //    		var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
		// 		if(position == -1 && row.trangThaiBienBan == "Đã duyệt"){
		// 			EquipmentManageCatalog._lstRowID.push(row.id);
		// 		}
	 //    	}
	 //    },
	 //    onUncheckAll:function(rows){
	 //    	for(var i = 0;i<rows.length;i++){
	 //    		var row = rows[i];
	 //    		EquipmentManageCatalog._mapEviction.remove(row.id);	
	 //    		var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
		// 		if(position != null){
		// 			EquipmentManageCatalog._lstRowID.splice(position,1);
		// 		}
	 //    	}
	 //    }

	});
	
});

function changeCombo() {
    var status = $('#statusRecordLabel').val().trim();
    var html = '';
    if(status == '2'||status == '-1'){
    	html = 
    	'<option value="0" selected="selected">Chọn</option> <option value="1" >Đã gửi</option> <option value="2" >Đã nhận</option> ';
    }else{
    	html = '<option value="0" selected="selected">Chọn</option>';
    }
    $('#statusDeliveryLabel').html(html);
    $('#statusDeliveryLabel').change();
};
var equipCreateLendSuggest = {
	deleteRow: function(i){
		$('#grid').datagrid("deleteRow", i);
	},
	datagridOption: {
		column0: {
			title: '<a href="javascript:void(0);" onclick="EquipmentManageCatalog.insertEquipmentGroupInGrid();"><img src="/resources/images/icon_add.png" title="Thêm"/></a>',
			formatter: function (value,row,index) {
				return '<a href="javascript:void(0);" onclick="equipCreateLendSuggest.deleteRow('+index+');"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
			}
		},
		column1: {
			title: 'Loại thiết bị',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox'
			}
		},
		column2: {
			title: 'Dung Tích',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column3: {
			title: 'Khách hàng (F9)',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column4: {
			title: 'Tên khách hàng',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column5: {
			title: 'Người đứng tên mượn tủ',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column6: {
			title: 'Quan hệ người đứng tên mượn tủ với chủ cửa hàng/Doanh nghiệp',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column7: {
			title: 'Điện thoại',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column8: {
			title: 'Số nhà',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column9: {
			title: 'Tỉnh/TP',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox'
			}
		},
		column10: {
			title: 'Quận/Huyện',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox'
			}
		},
		column11: {
			title: 'Phường/Xã',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox'
			}
		},
		column12: {
			title: 'Tên đường/Phố/Thôn/Ấp',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox'
			}
		},
		column13: {
			title: 'Số CMND',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column14: {
			title: 'Nơi cấp CMND',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column15: {
			title: 'Ngày cấp CMND',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'datebox'
			}
		},
		column16: {
			title: 'Số giấy phép ĐKKD',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column17: {
			title: 'Nơi cấp ĐKKD',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column18: {
			title: 'Ngày cấp ĐKKD',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'datebox'
			}
		},
		column19: {
			title: 'Thời gian mong muốn nhận tủ',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'datebox'
			}
		},
		column20: {
			title: 'Tình trạng tủ',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox'
			}
		},
		column21: {
			title: 'Kho xuất',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox'
			}
		},
		column22: {
			title: 'Doanh số trung bình 3 tháng gần nhất (D)',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		}
	}
}
</script>
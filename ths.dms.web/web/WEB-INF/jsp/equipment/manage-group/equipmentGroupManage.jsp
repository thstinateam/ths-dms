<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
#player_api {
	width: 678px !important;
}
</style>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">Thiết bị</a></li>
		<li><span>Quản lý danh mục nhóm thiết bị</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin tìm kiếm" style="width: 1000px; height: 200px; padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
					<div class="SearchInSection SProduct1Form">
						<label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Mã nhóm thiết bị</label> 
						<input type="text" class="InputTextStyle InputText1Style" id="code"  maxlength="50" /> 
						<label class="LabelStyle Label1Style" >Tên nhóm thiết bị</label>
						<input type="text"	class="InputTextStyle InputText1Style" id="name" maxlength="250"/> 
						<label class="LabelStyle Label1Style">Loại</label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="type">
								<option value="-1">Tất cả</option>
								<s:iterator value="listEquipCategory" var="obj" > 								
 									<option value="<s:property value="#obj.id" />"><s:property value="#obj.name" /></option>
 								</s:iterator>	
							</select>
						</div>	
						<div class="Clear"></div>						
						<label	class="LabelStyle Label1Style"  style=";text-align:left;;height:auto;" >Hiệu</label>
						<input	type="text" class="InputTextStyle InputText1Style" id="brand" maxlength="50" /> 
						<label class="LabelStyle Label1Style">Dung tích</label> 
						<input	type="text" class="InputTextStyle InputText10Style" id="fromCapacity" placeholder="Từ" maxlength="10" /> 
						<label 	class="LabelStyle LabelStyle" style=";text-align:right;width:12px;"> - </label>
						<input	type="text" class="InputTextStyle InputText10Style" placeholder="Đến" id="toCapacity" maxlength="10" />
						<label	class="LabelStyle Label1Style">Trạng thái</label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status">
								<option value="-1" selected="selected">Tất cả</option>
								<option value="1">Hoạt động</option>
						   		<option value="0">Tạm ngưng</option>						   		
							</select>
						</div>	
						<div class="Clear"></div>
						<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
					</div>
					<div class="SearchInSection SProduct1Form">
				<div class="Clear"></div>
					<div class="BtnCenterSection">
                        <button class="BtnGeneralStyle" id="btnSearch" onclick="return EquipmentManageCatalog.searchEquipGroup();">Tìm kiếm</button>
                    </div>
					<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
					<p class="ErrorMsgStyle" id="errMsgSearch" style="display: none;"></p>
				</div>
				</div>
				<h2 class="Title2Style">Danh mục nhóm thiết bị
					<a onclick = "EquipmentManageCatalog.exportBySearchEquipGroup();" style = "float: right;	padding: 0px 25px 0px 0px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_export.png" title="Xuất Excel"></a>
					<a onclick = "EquipmentManageCatalog.openDialogImportEquipGroup();" style = "float: right;	padding-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_import.png" title="Nhập Excel"></a>
					<a onclick = "EquipmentManageCatalog.downloadTemplateImportEquipGroup();" style = "float: right; padding-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_download_32.png" title="Tải mẫu Nhập Excel"></a>
				</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id=gridContainer>
							<table id="grid"></table>
						</div>
					</div>	
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Nhập Excel Danh nhóm thiết bị" data-options="closed:true,modal:true" style="width:465px;height:250px;">
		<div class="PopupContentMid">
			<div class="SearchInSection SProduct1Form" style="border: none;">
				<form action="/equipment-group-manage/importEquipGroup" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style" style="padding-right: 10px; text-align: right; width: 80px;">Tập tin Excel</label>
				    <div class="UploadFileSection Sprite1" style="width: 270px;">
						<input id="fakefilepc" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" style="width: 270px;" size="1" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm','fakefilepc', 'errExcelMsg');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a onclick="EquipmentManageCatalog.downloadTemplateImportEquipGroup();" href="javascript:void(0)" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="EquipmentManageCatalog.importEquipGroup();">Nhập Excel</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').dialog('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p id="successExcelMsg" class="SuccessMsgStyle" style="display:none;"></p>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsg" />
	</div>	
</div>
<%-- <s:hidden id="successUpload" value="0"></s:hidden> --%>
<script type="text/javascript">
$(document).ready(function() {	
	 $('#code, #name', '#type','#capacity','#brand','#status').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	}); 
	$('#panelDeliveryRecord').width($(window).width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
	},1000);
	
	var params = new Object();	
	params.code = $('#code').val().trim();
	params.name = encodeChar($('#name').val().trim());
	params.status = $('#status').val();
	$('#grid').datagrid({
		url : "/equipment-group-manage/searchEquipmentGroup",
		autoRowHeight : true,
		rownumbers : true, 		
		singleSelect: true,
		pagination:true,
		rowNum : 20,
		fitColumns:true,
		pageList  : [20,40,60],
		scrollbarSize:0,
		width: $('#gridContainer').width(),
		autoWidth: true,
		queryParams:params,	
	    columns:[[	        
		    {field:'groupEquip', title: 'Nhóm thiết bị', width: 100, sortable: false, resizable: true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field:'typeGroup', title: 'Loại', width:80,sortable:false,resizable:true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},	    
		    {field:'capacity',title: 'Dung tích(lít)', width: 50, align: 'center',sortable:false, resizable:true, formatter:function(value, row, index){
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field:'brandName',title: 'Hiệu', width: 80, align: 'left',sortable:false, resizable:true, formatter:function(value, row, index){
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field:'totalQuantity',title: 'Số lượng', width: 50, align: 'right',sortable:false, resizable:true, formatter:function(value, row, index){
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		   /*  {field:'quantity',title: 'Tồn kho tổng VNM', width: 50, align: 'right',sortable:false, resizable:true, formatter:function(value, row, index){
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},  */
		    {field:'status', title: 'Trạng thái', width: 50, align: 'left',sortable:false,resizable:true,formatter:function(value, row, index){
		    	if (value != undefined && value != null) {
		    		return statusTypeText.parseValue(value);
		    	}
		    	return "";
		    }},
		    {field:'edit',  title:'<a href="/equipment-group-manage/changeEquipmentGroupJSP?id=0"><img src="/resources/images/icon_add.png"/></a>',
		    	width: 20, align: 'center',sortable:false,resizable:true, formatter: function (value, row, index) {
		    		if (row.id != undefined && row.id != null && !isNaN(row.id)) {			
		    			return '<a href="/equipment-group-manage/changeEquipmentGroupJSP?id=' + row.id +'"><img width="16" style="" heigh="16" src="/resources/images/icon-edit.png" title="Chỉnh sửa"></a>';		
		    		} 
		    		return '<a><img width="16" style="" heigh="16" src="/resources/images/icon-edit_disable.png" title="Chỉnh sửa"></a>';
		    }},
		    {field: 'id', index: 'id', hidden: true},
	    ]],	    
	    onLoadSuccess :function(data){	    	
			$('.datagrid-header-rownumber').html('STT');			 
			 $(window).resize();
			 if (data == null || data.total == 0) {
				// $("#errMsgSearch").html("Không tìm thấy dữ liệu").show();
			 }
		}
	});	
});
</script>
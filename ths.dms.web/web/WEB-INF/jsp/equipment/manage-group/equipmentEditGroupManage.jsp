<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="/equipment-group-manage/info">Thiết bị</a>
		</li>
		<li>
			<span>Thiết lập danh mục nhóm thiết bị</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Mã nhóm <span class="ReqiureStyle">(*)</span></label> 
					<s:if test="id==null || id ==0">
						<input	type="text" class="InputTextStyle InputText1Style" id="code" maxlength="50" autocomplete="off"/> 	
					</s:if>
					<s:else>
						<input id="code" type="text" class="InputTextStyle InputText1Style" value="<s:property value="equipmentVO.code"/>" disabled="disabled" tabindex="2" autocomplete="off" maxlength="50"/>
					</s:else>						
					<label	class="LabelStyle Label1Style">Tên nhóm <span class="ReqiureStyle">(*)</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" id="name" maxlength="250" value="<s:property value="equipmentVO.name" />" autocomplete="off"/>
					<label class="LabelStyle Label1Style">Loại</label>
					<div class="BoxSelect BoxSelect2">							
						<select class="MySelectBoxClass" id="type">
							<option selected="selected" value=""></option>			
							<s:iterator value="listEquipCategory" var="obj" > 
								<%-- <s:if test="#obj.id == equipCategoryVO.id">
	 								<option value="<s:property value="#obj.id" />" selected="selected"><s:property value="#obj.name" /></option>
	 							</s:if>
	 							<s:else> --%>
	 								<option value="<s:property value="#obj.id" />"><s:property value="#obj.name" /></option>
	 							<%-- </s:else> --%>
	 						</s:iterator>	
						</select>
					</div>	
					<div class="Clear"></div>	
					<label	class="LabelStyle Label1Style"  style=";text-align:left;;height:auto;">Hiệu <span class="ReqiureStyle">(*)</span></label>
					<input	type="text" class="InputTextStyle InputText1Style" id="brand" maxlength="50" value="<s:property value="equipmentVO.BrandName" />" autocomplete="off"/>		
					<label class="LabelStyle Label1Style">Dung tích(lít)<span class="ReqiureStyle">(*)</span></label> 
					<input	type="text" class="InputTextStyle InputText10Style vinput-number-dot" id="fromCapacity" placeholder="Từ" maxlength="10" value="<s:property value="equipmentVO.fromCapacity" />" autocomplete="off"/> 
					<label 	class="LabelStyle LabelStyle" style=";text-align:right;width:12px;"> - </label>
					<input	type="text" class="InputTextStyle InputText10Style vinput-number-dot" id="toCapacity" placeholder="Đến" maxlength="10" value="<s:property value="equipmentVO.toCapacity" />" autocomplete="off"/>					
					<label	class="LabelStyle Label1Style">Trạng thái</label>
						<div class="BoxSelect BoxSelect2">
						<s:if test="id==null || id ==0">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status" disabled="disabled">							  
							    <option value="1">Hoạt động</option>
							</select>
						</s:if>		
						<s:else>
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status">	
								<s:if test="equipmentVO.status == 1">
								    <option value="1">Hoạt động</option>
								    <option value="0">Tạm ngưng</option>
								</s:if>
								<s:elseif test="equipmentVO.status == 0">					
								    <option value="0">Tạm ngưng</option>
								    <option value="1">Hoạt động</option>
								 </s:elseif>	
							</select>	
						</s:else>							
						</div>			
						<div class="Clear"></div>			
				</div>			
					<div class="SearchInSection SProduct1Form">                                
	                   <div class="BtnCenterSection" style="padding-bottom: 10px;">
						       <button id="btnCapNhat" class="BtnGeneralStyle" onclick="return EquipmentManageCatalog.saveEquipGroup();">Cập nhật</button> &nbsp;&nbsp;
						       <button class="BtnGeneralStyle" onclick="window.location.href='/equipment-group-manage/info'">Bỏ qua</button>
			           </div>
		            </div>	
		            <div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
		        <h2 class="Title2Style">Mức doanh số</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id=gridContainer>
							<table id="grid"></table>
						</div>
					</div>	
				</div>						
			  </div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<s:hidden id="idChange" name="id"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	EquipmentStockChange._idNewRow = -1;
	EquipmentStockChange._lstEquipSaleplanDel = [];
	var statusEq = '<s:property value="equipmentVO.status" />';
	if(statusEq != undefined && statusEq != "" && statusEq == equipStatusText.ACTIVE){
		enableSelectbox('status');
	} else {
		disableSelectbox('status');
	}
	$('#code, #name, #fromCapacity, #toCapacity, #brand, #status').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnCapNhat').click(); 
		}
	});	
	EquipmentManageCatalog._arrGridDelete = [];
	
	 $("#gridContainer td[field=aMount]").live("keyup", function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$('#btnCapNhat').click(); 
		}
	});
	 $("#gridContainer td[field=fromMonth]").live("keyup", function(event) {
			if (event.keyCode == keyCodes.ENTER) {
				$('#btnCapNhat').click(); 
				}
		});
	 $("#gridContainer td[field=toMonth]").live("keyup", function(event) {
			if (event.keyCode == keyCodes.ENTER) {
				$('#btnCapNhat').click(); 
				}
		});
	 $("#gridContainer td[field=customerType]").live("keyup", function(event) {
			if (event.keyCode == keyCodes.ENTER) {
				$('#btnCapNhat').click(); 
				}
		});
	 EquipmentManageCatalog._lstCustomerType = [];
	 <s:iterator value="lstCustomerType">
		var obj = {
			id: Utils.XSSEncode("<s:property value='id' />"),
			code: Utils.XSSEncode("<s:property value='channelTypeCode' />"),
			name: '<s:property value="escapeHTMLForXSS(channelTypeName)" escapeHtml="false" />'
		};
		EquipmentManageCatalog._lstCustomerType.push(obj);
	</s:iterator>
	EquipmentManageCatalog._pMap = new Map();
	$('#grid').datagrid({		
		url: "/equipment-group-manage/viewEquipGroup",		
		autoRowHeight: true,
		rownumbers: true, 		
		singleSelect: true,
		pagination: true,
		rowNum: 10,
		fitColumns: true,
		pageList: [10, 20, 30],
		scrollbarSize: 0,
		width: $('#gridContainer').width(),
		autoWidth: true,
		queryParams: {
			id: $('#idChange').val().trim()			
		},		
		onSelect: function(i) {
			if ($('.datagrid-editable-input.numberbox-f.validatebox-text').length == 0 || EquipmentManageCatalog._editIndex != i) {
				if (EquipmentManageCatalog.endEditing()) {
					$('#grid').datagrid('beginEdit', i);
					EquipmentManageCatalog._editIndex = i;
					$(".datagrid-editable-input.numberbox-f").attr("maxlength", "3");
					$("#gridContainer td[field=amount] .datagrid-editable-input.numberbox-f").attr("maxlength", "17");
					var edts = $("#grid").datagrid("getEditors", EquipmentManageCatalog._editIndex);
					// $(edts[2].target).combobox("loadData", 
					// 	[
					// 	 {customerType:"0", customerTypeName: ""}, 
					// 	 {customerType:"1", customerTypeName: "Thành thị"}, 
					// 	 {customerType:"2", customerTypeName: "Nông thôn"}
					// 	]
					// );
					$(edts[2].target).combobox("loadData", EquipmentManageCatalog._lstCustomerType);
				}
				$('.datagrid-editable-input.numberbox-f.validatebox-text').bind('keyup', function(event){
					if(event.keyCode == keyCodes.ENTER){
						$("#grid").datagrid('endEdit', i);
						$('#btnCapNhat').click();
					}
				});
			}
		},
		columns: [[
		 {field: 'id', index: 'id', hidden: true},       
		 {field:'fromMonth', title: 'Từ tháng',width:80, sortable:false,resizable:false, align: 'left',
			editor:{type:'numberbox'},	formatter: function(v, r, i) {
				return Utils.XSSEncode(r.fromMonth); 		
			}		
		 },
		 {field:'toMonth', title: 'Đến tháng', width:80,sortable:false,resizable:false, align: 'left',
			editor:{type:'numberbox'},	formatter: function(v, r, i) {
				return Utils.XSSEncode(r.toMonth); 		
			}					
		 },
		 {field:'customerType', title: 'Loại khách hàng', width:150,sortable:false,resizable:true , align: 'left', 
			editor: {
				type: "combobox",
				options: {
					valueField: 'id',
					textField: 'name',
					onHidePanel: function() {
						var edts = $("#grid").datagrid("getEditors", EquipmentManageCatalog._editIndex);
						var v = $(edts[2].target).combobox("getValue");
						var t = $(edts[2].target).combobox("getText");
						var r = $("#grid").datagrid("getRows")[EquipmentManageCatalog._editIndex];
						r.customerTypeId = v;
						r.customerTypeName = t;
					}
	    		}
		 	},
			formatter: function(v, r, i) {
				var value = 'Chưa xác định';
				if (r.customerTypeName != undefined && r.customerTypeName != null && r.customerTypeName.trim().length > 0) {
					value = r.customerTypeName;
				} else {
					r.customerTypeId = 0;
				}
				return Utils.XSSEncode(value); 		
			}
		 },		
		 {field:'amount', title: 'Doanh số', width: 90, sortable:false,resizable:false, align: 'right',
			 editor: {type:'numberbox',options:{groupSeparator:','}}, formatter: function(v, r, i) {
				return formatCurrency(r.amount); 		
			 }
		 },
		 {field:'delete', title:'<a href="javascript:void(0);" onclick="EquipmentManageCatalog.insertEquipmentGroupInGrid();"><img src="/resources/images/icon_add.png" title="Thêm"/></a>', width:50, align:'center', formatter: function(value, row, index) {
			 //Bo sung tinh dung dan du lieu khi truyen tham so
			 var id = row.id;
			 if (id == undefined || id == null) {
				 id = 0;
			 }
			 return '<a href="javascript:void(0);" onclick="EquipmentManageCatalog.deleteEquipment('+index+','+row.id+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
		 }}
		]],	    
		onLoadSuccess :function(data){
			$('.datagrid-header-rownumber').html('STT');
			Utils.updateRownumWidthAndHeightForDataGrid('grid');
			$(window).resize();
			EquipmentManageCatalog._mapEquipSaleplanNew = new Map();
			if (data.rows != undefined && data.rows!=null && data.rows.length > 0) {
				var rows = data.rows;
				for (var i = 0, size = rows.length; i < size; i++) {					
					var row = rows[i];
					/*if (row != undefined && row != null && row.id != undefined && row.id != null) {
						EquipmentManageCatalog._mapEquipSaleplanNew.put(row.id, row);						
					}*/
					if (row.fromMonth != undefined && row.fromMonth != null) {
						row.fromMonth = row.fromMonth.toString();
					} else {
						row.fromMonth = "";
					}
					if (row.toMonth != undefined && row.toMonth != null) {
						row.toMonth = row.toMonth.toString();
					} else {
						row.toMonth = "";
					}
					if (row.amount != undefined && row.amount != null) {
						row.amount = row.amount.toString();
					} else {
						row.amount = "";
					}
					if (row.customerType != undefined && row.customerType != null) {
						row.customerType = row.customerType.toString();
					} else {
						row.customerType = "0";
					}
				}
			}
		}	
	});	

	$('#type option').first().remove();
	$('#type').change();

	var typeId = '<s:property value="equipCategoryVO.id" />';
	if (typeId != undefined && typeId != '') {
		$('#type').val(typeId);
		$('#type').change();
	}
	

});
</script>
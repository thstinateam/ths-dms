
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Thiết bị</a>
		</li>
		<li><span>Thanh lý tài sản</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm <span style="float:right;margin-right:15px;">
						<a href="javascript:void(0);" title="Ẩn/ hiện" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
				<!-- <h2 class="Title2Style">Thông tin tìm kiếm</h2> -->
				<!-- <div class="SearchInSection SProduct1Form" style="padding-left: 0px;">
				 	<div id="panelLiquidation" class="easyui-panel" title="Thông tin tìm kiếm" style="width: 'auto'; height: 'auto'; padding-top: 20px;"data-options="collapsible:true,closable:false,border:false"> -->
				 	<div class="SearchInSection SProduct1Form">
						<label class="LabelLeftStyleM5 LabelLeft5Style">Ngày biên bản từ</label>
						<div id="fromDateDiv">
							<input type="text" id="fDate" class="InputTextStyle InputText6Style vinput-date"/>
						</div>
						<label class="LabelStyle Label9Style">Đến</label>
						<div id="toDateDiv">
							<input type="text" id="tDate" class="InputTextStyle InputText6Style vinput-date"/>
						</div>
						<label class="LabelStyle Label9Style">Mã biên bản</label>
						<input id="recordCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
						<div class="Clear"></div>
						<label class="LabelLeftStyleM5 LabelLeft5Style">Số CV/ Tờ trình thanh lý</label>
						<input id="docNumber" type="text" class="InputTextStyle InputText1Style" maxlength="50" />						
						<!-- <label class="LabelStyle Label9Style">Mã thiết bị(F9)</label> --> <!-- Khong su dung F9 nua -->
						<label class="LabelStyle Label9Style">Mã thiết bị </label>
						<input id="equipCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
						<label class="LabelStyle Label9Style">Trạng thái biên bản</label> 
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="statusRecord">
								<option value="" selected="selected">Tất cả</option>
								<option value="0" >Dự thảo</option>
								<option value="2" >Đã duyệt</option>
								<option value="7" >Đã thanh lý</option>
								<option value="4" >Hủy</option>
							</select>
						</div>
						<div class="Clear"></div>
						<div class="SearchInSection SProduct1Form">
							<div class="BtnCenterSection">
								<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol" onclick="EquipmentManagerLiquidation.searchLiquidation();">Tìm kiếm</button>
								<!-- <button id="btnSearch" class="BtnGeneralStyle" onclick="location.href='/equipment-manage-Liquidation/change-record';">Tạo mới</button> -->
							</div>
						</div>	
						<p id="errMsgInfo" class="ErrorMsgStyle" style="display: none;"></p>
						<p id="successMsgInfo" class="SuccessMsgStyle" style="display: none"></p>										
					</div>			
				
				<!-- danh sach bien ban Thanh ly -->	
				<div class="Title2Style" style="height:15px;font-weight: bold;">
					<span style="float:left;">Danh sách biên bản thanh lý tài sản</span>
					<div style="float:right; margin-top: -5px;margin-right: 27px;">
						<!-- Truong hop co xai xuat in -->
<!-- 						<a href="javascript:void(0);" onclick="" ><img src="/resources/images/icon-printer.png" height="20" width="20" style="margin-right: 5px;" title="In"></a> -->
						<!-- <a href="#" class="downloadTemplateReport"><img src="/resources/images/icon_download_32.png" height="20" width="20" style="border-left-color: blue; border-left-style: solid; border-left-width: 2px;" style="margin-right: 5px;" title="Tải tập tin mẫu"></a> -->
						<a id="btnImportDownloadTemplate" class="downloadTemplateReport cmsiscontrol" href="#"><img src="/resources/images/icon_download_32.png" height="20" width="20" style="margin-right: 5px;" title="Tải tập tin mẫu"></a>
						<a id="btnImportShowDlg" class="cmsiscontrol" href="javascript:void(0);" onclick="EquipmentManagerLiquidation.showDlgImportExcel();"><img src="/resources/images/icon_table_import.png" height="20" width="20" style="margin-right: 5px;" title="Nhập excel"></a>
						<a id="btnExport" class="downloadTemplateReport cmsiscontrol" href="javascript:void(0);" onclick="EquipmentManagerLiquidation.exportExcelBBTL();" ><img src="/resources/images/icon_table_export.png" height="20" width="20" style="margin-right: 5px;" title="Xuất excel"></a>					
						
						<button style="float:right;" id="btnSaveLst" class="BtnGeneralStyle cmsiscontrol" onclick="EquipmentManagerLiquidation.saveLiquidation();" >Lưu</button>
						<div style="float:right;margin-right: 5px;" id="btnSaveStatusRecord" class="BoxSelect BoxSelect11 cmsiscontrol">							
							<select class="MySelectBoxClass" id="statusRecordLabel" style="width:112px;" >
								<option value="" selected="selected">Chọn</option>
								<option value="2" >Duyệt</option>
								<option value="7" >Đã thanh lý</option>
								<option value="4" >Hủy</option>
							</select>
						</div>						
					</div>
				</div>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer" >
						<table id="gridLiquidation"></table>
					</div>
				</div>							
			</div> 
			</div> <!-- end <div class="ToolBarSection"> -->
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Import - Biên bản Thanh lý" data-options="closed:true,modal:true" style="width:465px;height:250px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-manage-liquidation/import-excel-liquidation" name="importFrmBBTL" id="importFrmBBTL" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">
						<input id="fakefilepcBBTL" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileBBTL" onchange="previewImportExcelFile(this,'importFrmBBTL','fakefilepcBBTL', 'errExcelMsgBBTL');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p id="btnImportDownloadTemplateDlg" class="DownloadSFileStyle cmsiscontrol">
					<a onclick="" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle cmsiscontrol" id="btnImportUploadExcelFile" onclick="return EquipmentManagerLiquidation.importExcelBBTL();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgBBTL"/>
	</div>	
</div>
<!-- popup chon thiet bi -->
<tiles:insertTemplate template="/WEB-INF/jsp/equipment/manage-liquidation/equipmentManageLiquidationEquipment.jsp"></tiles:insertTemplate>
<!-- popup xem chi tiet -->
<div style="display: none">
  	<div id="easyuiPopupViewInfo" class="easyui-dialog" title="Xem chi tiết - Biên bản Thanh lý" data-options="closed:true,modal:true" style="width:465px;height:250px;">
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {	
	//$('#panelLiquidation').width($(window).width());
	/*setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
		$('#recordCode').focus();
	},1000);*/

	// setDateTimePicker('fDate');
	// setDateTimePicker('tDate');	
	ReportUtils.setCurrentDateForCalendar("fDate");
	ReportUtils.setCurrentDateForCalendar("tDate");	
			
	$('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_import_bien_ban_thanh_ly_tai_san.xlsx');
	
	$('#fDate, #tDate, #docNumber').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});
	$('#recordCode, #status, #equipCode').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});
	/*$('#equipCode').bind('keyup',function(event){
		if(event.keyCode == keyCodes.F9){
			//EquipmentManagerLiquidation.showPopupSearchEquipmentEx(true); 
			//EquipmentManagerLiquidation.showPopupSearchEquipment(true);  // vuongmq; 24/07/2015; khong su dung popup search nua; cho nhap tim kiếm
		}
	});*/
	$('#excelFileBBTL').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnImportUploadExcelFile').click(); 
		}
	});
	
	EquipmentManagerLiquidation._lstRowId = new Array();
	// danh sach bien ban
	$('#gridLiquidation').datagrid({
		url : '/equipment-manage-liquidation/search-liquidation',
		autoRowHeight : true,
		rownumbers : true, 
		checkOnSelect :true,		
		pagination:true,
 		fitColumns:true,
		pageSize :50,
		pageList : [20, 30, 50, 100],
		scrollbarSize:15,
		width:  $('#gridContainer').width(),
		height: 400,
// 		autoWidth: true,	
		queryParams:{
			fromDate: $('#fDate').val().trim(),
			toDate: $('#tDate').val().trim(),
			status: $('#statusRecord').val().trim()
		},
		columns:[[		  
			{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left' },
			// {field: 'createDate',title:'Ngày',width: 100,sortable:false,resizable:false, align: 'left' , formatter:function(value,row,index) {
		 //    	//var date = new Date(row.createDate);
			// 	//return $.datepicker.formatDate('dd/mm/yyyy',date);
			// 	if(row.createDate!=null){
			// 		return row.createDate;
			// 	}
		 //    }},
		    {field: 'createDate',title:'Ngày biên bản',width: 100,sortable:false,resizable:false, align: 'left' , formatter:function(value,row,index) {
		    	//var date = new Date(row.createDate);
				//return $.datepicker.formatDate('dd/mm/yyyy',date);
				if(row.createDate!=null){
					return row.createDate;
				}
		    }},
			{field: 'liquidationCode',title:'Mã biên bản', width: 100, sortable:false,resizable:false, align: 'left' },
			{field: 'docNumber',title:'Số CV/ Tờ trình thanh lý', width: 200, sortable:false, resizable: false, align: 'left' },
			{field: 'equipCode',title:'Mã thiết bị', width: 250, sortable:false, resizable: false, align: 'left' },
			{field: 'equipSeri', title: 'Số serial', width: 100, sortable: false, resizable: false, align: 'left' },
			{field: 'status', title: 'Trạng thái biên bản', width: 80, sortable:false,resizable:false,align: 'left',formatter: function(value, row, index){
			   	var html='';
			   	html = statusRecordsEquip.parseValue(row.status);
			   	return html;
			}},
			{field: 'fileDownload',title:'Tập tin đính kèm',widt:150,align:'left',formatter: function(value, row, index){
				return '<a href="javascript:void(0)" onclick="General.downloadEquipAttechFile('+row.idLiquidation+', '+tradeTypeEquipment.TL+');">Tập tin</a>';
			}},	
			{field: 'note', title: 'Ghi chú', width: 200, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},	   
			{field:'edit', title:'<a id="btnNew" class="cmsiscontrol" href="javascript:void(0);" onclick="EquipmentManagerLiquidation.changeForm(-1);"><img src="/resources/images/icon_add.png" title="Thêm mới"/></a>', width:60, align:'left', formatter: function(value, row, index) {
			    var html='';
			    if(row.status != null && row.status != undefined){
			    	html='<a id="btnView_' + row.idLiquidation + '" class="cmsiscontrol" href="javascript:void(0);" onclick="EquipmentManagerLiquidation.viewInfoFormLiquidation('+row.idLiquidation+')"><img src="/resources/images/icon_print_view_34.png" height="20" width="20" style="margin-right:5px;" title="Xem biên bản"></a>';
			    	if(row.status == statusRecordsEquip.DT || row.status == statusRecordsEquip.DD){
				    	html+='<a id="btnEdit_' + row.idLiquidation + '" class="cmsiscontrol" href="javascript:void(0);" onclick="EquipmentManagerLiquidation.changeForm('+row.idLiquidation+');"><img src="/resources/images/icon-edit.png" height="20" width="20" title="Chỉnh sửa"></a>';
				    } 
			    }				
				return html;
			}},
		]],
		onBeforeLoad:function(param){	
		//$(this).datagrid('uncheckAll');		
		},
		onLoadSuccess :function(data){
	    	$(window).resize();
	    	if (data == null || data.total == 0) {
	    		//$('#gridLiquidation').datagrid('insertRow', {
	    		//	  index: 0,
	    		//	  row: {}
	    		//});
	    		//$('div[class="datagrid-cell-check"] input[type="checkbox"]').remove();
				//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
			}else{
				$('.datagrid-header-rownumber').html('STT');
				//if(data.total>10){
				//	$('div[class=datagrid-body]').attr('style','min-height: 350px; overflow-y: scroll;');
				//}
			}
			
			if(EquipmentManagerLiquidation._lstRowId != null){
				if(EquipmentManagerLiquidation._lstRowId.length > 0){
					
					var curRows = $('#gridLiquidation').datagrid('getRows');	
					for (var i = 0; i < curRows.length; ++i) {
						for(var j = 0; j < EquipmentManagerLiquidation._lstRowId.length; j++){
							if(curRows[i].idLiquidation == EquipmentManagerLiquidation._lstRowId[j]){
								$(this).datagrid('checkRow', i);
								break;
							}
						}
					}
				}
	    	}
			Utils.functionAccessFillControl('gridContainer', function(data){
			});
		},
		onCheck:function(index,row){
			var position = EquipmentManagerLiquidation._lstRowId.indexOf(row.idLiquidation);
	    	if(position == -1){
	    		EquipmentManagerLiquidation._lstRowId.push(row.idLiquidation);
	    	}
	    },
	    onUncheck:function(index,row){
	    	var position = EquipmentManagerLiquidation._lstRowId.indexOf(row.idLiquidation);
	    	if(position != -1){
	    		EquipmentManagerLiquidation._lstRowId.splice(position,1);
				$(this).datagrid('uncheckRow', position);
	    	}
	    },
	    onCheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
				var position = EquipmentManagerLiquidation._lstRowId.indexOf(row.idLiquidation);
				if(position == -1){
					EquipmentManagerLiquidation._lstRowId.push(row.idLiquidation);
				}
			}
	    },
	    onUncheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		var position = EquipmentManagerLiquidation._lstRowId.indexOf(row.idLiquidation);
		    	if(position != -1){
		    		EquipmentManagerLiquidation._lstRowId.splice(position,1);
					$(this).datagrid('uncheckRow', position);
		    	}
				
	    	}
	    }
		
	});
	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="GeneralForm ImportExcel1Form">	
	<h2 class="Title2Style">Thông tin chung</h2>
	<div class="SearchInSection SProduct1Form">
		<label class="LabelStyle Label6Style" style="width:180px;">Số công văn/Tờ trình thanh lý<span class="ReqiureStyle">(*)</span></label>
			<input disabled="disabled" type="text" value="<s:property value="docNumber"/>" class="InputTextStyle InputText1Style" maxlength="50"/>
			<!-- <label class="LabelStyle" style="width: 6%">Ngày biên bản<span class="ReqiureStyle">(*)</span></label>
			<input type="text" id="createForm" class="InputTextStyle InputText5Style" maxlength="50"> -->
			<label class="LabelStyle Label9Style" >Ngày biên bản<span class="ReqiureStyle">(*)</span></label>
			<div  id="createDateDiv">
				<input type="text" id="createDate" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="createDate"/>"/>
			</div>		
			<div class="Clear"></div>
			<input type="checkbox" id="checkLot" class="InputTextStyle" style="margin-left: 2%" disabled="disabled" >
			<label for ="checkLot" class="LabelStyle">Theo lô</label>
				<label class="LabelStyle Label1Style" style="margin-left: 3.2%;">Tổng giá trị thanh lý</label>
			<input type="text" id="tongThanhLy" class="InputTextStyle InputText5Style" style="text-align: right;" maxlength="17" disabled="disabled">
				<label class="LabelStyle Label1Style" style="margin-left: 1.8%">Tổng giá trị thu hồi</label>
			<input type="text" id="tongThuHoi" class="InputTextStyle InputText1Style" style="text-align: right;" maxlength="17" disabled="disabled">								
	</div>
	<div class="Clear"></div>
	<!-- danh sach bien ban Thanh ly -->	
	<h2 class="Title2Style"><span>Thông tin thiết bị</span>	</h2>			
	<div class="SearchInSection SProduct1Form">
		<div class="GridSection" id="gridContainerEquip">
			<table id="gridEquipmentLiquidation"></table>
		</div>
	</div>
	<div class="Clear"></div>
	<!-- thong tin nguoi mua -->
	<h2 class="Title2Style"><span>Thông tin người mua</span></h2>			
	<div class="SearchInSection SProduct1Form">
		<label class="LabelStyle Label1Style">Tên người mua</label>
		<input disabled="disabled" type="text" value="<s:property value="buyerName"/>" class="InputTextStyle InputText1Style" maxlength="250"/>
		<label class="LabelStyle Label6Style">Điện thoại</label>
		<input disabled="disabled" type="text" value="<s:property value="buyerPhone"/>" class="InputTextStyle InputText1Style" maxlength="50"/>
		<label class="LabelStyle Label11Style">Địa chỉ</label>
		<input disabled="disabled" type="text" value="<s:property value="buyerAddress"/>" class="InputTextStyle InputText3Style" maxlength="250"/>
	</div>
	<div class="Clear"></div>
	<!-- thong tin khac -->
	<h2 class="Title2Style"><span>Thông tin khác</span>	</h2>			
	<div class="SearchInSection SProduct1Form">
		<label class="LabelStyle Label1Style">Lý do</label>
		<input type="text" disabled="disabled" value="<s:property value="reason"/>" class="InputTextStyle InputText7Style" maxlength="250"/>
		<label class="LabelStyle Label11Style">Trạng thái</label>						
		<div class="BoxSelect BoxSelect2 BoxDisSelect">
			<select class="MySelectBoxClass" id="statusView" disabled="disabled">
				<s:if test="status == 0">
					<option value="0" >Dự thảo</option>
				</s:if>
				<s:if test="status == 2">
					<option value="2" >Duyệt</option>
				</s:if>
				<s:if test="status == 4">
					<option value="4" >Hủy</option>
				</s:if>
					<s:if test="status == 7">
					<option value="7" >Đã thanh lý</option>
				</s:if>
			</select>
		</div>
	</div>		
	<div class="Clear"></div>
	<!-- <h2 class="Title2Style">Tập tin đính kèm</h2>
	<div class="SearchInSection SProduct1Form">
		<a href="javascript:void(0);" onclick=""><img src="/resources/images/icon_attach_file.png"></a>
	</div>				 -->
	<div class="SearchInSection SProduct1Form">
		<div class="BtnCenterSection">					
			<button id="btnClose" class="BtnGeneralStyle" onclick="$('#easyuiPopupViewInfo').dialog('close');">Đóng</button>
		</div>
	</div>
</div>
<div><input type="hidden" id="formCreateDate" value="<s:property value="createDate"/>"></div>
<script type="text/javascript">
$(document).ready(function() {
	var valueCheckLot= '<s:property value="checkLot"/>';
	if (valueCheckLot != '' && valueCheckLot == activeType.RUNNING) {
		$('#checkLot').prop("checked", true);
		//enable('tongThanhLy');
		//enable('tongThuHoi');
		//EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._DISABLED);
	} else {
		$('#checkLot').prop("checked", false);
		//disabled('tongThanhLy');
		//disabled('tongThuHoi');
		//EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._ENABLE);
	}

	//tamvnm: them ngay lap 03/07/2015
	// setDateTimePicker('createDate');
	var createFormDate = $('#formCreateDate').val();
	if (createFormDate != undefined && createFormDate != null && createFormDate != '') {
		$('#createDate').val(createFormDate);
	} else {
		ReportUtils.setCurrentDateForCalendar("createDate");
	}


	var gtThanhLy = '<s:property value="liquidationValue"/>';
	var gtThuHoi = '<s:property value="evictionValue"/>';
	if (gtThanhLy != '') {
		$('#tongThanhLy').val(formatCurrency(gtThanhLy));
	}
	if (gtThuHoi != '') {
		$('#tongThuHoi').val(formatCurrency(gtThuHoi));
	}
});
</script>
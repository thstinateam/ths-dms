<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
.datagrid-editable-input.numberbox-f.validatebox-text {
  text-align: right;
}
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipment-manage-liquidation/info">Thiết bị</a>
		</li>
		<li><span>Lập biên bản thanh lý</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label6Style" style="width: 196px;">Số công văn/Tờ trình thanh lý<span class="ReqiureStyle">(*)</span></label>
					<s:if test="idLiquidation == null">
						<input id="docNumber" type="text" class="InputTextStyle InputText6Style" maxlength="50" autocomplete="off"/>
					</s:if>	
					<s:else>
						<input id="docNumber" type="text" value="<s:property value="docNumber"/>" class="InputTextStyle InputText1Style" maxlength="50" autocomplete="off"/>
					</s:else>
					<label class="LabelStyle Label9Style" >Ngày biên bản<span class="ReqiureStyle">(*)</span></label>
					<div  id="createDateDiv">
						<input type="text" id="createDate" class="InputTextStyle InputText9Style" value="<s:property value="createDate"/>" autocomplete="off"/>
					</div>		
					<div class="Clear"></div>
					<!-- <label class="LabelStyle" style="width: 6%">Ngày biên bản<span class="ReqiureStyle">(*)</span></label>
					<input type="text" id="createForm" class="InputTextStyle InputText5Style" maxlength="50"> -->
					<input type="checkbox" id="checkLot" class="InputTextStyle" style="margin-left: 2%" autocomplete="off" />
					<label for ="checkLot" class="LabelStyle" style='margin-left:0.5%'>Theo lô</label>
  					<label class="LabelStyle Label1Style" style="width:10.2%">Tổng giá trị thanh lý</label>
					<input type="text" id="tongThanhLy" class="InputTextStyle InputText5Style" style="text-align: right;" maxlength="17" autocomplete="off" />
  					<label class="LabelStyle Label1Style" style='margin-left: 1.8%'>Tổng giá trị thu hồi</label>
					<input type="text" id="tongThuHoi" class="InputTextStyle InputText1Style" style="text-align: right;" maxlength="17" autocomplete="off" />			
				</div>
				<div class="Clear"></div>
				<!-- danh sach bien ban Thanh ly -->	
				<h2 class="Title2Style"><span>Thông tin thiết bị</span>	</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="gridEquipmentLiquidation"></table>
					</div>
				</div>
				<div class="Clear"></div>
				<!-- thong tin nguoi mua -->
				<h2 class="Title2Style"><span>Thông tin người mua</span>	</h2>			
				<div class="SearchInSection SProduct1Form">
					<label class="LabelLeftStyleM2 LabelLeft3Style">Tên người mua</label>
					<s:if test="idLiquidation == null">
						<input id="buyerName" type="text" class="InputTextStyle InputText1Style" maxlength="250" />
					</s:if>
					<s:else>
						<input id="buyerName" type="text" value="<s:property value="buyerName"/>" class="InputTextStyle InputText1Style" maxlength="250"/>
					</s:else>
					<label class="LabelStyle Label6Style">Điện thoại</label>
					<s:if test="idLiquidation == null">
						<input id="buyerPhone" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
					</s:if>
					<s:else>
						<input id="buyerPhone" type="text" value="<s:property value="buyerPhone"/>" class="InputTextStyle InputText1Style" maxlength="50"/>
					</s:else>
					<label class="LabelStyle Label6Style">Địa chỉ</label>
					<s:if test="idLiquidation == null">
						<input id="buyerAddress" type="text" class="InputTextStyle InputText3Style" maxlength="250" />
					</s:if>
					<s:else>
						<input id="buyerAddress" type="text" value="<s:property value="buyerAddress"/>" class="InputTextStyle InputText3Style" maxlength="250"/>
					</s:else>
				</div>
				<div class="Clear"></div>
				<!-- thong tin khac -->
				<h2 class="Title2Style"><span>Thông tin khác</span>	</h2>			
				<div class="SearchInSection SProduct1Form">
					<label class="LabelLeftStyleM2 LabelLeft3Style">Lý do</label>
					<s:if test="idLiquidation == null">
						<input id="reason" type="text" class="InputTextStyle InputText7Style" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="reason" type="text" value="<s:property value="reason"/>" class="InputTextStyle InputText7Style" maxlength="250"/>
					</s:else>
					<label class="LabelStyle Label6Style">Trạng thái</label>
					<s:if test="idLiquidation == null">
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="status" >
								<option value="0" selected="selected">Dự thảo</option>
								<option value="2" >Duyệt</option>
								<option value="7" >Đã thanh lý</option>
							</select>
						</div>
					</s:if>
					<s:else>
						<s:if test="status == 0">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="status" >			
									<option value="0" selected="selected">Dự thảo</option>					
									<option value="2" >Duyệt</option>
									<option value="4" >Hủy</option>
								</select>
							</div>
						</s:if>	
						<s:else>
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="status" >			
									<option value="2" selected="selected">Duyệt</option>
									<option value="7" >Đã thanh lý</option>
								</select>
							</div>
						</s:else>
					</s:else>
					<div class="Clear"></div>
					<label class="LabelLeftStyleM2 LabelLeft3Style">Ghi chú</label>
					<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 552px; height: 100px; font-size: 13px;" maxlength="500"><s:property value="note"/></textarea>
				</div>		

				<div class="Clear"></div>
				<h2 class="Title2Style">Tập tin đính kèm</h2>
				<div class="SearchInSection SProduct1Form">
					<a href="javascript:void(0);" style="float: left;" title="Lựa chọn tập tin" ><img class="addFileBtn" src="/resources/images/icon_attach_file.png"></a>
					<div class="fileupload-process" style="width: 90%;">
						<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-striped active" id="total-progress" style="opacity: 0; width: 550px;">
							<div data-dz-uploadprogress="" style="width: 200px;" class="progress-bar progress-bar-success"></div>
						</div>
					</div>
					<div style="float:left; min-width: 500px; width: 90%;">
						<div style="height: auto; margin-left:20px;">
							<s:iterator id="fileVo" value="lstFileVo">
								<div class="divImageUploadFile" id = "divEquipAttachFile<s:property value="#fileVo.fileId"/>">
									<div class="imageUpdateShowIcon">
										<img class="addFileBtn" src="/resources/images/img-folder-001.png" width="50" height="50">
									</div>
									<div class="imageUpdateShowContent">
										<label><s:property value="#fileVo.fileName"/></label><br />
										<a href="javascript:void(0)" onclick="EquipmentManagerLiquidation.removeEquipAttachFile(<s:property value="#fileVo.fileId"/>);"><img title="Xóa tập tin" src="/resources/images/icon_delete.png" width="12" height="12" style="padding-left: 5px;"></a>
									</div>
								</div>
							</s:iterator>
						</div>
						<div class="Clear"></div>
						<div class="table table-striped" class="files" id="previews" style="min-height: 100px; margin-left:20px;">
							<div id="template" class="file-row" style="margin-bottom: 10px; width: 260px;float: left">
								<div style="margin-right: 10px; width: 50px; float: left">
									<span class="preview"><img data-dz-thumbnail /></span>
								</div>
								<div style="display: inline-block; width: 200px; clear: both">
									<div style="padding-right: 10px">
										<div>
											<p class="name" style="overflow: hidden" data-dz-name></p>
											<strong class="error text-danger" data-dz-errormessage></strong>
										</div>
										<div style="">
											<p class="size" data-dz-size></p>
											</div>
										<div>
											<a data-dz-remove href="javascript:void(0)"><span><img border="0" src="/resources/scripts/plugins/uploadify2.1/cancel.png"></span></a>
										</div>
									</div>								
								</div>							
							</div> 
						</div>
					</div>
					<div class="Clear"></div>
				</div>				
				<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnUpdate" class="BtnGeneralStyle cmsiscontrol" >Cập nhật</button>
<%-- 							<s:if test="idLiquidation == null">						 --%>
								<button id="btnClose" class="BtnGeneralStyle" onclick="location.href = '/equipment-manage-liquidation/info';">Đóng</button>
<%-- 							</s:if> --%>
<%-- 							<s:else> --%>
<%-- 								<button id="btnClose" class="BtnGeneralStyle" onclick="location.href = '/equipment-manage-liquidation/change-record?id=<s:property value="idLiquidation"/>';">Đóng</button> --%>
<%-- 							</s:else> --%>
						</div>
						<div class="Clear"></div>
					<p id="errMsgInfo" class="ErrorMsgStyle" style="display: none;"></p>	
					<p id="successMsgInfo" class="SuccessMsgStyle" style="display: none"></p>
				</div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup chon thiet bi -->
<tiles:insertTemplate template="/WEB-INF/jsp/equipment/manage-liquidation/equipmentManageLiquidationEquipment.jsp"></tiles:insertTemplate>
<div><input type="hidden" id="idRecordHidden" value="<s:property value="idLiquidation"/>"></div>
<div><input type="hidden" id="statusRecordHidden" value="<s:property value="status"/>"></div>
<div><input type="hidden" id="formCreateDate" value="<s:property value="createDate"/>"></div>
<div><input type="hidden" id="checkLotHidden" value="<s:property value="checkLot"/>"></div>
<script type="text/javascript">
$(document).ready(function() {	
	EquipmentManagerLiquidation._countFile = Number('<s:property value="lstFileVo.size()"/>');
	$('#docNumber, #buyerName, #buyerAddress, #buyerPhone, #reason, #status').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnUpdate').click(); 
		}
	});

	//tamvnm: them ngay lap 03/07/2015
	setDateTimePicker('createDate');
	var createFormDate = $('#formCreateDate').val();
	if (createFormDate != undefined && createFormDate != null && createFormDate != '') {
		$('#createDate').val(createFormDate);
	} else {
		ReportUtils.setCurrentDateForCalendar("createDate");
	}

	var title7 = '<a href="javascript:void(0);" onclick="EquipmentManagerLiquidation.insertEquipmentInGrid();"><img src="/resources/images/icon_add.png" title="Thêm mới"/></a>';
	if($('#statusRecordHidden').val()=='2'){
		title7 = '';
	}
	/** 06/07/2015; them dong header */
	/*var gridData = {
				total: 1,
				rows: [{}],
				footer: [{isFooter: true, price:0, liquidationValue:0, evictionValue: 0}]
			};*/
	/*var gridData = {
				footer: [{isFooter: true, price:0, liquidationValue:0, evictionValue: 0}]
			};*/
	$('#gridEquipmentLiquidation').datagrid({
		autoRowHeight : true,
		rownumbers : true, 
		singleSelect: true,
		//pagination:true,
		fitColumns:true,
		// pageSize :20,
		// pageList  : [20],
		scrollbarSize:0,
		width: $('#gridContainer').width() - 10,
		autoWidth: true,	
		queryParams:{
			idLiquidation: $("#idRecordHidden").val()
		},
		showFooter: true,
		//data: gridData,
// 		onSelect: function(i) {
// 			if ($('.datagrid-editable-input.numberbox-f.validatebox-text').length == 0 || EquipmentManagerLiquidation._eIndex != i) {
// 				if (EquipmentManagerLiquidation.endEditing()) {
// 					$('#gridEquipmentLiquidation').datagrid('beginEdit', i);
// 					EquipmentManagerLiquidation._eIndex = i;					
// 					$("#gridContainer td[field=groupEquipment] .datagrid-editable-input.numberbox-f").attr("maxlength", "50");		
// 					$("#gridContainer td[field=valueEquip] .datagrid-editable-input.numberbox-f").attr("maxlength", "17");					
// 				}
// 				$('.datagrid-editable-input.numberbox-f.validatebox-text').bind('keyup', function(event){
// 					if(event.keyCode == keyCodes.ENTER){
// 						$("#gridEquipmentLiquidation").datagrid('endEdit', i);
// 						$('#btnUpdate').click();
// 					}
// 				});
// 			}
// 		},
		columns:[[		  
			{field: 'typeEquipment',title:'Loại thiết bị', width: 80, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
					if (row.isFooter) {
						return "";
					} 
					return Utils.XSSEncode(row.typeEquipment);
			}},
			{field: 'groupEquipment',title:'Nhóm thiết bị', width: 220, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				if (row.isFooter) {
					return "";
				}
		    	var html="";
		    	if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
					html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
		    	}
				return Utils.XSSEncode(html);
			}},			
			{field: 'capacity',title:'Dung tích (lít)',width: 110,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
					if (row.isFooter) {
						return "";
					} 
					return Utils.XSSEncode(row.capacity);
			}},	
			{field: 'equipmentCode',title:'Mã thiết bị (F9)',width: 220,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				if (row.isFooter) {
					return "";
				}
				var html = '<input type="text" id="equipmentCodeInGrid" size="11" style="width: 100%;" maxlength="50">';
				if(row.equipmentCode!=null && row.equipmentCode != undefined && row.equipmentCode!=''){
// 					if($('#equipmentCodeInGrid').val() != undefined ){
// 						html = '<input type="text" id="equipmentCodeInGrid" size="11" value="'+row.equipmentCode+'">';					
// 					}else{
						html = Utils.XSSEncode(row.equipmentCode); 						
// 					}						
				}
				return html;
			}},
			{field: 'seriNumber',title:'Số serial',width: 120,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				if (row.isFooter) {
					return "<b style='float:right;'>Tổng:</b>";
				}
				var html = '';				
				if(row.seriNumber != null && row.seriNumber != undefined && row.seriNumber != ''){						
					html = row.seriNumber; 
				}	
				
				return Utils.XSSEncode(html);
			}},
			{field: 'price',title:'Nguyên giá',width: 120,sortable:false,resizable:false, align: 'right',formatter: function(v, r, i){
				if (r.isFooter) {
					return "<b style='float:right;'>"+ formatCurrency(r.price) +"</b>";
				}
				return formatCurrency(r.price); 		
			}},
			/*{field: 'valueEquip',title:'Giá trị',width: 100,sortable:false,resizable:false, align: 'left', editor:{
					type:'numberbox',
					options:{
						min:0,
						max:99999999999999999,
						precision:0,
						groupSeparator:','
					}
				},formatter: function(v, r, i) {
				  return formatCurrency(r.amount); 		
				}
			},*/
			{field: 'liquidationValue',title:'Giá trị thanh lý',width: 100,sortable:false,resizable:false, align: 'right', editor:{
					type:'numberbox',
					options:{
						min:0,
						max:99999999999999999,
						groupSeparator:','
					}
				},formatter: function(v, r, i) {
					if (r.isFooter) {
						return "<b style='float:right;'>"+ formatCurrency(r.liquidationValue) +"</b>";
					}
				   	return formatCurrency(r.liquidationValue); 		
				}
			},
			{field: 'evictionValue',title:'Giá trị thu hồi',width: 100,sortable:false,resizable:false, align: 'right', editor:{
					type:'numberbox',
					options:{
						min:0,
						max:99999999999999999,
						groupSeparator:','
					}
				},formatter: function(v, r, i) {
					if (r.isFooter) {
						return "<b style='float:right;'>"+ formatCurrency(r.evictionValue) +"</b>";
					}
				  	return formatCurrency(r.evictionValue); 		
				}
			},
		    {field: 'healthStatus', title: 'Tình trạng thiết bị', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(value, row, index) {
					if (row.isFooter) {
						return "";
					} 
					return Utils.XSSEncode(row.healthStatus); 	
			}},
		    {field: 'yearManufacture',title:'Năm sản xuất',width: 80,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
					if (row.isFooter) {
						return "";
					} 
					return Utils.XSSEncode(row.yearManufacture); 
			}},		
			{field:'delete', title:title7, width:50, align:'center', formatter: function(value, row, index) {
				if (row.isFooter) {
					return "";
				}
			    var	html='';
			    // if($("#idRecordHidden").val() == ""){
				html = '<a href="javascript:void(0);" class="deleteClass" onclick="EquipmentManagerLiquidation.deleteEquipment('+index+')"><img src="/resources/images/icon-delete.png" title="Xóa"  ></a>';
			    // }
				if($('#statusRecordHidden').val()=='2'){
					html = '<a href="javascript:void(0);"><img src="/resources/images/icon_delete_disable.png" title="Xóa"></a>';
				}
			    return html;
			}},
		]],
		onLoadSuccess: function (data) {   			 	    	
	    	$(window).resize();	
	    	if (data == null || data.total == 0) {
				//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
			} else {
				$('.datagrid-header-rownumber').html('STT');	
				if($("#idRecordHidden").val() != undefined && $("#idRecordHidden").val() != ""){
					EquipmentManagerLiquidation._numberEquipInRecord = data.total;
					//if ($('#checkLotHidden').val() != '' && $('#checkLotHidden').val() != activeType.RUNNING) {
						// khong check Lo thi enable gia tri thanh ly, gia tri thu hoi
						for (var i=0; i< data.total; i++) {
							$('#gridEquipmentLiquidation').datagrid('beginEdit', i);
							var valEquipTL = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'liquidationValue'});
							$(valEquipTL.target).bind('keydown', function(e){
								if(e.keyCode==110){
									e.preventDefault();
								}
							});
							$('#gridEquipmentLiquidation').datagrid('beginEdit', i);
							var valEquipTH = $("#gridEquipmentLiquidation").datagrid('getEditor', {index:i,field:'evictionValue'});
							$(valEquipTH.target).bind('keydown', function(e){
								if(e.keyCode==110){
									e.preventDefault();
								}
							});
							// update author nhutnn, 06/08/2015
							$(valEquipTL.target).attr('id', 'inputTL' + i).attr('maxlength', '22');
							Utils.bindFormatOnTextfield('inputTL' + i, Utils._TF_NUMBER);
							Utils.formatCurrencyFor('inputTL' + i);
							$(valEquipTH.target).attr('id', 'inputTH' + i).attr('maxlength', '22');
							Utils.bindFormatOnTextfield('inputTH' + i, Utils._TF_NUMBER);
							Utils.formatCurrencyFor('inputTH' + i);
						}
					//}
					if ($('#checkLotHidden').val() != '' && $('#checkLotHidden').val() == activeType.RUNNING) {
						// khong check Lo thi enable gia tri thanh ly, gia tri thu hoi
						EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._DISABLED);
					} else {
						EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._ENABLE);
					}
					if($('#statusRecordHidden').val()=='2'){
						$('input').attr('disabled','disabled');
						disabled('note');
					}
				}
			}
			//EquipmentManagerLiquidation.addFooterFrist();
			EquipmentManagerLiquidation.refreshFooter();	
		}
	});
	if($("#idRecordHidden").val() != undefined && $("#idRecordHidden").val() != ""){
		$('#gridEquipmentLiquidation').datagrid({url : '/equipment-manage-liquidation/search-equipment-in-form',queryParams:{idLiquidation:$("#idRecordHidden").val()}});
		$('#btnUpdate').bind('click', function(event) {		
			EquipmentManagerLiquidation.updateLiquidation();
		});
		var valueCheckLot= '<s:property value="checkLot"/>';
		if (valueCheckLot != '' && valueCheckLot == activeType.RUNNING) {
			$('#checkLot').prop("checked", true);
			enable('tongThanhLy');
			enable('tongThuHoi');
			EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._DISABLED);
		} else {
			$('#checkLot').prop("checked", false);
			disabled('tongThanhLy');
			disabled('tongThuHoi');
			EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._ENABLE);
		}
		EquipmentManagerLiquidation.addFooterFrist();
		EquipmentManagerLiquidation.refreshFooter();
	} else {
		$('#btnUpdate').bind('click', function(event) {		
			EquipmentManagerLiquidation.createLiquidation();
		});
		// mac dinh la no check
		$('#checkLot').prop("checked", false);
		disabled('tongThanhLy');
		disabled('tongThuHoi');	
		$('#tongThanhLy').val('');
		$('#tongThuHoi').val('');

	    EquipmentManagerLiquidation.insertEquipmentInGrid();
	    EquipmentManagerLiquidation.addFooterFrist();
	}
	$('#docNumber').focus();

	// upload file
	var urlUpdate="";
	if($("#idRecordHidden").val() == ""){
		urlUpdate = "/equipment-manage-liquidation/create-liquidation";
	}else{
		urlUpdate = "/equipment-manage-liquidation/update-liquidation";
	}
	// xu ly check lo khi change
	$('#checkLot').change(function (){ 
		if ($(this).is(':checked')) {
			enable('tongThanhLy');
			enable('tongThuHoi');
			// bin de gi gia tri so cho tong gia tri thanh ly, thu hoi
			Utils.bindFormatOntextfieldCurrencyFor('tongThanhLy', Utils._TF_NUMBER_COMMA)
			Utils.bindFormatOntextfieldCurrencyFor('tongThuHoi', Utils._TF_NUMBER_COMMA);
			EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._DISABLED);
		} else {
			disabled('tongThanhLy');
			disabled('tongThuHoi');
			$('#tongThanhLy').val('');
			$('#tongThuHoi').val('');
			Utils.unbindFormatOnTextfield('tongThanhLy');
			Utils.unbindFormatOnTextfield('tongThuHoi');
			EquipmentManagerLiquidation.checkGiaTriThanhLyThuHoi(EquipmentManagerLiquidation._ENABLE);
		}
	}); 
	var gtThanhLy = '<s:property value="liquidationValue"/>';
	var gtThuHoi = '<s:property value="evictionValue"/>';
	if (gtThanhLy != '') {
		$('#tongThanhLy').val(formatCurrency(gtThanhLy));
	}
	if (gtThuHoi != '') {
		$('#tongThuHoi').val(formatCurrency(gtThuHoi));
	}
	// bin de gi gia tri so cho tong gia tri thanh ly, thu hoi
	Utils.bindFormatOntextfieldCurrencyFor('tongThanhLy', Utils._TF_NUMBER_COMMA);
	Utils.bindFormatOntextfieldCurrencyFor('tongThuHoi', Utils._TF_NUMBER_COMMA);
	
	UploadUtil.initUploadFileByEquipment({
		url: urlUpdate,	
		elementSelector: 'body',
		elementId: 'body',
		clickableElement: '.addFileBtn',
		parallelUploads: 5
		//maxFilesize: 5
	}, function (data) {
		if(data.error){
			$('#errMsgInfo').html(data.errMsg).show();
			return false;
		}else{
			if($("#idRecordHidden").val() == ""){
				$('#successMsgInfo').html("Cập dữ liệu thành công.").show();	
				EquipmentManagerLiquidation._lstEquipInsert = null;
				EquipmentManagerLiquidation._editIndex = null;				
				setTimeout(function(){
					$('#successMsgInfo').html("").hide(); 
					window.location.href = '/equipment-manage-liquidation/info';						              		
				}, 3000);
			}else{
				$('#successMsgInfo').html("Cập dữ liệu thành công.").show();
				EquipmentManagerLiquidation._lstEquipInsert = null;
				EquipmentManagerLiquidation._editIndex = null;
				EquipmentManagerLiquidation._lstEquipInRecord = null;
				EquipmentManagerLiquidation._numberEquipInRecord = null;
				if($('#status').val() == statusRecordsEquip.DT){					
					setTimeout(function(){
						$('#successMsgInfo').html("").hide(); 
						window.location.href = '/equipment-manage-liquidation/change-record?id='+ $("#idRecordHidden").val().trim();						
					}, 3000);
				}else{
					setTimeout(function(){
						window.location.href = '/equipment-manage-liquidation/info';						              		
					}, 3000);
				}
			}			
		}
	});
});
</script>
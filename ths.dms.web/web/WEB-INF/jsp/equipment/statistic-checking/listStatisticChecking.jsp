<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
#player_api {
	width: 678px !important;
}
#fullDialogPhotoSection.FixFloat {
	overflow: auto;
}
</style>
<script type="text/javascript">
function makepage(src)
{
return "<html>\n" +
"<head>\n" +
"<title>Temporary Printing Window</title>\n" +
"<script>\n" +
"function step1() {\n" +
" setTimeout('step2()', 10);\n" +
"}\n" +
"function step2() {\n" +
" window.print();\n" +
" window.close();\n" +
"}\n" +
"</scr" + "ipt>\n" +
"</head>\n" +
"<body onLoad='step1()'>\n" +
"<img src='" + src + "'/>\n" +
"</body>\n" +
"</html>\n";
}
function printme(evt,type)
{
if (!evt) {
// Old IE
evt = window.event;
}
var image = null;
if (evt == undefined || evt == null) {
// 	src = $('.ImageFrame4').attr('src');
	if (type != undefined && type != null) {
		src = $('.ImagePrintingClassFull .DownloadPhotoLink ').attr('href');
	} else {
		src = $('.ImagePrintingClass .DownloadPhotoLink ').attr('href');
	}
	
} else {
	image = evt.target;
	if (!image) {
		// Old IE
		image = window.event.srcElement;
	}
	src = image.src;
}
var newWin = window.frames["printf"];
newWin.document.write(makepage(src));
window.frames["printf"].focus();
newWin.document.close();
setTimeout(function() {newWin.document.write('');},1000);
// link = "about:blank";
// var pw = window.open(link, "_new");
// pw.document.open();
// pw.document.write(makepage(src));
// pw.document.close();
}
</script>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipment-checking/info">Thiết bị</a></li>
		<li><span>Thông tin kiểm kê: </span><span class="ReqiureStyle"><s:property value="instance.code"/></span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">				
				<div class="SearchInSection SProduct1Form" id="search-from">
					<label class="LabelStyle Label1Style">Mã đơn vị</label>
					<input id="shopCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
					<label class="LabelStyle Label1Style">Khách hàng</label>
					<input id="customerCode" type="text" class="InputTextStyle InputText3Style" style="width: 546px;" autocomplete="off" maxlength="300" placeholder="Nhập mã khách hàng hoặc tên khách hàng hoặc địa chỉ"/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Mã thiết bị</label>
					<input id="deviceCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
					<label class="LabelStyle Label1Style">Số serial</label>
					<input id="seri" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
					<label class="LabelStyle Label1Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="status">
							<option value="-1" selected="selected">Tất cả</option>
							<option value="0" >Chưa kiểm kê</option>
							<option value="2" >Còn</option>
							<option value="3" >Mất</option>
						</select>
					</div>
					<s:hidden id="id" name="id"></s:hidden>
					<div class="Clear"></div>

					<label class="LabelStyle Label1Style">Từ ngày</label>
					<input id="fromDate" type="text" class="InputTextStyle InputText6Style vinput-date" maxlength="20" call-back="VTValidateUtils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày');"/>
					<label class="LabelStyle Label1Style">Đến ngày</label>
					<input id="toDate" type="text" class="InputTextStyle InputText6Style vinput-date"  maxlength="20" call-back="VTValidateUtils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');VTValidateUtils.getMessageCheckToDate('fromDate', 'Từ ngày', 'toDate', 'Đến ngày');"/>
					<label class="LabelStyle Label1Style">Người kiểm kê </label>
					<input id="staffCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" autocomplete="off" placeholder="Nhập mã hoặc tên nhân viên"/>
<!-- 					<label class="LabelStyle Label1Style">Tuyến thứ</label> -->
<!-- 					<div class="BoxSelect BoxSelect2"> -->
<%-- 						<select class="MySelectBoxClass" id="dateInWeek"> --%>
<!-- 							<option value="" selected="selected">Tất cả</option> -->
<!-- 							<option value="T2" >Thứ 2</option> -->
<!-- 							<option value="T3" >Thứ 3</option> -->
<!-- 							<option value="T4" >Thứ 4</option> -->
<!-- 							<option value="T5" >Thứ 5</option> -->
<!-- 							<option value="T6" >Thứ 6</option> -->
<!-- 							<option value="T7" >Thứ 7</option> -->
<!-- 							<option value="CN" >Chủ nhật</option> -->
<%-- 						</select> --%>
<!-- 					</div> -->
					<div class="Clear"></div>
<!-- 					<label class="LabelStyle Label1Style">Lần kiểm kê</label> -->
<!-- 					<div class="BoxSelect BoxSelect2"> -->
<%-- 						<select id="stepCheck" class="MySelectBoxClass"> --%>
<!-- 							<option value="-1">Tất cả</option> -->
<%-- 						</select> --%>
<!-- 					</div> -->
<!-- 					<label class="LabelStyle Label1Style">Tên NVBH</label> -->
<!-- 					<input id="staffName" type="text" class="InputTextStyle InputText1Style" maxlength="250" /> -->
<!-- 					<div class="Clear"></div> -->
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol" onclick="EquipStatisticChecking.searchListStatisticReturnThietBi();">Tìm kiếm</button>
						</div>
					</div>
				</div>
				<div class="Clear"></div>
				<div class="SearchInSection SProduct1Form">									
					<h2 class="Title2Style">Danh sách kiểm kê</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id="gridContainer" style="padding: 0px">
							<table id="grid"></table>
						</div>
					</div>	
				</div>
			</div>
		</div>
	<div class="Clear"></div>
</div>
<div id="popup-images" style="display:none;">
	<div class="GeneralDialog General2Dialog cmsiscontrol" id="viewImage_General2Dialog">
      	<div class="DialogPhotoSection FixFloat" style="height:615px;">
        	<div class="PhotoCols1">
              	<div id="listImage" class="DScrollpane">
              		<ul id="listImageUL" class="ResetList PhotoCols2List">
              		</ul>
            	</div>
            </div>
            <div class="PhotoCols2">
            	<div class="PhotoCols1Info1">
                	<p onclick="EquipStatisticChecking.showImageNextPre(0);" class="LeftStyle Sprite1"></p>
                	<div class="PhotoLarge"><span class="BoxFrame BoxFrame2"><span class="BoxMiddle">
<!--                 		<img id="bigImages" class="ImageMap ImageFrame4" width="752" height="508"/> -->
						<img id="bigImages" class="ImageMap ImageFrame4" />
                		<img style="display:none" id="loadingImages" width="50" height="50" src="/resources/images/loading.gif">
                	</span></span></div>
                    <p onclick="EquipStatisticChecking.showImageNextPre(1);" class="RightStyle Sprite1"></p>
                </div>
                <div class="PhotoCols1Info3">
                	<div id="divMapSection" class="Map2Section SmallMapSection">
                    	<p class="OnFucnStyle" onclick="EquipStatisticChecking.onOffMap(1);" style="display:none;"><span class="HideText Sprite1">Mở rộng</span></p>
                    	<p class="OffFucnStyle" onclick="EquipStatisticChecking.onOffMap(0);" style="display:none;"><span class="HideText Sprite1">Thu nhỏ</span></p>
                        <div class="MapSection">
                    		<div id="bigMap" style="width: 140px; height: 120px; position: relative;"></div>
                        </div>
                    </div>
					<p class="Text0Style" id="shopImage"></p>
                	<p class="Text1Style" id="staffImage"></p>
                	<p class="Text1Style" id="codeEquipImage"></p>
                	<p class="Text1Style" id="typeEquipImage"></p>
                    <p style="padding-bottom: 5px;" class="Text2Style">Thời gian chụp: <span id="timeImage"></span></p>
                    <p class="Text3Style ImagePrintingClass">
	                    <a href="javascript:void(0);" class="Sprite1 DownloadPhotoLink">Tải hình ảnh</a>
						<a href="javascript:void(0)" style="background-image: url('/resources/images/icon-rotate.png'); background-repeat : no-repeat; padding-left:22px;" onclick="EquipStatisticChecking.rotateImage(this)">Xoay hình ảnh</a>
                    	<!-- <a href="javascript:void(0)" style="background-image: url('/resources/images/icon-printer.png'); background-repeat : no-repeat; padding-left:22px;margin-left: 12px;" onclick="printme()">In</a> -->
						<input type="hidden" class="DeletePhotoLink" />
                    </p>
                    <div class="Clear"></div>
<!--                     <div class="SearchInSection PhotoSearchSection" id="divDetail" style="border:0px;font-size: 0.75em; padding-left: 0px; padding-bottom: 0px;"> -->
<!-- 						<input type="radio" class="LabelStyle cmsiscontrol" value="NOTOK" id="chkNotResult" name="formatType" style="width: 15px;float:left;margin-top: 3px;margin-right:7px;" onclick="EquipStatisticChecking.changeCheckDisplay(this);"><label class="LabelStyle cmsiscontrol" id="chkNotResultLabel" style="color: rgb(0, 153, 204);float:left;">Không đạt</label> -->
<!-- 						<input type="radio" class="LabelStyle cmsiscontrol" value="OK" id="chkResult" name="formatType" style="width: 15px;margin-left: 22px;float:left;margin-top: 3px;margin-right:7px;" onclick="EquipStatisticChecking.changeCheckDisplay(this);"><label class="LabelStyle cmsiscontrol" id="chkResultLabel" style="color: rgb(0, 153, 204);float:left;">Đạt trưng bày</label> -->
						
<!--                     	<input type="checkbox" class="cmsiscontrol" id="chkInspect" style="width: 15px;margin-left: 22px;float:left;margin-top: 3px;margin-right:7px;"/> -->
<!--                     	<label class="LabelStyle cmsiscontrol" id="chkInspectLabel" style="color: rgb(0, 153, 204);float:left;">Đã kiểm tra</label>		 -->
<!--                     	<div class="Clear"></div> -->
<!--                     	<label class="LabelLeftStyle LabelLeft3Style cmsiscontrol" id="txtNumberFaceLabel" style="color: rgb(0, 153, 204);margin-left:0px;" >Số mặt</label> -->
<!-- 		    			<input type="text" id="txtNumberFace" class="InputTextStyle InputText4Style cmsiscontrol" style="margin-left: 3px; margin-top: 3px;" maxlength="3" /> -->
<!-- 		    			<label class="LabelLeftStyleM5 LabelLeft3Style cmsiscontrol" id="txtPOSMLabel" style="color: rgb(0, 153, 204);" >POSM</label> -->
<!-- 		    			<input type="text" id="txtPOSM" class="InputTextStyle InputText4Style cmsiscontrol" style="margin-left: 3px; margin-top: 3px;" maxlength="6" /> -->
<!-- 		    			<div class="Clear"></div> -->
<!--                     	<label class="LabelLeftStyle LabelLeft3Style cmsiscontrol" id="txtNotedLabel" style="color: rgb(0, 153, 204);margin-left:0px;" >Ghi chú</label> -->
<!-- 		    			<input type="text" id="txtNoted" class="InputTextStyle cmsiscontrol" style="margin-left: 3px; margin-top: 3px;width: 222px;" maxlength="10" /> -->
<!--                     	<div class="SearchInSection SProduct1Form" style="padding-bottom: 0px;padding-top:0px;border:0px;"> -->
<!-- 					     	<button id="btnSaveDetail" class="BtnGeneralStyle BtnGeneralMStyle cmsiscontrol" onClick="EquipStatisticChecking.updateResult();">Lưu</button> -->
<!-- 					    </div> -->
<!--                     </div> -->
                    <p id="errMsgPopup" class="ErrorMsgStyle" style="display: none"></p>
                    <p style="display: none;margin:0px;padding:0px;" class="SuccessMsgStyle" id="successMsgPopup"></p>
                </div>
            </div>
            <div class="Clear"></div>
    	</div>
    </div>
</div>
<div id="popup-images-full" style="display:none;">
	<div class="GeneralDialog General2Dialog cmsiscontrol" id="viewImage_fullGeneral2Dialog">
      	<div class="DialogPhotoSection FixFloat" id="fullDialogPhotoSection">
        	<div class="PhotoCols1" id="fullPhotoCols1">
              	<div id="listImage" class="DScrollpane">
              		<ul id="listImageUL" class="ResetList PhotoCols2List">
              		</ul>
            	</div>
            </div>
            <div class="PhotoCols2" id="fullPhotoCols2">
            	<div class="PhotoCols1Info1" id="fullPhotoCols1Info1">
                	<p onclick="EquipStatisticChecking.showImageNextPre(0);" class="LeftStyle Sprite1"></p>
                	<div class="PhotoLarge"><span class="BoxFrame BoxFrame2" id="fullBoxFrame2"><span class="BoxMiddle">
<!--                 		<img id="bigImages" class="ImageMapFull ImageFrame4Full" width="752" height="508"/> -->
                		<img id="bigImages" class="ImageMapFull ImageFrame4Full" />
                		<img style="display:none; top: 0px;" id="loadingImages" width="50" height="50" src="/resources/images/loading.gif">
                	</span></span></div>
                    <p onclick="EquipStatisticChecking.showImageNextPre(1);" class="RightStyle Sprite1"></p>
                </div>
                <div class="PhotoCols1Info3">
                	<div id="divMapSection" class="Map2Section SmallMapSection">
                    	<p class="OnFucnStyle" onclick="EquipStatisticChecking.onOffMap(1);" style="display:none;"><span class="HideText Sprite1">Mở rộng</span></p>
                    	<p class="OffFucnStyle" style="top: 9px;" onclick="EquipStatisticChecking.onOffMap(0);"><span class="HideText Sprite1">Thu nhỏ</span></p>
                        <div class="MapSection">
                    		<div id="bigMap" style="width: 140px; height: 115px; position: relative;"></div>
                        </div>
                    </div>
					<p class="Text0Style" id="shopImage"></p>
                	<p class="Text1Style" id="staffImage"></p>
                	<p class="Text1Style" id="equipImage"></p>
                    <p style="padding-bottom: 5px;" class="Text2Style">Thời gian chụp: <span id="timeImage"></span></p>
                    <p class="Text3Style ImagePrintingClassFull">
	                    <a href="javascript:void(0);" class="Sprite1 DownloadPhotoLink">Tải hình ảnh</a>
						<a href="javascript:void(0)" style="background-image: url('/resources/images/icon-rotate.png'); background-repeat : no-repeat; padding-left:22px;" onclick="EquipStatisticChecking.rotateImage(this)">Xoay hình ảnh</a>
                    	<!-- <a href="javascript:void(0)" style="background-image: url('/resources/images/icon-printer.png'); background-repeat : no-repeat; padding-left:22px;margin-left: 12px;" onclick="printme(null,1)">In</a>
						<input type="hidden" class="DeletePhotoLink" /> -->
                    </p>
                    <div class="Clear"></div>
                    <div class="SearchInSection PhotoSearchSection" id="divDetail" style="border:0px;font-size: 0.75em; padding-left: 0px; padding-bottom: 0px;">
                		<input type="radio" class="LabelStyle cmsiscontrol" value="NOTOK" id="chkNotResult" name="formatType" style="width: 15px;float:left;margin-top: 3px;margin-right:7px;" onclick="EquipStatisticChecking.changeCheckDisplay(this);"><label class="LabelStyle cmsiscontrol" id="chkNotResultLabel" style="color: rgb(0, 153, 204);float:left;">Không đạt</label>
						<input type="radio" class="LabelStyle cmsiscontrol" value="OK" id="chkResult" name="formatType" style="width: 15px;margin-left: 22px;float:left;margin-top: 3px;margin-right:7px;" onclick="EquipStatisticChecking.changeCheckDisplay(this);"><label class="LabelStyle cmsiscontrol" id="chkResultLabel" style="color: rgb(0, 153, 204);float:left;">Đạt trưng bày</label>
						
                    	<input type="checkbox" class="cmsiscontrol" id="chkInspect" style="width: 15px;margin-left: 22px;float:left;margin-top: 3px;margin-right:7px;"/>
                    	<label class="LabelStyle cmsiscontrol" id="chkInspectLabel" style="color: rgb(0, 153, 204);float:left;">Đã kiểm tra</label>		
                    	<div class="Clear"></div>
                    	<label class="LabelLeftStyle LabelLeft3Style cmsiscontrol" id="txtNumberFaceLabel" style="color: rgb(0, 153, 204);margin-left:0px;" >Số mặt</label>
		    			<input type="text" id="txtNumberFace" class="InputTextStyle InputText4Style cmsiscontrol" style="margin-left: 3px; margin-top: 3px;" maxlength="3" />
		    			<label class="LabelLeftStyleM5 LabelLeft3Style cmsiscontrol" id="txtPOSMLabel" style="color: rgb(0, 153, 204);" >POSM</label>
		    			<input type="text" id="txtPOSM" class="InputTextStyle InputText4Style cmsiscontrol" style="margin-left: 3px; margin-top: 3px;" maxlength="6" />
		    			<div class="Clear"></div>
                    	<label class="LabelLeftStyle LabelLeft3Style cmsiscontrol" id="txtNotedLabel" style="color: rgb(0, 153, 204);margin-left:0px;" >Ghi chú</label>
		    			<input type="text" id="txtNoted" class="InputTextStyle cmsiscontrol" style="margin-left: 3px; margin-top: 3px;width: 222px;" maxlength="10" />
                    	<div class="SearchInSection SProduct1Form" style="padding-bottom: 0px;padding-top:0px;border:0px;">
					     	<button id="btnSaveDetail" class="BtnGeneralStyle BtnGeneralMStyle cmsiscontrol" onClick="EquipStatisticChecking.updateResult();">Lưu</button>
					    </div>
                    </div>
                    <p id="errMsgPopup" class="ErrorMsgStyle" style="display: none"></p>
                    <p style="display: none;margin:0px;padding:0px;" class="SuccessMsgStyle" id="successMsgPopup"></p>
                </div>
            </div>
            <div class="Clear"></div>
    	</div>
    </div>
</div>
<div style="display: none">
	<div id="easyuiPopupNumStatistic" class="easyui-dialog" title="Số lần kiểm kê" data-options="closed: true, modal: true"  style="width: 400px; height: 215px;">
		<div class="PopupContentMid">
	   		<div class="SearchInSection SProduct1Form">
				<div class="GridSection" id="gridNumStatisticDiv">
					<table id="gridNumStatistic"></table>
				</div>
			</div>        
		</div>
	</div>
</div>
<s:hidden id="solan" name="soLan"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
// 	if($('#solan').val()!=null && $('#solan').val()!= '' && Number($('#solan').val())> -1){
// 		var solan = Number($('#solan').val());
// 		var html = ' <option value="-1" selected="selected" >Tất cả</option> ';
// 		for(var i=0;i<solan;i++){
// 			html+=' <option value="'+(i+1)+'" >Lần '+(i+1)+'</option>';
// 		}
// 		$('#stepCheck').html(html).change();
// 		disableSelectbox('dateInWeek');
// 		enableSelectbox('stepCheck');
// 	}else{
// 		disableSelectbox('stepCheck');
// 		enableSelectbox('dateInWeek');
// 	}
	var params = {
			id: $('#id').val(),
			shopCode: $('#shopCode').val(),
			customerCode: $('#customerCode').val(),
			staffCode: $('#staffCode').val(),
			deviceCode: $('#deviceCode').val(),
			seri: $('#seri').val(),
			status: $('#status').val(),
			fromDate: $('#fromDate').val(),
			toDate: $('#toDate').val(),
		};
	EquipStatisticChecking._paramListStatistic = params;
	$('#grid').datagrid({
		url : "/equipment-checking/list-statistic/search",
		autoRowHeight : true,
// 		queryParams: VTUtilJS.getFormData('search-from'),
		queryParams: params,
		rownumbers : true, 
		singleSelect: true,
		pagination:true,
		pageSize: 20,
		pageList:[20, 50, 100],
		fitColumns:true,
		scrollbarSize:0,
		width: $('#gridContainer').width(),
		autoWidth: true,
	    columns:[[	        
		    {field : 'shopCode', title : 'Đơn vị', width : 120, align : 'left', sortable : false, resizable : false, formatter: function(value, row, index) {
		    	var html ="";
		    	if (row.shopCode != null && row.shopName != null) {
		    		html += row.shopCode + ' - ' + row.shopName;
		    	}
		    	return Utils.XSSEncode(html);
		    }},
		    {field : 'staffCode', title : 'Người kiểm kê', width : 120, align : 'left', sortable : false, resizable : false, formatter: function(value, row, index) {
		    	var html ="";
		    	if (row.staffCode != null && row.staffName != null) {
		    		html += row.staffCode + ' - ' + row.staffName;
		    	}
		    	return Utils.XSSEncode(html);
		    }},
		    {field : 'shortCode', title : 'Khách hàng', width : 120, align : 'left', sortable : false, resizable : false, formatter: function(value, row, index) {
		    	var html ="";
		    	if (row.customerCode != null && row.customerName != null) {
		    		html += row.customerCode + ' - ' + row.customerName;
		    	}
		    	return Utils.XSSEncode(html);
		    }},
		    {field : 'address', title : 'Địa chỉ', width : 200, align : 'left', sortable : false, resizable : false, formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},
		    {field : 'equipCode', title : 'Mã thiết bị', width : 100, align : 'left', sortable : false, resizable : false, formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},
		    {field : 'seri', title : 'Số serial', width : 100, align : 'left', sortable : false, resizable : false, formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},
		    {field : 'equipNameGroup', title : 'Nhóm thiết bị', width : 100, align : 'left', sortable : false, resizable : false, formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},
		    {field : 'equipCategoryName', title : 'Loại thiết bị', width : 100, align : 'left', sortable : false, resizable : false, formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},
		    {field : 'stepCheck', title : 'Số lần kiểm kê', width : 40, align : 'right', sortable : false, resizable : false, formatter: function(value, row, index) {
		    	if (value) {
			    	return '<a style="cursor:pointer" onclick="EquipStatisticChecking.showDlgNumStatistic(\'' + row.equipId + '\')">' + value + '</a>';
		    	}
		    	return '';
		    }},
		    {field : 'statisticDate', title : 'Ngày kiểm kê', width : 70, align : 'center', sortable : false, resizable : false, formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},
		    {field : 'status' , title : 'Tình trạng', width : 50, align : 'left', formatter : function(value, row, index) {
		    	return InventoryStatus.parseValue(row.status);
		    }},
			{field : 'detail', title : '', width : 30, align : 'center', sortable : false, resizable : false, formatter:function(value, row, index) {
				if (row.numImage != undefined && row.numImage != null && row.numImage > 0) {
					var cusCode = "";
			    	if (row.cusCodeAndName != null) {
			    		cusCode += Utils.XSSEncode(escapeQuot(row.cusCodeAndName)); 
			    	}
		    		return '<a id="viewImage_btnViewImage" class="cmsiscontrol" onclick="EquipStatisticChecking.detailImageRecord(' + $('#id').val() + ', ' + row.detailId + ',\'' + row.shopCode + '\', \'' + row.shortCode + '\', \'' + row.equipCode + '\',\'' + cusCode + '\')"><span style="cursor:pointer"><img title="Xem ảnh" src="/resources/images/icon_show_picture.png" width="20" height="20" /></span></a>';					
				}
		    }}
	    ]],	    
	    onLoadSuccess :function(data){	    	
			$('.datagrid-header-rownumber').html('STT');			
			Utils.functionAccessFillControl('gridContainer', function(data) {
			});
    		$(window).resize();
		}
	});
	
	$('#viewImage_fullGeneral2Dialog').css('width', $(window).width() - 56);
    $('#fullDialogPhotoSection').css('height', $(window).height() - 94);
    $('#fullPhotoCols1 #listImage').css('height', $(window).height() - 74 - 15 - 11);
    $('#fullPhotoCols2').css('width', $('#viewImage_fullGeneral2Dialog').width() - $('#fullPhotoCols1').width() - 1);
    $('#fullDialogPhotoSection #fullPhotoCols2').css('width', $('#viewImage_fullGeneral2Dialog').width() - $('#fullPhotoCols1').width() - 26);
    $('#fullPhotoCols1Info1').css('width', $('#viewImage_fullGeneral2Dialog').width() - $('#fullPhotoCols1').width() - 1 - 20);
    $('#fullBoxFrame2').css('width', $('#viewImage_fullGeneral2Dialog').width() - $('#fullPhotoCols1').width() - 1 - 20);
    $('#fullBoxFrame2').css('height', $(window).height() - 74 - 15 - 242);
    $('#fullDialogPhotoSection #bigImages').css('height', $('#fullDialogPhotoSection').height() - 50);
    
});
</script>
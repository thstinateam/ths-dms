<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="SearchInSection SProduct1Form">
	<div class="SearchInSection SProduct1Form">
		<div class="GridSection" id=gridShelfContainer>
			<table id="gridShelf"></table>
		</div>
	</div>	
</div>
<script type="text/javascript">
$(document).ready(function() {
// 	var status = $('#status').val();
	VTUtilJS.getFormJson({id:$('#id').val()}, '/equipment-checking/edit-shelf-product/search', function(list) {
		if(EquipStatisticChecking.status == 0) {
			list.push({isLast:true});			
		}
		$('#gridShelf').datagrid({
			data:list,
			autoRowHeight : true,
			rownumbers : true, 
			singleSelect: true,
			fitColumns:true,
			scrollbarSize:0,
			width: $('#gridShelfContainer').width(),
			autoWidth: true,
		    columns:[[	        
			    {field : 'productCode', title : 'Mã ụ kệ (F9)', width : 100, align : 'left', sortable : false, resizable : false, formatter : function(value, row, index) {
			    	if(row.isLast != null && row.isLast != undefined && row.isLast == true) {
			    		return '<input type="text" id="lastGroupF9" style="width:100%;">';
			    	} else {
			    		return Utils.XSSEncode(value);
			    	}
			    }},
			    {field : 'productName', title : 'Tên ụ kệ', width : 400, align : 'left', sortable : false, resizable : false, formatter:CommonFormatter.formatNormalCell},	    
			    {field:'delete', title:'', width: 20, align: 'center', formatter: function (value, row, index) {
			    	if(row.isLast != null && row.isLast != undefined && row.isLast ==  true) {
			    		return '';
			    	} else if (EquipStatisticChecking.status == statisticStatus.DT) {
			    		return '<a onclick="deleteEquipGroup('+row.id+')"><span style="cursor:pointer"><img title="Xóa nhóm thiết bị" src="/resources/images/icon-delete.png"/></span></a>';			    		
			    	}
			    }}
		    ]],	    
		    onLoadSuccess :function(data){	    	
				$('.datagrid-header-rownumber').html('STT');			
				$(window).resize();
				if($('#lastGroupF9').length > 0) {
					$('#lastGroupF9').unbind('keyup');
					$('#lastGroupF9').bind('keyup', function(e) {
						if(e.keyCode == keyCodes.F9) {
							showF9($('#id').val());
						}
					});					 
				}
				var rows  = $('#gridShelf').datagrid('getRows');
				if (rows != null && rows.length > 0) {
					for (var i = 0, size = rows.length; i < size; i++) {
						if (rows[i].productCode != undefined && rows[i].productCode != null && rows[i].productCode.trim().length > 0) {
							EquipStatisticChecking._lengthGridShelf = 1;
							break;
						}
					}
				}
			}
		});
	});
});
function deleteEquipGroup(id) {
	$('#errorMsg').html('').hide();
	$.messager.confirm("Xác nhận", "Bạn có muốn xóa thông tin?", function(r) {
		if(r) {
			VTUtilJS.postFormJson({id:id, recordId : $('#id').val()}, '/equipment-checking/edit-shelf-product/delete', function(data) {
				if(data.error) {
					$('#errorMsg').html(data.errMsg).show();
				} else {
					$('#successMsg').html('Xóa ụ, kệ thành công').show();
					VTUtilJS.getFormJson({id:$('#id').val()}, '/equipment-checking/edit-shelf-product/search', function(list) {
						if(EquipStatisticChecking.status == 0) {
							list.push({isLast:true});			
						}
						$('#gridShelf').datagrid('loadData', list);
					});
					setTimeout(function() {
						$('#successMsg').html('').hide();
					}, 3000);
				}
			});
		}
	});
}
function showF9(id) {
	var html = '<div id="popup-container"><div id="popup-add-group" class="PopupContentMid">\
		<div class="GeneralForm Search1Form" id="form-data">\
		<label style="width: 100px;" class="LabelStyle Label1Style">Mã ụ kệ</label>\
		<input type="text" class="InputTextStyle InputText1Style" style="width: 140px;" maxlength="50" id="code"> \
		<label style="width: 100px;"  class="LabelStyle Label1Style">Tên ụ kệ</label>\
		<input type="text" class="InputTextStyle InputText1Style" maxlength="50" style="width: 140px;" id="name"> \
		<input type="hidden" id="id" value="'+id+'">\
		<div class="Clear"></div>\
		<div class="BtnCenterSection">\
		<button class="BtnGeneralStyle" id="btn-search">Tìm kiếm</button>\
		</div>\
		<div class="Clear"></div>\
		<div class="SearchInSection SProduct1Form">\
		<div class="GridSection" id=gridSearchContainer>\
			<table id="searchGrid"></table>\
		</div>\
		</div>\
		<div class="Clear"></div>\
		<div class="BtnCenterSection">\
		<button class="BtnGeneralStyle" id="btn-update">Chọn</button>\
		<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#popup-add-group\').window(\'close\');">Đóng</button>\
		</div>\
		<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgLevelDlg"></p>\
		<p class="SuccessMsgStyle" style="display: none;" id="successMsgLevelDlg"></p>\
		</div></div></div>';
	$('body').append(html).change();
	EquipStatisticChecking.__listCheckId = new Map();
	$('#popup-add-group').dialog({
		title: 'Chọn nhóm thiết bị',
		width: 600,
		heigth: "auto",
		modal: true, close: false, cache: false,
		onOpen: function() {
			$("#popup-add-group #code").focus();
			$("#popup-add-group #code").keyup(function(event) {
				if (event.keyCode == keyCodes.ENTER) {
					$("#popup-add-group #btn-search").click();
				}
			});
			$("#popup-add-group #name").keyup(function(event) {
				if (event.keyCode == keyCodes.ENTER) {
					$("#popup-add-group #btn-search").click();
				}
			});
			$('#popup-add-group #btn-search').unbind('click');
			$('#popup-add-group #btn-search').bind('click', function() {
				$('#errMsgLevelDlg').html('').hide();
				var formParam = VTUtilJS.getFormData('form-data');
				var msg = Utils.getMessageOfSpecialCharactersValidate('popup-add-group #code', 'Mã ụ kệ', Utils._CODE);
				msg += Utils.getMessageOfSpecialCharactersValidate('popup-add-group #name', 'Tên ụ kệ', Utils._NAME);
				if(msg.length>0){
					$('#errMsgLevelDlg').html(msg).show();
					return;
				}
				if(formParam != null) {
					EquipStatisticChecking.__listCheckId = new Map();
					$('#searchGrid').datagrid('reload', formParam);					
				}
			});
			
			$('#popup-add-group #btn-update').unbind('click');
			$('#popup-add-group #btn-update').bind('click', function() {
				$('#errMsgLevelDlg').html('').hide();
				if(EquipStatisticChecking.__listCheckId.size() == 0) {
					$('#errMsgLevelDlg').html('Bạn chưa chọn ụ kệ nào. Vui lòng check chọn ụ kệ').show();
					return;
				}
				$.messager.confirm("Xác nhận", "Bạn có muốn lưu thông tin?", function(r) {
					if(r) {
						VTUtilJS.postFormJson({id:$('#id').val(), listId:EquipStatisticChecking.__listCheckId.keyArray}, '/equipment-checking/edit-shelf-product/save', function(data) {
							if(data.error) {
								$('#errMsgLevelDlg').html(data.errMsg).show();
								return;
							} else {
								data.listShelf.push({isLast:true});
								$('#gridShelf').datagrid('loadData', data.listShelf);
								$("#popup-add-group").dialog("destroy");
								$("#popup-container").remove();
							}
						});
					}
				});
			});
			$('#searchGrid').datagrid({
				url : "/equipment-checking/edit-shelf-product/dialog",
				autoRowHeight : true,
				rownumbers : true, 
				checkOnSelect: true,
				pagination:true,
				fitColumns:true,
				scrollbarSize:0,
				width: $('#gridSearchContainer').width(),
				autoWidth: true,
				queryParams :{
					id:$('#id').val()
				},
			    columns:[[	        
				    {field : 'productCode', title : 'Mã ụ kệ', width : 100, align : 'left', sortable : false, resizable : false, formatter : function(value, row, index) {
				    	return Utils.XSSEncode(value);
				    }},
				    {field : 'productName', title : 'Tên ụ kệ', width : 200, align : 'left', sortable : false, resizable : false, formatter: function(value, row, index) {
				    	return Utils.XSSEncode(value);
				    }},
				    {field:'cb', checkbox:true, align: 'center'}
			    ]],
			    onCheck:function(idx,row){
			    	EquipStatisticChecking.__listCheckId.put(row.id, row);
				},
				onUncheck:function(idx,row){
					EquipStatisticChecking.__listCheckId.remove(row.id);
				},
				onCheckAll:function(rows){
					for(var i = 0; i < rows.length; i++) {
						EquipStatisticChecking.__listCheckId.put(rows[i].id, rows[i]);
					}
				},
				onUncheckAll:function(rows){
					for(var i = 0; i < rows.length; i++) {
						EquipStatisticChecking.__listCheckId.remove(rows[i].id);
					}
				},
			    onLoadSuccess :function(data){
					$('.datagrid-header-rownumber').html('STT');			
					$(window).resize();
// 					if($.isArray(data)) {
// 						var isCheckAll=true;
// 						for(var i = 0; i < data.length; i++) {
// 							if(EquipStatisticChecking.__listCheckId.get(data.id) != null) {
// 								$('#searchGrid').datagrid('checkRow', i);
// 							} else {
// 								 isCheckAll = false;
// 							}
// 						}
// 						if(isCheckAll) {
// 							$('#gridSearchContainer .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
// 						}
// 					} else if(data.rows != null && data.rows != undefined && $.isArray(data.rows)) {
// 						var isCheckAll=true;
// 						for(var i = 0; i < data.rows.length; i++) {
// 							if(EquipStatisticChecking.__listCheckId.get(data.rows.id) != null) {
// 								$('#searchGrid').datagrid('checkRow', i);
// 							} else {
// 								 isCheckAll = false;
// 							}
// 						}
// 						if(isCheckAll) {
// 							$('#gridSearchContainer .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
// 						}
// 					}
					var rows = $('#searchGrid').datagrid('getRows');
			    	$('.datagrid-header-check input').removeAttr('checked');
			    	for(var i = 0; i < rows.length; i++) {
			    		if(EquipStatisticChecking.__listCheckId.get(rows[i].id)!=null) {
			    			$('#searchGrid').datagrid('checkRow', i);
			    		}
			    	}
				}
			});
		},
		onClose: function() {
			$("#popup-add-group").dialog("destroy");
			$("#popup-container").remove();
		}
	});
	
}
</script>
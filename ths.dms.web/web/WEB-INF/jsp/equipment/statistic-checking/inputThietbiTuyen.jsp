<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipment-checking/info">Thiết bị</a></li>
		<li><span>Nhập kiểm kê : <span><s:property value="instance.getCode()"/></span></span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">				
				<div class="SearchInSection SProduct1Form" id="search-from">
					<label class="LabelStyle Label9Style">Mã đơn vị</label>
					<div class="BoxSelect BoxSelect2">
						<s:select id="selectShop" cssClass="MySelectBoxClass" list="lstShop" listKey="id" listValue="shopCode" headerKey="-1" headerValue="Đơn vị kiểm kê" ></s:select>
					</div>
					<label style="width: 10%;" class="LabelStyle Label9Style">Nhóm thiết bị</label>
					<div class="BoxSelect BoxSelect2">
						<s:select id="selectEquipGroup" cssClass="MySelectBoxClass" list="listEquipGroup" headerKey="-1" headerValue="Tất cả" listKey="id" listValue="code"></s:select>
					</div>
					<label class="LabelStyle Label9Style">Mã thiết bị</label>
					<input id="equipCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					
					<div class="Clear"></div>
					<label class="LabelStyle Label9Style">Mã kho</label>
					<div class="BoxSelect BoxSelect2">
						<select id="selectStock" class="MySelectBoxClass">
							<option value="-1">Tất cả</option>
						</select>
					</div>
					<label class="LabelStyle Label1Style">Ngày</label>
					<input id="checkDate" type="text" class="InputTextStyle InputText1Style vinput-date" maxlength="20" call-back="VTValidateUtils.getMessageOfInvalidFormatDate('checkDate', 'Ngày');"/>
					<label class="LabelStyle Label1Style">Số seri</label>
					<input id="seri" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					
					<div class="Clear"></div>
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnLoad" class="BtnGeneralStyle" onclick="loadDetailData()">Lấy dữ liệu</button>
						</div>
					</div>
					<div class="Clear"></div>
					<p class="ErrorMsgStyle SpriteErr" id="errorMsg" style="display: none;"></p>
						<p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
			</div>
			<h2 class="Title2Style">Danh sách kiểm kê
				<div style="float:right; margin-top: -5px;margin-right: 27px;">
					<a href="#" class="downloadTemplateReport"><img src="/resources/images/icon_download_32.png" height="20" width="20" style="border-left-color: blue; border-left-style: solid; border-left-width: 2px;" style="margin-right: 5px;" title="Tải tập tin mẫu"></a>
					<a href="javascript:void(0);" onclick="EquipStatisticChecking.showDlgImportExcelKK();"><img src="/resources/images/icon_table_import.png" height="20" width="20" style="margin-right: 5px;" title="Nhập excel"></a>
					<a  onclick = "EquipStatisticChecking.exportExcelDetailGroupTuyen();" style = "float: right;	padding: 0px 25px 0px 0px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_export.png" title="Xuất Excel"></a>
				</div>
			</h2>
			<div class="SearchInSection SProduct1Form">
				<div class="GridSection" id=gridDetailContainer>
					<table id="detailGrid"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Import - Kiểm kê thiết bị dạng tuyến" data-options="closed:true,modal:true" style="width:465px;height:250px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-checking/import/group-tuyen" name="importFrmBBTL" id="importFrmBBTL" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">
						<input id="fakefilepcBBTL" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileBBTL" onchange="previewImportExcelFile(this,'importFrmBBTL','fakefilepcBBTL', 'errExcelMsgBBTL');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a onclick="" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadExcelFile" onclick="return EquipStatisticChecking.importExcelProductTuyen();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgBBTL"/>
	</div>	
</div>
<div id="popup-images" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogPhotoSection FixFloat">
        	<div class="PhotoCols1">
              	<div id="listImage" class="DScrollpane">
              		<ul id="listImageUL" class="ResetList PhotoCols2List">
              		</ul>
            	</div>
            </div>
            <div class="PhotoCols2">
            	<div class="PhotoCols1Info1">
                	<p onclick="EquipStatisticChecking.showImageNextPre(0);" class="LeftStyle Sprite1"></p>
                	<div class="PhotoLarge"><span class="BoxFrame BoxFrame2"><span class="BoxMiddle">
                		<img id="bigImages" class="ImageMap ImageFrame4" width="752" height="508"/>
                		<img style="display:none" id="loadingImages" width="50" height="50" src="/resources/images/loading.gif">
                	</span></span></div>
                    <p onclick="EquipStatisticChecking.showImageNextPre(1);" class="RightStyle Sprite1"></p>
                </div>
                <div class="PhotoCols1Info3">
                	<div id="divMapSection" class="Map2Section SmallMapSection">
                    	<%-- <p class="OnFucnStyle" onclick="EquipStatisticChecking.onOffMap(1);" style="display:none;"><span class="HideText Sprite1">Mở rộng</span></p>
                    	<p class="OffFucnStyle" onclick="EquipStatisticChecking.onOffMap(0);" style="display:none;"><span class="HideText Sprite1">Thu nhỏ</span></p> --%>
                        <div class="MapSection">
                    		<div id="bigMap" style="width: 140px; height: 120px; position: relative;"></div>
                        </div>
                    </div>
					<p class="Text0Style" id="shopImage"></p>
                	<p class="Text1Style" id="staffImage"></p>
                	<p class="Text1Style" id="equipImage"></p>
                    <p style="padding-bottom: 5px;" class="Text2Style">Thời gian chụp: <span id="timeImage"></span></p>
                    <p class="Text3Style ImagePrintingClass">
	                    <a href="javascript:void(0);" class="Sprite1 DownloadPhotoLink">Tải hình ảnh</a>
						<a href="javascript:void(0)" style="background-image: url('/resources/images/icon-rotate.png'); background-repeat : no-repeat; padding-left:22px;" onclick="EquipStatisticChecking.rotateImage(this)">Xoay hình ảnh</a>
                    	<!-- <a href="javascript:void(0)" style="background-image: url('/resources/images/icon-printer.png'); background-repeat : no-repeat; padding-left:22px;margin-left: 12px;" onclick="printme()">In</a> -->
						<input type="hidden" class="DeletePhotoLink" />
                    </p>
                    <p id="errMsgPopup" class="ErrorMsgStyle" style="display: none"></p>
                </div>
            </div>
            <div class="Clear"></div>
    	</div>
    </div>
</div>
<div id="popup-images-full" style="display:none;">
	<div class="GeneralDialog General2Dialog" id="fullGeneral2Dialog">
      	<div class="DialogPhotoSection FixFloat" id="fullDialogPhotoSection">
        	<div class="PhotoCols1" id="fullPhotoCols1">
              	<div id="listImage" class="DScrollpane">
              		<ul id="listImageUL" class="ResetList PhotoCols2List">
              		</ul>
            	</div>
            </div>
            <div class="PhotoCols2" id="fullPhotoCols2">
            	<div class="PhotoCols1Info1" id="fullPhotoCols1Info1">
                	<p onclick="EquipStatisticChecking.showImageNextPre(0);" class="LeftStyle Sprite1"></p>
                	<div class="PhotoLarge"><span class="BoxFrame BoxFrame2" id="fullBoxFrame2"><span class="BoxMiddle">
                		<img id="bigImages" class="ImageMapFull ImageFrame4Full" width="752" height="508"/>
                		<img style="display:none; top: 0px;" id="loadingImages" width="50" height="50" src="/resources/images/loading.gif">
                	</span></span></div>
                    <p onclick="EquipStatisticChecking.showImageNextPre(1);" class="RightStyle Sprite1"></p>
                </div>
                <div class="PhotoCols1Info3">
                	<div id="divMapSection" class="Map2Section SmallMapSection">
                    	<%-- <p class="OnFucnStyle" onclick="EquipStatisticChecking.onOffMap(1);" style="display:none;"><span class="HideText Sprite1">Mở rộng</span></p>
                    	<p class="OffFucnStyle" style="top: 9px;" onclick="EquipStatisticChecking.onOffMap(0);"><span class="HideText Sprite1">Thu nhỏ</span></p> --%>
                        <div class="MapSection">
                    		<div id="bigMap" style="width: 140px; height: 115px; position: relative;"></div>
                        </div>
                    </div>
					<p class="Text0Style" id="shopImage"></p>
                	<p class="Text1Style" id="staffImage"></p>
                	<p class="Text1Style" id="equipImage"></p>
                    <p style="padding-bottom: 5px;" class="Text2Style">Thời gian chụp: <span id="timeImage"></span></p>
                    <p class="Text3Style ImagePrintingClassFull">
	                    <a href="javascript:void(0);" class="Sprite1 DownloadPhotoLink">Tải hình ảnh</a>
						<a href="javascript:void(0)" style="background-image: url('/resources/images/icon-rotate.png'); background-repeat : no-repeat; padding-left:22px;" onclick="EquipStatisticChecking.rotateImage(this)">Xoay hình ảnh</a>
                    	<!-- <a href="javascript:void(0)" style="background-image: url('/resources/images/icon-printer.png'); background-repeat : no-repeat; padding-left:22px;margin-left: 12px;" onclick="printme(null,1)">In</a>
						<input type="hidden" class="DeletePhotoLink" /> -->
                    </p>
                    <p id="errMsgPopup" class="ErrorMsgStyle" style="display: none"></p>
                </div>
            </div>
            <div class="Clear"></div>
    	</div>
    </div>
</div>
<div style="display: none" id="easyuiPopupImportExcelContainer">
  	<div id="easyuiPopupImportExcelEx" class="easyui-dialog" title="Import - Đơn vị" data-options="closed:true,modal:true" style="width:465px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<div>
					<div class="UploadMediaBox">
							<div class="MultiMediaBox">
								<input type="file" id="imageFile" class="uploadFileImage" name="imageFile"></input>
						</div>
						<p id="imageMsg" class="ErrorMsgStyle SpriteErr" style="display: none;"></p>
						<s:hidden id="countSuccess" value="0"></s:hidden>
						<s:hidden id="countFail" value="0"></s:hidden>
						<div class="Clear"></div>
						<div class="uploadifyQueue" id="fileQueue"></div>
					</div>	
				</div>								
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadImageFile">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcelEx').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;" class="ErrorMsgStyle SpriteErr" id="errExcelMsgDVKK"/>
		<p id="successExcelMsgDVKK" class="SuccessMsgStyle" style="display: none"></p>
	</div>	
</div>
<s:hidden id="id" name="id"></s:hidden>
<s:hidden id="errorValue" value="0"></s:hidden>
<s:hidden id="mediaType" name="mediaType"></s:hidden>
<s:hidden id="fromDate" name="fromDate"></s:hidden>
<s:hidden id="toDate" name="toDate"></s:hidden>
<script type="text/javascript">
loadDetailData = function() {
	$('#divOverlay').show();
	$('.ErrorMsgStyle').html('').hide();
	var shopId = $('#selectShop').val();
	var equipGroupId = $('#selectEquipGroup').val();
	var equipCode = $('#equipCode').val().trim();
	var stockId = $('#selectStock').val();
	var checkDate = $('#checkDate').val();
	var seri = $('#seri').val();
	var obj = new Object();
	if(shopId != null && shopId != -1) {
		obj.shopId = shopId;
	}
	if(equipGroupId != null && equipGroupId != -1) {
		obj.equipGroupId = equipGroupId;
	}
	obj.equipCode = equipCode;
	if(stockId != null && stockId != -1) {
		obj.stockId = stockId;
	}
	if(checkDate != null) {
		obj.checkDate = checkDate;
	}
	var msg = '';
	if(checkDate=='' || checkDate == '__/__/____'){
		msg = 'Bạn vui lòng chọn Ngày';
	}
	if(msg.length ==0 && checkDate != '' && !Utils.isDate(checkDate)){
		msg = 'Ngày không tồn tại hoặc không đúng định dạng dd/mm/yyyy.';
	}
	var fromDate = $('#fromDate').val();
	var toDate = $('#toDate').val();
	if(msg.length ==0 && (Utils.compareDateForTowDate(checkDate,fromDate)==-1 || (toDate!=''&&Utils.compareDateForTowDate(checkDate,toDate)==1))){
		msg = 'Ngày phải nằm trong khoảng kiểm kê!';
	}
	if(msg.length !=0){
		$('#errorMsg').html(msg).show();
		$('#checkDate').focus();
		return false;
	}
	obj.seri = seri;
	EquipStatisticChecking.filter = new Object();
	EquipStatisticChecking.filter.seri = seri;
	obj.isFirstLoad = false;
	EquipStatisticChecking.filter.isFirstLoad = false;
	EquipStatisticChecking.filter.checkDate = checkDate;
	EquipStatisticChecking.filter.stockId = stockId;
	EquipStatisticChecking.filter.equipCode = equipCode;
	EquipStatisticChecking.filter.equipGroupId = equipGroupId;
	EquipStatisticChecking.filter.shopId = shopId;
	$('#detailGrid').datagrid('load', obj);
};
$(document).ready(function() {
	ReportUtils.setCurrentDateForCalendar("checkDate");
	$('#selectShop').bind('change', function(e) {
		VTUtilJS.getFormJson({shopId:$('#selectShop').val()}, '/equipment-checking/list-shop-stock', function(result) {
			if(!result.error && $.isArray(result.listStock)) {
				var html = '<option value="-1">Tất cả</option>';
				for(var i = 0; i < result.listStock.length; i++) {
					html += '<option value="'+result.listStock[i].id+'">'+result.listStock[i].code+'</option>';
				}
				$('#selectStock').html('');
				$('#selectStock').html(html);
				$('#selectStock').change();
			}
		});
	});
	
	$('#detailGrid').datagrid({
		url :'/equipment-checking/input/group-tuyen-load?id='+$('#id').val(),
		autoRowHeight : true,
		rownumbers : true, 
		pagination:true,
		pageList: [50, 100, 200, 500],
		pageSize:50,
		fitColumns:true,
		singleSelect:true,
		scrollbarSize:0,		
		width: $('#gridDetailContainer').width(),
		autoWidth: true,
		queryParams:{
			isFirstLoad : true
		},
		columns:[[
			{field : 'shopCode', title : 'Đơn vị', width : 50, align : 'left', sortable : false, resizable : false, formatter: CommonFormatter.formatNormalCell},
			{field : 'shopName', title : 'Tên đơn vị', width : 100, align : 'left', sortable : false, resizable : false, formatter: CommonFormatter.formatNormalCell},
			{field : 'stock' , title : 'Kho', width : 80, align : 'left', formatter:function(value, row, index) {
		    	var html ="";
		    	if(row.stockCode != null && row.stockName != null){
		    		html += row.stockCode + ' - ' + row.stockName;
		    	}
		    	return Utils.XSSEncode(html);
		    }},
			{field : 'equipCodeGroup' , title : 'Nhóm thiết bị', width : 100, align : 'left'},
			{field : 'equipCode' , title : 'Mã thiết bị', width : 90, align : 'left'},
			{field : 'seri' , title : 'Seri', width : 50, align : 'left'},
		    {field : 'checkDate', title : 'Ngày', width : 60, align : 'right', sortable : false, resizable : false},
		    {field : 'isBelong', title : 'Hiện còn', width : 50, align : 'center', sortable : false, resizable : false, 
		    	editor: {
					type:'checkbox',
					options:{on:1,off:0}
				}
		    },
		    {field : 'detail', title : '<a onclick="EquipStatisticChecking.saveGroupChecking('+$('#id').val()+')"><span style="cursor:pointer"><img title="Lưu" src="/resources/images/icon-save.png" width="20" height="20" /></span></a>', width : 50, align : 'center', sortable : false, resizable : false, formatter:function(value, row, index) {
		    	var html ='';
		    	if(row.numImage>0){
	    	 		html = '<a id="'+row.detailId+'" onclick="EquipStatisticChecking.detailImageRecord('+$('#id').val()+', '+row.detailId+')"><span style="cursor:pointer"><img title="Xem ảnh" src="/resources/images/icon_show_picture.png" width="20" height="20" /></span></a>';
	    		}else{
		    	 	html = '<a  id="'+row.detailId+'" style="display:none" onclick="EquipStatisticChecking.detailImageRecord('+$('#id').val()+', '+row.detailId+')"><span style="cursor:pointer"><img title="Xem ảnh" src="/resources/images/icon_show_picture.png" width="20" height="20" /></span></a>';
		    		
	    		}    	    	
		    	html += '<a style="padding-left: 20px;" onclick="EquipStatisticChecking.uploadImageRecord('+$('#id').val()+', '+row.detailId+', '+row.numImage+')"><span style="cursor:pointer"><img title="Upload hình ảnh kiểm kê" src="/resources/images/icon_login.png" width="20" height="20" /></span></a>';
		    	return html;
		    }}
		]],
		onLoadSuccess :function(data){
			if($.isArray(data)) {
				for(var i = 0; i < data.length; i++) {
					$('#detailGrid').datagrid('beginEdit', i);
				}
			} else if($.isArray(data.rows)) {
				for(var i = 0; i < data.rows.length; i++) {
					$('#detailGrid').datagrid('beginEdit', i);
				}
			}
			$('#divOverlay').hide();
		}
	});
	$('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_import_kiem_ke_thiet_bi_tuyen.xlsx');
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
#player_api {
	width: 678px !important;
}
</style>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">Thiết bị</a></li>
		<li><span>Kiểm kê</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">				
				<div class="SearchInSection SProduct1Form" id="search-from">
					<label class="LabelStyle Label9Style">Đơn vị </label>
					<input id="shop" type="text"  style="height: auto;" class="InputTextStyle InputText1Style"/>
					<label class="LabelStyle Label9Style">Mã kiểm kê</label>
					<input id="checkingCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
					<label class="LabelStyle Label9Style">Tên kiểm kê</label>
					<input id="checkingName" type="text" class="InputTextStyle InputText1Style" maxlength="100" />
					<div class="Clear"></div>
					
					<label class="LabelStyle Label9Style">Từ ngày</label>
					<input id="fromDate" type="text" class="InputTextStyle InputText1Style vinput-date" maxlength="20" call-back="VTValidateUtils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');"/>
					<label class="LabelStyle Label9Style" style="width: 10%;">Đến ngày</label>
					<input id="toDate" type="text" class="InputTextStyle InputText1Style vinput-date"  maxlength="20" call-back="VTValidateUtils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');VTValidateUtils.getMessageCheckToDate('fromDate', 'Từ ngày', 'toDate', 'Đến ngày');"/>
					<label class="LabelStyle Label9Style" style="width: 10%;">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="status">
							<option value="-1" selected="selected">Tất cả</option>
							<option value="0" >Dự thảo</option>
							<option value="1" >Hoạt động</option>
							<option value="2" >Hoàn thành</option>
							<option value="3" >Hủy</option>
						</select>
					</div>
					<div class="Clear"></div>
					
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol" onclick="$('#grid').datagrid('reload', VTUtilJS.getFormData('search-from')), EquipStatisticChecking._selectingEquipmentStatistics = new Map();">Tìm kiếm</button>
						</div>
					</div>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>	
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
				</div>
				<h2 class="Title2Style">Danh sách kiểm kê
					<div style="float:right; margin-top: -5px; margin-right: 27px;">
						<button style="float:right;" id="grp_save_btnSaveLst" class="BtnGeneralStyle cmsiscontrol" onclick="EquipStatisticChecking.updateEquipmentStatisticStatus();" >Lưu</button>
						<div id= "grp_save_CbxStatus" style="float:right;margin-right: 5px;" class="BoxSelect BoxSelect2 cmsiscontrol">							
							<select class="MySelectBoxClass InputTextStyle" id="cbxStatusUpdate">
								<option value="-1" selected="selected">Chọn</option>
							    <option value="1">Hoạt động</option>
							    <option value="2">Hoàn thành</option>
							    <option value="3">Hủy</option>
							</select>
						</div>
						<a id= "btnExport" class= "cmsiscontrol" onclick="EquipStatisticChecking.exportExcel();" style="float: right; padding: 0px 5px 0px 0px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_export.png" title="Xuất Excel"></a>
					</div>
				</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="grid"></table>
					</div>
				</div>
				<div id="gridDetail" style="display: none;" class="SearchInSection SProduct1Form">									
					<h2 class="Title2Style">Danh sách nhóm thiết bị</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="SearchInSection SProduct1Form">
							<div class="GridSection" id=gridDetailContainer>
								<table id="detailGrid"></table>
							</div>
						</div>	
					</div>
				</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	ReportUtils.setCurrentDateForCalendar("fromDate");
	ReportUtils.setCurrentDateForCalendar("toDate");
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });
	var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper"); 
	EquipStatisticChecking._lstRowId = new Array();
	$('#grid').datagrid({
		url : "/equipment-checking/search",
		autoRowHeight : true,
		rownumbers : true, 
		//singleSelect: true,
		pagination:true,
		pageList: [50],
		pageSize:50,
		fitColumns:true,
		scrollbarSize:0,		
		width: $('#gridContainer').width(),
		autoWidth: true,
		queryParams:{
			fromDate: $("#fromDate").val(),
			toDate: $("#toDate").val()
		},
	    columns:[[
	    	{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left'},
	    	{field: 'code' , title: 'Mã kiểm kê', width: 120, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field: 'name' , title : 'Tên kiểm kê', width : 180, align : 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field: 'fromDate', title : 'Từ ngày', width : 80, align : 'center', sortable : false, resizable : false, formatter:CommonFormatter.dateTimeFormatter},
		    {field: 'toDate', title : 'Đến ngày', width : 80, align : 'center', sortable : false, resizable : false, formatter:CommonFormatter.dateTimeFormatter},	    
		    {field: 'recordStatus', title: 'Trạng thái', width: 80, align: 'left', sortable:false, resizable: true, formatter: function(value, row, index){
		    		// if(value == 0){
		    		// 	return 'Dự thảo';
		    		// }else if(value == 1){
		    		// 	return 'Hoạt động';
		    		// }else if(value == 2){
		    		// 	return 'Hoàn thành';
		    		// } else if(value == 3) {
		    		// 	return 'Hủy';
		    		// }

		    	return statisticStatus.parseValue(value);
		    }},
		    {field:'edit', title:'<a id="btnNew" class="cmsiscontrol" href="/equipment-checking/edit"><span style="cursor:pointer"><img style="text-align: center;" title="Thêm mới" src="/resources/images/icon_add.png"/></span></a>', width: 45, align: 'center', sortable: false, resizable: false, formatter: function (value, row, index) {
				var html = ''; 
				//html += '<a onclick="EquipStatisticChecking.detailChecking('+row.id+')"><span style="cursor:pointer"><img title="Xem nhóm thiết bị" src="/resources/images/icon-view.png"/></span></a>';
				if(statisticStatus.HD == row.recordStatus){
					html = '<a id="btnViewResult' + row.id + '" class="cmsiscontrol" href="/equipment-checking/list-statistic?id=' + row.id + '"><span style="cursor:pointer"><img style="text-align: center" title="Kết quả kiểm kê" src="/resources/images/icon_detail.png"/></span></a>'; 
					html += '&emsp;';
					var now = new Date();
					var cYear = now.getFullYear();
					var cMonth = now.getMonth() + 1;
					var cDate = now.getDate();
					var currentDate = cDate+'/'+cMonth+'/'+cYear;
					
// 					if(Utils.compareDate(row.fromDateStr,currentDate) && (row.toDateStr == null || Utils.compareDate(currentDate,row.toDateStr))){
// 						html += '<a href="/equipment-checking/input?id='+row.id+'"><span style="cursor:pointer"><img style="width:20px; height:20px; text-align:center" title="Nhập kiểm kê" src="/resources/images/form_input.png"/></span></a>';
// 						html += '&emsp;';
// 					}
					html += '<a id="btnEdit' + row.id + '" class="cmsiscontrol" href="/equipment-checking/edit?id='+row.id+'"><span style="cursor:pointer"><img style="text-align: center;" title="Chỉnh sửa" src="/resources/images/icon_edit.png"/></span></a>';
				}
				if(statisticStatus.DT == row.recordStatus){				
					html += '<a id="btnEdit' + row.id + '" class="cmsiscontrol" href="/equipment-checking/edit?id='+row.id+'"><span style="cursor:pointer"><img style="text-align: center;" title="Chỉnh sửa" src="/resources/images/icon_edit.png"/></span></a>';
				}
				if(statisticStatus.HT == row.recordStatus){
					html = '<a id="btnViewResult' + row.id + '" class="cmsiscontrol" href="/equipment-checking/list-statistic?id='+row.id+'"><span style="cursor:pointer"><img style="text-align: center" title="Kết quả kiểm kê" src="/resources/images/icon_detail.png"/></span></a>';
				}
				return html;
		    }}
	    ]],	    
	    onLoadSuccess :function(data){	    	
			$('.datagrid-header-rownumber').html('STT');			
	    	$('.datagrid-header-check input').removeAttr('checked');
        	var rows = $('#grid').datagrid('getRows');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(EquipStatisticChecking._selectingEquipmentStatistics.get(rows[i].id)!=null) {
	    			$('#grid').datagrid('checkRow', i);
	    		}
	    	}
	    	Utils.functionAccessFillControl('gridContainer', function(data) {
			});
    		$(window).resize();
		},
	    onCheck:function(index,row){
			EquipStatisticChecking._selectingEquipmentStatistics.put(row.id,row);   
			EquipStatisticChecking._lstRowId.push(row.id);
	    },
	    onUncheck:function(index,row){
	    	EquipStatisticChecking._selectingEquipmentStatistics.remove(row.id);
			var position = EquipStatisticChecking._lstRowId.indexOf(row.id);
	    	if(position != null){
	    		EquipStatisticChecking._lstRowId.splice(position,1);
	    	}
	    },
	    onCheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		EquipStatisticChecking._selectingEquipmentStatistics.put(row.id,row);
				var position = EquipStatisticChecking._lstRowId.indexOf(row.id);
				if(position == -1){
					EquipStatisticChecking._lstRowId.push(row.id);
				}
	    	}
	    },
	    onUncheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		EquipStatisticChecking._selectingEquipmentStatistics.remove(row.id);	
				var position = EquipStatisticChecking._lstRowId.indexOf(row.id);
				if(position != null){
					EquipStatisticChecking._lstRowId.splice(position,1);
				}		
	    	}
	    }
	});
});
</script>
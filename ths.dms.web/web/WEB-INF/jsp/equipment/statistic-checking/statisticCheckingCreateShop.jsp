<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="SearchInSection SProduct1Form">		
	<div class="Title2Style" style="height:15px;font-weight: bold;">
		<span style="float:left;">Thông tin tìm kiếm</span>
	</div>		
	<div class="Clear"></div>
	<div class="SearchInSection SProduct1Form">
		<label class="LabelStyle" style="width:100px;">Mã đơn vị</label>
		<input id="txtCodeShop" style="width: 10%;" class="InputTextStyle InputText12Style" maxlength="40" />
		<label class="LabelStyle" style="width:100px;">Tên đơn vị</label>
		<input id="txtNameShop" class="InputTextStyle InputText3Style" maxlength="100" />
		<button class="BtnGeneralStyle cmsiscontrol" id="btnSearchTabShop" style="margin-left: 20px;">Tìm kiếm</button>
		<div class="Clear"></div>
		<div class="Title2Style" style="height:15px;font-weight: bold;">
			<span style="float:left;">Danh sách đơn vị</span>
			<div style="float:right; margin-top: -5px;margin-right: 27px;">
				<a id="tabShop_btnExport" class="cmsiscontrol" href="javascript:void(0);" onclick="EquipStatisticChecking.exportExcelDVKK();" style="margin-right: 5px;"><img src="/resources/images/icon_table_export.png" height="20" width="20" title="Xuất excel" style="margin-right: 5px;"></a>		
			</div>
		</div>		
		<div class="GridSection" id="gridGroupShopContainer">
			<table id="gridShop"></table>
		</div>
	</div>	
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel1" class="easyui-dialog" title="Import - Đơn vị" data-options="closed:true,modal:true" style="width:465px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-checking/edit-shop-import-excel" name="importFrmDVKK1" id="importFrmDVKK1" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">		    
						<input id="fakefilepcDVKK1" type="text" class="InputTextStyle InputText1Style">						
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileDVKK1" onchange="previewImportExcelFile(this,'importFrmDVKK1','fakefilepcDVKK1', 'errExcelMsgDVKK1');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a onclick="" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadExcelFile" onclick="return EquipStatisticChecking.importExcelDVKK();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel1').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgDVKK1"/>
	</div>	
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_import_don_vi_kiem_ke.xlsx');
	$('#txtCodeShop').focus();
	EquipStatisticChecking.status = $("#status").val().trim();
	var vtitle = "";
	if (EquipStatisticChecking.status == statisticStatus.DT) {
		vtitle = "<a id='tabShop_btnNew' class='cmsiscontrol' onclick='EquipStatisticChecking.showShopDlg();'><img title='Thêm mới' width='17' height='17' src='/resources/images/icon-add.png'></a>";
	}
	$("#btnSearchTabShop").click(function() {
		var p = {
				recordId : $("#id").val(),
				shopSearchCode: $("#txtCodeShop").val().trim(),
				shopSearchName: $("#txtNameShop").val().trim()
		};
		//$("#add-shopPopup #gridDlg").treegrid("load", p);
		$("#gridShop").treegrid({url: "/equipment-checking/edit-shop/search",queryParams : p});
	});
	$("#txtNameShop, #txtCodeShop").keyup(function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$("#btnSearchTabShop").click();
		}
	});
	$("#gridShop").treegrid({
		url: "/equipment-checking/edit-shop/search",
		queryParams: {recordId:$("#id").val()},
		rownumbers: false,
		width: $("#gridGroupShopContainer").width() - 10,
		height: "auto",
		fitColumns: true,
		idField: "nodeId",
		treeField: "text",
		scrollbarSize:0,
		columns: [[
			{field:"no", title:"STT", sortable: false, resizable: false, width:45, fixed:true, align:"center", formatter:function(v, r) {
				return "";
			}},
			{field:"text", title:"Đơn vị", sortable: false, resizable: false, width:200, align:"left", formatter:CommonFormatter.formatNormalCell},
			{field:"view", title:vtitle, sortable: false, resizable: false, width:30, fixed:true, align:"center", formatter: function(v, r) {
				if (r.attr.isNPP != 1) {
	        		if (EquipStatisticChecking.status == statisticStatus.DT) {
	        			return '<a id="tabShop_btnNew' + r.attr.id + '" class="cmsiscontrol" onclick="EquipStatisticChecking.showShopDlg('+r.attr.id+');"><img title="Thêm mới" width="17" height="17" src="/resources/images/icon-add.png"></a>';
	        		}
	        	} else {
	        		return '';
	        	}
			}},
			{field:"delete", title:"", sortable: false, resizable: false, width:30, fixed:true, align:"center", formatter: function(v, r) {
				if(EquipStatisticChecking.status == statisticStatus.DT){
					if(r.attr.isCheck == 1) {
						return '<a id="tabShop_btnDel' + r.attr.id + '" class="cmsiscontrol" onclick="EquipStatisticChecking.deleteShopMap('+r.attr.id+');"><img title="Xóa" width="17" height="17" src="/resources/images/icon-delete.png"></a>';						
					} else {
						return '';
					}
        		} else {
        			return '';
        		}
			}}
		]],
		onLoadSuccess: function(row, data) {
			var i = 1;
			$("#gridGroupShopContainer .datagrid-btable td[field=no] div").each(function() {
				$(this).html(i);
				i++;
			});
			Utils.functionAccessFillControl();
    		$(window).resize();
		}
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!-- popup thiet bi kiem ke -->
<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchEquipment" class="easyui-dialog"
		title="Chọn thiết bị"
		data-options="closed:true,modal:true" >
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label6Style">Mã thiết bị</label>
				<input id="equipmentCode" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label7Style">Số serial</label>
				<input id="seriNumber" type="text" class="InputTextStyle InputText4Style" maxlength="100"/>
				<label class="LabelStyle Label7Style">Loại</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="typeEquipment" onchange="EquipStatisticChecking.onChangeTypeEquip(this);">
						<option value="">Tất cả</option>
					</select>
				</div>
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Nhóm thiết bị</label>
				<div class="BoxSelect BoxSelect3">
					<!-- <select class="MySelectBoxClass" id="groupEquipment" onchange="">
						<option value="">Tất cả</option>
					</select> -->
					<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
					<select id="groupEquipment" style="width:193px !important;"></select>
				</div>
				<label class="LabelStyle Label7Style">Nhà cung cấp</label>
				<div class="BoxSelect BoxSelect3">
					<!-- <select class="MySelectBoxClass" id="providerId" onchange="">
						<option value="">Tất cả</option>
					</select> -->
					<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
					<select id="providerId" style="width:193px !important;"></select>
				</div>					
				<label class="LabelStyle Label7Style">Năm sản xuất</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="yearManufacturing">
					</select>
				</div>
				<!-- <div id="contractDateDiv">
					<input type="text" id="yearManufacturing" class="InputTextStyle InputText5Style"/>
				</div> -->
<!-- 				<div class="Clear"></div> -->
<!-- 				<label class="LabelStyle Label6Style">Kho</label> -->
<!-- 				<input type="text" id="stockCode" class="InputTextStyle InputText4Style"  /> -->
<!-- 				<label class="LabelStyle Label7Style">Đơn vị</label> -->
<!-- 				<input type="text" id="shopCode" class="InputTextStyle InputText4Style"  /> -->
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Khách hàng</label>
				<input id="customerCode" type="text" class="InputTextStyle InputText3Style" style="width: 511px;" autocomplete="off" maxlength="300" placeholder="Nhập mã khách hàng hoặc tên khách hàng hoặc địa chỉ"/>
<!-- 				<label class="LabelStyle Label6Style">Mã KH</label> -->
<!-- 				<input type="text" id="customerCode" class="InputTextStyle InputText4Style"  />				 -->
<!-- 				<label class="LabelStyle Label7Style">Tên KH</label> -->
<!-- 				<input type="text" id="customerName" class="InputTextStyle InputText4Style"  />				 -->
<!-- 				<label class="LabelStyle Label7Style">Địa chỉ</label> -->
<!-- 				<input type="text" id="customerAddress" class="InputTextStyle InputText4Style"  />				 -->
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="EquipStatisticChecking.searchEquipment();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="equipmentGridDialogContainer">
					<table id="equipmentGridDialog" ></table>
					<!-- class="easyui-datagrid" -->
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentAdd" class="BtnGeneralStyle"	onclick="EquipStatisticChecking.addEquipment();">Lưu</button>
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchEquipment').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
// 	$('#typeEquipment,#groupEquipment,#providerId,#yearManufacturing').customStyle();
});
</script>

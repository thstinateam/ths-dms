<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
#player_api {
	width: 678px !important;
}
/* span.CustomStyleSelectBox { 
	float: left;
	width: 70px;
} */
</style>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipment-checking/info">Thiết bị</a></li>
		<li><span>Tạo kiểm kê</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">				
				<div class="SearchInSection SProduct1Form" id="search-from">
					<label class="LabelStyle Label9Style">Mã kiểm kê<span class="ReqiureStyle"> *</span></label>
					<s:if test="id != null && id > 0">
						<input id="checkingCode" type="text" disabled="disabled" class="InputTextStyle InputText1Style" maxlength="50" value='<s:property value="instance.code"/>'/>
						<s:hidden id="id" name="id"></s:hidden>
					</s:if>
					<s:else>
						<input id="checkingCode" type="text" call-back="VTValidateUtils.getMessageOfRequireCheck('checkingCode', 'Mã kiểm kê', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate('checkingCode', 'Mã kiểm kê', Utils._CODE)" class="InputTextStyle InputText1Style" maxlength="50" />
					</s:else>
					<label class="LabelStyle Label9Style">Tên kiểm kê<span class="ReqiureStyle"> *</span></label>
					<s:if test="instance.recordStatus == 1">
						<input id="checkingName" disabled="disabled" type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck('checkingName', 'Tên kiểm kê', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate('checkingName', 'Tên kiểm kê', Utils._NAME)" maxlength="250" value='<s:property value="instance.name"/>'/>
					</s:if>
					<s:else>
						<input id="checkingName" type="text" class="InputTextStyle InputText1Style" call-back="VTValidateUtils.getMessageOfRequireCheck('checkingName', 'Tên kiểm kê', false, true);VTValidateUtils.getMessageOfSpecialCharactersValidate('checkingName', 'Tên kiểm kê', Utils._NAME)" maxlength="250" value='<s:property value="instance.name"/>'/>
					</s:else>
					
					<label class="LabelStyle Label9Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect4">
						<s:if test="instance.recordStatus == 0">
							<select class="MySelectBoxClass InputTextStyle" id="status">
								<option value="0" selected="selected">Dự thảo</option>
								<option value="1" >Hoạt động</option>
								<option value="3" >Hủy</option>
							</select>
						</s:if>
						<s:elseif test="instance.recordStatus == 1">
							<select class="MySelectBoxClass InputTextStyle" id="status">
								<option value="1" selected="selected">Hoạt động</option>
								<option value="2" >Hoàn thành</option>
								<!-- <option value="3" >Hủy</option> -->
							</select>
						</s:elseif>
						<s:elseif test="instance.recordStatus == 2">
							<select class="MySelectBoxClass InputTextStyle" id="status">
								<option value="2" selected="selected">Hoàn thành</option>
								<!-- <option value="3" >Hủy</option> -->
							</select>
						</s:elseif>
						<s:elseif test="instance.recordStatus == 3">
							<select class="MySelectBoxClass InputTextStyle" id="status">
								<option value="3" selected="selected">Hủy</option>
							</select>
						</s:elseif>
						<s:else>
							<select class="MySelectBoxClass InputTextStyle" id="status">
								<option value="0" >Dự thảo</option>
							</select>
						</s:else>
					</div>
										
					<div class="Clear"></div>
					<label class="LabelStyle Label9Style">Từ ngày<span class="ReqiureStyle"> *</span></label>
					<s:if test="instance.recordStatus == 1">
						<input id="fromDate" type="text" class="InputTextStyle InputText6Style vinput-date" call-back="VTValidateUtils.getMessageOfEmptyDate('fromDate', 'Từ ngày');VTValidateUtils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày')" maxlength="20" value='<s:property value="fromDate"/>'/>
					</s:if>
					<s:else>
						<input id="fromDate" type="text" class="InputTextStyle InputText6Style vinput-date" call-back="VTValidateUtils.getMessageOfEmptyDate('fromDate', 'Từ ngày');VTValidateUtils.getMessageOfInvalidFormatDate('fromDate', 'Từ ngày')" maxlength="20" value='<s:property value="fromDate"/>'/>
					</s:else>						
					<label class="LabelStyle Label9Style">Đến ngày<span class="ReqiureStyle"> *</span></label>
					<s:if test="instance.recordStatus == 1">
						<input id="toDate" type="text" call-back="VTValidateUtils.getMessageOfEmptyDate('toDate', 'Đến ngày');VTValidateUtils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');VTValidateUtils.getMessageCheckToDate('fromDate', 'Từ ngày', 'toDate', 'Đến ngày');" class="InputTextStyle InputText6Style vinput-date" maxlength="20" value='<s:property value="toDate"/>'/>
					</s:if>
					<s:else>
						<input id="toDate" type="text" call-back="VTValidateUtils.getMessageOfEmptyDate('toDate', 'Đến ngày');VTValidateUtils.getMessageOfInvalidFormatDate('toDate', 'Đến ngày');VTValidateUtils.getMessageCheckToDate('fromDate', 'Từ ngày', 'toDate', 'Đến ngày');" class="InputTextStyle InputText6Style vinput-date" maxlength="20" value='<s:property value="toDate"/>'/>
					</s:else>	
					<label class="LabelStyle Label9Style">Loại kiểm kê</label>
					<div class="BoxSelect BoxSelect4">
						<select class="MySelectBoxClass  InputTextStyle" id="type">
							<option value="0" selected="selected">Tất cả thiết bị</option>
							<option value="1">Theo nhóm thiết bị</option>
							<option value="3">Theo danh sách thiết bị</option>
						</select>
					</div>
					<div class="Clear"></div>
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnSave" class="BtnGeneralStyle cmsiscontrol" onclick="EquipStatisticChecking.save()">Cập nhật</button>
						</div>
					</div>
					<div class="Clear"></div>
					<p id="noteMsg" class="SuccessMsgStyle" style="display: none">Tất cả các thiết bị trên thị trường sẽ được kiểm kê với chương trình này</p>
					<p class="ErrorMsgStyle SpriteErr" id="errorMsg" style="display: none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
	<div class="Clear"></div>
	<s:if test="id != null && id > 0">
		<div class="ToolBarSection">
			<div id="tabSectionDiv" class="TabSection">
				<ul class="ResetList TabSectionList">
					<li><a id="shopTab" href="javascript:void(0);" onclick="showTab2()" class="Sprite1 Active"><span class="Sprite1" style="font-size: 12px;">Đơn vị tham gia</span></a></li>
					<li><a id="listGroupEquip" href="javascript:void(0);" onclick="showTab1()" class="Sprite1"><span id="listGroupEquipName" class="Sprite1" style="font-size: 12px;">Danh sách nhóm thiết bị</span></a></li>
<%-- 					<li><a id="listShelfEquip" href="javascript:void(0);" onclick="showTab4()" class="Sprite1"><span class="Sprite1" style="font-size: 12px;">Danh sách ụ, kệ</span></a></li> --%>
<%-- 			        <li><a id="customerTab" href="javascript:void(0);" onclick="showTab3()" class="Sprite1"><span class="Sprite1" style="font-size: 12px;">Khách hàng</span></a></li> --%>
			    </ul>
			    <div class="Clear"></div>
			</div>
		</div>
				
    </div>
    <div id="divTab" class="SearchSection GeneralSSection">
    </div>		
    </s:if>
</div>
<s:hidden id="id" name="id"></s:hidden>
<s:hidden id="oldType" name="type"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	var status = '<s:property value="instance.recordStatus"/>';
	if(status!=null && status!=undefined){
		if(status == activeType.STOPPED){
			enableDateTimePicker('fromDate');
			enableDateTimePicker('toDate');
		}else{
			disableDateTimePicker('fromDate');
			disableDateTimePicker('toDate');
			disableSelectbox('type');
		}
	}
	var type = '<s:property value="instance.type"/>';
	$('#type').val(type);
	$('#type').change();
	$('#checkingCode').focus();
	if($('#id').val() != null && $('#id').val() != undefined && $('#id').val() != '') {
		if (type == EquipStatisticChecking.ALL_EQUI_TYPE) {
			$('#noteMsg').show();
			$('#listGroupEquip').hide();
		} else if (type == EquipStatisticChecking.LIST_EQUI_TYPE) {
			$('#noteMsg').hide();
			$('#listGroupEquipName').html('Danh sách thiết bị');
		} else if (type == EquipStatisticChecking.GROUP_EQUI_TYPE) {
			$('#noteMsg').hide();
			$('#listGroupEquipName').html('Nhóm thiết bị');
		}
	} else {
		ReportUtils.setCurrentDateForCalendar("fromDate");
		ReportUtils.setCurrentDateForCalendar("toDate");
	}
	$('#shopTab').addClass('Active');
	showTab2();
});
function showTab1() {
	$('#shopTab').removeClass('Active');
// 	$('#customerTab').removeClass('Active');
// 	$('#listShelfEquip').removeClass('Active');
	$('#listGroupEquip').addClass('Active');
	VTUtilJS.getFormHtml({id:$('#id').val()}, '/equipment-checking/edit-group-product', function(html) {
		if ($("#easyuiPopupSearchEquipment").length > 0) {
			$("#easyuiPopupSearchEquipment").dialog("destroy");
		}
		if ($("#easyuiPopupImportExcelEx").length > 0) {
			$("#easyuiPopupImportExcelEx").dialog("destroy");
		}
		$('#divTab').html(html);
	});
}
function showTab2() {
	$('#listGroupEquip').removeClass('Active');
// 	$('#customerTab').removeClass('Active');
// 	$('#listShelfEquip').removeClass('Active');
	$('#shopTab').addClass('Active');
	VTUtilJS.getFormHtml({id:$('#id').val()}, '/equipment-checking/edit-shop', function(html) {
		if ($("#easyuiPopupImportExcel1").length > 0) {
			$("#easyuiPopupImportExcel1").dialog("destroy");
		}
		$('#divTab').html(html);
	});
}
function showTab3() {
	$('#listGroupEquip').removeClass('Active');
	$('#shopTab').removeClass('Active');
// 	$('#listShelfEquip').removeClass('Active');
// 	$('#customerTab').addClass('Active');
	VTUtilJS.getFormHtml({id:$('#id').val()}, '/equipment-checking/edit-customer', function(html) {
		if ($("#easyuiPopupImportExcel").length > 0) {
			$("#easyuiPopupImportExcel").dialog("destroy");
		}
		if ($("#easyuiPopupImportExcelStaff").length > 0) {
			$("#easyuiPopupImportExcelStaff").dialog("destroy");
		}
		$('#divTab').html(html);
	});
}
function showTab4() {
	$('#listGroupEquip').removeClass('Active');
	$('#shopTab').removeClass('Active');
// 	$('#customerTab').removeClass('Active');
// 	$('#listShelfEquip').addClass('Active');
	VTUtilJS.getFormHtml({id:$('#id').val()}, '/equipment-checking/edit-shelf-product', function(html) {

		$('#divTab').html(html);
	});
}
</script>
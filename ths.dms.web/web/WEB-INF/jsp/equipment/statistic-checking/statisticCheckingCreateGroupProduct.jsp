<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="SearchInSection SProduct1Form">
	<div id="gridEquipDIV" class="SearchInSection SProduct1Form">
		<div class="Title2Style" style="height:15px;font-weight: bold;">
<%-- 			<span style="float:left;">Danh sách thiết bị</span> --%>
			<div style="float:right; margin-top: -5px;margin-right: 27px;">	
			 <s:if test="instance.recordStatus == 0">			
				<a id="tabEquip_import_download_temolate" href="javascript:void(0);" class="downloadTemplateReport cmsiscontrol"><img src="/resources/images/icon_download_32.png" height="20" width="20" style="border-left-color: blue; border-left-style: solid; border-left-width: 2px;margin-right: 5px;" title="Tải tập tin mẫu"></a>
				<a id="tabEquip_import_show_dlg" href="javascript:void(0);" class="cmsiscontrol" onclick="EquipStatisticChecking.showDlgImportCustomerExcelEx();"><img src="/resources/images/icon_table_import.png" height="20" width="20" title="Nhập excel" style="margin-right: 5px;"></a>
			 </s:if>
				<a id="tabEquip_export" href="javascript:void(0);" class="cmsiscontrol" onclick="EquipStatisticChecking.exportExcelEquip();" style="margin-right: 5px;"><img src="/resources/images/icon_table_export.png" height="20" width="20" title="Xuất excel" style="margin-right: 5px;"></a>		
			</div>
			<div style="float:right; margin-top: -5px;margin-right: 27px;">	
				<span style="text-transform: none; float: left; margin-top: 3px; font-size: 13px; font-weight: normal;">Trạng thái KH</span>
				<div class="BoxSelect BoxSelect4" style="margin-left: 10px">			
					<select class="MySelectBoxClass" id="statusCustomer" >
						<option value="" selected="selected">Tất cả</option>
						<option value="1" >Hoạt động</option>
						<option value="0" >Tạm ngưng</option>
					</select>
				</div>			
				<input type="text" id="equipCode" style="margin-left:10px" class="InputTextStyle InputText1Style" placeholder="Nhập mã thiết bị">
				<input type="text" id="equipGroupName" style="margin-left: 10px; margin-right: 10px;" class="InputTextStyle InputText1Style" placeholder="Nhập tên nhóm thiết bị">
				<button class="BtnGeneralStyle cmsiscontrol" id="tabEquip_btn_search_equip" style="height: 27px;" onclick="EquipStatisticChecking.searchGridEquip();">Tìm kiếm</button>		
			</div>
		</div>	
		<div class="GridSection" id=gridEquipContainer>
			<table id="gridEquipStatistic" class="easyui-datagrid"></table>
		</div>
	</div>	
	<div class="Clear"></div>
	<div id="gridGroupProductDIV" class="SearchInSection SProduct1Form" >
		<div class="GridSection" id=gridGroupProductContainer>
			<table id="gridGroupProduct" class="easyui-datagrid"></table>
		</div>
	</div>
</div>
<tiles:insertTemplate template="/WEB-INF/jsp/equipment/statistic-checking/equipmentStatisticEquipment.jsp"></tiles:insertTemplate>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcelEx" class="easyui-dialog" title="Import - Thiết bị" data-options="closed:true,modal:true" style="width:465px;">
		<div id="tabEquip_import_dlg" class="PopupContentMid cmsiscontrol">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-checking/edit-import-equip" name="importFrmDVKKEx" id="importFrmDVKKEx" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">		    
						<input id="fakefilepcDVKKEx" type="text" class="InputTextStyle InputText1Style">						
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileDVKKEx" onchange="previewImportExcelFile(this,'importFrmDVKKEx','fakefilepcDVKKEx', 'errExcelMsgDVKKEx');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadExcelFile" onclick="return EquipStatisticChecking.importExcelEquip();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcelEx').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgDVKKEx"/>
	</div>	
</div>
<input type="hidden" id="isCheck" value="<s:property value="isCheck"/>" />
<script type="text/javascript">
$(document).ready(function() {
// 	var status = $('#status').val();	
	EquipStatisticChecking.status = '<s:property value="instance.recordStatus"/>';
	EquipStatisticChecking._equipHtml = $('#gridEquipDIV').html();
	EquipStatisticChecking._equipGroupHtml = $('#gridGroupProductDIV').html();
	if ($('#isCheck').val() != null && $('#isCheck').val() == EquipStatisticChecking.LIST_EQUI_TYPE) {
		$('#gridEquipDIV').show();
    	$('#gridEquipDIV').html(EquipStatisticChecking._equipHtml);
		$('#gridGroupProductDIV').html('');
    	loadGridEquip();
	} else if ($('#isCheck').val() != null && $('#isCheck').val() == EquipStatisticChecking.GROUP_EQUI_TYPE) {
		$('#gridEquipDIV').hide();
    	$('#gridGroupProductDIV').html(EquipStatisticChecking._equipGroupHtml);
		$('#gridEquipDIV').html('');
    	loadGridGroupEquip();
	}
	 $('#typeEquipment,#yearManufacturing').customStyle();
});
function deleteEquipGroup(id) {
	$('#errorMsg').html('').hide();
	$.messager.confirm("Xác nhận", "Bạn có muốn xóa thông tin?", function(r) {
		if(r) {
			VTUtilJS.postFormJson({id:id, recordId : $('#id').val()}, '/equipment-checking/edit-group-product/delete', function(data) {
				if(data.error) {
					$('#errorMsg').html(data.errMsg).show();
				} else {
					$('#successMsg').html('Xóa nhóm thiết bị thành công').show();
					VTUtilJS.getFormJson({id:$('#id').val()}, '/equipment-checking/edit-group-product/search', function(list) {
						$('#gridGroupProduct').datagrid('loadData', list);
					});
					setTimeout(function() {
						$('#successMsg').html('').hide();
					}, 3000);
				}
			});
		}
	});
}
function deleteEquip(id) {
	$('#errorMsg').html('').hide();
	$.messager.confirm("Xác nhận", "Bạn có muốn xóa thông tin?", function(r) {
		if(r) {
			VTUtilJS.postFormJson({id:id, recordId : $('#id').val()}, '/equipment-checking/edit-equip/delete', function(data) {
				if(data.error) {
					$('#errorMsg').html(data.errMsg).show();
				} else {
					$('#successMsg').html('Xóa thiết bị thành công').show();
					EquipStatisticChecking.searchGridEquip();
					setTimeout(function() {
						$('#successMsg').html('').hide();
					}, 3000);
				}
			});
		}
	});
}
function showF9(id) {
	var html = '<div id="popup-container"><div id="popup-add-group" class="PopupContentMid">\
		<div class="GeneralForm Search1Form" id="form-data">\
		<label style="width: 100px;" class="LabelStyle Label1Style">Mã / tên nhóm</label>\
		<input type="text" class="InputTextStyle InputText4Style" maxlength="50" id="code" style="width: 212px;"> \
		<input type="hidden" id="id" value="'+id+'">\
		<label style="width: 100px;" class="LabelStyle Label1Style">Loại</label>\
		<div class="BoxSelect BoxSelect7">\
		<select class="MySelectBoxClass" id="equipCategory">\
			<option value="-1" selected="selected">Tất cả</option>\
		</select>\
		</div>\
		<div class="Clear"></div>\
		<label style="width: 100px;" class="LabelStyle Label1Style">Dung tích</label>\
		<input type="text" class="InputTextStyle InputText1Style" maxlength="10" id="fromCapacity"> \
		<label style="text-align:right;width:12px;" class="LabelStyle LabelStyle"> - </label>\
		<input type="text" class="InputTextStyle InputText1Style" maxlength="10" id="toCapacity"> \
		<label style="width: 100px;" class="LabelStyle Label1Style">Hiệu</label>\
		<input type="text" class="InputTextStyle InputText1Style" maxlength="200" id="brand"> \
		<div class="Clear"></div>\
		<div class="BtnCenterSection">\
		<button class="BtnGeneralStyle" id="btn-search">Tìm kiếm</button>\
		</div>\
		<div class="Clear"></div>\
		<div class="SearchInSection SProduct1Form">\
		<div class="GridSection" id=gridSearchContainer>\
			<table id="searchGrid"></table>\
		</div>\
		</div>\
		<div class="Clear"></div>\
		<div class="BtnCenterSection">\
		<button class="BtnGeneralStyle" id="btn-update">Chọn</button>\
		<button class="BtnGeneralStyle" id="btn-close-dl">Đóng</button>\
		</div>\
		<p class="ErrorMsgStyle SpriteErr" style="display: none;" id="errMsgLevelDlg"></p>\
		<p class="SuccessMsgStyle" style="display: none;" id="successMsgLevelDlg"></p>\
		</div></div></div>';
	$('body').append(html).change();
	Utils.bindFormatOntextfieldCurrencyFor('fromCapacity');
	Utils.bindFormatOntextfieldCurrencyFor('toCapacity');
	EquipStatisticChecking.__listCheckId = new Map();
	$('#popup-add-group').dialog({
		title: 'Chọn nhóm thiết bị',
		width: 750,
		heigth: "auto",
		modal: true, close: false, cache: false,
		onOpen: function() {
			$("#popup-add-group #code").focus();
			VTUtilJS.bindFormatOnTextfield('capacity', VTUtilJS._TF_NUMBER);
			$('#equipCategory').customStyle();
			VTUtilJS.getFormJson({}, '/equipment-checking/edit-group-product/metadata', function(data) {
				if(!data.error && $.isArray(data.listEquipCategory)) {
					var html = '<option value="-1" selected="selected">Tất cả</option>';
					for(var i = 0; i < data.listEquipCategory.length; i++) {
						html += '<option value="'+data.listEquipCategory[i].id+'">' + Utils.XSSEncode(data.listEquipCategory[i].code + ' - ' + data.listEquipCategory[i].name) + '</option>';
					}
					$('#equipCategory').html(html).change();
				}
			});
			$("#popup-add-group #code").keyup(function(event) {
				if (event.keyCode == keyCodes.ENTER) {
					$("#popup-add-group #btn-search").click();
				}
			});
			$("#popup-add-group #fromCapacity, #popup-add-group #toCapacity").keyup(function(event) {
				if (event.keyCode == keyCodes.ENTER) {
					$("#popup-add-group #btn-search").click();
				}
			});
			$("#popup-add-group #brand").keyup(function(event) {
				if (event.keyCode == keyCodes.ENTER) {
					$("#popup-add-group #btn-search").click();
				}
			});
			$("#popup-add-group #equipCategory").keyup(function(event) {
				if (event.keyCode == keyCodes.ENTER) {
					$("#popup-add-group #btn-search").click();
				}
			});
			$('#popup-add-group #btn-search').unbind('click');
			$('#popup-add-group #btn-search').bind('click', function() {
				$('#errMsgLevelDlg').html('').hide();
				var formParam = VTUtilJS.getFormData('form-data');
				
				var capacityTmp = $('#fromCapacity').val().replace(',','').trim();
				var msg = '';
				if (isNaN(capacityTmp)) {
					msg = 'Giá trị Dung Tích Từ nhập vào chỉ nằm trong các ký tự [0-9.]';
				}
				capacityTmp = $('#toCapacity').val().replace(',','').trim();
				if (msg.length == 0 && isNaN(capacityTmp)) {
					msg = 'Giá trị Dung Tích Đến nhập vào chỉ nằm trong các ký tự [0-9.]';
				}
				if (msg.length > 0) {
					$('#errMsgLevelDlg').html(msg).show();
					return;
				}
				if (formParam != null) {
					EquipStatisticChecking.__listCheckId = new Map();
					$('#searchGrid').datagrid('reload', formParam);
				}
				$('#searchGrid').datagrid('uncheckAll');
			});
			$('#popup-add-group #btn-close-dl').unbind('click');
			$('#popup-add-group #btn-close-dl').bind('click', function() {
				$('#popup-add-group').dialog('close');
			});
			$('#popup-add-group #btn-update').unbind('click');
			$('#popup-add-group #btn-update').bind('click', function() {
				$('#errMsgLevelDlg').html('').hide();
				if(EquipStatisticChecking.__listCheckId.size() == 0) {
					$('#errMsgLevelDlg').html('Bạn chưa chọn nhóm thiết bị nào. Vui lòng check chọn nhóm thiết bị').show();
					return;
				}
				$.messager.confirm("Xác nhận", "Bạn có muốn lưu thông tin?", function(r) {
					if(r) {
						VTUtilJS.postFormJson({id:$('#id').val(), listId:EquipStatisticChecking.__listCheckId.keyArray}, '/equipment-checking/edit-group-product/save', function(data) {
							if(data.error) {
								$('#errMsgLevelDlg').html(data.errMsg).show();
								return;
							} else {
								$('#gridGroupProduct').datagrid('loadData', data.listEquipGroup);
								$("#popup-add-group").dialog("destroy");
								$("#popup-container").remove();
							}
						});
					}
				});
			});
			$('#searchGrid').datagrid({
				url : "/equipment-checking/edit-group-product/dialog",
				autoRowHeight : true,
				rownumbers : true, 
				checkOnSelect: true,
				pagination:true,
				fitColumns:true,
				scrollbarSize:0,
				width: $('#gridSearchContainer').width(),
				autoWidth: true,
				queryParams :{
					id:$('#id').val()
				},
			    columns:[[	        
				    {field : 'code', title : 'Mã nhóm thiết bị', width : 100, align : 'left', sortable : false, resizable : false, formatter : function(value, row, index) {
				    	return Utils.XSSEncode(value);
				    }},
				    {field : 'name', title : 'Tên nhóm thiết bị', width : 100, align : 'left', sortable : false, resizable : false, formatter: function(value, row, index) {
				    	return Utils.XSSEncode(value);
				    }},	    
				    {field : 'equipCategory' , title : 'Loại', width : 60, align : 'left', formatter : function(value, row, index) {
				    	return Utils.XSSEncode((row.equipCategory != null && row.equipCategory != undefined) ? row.equipCategory.name : '');
				    }},
				    {field : 'capacity' , title : 'Dung tích(lit)', width : 80, align : 'center', formatter : function(value, row, index) {
				    	if(row.capacityFrom != null && row.capacityFrom != undefined && row.capacityTo != null && row.capacityTo != undefined) {
				    		return (row.capacityFrom + ' - ' + row.capacityTo);
				    	} else if(row != null && row != undefined && row.capacityFrom != null && row.capacityFrom != undefined) {
				    		return ('>=' +row.capacityFrom);
				    	} else if(row != null && row != undefined && row.capacityTo != null && row.capacityTo != undefined) {
				    		return ('<=' +row.capacityTo);
				    	} else {
				    		return '';
				    	}
				    }},
					{field : 'equipBrandName' , title : 'Hiệu', width : 80, align : 'left', formatter : function(value, row, index) {
						return Utils.XSSEncode(row.equipBrandName);
				    }},
				    {field:'cb', checkbox:true, width: 50, align: 'center'}
			    ]],
			    onCheck:function(idx,row){
			    	EquipStatisticChecking.__listCheckId.put(row.id, row);
				},
				onUncheck:function(idx,row){
					EquipStatisticChecking.__listCheckId.remove(row.id);
				},
				onCheckAll:function(rows){
					for(var i = 0; i < rows.length; i++) {
						EquipStatisticChecking.__listCheckId.put(rows[i].id, rows[i]);
					}
				},
				onUncheckAll:function(rows){
					for(var i = 0; i < rows.length; i++) {
						EquipStatisticChecking.__listCheckId.remove(rows[i].id);
					}
				},
			    onLoadSuccess :function(data){
					$('.datagrid-header-rownumber').html('STT');			
					$(window).resize();
					if($.isArray(data)) {
						var isCheckAll=true;
						for(var i = 0; i < data.length; i++) {
							if(EquipStatisticChecking.__listCheckId.get(data.id) != null) {
								$('#searchGrid').datagrid('checkRow', i);
							} else {
								 isCheckAll = false;
							}
						}
						if(isCheckAll) {
							$('#gridSearchContainer .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
						}
					} else if(data.rows != null && data.rows != undefined && $.isArray(data.rows)) {
						var isCheckAll=true;
						for(var i = 0; i < data.rows.length; i++) {
							if(EquipStatisticChecking.__listCheckId.get(data.rows.id) != null) {
								$('#searchGrid').datagrid('checkRow', i);
							} else {
								 isCheckAll = false;
							}
						}
						if(isCheckAll) {
							$('#gridSearchContainer .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
						}
					}
				}
			});
			$("#popup-add-group #brand").width($("#popup-add-group #equipCategory").width()-8);
		},
		onClose: function() {
			$("#popup-add-group").dialog("destroy");
			$("#popup-container").remove();
		}
	});
	
}

function loadGridGroupEquip() {
	VTUtilJS.getFormJson({id:$('#id').val()}, '/equipment-checking/edit-group-product/search', function(list) {
		var titleAdd = '';
		if(EquipStatisticChecking.status == 0) {
			titleAdd = '<a id="tabEquipGroup_AddGroup" class="cmsiscontrol" onclick="showF9($(\'#id\').val());"><span style="cursor:pointer"><img title="Thêm nhóm thiết bị" src="/resources/images/icon_add.png"/></span></a>';
		}
		$('#gridGroupProduct').datagrid({
			data: list,
			autoRowHeight : true,
			rownumbers : true, 
			singleSelect: true,
			fitColumns: true,
			scrollbarSize: 0,
			width: $('#gridGroupProductContainer').width(),
			autoWidth: true,
		    columns: [[	        
			    {field : 'code', title : 'Mã nhóm thiết bị (F9)', width : 100, align : 'left', sortable : false, resizable : false, formatter : function(value, row, index) {
			    	return Utils.XSSEncode(value);
			    }},
			    {field : 'name', title : 'Tên nhóm thiết bị', width : 100, align : 'left', sortable : false, resizable : false, formatter:CommonFormatter.formatNormalCell},	    
			    {field : 'equipCategory' , title : 'Loại nhóm thiết bị', width : 60, align : 'left', formatter : function(value, row, index) {
			    	return Utils.XSSEncode((row.equipCategory != null && row.equipCategory != undefined) ? row.equipCategory.name : '');
			    }},
			    {field : 'capacity' , title : 'Dung tích(lit)', width : 80, align : 'center', formatter : function(value, row, index) {
			    	if(row.capacityFrom != null && row.capacityFrom != undefined && row.capacityTo != null && row.capacityTo != undefined) {
			    		return (row.capacityFrom + ' - ' + row.capacityTo);
			    	} else if(row != null && row != undefined && row.capacityFrom != null && row.capacityFrom != undefined) {
			    		return ('>=' +row.capacityFrom);
			    	} else if(row != null && row != undefined && row.capacityTo != null && row.capacityTo != undefined) {
			    		return ('<=' +row.capacityTo);
			    	} else {
			    		return '';
			    	}
			    }},
				{field : 'equipBrandName' , title : 'Hiệu', width : 80, align : 'center', formatter:CommonFormatter.formatNormalCell},
			    {field:'delete', title: titleAdd, width: 50, align: 'center', formatter: function (value, row, index) {
			    	return '<a id="tabEquipGroup_delGroup' + row.id + '" class="cmsiscontrol" onclick="deleteEquipGroup('+row.id+')"><span style="cursor:pointer"><img title="Xóa nhóm thiết bị" src="/resources/images/icon-delete.png"/></span></a>';			    		
			    }}
		    ]],	    
		    onLoadSuccess :function(data) {
				$('.datagrid-header-rownumber').html('STT');			
				$(window).resize();
				EquipStatisticChecking._lentghGridGroupProduct = 0;
				var rows  = $('#gridGroupProduct').datagrid('getRows');
				if (rows != null && rows.length > 0) {
					for (var i = 0, size = rows.length; i < size; i++) {
						if (rows[i].code != undefined && rows[i].code != null && rows[i].code.trim().length > 0) {
							EquipStatisticChecking._lentghGridGroupProduct = 1;
							break;
						}
					}
				}
				Utils.functionAccessFillControl();
	    		$(window).resize();
			}
		});
	});
}

function loadGridEquip() {
	$('#statusCustomer').customStyle();
	var params = new Object();	
	var titleEquip = '';
	if (EquipStatisticChecking.status == statisticStatus.DT) {
		titleEquip = '<a id="tabEquip_btnAddEquip" class="cmsiscontrol" onclick="EquipStatisticChecking.showPopupSearchEquipment();"><span style="cursor:pointer"><img title="Chọn thiết bị" src="/resources/images/icon_add.png"/></span></a>'; 
	}
	params.equipCode = $('#equipCode').val().trim();	
	params.equipGroupName = $('#equipGroupName').val().trim();	
	params.id = $('#id').val();
	params.statusCus = $('#statusCustomer').val().trim();	
	$('#gridEquipStatistic').datagrid({
		url : "/equipment-checking/edit-equip/search",
		autoRowHeight : true,
		rownumbers : true, 
// 		checkOnSelect :true,		
		pagination:true,
		pageSize :20,
		pageList : [20],
		scrollbarSize:0,
		fitColumns:true,
		queryParams: params,
		width: $('#gridEquipContainer').width(),
	    columns:[[
			{field: 'shopCode', title: 'Đơn vị', width: 70, align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
			{field: 'shortCode', title: 'Khách hàng', width: 80, align: 'left', formatter: function(value, row, index) {
// 		    	if(row.stockType == 2){
		    		return Utils.XSSEncode(row.shortCode);
// 		    	}
		    }},
		    {field: 'customerName', title: 'Khách hàng', width: 80, align: 'left', formatter: function(value, row, index) {
// 		    	if(row.stockType == 2){
		    		return Utils.XSSEncode(row.customerName);
// 		    	}
		    }},
		    {field: 'customerAddress' , title: 'Địa chỉ', width: 200, align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
		    {field: 'code', title : 'Mã thiết bị', width : 70, align: 'left', sortable: false, resizable: false, formatter : function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},
		    {field: 'equipGroupName', title: 'Nhóm thiết bị', width: 120, align: 'left', sortable: false, resizable: false, formatter:CommonFormatter.formatNormalCell},	   
// 		    {field: 'stock' , title : 'Kho', width : 90, align : 'left', formatter : function(value, row, index) {
// 		    	if(row.stockType == 1){
// 		    		return Utils.XSSEncode(row.stockName);
// 		    	}
// 		    }},
// 		    {field: 'statusCus' , title : 'Trạng thái KH', width : 60, align : 'left', formatter : function(value, row, index) {
// 		    	return activeType.parseText(value);
// 		    }},
		    {field: 'equipCategoryName', title: 'Loại', width: 100, align: 'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},
// 		    {field : 'capacity' , title : 'Dung tích(lit)', width : 60, align : 'center', formatter : function(value, row, index) {
// 		    	if(row.capacityFrom != null && row.capacityFrom != undefined && row.capacityTo != null && row.capacityTo != undefined) {
// 		    		var html =  (row.capacityFrom + ' - ' + row.capacityTo);
// 		    		if(row.capacityFrom == row.capacityTo){
// 		    			html = row.capacityFrom;
// 		    		}
// 		    		return html;
// 		    	} else if(row != null && row != undefined && row.capacityFrom != null && row.capacityFrom != undefined) {
// 		    		return ('>=' +row.capacityFrom);
// 		    	} else if(row != null && row != undefined && row.capacityTo != null && row.capacityTo != undefined) {
// 		    		return ('<=' +row.capacityTo);
// 		    	} else {
// 		    		return '';
// 		    	}
// 		    }},
// 			{field : 'equipBrandName' , title : 'Hiệu', width : 80, align : 'left', formatter:CommonFormatter.formatNormalCell},
		    {field:'delete', title: titleEquip, width: 30, align: 'center', formatter: function (value, row, index) {
		    	if (EquipStatisticChecking.status == statisticStatus.DT) {
		    		return '<a id="tabEquip_btnDelEquip' + row.equipId + '" class="cmsiscontrol" onclick="deleteEquip(' + row.equipId + ')"><span style="cursor:pointer"><img title="Xóa thiết bị" src="/resources/images/icon-delete.png"/></span></a>'; 
		    	}
		    	return '';
		    }}
	    ]],	    
	    onLoadSuccess: function(data) {
			$('.datagrid-header-rownumber').html('STT');
			var rows  = $('#gridEquipStatistic').datagrid('getRows');
			if (rows != null && rows.length > 0) {
				EquipStatisticChecking._lentghGridGroupProduct = 1;
			}else{
				EquipStatisticChecking._lentghGridGroupProduct = 0;
			}
			Utils.functionAccessFillControl();
			$(window).resize();
		}
	});
	$('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Impt_Thietbi.xlsx');
	$("#equipCode, #equipGroupName").keyup(function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$("#tabEquip_btn_search_equip").click();
		}
	});
}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipment-checking/info">Thiết bị</a></li>
		<li><span>Nhập kiểm kê : <span><s:property value="instance.getCode()"/></span></span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">				
				<div class="SearchInSection SProduct1Form" id="search-from">
					<label style="width: 5%;" class="LabelStyle Label1Style">Mã đơn vị</label>
					<div class="BoxSelect BoxSelect2">
						<s:select id="selectShop" cssClass="MySelectBoxClass" list="lstShop" headerKey="-1" headerValue="Đơn vị kiểm kê" listKey="id" listValue="shopCode"></s:select>
					</div>
					<label style="width: 5%;" class="LabelStyle Label1Style">Mã ụ kệ</label>
					<div class="BoxSelect BoxSelect2">
						<s:select id="selectEquipGroup" cssClass="MySelectBoxClass" list="listShelf" headerKey="-1" headerValue="Tất cả" listKey="id" listValue="productCode"></s:select>
					</div>
					<label style="width: 5%;" class="LabelStyle Label1Style">Mã kho</label>
					<div class="BoxSelect BoxSelect2">
						<select id="selectStock" class="MySelectBoxClass">
							<option value="-1">Tất cả</option>
						</select>
					</div>
					<label style="width: 5%;" class="LabelStyle Label1Style">Lần kiểm kê</label>
					<div class="BoxSelect BoxSelect2">
						<select id="soLan" class="MySelectBoxClass">
							<s:iterator begin="1" end="soLan" var="stepBy">
								<option value="<s:property value='#stepBy'/>">Lần <s:property value="#stepBy"/></option>
							</s:iterator>
						</select>
					</div>					
					<div class="Clear"></div>
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnLoad" class="BtnGeneralStyle" onclick="loadDetailData()">Lấy dữ liệu</button>
						</div>
					</div>
					<div class="Clear"></div>
					<p class="ErrorMsgStyle SpriteErr" id="errorMsg" style="display: none;"></p>
					<p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
			</div>
			<h2 class="Title2Style">Danh sách kiểm kê
				<div style="float:right; margin-top: -5px;margin-right: 27px;">
					<a href="#" class="downloadTemplateReport"><img src="/resources/images/icon_download_32.png" height="20" width="20" style="border-left-color: blue; border-left-style: solid; border-left-width: 2px;" style="margin-right: 5px;" title="Tải tập tin mẫu"></a>
					<a href="javascript:void(0);" onclick="EquipStatisticChecking.showDlgImportExcelKK();"><img src="/resources/images/icon_table_import.png" height="20" width="20" style="margin-right: 5px;" title="Nhập excel"></a>
					<a  onclick = "EquipStatisticChecking.exportExcelDetailUKeSoLan();" style = "float: right;	padding: 0px 25px 0px 0px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_export.png" title="Xuất Excel"></a>
				</div>
			</h2>
			<div class="SearchInSection SProduct1Form">
				<div class="GridSection" id=gridDetailContainer>
					<table id="detailGrid"></table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Import - Kiểm kê Ụ kệ dạng Số lần" data-options="closed:true,modal:true" style="width:465px;height:250px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-checking/import/shelf-solan" name="importFrmBBTL" id="importFrmBBTL" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">
						<input id="fakefilepcBBTL" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileBBTL" onchange="previewImportExcelFile(this,'importFrmBBTL','fakefilepcBBTL', 'errExcelMsgBBTL');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a onclick="" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadExcelFile" onclick="return EquipStatisticChecking.importExcelShelfSoLan();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgBBTL"/>
	</div>	
</div>
<div id="popup-images" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogPhotoSection FixFloat">
        	<div class="PhotoCols1">
              	<div id="listImage" class="DScrollpane">
              		<ul id="listImageUL" class="ResetList PhotoCols2List">
              		</ul>
            	</div>
            </div>
            <div class="PhotoCols2">
            	<div class="PhotoCols1Info1">
                	<p onclick="EquipStatisticChecking.showImageNextPre(0);" class="LeftStyle Sprite1"></p>
                	<div class="PhotoLarge"><span class="BoxFrame BoxFrame2"><span class="BoxMiddle">
                		<img id="bigImages" class="ImageMap ImageFrame4" width="752" height="508"/>
                		<img style="display:none" id="loadingImages" width="50" height="50" src="/resources/images/loading.gif">
                	</span></span></div>
                    <p onclick="EquipStatisticChecking.showImageNextPre(1);" class="RightStyle Sprite1"></p>
                </div>
                <div class="PhotoCols1Info3">
                	<div id="divMapSection" class="Map2Section SmallMapSection">
                    	<%-- <p class="OnFucnStyle" onclick="EquipStatisticChecking.onOffMap(1);" style="display:none;"><span class="HideText Sprite1">Mở rộng</span></p>
                    	<p class="OffFucnStyle" onclick="EquipStatisticChecking.onOffMap(0);" style="display:none;"><span class="HideText Sprite1">Thu nhĐ</span></p> --%>
                        <div class="MapSection">
                    		<div id="bigMap" style="width: 140px; height: 120px; position: relative;"></div>
                        </div>
                    </div>
					<p class="Text0Style" id="shopImage"></p>
                	<p class="Text1Style" id="staffImage"></p>
                	<p class="Text1Style" id="equipImage"></p>
                    <p style="padding-bottom: 5px;" class="Text2Style">Thời gian chụp: <span id="timeImage"></span></p>
                    <p class="Text3Style ImagePrintingClass">
	                    <a href="javascript:void(0);" class="Sprite1 DownloadPhotoLink">Tải hình ảnh</a>
						<a href="javascript:void(0)" style="background-image: url('/resources/images/icon-rotate.png'); background-repeat : no-repeat; padding-left:22px;" onclick="EquipStatisticChecking.rotateImage(this)">Xoay hình ảnh</a>
                    	<!-- <a href="javascript:void(0)" style="background-image: url('/resources/images/icon-printer.png'); background-repeat : no-repeat; padding-left:22px;margin-left: 12px;" onclick="printme()">In</a> -->
						<input type="hidden" class="DeletePhotoLink" />
                    </p>
                    <p id="errMsgPopup" class="ErrorMsgStyle" style="display: none"></p>
                </div>
            </div>
            <div class="Clear"></div>
    	</div>
    </div>
</div>
<div id="popup-images-full" style="display:none;">
	<div class="GeneralDialog General2Dialog" id="fullGeneral2Dialog">
      	<div class="DialogPhotoSection FixFloat" id="fullDialogPhotoSection">
        	<div class="PhotoCols1" id="fullPhotoCols1">
              	<div id="listImage" class="DScrollpane">
              		<ul id="listImageUL" class="ResetList PhotoCols2List">
              		</ul>
            	</div>
            </div>
            <div class="PhotoCols2" id="fullPhotoCols2">
            	<div class="PhotoCols1Info1" id="fullPhotoCols1Info1">
                	<p onclick="EquipStatisticChecking.showImageNextPre(0);" class="LeftStyle Sprite1"></p>
                	<div class="PhotoLarge"><span class="BoxFrame BoxFrame2" id="fullBoxFrame2"><span class="BoxMiddle">
                		<img id="bigImages" class="ImageMapFull ImageFrame4Full" width="752" height="508"/>
                		<img style="display:none; top: 0px;" id="loadingImages" width="50" height="50" src="/resources/images/loading.gif">
                	</span></span></div>
                    <p onclick="EquipStatisticChecking.showImageNextPre(1);" class="RightStyle Sprite1"></p>
                </div>
                <div class="PhotoCols1Info3">
                	<div id="divMapSection" class="Map2Section SmallMapSection">
                    	<%-- <p class="OnFucnStyle" onclick="EquipStatisticChecking.onOffMap(1);" style="display:none;"><span class="HideText Sprite1">Mở rộng</span></p>
                    	<p class="OffFucnStyle" style="top: 9px;" onclick="EquipStatisticChecking.onOffMap(0);"><span class="HideText Sprite1">Thu nhĐ</span></p> --%>
                        <div class="MapSection">
                    		<div id="bigMap" style="width: 140px; height: 115px; position: relative;"></div>
                        </div>
                    </div>
					<p class="Text0Style" id="shopImage"></p>
                	<p class="Text1Style" id="staffImage"></p>
                	<p class="Text1Style" id="equipImage"></p>
                    <p style="padding-bottom: 5px;" class="Text2Style">Thời gian chụp: <span id="timeImage"></span></p>
                    <p class="Text3Style ImagePrintingClassFull">
	                    <a href="javascript:void(0);" class="Sprite1 DownloadPhotoLink">Tải hình ảnh</a>
						<a href="javascript:void(0)" style="background-image: url('/resources/images/icon-rotate.png'); background-repeat : no-repeat; padding-left:22px;" onclick="EquipStatisticChecking.rotateImage(this)">Xoay hình ảnh</a>
                    	<!-- <a href="javascript:void(0)" style="background-image: url('/resources/images/icon-printer.png'); background-repeat : no-repeat; padding-left:22px;margin-left: 12px;" onclick="printme(null,1)">In</a>
						<input type="hidden" class="DeletePhotoLink" /> -->
                    </p>
                    <p id="errMsgPopup" class="ErrorMsgStyle" style="display: none"></p>
                </div>
            </div>
            <div class="Clear"></div>
    	</div>
    </div>
</div>
<div style="display: none" id="easyuiPopupImportExcelContainer">
  	<div id="easyuiPopupImportExcelEx" class="easyui-dialog" title="Import - Đơn vị" data-options="closed:true,modal:true" style="width:465px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<div>
					<div class="UploadMediaBox">
							<div class="MultiMediaBox">
								<input type="file" id="imageFile" class="uploadFileImage" name="imageFile"></input>
						</div>
						<p id="imageMsg" class="ErrorMsgStyle SpriteErr" style="display: none;"></p>
						<s:hidden id="countSuccess" value="0"></s:hidden>
						<s:hidden id="countFail" value="0"></s:hidden>
						<div class="Clear"></div>
						<div class="uploadifyQueue" id="fileQueue"></div>
					</div>	
				</div>								
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadImageFile">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcelEx').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;" class="ErrorMsgStyle SpriteErr" id="errExcelMsgDVKK"/>
		<p id="successExcelMsgDVKK" class="SuccessMsgStyle" style="display: none"></p>
	</div>	
</div>
<s:hidden id="id" name="id"></s:hidden>
<s:hidden id="errorValue" value="0"></s:hidden>
<s:hidden id="mediaType" name="mediaType"></s:hidden>
<script type="text/javascript">
loadDetailData = function() {
	$('#divOverlay').show();
	$('.ErrorMsgStyle').html('').hide();
	var shopId = $('#selectShop').val();
	var shelfId = $('#selectEquipGroup').val();
	var stockId = $('#selectStock').val();
	var soLan = $('#soLan').val();
	var obj = new Object();
	EquipStatisticChecking.filter = new Object();
	if(shopId != null && shopId != -1) {
		obj.shopId = shopId;
		EquipStatisticChecking.filter.shopId = shopId;
	}
	if(shelfId != null && shelfId != -1) {
		obj.shelfId = shelfId;
		EquipStatisticChecking.filter.shelfId = shelfId;
	}
	if(stockId != null && stockId != -1) {
		obj.stockId = stockId;
		EquipStatisticChecking.filter.stockId = stockId;
	}
	if(soLan != null) {
		obj.stepCheck = soLan;
		EquipStatisticChecking.filter.stepCheck = soLan;
	}
	obj.isFirstLoad = false;
	EquipStatisticChecking.filter.isFirstLoad = false;
	$('#detailGrid').datagrid('load', obj);
};
$(document).ready(function() {
	$('#selectShop').bind('change', function(e) {
		VTUtilJS.getFormJson({shopId:$('#selectShop').val(),id:$('#id').val()}, '/equipment-checking/list-shop-stock-uke', function(result) {
			if(!result.error && $.isArray(result.listStock)) {
				var html = '<option value="-1">Tất cả</option>';
				for(var i = 0; i < result.listStock.length; i++) {
					html += '<option value="'+result.listStock[i].id+'">'+result.listStock[i].warehouseCode+'</option>';
				}
				$('#selectStock').html('');
				$('#selectStock').html(html);
				$('#selectStock').change();
			}
		});
	});
	
	$('#detailGrid').datagrid({
		url :'/equipment-checking/input/shelf-solan-load?id='+$('#id').val(),
		autoRowHeight : true,
		rownumbers : true, 
		pagination:true,
		pageList: [50, 100, 200, 500],
		pageSize:50,
		fitColumns:true,
		singleSelect:true,
		scrollbarSize:0,		
		width: $('#gridDetailContainer').width(),
		autoWidth: true,
		queryParams:{
			isFirstLoad : true
		},
		columns:[[
			{field : 'shopCode', title : 'Đơn vị', width : 50, align : 'left', sortable : false, resizable : false, formatter: CommonFormatter.formatNormalCell},
			{field : 'shopName', title : 'Tên đơn vị', width : 150, align : 'left', sortable : false, resizable : false, formatter: CommonFormatter.formatNormalCell},
			{field : 'stock' , title : 'Kho', width : 70, align : 'left', formatter:function(value, row, index) {
		    	var html ="";
		    	if(row.stockCode != null && row.stockName != null){
		    		html += row.stockCode + ' - ' + row.stockName;
		    	}
		    	return Utils.XSSEncode(html);
		    }},
			{field : 'shelf' , title : 'Ụ kệ', width : 70, align : 'left', formatter:function(value, row, index) {
		    	var html ="";
		    	if(row.equipCode != null && row.equipName != null){
		    		html += row.equipCode + ' - ' + row.equipName;
		    	}
		    	return Utils.XSSEncode(html);
		    }},
		    {field : 'stepCheck', title : 'Lần kiểm tra', width : 50, align : 'right', sortable : false, resizable : false, formatter: CommonFormatter.formatNormalCell},
		    {field : 'actualQuantity', title : 'Số lượng', width : 50, align : 'left', sortable : false, resizable : false , 
		    	editor:{ 
					type:'numberbox',
		    		options:{
		    			groupSeparator: ','
		    		}
	    		}, 
	    		formatter:function(value){
		    		return formatCurrency(value);	
		    	}
		    },
		    {field : 'detail', title : '<a onclick="EquipStatisticChecking.saveShelfChecking('+$('#id').val()+')"><span style="cursor:pointer"><img title="Lưu" src="/resources/images/icon-save.png" width="20" height="20" /></span></a>', width : 50, align : 'center', sortable : false, resizable : false, formatter:function(value, row, index) {
		    	var html ='';
		    	if(row.numImage>0){
	    	 		html = '<a id="'+row.detailId+'" onclick="EquipStatisticChecking.detailImageRecord('+$('#id').val()+', '+row.detailId+')"><span style="cursor:pointer"><img title="Xem ảnh" src="/resources/images/icon_show_picture.png" width="20" height="20" /></span></a>';
	    		}else{
		    	 	html = '<a  id="'+row.detailId+'" style="display:none" onclick="EquipStatisticChecking.detailImageRecord('+$('#id').val()+', '+row.detailId+')"><span style="cursor:pointer"><img title="Xem ảnh" src="/resources/images/icon_show_picture.png" width="20" height="20" /></span></a>';
		    		
	    		}    	
		    	html += '<a style="padding-left: 20px;" onclick="EquipStatisticChecking.uploadImageRecord('+$('#id').val()+', '+row.detailId+','+row.numImage+')"><span style="cursor:pointer"><img title="Upload hình ảnh kiểm kê" src="/resources/images/icon_login.png" width="20" height="20" /></span></a>';
		    	return html;
		    }}
		]],
		onLoadSuccess :function(data){
			if($.isArray(data)) {
				for(var i = 0; i < data.length; i++) {
					$('#detailGrid').datagrid('beginEdit', i);
				}
			} else if($.isArray(data.rows)) {
				for(var i = 0; i < data.rows.length; i++) {
					$('#detailGrid').datagrid('beginEdit', i);
				}
			}
			EquipStatisticChecking.bindEventInput();
			$('#divOverlay').hide();
		}
	});
	$('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_import_kiem_ke_u_ke_so_lan.xlsx');
	
});
</script>
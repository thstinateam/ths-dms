<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="SearchInSection SProduct1Form">		
	<div class="Title2Style" style="height:15px;font-weight: bold;">
		<span style="float:left;">Thông tin tìm kiếm</span>
	</div>		
	<div class="Clear"></div>
	<div class="SearchInSection SProduct1Form">
		<s:if test="ckShop">
			<input id="ckShop" checked="checked" onchange="onCKShopChange();" style="float: left;" type="checkbox" name="checkLoadCustomer"/>
		</s:if>
		<s:else>
			<input id="ckShop" onchange="onCKShopChange();" style="float: left;" type="checkbox" name="checkLoadCustomer"/>
		</s:else>
		<label class="LabelStyle" style="width:150px; float: left;">Lấy từ đơn vị tham gia</label>
<!-- 		<div class="Clear"></div> -->
		<s:if test="ckPromotion">
			<input id="ckPromotion" checked="checked" onchange="onCKPromotionChange()" type="checkbox" style="float: left;margin-left: 50px;" name="checkLoadCustomer"/>		
		</s:if>
		<s:else>
			<input id="ckPromotion" onchange="onCKPromotionChange()" type="checkbox" style="float: left;margin-left: 50px;" name="checkLoadCustomer"/>
		</s:else>
		<label class="LabelStyle" style="width:120px; float: left;">Lấy từ CTHTTM</label>
		<input id="promotionCode" value='<s:property value="promotionCode"/>' style="float: left;" class="InputTextStyle InputText1Style" maxlength="100" />
				<s:if test="instance.recordStatus == 0">
					<button id="btnCustomerSave" style="margin: -3px 0 0 36px;" class="BtnGeneralStyle" onclick="saveAndLoadCustomer()">Cập nhật</button>
				</s:if>
				<s:else>
					<button id="btnCustomerSave" style="margin: -3px 0 0 36px;" class="BtnGeneralStyle BtnGeneralDStyle" disabled="disabled" onclick="">Cập nhật</button>
				</s:else>
		<div class="Clear"></div>
		<div class="Clear"></div>
		<div class="Clear"></div>
		<div style="float:left; margin-top: 0px;margin-left: 25px;" id="divNVBH">	
				<div class="Clear"></div>
				<label class="LabelStyle" style="width:120px; margin-left: 10px;">Xuất Excel</label>
				<s:if test="instance.recordStatus == 0">
<!-- 					<a href="javascript:void(0);" style="margin-top: 0px;margin-left: 25px;" class="downloadTemplateReportEx"><img src="/resources/images/icon_download_32.png" height="20" width="20" style="border-left-color: blue; border-left-style: solid; border-left-width: 2px;margin-right: -20px;" title="Tải tập tin mẫu"></a> -->
<!-- 					<a href="javascript:void(0);" style="margin-top: 0px;margin-left: 25px;" onclick="EquipStatisticChecking.showDlgImportStaffExcel();"><img src="/resources/images/icon_table_import.png" height="20" width="20" title="Nhập excel" style="margin-right: 5px;"></a> -->
				</s:if>
				<a href="javascript:void(0);" onclick="EquipStatisticChecking.exportExcelStaff();"><img src="/resources/images/icon_table_export.png" height="20" width="20" title="Xuất excel" style="margin-right: 5px;"></a>		
		</div>
		<div class="Clear"></div>
		<div class="SearchInSection SProduct1Form">
<!-- 			<div class="BtnCenterSection"> -->
<%-- 				<s:if test="instance.recordStatus == 0"> --%>
<!-- 					<button id="btnCustomerSave" class="BtnGeneralStyle" onclick="saveAndLoadCustomer()">Cập nhật</button> -->
<%-- 				</s:if> --%>
<%-- 				<s:else> --%>
<!-- 					<button id="btnCustomerSave" class="BtnGeneralStyle BtnGeneralDStyle" disabled="disabled" onclick="">Cập nhật</button> -->
<%-- 				</s:else> --%>
<!-- 			</div> -->
		</div>
		<div class="Title2Style" style="height:15px;font-weight: bold;">
			<span style="float:left;">Danh sách khách hàng</span>
			<div style="float:right; margin-top: -5px;margin-right: 27px;">	
				<s:if test="instance.recordStatus == 0">		
				<a href="javascript:void(0);" class="downloadTemplateReport"><img src="/resources/images/icon_download_32.png" height="20" width="20" style="border-left-color: blue; border-left-style: solid; border-left-width: 2px;margin-right: 5px;" title="Tải tập tin mẫu"></a>
				<a href="javascript:void(0);" onclick="EquipStatisticChecking.showDlgImportCustomerExcel();"><img src="/resources/images/icon_table_import.png" height="20" width="20" title="Nhập excel" style="margin-right: 5px;"></a>
				</s:if>
				<a href="javascript:void(0);" onclick="EquipStatisticChecking.exportExcelCustomer();" style="margin-right: 5px;"><img src="/resources/images/icon_table_export.png" height="20" width="20" title="Xuất excel" style="margin-right: 5px;"></a>		
			</div>
			<div style="float:right; margin-top: -5px;margin-right: 27px;">	
				<span style="text-transform: none; float: left; margin-top: 3px; font-size: 13px; font-weight: normal;">Trạng thái KH</span>
				<div class="BoxSelect BoxSelect4" style="margin-left: 10px">			
					<select class="MySelectBoxClass InputTextStyle" id="statusCus" >
						<option value="" selected="selected">Tất cả</option>
						<option value="1" >Hoạt động</option>
						<option value="0" >Tạm ngưng</option>
					</select>
				</div>
				<input type="text" id="customerCode" style="margin-left:10px" class="InputTextStyle InputText1Style" placeholder="Mã KH">
				<input type="text" id="customerName" style="margin-left: 10px; margin-right: 10px;" class="InputTextStyle InputText1Style" placeholder="Tên KH hoặc địa chỉ">
				<button class="BtnGeneralStyle" style="height: 27px;" onclick="EquipStatisticChecking.searchCustomer();">Tìm kiếm</button>		
			</div>
		</div>		
		<div class="GridSection" id="gridCustomerContainer">
			<table id="gridCustomer"></table>
		</div>
	</div>	
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Import - Khách hàng" data-options="closed:true,modal:true" style="width:465px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-checking/edit-import-customer" name="importFrmDVKK" id="importFrmDVKK" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">		    
						<input id="fakefilepcDVKK" type="text" class="InputTextStyle InputText1Style">						
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileDVKK" onchange="previewImportExcelFile(this,'importFrmDVKK','fakefilepcDVKK', 'errExcelMsgDVKK');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadExcelFile" onclick="return EquipStatisticChecking.importExcelCustomer();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgDVKK"/>
	</div>	
</div>
<div style="display: none">
  	<div id="easyuiPopupImportExcelStaff" class="easyui-dialog" title="Import - Thiết bị NVBH" data-options="closed:true,modal:true" style="width:465px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-checking/edit-import-staff" name="importFrmDVKKStaff" id="importFrmDVKKStaff" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">		    
						<input id="fakefilepcDVKKStaff" type="text" class="InputTextStyle InputText1Style">						
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileDVKKStaff" onchange="previewImportExcelFile(this,'importFrmDVKKStaff','fakefilepcDVKKStaff', 'errExcelMsgDVKKStaff');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a class="Sprite1 downloadTemplateReportEx" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadExcelFile" onclick="return EquipStatisticChecking.importExcelStaff();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcelStaff').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgDVKKStaff"/>
	</div>	
</div>
<script type="text/javascript">
$(function (){
// 	if (EquipStatisticChecking._lentghGridGroupProduct == 0) {
// 		$('#ckShop').prop('checked', false).change();
// 	} else {
// 		$('#ckPromotion').prop('checked', false).change();
// 	}

	if (EquipStatisticChecking._lentghGridGroupProduct > 0) {
		$('#divNVBH').show();
	}else{
		$('#divNVBH').hide();
	}
	
});
var deleteCustomer = function(entityId) {
	$('#errorMsg').html('').hide();
	$.messager.confirm("Xác nhận", "Bạn có muốn xóa thông tin?", function(r) {
		if(r) {
			VTUtilJS.postFormJson({id:entityId, recordId : $('#id').val()}, '/equipment-checking/delete-customer', function(data) {
				if(data.error) {
					$('#errorMsg').html(data.errMsg).show();
				} else {
					$('#gridCustomer').datagrid('reload');
					setTimeout(function() {
						$('#successMsg').html('').hide();
					}, 3000);
				}
			});
		}
	});
};
var deleteAllCustomer = function() {
	$('#errorMsg').html('').hide();
	$.messager.confirm("Xác nhận", "Bạn có muốn xóa thông tin?", function(r) {
		if(r) {
			VTUtilJS.postFormJson({recordId : $('#id').val()}, '/equipment-checking/delete-customer-all', function(data) {
				if(data.error) {
					$('#errorMsg').html(data.errMsg).show();
				} else {
					$('#gridCustomer').datagrid('reload');
					setTimeout(function() {
						$('#successMsg').html('').hide();
					}, 3000);
				}
			});
		}
	});
};
var onCKShopChange = function() {
	if($('#ckShop').is(':checked')) {
		$('#ckPromotion').removeAttr('checked');
	}else{
		$('#ckPromotion').attr('checked','checked');
	}
};
var onCKPromotionChange = function() {
	if($('#ckPromotion').is(':checked')) {
		$('#ckShop').removeAttr('checked');
	}else{
		$('#ckShop').attr('checked','checked');
	}
};
var saveAndLoadCustomer = function() {
	$('#errorMsg').html('').hide();
// 	if (EquipStatisticChecking._lentghGridGroupProduct == 0 && EquipStatisticChecking._lengthGridShelf == 0) {
// 		$('#errorMsg').html('Biên bản kiểm kê chưa có nhóm thiết bị hoặc danh sách ụ kệ.').show();
// 		return;
// 	}
	if(!$('#ckShop').is(':checked') && !$('#ckPromotion').is(':checked')) {
		$('#errorMsg').html('Bạn chưa chọn hình thức load khách hàng').show();
		return;
	}
	if($('#ckPromotion').is(':checked') && VTUtilJS.isNullOrEmpty($('#promotionCode').val())) {
		$('#errorMsg').html('Bạn chưa nhập mã CT HTTM').show();
		return;
	}
	$.messager.confirm("Xác nhận", "Bạn có muốn lưu thông tin?", function(r) {
		if(r) {
			VTUtilJS.postFormJson({recordId : $('#id').val(), ckShop : $('#ckShop').is(':checked'), ckPromotion : $('#ckPromotion').is(':checked'), promotionCode : $('#promotionCode').val()}, '/equipment-checking/save-load-customer', function(data) {
				
				if(data.error) {
					$('#errorMsg').html(data.errMsg).show();
				} else {
					$('#successMsg').html('Lưu thông tin thành công').show()
					setTimeout(function() {
						$('#successMsg').html('').hide();
					}, 3000);
				}
			});
		}
	});
};

$(document).ready(function() {
	$('#statusCus').customStyle();
	if(!$('#ckShop').is(':checked') && !$('#ckPromotion').is(':checked')) {
		$('#ckShop').attr('checked','checked');
	}
// 	var status = $('#status').val();
	var titleDeleteAll = '';
	if (EquipStatisticChecking.status == statisticStatus.DT) {
		titleDeleteAll = '<a onclick="deleteAllCustomer('+$('#id').val()+')"><span style="cursor:pointer"><img title="Xóa tất cả KH" src="/resources/images/icon-delete.png"/></span></a>';
	}
	$('#customerCode, #customerName').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			EquipStatisticChecking.searchCustomer();
		}
	 }); 
	$('#gridCustomer').datagrid({
		url : '/equipment-checking/search-customer?recordId=' + $('#id').val() + '&id=' + $('#id').val(),
		autoRowHeight : true,
		rownumbers : true, 
		singleSelect: true,
		pagination:true,
		fitColumns:true,
		scrollbarSize:0,
		width: $('#gridCustomerContainer').width(),
		autoWidth: true,
	    columns:[[
		    {field : 'shopCode', title : 'Đơn vị', width : 60, align : 'left', sortable : false, resizable : false, formatter : function(value, row, index) {
		    	return Utils.XSSEncode(row.shopCode);	
		    }},
		    {field : 'shortCode', title : 'Mã khách hàng', width : 60, align : 'left', sortable : false, resizable : false, formatter : function(value, row, index) {
		    	return Utils.XSSEncode(row.shortCode);
		    }},
		    {field : 'customerName', title : 'Tên khách hàng', width : 100, align : 'left', sortable : false, resizable : false, formatter : function(value, row, index) {
		    	return Utils.XSSEncode(row.customerName);
		    }},
		    {field : 'statusCus', title : 'Trạng thái KH', width : 50, align : 'left', sortable : false, resizable : false, formatter : function(value, row, index) {
		    	return activeType.parseText(value);
		    }},
		    {field : 'address', title : 'Địa chỉ', width : 170, align : 'left', sortable : false, resizable : false, formatter : function(value, row, index) {
		    	if(row.address != null && !VTUtilJS.isNullOrEmpty(row.address)) {
	    			return Utils.XSSEncode(row.address);	
	    		} else {
	    			return '';
	    		}
		    }},
		    {field : 'staffCode', title : 'NVBH', width : 170, align : 'left', sortable : false, resizable : false, formatter : function(value, row, index) {
		    	if(row.staffCode != null) {
	    			return Utils.XSSEncode(row.staffCode+' - '+row.staffName);	
	    		} else {
	    			return '';
	    		}
		    }},
		    {field : 'equipCode', title : 'Mã thiết bị', width : 70, align : 'left', sortable : false, resizable : false, formatter : function(value, row, index) {
		    	if(row.equipCode != null) {
	    			return Utils.XSSEncode(row.equipCode);	
	    		} else {
	    			return '';
	    		}
		    }},
		    {field:'delete', title:titleDeleteAll, width: 20, align: 'center', formatter: function (value, row, index) {
// 		    	console.log(row);
		    	if (EquipStatisticChecking.status == statisticStatus.DT && row.id != null && row.id >0) {
		    		return '<a onclick="deleteCustomer('+row.id+')"><span style="cursor:pointer"><img title="Xóa KH" src="/resources/images/icon-delete.png"/></span></a>';			    		
		    	} else {
		    		return '';
		    	}
		    }}
	    ]],	    
	    onLoadSuccess :function(data){	    	
			$('.datagrid-header-rownumber').html('STT');			
			$(window).resize();
		}
	});
	
	$('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_import_khachhang_kiem_ke.xls');
	$('.downloadTemplateReportEx').attr('href',excel_template_path+'/equipment/Bieu_mau_import_thietbiNVBH_kiem_ke.xlsx');
});
</script>
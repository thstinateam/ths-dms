<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<body>
<applet code="EmbedPDF.class" archive="/resources/dms_pdf_viewer.jar" width="<s:property value="windowWidth - 15"/>" height="<s:property value="windowHeight - 16"/>">
    <param name="pdf" value="<s:property value="downloadPath"/>"/> 
	<param name="enableOpenWindow" value="true"/>	
	<param name="enablePrinting" value="true"/>
	<param name="codebase_lookup" value="false"/>
	<param name="classloader_cache" value="false"/>	
	<param name="image" value="splash.gif"/>
	<param name="boxborder" value="false"/>
	<param name="centerimage" value="true"/>
	<param name="permissions" value="sandbox"/>
</applet>
</body>
</html>

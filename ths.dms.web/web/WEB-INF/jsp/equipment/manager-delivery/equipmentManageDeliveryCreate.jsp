<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Thiết bị</a>
		</li>
		<li><span>Lập hợp đồng & biên bản giao nhận</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Mã biên bản giao nhận</label>
					<s:if test="idRecordDelivery == null">
						<input id="recordCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled"/>
					</s:if>
					<s:else>
						<input id="recordCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="recordCode"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Số hợp đồng<span class="ReqiureStyle"> *</span></label>
					<input id="numberContract" type="text" class="InputTextStyle InputText1Style" maxlength="100" value="<s:property value="numberContract"/>"/>
					
					<label class="LabelStyle Label9Style">Ngày hợp đồng<span class="ReqiureStyle"> *</span></label>
					<div  id="contractDateDiv1">
						<input type="text" id="contractDate" class="InputTextStyle InputText9Style" value="<s:property value="contractDate"/>"/>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label3Style" id="divstatus" style="width: 140px; margin-left: 35px;">Trạng thái<span class="ReqiureStyle"> *</span></label>
					<s:if test="idRecordDelivery == null">
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="status" onchange="EquipmentManagerDelivery.changeComboUpdate();">
								<option value="0" selected="selected">Dự thảo</option>
								<option value="2" >Duyệt</option>
							</select>
						</div>
					</s:if>
					<s:else>
						<s:if test="statusRecord == 0">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="status" onchange="EquipmentManagerDelivery.changeComboUpdate();">
									<option value="0" selected="selected">Dự thảo</option>
									<option value="2" >Duyệt</option>
									<option value="4" >Hủy</option>
								</select>
							</div>
						</s:if>
						<s:else>
							<div class="BoxSelect BoxSelect2 BoxDisSelect" >
								<select class="MySelectBoxClass" id="status" disabled="disabled">
									<option value="2" selected="selected">Duyệt</option>
								</select>
							</div>
						</s:else>
					</s:else>
					<label class="LabelStyle Label9Style" id="divstatusDelivery">Trạng thái giao nhận<span class="ReqiureStyle"> *</span></label>
					<s:if test="idRecordDelivery == null">
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="statusDelivery" >
								<option value="0" selected="selected">Chưa gửi</option>
								<!-- <option value="1" >Đã gửi</option>								
								<option value="2" >Đã nhận</option> -->
							</select>
						</div>
					</s:if>
					<s:else>
						<s:if test="statusRecord == 0">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="statusDelivery">
									<option value="0" selected="selected">Chưa gửi</option>
								</select>
							</div>
						</s:if>
						<s:elseif test="statusDelivery == 0">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="statusDelivery">	
									<option value="0" selected="selected">Chưa gửi</option>								
									<option value="1" >Đã gửi</option>
								</select>
							</div>
						</s:elseif>
						<s:elseif test="statusDelivery == 1">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="statusDelivery">	
									<option value="1" selected="selected">Đã gửi</option>								
									<option value="2" >Đã nhận</option>
								</select>
							</div>
						</s:elseif>
						<s:else>
							<div class="BoxSelect BoxSelect2 BoxDisSelect">
								<select class="MySelectBoxClass" id="statusDelivery" disabled="disabled">			
									<option value="2" >Đã nhận</option>
								</select>
							</div>
						</s:else>
					</s:else>
					<label class="LabelStyle Label9Style">Mã biên bản đề nghị mượn thiết bị</label>
					<s:if test="idRecordDelivery == null">
						<input id="equipLendCode" type="text" class="InputTextStyle InputText6Style" disabled="disabled"/>
					</s:if>
					<s:else>
						<input id="equipLendCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="equipLendCode"/>"/>
					</s:else>				
					<div class="Clear"></div>
				</div>			

				<h2 class="Title2Style">Bên cho mượn</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Đơn vị (F9)<span class="ReqiureStyle"> *</span></label>
					<s:if test="fromShopCode == null">
						<input id="shopFromCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
					</s:if>
					<s:else>
						<input id="shopFromCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" onchange="" value="<s:property value="fromShopCode"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Địa chỉ</label>
					<s:if test="fromObjectAddress == null">
						<input id="address" type="text" class="InputTextStyle InputText1Style" maxlength="200"/>
					</s:if>
					<s:else>
						<input id="address" type="text" class="InputTextStyle InputText1Style" maxlength="200"
						value="<s:property value="fromObjectAddress"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Mã số thuế</label>
					<s:if test="fromObjectTax == null">
						<input id="taxCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					</s:if>
					<s:else>
						<input id="taxCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"
						value="<s:property value="fromObjectTax"/>"/>
					</s:else>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Điện thoại</label>
					<s:if test="fromObjectPhone == null">
						<input id="numberPhone" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					</s:if>
					<s:else>
						<input id="numberPhone" type="text" class="InputTextStyle InputText1Style" maxlength="50"
						value="<s:property value="fromObjectPhone"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Người đại diện</label>
					<s:if test="fromRepresentative == null">
						<input id="fromRepresentative" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					</s:if>
					<s:else>
						<input id="fromRepresentative" type="text" class="InputTextStyle InputText1Style" maxlength="50"
						value="<s:property value="fromRepresentative"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Chức vụ</label>
					<s:if test="fromObjectPosition == null">
						<input id="fromPosition" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					</s:if>
					<s:else>
						<input id="fromPosition" type="text" class="InputTextStyle InputText1Style" maxlength="50"
						value="<s:property value="fromObjectPosition"/>"/>
					</s:else>
					<div class="Clear"></div>

					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Giấy ủy quyền</label>
					<s:if test="fromPage == null">
						<input id="fromPage" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					</s:if>
					<s:else>
						<input id="fromPage" type="text" class="InputTextStyle InputText1Style" maxlength="50"
						value="<s:property value="fromPage"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Nơi cấp</label>
					<s:if test="fromPagePlace == null">
						<input id="fromPagePlace" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					</s:if>
					<s:else>
						<input id="fromPagePlace" type="text" class="InputTextStyle InputText1Style" maxlength="50"
						value="<s:property value="fromPagePlace"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Ngày cấp<span class="ReqiureStyle"> *</span></label>
					<div id="contractDateDiv2">
						<input type="text" id="pageDate" class="InputTextStyle InputText9Style" value="<s:property value="fromPageDate"/>"/>
					</div>
					<div class="Clear"></div>
				</div>

				<h2 class="Title2Style">Bên mượn</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Đơn vị (F9)<span class="ReqiureStyle"> *</span></label>
					<s:if test="toShopCode == null">
						<input id="shopToCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					</s:if>
					<s:else>
						<input id="shopToCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" onchange="" value="<s:property value="toShopCode"/>"/>
					</s:else>
					
					<label class="LabelStyle Label9Style">Mã khách hàng (F9)</label>
					<s:if test="customerCode == null">
						<input id="customerCode" type="text" class="InputTextStyle InputText1Style"/>
					</s:if>
					<s:else>
						<input id="customerCode" type="text" class="InputTextStyle InputText1Style"
						value="<s:property value="customerCode"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Địa chỉ đặt tủ</label>
					<s:if test="toObjectAddress == null">
						<input id="toObjectAddress" type="text" class="InputTextStyle InputText1Style" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="toObjectAddress" type="text" class="InputTextStyle InputText1Style" maxlength="250"
						value="<s:property value="toObjectAddress"/>"/>
					</s:else>

					<div class="Clear"></div>
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Số GP ĐKKD</label>
					<s:if test="toBusinessLicense == null">
						<input id="toBusinessLicense" type="text" class="InputTextStyle InputText1Style" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="toBusinessLicense" type="text" class="InputTextStyle InputText1Style" maxlength="250"
						value="<s:property value="toBusinessLicense"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Nơi cấp</label>
					<s:if test="toBusinessPlace == null">
						<input id="toBusinessPlace" type="text" class="InputTextStyle InputText1Style"/>
					</s:if>
					<s:else>
						<input id="toBusinessPlace" type="text" class="InputTextStyle InputText1Style"
						value="<s:property value="toBusinessPlace"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Ngày cấp GPKD<span class="ReqiureStyle"> *</span></label>
					<div id="contractDateDiv3">
						<input type="text" id="toBusinessDate" class="InputTextStyle InputText9Style" value="<s:property value="toBusinessDate"/>" />
					</div>

					<div class="Clear"></div>
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Người đại diện</label>
					<s:if test="toRepresentative == null">
						<input id="toRepresentative" type="text" class="InputTextStyle InputText1Style" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="toRepresentative" type="text" class="InputTextStyle InputText1Style" maxlength="250"
						value="<s:property value="toRepresentative"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Chức vụ</label>
					<s:if test="toObjectPosition == null">
						<input id="toPosition" type="text" class="InputTextStyle InputText1Style" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="toPosition" type="text" class="InputTextStyle InputText1Style" maxlength="250"
						value="<s:property value="toObjectPosition"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Số CMND</label>
					<s:if test="toIdNO == null">
						<input id="toIdNO" type="text" class="InputTextStyle InputText1Style" maxlength="20"/>
					</s:if>
					<s:else>
						<input id="toIdNO" type="text" class="InputTextStyle InputText1Style" maxlength="20"
						value="<s:property value="toIdNO"/>"/>
					</s:else>

					<div class="Clear"></div>

					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Nơi cấp</label>
					<s:if test="toIdNOPlace == null">
						<input id="toIdNOPlace" type="text" class="InputTextStyle InputText1Style" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="toIdNOPlace" type="text" class="InputTextStyle InputText1Style" maxlength="250"
						value="<s:property value="toIdNOPlace"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Ngày cấp<span class="ReqiureStyle"> *</span></label>
					<div id="contractDateDiv4">
						<input type="text" id="idNODate" class="InputTextStyle InputText9Style" value="<s:property value="toIdNODate"/>" />
					</div>
					<!-- <label class="LabelStyle Label9Style">Mã giám sát<span class="ReqiureStyle"> *</span></label>
					<s:if test="idRecordDelivery == null">
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="staffMonitorCode">
														
							</select>
						</div>
					</s:if>
					<s:else>
						<div class="BoxSelect BoxSelect2 BoxDisSelect">
							<select class="MySelectBoxClass" id="staffMonitorCode"  disabled="disabled" >
								<option  value=""/><s:property value="staffCode"/> - <s:property value="staffName"/></option> 							
							</select>
						</div>
					</s:else> -->

					<label class="LabelStyle Label9Style">Mã giám sát</label>
                    <div class="BoxSelect BoxSelect2" id="cbxStaffCode">
                        <select class="easyui-combobox" id="staffMonitorCode">
                        	<s:if test="staffCode != null">
                        		<option value="<s:property value="staffCode"/>"></option>
                        	</s:if>
                            <!-- <option value=""></option>
							<s:iterator value="lstGsnpp" var="obj" >
								<option value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffName" /></option>
							</s:iterator> -->
                        </select>
                    </div>
					<div class="Clear"></div>
				</div>

				<h2 class="Title2Style">Cam kết doanh số</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Tủ mát</label>
					<s:if test="freezer == null">
						<input id="freezer" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					</s:if>
					<s:else>
						<input id="freezer" type="text" class="InputTextStyle InputText1Style" maxlength="50"
						value="<s:property value="freezer"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Tủ đông</label>
					<s:if test="refrigerator == null">
						<input  id="refrigerator" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
					</s:if>
					<s:else>
						<input id="refrigerator" type="text" class="InputTextStyle InputText1Style" maxlength="50"
						value="<s:property value="refrigerator"/>"/>
					</s:else>
					<div class="Clear"></div>			
				</div>

				<h2 class="Title2Style">Tập tin đính kèm</h2>
				<div class="SearchInSection SProduct1Form">
					<a href="javascript:void(0);" style="float: left;" title="Lựa chọn tập tin" ><img class="addFileBtn" src="/resources/images/icon_attach_file.png"></a>
					<div class="fileupload-process" style="width: 90%;">
						<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-striped active" id="total-progress" style="opacity: 0; width: 550px;">
							<div data-dz-uploadprogress="" style="width: 200px;" class="progress-bar progress-bar-success"></div>
						</div>
					</div>
					<div style="float:left; min-width: 500px; width: 90%;">
						<div style="height: auto; margin-left:20px;">
							<s:iterator id="fileVo" value="lstFileVo">
								<div class="divImageUploadFile" id = "divEquipAttachFile<s:property value="#fileVo.fileId"/>">
									<div class="imageUpdateShowIcon">
										<img class="addFileBtn" src="/resources/images/img-folder-001.png" width="50" height="50">
									</div>
									<div class="imageUpdateShowContent">
										<label><s:property value="#fileVo.fileName"/></label><br />
										<a href="javascript:void(0)" onclick="EquipmentManagerDelivery.removeEquipAttachFile(<s:property value="#fileVo.fileId"/>);"><img title="Xóa tập tin" src="/resources/images/icon_delete.png" width="12" height="12" style="padding-left: 5px;"></a>
									</div>
								</div>
							</s:iterator>
						</div>
						<div class="Clear"></div>
						<div class="table table-striped" class="files" id="previews" style="min-height: 100px; margin-left:20px;">
							<div id="template" class="file-row" style="margin-bottom: 10px; width: 260px;float: left">
								<div style="margin-right: 10px; width: 50px; float: left">
									<span class="preview"><img data-dz-thumbnail /></span>
								</div>
								<div style="display: inline-block; width: 200px; clear: both">
									<div style="padding-right: 10px">
										<div>
											<p class="name" style="overflow: hidden" data-dz-name></p>
											<strong class="error text-danger" data-dz-errormessage></strong>
										</div>
										<div style="">
											<p class="size" data-dz-size></p>
											</div>
										<div>
											<a data-dz-remove href="javascript:void(0)"><span><img border="0" src="/resources/scripts/plugins/uploadify2.1/cancel.png"></span></a>
										</div>
									</div>								
								</div>							
							</div> 
						</div>
					</div>
					<div class="Clear"></div>
					<p id="errMsgInfo" class="ErrorMsgStyle" style="display: none;"></p>	
					<p id="successMsgInfo" class="SuccessMsgStyle" style="display: none"></p>
				</div>
				<!-- danh sach bien ban giao nhan -->	
				<h2 class="Title2Style">
					<span>Thiết bị giao nhận</span>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer1">
						<table id="gridEquipmentDelivery"></table>
					</div>
				</div>
				<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnUpdate" class="BtnGeneralStyle cmsiscontrol" >Cập nhật</button>	
							<s:if test="idRecordDelivery == null">	
								<button id="btnClose" class="BtnGeneralStyle cmsiscontrol" onclick="location.href = '/equipment-manage-delivery/info';">Bỏ qua</button>
							</s:if>
							<s:else>	
								<button id="btnClose" class="BtnGeneralStyle cmsiscontrol" onclick="location.href = '/equipment-manage-delivery/change-record?id=<s:property value="idRecordDelivery"/>';">Bỏ qua</button>
							</s:else>
						</div>
						<div class="Clear"></div>
					</div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup thiet bi giao nhan -->
<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchEquipment" class="easyui-dialog"
		title="Chọn thiết bị"
		data-options="closed:true,modal:true" >
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label6Style">Mã thiết bị</label>
				<input id="equipmentCode" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label7Style">Số serial</label>
				<input id="seriNumber" type="text" class="InputTextStyle InputText4Style" maxlength="100"/>
				<label class="LabelStyle Label7Style">Loại</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="typeEquipment" onchange="EquipmentManagerDelivery.onChangeTypeEquip(this);">
						<option value="">Tất cả</option>
					</select>
				</div>
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Nhóm thiết bị</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="groupEquipment" onchange="">
						<option value="">Tất cả</option>
					</select>
				</div>
				<label class="LabelStyle Label7Style">Nhà cung cấp</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="providerId" onchange="">
						<option value="">Tất cả</option>
					</select>
				</div>					
				<label class="LabelStyle Label7Style">Năm sản xuất</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="yearManufacturing">
					</select>
				</div>
				<!-- <div id="contractDateDiv">
					<input type="text" id="yearManufacturing" class="InputTextStyle InputText5Style"/>
				</div> -->
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Kho (F9)</label>
				<s:if test="idRecordDelivery == null">
					<input type="text" id="stockCode" class="InputTextStyle InputText4Style" />
				</s:if>
				<s:else>
					<input type="text" id="stockCode" class="InputTextStyle InputText4Style" />
				</s:else> 				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="EquipmentManagerDelivery.searchEquipment();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="equipmentGridDialogContainer">
					<table id="equipmentGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchEquipment').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!-- popup chon kho -->
<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchStock" class="easyui-dialog"
		title="Chọn kho"
		data-options="closed:true,modal:true">
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label1Style">Mã đơn vị</label>
				<input id="shopCodePopupStock" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label1Style">Tên đơn vị</label>
				<input id="shopNamePopupStock" type="text" class="InputTextStyle InputText5Style" maxlength="100"/>				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSearchStockDlg" class="BtnGeneralStyle" onclick="EquipmentManagerDelivery.searchStock();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="stockGridDialogContainer">
					<table id="stockGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnStockClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchStock').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!--Popup timf kiem kho nguon - kho dich  -->
<div id ="productDialog" style="width:600px;visibility: hidden;" >
	<div id="easyuiPopupSearchStock" class="easyui-dialog"	title="Tìm kiếm kho"
		data-options="closed:true,modal:true">
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label1Style">Mã đơn vị</label>
				<input id="shopCodePopupStock" type="text" class="InputTextStyle InputText4Style"  maxlength="50" />
				<label class="LabelStyle Label1Style">Tên đơn vị</label>
				<input id="shopNamePopupStock" type="text" class="InputTextStyle InputText4Style"  maxlength="100"/>				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="EquipmentStockChange.searchStockChange();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="stockGridDialogContainer">
					<table id="stockGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchStock').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!-- popup chon thiet bi -->
<div style="display: none">
	<div id="easyuiPopupSelectEquipment" class="easyui-dialog" title="Chọn thiết bị" data-options="closed:true,modal:true" >
		<div class="PopupContentMid">
	   		<div class="SearchInSection SProduct1Form" style="border:none;">
				<div class="GridSection" id="gridSelectEquipmentContainer">
					<table id="gridSelectEquipment"></table>
				</div>
			</div>        
		</div>
	</div>
</div>
<div><input type="hidden" id="idRecordHidden" value="<s:property value="idRecordDelivery"/>"></div>
<div><input type="hidden" id="statusRecordHidden" value="<s:property value="statusRecord"/>"></div>
<div><input type="hidden" id="statusDeliveryHidden" value="<s:property value="statusDelivery"/>"></div>
<div><input type="hidden" id="customerCodeHidden" value="<s:property value="customerCode"/>"></div>
<div><input type="hidden" id="customerIdHidden" value="<s:property value="customerId"/>"></div>
<div><input type="hidden" id="fromShopHidden" value="<s:property value="toShopCode"/>"></div>
<div><input type="hidden" id="lstEquipGroupId" value="<s:property value="lstEquipGroupId"/>"></div>
<script type="text/javascript">
$(document).ready(function() {	
	setDateTimePicker('contractDate');	
	setDateTimePicker('pageDate');
	setDateTimePicker('idNODate');
	setDateTimePicker('toBusinessDate');
	Utils.bindComboboxStaffEasyUI('staffMonitorCode');
	$("#customerCode").val($("#customerCodeHidden").val());

	EquipmentManagerDelivery._countFile = Number('<s:property value="lstFileVo.size()"/>');
	$('#shopFromCode, #recordCode, #staffMonitorCode, #shopToCode').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnUpdate').click(); 
		}
	});
	$('#customerCode, #numberContract, #contractDate').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnUpdate').click(); 
		}
	});
	$('#status, #statusDelivery').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnUpdate').click(); 
		}
	});	
	
	if ($("#statusRecordHidden").value == 2) { // da duyet

	}


	$('#shopFromCode').bind('keyup', function(e){
		if(e.keyCode==keyCode_F9){
			VCommonJS.showDialogSearch2({
				//params : {},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Đơn vị'},
			        {id:'name', maxlength:250, label:'Tên Đơn vị'},
			    ],
			    url : '/commons/search-shop-show-list-NPP',
			    columns : [[
			        {field:'shopCode', title:'Mã Đơn vị', align:'left', width: 110, sortable:false, resizable:false},
			        {field:'shopName', title:'Tên Đơn vị', align:'left', width: 200, sortable:false, resizable:false},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			            var html =  '<a href="javascript:void(0)" onclick="EquipmentManagerDelivery.selectFromShop({0},\'{1}\',\'{2}\')" >Chọn</a>';
			            return format(html,row.id,row.shopCode,row.shopName);
			        }}
			    ]]
			});				
			return false;
		}
	});	
	$('#shopToCode').bind('keyup', function(e){
		if(e.keyCode==keyCode_F9){
			VCommonJS.showDialogSearch2({
				//params : {},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Đơn vị'},
			        {id:'name', maxlength:250, label:'Tên Đơn vị'},
			    ],
			    url : '/commons/search-shop-show-list-NPP-GT-MT',
			    columns : [[
			        {field:'shopCode', title:'Mã Đơn vị', align:'left', width: 110, sortable:false, resizable:false},
			        {field:'shopName', title:'Tên Đơn vị', align:'left', width: 200, sortable:false, resizable:false},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			            var html =  '<a href="javascript:void(0)" onclick="EquipmentManagerDelivery.selectToShop({0},\'{1}\',\'{2}\')" >Chọn</a>';
			            return format(html,row.shopId,row.shopCode,row.shopName);
			        }}
			    ]]
			});				
			return false;
		}
	});	
	$('#customerCode').bind('keyup', function(event) {					
		if (event.keyCode == keyCode_F9) {
			$("#errMsgInfo").html('').hide();
			if($('#shopToCode').val().trim()== null || $('#shopToCode').val().trim()==''){
				$("#errMsgInfo").html('Xin chọn đơn vị trước khi nhấn F9!').show();
				return;
			}
			VCommonJS.showDialogSearch2({
				params : {
					shopCode: $('#shopToCode').val().trim()
				},
			    inputs : [
			        {id:'code', maxlength:250, label:'Khách hàng', width:410}
// 			        {id:'name', maxlength:250, label:'Tên KH'},
// 			        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
			    ],			   
			    url : '/commons/search-Customer-Show-List',			   
			    columns : [[
			        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false},
			        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false},
			        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	var cus = row;
			        	return "<a href='javascript:void(0)' onclick=\"return EquipmentManagerDelivery.selectCustomer('"+ row.shortCode + "'," + row.id + ",'" + row.street + "','" + row.housenumber + "',"+ row.areaId +");\">Chọn</a>";        
			        }}
			    ]]
			});
		} 
	});	

	 // Utils.functionAccessFillControl('contractDateDiv1');
	 // Utils.functionAccessFillControl('contractDateDiv2');
	 // Utils.functionAccessFillControl('contractDateDiv3');
	 // Utils.functionAccessFillControl('contractDateDiv4');
	
	// $("#staffMonitorCode").combobox({ 
	//     select: function (event, ui) { 
	//         alert('dfgsdghsdfgdfg');
	//     } 
	// });
	$('#gridEquipmentDelivery').datagrid({
		//url : '/equipment-manage-delivery/search-equipment-in-record',
		autoRowHeight : true,
		rownumbers : true, 
		//checkOnSelect :true,
		//selectOnCheck: true,
		singleSelect: true,
		// pagination:true,
		 fitColumns:true,
		// pageSize :20,
		// pageList  : [20],
		scrollbarSize:0,
		width: $('#gridContainer1').width(),
		autoWidth: true,	
		queryParams:{
			idRecordDelivery: $("#idRecordHidden").val()
		},
		columns:[[		  
			{field: 'typeEquipment',title:'Loại thiết bị', width: 80, sortable:false,resizable:false, align: 'left' },
			{field: 'groupEquipment',title:'Nhóm thiết bị', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
		    	var html="";
		    	if(row.groupEquipmentCode != null && row.groupEquipmentName!= null){
					html = row.groupEquipmentCode+' - '+row.groupEquipmentName; 
		    	}
				return Utils.XSSEncode(html);
			}},
			{field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'left' },
			{field: 'equipmentCode',title:'Mã thiết bị (F9)',width: 220,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var html = '<input type="text" id="equipmentCodeInGrid" size="11">';
				if(row.equipmentCode!=null && row.equipmentCode != undefined && row.equipmentCode!=''){
					if($('#equipmentCodeInGrid').val() != undefined ){
						html = '<input type="text" id="equipmentCodeInGrid" size="11" value="'+row.equipmentCode+'">';					
					}else{
						html = Utils.XSSEncode(row.equipmentCode); 						
					}						
				}
				return html;
			}},
			{field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var html = '<input type="text" id="seriNumberInGrid' + index + '" size="11">';				
				if(row.seriNumber!=null && row.seriNumber != undefined && row.seriNumber!=''){						
					html = Utils.XSSEncode(row.seriNumber); 
				}	 
				return html;
			}},
			{field: 'contentDelivery',title:'Nội dung',width: 180,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {	 
				var html = ''; 
				if(row.contentDelivery == 0){
					html+='Cấp mới';
				}else if(row.contentDelivery == 1){
					html+='Điều chuyển từ KH Khác';
				}else if(row.contentDelivery == 2){
					html+='Khác';
				}else{
					if(row.seriNumber == null && row.equipmentCode!=null){
						html = '<select id="contentDelivery" style="width:150;"><option value="0" selected="selected">Cấp mới</option></select>';
					}else{
						html = '<select id="contentDelivery" style="width:150;"><option value="0" selected="selected">Cấp mới</option> <option value="1" >Điều chuyển từ KH Khác</option> <option value="2" >Khác</option></select>';
					}				
				}
				return html;
			}},
			{field: 'depreciation',title:'Số tháng khấu hao',width: 100,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
				var html = '<input type="text" id="depreciationInGrid' + index + '" size="11">';
		    	if (row.depreciation != null) {
					html = Utils.XSSEncode(row.depreciation);
		    	}
				return html;
			}},
			{field: 'price',title:'Giá trị TBBH',width: 100,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
				return VTUtilJS.XSSEncode(value);  
			}},
			{field: 'stockName', title: 'Kho', width: 150, sortable:false,resizable:false, hidden:true,align: 'left', formatter: function(value, row, index) {
		    	var html = '<input type="text" id="stockName" size="11">';
		    	if (row.stockName != null) {
					html = Utils.XSSEncode(row.stockName);
		    	}
				return html;
			}},	
			{field: 'stockCode', title: 'Kho', width: 150, sortable:false,resizable:false,align: 'left', formatter: function(value, row, index) {
		    	var html = '<input type="text" id="stockInGrid" size="11">';
		    	if (row.stockCode != null && row.stockName != null) {
					html = Utils.XSSEncode(row.stockCode + ' - ' + row.stockName); 
		    	}
				return html;
			}},
		    {field: 'healthStatus', title: 'Tình trạng thiết bị', width: 100, sortable:false,resizable:false,align: 'left'},
		    {field: 'firstYearInUse',title:'Năm bắt đầu sử dụng',width: 100,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
				return VTUtilJS.XSSEncode(value);  
			}},
			{field:'delete', title:'<a href="javascript:void(0);"  id="dg_add_change" onclick="EquipmentManagerDelivery.addEquipmentInRecord();"><img src="/resources/images/icon_add.png" title="Thêm thiết bị"/></a>', width:50, align:'center', formatter: function(value, row, index) {
			    var	html='';
			    var deleteRow = 'dg_delete_change_' + index;
			    if($("#statusRecordHidden").val() != undefined && $("#statusRecordHidden").val() == '0'){
			    	html = '<a href="javascript:void(0);"  id="'+deleteRow+'" onclick="EquipmentManagerDelivery.deleteEquipmentForDelivery('+index+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
			    }else if(EquipmentManagerDelivery._numberEquipInRecord !=null && index >= EquipmentManagerDelivery._numberEquipInRecord){
			    	html = '<a href="javascript:void(0);"  id="'+deleteRow+'" onclick="EquipmentManagerDelivery.deleteEquipmentForDelivery('+index+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
			    }else if($("#idRecordHidden").val() == ""){
					html = '<a href="javascript:void(0);"  id="'+deleteRow+'" onclick="EquipmentManagerDelivery.deleteEquipmentForDelivery('+index+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
			    }
			    return html;
			}},
		]],
		onAfterEdit : function(index, row, changes){
			
		},
		onLoadSuccess :function(data){   			 	    	
	    	$(window).resize();	
	    	if (data == null || data.total == 0) {
				//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
			} else {
				$('.datagrid-header-rownumber').html('STT');	
				if($("#idRecordHidden").val() != undefined && $("#idRecordHidden").val() != ""){
					EquipmentManagerDelivery._numberEquipInRecord = data.total;
				}
			}
			 Utils.functionAccessFillControl('gridContainer1');
			if($('#gridContainer1 [id^="dg_"]').length == 0){
				$('#gridEquipmentDelivery').datagrid("hideColumn", "delete");
			} else {
				$('#gridEquipmentDelivery').datagrid("showColumn", "delete");
			}
		}
	});
	if($("#idRecordHidden").val() != undefined && $("#idRecordHidden").val() != ""){
		$('#gridEquipmentDelivery').datagrid({url : '/equipment-manage-delivery/search-equipment-in-record',queryParams:{idRecordDelivery:$("#idRecordHidden").val()}});
		$('#btnUpdate').bind('click', function(event) {		
			EquipmentManagerDelivery.updateRecordDelivery();
		});
	}else{
		$('#btnUpdate').bind('click', function(event) {		
			EquipmentManagerDelivery.createRecordDelivery();
		});
		EquipmentManagerDelivery.insertEquipmentInGrid();
	}
	$('#shopToCode').focus();
	// upload file
	var urlUpdate="";
	if($("#idRecordHidden").val() == ""){
		urlUpdate = "/equipment-manage-delivery/create-record-delivery";
	}else{
		urlUpdate = "/equipment-manage-delivery/update-record-delivery";
	}
	UploadUtil.initUploadFileByEquipment({
		url: urlUpdate,	
		elementSelector: 'body',
		elementId: 'body',
		clickableElement: '.addFileBtn',
		parallelUploads: 5
		//maxFilesize: 5
	}, function (data) {
		if(data.error){
			$('#errMsgInfo').html(data.errMsg).show();
			return false;
		}else{
			if($("#idRecordHidden").val() == ""){
				$('#successMsgInfo').html("Cập dữ liệu thành công.").show();	
				EquipmentManagerDelivery._lstEquipInsert = null;
				EquipmentManagerDelivery._editIndex = null;				
				setTimeout(function(){
					$('#successMsgInfo').html("").hide(); 
					window.location.href = '/equipment-manage-delivery/info';						              		
				}, 3000);
			}else{
				$('#successMsgInfo').html("Cập dữ liệu thành công.").show();
				EquipmentManagerDelivery._lstEquipInsert = null;
				EquipmentManagerDelivery._editIndex = null;
				EquipmentManagerDelivery._lstEquipInRecord = null;
				EquipmentManagerDelivery._numberEquipInRecord = null;
				if($('#status').val() != 4){					
					setTimeout(function(){
						$('#successMsgInfo').html("").hide(); 
						window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
					}, 3000);
				}else{
					setTimeout(function(){
						window.location.href = '/equipment-manage-delivery/info';						              		
					}, 3000);
				}
			}
		}
	});
	EquipmentManagerDelivery.selectToShop();
});
</script>
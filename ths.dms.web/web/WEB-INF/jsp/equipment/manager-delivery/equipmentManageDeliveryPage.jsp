<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Thiết bị</a>
		</li>
		<li><span>Quản lý giao nhận & hợp đồng</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<!-- <h2 class="Title2Style">Thông tin tìm kiếm</h2> -->
				<h2 class="Title2Style">Thông tin tìm kiếm <span style="float:right;margin-right:15px;">
					<a href="javascript:void(0);" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
				<div class="SearchInSection SProduct1Form" style="padding-left: 0px;">
				 	<!-- <div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin tìm kiếm" style="width: 'auto'; height: 'auto'; padding-top: 20px;"data-options="collapsible:true,closable:false,border:false"> -->
						<label class="LabelStyle Label3Style" style="width: 120px; ">Đơn vị</label>
						<div class="BoxSelect BoxSelect2">				
							<div class="Field2">
								<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="height: auto;" class="InputTextStyle InputText1Style" />
							</div>
						</div>
						<label class="LabelStyle Label17Style" style="width: 220px; margin-left: 2.4%;">Khách hàng</label>
						<input id="customerCode" type="text" class="InputTextStyle InputText3Style" style="width: 650px;" autocomplete="off" maxlength="300" placeholder="Nhập mã khách hàng hoặc tên khách hàng hoặc địa chỉ"/>
						<%-- <label class="LabelStyle"><span>&nbsp;&nbsp;&nbsp;</span></label> --%>
<!-- 						<input id="customerName" type="text" class="InputTextStyle InputText3Style" maxlength="250" style="width: 380px; margin-left: 10px;" placeholder="Tên khách hàng hoặc địa chỉ"/> -->
						
						<div class="Clear"></div>

						<label class="LabelStyle Label3Style" style="width: 120px; ">Mã thiết bị</label>
						<input id="equipCode" type="text" class="InputTextStyle InputText1Style" maxlength="100" />
						
						<label style="width: 220px; margin-left: 2.4%;" class="LabelStyle Label16Style">Mã biên bản đề nghị mượn thiết bị</label>
						<input id="equipLendCode" type="text" class="InputTextStyle InputText1Style" maxlength="100" />
						
						<label class="LabelStyle Label17Style" ">Mã giám sát</label>
						<input id="staffCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
						<div class="Clear"></div>

						<label class="LabelStyle Label3Style" style="width: 120px; ">Mã biên bản</label>
						<input id="recordCode" type="text" class="InputTextStyle InputText1Style" maxlength="100" />
						
						<label class="LabelStyle Label17Style" style="width: 220px; margin-left: 2.4%;">Ngày biên bản từ</label>
						<div id="fromDateDiv">
							<input type="text" id="fDate" class="InputTextStyle InputText6Style"/>
						</div>
						<label class="LabelStyle Label17Style">Đến</label>
						<div id="toDateDiv">
							<input type="text" id="tDate" class="InputTextStyle InputText6Style"/>
						</div>
						<div class="Clear"></div>

						<label class="LabelStyle Label3Style" style="width: 120px; ">Số hợp đồng</label>
						<input id="numberContract" type="text" class="InputTextStyle InputText1Style" maxlength="50" "/>
						
						<label class="LabelStyle Label17Style" style="width: 220px; margin-left: 2.4%;">Ngày hợp đồng từ</label>
						<div id="fromDateDiv">
							<input  type="text" id="fDateContract" class="InputTextStyle InputText6Style"/>
						</div>
						<label class="LabelStyle Label17Style">Đến</label>
						<div  id="toDateDiv">
							<input  type="text" id="tDateContract" class="InputTextStyle InputText6Style"/>
						</div>
						<div class="Clear"></div>
										
						<label class="LabelStyle Label3Style" style="width: 120px; ">Trạng thái biên bản</label>
						<div  class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="statusRecord" onchange="EquipmentManagerDelivery.changeComboSearch();">
								<option value="" selected="selected">Tất cả</option>
								<option value="0" >Dự thảo</option>
								<option value="2" >Đã duyệt</option>
								<option value="4" >Hủy</option>
							</select>
						</div>
						<label  class="LabelStyle Label17Style" style="width: 220px; margin-left: 2.4%;">Trạng thái giao nhận</label>
						<div  class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="statusDelivery">
								<option value="" selected="selected">Tất cả</option>
								<option value="0" >Chưa giao</option>
								<option value="1" >Đã giao</option>
								<option value="2" >Đã nhận</option>
							</select>
						</div>
						<label  class="LabelStyle Label17Style">Trạng thái in</label>
						<div  class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="statusPrint">
								<option value="" selected="selected">Tất cả</option>
								<option value="1" >Đã in</option>
								<option value="0" >Chưa in</option>
							</select>
						</div>
						<div class="Clear"></div>

						<div class="SearchInSection SProduct1Form">
							<div class="BtnCenterSection">
								<button id="btnSearch" class="BtnGeneralStyle" onclick="EquipmentManagerDelivery.searchRecordDelivery();">Tìm kiếm</button>
								<!-- <button id="btnSearch" class="BtnGeneralStyle" onclick="location.href='/equipment-manage-delivery/change-record';">Tạo mới</button> -->
							</div>
						</div>	
						<p id="errMsgInfo" class="ErrorMsgStyle" style="display: none;"></p>
						<p id="successMsgInfo" class="SuccessMsgStyle" style="display: none"></p>										
					<!-- </div>			 -->
				</div>
				<!-- danh sach bien ban giao nhan -->	
				<div class="Title2Style" style="height:15px;font-weight: bold;">
					<span style="float:left;">Danh sách biên bản giao nhận & hợp đồng </span>
					<div style="float:right; margin-top: -5px;margin-right: 27px;">
						<a href="javascript:void(0);" class="cmsiscontrol" id="btnPrint" onclick="EquipmentManagerDelivery.ShowPopUpPrint();" ><img src="/resources/images/icon-printer.png" height="20" width="20" title="In"></a>
						<a href="javascript:void(0);" onclick="EquipmentManagerDelivery.downloadTemplate();" class="cmsiscontrol" id="btnTemplate"><img src="/resources/images/icon_download_32.png" height="20" width="20" style="border-left-color: blue; border-left-style: solid; border-left-width: 2px;" title="Tải tập tin mẫu"></a>
						<a href="javascript:void(0);" class="cmsiscontrol" id="btnImport" onclick="EquipmentManagerDelivery.showDlgImportExcel();"><img src="/resources/images/icon_table_import.png" height="20" width="20" title="Nhập excel"></a>
						<a href="javascript:void(0);" class="cmsiscontrol" id="btnExport" onclick="EquipmentManagerDelivery.exportExcelBBGN();" style="margin-right: 5px;"><img src="/resources/images/icon_table_export.png" height="20" width="20" title="Xuất excel"></a>					
						<div style="float:right;">
							<div style="margin-right: 5px;" class="BoxSelect BoxSelect11 cmsiscontrol" id = "statusDeliveryLabelClass">
								<select class="MySelectBoxClass cmsiscontrol" id="statusDeliveryLabel" style="width:112px;">
									<option value="" selected="selected">Chọn</option>
									<option value="1" >Đã gửi</option>
									<option value="2" >Đã nhận</option>
								</select>
							</div>
							<div style="margin-right: 5px;" class="BoxSelect BoxSelect11 cmsiscontrol" id = "statusRecordLabelsClass">
								<select class="MySelectBoxClass cmsiscontrol" id="statusRecordLabel" style="width:112px;" onchange="EquipmentManagerDelivery.changeCombo();">
									<option value="" selected="selected">Chọn</option>
									<option value="2" >Duyệt</option>
									<option value="4" >Hủy</option>
								</select>
							</div>
							<button id="btnSaveLst" class="BtnGeneralStyle cmsiscontrol" onclick="EquipmentManagerDelivery.saveRecordDelivery();" >Lưu</button>
						</div>
					</div>
				</div>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer" >
						<table id="gridRecordDelivery"></table>
					</div>
				</div>							
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup lich su giao nhan -->
<div style="display: none">
	<div id="easyuiPopupHistory" class="easyui-dialog" title="Lịch sử giao nhận" data-options="closed:true,modal:true" >
		<div class="PopupContentMid">
	   		<div class="GridSection" id="gridHistory">
				<table id="gridHistoryDelivery"></table>
			</div>
		</div>
	</div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Import - Biên bản giao nhận" data-options="closed:true,modal:true" style="width:470px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-manage-delivery/import-excel-record-delivery" name="importFrmBBGN" id="importFrmBBGN" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">
						<input id="fakefilepcBBGN" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileBBGN" onchange="previewImportExcelFile(this,'importFrmBBGN','fakefilepcBBGN', 'errExcelMsgBBGN');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="">
					<!-- <a onclick="" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a> -->
					<a onclick="EquipmentManagerDelivery.downloadTemplate();" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadExcelFile" onclick="return EquipmentManagerDelivery.importExcelBBGN();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgBBGN"/>
	</div>	
</div>
<!-- popup in -->
<div style="display: none">
  	<div id="easyuiPopupPrint" class="easyui-dialog" title="In" data-options="closed:true,modal:true" style="width:470px;">
		<div class="PopupContentMid" style="color: #215ea2;">
			<div class="GeneralForm ImportExcel1Form">			     	
		        <input type="checkbox" id="printDelivery" style="margin-left: 20px; margin-top: 5px;"> In biên bản giao nhận</input>
		        <div class="Clear"></div>
		        <input type="checkbox" id="printRecord" style="margin-left: 20px; margin-top: 5px;"> In hợp đồng</input>			            
		        <div class="Clear"></div>
		        <input type="checkbox" id="printLend" style="margin-left: 20px; margin-top: 5px;"> In phiếu đề nghị giao thiết bị</input>	
				<div class="Clear"></div>
		        <label id="labelChoose" class="LabelStyle Label9Style" style="margin-left: 40px; margin-top: 5px;">Đối tượng<span class="ReqiureStyle">(*)</span></label>
				<div class="BoxSelect BoxSelect2" id="comboboxPrint"style="margin-top: 5px;">
					<select class="MySelectBoxClass" id="statusIn">
						<option value="0" >Tất cả</option>
						<option value="1" >Phòng dự án</option>
						<option value="2" >XNKV, kho chi nhánh</option>
						<option value="3" selected="selected">Nhà phân phối</option>

					</select>
				</div>
				<button id="btnSearch" style="width: 90px; margin-left: 175px; margin-top: 10px;" class="BtnGeneralStyle" onclick="EquipmentManagerDelivery.printBBGN();">In</button>
			</div>	
			<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgPrint"/>
	</div>	
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="curShopCode" name="curShopCode"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {	
	EquipmentManagerDelivery._curShopId = '<s:property value="shopId"/>';
	//$('#panelDeliveryRecord').width($(window).width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
		$('#recordCode').focus();
	},1000);
	$('#customerName').width($('#numberContract').width()+$($("label[class='LabelStyle Label17Style']")[0]).width());
		
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar("fDate");
	ReportUtils.setCurrentDateForCalendar("tDate");
	setDateTimePicker('fDateContract');
	setDateTimePicker('tDateContract');
	//load dieu kien mac dinh 
	$('#staffCode').val('');
	$('#customerCode').val('');
	$('#customerName').val('');
	// $('#fDate').val('');
	// $('#tDate').val('');
	// $('#fDateContract').val('');
	// $('#tDateContract').val('');
	$('#numberContract').val('');
	$('#recordCode').val('');
	// $('#statusRecord').val('');
	// $('#statusDelivery').val('');
	// $('#statusPrint').val('');
	$('#equipLendCode').val('');

	
	// ReportUtils.setCurrentDateForCalendar("fDateContract");
	// ReportUtils.setCurrentDateForCalendar("tDateContract");	
	$('#statusRecord').val('').change();
	$('#statusDelivery').val('').change();
	
	// $('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_import_bien_ban_giao_nhan_thiet_bi.xls');
	
	$('#staffCode, #customerCode, #customerName').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});
	$('#fDate, #tDate, #numberContract, #fDateContract, #tDateContract').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});
	$('#recordCode, #statusRecord, #statusDelivery, #equipLendCode').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});
	
	$('#excelFileBBGN').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#uploadExcelFile').click(); 
		}
	});
	EquipmentManagerDelivery._mapDelivery = new Map();

	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 3) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 4 || level == 21) {
					return '<div class="tree-kenh" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-kenh-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-kenh-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 18 || level == 22){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 6){
				return '<div class="tree-vung-con" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-con-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-con-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
//         value: [$('#curShopId').val()]
        value: [EquipmentManagerDelivery._curShopId]
    });
	
	var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");

 //    var shopKendo = $("#shop").data("kendoMultiSelect");
 //    shopKendo.wrapper.attr("id", "shop-wrapper"); 
	
 //    if (AppData.hasOwnProperty('equipment') && AppData.equipment.hasOwnProperty('delivery') 
 //    	&& AppData.equipment.delivery.hasOwnProperty('search_param')) {
	// 	$('#staffCode').val(AppData.equipment.delivery.search_param.staffCode);
	// 	$('#customerCode').val(AppData.equipment.delivery.search_param.customerCode);
	// 	$('#customerName').val(AppData.equipment.delivery.search_param.customerName);
	// 	$('#fDate').val(AppData.equipment.delivery.search_param.fromDate);
	// 	$('#tDate').val(AppData.equipment.delivery.search_param.toDate);
	// 	$('#fDateContract').val(AppData.equipment.delivery.search_param.fromContractDate );
	// 	$('#tDateContract').val(AppData.equipment.delivery.search_param.toContractDate);
	// 	$('#numberContract').val(AppData.equipment.delivery.search_param.numberContract);
	// 	$('#recordCode').val(AppData.equipment.delivery.search_param.recordCode);
	// 	$('#statusRecord').val(AppData.equipment.delivery.search_param.statusRecord);
	// 	$('#statusDelivery').val(AppData.equipment.delivery.search_param.statusDelivery);
	// 	$('#statusPrint').val(AppData.equipment.delivery.search_param.statusPrint);
	// 	$('#equipLendCode').val(AppData.equipment.delivery.search_param.equipLendCode);
	// 	var selected = $("#shop").data("kendoMultiSelect").value();
	// 	var res = $.merge($.merge([], selected), AppData.equipment.delivery.search_param.mutilShop);
	// 	$("#shop").data("kendoMultiSelect").value(res);
	// }

	// danh sach bien ban
	var params=new Object(); 
		params.staffCode = $('#staffCode').val().trim();
		params.customerCode = $('#customerCode').val().trim();
// 		params.customerName = $('#customerName').val().trim();
		params.fromDate = $('#fDate').val().trim();
		params.toDate = $('#tDate').val().trim();
		params.fromContractDate = $('#fDateContract').val().trim();
		params.toContractDate = $('#tDateContract').val().trim();
		params.numberContract = $('#numberContract').val().trim();
		params.recordCode = $('#recordCode').val().trim();
		params.statusRecord = $('#statusRecord').val().trim();
		params.statusDelivery = $('#statusDelivery').val().trim();
		params.statusPrint = $('#statusPrint').val().trim();
		params.equipLendCode = $('#equipLendCode').val().trim();
		var multiShop = $("#shop").data("kendoMultiSelect");
		var dataShop = multiShop.dataItems();
		if(dataShop!=null && dataShop.length>0) {
			var lstShop = new Array();
			var curShopCode = $('#curShopCode').val();
			for(var i=0;i<dataShop.length;i++){
				if(dataShop[i].shopCode != curShopCode) {
					lstShop.push(dataShop[i].shopCode);
				}
			}
			params.lstShop = lstShop.join(",");
		}
	

	$('#gridRecordDelivery').datagrid({
		url : '/equipment-manage-delivery/search-record-delivery',
		autoRowHeight : true,
		rownumbers : true, 
		checkOnSelect :true,		
		pagination:true,
 		// fitColumns:true,
		pageSize :20,
		pageList : [20, 50, 100],
		scrollbarSize:15,
		width:  $('#gridContainer').width(),
		height: 400,
// 		autoWidth: true,	
		// queryParams:{
		// 	fromDate: $('#fDate').val().trim(),
		// 	toDate: $('#tDate').val().trim(),
		// 	recordCode: $('#recordCode').val().trim(),
		// },
		queryParams: params,
		frozenColumns:[[
	        {field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left' },
	        {field:'edit', title:'<a href="javascript:void(0);" class="cmsiscontrol" id="dg_add" onclick="EquipmentManagerDelivery.checkPeriod(-1);"><img src="/resources/images/icon_add.png" title="Tạo mới"/></a>', width:80, align:'left', formatter: function(value, row, index) {
			    var html='';
			    if(row.statusRecord != null && row.statusRecord != undefined){
			    	var divdgView = 'dg_view' + row.idRecord;
			    	var divDGEdit = 'dg_edit' + row.idRecord;
			    	html='<a href="javascript:void(0);" class="cmsiscontrol" id="'+divdgView+'" onclick="EquipmentManagerDelivery.ShowPopUpPrint('+row.idRecord+','+row.statusRecord+');"><img src="/resources/images/icon-printer.png" height="20" width="20" style="margin-right:5px;" title="In biên bản"></a>';
			    	if(row.statusRecord != 4){
				    	html+='<a href="javascript:void(0);" class="cmsiscontrol" id="'+divDGEdit+'" onclick="EquipmentManagerDelivery.checkPeriod('+row.idRecord+');"><img src="/resources/images/icon-edit.png" height="20" width="20" title="Chỉnh sửa"></a>';
				    } 
			    }				
				return html;
			}},
			{field: 'shopCodeName', title:'Đơn vị', width: 250, sortable:false, resizable:false, align: 'left' , formatter: function(value, row, index) {
				return Utils.XSSEncode(row.shopCode) + ' - ' + Utils.XSSEncode(row.shopName);
		    }},
	        {field: 'recordCode',title:'Mã biên bản', width: 110, sortable:false,resizable:false, align: 'left'  ,formatter: function(value, row, index){
				return Utils.XSSEncode(value);
				
		    }},
   		 ]],
		columns:[[		  
			// {field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left' },
			// {field: 'shopName',title:'Đơn vị', width: 100, sortable:false,resizable:false, align: 'left' ,formatter: function(value, row, index){
			// 		return Utils.XSSEncode(row.shopCode + ' - ' + row.shopName);
				
		 //    }},
			// {field: 'recordCode',title:'Mã biên bản', width: 100, sortable:false,resizable:false, align: 'left'  ,formatter: function(value, row, index){
			// 		return Utils.XSSEncode(value);
				
		 //    }},
			{field: 'staffInfo',title:'Nhân viên', width: 250, sortable:false,resizable:false, align: 'left' ,formatter: function(value, row, index){
				var html="";
		    	if(row.staffCode != null && row.staffName!= null){
		    		return Utils.XSSEncode(row.staffCode) + ' - ' + Utils.XSSEncode(row.staffName);
		    	}
				return html;
				
		    }},
			// {field: 'staffType',title:'Chức vụ', width: 80, sortable:false,resizable:false, align: 'left' },
			{field: 'customerInfo',title:'Khách hàng',width: 250,sortable:false,resizable:false, align: 'left' ,formatter: function(value, row, index){
				var html="";
		    	if(row.customerCode != null && row.customerName!= null){
		    		return Utils.XSSEncode(row.customerCode) + ' - ' + Utils.XSSEncode(row.customerName);
		    	}
				return html;
				
		    }},
			{field: 'customerAddress',title:'Địa chỉ',width: 450,sortable:false,resizasble:false, align: 'left' ,formatter: function(value, row, index){
					return Utils.XSSEncode(value);
				
		    } },
			// {field: 'createDate',title:'Ngày tạo',width: 100,sortable:false,resizable:false, align: 'center' , formatter:function(value,row,index) {
		 //    	return Utils.XSSEncode(value);
		 //    }},
		 	 //tamvnm: thay doi thanh ngay lap
			 {field: 'createDate',title:'Ngày biên bản',width: 100,sortable:false,resizable:false, align: 'center' , formatter:function(value,row,index) {
		    	return Utils.XSSEncode(value);
		    }},
			{field: 'numberContract', title: 'Số hợp đồng', width: 100, sortable:false,resizable:false,align: 'left' ,formatter: function(value, row, index){
					return Utils.XSSEncode(value);
				
		    }},	
		    {field: 'dateContract',title:'Ngày hợp đồng',width: 100,sortable:false,resizable:false, align: 'center' , formatter:function(value,row,index) {
		    	return Utils.XSSEncode(value);
		    }},
		    {field: 'equipLendCode',title:'Mã biên bản đề nghị mượn thiết bị', width: 110, sortable:false, resizable:false, align: 'left'  ,formatter: function(value, row, index){
					return Utils.XSSEncode(value);
				
		    }},
			{field: 'numberFile',title:'Tập tin đính kèm',width:75, resizable:false, sortable:false, align:'left',formatter: function(value, row, index){
				if (value > 0) {
					return '<a href="javascript:void(0)" onclick="General.downloadEquipAttechFile('+row.idRecord+', '+tradeTypeEquipment.GN+');" width = "50">Tập tin</a>';
				} else {
					// return '<a href="javascript:void(0)" onclick="" width = "50" disabled="disabled">Tập tin</a>';
				}

				
			}},
		    {field: 'statusRecord', title: 'Trạng thái biên bản', width: 80, sortable:false,resizable:false,align: 'left',formatter: function(value, row, index){
		    	var html='';
		    	if(row.statusRecord == 4){
		    		html+='Hủy';
		    	}else if(row.statusRecord == 2){
		    		html+='Duyệt';
		    	}else if(row.statusRecord == 0){
		    		html+='Dự thảo';
		    	}
		    	return html;
		    }},
		    {field: 'statusDelivery',title:'Trạng thái giao nhận',width:80,align:'left',formatter: function(value, row, index){
		    	var html='';
		    	if(row.statusDelivery == 0){
		    		html+='Chưa gửi';
		    	}else if(row.statusDelivery == 1){
		    		html+='Đã gửi';
		    	}else if(row.statusDelivery == 2){
		    		html+='Đã nhận';
		    	}
		    	if(row.statusDelivery != null){
		    		return '<a href="javascript:void(0);" onclick="EquipmentManagerDelivery.getHistoryDelivery('+row.idRecord+');">'+html+'</a>';
		    	}else{
		    		return html;		    		
		    	}
		    }},
		    {field: 'statusPrint',title:'Trạng thái in',width:80,align:'left',formatter: function(value, row, index){
		    	var html='';
		    	if(row.statusPrint == 0){
		    		html+='Chưa in';
		    	}else if(row.statusPrint == 1) {
		    		html+='Đã in';
		    	}
		    	return html;		    		
		    }},
		    {field: 'note', title: 'Ghi chú', width: 150, align: 'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);	    		
		    }},
		]],
		onBeforeLoad:function(param){											 
		},
		onLoadSuccess :function(data){  	 		    	
	    	$(window).resize();
	    	if (data == null || data.total == 0) {
	    		//$('#gridRecordDelivery').datagrid('insertRow', {
	    		//	  index: 0,
	    		//	  row: {}
	    		//});
	    		//$('div[class="datagrid-cell-check"] input[type="checkbox"]').remove();
				//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
			}else{
				$('.datagrid-header-rownumber').html('STT');
				//if(data.total>10){
				//	$('div[class=datagrid-body]').attr('style','min-height: 350px; overflow-y: scroll;');
				//}
			}	
			Utils.functionAccessFillControl('gridContainer');
			if($('#gridContainer .cmsiscontrol[id^="dg_"]').length == 0){
				$('#gridRecordDelivery').datagrid("hideColumn", "edit");
			} else {
				$('#gridRecordDelivery').datagrid("showColumn", "edit");
			}
			$('.datagrid-header-check input').removeAttr('checked');
        	var rows = $('#gridRecordDelivery').datagrid('getRows');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(EquipmentManagerDelivery._mapDelivery.get(rows[i].idRecord)!=null) {
	    			$('#gridRecordDelivery').datagrid('checkRow', i);
	    		}
	    	}	    	
		},
		onCheck:function(index,row){
			EquipmentManagerDelivery._mapDelivery.put(row.idRecord,row);   
	    },
	    onUncheck:function(index,row){
	    	EquipmentManagerDelivery._mapDelivery.remove(row.idRecord);
	    },
	    onCheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		EquipmentManagerDelivery._mapDelivery.put(row.idRecord,row);
	    	}
	    },
	    onUncheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		EquipmentManagerDelivery._mapDelivery.remove(row.idRecord);	
	    	}
	    }
	});
	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipment-manage-delivery/info">Thiết bị</a>
		</li>
		<li><span>Lập hợp đồng & biên bản giao nhận</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Mã biên bản giao nhận</label>
					<s:if test="idRecordDelivery == null">
						<input id="recordCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled"/>
					</s:if>
					<s:else>
						<input id="recordCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="recordCode"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Ngày biên bản<span class="ReqiureStyle"> *</span></label>
					<div  id="createDateDiv">
						<input type="text" id="createDate" class="InputTextStyle InputText9Style cmsiscontrol" value="<s:property value="createDate"/>"/>
					</div>

					<div class="Clear"></div>
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Số hợp đồng<span class="ReqiureStyle"> *</span></label>
					<input id="numberContract" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="100" value="<s:property value="numberContract"/>"/>
					
					<label class="LabelStyle Label9Style">Ngày hợp đồng<span class="ReqiureStyle"> *</span></label>
					<div  id="contractDateDiv">
						<input type="text" id="contractDate" class="InputTextStyle InputText9Style cmsiscontrol" value="<s:property value="contractDate"/>"/>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label3Style" id="divstatus" style="width: 140px; margin-left: 35px;">Trạng thái<span class="ReqiureStyle"> *</span></label>
					<s:if test="idRecordDelivery == null">
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass cmsiscontrol" id="status" onchange="EquipmentManagerDelivery.changeComboUpdate();">
								<option value="0" selected="selected">Dự thảo</option>
								<option value="2" >Duyệt</option>
							</select>
						</div>
					</s:if>
					<s:else>
						<s:if test="statusRecord == 0">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass cmsiscontrol" id="status" onchange="EquipmentManagerDelivery.changeComboUpdate();">
									<option value="0" selected="selected">Dự thảo</option>
									<option value="2" >Duyệt</option>
									<option value="4" >Hủy</option>
								</select>
							</div>
						</s:if>
						<s:else>
							<div class="BoxSelect BoxSelect2 BoxDisSelect" >
								<select class="MySelectBoxClass cmsiscontrol" id="status" disabled="disabled">
									<option value="2" selected="selected">Duyệt</option>
								</select>
							</div>
						</s:else>
					</s:else>
					<label class="LabelStyle Label9Style" id="divstatusDelivery">Trạng thái giao nhận<span class="ReqiureStyle"> *</span></label>
					<s:if test="idRecordDelivery == null">
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass cmsiscontrol" id="statusDelivery" >
								<option value="0" selected="selected">Chưa gửi</option>
								<!-- <option value="1" >Đã gửi</option>								
								<option value="2" >Đã nhận</option> -->
							</select>
						</div>
					</s:if>
					<s:else>
						<s:if test="statusRecord == 0">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass cmsiscontrol" id="statusDelivery">
									<option value="0" selected="selected">Chưa gửi</option>
								</select>
							</div>
						</s:if>
						<s:elseif test="statusDelivery == 0">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass cmsiscontrol" id="statusDelivery">	
									<option value="0" selected="selected">Chưa gửi</option>								
									<option value="1" >Đã gửi</option>
								</select>
							</div>
						</s:elseif>
						<s:elseif test="statusDelivery == 1">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass cmsiscontrol" id="statusDelivery">	
									<option value="1" selected="selected">Đã gửi</option>								
									<option value="2" >Đã nhận</option>
								</select>
							</div>
						</s:elseif>
						<s:else>
							<div class="BoxSelect BoxSelect2 BoxDisSelect">
								<select class="MySelectBoxClass cmsiscontrol" id="statusDelivery" disabled="disabled">
									<option value="2" >Đã nhận</option>
								</select>
							</div>
						</s:else>
					</s:else>
					<label class="LabelStyle Label9Style">Mã biên bản đề nghị mượn thiết bị</label>
					<s:if test="idRecordDelivery == null">
						<input id="equipLendCode" type="text" class="InputTextStyle InputText6Style" disabled="disabled"/>
					</s:if>
					<s:else>
						<input id="equipLendCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="equipLendCode"/>"/>
					</s:else>				
					<div class="Clear"></div>
				</div>			

				<h2 class="Title2Style">Bên cho mượn</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Đơn vị<span class="ReqiureStyle"> *</span></label>
					<s:if test="fromShopCode == null">
						<input id="shopFromCode" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="50" />
					</s:if>
					<s:else>
						<input id="shopFromCode" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="50" onchange="" value="<s:property value="fromShopCode"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Địa chỉ<span class="ReqiureStyle"> *</label>
					<s:if test="fromObjectAddress == null">
						<input id="address" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="200"/>
					</s:if>
					<s:else>
						<input id="address" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="200"
						value="<s:property value="fromObjectAddress"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Mã số thuế<span class="ReqiureStyle"> *</label>
					<s:if test="fromObjectTax == null">
						<input id="taxCode" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="20"/>
					</s:if>
					<s:else>
						<input id="taxCode" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="20"
						value="<s:property value="fromObjectTax"/>"/>
					</s:else>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Điện thoại<span class="ReqiureStyle"> *</label>
					<s:if test="fromObjectPhone == null">
						<input id="numberPhone" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="10"/>
					</s:if>
					<s:else>
						<input id="numberPhone" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="10"
						value="<s:property value="fromObjectPhone"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Người đại diện<span class="ReqiureStyle"> *</label>
					<s:if test="fromRepresentative == null">
						<input id="fromRepresentative" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="fromRepresentative" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"
						value="<s:property value="fromRepresentative"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Chức vụ<span class="ReqiureStyle"> *</label>
					<s:if test="fromObjectPosition == null">
						<input id="fromPosition" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="fromPosition" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"
						value="<s:property value="fromObjectPosition"/>"/>
					</s:else>
					<div class="Clear"></div>

					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Giấy ủy quyền<span class="ReqiureStyle"> *</label>
					<s:if test="fromPage == null">
						<input id="fromPage" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="fromPage" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"
						value="<s:property value="fromPage"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Nơi cấp<span class="ReqiureStyle"> *</label>
					<s:if test="fromPagePlace == null">
						<input id="fromPagePlace" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="fromPagePlace" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"
						value="<s:property value="fromPagePlace"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Ngày cấp<span class="ReqiureStyle"> *</span></label>
					<div id="pageDateDiv">
						<input type="text" id="pageDate" class="InputTextStyle InputText9Style cmsiscontrol" value="<s:property value="fromPageDate"/>"/>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Số FAX</label>
					<s:if test="fromFax == null">
						<input id="fromFax" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="20"/>
					</s:if>
					<s:else>
						<input id="fromFax" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="20"
						value="<s:property value="fromFax"/>"/>
					</s:else>
					<div class="Clear"></div>
				</div>

				<h2 class="Title2Style">Bên mượn</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Đơn vị (F9)<span class="ReqiureStyle"> *</span></label>
					<s:if test="toShopCode == null">
						<input id="shopToCode" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="50"/>
					</s:if>
					<s:else>
						<input id="shopToCode" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="50" onchange="" value="<s:property value="toShopCode"/>"/>
					</s:else>
					
					<label class="LabelStyle Label9Style">Mã khách hàng (F9)<span class="ReqiureStyle"> *</span></label>
					<s:if test="customerCode == null">
						<input id="customerCode" type="text" onchange = "EquipmentManagerDelivery.changeEventCustomerCode(this);" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="50"/>
					</s:if>
					<s:else>
						<input id="customerCode" type="text" onchange = "EquipmentManagerDelivery.changeEventCustomerCode(this);"  class="InputTextStyle InputText1Style cmsiscontrol" maxlength="50"
						value="<s:property value="customerCode"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Địa chỉ thường trú</label>
					<s:if test="toPermanentAddress == null">
						<input id="toPermanentAddress" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="toPermanentAddress" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250" value="<s:property value="toPermanentAddress"/>"/>
					</s:else>
					<div class="Clear"></div>
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Số GP ĐKKD</label>
					<s:if test="toBusinessLicense == null">
						<input id="toBusinessLicense" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="toBusinessLicense" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"
						value="<s:property value="toBusinessLicense"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Nơi cấp GPKD</label>
					<s:if test="toBusinessPlace == null">
						<input id="toBusinessPlace" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="toBusinessPlace" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"	value="<s:property value="toBusinessPlace"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Ngày cấp GPKD</label>
					<div id="toBusinessDateDiv">
						<input type="text" id="toBusinessDate" class="InputTextStyle InputText9Style cmsiscontrol" value="<s:property value="toBusinessDate"/>" />
					</div>

					<div class="Clear"></div>
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Người đại diện<span class="ReqiureStyle"> *</span></label>
					<s:if test="toRepresentative == null">
						<input id="toRepresentative" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="toRepresentative" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"
						value="<s:property value="toRepresentative"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Chức vụ<span class="ReqiureStyle"> *</span></label>
					<s:if test="toObjectPosition == null">
						<input id="toPosition" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="toPosition" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"
						value="<s:property value="toObjectPosition"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Số CMND</label>
					<s:if test="toIdNO == null">
						<input id="toIdNO" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="20"/>
					</s:if>
					<s:else>
						<input id="toIdNO" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="20"
						value="<s:property value="toIdNO"/>"/>
					</s:else>

					<div class="Clear"></div>

					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Nơi cấp CMND</label>
					<s:if test="toIdNOPlace == null">
						<input id="toIdNOPlace" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="toIdNOPlace" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"
						value="<s:property value="toIdNOPlace"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Ngày cấp CMND</span></label>
					<div id="idNODateDiv">
						<input type="text" id="idNODate" class="InputTextStyle InputText9Style cmsiscontrol" value="<s:property value="toIdNODate"/>" />
					</div>
					<!-- <label class="LabelStyle Label9Style">Mã giám sát<span class="ReqiureStyle"> *</span></label>
					<s:if test="idRecordDelivery == null">
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass cmsiscontrol" id="staffMonitorCode">
														
							</select>
						</div>
					</s:if>
					<s:else>
						<div class="BoxSelect BoxSelect2 BoxDisSelect">
							<select class="MySelectBoxClass cmsiscontrol" id="staffMonitorCode"  disabled="disabled" >
								<option  value=""/><s:property value="staffCode"/> - <s:property value="staffName"/></option> 							
							</select>
						</div>
					</s:else> -->
					
					<label id="staffCodeDiv" class="LabelStyle Label9Style">Mã giám sát<span class="ReqiureStyle"> *</span></label>
                    <div class="BoxSelect BoxSelect2 cmsiscontrol" id="cbxStaffCode">
                        <select class="easyui-combobox" id="staffMonitorCode">
                        	<s:if test="staffCode != null">
                        		<option value="<s:property value="staffCode"/>"></option>
                        	</s:if>
                            <!-- <option value=""></option>
							<s:iterator value="lstGsnpp" var="obj" >
								<option value="<s:property value="#obj.staffCode" />"><s:property value="#obj.staffName" /></option>
							</s:iterator> -->
                        </select>
                    </div>
					<div class="Clear"></div>
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;"><b>Địa chỉ đặt tủ</b></label>

					<div class="Clear"></div>
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Số nhà</label>
					<s:if test="addressName == null">
						<input id="addressName" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="addressName" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"
						value="<s:property value="addressName"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Tên đường phố/Thôn ấp<span class="ReqiureStyle"> *</span></label>
					<s:if test="street == null">
						<input id="street" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="street" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"	value="<s:property value="street"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Phường/Xã</label>
					<s:if test="wardName == null">
						<input id="wardName" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="wardName" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"	value="<s:property value="wardName"/>"/>
					</s:else>

					<div class="Clear"></div>
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Quận/Huyện<span class="ReqiureStyle"> *</span></label>
					<s:if test="districtName == null">
						<input id="districtName" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="districtName" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"
						value="<s:property value="districtName"/>"/>
					</s:else>
					<label class="LabelStyle Label9Style">Tỉnh/TP<span class="ReqiureStyle"> *</span></label>
					<s:if test="provinceName == null">
						<input id="provinceName" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"/>
					</s:if>
					<s:else>
						<input id="provinceName" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="250"	value="<s:property value="provinceName"/>"/>
					</s:else>
					<div class="Clear"></div>
				</div>

				<h2 class="Title2Style">Cam kết doanh số</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Tủ mát</label>
					<s:if test="freezer == null">
						<input id="freezer" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="17"/>
					</s:if>
					<s:else>
						<input id="freezer" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="17"
						value="<s:property value="freezer"/>"/>
					</s:else>

					<label class="LabelStyle Label9Style">Tủ đông</label>
					<s:if test="refrigerator == null">
						<input  id="refrigerator" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="17"/>
					</s:if>
					<s:else>
						<input id="refrigerator" type="text" class="InputTextStyle InputText1Style cmsiscontrol" maxlength="17"
						value="<s:property value="refrigerator"/>"/>
					</s:else>
					<div class="Clear"></div>			
				</div>
				<h2 class="Title2Style">Khác</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label3Style" style="width: 140px; margin-left: 35px;">Ghi chú</label>
<!-- 					<s:if test="freezer == null">
						<input id="note" type="text" class="easyui-textbox" maxlength="500" style="width: 570px; height: 100px;" data-options="multiline:true"/>
					</s:if>
					<s:else>
						<input id="note" type="text" class="easyui-textbox" maxlength="500"
						value="<s:property value="freezer"/>" style="width: 570px; height: 100px;" data-options="multiline:true"/>
					</s:else>		 -->
					<s:if test="note == null">
						<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 570px; height: 100px; font-size: 13px;" maxlength="500" ></textarea>	
					</s:if>
					<s:else>
						<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 570px; height: 100px; font-size: 13px;" maxlength="500"><s:property value="note"/></textarea>
					</s:else>

				</div>
				<div class="Clear"></div>	

				<h2 class="Title2Style">Tập tin đính kèm</h2>
				<div class="SearchInSection SProduct1Form">
<!-- 					<a  href="javascript:void(0);" style="float: left;" title="Lựa chọn tập tin" ><img id="imgAddFile" class="addFileBtn cmsiscontrol" src="/resources/images/icon_attach_file.png" ></a> -->
					<a  href="javascript:void(0);" style="float: left;" title="Lựa chọn tập tin" ><img id="imgAddFile" class="addFileBtn cmsiscontrol dz-clickable" src="/resources/images/icon_attach_file.png" style="display: inline; background-image: none;"></a>
					<div class="fileupload-process" style="width: 90%;">
						<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-striped active" id="total-progress" style="opacity: 0; width: 550px;">
							<div data-dz-uploadprogress="" style="width: 200px;" class="progress-bar progress-bar-success"></div>
						</div>
					</div>
					<div style="float:left; min-width: 500px; width: 90%;">
						<div style="height: auto; margin-left:20px;">
							<s:iterator id="fileVo" value="lstFileVo">
								<div class="divImageUploadFile" style="float:none; height: auto;" id = "divEquipAttachFile<s:property value="#fileVo.fileId"/>">
									<!-- <div class="imageUpdateShowIcon">
										<img class="addFileBtn" id="imgFolder" src="/resources/images/img-folder-001.png" width="50" height="50" alt="<s:property value="#fileVo.fileId"/>"/>
									</div> -->
									<div class="imageUpdateShowContent" style="float:none; height: auto;  width: 500px;" >
										<label><s:property value="#fileVo.fileName"/></label>
										<a style="margin-right: 400px;" class="cmsiscontrol" id="imgDelete" href="javascript:void(0)" onclick="EquipmentManagerDelivery.removeEquipAttachFile(<s:property value="#fileVo.fileId"/>);"><img title="Xóa tập tin" src="/resources/images/icon_delete.png" width="12" height="12" style="padding-left: 5px;"></a>

									</div>
								</div>
							</s:iterator>
						</div>
						<div class="Clear"></div>
						<div class="table table-striped" class="files" id="previews" style="min-height: 100px; margin-left:20px;">
							<div id="template" class="file-row" style="margin-bottom: 10px; width: 260px;float: left">
								<div style="margin-right: 10px; width: 50px; float: left">
									<!-- <span class="preview"><img data-dz-thumbnail /></span> -->
								</div>
								<div style="display: inline-block; width: 200px; clear: both">
									<div style="padding-right: 10px">
										<div>
											<p class="name" style="overflow: hidden" data-dz-name></p>
											<strong class="error text-danger" data-dz-errormessage></strong>
										</div>
										<div style="">
											<p class="size" data-dz-size></p>
											</div>
										<div>
											<a data-dz-remove href="javascript:void(0)"><span><img id="imgCancel" border="0" src="/resources/scripts/plugins/uploadify2.1/cancel.png"></span></a>
										</div>
									</div>								
								</div>							
							</div> 
						</div>
					</div>
					<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>	
					<p id="errMsgInfo" class="ErrorMsgStyle" style="display: none;"></p>	
					<p id="successMsgInfo" class="SuccessMsgStyle" style="display: none"></p>
				</div>
				<!-- danh sach bien ban giao nhan -->	
				<h2 class="Title2Style">
					<span>Thiết bị giao nhận</span>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer1">
						<table id="gridEquipmentDelivery"></table>
					</div>
				</div>
				<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnUpdate" class="BtnGeneralStyle" >Cập nhật</button>	
							<s:if test="idRecordDelivery == null">	
								<button id="btnClose" class="BtnGeneralStyle" onclick="location.href = '/equipment-manage-delivery/info';">Bỏ qua</button>
							</s:if>
							<s:else>	
								<button id="btnClose" class="BtnGeneralStyle" onclick="location.href = '/equipment-manage-delivery/info';">Bỏ qua</button>
							</s:else>
						</div>
						<div class="Clear"></div>
					</div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup thiet bi giao nhan -->
<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchEquipment" class="easyui-dialog"
		title="Chọn thiết bị"
		data-options="closed:true,modal:true" >
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label6Style">Mã thiết bị</label>
				<input id="equipmentCode" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label7Style">Số serial</label>
				<input id="seriNumber" type="text" class="InputTextStyle InputText4Style" maxlength="100"/>
				<label class="LabelStyle Label7Style">Loại</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="typeEquipment" onchange="EquipmentManagerDelivery.onChangeTypeEquip(this);">
						<option value="">Tất cả</option>
					</select>
				</div>
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Nhóm thiết bị</label>
				<div class="BoxSelect BoxSelect3">
					<!-- <select class="MySelectBoxClass" id="groupEquipment" onchange="">
						<option value="">Tất cả</option>
					</select> -->
					<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
					<select id="groupEquipment" style="width:193px !important;"></select>
				</div>
				<label class="LabelStyle Label7Style">Nhà cung cấp</label>
				<div class="BoxSelect BoxSelect3">
					<!-- <select class="MySelectBoxClass" id="providerId" onchange="">
						<option value="">Tất cả</option>
					</select> -->
					<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
					<select id="providerId" style="width:193px !important;"></select>
				</div>					
				<label class="LabelStyle Label7Style">Năm sản xuất</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="yearManufacturing">
					</select>
				</div>
				<!-- <div id="contractDateDiv">
					<input type="text" id="yearManufacturing" class="InputTextStyle InputText5Style"/>
				</div> -->
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Kho (F9)</label>
				<s:if test="idRecordDelivery == null">
					<input type="text" id="stockCode" class="InputTextStyle InputText4Style" />
				</s:if>
				<s:else>
					<input type="text" id="stockCode" class="InputTextStyle InputText4Style" />
				</s:else> 				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="EquipmentManagerDelivery.searchEquipment();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="equipmentGridDialogContainer">
					<table id="equipmentGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchEquipment').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgInfoEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!-- popup chon kho -->
<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchStock" class="easyui-dialog"
		title="Chọn kho"
		data-options="closed:true,modal:true">
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label1Style">Mã đơn vị</label>
				<input id="shopCodePopupStock" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label1Style">Tên đơn vị</label>
				<input id="shopNamePopupStock" type="text" class="InputTextStyle InputText5Style" maxlength="100"/>				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSearchStockDlg" class="BtnGeneralStyle" onclick="EquipmentManagerDelivery.searchStock();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="stockGridDialogContainer">
					<table id="stockGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnStockClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchStock').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgInfoEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!--Popup timf kiem kho nguon - kho dich  -->
<div id ="productDialog" style="width:600px;visibility: hidden;" >
	<div id="easyuiPopupSearchStock" class="easyui-dialog"	title="Tìm kiếm kho"
		data-options="closed:true,modal:true">
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label1Style">Mã đơn vị</label>
				<input id="shopCodePopupStock" type="text" class="InputTextStyle InputText4Style"  maxlength="50" />
				<label class="LabelStyle Label1Style">Tên đơn vị</label>
				<input id="shopNamePopupStock" type="text" class="InputTextStyle InputText4Style"  maxlength="100"/>				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="EquipmentStockChange.searchStockChange();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="stockGridDialogContainer">
					<table id="stockGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchStock').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgInfoEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!-- popup chon thiet bi -->
<div style="display: none">
	<div id="easyuiPopupSelectEquipment" class="easyui-dialog" title="Chọn thiết bị" data-options="closed:true,modal:true" >
		<div class="PopupContentMid">
	   		<div class="SearchInSection SProduct1Form" style="border:none;">
				<div class="GridSection" id="gridSelectEquipmentContainer">
					<table id="gridSelectEquipment"></table>
				</div>
			</div>        
		</div>
	</div>
</div>
<div><input type="hidden" id="idRecordHidden" value="<s:property value="idRecordDelivery"/>"></div>
<div><input type="hidden" id="statusRecordHidden" value="<s:property value="statusRecord"/>"></div>
<div><input type="hidden" id="statusDeliveryHidden" value="<s:property value="statusDelivery"/>"></div>
<div><input type="hidden" id="customerCodeHidden" value="<s:property value="customerCode"/>"></div>
<div><input type="hidden" id="customerIdHidden" value="<s:property value="customerId"/>"></div>
<div><input type="hidden" id="fromShopHidden" value="<s:property value="toShopCode"/>"></div>
<div><input type="hidden" id="lstEquipGroupId" value="<s:property value="lstEquipGroupId"/>"></div>
<div><input type="hidden" id="formCreateDate" value="<s:property value="createDate"/>"></div>
<div><input type="hidden" id="lendCode" value="<s:property value="equipLendCode"/>"></div>
<div><input type="hidden" id="noteHidden" value="<s:property value="note"/>"></div>
<div><input type="hidden" id="shopToId" ></div>
<script type="text/javascript">
$(document).ready(function() {	
	var equipNumberDeprec = '<s:property value="equipNumberDeprec"/>';
	if (equipNumberDeprec != undefined && equipNumberDeprec != null && !isNaN(equipNumberDeprec)) {
		EquipmentManagerDelivery.equipNumberDeprec = Number(equipNumberDeprec);
	}
	setDateTimePicker('contractDate');	
	setDateTimePicker('pageDate');
	setDateTimePicker('idNODate');
	setDateTimePicker('toBusinessDate');
	//tamvnm: them ngay lap 03/07/2015
	setDateTimePicker('createDate');
	var createFormDate = $('#formCreateDate').val();
	if (createFormDate != undefined && createFormDate != null && createFormDate != '') {
		$('#createDate').val(createFormDate);
	} else {
		ReportUtils.setCurrentDateForCalendar("createDate");
	}

	if ($('#freezer').val().indexOf(',')==-1) {
		$('#freezer').val(formatCurrency($('#freezer').val()));
		Utils.formatCurrencyFor('freezer');
		Utils.bindFormatOnTextfield('freezer',Utils._TF_NUMBER_COMMA);
	}

	if ($('#refrigerator').val().indexOf(',')==-1) {
		$('#refrigerator').val(formatCurrency($('#refrigerator').val()));
		Utils.formatCurrencyFor('refrigerator');
		Utils.bindFormatOnTextfield('refrigerator',Utils._TF_NUMBER_COMMA);
	}

	Utils.bindComboboxStaffEasyUI('staffMonitorCode');
	$("#customerCode").val($("#customerCodeHidden").val());

	EquipmentManagerDelivery._countFile = Number('<s:property value="lstFileVo.size()"/>');
	$('#shopFromCode, #recordCode, #staffMonitorCode, #shopToCode').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnUpdate').click(); 
		}
	});
	/* $('#customerCode, #numberContract, #contractDate').bind('keyup',function(event){ */
	$('#customerCode, #numberContract').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnUpdate').click(); 
		}
	});
	$('#status, #statusDelivery').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnUpdate').click(); 
		}
	});	
	// $('#shopToCode').bind('keyup',function(event){
	// 	if(event.keyCode == keyCodes.ENTER){
	// 		EquipmentManagerDelivery.onChangeShopCode();
	// 	}
	// });	
	EquipmentManagerDelivery._shopToCode = $('#shopToCode').val().trim();
	$('#shopToCode').bind('blur', function () {
		var curShopCode = $('#shopToCode').val().trim();
		if (curShopCode != EquipmentManagerDelivery._shopToCode) {
			var staffCode = $('#staffMonitorCode').combobox('getValue').trim();
			EquipmentManagerDelivery.onChangeShopCode();
			$('#customerCode').val('');
			$('#addressName').val("");
			$('#street').val("");
			$('#wardName').val("");
			$('#districtName').val("");
			$('#provinceName').val("");
		}
    });
	// $('#shopFromCode').bind('keyup', function(e){
	// 	if(e.keyCode==keyCode_F9){
	// 		VCommonJS.showDialogSearch2({
	// 			//params : {},
	// 			inputs : [
	// 		        {id:'code', maxlength:50, label:'Mã Đơn vị'},
	// 		        {id:'name', maxlength:250, label:'Tên Đơn vị'},
	// 		    ],
	// 		    url : '/commons/search-shop-show-list-NPP',
	// 		    columns : [[
	// 		        {field:'shopCode', title:'Mã Đơn vị', align:'left', width: 110, sortable:false, resizable:false},
	// 		        {field:'shopName', title:'Tên Đơn vị', align:'left', width: 200, sortable:false, resizable:false},
	// 		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
	// 		            var html =  '<a href="javascript:void(0)" onclick="EquipmentManagerDelivery.selectFromShop({0},\'{1}\',\'{2}\')" >Chọn</a>';
	// 		            return format(html,row.id,row.shopCode,row.shopName);
	// 		        }}
	// 		    ]]
	// 		});				
	// 		return false;
	// 	}
	// });	
	$('#shopToCode').bind('keyup', function(e){
		if(e.keyCode==keyCode_F9){
			VCommonJS.showDialogSearch2({
				//params : {},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Đơn vị'},
			        {id:'name', maxlength:250, label:'Tên Đơn vị'},
			    ],
			    url : '/commons/search-shop-show-list-NPP-GT-MT',
			    columns : [[
			        {field:'shopCode', title:'Mã Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.shopCode);
			        }},
			        {field:'shopName', title:'Tên Đơn vị', align:'left', width: 200, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.shopName);
			        }},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			            var html =  '<a href="javascript:void(0)" onclick="EquipmentManagerDelivery.selectToShop({0},\'{1}\',\'{2}\')" >Chọn</a>';
			            return format(html,row.shopId,Utils.XSSEncode(row.shopCode),Utils.XSSEncode(row.shopName));
			        }}
			    ]]
			});				
			return false;
		}
	});	
	$('#customerCode').bind('keyup', function(event) {					
		if (event.keyCode == keyCode_F9) {
			$("#errMsgInfo").html('').hide();
			if($('#shopToCode').val().trim()== null || $('#shopToCode').val().trim()==''){
				$("#errMsgInfo").html('Xin chọn đơn vị trước khi nhấn F9!').show();
				return;
			}
			VCommonJS.showDialogSearch2({
				params : {
					shopId: $('#shopToId').val().trim()
				},
			    inputs : [
			        {id:'code', maxlength:250, label:'Khách hàng', width:410}
// 			        {id:'name', maxlength:250, label:'Tên KH'},
// 			        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
			    ],			   
			    url : '/commons/search-Customer-Show-List',			   
			    columns : [[
			        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.shortCode);
			        }},
			        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.customerName);
			        }},
			        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.address);
			        }},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return "<a href='javascript:void(0)' onclick=\"return EquipmentManagerDelivery.selectCustomer('"+ Utils.XSSEncode(row.shortCode) + "'," + row.id + ",'" + Utils.XSSEncode(row.street) + "','" + Utils.XSSEncode(row.housenumber) + "',"+ row.areaId +");\">Chọn</a>";  
			        }}
			    ]]
			});
		} 
	});	
	EquipmentManagerDelivery._lstEquipGroup = new Object();
	EquipmentManagerDelivery._lstEquipProvider = new Object();
	EquipmentManagerDelivery._lstEquipStock = new Object();


	$('#gridEquipmentDelivery').datagrid({
		//url : '/equipment-manage-delivery/search-equipment-in-record',
		autoRowHeight : true,
		rownumbers : true, 
		//checkOnSelect :true,
		//selectOnCheck: true,
		singleSelect: true,
		// pagination:true,
		 //fitColumns:true,
		// pageSize :20,
		// pageList  : [20],
		scrollbarSize:15,
		width: $('#gridContainer1').width(),
		autoWidth: true,	
		queryParams:{
			idRecordDelivery: $("#idRecordHidden").val()
		},
		frozenColumns:[[
			{field:'delete', title:'<a href="javascript:void(0);" class="cmsiscontrol" id="dg_add_change" onclick="EquipmentManagerDelivery.addEquipmentInRecord();"><img src="/resources/images/icon_add.png" title="Thêm thiết bị"/></a>', width:50, align:'center', formatter: function(value, row, index) {
			    var	html='';
			    var deleteRow = 'dg_delete_change_' + index;
			    if($("#statusRecordHidden").val() != undefined && $("#statusRecordHidden").val() == '0'){
			    	html = '<a href="javascript:void(0);" class="cmsiscontrol" id="'+deleteRow+'" onclick="EquipmentManagerDelivery.deleteEquipmentForDelivery('+index+')"><img class="cmsiscontrol" id="'+deleteRow+'"src="/resources/images/icon-delete.png" title="Xóa"></a>';
			    }else if(EquipmentManagerDelivery._numberEquipInRecord !=null && index >= EquipmentManagerDelivery._numberEquipInRecord){
			    	html = '<a href="javascript:void(0);" class="cmsiscontrol" id="'+deleteRow+'" onclick="EquipmentManagerDelivery.deleteEquipmentForDelivery('+index+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
			    }else if($("#idRecordHidden").val() == ""){
					html = '<a href="javascript:void(0);" class="cmsiscontrol" id="'+deleteRow+'" onclick="EquipmentManagerDelivery.deleteEquipmentForDelivery('+index+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
			    }
			    return html;
			}},  
			{field: 'contentDelivery',title:'Nội dung',width: 180,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {	 
				html = '';
				var apParamEquipCode = '';
				<s:iterator value="lstContent" var="obj" >
					apParamEquipCode = '<s:property value="#obj.apParamEquipCode"/>';
					if (apParamEquipCode == value) {
						html = '<s:property value="#obj.value"/>';
					}
				</s:iterator>
				if (html != '') {
					return html;
				} else {
					html = '<select id="contentDelivery" style="width:100%;">';
					// if(row.seriNumber == null && row.equipmentCode!=null){
					// 	html += '<option selected="selected" value="<s:property value="ContentNew.apParamEquipCode" />"><s:property value="ContentNew.value" /></option>';
					// }else{
					// 	var contentDeliveryTypeVale = '';
					// 	<s:iterator value="lstContent" var="obj" >
					// 		contentDeliveryTypeVale = '<s:property value="#obj.value" />';
					// 		 if (contentDeliveryTypeVale == 'Cấp mới') {
					// 				html += '<option value="<s:property value="ContentNew.apParamEquipCode" />" selected="selected"><s:property value="ContentNew.value" /></option>';									
					// 		} else {
					// 			html += '<option value="<s:property value="#obj.apParamEquipCode" />"><s:property value="#obj.value" /></option>';
					// 		}
					// 	</s:iterator>
					// 	//html = '<select id="contentDelivery" style="width:150;"><option value="0" selected="selected">Cấp mới</option> <option value="1" >Điều chuyển từ KH Khác</option> <option value="2" >Khác</option></select>';					
					// }
					var contentDeliveryTypeVale = '';
					<s:iterator value="lstContent" var="obj" >
						contentDeliveryTypeVale = '<s:property value="#obj.value" />';
						 if (contentDeliveryTypeVale == 'Điều chuyển từ KH Khác') {
								html += '<option value="<s:property value="ContentNew.apParamEquipCode" />" selected="selected"><s:property value="ContentNew.value" /></option>';									
						} else {
							html += '<option value="<s:property value="#obj.apParamEquipCode" />"><s:property value="#obj.value" /></option>';
						}
					</s:iterator>			
					html += '</select>';
					return html;	
				}
			}},
			{field: 'typeEquipment',title:'Loại thiết bị', width: 80, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(row.typeEquipment);
			}},
			{field: 'groupEquipmentCode',title:'Nhóm thiết bị', width: 200, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
		    	var html = '';
		    	if(row.groupEquipmentName != null) {
					html = Utils.XSSEncode(row.groupEquipmentName);
		    	} else if (row.editGroup == 1 || row.editGroup == undefined) {
		    		html = '<select class="easyui-combobox" id="groupEquipCBX' + index +'" style="width: 190%" data-options="onSelect: EquipmentManagerDelivery.getValueEquipGroup"></select>';
		    	}
				return html;
			}}
		]],
		columns:[[		
			
			{field: 'capacity',title:'Dung tích (lít)',width: 80,sortable:false,resizable:false, align: 'left' , formatter: function(value, row, index) {
				return Utils.XSSEncode(row.capacity);
			}},
			{field: 'equipmentProviderCode',title:'Nhà cung cấp',width: 180,sortable:false,resizable:false, align: 'left' , formatter: function(value, row, index) {
		    	var html = '';
		    	if (row.equipmentProvider != null) {
					html = Utils.XSSEncode(row.equipmentProvider);
		    	} else if (row.editProvider == 1 || row.editProvider == undefined) {
		    		html = '<select class="easyui-combobox" id="providerEquipCBX' + index +'" style="width: 170%" data-options="onSelect: function(rec) {}"></select>';     
		    	}
				return html;
			}},
			
			{field: 'equipmentCode',title:'Mã thiết bị (F9)',width: 220,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var html = '<input type="text" id="equipmentCodeInGrid' + index + '" size="11" style="width: 95%">';
				if(row.equipmentCode!=null && row.equipmentCode != undefined && row.equipmentCode!=''){
					if($('#equipmentCodeInGrid' + index).val() != undefined ){
						html = '<input type="text" id="equipmentCodeInGrid' + index + '" size="11" value="'+row.equipmentCode+'" style="width: 100%">';					
					}else{
						html = Utils.XSSEncode(row.equipmentCode); 						
					}						
				}
				return html;
			}},
			{field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var html = '<input type="text" class="cmsiscontrol" id="seriNumberInGrid' + index + '" size="11" maxlength="100" style="width: 95%">';				
				if(row.seriNumber!=null && row.seriNumber != undefined && row.seriNumber!=''){						
					html = Utils.XSSEncode(row.seriNumber);
				}	 
				return html;
			}},
			{field: 'depreciation',title:'Số tháng khấu hao',width: 100,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
				var html = '<input type="text" id="depreciationInGrid' + index + '" size="11" maxlength="5" style="width: 95%" value="'+EquipmentManagerDelivery.equipNumberDeprec+'">';
		    	if (row.depreciation != null) {
					html = Utils.XSSEncode(row.depreciation);
		    	} else {
		    		$('#depreciationInGrid' + index).val(EquipmentManagerDelivery.equipNumberDeprec);
		    	}
				return html;
			}},
			{field: 'price',title:'Giá trị TBBH',width: 100,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
				var curPrice = formatCurrency(value);
				var html = '<input type="text" id="price' + index + '" size="11" maxlength="17" style="width: 95%">';
		    	if (row.price != null) {
					html = curPrice;
		    	}
				return html;
			}},
			{field: 'stockCode', title: 'Kho', width: 180, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
				// if (row.stockCode != undefined && row.stockCode != null && row.stockName != undefined && row.stockName != null) {
				// 	return VTUtilJS.XSSEncode(row.stockCode+' - '+row.stockName);
				// } else {
				// 	return VTUtilJS.XSSEncode(value);  
				// }
				var html = '';
				if (row.stockCode != null) {
					html = Utils.XSSEncode(row.stockCode);
				} else if (row.editStock == 1 || row.editStock == undefined) {
					html = '<select class="easyui-combobox" id="stockEquipCBX' + index + '" style="width: 170%"></select>';
				}
				return html;
			}},
		    {field: 'healthStatus', title: 'Tình trạng thiết bị', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(value, row, index) {
				html = '';
				var apParamHealthStatus = '';
				<s:iterator value="lstHealthy" var="obj" >
					apParamHealthStatus = '<s:property value="#obj.apParamCode"/>';
					if (apParamHealthStatus == row.healthStatus) {
						html = '<s:property value="#obj.value"/>';
					}
				</s:iterator>
				if (html != '') {
					return html;
				} else if (row.healthStatus != null) {
					return Utils.XSSEncode(row.healthStatus);
				} else {
					html = '<select id="healthStatus' + index +'" style="width:100%;">';
					var contentDeliveryTypeVale = '';
					<s:iterator value="lstHealthy" var="obj" >
						//contentDeliveryTypeVale = '<s:property value="#obj.value" />';
						//  if (contentDeliveryTypeVale == 'Điều chuyển từ KH Khác') {
						// 		html += '<option value="<s:property value="ContentNew.apParamEquipCode" />" selected="selected"><s:property value="ContentNew.value" /></option>';									
						// } else {
						// 	html += '<option value="<s:property value="#obj.apParamEquipCode" />"><s:property value="#obj.value" /></option>';
						// }
						html += '<option value="<s:property value="#obj.apParamCode" />"><s:property value="#obj.value" /></option>';
					</s:iterator>			
					html += '</select>';
					return html;		
				}
				// var html = '';
		  //   	if(row.healthStatus != null){
				// 	html = row.healthStatus; 
		  //   	} else {
		  //   		html = '<select class="easyui-combobox" id="healthStatusEquipCBX' + index +'" style="width: 140%"></select>';     
		  //   	}
				// return html;
			}},
			{field: 'healthCode', title: 'Tình trạng thiết bị', width: 100, sortable:false,resizable:false, hidden: true,align: 'left', formatter: function(value, row, index) {
				html = '';
				var apParamHealthStatus = '';
				<s:iterator value="lstHealthy" var="obj" >
					apParamHealthStatus = '<s:property value="#obj.value"/>';
					if (apParamHealthStatus == row.healthStatus) {
						html = '<s:property value="#obj.apParamCode"/>';
					}
				</s:iterator>
				if (html != '') {
					return html;
				} else {
					html = '<select id="healthStatus' + index +'" style="width:100%;">';
					var contentDeliveryTypeVale = '';
					<s:iterator value="lstHealthy" var="obj" >
						//contentDeliveryTypeVale = '<s:property value="#obj.value" />';
						//  if (contentDeliveryTypeVale == 'Điều chuyển từ KH Khác') {
						// 		html += '<option value="<s:property value="ContentNew.apParamEquipCode" />" selected="selected"><s:property value="ContentNew.value" /></option>';									
						// } else {
						// 	html += '<option value="<s:property value="#obj.apParamEquipCode" />"><s:property value="#obj.value" /></option>';
						// }
						html += '<option value="<s:property value="#obj.apParamCode" />"><s:property value="#obj.value" /></option>';
					</s:iterator>			
					html += '</select>';
					return html;		
				}
			}},
			 {field: 'yearManufacture',title:'Năm sản xuất',width: 100,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
				 var html = '<input type="text" id="yearManufacture' + index + '" size="11" maxlength="5" style="width: 95%">';
				//var html = '';
		    	if (row.yearManufacture != null) {
					html = Utils.XSSEncode(row.yearManufacture);
		    	}
				return html;
			}},
		    {field: 'firstYearInUse',title:'Năm bắt đầu sử dụng',width: 100,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
				// var html = '<input type="text" id="firstYearInUse' + index + '" size="11" maxlength="5" style="width: 95%">';
				var html = '';
		    	if (row.firstYearInUse != null) {
					html = Utils.XSSEncode(row.firstYearInUse);
		    	}
				return html;
			}},
			
		]],
		onAfterEdit : function(index, row, changes){
			
		},
		onLoadSuccess :function(data){   			 	    	
	    	$(window).resize();	
	    	if (data == null || data.total == 0) {
				//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
			} else {
				$('.datagrid-header-rownumber').html('STT');	
				if($("#idRecordHidden").val() != undefined && $("#idRecordHidden").val() != ""){
					EquipmentManagerDelivery._numberEquipInRecord = data.total;
				}
			}
			Utils.functionAccessFillControl('gridContainer1');
			if($('#gridContainer1 .cmsiscontrol[id^="dg_"]').length == 0 || $("#statusRecordHidden").val() == 2){
				$('#gridEquipmentDelivery').datagrid("hideColumn", "delete");
			} else {
				$('#gridEquipmentDelivery').datagrid("showColumn", "delete");
			}
			var rows = $('#gridEquipmentDelivery').datagrid('getRows');
			var isEnableUpdate = false;
			for (var i = 0; i < rows.length; i++) {
				if (rows[i].seriNumber == null || rows[i].seriNumber == '') {
					isEnableUpdate = true;
				}
			}
			if ($('#cbxStaffCode').is('[disabled=disabled]')) {
				disableCombo('staffMonitorCode');

			} else {
				isEnableUpdate = true;
			}
			if (!isEnableUpdate) {
				disabled('btnUpdate');
			}


			if ($('#contractDate').is('[disabled=disabled]')) {
				$('.CalendarLink').each(function() {
   					 $(this).hide();
				});
				$('#staffCodeDiv').css("margin-left","21px");
			}
			// if ($("#statusRecordHidden").val() != 2) {
			// 	EquipmentManagerDelivery.initUploadFile(urlUpdate);
			// }
			
			EquipmentManagerDelivery.disableField();
			EquipmentManagerDelivery.getLstEquipInfo();

			//set lai combobox staff
			var staffCode = $('#staffMonitorCode').combobox('getValue').trim();
			EquipmentManagerDelivery.onChangeShopCode(staffCode);
			if ($('#idRecordHidden').val() != "") {
				EquipmentManagerDelivery.getDataEquipStock(EquipmentManagerDelivery._isSetStockCombo);
			}
			
		}
		// ,
		// onDblClickCell: function(index,field,value){
		// 	var row = $('#gridEquipmentDelivery').datagrid('getRows');
		// 	if (field == 'groupEquipmentCode') {
		// 		row[index].groupEquipmentName = null;
		// 		var group = row[index].groupEquipmentCode;
		// 		$('#groupEquipCBX' +  index).combobox('destroy');
		// 		$('#gridEquipmentDelivery').datagrid('updateRow',{
		// 			index: index,	
		// 			row: row
		// 		});
		// 		Utils.bindComboboxEquipGroupEasyUI('groupEquipCBX' + index);
		// 		EquipmentManagerDelivery.setEquipGroup(index);
		// 		$('#groupEquipCBX' + index).combobox("setValue", group);
		// 	} else if (field == 'equipmentProviderCode') {
		// 		row[index].equipmentProvider = null;
		// 		var provider = row[index].equipmentProviderCode;
		// 		$('#providerEquipCBX' +  index).combobox('destroy');
		// 		$('#gridEquipmentDelivery').datagrid('updateRow',{
		// 			index: index,	
		// 			row: row
		// 		});
		// 		Utils.bindComboboxEquipGroupEasyUI('providerEquipCBX' + index);
		// 		EquipmentManagerDelivery.setEquipProvider(index);
		// 		$('#providerEquipCBX' + index).combobox("setValue", provider);
		// 	}
		// }	
	});
	if($("#idRecordHidden").val() != undefined && $("#idRecordHidden").val() != ""){
		$('#gridEquipmentDelivery').datagrid({url : '/equipment-manage-delivery/search-equipment-in-record',queryParams:{idRecordDelivery:$("#idRecordHidden").val()}});
		$('#btnUpdate').bind('click', function(event) {		
			EquipmentManagerDelivery.updateRecordDelivery();
		});
	}else{
		$('#btnUpdate').bind('click', function(event) {		
			EquipmentManagerDelivery.createRecordDelivery();
		});
		var params = new Object();
		var equipLendCode = $("#lendCode").val().trim();
		if(equipLendCode != undefined && equipLendCode != null && equipLendCode != '') {
			params.equipLendCode = equipLendCode;
		}
		Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-manage-delivery/get-equipment-info', function(result) {
			if (result.lstEquipGroup != undefined && result.lstEquipGroup != null && result.lstEquipGroup.length > 0) {
				EquipmentManagerDelivery._lstEquipGroup = result.lstEquipGroup;
			}
			if (result.lstEquipProvider != undefined && result.lstEquipProvider != null && result.lstEquipProvider.length > 0) {
				EquipmentManagerDelivery._lstEquipProvider = result.lstEquipProvider;
			}
			EquipmentManagerDelivery.getDataEquipStock();

			EquipmentManagerDelivery.insertEquipmentInGrid();
		}, null, null);
		
	}
	
	// upload file
	var urlUpdate="";
	if($("#idRecordHidden").val() == ""){
		urlUpdate = "/equipment-manage-delivery/create-record-delivery";
	}else{
		urlUpdate = "/equipment-manage-delivery/update-record-delivery";
	}

	// EquipmentManagerDelivery.selectToShop();
	UploadUtil.initUploadFileByEquipment({
		url: urlUpdate,	
		elementSelector: 'body',
		elementId: 'body',
		clickableElement: '.addFileBtn',
		parallelUploads: 5
		//maxFilesize: 5
	}, function (data) {
		if(data.error){
			$('#errMsgInfo').html(data.errMsg).show();
			return false;
		}else{
			if($("#idRecordHidden").val() == ""){
				$('#successMsgInfo').html("Cập dữ liệu thành công.").show();	
				EquipmentManagerDelivery._lstEquipInsert = null;
				EquipmentManagerDelivery._editIndex = null;				
				setTimeout(function(){
					$('#successMsgInfo').html("").hide(); 
					window.location.href = '/equipment-manage-delivery/info';						              		
				}, 3000);
			} else {
				$('#successMsgInfo').html("Cập dữ liệu thành công.").show();
				EquipmentManagerDelivery._lstEquipInsert = null;
				EquipmentManagerDelivery._editIndex = null;
				EquipmentManagerDelivery._lstEquipInRecord = null;
				EquipmentManagerDelivery._numberEquipInRecord = null;
				if($('#status').val() != 4){					
					setTimeout(function(){
						$('#successMsgInfo').html("").hide(); 
						window.location.href = '/equipment-manage-delivery/change-record?id='+ $("#idRecordHidden").val().trim();						
					}, 3000);
				}else{
					setTimeout(function(){
						window.location.href = '/equipment-manage-delivery/info';						              		
					}, 3000);
				}
			}
		}
	});
	setTimeout(function() {
		if ($('#idRecordHidden').val() == "") {
			if ($('#contentDelivery').val() != EquipmentManagerDelivery._DELIVERY_CONTENT_NEW) {
				disableCombo('groupEquipCBX' + EquipmentManagerDelivery._editIndex);
				disableCombo('providerEquipCBX' + EquipmentManagerDelivery._editIndex);
				disableCombo('stockEquipCBX' + EquipmentManagerDelivery._editIndex);
			}
		}
	}, 3000);
	
// 	var tm = setTimeout(function(){
// 		var tpmfc =  $('#numberContract').val();
// 		if (tpmfc.trim().length == 0) {
// 			$('#numberContract').focus();
// 		}
// 		clearTimeout(tm);
// 	}, 50);
});
</script>
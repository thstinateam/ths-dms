<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!-- Begin vuongmq; 07/05/2015; ATTT xuat file -->
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<!-- End vuongmq; 07/05/2015; ATTT xuat file -->
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="javascript:void(0);">Tìm kiếm thiết bị</a>
		</li>
		<li>
			<span>Danh sách thiết bị</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div id="pnlListEquipment" class="easyui-panel" title="Thông tin tìm kiếm" style="width:auto; height:auto; padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
					<div class="SearchInSection SProduct1Form">
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style" style="">Đơn vị</label>
						<!-- <input id="shop" type="text" data-placeholder="Tất cả" style="height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" /> -->
						<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />

						<div class="Clear"></div>
						<label class="LabelStyle Label1Style" style="">Mã thiết bị</label> 
						<input	type="text" class="InputTextStyle InputText1Style" id="txtCode" /> 
						<label	class="LabelStyle Label1Style">Số serial</label> 
						<input type="text" class="InputText1Style" style="float: left; border: 1px solid #c4c4c4; height: 20px; line-height: 20px; margin-bottom: 9px; padding: 0 4px;" id="txtSerial" />					
						<label	class="LabelStyle Label1Style">Nhà cung cấp</label>
						<div class="BoxSelect BoxSelect2" id="ddlEquipProviderIdDiv">
							<!-- <select class="MySelectBoxClass" id="ddlEquipProviderId">
								<option value="-1" selected="selected">Tất cả</option>
								<s:iterator value="lstEquipProvider" var="equipPro">
									<option value="<s:property value='id' />"><s:property value="code" /> - <s:property value="name" /></option>
								</s:iterator>
							</select> -->
							<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
							<select id="ddlEquipProviderId"></select>
						</div>
						<div class="Clear"></div>
						<label	class="LabelStyle Label1Style" title="Trạng thái tham gia giao dịch" style="">Trạng thái TGGD</label>
						<div class="BoxSelect BoxSelect2" id="ddlTradeStatusDiv">
							<select class="MySelectBoxClass InputTextStyle" id="ddlTradeStatus" onchange="EquipmentListEquipment.onchangeDdlTradeStatusDiv(this);">
								<option value="-1" selected="selected">Tất cả</option>
								<option value="1">Đang giao dịch</option>
								<option value="0">Không giao dịch</option>
							</select>
						</div>	
						<label	class="LabelStyle Label1Style">Loại giao dịch</label>
						<div class="BoxSelect BoxSelect2" id="ddlTradeTypeDiv">
							<select class="MySelectBoxClass InputTextStyle" id="ddlTradeType">
								<option value="-1" selected="selected">Tất cả</option>
								<option value="0">Giao nhận</option>
								<option value="1">Chuyển kho</option>
								<option value="2">Sửa chữa</option>
								<option value="3">Báo mất</option>
								<option value="4">Báo mất từ mobile</option>
								<option value="5">Thu hồi thanh lý</option>
								<option value="6">Thanh lý</option>
								<option value="7">Chờ thanh lý</option>
							</select>
						</div>
						<label class="LabelStyle Label1Style">Loại</label>
						<div class="BoxSelect BoxSelect2" id="ddlEquipCategoryDiv">
							<select class="MySelectBoxClass" id="ddlEquipCategory" onchange="EquipmentListEquipment.onchangeDdlEquipCategoryAuto(this);">
								<option value="-1" selected="selected">Tất cả</option>
								<s:iterator value="lstEquipCategory" var="equiqCtg">
									<option value="<s:property value='id' />"><s:property value="code" /> - <s:property value="name" /></option>
								</s:iterator>
							</select>
						</div>
						<div class="Clear"></div>	
						<label	class="LabelStyle Label1Style" style="">Nhóm thiết bị</label>
						<div class="BoxSelect BoxSelect2" id="ddlEquipGroupIdDiv">
							<!-- <select class="MySelectBoxClass InputTextStyle" id="ddlEquipGroupId">
								<option value="-1" selected="selected">Tất cả</option>
								<s:iterator value="result.get('lstEquipGroup')" var="equipPro">
									<option value="<s:property value='id' />"><s:property value="code" /> - <s:property value="name" /></option>
								</s:iterator>
							</select> -->
							<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
							<select id="ddlEquipGroupId"></select>
						</div>
						<label class="LabelStyle Label1Style">Mã kho</label> 
						<input	type="text" class="InputTextStyle InputText1Style" id="txtStockCode" /> 
						<label	class="LabelStyle Label1Style">Tên kho</label> 
						<input type="text" class="InputText1Style" id="txtStockName" style="float: left; border: 1px solid #c4c4c4; height: 20px; line-height: 20px; margin-bottom: 9px; padding: 0 4px;" />
						<div class="Clear"></div> 
						<label class="LabelStyle Label1Style" style="">Mã khách hàng</label>
						<input type="text" id="txtCustomerCode" class="InputTextStyle InputText1Style" />
						<label class="LabelStyle Label1Style">Tên khách hàng</label>
						<input type="text" id="txtCustomerName" class="InputTextStyle InputText1Style" />
						<label class="LabelStyle Label1Style">Địa chỉ KH</label>
						<input type="text" id="txtCustomerAddress" class="InputTextStyle InputText1Style" />				
						<div class="Clear"></div>
<!-- 						<label class="LabelStyle Label1Style"  style="">Mã biên bản</label>  -->
<!-- 						<input	type="text" class="InputTextStyle InputText1Style" tabindex="9" id="txtEquipImportRecordCode" /> -->
<!-- 						<div class="Clear"></div> -->
						<label class="LabelStyle Label1Style" style="">Ngày đưa vào SD từ</label>
						<div id="fromDateDiv">
							<input type="text" id="fDate" tabindex="10" class="InputTextStyle InputText6Style" />
						</div>
						<label class="LabelStyle Label1Style">Đến ngày</label>
						<div id="toDateDiv">
							<input type="text" id="tDate" tabindex="11" class="InputTextStyle InputText6Style" />
						</div>
						<label class="LabelStyle Label1Style">Trạng thái</label>
						<div class="BoxSelect BoxSelect2" id="ddlUsageStatusDiv">
							<select class="MySelectBoxClass InputTextStyle" id="ddlUsageStatus">
								<option value="-1" selected="selected">Tất cả</option>
							    <option value="0">Đã mất</option>
							    <option value="1">Đã thanh lý</option>
							    <option value="2">Đang ở kho</option>
							    <option value="3">Đang sử dụng</option>
							    <option value="4">Đang sửa chữa</option>
							</select>
						</div>
						<div class="Clear"></div>
						<div class="SearchInSection SProduct1Form">
							<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
							<div class="BtnCenterSection">
								<button id="btnSearch" class="BtnGeneralStyle" onclick="EquipmentListEquipment.searchListEquipment();">Tìm kiếm</button>
							</div>
							<div class="Clear"></div>
						</div>	
					</div>			
				</div>
				<h2 class="Title2Style">Danh sách thiết bị
					<a onclick = "EquipmentListEquipment.exportBySearchListEquipment();" style = "float: right;	padding: 0px 25px 0px 0px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_export.png" title="Xuất Excel"></a>
					<!-- <a onclick = "EquipmentListEquipment.openDialogImportListEquipment();" tabindex="13" style = "float: right;	padding-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_import.png" title="Nhập Excel"></a> -->
<!-- 					<a onclick = "EquipmentListEquipment.downloadTemplateImportListEquip();" tabindex="12" style = "float: right; padding-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_download_32.png" title="Tải mẫu Nhập Excel"></a> -->
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="listEquipmentDg" class="easyui-datagrid"></table>
					</div>
				</div>		
				<div class="Clear"></div>
			</div>
		</div>
	</div>
	<div class="Clear"></div>
	<!-- Chi tiet chuc nang Form -->
	<div class="ContentSection" style="visibility:hidden; display: none;" id="equipHistoryTemplateDiv">
		<tiles:insertTemplate template="/WEB-INF/jsp/equipment/list-manage-equipment/insertTemplateEquipHistory.jsp" />
	</div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Nhập Excel Danh sách thiết bị" data-options="closed:true,modal:true" style="width:465px;height:250px;">
		<div class="PopupContentMid">
			<div class="SearchInSection SProduct1Form" style="border: none;">
				<form action="/equipment-list-manage/importEquipListEquip" name="importFrmListEquipListEquip" id="importFrmListEquip" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style" style="padding-right: 10px; text-align: right; width: 80px;">Tập tin Excel</label>
				    <div class="UploadFileSection Sprite1" style="width: 270px;">
						<input id="fakefilepcListEquip" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" style="width: 270px;" size="1" name="excelFile" id="excelFileListEquip" onchange="previewImportExcelFile(this,'importFrmListEquipListEquip','fakefilepcListEquip', 'errExcelMsgListEquip');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a onclick="EquipmentListEquipment.downloadTemplateImportListEquip();" href="javascript:void(0)" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="EquipmentListEquipment.importListEquipment();">Nhập Excel</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').dialog('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p id="successExcelMsgListEquip" class="SuccessMsgStyle" style="display:none;"></p>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgListEquip" />
	</div>	
</div>

<script type="text/javascript">
$(document).ready(function() {
	EquipmentListEquipment._curShopId = '<s:property value="shopId"/>';
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	
	// $("#shop").kendoMultiSelect({
 //        dataTextField: "shopCode",
 //        dataValueField: "shopId",
 //        filter: "contains",
	// 	itemTemplate: function(data, e, s, h, q) {
	// 		var level = data.level;
	// 		if (level == ShopDecentralizationSTT.VNM) {
	// 			return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px;margin-left: -10px;"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
	// 		} else if (level == ShopDecentralizationSTT.KENH) {
	// 			return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
	// 		} else if (level == ShopDecentralizationSTT.MIEN) {
	// 			return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
	// 		} else if (level == ShopDecentralizationSTT.VUNG) {
	// 			return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
	// 		} else if (level == ShopDecentralizationSTT.NPP) {
	// 			return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
	// 		} else {
	// 			return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
	// 		}
	// 	},
 //        tagTemplate: '#: data.shopCode #',
 //        change: function(e) {
 //        },
 //        dataSource: {
 //            transport: {
 //                read: {
 //                    dataType: "json",
 //                    url: "/rest/report/shop/kendo-ui-combobox-ho-have-shop-off/0.json"
 //                }
 //            }
 //        },
 //        value: [EquipmentListEquipment._curShopId]
 //    });
 //    var shopKendo = $("#shop").data("kendoMultiSelect");
 //    shopKendo.wrapper.attr("id", "shop-wrapper");

 	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [EquipmentListEquipment._curShopId]
    });

    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");


	// Utils.initUnitCbx('shop', null, 206, function(rec) {
 //    	if (rec != null && rec.shopCode != ReportUtils._currentShopCode) {
	// 		$('#shopCode').val(rec.shopCode);
	// 		ReportUtils._currentShopCode = rec.shopCode;
	// 		disabled('paidDebit');
			
	// 		var chooseStatus = $('#invoiceType').val();
	// 		var shopCode = $('#shopCode').val();
	// 		var params = new Object();
	// 		params.invoiceTypeChoice = chooseStatus;
	// 		params.shopCode = shopCode;
	// 		$('#dg').datagrid('load',params);
 //    	}
	// }, function(arr) {
 //       	if (arr != null && arr.length > 0) {
 //       		ReportUtils._currentShopCode = arr[0].shopCode;
 //       	}
	// });

	$('#txtSerial').width($('#txtCode').width());
	$('#txtCode, #txtSerial, #ddlEquipCategoryDiv, #ddlEquipGroupIdDiv, #ddlEquipProviderIdDiv, #ddlUsageStatusDiv, #ddlTradeStatusDiv, #ddlTradeTypeDiv').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});
	//tamvnm: Khoi tao du lieu cho autoCompleteCombobox.
	Utils.bindComboboxEquipGroupEasyUICodeName('ddlEquipProviderId');
	Utils.bindComboboxEquipGroupEasyUICodeName('ddlEquipGroupId');
	EquipmentListEquipment._lstEquipProvider = [];
	EquipmentListEquipment._lstEquipGroup = [];
	<s:iterator value="lstEquipProvider" var="equipPro">
		var obj = {
			id: Utils.XSSEncode("<s:property value='id' />"),
			code: Utils.XSSEncode("<s:property value='code' />"),
			name: '<s:property value="escapeHTMLForXSS(name)" escapeHtml="false" />',
			codeName: '<s:property value="escapeHTMLForXSS(codeName)" escapeHtml="false" />'
			
		};
		EquipmentListEquipment._lstEquipProvider.push(obj);
	</s:iterator>
	<s:iterator value="result.get('lstEquipGroup')" var="equipGroup">
		var obj = {
			id: Utils.XSSEncode("<s:property value='id' />"),
			code: Utils.XSSEncode("<s:property value='code' />"),
			name: '<s:property value="escapeHTMLForXSS(name)" escapeHtml="false" />',
			codeName: "<s:property value='escapeHTMLForXSS(codeName)' escapeHtml='false' />"
		};
		EquipmentListEquipment._lstEquipGroup.push(obj);
	</s:iterator>

	$('#pnlListEquipment').width($(window).width()- 25);
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
	}, 1000);

	//tamvnm: update combobox thanh autoCompleteCombobox
	var providerId = -1;
	var groupId = -1;
	if ($('#ddlEquipProviderId').combobox("getValue") != "" 
		&& $('#ddlEquipProviderId').combobox("getValue") != undefined
		&& $('#ddlEquipProviderId').combobox("getValue") != null) {
		providerId = $('#ddlEquipProviderId').combobox("getValue");
	}
	if ($('#ddlEquipGroupId').combobox("getValue") != "" 
		&& $('#ddlEquipGroupId').combobox("getValue") != undefined
		&& $('#ddlEquipGroupId').combobox("getValue") != null) {
		groupId = $('#ddlEquipGroupId').combobox("getValue");
	}

	EquipmentListEquipment._params = {
			equipCategoryId: $('#ddlEquipCategory').val(),
			// equipGroupId: $('#ddlEquipGroupId').val(),
			equipGroupId: groupId,
			tradeType: $('#ddlTradeType').val(),
			tradeStatus: $('#ddlTradeStatus').val(),
			usageStatus: $('#ddlUsageStatus').val(), 
			equipProviderId: providerId, 
			code: $('#txtCode').val(),
			seriNumber: $('#txtSerial').val(),
// 			equipImportRecordCode: $('#txtEquipImportRecordCode').val(),
			stockCode: $('#txtStockCode').val().trim(),
			stockName: $('#txtStockName').val().trim(),
			customerCode: $('#txtCustomerCode').val().trim(),
			customerName: $('#txtCustomerName').val().trim(),
			customerAddress: $('#txtCustomerAddress').val().trim(),
			fromDateStr: $('#fDate').val().trim(), 
			toDateStr: $('#tDate').val().trim()
	};
	
	$('#listEquipmentDg').datagrid({
		url : "/equipment-list-manage/searchListEquipmentVO",
		pagination : true,
        rownumbers : true,
        pageNumber : 1,
        scrollbarSize: 0,
        autoWidth: true,
        height: 345,
        pageList: [20, 50, 100],
        autoRowHeight : true,
        fitColumns : false,
        singleSelect: true,
		queryParams: EquipmentListEquipment._params,
		width: $('#gridContainer').width() - 25,
		frozenColumns:[[
			{field:'edit', title:'', width: 60, align: 'center', sortable: false, resizable: true, formatter: function(value,row,index){
				   	return '<a onclick="EquipmentListEquipment.viewEquipmentHistory('+row.equipId+',\''+row.code+'\')" href="javascript:void(0)"><img width="18" height="18" src="/resources/images/icon_view_history.png" title="Lịch sử sửa chữa '+row.code+'"></a><a onclick="" href="/equipment-list-manage/viewDetailListEquipmentJSP?id='+row.equipId+'"><img width="18" style="padding-left: 5px;" height="18" src="/resources/images/icon-edit.png" title="Chỉnh sửa '+row.code+'"></a>';
				}},
// 			{field:'equipImportRecordCode', title: 'Mã biên bản', width:100, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
// 				return Utils.XSSEncode(value);
// 			}},
			{field:'createDateStr', title: 'Ngày đưa vào SD', width:110, sortable:false, resizable: false, align: 'center', formatter:function(value,row,index){
				return Utils.XSSEncode(value);
			}},
			{field:'code', title: 'Mã thiết bị', width: 180, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
				return Utils.XSSEncode(value);
			}},
			{field:'serial', title: 'Số serial', width: 160, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
				return Utils.XSSEncode(value);
			}},
			{field:'healthStatus', title: 'Tình trạng TB', width: 130, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
				return Utils.XSSEncode(value);
			}},
		]],
		columns:[[
			{field:'usageStatus', title: 'Trạng thái', width: 110, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
				return usageStatusEquipment.parseValue(value);
			}},
		    {field:'tradeStatus', title: 'Trạng thái TGGD', width: 120, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
		    	return tradeStatusEquipment.parseValue(value);
		    }},
		    {field:'tradeType', title: 'Loại giao dịch', width: 90, align: 'left',sortable: false, resizable: false, formatter:function(value,row,index){
		    	return tradeTypeEquipment.parseValue(value);
		    }},
		    {field:'equipGroupName', title: 'Nhóm thiết bị', width:150, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},	
		    {field:'equipCategoryName',title: 'Loại', width: 100, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
			{field:'capacityStr', title: 'Dung tích (lít)', width: 80, sortable: false, resizable: false, align: 'right', formatter:function(value,row,index){
				return Utils.XSSEncode(value);
		    }},	
			{field:'equipBrandName', title: 'Hiệu', width:120, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},	
			{field:'equipProviderName', title: 'NCC', width:100, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);	
		    }},
		    {field:'manufacturingYear', title: 'Năm sản xuất', width:60, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},	
		    {field:'stockCode', title: 'Kho', width:220, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value) + ' - ' + Utils.XSSEncode(row.stockName);
		    }},
		    {field:'shortCode', title: 'Khách hàng', width:180, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	if (value != undefined && value != null && value.trim().length > 0) {
		    		return Utils.XSSEncode(value) + ' - ' + Utils.XSSEncode(row.customerName);		    		
		    	}
		    	return '';
		    }},
		    {field:'customerAddress', title: 'Địa chỉ KH', width:150, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	if (value != undefined && value != null && value.trim().length > 0) {
		    		return Utils.XSSEncode(row.customerAddress);
		    	}
		    	return '';
		    }},
		    {field:'file', title:'Tập tin đính kèm', width:80, sortable: false, resizable: false, align:'center', hidden: true, formatter: function(value, row, index){
		    	if (row.countFile > 0) {
		    		return '<a href="javascript:void(0)" onclick="General.downloadEquipAttechFile('+row.equipImportRecordId+', 9);">Tập tin</a>';		    		
		    	}
// 		    	return '<a href="javascript:void(0)" onclick="return false;" style="color: #ccc" >Tập tin</a>';
		    	return '';
		    }}
	    ]],
	    onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');
	    	Utils.updateRownumWidthAndHeightForDataGrid('listEquipmentDg');
   		 	$(window).resize();    		 
	    }
	});
	
	
	$('#pnlListEquipment input').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});
	$('#txtCode').focus();
	EquipmentListEquipment.setEquipProvider();
	EquipmentListEquipment.setEquipGroup();
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li><span>Lịch sử sửa chữa </span><span id="equipmentCodeFromSpan" style="color:#199700"></span></li>
    </ul>
</div>
<div class="GeneralCntSection">
	<div class="GridSection" id="equipHistoryDgContainerGrid">
		 <table id="equipHistoryDg"></table>
	</div>
</div>

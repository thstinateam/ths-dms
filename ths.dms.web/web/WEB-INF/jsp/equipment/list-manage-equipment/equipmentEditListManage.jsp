<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="/equipment-list-manage/info">Danh sách thiết bị</a>
		</li>
		<li>
		<s:if test='equip!= null && equip.id > 0'>
			<span>Cập nhật thông tin thiết bị</span>
		</s:if>
		<s:else>
		<!-- tamvnm: bo cap moi -->
			<!-- <span>Cấp mới thiết bị</span> -->
			<span>Cập nhật thông tin thiết bị</span>
		</s:else>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin thiết bị</h2>
				<div class="SearchInSection SProduct1Form">
					<s:if test='equip!= null && equip.id > 0'>
						<label class="LabelStyle Label1Style" style="height:auto;">Mã thiết bị</label> 
						<input id="txtCode" type="text" readonly="readonly" disabled="disabled" value="<s:property value="equip.code" />" class="InputTextStyle InputText1Style" maxlength="100"/> 
						<label class="LabelStyle Label1Style">Số serial </label> 
						<input id="txtSerial" type="text" class="InputText1Style" style="float: left; border: 1px solid #c4c4c4; height: 20px; line-height: 20px; margin-bottom: 9px; padding: 0 4px;" value="<s:property value="equip.serial" />" maxlength="100"/> 
		       		</s:if>
		       		<s:else>
						<label class="LabelStyle Label1Style" style="text-align:left;padding-left: 10px;height:auto;">Mã biên bản</label> 
						<input id="txtEquipImportRecordCode" readonly="readonly" disabled="disabled" value="" type="text" class="InputTextStyle InputText1Style" maxlength="10"/> 
						<label class="LabelStyle Label1Style">Số lượng <span class="ReqiureStyle"> *</span></label> 
						<input id="txtQuantity" type="text" class="InputTextStyle InputText1Style vinput-money" maxlength="4"/> 
		       		</s:else>
					<label class="LabelStyle Label1Style">Nhóm thiết bị</label>
					<div class="BoxSelect BoxSelect2" id="ddlEquipGroupIdDiv">
						<select class="MySelectBoxClass InputTextStyle" id="ddlEquipGroupId">
							<s:iterator value="result.get('lstEquipGroup')" var="equipPro">
								<option value="<s:property value='id' />"><s:property value="code" /> - <s:property value="name" /></option>
							</s:iterator>
						</select>
					</div>
					<div class="Clear"></div>
					<label	class="LabelStyle Label1Style" style="height:auto;">Nhà cung cấp</label>
					<div class="BoxSelect BoxSelect2" id="ddlEquipProviderIdDiv">
						<!-- <select class="MySelectBoxClass" id="ddlEquipProviderId">
							<s:iterator value="lstEquipProvider" var="equipPro">
								<option value="<s:property value='id' />"><s:property value="code" /> - <s:property value="name" /></option>
							</s:iterator>
						</select> -->
						<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
						<select id="ddlEquipProviderId"></select>
					</div>		
					<label class="LabelStyle Label1Style">Nguyên giá<span class="ReqiureStyle"> *</span></label> 
					<input id="txtPrice" type="text" value="<s:property value="equip.price" />" class="InputTextStyle InputText1Style vinput-money" maxlength="22" />
					<label class="LabelStyle Label1Style">Ngày hết bảo hành</label> 
					<input id="txtWarrantyExpiredDate" type="text" class="InputTextStyle InputText1Style vinput-date" value="<s:property value="warrantyExpiredDateStr" />" />
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style" style="height:auto;">Năm sản xuất<span class="ReqiureStyle"> *</span></label> 
					<input id="txtManufacturingYear" type="text" class="InputTextStyle InputText1Style vinput-number" maxlength="4" value="<s:property value="manufacturingYearStr" />" />			
					<label class="LabelStyle Label1Style">Trạng thái sử dụng</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass InputTextStyle" id="ddlUsageStatus">
						    <option value="0">Đã mất</option>
						    <option value="2" selected="selected">Đang ở kho</option>
						    <option value="1">Đã thanh lý</option>
						    <option value="3">Đang sử dụng</option>
						    <option value="4">Đang sửa chữa</option>
						</select>
					</div>			
					<label	class="LabelStyle Label1Style">Tình trạng thiết bị</label>
					<div class="BoxSelect BoxSelect2" id="ddlUsageStatusDiv">
						<select class="MySelectBoxClass" id="ddlHealthStatus">
							<option selected="selected" value=""></option>	
							<s:iterator value="result.get('lstHealthStatus')" var="healthStatus">
								<option value="<s:property value='apParamCode' />"><s:property value="value" escapeXml="true"/></option>
							</s:iterator>
						</select>
					</div>	
					<div class="Clear"></div>
					<s:if test='equip!= null && equip.id > 0'>
						<label class="LabelStyle Label1Style" style="height:auto;" >Kho(F9)<span class="ReqiureStyle"> *</span></label>
						<input type="text"	class="InputTextStyle InputText1Style" readonly="readonly" disabled="disabled" id="stockCode" value="<s:property value="equip.stockCode" />" /> 
		       		</s:if>
		       		<s:else>
						<label class="LabelStyle Label1Style" style="text-align:left;padding-left: 10px;height:auto;" >Kho(F9)<span class="ReqiureStyle"> *</span></label>
						<input type="text"	class="InputTextStyle InputText1Style" id="stockCode" value="<s:property value="stockCode" />" /> 
		       		</s:else>
					<div class="Clear"></div>
				</div>	
				<s:if test='equip != null && equip.id > 0'>
		       	</s:if>
		       	<s:else>
					<h2 class="Title2Style">Tập tin đính kèm</h2>
					<div class="SearchInSection SProduct1Form">
						<a href="javascript:void(0);" style="float: left;" title="Lựa chọn tập tin" ><img class="addFileBtn" src="/resources/images/icon_attach_file.png"></a>
						<div class="fileupload-process" style="width: 90%;">
							<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-striped active" id="total-progress" style="opacity: 0; width: 550px;">
								<div data-dz-uploadprogress="" style="width: 200px;" class="progress-bar progress-bar-success"></div>
							</div>
						</div>
						<div style="float:left; min-width: 500px; width: 90%;">
							<div style="height: auto; margin-left:20px;">
								<s:iterator id="fileVo" value="lstFileVo">
									<div class="divImageUploadFile" id = "divEquipAttachFile<s:property value="#fileVo.fileId"/>">
										<div class="imageUpdateShowIcon">
											<img class="addFileBtn" src="/resources/images/img-folder-001.png" width="50" height="50">
										</div>
										<div class="imageUpdateShowContent">
											<label><s:property value="#fileVo.fileName"/></label><br />
											<a href="javascript:void(0)" onclick="EquipmentListEquipment.removeEquipAttachFile(<s:property value="#fileVo.fileId"/>);"><img title="Xóa tập tin" src="/resources/images/icon_delete.png" width="12" height="12" style="padding-left: 5px;"></a>
										</div>
									</div>
								</s:iterator>
							</div>
							<div class="Clear"></div>
							<div class="table table-striped" class="files" id="previews" style="min-height: 100px; margin-left:20px;">
								<div id="template" class="file-row" style="margin-bottom: 10px; width: 260px;float: left">
									<div style="margin-right: 10px; width: 50px; float: left">
										<span class="preview"><img data-dz-thumbnail /></span>
									</div>
									<div style="display: inline-block; width: 200px; clear: both">
										<div style="padding-right: 10px">
											<div>
												<p class="name" style="overflow: hidden" data-dz-name></p>
												<strong class="error text-danger" data-dz-errormessage></strong>
											</div>
											<div style="">
												<p class="size" data-dz-size></p>
											</div>
											<div>
												<a data-dz-remove href="javascript:void(0)"><span><img border="0" src="/resources/scripts/plugins/uploadify2.1/cancel.png"></span></a>
											</div>
										</div>								
									</div>							
								</div> 
							</div>
						</div>
						<div class="Clear"></div>
						
					</div>	
		       	</s:else>
				<div class="Clear"></div>
				<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
				<p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
				<div class="Clear"></div>	
				<div class="SearchInSection SProduct1Form">                                
                   <div class="BtnCenterSection" style="padding-bottom: 10px;">
				       <s:if test='equip!= null && equip.id > 0'>
				       		<button id="btnChangeEquipment" class="BtnGeneralStyle" onclick="EquipmentListEquipment.addOrUpdateEquipment(<s:property value='equip.id' />);">Cập nhật</button> &nbsp;&nbsp;
				       </s:if>
				       <s:else>
				       		<button id="btnChangeEquipment" class="BtnGeneralStyle" onclick="EquipmentListEquipment.addOrUpdateEquipment(0);">Cập nhật</button> &nbsp;&nbsp;
				       </s:else>	
				       <button class="BtnGeneralStyle" onclick="window.location.href='/equipment-list-manage/info'">Bỏ qua</button>
		           </div>
	            </div>		
			</div>
			<div class="Clear"></div>
		</div>
		
	</div>
	<div class="Clear"></div>
</div>
<s:if test='equip!= null && equip.id > 0'>
	<script type="text/javascript">
		$(function(){
			EquipmentListEquipment._equipChange = {
				id: '<s:property value="equip.id" />',
				code : '<s:property value="equip.code" />',
				serial: '<s:property value="equip.serial" />',
				equipGroupId: '<s:property value="equip.equipGroup.id" />',
				equipProviderId: '<s:property value="equip.equipProvider.id" />',
				price: '<s:property value="equip.price" />',
				usageStatus: '<s:property value="equip.usageStatus" />',
				healthStatus: '<s:property value="equip.healthStatus" />'
			};
			//tamvnm: Khoi tao du lieu cho autoCompleteCombobox.
			Utils.bindComboboxEquipGroupEasyUICodeName('ddlEquipProviderId');
			EquipmentListEquipment._lstEquipProvider = [];
			// <s:iterator value="lstEquipProvider" var="equipPro">
			// 	var obj = {
			// 		id: Utils.XSSEncode("<s:property value='id' />"),
			// 		code: Utils.XSSEncode("<s:property value='code'  escapeHtml='false' />"),
			// 		name: Utils.XSSEncode("<s:property value='name' escapeHtml='false' />"),
			// 		codeName: Utils.XSSEncode("<s:property value='codeName' escapeHtml='false' />")
			// 	};
			// 	EquipmentListEquipment._lstEquipProvider.push(obj);
			// </s:iterator>
			<s:iterator value="lstEquipProvider" var="equipPro">
				var obj = {
					id: Utils.XSSEncode("<s:property value='id' />"),
					code: Utils.XSSEncode("<s:property value='code' />"),
					name: '<s:property value="name.replace(\"\'\", \"\\\\\'\").replace(\"/\", \"\\\\/\")" escapeHtml="false" />',
					codeName: "<s:property value='codeName.replace(\"\'\", \"\\\\\'\").replace(\"/\", \"\\\\/\")' escapeHtml='false' />"
				};
				EquipmentListEquipment._lstEquipProvider.push(obj);
			</s:iterator>
			EquipmentListEquipment.setEquipProvider();
			// if (!isNaN(EquipmentListEquipment._equipChange.equipProviderId)) {
			// 	selectedDropdowlist('ddlEquipProviderId', EquipmentListEquipment._equipChange.equipProviderId);				
			// } else {
			// 	selectedDropdowlist('ddlEquipProviderId', 0);
			// }
			if (!isNaN(EquipmentListEquipment._equipChange.equipProviderId)) {
				// selectedDropdowlist('ddlEquipProviderId', EquipmentListEquipment._equipChange.equipProviderId);	
				$('#ddlEquipProviderId').combobox("setValue", EquipmentListEquipment._equipChange.equipProviderId);			
			} else {
				// selectedDropdowlist('ddlEquipProviderId', 0);
				$('#ddlEquipProviderId').combobox("setValue", 0);	
			}
			if (!isNaN(EquipmentListEquipment._equipChange.equipGroupId)) {
				selectedDropdowlist('ddlEquipGroupId', EquipmentListEquipment._equipChange.equipGroupId);				
			} else {
				selectedDropdowlist('ddlEquipGroupId', 0);
			}
			if (EquipmentListEquipment._equipChange.usageStatus != undefined && EquipmentListEquipment._equipChange.usageStatus != null) {
				selectedDropdowlist('ddlUsageStatus', usageStatusEquipment.parseName(EquipmentListEquipment._equipChange.usageStatus));				
			} else {
				selectedDropdowlist('ddlUsageStatus', 0);
			}
			if (EquipmentListEquipment._equipChange.healthStatus != undefined && EquipmentListEquipment._equipChange.healthStatus != null) {
				//selectedDropdowlist('ddlHealthStatus', Utils.XSSEncode(EquipmentListEquipment._equipChange.healthStatus));
			} else {
				//selectedDropdowlist('ddlHealthStatus', "");				
			}
			$('#txtSerial').width($('#txtCode').width());
		});
	</script>
</s:if>
<s:else>
<script type="text/javascript">
		$(function(){
			// upload file
			UploadUtil.initUploadFileByEquipment({
				url: '/equipment-list-manage/addOrUpdateEquipment',	
				elementSelector: 'body',
				elementId: 'body',
				clickableElement: '.addFileBtn',
				parallelUploads: 5
				//maxFilesize: 5
			}, function (data) {
				if (!data.error) {
					$("#successMsg").html("Lưu dữ liệu thành công").show();
					var t = setTimeout(function(){
						$('.SuccessMsgStyle').html("").hide();
						window.location.href = '/equipment-list-manage/info';
						clearTimeout(t);
					 }, 1000);
				} else {
					$('#errMsg').html(data.errMsg).show();
				}
			});
			
		});

		Utils.bindComboboxEquipGroupEasyUICodeName('ddlEquipProviderId');
	</script>
</s:else>
<script type="text/javascript">
$(document).ready(function() {
	$('#txtWarrantyExpiredDate').width($('#txtManufacturingYear').width()-22);
	disableSelectbox('ddlUsageStatus');
	if (EquipmentListEquipment._equipChange == null || EquipmentListEquipment._equipChange.id == undefined || EquipmentListEquipment._equipChange.id == null) {
		$('#ddlUsageStatus').html('<option value="2" selected="selected">Đang ở kho</option>').change().show();
	} else {
		 disableSelectbox('ddlEquipGroupId');
		
	}
	$('#txtCode, #txtSerial, #txtEquipImportRecordCode, #txtQuantity,  #txtWarrantyExpiredDate, #txtManufacturingYear').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnChangeEquipment').click(); 
		}
	});
	$('#txtPrice').val(formatCurrency($('#txtPrice').val()));
	

	// Kho F9
	$('#stockCode').bind('keyup', function(event) {
		if(event.keyCode == keyCode_F9){
			var txt = "stockCode";
			VCommonJS.showDialogSearch2({
				dialogInfo: {
					title: "Tìm kiếm kho"
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã đơn vị'},
			        {id:'name', maxlength:250, label:'Tên đơn vị'},
			    ],
			    url : '/equipment-list-manage/get-list-equip-stock-vo',
			    columns : [[
			        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	var nameCode = row.shopCode + '-' + row.shopName;
			        	return Utils.XSSEncode(nameCode);         
			        }},
			        {field:'code', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
// 			        	var nameCode = row.shopCode + '-' + row.name;
			            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.equipStockId +', \''+row.code+'\', \''+row.name+'\','+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
			        }}
			    ]]
			});
		}
	});
	
	$('#ddlHealthStatus option').first().remove();
	$('#ddlHealthStatus').change();
	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipment-period-manage/info">Thiết bị</a></li>
		<li><span>Chi tiết kỳ</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Mã kỳ</label> 
						<input	type="text" class="InputTextStyle InputText1Style InputValue " disabled="disabled" id="code" value="<s:property value="equipPeriod.code"/>"/> 						
					<label	class="LabelStyle Label6Style">Tên kỳ <span class="ReqiureStyle">(*)</span></label> 
					<input type="text" class="InputTextStyle InputText1Style InputValue " id="name" maxlength="250"
						value="<s:property value="equipPeriod.name"/>" />	
					
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Từ ngày <span class="ReqiureStyle">(*)</span></label> 
					<input maxlength="10" id="fromDate" value="<s:property value="fromDate"/>" type="text" class="InputTextStyle InputText6Style InputValue " />
					<label class="LabelStyle Label6Style">Đến ngày <span class="ReqiureStyle">(*)</span></label> 
					<input maxlength="10" id="toDate" type="text" value="<s:property value="toDate"/>" class="InputTextStyle InputText6Style InputValue " />	
					<div class="Clear"></div>
					<label	class="LabelStyle Label1Style">Trạng thái <span class="ReqiureStyle">(*)</span></label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status">
								<option value="1">Tạo mới</option>
							    <option value="2">Mở</option>
							    <option value="3">Đóng</option>
							</select>
						</div>						
					<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>										
				</div>			
					<div class="SearchInSection SProduct1Form">                                
	                   <div class="BtnCenterSection" style="padding-bottom: 10px;">
						       <button id="btnCapNhat" class="BtnGeneralStyle" >Cập nhật</button> &nbsp;&nbsp;
						       <button class="BtnGeneralStyle" onclick="window.location.href='/equipment-period-manage/info'">Bỏ qua</button>
			           </div>
		            </div>	
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<input type="hidden" value="<s:property value="id"/>" id="idObject" />
<script type="text/javascript">
$(document).ready(function() {	
	VTUtilJS.applyDateTimePicker('fromDate');
	VTUtilJS.applyDateTimePicker('toDate');
	var CREATED = 1,OPENED = 2,CLOSED = 3;
	var eqStatus = Number('<s:property value="equipPeriod.status.getValue()"/>');	
	if(eqStatus == OPENED){		
		$("#status option[value='"+CREATED+"']").remove();		
		$('.InputValue').attr('disabled',true);
		disableDateTimePicker('fromDate');
		disableDateTimePicker('toDate');
	}else if(eqStatus == CREATED){		
		$("#status option[value='"+CLOSED+"']").remove();

	}else if(eqStatus == CLOSED){
// 		$('.InputValue').attr('disabled',true);
// 		disableSelectbox('status');
// 		$('#btnCapNhat').hide();
// 		disableDateTimePicker('fromDate');
// 		disableDateTimePicker('toDate');
		$("#status option[value='"+CREATED+"']").remove();		
		$('.InputValue').attr('disabled',true);
		disableDateTimePicker('fromDate');
		disableDateTimePicker('toDate');
	}else{
		$("#status option[value='"+CLOSED+"']").remove();		
	}
	$('#name').focus();
	$("#status").val(eqStatus);
	$('#status').change();
	$('#btnCapNhat').bind('click',function(){
		var msg = '';	
		$('#errMsg').hide();	
		var isEdit = !isNullOrEmpty($('#idObject').val().trim());
		msg = Utils.getMessageOfRequireCheck('name','Tên kỳ');
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('fromDate','Từ ngày');	
		}
		if(msg.length == 0){
			msg = Utils.getMessageOfRequireCheck('toDate','Đến ngày');
		}				
		var fromDate = $('#fromDate').val().trim();
		var toDate = $('#toDate').val().trim();		
		var status = $('#status').val().trim();
		if ( (!isEdit || (isEdit && status == CREATED)) 
			&& (fromDate != '' || toDate != '') && msg.length ==0  ){			
			if(!Utils.compareDate(getCurrentDate(),fromDate) && msg.length == 0){
				msg = 'Từ ngày là ngày lớn hơn hoặc bằng ngày hiện tại';	
				$('#fromDate').focus();
			}
			if(!Utils.compareDate(getCurrentDate(),toDate) && msg.length == 0){
				msg = 'Đến ngày là ngày lớn hơn hoặc bằng ngày hiện tại';	
				$('#toDate').focus();
			}
			if(fromDate != '' && toDate != '' && !Utils.compareDate(fromDate, toDate) && msg.length ==0){
				msg = 'Từ ngày phải là ngày trước hoặc cùng ngày với đến ngày';	
				$('#fromDate').focus();
			}
		}
// 		if(msg.length == 0){
// 			msg = Utils.getMessageOfSpecialCharactersValidate('name','Tên kỳ');
// 		}
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}
		var params = new Object();
		params.fromDate = fromDate;
		params.toDate = toDate;
		params.status = $('#status').val().trim();
		params.name = $('#name').val().trim();
		params.id = $('#idObject').val().trim();
		Utils.addOrSaveData(params,'/equipment-period-manage/save',null,'errMsg',function(result){
			if (!result.error) {
				$('#code').val(result.equipmentPeriodCode);
				$('#btnCapNhat').remove();
				disabled('name');
				disableDateTimePicker('fromDate');
				disableDateTimePicker('toDate');
				disableSelectbox('status');
				setTimeout(function(){
					//window.location.href='/equipment-period-manage/view-detail?id=' + result.id;
					window.location.href = '/equipment-period-manage/info';
				}, 3000);
			}
		});

	});
});
</script>
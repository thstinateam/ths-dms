<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
#player_api {
	width: 678px !important;
}
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">Thiết bị</a></li>
		<li><span>Quản lý kỳ</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">				
					<div class="SearchInSection SProduct1Form">
						<label class="LabelStyle Label1Style">Năm</label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="year"> 							
							</select>
						</div>
						<div class="Clear"></div>
						<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
					</div>
					<div class="SearchInSection SProduct1Form">									
					<h2 class="Title2Style">Danh sách các kỳ trong năm</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="SearchInSection SProduct1Form">
							<div class="GridSection" id=gridContainer>
								<table id="grid"></table>
							</div>
						</div>	
					</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<script type="text/javascript">
$(document).ready(function() {		
	General.periodIdCloseMaxToDate = '<s:property value="periodIdCloseMaxToDate" />';
	if (General.periodIdCloseMaxToDate == null || General.periodIdCloseMaxToDate == undefined) {
		General.periodIdCloseMaxToDate = 0;
	}
	var currentYear = getCurrentYear();
	var html = '<option value="{0}">{1}</option>';
	var htmlFull = new Array();
	for(var i = 10; i > 0; -- i){
		htmlFull.push(format(html,currentYear - i,currentYear - i));
	}
	htmlFull.push(format('<option selected="selected" value="{0}">{1}</option>',currentYear,currentYear));
	for(var i = 1; i <= 5; ++ i){
		htmlFull.push(format(html,currentYear + i,currentYear + i));
	}
	$('#year').html(htmlFull.join(""));
	$('#year').val(currentYear);
	$('#year').change();

	$('#year').change(function(){
		var year = $(this).val().trim();
		var params = new Object();
		params.year = year;
		$('#grid').datagrid('load',params);
	});		
	var HTML_CREATED = '<a href="/equipment-period-manage/view-detail?id=0"><img src="/resources/images/icon_add.png" title="Thêm mới"/></a>';
	var HTML_UPDATED = '<a href="/equipment-period-manage/view-detail?id={0}"><img width="16" style="" heigh="16" src="/resources/images/icon-edit.png" title="Chỉnh sửa"></a>';	
	$('#grid').datagrid({
		url : "/equipment-period-manage/search",
		autoRowHeight : true,
		rownumbers : true, 
		checkOnSelect :true,
		singleSelect: true,
		pagination:true,
		rowNum : 50,
		fitColumns:true,
		pageList  : [20,50,100],
		scrollbarSize:0,
		width: $('#gridContainer').width(),
		autoWidth: true,
		queryParams:{year : currentYear},	
	    columns:[[	        
		    {field:'code', title: 'Mã kỳ', width: 100},
		    {field:'name', title: 'Tên kỳ', width:200, formatter:function(value, row, index) {
				return Utils.XSSEncode(value);	
			}},	    
		    {field:'fromDate',title: 'Từ ngày', width: 80, align: 'center',formatter:CommonFormatter.dateTimeFormatter},
		    {field:'toDate',title: 'Đến ngày', width: 80, align: 'center',formatter:CommonFormatter.dateTimeFormatter},		    
		    {field:'status', title: 'Trạng thái', width: 80, align: 'center',sortable:false,resizable:true,formatter:function(value, row, index){
		    		if(row.status == 'CREATED'){
		    			return 'Tạo mới';
		    		}else if(row.status == 'OPENED'){
		    			return 'Mở';
		    		}else if(row.status == 'CLOSED'){
		    			return 'Đóng';
		    		}
		    	return "";
		    }},
		    {field:'edit', title:HTML_CREATED, width: 20, align: 'center',formatter: function (value, row, index) {
		    	return (row.status == 'CREATED' || row.status == 'OPENED' || (row.status == 'CLOSED' && row.id == General.periodIdCloseMaxToDate))? format(HTML_UPDATED,row.id) : '';	 		    		
		    }}
	    ]],	    
	    onLoadSuccess :function(data){	    	
			$('.datagrid-header-rownumber').html('STT');			
			 Utils.updateRownumWidthAndHeightForDataGrid('grid');
			 $(window).resize();			 
		}
	});	
});
</script>
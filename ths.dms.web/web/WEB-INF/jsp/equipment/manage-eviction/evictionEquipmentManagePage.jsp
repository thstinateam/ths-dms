<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Thiết bị</a>
		</li>
		<li><span>Thu hồi và thanh lý</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
<!-- 				<h2 class="Title2Style">Thông tin tìm kiếm</h2> -->
				<h2 class="Title2Style">Thông tin tìm kiếm <span style="float:right;margin-right:15px;">
					<a href="javascript:void(0);" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
				<div class="SearchInSection SProduct1Form" style="padding-left: 0px;">
				<!-- <div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin tìm kiếm" style="width:1000px;height:300px;padding-top: 20px;"data-options="collapsible:true,closable:false,border:false"> -->
					<label class="LabelStyle Label3Style">Đơn vị </label>
					<input id="shop" type="text"  style="height: auto;" class="InputTextStyle InputText1Style"/>
<!-- 					<input  type="text" class="InputTextStyle InputText1Style" id="shopCode" maxlength="50" tabindex="1" /> -->
					<label class="LabelStyle Label3Style">Mã biên bản đề nghị thu hồi</label>
					<input id="equipSugEvictionCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
					<label class="LabelStyle Label1Style">Trạng thái biên bản</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="status" >
							<option value="-1" selected="selected">Tất cả</option>
							<option value="0" >Dự thảo</option>
							<option value="2" >Đã duyệt</option>
							<option value="4" >Hủy</option>
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label3Style">Ngày biên bản: Từ ngày</label>
					<div id="fromDateDiv">
						<input type="text" id="fDate" class="InputTextStyle InputText6Style" />
					</div>
					<label class="LabelStyle Label3Style">Đến ngày</label>
					<div id="toDateDiv">
						<input type="text" id="tDate" class="InputTextStyle InputText6Style" />
					</div>
					<label class="LabelStyle Label1Style">Trạng thái giao nhận</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="statusDelivery" >
							<option value="-1" selected="selected">Tất cả</option>
							<option value="0" >Chưa gửi</option>
							<option value="1" >Đã gửi</option>
							<option value="2" >Đã nhận</option>
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label3Style">Mã thiết bị</label>
					<input id="equipCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" />
					<label class="LabelStyle Label3Style">Số serial</label>
					<input id="seri" type="text" class="InputTextStyle InputText1Style" maxlength="100" />
					<div class="Clear"></div>
					<label class="LabelStyle Label3Style">Khách hàng</label>
					<input id="customerCode" type="text" class="InputTextStyle InputText3Style" style="width: 578px;" autocomplete="off" maxlength="300" placeholder="Nhập mã khách hàng hoặc tên khách hàng hoặc địa chỉ"/>
						
<!-- 					<label class="LabelStyle Label3Style" style=";text-align:left;padding-left: 10px;height:auto;">Mã KH</label> -->
<!-- 					<input id="customerCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"  /> -->
					
<!-- 					<label class="LabelStyle Label1Style">Tên KH</label> -->
<!-- 					<input id="customerName" type="text" class="InputTextStyle InputText1Style" maxlength="250" /> -->
					
<!-- 					<label class="LabelStyle Label1Style" >Địa chỉ</label> -->
<!-- 					<input id="customerAddr" type="text" class="InputTextStyle InputText1Style" maxlength="250" style="width: 313px;"  /> -->
					<div class="Clear"></div>
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol" onclick="EquipmentManageCatalog.searchEviction();">Tìm kiếm</button>
						</div>
						<div class="Clear"></div>
					</div>	
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
				<!-- </div>		 -->
				</div>	
				<!-- danh sach bien ban giao nhan -->	
				<h2 class="Title2Style" style="height:15px;">
					<span style="float:left;">Danh sách biên bản</span>
					<div style="float:right; margin-top: -5px;margin-right: 27px;">
						<a class="cmsiscontrol" id="btnPrint" href="javascript:void(0);" onclick="EquipmentManageCatalog.printBBTHTL();" ><img src="/resources/images/icon-printer.png" height="20" width="20" title="In biên bản"></a>
						<a class="cmsiscontrol" id="btnImportDownloadTemplate" href="javascript:void(0);" onclick="EquipmentManageCatalog.downloadTemplate();"><img src="/resources/images/icon_download_32.png" height="20" width="20" title="Tải mẫu Nhập Excel"></a>
						<a class="cmsiscontrol" id="btnImportShowDlg" href="javascript:void(0);" onclick="EquipmentManageCatalog.showDlgImportExcel();"><img src="/resources/images/icon-import.png" height="25" width="25" title="Nhập Excel"></a>
						<a class="cmsiscontrol" id="btnExport" href="javascript:void(0);" onclick="EquipmentManageCatalog.exportExcelTHTL();"><img src="/resources/images/icon-export.png" height="25" width="25" title="Xuất Excel"></a>					
						
						<div style="float:right;">
							<div id="btnSaveDeliveryStatus" style="margin-right: 5px;" class="BoxSelect BoxSelect11 cmsiscontrol">
								<select class="MySelectBoxClass" id="statusDeliveryLabel" style="width:112px;">
									<option value="0" selected="selected">Chọn</option>
									<option value="1" >Đã gửi</option>
									<option value="2" >Đã nhận</option>
								</select>
							</div>
							<div id="btnSaveRecordStatus" style="margin-right: 5px;" class="BoxSelect BoxSelect11 cmsiscontrol">							
								<select class="MySelectBoxClass" id="statusRecordLabel" style="width:112px;" onchange="changeCombo();">
									<option value="-1" selected="selected">Chọn</option>
									<option value="2" >Duyệt</option>
									<option value="4" >Hủy</option>
								</select>
							</div>
							<button style="" id="btnSaveLst" class="BtnGeneralStyle" onclick="EquipmentManageCatalog.updateStatus();" >Lưu</button>
						</div>
					</div>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="grid"></table>
					</div>
				</div>							
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup lich su giao nhan -->
<div style="display: none">
	<div id="easyuiPopupHistory" class="easyui-dialog" title="Lịch sử giao nhận" data-options="closed:true,modal:true"  style="width:400px;height:215px;">
		<div class="PopupContentMid">
	   		<div class="SearchInSection SProduct1Form">
				<div class="GridSection" id="gridHitoryDiv">
					<table id="gridHistory"></table>
				</div>
			</div>        
		</div>
	</div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Import - Biên bản thu hồi và thanh lý" data-options="closed:true,modal:true" style="width:465px;height:200px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-eviction-manage/importExcel" name="importFrmBBGN" id="importFrmBBGN" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">
						<input id="fakefilepcBBGN" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileBBGN" onchange="previewImportExcelFile(this,'importFrmBBGN','fakefilepcBBGN', 'errExcelMsgBBGN');" />				
					</div>
				</form>
				<div class="Clear"></div>
				<p class="">
					<a onclick="EquipmentManageCatalog.downloadTemplate();" class="Sprite2" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle cmsiscontrol" id="btnImport" onclick="return EquipmentManageCatalog.importExcelBBTHTL();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgBBGN"/>
	</div>	
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {	
	$('#panelDeliveryRecord').width($('#panelDeliveryRecord').parent().width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 5px;");
	},1000);
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar("fDate");
	ReportUtils.setCurrentDateForCalendar("tDate");	
	$('#shopCode').focus();
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper"); 
	// $('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_import_bien_ban_thu_hoi_thanh_ly.xlsx');
	var params = new Object();
	EquipmentManageCatalog._mapEviction = new Map();
	EquipmentManageCatalog._lstRowID = new Array();
	params.fromDate = $('#fDate').val();
	params.toDate = $('#tDate').val();	
	
	params.equipCode = $('#equipCode').val().trim();
	params.seri = $('#seri').val().trim();
	params.customerCode = $('#customerCode').val().trim();
	params.statusForm = $('#status').val().trim();
	params.statusAction = $('#statusDelivery').val().trim();
	params.equipSugEvictionCode = $('#equipSugEvictionCode').val().trim();
	$('#grid').datagrid({
		url:'/equipment-eviction-manage/searchEviction',
		method : 'GET',
		height: 'auto',
		width : $(window).width() - 35,
		scrollbarSize:0,
		rownumbers:true,
		// fitColumns:true,
		autoRowHeight:true, 
		pagination:true, 
		pageSize:10,
		queryParams:params,
		pageList  : [20],
// 		width: $('#gridContainer').width(),
		frozenColumns:[[
			{field: 'check', checkbox: true, width: 50, sortable: false, resizable: false, align: 'left'
			}, 
			{field: 'id', title: '<a id="btnNew" class="cmsiscontrol" href="/equipment-eviction-manage/change-record"><img src="/resources/images/icon_add.png"/></a>', width: 50, align: 'center', formatter: function(value, row, index) {
				var html = '';
				if (row.trangThaiBienBan == DA_DUYET) {
					html += '<a id="btnPrint_' + row.id + '" class="cmsiscontrol" href="javascript:void(0);" onclick="EquipmentManageCatalog.printBBTHTL(' + row.id + ');"><img src="/resources/images/icon_print_view_34.png" height="20" width="20" style="margin-right:5px;" title="Xem biên bản"></a>';
				}
				if (row.trangThaiBienBan == DU_THAO) {
					html += '<a id="btnEdit_' + row.id + '" class="cmsiscontrol" href="/equipment-eviction-manage/change-record?id=' + row.id + '"><img src="/resources/images/icon-edit.png"></a>';
				}
				return html;
			}},
			{field: 'ngay', title: 'Ngày biên bản', width: 90, sortable: false, resizable: false, align: 'center', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}}, 
			{field: 'maPhieu', title: 'Mã phiếu', width: 100, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}}, 
			{field: 'maNPP', title: 'Đơn vị', width: 150, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(row.maNPP + " - " + row.tenNPP);
			}}, 
			{field: 'maKH', title: 'Mã KH', width: 70, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}}, 
			{field: 'tenKH', title: 'Tên KH', width: 150, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
		]],
		columns:[[
			{field: 'diaChi', title: 'Địa chỉ', width: 250, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}}, 
			{field: 'trangThaiBienBan', title: 'Trạng thái biên bản', width: 100, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}}, 
			{field: 'equipSugEvictionCode', title: 'Mã biên bản đề nghị thu hồi', width: 180, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}}, 
			{field: 'trangThaiGiaoNhan', title: 'Trạng thái giao nhận', width: 100, align: 'left', formatter: function(value, row, index) {
				return '<a href="javascript:void(0);" onclick="EquipmentManageCatalog.getHistory(' + row.id + ');">' + Utils.XSSEncode(value) + '</a>';
			}}, 
			{field: 'numberFile', title: 'Tập tin đính kèm', width: 100, align: 'left', formatter: function(value, row, index) {
				if (value > 0) {
					return '<a href="javascript:void(0)" onclick="General.downloadEquipAttechFile(' + row.id + ', 5);">Tập tin</a>';
				}
			}},
		    {field: 'note', title: 'Ghi chú', width: 250, align: 'left', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
		    }},
		]],
		onBeforeLoad:function(param){											 
		},
		onLoadSuccess :function(data){
	    	if (data != null && data.total > 0) {
				$('.datagrid-header-rownumber').html('STT');		    	
			}		
	    	$('.datagrid-header-row td div').css('text-align','center');
	    	updateRownumWidthForDataGrid('');
			var rows = $('#grid').datagrid('getRows');
	    	$('.datagrid-header-check input').removeAttr('checked');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(EquipmentManageCatalog._mapEviction.get(rows[i].id)!=null) {
	    			$('#grid').datagrid('checkRow', i);
	    		}
	    	}
	    	Utils.functionAccessFillControl('gridContainer', function(data){
			});
    		$(window).resize();
		},
		onCheck:function(index,row){
			EquipmentManageCatalog._mapEviction.put(row.id,row);  
			if(row.trangThaiBienBan == "Đã duyệt"){
				EquipmentManageCatalog._lstRowID.push(row.id);
			}
			
	    },
	    onUncheck:function(index,row){
	    	EquipmentManageCatalog._mapEviction.remove(row.id);
			var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
	    	if(position != null){
	    		EquipmentManageCatalog._lstRowID.splice(position,1);
	    	}
	    },
	    onCheckAll:function(rows){
		 	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		EquipmentManageCatalog._mapEviction.put(row.id,row);
	    		var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
				if(position == -1 && row.trangThaiBienBan == "Đã duyệt"){
					EquipmentManageCatalog._lstRowID.push(row.id);
				}
	    	}
	    },
	    onUncheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		EquipmentManageCatalog._mapEviction.remove(row.id);	
	    		var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
				if(position != null){
					EquipmentManageCatalog._lstRowID.splice(position,1);
				}
	    	}
	    }
	});
});
function changeCombo() {
    var status = $('#statusRecordLabel').val().trim();
    var html = '';
    if(status == '2'||status == '-1'){
    	html = 
    	'<option value="0" selected="selected">Chọn</option> <option value="1" >Đã gửi</option> <option value="2" >Đã nhận</option> ';
    }else{
    	html = '<option value="0" selected="selected">Chọn</option>';
    }
    $('#statusDeliveryLabel').html(html);
    $('#statusDeliveryLabel').change();
}
</script>
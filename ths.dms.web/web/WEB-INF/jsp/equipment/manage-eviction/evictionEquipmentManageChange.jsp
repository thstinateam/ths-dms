<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipment-eviction-manage/info">Thiết bị</a>
		</li>
		<li><span>Lập biên bản thu hồi thanh lý</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style" style="width: 100px;text-align:left;padding-left: 5%;height:auto;">Mã NPP (F9)<span class="ReqiureStyle"> *</span></label>
					<input id="shopCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" value='<s:property value="equipEvictionForm.shop.shopCode"/>'/>
					<label class="LabelStyle Label1Style">Mã biên bản đề nghị thu hồi</label>
					<input id="equipSugEvictionCode" type="text" class="InputTextStyle InputText1Style" maxlength="250" value='<s:property value="equipEvictionForm.equipSugEviction.code"/>'/>
					<label class="LabelStyle Label1Style">Ngày biên bản<span class="ReqiureStyle"> *</span></label>
					<div id="createDateDiv">
						<input type="text" id="createDate" class="InputTextStyle InputText6Style" />
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style" style="width: 100px;text-align:left;padding-left: 5%;height:auto;">Mã KH (F9)<span class="ReqiureStyle"> *</span></label>
					<input id="customerCode" type="text" class="InputTextStyle InputText1Style"  maxlength="50" value='<s:property value="equipEvictionForm.customer.shortCode"/>'/>
					<label class="LabelStyle Label1Style">Tên KH</label>
					<input id="customerName" type="text" class="InputTextStyle InputText1Style" maxlength="250"  value='<s:property value="equipEvictionForm.customer.customerName"/>'/>
					<label class="LabelStyle Label1Style">Người đại diện<span class="ReqiureStyle"> *</span></label>
					<input id="fromRePresentor" type="text" class="InputTextStyle InputText1Style" maxlength="250" value='<s:property value="equipEvictionForm.fromRepresentor"/>'/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style" style="width: 100px;text-align:left;padding-left: 5%;height:auto;">Điện thoại</label>
					<input type="text" id="phone" class="InputTextStyle InputText1Style"  value='<s:property value="equipEvictionForm.customer.phone"/>'/>
					
					<label class="LabelStyle Label1Style">Địa chỉ</label>
					<input type="text" id="address" class="InputTextStyle InputText3Style" style="width:545px" value='<s:property value="equipEvictionForm.customer.address"/>'/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style" style="width: 100px;text-align:left;padding-left: 5%;height:auto;">Bên cho mượn<span class="ReqiureStyle"> *</span></label>
					<input id="toRePresentor" type="text" class="InputTextStyle InputText1Style" maxlength="250" value='<s:property value="equipEvictionForm.toRepresentator"/>'/>
					<label class="LabelStyle Label1Style">Kho nhập thu hồi (F9)<span class="ReqiureStyle"> *</span></label>
					<input id="warehouse" type="text" class="InputTextStyle InputText1Style"  maxlength="50" value='<s:property value="stockCode"/>'/>
					<label class="LabelStyle Label1Style">Lý do thu hồi</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass"  id="reason">
							<!-- <option value="0" selected="selected">Điều chuyển khách hàng khác</option>
							<option value="1" >Hư hỏng</option>
							<option value="2" >Mất tủ</option>
							<option value="3" >Khác</option> -->
							<s:iterator value="lstReasonEviction" >
								<option value="<s:property value='apParamEquipCode' />"><s:property value="value" /></option>
							</s:iterator>
						</select>
					</div>
					<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
				</div>		
				<!-- danh sach thiet bi -->	
				<h2 class="Title2Style"><span>Thông tin thiết bị</span></h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="grid"></table>
					</div>
				</div>	
				<h2 class="Title2Style">Thông tin khác</h2>
				<div class="SearchInSection SProduct1Form">
					<!-- <label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Bên cho mượn<span class="ReqiureStyle"> *</span></label>
					<input id="toRePresentor" type="text" class="InputTextStyle InputText1Style" maxlength="250" value='<s:property value="equipEvictionForm.toRepresentator"/>'/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Kho nhập thu hồi (F9)<span class="ReqiureStyle"> *</span></label>
					<input id="warehouse" type="text" class="InputTextStyle InputText1Style"  maxlength="50" value='<s:property value="stockCode"/>'/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Lý do thu hồi</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass"  id="reason">
							<s:iterator value="lstReasonEviction" >
								<option value="<s:property value='apParamEquipCode' />"><s:property value="value" /></option>
							</s:iterator>
						</select>
					</div>
					
					<div class="Clear"></div> 
					<label class="LabelStyle Label1Style" style="text-align:left;padding-left: 10px;height:auto;">Vị trí thiết bị hiện tại<span class="ReqiureStyle"> *</span></label>
					<input type="radio" id="currentPositionNPP"  name="currentPosition" value="1">Nhà phân phối
					<input type="radio" id="currentPositionKH"  name="currentPosition" value="2">Điểm bán<br>
					<input type="radio" id="currentPositionKhac"  name="currentPosition" value="3"  checked="checked">Chờ chuyển khách hàng khác<br>
					<div class="Clear"></div>
						<label class="LabelStyle Label1Style"  style="margin-left:100px">Mã NPP (F9)</label>
						<input id="newShopCode" type="text"  class="InputTextStyle InputText1Style" maxlength="50" />
						<label class="LabelStyle Label1Style">Mã KH (F9)</label>
						<input id="newCustomerCode"  type="text" class="InputTextStyle InputText1Style" maxlength="50" />
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style" style="margin-left:100px">Địa chỉ mới</label>
						<input type="text" id="newAddress"  class="InputTextStyle InputText3Style" style="width:544px "/>
					<div class="Clear"></div>-->
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Trạng thái biên bản</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass"  id="status" onchange="changeCombo();">
							<option value="0" selected="selected">Dự thảo</option>
							<option value="2" >Duyệt</option>
<!-- 							<option value="0" >Hủy</option> -->
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Trạng thái giao nhận</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass"  id="statusDelivery" >
							<option value="0">Chưa gửi</option>
						</select>
					</div>
					<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
				</div>
				<h2 class="Title2Style">Khác</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Ghi chú</label>
<!-- 					<s:if test="freezer == null">
						<input id="note" type="text" class="easyui-textbox" maxlength="500" style="width: 570px; height: 100px;" data-options="multiline:true"/>
					</s:if>
					<s:else>
						<input id="note" type="text" class="easyui-textbox" maxlength="500"
						value="<s:property value="freezer"/>" style="width: 570px; height: 100px;" data-options="multiline:true"/>
					</s:else>		 -->
					<s:if test="equipEvictionForm.note == null">
						<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 570px; height: 100px; font-size: 13px;" maxlength="500" ></textarea>	
					</s:if>
					<s:else>
						<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 570px; height: 100px; font-size: 13px;" maxlength="500"><s:property value="equipEvictionForm.note"/></textarea>
					</s:else>

				</div>
				<div class="Clear"></div>	
				<h2 class="Title2Style">Tập tin đính kèm</h2>
				<div class="SearchInSection SProduct1Form">
					<a href="javascript:void(0);" style="float: left;" title="Lựa chọn tập tin" ><img class="addFileBtn" src="/resources/images/icon_attach_file.png"></a>
					<div class="fileupload-process" style="width: 90%;">
						<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-striped active" id="total-progress" style="opacity: 0; width: 550px;">
							<div data-dz-uploadprogress="" style="width: 200px;" class="progress-bar progress-bar-success"></div>
						</div>
					</div>
					<div style="float:left; min-width: 500px; width: 90%;">
						<div style="height: auto; margin-left:20px;">
							<s:iterator id="fileVo" value="lstFileVo">
								<div class="divImageUploadFile" style="float:none; height: auto;" id = "divEquipAttachFile<s:property value="#fileVo.fileId"/>">
									<!-- <div class="imageUpdateShowIcon">
										<img class="addFileBtn" src="/resources/images/img-folder-001.png" width="50" height="50">
									</div> -->
									<div class="imageUpdateShowContent" style="float:none; height: auto;  width: 500px;" >
										<label><s:property value="#fileVo.fileName"/></label>
										<a href="javascript:void(0)" onclick="EquipmentManageCatalog.removeEquipAttachFile(<s:property value="#fileVo.fileId"/>);"><img title="Xóa tập tin" src="/resources/images/icon_delete.png" width="12" height="12" style="padding-left: 5px;"></a>
									</div>
								</div>
							</s:iterator>
						</div>
						<div class="Clear"></div>
						<div class="table table-striped" class="files" id="previews" style="min-height: 100px; margin-left:20px;">
							<div id="template" class="file-row" style="margin-bottom: 10px; width: 260px;float: left">
								<div style="margin-right: 10px; width: 50px; float: left">
									<!-- <span class="preview"><img data-dz-thumbnail /></span> -->
								</div>
								<div style="display: inline-block; width: 200px; clear: both">
									<div style="padding-right: 10px">
										<div>
											<p class="name" style="overflow: hidden" data-dz-name></p>
											<strong class="error text-danger" data-dz-errormessage></strong>
										</div>
										<div style="">
											<p class="size" data-dz-size></p>
											</div>
										<div>
											<a data-dz-remove href="javascript:void(0)"><span><img border="0" src="/resources/scripts/plugins/uploadify2.1/cancel.png"></span></a>
										</div>
									</div>								
								</div>							
							</div> 
						</div>
					</div>
					<div class="Clear"></div>
				</div>
<!-- 				<h2 class="Title2Style">Tập tin đính kèm</h2> -->
<!-- 					<div class="SearchInSection SProduct1Form"> -->
<!-- 						<a href="javascript:void(0);" onclick=""><img src="/resources/images/icon_attach_file.png"></a> -->
<!-- 					</div> -->
				<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnUpdate" class="BtnGeneralStyle cmsiscontrol" onclick="EquipmentManageCatalog.saveEviction();">Cập nhật</button>						
							<button id="btnClose" class="BtnGeneralStyle" onclick="location.href = '/equipment-eviction-manage/info';">Bỏ qua</button>
						</div>
						<div class="Clear"></div>
					</div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup chon kho -->
<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchStock" class="easyui-dialog"
		title="Chọn kho"
		data-options="closed:true,modal:true">
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label1Style">Mã đơn vị</label>
				<input id="shopCodePopupStock" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label1Style">Tên đơn vị</label>
				<input id="shopNamePopupStock" type="text" class="InputTextStyle InputText4Style" maxlength="100"/>				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="EquipmentManagerDelivery.searchStock();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="stockGridDialogContainer">
					<table id="stockGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchStock').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<s:hidden id="idForm" ></s:hidden>
<div><input type="hidden" id="formCreateDate" value="<s:property value="createDate"/>"></div>
<div><input type="hidden" id="equipSugEvictionCode" value="<s:property value="equipSugEvictionCode"/>"></div>
<script type="text/javascript">
$(document).ready(function() {	
	disabled('address');
	disabled('newAddress');
	disabled('phone');
	disabled('customerName');
	disabled('equipSugEvictionCode');
	$("#reason").val('<s:property value="equipEvictionForm.reason"/>').change();
	
	setDateTimePicker('createDate');
	var createFormDate = $('#formCreateDate').val();
	if (createFormDate != undefined && createFormDate != null && createFormDate != '') {
		$('#createDate').val(createFormDate);
	} else {
		ReportUtils.setCurrentDateForCalendar("createDate");
	}
	

	if ($('#equipSugEvictionCode').val() != '') {
		disabled('fromRePresentor');
	}
	var isEdit = '<s:property value="isEdit"/>';
	EquipmentManageCatalog._countFile = Number('<s:property value="lstFileVo.size()"/>');
	
	if(isEdit!=null && isEdit !='' && isEdit != '0'){
	    var	html = 
	    	'<option value="0" selected="selected">Dự thảo</option> <option value="2" >Duyệt</option> <option value="4" >Hủy</option>';
	    $('#status').html(html);
	    $('#status').change();
		disabled('shopCode');
		disabled('customerCode');
		var viTri = '<s:property value="viTri"/>';
		// if(viTri!=null){
		// 	if(viTri=='3'){
		// 		var newShopCode = '<s:property value="newShopCode"/>';
		// 		var newCustomerCode = '<s:property value="newCustomerCode"/>';
		// 		var newAddress = '<s:property value="newAddress"/>';
		// 		$("#currentPositionKhac").attr("checked",true);
		// 		$('#newCustomerCode').val(newCustomerCode);
		// 		$('#newShopCode').val(newShopCode);
		// 		$('#newAddress').val(newAddress);
		// 	}else if(viTri=='1'){
		// 		$("#currentPositionNPP").attr("checked",true);
		// 		disabled('newCustomerCode');
		// 		disabled('newShopCode');
		// 		$('#newCustomerCode').val('');
		// 		$('#newShopCode').val('');
		// 		$('#newAddress').val('');
		// 	}else if(viTri=='2'){
		// 		$("#currentPositionKH").attr("checked",true);
		// 		disabled('newCustomerCode');
		// 		disabled('newShopCode');
		// 		$('#newCustomerCode').val('');
		// 		$('#newShopCode').val('');
		// 		$('#newAddress').val('');
		// 	}
		// }
		var idForm = '<s:property value="idForm"/>';
		$('#idForm').val(idForm);
		EquipmentManageCatalog.eventselectCustomer($('#customerCode').val(),idForm);
	}else{
		enable('shopCode');
		enable('customerCode');
		$('#grid').datagrid({
			data: [],
			width :  $('#gridContainer').width() - 15,
			fitColumns : true,
		    rownumbers : true,
		    singleSelect:true,
		    height: 275,
		    columns:[[
			    {field:'soHopDong', title:'Số hợp đồng', width:120, align:'left', formatter: function(value, row, index){
			    	var prdName = 'dgSoHopDong'+ row.id;
			    	var html = '';
			    	if(row.soHopDong!=null){
			    		html = '<label id="'+prdName+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
			    	}
			    	return html;
			    }},
			    {field:'loaiThietBi',title:'Loại thiết bị', width:80,align:'left', formatter: function(value, row, index){
			    	var idLot = "dgLoaiThietBi"+ row.id;
			    	return '<label id="'+idLot+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
			    }},
			    {field:'nhomThietBi', title:'Nhóm thiết bị', width:80, align:'left', formatter: function(value, row, index){
			    	var idAvlQ = "dgNhomThietBi"+ row.id;
			    	return '<label id="'+idAvlQ+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
			    }},
			    {field:'maThietBi',title:'Mã thiết bị', width:120,align:'left',formatter: function(value, row, index){
					return Utils.XSSEncode(value);
				}
				, editor: {
					type:'combobox',
						options:{ 
						valueField:'id',
					    textField:'maThietBi',
					    data: EquipmentManageCatalog._lstEquipAvailable,
					    width: 200,
					    formatter: function(row) {
					    	return '<span style="font-weight:bold">' + row.maThietBi  + '</span>';
					    },
					    onSelect:function(rec){
					    	EquipmentManageCatalog._rowDgTmp = {
				    			id: rec.id,
								maThietBi: rec.maThietBi,
								soHopDong: rec.soHopDong,
								loaiThietBi: rec.loaiThietBi,
								nhomThietBi: rec.nhomThietBi,
								soSeri: rec.soSeri,
								soLuong: rec.soLuong,
								tinhTrangThietBi: rec.tinhTrangThietBi
					    	};
					    	$('#dgSoHopDong0').html(Utils.XSSEncode(rec.soHopDong.trim())).change();
					    	$('#dgLoaiThietBi0').html(Utils.XSSEncode(rec.loaiThietBi)).change();
					    	$('#dgNhomThietBi0').html(Utils.XSSEncode(rec.nhomThietBi.trim())).change();
					    	$('#dgSoSeri0').html(Utils.XSSEncode(rec.soSeri.trim())).change();
					    	$('#dgSoLuong0').html(Utils.XSSEncode(rec.soLuong)).change();
					    	$('#dgTinhTrangThietBi0').html(Utils.XSSEncode(rec.tinhTrangThietBi.trim())).change();
					    	
					    	EquipmentManageCatalog._mapDetail.put(EquipmentManageCatalog._rowDgTmp.id, EquipmentManageCatalog._rowDgTmp);
					    	$('#grid').datagrid("loadData", EquipmentManageCatalog.getDataInPrdStockOutputDg());
					    },
					    onChange:function(pCode){
					    	$('.combo-panel').each(function(){
								if(!$(this).is(':hidden')){
									$(this).css('display','none');
								}			
							});
					    },
					    filter: function(q, row){
							q = new String(q).toUpperCase();
							var opts = $(this).combobox('options');
							return row[opts.textField].indexOf(q)>=0;
						}
					}
				}},
			    {field:'soSeri', title:'Số serial', width:100, align:'left', formatter: function(value, row, index){
			    	var idQuan = "dgSoSeri"+ row.id;
			    	return '<label id="'+idQuan+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
			    }},
			    {field:'soLuong', title:'Số lượng', width:80, align:'right', formatter: function(value, row, index){
			    	var idPrice= "dgSoLuong"+ row.id;
			    	return '<label id="'+idPrice+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
			    }},
			    {field:'tinhTrangThietBi', title:'Tình trạng thiết bị', width:80, align:'left', formatter: function(value, row, index){
			    	var idAmount = "dgTinhTrangThietBi"+ row.id;
			    	return '<label id="'+idAmount+'" style="width: 100%; text-align: left;">'+Utils.XSSEncode(value)+'</label>';
			    }},
			    {field:'manufacturingYear', title:'Năm sản xuất', width:80, align:'left', formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'detelte',title:'',width:50, align:'center', formatter: function(value, row, index){
			    	if (row.id != undefined && row.id != null && row.id > 0 && ($('#equipSugEvictionCode').val() == undefined ||  $('#equipSugEvictionCode').val() == '')) { 
			    		return '<a href="javascript:void(0)" id="dg_prdStockOutputDg_delete" onclick="return EquipmentManageCatalog.deleteRowInPrdStockOutputDg('+row.id+');"><img title="Xóa" src="/resources/images/icon_delete.png" width="15" heigh="15"></a>';
			    	}
			    }}
			]],
			onClickRow: function(rowIndex, rowData) {
				
			},
			onAfterEdit: function(rowIndex, rowData, changes) {
			},
			onBeforeEdit : function(rowIndex, rowData, changes){
			},
	        onLoadSuccess :function(data){
		    	 $('.datagrid-header-rownumber').html('STT');
		    	 Utils.updateRownumWidthAndHeightForDataGrid('grid');
		    	 var arrInput = '';
		    	 EquipmentManageCatalog.getDataComboboxInPrdStockOutputDg();
		    	 var rows = $('#grid').datagrid('getRows');
		    	 if ($('#equipSugEvictionCode').val() != "") {
			    		$('#grid').datagrid("hideColumn", "detelte");
		    	 } else if(rows == null || rows.length == 0 || rows[rows.length-1].id != null){
		    		 EquipmentManageCatalog.insertRowStockTransDetailDg();
		    	 }
		    	 
		    	 if (EquipmentManageCatalog._mapDetail != null && EquipmentManageCatalog._mapDetail.size() > 0) {
		    	 } else {
		    		 EquipmentManageCatalog._mapDetail = new Map();
		    	 }
		    	$('.combobox-f.combo-f').combobox("loadData", EquipmentManageCatalog._lstEquipAvailable);
		    	if ($('#equipSugEvictionCode').val() != "") {
		    		$('#grid').datagrid("hideColumn", "detelte");
		    	}
	    	}
		});
	}
	$('#shopCode').focus();
	
	$('#shopCode').bind('keyup', function(e) {
		if (e.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2({
				params : {status : activeType.ALL},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Đơn vị'},
			        {id:'name', maxlength:250, label:'Tên Đơn vị'},
			    ],
			    url : '/commons/search-shop-show-list-NPP-GT-MT',
			    columns : [[
			        {field:'shopCode', title:'Mã Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
			        {field:'shopName', title:'Tên Đơn vị', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
				    }},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			            var html = '<a href="javascript:void(0)" onclick="EquipmentManageCatalog.selectShop({0},\'{1}\',\'{2}\')" >Chọn</a>';
			            return format(html, row.shopId, row.shopCode, row.shopName);
			        }}
			    ]]
			});				
			return false;
		}
	});	
	$('#shopCode').bind('blur', function () {
		if (EquipmentManageCatalog._curShopCode != $('#shopCode').val()) {
			EquipmentManageCatalog.selectShop(null, $('#shopCode').val(), null);
		}

    });


	// $('#newShopCode').bind('keyup', function(e){
	// 	if(e.keyCode==keyCode_F9){
	// 		VCommonJS.showDialogSearch2({
	// 			//params : {},
	// 			inputs : [
	// 		        {id:'code', maxlength:50, label:'Mã Đơn vị'},
	// 		        {id:'name', maxlength:250, label:'Tên Đơn vị'},
	// 		    ],
	// 		    url : '/commons/search-shop-show-list-NPP',
	// 		    columns : [[
	// 		        {field:'shopCode', title:'Mã Đơn vị', align:'left', width: 110, sortable:false, resizable:false},
	// 		        {field:'shopName', title:'Tên Đơn vị', align:'left', width: 200, sortable:false, resizable:false},
	// 		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
	// 		            var html =  '<a href="javascript:void(0)" onclick="EquipmentManageCatalog.selectNewShop({0},\'{1}\',\'{2}\')" >Chọn</a>';
	// 		            return format(html,row.id,row.shopCode,row.shopName);
	// 		        }}
	// 		    ]]
	// 		});				
	// 		return false;
	// 	}
	// });	
	EquipmentManageCatalog._isPressF9Customer = false;
	$('#customerCode').bind('keyup', function(event) {					
		if (event.keyCode == keyCode_F9) {
			$("#errMsg").html('').hide();
			if($('#shopCode').val().trim()== null || $('#shopCode').val().trim()==''){
				$("#errMsg").html('Bạn chưa chọn NPP trước khi chọn Khách hàng').show();
				return;
			}
			EquipmentManageCatalog._isPressF9Customer = true;
			$('#customerCode').val('');
			VCommonJS.showDialogSearch2({
				params : {
					shopCode: $('#shopCode').val().trim()
				},
			    inputs : [
			        {id:'code', maxlength:50, label:'Mã KH'},
			        {id:'name', maxlength:250, label:'Tên KH'},
			        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
			    ],			   
			    url : '/commons/customer-in-shop/filter',	   
			    columns : [[
			        {field:'shortCode', title:'Mã KH', align:'left', width: 60, sortable:false, resizable:false},
			        {field:'customerName', title:'Tên KH', align:'left', width: 140, sortable:false, resizable:false},
			        {field:'address', title:'Địa chỉ', align:'left', width: 150, sortable:false, resizable:false},
			        {field:'phone', title:'Điện thoại', align:'left', width: 80, sortable:false, resizable:false,formatter: function(value, row, index){
						if (row.phone != null) {
							return Utils.XSSEncode(row.phone);
						} else {
							return Utils.XSSEncode(row.mobiphone);
						}
		    		}},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	var address = '';
			        	var phone = '';
			        	if(row.address!=null){
			        		address = row.address;
			        	}
			        	if(row.phone!=null){
			        		phone = row.phone;
			        	} else {
			        		phone = row.mobiphone;
			        	}
			        	return "<a href='javascript:void(0)' onclick=\"return EquipmentManageCatalog.selectCustomer('"+ row.shortCode + "','"+row.customerName+ "','"+address+ "','"+phone+"');\">Chọn</a>";        
			        }}
			    ]]
			});
		} 
	});	
	
	// $('#newCustomerCode').bind('keyup', function(event) {					
	// 	if (event.keyCode == keyCode_F9) {
	// 		$("#errMsg").html('').hide();
	// 		if($('#shopCode').val().trim()== null || $('#newShopCode').val().trim()==''){
	// 			$("#errMsg").html('Bạn chưa chọn NPP trước khi chọn Khách hàng').show();
	// 			return;
	// 		}
	// 		VCommonJS.showDialogSearch2({
	// 			params : {
	// 				shopCode: $('#newShopCode').val().trim()
	// 			},
	// 		    inputs : [
	// 		        {id:'code', maxlength:50, label:'Mã KH'},
	// 		        {id:'name', maxlength:250, label:'Tên KH'},
	// 		        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
	// 		    ],			   
	// 		    url : '/commons/search-Customer-Show-List',			   
	// 		    columns : [[
	// 		        {field:'shortCode', title:'Mã KH', align:'left', width: 60, sortable:false, resizable:false},
	// 		        {field:'customerName', title:'Tên KH', align:'left', width: 140, sortable:false, resizable:false},
	// 		        {field:'address', title:'Địa chỉ', align:'left', width: 150, sortable:false, resizable:false},
	// 		        {field:'phone', title:'Điện thoại', align:'left', width: 80, sortable:false, resizable:false},
	// 		        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
	// 		        	var address = '';
	// 		        	var phone = '';
	// 		        	if(row.address!=null){
	// 		        		address = row.address;
	// 		        	}
	// 		        	if(row.phone!=null){
	// 		        		phone = row.phone;
	// 		        	}
	// 		        	return "<a href='javascript:void(0)' onclick=\"return EquipmentManageCatalog.selectNewCustomer('"+ row.shortCode + "','"+row.customerName+ "','"+address+ "','"+phone+"');\">Chọn</a>";        
	// 		        }}
	// 		    ]]
	// 		});
	// 	} 
	// });	
	$('#warehouse').bind('keyup', function(e){
		if(e.keyCode==keyCode_F9){
			// $('#easyuiPopupSearchStock').show();
			// $('#easyuiPopupSearchStock').dialog({  
		 //        closed: false,  
		 //        cache: false,  
		 //        modal: true,
		 //        width : 600,
		 //        height : 'auto',
		 //        onOpen: function(){	
		 //        	$('#shopCodePopupStock').focus();
			// 		$('#shopCodePopupStock, #shopNamePopupStock').bind('keyup',function(event){
			// 			if(event.keyCode == keyCodes.ENTER){
			// 				$('#btnSeachEquipmentDlg').click(); 
			// 			}
			// 		});
			// 		$('#stockGridDialog').datagrid({
			// 			// url : '/commons/search-shop-show-list-NPP',
			// 			url : '/equipment-stock-change-manage/get-list-equip-stock-vo',
			// 			autoRowHeight : true,
			// 			rownumbers : true, 
			// 			fitColumns:true,
			// 			scrollbarSize:0,
			// 			pagination:true,
			// 			pageSize :20,
			// 			pageList: [10],
			// 			width: $('#stockGridDialogContainer').width(),
			// 			height: 'auto',
			// 			autoWidth: true,	
			// 			queryParams:{
			// 				status : 1,
			// 				isUnionShopRoot: true,
			// 				code: $('#shopCodePopupStock').val().trim(),
			// 				name: $('#shopNamePopupStock').val().trim()
			// 			},
			// 			columns:[[
			// 				{field:'shopName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false},
			// 				{field:'code', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
			// 				{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			// 					var html =  "<a href='javascript:void(0)' onclick=\"return EquipmentManageCatalog.selectStock('"+ row.code + "');\">Chọn</a>";
			// 					return html;
			// 				}}				
			// 			]],
			// 			onBeforeLoad:function(param){											 
			// 			},
			// 			onLoadSuccess :function(data){   			 
			// 				if (data == null || data.total == 0) {
			// 					//$("#errMsgInfo").html("Không tìm thấy danh sách biên bản giao nhận thiết bị!").show();
			// 				}else{
			// 					$('.datagrid-header-rownumber').html('STT');		    	
			// 				}	
			// 				$('#shopCodePopupStock').focus();
			// 			}
			// 		});
		 //        },
			// 	onClose: function(){
			// 		$('#easyuiPopupSearchStock').hide();
			// 		$('#shopCodePopupStock').val('').change();
			// 		$('#shopNamePopupStock').val('').change();
			// 		$('#warehouse').focus();
			// 	}
			// });	
			var txt = "warehouse";
			$('#common-dialog-search-2-textbox').dialog("close");
			$('#common-dialog-search-2-textbox').remove();
			VCommonJS.showDialogSearch2({
				inputs : [
			        {id:'code', maxlength:50, label:'Mã đơn vị'},
			        {id:'name', maxlength:250, label:'Tên đơn vị'},
			    ],
			    url : '/commons/get-equipment-stock',
			    isCenter : true,
			    columns : [[
			        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	var nameCode = row.shopCode + '-' + row.shopName;
			        	return Utils.XSSEncode(nameCode);         
			        }},
			        {field:'equipStockCode', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
	//			        	var nameCode = row.shopCode + '-' + row.name;
			            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.equipStockId +', \''+Utils.XSSEncode(row.equipStockCode)+'\', \''+Utils.XSSEncode(row.name)+'\','+Utils.XSSEncode(Utils._isShowInputTextByDlSearch2.code)+');">chọn</a>';         
			        }}
			    ]]
			});
		}
	});

	// $('input:radio[name=currentPosition]').change(function(){
	// 	if($("input[name='currentPosition']:checked").val() == '3'){
	// 		enable('newCustomerCode');
	// 		enable('newShopCode');
	// 	}else{
	// 		disabled('newCustomerCode');
	// 		disabled('newShopCode');
	// 		$('#newCustomerCode').val('');
	// 		$('#newShopCode').val('');
	// 		$('#newAddress').val('');
	// 	}
	// });

	EquipmentManageCatalog._customerCode = $('#customerCode').val().trim();
	$('#customerCode').bind('blur', function () {
		if (!EquipmentManageCatalog._isPressF9Customer) {
			var curCustomerCode = $('#customerCode').val().trim();
			if (curCustomerCode != EquipmentManageCatalog._customerCode) {
				$("#errMsg").html('').hide();
					if($('#shopCode').val().trim()== null || $('#shopCode').val().trim()==''){
						$("#errMsg").html('Bạn chưa chọn NPP trước khi chọn Khách hàng').show();
						return;
					}
					Utils.getJSONDataByAjax({
						shopCode: $('#shopCode').val().trim(),
						code: $('#customerCode').val().trim(),
						page: 1,
						rows: 10
					}, '/commons/get-equipment-stock', function(data) {
						if (data.rows != undefined && data.rows.length > 0) {
							EquipmentManageCatalog.selectCustomer(data.rows[0].shortCode, data.rows[0].customerName, data.rows[0].address, data.rows[0].phone);
						} else {
							EquipmentManageCatalog.selectCustomer(curCustomerCode, '', '', '');
						}
					}, null, null);
				}
			}
    });

	
	UploadUtil.initUploadFileByEquipment({
		url: '/equipment-eviction-manage/saveEviction',	
		elementSelector: 'body',
		elementId: 'body',
		clickableElement: '.addFileBtn',
		parallelUploads: 5
		//maxFilesize: 5
	}, function (data) {
		if (data.error != undefined && !data.error) {
			$("#successMsg").html("Lưu dữ liệu thành công").show();
			setTimeout(function(){
				window.location.href = '/equipment-eviction-manage/info';
			}, 1000);				
		}else{
			if(data.errMsg!=undefined && data.errMsg!= null && data.errMsg!=''){
				$('#errMsg').html(data.errMsg).show();
			}
		}
	});
});
function changeCombo() {
	var isEdit = '<s:property value="isEdit"/>';
    var status = $('#status').val().trim();
    var html = '';
    if(status == '2'){
    	html = 
    	'<option value="0" selected="selected">Chưa gửi</option> <option value="1" >Đã gửi</option>';
    	if(isEdit!=null && isEdit !='' && isEdit != '0'){
    		html = 
    	    	'<option value="0" selected="selected">Chưa gửi</option> <option value="1" >Đã gửi</option>';
    	}
    }else{
    	html = '<option value="0" selected="selected">Chưa gửi</option>';
    }
    $('#statusDelivery').html(html);
    $('#statusDelivery').change();
}
</script>
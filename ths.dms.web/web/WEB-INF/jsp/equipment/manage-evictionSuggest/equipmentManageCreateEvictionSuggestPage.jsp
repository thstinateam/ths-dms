<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a id="titleUrl" href="/equip-suggest-eviction/info"></a>
		</li>
		<li>
			<s:if test="id == null">
				<span>Lập đề nghị thu hồi thiết bị</span>
			</s:if>
			<s:else>
				<span id="titleEditPage"></span>
			</s:else>		
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung
					<span style="float:right;margin-right:15px;">
              			<a href="javascript:void(0);" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
       				</span>
				</h2>
				<div class="SearchInSection SProduct1Form" style="padding-left: 0px;">
					<label class="LabelLeftStyleM5 LabelLeft2Style">Mã biên bản</label>
					<input id="code" type="text" class="InputTextStyle InputText1Style" value="<s:property value="code"/>" maxlength="50" disabled="disabled"/>
					<label class="LabelLeftStyleM5 LabelLeft4Style">Trạng thái biên bản</label>
					<s:if test="id == null">
						<div class="BoxSelect BoxSelect2 BoxDisSelect">
							<select class="MySelectBoxClass" id="status" disabled="disabled">
								<option value="0" selected="selected">Dự thảo</option>
							</select>
						</div>		
					</s:if>
					<s:else>
						<s:if test="statusRecord == 0">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="status">
									<option value="0" selected="selected">Dự thảo</option>
									<option value="1" >Chờ duyệt</option>
									<option value="4" >Hủy</option>
								</select>
							</div>		
						</s:if>
						<s:elseif test="statusRecord == 3">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="status">
									<option value="3" selected="selected">Không duyệt</option>
									<option value="1" >Chờ duyệt</option>
									<option value="4" >Hủy</option>
								</select>
							</div>		
						</s:elseif>
						<s:elseif test="statusRecord == 1">
							<div class="BoxSelect BoxSelect2">
								<select class="MySelectBoxClass" id="status">
									<option value="1" selected="selected">Chờ duyệt</option>
									<option value="2" >Duyệt</option>
									<option value="3" >Không duyệt</option>
								</select>
							</div>		
						</s:elseif>
						<s:elseif test="statusRecord == 2">
							<div class="BoxSelect BoxSelect2 BoxDisSelect">
								<select class="MySelectBoxClass" id="status" disabled="disabled">
									<option value="2" selected="selected">Duyệt</option>
								</select>
							</div>
						</s:elseif>
						<s:elseif test="statusRecord == 4">
							<div class="BoxSelect BoxSelect2 BoxDisSelect">
								<select class="MySelectBoxClass" id="status" disabled="disabled">
									<option value="4" selected="selected">Hủy</option>
								</select>
							</div>
						</s:elseif>
						<s:elseif test="statusRecord == 6">
							<div class="BoxSelect BoxSelect2 BoxDisSelect">
								<select class="MySelectBoxClass" id="status" disabled="disabled">
									<option value="6" selected="selected">Hoàn tất</option>
								</select>
							</div>
						</s:elseif>
					</s:else>
					<label class="LabelStyle Label1Style">Ngày biên bản</label>
					<s:if test="statusRecord == 0">
						<div id="createFormDateDiv">
							<input type="text" id="createFormDate" class="InputTextStyle InputText6Style" value="<s:property value="createFormDate"/>" />
						</div>
					</s:if>
					<s:else>
						<div id="createFormDateDiv">
							<input type="text" id="createFormDate" class="InputTextStyle InputText6Style" value="<s:property value="createFormDate"/>" disabled="disabled"/>
						</div>
					</s:else>
					<div class="Clear"></div>	
					<label class="LabelLeftStyleM5 LabelLeft2Style">Ghi chú</label>
					<s:if test="statusRecord == 0">
						<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 45%; height: 45px; font-size: 13px;" maxlength="500"><s:property value="note"/></textarea>
					</s:if>
					<s:else>
						<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 45%; height: 45px; font-size: 13px;" maxlength="500" disabled="disabled"><s:property value="note"/></textarea>
					</s:else>
					<div class="Clear"></div>						
				</div>	
				<!-- danh sach bien ban -->	
				<h2 class="Title2Style" style="height:15px;">
					<span style="float:left;">Danh sách đề nghị</span>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridEquipSuggestEvictionChangeDiv">
						<table class="easyui-datagrid" id="gridEquipSuggestEvictionChange">
							<thead frozen="true">
								<tr>
									<s:if test="id == null || statusRecord == 0 || statusRecord == 3">
										<th data-options="field:'delete',width:35,
											formatter: equipCreateLendSuggest.datagridOption.column0.formatter"rowspan="2">											
											<a href="javascript:void(0);" onclick="EquipmentSuggestEviction.insertEquipmentInGrid();"><img src="/resources/images/icon_add.png" title="Thêm"/></a>											
										</th>
									</s:if>
									<!-- <th data-options="field:'shopCodeMien',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column1.formatter,
										editor: equipCreateLendSuggest.datagridOption.column1.editor" rowspan="2">Miền</th>
									<th data-options="field:'shopCodeKenh',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column2.formatter,
										editor: equipCreateLendSuggest.datagridOption.column2.editor" rowspan="2">Vùng</th> -->
									<th data-options="field:'shopName',width:200,
										formatter: equipCreateLendSuggest.datagridOption.column2.formatter,
										editor: equipCreateLendSuggest.datagridOption.column2.editor" rowspan="2">Đơn vị</th>
									<th data-options="field:'shopCode',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column3.formatter,
										editor: equipCreateLendSuggest.datagridOption.column3.editor" rowspan="2">NPP</th>
									<th data-options="field:'customerCode',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column4.formatter,
										editor: equipCreateLendSuggest.datagridOption.column4.editor" rowspan="2">Mã KH (F9)</th>
									<th data-options="field:'customerName',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column5.formatter,
										editor: equipCreateLendSuggest.datagridOption.column5.editor" rowspan="2">Tên khách hàng</th>
								</tr>
							</thead>
							<thead>
								<tr>
									<th data-options="field:'equipCode',width:200,
										formatter: equipCreateLendSuggest.datagridOption.column6.formatter,
										editor: equipCreateLendSuggest.datagridOption.column6.editor" rowspan="2">Mã thiết bị (F9)</th>
									<th data-options="field:'equipSeri',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column7.formatter,
										editor: equipCreateLendSuggest.datagridOption.column7.editor" rowspan="2">Số serial</th>
									<th data-options="field:'timeEviction',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column8.formatter,
										editor: equipCreateLendSuggest.datagridOption.column8.editor" rowspan="2">Thời gian đề nghị thu hồi</th>
									<th data-options="field:'stockType',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column9.formatter,
										editor: equipCreateLendSuggest.datagridOption.column9.editor" rowspan="2">Kho nhận</th>
									<th data-options="field:'reason',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column10.formatter,
										editor: equipCreateLendSuggest.datagridOption.column10.editor" rowspan="2">Lý do đề nghị thu hồi</th>
									<th data-options="field:'staffCode',width:200,
										formatter: equipCreateLendSuggest.datagridOption.column11.formatter,
										editor: equipCreateLendSuggest.datagridOption.column11.editor" rowspan="2">Giám sát</th>
									<th data-options="field:'phone',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column12.formatter,
										editor: equipCreateLendSuggest.datagridOption.column12.editor" rowspan="2">Điện thoại</th>
									<th data-options="field:'representative',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column13.formatter,
										editor: equipCreateLendSuggest.datagridOption.column13.editor" rowspan="2">Người đứng tên mượn thiết bị</th>
									<th data-options="field:'relation',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column14.formatter,
										editor: equipCreateLendSuggest.datagridOption.column14.editor" rowspan="2">Quan hệ người đứng tên mượn thiết bị với chủ cửa hàng/Doanh nghiệp</th>									
									<th data-options="field:null,width:100," colspan="6"><span style="font-size: 12px; font-weight: bold"> Địa chỉ đặt thiết bị</span></th>	
									<th data-options="field:'quantity',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column21.formatter,
										editor: equipCreateLendSuggest.datagridOption.column21.editor" rowspan="2">Số lượng</th>
									<th data-options="field:'equipGroup',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column22.formatter,
										editor: equipCreateLendSuggest.datagridOption.column22.editor" rowspan="2">Nhóm thiết bị</th>
									<th data-options="field:'healthStatus',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column23.formatter,
										editor: equipCreateLendSuggest.datagridOption.column23.editor" rowspan="2">Trình trạng thiết bị</th>
									
								</tr>
								<tr>
									<th data-options="field:'mobile',width:100,
											formatter: equipCreateLendSuggest.datagridOption.column15.formatter,
											editor: equipCreateLendSuggest.datagridOption.column15.editor">Điện thoại</th>
									<th data-options="field:'address',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column16.formatter,
										editor: equipCreateLendSuggest.datagridOption.column16.editor">Số nhà</th>
									<th data-options="field:'province',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column17.formatter,
										editor: equipCreateLendSuggest.datagridOption.column17.editor">Tỉnh/TP</th>
									<th data-options="field:'district',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column18.formatter,
										editor: equipCreateLendSuggest.datagridOption.column18.editor">Quận/Huyện</th>
									<th data-options="field:'ward',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column19.formatter,
										editor: equipCreateLendSuggest.datagridOption.column19.editor">Phường/Xã</th>
									<th data-options="field:'street',width:100,
										formatter: equipCreateLendSuggest.datagridOption.column20.formatter,
										editor: equipCreateLendSuggest.datagridOption.column20.editor">Tên đường/Phố/Thôn/Ấp</th>									
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="Clear"></div>     
				<div class="SearchInSection SProduct1Form">                       
	               	<div class="BtnCenterSection" style="padding-bottom: 10px;" id="divBtnEdit">
	               		<s:if test="statusRecord == 0 || statusRecord == 3 ">
				       		<button id="editRecord_btnCapNhat" class="BtnGeneralStyle cmsiscontrol" onclick="return EquipmentSuggestEviction.updateRecord();">Cập nhật</button> &nbsp;&nbsp;				       		
				       	</s:if>
				       	<s:elseif test="statusRecord == 1">
							<button id="editApproved_btnCapNhat" class="BtnGeneralStyle cmsiscontrol" onclick="return EquipmentSuggestEviction.updateRecord();">Cập nhật</button> &nbsp;&nbsp;	
						</s:elseif>
				       	<button class="BtnGeneralStyle" onclick="EquipmentSuggestEviction.returnSearchPage();">Bỏ qua</button>
		           	</div>
		           	<div class="Clear"></div> 
		           	<p id="errMsgChangeSuggestEviction" class="ErrorMsgStyle" style="display: none;"></p>
		           	<p id="successMsgChangeSuggestEviction" class="SuccessMsgStyle" style="display: none"></p>
	           	</div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="idEquipSuggestEviction" name="id"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {	
	setDateTimePicker('createFormDate');
	var statusRecordEquipLend = '<s:property value="statusRecord"/>';
	if(statusRecordEquipLend != StatusRecordsEquip.DRAFT){
		$('#createFormDate').next().unbind('click');
	}

	if(window.location.pathname == "/equip-suggest-eviction/edit"){
		$("#titleUrl").attr("href",'/equip-suggest-eviction/info');
		$("#titleUrl").html("Quản lý đề nghị thu hồi thiết bị");	
		if(statusRecordEquipLend == StatusRecordsEquip.WAITING_APPROVAL){
			$("#status").attr("disabled","disabled")
			$($("#status").parent()).addClass("BoxDisSelect");
		}	
	} else if(window.location.pathname == "/equip-suggest-eviction/change"){
		$("#titleUrl").attr("href",'/equip-suggest-eviction/approve');	
		$("#titleUrl").html("Phê duyệt đề nghị thu hồi thiết bị");	
	}

	Utils._functionCallback = function() {
		if ($("#divBtnEdit button").length < 2) {
			$("#titleEditPage").html("Xem chi tiết biên bản đề nghị thu hồi thiết bị");
		} else if ($("#titleEditPage").html() == "") {
			$("#titleEditPage").html("Chỉnh sửa đề nghị thu hồi thiết bị");
		}
	};

	$('#gridEquipSuggestEvictionChange').datagrid({
		url: '/equip-suggest-eviction/get-list-detail-in-record',
		queryParams: {
			id: $("#idEquipSuggestEviction").val() > 0 ? $("#idEquipSuggestEviction").val() : ""
		},
		width: $('#gridEquipSuggestEvictionChangeDiv').width(),		
		height: 'auto',
		scrollbarSize:0,
		rownumbers:true,
		autoRowHeight:true, 
		singleSelect: true,
		// pagination:true, 
		// pageSize:20,		
		onDblClickRow: function(index, row) {
			if (statusRecordEquipLend == StatusRecordsEquip.DRAFT || statusRecordEquipLend == StatusRecordsEquip.NO_APPROVAL) {
				$('.ErrorMsgStyle').html('').hide();
				if (EquipmentSuggestEviction._editIndex != null) {
					var msg = EquipmentSuggestEviction.validateEquipmentInGrid();
					if (msg.length > 0) {
						$('#errMsgChangeSuggestEviction').html(msg).show();
						return false;
					}
					// them dong moi
					var row = EquipmentSuggestEviction.getDetailEdit();
					$('#gridEquipSuggestEvictionChange').datagrid('updateRow', {
						index: EquipmentSuggestEviction._editIndex,
						row: row
					});
				}
				// debugger;
				EquipmentSuggestEviction._editIndex = index;
				$('#gridEquipSuggestEvictionChange').datagrid('beginEdit', EquipmentSuggestEviction._editIndex);
				row = $('#gridEquipSuggestEvictionChange').datagrid('getRows')[EquipmentSuggestEviction._editIndex];
				EquipmentSuggestEviction.addEditorInGridChangeLend();

				var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);
				$(ed[2].target).combobox('clear');
				$(ed[6].target).combobox('clear');
				if(row.customerCode != null && row.shopCode != null && row.customerId != null) {
					// EquipmentSuggestEviction.getLstEquip(row.customerCode, row.shopCode, row.customerId);
					EquipmentSuggestEviction.getLstGSNPP(row.customerCode, row.shopCode, row.customerId);
				}				
			}
		},
		onLoadSuccess :function(data){ 	 
	    	$('.datagrid-header-rownumber').html('STT');		    		
	    	$('.datagrid-header-row td div').css('text-align','center');
	    	// updateRownumWidthForDataGrid('');	    	
		}
	});	
	EquipmentSuggestEviction._editIndex = null;
});

var equipCreateLendSuggest = {
	datagridOption: {
		column0: {			
			formatter: function (value,row,index) {
				return '<a href="javascript:void(0);" onclick="EquipmentSuggestEviction.deleteRowEquipLendDetail('+index+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
			}
		},
		/* column1: {
			title: 'Miền',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		}, */
		column2: {
			title: 'Đơn vị',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		},
		column3: {
			title: 'NPP',
			formatter: function(value, row, index) {
				if(row.shopCode != null && row.shopCode != ""){
					return Utils.XSSEncode(row.shopCode);
				}				
			},
			editor: {
				type:'text'
			}
		},
		column4: {
			title: 'Khách hàng (F9)',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column5: {
			title: 'Tên khách hàng',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			}
		},
		column6: {
			title: 'Mã thiết bị',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'combobox',
				options:{
					editable: false,
					valueField : 'code',
					textField : 'code',
					formatter: function(r) {
						if(r != null){
							return Utils.XSSEncode(r['code'].trim());
						}
					},
					// filter: function(q, r){
					// 	q = q.toString().trim().toUpperCase();
					// 	q = unicodeToEnglish(q);
					// 	return (r['searchText'].indexOf(q)>=0);
					// },
					onSelect: function(r) {
						if(r != null){
							var row = EquipmentSuggestEviction.getDetailEdit();
							var data = {};
							data.customerCode = row.customerCode; 
							data.equipCode = r.code;
							data.shopCode = row.shopCode;

							Utils.getJSONDataByAjaxNotOverlay(data, '/equip-suggest-eviction/get-equip-delivery-record',function(result) {
								if(result.equipmentRecordDeliveryVO != undefined && result.equipmentRecordDeliveryVO != null){		
									row.relation = result.equipmentRecordDeliveryVO.toRelation;
									row.representative = result.equipmentRecordDeliveryVO.toRepresentative;
									row.ward = result.equipmentRecordDeliveryVO.toWardName;
									row.district = result.equipmentRecordDeliveryVO.toDistrictName;
									row.province = result.equipmentRecordDeliveryVO.toProvinceName;
									row.address = result.equipmentRecordDeliveryVO.toHouseNumber;
									row.street = result.equipmentRecordDeliveryVO.toStreet;								
								}else{
									row.relation = "";
									row.representative = "";
									row.ward = "";
									row.district = "";
									row.province = "";
									row.address = "";
									row.street = "";		
								}		

								// set data
								row.equipCode = r.code;
								row.equipSeri = r.serial;
								row.equipGroup = r.equipGroupName;
								row.healthStatus = r.healthStatus;
								// them dong moi			
								$('#gridEquipSuggestEvictionChange').datagrid('endEdit', EquipmentSuggestEviction._editIndex);

								$('#gridEquipSuggestEvictionChange').datagrid('beginEdit', EquipmentSuggestEviction._editIndex);			
								EquipmentSuggestEviction.addEditorInGridChangeLend();															
							});
						}
					}
				}
			}
		},
		column7: {
			title: 'serial',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		},
		column8: {
			title: 'Thời gian mong muốn thu hồi thiết bị',
			formatter: function (value,row,index) {
				var html = "";
				if(row.timeEviction != null) {
					html = Utils.XSSEncode(value);
				} 
				return html;
			},
			editor: {
				type:'datebox',
				options:{
					validType:{
						length:[0,5]
					}
				}
			}
		},
		column9: {
			title: 'Kho nhận',
			formatter: function (value,row,index) {
				var html = ""
				if(value != null && value != ""){
					html = value;
				}else {
					html = TypeStockEquipLend.parseValueNPP(row.stockTypeValue);
				}
				return Utils.XSSEncode(html);
			},
			editor: {
				type:'combobox',
				options:{
					editable: false,
					data: [{"value":1, "text":"Kho NPP"},{"value":2, "text":"Kho công ty"}],
				    valueField: 'value',
				    textField: 'text'
				}
			}
		},
		column10: {
			title: 'Lý do đề nghị',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
			editor: {
				type:'text'
			}
		},
		column11: {
			title: 'Giám sát',
			formatter: function(value, row, index) {
				return Utils.XSSEncode(row.staffCode + " - " + row.staffName);
			},
			editor: {
				type:'combobox',
				options:{
					editable: false,
					valueField : 'staffCode',
					textField : 'nameText',
					formatter: function(r) {
						return Utils.XSSEncode(r['nameText'].trim());
					},
					// filter: function(q, r){
					// 	q = q.toString().trim().toUpperCase();
					// 	q = unicodeToEnglish(q);
					// 	return (r['searchText'].indexOf(q)>=0);
					// },
					onSelect: function(r) {
						if(r != null) {
							var row = EquipmentSuggestEviction.getDetailEdit();
							row.staffCode = r.staffCode;
							row.staffName = r.staffName;
							if (r.phone != null && r.phone != "" && r.mobilephone != null && r.mobilephone != "") {
								row.phone = r.phone + " / " + r.mobilephone;
							} else if (r.phone != null && r.phone != "") {
								row.phone = r.phone;
							} else if (r.mobilephone != null && r.mobilephone != "") {
								row.phone = r.mobilephone;
							} else {
								row.phone = "";
							}
							var equipCode = row.equipCode;
							// them dong moi	
							$('#gridEquipSuggestEvictionChange').datagrid('endEdit', EquipmentSuggestEviction._editIndex);
							row.equipCode = equipCode;

							$('#gridEquipSuggestEvictionChange').datagrid('beginEdit', EquipmentSuggestEviction._editIndex);
							var ed = $('#gridEquipSuggestEvictionChange').datagrid('getEditors', EquipmentSuggestEviction._editIndex);		
							EquipmentSuggestEviction.addEditorInGridChangeLend();							
						}
					}
				}
			}
		},
		column12: {
			title: 'Điện thoại',
			formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			},
		},
		column13: {
			title: 'Người đứng tên mượn thiết bị',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		},
		column14: {
			title: 'Quan hệ người đứng tên mượn thiết bị với chủ cửa hàng/Doanh nghiệp',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		},
		column15: {
			title: 'Điện thoại',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		},
		column16: {
			title: 'Số nhà',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		},
		column17: {
			title: 'Tỉnh/Thành phố',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		},
		column18: {
			title: 'Quận/Huyện',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		},
		column19: {
			title: 'Phường/Xã',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		},
		column20: {
			title: 'Tên đường/Phố/Thôn/Ấp',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		},
		column21: {
			title: 'Số lượng',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(1);
			},
		},
		column22: {
			title: 'Nhóm thiết bị',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			},
		},
		column23: {
			title: 'Trình trạng thiết bị',
			formatter: function (value,row,index) {
				return Utils.XSSEncode(value);
			}
		}
	}
}
</script>
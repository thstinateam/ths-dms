<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Thiết bị</a>
		</li>
		<li><span>Quản lý đề nghị mượn thiết bị</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
<!-- 				<h2 class="Title2Style">Thông tin tìm kiếm</h2> -->
				<div class="SearchInSection SProduct1Form" style="padding-left: 0px;">
				<div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin tìm kiếm" style="width:1000px;height:200px;padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
					<!-- <label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Đơn vị </label>
					<input id="shop" type="text"  style="height: auto;" class="InputTextStyle InputText1Style" tabindex="1" />
					<div class="Clear"></div> -->
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Mã biên bản</label>
					<input id="lendSuggestCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="4"/>
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 60px;height:auto;">Trạng thái biên bản</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="status" tabindex="9">
							<option value="-1" selected="selected">Tất cả</option>
							<option value="0" >Dự thảo</option>
							<option value="2" >Đã duyệt</option>
							<option value="4" >Hủy</option>
						</select>
					</div>
					<div class="Clear"></div>

					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 10px;height:auto;">Ngày tạo từ ngày</label>
					<div id="fromDateDiv">
						<input type="text" id="fDate" class="InputTextStyle InputText6Style" tabindex="2"/>
					</div>
					<label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 60px;height:auto;">Đến ngày</label>
					<div id="toDateDiv">
						<input type="text" id="tDate" class="InputTextStyle InputText6Style" tabindex="3"/>
					</div>
					<!-- <label class="LabelStyle Label1Style" style=";text-align:left;padding-left: 60px;height:auto;">Xuất hợp đồng/Giao nhận</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="statusDelivery" tabindex="10">
							<option value="-1" selected="selected">Tất cả</option>
							<option value="0" >Đã xuất</option>
							<option value="1" >Chưa xuất</option>
						</select>
					</div> -->
					<div class="Clear"></div>
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnSearch" class="BtnGeneralStyle" onclick="" tabindex="11">Tìm kiếm</button>
						</div>
						<div class="Clear"></div>
					</div>	
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
				</div>		
				</div>	
				<!-- danh sach bien ban giao nhan -->	
				<h2 class="Title2Style" style="height:15px;">
					<span style="float:left;">Danh sách đề nghị mượn tủ</span>
					<div style="float:right; margin-top: -5px;margin-right: 27px;">
						<a href="javascript:void(0);" onclick="EquipmentManageCatalog.printBBTHTL();" ><img src="/resources/images/icon-printer.png" height="20" width="20" title="In biên bản"></a>
						<a href="#" class="downloadTemplateReport"><img src="/resources/images/icon_download_32.png" height="20" width="20" title="Tải mẫu Nhập Excel"></a>
						<a href="javascript:void(0);" onclick="EquipmentManageCatalog.showDlgImportExcel();"><img src="/resources/images/icon-import.png" height="25" width="25" title="Nhập Excel"></a>
						<a href="javascript:void(0);" onclick="EquipmentManageCatalog.exportExcelTHTL();"><img src="/resources/images/icon-export.png" height="25" width="25" title="Xuất Excel"></a>					
						
						<button style="float:right;" id="btnSaveLst" class="BtnGeneralStyle" onclick="EquipmentManageCatalog.updateStatus();" >Lưu</button>
						<div style="float:right;margin-right: 5px;" class="BoxSelect BoxSelect11">							
							<select class="MySelectBoxClass" id="statusRecordLabel" style="width:112px;" onchange="changeCombo();">
								<option value="-1" selected="selected">Hủy/Duyệt</option>
								<option value="2" >Duyệt</option>
								<option value="4" >Hủy</option>
							</select>
						</div>
					</div>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="grid"></table>
					</div>
				</div>							
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup lich su giao nhan -->
<div style="display: none">
	<div id="easyuiPopupHistory" class="easyui-dialog" title="Lịch sử giao nhận" data-options="closed:true,modal:true"  style="width:400px;height:215px;">
		<div class="PopupContentMid">
	   		<div class="SearchInSection SProduct1Form">
				<div class="GridSection" id="gridHitoryDiv">
					<table id="gridHistory"></table>
				</div>
			</div>        
		</div>
	</div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Import - Biên bản thu hồi và thanh lý" data-options="closed:true,modal:true" style="width:465px;height:250px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-eviction-manage/importExcel" name="importFrmBBGN" id="importFrmBBGN" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">
						<input id="fakefilepcBBGN" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileBBGN" onchange="previewImportExcelFile(this,'importFrmBBGN','fakefilepcBBGN', 'errExcelMsgBBGN');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a onclick="" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="return EquipmentManageCatalog.importExcelBBTHTL();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgBBGN"/>
	</div>	
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {	
	$('#panelDeliveryRecord').width($('#panelDeliveryRecord').parent().width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 5px;");
	},1000);
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar("fDate");
	ReportUtils.setCurrentDateForCalendar("tDate");	
	$('#shopCode').focus();
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

 //    var shopKendo = $("#shop").data("kendoMultiSelect");
 //    shopKendo.wrapper.attr("id", "shop-wrapper"); 
	// $('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_import_bien_ban_thu_hoi_thanh_ly.xlsx');
	var params = new Object();
	// EquipmentManageCatalog._mapEviction = new Map();
	// EquipmentManageCatalog._lstRowID = new Array();
	// params.fromDate = $('#fDate').val();
	// params.toDate = $('#tDate').val();	
	$('#grid').datagrid({
		url:'/equipCollectionSuggest/search',
		method : 'GET',
		height: 'auto',
		width : $(window).width() - 35,
		scrollbarSize:0,
		rownumbers:true,
		fitColumns:true,
		autoRowHeight:true, 
		pagination:true, 
		pageSize:10,
		queryParams:params,
		pageList  : [20],
// 		width: $('#gridContainer').width(),
		columns:[[		  
			{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left'},
			{field: 'maBienBan',title:'Mã biên bản', width: 100, sortable:false,resizable:false, align: 'left', formatter: function(value,row,index){
				return Utils.XSSEncode(value);
			}  },
			{field: 'ngay',title:'Ngày',width: 100,sortable:false,resizable:false, align: 'center', formatter: function(value,row,index){
				return Utils.XSSEncode(value);
			} },
			{field: 'giamSat',title:'Giám sát',width: 150,sortable:false,resizable:false, align: 'left' , formatter: function(value,row,index){
				return Utils.XSSEncode(value);
			} },
			{field: 'trangThaiBienBan', title: 'Trạng thái biên bản', width: 100, sortable:false,resizable:false,align: 'left', formatter: function(value,row,index){
				return Utils.XSSEncode(value);
			} },
			{field:'edit', title:'<a href="/equipment-checking/edit"><span style="cursor:pointer"><img title="Thêm mới" src="/resources/images/icon_add.png"/></span></a>', width: 50, align: 'center', formatter: function (value, row, index) {
				var html = '<a href="#""><span style="cursor:pointer"><img title="Xem kiểm kê" src="/resources/images/icon_detail.png"/></span></a>'; 
				html += '&emsp;'; 
				html += '<a onclick=""><span style="cursor:pointer"><img title="Xem nhóm thiết bị" src="/resources/images/icon-view.png"/></span></a>';
				// if(statisticStatus.DT == row.recordStatus || statisticStatus.HD == row.recordStatus){
					html += '&emsp;';
					html += '<a href="#"><span style="cursor:pointer"><img title="Chỉnh sửa" src="/resources/images/icon_edit.png"/></span></a>';
				// }
				return html;
		    }}
		]],
		onBeforeLoad:function(param){											 
		},
		onLoadSuccess :function(data){  	 		    	
	    	$(window).resize();
	    	if (data == null || data.total == 0) {
				
			}else{
				$('.datagrid-header-rownumber').html('STT');		    	
			}		
	    	$('.datagrid-header-row td div').css('text-align','center');
	    	updateRownumWidthForDataGrid('');
			var rows = $('#grid').datagrid('getRows');
	    	$('.datagrid-header-check input').removeAttr('checked');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(EquipmentManageCatalog._mapEviction.get(rows[i].id)!=null) {
	    			$('#grid').datagrid('checkRow', i);
	    		}
	    	}
    		 $(window).resize();
		},
		// onCheck:function(index,row){
		// 	EquipmentManageCatalog._mapEviction.put(row.id,row);  
		// 	if(row.trangThaiBienBan == "Đã duyệt"){
		// 		EquipmentManageCatalog._lstRowID.push(row.id);
		// 	}
			
	 //    },
	 //    onUncheck:function(index,row){
	 //    	EquipmentManageCatalog._mapEviction.remove(row.id);
		// 	var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
	 //    	if(position != null){
	 //    		EquipmentManageCatalog._lstRowID.splice(position,1);
	 //    	}
	 //    },
	 //    onCheckAll:function(rows){
		//  	for(var i = 0;i<rows.length;i++){
	 //    		var row = rows[i];
	 //    		EquipmentManageCatalog._mapEviction.put(row.id,row);
	 //    		var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
		// 		if(position == -1 && row.trangThaiBienBan == "Đã duyệt"){
		// 			EquipmentManageCatalog._lstRowID.push(row.id);
		// 		}
	 //    	}
	 //    },
	 //    onUncheckAll:function(rows){
	 //    	for(var i = 0;i<rows.length;i++){
	 //    		var row = rows[i];
	 //    		EquipmentManageCatalog._mapEviction.remove(row.id);	
	 //    		var position = EquipmentManageCatalog._lstRowID.indexOf(row.id);
		// 		if(position != null){
		// 			EquipmentManageCatalog._lstRowID.splice(position,1);
		// 		}
	 //    	}
	 //    }
	});
});
function changeCombo() {
    var status = $('#statusRecordLabel').val().trim();
    var html = '';
    if(status == '2'||status == '-1'){
    	html = 
    	'<option value="0" selected="selected">Chọn</option> <option value="1" >Đã gửi</option> <option value="2" >Đã nhận</option> ';
    }else{
    	html = '<option value="0" selected="selected">Chọn</option>';
    }
    $('#statusDeliveryLabel').html(html);
    $('#statusDeliveryLabel').change();
}
</script>
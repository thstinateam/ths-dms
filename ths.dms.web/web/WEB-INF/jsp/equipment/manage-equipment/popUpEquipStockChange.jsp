<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="dialogEquipStockChange" class="easyui-dialog" title="Thêm mới kho" data-options="closed:true,modal:true">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">
			<label class="LabelStyle Label4Style"><span style="margin-left:30px;">Mã đơn vị (F9) </span><span class="RequireStyle">(*)</span></label>
      <input type="text" id="shopCodeDl" class="InputTextStyle InputText5Style" />
        	<!-- <div class="BoxSelect BoxSelect2"> -->
        	<!-- <div class="BoxSelect" id="shopCodeDl">
             	<input type="text" id="shopTreeDialog" class="InputTextStyle InputText5Style" />
 				     <input type="hidden" id="shopCodeDialog"/>
			     </div> -->
			<div class="Clear"></div>
			
         	<label class="LabelStyle Label4Style" id="labelStockCode"><span style="margin-left:30px;">Mã kho </span><span class="RequireStyle">(*)</span></label>
           	<input id="txtCodeDl" type="text" class="InputTextStyle InputText5Style" maxlength="50"/>
           	<div class="Clear"></div>
           	<label class="LabelStyle Label4Style" id="labeStockName"><span style="margin-left:30px;">Tên kho </span><span class="RequireStyle">(*)</span></label>
           	<input id="txtNameDl" type="text" class="InputTextStyle InputText5Style" maxlength="250"/>
           	<!-- <label class="LabelStyle Label2Style" id="labelBankPhone">Số điện thoại</label>
           	<input id="bankPhoneDl" type="text" class="InputTextStyle InputText5Style" maxlength="50"/>
           	<div class="Clear"></div>
           	
           	<label class="LabelStyle Label7Style" id="labelBankAddress">Địa chỉ</label>
           	<input id="bankAddressDl" type="text" class="InputTextStyle InputText5Style" maxlength="100"/>
           	<label class="LabelStyle Label2Style" id="labelBankAccount">Số tài khoản</label>
           	<input id="bankAccountDl" type="text" class="InputTextStyle InputText5Style" maxlength="100"/> -->
           	<label class="LabelStyle Label2Style">Trạng thái</label>
				    <div class="BoxSelect BoxSelect2">
                  <select id="statusDl" class="MySelectBoxClass">
                      <option value="1" selected="selected">Hoạt động</option>
                      <option value="0">Tạm ngưng</option>
                  </select>
              	</div>
             <div class="Clear"></div>
             <label class="LabelStyle Label4Style" id="labelStockDescription"><span style="margin-left:30px;">Ghi chú</span></label>
           	<input id="txtDescriptionDl" type="text" class="InputTextStyle InputText5Style" maxlength="250"/>
           	<label class="LabelStyle Label2Style" id="labelStockOrdinal">Thứ tự <span class="RequireStyle">(*)</span></label>
           	<input id="txtOrdinalDl" type="text" class="InputTextStyle InputText5Style" maxlength="2" onkeypress="return numbersonly(event,false);"/>
           	<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnSaveEquipStock" class="BtnGeneralStyle">Lưu</button>
				<button class="BtnGeneralStyle" onclick="$('#dialogEquipStockChange').dialog('close');">Bỏ qua</button>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="Clear"></div>
		<p id="errorMsgDl" class="ErrorMsgStyle" style="display: none;"></p>
		<p id="successMsgDl" class="SuccessMsgStyle" style="display: none"></p>
	</div>
</div>
<input type="hidden" id="idStock" />
<script type="text/javascript">
	$(function(){
	});
</script>

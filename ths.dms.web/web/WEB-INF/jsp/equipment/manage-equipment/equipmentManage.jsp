<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!-- begin vuongmq; 11/05/2015; xuat Excel ATTT -->
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<!-- end vuongmq; 11/05/2015; xuat Excel ATTT -->
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="javascript:void(0);">Thiết bị</a>
		</li>
		<li>
			<span>Quản lý kho thiết bị</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div id="pnlListEquipment"  class="easyui-panel" title="Thông tin tìm kiếm" style="width:auto;height:205px;padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
					<div class="SearchInSection SProduct1Form">
						<div class="Clear"></div>
						<label class="LabelStyle Label3Style" ><span style="margin-left:60px;">Mã đơn vị (F9)</span></label>
						<input type="text"	class="InputTextStyle InputText1Style" id="txtShopCode" tabindex="1"/> 
						<div class="Clear"></div>
						<label class="LabelStyle Label3Style"><span style="margin-left:60px;">Mã kho</span></label> 
						<input	type="text" class="InputTextStyle InputText1Style" id="txtCode" maxlength="50" tabindex="2" /> 
						<label	class="LabelStyle Label1Style">Tên kho</label> 
						<input type="text" class="InputText1Style" style="float: left; border: 1px solid #c4c4c4; height: 20px; line-height: 20px; margin-bottom: 9px; padding: 0 4px;" id="txtName" maxlength="250" tabindex="3" />
						<label	class="LabelStyle Label1Style">Trạng thái</label>
						<div class="BoxSelect BoxSelect2" id="ddlUsageStatusDiv">
							<select class="MySelectBoxClass InputTextStyle" id="ddlUsageStatus" tabindex="3">
								<option value="-1" selected="selected">Tất cả</option>
							    <option value="1">Hoạt động</option>
							    <option value="0">Tạm ngưng</option>
							</select>
						</div>
						<div class="Clear"></div>
						<label class="LabelStyle Label3Style"><span style="margin-left:60px;">Từ ngày</span></label>
						<div id="fromDateDiv">
							<input type="text" id="fDate" class="InputTextStyle InputText6Style" tabindex="5"/>
						</div>
						<label class="LabelStyle Label1Style">Đến ngày</label>
						<div id="toDateDiv">
							<input type="text" id="tDate" class="InputTextStyle InputText6Style" tabindex="6"/>
						</div>	
						<div class="Clear"></div>
						<div class="SearchInSection SProduct1Form">									
							<div class="BtnCenterSection">
								<button id="btnSearch" class="BtnGeneralStyle" onclick="EquipmentStockChange.searchListEquipmentStock();" tabindex="7">Tìm kiếm</button>
							</div>
							<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
							<div class="Clear"></div>
						</div>	
					</div>			
				</div>
				<h2 class="Title2Style">Danh sách kho
					<a tabindex="10" onclick = "EquipmentStockChange.exportExcelBySearchEquipmentStock();" style = "float: right;	padding: 0px 25px 0px 0px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_export.png" title="Xuất Excel"></a>
					<a tabindex="9" onclick = "EquipmentStockChange.openDialogImportListEquipmentStock();" style = "float: right;	padding-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_import.png" title="Nhập Excel"></a>
					<a tabindex="8" id="downloadTemplate" onclick = "" style = "float: right; padding-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_download_32.png" title="Tải mẫu Nhập Excel"></a>
					<!-- <a onclick = "EquipmentStockChange.downloadTemplateImportListEquipStock();" style = "float: right; padding-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_download_32.png" title="Tải mẫu Nhập Excel"></a> -->
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<!-- <table id="listEquipmentDg" class="easyui-datagrid"></table> -->
						<table id="listEquipmentDg" ></table>
					</div>
				</div>		
				<div class="Clear"></div>
			</div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- begin popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Nhập Excel Kho thiết bị" data-options="closed:true,modal:true" style="width:500px;height:250px;">
		<div class="PopupContentMid">
			<!-- <div class="SearchInSection SProduct1Form" style="border: none;"> -->
			<div class="GeneralForm ImportExcel1Form" style="border: none;">
				<form action="/equipment-manage/importEquipStock" name="importFrmEquipStockName" id="importFrmEquipStock" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style" style="text-align: right; width: 80px;">Tập tin Excel</label>
				    <div class="UploadFileSection Sprite1" style="width: 368px;">
						<input id="fakefilepcEquipStock" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" style="width: 270px;" size="1" name="excelFile" id="excelFileEquipStock" onchange="previewImportExcelFile(this,'importFrmEquipStockName','fakefilepcEquipStock', 'errExcelMsgEquipStock');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a id="downloadTemplate" onclick="" href="javascript:void(0)" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
					<!-- <a onclick="EquipmentListEquipment.downloadTemplateImportListEquip();" href="javascript:void(0)" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a> -->
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="EquipmentStockChange.importEquipmentStock();">Nhập Excel</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').dialog('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p id="successExcelMsgEquipStock" class="SuccessMsgStyle" style="display:none;"></p>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgEquipStock" />
	</div>	
</div>
<!-- end popup import -->
<!-- popUp view don vi -->
<div id="popup-unit-tree" title="Tìm kiếm đơn vị" data-options="closed:true,modal:true" >
	<div class="dialogUnitTreeContent" style="overflow:hidden; display: none;">
		<tiles:insertTemplate template="/WEB-INF/jsp/equipment/manage-equipment/popupUnitTree.jsp" />
	</div>							
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/general/dialogSearchShopTree.jsp" />
</div>
<!-- popUp view change equip stock -->
<div class="ContentSection" style="visibility: hidden;" id="dialogEquipStockDiv">
	<tiles:insertTemplate template="/WEB-INF/jsp/equipment/manage-equipment/popUpEquipStockChange.jsp" />
</div>
<script type="text/javascript">
$(document).ready(function() {	
	$('#txtName').width($('#txtCode').width());
	/*$('#shopCode, #txtCode, #txtName, #ddlUsageStatusDiv, #fDate, #tDate').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});*/
	$('.ContentSection .InputTextStyle,.SearchSection .InputTextStyle').unbind("keyup");
	$("#shopCode").parent().unbind("keyup");
	$("#shopCode").parent().bind("keyup", function(event) {
		if (event.keyCode == keyCodes.ENTER && $(".easyui-dialog:not(:hidden)").length == 0) {
			$("#btnSearch").click();
		}
	});
	$("#shopCodeDl").parent().unbind("keyup");
	$("#shopCodeDl").parent().bind("keyup", function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$("#btnSaveEquipStock").click();
		}
	});
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	/*ReportUtils.setCurrentDateForCalendar("fDate");
	ReportUtils.setCurrentDateForCalendar("tDate");	*/
	$('#ddlUsageStatus').val(1).change();
	//$('#pnlListEquipment').width($(window).width()-15);
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
		/** bỏ Hover buttom mui ten*/
		$('[class="panel-header panel-header-noborder"] a.panel-tool-collapse').attr('style', 'margin-right: 19px; width: 16px;');
		$('#shopCode').focus();
	}, 1000);
	/** tai file mau temple*/
	$('#downloadTemplate').attr('href',excel_template_path + 'equipment/Bieu_mau_quan_ly_kho_thiet_bi.xlsx');
	$('.easyui-dialog #downloadTemplate').attr('href',excel_template_path + 'equipment/Bieu_mau_quan_ly_kho_thiet_bi.xlsx');
	/** keyup F9 cho ma don vi*/
	var txt = 'txtShopCode';
	$('#txtShopCode').bind('keyup', function(event) {
		 if(event.keyCode == keyCode_F9){
		 	/** type: 0 la form tim kiem; 1: form them moi*/
			 // EquipmentStockChange.viewPopupUnit(EquipmentStockChange._FormSearch);
		 	
					/*VCommonJS.showDialogSearch2({
						dialogInfo: {title:'Tìm kiếm kho'},
						params : {
							status : 1,
							isUnionShopRoot: true
						},
						inputs : [
					        {id:'code', maxlength:50, label:'Mã Đơn Vị'},
					        {id:'name', maxlength:250, label:'Tên Đơn Vị'},
					    ],
					    url : '/commons/search-shop-show-list-NPP',
					    columns : [[
					        {field:'shopName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
					            return row.shopCode + '-' + row.shopName;         
					        }},					
							{field:'shopCode', title:'Mã kho', align:'left', width: 110, sortable:false, resizable:false},
					        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
					            return '<a href="javascript:void(0)" onclick="return EquipmentStockChange.selectStock(\''+txt+'\', \''+row.shopCode+'\');">chọn</a>';         
					        }}
					    ]]
					});*/
			//$('#code').focus();

			$('#common-dialog-search-tree-in-grid-choose-single').dialog({
				title: "Tìm kiếm Đơn vị",
				closed: false,
		        cache: false, 
		        modal: true,
		        width: 680,
		        height: 550,
		        onOpen: function(){
		        	$('#txtShopCodeDlg').val("");
		        	$('#txtShopNameDlg').val("");
		        	//Tao cay don vi
		        	$('#commonDialogTreeShopG').treegrid({  
		        		data: StockManager._arrRreeShopTocken,
		        		url : '/cms/searchTreeShop',
		    		    width: 650,
		    		    height: 325,
		    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
		    			fitColumns : true,
		    			checkOnSelect: false,
		    			selectOnCheck: false,
		    		    rownumbers : true,
		    	        idField: 'id',
		    	        treeField: 'code',
		    		    columns:[[
		    		        {field:'code',title:work_date_unit_code,resizable:false, width:200, align:'left', formatter:function(value, row, index){
		    		        	return Utils.XSSEncode(value);
		    		        }},
		    		        {field:'name',title:work_date_unit_name,resizable:false, width:200, align:'left', formatter:function(value, row, index){
		    		        	return Utils.XSSEncode(value);
		    		        }},
		    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
		    		        	return '<a href="javascript:void(0)" onclick="WorkingDatePlan.fillCodeForInputTextByDialogSearch2(\''+Utils.XSSEncode(txt)+'\', ' + row.id +', \''+Utils.XSSEncode(row.code)+'\', \''+Utils.XSSEncode(row.name)+'\' ,\''+Utils.XSSEncode(Utils._isShowInputTextByDlSearch2.code)+'\');">'+work_date_chon+'</a>';   
		    		        }}
		    		    ]],
		    	        onLoadSuccess :function(data){
		    				$('.datagrid-header-rownumber').html('STT');
		    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
		    	        }
		    		});
		        	$('#txtShopCodeDlg').focus();
		        },
		        onClose:function() {
		        	
		        }
			});
			}
		});
	EquipmentStockChange._params = {
			shopCode:  $('#txtShopCode').val(),
			code: $('#txtCode').val(),
			name: $('#txtName').val(),
			status: $('#ddlUsageStatus').val(),
			fromDate: $('#fDate').val(),
			toDate: $('#tDate').val(),
	};
	$('#listEquipmentDg').datagrid({
		url : "/equipment-manage/search-Equipment-Stock",
		pagination : true,
        rownumbers : true,
        pageNumber : 1,
        scrollbarSize: 0,
        //autoWidth: true,
        //height: 345,
        pageList: [10, 20, 50],
        pageSize: 20, //mac dinh lan dau tien 20 dong
        autoRowHeight : true,
        fitColumns : true,
        singleSelect: true,
		queryParams: EquipmentStockChange._params,
		width: $('#gridContainer').width() - 25,
	    columns:[[	
			{field:'createDate', title: 'Ngày tạo', width: 50, sortable: false, resizable: false, align: 'center', formatter:function(value,row,index){
				//return Utils.XSSEncode(value);
				return $.datepicker.formatDate('dd/mm/yy', new Date(Utils.XSSEncode(row.createDate)));
			}},	
			{field:'shopCode', title: 'Đơn vị', width:110, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
				var strShop = '';
				if(row.shop != undefined && row.shop != null){
					if(row.shop.shopCode != undefined && row.shop.shopCode != null){
						strShop += row.shop.shopCode;
					}
					if(row.shop.shopName != undefined && row.shop.shopName != null){
						strShop += ' - '+row.shop.shopName;
					}	
				}
				return Utils.XSSEncode(strShop);
			}},	
			{field:'typeShop', title: 'Loại đơn vị', width:50, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
				return Utils.XSSEncode(row.shop.type.name);	
			}},	
		    {field:'ordinal', title: 'Thứ tự', width: 30, sortable: false, resizable: false , align: 'right', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'code', title: 'Mã kho', width: 100, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'name', title: 'Tên kho', width: 120, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'status', title: 'Trạng thái', width: 60, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
		    	if(row.status != null && activeType.parseValue(row.status) == activeType.RUNNING){
		    		return activeStatusText;
		    	} else if(row.status != null && activeType.parseValue(row.status) == activeType.STOPPED){
		    		return inActiveStatusText;
		    	}
		    }},
		    {field:'edit', title:'<a href="javascript:void(0)" onclick="return EquipmentStockChange.dialogAddOrUpdateEquipmentStock(); " title="Thêm mới"><img src="/resources/images/icon_add.png"/></a>', width: 30, align: 'center', sortable: false, resizable: true, formatter: function(value,row,index){
		    	return '<a href="javascript:void(0)" onclick="return EquipmentStockChange.dialogAddOrUpdateEquipmentStock('+row.id+',\''+index+'\')"><img width="15" height="15" src="/resources/images/icon-edit.png" title="Chỉnh sửa"></a>';
// 				if (row.tradeStatus == null || tradeStatusEquipment.STOPPED != Number(row.tradeStatus)) {
// 					return '<a onclick="EquipmentListEquipment.viewEquipmentHistory('+row.equipId+',\''+row.code+'\')" href="javascript:void(0)"><img width="18" height="18" src="/resources/images/icon_view_history.png" title="Lịch sử sửa chữa '+row.code+'"></a><a onclick="return false;" href="javascript:void(0)"><img width="18" style="padding-left: 5px;" height="18" src="/resources/images/icon-edit_disable.png" title="Chỉnh sửa '+row.code+'"></a>';
// 				}
		    	/*return '<a onclick="EquipmentListEquipment.viewEquipmentHistory('+row.equipId+',\''+row.code+'\')" href="javascript:void(0)"><img width="18" height="18" src="/resources/images/icon_view_history.png" title="Lịch sử sửa chữa '+row.code+'"></a><a onclick="" href="/equipment-list-manage/viewDetailListEquipmentJSP?id='+row.equipId+'"><img width="18" style="padding-left: 5px;" height="18" src="/resources/images/icon-edit.png" title="Chỉnh sửa '+row.code+'"></a>';*/
			}}
	    ]],
	    onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');
	    	Utils.updateRownumWidthAndHeightForDataGrid('listEquipmentDg');
	    	//Utils.functionAccessFillControl('listEquipmentDg');
   		 	$(window).resize();    		 
	    }
	});
	
	$('#shopCode').focus();

	$('#txtShopCode, #txtCode, #txtName, #ddlUsageStatus, #fDate, #tDate').bind('keyup', function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$('#btnSearch').click();
		}
	});
});
</script>
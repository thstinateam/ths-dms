<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipment-stock-change-manage/info">Quản lý chuyển kho</a>
		</li>
		<li><span>Biên bản chuyển kho</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">		
					<label class="LabelStyle Label9Style">Mã biên bản</label>
					<input id="codeStockTran" type="text" class="InputTextStyle InputText1Style" value="<s:property value="equipmentStockVO.code"/>" disabled="disabled" tabindex="2" autocomplete="off" maxlength="50"/>												
					<label class="LabelStyle Label1Style" >Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClass" id="status" tabindex="2">
							<option value="2" >Duyệt</option>
							<option value="4" >Hủy</option>
						</select>
					</div>					
					<label class="LabelStyle Label1Style">Trạng thái thực hiện</label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="statusPerform" tabindex="3">
								<option value="2">Đang thực hiện</option>
						   		<option value="1">Hoàn thành</option>
							</select>
						</div>	
					<div class="Clear"></div>
					<label class="LabelStyle Label9Style">Ngày biên bản <span class="ReqiureStyle"> *</span></label>
					<input	type="text" class="InputTextStyle InputText1Style" id="createDate" maxlength="10" tabindex="6" disabled="disabled" value="<s:property value="equipmentStockVO.createFormDate"/>"/> 		
					<label class="LabelStyle Label1Style">Ghi chú</label>
					<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 41.7%; height: 100px; font-size: 13px;" maxlength="500" disabled="disabled"><s:property value="equipmentStockVO.note"/></textarea>
					<div class="Clear"></div>											
				</div>			
			<h2 class="Title2Style">Tập tin đính kèm</h2>
				<div class="SearchInSection SProduct1Form">
					<a href="javascript:void(0);" style="float: left;" title="Lựa chọn tập tin" ><img class="addFileBtn" src="/resources/images/icon_attach_file.png"></a>
					<div class="fileupload-process" style="width: 90%;">
						<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-striped active" id="total-progress" style="opacity: 0; width: 550px;">
							<div data-dz-uploadprogress="" style="width: 200px;" class="progress-bar progress-bar-success"></div>
						</div>
					</div>
					<div style="float:left; min-width: 500px; width: 90%;">
						<div style="height: auto; margin-left:20px;">
							<s:iterator id="fileVo" value="lstFileVo">
								<div class="divImageUploadFile" id = "divEquipAttachFile<s:property value="#fileVo.fileId"/>">
									<div class="imageUpdateShowIcon">
										<img class="addFileBtn" src="/resources/images/img-folder-001.png" width="50" height="50">
									</div>
									<div class="imageUpdateShowContent">
										<label><s:property value="#fileVo.fileName"/></label><br />
										<a href="javascript:void(0)" onclick="EquipmentStockChange.removeEquipAttachFile(<s:property value="#fileVo.fileId"/>);"><img title="Xóa tập tin" src="/resources/images/icon_delete.png" width="12" height="12" style="padding-left: 5px;"></a>
									</div>
								</div>
							</s:iterator>
						</div>
						<div class="Clear"></div>
						<div class="table table-striped" class="files" id="previews" style="min-height: 100px; margin-left:20px;">
							<div id="template" class="file-row" style="margin-bottom: 10px; width: 260px;float: left">
								<div style="margin-right: 10px; width: 50px; float: left">
									<span class="preview"><img data-dz-thumbnail /></span>
								</div>
								<div style="display: inline-block; width: 200px; clear: both">
									<div style="padding-right: 10px">
										<div>
											<p class="name" style="overflow: hidden" data-dz-name></p>
											<strong class="error text-danger" data-dz-errormessage></strong>
										</div>
										<div style="">
											<p class="size" data-dz-size></p>
											</div>
										<div>
											<a data-dz-remove href="javascript:void(0)"><span><img border="0" src="/resources/scripts/plugins/uploadify2.1/cancel.png"></span></a>
										</div>
									</div>								
								</div>							
							</div> 
						</div>
					</div>
					<div class="Clear"></div>
				</div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>	
					<p id="successMsgInfo" class="SuccessMsgStyle" style="display: none"></p>	
				<!-- danh sach bien ban giao nhan -->	
				<h2 class="Title2Style">
					<span>Thiết bị giao nhận</span>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="gridEquipmentStockChange"></table>
					</div>
				</div>
				<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<s:if test="equipmentStockVO.performStatus!=1 && equipmentStockVO.status !=4">
								<button id="btnUpdate" class="BtnGeneralStyle" onclick="EquipmentStockChange.createRecordStockChangeApproved();" tabindex="2">Cập nhật</button>		
							</s:if>				
							<button id="btnClose" class="BtnGeneralStyle" onclick="location.href = '/equipment-stock-change-manage/info';" tabindex="2">Bỏ qua</button>
						</div>
						<div class="Clear"></div>
					</div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div><input type="hidden" id="idChange" name="id" value='<s:property value="id"/>'></div>
<div><s:hidden id="id" name="id"></s:hidden></div>
<div><input type="hidden" id="status"></div>
<script type="text/javascript">
$(document).ready(function() {	
	EquipmentStockChange._countFile = Number('<s:property value="lstFileVo.size()"/>');
	 EquipmentStockChange._lstEquipStockChangeDel = [];
	 disableSelectbox('status');
	var status = '<s:property value="equipmentStockVO.status"/>';
	$('#status').val(status).change();
	
	var perStatus = '<s:property value="equipmentStockVO.performStatus"/>';
	$('#statusPerform').val(perStatus).change();
	if(perStatus == '1' || status == '4' || status == '2'){
		disableSelectbox('statusPerform');
	}
	var params = new Object();	
	params.id = $('#id').val().trim();
	var equipmentStockTransRecordCode = $('#codeStockTran').val();
	params.code = equipmentStockTransRecordCode ? equipmentStockTransRecordCode : '';
	var lstPerformStatus = [{performStatus:2,performStatusText:'Đang thực hiện'},{performStatus:1,performStatusText:'Hoàn thành'},{performStatus:0,performStatusText:'Hủy'}];
	EquipmentStockChange.editIndex = undefined; 
	$('#gridEquipmentStockChange').datagrid({
		url : '/equipment-stock-change-manage/search-equipment-stock',
		autoRowHeight : true,
		rownumbers : true, 
		//checkOnSelect :true,
		//selectOnCheck: true,
		singleSelect: true,
		pagination:true,
		fitColumns:true,
		pageSize :20,
		pageList  : [20],
		scrollbarSize:0,
		width: $('#gridContainer').width(),
		queryParams: params,
		autoWidth: true,		
		columns:[[				  
			{field: 'performStatus',title:'Trạng thái thực hiện', width: 100, sortable:false,resizable:false, align: 'left', 
    	    	formatter: function(value,row,index) {
    	    		if(isNullOrEmpty(row.performStatus)) return '';
    	    		if(row.performStatus!=null){
    	    			if(row.performStatus == '2'){
    	    				return 'Đang thực hiện';
    	    			}else if(row.performStatus == '1'){
    	    				return 'Hoàn thành';
    	    			}else{
    	    				return 'Hủy';
    	    			}
    	    		}
    	    	}, 
    	    	editor: {
    	    		type:'combobox',
    	    		options:{ 
						valueField:'performStatus',
						textField:'performStatusText',
						data:lstPerformStatus,
						width:100,
						required:true,
						invalidMessage:'Vui lòng chọn Trạng thái thực hiện !!!',
						missingMessage:'Vui lòng chọn Trạng thái thực hiện !!!',
					    filter: function(q, row){
					    	q = new String(q).toUpperCase();
							var opts = $(this).combobox('options');
							return row[opts.textField].indexOf(q)>=0;
					    },
					    onChange:function(newvalue,oldvalue){
							$('.combo-panel').each(function(){if(!$(this).is(':hidden')){$(this).parent().css('width','100px');$(this).css('width','98px');}});
						}
				}
			}},      
			{field: 'typeEquipment',title:'Loại thiết bị', width: 70, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
		    	var html="";
		    	if(row.typeEquipment!= null){
					html = row.typeEquipment; 
		    	}
				return Utils.XSSEncode(html);
			}},
			{field: 'groupEquipmentCode',title:'Nhóm thiết bị', width: 110, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
		    	var html="";
		    	if(row.groupEquipmentCode!= null){
					html = row.groupEquipmentCode; 
		    	}
				return Utils.XSSEncode(html);
			}},
			{field: 'capacity',title:'Dung tích (lít)',width: 60,sortable:false,resizable:false, align: 'left' },
			{field: 'equipmentCode',title:'Mã thiết bị',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var html = '<input type="text" id="equipmentCodeInGrid" size="20" maxlength ="50">';
				if(row.equipmentCode!=null && row.equipmentCode != undefined && row.equipmentCode!=''){
// 					if($('#equipmentCodeInGrid').val() != undefined ){
// 						html = '<input type="text" id="equipmentCodeInGrid" size="20" maxlength ="50" value="'+row.equipmentCode+'">';					
// 					}else{
// 						html = row.equipmentCode; 						
// 					}		
					html = Utils.XSSEncode(row.equipmentCode);			
				}
				return html;
			}},
			{field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var html = '<input type="text" id="seriNumberInGrid" size="20" maxlength ="100">';				
				if(row.seriNumber!=null && row.seriNumber != undefined && row.seriNumber!=''){						
					html = Utils.XSSEncode(row.seriNumber); 
				}					
				return html;		
			}},	
			{field: 'stock',title:'Kho giao',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var html="";
		    	if(row.stock!= null){
					html = Utils.XSSEncode(row.stock); 
		    	}
				return html;		
			}},
			{field: 'toStock',title:'Kho nhận(F9)',width: 100,sortable:false,resizable:false, align: 'left', 
				formatter: function(value, row, index) {
					var code = '';
					if(EquipmentStockChange._toStockDefault!=null){
						code = EquipmentStockChange._toStockDefault;
					}
					var html = '<input type="text" id="toStockInGrid"  size="20" maxlength ="100">';	
					if(code!=''){
						html = '<input type="text" id="toStockInGrid" value='+code+' size="20" maxlength ="100">';	
					}
					if(row.toStock!=null && row.toStock != undefined && row.toStock!=''){						
						html = row.toStock; 
					}					
					return html;		 
				},
				editor: {
					type:'validatebox',
					options:{ 
						width:100,
						required:true,	
						missingMessage:'Vui lòng chọn Kho nhận!!!'
		    		}
				}
			},
		    {field: 'healthStatus', title: 'Tình trạng thiết bị', width: 100, sortable:false,resizable:false,align: 'left'},

		]],
		onBeforeEdit :function(rowIndex,rowData){
	    	var rows = $('#gridEquipmentStockChange').datagrid('getRows');
	    	for(var i=0;i<rows.length;++i){
	    		if(i==rowIndex)
	    			continue;
	    		$('#gridEquipmentStockChange').datagrid('endEdit', i);    		
	    	}	
		},
		onAfterEdit : function(index, row, changes){
			$('#gridEquipmentStockChange').datagrid('refreshRow', index);
		},
		onDblClickRow:function(rowIndex, rowData) {
			if (EquipmentStockChange.editIndex != rowIndex){
				if (EquipmentStockChange.editIndex!=undefined && $('#gridEquipmentStockChange').datagrid('validateRow', EquipmentStockChange.editIndex)){
					$('#gridEquipmentStockChange').datagrid('beginEdit', rowIndex);	
					EquipmentStockChange.editIndex = rowIndex;
					$('.datagrid-editable-input').attr('id','toStockInGrid'+rowIndex);
			    	$('.datagrid-editable-input').bind('keyup', function(e){
						if(e.keyCode==keyCode_F9){			
							$('#errMsg').html('').show();				
							EquipmentStockChange.showPopupSearchStock('toStockInGrid'+rowIndex);
						} 
					});	
				}else{
					if(EquipmentStockChange.editIndex == undefined){
						EquipmentStockChange.editIndex = rowIndex;
						$('#gridEquipmentStockChange').datagrid('beginEdit', rowIndex);	
						$('.datagrid-editable-input').attr('id','toStockInGrid'+rowIndex);
				    	$('.datagrid-editable-input').bind('keyup', function(e){
							if(e.keyCode==keyCode_F9){			
								$('#errMsg').html('').show();				
								EquipmentStockChange.showPopupSearchStock('toStockInGrid'+rowIndex);
							} 
						});	
					}
				}
			}
		},
		onLoadSuccess :function(data){   			 	    	
	    	$(window).resize();	
	    	if (data == null || data.total == 0) {				
			}else{
				$('.datagrid-header-rownumber').html('STT');				
			}
	    /*	$('#equipmentCode').bind('keyup',function(event){
				if(event.keyCode == keyCodes.ENTER){
					EquipmentStockChange.insertSeri();
				}
			});*/			
		}
	});
	UploadUtil.initUploadFileByEquipment({
		url: '/equipment-stock-change-manage/create-record-stock-approved',
		elementSelector: 'body',
		elementId: 'body',
		clickableElement: '.addFileBtn',
		parallelUploads: 5
	}, function (data) {
		if (!data.error) {
			$("#successMsgInfo").html("Lưu dữ liệu thành công").show();
			var t = setTimeout(function(){
				$('.SuccessMsgStyle').html("").hide();
				window.location.href = '/equipment-stock-change-manage/info';
				clearTimeout(t);
			 }, 1000);
		} else {
			$('#errMsg').html(data.errMsg).show();
		}
	});
});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!-- begin vuongmq; 11/05/2015; xuat Excel ATTT -->
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<!-- end vuongmq; 11/05/2015; xuat Excel ATTT -->
<style type="text/css">
#player_api {
	width: 678px !important;
}
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">THIẾT BỊ</a></li>
		<li><span>Quản lý quyền kho thiết bị</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
					<div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin tìm kiếm" style="width: 1000px; height:auto; padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
					<div class="SearchInSection SProduct1Form">
						<label class="LabelStyle Label1Style">Mã quyền</label> 
							<input type="text" class="InputTextStyle InputText1Style" id="code"  maxlength="50" tabindex="2"/> 
						<label class="LabelStyle Label1Style">Tên quyền</label> 
							<input type="text" class="InputTextStyle InputText1Style" id="name"  maxlength="250" tabindex="2"/> 																
						<label	class="LabelStyle Label1Style">Trạng thái</label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status" tabindex="2">
								<option value="-2">Tất cả</option>								
						   		<option value="1" selected="selected">Hoạt động</option>
						   		<option value="0">Tạm ngưng</option>
							</select>
						</div>											
						<div class="Clear"></div>						
					</div>
					<div class="SearchInSection SProduct1Form">
				<div class="Clear"></div>
					<div class="BtnCenterSection">
                        <button class="BtnGeneralStyle"  tabindex="2" id="btnSearchStockPermission" onclick="return EquipmentStockPermission.searchStockPermission();">Tìm kiếm</button>
                    </div>					
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
					<p class="ErrorMsgStyle" id="errMsgSearch" style="display: none;"></p>
					<p id="successMsgDl" class="SuccessMsgStyle" style="display: none"></p>
					<div class="Clear"></div>
				</div>
				</div>
				<h2 class="Title2Style">Danh sách quyền</h2>
				<div style="float:right; margin-top: -25px;margin-right: 27px;">					
						<a href="javascript:void(0);" id="downloadTemplate" ><img src="/resources/images/icon_download_32.png" height="20" width="20" title="Tải mẫu Nhập Excel"></a>
						<a href="javascript:void(0);" onclick="EquipmentStockPermission.openDialogImportEquipmentStockPermission();"><img src="/resources/images/icon-import.png" height="25" width="25" title="Nhập Excel"></a>
						<a href="javascript:void(0);" onclick="EquipmentStockPermission.exportExcel();"><img src="/resources/images/icon-export.png" height="25" width="25" title="Xuất Excel"></a>						
					</div>
				<div class="Clear"></div>
				<div class="SearchInSection SProduct1Form">
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id=gridContainer>
							<table id="gridStockPermission"></table>
						</div>
					</div>	
				</div>
				<div class="GeneralMilkBox" style="display: none;" id="stockTransDetailTable">
					<h2 class="Title2Style" id="title"></h2>
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id="productDetailGrid">
							<table id="gridDetail"></table>
							<div id="pagerDetail"></div>
						</div>
					</div>					
                    <div class="Clear"></div>                   
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div class="ContentSection" style="visibility: hidden;" id="dialogBankDiv">
	<tiles:insertTemplate template="/WEB-INF/jsp/equipment/manage-stock/popUpAddPermissionStock.jsp" />
</div>
<div class="ContentSection" style="visibility: hidden;" id="dialogEditDiv">
	<tiles:insertTemplate template="/WEB-INF/jsp/equipment/manage-stock/popUpEditPermissionStock.jsp" />
</div>
<div class="ContentSection" style="visibility: hidden;" id="dialogCopyDiv">
	<tiles:insertTemplate template="/WEB-INF/jsp/equipment/manage-stock/popUpCopyPermissionStock.jsp" />
</div>
<!-- begin popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Nhập Excel Quyền kho thiết bị" data-options="closed:true,modal:true" style="width:465px;height:250px;">
		<div class="PopupContentMid">
			<!-- <div class="SearchInSection SProduct1Form" style="border: none;"> -->
			<div class="GeneralForm ImportExcel1Form" style="border: none;">
				<form action="/equipment-manager-stock-permission/importEquipStockPermission" name="importFrmName" id="importFrmEquipStockPermission" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style" style="padding-right: 10px; text-align: right; width: 80px;">Tập tin Excel</label>
				    <div class="UploadFileSection Sprite1" style="width: 368px;">
						<input id="fakefilepcEquipStockPermission" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" style="width: 270px;" size="1" name="excelFile" id="excelFileEquipStockPermission" onchange="previewImportExcelFile(this,'importFrmName','fakefilepcEquipStockPermission', 'errExcelMsg');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a id="downloadTemplate" onclick="" href="javascript:void(0)" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
					<!-- <a onclick="EquipmentListEquipment.downloadTemplateImportListEquip();" href="javascript:void(0)" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a> -->
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="EquipmentStockPermission.importEquipmentStockPermission();">Nhập Excel</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').dialog('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p id="successExcelMsg" class="SuccessMsgStyle" style="display:none;"></p>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsg" />
	</div>	
</div>
<!-- end popup import -->
<script type="text/javascript">
$(document).ready(function() {	
	 $('#code, #name').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearchStockPermission').click(); 
		}
	 }); 	  
	  EquipmentStockPermission._lstEditStock = [];	 	 
	  EquipmentStockPermission._lstStockNow = [];
	  EquipmentStockPermission._lstEquipStockDel = [];
	  EquipmentStockPermission._lstEquipStockInsert = [];	 
	 $('#panelDeliveryRecord').width($(window).width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
		$('#code').focus();
	},1000);
	
	/** tai file mau temple*/
	$('#downloadTemplate').attr('href',excel_template_path + 'equipment/Bieu_mau_quan_ly_quyen_kho_thiet_bi.xlsx');
	$('.easyui-dialog #downloadTemplate').attr('href',excel_template_path + 'equipment/Bieu_mau_quan_ly_quyen_kho_thiet_bi.xlsx');

	/**vuongmq; 25/03/2015; de dung xuat excel theo dk search*/
	EquipmentStockPermission._params = new Object();
	var params = new Object();
	params.name = $('#name').val().trim();
	params.code = $('#code').val().trim();
	params.status = $('#status').val();	
	EquipmentStockPermission._params.name = $('#name').val().trim();
	EquipmentStockPermission._params.code = $('#code').val().trim();
	EquipmentStockPermission._params.status = $('#status').val().trim();
	$('#gridStockPermission').datagrid({
		url : "/equipment-manager-stock-permission/search",
		autoRowHeight : true,
		rownumbers : true, 
		checkOnSelect :true,
		singleSelect: true,
		pagination:true,
		rowNum : 20,
		fitColumns:true,
		pageList  : [20,40,60],
		scrollbarSize:0,
		width : $(window).width() - 80,		
		autoWidth: true,
		queryParams:params,
		columns:[[		  
		    {field: 'id', index: 'id', hidden: true},		
		    {field:'code', title: 'Mã quyền', width: 100, sortable: false, resizable: true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field:'name', title: 'Tên quyền', width: 150, sortable: false, resizable: true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field:'description', title: 'Mô tả', width: 130, sortable: false, resizable: true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field: 'status', title: 'Trạng thái', width: 80, sortable:false,resizable:false,align: 'left',formatter: function(value, row, index){
		    	var html='';
		    	if(row.status == 1){
		    		html+='Hoạt động';		    	
		    	}else if(row.status == 0){
		    		html+='Tạm ngưng';
		    	}
		    	return html;
		    }},		  
		    {field:'edit', title:'<a href="javaScript:void(0);" onclick="return EquipmentStockPermission.openDialogChangeAdd();"><span style="cursor:pointer"><img title="Thêm mới" src="/resources/images/icon_add.png"/></span></a>', width: 60, align: 'center', formatter: function (value, row, index) {
				var html = '<a href="javascript:void(0);" onclick="return EquipmentStockPermission.viewDetail('+index+','+row.id+');"><span style="cursor:pointer"><img title="Xem kho" src="/resources/images/icon-view.png"/></span></a>'; 
				html += '&emsp;'; 
				html += '<a onclick="return EquipmentStockPermission.openDialogChangeEdit('+index+','+row.id+');"><span style="cursor:pointer"><img title="Chỉnh sửa" src="/resources/images/icon_edit.png"/></span></a>';
				html += '&emsp;'; 
				html += '<a onclick="return EquipmentStockPermission.openDialogChangeCopy('+index+','+row.id+');"><span style="cursor:pointer"><img title="Sao chép" src="/resources/images/icon-coppy.png" width="16" height="16"/></span></a>';
				html += '&emsp;';
				html += '<a href="javascript:void(0);" onclick="EquipmentStockPermission.deletePermissionStockInGrid('+index+','+row.id+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
				return html;
		    }}
		]],
		onBeforeLoad:function(param){											 
		},
		onLoadSuccess :function(data){  
			$(window).resize();
			Utils.updateRownumWidthAndHeightForDataGrid('gridStockPermission');
	    	if (data == null || data.total == 0) {
			}else{
				$('.datagrid-header-rownumber').html('STT');				
			}	    	
		},
	});
	
});
</script>
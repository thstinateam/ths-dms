<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="dialogParamEdit" class="easyui-dialog" title="Cập nhật quyền" data-options="closed:true,modal:true">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">
         	<label class="LabelStyle Label7Style" id="labelBankCode">Mã quyền<span class="RequireStyle">(*)</span></label>
           	<input id="codeEdit" type="text" class="InputTextStyle InputText5Style" maxlength="50"/>
           	<label class="LabelStyle Label2Style" id="labelBankName">Tên quyền<span class="RequireStyle">(*)</span></label>
           	<input id="nameEdit" type="text" class="InputTextStyle InputText5Style" maxlength="250"/>           
           	<div class="Clear"></div>
           	
           	<label class="LabelStyle Label7Style" id="labelBankAddress">Mô tả</label>
           	<input id="descEdit" type="text" class="InputTextStyle InputText5Style" maxlength="100"/>           	
           	<label class="LabelStyle Label2Style">Trạng thái</label>
				<div class="BoxSelect BoxSelect2">
                  <select id="statusEdit" class="MySelectBoxClass">
                      <option value="1" selected="selected">Hoạt động</option>
                      <option value="0">Tạm ngưng</option>
                  </select>
              	</div>
             <div class="Clear"></div>
             <h2 class="Title2Style">Danh sách kho</h2>	
             <div class="SearchInSection SProduct1Form">
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id=gridContainerStockEdit>
							<table id="gridStockRoleEdit"></table>
						</div>
					</div>	
			</div>
             
			<div class="BtnCenterSection">
				<button id="btnSaveAddStock" class="BtnGeneralStyle">Cập nhật</button>
				<button class="BtnGeneralStyle" onclick="$('#dialogParamEdit').dialog('close');">Bỏ qua</button>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="Clear"></div>
		<p id="errorEditMsgDl" class="ErrorMsgStyle" style="display: none;"></p>
		<p id="successMsgDl" class="SuccessMsgStyle" style="display: none"></p>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() { 
	
});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipment-stock-change-manage/info">Quản lý chuyển kho</a>
		</li>
		<li><span>Biên bản chuyển kho</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
<!-- 					<label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Kho giao (F9)</label> -->
<%-- 					<s:if test="id==null || id ==0"> --%>
<!-- 						<input type="text" id="fStock" class="InputTextStyle InputText1Style" tabindex="2" maxlength="50"/> -->
<%-- 					</s:if> --%>
<%-- 					<s:else> --%>
<%-- 						<input type="text" id="fStock" class="InputTextStyle InputText1Style"  tabindex="2" maxlength="50"  value="<s:property value="equipmentShopVO.FromStock"/>"/> --%>
<%-- 					</s:else> --%>
												
<!-- 					<label class="LabelStyle Label1Style" style=";text-align:right;height:auto;">Kho nhận (F9)</label>  -->
<%-- 						<s:if test="id==null || id ==0"> --%>
<!-- 						<input type="text" id="tStock" class="InputTextStyle InputText1Style" tabindex="2" maxlength="50" /> -->
<%-- 					</s:if> --%>
<%-- 					<s:else> --%>
<%-- 						<input type="text" id="tStock" class="InputTextStyle InputText1Style"  tabindex="2" maxlength="50" value="<s:property value="equipmentShopVO.ToStock"/>"/> --%>
<%-- 					</s:else>	 --%>
						
					<label class="LabelStyle Label9Style">Mã biên bản</label>
					<s:if test="id==null || id ==0">
						<input	type="text" class="InputTextStyle InputText1Style" id="codeStockTran" maxlength="50" disabled="disabled" autocomplete="off" tabindex="2"/> 	
					</s:if>
					<s:else>
						<input id="codeStockTran" type="text" class="InputTextStyle InputText1Style" value="<s:property value="equipmentStockVO.code"/>" disabled="disabled" tabindex="2" autocomplete="off" maxlength="50"/>
					</s:else>		
												
					
					<label class="LabelStyle Label9Style" >Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
					<s:if test="id==null || id ==0">
						<select class="MySelectBoxClass" id="status" tabindex="2" onchange="changeCombo();">
							<option value="0" selected="selected">Dự thảo</option>
<!-- 							<option value="2" >Duyệt</option>							 -->
						</select>
					</s:if>
					<s:else>
						<select class="MySelectBoxClass" id="status" tabindex="2" onchange="changeCombo();">
							<option value="0" selected="selected">Dự thảo</option>
<!-- 							<option value="2" >Duyệt</option> -->
							<option value="4" >Hủy</option>
						</select>
					</s:else>	
					</div>					
								
					<label class="LabelStyle Label1Style" style=";text-align:right;height:auto;">Trạng thái thực hiện</label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="statusPerform" tabindex="2">
								<option value="2">Đang thực hiện</option>
<!-- 						   		<option value="1">Hoàn thành</option> -->
							</select>
						</div>

			<div class="Clear"></div>
					<label class="LabelStyle Label9Style">Ngày biên bản <span class="ReqiureStyle"> *</span></label>
					<input	type="text" class="InputTextStyle InputText1Style" id="createDate" maxlength="10" tabindex="6"/> 
					<!-- <s:if test="id==null || id ==0">
						<input	type="text" class="InputTextStyle InputText1Style" id="createDate" maxlength="10" tabindex="6"/> 	
					</s:if>
					<s:else>
						<input id="createDate" type="text" class="InputTextStyle InputText1Style" value="<s:property value="equipmentStockVO.createFormDate"/>"  tabindex="6" maxlength="10"/>
					</s:else>	 -->	
					
					<label class="LabelStyle Label1Style">Ghi chú</label>
					<s:if test="id == null || id == 0">
						<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 43.5%; height: 100px; font-size: 13px;" maxlength="500"></textarea>
					</s:if>
					<s:else>
						<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 43.5%; height: 100px; font-size: 13px;" maxlength="500" ><s:property value="equipmentStockVO.note"/></textarea>
					</s:else>
					<div class="Clear"></div>													
				</div>			
			<h2 class="Title2Style">Tập tin đính kèm</h2>
				<div class="SearchInSection SProduct1Form">
					<a href="javascript:void(0);" style="float: left;" title="Lựa chọn tập tin" ><img class="addFileBtn" src="/resources/images/icon_attach_file.png"></a>
					<div class="fileupload-process" style="width: 90%;">
						<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-striped active" id="total-progress" style="opacity: 0; width: 550px;">
							<div data-dz-uploadprogress="" style="width: 200px;" class="progress-bar progress-bar-success"></div>
						</div>
					</div>
					<div style="float:left; min-width: 500px; width: 90%;">
						<div style="height: auto; margin-left:20px;">
							<s:iterator id="fileVo" value="lstFileVo">
								<div class="divImageUploadFile" id = "divEquipAttachFile<s:property value="#fileVo.fileId"/>">
									<div class="imageUpdateShowIcon">
										<img class="addFileBtn" src="/resources/images/img-folder-001.png" width="50" height="50">
									</div>
									<div class="imageUpdateShowContent">
										<label><s:property value="#fileVo.fileName"/></label><br />
										<a href="javascript:void(0)" onclick="EquipmentStockChange.removeEquipAttachFile(<s:property value="#fileVo.fileId"/>);"><img title="Xóa tập tin" src="/resources/images/icon_delete.png" width="12" height="12" style="padding-left: 5px;"></a>
									</div>
								</div>
							</s:iterator>
						</div>
						<div class="Clear"></div>
						<div class="table table-striped" class="files" id="previews" style="min-height: 100px; margin-left:20px;">
							<div id="template" class="file-row" style="margin-bottom: 10px; width: 260px;float: left">
								<div style="margin-right: 10px; width: 50px; float: left">
									<span class="preview"><img data-dz-thumbnail /></span>
								</div>
								<div style="display: inline-block; width: 200px; clear: both">
									<div style="padding-right: 10px">
										<div>
											<p class="name" style="overflow: hidden" data-dz-name></p>
											<strong class="error text-danger" data-dz-errormessage></strong>
										</div>
										<div style="">
											<p class="size" data-dz-size></p>
											</div>
										<div>
											<a data-dz-remove href="javascript:void(0)"><span><img border="0" src="/resources/scripts/plugins/uploadify2.1/cancel.png"></span></a>
										</div>
									</div>								
								</div>							
							</div> 
						</div>
					</div>
					<div class="Clear"></div>
				</div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>	
					<p id="successMsgInfo" class="SuccessMsgStyle" style="display: none"></p>	
				<!-- danh sach bien ban giao nhan -->	
				<h2 class="Title2Style">
					<span>Thiết bị giao nhận</span>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="gridEquipmentStockChange"></table>
					</div>
				</div>
				<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnUpdate" class="BtnGeneralStyle" onclick="EquipmentStockChange.createRecordStockChange();" tabindex="2">Cập nhật</button>						
							<button id="btnClose" class="BtnGeneralStyle" onclick="location.href = '/equipment-stock-change-manage/info';" tabindex="2">Bỏ qua</button>
						</div>
						<div class="Clear"></div>
					</div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- popup thiet bi giao nhan -->
<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchEquipment" class="easyui-dialog"
		title="Chọn thiết bị"
		data-options="closed:true,modal:true" >
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label6Style">Mã thiết bị</label>
				<input id="equipmentCode" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label7Style">Số serial</label>
				<input id="seriNumber" type="text" class="InputTextStyle InputText4Style" maxlength="100"/>
				<label class="LabelStyle Label7Style">Loại</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="typeEquipment" onchange="EquipmentStockChange.onChangeTypeEquip(this);">
						<option value="">Tất cả</option>
					</select>
				</div>
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Nhóm thiết bị</label>
				<div class="BoxSelect BoxSelect3">
					<!-- <select class="MySelectBoxClass" id="groupEquipment" onchange="">
						<option value="">Tất cả</option>
					</select> -->
					<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
					<select id="groupEquipment" style="width:193px !important;"></select>
				</div>
				<label class="LabelStyle Label7Style">Nhà cung cấp</label>
				<div class="BoxSelect BoxSelect3">
					<!-- <select class="MySelectBoxClass" id="providerId" onchange="">
						<option value="">Tất cả</option>
					</select> -->
					<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
					<select id="providerId" style="width:193px !important;"></select>
				</div>					
				<label class="LabelStyle Label7Style">Năm sản xuất</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="yearManufacturing">
					</select>
				</div>				
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Kho (F9)</label>				
					<input type="text" id="stockCode" class="InputTextStyle InputText4Style"/>		
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="EquipmentStockChange.searchEquipmentStock();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="equipmentGridDialogContainer">
					<table id="equipmentGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchEquipment').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!--Popup timf kiem kho nguon - kho dich  -->
<div id ="productDialog" style="width:600px;visibility: hidden;" >
	<div id="easyuiPopupSearchStock" class="easyui-dialog"	title="Tìm kiếm kho"
		data-options="closed:true,modal:true">
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label1Style">Mã đơn vị</label>
				<input id="shopCodePopupStock" type="text" class="InputTextStyle InputText4Style"  maxlength="50" />
				<label class="LabelStyle Label1Style">Tên đơn vị</label>
				<input id="shopNamePopupStock" type="text" class="InputTextStyle InputText4Style"  maxlength="100"/>				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="EquipmentStockChange.searchStockChange();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="stockGridDialogContainer">
					<table id="stockGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchStock').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!-- popup chon thiet bi -->
<div style="display: none">
	<div id="easyuiPopupSelectEquipment" class="easyui-dialog" title="Chọn thiết bị" data-options="closed:true,modal:true" >
		<div class="PopupContentMid">
	   		<div class="SearchInSection SProduct1Form">
				<div class="GridSection" id="gridSelectEquipmentContainer">
					<table id="gridSelectEquipment"></table>
				</div>
			</div>        
		</div>
	</div>
</div>
<div><input type="hidden" id="idChange" name="id" value='<s:property value="id"/>'></div>
<div><s:hidden id="id" name="id"></s:hidden></div>
<div><input type="hidden" id="status"></div>
<script type="text/javascript">
$(document).ready(function() {	
	setDateTimePicker('contractDate');	
	//tamvnm: them ngay lap 07/09/2015
	setDateTimePicker('createDate');
	var createFormDate = '<s:property value="equipmentStockVO.createFormDate"/>';
	if (createFormDate != undefined && createFormDate != null && createFormDate != '') {
		$('#createDate').val(createFormDate);
	} else {
		ReportUtils.setCurrentDateForCalendar("createDate");
	}	
	
	EquipmentStockChange._countFile = Number('<s:property value="lstFileVo.size()"/>');
	$('#fStock').focus();
	 $('#fStock').bind('keyup', function(event){
			var txt = '#fStock';	
			 if(event.keyCode == keyCode_F9){
						VCommonJS.showDialogSearch2({
							dialogInfo: {title:'Tìm kiếm kho'},
							params : {
								status : 1,
								isUnionShopRoot: true
							},
							inputs : [
						        {id:'code', maxlength:50, label:'Mã Đơn Vị'},
						        {id:'name', maxlength:250, label:'Tên Đơn Vị'},
						    ],
						    url : '/commons/search-shop-show-list-NPP',
						    columns : [[
						        {field:'shopName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
						            return row.shopCode + '-' + row.shopName;         
						        }},					
								{field:'shopCode', title:'Mã kho', align:'left', width: 110, sortable:false, resizable:false},
						        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
						            return '<a href="javascript:void(0)" onclick="return EquipmentStockChange.selectStock(\''+txt+'\', \''+row.shopCode+'\', \''+row.id+'\');">chọn</a>';         
						        }},
						    ]]
						});
				
				}
		});
	 EquipmentStockChange._lstEquipStockChangeDel = [];
// 	 $('#tStock').bind('keyup', function(event){
// 		 var txt = '#tStock';	
// 		 if(event.keyCode == keyCode_F9){
// 			 VCommonJS.showDialogSearch2({
// 					inputs : [
// 				        {id:'code', maxlength:50, label:'Mã đơn vị'},
// 				        {id:'name', maxlength:250, label:'Tên đơn vị'},
// 				    ],
// 				    url : '/equipment-stock-change-manage/get-list-equip-stock-vo',
// 				    columns : [[
// 				        {field:'codeName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
// 				        	var nameCode = row.shopCode + '-' + row.shopName;
// 				        	return Utils.XSSEncode(nameCode);         
// 				        }},
// 				        {field:'code', title:'Mã kho', align:'left', width: 200, sortable:false, resizable:false},
// 				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
// 				        	return '<a href="javascript:void(0)" onclick="return EquipmentStockChange.selectStock(\''+txt+'\', \''+row.code+'\');">chọn</a>';            
// 				        }}
// 				    ]]
// 			});
// 		}else if(event.keyCode == keyCodes.ENTER){
// 			var shortCode = $('#tStock').val().trim();
// 			if(!/^[0-9a-zA-Z-_.,]+$/.test(shortCode)){
// 				$('#errMsg').html('Mã Kho nhận không hợp lệ!').show();
// 				return false;
// 			}
// 			var rows = $('#gridEquipmentStockChange').datagrid('getRows');
// 			if (rows != undefined && rows != null && rows.length > 0) {
// 				 $.messager.confirm('Xác nhận', 'Bạn có muốn chọn lại Kho nhận ?', function(r){
// 						if (r){
// 							EquipmentStockChange._toStockDefault = shortCode;
// 							for ( var i = 0, size = rows.length; i < size; i++) {
// 								rows[i].toStock = shortCode;
// 							}
// 							$('#gridEquipmentStockChange').datagrid('loadData', rows );
// 						}
// 					});	
// 			 }else{
// 				EquipmentStockChange._toStockDefault = shortCode;
// 			 }
// 		}
// 	});	
	
	$('#status').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnUpdate').click(); 
		}
	});	
	var params = new Object();	
	params.id = $('#id').val().trim();
	var equipmentStockTransRecordCode = $('#codeStockTran').val();
	params.code = equipmentStockTransRecordCode ? equipmentStockTransRecordCode : '';
	$('#gridEquipmentStockChange').datagrid({
		url : '/equipment-stock-change-manage/search-equipment-stock',
		autoRowHeight : true,
		rownumbers : true, 
		//checkOnSelect :true,
		//selectOnCheck: true,
		singleSelect: true,
		pagination:true,
		fitColumns:true,
		pageSize :20,
		pageList  : [20],
		scrollbarSize:0,
		width: $('#gridContainer').width(),
		queryParams: params,
		autoWidth: true,		
		columns:[[				  
			{field: 'typeEquipment',title:'Loại thiết bị', width: 70, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
		    	var html="";
		    	if(row.typeEquipment!= null){
					html = row.typeEquipment; 
		    	}
				return Utils.XSSEncode(html);
			}},
			{field: 'groupEquipmentCode',title:'Nhóm thiết bị', width: 110, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
		    	var html="";
		    	if(row.groupEquipmentCode!= null){
					html = row.groupEquipmentCode; 
		    	}
				return Utils.XSSEncode(html);
			}},
			{field: 'capacity',title:'Dung tích (lít)',width: 60,sortable:false,resizable:false, align: 'left' },
			{field: 'equipmentCode',title:'Mã thiết bị (F9)',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var html = '<input type="text" id="equipmentCodeInGrid" size="20" maxlength ="50" readonly="true">';
				if(row.equipmentCode!=null && row.equipmentCode != undefined && row.equipmentCode!=''){
// 					if($('#equipmentCodeInGrid').val() != undefined ){
// 						html = '<input type="text" id="equipmentCodeInGrid" size="20" maxlength ="50" value="'+row.equipmentCode+'">';					
// 					}else{
// 						html = row.equipmentCode; 						
// 					}		
					html = Utils.XSSEncode(row.equipmentCode);	
				}
				return html
			}},
			{field: 'seriNumber',title:'Số serial',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var html = '<input type="text" id="seriNumberInGrid" size="20" maxlength ="100">';				
				if(row.seriNumber!=null && row.seriNumber != undefined && row.seriNumber!=''){						
					html = Utils.XSSEncode(row.seriNumber); 
				}					
				return html;		
			}},	
			{field: 'stock',title:'Kho giao',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var html="";
		    	if(row.stock!= null){
					html = row.stock; 
		    	}
		    	return Utils.XSSEncode(html);	
			}},
			{field: 'toStock',title:'Kho nhận(F9)',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
// 				var code = '';
// 				if(EquipmentStockChange._toStockDefault!=null){
// 					code = EquipmentStockChange._toStockDefault;
// 				}
				var html = '<input type="text" id="toStockInGrid"  size="20" maxlength ="100">';	
// 				if(code!=''){
// 					html = '<input type="text" id="toStockInGrid" value='+code+' size="20" maxlength ="100">';	
// 				}
				if(row.toStock!=null && row.toStock != undefined && row.toStock!=''){						
					html = Utils.XSSEncode(row.toStock);
				}					
				return html;		 
			},editor: {
				type:'validatebox',
				options:{ 
					width:100,
					required:true,	
					missingMessage:'Vui lòng chọn Kho nhận!!!'
	    		}
			}},
		    {field: 'healthStatus', title: 'Tình trạng thiết bị', width: 100, sortable:false,resizable:false,align: 'left'},
			{field:'delete', title:'<a href="javascript:void(0);" onclick="EquipmentStockChange.insertEquipmentStockTransInGrid();"><img src="/resources/images/icon_add.png" title="Thêm"/></a>', width:50, align:'center', formatter: function(value, row, index) {
			    	return '<a href="javascript:void(0);" onclick="EquipmentStockChange.deleteEquipmentStockTransInGrid('+index+','+row.idStockRecordDtl+')"><img src="/resources/images/icon-delete.png" title="Xóa"></a>';
			}},
		]],
		onDblClickRow:function(rowIndex, rowData) {
			if (EquipmentStockChange.editIndex != rowIndex){
				if(EquipmentStockChange._editIndex != null){
					var row = $('#gridEquipmentStockChange').datagrid('getRows')[EquipmentStockChange._editIndex];
					if($('#seriNumberInGrid').val() == ''){
						$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
						return false;
					}else if($('#seriNumberInGrid').val() != undefined){
						row.seriNumber = $('#seriNumberInGrid').val();
					}	
					
					if($('#toStockInGrid').val() == ''){
						$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
						return false;
					}else if($('#toStockInGrid').val() != undefined){
						var toStockCode = $('#toStockInGrid').val().trim();							
						row.toStock = $('#toStockInGrid').val();
					}	
					if($('#equipmentCodeInGrid').val() == ''){
						$('#errMsg').html('Bạn chưa xử lý xong dòng thiết bị thêm mới!').show();
						return false;
					}else if($('#equipmentCodeInGrid').val() != undefined){
						isCheckingIfEquipmentCodeValid = true;
						var equipCode = $('#equipmentCodeInGrid').val().trim();							
						var params = {equipCode:equipCode};	
						Utils.getJSONDataByAjaxNotOverlay(params, '/equipment-stock-change-manage/getEquipment', 
									function(equipments){
										if(equipments != undefined && equipments != null && equipments.length > 0) {
											insertNewRow();
											EquipmentStockChange.isHavingInvalidRowOnGrid = false;
										} else{
											$('#errMsg').html('Mã thiết bị nhập không đúng!').show();							
											$('#equipmentCodeInGrid').val() == '';
											$('#equipmentCodeInGrid').focus();
											EquipmentStockChange.isHavingInvalidRowOnGrid = true;									
																		
											return false;
										}								
						}, null, null);	
						row.equipmentCode = $('#equipmentCodeInGrid').val();
					}		
					
					$('#equipmentCodeInGrid').remove();
					$('#seriNumberInGrid').remove();		
					$('#gridEquipmentStockChange').datagrid('updateRow',{
						index: EquipmentStockChange._editIndex,	
						row: row
					});
					if(EquipmentStockChange._lstEquipInsert == null){
						EquipmentStockChange._lstEquipInsert = new Array();
					}
					if(EquipmentStockChange._lstToStockCodeInsert == null){
						EquipmentStockChange._lstToStockCodeInsert = new Array();
					}
					EquipmentStockChange._lstEquipInsert.push(row.equipmentCode);		
					
					$('#gridEquipmentStockChange').datagrid('beginEdit', rowIndex);	
					$('.datagrid-editable-input').attr('id','toStockInGrid');
			    	$('.datagrid-editable-input').bind('keyup', function(e){
						if(e.keyCode==keyCode_F9){			
							$('#errMsg').html('').show();				
							EquipmentStockChange.showPopupSearchStock('toStockInGrid');
						} 
					});	
					$("#gridEquipmentStockChange").datagrid("selectRow", rowIndex);	
					$('#toStockInGrid').focus();
				}
				EquipmentStockChange._editIndex = rowIndex;
			}
		},
		onLoadSuccess :function(data){   			 	    	
	    	$(window).resize();	
	    	if (data == null || data.total == 0) {				
			}else{
				$('.datagrid-header-rownumber').html('STT');				
			}
	    /*	$('#equipmentCode').bind('keyup',function(event){
				if(event.keyCode == keyCodes.ENTER){
					EquipmentStockChange.insertSeri();
				}
			});*/			
		}
	});
	UploadUtil.initUploadFileByEquipment({
		url: '/equipment-stock-change-manage/create-record-stock',
		elementSelector: 'body',
		elementId: 'body',
		clickableElement: '.addFileBtn',
		parallelUploads: 5
	}, function (data) {
		if (!data.error) {
			$("#successMsgInfo").html("Lưu dữ liệu thành công").show();
			var t = setTimeout(function(){
				$('.SuccessMsgStyle').html("").hide();
				window.location.href = '/equipment-stock-change-manage/info';
				clearTimeout(t);
			 }, 1000);
		} else {
			$('#errMsg').html(data.errMsg).show();
		}
	});
});
function changeCombo() {
    var status = $('#status').val().trim();
    var html = '';
    if(status == '2'){ // duyet
    	html = 
    	'<option value="2">Đang thực hiện</option> <option value="1">Hoàn thành</option>';
    }else{
    	html = '<option value="2">Đang thực hiện</option>';
    }
    $('#statusPerform').html(html);
    $('#statusPerform').change();
}
</script>

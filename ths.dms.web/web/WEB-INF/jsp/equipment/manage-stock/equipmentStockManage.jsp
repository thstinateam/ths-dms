<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
#player_api {
	width: 678px !important;
}
</style>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">THIẾT BỊ</a></li>
		<li><span>Quản lý chuyển kho</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
					<div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin tìm kiếm" style="width: 1000px; height:auto; padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
					<div class="SearchInSection SProduct1Form">
						<label class="LabelStyle Label1Style" style=";text-align:right;height:auto;">Mã biên bản</label> 
							<input type="text" class="InputTextStyle InputText1Style" id="codeStockTran"  maxlength="50" tabindex="2"/>
							<label class="LabelStyle Label1Style" style=";text-align:right;height:auto;">Trạng thái thực hiện</label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="statusPerform" tabindex="2">
								<option value="" selected="selected">Tất cả</option>
								<option value="2">Đang thực hiện</option>
						   		<option value="1">Hoàn thành</option>
							</select>
						</div>		
																	
						<label	class="LabelStyle Label1Style">Trạng thái</label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status" tabindex="2">
								<option value="" selected="selected">Tất cả</option>
								<option value="0">Dự thảo</option>
						   		<option value="2">Duyệt</option>
						   		<option value="4">Hủy</option>
							</select>
						</div>		 
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style" style=";text-align:right;;height:auto;">Ngày tạo từ</label>
							<div id="fromDate">
								<input type="text" id="fDate" class="InputTextStyle InputText6Style" tabindex="2" />
							</div>
						<label class="LabelStyle Label1Style">Ngày tạo đến </label>
							<div id="toDate">
								<input type="text" id="tDate" class="InputTextStyle InputText6Style" tabindex="2"/>
							</div>	
							<label class="LabelStyle Label1Style" style=";text-align:right;;height:auto;" >Kho giao (F9)</label>
							 <input type="text"	class="InputTextStyle InputText1Style" id="fStock" maxlength="50" tabindex="2"/> 
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style" style=";text-align:right;;height:auto;">Ngày biên bản từ</label>
							<div id="cFromDate">
								<input type="text" id="cFDate" class="InputTextStyle InputText6Style" tabindex="2" />
							</div>
						<label class="LabelStyle Label1Style">Ngày biên bản đến </label>
							<div id="cToDate">
								<input type="text" id="cTDate" class="InputTextStyle InputText6Style" tabindex="2"/>
							</div>	
							<label class="LabelStyle Label1Style">Kho nhận (F9)</label> 
							<input	type="text" class="InputTextStyle InputText1Style" id="tStock" maxlength="50" tabindex="2"/>
															
						<div class="Clear"></div>						
					</div>
					<div class="SearchInSection SProduct1Form">
				<div class="Clear"></div>
					<div class="BtnCenterSection">
                        <button class="BtnGeneralStyle"  tabindex="2" id="btnSearch" onclick="return EquipmentStockChange.searchStockChange();">Tìm kiếm</button>
                    </div>					
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
					<p class="ErrorMsgStyle" id="errMsgSearch" style="display: none;"></p>
					<div class="Clear"></div>
				</div>
				</div>
				<h2 class="Title2Style">Danh sách biên bản chuyển kho</h2>
				<div style="float:right; margin-top: -25px;margin-right: 27px;">
						<!-- <a href="javascript:void(0);" onclick=""><img src="/resources/images/icon-printer.png" height="20" width="20"  title="In"></a>	 -->
<!-- 						<a style="margin-right: 10px;" href="javascript:void(0);" onclick="return EquipmentStockChange.printRecodeStockTran();"><img src="/resources/images/icon-printer.png" title="In" height="20" width="20"></a> -->
						<a href="#" class="downloadTemplateReport"><img src="/resources/images/icon_download_32.png" height="20" width="20" title="Tải mẫu Nhập Excel"></a>
						<a href="javascript:void(0);" onclick="EquipmentStockChange.showDlgCatalogImportExcel();"><img src="/resources/images/icon-import.png" height="25" width="25" title="Nhập Excel"></a>
						<a href="javascript:void(0);" onclick="EquipmentStockChange.exportExcel();"><img src="/resources/images/icon-export.png" height="25" width="25" title="Xuất Excel"></a>
						 <button style="float:right;" class="BtnGeneralStyle" id="btnDone" onclick="return EquipmentStockChange.donePerformStatusStockChange();">Hoàn thành</button>
						 <button style="float:right;margin-right: 5px;" class="BtnGeneralStyle" id="btnSearch" onclick="return EquipmentStockChange.saveStockChange();">Lưu</button>				
						<div style="float:right;margin-right: 5px;" class="BoxSelect BoxSelect11">							
							<select class="MySelectBoxClass" id="statusRecordLabel" style="width:112px;">
								<option value="-2" selected="selected">Chọn</option>
								<option value="2" >Duyệt</option>
								<option value="4" >Hủy</option>
							</select>
						</div>						
					</div>
				<div class="SearchInSection SProduct1Form">
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id=gridContainer>
							<table id="gridStockChange"></table>
						</div>
					</div>	
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!--Popup tim kiem kho nguon - kho dich  -->
<div id ="productDialog" style="width:600px;visibility: hidden;" >
	<div id="easyuiPopupSearchStock" class="easyui-dialog"	title="Tìm kiếm kho"
		data-options="closed:true,modal:true">
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label1Style">Mã đơn vị</label>
				<input id="shopCodePopupStock" type="text" class="InputTextStyle InputText4Style" tabindex="15" maxlength="50" />
				<label class="LabelStyle Label1Style">Tên đơn vị</label>
				<input id="shopNamePopupStock" type="text" class="InputTextStyle InputText4Style" tabindex="15" maxlength="100"/>				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="EquipmentStockChange.searchStock();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="stockGridDialogContainer">
					<table id="stockGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchStock').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Import - Chuyển kho" data-options="closed:true,modal:true" style="width:470px;">
		<div class="PopupContentMid">
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-stock-change-manage/import-excel" name="importFrmCatalog" id="importFrmCatalog" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style">File excel</label>
				    <div class="UploadFileSection Sprite1">
						<input id="fakefilepcCatalog" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileCatalog" onchange="previewImportExcelFile(this,'importFrmCatalog','fakefilepcCatalog', 'errExcelMsgCatalog');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a onclick="" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="uploadExcelFile" onclick="return EquipmentStockChange.importExcelCatalog();">Tải lên</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').window('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgCatalog"/>
	</div>	
</div>
<script type="text/javascript">
$(document).ready(function() {	
	 $('#fDate, #tDate, #code').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	 }); 	
	 $('#tStock').bind('keyup', function(event) {
		 if(event.keyCode == keyCode_F9){
			 EquipmentStockChange.showPopupSearchStock('tStock');
		}
	});
	 $('#fStock').bind('keyup', function(event) {
		 if(event.keyCode == keyCode_F9){
			 EquipmentStockChange.showPopupSearchStock('fStock');
		}
	});
// 	 $('#tStock').bind('keyup', function(event) {
// 		 var txt = '#tStock';	
// 		 if(event.keyCode == keyCode_F9){
// 					VCommonJS.showDialogSearch2({
// 						dialogInfo: {title:'Tìm kiếm kho'},
// 						params : {
// 							status : 1,
// 							isUnionShopRoot: true
// 						},
// 						inputs : [
// 					        {id:'code', maxlength:50, label:'Mã Đơn Vị'},
// 					        {id:'name', maxlength:250, label:'Tên Đơn Vị'},
// 					    ],
// 					    url : '/commons/search-shop-show-list-NPP',
// 					    columns : [[
// 					        {field:'shopName', title:'Đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter:function(value,row,index) {
// 					            return row.shopCode + '-' + row.shopName;         
// 					        }},					
// 							{field:'shopCode', title:'Mã kho', align:'left', width: 110, sortable:false, resizable:false},
// 					        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
// 					        	 return '<a href="javascript:void(0)" onclick="return EquipmentStockChange.selectStock(\''+txt+'\', \''+row.shopCode+'\');">chọn</a>';    
// 					        }}
// 					    ]]
// 					});
// 				$('#code').focus();
// 			}
// 		});
	 $('#panelDeliveryRecord').width($(window).width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
		$('#codeStockTran').focus();
	},1000);
	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	setDateTimePicker('cFDate');
	setDateTimePicker('cTDate');	
	$("#fDate").val('<s:property value="fromDate"/>');
	ReportUtils.setCurrentDateForCalendar("tDate");	 
// 	ReportUtils.setCurrentDateForCalendar("cFDate");
// 	ReportUtils.setCurrentDateForCalendar("cTDate");	 
	

	var params = new Object();	
	params.code = $('#codeStockTran').val().trim();	
	params.status = $('#status').val();	
	params.fromDate = $('#fDate').val().trim();
	params.toDate =  $('#tDate').val().trim();
	params.createFromDate = $('#cFDate').val().trim();
	params.createToDate = $('#cTDate').val().trim();
	$('#gridStockChange').datagrid({
		url : "/equipment-stock-change-manage/searchEquipmentStockChange",
		autoRowHeight : true,
		rownumbers : true, 
		checkOnSelect :true,		
		pagination:true,
		pageSize :20,
		pageList : [20],
		scrollbarSize:0,
		fitColumns:true,
		width:  $('#gridContainer').width(),
		queryParams: params,
		columns:[[		  
		    {field: 'id', index: 'id', hidden: true},
			{field: 'check',checkbox:true, width: 100, sortable:false,resizable:false, align: 'left' },
		    {field:'code', title: 'Mã biên bản', width: 120, sortable: false, resizable: true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field: 'creatDate',title:'Ngày tạo',width: 100,sortable:false,resizable:false, align: 'center' , formatter:function(value,row,index) {
		    	return row.creatDate;
		    }},
		    {field: 'createFormDate',title:'Ngày biên bản',width: 100,sortable:false,resizable:false, align: 'center' , formatter:function(value,row,index) {
		    	return Utils.XSSEncode(row.createFormDate);
		    }},
		    {field:'fromStock', title: 'Các kho giao', width: 300, sortable: false, resizable: true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field:'toStock', title: 'Các kho nhận', width: 300, sortable: false, resizable: true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    
		    {field: 'performStatus', title: 'Trạng thái thực hiện', width: 150, sortable:false,resizable:false,align: 'left',formatter: function(value, row, index){
		    	var html='';
		    	if(row.performStatus == 2){
		    		html+='Đang thực hiện';
		    	}else if(row.performStatus == 1){
		    		html+='Hoàn thành';
		    	}
		    	return html;
		    }},
		    {field: 'status', title: 'Trạng thái biên bản', width: 150, sortable:false,resizable:false,align: 'left',formatter: function(value, row, index){
		    	var html='';
		    	if(row.status == 4){
		    		html+='Hủy';
		    	}else if(row.status == 2){
		    		html+='Duyệt';
		    	}else if(row.status == 0){
		    		html+='Dự thảo';
		    	}
		    	return html;
		    }},
		    {field:'file', title:'Tập tin đính kèm', width:70, sortable: false, resizable: false, align:'center', formatter: function(value, row, index){
		    	/*type= 3: la file bao mat*/
		    	if (row != undefined && row.url != undefined && row.url != null && row.url != '') {
		    		return '<a href="javascript:void(0)" onclick="General.downloadEquipAttechFile('+row.id+', 1);">Tập tin</a>';
		    	} else {
		    		return '';
		    	}
		    }},
		    {field: 'note', title: 'Ghi chú', width: 200, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},	  
			{field:'edit', title:'<a href="/equipment-stock-change-manage/viewEquipStockChange?id=0"><img title="Thêm mới" src="/resources/images/icon_add.png"/></a>', width:60, align:'center', formatter: function(value, row, index) {
// 			    var html='<a href="javascript:void(0);" onclick="return EquipmentStockChange.viewRecodeStockTran('+row.id+');"><img src="/resources/images/icon_print_view_34.png" height="20" width="20" style="margin-right:5px;" title="Xem chi tiết"></a>';
			    var html='';
			    if(row.status == 0){
			    	html+='<a href="/equipment-stock-change-manage/viewEquipStockChange?id=' + row.id +'"><img width="16" style="" heigh="16" src="/resources/images/icon-edit.png" title="Xem thông tin"></a>';	    		
		    		
			    }else {
			    	if(row.status==2 || row.status==4){
			    		html+='<a href="/equipment-stock-change-manage/viewEquipStockChangeApproved?id=' + row.id +'"><img width="16" style="" heigh="16" src="/resources/images/icon-view.png" title="Xem thông tin"></a>';
			    	}
			    }
				return html;
			}},
		]],
		onBeforeLoad:function(param){											 
		},
		onLoadSuccess :function(data){  	 		    	
	    	$(window).resize();
	    	if (data == null || data.total == 0) {
			}else{
				$('.datagrid-header-rownumber').html('STT');				
			}	    	
		}
	});
	$('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Impt_ChuyenKho.xlsx');
});
</script>
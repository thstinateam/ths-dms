<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Thiết bị</a>
		</li>
		<li><span>Báo mất từ máy tính bảng</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm <span style="float:right;margin-right:15px;">
						<a href="javascript:void(0);" title="Ẩn/ hiện" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
				<div class="SearchInSection SProduct1Form"
					style="padding-left: 0px;">
					<!-- <div id="panelSearchDiv" class="easyui-panel" title="Thông tin tìm kiếm" style="width:1000px; height:205px; padding-top: 20px;" data-options="collapsible:true,closable:false,border:false"> -->
					<label class="LabelStyle Label17Style">Đơn vị (F9) </label> <input
						id="txtShopCode" type="text" onchange=""
						class="InputTextStyle InputText1Style" maxlength="50"
						value='<s:property value="shopCode"/>' tabindex="1" />
						
						<label class="LabelStyle Label17Style">Khách hàng</label> <input
						id="txtShortCode" type="text"
						class="InputTextStyle InputText1Style" 	autocomplete="off" maxlength="300"
						placeholder="Nhập mã, tên hoặc địa chỉ KH"
						tabindex="2" />
						
						<div class="Clear"></div> 
						<label class="LabelStyle Label17Style">Từ ngày</label> <input type="text"
						id="txtFromDate"
						class="InputTextStyle InputText1Style vinput-date" tabindex="3" />
					<label class="LabelStyle Label17Style">Đến ngày</label> <input
						type="text" id="txtToDate"
						class="InputTextStyle InputText1Style vinput-date" tabindex="4" />
					

					<!-- 					<label class="LabelLeftStyleM4 LabelLeft2Style">Mã KH</label> -->
					<!-- 					<input id="txtShortCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="6" /> -->
					<!-- 					<label class="LabelStyle Label1Style">Tên KH</label> -->
					<!-- 					<input id="txtCustomerName" type="text" class="InputTextStyle InputText1Style" maxlength="250" tabindex="7"/> -->
					<!-- 					<label class="LabelStyle Label1Style">Địa chỉ</label> -->
					<!-- 					<input id="txtAddress" type="text" class="InputTextStyle InputText1Style" maxlength="250" tabindex="8" /> -->

					

					<div class="Clear"></div>
					<label class="LabelStyle Label17Style">Phiếu báo mất</label>
					<div class="BoxSelect BoxSelect2" id="ddlStatusEquipRecordDiv">
						<select class="MySelectBoxClass" id="processingStatus" tabindex="9">
							<option value="-2" selected="selected">Tất cả</option>
							<option value="1">Đã có</option>
							<option value="2">Chưa có</option>
						</select>
					</div>
					<label class="LabelStyle Label17Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2" id="ddlDeliveryStatusDiv">
						<select class="MySelectBoxClass" id="statusEquipRecord" tabindex="10">
							<option value="-1" selected="selected">Tất cả</option>
							<option value="0">Chưa xử lý</option>
							<option value="1">Đã xử lý</option>
						</select>
					</div>
					<div class="Clear"></div>
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnSearch" class="BtnGeneralStyle"  tabindex="11">Tìm kiếm</button>
						</div>
						<div class="Clear"></div>
					</div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
					<!-- </div>	 -->
				</div>
				<div class="Title2Style" style="height: 15px; font-weight: bold;">
					<span style="float: left;">Danh sách báo mất</span>
					<div style="float: right; margin-top: -5px; margin-right: 27px;">
						<button id="btnSaveLst" class="BtnGeneralStyle">Hủy</button>
					</div>
				</div>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="lostRecordDgContainer">
						<table id="lostRecordDg"></table>
					</div>
				</div>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="lostRecordDgContainerDetail">
						<table id="lostRecordDgDetail"></table>
					</div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	$('#txtFromDate, #txtToDate').width($('#txtFromDate').width() - 22);
	/*$('#panelSearchDiv').width($(window).width()- 20);
	$('.panelSearchDiv input').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click();
		}
	});*/
	$('.GeneralSSection input').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click();
		}
	});
	$('#ddlStatusEquipRecordDiv, #ddlDeliveryStatusDiv, #txtSerial').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});
	//$('#panelSearchDiv').width($('#panelSearchDiv').parent().width());
	//$("#txtFromDate").val(getLastWeek());//Lay truoc 7 ngay
	//ReportUtils.setCurrentDateForCalendar("txtToDate");    

	var ProcessingStatus =  {		
		parseValue: function(value){
			if(value != null && value != undefined && value > 0){
				return 'Đã có';
			}
			return 'Chưa có';
		}
	};

						var StatusEquipRecord = {
							CHUA_XU_LY : 0,
							DA_XU_LY : 1,
							parseValue : function(value) {
								if (value == StatusEquipRecord.DA_XU_LY) {
									return 'Đã xử lý';

			}else if(value == StatusEquipRecord.CHUA_XU_LY){
				return 'Chưa xử lý';
			}
		}
	};
    
    $('#lostRecordDg').datagrid({
		url: '/equipLostRecord/mobile/search',
		width : $(window).width() - 50,	    
		queryParams: {
			shopCode : $('#txtShopCode').val().trim(), // vuongmq; 28/05/2015; cap nhat them shopCode
			fromDateStr : $('#txtFromDate').val().trim(),
			toDateStr : $('#txtToDate').val().trim()			
		},
        autoWidth: true,        
		pagination : true,
        rownumbers : true,
        fitColumns : true,
        autoRowHeight : true,
        pageNumber : 1,
        scrollbarSize: 15,
		height: 378,
		pageSize: 20,
        singleSelect : true,
        pageList: [20, 50],
		columns:[[
			 {field:'id', title:'', width:25, align:'center', formatter: function(value, row, index){
			 	if(row.recordStatus == StatusEquipRecord.CHUA_XU_LY){
			 		var html = '<input type="checkbox" value="{0}" id="" class="class-id" />';
			 		return format(html,value);	
			 	}
		    	
		    }},		
		    {field:'shopCode', title:'Đơn vị', width:140, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	var html = '';
		    	if (row.shopCode != null && row.shopName != null) {
		    		html = row.shopCode + ' - ' + row.shopName;
		    	} else if (row.shopCode != null) {
		    		html = row.shopCode;
		    	} else if (row.shopName != null) {
		    		html = row.shopName;
		    	}
		    	return Utils.XSSEncode(html);
		    }}, 
		    {field:'stockCodeName', title:'Khách hàng', width:165, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
// 		    {field:'stockName', title:'Tên khách hàng', width:120, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
// 		    	return Utils.XSSEncode(value);
// 		    }},
		    {field:'stockAddress', title:'Địa chỉ', width:200, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'staffCodeName', title:'NVBH', width:150, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'equipLostId', title:'Phiếu báo mất', width:60, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return ProcessingStatus.parseValue(value);
		    }},
		    {field:'recordStatus', title:'Trạng thái', width:50, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return StatusEquipRecord.parseValue(value);
		    }},		    
		    {field:'lostDate', title:'Ngày mất', width:60, sortable: false, resizable: false, align:'center',formatter : CommonFormatter.dateTimeFormatter},
		    {field:'change', title:'', width:30, align:'center', fixed:true, formatter: function(value, row, index) {
		    	var html = '<a onclick="EquipLostRecord.equipmentDetail({0},\'{1}\', \'{2}\')" href="javascript:void(0);" ><img  title="Xem chi tiết" src="/resources/images/icon-view.png" /></a>';
		    	return format(html,row.equipId,CommonFormatter.dateTimeFormatter(row.lostDate), CommonFormatter.dateTimeFormatter(row.lastArisingSalesDate));
		    }},
		    {field:'added', title:'', width:30, align:'center', fixed:true, formatter: function(value, row, index) {
		    	if((row.equipLostId == null  || row.equipLostId <0) && row.recordStatus == StatusEquipRecord.CHUA_XU_LY){
		    		var customerId = 0;
		    		if (row.customerId != undefined && row.customerId != null) {
		    			customerId = row.customerId;
		    		}
		    		var equipId = 0;
		    		if (row.equipId != undefined && row.equipId != null) {
		    			equipId = row.equipId;
		    		}
		    		var html = '<a href="/equipLostRecord/equipLostRecordChangeJSP?eqLostMobiRecId='+row.id+'&customerId='+customerId+'&equipId='+equipId+'" ><img  title="Tạo biên bản báo mất" src="/resources/images/icon_add.png" /></a>';	
		    		return format(html, row.id);
		    	}	    	
		    	
		    }}
		]],
		onClickRow:function(rowIndex, rowData) {
			var rowCbx = $('[name=id]')[rowIndex];			
			if(rowCbx!=undefined && rowCbx!=null && !rowCbx.checked){
				$('#lostRecordDg').datagrid('unselectRow', rowIndex);
			}else{
				$('#lostRecordDg').datagrid('selectRow', rowIndex);
			}
			return true;
		},
        onLoadSuccess :function(data){
			$('#lostRecordDgContainer .datagrid-header-rownumber').html('STT');
			Utils.updateRownumWidthAndHeightForDataGrid('lostRecordDg');
		 	$(window).resize();
		 	var row = $( '#lostRecordDgContainer input[type="checkbox"]');
		 	if (row == undefined || row == null || row.length < 1) {
		 		$('#lostRecordDg').datagrid("hideColumn", "id");
		 	} else {
		 		$('#lostRecordDg').datagrid("showColumn", "id");
		 	}
        }
	});
    $('#txtCode').focus();
    $('#btnSearch').bind('click',function(){
    	$('#lostRecordDgContainerDetail').hide();
    	var params = new Object();
    	params.shopCode = $('#txtShopCode').val().trim(); // vuongmq; 28/05/2015; cap nhat them shopCode
    	params.fromDateStr = $('#txtFromDate').val().trim();
    	params.toDateStr = $('#txtToDate').val().trim();

    	params.stockCode = $('#txtShortCode').val().trim();
//     	params.customerName = $('#txtCustomerName').val().trim();
//     	params.address = $('#txtAddress').val().trim();
    	params.recordStatus = $('#statusEquipRecord').val().trim();
    	params.processingStatus = $('#processingStatus').val().trim();
    	$('#lostRecordDg').datagrid('reload',params);    	
    });
    $('#btnSaveLst').bind('click',function(){    	
    	var params = new Object();
    	var listLostRecordMobile = new Array();
    	$('.class-id:checked').each(function(){    		
    		var id = $(this).val().trim();
    		listLostRecordMobile.push(id);
    	});
    	params.listLostRecordMobile = listLostRecordMobile;
    	
    	Utils.addOrSaveData(params,'/equipLostRecord/mobile/update',null,'errMsg',function(data){
    		$('#lostRecordDg').datagrid('reload');
    		$("#successMsg").html("Lưu dữ liệu thành công").show();
			var t = setTimeout(function(){
				$('.SuccessMsgStyle').html("").hide();
				clearTimeout(t);
			 }, 1500);
    	}, null, null, null);
    });
    
    /** vuongmq; 28/05/2015; truong hop co shopCode la user Chi quan ly 1 shop; khi them moi thi disabled ma shopCode luon*/
	var flagShopCode = '<s:property value="shopCode"/>';
	if (flagShopCode != undefined && flagShopCode != null && flagShopCode != '') {
		disabled('txtShopCode'); //vuongmq 19/05/2015
		//$('#txtShopCode').val(flagShopCode);
	}
    /* vuongmq; 28/05/2015; show popup NPP phan quyen CMS*/
	$('#txtShopCode').bind('keyup', function(event) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if (event.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2({
				dialogInfo: {
					title: 'Tìm kiếm đơn vị'
				},
				/*params : {
					shopCode : $('#txtShopCode').val().trim(),
					status : activeType.RUNNING
				},*/
				inputs : [
				{id:'code', maxlength:50, label:'Mã đơn vị'},
				{id:'name', maxlength:250, label:'Tên đơn vị'},
				      ],
				url : '/commons/search-shop-show-list-NPP-GT-MT',
				columns : [[
					{field:'shopCode', title:'Mã đơn vị', align:'left', width: 120, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.shopCode);
			        }},
					{field:'shopName', title:'Tên đơn vị', align:'left', width: 180, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.shopName);
			        }},
					{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
						return '<a href="javascript:void(0)" onclick="EquipLostRecord.fillTextBoxByShopCode(\''+row.shopCode+'\',\''+row.shopId+'\')">chọn</a>';         
					}}
				]]
			});
		}
	});
    setTimeout(function() {
		$('#btnSearch').click(); 
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 5px;");
    	$('.panel').width($(window).width()- 10);
	}, 1000);
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!-- Begin vuongmq; 07/05/2015; ATTT xuat file -->
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<!-- End vuongmq; 07/05/2015; ATTT xuat file -->
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipLostRecord/approve">Thiết bị</a>
		</li>
		<li><span>Phê duyệt Báo mất</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm <span style="float:right;margin-right:15px;">
						<a href="javascript:void(0);" title="Ẩn/ hiện" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
				<div class="SearchInSection SProduct1Form" style="padding-left: 0px;">
				<!-- <div id="panelSearchDiv" class="easyui-panel" title="Thông tin tìm kiếm" style="width:1000px; height:265px; padding-top: 20px;" data-options="collapsible:true,closable:false,border:false"> -->
					<label class="LabelStyle Label3Style">Đơn vị <span class="ReqiureStyle"> *</span></label>
					<input id="txtShop" type="text"  style="height: auto;" class="InputTextStyle InputText1Style" tabindex="1" />
					<label class="LabelStyle Label1Style">Báo mất cho</label>
					<div class="BoxSelect BoxSelect2" id="ddlStockTypeDiv">
						<select class="MySelectBoxClass" id="cboStockType" tabindex="2" >
				       		<option value="-2" selected="selected">Tất cả</option>
							<option value="1" >NPP</option>
							<option value="2" >Khách hàng</option>
						</select>
					</div>
					<label class="LabelStyle Label1Style">Khách hàng</label> <input
						id="txtShortCode" type="text"
						class="InputTextStyle InputText1Style" 	autocomplete="off" maxlength="300"
						placeholder="Nhập mã, tên hoặc địa chỉ KH"
						tabindex="3" />
					<div class="Clear"></div>
					<label class="LabelStyle Label3Style">Ngày biên bản từ</label>
					<input type="text" id="txtFromDate" class="InputTextStyle InputText1Style vinput-date" tabindex="4"/>
					<label class="LabelStyle Label1Style">Đến</label>
					<input type="text" id="txtToDate" class="InputTextStyle InputText1Style vinput-date" tabindex="5"/>
					<label class="LabelStyle Label1Style" style="margin-left: -0.2%">Trạng thái biên bản</label>
					<div class="BoxSelect BoxSelect2" id="ddlStatusEquipRecDiv">
						<select class="MySelectBoxClass" id="ddlStatusEquipRecord" tabindex="6">
							<option value="1" >Chờ duyệt</option>
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label3Style">Mã phiếu</label>
					<input id="txtCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="7"/>
					<label class="LabelStyle Label1Style">Mã thiết bị</label>
					<input id="txtEquipCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="8"/>
					<label class="LabelStyle Label1Style">Số serial</label>
					<input type="text" id="txtSerial" class="InputText1Style" style="float: left; border: 1px solid #c4c4c4; height: 20px; line-height: 20px; margin-bottom: 9px; padding: 0 4px;" tabindex="9" maxlength="50"/>
					<div class="Clear"></div>
<!-- 					<label class="LabelLeftStyleM3 LabelLeft3Style">Mã KH</label> -->
<!-- 					<input id="txtShortCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" tabindex="9" /> -->
<!-- 					<label class="LabelStyle Label1Style">Tên KH</label> -->
<!-- 					<input id="txtCustomerName" type="text" class="InputTextStyle InputText1Style" maxlength="250" tabindex="10"/> -->
<!-- 					<label class="LabelStyle Label1Style">Địa chỉ</label> -->
<!-- 					<input id="txtAddress" type="text" class="InputTextStyle InputText1Style" maxlength="250" tabindex="11" /> -->
<!-- 					<div class="Clear"></div> -->
					
					<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection">
							<button id="btnSearch" class="BtnGeneralStyle" onclick="EquipLostRecord.searchListEquipLostRecord(EquipLostRecord._DUYET);" tabindex="11">Tìm kiếm</button>
						</div>
						<div class="Clear"></div>
					</div>										
				<!-- </div>	 -->	
				</div>	
				<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
				<p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
				<div class="Title2Style" style="height:15px; font-weight: bold;">
					<span style="float:left;">Danh sách biên bản</span>
					<div id="divControlByGrid" style="float:right; margin-top: -5px; margin-right: 27px;">
						<a id="eqLostRec_printf_All" class ="cmsiscontrol" onclick="EquipLostRecord.printEquipLostRec(-1);" href="javascript:void(0);"><img width="20" height="20" title="Print" src="/resources/images/icon-printer.png"></a>
						<a id="eqLostRec_import_teamplate" class ="cmsiscontrol" href="/resources/templates/equipment/Bieu_Mau_Nhap_Excel_Bien_Ban_Bao_Mat.xlsx" class="downloadTemplateReport"><img src="/resources/images/icon_download_32.png" height="20" width="20" title="Tải mẫu Nhập Excel"></a>
						<a id="eqLostRec_import_openDialog" class ="cmsiscontrol" href="javascript:void(0);" onclick="EquipLostRecord.openDialogImportEquipLostRecord();"><img src="/resources/images/icon_table_import.png" height="22" width="22" title="Nhập Excel"></a>
						<a id="eqLostRec_export_Excel" class ="cmsiscontrol" href="javascript:void(0);" onclick="EquipLostRecord.exportBySearchEquipLostRecord();"><img src="/resources/images/icon_table_export.png" height="22" width="22" title="Xuất Excel"></a>
<%-- 					<s:if test='isLevel!= null && isLevel == 5'> --%>
<%-- 					</s:if>	 --%>
						<!-- <button id="btnChangeByGridSave" style="background: none repeat scroll 0 0 #eee; border: 1px solid #ccc; float: right; font-size: 11px;" id="btnSaveLst" class="BtnGeneralStyle" onclick="EquipLostRecord.changeByListEquipLostRecordApprove();" >Lưu</button> -->
						<button id="btnChangeByGridSave" style="float: right;" id="btnSaveLst" class="BtnGeneralStyle" onclick="EquipLostRecord.changeByListEquipLostRecordApprove();" >Lưu</button>
						
						<div style="float:right;margin-right: 5px;" class="BoxSelect BoxSelect6" id="permisionChangeApprove_ddlStatusEqRecImportDiv">
							<select class="MySelectBoxClass" id="ddlStatusEquipRecordImport" style="width:112px;" onchange="EquipLostRecord.changeDdlStatusEqRecImpPBrowser(this);" > 
								<option value="-2" selected="selected">Chọn</option>
								<option value="2">Duyệt</option>
								<option value="3">Không duyệt</option>
							</select>
						</div>
					</div>
				</div>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="lostRecordDgContainer">
						<table id="lostRecordDg"></table>
					</div>
				</div>							
			</div>
			<div class="Clear"></div>
		</div> <!-- end class="ToolBarSection" -->
	</div>
	<div class="Clear"></div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="eqLostRec_import_easyuiPopupImportExcel" class="easyui-dialog" title="Nhập Excel Biên bản báo mất" data-options="closed:true,modal:true" style="width:465px;height:250px;">
		<div class="PopupContentMid">
			<div class="SearchInSection SProduct1Form" style="border: none;">
				<form action="/equipLostRecord/importEquipLostRecord" name="importExcelFrm" id="importExcelFrm" method="post" enctype="multipart/form-data">						            	
			        <label class="LabelStyle Label1Style" style="padding-right: 10px; text-align: right; width: 80px;">Tập tin Excel</label>
				    <div class="UploadFileSection Sprite1" style="width: 270px;">
						<input id="fakefileExcelpc" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" style="width: 270px;" size="1" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importExcelFrm','fakefileExcelpc', 'errImpExcelMsg');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a href="/resources/templates/equipment/Bieu_Mau_Nhap_Excel_Bien_Ban_Bao_Mat.xlsx" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="EquipLostRecord.importEquipLostRec();">Nhập Excel</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#eqLostRec_import_easyuiPopupImportExcel').dialog('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p id="successImpExcelMsg" class="SuccessMsgStyle" style="display:none;"></p>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errImpExcelMsg" />
	</div>	
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	EquipLostRecord._isLevelShopRoot = '<s:property value="isLevel"/>';
	//EquipLostRecord.createDdlDeliveryStatusSearch(-1, Number(EquipLostRecord._isLevelShopRoot));
	//EquipLostRecord.createDdlStatusEquipRecordSearch(-1, Number(EquipLostRecord._isLevelShopRoot));
	$('#txtFromDate, #txtToDate').width($('#txtFromDate').width() - 22);
	/*$('#panelSearchDiv').width($(window).width()- 20);
	$('#panelSearchDiv input').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click();
		}
	});*/
	$('.GeneralSSection input').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click();
		}
	});
	$('#ddlStatusEquipRecordDiv, #ddlDeliveryStatusDiv, #txtSerial').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnSearch').click(); 
		}
	});
	
	//$('#panelSearchDiv').width($('#panelSearchDiv').parent().width());
	
	$("#txtFromDate").val(getLastWeek());//Lay truoc 7 ngay
	ReportUtils.setCurrentDateForCalendar("txtToDate");	
	$("#txtShop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 3) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 4 || level == 21) {
					return '<div class="tree-kenh" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-kenh-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-kenh-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 18 || level == 22){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 6){
				return '<div class="tree-vung-con" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-con-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-con-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });
    var shopKendo = $("#txtShop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");
    //Khoi tao luu vet tim kiem; /*** 15/05/2015; tim kiem phe duyet o cho duyet */
    EquipLostRecord.createParamanterBySearch(EquipLostRecord._DUYET);
   
    $('#lostRecordDg').datagrid({
		url: '/equipLostRecord/searchEquipLostRecordVO',
		width : $('#lostRecordDgContainer').width() - 25,
	    height: 378,
		queryParams: EquipLostRecord._params,
        autoWidth: true,
		pagination : true,
        rownumbers : true,
        //fitColumns : true,
        autoRowHeight : true,
		pageSize: 20,
        pageNumber : 1,
        scrollbarSize: 15,
        pageList: [20],
        checkOnSelect: false, // vuongmq; 19/05/2015; chi cho check checkox

        frozenColumns:[[		
			{field:'id',checkbox:true},
		    {field:'changeRow', title:'', width:40, align:'center', fixed:true, formatter: function(value, row, index) {
				 //hidden: true,
	    		return '<a id="eqLostRec_change_'+row.id+'" href="/equipLostRecord/equipLostRecordChangeJSPApprove?id='+row.id+'"><img title="Chỉnh sửa" src="/resources/images/icon-edit.png" width="18" heigh="18"></a>';		    		
		    }},
		    {field:'statusStr', title:'', width:40, sortable: false, resizable: false, align:'center', formatter: function(value, row, index){
		    	return '<a id="eqLostRec_printf_'+row.id+'" href="javascript:void(0)" onclick="EquipLostRecord.printEquipLostRec('+index+');"><img title="In biên bản" src="/resources/images/icon-printer.png" width="18" heigh="18" ></a>';
		    }},
			{field:'shopCodeName', title:'NPP', width:200, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				return Utils.XSSEncode(value);
			}},  
		    {field:'lostDateStr', title:'Ngày biên bản', width:70, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'code', title:'Mã phiếu', width:120, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'equipCode', title:'Mã thiết bị', width:180, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		]],
		columns:[[
		    {field:'serial', title:'Số serial', width:120, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'shortCodeName', title:'Khách hàng', width:220, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
// 		    {field:'customerName', title:'Tên khách hàng', width:150, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
// 		    	return Utils.XSSEncode(value);
// 		    }},
		    {field:'address', title:'Địa chỉ', width:200, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'recordStatus', title:'Trạng thái', width:70, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return statusRecordsEquip.parseValue(value);
		    }},
		    {field:'deliveryStatus', title:'Trạng thái giao nhận', width:70, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return '<a href="javascript:void(0)" onclick="General.openDialogListHistoryEquipRecord('+row.id+', 3);">'+deliveryStatus.parseValue(value)+'</a>';
		    }},
		    {field:'file', title:'Tập tin đính kèm', width:70, sortable: false, resizable: false, align:'center', formatter: function(value, row, index){
		    	/*type= 3: la file bao mat*/
		    	if (row != undefined && row.url != undefined && row.url != null && row.url != '') {
		    		return '<a href="javascript:void(0)" onclick="General.downloadEquipAttechFile('+row.id+', 3);">Tập tin</a>';
		    	} else {
		    		return '';
		    	}
		    }},
		    {field: 'note', title: 'Ghi chú', width: 200, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},
		    //equipLostRecordChangeJSPApprove
		]],
		onClickRow:function(rowIndex, rowData) {
			var rowCbx = $('[name=id]')[rowIndex];
			if(rowCbx!=undefined && rowCbx!=null && !rowCbx.checked){
				$('#lostRecordDg').datagrid('unselectRow', rowIndex);
			}else{
				$('#lostRecordDg').datagrid('selectRow', rowIndex);
			}
			return true;
		},
        onLoadSuccess :function(data){
			$('#lostRecordDgContainer .datagrid-header-rownumber').html('STT');
			//Utils.updateRownumWidthAndHeightForDataGrid('lostRecordDg');
		 	$('#lostRecordDg').datagrid('uncheckAll').datagrid('unselectAll');
			/**if (Number(EquipLostRecord._isLevelShopRoot) == ShopDecentralizationSTT.VNM) {
				$('#lostRecordDg').datagrid("hideColumn", "changeRow");
				$('#lostRecordDg').datagrid("hideColumn", "statusStr");
	    	} else {
				$('#lostRecordDg').datagrid("showColumn", "changeRow");
	    	}**/
			/*Utils.functionAccessFillControl('lostRecordDgContainer');
			if($('#lostRecordDgContainer .cmsiscontrol[id^="eqLostRec_printf"]').length == 0){
				$('#lostRecordDg').datagrid("hideColumn", "statusStr");
			} else {
				$('#lostRecordDg').datagrid("showColumn", "statusStr");
			}
			if($('#lostRecordDgContainer .cmsiscontrol[id^="eqLostRec_change"]').length == 0){
				if($('#lostRecordDgContainer .cmsiscontrol[id^="permisionBrowser_change"]').length == 0){
					$('#lostRecordDg').datagrid("hideColumn", "changeRow");
				}
			} else {
				$('#lostRecordDg').datagrid("showColumn", "changeRow");
			}*/
		 	$(window).resize();
        }
	});
    $('#txtCode').focus();
    setTimeout(function() {
		$('#btnSearch').click(); 
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 5px;");
    	$('.panel').width($(window).width()- 10);
	}, 1000);
});
</script>
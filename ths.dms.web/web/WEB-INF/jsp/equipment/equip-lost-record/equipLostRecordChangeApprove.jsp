<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/equipLostRecord/approve">Thiết bị</a>
		</li>
		<li>
		<s:if test='equipLostRecord!= null && equipLostRecord.id > 0'>
			<span>Chỉnh sửa biên bản báo mất <s:property value="equipLostRecord.code"/></span>
		</s:if>
		<s:else>
			<span>Thêm mới biên bản báo mất</span>
		</s:else>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form" id= "cusInfFormDiv">
<%-- 					<label class="LabelStyle Label1Style">Mã NPP (F9)<span class="ReqiureStyle"> *</span></label> --%>
<%-- 					<input id="txtShopCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" value='<s:property value="shopCode"/>'/> --%>
<!-- 					<div class="Clear"></div> -->
					<label class="LabelStyle Label1Style">Mã phiếu</span></label>
					<input id="lostRecordCode" type="text" disabled="disabled" class="InputTextStyle InputText1Style" maxlength="50" value='<s:property value="equipLostRecord.code"/>'/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Mã NPP (F9) <span class="ReqiureStyle"> *</span></label>
					<input id="txtShopCode" type="text" onchange="" class="InputTextStyle InputText1Style" maxlength="50" value='<s:property value="shopCode"/>'/>
					<label class="LabelStyle Label1Style">Báo mất cho</label>
					<div class="BoxSelect BoxSelect2" id="permisionBrowser_ddlStockTypeDiv">
						<select class="MySelectBoxClass" id="cboStockType" tabindex="1" >
							<option value="1" >NPP</option>
							<option value="2" >Khách hàng</option>
						</select>
					</div>
					<label class="LabelStyle Label1Style">Ngày biên bản<span class="ReqiureStyle"> *</span></label>
					<div id="createDateDiv">
						<input type="text" id="createDate"  disabled="disabled"  class="InputTextStyle InputText1Style" />
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Mã KH (F9) <span class="ReqiureStyle"> *</span></label>
					<input id="txtCustomerCode" type="text" onchange="EquipLostRecord.onchangeTxtCustomerCode(this);" class="InputTextStyle InputText1Style" maxlength="50" value='<s:property value="customer.shortCode"/>'/>
					<label class="LabelStyle Label1Style">Tên KH</label>
					<input id="txtCustomerName" type="text" disabled="disabled" class="InputTextStyle InputText1Style" maxlength="250" value='<s:property value="customer.customerName"/>'/>
					<label class="LabelStyle Label1Style">Người đại diện <span class="ReqiureStyle"> *</span></label>
					<input id="txtRepresentor" type="text" class="InputTextStyle InputText1Style" maxlength="250" value='<s:property value="equipLostRecord.representor"/>' disable/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Điện thoại</label>
					<input type="text" id="txtPhone" disabled="disabled" class="InputTextStyle InputText1Style" value='<s:property value="phoneStr"/>'/>
					<label class="LabelStyle Label1Style">Địa chỉ</label>
					<input type="text" id="txtAddress" disabled="disabled" class="InputTextStyle InputText3Style" style="width:545px" value='<s:property value="customer.address"/>'/>
					<div class="Clear"></div>
				</div>		
				<!-- danh sach thiet bi -->	
				<h2 class="Title2Style"><span>Thông tin thiết bị</span></h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridEquipContainer">
						<table id="gridEquipment"></table>
					</div>
				</div>
				<p id="errMsgInfo" class="ErrorMsgStyle" style="display: none;"></p>
				<h2 class="Title2Style">Thông tin khác</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Thời gian mất <span class="ReqiureStyle"> *</span></label>
					<input id="txtLostDate" type="text" class="InputTextStyle InputText1Style vinput-date" value='<s:property value="equipLostRecord.lostDateStrByLostDate"/>'/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Ngày hết phát sinh doanh số <span class="ReqiureStyle"> *</span></label>
					<input id="txtLastArisingSalesDate" type="text" class="InputTextStyle InputText1Style vinput-date" value='<s:property value="equipLostRecord.lasdByLastArisingSalesDate"/>'/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">GS và NPP truy tìm tủ mất <span class="ReqiureStyle"> *</span></label>
					<input type="radio" id="radTracingPlace0" checked="checked" name="radTracingPlace" style="margin-top: 5px;" value="0">Tại địa chỉ kinh doanh<br>
					<input type="radio" id="radTracingPlace1" name="radTracingPlace" style="margin-top: 10px;" value="1">Tại địa chỉ thường trú của Khách hàng<br>
					<div class="Clear" style="height: 15px;"></div>
					<label class="LabelStyle Label1Style">Kết quả truy tìm <span class="ReqiureStyle"> *</span></label>
					<input type="radio" id="radTracingResult0" checked="checked" name="radTracingResult" style="margin-top: 5px;" value="0">Tìm được khách hàng nhưng không thấy tủ <br>
					<input type="radio" id="radTracingResult1" name="radTracingResult" style="margin-top: 10px;" value="1">Không tìm thấy khách hàng<br>
					<div class="Clear" style="height: 15px;"></div>
					<label class="LabelStyle Label1Style">Kết luận</label>
					<input id="txtConclusion" type="text" class="InputTextStyle InputText1Style" maxlength="500" value='<s:property value="equipLostRecord.conclusion"/>'/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Hướng đề xuất xử lý của công ty IDP</label>
					<input id="txtRecommendedTreatment" type="text" style="margin-top: 5px;" class="InputTextStyle InputText1Style" maxlength="500"  value='<s:property value="equipLostRecord.recommendedTreatment"/>'/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Trạng thái biên bản</label>
<%-- 					<s:if test='equipLostRecord.recordStatus != null && equipLostRecord.recordStatus == 0'> --%>
						
<%-- 					</s:if> --%>
						<div class="BoxSelect BoxSelect2" id="permisionCreate_ddlStatusEquipRecordDiv">
							<!-- <select class="MySelectBoxClass" id="ddlStatusEquipRecord" onchange="EquipLostRecord.changeDdlStatusEquipRecordDt(this);"> -->
							<select class="MySelectBoxClass" id="ddlStatusEquipRecord">
								<option value="2">Duyệt</option>
								<option value="3">Không duyệt</option>
							</select>
						</div>
						<div class="Clear"></div>
				<label class="LabelStyle Label1Style">Ghi chú</label>
				<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 41%; height: 100px; font-size: 13px;" maxlength="500"><s:property value="equipLostRecord.note"/></textarea>
				<div class="Clear"></div>		
				</div>
				<h2 class="Title2Style">Tập tin đính kèm</h2>
				<div class="SearchInSection SProduct1Form">
					<a href="javascript:void(0);" style="float: left;" title="Lựa chọn tập tin" ><!-- <img class="addFileBtn" src="/resources/images/icon_attach_file.png"> --></a>
					<div class="fileupload-process" style="width: 90%;">
						<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-striped active" id="total-progress" style="opacity: 0; width: 550px;">
							<div data-dz-uploadprogress="" style="width: 200px;" class="progress-bar progress-bar-success"></div>
						</div>
					</div>
					<div style="float:left; min-width: 500px; width: 90%;">
						<div style="height: auto; margin-left:20px;">
							<s:iterator id="fileVo" value="lstFileVo">
								<div class="divImageUploadFile" id = "divEquipAttachFile<s:property value="#fileVo.fileId"/>">
									<div class="imageUpdateShowIcon">
										<img class="addFileBtn" src="/resources/images/img-folder-001.png" width="50" height="50">
									</div>
									<div class="imageUpdateShowContent">
										<label><s:property value="#fileVo.fileName"/></label><br />
										<a href="javascript:void(0)" onclick="EquipLostRecord.removeEquipAttachFile(<s:property value="#fileVo.fileId"/>);"><!-- <img title="Xóa tập tin" src="/resources/images/icon_delete.png" width="12" height="12" style="padding-left: 5px;"> --></a>
									</div>
								</div>
							</s:iterator>
						</div>
						<div class="Clear"></div>
						<div class="table table-striped" class="files" id="previews" style="min-height: 100px; margin-left:20px;">
							<div id="template" class="file-row" style="margin-bottom: 10px; width: 260px;float: left">
								<div style="margin-right: 10px; width: 50px; float: left">
									<span class="preview"><img data-dz-thumbnail /></span>
								</div>
								<div style="display: inline-block; width: 200px; clear: both">
									<div style="padding-right: 10px">
										<div>
											<p class="name" style="overflow: hidden" data-dz-name></p>
											<strong class="error text-danger" data-dz-errormessage></strong>
										</div>
										<div style="">
											<p class="size" data-dz-size></p>
											</div>
										<div>
											<a data-dz-remove href="javascript:void(0)"><span><!-- <img border="0" src="/resources/scripts/plugins/uploadify2.1/cancel.png"> --></span></a>
										</div>
									</div>								
								</div>							
							</div> 
						</div>
					</div>
					<div class="Clear"></div>
				</div>
				<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
				<p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>
				<div class="SearchInSection SProduct1Form">
					<div class="BtnCenterSection">
						<!-- <button id="permisionCreate_btnChange" class="BtnGeneralStyle cmsiscontrol" onclick="EquipLostRecord.insertOrUpdateEquipLostRecord();">Cập nhật</button>	 -->
						<button id="permisionCreate_btnChange" class="BtnGeneralStyle" onclick="EquipLostRecord.changeByListEquipLostRecordApprove(EquipLostRecord._DUYET);">Cập nhật</button>						
						<button id="btnClose" class="BtnGeneralStyle" onclick="location.href = '/equipLostRecord/approve';">Bỏ qua</button>
					</div>
					<div class="Clear"></div>
				</div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>

<!-- begin popup chon kho -->
<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchStock" class="easyui-dialog"
		title="Chọn kho"
		data-options="closed:true,modal:true">
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label2Style">Mã đơn vị</label>
				<input id="shopCode" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label2Style">Tên đơn vị</label>
				<input id="shopName" type="text" class="InputTextStyle InputText4Style" maxlength="100"/>		
				<div class="Clear"></div>		
				<!-- <label class="LabelStyle Label2Style">Mã KH</label>
				<input id="customerCode" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label2Style">Tên KH/Địa chỉ</label>
				<input id="customerName" type="text" class="InputTextStyle InputText4Style" maxlength="250"/>				
				<div class="Clear"></div> -->
				<div class="BtnCenterSection">
					<button id="btnSearchStockDlg" class="BtnGeneralStyle" onclick="EquipLostRecord.searchStockLost();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="stockGridDialogContainer">
					<table id="stockGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnStockClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchStock').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!-- end popup chon kho -->
<!-- begin popup chon thiet bi -->
<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchEquipment" class="easyui-dialog"
		title="Chọn thiết bị"
		data-options="closed:true,modal:true" >
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label6Style">Mã thiết bị</label>
				<input id="equipmentCode" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label7Style">Số serial</label>
				<input id="seriNumber" type="text" class="InputTextStyle InputText4Style" maxlength="100"/>
				<label class="LabelStyle Label7Style">Loại</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="typeEquipment" onchange="EquipLostRecord.onChangeTypeEquipLost(this);">
						<option value="">Tất cả</option>
					</select>
				</div>
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Nhóm thiết bị</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="groupEquipment" onchange="">
						<option value="">Tất cả</option>
					</select>
				</div>
				<label class="LabelStyle Label7Style">Nhà cung cấp</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="providerId" onchange="">
						<option value="">Tất cả</option>
					</select>
				</div>					
				<label class="LabelStyle Label7Style">Năm sản xuất</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="yearManufacturing">
					</select>
				</div>
				<!-- <div id="contractDateDiv">
					<input type="text" id="yearManufacturing" class="InputTextStyle InputText5Style"/>
				</div> -->
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Kho (F9)</label>
				<s:if test="idRecordDelivery == null">
					<input type="text" id="stockCode" class="InputTextStyle InputText4Style" />
				</s:if>
				<s:else>
					<input type="text" id="stockCode" class="InputTextStyle InputText4Style" value="<s:property value="shopCode"/>"/>
				</s:else>				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="EquipLostRecord.searchEquipmentLost();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="equipmentGridDialogContainer">
					<table id="equipmentGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchEquipment').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!-- end popup chon thiet bi -->
<div><input type="hidden" id="formCreateDate" value="<s:property value="createDate"/>"></div>
<script type="text/javascript">
$(document).ready(function() {	
	EquipLostRecord._rowsDetail = [];
	EquipLostRecord._countFile = Number('<s:property value="lstFileVo.size()"/>');
	$('input').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#permisionCreate_btnChange').click(); 
		}
	});
	$('#btnClose').bind('keyup',function(event){
		if(event.keyCode == keyCodes.ENTER){
			window.location.href = "/equipLostRecord/info";
		}
	});
	// setDateTimePicker('createDate');
	var createFormDate = $('#formCreateDate').val();
	/*if (createFormDate != undefined && createFormDate != null && createFormDate != '') {
		$('#createDate').val(createFormDate);
	} else {
		//ReportUtils.setCurrentDateForCalendar("createDate");
	}*/
	<s:if test="equipLostRecord.createFormDate!=null">
		$('#createDate').val('<s:property value="displayDate(equipLostRecord.createFormDate)" />');
	</s:if>
	EquipLostRecord._sysDateStr = '<s:property value="sysDateStr"/>';
	EquipLostRecord._stockIdChange  = '<s:property value="customer.id"/>';
	EquipLostRecord._equipIdChange = '<s:property value="equipId"/>';
	EquipLostRecord._eqLostRecId = '<s:property value="equipLostRecord.id"/>';
	EquipLostRecord._isLevelShopRoot = '<s:property value="isLevel"/>';
	EquipLostRecord._isFlagMobile = '<s:property value="flagMobile"/>';
	EquipLostRecord._eqLostMobiRecId = '<s:property value="eqLostMobiRecId"/>';
	EquipLostRecord._tracingResult = '<s:property value="equipLostRecord.tracingResult"/>';
	EquipLostRecord._tracingPlace  = '<s:property value="equipLostRecord.tracingPlace"/>';
	EquipLostRecord._recordStatus  = '<s:property value="equipLostRecord.recordStatus"/>';
	EquipLostRecord._deliveryStatus  = '<s:property value="equipLostRecord.deliveryStatus"/>';
	EquipLostRecord._contractNumber = '<s:property value="equipLostRecord.equipDeliveryRecord.contractNumber" />';
	var idEqLostRec = -1;
	/** chung cho ca tao moi va cap nhat bao mat*/
	EquipLostRecord._isFlaglostRecord = '<s:property value="flagLostRecord"/>'; // cai nay phai goi truoc khi chinh sua // de goi ham loadEquipByEquipLostRec
	if (EquipLostRecord._eqLostRecId != null && EquipLostRecord._eqLostRecId.toString().trim().length > 0) {
		/** truong hop cap nhat bao mat*/		
		$('#txtCustomerCode').attr('readonly', true);
		idEqLostRec = Number(EquipLostRecord._eqLostRecId);
		var arrChkRadTracingPlace = $('[name="radTracingPlace"]');
		for (var i = 0, size = arrChkRadTracingPlace.length; i < size; i++) {
			if(EquipLostRecord._tracingPlace.toString().length > 0 && Number($(arrChkRadTracingPlace[i]).val()) == Number(EquipLostRecord._tracingPlace)) {
				$(arrChkRadTracingPlace[i]).prop("checked", true);
			}
		}
		var arrChktracingResult = $('[name="radTracingResult"]');
		for (var i = 0, size = arrChktracingResult.length; i < size; i++) {
			if(EquipLostRecord._recordStatus.toString().length > 0 && Number($(arrChktracingResult[i]).val()) == Number(EquipLostRecord._tracingResult)) {
				$(arrChktracingResult[i]).prop("checked", true);
			}
		}
		//EquipLostRecord.searchEquipByEquipDeliRec(); // cai nay goi khi chọn bao mat NPP chon thiet bị và khi chon KH se load len grid 1 thiet bi dau tien
		//vuongmq; 15/05/2015 khi chinh sua ben chuc nang phe duyet bao mat; cho disable grid
		EquipLostRecord._FLAG_CHANGE_PHE_DUYET = activeType.RUNNING; // chinh sua trang phe duyet bao mat
		EquipLostRecord.loadEquipByEquipLostRec(); // vuongmq; 06/05/2015; truong hop cap nhat se load cai nay len
		disableSelectbox('cboStockType');
		disabled('txtShopCode'); //vuongmq 14/05/2015
	} else {
		/** truong hop tao moi bao mat*/
		enableSelectbox('cboStockType');
		enable('txtShopCode'); //vuongmq 14/05/2015
	}
	if (EquipLostRecord._stockIdChange != null && EquipLostRecord._stockIdChange.toString().trim().length > 0) {
		$('#txtCustomerCode').attr('readonly', true);
		disabled('txtCustomerCode');
	} else {
		$('#txtCustomerCode').attr('readonly', false);
		enable('txtCustomerCode');
	}
	// bao mat cua NPP; lay shop gan vao EquipLostRecord._phoneShop; EquipLostRecord._addressShop; ben js ham fillTextBoxByShopCode gan lai
	var phone1 = '<s:property value="shop.phone"/>';
	var phone2 = '<s:property value="shop.mobiphone"/>';
	if (phone1 != undefined && phone1 != '' && phone2 != undefined && phone2 != '') {
		EquipLostRecord._phoneShop = phone1 +' - '+ phone2;
	} else if (phone1 != undefined && phone1 != '') {
		EquipLostRecord._phoneShop = phone1;
	} else if (phone2 != undefined && phone2 != '') {
		EquipLostRecord._phoneShop = phone2;
	}
	EquipLostRecord._addressShop = '<s:property value="shop.address"/>';
	/** khi chinh sua thi lay lai combobox theo loai bao mat; */
	$('#cboStockType').val(EquipLostRecord._isFlaglostRecord).change();
	if (EquipLostRecord._isFlaglostRecord == activeType.RUNNING) {
		// bao mat cua NPP
		$('#txtPhone').val(EquipLostRecord._phoneShop);
		$('#txtAddress').val(EquipLostRecord._addressShop);
		$('#txtCustomerCode').val('');
		$('#txtCustomerName').val('');
		$('#txtCustomerCode').attr('readonly', true);
		disabled('txtCustomerCode');
	}
	/*** change combo bao mat */
	$('#cboStockType').change( function() {
		if ($(this).val() == activeType.RUNNING) {
			// bao mat cua NPP
			$('#txtPhone').val(EquipLostRecord._phoneShop);
			$('#txtAddress').val(EquipLostRecord._addressShop);
			$('#txtCustomerCode').val('');
			$('#txtCustomerName').val('');
			$('#txtRepresentor').val('');
			$('#txtCustomerCode').attr('readonly', true);
			disabled('txtCustomerCode');
			EquipLostRecord._rowsDetail = [];
			EquipLostRecord.createDataGidEquipment();
		} else {
			// bao mat cua KH
			$('#txtCustomerCode').val('');
			$('#txtCustomerName').val('');
			$('#txtRepresentor').val('');
			$('#txtPhone').val('');
			$('#txtAddress').val('');
			$('#txtCustomerCode').attr('readonly', false);
			enable('txtCustomerCode');
			EquipLostRecord._rowsDetail = [];
			EquipLostRecord.createDataGidEquipment();
		}
	});
	/*** change shop code */
	$('#txtShopCode').change( function() {
		var code = $(this).val();
		EquipLostRecord.changetxtShopCode(code);
	});
	
	/*** vuongmq; 15/05/2015/ duyet;cho disable cac gia tri*/
	disabled('txtRepresentor');
	/*disabled('equipItemCodeInGrid'); // gia tri code thiet bi trong grid
	$('.priceActuallyInGridzz').attr('disabled',true); // gia tri con lai*/
	//disabled('txtLostDate');
	disableDateTimePicker('txtLostDate');
	//disabled('txtLastArisingSalesDate');
	disableDateTimePicker('txtLastArisingSalesDate');
	disabled('radTracingPlace0');
	disabled('radTracingPlace1');
	disabled('radTracingResult0');
	disabled('radTracingResult1');
	disabled('txtConclusion');
	disabled('txtRecommendedTreatment');
	disabled('note');
	
	//EquipLostRecord.createDdlDeliveryStatus(idEqLostRec, Number(EquipLostRecord._isLevelShopRoot));
	//EquipLostRecord.createDdlStatusEquipRecord(idEqLostRec, Number(EquipLostRecord._isLevelShopRoot));
	// vuongmq; 15/05/2015; grid nay se tao ben ham EquipLostRecord.loadEquipByEquipLostRec();
	$('#gridEquipment').datagrid({
		//url: '/equipLostRecord/searchEquipByEquipDeliveryRecordNew',
		data : [],
		width : $('#gridEquipment').width() - 15,
// 		queryParams: {
// 			eqLostRecId: idEqLostRec,
// 			flagPagination: false
// 		},
        autoWidth: true,
        rownumbers : true,
        fitColumns : true,
        autoRowHeight : true,
        scrollbarSize: 0,
		columns:[[
			{field:'contractNumber', title:'Số hợp đồng', width:120, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
				if (Number(EquipLostRecord._equipIdChange) == row.equipId && Number(EquipLostRecord._eqLostMobiRecId) <= 0){
					return Utils.XSSEncode(EquipLostRecord._contractNumber);
				}
				return Utils.XSSEncode(row.contractNumber);
			}},  
		    {field:'equipCategoryCode', title:'Loại thiết bị', width:80, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	if (row.equipId != undefined && row.equipId != null && row.equipId > 0) {
		    		return Utils.XSSEncode(row.equipCategoryCode +' - '+ row.equipCategoryName);		    		
		    	}
		    	return '';
		    }},
		    {field:'equipGroupCode', title:'Nhóm thiết bị', width:90, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	if (row.equipId != undefined && row.equipId != null && row.equipId > 0) {
			    	return Utils.XSSEncode(row.equipGroupCode +' - '+ row.equipGroupName);
		    	}
		    	return '';
		    }},
		    {field:'equipCode', title:'Mã thiết bị (F9)', width:100, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	//return Utils.XSSEncode(value);
		    	var html = '<input type="text" class="equipItemCodeInGridClzz InputTextStyle" id="equipItemCodeInGrid" style="margin-bottom:2px; width:94%" maxlength="50" readonly="true" >';
		    	if (value != null) {
		    		html = '<input type="text" class="equipItemCodeInGridClzz InputTextStyle" id="equipItemCodeInGrid" style="margin-bottom:2px; width:94%" maxlength="50" readonly="true" value ="'+value+'" >';
		    	}
    			return html;
		    }},
		    {field:'serial', title:'Số serial', width:100, sortable: false, resizable: false, align:'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'price', title:'Nguyên giá', width:60, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
		    	return formatCurrency(value);
		    }},
		    {field:'priceActually', title:'Giá trị còn lại', width:60, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
		    		var id = '';
					if (row.equipId != null) {
						id = row.equipId;
					}
					var idx = '';
					if (index != null) {
						idx = index;
					}
					var html = '<input type="text" class="priceActuallyInGridzz InputTextStyle vinput-money" id="priceActuallyInGrid_'+id+'" style="margin-bottom:2px; width: 92%; text-align: right;" maxlength="17" gridIndex="'+idx+'" >';
					if (value != null) {
						html = '<input type="text" class="priceActuallyInGridzz InputTextStyle vinput-money" id="priceActuallyInGrid_'+id+'" style="margin-bottom:2px; width: 92%; text-align: right;" maxlength="17" gridIndex="'+idx+'" value="'+formatCurrency(value)+'" >';
					}
					return html;
		    }},
		    {field:'quantity', title:'Số lượng', width:40, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
		    	if (row.equipId != undefined && row.equipId != null && row.equipId > 0) {
			    	return 1;
		    	}
		    	return '';
		    }},
		    {field:'manufacturingYear', title:'Năm sản xuất', width:55, sortable: false, resizable: false, align:'right', formatter: function(value, row, index){
		    	if (row.manufacturingYear != undefined && row.manufacturingYear != null && row.manufacturingYear > 0) {
			    	return Utils.XSSEncode(row.manufacturingYear);
		    	}
		    	return '';
		    }},
		    /*{field:'changeRow', title:'', width:60, sortable: false, resizable: false, align:'center', formatter: function(value, row, index){
				if (row.equipId > 0) {
					return '<a id="permisionCreate_GridChange_'+row.equipId+'" class="cmsiscontrol" href="javascript:void(0)" onclick="EquipLostRecord.editRowGridEquipmentChangePage();"><img title="Chỉnh sửa" src="/resources/images/icon-edit.png" width="18" height="18" style="padding-left: 5px;"></a>';
				}
		    }}*/
		]],
        onLoadSuccess: function(data){
			$('#gridEquipContainer .datagrid-header-rownumber').html('STT');
			Utils.updateRownumWidthAndHeightForDataGrid('gridEquipment');
			$(window).resize();
		 	var rows = $('#gridEquipment').datagrid('getRows');
 	    	if(rows == null || rows.length == 0){
				//$('#gridEquipment').datagrid("hideColumn", "changeRow");
				/*if ($('#txtCustomerCode').val().trim().length > 0) { 
 	    			EquipLostRecord.insertRowEquipmentDg();
				}*/
				/** cai nay insert grid luc dau khi qua them moi or chinh sua phieu bao mat*/
				EquipLostRecord.insertRowEquipmentDg();
	    	} else {
				if (EquipLostRecord._eqLostMobiRecId != undefined && EquipLostRecord._eqLostMobiRecId != null && EquipLostRecord._eqLostMobiRecId.toString().trim() > 0) {
					//$('#gridEquipment').datagrid("hideColumn", "changeRow");
				} else {
					if (EquipLostRecord._recordStatus != undefined && EquipLostRecord._recordStatus != null && EquipLostRecord._recordStatus.toString().trim() > 0 && statusRecordsEquip.KD == Number(EquipLostRecord._recordStatus)) {
						//$('#gridEquipment').datagrid("hideColumn", "changeRow");
					} else {
						//$('#gridEquipment').datagrid("showColumn", "changeRow");
					}
				}
	    	}
 	    	/*Utils.functionAccessFillControl('gridEquipContainer');
 	    	if($('#gridEquipContainer .cmsiscontrol[id^="permisionCreate_GridChange"]').length == 0){
				//$('#gridEquipment').datagrid("hideColumn", "changeRow");
			} else {
				//$('#gridEquipment').datagrid("showColumn", "changeRow");
			}*/
			$('.ErrorMsgStyle').html('').hide();
			 /*su kien tren F9 grid*/
			EquipLostRecord.editInputInGridLost();
			EquipLostRecord.bindEventChangeTextLost();
        }
	});
	/*show popup khach hang*/
	$('#txtCustomerCode').bind('keyup', function(event) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if (event.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2({
				dialogInfo: {
					title: 'Tìm kiếm khách hàng'
				},
				params : {
					shopCode : $('#txtShopCode').val().trim(),
					status : activeType.RUNNING
				},
				inputs : [
				{id:'code', maxlength:250, label:'Khách hàng', width:410}
// 				{id:'name', maxlength:250, label:'Tên khách hàng'},
				      ],
				url : '/commons/search-Customer-Show-List',
				columns : [[
					{field:'shortCode', title:'Mã khách hàng', align:'left', width: 120, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.shortCode);
			        }},
					{field:'customerName', title:'Tên khách hàng', align:'left', width: 180, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.customerName);
			        }},
					{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
						return '<a href="javascript:void(0)" onclick="EquipLostRecord.fillTextBoxByCustomerCode(\''+row.shortCode+'\')">chọn</a>';         
					}}
				]]
			});
		}
	});
	/* vuongmq; 13/05/2015; show popup NPP phan quyen CMS*/
	$('#txtShopCode').bind('keyup', function(event) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if (event.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2({
				dialogInfo: {
					title: 'Tìm kiếm đơn vị'
				},
				/*params : {
					shopCode : $('#txtShopCode').val().trim(),
					status : activeType.RUNNING
				},*/
				inputs : [
				{id:'code', maxlength:50, label:'Mã đơn vị'},
				{id:'name', maxlength:250, label:'Tên đơn vị'},
				      ],
				url : '/commons/search-shop-show-list-NPP-GT-MT',
				columns : [[
					{field:'shopCode', title:'Mã đơn vị', align:'left', width: 120, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.shopCode);
			        }},
					{field:'shopName', title:'Tên đơn vị', align:'left', width: 180, sortable:false, resizable:false, formatter:function(value,row,index) {
			        	return Utils.XSSEncode(row.shopName);
			        }},
					{field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
						return '<a href="javascript:void(0)" onclick="EquipLostRecord.fillTextBoxByShopCode(\''+row.shopCode+'\',\''+row.shopId+'\')">chọn</a>';         
					}}
				]]
			});
		}
	});
	/*show popup kho cua thiet bi*/
	$('#stockCode').bind('keyup',function(event) {
		$('.SuccessMsgStyle').html('').hide();
		$('.ErrorMsgStyle').html('').hide();
		if (event.keyCode == keyCodes.F9) {
			EquipLostRecord.showPopupStockLost();
		}
	});	
	
	//EquipLostRecord.loadEquipByEquipLostRec();
	if (EquipLostRecord._eqLostMobiRecId == undefined || EquipLostRecord._eqLostMobiRecId == null) {
		EquipLostRecord._eqLostMobiRecId = 0;
	}
	
 	//UploadUtil.initUploadImageElement('body', 'body', '.addFileBtn');
	/*UploadUtil.initUploadFileByEquipment({
		url: '/equipLostRecord/insertOrUpdateLoadRecord',	
		elementSelector: 'body',
		elementId: 'body',
		clickableElement: '.addFileBtn',
		parallelUploads: 5
		//maxFilesize: 5
	}, function (data) {
		if (!data.error) {
			$("#successMsg").html("Lưu dữ liệu thành công").show();
			var t = setTimeout(function(){
				$('.SuccessMsgStyle').html("").hide();
				window.location.href = '/equipLostRecord/info';
				clearTimeout(t);
			 }, 1000);
		} else {
			$('#errMsg').html(data.errMsg).show();
		}
	});*/
});
</script>
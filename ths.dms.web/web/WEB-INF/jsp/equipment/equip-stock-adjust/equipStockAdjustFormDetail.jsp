<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="/equip-stock-adjust/info">Thiết bị</a>
		</li>
		<li>
			<s:if test='id != null'>
				<span>Cập nhật phiếu nhập kho điều chỉnh</span>
			</s:if>
			<s:else>
				<span>Lập phiếu nhập kho điều chỉnh</span>
				
			</s:else>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<!-- <h2 class="Title2Style">Thông tin chung</h2> -->
				<h2 class="Title2Style">Thông tin chung <span style="float:right;margin-right:15px;">
						<a href="javascript:void(0);" title="Ẩn/ hiện" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
				<div class="SearchInSection SProduct1Form">
					<label	class="LabelStyle Label1Style">Mã phiếu</label>
					<input 	type="text" class="InputTextStyle InputText1Style" id="txtEquipStockAdjustCode" maxlength="50" value='<s:property value="code"/>' />	
					<label	class="LabelStyle Label1Style">Lý do lập phiếu</label>
					<input	type="text" class="InputTextStyle InputText6Style" id="txtDescription" style="width:300px" maxlength="250" value='<s:property value="description"/>' />	
					<label class="LabelStyle Label1Style" >Trạng thái duyệt</label>
					<div class="BoxSelect BoxSelect2" id="ddlUsageStatusDiv">
						<select class="MySelectBoxClass InputTextStyle" id="cbxStatus">
						    <option value="0">Dự thảo</option>
						    <option value="1">Chờ duyệt</option>
						</select>
					</div>			
					<div class="Clear"></div>			
					<label class="LabelStyle Label1Style">Ghi chú</label>
					<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 49.5%; height: 100px; font-size: 13px;" maxlength="500" ><s:property value="note"/></textarea>
					<div class="Clear"></div>
					<p id="successMsgDetail" class="SuccessMsgStyle" style="display: none"></p>
					<p id="errMsgDetail" class="ErrorMsgStyle" style="display: none;"></p>
				</div>				
				<h2 class="Title2Style"><span>Danh sách phiếu nhập kho điều chỉnh</span></h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridEquipStockAdjustDtlItemContainer">
						<table id="gridEquipStockAdjustDtlItem"></table>
					</div>
				</div>	
				<div class="SearchInSection SProduct1Form">
					<div class="BtnCenterSection">
						<button id="btnUpdate" class="BtnGeneralStyle " onclick="EquipStockAdjustForm.updateEquipStockAdjustRecord(<s:property value="id"/>);">Cập nhật</button>
						<button id="btnClose" class="BtnGeneralStyle" onclick="location.href = '/equip-stock-adjust/info';">Bỏ qua</button>
					</div>
					<div class="Clear"></div>
				</div>								
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>

<script type="text/javascript">
	
	$(document).ready(function() {
		disabled('txtEquipStockAdjustCode');
		var id = '<s:property value="id"/>';
		
		var status = '<s:property value="status"/>';
		EquipStockAdjustForm._status = status;
		
		EquipStockAdjustForm.initializeEquipStockAdjustDtlGrid(id);
		
		//status 0: du thao, 1: cho duyet, 2: da duyet, 3 khong duyet, 4 da huy
		if(status!=null && status.trim()!=''){
			$('#cbxStatus').val(status).change();
			var html;

			if(status==2 || status ==4){
				if (status == 2) {
					html = '<option>Đã duyệt</option>';
				} else if(status == 4) {
					html = '<option>Đã hủy</option>';
				}
				
				$('#cbxStatus').html(html).change();
				disabled('txtDescription');
				disabled('note');
				disableSelectbox('cbxStatus');
				$('#btnUpdate').remove();
				
			}
			 if(status==1){
				html = '<option value="1" >Chờ duyệt</option> <option value="4" >Hủy</option>';
				$('#cbxStatus').html(html).change();
				disabled('txtDescription');
				disabled('note');
			} 
			//Van ban o trang thai la khong duyet, cho phep chuyen sang khong duyet, huy hoac cho duyet
			if(status==3){
				html = '<option value="3" >Không duyệt</option> <option value="1" >Chờ duyệt</option><option value="4" >Hủy</option>';
				$('#cbxStatus').html(html).change();
			}
			
			if(status==0){
				html = ' <option value="0">Dự thảo</option><option value="1">Chờ duyệt</option><option value="4" >Hủy</option>';
				$('#cbxStatus').html(html).change();
			}
		}
		
	  $('#txtEquipStockAdjustFormId').val(id);
		
	  if ($('#txtDescription').is('[disabled=disabled]')) { 
		  $("#gridEquipStockAdjustDtlItem").datagrid('hideColumn', 'edit');
	  }
		  
		  
		  //Auto complete
		  $("#ddlEquipGroupId").kendoComboBox();
		  var widgetEquipGroupId = $("#ddlEquipGroupId").data("kendoComboBox");
		  widgetEquipGroupId.bind("change", function() {
            if (widgetEquipGroupId.selectedIndex === -1 && widgetEquipGroupId.value()) {
              if (widgetEquipGroupId.dataSource.view().length > 0) {
            	$('#errMsgEquipmentDlg').html("Nhóm thiết bị không tồn tại").show();
      			setTimeout(function(){$('#errMsgEquipmentDlg').html('').hide();},1500);
      			widgetEquipGroupId.value("");
              } else {
            	  widgetEquipGroupId.value("");                          
              }
            }
          });
		  
		  $("#ddlEquipProviderId").kendoComboBox();
          var widgetEquipProviderId = $("#ddlEquipProviderId").data("kendoComboBox");
          widgetEquipProviderId.bind("change", function() {
            if (widgetEquipProviderId.selectedIndex === -1 && widgetEquipProviderId.value()) {
              if (widgetEquipProviderId.dataSource.view().length > 0) {
            	 $('#errMsgEquipmentDlg').html("NCC không tồn tại").show();
      			 setTimeout(function(){$('#errMsgEquipmentDlg').html('').hide();},1500);
      			 widgetEquipProviderId.value("");
              } else {
            	  widgetEquipProviderId.value("");                          
              }
            }
          });
          
		  
	});
</script>
<!-- Popup them moi thiet bi -->
<tiles:insertTemplate template="/WEB-INF/jsp/equipment/equip-stock-adjust/popupStockAdjust.jsp" /> 


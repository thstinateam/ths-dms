<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<style>
	.k-combobox {
		width: 206px;
	}
</style>


<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchStockAdjust" class="easyui-dialog"
		title="Nhập thông tin thiết bị"
		data-options="closed:true,modal:true" >
		<div class="GeneralForm Search1Form">
			<div class="SProduct1Form" style="padding-left:15px;">
			
				<label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;" >Mã thiết bị</span></label>	
				<input id="ddlEquipId" type="text" class="InputTextStyle InputText1Style vinput-money" />
				<label class="LabelStyle Label1Style">Số thứ tự</label>
				<input id="ddlNo" type="text" class="InputTextStyle InputText1Style vinput-number-comma" />
				
				<div class="Clear"></div>
				
				<label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;" >Nhóm thiết bị<span class="ReqiureStyle">(*)</span></label>	
				<div class="BoxSelect BoxSelect2" id="ddlEquipGroupIdDiv">
					<select  id="ddlEquipGroupId">
						<s:iterator value="result.get('lstEquipGroup')" var="equipPro">
							<option value="<s:property value='id' />"><s:property value="code" /> - <s:property value="name" /></option>
						</s:iterator>
					</select>
				</div>
				<label class="LabelStyle Label1Style">NCC<span class="ReqiureStyle">(*)</span></label>	
				<div class="BoxSelect BoxSelect2" id="ddlEquipProviderIdDiv">
					<select id="ddlEquipProviderId" >
						<s:iterator value="result.get('lstEquipProvider')" var="equipPro">
							<option value="<s:property value='id' />"><s:property value="code" /> - <s:property value="name" /></option>
						</s:iterator>
					</select>
				</div>	
				<label class="LabelStyle Label1Style">Nguyên giá<span class="ReqiureStyle">(*)</span></label>	
				<input id="ddlPrice" type="text" class="InputTextStyle InputText1Style vinput-money" maxlength="17"  />
				
				<div class="Clear"></div>
				
				<label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Năm sản xuất<span class="ReqiureStyle">(*)</span></label>
				<input id="ddlManufacturing" type="text" class="InputTextStyle InputText1Style vinput-number-comma" maxlength="4"  />
				<label class="LabelStyle Label1Style">Tình trạng thiết bị<span class="ReqiureStyle">(*)</span></label>		
				<div class="BoxSelect BoxSelect2" id="ddlUsageStatusDiv">
					<select class="MySelectBoxClass InputTextStyle" id="ddlHealthStatus">
						<s:iterator value="result.get('lstHealthStatus')" var="status">
							<option value="<s:property value='apParamCode' />"> <s:property value="value" escapeXml="true"/></option>
						</s:iterator>
					</select>
				</div>	
				<label class="LabelStyle Label1Style">Kho (F9)<span class="ReqiureStyle">(*)</span></label>		
				<input id="stockCode" type="text"  class="InputTextStyle InputText1Style maxlength="50"  />
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Ngày hết hạn bảo hành</label>
				<div id="toDateDiv">
					<input type="text" id="ddlExpireDate" class="InputTextStyle InputText6Style" style="width: 174px;"/>
				</div>
				<label class="LabelStyle Label1Style">Tình trạng</label>		
				<label class="LabelStyle "><b>Đang ở kho</b></label>			
				<label class="LabelStyle Label1Style" style="margin-left: 131px;">Số serial</label>
				<input id="ddlSeri" type="text"  class="InputTextStyle InputText1Style" maxlength="100" />
				
				<div class="Clear"></div>
			
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
				<div class="Clear"></div>
				
				<div class="SProduct1Form">
					<div class="BtnCenterSection">
						<button id="btnPopupEquipStockAdjustChoose" class="BtnGeneralStyle"	onclick="return EquipStockAdjustForm.chooseStockAdjustPopup();">Chọn</button>
						<button id="btnPopupPopupEquipStockAdjustClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchStockAdjust').dialog('close');">Đóng</button>
					</div>							
				</div>
		</div>
		</div>
		
	</div>
</div>

<div style="display:none" class="BoxSelect BoxSelect2" >
	<select class="MySelectBoxClass InputTextStyle" id="ddlBrandName">
		<s:iterator value="result.get('lstEquipGroup')" >
			<option value="<s:property value='id' />"><s:property value="brandName" /> </option>
		</s:iterator>
	</select>
</div>
<div style="display:none" class="BoxSelect BoxSelect2" >
	<select class="MySelectBoxClass InputTextStyle" id="ddlEquipCategoryName">
		<s:iterator value="result.get('lstEquipGroup')" >
			<option value="<s:property value='id' />"><s:property value="equipCategoryName" escapeXml="true"/> </option>
		</s:iterator>
	</select>
</div>

<div style="display:none" class="BoxSelect BoxSelect2" >
	<select class="MySelectBoxClass InputTextStyle" id="ddlCapacity">
		<s:iterator value="result.get('lstEquipGroup')" >
			<option value="<s:property value='id' />"><s:property value="capacityStr" /> </option>
		</s:iterator>
	</select>
</div>

<div style="display:none" class="BoxSelect BoxSelect2" >
	<select class="MySelectBoxClass InputTextStyle" id="ddlStock">
		<s:iterator value="result.get('lstStock')" >
			<option value="<s:property value='equipStockCode' />"><s:property value="equipStockName" escapeXml="true"/> </option>
		</s:iterator>
	</select>
</div>

<!--Popup tim kiem kho   -->
<div id ="productDialog" style="width:500px;visibility: hidden;" >
	<div id="easyuiPopupSearchStock" class="easyui-dialog"	title="Tìm kiếm kho"
		data-options="closed:true,modal:true" >
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label1Style">Mã đơn vị</label>
				<input id="shopCodePopupStock" type="text" class="InputTextStyle InputText4Style"  maxlength="50" />
				<label class="LabelStyle Label1Style">Tên đơn vị</label>
				<input id="shopNamePopupStock" type="text" class="InputTextStyle InputText4Style"  maxlength="100"/>				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="EquipmentStockChange.searchStockChange();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="stockGridDialogContainer">
					<table id="stockGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchStock').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>

<input id="txtEquipStockAdjustFormId" type="hidden"/>
<input  id="ddlStockId"  type="hidden" />
<input  id="ddlStockName"  type="hidden" />

<script type="text/javascript">
$(document).ready(function() {	
	setDateTimePicker('ddlExpireDate');	
	disabled('ddlNo');
	disabled('ddlEquipId');
	ReportUtils.setCurrentDateForCalendar('ddlExpireDate');
	
	 $('#ddlManufacturing, #stockCode, #ddlSeri, #ddlExpireDate, #ddlPrice ').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#btnPopupEquipStockAdjustChoose').click(); 
			}
		 }); 	
	 
	$('#stockCode').bind('keyup',function(event){
		if(event.keyCode == keyCodes.F9){
			EquipStockAdjustForm.showPopupSearchStock();
		}
	});	
	
	$('#stockCode').change(function() {
		if($('#stockCode').val().trim()==""){
			$('#ddlStockId').val("");
		}
	});
	
	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="javascript:void(0);">Thiết bị</a>
		</li>
		<li>
			<span>Quản lý nhập kho điều chỉnh</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm <span style="float:right;margin-right:15px;">
						<a href="javascript:void(0);" title="Ẩn/ hiện" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="Clear"></div>
						<label	class="LabelStyle Label2Style" style=";text-align:left;;height:auto;" >Mã biên bản</label> 
						<input type="text" class="InputTextStyle InputText6Style" id="txtCode" maxlength="50"/>	
						<label	class="LabelStyle Label2Style" >Trạng thái</label>
						<div class="BoxSelect BoxSelect2" id="ddlUsageStatusDiv">
							<select class="MySelectBoxClass InputTextStyle" id="cbxStatus">
								<option value="-1" selected="selected">Tất cả</option>
							    <option value="0">Dự thảo</option>
							    <option value="1">Chờ duyệt</option>
							    <option value="2">Duyệt</option>
							    <option value="3">Không duyệt</option>
							    <option value="4">Hủy</option>
							</select>
						</div>
						<label class="LabelStyle Label1Style" >Ngày tạo từ</label>
						<div id="fromDateDiv">
							<input type="text" id="fDate" class="InputTextStyle InputText5Style" />
						</div>
						<label class="LabelStyle Label2Style">Đến</label>
						<div id="toDateDiv">
							<input type="text" id="tDate" class="InputTextStyle InputText5Style" />
						</div>
						<div class="Clear"></div>	
						<div class="SearchInSection SProduct1Form">
							<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
							<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
							<div class="BtnCenterSection">
								<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol" onclick="EquipStockAdjustForm.searchEquipStockAdjustForm();">Tìm kiếm</button>
							</div>
							<div class="Clear"></div>
						</div>	
					</div>	
						
				<h2 class="Title2Style">Danh sách biên bản nhập kho điều chỉnh
					<div style="float:right; margin-top: -5px; margin-right: 27px;" id="actionBar">
					<button style="float:right;" id="btnSaveLst" class="BtnGeneralStyle cmsiscontrol" onclick="EquipStockAdjustForm.updateStatus();" >Lưu</button>
						<div style="float:right;margin-right: 5px;" class="BoxSelect BoxSelect2">							
							<select class="MySelectBoxClass InputTextStyle" id="cbxStatusUpdate">
							    <option value="1">Chờ duyệt</option>
							    <option value="4">Hủy</option>
							    
							    <option value="2">Duyệt</option>
							    <option value="3">Không duyệt</option>
							</select>
						</div>
					<a  onclick = "EquipStockAdjustForm.exportEquipStockAdjust();" style = "float: right;	margin: 0px 25px 0px 0px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_export.png" title="Xuất Excel"></a>
					<a id="importDataStockAdjust" class="cmsiscontrol" onclick = "EquipStockAdjustForm.openDialogImportStockAdjust();" style = "float: right;	margin-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_import.png" title="Nhập Excel"></a>
					<a id="downloadTemplate"  class="cmsiscontrol" onclick="EquipStockAdjustForm.downloadTemplateExcel()"  class="downloadTemplateReport"  style = "float: right; margin-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_download_32.png" title="Tải mẫu Nhập Excel"></a>
					</div>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="grid"></table>
					</div>
				</div>		
				<div class="Clear"></div>
				
			</div> <!-- end class="ToolBarSection" -->
		</div>
	</div>
	
	<div class="Clear"></div>
	
	<!-- Chi tiet chuc nang Form -->
	<div class="ContentSection" style="visibility:hidden; display: none;" id="equipStockAdjustDtlTemplateDiv">
		<div class="BreadcrumbSection">
		    <ul class="ResetList FixFloat BreadcrumbList">
		        <li><span>Danh sách thiết bị </span><span id="equipStockAdjustCode" style="color:#199700"></span></li>
		    </ul>
		</div>
		<div class="GeneralCntSection">
			<div class="GridSection" id="gridEquipStockAdjustDtlItemContainer">
				<table id="gridEquipStockAdjustDtlItem"></table>
			</div>
		</div>
	</div>
	<!-- popup import -->
	<div style="display: none">
	  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Nhập Excel phiếu cập nhật kho điều chỉnh" data-options="closed:true,modal:true" style="width:465px;height:250px;">
			<div class="PopupContentMid">
				<div class="GeneralForm ImportExcel1Form">
					<form action="/equip-stock-adjust/importExcel" name="importFrmListEquipListEquip" id="importFrmListEquip" method="post" enctype="multipart/form-data">						            	
				        <label class="LabelStyle Label1Style" style="margin-top:2px">File Excel</label>
					    <div class="UploadFileSection Sprite1" >
							<input id="fakefilepcListEquip" type="text" class="InputTextStyle InputText1Style">
							<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileListEquip" onchange="previewImportExcelFile(this,'importFrmListEquipListEquip','fakefilepcListEquip', 'errExcelMsgListEquip');" />						               		
						</div>
					</form>
					<div class="Clear"></div>
					<p class="DownloadSFileStyle">
						<a onclick="EquipStockAdjustForm.downloadTemplateExcel()"  class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
					</p>									
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" onclick="EquipStockAdjustForm.importExcelEquipStockAdjust();">Nhập Excel</button>
						<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').dialog('close');">Đóng</button>
					</div>
				</div>
			</div>
			<p id="successExcelMsgListEquip" class="SuccessMsgStyle" style="display:none;"></p>
			<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgListEquip" />
		</div>	
</div>
</div>


<script type="text/javascript">

$(document).ready(function() {	
	
	$('#txtCode').focus();
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar("fDate");
	ReportUtils.setCurrentDateForCalendar("tDate");	
	
	//Kiem tra phan quyen chuc nang
	<s:if test='mapControl.size > 0' >
		EquipStockAdjustForm._isCreater = 1;
	</s:if>
	EquipStockAdjustForm._params = {
		code: $('#txtCode').val(),			
		status: $('#cbxStatus').val(), 			
		fromDate: $('#fDate').val(),
		toDate: $('#tDate').val(),
		isCreate:EquipStockAdjustForm._isCreater
	};
	
	EquipStockAdjustForm._mapEquipStockAdjust = new Map();

	$('#grid').datagrid({
		url : "/equip-stock-adjust/searchEquipStockAdjust",
		pagination : true,
        rownumbers : true,
        pageNumber : 1,
        scrollbarSize: 0,
        autoWidth: true,
        pageSize: 20,
        pageList: [20, 30, 50],
        autoRowHeight : true,
        fitColumns : true,
		queryParams: EquipStockAdjustForm._params,
		width: $('#gridContainer').width() - 15,
		columns:[[
			{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left'},
			
		    {field:'code', title: 'Mã biên bản', width: 150, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'createFormDate', title: 'Ngày tạo', width: 80, sortable: false, resizable: false , align: 'center', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},	
		    {field:'description', title: 'Lý do',  width: $(window).width()-600, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},	
		    {field:'status',title: 'Trạng thái', width: 100, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
				//0: du thao, 1: cho duyet, 2: da duyet, 3 khong duyet, 4 da huy
		    	var status = "";
		    	if(value == 0){
		    		status = "Dự thảo";
		    	}else if(value == 1){
		    		status = "Chờ duyệt";
		    	}else if(value == 2){
		    		status = "Đã duyệt";
		    	}else if(value == 3){
		    		status = "Không duyệt";
		    	}else if(value == 4){
		    		status = "Đã hủy";
		    	}
		    	return Utils.XSSEncode(status);
		    }},
		    {field: 'note', title: 'Ghi chú', width: 150, sortable: false, resizable: false , align: 'center', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
		    }},	
		    {field:'edit', title:'<a  id="dg_add" class="cmsiscontrol" href="/equip-stock-adjust/change-record" title="Tạo mới"><img src="/resources/images/icon_add.png"/></a>', width: 50, align: 'center', sortable: false, resizable: true, formatter: function(value,row,index){
		    	var html=' ';
		    	//Khong duyet, da duyet hoac da huy
		    	if(row.status == 2 || row.status == 3 || row.status == 4){
		    		html  = html + '<a onclick="EquipStockAdjustForm.initializeEquipStockAdjustDtlGrid('+row.id+',\''+row.code+'\')" href="javascript:void(0)"><img width="18" height="18" src="/resources/images/icon-view.png" title="Xem phiếu nhập kho điều chỉnh"></a>';
		    	}
		    	//Du thao, cho duyet hoac khong duyet
		    	if(row.status == 0 || row.status == 1 || row.status==3){
		    		html = html + '<a  href="/equip-stock-adjust/change-record?id='+row.id+'"><img width="18" height="18" src="/resources/images/icon-edit.png" title="Sửa phiếu nhập kho điều chỉnh"></a>';	
		    	}
		    	return html;
		    }},
	    ]],
	    
	    onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');
	    	$('.datagrid-header-check input').removeAttr('checked');
        	var rows = $('#grid').datagrid('getRows');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(EquipStockAdjustForm._mapEquipStockAdjust.get(rows[i].id)!=null) {
	    			$('#grid').datagrid('checkRow', i);
	    		}
	    	}
	    	
	    	Utils.functionAccessFillControl('gridContainer');
			if($('#gridContainer .cmsiscontrol[id^="dg_"]').length == 0){
				//Quyen duyet 
				EquipStockAdjustForm._isCreater = 0;
				
				//Remove cho duyet va da huy
				$("#cbxStatusUpdate option[value='1']").remove();
				$("#cbxStatusUpdate option[value='4']").remove();
				
				$("#cbxStatus option[value='4']").remove();
				$("#cbxStatus option[value='0']").remove();
				$("#cbxStatus option[value='3']").remove();
				$("#cbxStatusUpdate").val($("#cbxStatusUpdate option:first").val()).change();
				
			} else {
				//Quyen tao
				EquipStockAdjustForm._isCreater = 1;
				$("#cbxStatusUpdate option[value='2']").remove();
				$("#cbxStatusUpdate option[value='3']").remove();
				$("#cbxStatusUpdate").val($("#cbxStatusUpdate option:first").val()).change();
			}
			
   		 	$(window).resize();    		 

	    },
	    onCheck:function(index,row){
			EquipStockAdjustForm._mapEquipStockAdjust.put(row.id,row);   
	    },
	    onUncheck:function(index,row){
	    	EquipStockAdjustForm._mapEquipStockAdjust.remove(row.id);
	    },
	    onCheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		EquipStockAdjustForm._mapEquipStockAdjust.put(row.id,row);
	    	}
	    },
	    onUncheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		EquipStockAdjustForm._mapEquipStockAdjust.remove(row.id);		    		
	    	}
	    }
	});
	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="/equipment-repair-manage/info">Thiết bị</a>
		</li>
		<li>
		<s:if test='isEdit!= null'>
			<span>Cập nhật phiếu yêu cầu sửa chữa</span>
		</s:if>
		<s:else>
			<span>Lập phiếu yêu cầu sửa chữa</span>
		</s:else>
		</li>
	</ul>
</div>
<!-- Trang này - cập nhật QL sửa chữa -->
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label	class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Mã phiếu</label>
					<input	type="text" class="InputTextStyle InputText1Style" id="txtRepairCode" maxlength="50" value='<s:property value="equipRepairForm.equipRepairFormCode"/>'/>	
					<label	class="LabelStyle Label3Style">Mã thiết bị (F9)</label>
					<input	type="text" class="InputTextStyle InputText1Style" id="txtEquipCode" maxlength="50" value='<s:property value="equipRepairForm.equip.code"/>' readonly="true"/>
<%-- 					<span id="lblEquipName"  style="visibility:hidden;color:#199700;width:100px"></span> --%>
					<label	id="lblEquipName"  class="LabelStyle Label1Style" style="color:green;min-width:25px;padding-left:7px;text-align:left;width:auto"></label>
					<label	class="LabelStyle Label11Style">Trạng thái</label>
						<div class="BoxSelect BoxSelect2" id="ddlUsageStatusDiv">
							<select class="MySelectBoxClass InputTextStyle" id="cbxStatus">
							    <option value="0">Dự thảo</option>
							    <option value="1">Chờ duyệt</option>
							    <!-- <option value="3">Đang sửa chữa</option>
							    <option value="5">Hoàn tất</option>
							    <option value="4">Không duyệt</option>
							    <option value="6">Hủy</option> -->
							   <!-- * @description 0: Du thao, 1: Cho duyet, 2: Da duyet, 3: Dang sua chua, 4: Khong duyet, 5: Hoan tat sua chua, 6: Huy. -->
							    <!-- Combobox dacta: Dự thảo, Chờ duyệt, Sửa chữa, Hoàn tất, Không duyệt, Hủy -->
							</select>
						</div>
					<div class="Clear"></div>	
					<label	class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Kho</label>
					<input	type="text" class="InputTextStyle InputText1Style" id="txtStock" maxlength="50" value='<s:property value="stockCode"/>'/>	
					<label	class="LabelStyle Label3Style">Ngày hết hạn bảo hành</label>
					<input	type="text" class="InputTextStyle InputText1Style" id="txtExpiredDate" maxlength="50" value='<s:property value="equipRepairForm.warrantyExpireDateStr"/>' style="text-align: right;"/>
					<label	class="LabelStyle Label1Style">Giá nhân công</label>
					<input	type="text" class="InputTextStyle InputText1Style vinput-money cmsiscontrol" id="txtWorkerPrice" maxlength="50" value='<s:property value="equipRepairForm.workerPrice"/>' style="text-align: right;"/>
					<div class="Clear"></div>	
					<label	class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Tổng tiền</label>
					<input	type="text" class="InputTextStyle vinput-money" id="txtTotalAmount" value='<s:property value="equipRepairForm.totalAmount"/>' style="width: 166px;text-align: right;"/><span style="float:left;margin-top:4px;margin-left:4px;">VNĐ</span>
					<label	class="LabelStyle Label3Style">Tình trạng hư hỏng</label>
					<input	type="text" class="InputTextStyle InputText7Style" id="txtCondition" maxlength="500" value='<s:property value="equipRepairForm.condition"/>'/>
					<div class="Clear"></div>	
					<label	class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Sửa lần thứ</label>
					<input	type="text" class="InputTextStyle InputText1Style" id="txtRepairCount" value='<s:property value="equipRepairForm.repairCount"/>'/>	
					<label	class="LabelStyle Label3Style">Lý do/đề nghị</label>
					<input	type="text" class="InputTextStyle InputText7Style" id="txtReason" maxlength="500" value='<s:property value="equipRepairForm.reason"/>' />
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Ngày biên bản<span class="ReqiureStyle"> *</span></label>
					<div  id="createDateDiv">
						<input type="text" id="createDate" class="InputTextStyle InputText9Style" value="<s:property value="createDate"/>"/>
					</div>
					<label class="LabelStyle Label3Style">Ghi chú</label>
					<textarea rows="4" cols="50" id="note" style="border: 1px solid #c4c4c4; width: 41.5%; height: 40px; font-size: 13px;" maxlength="500"><s:property value="equipRepairForm.note"/></textarea>
					<div class="Clear"></div>
					<div id="divRejectReason">	
						<label class="LabelStyle Label1Style" style="text-align:left;height:auto;">Lý do không duyệt/ duyệt</label>
						<input	type="text" disabled class="InputTextStyle InputText7Style" id="txtRejectReason" maxlength="500" style="margin-top: 6px;" value='<s:property value="equipRepairForm.rejectReason"/>' />
					</div>
					<div class="Clear"></div>	
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
					<p id="successMsg" class="SuccessMsgStyle" style="display:none;"></p>								
				</div>

				<!-- Begin tap tin dinh kem -->
				<h2 class="Title2Style">Tập tin đính kèm</h2>
				<div class="SearchInSection SProduct1Form">
					
					<s:if test='isEdit == null || isEdit == 1 || isEdit == 2'>
						<a href="javascript:void(0);" style="float: left;" title="Lựa chọn tập tin" ><img class="addFileBtn" src="/resources/images/icon_attach_file.png"></a>
						<div class="fileupload-process" style="width: 90%;">
							<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-striped active" id="total-progress" style="opacity: 0; width: 550px;">
								<div data-dz-uploadprogress="" style="width: 200px;" class="progress-bar progress-bar-success"></div>
							</div>
						</div>
					</s:if>
					<div style="float:left; min-width: 500px; width: 90%;">
						<div style="height: auto; margin-left:20px;">
							<s:iterator id="fileVo" value="lstFileVo">
								<div id = "divEquipAttachFile<s:property value="#fileVo.fileId"/>">
								<!-- <div class="divImageUploadFile" id = "divEquipAttachFile<s:property value="#fileVo.fileId"/>"> -->
									<!-- <div class="imageUpdateShowIcon">
										<img class="addFileBtn" src="/resources/images/img-folder-001.png" width="50" height="50">
									</div> -->
									<div class="imageUpdateShowContent">
										<label><s:property value="#fileVo.fileName"/></label><!-- <br /> -->
										<s:if test='isEdit == null || isEdit == 1 || isEdit == 2'>
											<a href="javascript:void(0)" onclick="RepairEquipmentManageCatalog.removeEquipAttachFile(<s:property value="#fileVo.fileId"/>);"><img title="Xóa tập tin" src="/resources/images/icon_delete.png" width="12" height="12" style="padding-left: 5px;"></a>
										</s:if>
									</div>
								</div>
							</s:iterator>
						</div>
						<s:if test='isEdit == null || isEdit == 1 || isEdit == 2'>
							<div class="Clear"></div>
							<div class="table table-striped" class="files" id="previews" style="min-height: 100px; margin-left:20px;">
								<div id="template" class="file-row" style="margin-bottom: 10px; width: 260px;float: left">
									<div style="margin-right: 10px; width: 50px; float: left">
										<span class="preview"><img data-dz-thumbnail /></span>
									</div>
									<div style="display: inline-block; width: 200px; clear: both">
										<div style="padding-right: 10px">
											<div>
												<p class="name" style="overflow: hidden" data-dz-name></p>
												<strong class="error text-danger" data-dz-errormessage></strong>
											</div>
											<div style="">
												<p class="size" data-dz-size></p>
												</div>
											<div>
												<a data-dz-remove href="javascript:void(0)"><span><img border="0" src="/resources/scripts/plugins/uploadify2.1/cancel.png"></span></a>
											</div>
										</div>								
									</div>							
								</div> 
							</div>
						</s:if>
					</div>
					<div class="Clear"></div>
				</div>
				<!-- End tap tin dinh kem -->
				<h2 class="Title2Style">
					<span>Chi tiết hạng mục sửa chữa</span>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="gridDetail"></table>
					</div>
				</div>
				<div class="Clear"></div>
				<p id="successMsgEquipDetail" class="SuccessMsgStyle" style="display:none;"></p>	
				<p id="errMsgEquipDetail" class="ErrorMsgStyle" style="display: none;"></p>
				
				<div class="SearchInSection SProduct1Form">                                
                   <div class="BtnCenterSection" style="padding-bottom: 10px;">
                   		<!-- <s:if test='isEdit == null || isEdit == 1 || isEdit == 2'> -->
							<!-- <button id="btnChangeEquipment" class="BtnGeneralStyle" onclick="RepairEquipmentManageCatalog.saveRepair(<s:property value='equip.id' />);">Cập nhật</button> &nbsp;&nbsp; -->
						<!-- </s:if> -->
						<button id="btnChangeEquipment" class="BtnGeneralStyle cmsiscontrol" onclick="RepairEquipmentManageCatalog.saveRepair(<s:property value='equip.id' />);">Cập nhật</button> &nbsp;&nbsp;
				       <button class="BtnGeneralStyle" onclick="window.location.href='/equipment-repair-manage/info'">Bỏ qua</button>
		           </div>
	            </div>		
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<!-- begin popup chon kho -->
<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchStock" class="easyui-dialog"
		title="Chọn kho"
		data-options="closed:true,modal:true">
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label2Style">Mã đơn vị</label>
				<input id="shopCode" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label2Style">Tên đơn vị</label>
				<input id="shopName" type="text" class="InputTextStyle InputText4Style" maxlength="100"/>		
				<div class="Clear"></div>		
				<label class="LabelStyle Label2Style">Mã KH</label>
				<input id="customerCode" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label2Style">Tên KH/Địa chỉ</label>
				<input id="customerName" type="text" class="InputTextStyle InputText4Style" maxlength="250"/>				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSearchStockDlg" class="BtnGeneralStyle" onclick="RepairEquipmentManageCatalog.searchStock();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="stockGridDialogContainer">
					<table id="stockGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnStockClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchStock').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!-- end popup chon kho -->
<!-- begin popup chon thiet bi -->
<div id="divEquipmentContainer" style="display: none;">
	<div id="easyuiPopupSearchEquipment" class="easyui-dialog"
		title="Chọn thiết bị"
		data-options="closed:true,modal:true" >
		<div class="GeneralForm Search1Form">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<label class="LabelStyle Label6Style">Mã thiết bị</label>
				<input id="equipmentCode" type="text" class="InputTextStyle InputText4Style" maxlength="50" />
				<label class="LabelStyle Label7Style">Số serial</label>
				<input id="seriNumber" type="text" class="InputTextStyle InputText4Style" maxlength="100"/>
				<label class="LabelStyle Label7Style">Loại</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="typeEquipment" onchange="RepairEquipmentManageCatalog.onChangeTypeEquip(this);">
						<option value="">Tất cả</option>
					</select>
				</div>
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Nhóm thiết bị</label>
				<div class="BoxSelect BoxSelect3">
					<!-- <select class="MySelectBoxClass" id="groupEquipment" onchange="">
						<option value="">Tất cả</option>
					</select> -->
					<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
					<select id="groupEquipment" style="width:193px !important;"></select>
				</div>
				<label class="LabelStyle Label7Style">Nhà cung cấp</label>
				<div class="BoxSelect BoxSelect3">
					<!-- <select class="MySelectBoxClass" id="providerId" onchange="">
						<option value="">Tất cả</option>
					</select> -->
					<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
					<select id="providerId" style="width:193px !important;"></select>
				</div>					
				<label class="LabelStyle Label7Style">Năm sản xuất</label>
				<div class="BoxSelect BoxSelect3">
					<select class="MySelectBoxClass" id="yearManufacturing">
					</select>
				</div>
				<!-- <div id="contractDateDiv">
					<input type="text" id="yearManufacturing" class="InputTextStyle InputText5Style"/>
				</div> -->
				<div class="Clear"></div>
				<label class="LabelStyle Label6Style">Kho (F9)</label>
				<s:if test="idRecordDelivery == null">
					<input type="text" id="stockCode" class="InputTextStyle InputText4Style" />
				</s:if>
				<s:else>
					<input type="text" id="stockCode" class="InputTextStyle InputText4Style" value="<s:property value="shopCode"/>"/>
				</s:else>				
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnSeachEquipmentDlg" class="BtnGeneralStyle" onclick="RepairEquipmentManageCatalog.searchEquipment();">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="GeneralForm SProduct1Form">
				<h2 class="Title2Style">Kết quả tìm kiếm</h2>
				<div class="GridSection" id="equipmentGridDialogContainer">
					<table id="equipmentGridDialog" class="easyui-datagrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="btnEquipmentClose" class="BtnGeneralStyle"	onclick="$('#easyuiPopupSearchEquipment').dialog('close');">Đóng</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgEquipmentDlg" class="ErrorMsgStyle" style="display: none;"></p>
			</div>
	</div>
</div>
<!-- end popup chon thiet bi -->
<input type="hidden" id="idRecordHidden" value="<s:property value="idForm"/>">
<input type="hidden" id="isEdit" value="<s:property value="isEdit"/>">
<s:hidden id="checkExpiredDate" name="expiredDate"></s:hidden>
<div><input type="hidden" id="formCreateDate" value="<s:property value="createDate"/>"></div>
<script type="text/javascript">
$(document).ready(function() {
	disabled('txtRepairCode');
	disabled('txtStock');
	disabled('txtTotalAmount');
	disabled('txtRepairCount');
	setDateTimePicker('createDate');
	var createFormDate = $('#formCreateDate').val();
	if (createFormDate != undefined && createFormDate != null && createFormDate != '') {
		$('#createDate').val(createFormDate);
	} else {
		ReportUtils.setCurrentDateForCalendar("createDate");
	}

	/** Begin vuongmq; 06/04/2015 */
	disabled('txtExpiredDate');
	//disabled('txtWorkerPrice');
	RepairEquipmentManageCatalog._flagPagePheDuyet = false; /** // de cai nay de tinh tien dung cac ham js: totalAmountChangeText, tongVatTuDetail; cho them hang muc nua*/
	RepairEquipmentManageCatalog._rolePermission = null;
	if ($('#txtWorkerPrice').prop("disabled") == true) {
		RepairEquipmentManageCatalog._rolePermission = RepairEquipmentManageCatalog._roleKTNPP;
	} else {
		RepairEquipmentManageCatalog._rolePermission = RepairEquipmentManageCatalog._roleTNKD;
	}
	RepairEquipmentManageCatalog._msgGiaNhanCongMax = '';
	$('#txtWorkerPrice').blur( function(){
		RepairEquipmentManageCatalog.maxWorkerPriceChangeTextDetail(true);
		var inputNum = $(this).val();
		if (inputNum == null || inputNum == '') {
			inputNum = 0;
		}
		if (RepairEquipmentManageCatalog._maxWorkerPriceDetail == null || RepairEquipmentManageCatalog._maxWorkerPriceDetail == '') {
			RepairEquipmentManageCatalog._maxWorkerPriceDetail = 0;
		}
		// luc nay gia tri RepairEquipmentManageCatalog._maxWorkerPriceDetail: da lay duoc max duoi gridDetail de so sanh
		if (Number($(this).val()) > Number(RepairEquipmentManageCatalog._maxWorkerPriceDetail)) {
			var msg = 'Giá nhân công <= max (giá nhân công từng hạng mục) là '+formatCurrency(RepairEquipmentManageCatalog._maxWorkerPriceDetail);
			$('#errMsgEquipDetail').html(msg).show();
			RepairEquipmentManageCatalog._msgGiaNhanCongMax = msg;
			RepairEquipmentManageCatalog._msgGiaNhanCongMaxFocus = '#txtWorkerPrice';
		} else {
			$('.ErrorMsgStyle').html('').hide();
			RepairEquipmentManageCatalog._msgGiaNhanCongMax = '';
			RepairEquipmentManageCatalog._msgGiaNhanCongMaxFocus = '';
		}
	});
	/** End vuongmq; 06/04/2015 */
	var isEdit = $('#isEdit').val();
	var status = '<s:property value="status"/>';
	
	RepairEquipmentManageCatalog._arrFileDelete = [];
	RepairEquipmentManageCatalog._countFile = Number('<s:property value="lstFileVo.size()"/>');
	
	$('#gridDetail').datagrid({
// 		url : '/equipment-repair-manage/searchRepairDetailEx',
		autoRowHeight : true,
		rownumbers : true, 
		singleSelect: true,
		fitColumns:true,
		scrollbarSize:0,
		width: $('#gridContainer').width(),
		autoWidth: true,	
		queryParams:{
			equipRepairId: $("#idRecordHidden").val()
		},
		columns:[[		  
			{field: 'hangMuc',title:'Hạng mục (F9)', width: 330, sortable:false,resizable:false, align: 'left' , formatter: function(value, row, index) {
				var index1 = '';
		    	if(row.id != undefined && row.id != null){
		    		index1 = row.id;
		    	}
				var html = '<input type="text" class="equipItemCodeInGridClzz InputTextStyle" id="equipItemCodeInGrid_'+index1+'" style="margin-bottom:2px; width:100%" maxlength="500">';
				if( row.hangMuc != undefined && row.hangMuc!=null && row.hangMuc!=''){
					html = '<span id="hangMuc_' + row.id + '" class="equipItemCodeInGridClzz" style="margin-bottom:2px">' + Utils.XSSEncode(row.hangMuc) + '</span>';					
				}
				return html;
			}},
			{field: 'ngayBatDauBaoHanh',title:'Ngày bắt đầu bảo hành', width: 130, fixed:true, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var idx = '';
				if (index != null) {
					idx = index;
				}
		    	var index1 = -1;
		    	if(row.id != undefined && row.id != null){
		    		index1 = row.id;
		    	}
				var html='<input type="text" id="warantyDateInGrid_'+index1+'" class="warantyDateInGridClzz InputTextStyle InputText4Style" style="margin-right:0px; margin-bottom:2px; text-align: right;" onchange="RepairEquipmentManageCatalog.changeNgayBatDauBaoHanh('+idx+','+index1+');" >';
				if (row.ngayBatDauBaoHanh != undefined && row.ngayBatDauBaoHanh != null && row.ngayBatDauBaoHanh != '') {
					html = '<input type="text" id="warantyDateInGrid_' + row.id + '" class="warantyDateInGridClzz InputTextStyle InputText4Style" style="margin-right:0px; margin-bottom:2px; text-align: right;" value="' + Utils.XSSEncode(row.ngayBatDauBaoHanh) + '" onchange="RepairEquipmentManageCatalog.changeNgayBatDauBaoHanh(' + idx + ',' + index1 + ');">';
					//setDateTimePicker('fDate');					
				}
				return html;
			}},
			{field: 'baoHanh',title:'Số tháng bảo hành', width: 100, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
		    	var index1 = '';
		    	if(row.id != undefined && row.id != null){
		    		index1 = row.id;
		    	}
				var html='<input type="text" id="warantyInGrid_'+index1+'" class="warantyInGridClzz InputTextStyle vinput-money" style="margin-bottom:2px; width: 84%; text-align: right;" maxlength="17">';
		    	if( row.baoHanh != undefined && row.baoHanh!=null && row.baoHanh!=''){
					html = '<input type="text" id="warantyInGrid_'+row.id+'" class="warantyInGridClzz InputTextStyle vinput-money" style="margin-bottom:2px; width: 84%; text-align: right;" maxlength="17" value="'+formatCurrency(row.baoHanh)+'">';						
				}
				return html;
			}},
			{field: 'ngayHetHanBaoHanh',title:'Ngày hết hạn bảo hành', width: 130, fixed:true, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
		    	var index1 = '';
		    	if(row.id != undefined && row.id !=null){
		    		index1 = row.id;
		    	}
				var html='<input type="text" id="warantyExpiredDateInGrid_'+index1+'" class="warantyExpiredDateInGridClzz InputTextStyle InputText4Style" style="margin-right:0px; margin-bottom:2px; text-align: right;" >';
				if (row.ngayHetHanBaoHanh != undefined && row.ngayHetHanBaoHanh != null && row.ngayHetHanBaoHanh != '') {
					html = '<input type="text" id="warantyExpiredDateInGrid_' + row.id + '" class="warantyExpiredDateInGridClzz InputTextStyle InputText4Style" style="margin-right:0px; margin-bottom:2px; text-align: right;" value="' + Utils.XSSEncode(row.ngayHetHanBaoHanh) + '">';
				}
				return html;
			}},
			{field: 'donGiaVatTuDinhMuc',title:'Đơn giá vật tư định mức',width: 165,sortable:false,resizable:false, align: 'right' , formatter: function(value, row, index) {
				var index1 = '';
		    	if(row  != undefined && row.id != null){
		    		index1 = row.id;
		    	}
				var html='<span class="materialPriceDefaultInGridClzz" id="materialPriceDefaultInGrid_'+index1+'" style="margin-bottom:2px; text-align: right;" maxlength="22"></span>';
		    	if( row.donGiaVatTuDinhMuc != undefined && row.donGiaVatTuDinhMuc!=null && row.donGiaVatTuDinhMuc!=''){
		    		html = '<span id="materialPriceDefaultInGrid_'+row.id+'" class="materialPriceDefaultInGridClzz" style="margin-bottom:2px; text-align: right;">'+row.donGiaVatTuDinhMuc+'</span>';					
				}
				return html;
			}},
			{field: 'donGiaVatTu',title:'Đơn giá vật tư',width: 140,sortable:false,resizable:false, align: 'left' , formatter: function(value, row, index) {
				var idx = '';
				if (index != null) {
					idx = index;
				}
				var index1 = '';
		    	if(row.id != undefined && row.id != null){
		    		index1 = row.id;
		    	}
				var html='<input type="text" class="materialPriceInGridClzz InputTextStyle vinput-money" id="materialPriceInGrid_'+index1+'" style="margin-bottom:2px; text-align: right; width:90%" maxlength="17" gridIndex="'+idx+'">';
		    	if( row.donGiaVatTu != undefined && row.donGiaVatTu!=null && row.donGiaVatTu!=''){
					html = '<input type="text" class="materialPriceInGridClzz InputTextStyle vinput-money" id="materialPriceInGrid_'+row.id+'" style="margin-bottom:2px; text-align: right; width:90%" maxlength="17" gridIndex="'+idx+'" value="'+formatCurrency(row.donGiaVatTu)+'">';						
				}
				return html;
			}},
			{field: 'donGiaNhanCongDinhMuc',title:'Đơn giá nhân công định mức',width: 170,sortable:false,resizable:false, align: 'right', formatter: function(value, row, index) {
				var index1 = '';
		    	if(row.id != undefined && row.id != null){
		    		index1 = row.id;
		    	}
				var html='<span type="text" class="wokerPriceDefaultInGridClzz" style="margin-bottom:2px; text-align: right;" id="wokerPriceDefaultInGrid_'+index1+'" maxlength="22"></span>';
				if( row.donGiaNhanCongDinhMuc != undefined && row.donGiaNhanCongDinhMuc!=null && row.donGiaNhanCongDinhMuc!=''){
					html = '<span id="wokerPriceDefaultInGrid_'+row.id+'" class="wokerPriceDefaultInGridClzz" style="margin-bottom:2px; text-align: right;">'+row.donGiaNhanCongDinhMuc+'</span>';				
				}
				return html;
			}},
			{field: 'donGiaNhanCong',title:'Đơn giá nhân công',width: 140,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var idx = '';
				if (index != null) {
					idx = index;
				}
				var index1 = '';
		    	if(row.id != undefined && row.id != null){
		    		index1 = row.id;
		    	}
				var html='<input type="text" class="wokerPriceInGridClzz InputTextStyle vinput-money" id="wokerPriceInGrid_'+index1+'" style="margin-bottom:2px; text-align: right; width:90%" maxlength="17" gridIndex="'+idx+'" >';
				if( row.donGiaNhanCong != undefined && row.donGiaNhanCong!=null && row.donGiaNhanCong!=''){
					html = '<input type="text" class="wokerPriceInGridClzz InputTextStyle vinput-money" style="margin-bottom:2px; text-align: right; width:90%" id="wokerPriceInGrid_'+row.id+'" maxlength="17" gridIndex="'+idx+'" value="'+formatCurrency(row.donGiaNhanCong)+'">';						
				}
				return html;
			}},
			{field: 'soLuong',title:'Số lượng',width: 100,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var idx = '';
				if (index != null) {
					idx = index;
				}
				var index1 = '';
		    	if(row.id != undefined && row.id != null){
		    		index1 = row.id;
		    	}
				var html='<input type="text" class="quantityInGridClzz InputTextStyle vinput-money" id="quantityInGrid_'+index1+'" style="margin-bottom:2px; width: 85%; text-align: right;" maxlength="17" gridIndex="'+idx+'" >';
				if( row.soLuong != undefined && row.soLuong!=null && row.soLuong!=''){
					html = '<input type="text" class="quantityInGridClzz InputTextStyle vinput-money" id="quantityInGrid_'+row.id+'" style="margin-bottom:2px; width: 85%; text-align: right;" maxlength="17" gridIndex="'+idx+'" value="'+formatCurrency(row.soLuong)+'">';						
				}
				return html;
			}},
			{field: 'description',title:'Ghi chú',width: 180,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {
				var idx = '';
				if (index != null) {
					idx = index;
				}
				var index1 = '';
				if(row.id != undefined && row.id != null){
					index1 = row.id;
				}
				var html='<input type="text" class="descriptionInGridClzz InputTextStyle" id="descriptionInGrid_'+index1+'" style="margin-bottom:2px; width: 90%; text-align: left;" maxlength="250" gridIndex="'+idx+'" >';
				if( row.description != undefined && row.description!=null && row.description!=''){
					html = '<input type="text" class="descriptionInGridClzz InputTextStyle" id="descriptionInGrid_' + row.id + '" style="margin-bottom:2px; width: 90%; text-align: left;" maxlength="250" gridIndex="' + idx + '" value="' + Utils.XSSEncode(row.description) + '">';						
				}
				return html;
			}},
			{field: 'tongTien',title:'Tổng tiền',width: 170,sortable:false,resizable:false, align: 'left', formatter: function(value, row, index) {	 
				var index1 = '';
		    	if(row.id != undefined && row.id != null){
		    		index1 = row.id;
		    	}
				var html='<input type="text" class="tongTienClzz InputTextStyle vinput-money" id="tongTien_'+index1+'" style="margin-bottom:2px; width: 90%; text-align: right;" disabled="disabled">';
				if( row.tongTien != undefined && row.tongTien!=null) {
					html = '<input type="text" class="tongTienClzz InputTextStyle vinput-money" id="tongTien_'+row.id+'" style="margin-bottom:2px; width: 90%; text-align: right;" value="'+formatCurrency(row.tongTien)+'" disabled="disabled">';						
				}
				return html;
			}},
			{field: 'lanSua', title: 'Lần sửa', width: 60, sortable:false,resizable:false,align: 'left', formatter: function(value, row, index) {
				var index1 = '';
		    	if(row.id != undefined && row.id != null){
		    		index1 = row.id;
		    	}
				var html='<input type="text" class="lanSuaClzz InputTextStyle" id="lanSua_'+index1+'" style="width: 80%; margin-bottom: 2px; text-align: right;" disabled="disabled">';
				if( row.lanSua != undefined && row.lanSua!=null && row.lanSua!=''){
					html = '<input type="text" class="lanSuaClzz InputTextStyle" id="lanSua_'+row.id+'" style="margin-bottom:2px; width: 80%; text-align: right;" value="'+formatCurrency(row.lanSua)+'" disabled="disabled">';						
				}
				return html;
			}},	
			{field:'delete', title:'', width:50, align:'center', formatter: function(value, row, index) {
			    var	html='';
			    if(isEdit == undefined || isEdit == null || isEdit == '' || isEdit == '1'){
			    	html = '<a href="javascript:void(0);" onclick="RepairEquipmentManageCatalog.deleteRow('+index+')"><img src="/resources/images/icon-delete.png"></a>';
			    }
			    return html;
			}},
		]],
		onAfterEdit : function(index, row, changes){
			
		},
		onLoadSuccess :function(data){   			 	    	
	    	$(window).resize();	
	    	if (data != null) {
				$('.datagrid-header-rownumber').html('STT');	
// 				if($("#idRecordHidden").val() != undefined && $("#idRecordHidden").val() != ""){
// 					RepairEquipmentManageCatalog._numberEquipInRecord = data.total;
// 				}
			}
	    	RepairEquipmentManageCatalog.insertEquipmentItemInGrid();
	    	if(RepairEquipmentManageCatalog._colum1Width!=null){

			}
        	RepairEquipmentManageCatalog.bindEventChangeText();
        	
        	if(isEdit == '2' || isEdit == '0'){
        		$('input').attr('disabled','disabled');
        		// vuongmq; 01/07/2015; disableDateTimePicker; ngay bat dau bao hanh va het han bao hanh 
        		$('.quantityInGridClzz').each(function () {
				    var idQuan = $(this).attr('id');
				    var id = idQuan.split('_')[1];
				    if (id != undefined && id != null && id != '') {
			           disableDateTimePicker('warantyDateInGrid_'+id);
			           disableDateTimePicker('warantyExpiredDateInGrid_'+id);
					}
				});
        	}
        	RepairEquipmentManageCatalog.loadDateTimerPickerNgayBaoHanh();
        	/*$(".warantyDateInGridClzz:not(.hasDatepicker)").each(function() {
				setDateTimePicker($(this).attr("id"));
			});*/
		}
	});
	if (isEdit!=null && isEdit != '') {
		var html = '';
		var ngayHetHanBaoHanh = '<s:property value="equipRepairForm.warrantyExpireDateStr"/>';
		$('#txtExpiredDate').val(ngayHetHanBaoHanh);
		var totalAmount = '<s:property value="equipRepairForm.totalAmount"/>';
		$('#txtTotalAmount').val(formatCurrency(totalAmount));
		var workerPrice = '<s:property value="equipRepairForm.workerPrice"/>';
		$('#txtWorkerPrice').val(formatCurrency(workerPrice));
		//RepairEquipmentManageCatalog._maxWorkerPriceDetail = workerPrice; // gia tri max nhan cong duoc gan lai khi edit; vuongmq; 06/05/2015 khong su dung nua
		$('#txtTotalAmount').val(formatCurrency(totalAmount));
		if (isEdit == '0') {// ko cho sua gi ca
			disabled('txtEquipCode');
			disabled('txtCondition');
			disabled('txtReason');
			disabled('note');
			// vuongmq; 29/06/2015; cap nhat trang thai cho duyet sang huy;
			if (status == StatusRecordsEquip.WAITING_APPROVAL) {
				html = '<option value="1">Chờ duyệt</option> <option value="4">Hủy</option>';
			} else {
				disableSelectbox('cbxStatus');
				html = '<option value="6">Hoàn tất</option> <option value="4">Hủy</option>';
				$('#btnChangeEquipment').remove();
			}
			/*disableSelectbox('cbxStatus');
			html = '<option value="1">Chờ duyệt</option> <option value="6">Hoàn tất</option> <option value="4">Hủy</option>';*/
			$('#cbxStatus').html(html).change();
			$('#cbxStatus').val(status).change();
		} else if (isEdit == '2'){ // chi cho sua trang thai
			disabled('txtEquipCode');
			disabled('txtCondition');
			disabled('txtReason');
			disabled('note');
			if (status == '2') {
				html = '<option value="2">Đã Duyệt</option> <option value="5">Đang sửa chữa</option>';
			} else if (status == '5') {
				html = '<option value="5">Đang sửa chữa</option> <option value="6">Hoàn tất</option>';
			}
			$('#cbxStatus').html(html).change();
			$('#cbxStatus').val(status).change();
			
		} else if (isEdit == '1'){ // cho sua tat ca
			if (status == '0') {
				html = '<option value="0">Dự thảo</option> <option value="1">Chờ duyệt</option> <option value="4">Hủy</option>';
				$('#divRejectReason').hide();
			} else if(status == '3') {
				html = '<option value="1">Chờ duyệt</option> <option value="3">Không duyệt</option> <option value="4">Hủy</option>';
			}
			
			$('#cbxStatus').html(html).change();
			$('#cbxStatus').val(status).change();
		}
		$('#gridDetail').datagrid({url : '/equipment-repair-manage/searchRepairDetailEx',queryParams:{equipRepairId: $("#idRecordHidden").val()}});
		
		
		if(status==0 || status==1){
			$('#divRejectReason').hide();
		} 
		
	} else {
		// vuongmq; 17/04/2015
		$('#txtTotalAmount').val('');
		$('#txtWorkerPrice').val('');
		$('#txtEquipCode').focus();
		$('#divRejectReason').hide();
	}
	RepairEquipmentManageCatalog.insertEquipmentItemInGrid();
	$('#txtEquipCode').bind('keyup', function(e){
		if (e.keyCode==keyCode_F9) {
			RepairEquipmentManageCatalog.showPopupSearchEquipment();
		}
	});
	$('#stockCode').bind('keyup',function(event){
		if(event.keyCode == keyCodes.F9){
			RepairEquipmentManageCatalog.showPopupStock();
		}
	});	
	if (isEdit == undefined || isEdit == null || isEdit == '' || isEdit == '1' || isEdit == '2') {
		UploadUtil.initUploadFileByEquipment({
			url: '/equipment-repair-manage/saveRepair',
			elementSelector: 'body',
			elementId: 'body',
			clickableElement: '.addFileBtn',
			parallelUploads: 5
		}, function (data) {
			if (data.error != undefined && !data.error) {
				$("#successMsg").html("Lưu dữ liệu thành công").show();
				setTimeout(function() {
					window.location.href = '/equipment-repair-manage/info';
				}, 1000);				
			} else {
				if (data.errMsg != undefined && data.errMsg != null && data.errMsg != '') {
					$('#errMsg').html(data.errMsg).show();
				}
			}
		});
	}
});
</script>
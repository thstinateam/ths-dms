<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="javascript:void(0);">Thiết bị</a>
		</li>
		<li>
			<span>Phê duyệt sửa chữa</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<!-- <div id="pnlListEquipment" class="easyui-panel" title="Thông tin tìm kiếm" style="width:auto; height:auto; padding-top: 20px;"data-options="collapsible:true,closable:false,border:false"> -->
				<h2 class="Title2Style">Thông tin tìm kiếm <span style="float:right;margin-right:15px;">
						<a href="javascript:void(0);" title="Ẩn/ hiện" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="Clear"></div>
						<!-- <label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Mã đơn vị</label> -->
						<label class="LabelLeftStyleM3 LabelLeft3Style">Mã đơn vị</label>						
						<input id="shop" type="text" style="height: auto;" class="InputTextStyle InputText1Style" maxlength="50"/>
						<label	class="LabelStyle Label1Style">Mã phiếu</label>
						<input	type="text" class="InputTextStyle InputText1Style" id="txtRepairCode" maxlength="50" />	
						<label	class="LabelStyle Label1Style">Số serial</label> 
						<input type="text" class="InputTextStyle InputText1Style" id="txtSerial" maxlength="100"/>
						<div class="Clear"></div>
						<!-- <label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Mã KH</label> -->
						<label class="LabelLeftStyleM3 LabelLeft3Style">Mã KH</label>
						<input id="customerCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"  />
						<label class="LabelStyle Label1Style">Tên KH</label>
						<input id="customerName" type="text" class="InputTextStyle InputText1Style" maxlength="250" />
						<label class="LabelStyle Label1Style">Mã thiết bị</label> 
						<input	type="text" class="InputTextStyle InputText1Style" id="txtCode" maxlength="50" /> 
						<div class="Clear"></div>
						<!-- <label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Ngày tạo từ</label> -->
						<label class="LabelLeftStyleM3 LabelLeft3Style">Ngày biên bản từ</label>
						<div id="fromDateDiv">
							<input type="text" id="fDate" value='<s:property value="fromDate"/>' class="InputTextStyle InputText6Style" />
						</div>
						<label class="LabelStyle Label1Style">Đến</label>
						<div id="toDateDiv">
							<input type="text" id="tDate" value='<s:property value="toDate"/>' class="InputTextStyle InputText6Style" />
						</div>
						<div class="Clear"></div>		
						<div class="SearchInSection SProduct1Form">
							<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
							<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
							<div class="BtnCenterSection">
								<button id="btnSearch" class="BtnGeneralStyle" onclick="RepairEquipmentManageCatalog.searchRepairEx();">Tìm kiếm</button>
							</div>
							<div class="Clear"></div>
						</div>	
					</div>			
				
				<h2 class="Title2Style">Danh sách phiếu sửa chữa thiết bị
					<div style="float:right; margin-top: -5px;margin-right: 27px;">
					<button style="float:right;" id="btnSaveLst" class="BtnGeneralStyle" onclick="RepairEquipmentManageCatalog.updateStatus();" >Lưu</button>
					
					<input style="float:right;margin-right: 5px;" id="rejectReason" type="text" class="InputTextStyle InputText2Style" maxlength="500" tabindex="7"/>		
					<label style="float:right;margin-right: 5px;"class="LabelStyle Label1Style">Lý do</label>
					<div style="float:right;margin-right: 5px;" class="BoxSelect BoxSelect2">							
							<select class="MySelectBoxClass InputTextStyle" id="cbxStatusUpdate">
								<option value="-1" selected="selected">Chọn</option>
							    <option value="2">Duyệt</option>
							    <option value="3">Không duyệt</option>
							</select>
						</div>
					</div>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="grid"></table>
					</div>
				</div>		
				<div class="Clear"></div>
				<!-- </div> -->
			</div> <!-- end class="ToolBarSection" -->
		</div>
	</div>
	<div class="Clear"></div>
	<!-- Chi tiet chuc nang Form -->
	<div class="ContentSection" style="visibility:hidden; display: none;" id="equipHistoryTemplateDiv">
		<div class="BreadcrumbSection">
		    <ul class="ResetList FixFloat BreadcrumbList">
		        <li><span>Chi tiết hạng mục sửa chữa </span><span id="equipmentCodeFromSpan" style="color:#199700"></span></li>
		    </ul>
		</div>
		<div class="GeneralCntSection">
			<div class="GridSection" id="equipHistoryDgContainerGrid">
				 <table id="equipHistoryDg"></table>
			</div>
		</div>
	</div>
</div>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {	
	$('#txtRepairCode').focus();
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
// 	ReportUtils.setCurrentDateForCalendar("fDate");
// 	ReportUtils.setCurrentDateForCalendar("tDate");	

	/*$('#pnlListEquipment').width($(window).width()- 18);
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
	}, 1000);*/
	$('#shopCode').focus();
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if (level == ShopTreeNode.ROOT) {//Root
				return '<div class="tree-root" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:240px"><span class="tree-root-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-root-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if (level == ShopTreeNode.IDP) {//IDP
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if (level == ShopTreeNode.KENH4 || level == ShopTreeNode.KENH21) {
					return '<div class="tree-kenh" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-kenh-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-kenh-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if (level == ShopTreeNode.SUBMIEN18 || level == ShopTreeNode.SUBMIEN22) {
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if (level == ShopTreeNode.MIEN) {
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if (level == ShopTreeNode.VUNG) {
				return '<div class="tree-vung-con" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-con-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-con-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper"); 
	RepairEquipmentManageCatalog._params = {
			equipCode: $('#txtCode').val(),
			formCode: $('#txtRepairCode').val(),
			fromDate: $('#fDate').val(),
			toDate: $('#tDate').val(),
			customerCode: $('#customerCode').val(),
			customerName: $('#customerName').val(),
			seri: $('#txtSerial').val()
	};
	RepairEquipmentManageCatalog._mapRepair = new Map();
	$('#grid').datagrid({
		url : "/equipment-repair-manage/searchRepairEx",
		pagination : true,
        rownumbers : true,
        pageNumber : 1,
        scrollbarSize: 0,
        autoWidth: true,
        pageSize: 10,
        pageList: [10, 20, 30, 50, 100],
        autoRowHeight : true,
        //fitColumns : true,
		queryParams: RepairEquipmentManageCatalog._params,
		width: $('#gridContainer').width() - 15,
		checkOnSelect: false, // vuongmq; 19/05/2015; chi cho check checkox

		frozenColumns:[[        
			{field: 'check',checkbox:true, width: 50, sortable:false,resizable:false, align: 'left'},
			{field:'edit', title:'', width: 70, align: 'center', sortable: false, resizable: true, formatter: function(value,row,index){
		    	return '<a onclick="RepairEquipmentManageCatalog.viewDetail('+row.id+',\''+row.maPhieu+'\')" href="javascript:void(0)"><img width="18" height="18" src="/resources/images/icon-view.png" title="Xem hạng mục sửa chữa '+row.maPhieu+'"></a><a onclick="" href="/equipment-repair-manage/change-recordEx?id='+row.id+'"><img width="18" style="padding-left: 5px;" height="18" src="/resources/images/icon-edit.png" title="Chỉnh sửa '+row.maPhieu+'"></a>';
			}},
			{field:'donVi', title: 'Đơn vị', width: 180, align: 'left',sortable: false, resizable: false, formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'maPhieu', title: 'Mã phiếu', width: 120, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'loaiThietBi', title: 'Loại thiết bị', width: 160, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'nhomThietBi', title: 'Nhóm thiết bị', width: 180, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'maThietBi', title: 'Mã thiết bị', width: 180, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		]],
		columns:[[
		    {field:'soSeri', title: 'Số serial', width: 120, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    // {field:'ngayTao', title: 'Ngày tạo', width: 80, sortable: false, resizable: false , align: 'centre', formatter:function(value,row,index){
		    // 	return Utils.XSSEncode(value);
		    // }},	
		    {field:'ngayTao', title: 'Ngày biên bản', width: 80, sortable: false, resizable: false , align: 'centre', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},	
		    {field:'khachHang', title: 'Khách hàng', width: 160, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);	
		    }},
		    {field:'diaChi', title: 'Địa chỉ', width: 200, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);	
		    }},
// 		    {field:'trangThai',title: 'Trạng thái', width: 50, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
// 		    	var status = "";
// 		    	if(value == 0){
// 		    		status = "Dự thảo";
// 		    	}else if(value == 1){
// 		    		status = "Chờ duyệt";
// 		    	}else if(value == 2){
// 		    		status = "Đã Duyệt";
// 		    	}else if(value == 3){
// 		    		status = "Không duyệt";
// 		    	}else if(value == 4){
// 		    		status = "Hủy";
// 		    	}else if(value == 5){
// 		    		status = "Đang sửa chữa";
// 		    	}else if(value == 6){
// 		    		status = "Hoàn tất";
// 		    	}
// 		    	return Utils.XSSEncode(status);
// 		    }},
			{field: 'url', title: 'Tập tin đính kèm', width: 70, sortable: false, resizable: false, align: 'center', formatter: function(value, row, index) {
		    	/*type= 2: la file sua chua */
		    	if (value != null && value != '') {
		    		return '<a href="javascript:void(0)" onclick="General.downloadEquipAttechFile(' + row.id + ', 2);" title="Danh sách tập tin">Tập tin</a>';
		    	} else {
		    		return '';
		    	}
		    }},
			{field:'tinhTrangHuHong', title: 'Tình trạng hư hỏng', width: 150, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
				return Utils.XSSEncode(value);
		    }},	
			{field:'lyDoDeNghi', title: 'Lý do/Đề nghị', width: 150, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},	
			{field:'tongTien', title: 'Tổng tiền', width: 120, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
				if (value != undefined && value != null) {
		    		return formatCurrency(value);
		    	}	
		    	return '';	
		    }},
		    {field: 'note', title: 'Ghi chú', width: 170, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);	
		    }},
	    ]],
	    onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');
	    	$('.datagrid-header-check input').removeAttr('checked');
        	var rows = $('#grid').datagrid('getRows');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(RepairEquipmentManageCatalog._mapRepair.get(rows[i].id)!=null) {
	    			$('#grid').datagrid('checkRow', i);
	    		}
	    	}
   		 	$(window).resize();    		 
	    },
	    onCheck:function(index,row){
			RepairEquipmentManageCatalog._mapRepair.put(row.id,row);   
	    },
	    onUncheck:function(index,row){
	    	RepairEquipmentManageCatalog._mapRepair.remove(row.id);
	    },
	    onCheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		RepairEquipmentManageCatalog._mapRepair.put(row.id,row);
	    	}
	    },
	    onUncheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		RepairEquipmentManageCatalog._mapRepair.remove(row.id);		    		
	    	}
	    }
	});
	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="javascript:void(0);">Thiết bị</a>
		</li>
		<li>
			<span>Quản lý sửa chữa</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<!-- <div id="pnlListEquipment" class="easyui-panel" title="Thông tin tìm kiếm" style="width:auto; height:auto; padding-top: 20px;"data-options="collapsible:true,closable:false,border:false"> -->
				<h2 class="Title2Style">Thông tin tìm kiếm <span style="float:right;margin-right:15px;">
						<a href="javascript:void(0);" title="Ẩn/ hiện" id="searchHiddenLink" class="searchShow" onclick="Utils.toggleSearchInput(this);"></a>
					</span>
				</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="Clear"></div>
						<!-- <label	class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Số serial</label>  -->
						<label	class="LabelLeftStyleM3 LabelLeft2Style" >Số serial</label> 
						<input type="text" class="InputTextStyle InputText1Style" id="txtSerial" maxlength="100"/>		
						<label class="LabelStyle Label1Style">Mã thiết bị</label> 
						<input	type="text" class="InputTextStyle InputText1Style" id="txtCode" maxlength="50" /> 
						<label class="LabelStyle Label1Style">Loại thiết bị</label>
						<div class="BoxSelect BoxSelect2" id="ddlEquipCategoryDiv">
							<select class="MySelectBoxClass" id="ddlEquipCategory" onchange="RepairEquipmentManageCatalog.onchangeDdlEquipCategoryAuto(this);">
								<option value="-1" selected="selected">Tất cả</option>
								<s:iterator value="lstEquipCategory" var="equiqCtg">
									<option value="<s:property value='id' />"><s:property value="code" /> - <s:property value="name" /></option>
								</s:iterator>
							</select>
						</div>
						<div class="Clear"></div>	
						<!-- <label	class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Nhóm thiết bị</label> -->
						<label	class="LabelLeftStyleM3 LabelLeft2Style" >Nhóm thiết bị</label>
						<div class="BoxSelect BoxSelect2" id="ddlEquipGroupIdDiv">
							<!-- <select class="MySelectBoxClass InputTextStyle" id="ddlEquipGroupId">
								<option value="-1" selected="selected">Tất cả</option>
								<s:iterator value="result.get('lstEquipGroup')" var="equipPro">
									<option value="<s:property value='id' />"><s:property value="code" /> - <s:property value="name" /></option>
								</s:iterator>
							</select> -->
							<!-- tamvnm: thay doi thanh autoCompleteCombobox -->
							 <select id="ddlEquipGroupId"></select>
						</div>	
						<label	class="LabelStyle Label1Style">Mã phiếu</label>
						<input	type="text" class="InputTextStyle InputText1Style" id="txtRepairCode" maxlength="50" />	
						<!-- <label	class="LabelLeftStyleM3 LabelLeft2Style" >Trạng thái</label> -->
						<label	class="LabelStyle Label1Style" >Trạng thái</label>
						<div class="BoxSelect BoxSelect2" id="ddlUsageStatusDiv">
							<select class="MySelectBoxClass InputTextStyle" id="cbxStatus">
								<option value="-1" selected="selected">Tất cả</option>
							    <option value="0">Dự thảo</option>
							    <option value="1">Chờ duyệt</option>
							    <!-- <option value="2">Đã Duyệt</option> -->
							    <option value="3">Không duyệt</option>
							    <option value="5">Đang sửa chữa</option>
							    <option value="6">Hoàn tất</option>
							    <option value="4">Hủy</option>
							</select>
						</div>
						<div class="Clear"></div>		
						<!-- <label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Ngày tạo từ</label> -->
						<label class="LabelLeftStyleM3 LabelLeft2Style" >Ngày biên bản từ</label>
						<div id="fromDateDiv">
							<input type="text" id="fDate" class="InputTextStyle InputText6Style" />
						</div>
						<label class="LabelStyle Label1Style">Đến</label>
						<div id="toDateDiv">
							<input type="text" id="tDate" class="InputTextStyle InputText6Style" />
						</div>
						<label	class="LabelStyle Label1Style" title="Trạng thái tham gia giao dịch">Trạng thái thanh toán</label>
						<div class="BoxSelect BoxSelect2" id="ddlTradeStatusDiv">
							<select class="MySelectBoxClass InputTextStyle" id="cbxStatusPayment">
								<option value="-1" selected="selected">Tất cả</option>
								<option value="2">Đã thanh toán</option>	
								<option value="1">Đang tham gia thanh toán</option>
								<option value="0">Chưa thanh toán</option>
							</select>
						</div>
						<div class="Clear"></div>
						<div class="SearchInSection SProduct1Form">
							<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
							<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
							<div class="BtnCenterSection">
								<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol" onclick="RepairEquipmentManageCatalog.searchRepair();">Tìm kiếm</button>
							</div>
							<div class="Clear"></div>
						</div>	
					</div>			
				<!-- </div> -->
				<h2 class="Title2Style">Danh sách phiếu sửa chữa thiết bị
					<div style="float:right; margin-top: -5px;margin-right: 27px;">
					<button style="float:right;" id="grpSave_btnSaveLst" class="BtnGeneralStyle cmsiscontrol" onclick="RepairEquipmentManageCatalog.updateStatus();" >Lưu</button>
					<div id="grpSave_cbxStatusDiv" style="float:right;margin-right: 5px;" class="BoxSelect BoxSelect2 cmsiscontrol">							
						<select class="MySelectBoxClass InputTextStyle" id="cbxStatusUpdate">
							<option value="-1" selected="selected">Chọn</option>
						    <option value="1">Chờ duyệt</option>
						   <!--  <option value="5">Đang sửa chữa</option> -->
						    <option value="6">Hoàn tất</option>
						    <option value="4">Hủy</option>
						</select>
					</div>
					<a id="btnExport" class="cmsiscontrol" onclick = "RepairEquipmentManageCatalog.exportBySearchListRepair();" style = "float: right;	margin: 0px 25px 0px 0px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_export.png" title="Xuất Excel"></a>
					<a id="grpImport_btnOpenDlg" class="cmsiscontrol" onclick = "RepairEquipmentManageCatalog.openDialogImportRepair();" style = "float: right;	margin-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_table_import.png" title="Nhập Excel"></a>
					<a id="grpImport_btnDownloadTemplate" class="downloadTemplateReport cmsiscontrol"  style = "float: right; margin-right:7px; width: 20px; height: 20px; cursor: pointer;" href="javascript:void(0)"><img width="20" height="20" src="/resources/images/icon_download_32.png" title="Tải mẫu Nhập Excel"></a>
					<a id="btnPrint" class="cmsiscontrol" href="javascript:void(0);" onclick="RepairEquipmentManageCatalog.printRepair();" style="margin-right:7px;"><img src="/resources/images/icon-printer.png" height="20" width="20" title="In"></a>
					</div>
				</h2>			
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<table id="grid"></table>
					</div>
				</div>		
				<div class="Clear"></div>
				<!-- </div> -->
			</div> <!-- end class="ToolBarSection" -->
		</div>
	</div>
	<div class="Clear"></div>
	<!-- Chi tiet chuc nang Form -->
	<div class="ContentSection" style="visibility:hidden; display: none;" id="equipHistoryTemplateDiv">
		<div class="BreadcrumbSection">
		    <ul class="ResetList FixFloat BreadcrumbList">
		        <li><span>Chi tiết hạng mục sửa chữa </span><span id="equipmentCodeFromSpan" style="color:#199700"></span></li>
		    </ul>
		</div>
		<div class="GeneralCntSection">
			<div class="GridSection" id="equipHistoryDgContainerGrid">
				 <table id="equipHistoryDg"></table>
			</div>
		</div>
	</div>
</div>
<!-- popup import -->
<div style="display: none">
  	<div id="easyuiPopupImportExcel" class="easyui-dialog" title="Nhập excel phiếu sửa chữa" data-options="closed:true,modal:true" style="width:465px;height:250px;">
		<div id="grpImport_dlg" class="PopupContentMid cmsiscontrol">
			<!-- <div class="SearchInSection SProduct1Form" style="border: none;"> -->
			<div class="GeneralForm ImportExcel1Form">
				<form action="/equipment-repair-manage/importExcel" name="importFrmListEquipListEquip" id="importFrmListEquip" method="post" enctype="multipart/form-data">						            	
			        <!-- <label class="LabelStyle Label1Style" style="padding-right: 10px; text-align: right; width: 80px;">Tập tin Excel</label> -->
			        <label class="LabelStyle Label1Style" style="margin-top:2px">File Excel</label>
				    <div class="UploadFileSection Sprite1" >
						<input id="fakefilepcListEquip" type="text" class="InputTextStyle InputText1Style">
						<input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFileListEquip" onchange="previewImportExcelFile(this,'importFrmListEquipListEquip','fakefilepcListEquip', 'errExcelMsgListEquip');" />						               		
					</div>
				</form>
				<div class="Clear"></div>
				<p class="DownloadSFileStyle">
					<a href="javascript:void(0)" class="Sprite1 downloadTemplateReport" >Tải file excel mẫu</a>
				</p>									
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" onclick="RepairEquipmentManageCatalog.importExcelRepair();">Nhập Excel</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopupImportExcel').dialog('close');">Đóng</button>
				</div>
			</div>
		</div>
		<p id="successExcelMsgListEquip" class="SuccessMsgStyle" style="display:none;"></p>
		<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsgListEquip" />
	</div>	
</div>
<s:hidden id="checkExpiredDate" name="expiredDate"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {	
	$('#txtSerial').focus();
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar("fDate");
	ReportUtils.setCurrentDateForCalendar("tDate");	
	Utils.bindComboboxEquipGroupEasyUICodeName('ddlEquipGroupId');
	//tamvnm-22/07/2015: thay doi combobox Nhom thiet bi thanh autoCompletedCombobox
	RepairEquipmentManageCatalog._lstEquipGroup = [];
	<s:iterator value="result.get('lstEquipGroup')" var="equipPro">
		var obj = {
			id: Utils.XSSEncode("<s:property value='id' />"),
			code: Utils.XSSEncode("<s:property value='code' />"),
			name: "<s:property value='escapeHTMLForXSS(name)' escapeHtml='false' />",
			codeName: "<s:property value='escapeHTMLForXSS(codeName)' escapeHtml='false' />"
		};
		RepairEquipmentManageCatalog._lstEquipGroup.push(obj);
	</s:iterator>
	
	/*$('#pnlListEquipment').width($(window).width()- 18);
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
	}, 1000);*/
	/** vuongmq; 20/04/2015; Xuat excel template*/
	var valueCheckNgayHetHanBaoHanh = $('#checkExpiredDate').val();
	if (valueCheckNgayHetHanBaoHanh == 0 || valueCheckNgayHetHanBaoHanh == '0') {
		// khong cho phep thay doi; xuat template khong co ngay het han bao hanh
		$('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/Bieu_mau_impot_phieu_sua_chua.xlsx');
	} else {
		// Cho phep thay doi; xuat template co cot ngay het han bao hanh
		$('.downloadTemplateReport').attr('href',excel_template_path+'/equipment/equip-repair-form/Bieu_mau_impot_phieu_sua_chua.xlsx');
	}
	
	//tamvnm: chuyen thanh autoCombobox.
	var groupId = -1;
	if ($('#ddlEquipGroupId').combobox("getValue") != "" 
		&& $('#ddlEquipGroupId').combobox("getValue") != undefined
		&& $('#ddlEquipGroupId').combobox("getValue") != null) {
		groupId = $('#ddlEquipGroupId').combobox("getValue");
	}
	RepairEquipmentManageCatalog._params = {
			equipCategoryId: $('#ddlEquipCategory').val(),
			equipGroupId: groupId,
			statusPayment: $('#cbxStatusPayment').val(),
			status: $('#cbxStatus').val(), 
			equipCode: $('#txtCode').val(),
			formCode: $('#txtRepairCode').val(),
			fromDate: $('#fDate').val(),
			toDate: $('#tDate').val(),
			seri: $('#txtSerial').val()
	};
	$('#ddlEquipGroupId').next().find(".combo-text.validatebox-text").bind('keyup',function(event){
	        		if(event.keyCode == keyCodes.ENTER){
	        			$('#btnSearch').click(); 
	        		}
	        	});
	RepairEquipmentManageCatalog._mapRepair = new Map();
	$('#grid').datagrid({
		url : "/equipment-repair-manage/searchRepair",
		pagination : true,
        rownumbers : true,
        pageNumber : 1,
        scrollbarSize: 0,
        autoWidth: true,
        pageSize: 10,
        pageList: [10, 20, 30, 50, 100],
        autoRowHeight : true,
        //fitColumns : true,
		queryParams: RepairEquipmentManageCatalog._params,
		width: $('#gridContainer').width() - 15,
		checkOnSelect: false, // vuongmq; 19/05/2015; chi cho check checkox
	    //height: 400,
	    frozenColumns:[[	        
			{field: 'check',checkbox:true, width: 40, sortable:false,resizable:false, align: 'left'},
			{field:'edit', title:'<a id="btnNew" class="cmsiscontrol" href="/equipment-repair-manage/change-record" title="Tạo mới"><img src="/resources/images/icon_add.png"/></a>', width: 70, align: 'center', sortable: false, resizable: true, formatter: function(value,row,index){
		    	return '<a id="btnView' + row.id + '" class="cmsiscontrol" onclick="RepairEquipmentManageCatalog.viewDetail('+row.id+',\''+row.maPhieu+'\')" href="javascript:void(0)"><img width="18" height="18" src="/resources/images/icon-view.png" title="Xem hạng mục sửa chữa '+row.maPhieu+'"></a><a id="btnEdit' + row.id + '" class="cmsiscontrol" onclick="" href="/equipment-repair-manage/change-record?id='+row.id+'"><img width="18" style="padding-left: 5px;" height="18" src="/resources/images/icon-edit.png" title="Chỉnh sửa '+row.maPhieu+'"></a>';
			}},
			{field:'daThanhToan', title: 'Thanh toán', width:80, sortable:false, resizable: true, align: 'center', formatter:function(value,row,index) {
				if (value == PaymentStatusRepair.NOT_PAID_YET) {
					return PaymentStatusRepair.parseValue(PaymentStatusRepair.NOT_PAID_YET);
				} else if (value == PaymentStatusRepair.PAID_TRANSACTION) {
					return PaymentStatusRepair.parseValue(PaymentStatusRepair.PAID_TRANSACTION);
				} else if (value == PaymentStatusRepair.PAID) {
					return PaymentStatusRepair.parseValue(PaymentStatusRepair.PAID);
				}
		    	//var cellContent = '';
		    	/** trang thai da hoan thanh*/
		    	/*if (row.trangThai == 6) { 
		    		if (!row.daThanhToan) { 
		    			/** !row.daThanhToan: la !0 ; chua thanh toan*/
		    			/*return '<a id="imgThanhToan'+row.id+'" class="cmsiscontrol" href="/equipment-repair-manage/payment?equipRepairId=' + row.id + '"><img width="18" height="18" src="/resources/images/icon-payment.png" title="Thanh toán"></a>';
		    		} else {
		    			return '<a id="imgXem" href="/equipment-repair-manage/payment?equipRepairId=' + row.id + '"><img width="18" height="18" src="/resources/images/icon_print_view_34.png" title="Xem chi tiết"></a>';
		    		}*/
		    		/** ky: paymentPeriodOpen */
		    		/*if (row.daThanhToan && !row.paymentPeriodOpen) { 
		    			cellContent += '<a id="imgXem"  href="/equipment-repair-manage/payment?equipRepairId=' + row.id + '"><img width="18" height="18" src="/resources/images/icon_print_view_34.png" title="Xem chi tiết"></a>';
		    		}*/
		    	//}
		    	//return cellContent;
		    }},	
			{field:'tongTien', title: 'Tổng tiền', width: 120, sortable:false, resizable: false, align: 'right', formatter:function(value,row,index){
				if (value != undefined && value != null) {
		    		return formatCurrency(value);
		    	}	
		    	return '';	
		    }},	
		    {field:'maPhieu', title: 'Mã phiếu', width: 120, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'ngayTao', title: 'Ngày biên bản', width: 80, sortable: false, resizable: false , align: 'center', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},	
		    {field:'maThietBi', title: 'Mã thiết bị', width: 180, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		 ]],
		columns:[[
		    {field:'soSeri', title: 'Số serial', width: 120, align: 'left', sortable: false, resizable: false, formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'loaiThietBi', title: 'Loại thiết bị', width: 160, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'nhomThietBi', title: 'Nhóm thiết bị', width: 180, sortable: false, resizable: false , align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field:'trangThai',title: 'Trạng thái', width: 100, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	var status = "";
		    	if(value == 0){
		    		status = "Dự thảo";
		    	}else if(value == 1){
		    		status = "Chờ duyệt";
		    	}else if(value == 2){
		    		status = "Đã Duyệt";
		    	}else if(value == 3){
		    		status = "Không duyệt";
		    	}else if(value == 4){
		    		status = "Hủy";
		    	}else if(value == 5){
		    		status = "Đang sửa chữa";
		    	}else if(value == 6){
		    		status = "Hoàn tất";
		    	}
		    	return Utils.XSSEncode(status);
		    }},
		    {field: 'url', title: 'Tập tin đính kèm', width: 70, sortable: false, resizable: false, align: 'center', formatter: function(value, row, index) {
		    	/*type= 2: la file sua chua */
		    	if (value != null && value != '') {
		    		return '<a href="javascript:void(0)" onclick="General.downloadEquipAttechFile(' + row.id + ', 2);"  title="Danh sách tập tin">Tập tin</a>';
		    	} else {
		    		return '';
		    	}
		    }},
		    {field:'kho', title: 'Kho', width: 200, align: 'left',sortable: false, resizable: false, formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},
			{field:'tinhTrangHuHong', title: 'Tình trạng hư hỏng', width: 150, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
				return Utils.XSSEncode(value);
		    }},	
			{field:'lyDoDeNghi', title: 'Lý do/ Đề nghị', width: 150, sortable: false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);
		    }},	
			{field:'lyDoTuChoi', title: 'Lý do duyệt/ Không duyệt', width: 170, sortable:false, resizable: false, align: 'left', formatter:function(value,row,index){
		    	return Utils.XSSEncode(value);	
		    }},
		    {field: 'note', title: 'Ghi chú', width: 170, sortable: false, resizable: false, align: 'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);	
		    }},
		   /*  {field: 'fileDownload',title:'Tập tin đính kèm',width:100,align:'left', sortable:false, resizable: false, formatter: function(value, row, index){
				return '<a href="javascript:void(0)" onclick="General.downloadEquipAttechFile('+row.id+', 2);">Tập tin</a>';
		    }}, */
	    ]],
	    onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');
	    	Utils.functionAccessFillControl('gridContainer'); /**phan quyen grid co thanh toan*/
	    	$('.datagrid-header-check input').removeAttr('checked');
        	var rows = $('#grid').datagrid('getRows');
	    	for(var i = 0; i < rows.length; i++) {
	    		if(RepairEquipmentManageCatalog._mapRepair.get(rows[i].id)!=null) {
	    			$('#grid').datagrid('checkRow', i);
	    		}
	    	}
   		 	$(window).resize();    		 
   		 	
	    },
	    onCheck:function(index,row){
			RepairEquipmentManageCatalog._mapRepair.put(row.id,row);   
	    },
	    onUncheck:function(index,row){
	    	RepairEquipmentManageCatalog._mapRepair.remove(row.id);
	    },
	    onCheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		RepairEquipmentManageCatalog._mapRepair.put(row.id,row);
	    	}
	    },
	    onUncheckAll:function(rows){
	    	for(var i = 0;i<rows.length;i++){
	    		var row = rows[i];
	    		RepairEquipmentManageCatalog._mapRepair.remove(row.id);		    		
	    	}
	    }
	});

	/** vuongmq; 20/04/2015; put params vao de dung export Excel theo tim kiem */
	var equipCategoryId = $('#ddlEquipCategory').val();
	var equipGroupId = $('#ddlEquipGroupId').combobox("getValue");
	var statusPayment = $('#cbxStatusPayment').val();
	var status = $('#cbxStatus').val();
	var equipCode = $('#txtCode').val();
	var formCode = $('#txtRepairCode').val();
	var fromDate = $('#fDate').val();
	var toDate = $('#tDate').val();
	var seri = $('#txtSerial').val();
	/** put params vao; xu dung cho export */
	RepairEquipmentManageCatalog._paramsSearch = new Object();
	RepairEquipmentManageCatalog._paramsSearch.equipCategoryId = equipCategoryId;
	RepairEquipmentManageCatalog._paramsSearch.equipGroupId = equipGroupId;
	RepairEquipmentManageCatalog._paramsSearch.statusPayment = statusPayment;
	RepairEquipmentManageCatalog._paramsSearch.status = status;
	RepairEquipmentManageCatalog._paramsSearch.equipCode = equipCode;
	RepairEquipmentManageCatalog._paramsSearch.formCode = formCode;
	RepairEquipmentManageCatalog._paramsSearch.fromDate = fromDate;
	RepairEquipmentManageCatalog._paramsSearch.toDate = toDate;
	RepairEquipmentManageCatalog._paramsSearch.seri = seri;

	RepairEquipmentManageCatalog.setEquipGroup();
	
	
});
</script>
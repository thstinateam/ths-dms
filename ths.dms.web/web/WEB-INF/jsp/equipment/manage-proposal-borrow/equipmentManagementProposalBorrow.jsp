<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
#player_api {
	width: 678px !important;
}
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">Thiết bị</a></li>
		<li><span>Quản lý đề nghị mượn thiết bị (</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div id="panelDeliveryRecord" class="easyui-panel" title="Thông tin tìm kiếm" style="width: 1000px; height: 200px; padding-top: 20px;"data-options="collapsible:true,closable:false,border:false">
					<div class="SearchInSection SProduct1Form">

						<label class="LabelStyle Label1Style">Đơn vị</label>
	              			<div class="BoxSelect BoxSelect2">
		              			<input type="text" id="shopTree" class="InputTextStyle InputText1Style" />
			 					<input type="hidden" id="shopCode"/>
		 					</div>	
		 				<div class="Clear"></div>

						<label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Mã biên bản</label> 
							<input type="text" class="InputTextStyle InputText1Style" id="code"  maxlength="50" /> 

						<label class="LabelStyle Label1Style" >Mã giám sát</label>
							 <input type="text"	class="InputTextStyle InputText1Style" id="name" maxlength="50"/> 

						<label	class="LabelStyle Label1Style">Trạng thái biên bản</label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status">
								<option value="-1" selected="selected">Tất cả</option>
								<option value="0">Dự thảo</option>
						   		<option value="2">Đã duyệt</option>	
						   		<option value="4">Hủy</option>						   		
							</select>
						</div>	
						<div class="Clear"></div>	

						 <label class="LabelStyle Label1Style" style=";text-align:left;;height:auto;">Ngày tạo từ</label>
							<div id="fromDate">
								<input type="text" id="fDate" class="InputTextStyle InputText6Style" />
							</div>
						<label class="LabelStyle Label1Style">Ngày tạo đến </label>
							<div id="toDate">
								<input type="text" id="tDate" class="InputTextStyle InputText6Style" />
							</div>
							
						<label	class="LabelStyle Label1Style">Xuất hợp đồng/ giao nhận</label>
						<div class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status">
								<option value="-1" selected="selected">Tất cả</option>
								<option value="0">Đã xuất</option>
						   		<option value="2">Chưa xuất</option>						   								   		
							</select>
						</div>				
						<div class="Clear"></div>						
					</div>
					<div class="SearchInSection SProduct1Form">
				<div class="Clear"></div>
					<div class="BtnCenterSection">
                        <button class="BtnGeneralStyle"  tabindex="2" id="btnSearch" onclick="return EquipmentStockChange.searchStockChange();">Tìm kiếm</button>
                    </div>					
					<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>										
					<p class="ErrorMsgStyle" id="errMsgSearch" style="display: none;"></p>
					<div class="Clear"></div>
				</div>
			</div>

				<h2 class="Title2Style">Danh sách đề nghị mượn tủ</h2>
					<div style="float:right; margin-top: -25px;margin-right: 27px;">						
						<a style="margin-right: 10px;" href="javascript:void(0);" onclick="return EquipmentStockChange.printRecodeStockTran();"><img src="/resources/images/icon-printer.png" title="In" height="20" width="20"></a>										
						<div style="float:right;margin-right: 5px;" class="BoxSelect BoxSelect11">						
							<select class="MySelectBoxClass" id="statusRecordLabel" style="width:112px;">
								<option value="-2" selected="selected">Chọn</option>
								<option value="2" >Duyệt</option>
								<option value="4" >Hủy</option>
							</select>
						</div>	
						 <button style="float:right;" class="BtnGeneralStyle" id="btnSearch" onclick="return EquipmentStockChange.saveStockChange();">Lưu</button>					
					</div>


				<div class="SearchInSection SProduct1Form">
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id=gridContainer>
							<table id="grid"></table>
						</div>
					</div>	
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>

<%-- <s:hidden id="successUpload" value="0"></s:hidden> --%>
<script type="text/javascript">
$(document).ready(function() {	
	TreeUtils.loadComboTreeShopHasTitle('shopTree', 'shopCode',function(data) {
		}, true);
	$('#panelDeliveryRecord').width($(window).width());
	setTimeout(function() {
		$('[class="panel-header panel-header-noborder"]').attr("style","width: auto; background: url('/resources/images/bg_title2.jpg') repeat-x scroll left top transparent; border: medium none; padding-left: 10px;");
	},1000);
	
	var params = new Object();	
	params.code = $('#code').val().trim();
	params.name = encodeChar($('#name').val().trim());
	params.status = $('#status').val();
	$('#grid').datagrid({
		url : "/equipment-group-manage/searchEquipmentGroup",
		autoRowHeight : true,
		rownumbers : true, 		
		singleSelect: true,
		pagination:true,
		rowNum : 20,
		fitColumns:true,
		pageList  : [20,40,60],
		scrollbarSize:0,
		width: $('#gridContainer').width(),
		autoWidth: true,
		queryParams:params,	
	    columns:[[	        
		    {field:'groupEquip', title: 'Nhóm thiết bị', width: 100, sortable: false, resizable: true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field:'typeGroup', title: 'Loại', width:80,sortable:false,resizable:true , align: 'left', formatter: function(value, row, index) {
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},	    
		    {field:'capacity',title: 'Dung tích', width: 50, align: 'center',sortable:false, resizable:true, formatter:function(value, row, index){
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field:'brandName',title: 'Hiệu', width: 80, align: 'left',sortable:false, resizable:true, formatter:function(value, row, index){
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field:'totalQuantity',title: 'Số lượng', width: 50, align: 'right',sortable:false, resizable:true, formatter:function(value, row, index){
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }},
		    {field:'quantity',title: 'Tồn kho tổng VNM', width: 50, align: 'right',sortable:false, resizable:true, formatter:function(value, row, index){
		    	if (value != undefined && value != null) {
		    		return Utils.XSSEncode(value);
		    	}
		    	return "";
		    }}, 
		    {field:'status', title: 'Trạng thái', width: 50, align: 'left',sortable:false,resizable:true,formatter:function(value, row, index){
		    	if (value != undefined && value != null) {
		    		return statusTypeText.parseValue(value);
		    	}
		    	return "";
		    }},
		    {field:'edit',  title:'<a href="/equipment-group-manage/changeEquipmentGroupJSP?id=0"><img src="/resources/images/icon_add.png"/></a>',
		    	width: 20, align: 'center',sortable:false,resizable:true, formatter: function (value, row, index) {
		    		if (row.id != undefined && row.id != null && !isNaN(row.id)) {			
		    			return '<a href="/equipment-group-manage/changeEquipmentGroupJSP?id=' + row.id +'"><img width="16" style="" heigh="16" src="/resources/images/icon-edit.png" title="Chỉnh sửa"></a>';		
		    		} 
		    		return '<a><img width="16" style="" heigh="16" src="/resources/images/icon-edit_disable.png" title="Chỉnh sửa"></a>';
		    }},
		    {field: 'id', index: 'id', hidden: true},
	    ]],	    
	    onLoadSuccess :function(data){	    	
			$('.datagrid-header-rownumber').html('STT');			 
			 $(window).resize();
			 if (data == null || data.total == 0) {
				// $("#errMsgSearch").html("Không tìm thấy dữ liệu").show();
			 }
		}
	});	
});
</script>
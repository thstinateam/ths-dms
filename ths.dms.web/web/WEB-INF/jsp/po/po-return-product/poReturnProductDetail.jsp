<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<h2 class="Title2Style">Trả hàng - <s:property value="poConfirmCodeDetail" /> </h2>
<div class="SearchInSection SProduct1Form">	
	<div class="GridSection" id="lstPOVnmDetailTableScroll">
		 <div class="GeneralTable General1Table">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col style="width: 30px;" />
					<col style="width: 90px;" />
					<col style="width: 220px;" />
					<col style="width: 65px;" />
					<col style="width: 70px;" />
					<col style="width: 65px;" />
					<col style="width: 100px;" />
					<col style="width: 70px;" />
					<col style="width: 65px;" />
					<col style="width: 95px;">
					<col style="width: 99px;" />
				</colgroup>
				<thead>
					<tr>
						<th class="ColsThFirst">STT</th>
						<th>Mã sản phẩm</th>
						<th>Tên sản phẩm</th>
						<th>Đơn giá</th>
						<th>Số lượng</th>
						<th>Số lô</th>
						<th>Kho</th>
						<th>Tồn kho</th>
						<th>Đã trả</th>
						<th>Số lượng trả</th>
						<th class="ColsThEnd">Thành tiền</th>
					</tr>
				</thead>
				<tbody id="vnmPoProductDetail">
					<s:if
						test="lstPOVnmDetailLLotVo != null && lstPOVnmDetailLLotVo.size() > 0">
						<s:iterator value="lstPOVnmDetailLLotVo" status="status">
							<tr>
								<s:hidden cssClass="poVnmDetailLotVoID"	value="%{poVnmDetailLotId}"></s:hidden>
								<s:hidden cssClass="Convfact" value="%{convfact}"></s:hidden>
								<s:hidden cssClass="Price" value="%{price}"></s:hidden>
								<s:hidden cssClass="SLChoPhep" value="%{quantity-quantityPay}"></s:hidden>
								<s:hidden cssClass="WarehouseId" value="%{wareHouseId}"></s:hidden>
								<s:hidden cssClass="ProductId" value="%{productId}"></s:hidden>
								<td class="ColsTd1 AlignColsCenter"><s:property value="#status.index+1" /></td>
								<td class="AlignLeft Products"><s:property value="productCode" /></td>
								<td class="AlignLeft"><s:property value="productName" /></td>
								<td class="AlignRight"><span><s:property value="convertMoney(price)" /></span></td>
								<td class="AlignRight"><s:property value="cellQuantityFormatter(quantity,convfact)" /></td>
								<td class="Lot"><s:property value="lot" /></td>
								<td class="Warehouse"><s:property value="wareHouseName" /></td>
								<td class="AlignRight Stock" convfact="<s:property value="convfact" />" ><s:property value="cellQuantityFormatter(quantityStock,convfact)" /></td>
								<td class="AlignRight Pay"><s:property value="cellQuantityFormatter(quantityPay,convfact)" /></td>
								<s:if test="checkenable == true">
									<td class="AlignRCols"> 
									<input type="text" convfact="<s:property value="convfact" />" id="quantityPay<s:property value='#status.index+1'/>" maxlength="20" size="20"
										class="InputTextStyle InputText1Style Count InputStyleReturnProduct InputStyleReturnProduct BindFormatOnInputCss" tabindex=""
										onchange="return POReturnProduct.changeInput(this,'money<s:property value="#status.index+1"/>', <s:property value="price"/>, <s:property value="convfact"/>)" 
										value='<s:property value="cellQuantityFormatter(returnValue,convfact)" />'
										/>
									</td>
								</s:if>
								<s:else>
									<td class="ColsTd10"><input type="text" maxlength="20" size="20" disabled="disabled" class="InputTextStyle InputText1Style InputStyleReturnProduct" tabindex=""/>
									</td>
								</s:else>
								<td class="ColsTd11 ColsTdEnd AlignRight Money" id="money<s:property value="#status.index+1"/>"><span><s:property value="convertMoney(amount)" /></span></td>								
							</tr>							
						</s:iterator>
					</s:if>
					<s:else>
						<tr>
							<td colspan="11" class="NotData">Không có dữ liệu nào</td>
						</tr>
					</s:else>
					<tr>
						<td colspan="11"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="Clear"></div>
	<p class="TotalInfo TotalLInfo">
		<span class="Tem1Style">Tổng số lượng: <span id="totalQuantity"> <s:property value="totalQuantity" /> </span>
		</span> <span class="Tem1Style">Tổng tiền: <span id="totalAmount"><s:property value="totalAmount" /></span>
		</span>
	</p>
	<s:if test="checkenable != false">
	<div class="BtnCenterSection">
		<s:if test="shopLocked">
			<button disabled="disabled" class="BtnGeneralStyle BtnMSection BtnGeneralDStyle" id="btnReturnProduct">Trả hàng</button>
		</s:if>
		<s:else>
			<button class="BtnGeneralStyle BtnMSection" id="btnReturnProduct" onclick="POReturnProduct.returnProduct();">Trả hàng</button>
		</s:else>
		<button class="BtnGeneralStyle" id="btnExportExcel" onclick="POReturnProduct.exportExcel();">In đơn hàng trả</button>
	</div>
	</s:if>
	<div class="Clear"></div>
	<p class="ErrorMsgStyle" id="errMsgReturn" style="display: none;"></p>
	
	<p id="successMsgEx" class="SuccessMsgStyle" style="display: none"></p>
</div>
<input type="hidden" id="poConfirmCodeReturn" value="<s:property value="poConfirmCodeDetail" />"/>
<input type="hidden" id="errorPOReturn" value="<s:property value="errMsg" />"/>
<input type="hidden" id="poVnmExportCode" />
<s:hidden id="checkenableHidden" name="checkenable"/>
<script type="text/javascript">
$(document).ready(function(){
	var checkenable = $('#checkenableHidden').val();
	if (checkenable == false || checkenable == 'false') {
		$('#errMsgReturn').html('Đơn hàng đã hết hạn trả hàng').show();
// 		setTimeout(function(){$('#errMsgReturn').hide();},3000);
	}
	$("#btnExportExcel").attr("disabled",true);
	$("#btnExportExcel").addClass("BtnGeneralDStyle");
	setDateTimePicker('invoiceDateReturn');
	Utils.bindFormatOnTextfieldInputCss('BindFormatOnInputCss', Utils._TF_NUMBER_CONVFACT);
	Utils.bindFormatOnTextfield('countKSReturn',Utils._TF_NUMBER_COMMA);
	Utils.bindFormatOnTextfield('valueKSReturn',Utils._TF_NUMBER_COMMA);
	Utils.bindFormatOnTextfield('disCount',Utils._TF_NUMBER_COMMA);
	Utils.formatCurrencyFor('countKSReturn');
	Utils.formatCurrencyFor('valueKSReturn');
	Utils.formatCurrencyFor('disCount');
	$('#totalQuantity').html($('#totalQuantity').html());
	$('#totalAmount').html(formatCurrency($('#totalAmount').html()));
});
</script>

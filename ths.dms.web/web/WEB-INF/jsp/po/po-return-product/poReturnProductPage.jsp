<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Mua hàng</a></li>
		<li><span>Trả hàng Nutifood</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Số hóa đơn</label> 
					<input id="invoiceCode" type="text" class="InputTextStyle InputText1Style" maxlength="20" />
					 
					<label class="LabelStyle Label1Style">Ngày nhập hàng từ</label> 
					<input id="fromDate" type="text" class="InputTextStyle InputText6Style  vinput-date" maxlength="10" value="<s:property value="fromDate"/>" /> 
					
					<label class="LabelStyle Label1Style">Đến</label> 
					<input id="toDate" type="text" class="InputTextStyle InputText6Style  vinput-date" maxlength="10" value="<s:property value="toDate"/>" />
					
<!-- 					<label class="LabelStyle Label1Style">Ngày nhập hàng</label>  -->
<%-- 					<input id="fromDate" type="text" class="InputTextStyle InputText6Style" maxlength="10" value="<s:property value="fromDate"/>" /> --%>
					 
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Số PO Confirm</label> 
					<input id="poConfirmCode" type="text" class="InputTextStyle InputText1Style" maxlength="20" />  
					
					<label	class="LabelStyle Label1Style">Trạng thái</label>
					<p class="ValueStyle Value1Style">Đã nhập hàng</p>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnSearch" onclick="POReturnProduct.search()">Tìm kiếm</button>
					</div>
					<div class="Clear"></div>
					<img id="loadingSearch" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
					<p class="ErrorMsgStyle" id="errorSearchMsg" style="display: none;"></p>
				</div>
				<h2 class="Title2Style">Danh sách đơn hàng tìm được</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="productorderSearchTable">						
						<tiles:insertTemplate template="/WEB-INF/jsp/po/po-return-product/poReturnProductSearch.jsp"></tiles:insertTemplate>
					</div>					
					<div class="Clear"></div>
				</div>
				<div class="Clear"></div>
				<div id="productorderDetailTable"></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="k" uri="/kryptone"%>
 <div class="GeneralTable General1Table">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col style="width: 40px;" />
					<col style="width: 105px;" />					
					<col style="width: 149px;" />
					<col style="width: 90px;" />
					<col style="width: 90px;" />
					<col style="width: 160px;" />
					<col style="width: 110px;" />
					<col style="width: 45px;" />
				</colgroup>
				<thead>
					<tr>
						<th class="ColsThFirst">STT</th>
						<th>Số hoá đơn</th>						
						<th>Số đơn hàng</th>
						<th>Ngày tạo</th>
						<th>Ngày nhập</th>
						<th>Tổng tiền</th>
						<th>Chiết khấu</th>
						<th class="ColsThEnd">Trả hàng</th>
					</tr>
				</thead>
				<tbody>
						<k:repeater value="lstPOConfirm" status="status">
							<k:itemTemplate>
								<tr>
									<td class="ColsTd1 AlignColsCenter" style="height:20px"><s:property value="#status.index+1" /></td>
									<td class="ColsTd2 AlignColsLeft"><s:property value="invoiceNumber" /></td>									
									<td class="ColsTd4 AlignColsLeft"><s:property value="poCoNumber" />	</td>
									<td class="ColsTd5 AlignColsCenter"><s:date name="poVnmDate" format="dd/MM/yyyy" /></td>
									<td class="ColsTd6 AlignColsCenter"><s:date name="importDate" format="dd/MM/yyyy" /></td>
									<td class="ColsTd7 AlignRight AlignColsRight"><s:property value="convertMoney(amount)" /></td>
									<td class="ColsTd8 AlignRight AlignColsRight"><s:property value="convertMoney(discount)" /></td>
									<td class="ColsTd9 ColsTdEnd AlignColsCenter" style="text-align: center;">
									<a id="traHang_<s:property value='#status.index+1'/>" href="javascript:void(0);" onclick="return POReturnProduct.detail('<s:property value='poCoNumber'/>', true);">
										<img src="/resources/images/icon_nhaphang_active.png" width="25" height="13" /> 
									</a>
								</td>
							</tr>
					</k:itemTemplate>
				</k:repeater>
			</tbody>
	</table>
</div>
<script type="text/javascript">
$(function(){
	Utils.bindTRbackground();
});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style"><s:property value="titleDetail"/> - <s:property value="poCoNumber"/></h2>
				<div class="SearchInSection SProduct1Form" style="padding-left:0;">
					<input type="hidden" id="check" name="check" value="<s:property value="check"/>">
					<input type="hidden" id="error" name="error" value="<s:property value="error"/>"/>
					
					<label class="LabelStyle Label1Style">Số hóa đơn <span class="ReqiureStyle">*</span></label>
					<s:if test="status==1 || status==2">
							<input id="invoiceNumber" maxlength="20" class="InputTextStyle InputText1Style" disabled="disabled" type="text" value="<s:property value="invoiceNumber"/>"/>
					</s:if>
					<s:else>
						<input id="invoiceNumber" maxlength="20" type="text" class="InputTextStyle InputText1Style" />						
					</s:else>                    
                    
                    <label class="LabelStyle Label1Style">Ngày tạo HĐ</label>
                    <input type="text" class="InputTextStyle InputText6Style vinput-date" id="creatDate" maxlength="10" value="<s:property value="poVnmDate"/>"/>
                   <%--  <input	type="text" class="InputTextStyle InputText6Style vinput-date" maxlength="10" id="toDate" value="<s:property value="toDate"/>"/> --%>
                    
                    <s:if test="typePo == 2">
						<label class="LabelStyle Label1Style">Thời gian xe về</label>
					</s:if>
					<s:else>
						<label class="LabelStyle Label1Style">Thời gian xe đi</label>
					</s:else>
                    <input type="text" class="InputTextStyle InputText6Style vinput-date" id="deliveryDate" value="<s:property value="deliveryDate"/>"/>
                    <div class="Clear"></div>
					
                   <!--  <label class="LabelStyle Label1Style">Số lượng KS <span class="ReqiureStyle">*</span></label>
                   	<s:if test="status==2">
							<input id="quantity_check" maxlength="20" class="InputTextStyle InputText1Style" type="text" disabled="disabled" value="<s:property value="convertMoney(quantity_check)"/>"/>
					</s:if>
					<s:else>
						<input id="quantity_check" maxlength="20" class="InputTextStyle InputText1Style" type="text" value="" onChange="myArletFunction01()" />		
					</s:else>
                    
                    <label class="LabelStyle Label1Style">Giá trị KS <span class="ReqiureStyle">*</span></label>                   
                    <s:if test="status==2">
						<input id="amount_check" maxlength="20"  class="InputTextStyle InputText1Style" type="text" value="<s:property value="convertMoney(amount_check)"/>" disabled="disabled" />
					</s:if>
					<s:else>
						<input id="amount_check" maxlength="20" class="InputTextStyle InputText1Style" type="text" value="" onChange="myArletFunction02()"/>
					</s:else> -->
					<div class="Clear"></div>
					<div style="width: 50%; float:left;">
	                   	<p class="TotalInfo TotalLInfo">
	                   		<span class="Tem1Style">Tổng số lượng: <span ><s:property value="convertMoney(totalcount)"/></span></span>
	                 		<span class="Tem1Style">Tổng tiền: <span id="totalDetail"><s:property value="convertMoney(totalmoney)"/></span></span>
	                		<span class="Tem1Style">Chiết khấu: <span id="discountDetail"><s:property value="convertMoney(discount)"/></span></span>
	               			<span class="Tem1Style">Thuế VAT: <span id="vatDetail"><s:property value="convertMoney(vat)"/></span></span>
	                  		<span class="Tem1Style">Thanh toán: <span id="payDetail"><s:property value="convertMoney(pay)"/></span></span>
	         			</p>
	         		</div>
         			<div style="width: 50%; float:left; margin-top: 10px">
         				<label class="LabelStyle">Chiết khấu (chưa VAT)</label>                   
	                    <s:if test="status==2">
								<input id="discountNotVat" maxlength="17"  onChange="POStockIn.discountChange();" 
								class="InputTextStyle InputText1Style" type="text" value="<s:property value="convertMoney(discount)"/>" disabled="disabled" />
						</s:if>
						<s:else>
							<input id="discountNotVat" maxlength="17" onChange="POStockIn.discountChange();"  class="InputTextStyle InputText1Style" type="text" value="" />
						</s:else>
						<s:if test="typePo == 2">
							<button id="btn" style="float: right; margin-right: 10px" onClick="return POStockIn.addQuantityFull();" class="BtnGeneralStyle">Nhập đủ</button>
						</s:if>
						<s:else>
							<button id="btn" style="float: right; margin-right: 10px" onClick="return POStockIn.addQuantityFull();" class="BtnGeneralStyle">Trả đủ</button>
						</s:else>
					</div>
                    <div class="Clear"></div>
                    <div class="SearchInSection SProduct1Form">	
                    	<div class="GridSection">	
                    		<table id="dg_detail" class="easyui-datagrid" ></table>
                    	</div>			
						<div class="Clear"></div>
						<div class="BtnCenterSection" style="margin-left:11px;margin-right:-11px;">
							<s:if test="status==2 || status==3"></s:if>
							<s:elseif test="poCoNumber != null">
								<s:if test="shopLocked">
									<button id="btnRemove" style="float: left;" disabled="disabled" class="BtnGeneralDStyle BtnGeneralStyle">Hủy đơn hàng</button>
									<button id="btstockToal" style="float: right;" disabled="disabled" class="BtnGeneralDStyle BtnGeneralStyle">Hoàn tất nhập hàng</button>
									<button id="btstock" style="float: right; margin-right: 10px" disabled="disabled" class="BtnGeneralDStyle BtnGeneralStyle">Nhập hàng</button>
								</s:if>
								<s:else>
									<s:if test="status != 1">
										<button id="btnRemove" style="float: left;" onClick="return POStockIn.removePOConfirm('<s:property value="poCoNumber"/>',<s:property value="poVnmId"/>);" class="BtnGeneralStyle">Hủy đơn hàng</button>
									</s:if>
									<!-- Thay doi tile ung voi tung loai don (typePo), nhap: 2, tra: 3  -->
									<s:if test="typePo == 2">
										<button id="btnStockInImported" style="float: right;" onClick="return POStockIn.stockin('<s:property value="poCoNumber"/>',<s:property value="poVnmId"/>, POStockIn._IMPORTED);" class="BtnGeneralStyle">Hoàn tất nhập hàng</button>
										<button id="btnStockInImporting" style="float: right; margin-right: 10px" onClick="return POStockIn.stockin('<s:property value="poCoNumber"/>',<s:property value="poVnmId"/>, POStockIn._IMPORTING);" class="BtnGeneralStyle">Nhập hàng</button>
									</s:if>
									<s:else>
										<button id="btnStockOutImported" style="float: right;" onClick="return POStockIn.stockin('<s:property value="poCoNumber"/>',<s:property value="poVnmId"/>, POStockIn._IMPORTED);" class="BtnGeneralStyle">Hoàn tất trả hàng</button>
										<button id="btnStockOutImporting" style="float: right; margin-right: 10px" onClick="return POStockIn.stockin('<s:property value="poCoNumber"/>',<s:property value="poVnmId"/>, POStockIn._IMPORTING);" class="BtnGeneralStyle">Trả hàng</button>
									</s:else>
								</s:else>
							</s:elseif>
							<div class="Clear"></div>
							<p class="ErrorMsgStyle" id="errMsg" style="display: none;"></p>	
							<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>						
						</div>
					</div>  
			</div>			
		</div>
	</div>
	<div class="Clear"></div>
</div>
</div>
<s:hidden id="typePoVNM" name="typePo"></s:hidden>
<s:hidden id="statusPoVNM" name="status"></s:hidden>
<script type="text/javascript">
var totalcount = '<s:property value="totalcount"/>';
var totalmoney = '<s:property value="totalmoney"/>';
$(document).ready(function() {
	$('.currencyPOVNMstockin').each(function () {
		if($(this).html().length > 0){
			$(this).html(formatCurrency($(this).html()));
		}		
	});
	VTUtilJS.applyDateTimePicker("creatDate");
	//Utils.formatCurrencyFor('quantity_check');
	//Utils.formatCurrencyFor('amount_check');
	Utils.formatCurrencyFor('discountNotVat');
	//Utils.bindFormatOnTextfield('quantity_check',Utils._TF_NUMBER_COMMA);
	//Utils.bindFormatOnTextfield('amount_check',Utils._TF_NUMBER_COMMA);
	Utils.bindFormatOnTextfield('discountNotVat',Utils._TF_NUMBER_COMMA);
	var titleColSL = '';
	var titleColSLInput = '';
	var typePo = $('#typePoVNM').val();
	if (typePo == PoType.PO_CUSTOMER_SERVICE || typePo == PoType.PO_CONFIRM) {
		titleColSL = 'SL đã nhập';
		titleColSLInput = 'SL nhập';
	} else if (typePo == PoType.RETURNED_SALES_ORDER || typePo == PoType.PO_CUSTOMER_SERVICE_RETURN) {
		titleColSL = 'SL đã trả';
		titleColSLInput = 'SL trả';
	}
	// neu da nhap, hay da tra; disable Ngay tao HD
	var statusPo = $('#statusPoVNM').val();
	if (statusPo == PoStatus.IMPORTED) {
		disableDateTimePicker('creatDate');
	}
	// vuongmq; 29/01/2016; delivery date
	$('#deliveryDate').datetimepicker({
		lang:'vi',
		format:'d/m/Y H:i'
	});
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	var hour = currentTime.getHours();
	var minute = currentTime.getMinutes();
	if (month < 10) {
		month = '0' + month;
	}
	if (day < 10) {
		day = '0' + day;
	}
	if (hour < 10) {
		hour = '0' + hour;
	}
	if (minute < 10) {
		minute = '0' + minute;
	}
	//$('#fDate').val(day + '/' + month + '/' + year + ' 00:00');
	$('#dg_detail').datagrid({  
	    url:'/po/stock-in/getinfor-detail',		    	    
	    singleSelect:true,	     
	    width: $(window).width() -35,
	    height: 250,
	    rownumbers : true,
	    fitColumns:true,		    
	    queryParams:{
	    	poVnmId:'<s:property value="poVnmId"/>'
	    },
	    showFooter: true,
	    columns:[[ 
			{field: 'lineSaleOrder', title: 'Số dòng sale order', width: 65, align: 'center', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
			{field: 'linePo', title: 'Số dòng đơn đặt', width: 55, align: 'center', formatter: function(value, row, index) {
				return Utils.XSSEncode(value);
			}},
			{field:'productCode', title:'Mã sản phẩm', width:110, align:'left', formatter:function(value,row,index){
				return Utils.XSSEncode(value);
			}}, 
			{field:'productName', title:'Tên sản phẩm', width:200, align:'left', formatter:function(value,row,index){
				return Utils.XSSEncode(value);
			}}, 
			/* {field:'productType', title:'Loại sản phẩm', width:90, align:'left', formatter:function(value,row,index){
				if (value != undefined && value != null) {
					return Utils.XSSEncode(ProductTypeVNM.parseValue(value));
				}
				return '';
			}},  */
			/* {field:'price', title:'Đơn giá', width:100, align:'right',formatter:CommonFormatter.numberFormatter}, */
			{field: 'price', title: 'Đơn giá', width:110, align: 'right', formatter: function(value, row, index) {
				return formatCurrencyInterger(row.packagePrice) + '/' + formatCurrencyInterger(row.price);
			}},
			{field:'wareHouseId', title:'Kho', width:140, align:'left',formatter:function(value,row,index){
				//return VTUtilJS.XSSEncode(value);
				var dataRows = row.lstWarehouse;
				var str = '<div class="BoxSelect BoxSelect4" style="margin-bottom: 2px">';
				if (row.wareHouseId != null) {
					str += '<select   id="wareHouse' + row.poVnmDetailLotId + '"  class="customstyle MySelectBoxClass MySelectBoxClass1 wareHouseClazz" >';
				} else {
	    			str += '<select   id="wareHouse' + row.poVnmDetailLotId + '"  class="customstyle MySelectBoxClass MySelectBoxClass1 wareHouseClazz" onchange="POStockIn.changeWarehouse(' + row.poVnmDetailLotId + ','+index+');" >';
	    		}
	    		for (var k = 0 ; k < dataRows.length ; k++) {
	    			if (row.wareHouseIdTmp == dataRows[k].id) {
	    				str += '<option selected="selected" value="' + dataRows[k].id + '">' + Utils.XSSEncode(dataRows[k].warehouseName) + '</option>';
	    			} else {
						str += '<option value="' + dataRows[k].id + '">' + Utils.XSSEncode(dataRows[k].warehouseName) + '</option>';
	    			}
	    		}
	    		str += '</select>';
	    		str += '</div>';
	    		return str;
			}},
			{field:'quantityStock', title:'Tồn kho', width:90, align:'right',formatter:function(value,row,index){
				return formatQuantity(row.quantityStock,row.convfact);
			}},
			{field:'quantity', title:'SL tổng', width:90, align:'right',formatter:function(value,row,index){
				return formatQuantity(row.quantity,row.convfact);
			}},
			{field:'quantityReceived', title: titleColSL, width:90, align:'right',formatter:function(value,row,index){
				return formatQuantity(row.quantityReceived,row.convfact);
			}},
			{field:'quantityInput', title: titleColSLInput, width:90, align:'right',formatter:function(value,row,index){
				return '<input id="quantityInput'+index+'" maxlength="20" class="InputTextStyle InputText10Style vinput-money quantityInputClazz" type="text" qIndex="'+index+'" style="margin-bottom: 0px; text-align: right;" />';
			}},
			/* {field:'lot', title:'Số lô', width:120, align:'left'}, */
			
			{field:'amount', title:'Tổng tiền', width:110, align:'right',formatter:CommonFormatter.numberFormatter}			
		]],    
		onLoadSuccess :function(data){			
			$('.datagrid-header-rownumber').html('STT');
		   	$('.datagrid-header-row td div').css('text-align','center');		   	
		   	 updateRownumWidthForJqGrid('.easyui-dialog');
	   	 	//$(window).resize(); 
	   	 	$('#dg_detail').datagrid('resize');
	      	//$('.datagrid-htable .datagrid-cell').addClass('GridHeaderLargeHeight');
	      	$('.MySelectBoxClass1').customStyle();
	      	//$('.datagrid-view .CustomStyleSelectBox').css('width','140px');
	      	var dataLst = data.rows;
	      	POStockIn.blurQuantityInput(dataLst);
	      	/* for (var k = 0 ; k < dataLst.length ; k++) {
				if (dataLst[k].wareHouseId != null) {
					disableSelectbox('wareHouse' + dataLst[k].poVnmDetailLotId);
				}
				Utils.bindFormatOnTextfield('quantityInput' + k, Utils._TF_NUMBER);
				Utils.formatCurrencyFor('quantityInput' + k);
			} */
		}
	});
});
function myArletFunction01(){
	$('#errMsg').html('').hide();
	var quantity_check = Utils.returnMoneyValue($('#quantity_check').val().trim());
	var a = 0;
	if (totalcount == '') a = 0;
	else a = parseInt(totalcount,10);
	if (parseInt(quantity_check) != a) {
		$('#errMsg').html('Số lượng kiểm soát nhập vào khác với tổng số lượng của đơn hàng, bạn hãy nhập đúng').show();
		$('#quantity_check').val('');
		$('#quantity_check').focus();
	}
}
function myArletFunction02(){
	$('#errMsg').html('').hide();
	var payDetail=0;
	if($('#payDetail').html().trim()!=''){
		payDetail=Utils.returnMoneyValue($('#payDetail').html().trim());
	}
	var amount_check = Utils.returnMoneyValue($('#amount_check').val().trim());
	if (parseInt(amount_check) != parseInt(payDetail)){
		$('#errMsg').html('Giá trị kiểm soát nhập vào khác với số tiền thanh toán của đơn hàng, bạn hãy nhập đúng').show();
		$('#amount_check').val('');
		$('#amount_check').focus();
	}
}
</script>
           
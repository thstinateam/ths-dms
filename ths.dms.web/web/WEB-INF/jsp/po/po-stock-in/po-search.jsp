<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="GeneralTable Table6Section">
	<div class="ScrollSection">
		<div class="BoxGeneralTTitle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="fixed" style="border: 1px solid #CCC">
				<colgroup>
					<col style="width: 40px;" />
					<col style="width: 144px;" />
					<col style="width: 90px;" />
					<col style="width: 100px;" />
					<col style="width: 180px;" />
					<col style="width: 140px;" />
					<col style="width: 110px;" />
					<col style="width: 50px;" />
				</colgroup>
				<thead>
					<tr>
						<th class="ColsThFirst">STT</th>
						<th>Số PO confirm</th>
						<th>Trạng thái</th>
						<th>Ngày tạo</th>
						<th>Tổng tiền</th>
						<th>Chiết khấu</th>
						<th>Tổng SL</th>
						<th class="ColsThEnd">Nhập hàng</th>
					</tr>
				</thead>
			</table>
		</div>
		<div class="BoxGeneralTBody">
			<div class="ScrollBodySection" id="posearch">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="fixed">
					<colgroup>
						<col style="width: 40px;" />
						<col style="width: 144px;" />
						<col style="width: 90px;" />
						<col style="width: 100px;" />
						<col style="width: 180px;" />
						<col style="width: 140px;" />
						<col style="width: 110px;" />
						<col style="width: 50px;" />
					</colgroup>
					<tbody>
						<s:if test="lstPovnm != null && lstPovnm.size() > 0">
							<s:iterator id="abc" value="lstPovnm" status="status">
								<tr>
									<td class="ColsTd1"><s:property value="#status.index+1" /></td>
									<td class="ColsTd2 AlignLeft"><s:property value="#abc.poCoNumber"/></td>
									<td class="ColsTd3">
										<span>
											<s:if test="#abc.status.value == 0">Chưa nhập</s:if>
											<s:elseif test="#abc.status.value == 2">Đã nhập</s:elseif>										
										</span>
									</td>
									<td class="ColsTd4"><s:date name="#abc.poVnmDate" format="dd/MM/yyyy"/></td>
									<td class="ColsTd5 AlignRight currencyPOVNMSearch"><s:property value="#abc.amount"/></td>
									<td class="ColsTd6 AlignRight currencyPOVNMSearch"><s:property value="#abc.discount"/></td>
									<td class="ColsTd7 AlignRight currencyPOVNMSearch"><s:property value="#abc.quantity"/></td>
									<td class="ColsTd8 colsTdEnd" style="text-align: center;"><span>
										<s:set id="num" value="0"></s:set>
										<s:if test="#abc.discount != null">
											<s:set id="num" value="#abc.discount"></s:set>
										</s:if>
										<a href="javascript:void(0);" onClick="return POStockIn.detail(<s:property value="#abc.id"/>, <s:property value="#abc.status.value"/>, '<s:property value="displayDate(#abc.poVnmDate)"/>' ,<s:property value="#num"/>,'<s:property value="#abc.poCoNumber"/>','<s:property value="#abc.invoiceNumber"/>','<s:property value="#abc.quantityCheck"/>','<s:property value="#abc.amountCheck"/>');">
										<img src="/resources/images/icon_nhaphang_active.png" width="18" height="19" /></a>
									</span></td>
								</tr>
							</s:iterator>
						</s:if>
						<s:else>
							<p class="NotData">Không có dữ liệu nào</p>
						</s:else>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){	
	$('.currencyPOVNMSearch').each(function () {
		if($(this).html().length > 0){
			$(this).html(formatCurrency($(this).html()));
		}		
	});
	Utils.resizeHeightAutoScrollCross('posearch');
});
</script>

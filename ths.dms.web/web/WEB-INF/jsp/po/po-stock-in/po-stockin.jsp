<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Mua hàng</a></li>
		<li><span>Nhập/trả hàng</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label6Style">Mã NPP<span class="ReqiureStyle"> *</span></label>
                	<div class="BoxSelect BoxSelect2">
                		<input  type="text" class="InputTextStyle InputText1Style" id="cbxShop" maxlength="50" />
                	</div>
                	<!-- <label class="LabelStyle Label6Style">Ngày làm việc:</label>
                	<p id="strLockDate" class="LabelStyle"><s:property value="lockDate"/></p> -->
                	<label class="LabelStyle Label6Style">Loại đơn</label>
					<div class="BoxSelect BoxSelect2">
						<select id="typePo" class="MySelectBoxClass">
							<option value="-1" selected="selected">Tất cả</option>
							<option value="2">Ðơn nhập</option>
							<option value="3">Đơn trả</option>
						</select>
					</div>
					<label class="LabelStyle Label6Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<select id="status" class="MySelectBoxClass">
							<option value="-1">Tất cả</option>
							<option value="0" selected="selected">Chưa nhập/ trả</option>
							<option value="1">Ðã nhập/ trả một phần</option>
							<option value="2">Ðã nhập/ trả</option>
							<option value="3">Hủy</option>
						</select>
					</div>	
					<div class="Clear"></div>
					
					<label class="LabelStyle Label6Style" >Từ ngày<span class="RequireStyle"> *</span></label> 
					<input	type="text" class="InputTextStyle InputText6Style vinput-date" maxlength="10" id="fromDate" value="<s:property value="fromDate"/>"/> 
					<label class="LabelStyle Label6Style">Đến ngày<span class="RequireStyle"> *</span></label> 
					<input	type="text" class="InputTextStyle InputText6Style vinput-date" maxlength="10" id="toDate" value="<s:property value="toDate"/>"/> 
					<label class="LabelStyle Label6Style" >Số hóa đơn</label> 
					<input	type="text" class="InputTextStyle InputText1Style" maxlength="20" id="invoiceNumberSearch"/>
					<div class="Clear"></div>
					<label class="LabelStyle Label6Style" >Số đơn hàng</label> 
					<input	type="text" class="InputTextStyle InputText1Style" maxlength="20" id="saleOrderNumber"/>
					<label class="LabelStyle Label6Style" >Số ASN</label> 
					<input	type="text" class="InputTextStyle InputText1Style" maxlength="20" id="poCoNumber"/>
					<label class="LabelStyle Label6Style" >Số đơn đặt hàng</label> 
					<input	type="text" class="InputTextStyle InputText1Style" maxlength="20" id="poAutoNumber"/>
					
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnSearch" onclick="return POStockIn.search(false);">Tìm kiếm</button>
						<div class="Clear"></div>
						<img id="loading1" style="visibility: hidden;" src="/resources/images/loading.gif" class="LoadingStyle" />
					</div>
					<p class="ErrorMsgStyle" id="errorSearchMsg" style="display: none;"></p>
					<div class="Clear"></div>
				</div>				
				<h2 class="Title2Style">Danh sách đơn hàng tìm được</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection">
						<!-- <table id="dg" class="easyui-datagrid" ></table> -->
						<table id="dg" ></table>
					</div>					
					<div class="Clear"></div>
					<p id="successMsg1" class="SuccessMsgStyle" style="display: none"></p>				
				</div>				
			</div>			
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div id="InputTable">
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	//$('#status').customStyle();
	//applyDateTimePicker("#fromDate");
	//applyDateTimePicker("#toDate");
	$('.MySelectBoxClass').css('width','206');
	$('.CustomStyleSelectBox').css('width','173');
	// vuongmq; 08/03/2015; lay danh sach combobox don vi
	Utils.initUnitCbx('cbxShop', {}, null, function(){
		POStockIn.handleSelectShopPOStockIn();
		setTimeout(function() {
			POStockIn.searchGridInit();
		}, 2000);
	});
	
});
</script>
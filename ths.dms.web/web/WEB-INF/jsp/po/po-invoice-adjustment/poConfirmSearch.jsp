<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="GeneralTable General1Table">
                                    
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<colgroup>
										<col style="width: 50px;" />
										<col style="width: 40px;" />
										<col style="width: 135px;" />
										<col style="width: 135px;" />
										<col style="width: 250px;" />
										<col style="width: 195px;" />
										<col style="width: 135px;" />
										<col style="width: 141px;" />
										<col style="width: 140px;" />
										<col style="width: 140px;" />
									</colgroup>
									<thead>
										<tr>
											<th style="text-align: center" class="ColsThFirst">STT</th>
											<th ><input style="text-align: center" type="checkbox" id="allCheck" onchange="return POInvoiceAdjustment.onCheckAllChange(this.checked);"/></th>
											<th>Số hóa đơn</th>
											<th>Số hóa đơn mới</th>
											<th>Lý do</th>
											<th>Đơn vị</th>
											<th>Số ASN</th>
											<th>Ngày tạo</th>
											<th>Tổng tiền</th>
											<th class="ColsThEnd">Chiết khấu</th>
										</tr>
									</thead>
									<tbody>
											<s:iterator value="listPo" status="status">
												<tr>
													<td class="ColsTd1" style="text-align:center; padding:0px 5px;"><s:property value="#status.index+1" />
														<input type="hidden" id="poVnmId<s:property value="id" />" value="<s:property value="id" />"/>
													</td>
													<td class="ColsTd2" style="text-align:center; padding:5px;">
														<input tabindex="-1" type="checkbox"  invoice-num="<s:property value="id" />" 																	
																	id="check<s:property value="id" />"  class="CheckAll" 
																	onchange="return POInvoiceAdjustment.onChangePoAutoCheckbox(this);" /></td>
													<td class="ColsTd3" style="text-align:left; padding:0px 5px;" id="invoiceNumber<s:property value="id" />" ><s:property value="invoiceNumber" /></td>
													<td class="ColsTd4" style="text-align:left; padding:0px 5px;">
														<div>
															<input maxlength="40" class="skuItem InputTextStyle" onkeypress="NextAndPrevTextField(event,this,'skuItem')" type="text" style="width: 94%;" id="newInvoiceNumber<s:property value="id" />" value="<s:property value="newInvoiceNumber" />"  />
														</div>
													</td>
													<td class="ColsTd5" style="text-align:left; padding:2px 5px;">
														<div class="BoxSelect BoxSelect4"> 	
															<select id="apParamCode<s:property value="id" />" 
																	class="customstyle" style="width: 100%;">												
															<s:iterator value="listReason">
																<s:if test="apParamCode==reasonCode">
																	<option selected="selected" value="<s:property value="apParamCode" />"><s:property value="value" /></option>
																</s:if>
																<s:else>
																	<option value="<s:property value="apParamCode" />"><s:property value="value" /></option>
																</s:else>														
															</s:iterator>
															</select>
														</div>
														<%-- <s:select  id="apParamCode_#status.index+1" list="listReason" listKey="apParamCode" listValue="apParamName"></s:select> --%>
													</td>
													<td class="ColsTd6" style="text-align:left; padding:0px 5px;"><s:property value="shop.shopCode" /> - <s:property value="shop.shopName" /></td>
													<td class="ColsTd7" style="text-align:left; padding:0px 5px;"><s:property value="saleOrderNumber" /></td>
													<td class="ColsTd8" style="text-align:center; padding:0px 5px;"><s:property value="displayDate(poVnmDate)" /></td>
													<td class="ColsTd9" style="text-align:right; padding:0px 5px;">
														<!-- <s:if test="type.value==2"><s:property value="convertMoney(amount)" /></s:if>
														<s:elseif test="type.value==3">-<s:property value="convertMoney(amount)" /></s:elseif> -->
														<s:property value="convertMoneyPOVNM(amount)" /> 
														<!-- <s:property value="convertMoney(amount)" /> -->
													</td>
													<td class="ColsTd10 ColsTdEnd" style="text-align:right; padding:0px 5px;"><s:property value="convertMoney(discount)" />   </td>
												</tr>
											</s:iterator>
										</tbody>
								</table>
							</div>
							
<script type="text/javascript">
</script>
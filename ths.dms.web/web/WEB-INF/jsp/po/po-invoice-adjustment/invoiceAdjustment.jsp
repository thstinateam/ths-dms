<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Mua hàng</a></li>
		<li><span>Điều chỉnh số hóa đơn</span> </li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label2Style">Đơn vị</label>
					<!-- <label class="LabelStyle Label2Style">Đơn vị (F9)</label> -->
					<div style="float: left">
						<input type="text" id="shop" style="width:206px;" class="InputTextStyle InputText1Style" />
					</div>
					<label class="LabelStyle Label6Style">Số ASN</label> 
					<input type="text" maxlength="20" id="poNumber" class="InputTextStyle InputText1Style" value="<s:property value="poNumber"/>" /> 
					<label class="LabelStyle Label1Style">Số hóa đơn</label> 
					<input type="text" class="InputTextStyle InputText1Style" type="text" maxlength="20" id="invoiceNumber" value="<s:property value="invoiceNumber"/>" />
					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Từ ngày</label> 
					<input type="text" maxlength="10" id="createDateFrom" class="InputTextStyle InputText6Style" value="<s:property value="createDateFrom"/>" /> 
					<label class="LabelStyle Label6Style">Đến ngày</label> 
					<input type="text" maxlength="10" id="createDateTo" class="InputTextStyle InputText6Style" value="<s:property value="createDateTo"/>" autocomplete="off" />
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnSearch" onclick="return POInvoiceAdjustment.searchPoConfirm();">Tìm kiếm</button>
						<img src="/resources/images/loading.gif" style="visibility: hidden;" id="poSearchLoading" class="LoadingStyle" />
					</div>
					<div id="errorSearchMsg" class="ErrorMsgStyle" style="display: none;"></div>
					<div class="Clear"></div>
				</div>
				<h2 class="Title2Style">Danh sách hóa đơn tìm được</h2>
				<div class="SearchInSection SProduct1Form">
					<div id="listSearchPOAdjustSearch">
							<tiles:insertTemplate template="/WEB-INF/jsp/po/po-invoice-adjustment/poConfirmSearch.jsp"></tiles:insertTemplate>
					</div>
					<div class="Clear"></div>
					<div class="BtnRSection">
						<input id="checkReason" name="check" type="checkbox" class="InputCbxStyle InputCbx6Style" /> 
						<label class="LabelStyle Label7Style">Chọn lý do</label>
						<div class="BoxSelect BoxSelect2"> 
							<s:select cssClass="MySelectBoxClass" id="apParamCode" list="listReason" listKey="apParamCode" listValue="value"></s:select>
						</div>
						<button class="BtnGeneralStyle" id="adjustPOConfirm" onclick="return POInvoiceAdjustment.adjustPOConfirm();">Cập nhật</button>
						<img src="/resources/images/loading.gif" style="visibility: hidden;" class="LoadingStyle" id="adjustLoading" />
					</div>
					<div class="Clear"></div>
					<div class="Clear"></div>
					<p id="errMsg" class="ErrorMsgStyle" style="display: none">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>

<s:if test="isEditShopCode==false">
	<script type="text/javascript">
		$(document).ready(function(){	
			VTUtilJS.applyDateTimePicker('createDateFrom');
			VTUtilJS.applyDateTimePicker('createDateTo');
			ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
			});
		});
	</script>
</s:if>
<s:else>
	<script type="text/javascript">
	$(document).ready(function(){	
		VTUtilJS.applyDateTimePicker('createDateFrom');
		VTUtilJS.applyDateTimePicker('createDateTo');
		ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
		});
	});
	</script>
</s:else>
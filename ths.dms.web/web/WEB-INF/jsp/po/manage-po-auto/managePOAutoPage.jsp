<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="#" class="cmsdefault">Mua hàng</a> </li>
		<li><span>Quản lý đơn mua hàng</span> </li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label2Style">Đơn vị(F9)</label> 
					<%-- <div class="BoxSelect BoxSelect2">
						<select class="easyui-combobox" data-options="panelWidth : '206'" id="shopSelected">
							<s:iterator value="listShop"  var="obj" >
								<option value="<s:property value="#obj.id" />"><s:property value="#obj.shopCode" />-<s:property value="#obj.shopName" /></option>
							</s:iterator>	
						</select>
					</div> --%>
					<input id="shopCode" type="text" class="InputTextStyle InputText1Style" maxlength="300"/>
					
					<label class="LabelStyle Label1Style">Trạng thái</label>
					<s:if test="isEditShopCode">
						<div class="BoxSelect BoxSelect8">
							<s:select list="listApproveStatus" cssClass="MySelectBoxClass" id="approveStatus" listKey="value" listValue="name" headerKey="-1" headerValue="Tất cả"></s:select>
						</div>
					</s:if>
					<s:else>
						<div class="BoxSelect BoxSelect8">
							<s:select list="listApproveStatus" cssClass="MySelectBoxClass" value="1" disabled="true" id="approveStatus" listKey="value" listValue="name" headerKey="-1" headerValue="Tất cả"></s:select>
						</div>
					</s:else>
					<div class="Clear"></div>
					<label class="LabelStyle Label2Style">Số PO</label> 
					<input maxlength="14" id="poNumber" type="text" class="InputTextStyle InputText1Style"/>
					<label class="LabelStyle Label1Style">Ngày tạo từ <span class="ReqiureStyle">*</span></label> 
					<input maxlength="10" id="startDate" value="<s:property value="startDate"/>" type="text" class="InputTextStyle InputText6Style" />
					<label class="LabelStyle Label6Style">Đến <span class="ReqiureStyle">*</span></label> 
					<input maxlength="10" id="endDate" type="text" value="<s:property value="endDate"/>" class="InputTextStyle InputText6Style" />
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle cmsdefault" onclick="return POAutoManage.search();" >Tìm kiếm</button>
						<img id="searchLoading" style="visibility: hidden;" src="/resources/images/loading.gif" class="LoadingStyle" />
					</div>
					<p class="ErrorMsgStyle" id="errorSearchMsg" style="display: none;"></p>
                </div>
					<div class="Clear"></div>
				
				<h2 class="Title2Style">Danh sách đơn hàng tìm được</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection">
						<div id="divPOAutoTable">
							<tiles:insertTemplate template='/WEB-INF/jsp/po/manage-po-auto/POAutoTable.jsp' />
						</div>
					</div>
				</div>
				<div id="poAutoDetailTableDiv"> </div> 
				<div class="Clear"></div>
			
			</div>
		</div>
		<div class="Clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	var isEdit = '<s:property value="isEditShopCode"/>';
	VTUtilJS.applyDateTimePicker('startDate');
	VTUtilJS.applyDateTimePicker('endDate');
	if(isEdit != null && isEdit =='true'){
		$('#shopCode').val('<s:property value="shop.shopCode"/> - <s:property value="shop.shopName"/>');
		$('#shopCode').attr("disabled", "disabled");
	}else{
		$('#shopCode').bind('keyup', function(event){
			if(event.keyCode == keyCode_F9){
				VCommonJS.showDialogSearch2({
				    inputs : [
				        {id:'code', maxlength:50, label:'Mã đơn vị'},
				        {id:'name', maxlength:250, label:'Tên đơn vị'}
				    ],
				    url : '/commons/search-shop-show-list',
				    onShow : function() {
			        	$('.easyui-dialog #code').focus();
				    },
				    columns : [[
				        {field:'shopCode', title:'Mã đơn vị', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
					    	return Utils.XSSEncode(value);
						}},
				        {field:'shopName', title:'Tên đơn vị', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index) {
					    	return Utils.XSSEncode(value);
						}},
				        {field:'choose', title:'', align:'center', width:40, fixed:true, sortable:false, resizable:false, formatter:function(value,row,index) {
				        	return "<a href='javascript:void(0)' onclick='chooseShop(\""+ Utils.XSSEncode(row.shopCode) + "\",\""+ Utils.XSSEncode(row.shopName)+"\");'>Chọn</a>";        
				        }}
				    ]]
				});
			}
		});
		$('#shopCode').val('');
	}
	POAutoManage._searchParams = {
			startDate: $('#startDate').val().trim(),
			endDate: $('#endDate').val().trim(),
			approveStatus: $('#approveStatus').val().trim()
	};
	var s = $("#shopCode").val().trim();
	if (s.length > 0) {
		POAutoManage._searchParams.shopCode = s.split("-")[0].trim();
	}
});

function chooseShop(shopCode, shopName) {
	$(".easyui-dialog").dialog("close");
	$('#shopCode').val(shopCode + ' - ' + shopName);
}
</script>

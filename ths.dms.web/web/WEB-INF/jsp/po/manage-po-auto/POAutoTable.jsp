<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="GeneralTable">
	<div class="ScrollSection">
		<div class="BoxGeneralTTitle">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	             <colgroup>
	                 <col style="width:40px;" />
			         <col style="width:44px;" />
			         <col /><!-- style="width:210px;" -->
			         <col style="width:145px;" /><!-- -15 -->
			         <col style="width:127px;" /><!-- -8 -->
			         <col style="width:118px;" /><!-- -7 -->
			         <col style="width:150px;" /><!-- -40 -->
			         <col style="width:75px;" /><!-- -40 -->
	             </colgroup>
	             <thead>
	                 <tr>
	                     <th class="ColsThFirst"><input type="checkbox" id="allPOAutoCheck" onchange="return POAutoManage.onCheckAllChange(this.checked);"/></th>
	                     <th>STT</th>
	                     <th>Đơn vị</th>
	                     <th>Số đơn hàng</th>
	                     <th>Trạng thái</th>
	                     <th>Ngày tạo</th>
	                     <th>Tổng tiền</th>
<!-- 	                     <th>Tổng SL</th> -->
	                     <th class="ColsThEnd">Chi tiết</th>
	                 </tr>
	             </thead>
				<tbody>
					<s:iterator value="lstPOAuto" status="status">
						<tr>
							<td style="text-align: center" class="ColsTd1 AlignColsCenter">
								<s:if test="isEditShopCode">
								<s:if test="status.value == 1 && approveStatus == 1">
									<input type="checkbox" name="choosePOAuto" onchange="return POAutoManage.onChangePoAutoCheckbox(this.checked)" class="CheckAllPoAuto" value="<s:property value="poAutoId"/>" />
								</s:if>
								<s:elseif test="status.value == 1 && approveStatus != 1">
									<!-- chua duyet -> cho chon -->
									<input type="checkbox" name="choosePOAuto" onchange="return POAutoManage.onChangePoAutoCheckbox(this.checked)" class="CheckAllPoAuto" value="<s:property value="poAutoId"/>" disabled="disabled" />
								</s:elseif>
								<s:elseif test="status.value == 2 ||status.value == 3 ||status.value == 4">
									<!-- chua duyet -> cho chon -->
									<input type="checkbox" name="choosePOAuto" onchange="return POAutoManage.onChangePoAutoCheckbox(this.checked)" class="CheckAllPoAuto" value="<s:property value="poAutoId"/>" disabled="disabled" />
								</s:elseif>
								<s:else>
									<input type="checkbox" name="choosePOAuto" onchange="return POAutoManage.onChangePoAutoCheckbox(this.checked)" class="CheckAllPoAuto" value="<s:property value="poAutoId"/>" />
								</s:else>
								</s:if>
								<s:else>
									<input type="checkbox" name="choosePOAuto" onchange="return POAutoManage.onChangePoAutoCheckbox(this.checked)" class="CheckAllPoAuto" value="<s:property value="poAutoId"/>" disabled="disabled" />
								</s:else>
							</td>
							<td style="text-align: center" class="ColsTd2 AlignColsCenter"><s:property value="#status.index+1" /> </td>
							<td class="ColsTd3 AlignColsLeft"><s:property value="shopCode" /> - <s:property value="shopName"/> </td>
							<td class="ColsTd4 AlignColsLeft"><s:property value="poAutoNumber" /> </td>
							<td class="ColsTd5 AlignColsLeft">
								<s:if test="status.value == 0"> Chưa duyệt </s:if> 
								<s:elseif test="status.value == 1"> Đã gửi </s:elseif> 
								<s:elseif test="status.value == 2"> Đã hủy </s:elseif> 
								<s:elseif test="status.value == 3"> Từ chối </s:elseif> 
							</td>
							<td style="text-align: center" class="ColsTd6 AlignColsCenter"><s:date name="poAutoDate" format="dd/MM/yyyy" /> </td>
							<td class="ColsTd7" style="text-align: right; padding-right: 10px">
								<span class="currencyPOAuto"><s:property value="convertMoney(amount)" /> </span>
							</td>
							<td style="text-align: center" class="ColsTd8 ColsTdEnd AlignColsCenter">
								<a href="javascript:void(0);" class="cmsdefault" onclick="return POAutoManage.loadPOAutoDetail(<s:property value="poAutoId"/>, '<s:property value="poAutoNumber"/>');"> 
									<img src="/resources/images/icon-view.png" width="18" height="19" /> 
								</a>
							</td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
		</div>
	</div>	
</div>

<s:if test="lstPOAuto != null && lstPOAuto.size() > 0 && isEditShopCode">
	<div class="BtnCenterSection">
		<button class="BtnGeneralStyle BtnMSection cmsiscontrol" id="btnHuy" onclick="return POAutoManage.approvePOAuto(0);">Hủy</button>
		<button class="BtnGeneralStyle cmsiscontrol" id="btnDuyet" onclick="return POAutoManage.approvePOAuto(1);">Duyệt</button>
		<button class="BtnGeneralStyle cmsiscontrol" id="btnXml" onclick="return POAutoManage.exportXml();">Xuất XML</button>
		<img id="ApproveAndRefuseLoading" style="display: none;" src="/resources/images/loading.gif" class="LoadingStyle" />
	 </div>
	<p class="ErrorMsgStyle" id="errMsg1" style="display: none;"></p>
	<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
<%-- 	<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" /> --%>
</s:if>
<script type="text/javascript">
$(document).ready(function() {
	setTimeout(function() {
		if (!_isFullPrivilege) {
			if (_MapControl.get("btnHuy") == 3) {
				$("#btnHuy").attr("disabled", "disabled");
				$("#btnHuy").addClass("BtnGeneralDStyle");
				$("#btnHuy").attr("onclick", "");
			} else if (_MapControl.get("btnHuy") != 2) {
				$("#btnHuy").remove();
			}
			if (_MapControl.get("btnDuyet") == 3) {
				$("#btnDuyet").attr("disabled", "disabled");
				$("#btnDuyet").addClass("BtnGeneralDStyle");
				$("#btnDuyet").attr("onclick", "");
			} else if (_MapControl.get("btnDuyet") != 2) {
				$("#btnDuyet").remove();
			}
			console.log(_MapControl.get("btnXml"));
			if (_MapControl.get("btnXml") == 3) {
				$("#btnXml").attr("disabled", "disabled");
				$("#btnXml").addClass("BtnGeneralDStyle");
				$("#btnXml").attr("onclick", "");
			} else if (_MapControl.get("btnXml") != 2) {
				$("#btnXml").remove();
			}
		}
	}, 500);
});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<style type="text/css">
.BoxSelect .ui-dropdownchecklist-text,
.BoxSelect .ui-dropdownchecklist-selector {
    width: 173px !important;
}
</style>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);" class="cmsdefault">Mua hàng</a></li>
		<li><span>Quản lý sale order</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection" id="divContentW">			
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Mã NPP <span class="ReqiureStyle"></span></label>
                	<div class="BoxSelect BoxSelect2">
                		<input  type="text" class="InputTextStyle InputText1Style" id="cbxShop" maxlength="50" />
                	</div>
                	<label class="LabelStyle Label1Style">Kho <span class="ReqiureStyle"></span></label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClas" id="warehouse" style="width: 206px;">
							<%-- <s:iterator value="lstStaff" var="obj">
								<option value="<s:property value='#obj.staffCode' />"><s:property value='#obj.staffName' /></option>
							</s:iterator> --%>
						</select>
					</div>
                	<div class="Clear"></div>
                	<label class="LabelStyle Label1Style">Số đơn hàng</label>
					<input type="text" class="InputTextStyle InputText1Style"  maxlength="14" id="poNumber"/>
					<input id="shopId" type="hidden" value="<s:property value="shopId"/>"/>
					
                	<label class="LabelStyle Label1Style">Số đơn đặt hàng</label>
					<input type="text" class="InputTextStyle InputText1Style"  maxlength="14" id="poAutoNumber"/>					
					
					<!-- <label class="LabelStyle Label1Style">Số PO Confirm</label>
					<input type="text" class="InputTextStyle InputText1Style"  id="poCoNumber" maxlength="14"/> -->

					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
							<s:select cssClass="MySelectBoxClas" listKey="value" listValue="name" id="poVnmStatus" list="listPoVnmStatus" headerKey="-1" headerValue="Tất cả"></s:select>
						</div>
					<label class="LabelStyle Label1Style">Ngày tạo từ <span class="ReqiureStyle">*</span></label> 
					<input	type="text" class="InputTextStyle InputText6Style" maxlength="10" id="startDate" value="<s:property value="startDate"/>" /> 
					
					<label class="LabelStyle Label1Style">Đến <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText6Style" maxlength="10" id="endDate" value="<s:property value="endDate"/>" /> 
										
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle cmsdefault" id="btnSearch" onclick="POCustomerServiceManage.search();">Tìm kiếm</button>
					</div>
					<div class="Clear"></div>
					<img id="searchLoading" style="display: none;" src="/resources/images/loading.gif" class="LoadingStyle" />
					<div id="errorSearchMsg" class="ErrorMsgStyle" style="display: none"></div>
					
				</div>
				<h2 class="Title2Style">Danh sách đơn hàng tìm được</h2>
				<div class="SearchInSection SProduct1Form">
					<div id="divPOCSTable">
						<tiles:insertTemplate template='/WEB-INF/jsp/po/manage-po-customer-service/POCSTableSearch.jsp' />
					</div>					
					<div class="Clear"></div>
				</div>
				<div id="divPOCSConfirmTable" style="display: none"></div>							
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>

<div id="fullDettailPoVnmComfirmBox" style="display: none;">
	<div id="fullDettailPoVnmComfirm" class="easyui-dialog" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">
				<div class="GridSection" id="searchStyleShopContainerGrid">
					<table id="searchStyleShopGrid"></table>
				</div>
				<div class="Clear"></div>
				<p class="TotalInfo TotalLInfo">
				    <span class="Tem1Style">Tổng số lượng: <span id="spTotalQuantity"></span></span>
					<span class="Tem1Style">Tổng tiền: <span id="spTotalAmount"></span></span>
				</p>
<!-- 				<div class="BtnCenterSection">					 -->
<!-- 					<button class="BtnGeneralStyle" tabindex="17" onclick="return POCustomerServiceManage.closeDialogFullDetailPoVnmComfirm();">Đóng</button> -->
<!-- 				</div> -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#poNumber').focus();
	$('#btnExportPOConfirmReport').attr('disabled','disabled').addClass('BtnGeneralDStyle');
	$('#poVnmStatus').dropdownchecklist( {
		forceMultiple: true,
		firstItemChecksAll: true,
		maxDropHeight: 350,
		width: 300
	} );
	$('#poVnmStatus').val([0,1]).change();
	VTUtilJS.applyDateTimePicker("startDate");
	VTUtilJS.applyDateTimePicker("endDate");
	var width = $(window).width();
	if(width<800){
		$('#divContentW input').each(function(){
			if($(this).is(':text'))
				$(this).css('width','125px');
		});
		$('#poVnmStatus').next().css('width','103px');
	}
	// vuongmq; 08/03/2015; lay danh sach combobox don vi
	Utils.initUnitCbx('cbxShop', {isChoiceAll: true}, null, function(rec) {
		//POCustomerServiceManage.handleSelectShopPOVNM();
		console.log('shopId ne: ' + rec.id);
		$('#shopId').val(rec.id);
		hideAllMessage();
		var url = '/commons/change-shop-get-warehouse';
		var params = new Object();
		params.shopCode = rec.shopCode;
		params.isChoiceAll = true;
		Utils.changeShopGetWareHouse('#warehouse', url, params, 206, function(data) {
			
		});
	});
});
</script>
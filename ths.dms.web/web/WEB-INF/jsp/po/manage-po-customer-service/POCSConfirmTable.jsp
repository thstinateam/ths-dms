<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<h2 class="Title2Style">Danh sách đơn ASN của sale order: <span style="color:#199700;"><s:property value="poNumber"/></span></h2>

<div class="GeneralTable General1Table" id="gridIndexDetail" style="padding:0 10px 0 10px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
					    <colgroup>
					    	 <col style="width:35px;">
					        <col style="width:150px;" />
					        <col style="width:125px;" />
					        <col style="width:80px;" />
					        <col style="width:115px;" />
					        <col style="width:125px;" />
					        <col style="width:89px;" />
					        <col style="width:120px;" />
					        <col style="width:150px;" />
					        <col style="width:55px;" />
					    </colgroup>
					    <thead>
					        <tr>
					        	<th class="ColsThFirst">STT</th>
					            <th>Số ASN</th>
					            <th>Trạng thái</th>
					            <th>Ngày tạo</th>
					            <th>Tổng tiền</th>
					            <th>TT đã nhận theo HĐ</th>
					            <th>Tổng SL</th>
					            <th>Tổng SL đã nhận</th>
					            <th>Số HĐ</th>
					            <th class="ColsThEnd">Chi tiết</th>
					        </tr>
					    </thead>
					    <tbody id="pocscfTable">	
									<s:if test="lstPOVNMConfirm != null && lstPOVNMConfirm.size() > 0">					
										<s:iterator value="lstPOVNMConfirm"  status="status">
											<tr>
												<td class="ColsTd1" style="text-align: center"><s:property value="#status.index+1" /></td>
												<td class="ColsTd2 AlignLeft"><s:property value="saleOrderNumber"/></td>
												<td class="ColsTd3 AlignLeft">
													<s:if test="type.value == 2">
														<s:if test="status.value == 0">Chưa nhập</s:if>
														<s:elseif test="status.value == 1">Đã nhập một phần</s:elseif>
														<s:elseif test="status.value == 2">Đã nhập</s:elseif>
														<s:elseif test="status.value == 3">Đã hủy</s:elseif>
														<s:elseif test="status.value == 4">Đã treo</s:elseif>
													</s:if>
													<s:elseif test="type.value == 3">
														<s:if test="status.value == 0">Chưa trả</s:if>
														<s:elseif test="status.value == 1">Đã trả một phần</s:elseif>
														<s:elseif test="status.value == 2">Đã trả</s:elseif>
														<s:elseif test="status.value == 3">Đã hủy</s:elseif>
														<s:elseif test="status.value == 4">Đã treo</s:elseif>
													</s:elseif>
												</td>
												<td class="ColsTd4" style="text-align: center"><s:property value="displayDate(poVnmDate)"/></td>
												<td style="text-align: right; padding-right: 3px"><span class="currencyPOCSConfirm"><s:property value="convertMoneyPOVNM(amount)"/></span></td>
												<td style="text-align: right; padding-right: 3px">
													<s:if test="status.value == 0">0</s:if>
													<s:elseif test="status.value == 1 || status.value == 2">
														<span class="currencyPOCSConfirm">
															<s:property value="convertMoneyPOVNM(amountReceived)"/>
														</span>
													</s:elseif>
													<%-- <s:elseif test="status.value == 2">
														<span class="currencyPOCSConfirm">
															<s:property value="convertMoney(amount)"/>
														</span>
													</s:elseif> --%>												
												</td>
												<td style="text-align: right; padding-right: 3px"><span class="currencyPOCSConfirm"><s:property value="displayNumberPOVNM(quantity)"/></span></td>
												<td style="text-align: right; padding-right: 3px">
													<s:if test="status.value == 0">0</s:if>
													<s:elseif test="status.value == 1 || status.value == 2">
														<span class="currencyPOCSConfirm"><s:property value="displayNumberPOVNM(quantityReceived)"/></span>
													</s:elseif>
													<%-- <s:elseif test="status.value == 2">
														<span class="currencyPOCSConfirm"><s:property value="displayNumber(quantity)"/></span>
													</s:elseif> --%>												
												</td>
												<td class="ColsTd2 AlignLeft"><s:property value="invoiceNumber"/></td>
												<td  class="ColsTd9 ColsTdEnd" style="text-align: center;">
													<a id="colIndexDetail" class="cmsdefault" href="javascript:void(0);" onclick="return POCustomerServiceManage.getDetailPOConfirm(<s:property value="id"/>, '<s:property value="poCoNumber"/>');">
														<img src="/resources/images/icon-view.png" width="18" height="19" />
													</a>
												</td>
											</tr>
										</s:iterator>	
									</s:if>
									<s:else>
										<p class="ValueStyle Value1Style">Không có dữ liệu nào</p>
									</s:else>					
							</tbody>
					</table>
</div>
	
<div id="poCSConfirmTableDetailProductPopup">
	<div id="poCSConfirmTableDetailProduct"></div>
</div>

<script type="text/javascript">
	$(document).ready(function() {	
		Utils.bindTRbackground();
		
	});
</script>



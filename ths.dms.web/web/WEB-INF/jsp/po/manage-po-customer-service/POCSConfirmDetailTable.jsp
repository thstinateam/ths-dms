<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="GeneralTable Table6Section">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">            	
			<div class="GridSection">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<colgroup>
						<col style="width: 60px;" />
						<col style="width: 100px;" />
						<col style="width: 80px;" />
						<col style="width: 80px;" />
						<col style="width: 240px;" />
						<!-- <col style="width: 110px;" /> -->
						<col style="width: 90px;" />
						<col style="width: 90px;" />
						<col style="width: 150px;" />
					</colgroup>
					<thead>
						<tr>
							<th class="ColsThFirst" style="text-align: center">STT</th>
							<th>Số dòng sale order</th>
							<th>Số dòng đơn đặt</th>
							<th>Mã sản phẩm</th>
							<th>Tên sản phẩm</th>
							<!-- <th>Loại SP</th> -->
							<th>Đơn giá</th>
							<!-- <th>Số lô</th> -->
							<th>Số lượng</th>
							<th class="ColsThEnd">Tổng tiền</th>
						</tr>
					</thead>
					<tbody id="pocscfDetail">
					<s:iterator value="listPoConfirmDetail" status="status">
						<tr>
							<td class="ColsTd1" style="text-align: center"><s:property value="#status.index+1" /></td>
							<td class="ColsTd2" style="text-align: center"><s:property value="lineSaleOrder"/></td>
							<td class="ColsTd3" style="text-align: center"><s:property value="linePo"/></td>
							<td class="ColsTd4 AlignLeft" style="padding-left:2px !important;"><s:property value="productCode"/></td>
							<td style="text-align: left; padding-left: 2px;"><s:property value="productName"/></td>
							<!-- <td style="text-align: left; padding-left: 2px;">
								<s:if test="productType == 1">Hàng bán</s:if>
								<s:elseif test="productType == 2">Hàng KM</s:elseif>
								<s:elseif test="productType == 3">POSM</s:elseif>
							</td> -->
							<td style="text-align: right; padding-right: 2px;">
								<s:property value="convertMoney(price)"/>
							</td>
							<!-- <td style="text-align: right; padding-right: 2px;">
								<span class="currencyPOCSConfirmDetail">
									<s:property value="lot"/>
								</span>
							</td> -->
							<%-- <td class="ColsTd6" style="text-align: right; padding-right: 2px;"><s:property value="cellQuantityFormatter(quantity, convfact)"/></td> --%>
							<td class="ColsTd6" style="text-align: right; padding-right: 2px;"><s:property value="cellQuantityFormatter(displayPositiveNumber(quantity), convfact)"/></td>
							<td style="text-align: right; padding-right: 2px;">
								<span class="currencyPOCSConfirmDetail">
									<s:property value="convertMoneyPOVNM(amount)"/>
								</span>
							</td>
						</tr>
					</s:iterator>																										
					</tbody>
				</table>
			</div>
			<div class="Clear"></div>
			<p class="TotalInfo TotalLInfo">
			    <span class="Tem1Style">Tổng số lượng: <span><s:property value="displayNumberPOVNM(totalQtyDetail)"/></span></span>
			<span class="Tem1Style">Tổng tiền: <span><s:property value="displayNumberPOVNM(totalAmountDetail)"/></span></span>
			</p>
			<div class="BtnCenterSection">
			    <button class="BtnGeneralStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {	
		Utils.bindTRbackground();
	});
</script>
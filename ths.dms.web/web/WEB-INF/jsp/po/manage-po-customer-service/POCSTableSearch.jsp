<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

 <div class="GeneralTable General1Table" id="gridIndex">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" >           
		<colgroup>
			<col style="width:25px;" />
			<col style="width:35px;">
			<col style="width:120px;" />
			<col style="width:100px;" />
			<col style="width:100px;" />
			<col style="width:120px;" />
			<col style="width:80px;" />
			<col style="width:105px;" />
			<col style="width:125px;" />
			<col style="width:80px;" />
			<col style="width:104px;" />
			<col style="width:80px;" />
			<col style="width:104px;" />
			<col style="width:55px;" />
			<col style="width:55px;" />
		</colgroup>
		<thead>
			<tr>
				<th class="ColsThFirst" style="text-align: center;">
					<input id="allPOVNMStatus" type="checkbox" onchange="POCustomerServiceManage.onPOVNMStatusChange(this.checked);"/>
				</th>
				<th>STT</th>
				<th>Mã nhà phân phối</th>
				<th>Số đơn hàng</th>
				<th>Số đơn đặt hàng</th>
				<th>Trạng thái</th>
				<th>Ngày tạo</th>
				<th>Tổng tiền</th>
				<th>TT đã nhận theo HĐ</th>
				<th>Tổng SL</th>
				<th>Tổng SL đã nhận</th>
				<th>Chiết khấu</th>
				<th>Tổng tiền thanh toán</th>
				<th class="ColsThEnd" >Chi tiết</th>
				<th class="ColsThEnd" >Ds mặt hàng</th>
			</tr>
		</thead>
		<tbody id="pocsTabletSearch">	
			<s:if test="lstPOVNM != null && lstPOVNM.size() > 0">					
				<s:iterator value="lstPOVNM" status="status">
					<tr>
						<td class="ColsTd1 AlignCCols" >
							<s:if test="status == 0 || status == 1">
								<input class="POVNMStatus" name="poVnmStatus" type="checkbox"  value="<s:property value="poVNMId"/>" 
													onchange="return POCustomerServiceManage.onPOVNMStatusCheckBoxChange(this.checked);"/>
							</s:if>
							<s:else>
								<input class="POVNMStatus" name="poVnmStatus" disabled="disabled" type="checkbox" value="<s:property value="poVNMId"/>" />
							</s:else>
						</td>
						<td class="ColsTd2 AlignCCols"><s:property value="#status.index+1" /></td>
						<td class="ColsTd3 AlignLCols"><s:property value="shopCode"/> - <s:property value="shopName"/></td>
						<td class="ColsTd4 AlignLCols"><s:property value="saleOrderNumber"/></td>
						<td class="AlignLCols"><span class="currencyPOCS"><s:property value="poAutoNumber"/></td>
						<td class="ColsTd5 AlignLCols">
							<s:if test="type == 1">
								<s:if test="status == 0">Chưa nhập</s:if>
								<s:elseif test="status == 1">Đã nhập một phần</s:elseif>
								<s:elseif test="status == 2">Đã nhập</s:elseif>
								<s:elseif test="status == 3">Có thể treo</s:elseif>
								<s:elseif test="status == 4">Đã treo</s:elseif>
							</s:if>		
							<s:elseif test="type == 4">
								<s:if test="status == 0">Chưa trả</s:if>
								<s:elseif test="status == 1">Đã trả một phần</s:elseif>
								<s:elseif test="status == 2">Đã trả</s:elseif>
								<s:elseif test="status == 3">Có thể treo</s:elseif>
								<s:elseif test="status == 4">Đã treo</s:elseif>
							</s:elseif>						
						</td>
						<td class="ColsTd6 AlignCCols"><s:property value="displayDate(poVNMDate)"/></td>
						<td class="AlignRCols"><span class="currencyPOCS"><s:property value="convertMoneyPOVNM(amount)"/></span></td>
						<td class="AlignRCols"><span class="currencyPOCS"><s:property value="convertMoneyPOVNM(amountReceived)"/></span></td>
						<td class="AlignRCols"><span class="currencyPOCS"><s:property value="displayNumberPOVNM(quantity)"/></span></td>
						<td class="AlignRCols"><span class="currencyPOCS"><s:property value="displayNumberPOVNM(quantityReceived)"/></span></td>
						<td class="AlignRCols"><span class="currencyPOCS"><s:property value="displayNumberPOVNM(discount)"/></span></td>
						<td class="AlignRCols"><span class="currencyPOCS"><s:property value="displayNumberPOVNM(total)"/></span></td>
						<td class="AlignCCols ColsTdEnd" >
							<a id="colIndex" href="javascript:void(0);" class="cmsdefault" onclick="POCustomerServiceManage.getDetailPOCustomerService(<s:property value="poVNMId"/>, '<s:property value="saleOrderNumber"/>');">
								<img src="/resources/images/icon-view.png" width="18" height="19" />
							</a>
						</td>
						<td class="AlignCCols ColsTdEnd" >
							<a id="colIndex" href="javascript:void(0);" class="cmsdefault" onclick="POCustomerServiceManage.getFullDetailPOConfirm(<s:property value="poVNMId"/>, '<s:property value="saleOrderNumber"/>');">
								<img src="/resources/images/icon-view.png" width="18" height="19" />
							</a>
						</td>
					</tr>
				</s:iterator>	
			</s:if>	
			<s:else>
				<p class="NotData">Không có dữ liệu nào</p>
			</s:else>					
		</tbody>
	</table>
</div>

<div class="BtnCenterSection">
	<button class="BtnGeneralStyle cmsiscontrol" id="btnProcessStop" onclick="POCustomerServiceManage.suspend();">Xử lý treo</button>
	&nbsp;&nbsp;
	<button class="BtnGeneralStyle cmsiscontrol" id="btnExportPOConfirmReport" onclick="POCustomerServiceManage.exportCompare();">Xuất báo cáo đối chiếu Sale order và ASN</button>
	<script type="text/javascript">
	$(document).ready(function() {
		/*setTimeout(function() {
			if (!_isFullPrivilege) {
				if (_MapControl.get("btnProcessStop") == 3) {
					$("#btnProcessStop").attr("disabled", "disabled");
					$("#btnProcessStop").addClass("BtnGeneralDStyle");
					$("#btnProcessStop").attr("onclick", "");
				} else if (_MapControl.get("btnProcessStop") != 2) {
					$("#btnProcessStop").remove();
				}
				if (_MapControl.get("btnExportPOConfirmReport") == 3) {
					$("#btnExportPOConfirmReport").attr("disabled", "disabled");
					$("#btnExportPOConfirmReport").addClass("BtnGeneralDStyle");
					$("#btnExportPOConfirmReport").attr("onclick", "");
				} else if (_MapControl.get("btnExportPOConfirmReport") != 2) {
					$("#btnExportPOConfirmReport").remove();
				}
			}
		}, 500);*/
	});
	</script>
</div>
<img id="suspendLoading" style="display: none;" src="/resources/images/loading.gif" class="LoadingStyle" />
<p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none;"></p>
<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />

<s:hidden id="countTemp" name="countTemp"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){		
	Utils.bindTRbackground();
	/* var count = 0;
	var tabindex = 7;
	$('#gridIndex').find('#colIndex').each(function() {
        if (this.type != "hidden") {
            var $input = $(this);
            $input.attr("tabindex", tabindex);

            // select the first one.
            if (tabindex == 7) {
               $input.select(); 
            }
            tabindex++;
            count = tabindex;
        }
    });
	$('#btnProcessStop').attr('tabindex',count+1).select();
	$('#btnExportPOConfirmReport').attr('tabindex',count+2).select(); */
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="javascript:void(0);" class="cmsdefault">Mua hàng</a></li>
        <li><span>Tạo đơn mua hàng</span></li>
    </ul>
</div>

<div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection">
            <div class="SearchSection GeneralSSection">
            	<h2 class="Title2Style">Thông tin chung</h2>
                <div class="SearchInSection SProduct1Form">
                	<label class="LabelStyle Label12Style">Từ ngày:</label>
                    <p class="ValueStyle Value2Style"><s:property value="fromDate"/></p>
                    <label class="LabelStyle Label12Style">Đến ngày:</label>
                    <p class="ValueStyle Value2Style"><s:property value="toDate"/></p>
                    <div class="Clear"></div>
                    <label class="LabelStyle Label12Style">Số ngày bán hàng thực tế:</label>
                    <p class="ValueStyle Value2Style"><s:property value="buyingDate" /></p>
                    <label class="LabelStyle Label12Style">Số ngày bán hàng trong tháng:</label>
                    <p class="ValueStyle Value1Style"><s:property value="buyingDateInMonth" /></p>
                    <div class="Clear"></div>
                    <%-- <p class="LabelStyle Label1Style">Đơn vị</p>
		            <div class="BoxSelect BoxSelect2" style="margin-left: 45px;">
						<select class="easyui-combobox" data-options="panelWidth : '206'" id="shopSelected">
							<s:iterator value="listShop"  var="obj" >
								<option value="<s:property value="#obj.id" />"><s:property value="#obj.shopCode" />-<s:property value="#obj.shopName" /></option>
							</s:iterator>	
						</select>                  
					</div>
					<div class="Clear"></div> --%>
                </div>
                <h2 class="Title2Style">Đơn hàng đề nghị</h2>
                <div class="SearchInSection SProduct1Form">
                	<div class="GridSection" id="gridSection">
                        <div class="GeneralMilkBox">
							<div class="GeneralMilkTopBox">
								<div class="GeneralMilkBtmBox">
									<div class="GeneralMilkInBox" id="ListPoAutoTable">
										<div class="CPurchase21Form">
											<table id="poDataGrid" title="Đơn hàng đề nghị" >
												<thead data-options="frozen:true">
													<tr>
														<th data-options="field:'productCode',width:100" rowspan="2">Mã hàng</th>
														<th data-options="field:'productName',width:250" rowspan="2">Tên hàng</th>
														<th data-options="field:'openStockTotal',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.openStockTotal);}" rowspan="2">Đầu kỳ</th>
														<th data-options="field:'impQty',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.impQty);}" rowspan="2">Nhập</th>
														<th data-options="field:'expQty',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.expQty);}" rowspan="2">Xuất</th>
														<th data-options="field:'stockQty',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.stockQty);}" rowspan="2">Tồn</th>														
													</tr>
												</thead>
												<thead>
													<tr>
														<th data-options="field:'monthCumulate',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.monthCumulate);}" rowspan="2">LKTTT</th>
														<th data-options="field:'monthPlan',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.monthPlan);}" rowspan="2">KHTTT</th>
														<th data-options="field:'dayPlan',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.dayPlan);}" rowspan="2">DMKH</th>
														<th colspan="2">Dự trữ</th>
														<th colspan="3">Tồn kho</th>
														<th data-options="field:'requimentStock',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.requimentStock);}" rowspan="2">Y/c tồn SL</th>
														<th data-options="field:'qtyGoPoConfirm',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.goPoConfirmQty);}" rowspan="2">PO Confirm</th>
														<th data-options="field:'stockPoDvkh',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.stockPoDvkh);}" rowspan="2">PO DVKH</th>
														<th colspan="4">Yêu cầu</th>
													</tr> 
													<tr>
														<th data-options="field:'dayReservePlan',width:65, align:'right',formatter:function(value, row, index) {var n = Number(row.dayReservePlan); var num = 0; num = (typeof n === 'number' && n % 1 == 0) ? Number(row.dayReservePlan) : (Number(row.dayReservePlan)).toFixed(2); return formatCurrency(num);}">KH</th>
														<th data-options="field:'dayReserveReal',width:65, align:'right',formatter:function(value, row, index) {var n = Number(row.dayReserveReal); var num = 0; num = (typeof n === 'number' && n % 1 == 0) ? Number(row.dayReserveReal) : (Number(row.dayReserveReal)).toFixed(2); return formatCurrency(num);}">TT</th>
														<th data-options="field:'safetyStockMin',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.safetyStockMin);}">Min</th>
														<th data-options="field:'next',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.next);}">Next</th>
														<th data-options="field:'lead',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.lead);}">Lead</th>
														<th data-options="field:'convfact',width:65, align:'right',formatter:function(value, row, index) {return formatCurrency(row.convfact);}">QC</th>
														<th data-options="field:'quantity',width:65, align:'right',formatter:function(value, row, index) {
																if(row.quantity > 0) {
																	POInit.totalStockNumber = POInit.totalStockNumber + row.convfact * row.quantity; 
																	POInit.totalMoney = POInit.totalMoney +  row.convfact * row.quantity * row.priceValue;
																}
																return formatCurrency(row.quantity);
															}">SLT</th>
														<th data-options="field:'amount',width:120, align:'right',formatter:function(value, row, index) {return formatCurrency(row.amount);}">Thành tiền</th>
														<th data-options="field:'weight',width:120, align:'right',formatter:function(value, row, index) {return formatFloatValue(row.grossWeight,2);}">Tổng trọng lượng</th>
														<th data-options="field:'warning',width:65, align:'center',formatter:function(value, row, index) {return row.warning}">CB</th>
													</tr>
												</thead>
											</table>
											<div class="Clear"></div>
											
											<p id="errMsg" class="ErrorMsgStyle" style="display: none">Có lỗi xảy ra khi cập nhật dữ liệu</p>
											<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
											<div class="Clear"></div>
											
						                    <p class="TotalInfo">
						                    	<span class="Tem1Style">Tổng SL: <span id="totalStockNumber" style="color:#f00;"></span></span>
						                        <span class="Tem1Style">Tổng tiền (bao gồm VAT): <span id="totalMoney" style="color:#f00;"></span></span>
						                        <span class="Tem1Style">Tổng trọng lượng: <span id="totalWeight" style="color:#f00;"></span></span>
						                    </p>
						                    <div class="BtnCenterSection">
						                        <button id="btnCreate" class="BtnGeneralStyle cmsiscontrol" onclick="return POInit.convertPO();">Tạo đơn hàng</button>
						                        <button id="btnExport" class="BtnGeneralStyle cmsiscontrol" onclick="return POInit.exportExcelPO();">Xuất Excel</button>
						                    </div>
						                    <div class="Clear"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
            <div class="Clear"></div>
        </div>
    </div>
    <div class="Clear"></div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$('#poDataGrid').datagrid({
		url: '/po/init-po-auto/getpo',
		queryParams : {checkAllProductType : true/* , shopId:shopId */},
		singleSelect:true,
		pagination:false,
		width: $(window).width() - 45,
		height: 350,
		rownumbers:true,
		onLoadSuccess: function(data) {
			var totalStockNumber = 0;
			var totalMoney = 0;
			var totalWeight = 0;
			if (data!=null) {
				for(var i=0;i<data.rows.length;++i){
					totalStockNumber += Number(data.rows[i].convfact * data.rows[i].quantity);
					totalMoney += Number(data.rows[i].convfact*data.rows[i].quantity*data.rows[i].priceValue);
					totalWeight += Number(data.rows[i].grossWeight);
				}
			}
			$('#totalStockNumber').html(formatCurrency(totalStockNumber));
			$('#totalMoney').html(formatCurrency(totalMoney) + ' VNĐ');
			$('#totalWeight').html(formatFloatValue(totalWeight,2) + ' kg');
		}
	});
});
</script>
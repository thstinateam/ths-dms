<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="ths.dms.helper.Configuration"%>

<style>
#player_api { width:98% !important; height: 546px; }
</style>
			<div class="UploadMediaSection">
				<div class="MediaBox ">
					<label style="font-weight: bold"><s:text name="feedback.upload.file"/>:</label>
					<!-- <label class="LabelStyle Label1Style Text1Style"><s:text name="feedback.upload.file"/>:</label> -->
					<div class="Clear"></div>
					<div class="UploadMediaBox"	>
						<span class="addFileBtn" style="margin-bottom: 10px; margin-top:10px; background-color: #0299cb;"><s:text name="feedback.upload.select.file"/></span>
						<span class="fileupload-process">
							<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-striped active" id="total-progress" style="opacity: 0; ">
								<div data-dz-uploadprogress="" style="width: 200px;" class="progress-bar progress-bar-success"></div>
							</div>
						</span>
	
						<p id="imageMsg" class="ErrorMsgStyle SpriteErr"style="display: none;"></p>	
						<div  style="border: 1px solid #c4c4c4">
							<div class="table table-striped" class="files" id="previews" style="height: 140px; overflow-x: hidden; overflow-y: scroll">
								<div id="template" class="file-row" style="margin-bottom: 10px; width: 260px;float: left">
									<!-- This is used as the file preview template -->
									<div style="margin-right: 10px; width: 50px; float: left">
										<span class="preview"><img data-dz-thumbnail /></span>
									</div>
									<div style="display: inline-block; width: 200px; clear: both">
										<div style="padding-right: 10px">
											<div>
												<p class="name" style="overflow: hidden" data-dz-name></p>
												<strong class="error text-danger" data-dz-errormessage style="color: #e50303;"></strong>
											</div>
											<div style="">
												<p class="size" data-dz-size></p>
												</div>
											<div>
												<a data-dz-remove href="javascript:void(0)"><span><img border="0" src="/resources/scripts/plugins/uploadify2.1/cancel.png"></span></a>
											</div>
										</div>								
									</div>							
								</div> 
							</div>
						</div>
					</div>						
			</div>
			<div class="GeneralForm GeneralNoTPForm">
			    <div class="BtnCenterSection">
			        <button id="img_db_video_btnUpload" class="BtnGeneralStyle" onclick="return FeedBack.uploadImageFileFeedback();">Lưu vấn đề</button>
			   </div>                        
			   </div>
			   <div class="Clear"></div>
			</div>	
			<p id="errMsgUpload" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
			<p id="successMsg" class="SuccessMsgStyle" style="display: none; padding-left: 10px;"></p>
			<!-- <p id="sucMsgUpload" class="SuccessMsgStyle" style="display: none"></p> -->
<s:hidden id="maxTotalSize" name="maxTotalSize"></s:hidden>
<s:hidden id="fileExConfigAttach" name="fileExConfig"></s:hidden>
<script type="text/javascript">
var formatUrl = '<s:property value="mediaItem.url"/>';
$(document).ready(function(){
	/* Utils.functionAccessFillControl('divTabImageVideo', function (data) {
		//Xu ly cac luong lien quan den Cac control phan quyen
	}); */
	
	// upload images
	if(DropzonePlugin._dropzoneObject != undefined && DropzonePlugin._dropzoneObject != null){
		DropzonePlugin._dropzoneObject.destroy();	
	}
	var fileExConfig = $('#fileExConfigAttach').val();
	FeedBack.initUploadImageElement('body', 'body', '.addFileBtn', fileExConfig);	
});
</script>
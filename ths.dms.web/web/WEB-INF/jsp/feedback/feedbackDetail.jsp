<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
.labelPopStaff {
	float: left;
	padding: 2px 0 0;
	margin-left: 30px; 
	width: 100px;
	color: #215ea2;
}
.labelPopCustomer {
	float: left;
	padding: 2px 0 0;
	margin-left: 30px; 
	width: 80px;
	color: #215ea2;
}
</style> 
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/feedback/info">Phản hồi</a></li>
		<li><span>Thông tin vấn đề</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection"> 
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
   		<!-- <div class="ContentSection">
       		<div class="GeneralCntSection GeneralCntNPSection"> -->
				<h2 class="Title2Style">Thông tin vấn đề</h2>
				<div class="SearchInSection SProduct1Form" id="search-form">
				 	<label class="LabelStyle Label1Style">Loại vấn đề <span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2" id="subcat">
		               	<select id="typeFeedback" class="MySelectBoxClass" >
		               			<option value="">Chọn loại vấn đề</option>
	                            <s:iterator var="obj" value="lstApParam"> 
	                               <option value="<s:property value="#obj.apParamCode" />"><s:property value="#obj.apParamName" /></option>                        		
	                            </s:iterator>                               		
		               </select> 
		             </div>
					<label class="LabelStyle Label1Style">Ngày nhắc nhở <span class="ReqiureStyle">*</span></label>
					<input id="fDate" class="InputTextStyle InputText6Style" value="<s:property value='fromDate' />">
					<div class="Clear"></div>
					<p class="ErrorMsgStyle SpriteErr" id="errMsgSearch" style="display: none;"></p>
					<div class="Clear"></div>
				</div>
				<div class="Clear"></div>
				<h2 class="Title2Style">Danh sách nhân viên - khách hàng</h2>
				<div class="SearchInSection SProduct1Form">
                    <div class="GridSection">
                		<div id="feedbackResultAddStaff" class="GeneralTable">
							<div class="BoxGeneralTTitle" style="width: 100%;" id="dgGridContainer">
								<table id="gridAddStaff"></table>
								<div id="pager"></div>
							</div>
						</div>
		                <div class="Clear"></div>
                     </div>
                     <div class="Clear"></div>
                 	<label class="LabelStyle Label1Style">Nội dung vấn đề <span class="ReqiureStyle">*</span></label>
			         <textarea maxlength="1000" rows="10" cols="50" id="popContent" style="width: 522px; margin-left: 0px; border: 1px solid #c4c4c4; font-size: 1em;"></textarea>
		            <div class="Clear"></div>
                    <!-- <p id="successMsg" class="SuccessMsgStyle" style="display: none; padding-left: 10px;"></p> -->
                    <br>
                    <p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none; margin-left: 5px;"></p>
                </div>
				<div class="Clear"></div>
				<h2 class="Title2Style">Tập tin đính kèm</h2>
				<div class="SearchInSection SProduct1Form" id="tabFeedbackImage">
					<tiles:insertTemplate template="/WEB-INF/jsp/feedback/feedbackImage.jsp" />
				</div>
				<div class="Clear"></div>
			</div>
		</div>
		<div class="Clear"></div>
	</div>
</div>
<div class="Clear"></div>
<!--vuongmq: Popup Them NV -->
<div id ="searchStyle2EasyUIDialogDiv" style="display:none; width: 630px; height: auto;" >
	<div id="searchStyle2EasyUIDialog" data-options="closed: true, modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">
				<div style ="margin-bottom: 10px;">
					<input id="checkBoxStaff" type="checkbox" maxlength="250" tabindex="1"/>
					<label for="checkBoxStaff" class="labelPopStaff" style="margin-top: -3px">Gửi lên cấp trên</label> 
				</div>
				<div class="Clear"></div>
				<label class="labelPopStaff"><s:text name="image_manager_unit"/> <span class="ReqiureStyle">*</span></label>
				<div class="BoxSelect ">
					<input  type="text" class="InputTextStyle InputText1Style" style="width: 205px;" id="cbxUnitPopupStaff" maxlength="50" />
				</div>
				<div class="Clear"></div> 
			 	<label class="labelPopStaff">Loại nhân viên</label>
				<div class="BoxSelect BoxSelect2" id="subcat">
	               	<select id="typePopupStaff" class="MySelectBoxClass" tabindex="2">  
	                   		<option value="">Chọn loại nhân viên</option>                            		
	               </select> 
	             </div>
	            <div class="Clear"></div> 
				<label id="searchStyle2EasyUINameLabel" class="labelPopStaff">Tìm nhân viên</label> 
				<input id="searchStyle2EasyUIName" placeholder="Nhập mã hoặc tên nhân viên" type="text" maxlength="250" tabindex="3"  class="InputTextStyle InputText3Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnSearchOnDialog" tabindex="4" id="btnSearchStyle2EasyUI">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
				<s:hidden id="shopIdPopupStaff" name="shopId"></s:hidden>
				<s:hidden id="curShopIdPopupStaff" name="shopId"></s:hidden>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle" id="searchStyle2EasyUIContainerTitle">Kết quả tìm kiếm </h2>
				<div class="GridSection" id="searchStyle2EasyUIContainerGrid">
					<table id="searchStyle2EasyUIGrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="btnSearchStyle2EasyUIUpdate">Lưu</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" id="btnSearchStyle2EasyUIClose" onclick="$('#searchStyle2EasyUIDialog').dialog('destroy');">Đóng</button>
				</div>
			</div>
			<p id="errMsgSearchStyle2EasyUI" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
		</div>
	</div>
</div>
<div class="Clear"></div>
<!--vuongmq: Popup Them Customer -->
<div id ="searchCustomerDiv" style="display:none; width: 630px; height: auto;" >
	<div id="searchCustomerDialog" data-options="closed: true, modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">
				<label class="labelPopCustomer"><s:text name="image_manager_unit"/><span class="ReqiureStyle"> *</span></label>
				<div class="BoxSelect BoxSelect12">
					<input  type="text" style="width:226px;" id="cbxShop" maxlength="50" />
				</div>
				<div class="Clear"></div> 
				<label id="searchCustomerNameLabel" class="labelPopCustomer">Khách hàng</label> 
				<input id="searchCustomerName" placeholder="Nhập mã tên địa chỉ khách hàng" type="text" maxlength="250" tabindex="3"  class="InputTextStyle InputText3Style" />
				<div style="margin-top:4px; margin-left: 10px; float:left">
					<input id="checkBoxRouting" type="checkbox" maxlength="250" tabindex="1"/>
					<label for="checkBoxRouting" class="" style="margin-top: -3px; color: #215ea2;">Theo tuyến</label> 
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnSearchOnDialog" tabindex="4" id="btnSearchCustomer">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
				<s:hidden id="shopIdPopupCustomer"></s:hidden>
				<s:hidden id="shopCodePopupCustomer"></s:hidden>
				<s:hidden id="staffIdPopup"></s:hidden>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle" id="searchStyle2EasyUIContainerTitle">Kết quả tìm kiếm </h2>
				<div class="GridSection" id="searchCustomerContainerGrid">
					<table id="searchCustomerGrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="btnCustomerUpdate">Lưu</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" id="btnCustomerClose" onclick="$('#searchCustomerDialog').dialog('destroy');">Đóng</button>
				</div>
			</div>
			<p id="errMsgSearchCustomer" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
		</div>
	</div>
</div>
<div class="Clear"></div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	$('#fDate').datetimepicker({
 		lang:'vi',
 		format:'d/m/Y H:i'
	});
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	var hour = currentTime.getHours();
	var minute = currentTime.getMinutes();
	if (month < 10) {
		month = '0' + month;
	}
	if (day < 10) {
		day = '0' + day;
	}
	if (hour < 10) {
		hour = '0' + hour;
	}
	if (minute < 10) {
		minute = '0' + minute;
	}
	$('#fDate').val(day + '/' + month + '/' + year + ' 00:00');
	FeedBack.initGridAddStaff();
});
</script>
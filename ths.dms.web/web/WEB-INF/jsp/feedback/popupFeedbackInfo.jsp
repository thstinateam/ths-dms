<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
.GeneralForm .labelPopFB, .GeneralSSection .labelPopFB {
	float: left;
	color: #000000;
	padding: 2px 0 0;
}
.labelWidth {
	width: 260px; 
	float:left;
}
</style>
<div id="divDialogFeedback" style="visibility: hidden;">
<div id="popupInfoFeedback" class="easyui-dialog" title="Thông tin vấn đề" data-options="closed:true,modal:true" style="width:960px;height:200px;">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form" id="fm">
        		<label class="LabelLeftStyle">Nội dung vấn đề</label>
	             <textarea class="InputTextStyle InputArea1Style" rows="4" cols="50" id="popContent" style="width: 522px; margin-left: 0px" maxlength="1000"></textarea>
	            <div class="Clear"></div>
	            <div class="labelWidth">
	            	<label class="LabelLeftStyle" style="margin-right: 44px">Người tạo:</label>
	            	<label id="popCreateName" class="labelPopFB">Mai Quốc Vương 1</label>	
	            </div>
                <div class="labelWidth">
		            <label class="LabelLeftStyle">Ngày nhắc nhở:</label>
		            <label id="popRemindDate" class="labelPopFB">11/11/2015 15:00</label>
	            </div>
	            <div class="Clear"></div>
	            <div class="labelWidth">
		            <label class="LabelLeftStyle">Người thực hiện:</label>
		            <label id="popStaffName"  class="labelPopFB">Mai Quốc Vương 2</label>
	            </div>
	            <div class="labelWidth">
		            <label class="LabelLeftStyle">Ngày thực hiện:</label>
		            <label id="popDoneDate" class="labelPopFB">11/11/2015 17:00</label>	
		        </div>
	            <div class="Clear"></div>
	            <div class="labelWidth">
		            <label class="LabelLeftStyle" style="margin-right: 44px">Trạng thái:</label>
		            <label id="popStatus" class="labelPopFB">Chưa hoàn thành</label>
	            </div>
	            <div class="labelWidth">
		            <label class="LabelLeftStyle" style="margin-right: 32px">Loại vấn đề:</label>
		            <label id="popTypeName" class="labelPopFB">Loại</label>
	            </div>
	            <div class="Clear"></div>
	            <div>
		            <label class="LabelLeftStyle" style="margin-right: 32px">Khách hàng:</label>
		            <label id="popCustomerName" class="labelPopFB">ABC</label>
		        </div>
	            <div class="Clear"></div>
	            <label class="LabelLeftStyle" id="lblAttach">Đính kèm:</label>
	            <label id="popAttach" class="labelPopFB">ABC</label>
	            <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnClose" onclick="return $('#popupInfoFeedback').dialog('close')" class="BtnGeneralStyle">Đóng</button>
                    <img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                </div>
                <p class="ErrorMsgStyle" id="errMsgDialog" style="display: none;"></p>
	            <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
            </div>
		</div>
 </div>
 </div>
<script type="text/javascript">
$(document).ready(function() {

});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
</style> 
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/feedback/info">Phản hồi</a></li>
		<li><span>Theo dõi khắc phục</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form" id="search-form">
					<label class="LabelStyle Label1Style"><s:text name="image_manager_unit"/> <span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect ">
						<input  type="text" class="InputTextStyle InputText1Style" style="width: 205px;" id="cbxUnit" maxlength="50" />
					</div>
					<label class="LabelStyle Label1Style">Nhân viên</label>
					<div class="BoxSelect BoxSelect12">
						<input  type="text" style="width:208px;" class="easyui-combobox" id="cbxStaff" maxlength="50" />
					</div>
				 	<label class="LabelStyle Label1Style">Loại vấn đề</label>
					<div class="BoxSelect BoxSelect2" id="subcat">
		               	<select id="typeFeedback" class="MySelectBoxClass" >  
		                   		<option value="">Tất cả</option>
	                            <s:iterator var="obj" value="lstApParam"> 
	                               <option value="<s:property value="#obj.apParamCode" />"><s:property value="#obj.apParamName" /></option>                        		
	                            </s:iterator>                               		
		               </select> 
		             </div>
		             <div class="Clear"></div>
					<label class="LabelStyle Label1Style">Trạng thái</label> 
					<div class="BoxSelect BoxSelect2">
						<select id="status" class="MySelectBoxClass">
							<option value="-1" selected="selected">Tất cả</option>
							<option value="0" >Chưa thực hiện</option>
							<option value="1" >Đã thực hiện</option>
							<option value="2" >Đã duyệt</option>
						</select>
					</div>
					<label class="LabelStyle Label1Style">Khách hàng</label>					 
					<input type="text" class="InputTextStyle InputText1Style" placeholder="Mã tên địa chỉ" tabindex="8" maxlength="50" id="customerCode" />
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Từ ngày <span class="ReqiureStyle">*</span></label>
					<input id="fDate" class="InputTextStyle InputText6Style" value="<s:property value='fromDate' />">
					<label class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle">*</span></label>
					<input id="tDate" class="InputTextStyle InputText6Style" value="<s:property value='toDate' />">
     				<div class="Clear"></div>
					<div class="BtnCSection">
						<button class="BtnGeneralStyle" id="btnSearch" onclick="return FeedBack.searchFeedback();">Tìm kiếm</button>
					</div>
					<div class="Clear"></div>
					<p class="ErrorMsgStyle SpriteErr" id="errMsgSearch" style="display: none;"></p>
					<div class="Clear"></div>
				</div>
				<div class="Clear"></div>
				<h2 class="Title2Style">Danh sách vấn đề</h2>
				<div class="SearchInSection SProduct1Form">
                    <div class="GridSection">
                		<div id="searchFeedbackResult" class="GeneralTable">
							<div class="BoxGeneralTTitle" style="width: 100%;" id="dgGridContainer">
								<table id="grid"></table>
								<div id="pager"></div>
							</div>
						</div>
		                <div class="Clear"></div>
		                <div class="BtnCenterSection">
		                	<button class="BtnGeneralStyle BtnMSection" id="btnApproved" onclick="return FeedBack.approvedFeedback(FeedbackStatus.APPROVED);" style="float:right; margin-right:20px;">Duyệt</button>
		                    <button class="BtnGeneralStyle BtnMSection" id="btnConfirm" onclick="return FeedBack.approvedFeedback(FeedbackStatus.NEW);" style="float:right; margin-right:20px;">Từ chối</button>
		                    <div class="Clear"></div>
		                </div>
	                    <div class="Clear"></div>
	                    <p id="successMsg" class="SuccessMsgStyle" style="display: none; padding-left: 10px;"></p>
	                    <br>
	                    <p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none; margin-left: 5px;"></p>
                      </div>
                </div>
				<div class="Clear"></div>
			</div>
		</div>
		<div class="Clear"></div>
	</div>
</div>
<!-- popup feedback info -->
<tiles:insertTemplate template="/WEB-INF/jsp/feedback/popupFeedbackInfo.jsp" />
<div class="Clear"></div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	//load Cay don vi cbx
	ReportUtils.loadComboTree('cbxUnit', 'shopId', $('#curShopId').val(), function(shopId) {
		var param = {};
		param.shopId = shopId;
		param.typeCurrentUser = StaffSpecType.STAFF;
		param.isChoiceAll = true;
		Utils.initStaffCbx('cbxStaff', '/commons/load-staff-inherit', param, 208, function (rec) {
		});
		FeedBack.initGridFeedback();
		
	});
	
	setDateTimePicker('fDate');	
	setDateTimePicker('tDate');
	/*ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');*/
	$('.MySelectBoxClass').css('width','206');
	$('.CustomStyleSelectBox').css('width','173');

});
</script>
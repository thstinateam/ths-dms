<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<style type="text/css">
#player_api { width:678px !important; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1">
			<a href="/videos/info"><s:text name="catalog_sales_brand_catalogy"/></a>
		</li>
		<li>
			<span><s:text name="jsp.videos.info.title"/></span>
		</li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="SearchSection GeneralSSection">
			<div class="SearchInSection SVideo1Form">
				<div class="SearchInSection">
					<label class="LabelStyle Label1Style">Mã video</label>
					<input type="text" class="InputTextStyle InputText1Style" id="mediaCode" maxlength="50" style="width:140px"/>
					<label class="LabelStyle Label1Style">Tên video</label>
					<input type="text" class="InputTextStyle InputText1Style" id="mediaName" maxlength="100"/>
					<label class="LabelStyle Label1Style">Trạng thái</label>
				    <div class="BoxSelect BoxSelect5">
				        <select class="MySelectBoxClass" id="status">
				        	<option value="-1">Tất cả</option>
				        	<option value="1" selected="selected">Hoạt động</option>					           
				            <option value="0">Tạm ngưng</option>
				        </select>
				    </div>
					<div class="BtnLSection" style="padding-left:18px;">
					    <button id="btnSearch" class="BtnGeneralStyle BtnSearch" style="margin-top:-4px;" onclick="return Videos.searchVideos();">Tìm kiếm</button>
					</div>					 
					<div class="Clear"></div>					
					<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
				</div>
				<div class="Clear"></div>
				<div class="GridSection" id="gridVideoSection">
					<div id="grid"></div>
				</div>
			</div>		
		</div>	
	</div>
</div>
<div id = "responseDiv" style="display:none;"></div>

<!-- <div id="divDisplayNone" style="display: none;"> -->
<div id="videoUploadContainerDlg" style="visibility: hidden;">
	<div id="videoUploadPopup" class="easyui-dialog" title="Upload Video"
		style="width: 475px; height: 240px; top: 120px"
		data-options="closed:true,modal:true">
		<div class="SVideo1Form">
			<div class="PopupContentMid">
<!-- 				<div class="GeneralForm ImportVideo1Form"> -->
				<div class="ImportVideo1Form">
					<form action="/videos/add-video" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
						<label class="LabelStyle Label1Style">Mã video</label> 
						<input type="text" name="mediaCode" class="InputTextStyle" maxlength="50" id="mediaCode">
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style">Tên video</label> 
						<input type="text" class="InputTextStyle" name="mediaName" maxlength="100" id="mediaName">
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style">Trạng thái</label>
						<div class="BoxSelect BoxSelect5">
							<select class="MySelectBoxClass" id="status" name="status">
								<option value="1">Hoạt động</option>
								<option value="0">Tạm ngưng</option>
							</select>
						</div>
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style">File video</label> 
						<input type="hidden" name="token" id="importTokenVal" value='<s:property value="token"/>'>
						<div class="UploadFileSection Sprite1">
							<input id="fakefilepc" type="text" class="InputTextStyle"> 
							<input id="videoFile" type="file" class="UploadFileStyle" size="1" name="videoFile"	onchange="Videos.previewUploadVideoFile(this, 'importFrm')" />
						</div>
						<div class="Clear"></div>
					</form>					
				</div>			
			</div>			
			<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" id="btnUploadVideo"
						onclick="return Videos.uploadVideoFile();">Tải lên</button>
					<button class="BtnGeneralStyle"
						onclick="$('#videoUploadPopup').window('close');">Đóng</button>
			</div>
		</div>
		<p style="display: none; margin-top: 10px; margin-left: 10px" class="ErrorMsgStyle" id="errMsgDlg" />				
		<p style="display: none; margin-top: 10px; margin-left: 10px" class="ErrorMsgStyle" id="errMsgVideo" />
		<p style="display: none; margin-top: 10px; margin-left: 10px" class="ErrorMsgStyle" id="resultMsgVideo" />
	</div>
</div>
<div style="display: none">
<div id="showVideoDlg" class="easyui-dialog" title="Mã video - Tên video" data-options="closed:true,modal:true">
<div class="SVideo1Form">
	<div class="BoxPlayer" id="boxPlayer" style="display:none; width: 639px ! important;">
		<span class="BoxMiddle" id="videoBox" >
			<div style="display: block; width: 678px !important; margin: 10px; height: 480px;" id="player"></div>
		</span>				
	</div>
	</div>
</div>
</div>

<div id="productDialogContainer" style="visibility: hidden">
	<div id="productEasyUIDialog" class="easyui-dialog" title="" data-options="closed:true,modal:true" style="width: 580px; height: 570spx">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
				<input id="mediaCodeTemp" style="display: none;">
				<input id="indexMediaTemp" style="display: none;">
			
				<label class="LabelStyle Label1Style" style="width: 40px;">Mã SP</label>
				<input id="productCodeUIDialog" maxlength="50" type="text" class="InputTextStyle InputText1Style" /> 
				<label class="LabelStyle Label2Style" style=" width: 54px;">Tên SP</label> 
				<input id="productNameUIDialog" type="text" maxlength="250" class="InputTextStyle InputText1Style" />
				<label class="LabelStyle Label3Style" style="width: 86px;">Ngành hàng</label> 
				<div class="BoxSelect BoxSelect1">
	                <select id="categoryCodeUIDialog" class="MySelectBoxClass" >
	                    <option selected="selected" value="">Tất cả</option>
	                    <s:iterator value="lstCategory" >                    	
	                    	<option value="<s:property value="productInfoCode" />"><s:property value="productInfoName" /></option>                                        	 
	                    </s:iterator>
	                </select>
	            </div>
	            
	            
	            
	            
	            <div class="Clear"></div>
	            <div class="BtnCenterSection" >
					<button class="BtnGeneralStyle BtnSearchOnDialog" id="btnSearchUIDialog">Tìm kiếm</button>
				</div>	
				
				<div class="Clear"></div>
	            <div style="margin-left: 400px; margin-right: 10px; margin-top: 5px;">
		            <span style="margin-right: 6px;">Chọn tất cả sản phẩm</span> 
					<input type="checkbox" id="chbProductAll" name="chbProductAll" onChange="return Videos.chbProductAllClick();"> 
	            </div>
				<div class="GridSection" id="grid">
					<!--Đặt Grid tại đây-->
					<table id="searchGrid" class="easyui-datagrid"></table>
					<div id="gridPager"></div>
				</div>
				<div class="BtnCenterSection">
					<button id="btnSaveProductForVideo" tabindex="16" onclick="return Videos.addProductForVideo();" class="BtnGeneralStyle">Lưu</button>
					<button class="BtnGeneralStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>				
				<p id="errMsgDialog" class="ErrorMsgStyle" style="display: none"></p>
				<p style="display: none; margin-left: 10px" class="SuccessMsgStyle" id="successMsg"></p>
				
			</div>
		</div>
	</div>
</div>


<!-- </div> -->

<s:hidden id="successUpload" value="0"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.SVideo1Form #mediaCode').focus();
	
	/* $('.MySelectBoxClass').customStyle(); */
	var iSTT = 0;
	$('#grid').datagrid({
		url:'/videos/search-videos',
		fitColumns:true,
		singleSelect:true,
		width : $(window).width() - 50, 
		height: 500,
		rownumbers:true,
		columns:[[
		          {field:'mediaCode', title:'Mã video', width:100, align:'left', sortable:false, resizable:false, formatter:function(value, row, index){
		        	  return '<a href="javascript:void(0)" onclick="return Videos.getUrlVideo(\'' + Utils.XSSEncode(value) + '\',\'' + Utils.XSSEncode(row.mediaName) +  '\')">' + Utils.XSSEncode(value) + '</a>';
		          }},
		          {field:'mediaName', title:'Tên video', width:150, align:'left', sortable:false, resizable:false, formatter:function(value, row, index){
		        	  return Utils.XSSEncode(value);
		          }},
		          {field:'status', title:'Trạng thái', width:50, align:'left', sortable:false, resizable:false, formatter:function(value, row, index){
		        	return value==1?'Hoạt động':'Tạm ngưng';  
		          }},
		          {field:'add', title:'<a href="javascript:void(0)" onclick="return Videos.openPopupUpload();"><img width="15" height="16" src="/resources/images/icon_add.png"></a>', width:10, align:'center', sortable:false, resizable:false, formatter:function(value, row, index){
		        	  return '<a href="javascript:void(0)" onclick="return Videos.showUpdateVideo(\'' + Utils.XSSEncode(row.mediaCode) + '\',\'' + Utils.XSSEncode(row.mediaName) +  '\',\'' + row.status +  '\')"><img width="15" height="16" src="/resources/images/icon-edit.png"></a>';
		          }}
		]],
		          
		view:detailview,
		detailFormatter:function(index, row){
			return '<div style="padding:2px"><table class="ddv"></table></div>';			
		},
		onExpandRow:function(index, row){		
			$('#divOverlay').show();
			var ddv = $(this).datagrid('getRowDetail', index).find('table.ddv');
			ddv.datagrid({
				url:'/videos/search-product-by-video?mediaCode=' + row.mediaCode,
				rownumbers:true,
				fitColumns:true,
				singleSelect:true,
				height:'auto',
				width:550,
				loadMsg:'',
				columns:[[
				          {field:'productCode', title:'Mã SP', width:50, align:'left', sortable:false, resizable:false, formatter:function(value, row, index){
				        	  return Utils.XSSEncode(value);
				          }},
				          {field:'productName', title:'Tên SP', width:100, align:'left', sortable:false, resizable:false, formatter:function(value, row, index){
				        	  return Utils.XSSEncode(value);				        	 	 
				          }},
// 				          {field:'add', title:'<a href="javascript:void(0)" onclick="return Videos.searchProductForVideo(\'' + Utils.XSSEncode(row.mediaCode) + '\',\'' + index + '\')"><img width="15" height="16" src="/resources/images/icon_add.png"></a>', width:10, align:'center', sortable:false, resizable:false, formatter:function(valueExpand, rowExpand, indexExpand){
// 				        		//return  			format('<a href="" onclick="change({0},{1},{3})" ></a>',10,12,20);				        		
// 				        	  return '<a href="javascript:void(0)" onclick="return Videos.deleteProductForVideo(\'' + Utils.XSSEncode(rowExpand.productCode) + '\',\'' + Utils.XSSEncode(row.mediaCode) +  '\',\'' + index +  '\');"><img width="15" height="16" src="/resources/images/icon_delete.png"></a>'; 
// 				          }}
				          {field:'add', title:'<a href="javascript:void(0)" onclick="return Videos.openProductUIDialog(\'' + Utils.XSSEncode(row.mediaCode) + '\',\'' + index + '\')"><img width="15" height="16" src="/resources/images/icon_add.png"></a>', width:10, align:'center', sortable:false, resizable:false, formatter:function(valueExpand, rowExpand, indexExpand){
				        		//return  			format('<a href="" onclick="change({0},{1},{3})" ></a>',10,12,20);				        		
				        	  return '<a href="javascript:void(0)" onclick="return Videos.deleteProductForVideo(\'' + Utils.XSSEncode(rowExpand.productCode) + '\',\'' + Utils.XSSEncode(row.mediaCode) +  '\',\'' + index +  '\');"><img width="15" height="16" src="/resources/images/icon_delete.png"></a>'; 
				          }}
				]],
				
				onResize:function(){
					$('#grid').datagrid('fixDetailRowHeight',index);
				},
				onLoadSuccess:function(){
					$('#divOverlay').hide();
					//$('.datagrid-view1 .datagrid-header-row').html('STT');
					$('.datagrid-header-rownumber').html('STT');
					setTimeout(function(){
					$('#grid').datagrid('fixDetailRowHeight',index);
				 	},0);
				}
						
			});
			 $('#dg').datagrid('fixDetailRowHeight',index);
		},
        onLoadSuccess :function(data){
	    	$('.datagrid-header-rownumber').html('STT');
	    	iSTT = 0;
	    }
		
	});	
	
	Utils.bindAutoSearch();
	Utils.bindAutoSearchPage('videoUploadPopup'); 
	$('.ContentSection .InputTextStyle').each(function(){
	    if(!$(this).is(':hidden')){
	   		$(this).bind('keyup',function(event){
	   			if(event.keyCode == keyCodes.ENTER){		   				
	   				if($('#btnSearch')!= null && $('#btnSearch').html()!= null && $('#btnSearch').html().trim().length > 0 && !$('#btnSearch').is(':hidden')){
	   					$('#btnSearch').click();
	   				}
	   				$('.BtnSearch').each(function(){	   					
	   					if(!$(this).is(':hidden')){
	   						$(this).click();
	   					}
	   				});
	   			}
	   		});    
	    }	    
	});
	
	$('.SVideo1Form .InputTextStyle ').each(function(){
	    if(!$(this).is(':hidden')){
	   		$(this).bind('keyup',function(event){
	   			if(event.keyCode == keyCodes.ENTER){		   				
	   				if($('#btnUploadVideo')!= null && $('#btnUploadVideo').html()!= null && !$('#btnUploadVideo').is(':hidden')){
	   					$('#btnUploadVideo').click();
	   				}
	   			}
	   		});    
	    }	    
	});
});
</script>
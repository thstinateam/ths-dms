<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="errorVideo" style="display: none"><s:property value="isError"/></div>
<div id="errorVideoMsg" style="display: none"><s:property value="errMsg"/></div>
<input id="tokenHiddenFiled" type="hidden" value="<s:property value="getSessionToken()"/>"  />
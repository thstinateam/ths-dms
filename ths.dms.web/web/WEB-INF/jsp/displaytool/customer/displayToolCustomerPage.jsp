<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
        	<div class="BreadcrumbSection">
                <ul class="ResetList FixFloat BreadcrumbList">
                    <li class="Sprite1"><a href="#">Thiết bị</a></li>
                    <li><span>Khách hàng mượn tủ</span></li>
                </ul>
            </div>            
            <div class="CtnOneColSection">
                <div class="ContentSection">
                	<div class="ToolBarSection">
                	<div class="SearchInSection SProduct1Form">	
                        <div class="SearchSection GeneralSSection">
                            <div class="SearchInSection">
                                <div class="ComboTreeSection">
                                    <input id="shop" type="text"  style="width:226px;" class="InputTextStyle InputText1Style"/> 
                                </div>
                                <label class="LabelStyle Label1Style">Nhân viên</label>
                                <div class="BoxSelect BoxSelect1">
                                    <select id="staffCode" name="LevelSchool" class="MySelectBoxClass" style="opacity: 0; height: 22px;">
                                        <option selected="selected" value="">Tất cả</option>
                                        <s:iterator value="lstStaff">
                                        	<option value="<s:property value="staffCode" />"><s:property value="staffCode" /> - <s:property value="staffName" /></option>                                        	 
                                        </s:iterator>
                                     </select>                                  
                                   
                                </div>
                                <label class="LabelStyle Label1Style">Tháng</label>
                                <input type="text" class="InputTextStyle InputText1Style" id="month" style="width: 120px;">                                
                                 <div class="BtnLSection" style="margin-left:15px; margin-top:-2px">
                                    <button class="BtnGeneralStyle" id="btnSearch" onclick="DisplayToolCustomer.search();">Tìm kiếm</button>
                                </div>
                                <div class="Clear"></div>
                            </div>
                            </div>
                            
                            
                        </div>
                        <div class="Clear"></div>                                           
                    </div>
                    <div class="GeneralCntSection">
                    	<div class="GeneralForm GeneralNoTPForm">
                        	<div class="Func1Section">
                        		<button class="BtnGeneralStyle" id="copyDisplayTools" onclick="DisplayToolCustomer.copyDisplayTool();">Sao chép</button>
                        		<button class="BtnGeneralStyle" onclick="return DisplayToolCustomer.importExcel();">Nhập từ Excel</button>
                        		<button class="BtnGeneralStyle" onclick="return DisplayToolCustomer.exportExcel();">Xuất Excel</button><p class="DownloadSFileStyle">
                        		<a class="Sprite1" id="downloadTemplate" href="javascript:void(0)" onclick="">Tải file excel mẫu</a></p>

                            </div>
                            <div class="Func2Section">
                            	<label class="LabelStyle Label1Style"><input id="selectAllShop"  type="checkbox">Chọn toàn bộ của NPP</label><button class="BtnGeneralStyle" onclick="DisplayToolCustomer.deleteDisplayTool();">Xóa</button>
                            </div>
                            <div class="Clear"></div>
                        </div>
                        <div class="Clear"></div>
                        <div class="Clear"></div>
                        <p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
						<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>

                    	<div class="GridSection">
                        	<div class="ResultSection" id="productToolContainer" >
								<p style="display: none" class="WarningResultStyle">Không có kết quả</p>
								<table id="grid"></table>
								<div id="pager"></div>
                        </div>                        
                    </div>
                </div>
                <div class="Clear"></div>
            </div>
            </div>
            <div id = "responseDiv" style="display:none;"></div>
            			<div id="easyuiPopup" class="easyui-dialog" title="Nhập thiết bị - Khách hàng từ excel" data-options="closed:true,modal:true" style="width:465px;height:250px;">
					        <div class="PopupContentMid">
					        	<div class="GeneralForm ImportExcel1Form">
					        		<form action="/displaytool-customer/import" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
						            	<label class="LabelStyle Label1Style">Tháng</label>
						                <input type="text" class="InputTextStyle InputText2Style" id="dateStrExcel" name="dateStrExcel"/>
						                <div class="Clear"></div>
						                <label class="LabelStyle Label1Style">File excel</label>
						                <div class="UploadFileSection Sprite1">
						                	<input id="fakefilepc" type="text" class="InputTextStyle InputText1Style">
						                    <input type="file" class="UploadFileStyle" size="1" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');" />
						                    <input type="hidden" id="isView" name="isView">
						               		<input type="hidden" name="token" value="<s:property value="token" />" id="importTokenVal"  />
						                
						                </div>
									</form>
					                <div class="Clear"></div>
					                <p class="DownloadSFileStyle"><a id="downloadTemplate" href="javascript:void(0)" onclick="" class="Sprite1">Tải file excel mẫu</a></p>
					                <div class="BtnCenterSection">
					                    <button class="BtnGeneralStyle" onclick="return DisplayToolCustomer.upload();">Tải lên</button>
					                    <button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopup').window('close');">Đóng</button>
					                </div>;
					            </div>
							</div>
							<p style="display:none;margin-top:10px;margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errExcelMsg"/>
							<p style="display:none;margin-left: 10px" id="resultExcelMsg" />
				    	</div>
<input type="hidden" value="<s:property value="yesterDay" />" id="yesterDay" />
<s:hidden id="dv" ></s:hidden>
<s:hidden id="nv" ></s:hidden>
<s:hidden id="th" ></s:hidden>
<s:hidden id="shopId" ></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	 $('.MySelectBoxClass').customStyle();
	 $('.MySelectBoxClass').css('width','300');
	 $('.CustomStyleSelectBox').css('width','268');
	 
	 DisplayToolCustomer.mapDisplayTool = new Map();
	 var shopId = '<s:property value="shopId" />';
	 ReportUtils.loadComboTree('shop', 'shopId',shopId,function(id){
		 DisplayToolCustomer.showStaffByShop(id);
	 });
	 	 
	 applyMonthPicker('month');
	 Utils.bindFormatOnTextfield('month',Utils._TF_NUMBER_CONVFACT);		
	 $('#month').val(getCurrentMonth());
	 Utils.bindAutoSearch(); 
	 
	 $('#grid').datagrid({
		url : DisplayToolCustomer.searchGridUrl('','',getCurrentMonth()),
		autoRowHeight : true,
		rownumbers : true, 
		checkOnSelect :true,
		pagination:true,
		rowNum : 10,
		fitColumns:true,
		pageList  : [10,20,30],
		scrollbarSize:0,
		width: $(window).width()-20,
		autoWidth: true,
		queryParams:{
			staffCode : '', 
			shopId: $('#shop').combotree('getValue'),
			dateStr:getCurrentMonth()
	 	},
	    columns:[[  
	        {field:'shopCode',title:'NPP',align:'left', sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	return Utils.XSSEncode(value);
	        }},
	        {field:'staffCodeName',title:'NVBH',align:'left', width:170, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	return Utils.XSSEncode(value);
	        }}, 
	        {field:'customerCodeName',title:'Khách hàng',align:'left', width:160, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	return Utils.XSSEncode(value);
	        }},  
	        {field:'toolCode', title:'Mã thiết bị', align:'left',width:40,sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	return Utils.XSSEncode(value);
	        }},
	        {field:'amount', title:'Doanh số', align:'right',width:70,sortable : false,resizable : false,formatter:CommonFormatter.numberFormatter},
	        {field:'quantity', title:'Số lượng', align:'right',width:40,sortable : false,resizable : false,formatter:CommonFormatter.numberFormatter},
	        {field:'month', title:'Tháng', align:'center',width:30,sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	return Utils.XSSEncode(value);
	        }},
	        {field:'id', checkbox: true}
	    ]],	    
	    onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');
	    	updateRownumWidthForJqGrid('');
	    	var i = 0;
	    	$('input[name="id"]').each(function(){
	    		var checkAll = $('#selectAllShop').is(':checked');
			    var temp = DisplayToolCustomer.mapDisplayTool.get($(this).val());
			    if(temp!=null||checkAll) {
			    	$(this).attr('checked','checked');
			    	$('#grid').datagrid('selectRow',i);
			    	}
			    i++;
		    });   
	    	var length = 0;
	    	$('input[name="id"]').each(function(){
	    		if($(this).is(':checked')){
	    			++length;
	    		}	    		
	    	});	    	
	    	if(data.rows.length==length && length != 0){
	    		$('.datagrid-header-check input').attr('checked',true);
	    	}else{
	    		$('.datagrid-header-check input').attr('checked',false);
	    	}
	    	 updateRownumWidthForJqGrid('.easyui-dialog');
    		 $(window).resize();
	    },
	    onCheck:function(i,r){
	    	//$('#selectAllShop').attr('checked',false);
	    	DisplayToolCustomer.mapDisplayTool.put(r.id,r);
	    },
	    onUncheck:function(i,r){
	    	$('#selectAllShop').attr('checked',false);
	    	$('.datagrid-header-check input').attr('disabled',false);
	    	DisplayToolCustomer.mapDisplayTool.remove(r.id);	    	
	    },
	    onCheckAll:function(r){
	    	//$('#selectAllShop').attr('checked',false);
	    	for(i=0;i<r.length;i++){
	    		DisplayToolCustomer.mapDisplayTool.put(r[i].id,r[i]);
	    	}
	    },
	    onUncheckAll:function(r){
	    	$('#selectAllShop').attr('checked',false);
	    	for(i=0;i<r.length;i++){
	    		DisplayToolCustomer.mapDisplayTool.remove(r[i].id);
	    	}	    	
	    }
	});	 
	$('#dv').val($('#shop').combotree('getValue'));
	$('#nv').val($('#staffCode').val().trim());
	$('#th').val($('#month').val().trim());		
	 $('#month').change(function(){
		 if($('#month').val().trim()==$('#yesterDay').val().trim()){
			 enable('copyDisplayTools');
		 }else{
			 disabled('copyDisplayTools');
		 }
	 });
	 $('#month').change();
	 $('#selectAllShop').change(function(){		 
		 if($(this).is(':checked')){
			 DisplayToolCustomer.mapDisplayTool = new Map();
			 //$('#grid').datagrid('uncheckAll');
			 //$('#selectAllShop').attr('checked', 'checked');
			 //$('#grid').datagrid('uncheckAll');
			 $('#grid').datagrid('checkAll');
			 $('.datagrid-header-check input').attr('checked',true);
			 //$('.datagrid-header-check input').attr('disabled',true);
		 }else{
			 DisplayToolCustomer.mapDisplayTool = new Map();
			 $('#grid').datagrid('uncheckAll');
			 //$('.datagrid-header-check input').attr('disabled',false);
		 }  
	 });
	 
 });
	//applyDateTimePicker("#dateStrExcel", "mm/yy", null, null, null, null, null, null, null,true,null);
	//$('#dateStrExcel').val(getCurrentMonth());
	applyMonthPicker('dateStrExcel');
	Utils.bindFormatOnTextfield('dateStrExcel',Utils._TF_NUMBER_CONVFACT);		
	$('#dateStrExcel').val(getCurrentMonth());
	$('#downloadTemplate').attr('href',excel_template_path + 'toolcustomer/ThietBi_KhachHang.xls');
	$('.easyui-dialog #downloadTemplate').attr('href',excel_template_path + 'toolcustomer/ThietBi_KhachHang.xls');
	
	 function choseFile() {
			//todo
			$('.easyui-dialog #fakefilepc').click();
			$('.easyui-dialog #importFrm').click();
			return false;
	}
	
	
 </script>
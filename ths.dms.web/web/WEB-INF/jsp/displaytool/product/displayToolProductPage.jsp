<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="#">Thiết bị</a>
		</li>
		<li><span>Sản phẩm thuộc tủ</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection ">
                <div class="ContentSection">
                	<div class="ToolBarSection">
                	   <div class="SearchInSection SProduct1Form">	
                        <div class="SearchSection GeneralSSection">
                            <div class="SearchInSection">
                                <label class="LabelStyle Label1Style">Mã tủ</label>
                                <input id="displayToolCode" type="text" class="InputTextStyle InputText2Style" style="width: 250px"/>
                                <label class="LabelStyle Label1Style">Tên tủ</label>
                                <input id="displayToolName" type="text" class="InputTextStyle InputText2Style" style="width: 250px"/>
                                
                                <div class="BtnLSection" style="margin-left:15px; margin-top:-2px">
                                    <button class="BtnGeneralStyle" id="btnSearch" onclick="return DisplayToolProduct.searchDisplaytool();">Tìm kiếm</button>
                                </div>
                                <div class="Clear"></div>
                            </div>
                        </div>
                        </div>
                        <div class="Clear"></div>
                    </div>
                    <div class="GeneralCntSection">
                    	<div class="GridSection" id="displaytoolGridTmb">
<!-- 							<table id="gridProductTool"></table> -->
<!-- 							<div id="pagerProductTool"></div> -->

							<table id="dg" data-options="  
									                url: '/displaytool-product/search-displaytool',  
									                fitColumns: true,  
									                striped: true,
									                singleSelect: true,  
											        autoRowHeight:false, 
									                pagination:true, 
									                pageSize:50,
									                scrollbarSize: 0, 
									            ">  
					    	</table> 	
						</div>
                    </div>
                </div>
                <div class="Clear"></div>
                <p style="display: none" class="ErrorMsgStyle SpriteErr" id="errMsg"></p>
                <p style="display: none;" class="SuccessMsgStyle" id="successMsg"></p>
            </div>
<s:hidden id="disToolId"/>            
<script type="text/javascript" >
$(document).ready(function() {
	DisplayToolProduct.lstNewDisplayToolId = new Array();
	DisplayToolProduct.lstNewDisplayToolCode = new Array();
	DisplayToolProduct.lstNewDisplayToolName = new Array();
	$('#displayToolCode').focus();
	Utils.bindQuicklyAutoSearch();
    $('#dg').datagrid({  
        view: detailview, 
        width : $(window).width() - 35,
        detailFormatter:function(index,row){  
            return '<div style="padding:2px"><table id="ddv-' + index + '" style="width:600px;"></table></div>';  
        },  
        columns:[[  
			{field:'displayToolCode',title:'Mã tủ',width:250,align:'left', formatter : function(value,rowData,rowIndex) {
			  	return Utils.XSSEncode(value);
			     }},  
			  {field:'displayToolName',title:'Tên tủ',width:$(window).width() - 35 - 250 - 30,align:'left', formatter : function(value,rowData,rowIndex) {
			  	return Utils.XSSEncode(value);
			     }},
			  {field:'delTool', title:'<a href="javascript:void(0)" id="btnGridAdd" onclick="DisplayToolProduct.openAddDToolDialog();"><img src="/resources/images/icon_add.png"/></a>',
			        	width:30,align:'center',
			        	formatter: function(val,row,index){
			        		return "<a href=\"javascript:void(0)\" class='btnGridDelete' onclick=\"DisplayToolProduct.deleteDisplayTool("+ row.displayToolId +");\"><img src=\"/resources/images/icon_delete.png\" width=\"15\" height=\"16\"/></a>";
			        	}
			  	},
			  {field:'displayToolId',hidden: true}
              ]],  
        onExpandRow: function(index,row){
        	$('#dg').datagrid('resize');
            $('#ddv-'+index).datagrid({  
            	url:DisplayToolProduct.getGridProductUrl(row.displayToolId),  
                singleSelect:true,  
                rownumbers:true,  
                loadMsg:'',
                fitColumns:true,  
                scrollbarSize: 0,
                columns:[[  
                    {field:'productCode',title:'Mã sản phẩm',width:230,align:'left', formatter : function(value,rowData,rowIndex) {
        	        	return Utils.XSSEncode(value);
        	        }},  
                    {field:'productName',title:'Tên sản phẩm',width:350,align:'left', formatter : function(value,rowData,rowIndex) {
        	        	return Utils.XSSEncode(value);
        	        }},
                    {field:'f1', title:'<a href="javascript:void(0)" class="btnGridDetailAdd" onclick=" return DisplayToolProduct.addProduct4Tool(\''+Utils.XSSEncode(row.displayToolCode)+'\', \''+'ddv-'+index+'\');"><img src="/resources/images/icon_add.png"></a>',width:30,align:'center', 
	                    	formatter : function(val,__row,idx) {
	                    		return '<a href=\"javascript:void(0)\" class="btnGridDetailDelete" onclick=\"DisplayToolProduct.deleteProductOfDisplayTool('+ row.displayToolId +','+ __row.id +', \''+'ddv-'+index+'\');\"><img src=\"/resources/images/icon_delete.png\"></a>';
	                    	}
                    	},
                    {field:'id',hidden: true}
                ]],  
                onResize:function(){  
                    $('#dg').datagrid('fixDetailRowHeight',index);
                },  
                onLoadSuccess:function(){  
                	$('.datagrid-header-rownumber').html('STT');
                	var i = CommonFormatter.checkPermissionHideGridButton("btnGridDetailAdd", true);
              	  	var i1 = CommonFormatter.checkPermissionHideGridButton("btnGridDetailDelete", true);
              	  	if (i + i1 == 2) {
              	  		$('#ddv-'+index).datagrid("hideColumn", "f1");
              	  	}
                    setTimeout(function(){  
                        $('#dg').datagrid('fixDetailRowHeight',index);  
                    },0);
                    $('#dg').datagrid('resize');
                }  
            });
            $('#dg').datagrid('fixDetailRowHeight',index);
            $('#dg').datagrid('resize');
        },
        onLoadSuccess: function(data) {
        	var i = CommonFormatter.checkPermissionHideGridButton("btnGridAdd", false, "displaytoolGridTmb");
      	  	var i1 = CommonFormatter.checkPermissionHideGridButton("btnGridDelete", true, "displaytoolGridTmb");
      	  	if (i + i1 == 2) {
      	  		$("#dg").datagrid("hideColumn", "delTool");
      	  	}
        }
    });  
    
    $('.BtnLSection').bind('keyup',function(event){
        if(event.keyCode == keyCodes.ENTER){
        	DisplayToolProduct.searchDisplaytool();
        }
	});
});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">      
     <li class="Sprite1"><a href="/working-date-plan/info#"><s:text name="organization_system"/></a></li>  
        <li><span><s:text name="work.plan.menu" /></span></li>
    </ul>
</div>
<style type="text/css">
	.Label1ExStyle {width:400px !important; color:#215ea2; padding-left: 40%;}
</style>
<div class="CtnOneColSection">
       <div class="ContentSection">
       	<div class="ToolBarSection">
	       	<div class="SearchSection GeneralSSection">
		       	<div class="SearchInSection SProduct1Form">
		       		<label class="LabelStyle Label1Style"><s:text name="work.date.don.vi" /></label>
					<input id="txtShopCode"  type="text"  class="InputTextStyle InputText1Style" maxlength="50" value="<s:property value='shopCodeStr' />"/>	
		       		<!--                     <button id="btnSearch" class="BtnGeneralStyle" style="margin-left: 10px;margin-top: -1px;padding-top: 1px;" onclick="">Xem</button> -->
		       		<div class="Clear"></div>
		       		<label id="lblLoadFirst" class="Label1ExStyle"><b><s:text name="work.date.first.mess" /></b></label>
		       		<p id="msgNote" class="ErrorMsgStyle" style="display: none"></p>
		       		<p id="errMsgSearch" class="ErrorMsgStyle" style="display: none"></p>
					
		       	</div>
		     </div>
             <div id="idDivWorkingDate" class="SearchSection GeneralSSection" style="display: none">
               	<div class="TabSection">
                       <ul class="ResetList TabSectionList">                           
                           <li onclick="WorkingDatePlan.showYearTab();"><a id="viewByYear" href="javascript:void(0);" class="Sprite1 Active"><span class="Sprite1"><s:text name="work.date.for.year" /></span></a></li>
                           <li onclick="WorkingDatePlan.showMonthTab();"><a id="viewByMonth" href="javascript:void(0);" class="Sprite1"><span class="Sprite1"><s:text name="work.date.for.month" /></span></a></li>
                       </ul>
                       <div class="Clear"></div>
                   </div>
               	<div id="lbhtab1" class="SearchInSection SProduct1Form">
                   	<div id="viewYear" class="CalendarYearBox">
                           <div class="BtnCenterSection">
                               <div class="BoxSelect BoxSelect1">
                               		<s:select id="currentYear" name="currentYear" cssClass="MySelectBoxClass" list="lstYears" listKey="name" listValue="value" onchange="WorkingDatePlan.currentYearChanged();"></s:select>                                   
                               </div>
                               <button id="btnThuaKe" class="BtnGeneralStyle cmsiscontrol" style="margin-top: -1px;padding-top: 1px;" onclick="return WorkingDatePlan.inheritWorkingDate();"><s:text name="work.date.inherit" /></button>
                               	<button id="btnThietLap" class="BtnGeneralStyle cmsiscontrol" style="margin-top: -1px;margin-right: 5%;padding-top: 1px;" onclick="WorkingDatePlan.viewDialogWorking()"><s:text name="work.date.institute" /></button>
                               <div class="Clear"></div>
                           </div>
                           <!-- <div id="btnDialogSaleDay" class="BtnCenterSection" style="float: right;">
				                      <button class="BtnGeneralStyle" onclick="WorkingDatePlan.viewDialogWorking()">Thiết lập ngày làm việc</button>
				                  </div> -->

                           <ul class="ResetList CYList FixFloat">                               
                                <li id="m1" class="FirstItem"></li>                                            
                               	<li id="m2"></li>
                               	<li id="m3"></li>
                               	<li id="m4" class="EndItem"></li>
                               	<div class="Clear"></div>
                               	<li id="m5" class="FirstItem"></li>
                               	<li id="m6"></li>
                               	<li id="m7"></li>
                               	<li id="m8" class="EndItem"></li>
                               	<div class="Clear"></div>
                               	<li id="m9" class="FirstItem"></li>
                               	<li id="m10"></li>
                               	<li id="m11"></li>
                               	<li id="m12" class="EndItem"></li>
                               	<div class="Clear"></div>
                           </ul>
                           <div class="Clear"></div>
                       </div>
                       <div id="viewMonth" class="CalendarMonthBox" style="display: none">
                        	<div class="BtnCenterSection">
                                <div class="BoxSelect BoxSelect1">
                                    <s:select id="currentYearMonthView" name="currentYear" cssClass="MySelectBoxClass" list="lstYears" listKey="name" listValue="value" onchange="WorkingDatePlan.currentMonthChanged();"></s:select>
                                </div>
                                <div class="BoxSelect BoxSelect1 BoxSelect2">
                                    <s:select cssClass="MySelectBoxClass" id="currentMonth" name ="currentMonth" 
                                    list="lstBeanMonth" listKey="content1" listValue="content2" onchange="WorkingDatePlan.currentMonthChanged();"></s:select>
                                </div>
                                <button id="btnThuaKe" class="BtnGeneralStyle cmsiscontrol" style="margin-top: -1px;padding-top: 1px;" onclick="return WorkingDatePlan.inheritWorkingDate();"><s:text name="work.date.inherit" /></button>
                               	<button id="btnThietLap" class="BtnGeneralStyle cmsiscontrol" style="margin-top: -1px;margin-right: 5%;padding-top: 1px;" onclick="WorkingDatePlan.viewDialogWorking()"><s:text name="work.date.institute" /></button>
                                <div class="Clear"></div>
                            </div>
                            <p id="numWorkDayByMonth" class="Warning2Style"></p>
                            <div id="monthContent" class="GeneralTable Table22Section">                                        
                            </div>
                        </div>
                   </div>
               </div>
               <div class="Clear"></div>  
<!--                <p id="msgNote" class="ErrorMsgStyle" style="display: none"></p>              -->
               <p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
				<div id="responseDiv" style="display: none"></div>
	             <div class="Clear"></div>
				 <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
           </div>
       </div>
    <div class="Clear"></div>
    <input type="hidden" id="parentCodeNameHid" />
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/general/dialogSearchShopTree.jsp" />
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/working-plan/working-date/popupWorkingDate.jsp" />
</div>
<div id="dataBox" style="display: none" class="SearchInSection SProduct1Form">
<div class="BoxEditState">
 	<div class="BoxEditStateBtm">
     	<div class="BoxEditStateLoop">
     		<a class="Sprite1 BtnClose" href="javascript:void(0)" onclick="$('#dataBox').hide();"><span class="HideText"><s:text name="jsp.common.dong"/></span></a>
     		<div class="Clear"></div>
         	<label class="LabelStyle Label1Style"><s:text name="jsp.common.status"/></label>
         	<div class="BoxSelect BoxSelect3">
                 <s:select id="dateType" name="dateType" cssClass="MySelectBoxClass" list="lstBeanDateType" listKey="content1" listValue="content2" onchange="dateTypeChanged();"></s:select>
             </div>
             <div class="Clear"></div>
             <label class="LabelStyle Label1Style"><s:text name="work.date.reason" /></label>
			 <textarea class="InputTextStyle InputArea1Style" rows="3" cols="50" id="reasonGroup" style="width: 193px;margin-left: 0px;font: arial;font-size: 11px" maxlength="1000"></textarea>
             <div class="Clear"></div>
             <p id="errMsg" class="ReqiureStyle" style="display: none"></p>
             <div class="Clear"></div>
             <div id="divBtnUpdateSaleDay" class="BtnCenterSection">
                 <button id="btnUpdateSaleDay" class="BtnGeneralStyle cmsiscontrol" onclick="WorkingDatePlan.saveDateInfo()"><s:text name="work.date.update" /></button>
             </div>
             <div class="Clear"></div>  
             <s:hidden id="dateValue"></s:hidden>           
     	</div>
     </div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function(){	
	$('#layoutContent').removeClass('Content');
	$('#layoutContent').addClass('MainContent');
	shopChanged();
	WorkingDatePlan.loadCalendar();
	dateTypeChanged();	
	var txt = 'txtShopCode';
	$('#txtShopCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			$('#common-dialog-search-tree-in-grid-choose-single').dialog({
				title: "Tìm kiếm Đơn vị",
				closed: false,
		        cache: false, 
		        modal: true,
		        width: 680,
		        height: 550,
		        onOpen: function(){
		        	$('#txtShopCodeDlg').val("");
		        	$('#txtShopNameDlg').val("");
		        	//Tao cay don vi
		        	$('#commonDialogTreeShopG').treegrid({  
		        		data: StockManager._arrRreeShopTocken,
		        		url : '/cms/searchTreeShop',
		    		    width: 650,
		    		    height: 325,
		    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
		    			fitColumns : true,
		    			checkOnSelect: false,
		    			selectOnCheck: false,
		    		    rownumbers : true,
		    	        idField: 'id',
		    	        treeField: 'code',
		    		    columns:[[
		    		        {field:'code',title:work_date_unit_code,resizable:false, width:200, align:'left', formatter:function(value, row, index){
		    		        	return Utils.XSSEncode(value);
		    		        }},
		    		        {field:'name',title:work_date_unit_name,resizable:false, width:200, align:'left', formatter:function(value, row, index){
		    		        	return Utils.XSSEncode(value);
		    		        }},
		    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
		    		        	return '<a href="javascript:void(0)" onclick="WorkingDatePlan.fillCodeForInputTextByDialogSearch2(\''+Utils.XSSEncode(txt)+'\', ' + row.id +', \''+Utils.XSSEncode(row.code)+'\', \''+Utils.XSSEncode(row.name)+'\' ,\''+Utils.XSSEncode(Utils._isShowInputTextByDlSearch2.code)+'\');">'+work_date_chon+'</a>';   
		    		        }}
		    		    ]],
		    	        onLoadSuccess :function(data){
		    				$('.datagrid-header-rownumber').html('STT');
		    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
		    	        }
		    		});
		        	$('#txtShopCodeDlg').focus();
		        },
		        onClose:function() {
		        	
		        }
			});
		}
	}).bind('change',function(){
		shopChanged();
	}).bind('focus', function(event) {
		$('#common-dialog-search-tree-in-grid-choose-single').dialog({
			title: "Tìm kiếm Đơn vị",
			closed: false,
	        cache: false, 
	        modal: true,
	        width: 680,
	        height: 550,
	        onOpen: function(){
	        	$('#txtShopCodeDlg').val("");
	        	$('#txtShopNameDlg').val("");
	        	//Tao cay don vi
	        	$('#commonDialogTreeShopG').treegrid({  
	        		data: StockManager._arrRreeShopTocken,
	        		url : '/cms/searchTreeShop',
	    		    width: 650,
	    		    height: 325,
	    		    queryParams: {code: $('#txtShopCodeDlg').val().trim(), name: $('#txtShopNameDlg').val().trim()},
	    			fitColumns : true,
	    			checkOnSelect: false,
	    			selectOnCheck: false,
	    		    rownumbers : true,
	    	        idField: 'id',
	    	        treeField: 'code',
	    		    columns:[[
	    		        {field:'code',title:work_date_unit_code,resizable:false, width:200, align:'left', formatter:function(value, row, index){
	    		        	return Utils.XSSEncode(value);
	    		        }},
	    		        {field:'name',title:work_date_unit_name,resizable:false, width:200, align:'left', formatter:function(value, row, index){
	    		        	return Utils.XSSEncode(value);
	    		        }},
	    		        {field:'choose',title:'',resizable:false, width:60, align:'center', fixed:true, formatter:function(value, row, index){
	    		        	return '<a href="javascript:void(0)" onclick="WorkingDatePlan.fillCodeForInputTextByDialogSearch2(\''+Utils.XSSEncode(txt)+'\', ' + row.id +', \''+Utils.XSSEncode(row.code)+'\', \''+Utils.XSSEncode(row.name)+'\' ,\''+Utils.XSSEncode(Utils._isShowInputTextByDlSearch2.code)+'\');">'+work_date_chon+'</a>';   
	    		        }}
	    		    ]],
	    	        onLoadSuccess :function(data){
	    				$('.datagrid-header-rownumber').html('STT');
	    				Utils.updateRownumWidthAndHeightForDataGrid('commonDialogTreeShopG');
	    	        }
	    		});
	        	$('#txtShopCodeDlg').focus();
	        },
	        onClose:function() {
	        	
	        }
		});
	});
});
function dateTypeChanged(){
	if($('#dateType').val() == -2){
		setSelectBoxValue('reasonGroup');
		setTextboxValue('reasonGroup','');
		$('#reasonGroup').prop('disabled','disabled');
	} else if($('#dateType').val() == 0){
		setSelectBoxValue('reasonGroup');
		setTextboxValue('reasonGroup','');
		$('#reasonGroup').prop('disabled','disabled');
	} else if($('#dateType').val() == 1){
		setTextboxValue('reasonGroup','');
		$('#reasonGroup').removeAttr('disabled');
	} 
}
function shopChanged(){
	var shopCode = $('#txtShopCode').val();
	if(!Utils.isEmpty(shopCode)){
		$('#lblLoadFirst').hide();
		$('#idDivWorkingDate').show();
	}else{
		$('#lblLoadFirst').show();
		$('#idDivWorkingDate').hide();
	}
}
</script>
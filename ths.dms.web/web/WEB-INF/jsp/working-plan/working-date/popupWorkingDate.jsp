<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style>
<!--
.canhtrenduoi{
	margin: 10px 0px;
	padding-left: 15px;
}
.canhtrai{
	margin: 0px 30px;
}
-->
</style>
<div id="searchStyle1EasyUIDialogDiv" style="display: none;">
	<div id="popupWorkingDate" class="easyui-dialog" title="<s:text name="work.date.thiet.lap.ngay.lam.viec"/>" style="width:550px;height:220px;" data-options="closed:true,modal:true">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form">
        		<div class="canhtrenduoi">
					<label class="LabelStyle"><s:text name="work.date.thoi.gian.thiet.lap"/></label>
					<input type="text" class="InputTextStyle InputText1Style" id="fromDate"	maxlength="10" />
					<label class="LabelStyle Label2Style Label2PopupStyle"><s:text name="work.date.den"/></label>
					<input type="text" class="InputTextStyle InputText1Style" id="toDate"	maxlength="10" />
				</div>
				<div class="Clear"></div>
                <div class="canhtrenduoi">
	                <input id ="copyParentDl" name="checkCreate" type="radio" value="1" ></input>
					<label style="color: #215ea2;" for="copyParentDl"><s:text name="work.date.copy.lich.tu.cha"/></label>
				</div>
               	<div class="Clear"></div>
               	<!-- <fieldset >
					<legend>
						<input id ="createDl" name="checkCreate" type="radio" value="2" ></input>
						<label style="color: #215ea2;" for="createDl">Thiết lập riêng:</label>
					</legend>  -->
					<div class="canhtrenduoi">
						<input id ="createDl" name="checkCreate" type="radio" value="2" ></input>
						<label style="color: #215ea2;" for="createDl"><s:text name="work.date.thiet.lap.rieng"/></label>
					</div>
					<div class="Clear"></div>
					<div class="canhtrenduoi canhtrai">
						<label style="color: #215ea2; float: left; margin-right:10px"><s:text name="work.date.chon.ngay.nghi.trong.tuan"/></label>
		                <!-- <div id="valueDefaultWorkDateDiv" class="BoxSelect BoxSelect6">
		                    <select id="valueDefaultWorkDate" class="MySelectBoxClass">
		                        <option value="0">Thứ hai</option>
		                        <option value="1">Thứ ba</option>
		                        <option value="2">Thứ tư</option>
		                        <option value="3">Thứ năm</option>
		                        <option value="4">Thứ sáu</option>
		                        <option value="5">Thứ bảy</option>
		                        <option value="6">Chủ nhật</option>
		                    </select>
		                </div> -->
                        <div class="BoxSelect BoxSelect2" >
                            <select id="valueDefaultWorkDate" class="MySelectBoxClassNo" multiple="multiple">
                            </select>
                       </div>
		            </div>
	                <div class="Clear"></div>
	                <div class="canhtrenduoi canhtrai">
		                <label style="color: #215ea2;"><s:text name="work.date.chon.ngay.nghi.trong.nam"/></label>
		                <!-- <div class="GeneralCntSection"> -->
							<div class="GridSection" id="gridContainer">
								<table id="gridChooseWorking" ></table>
								<!-- <table id="gridChooseWorking" class="easyui-datagrid"></table> -->
							</div>
							<div class="Clear"></div>
						<!-- </div> -->
					</div>
				<!-- </fieldset>  -->
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSaveAttr" onclick="return WorkingDatePlan.saveWorkingDate();" class="BtnGeneralStyle"><s:text name="jsp.common.capnhat"/></button>
                    <button onclick="$('.easyui-dialog').dialog('close');" class="BtnGeneralStyle" ><s:text name="jsp.common.bo.qua"/></button>
                </div>
                <input type="hidden" id="attributeIdHid" value="0" />
                <input type="hidden" id="shopParent"  />
            </div>
            <div class="Clear"></div>
            <p id="errMsg" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
            <p id="successMsg" style="display: none;" class="SuccessMsgStyle"></p>
		</div>
    </div>
 </div>   

<script type="text/javascript">
$(document).ready(function() {
	//applyMonthPicker('fromDate');
	applyMonthPickerDialog('fromDate');
	applyMonthPickerDialog('toDate', 1);
	//$('#fromDate').val(getCurrentMonth() +1);
	//$('#fromDate').val(getCurrentMonthAddOne());
	//applyMonthPicker('toDate');
	//$('#toDate').val(getCurrentMonth() +1);
	//$('#toDate').val(getCurrentMonthAddOneNextYear());
        //Utils.bindAutoSearchForEasyUI();
});

</script>

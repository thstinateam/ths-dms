<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt><label class="LabelStyle Label1Style">Mã NPP <span class="ReqiureStyle">(*)</span></label></dt>
        <dd>
        	<s:if test="Npp=='NPP'">
        		<input id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
        	</s:if>
        	<s:else>
        		<input id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
        	</s:else>
        </dd>
        
        <dt><label class="LabelStyle Label1Style">Mã CT<span class="ReqiureStyle">(*)</span></label></dt>
		<dd><input type="text" id="promotionCode" style="width:216px;" class="InputTextStyle InputText1Style" /></dd>
		
		<dt><label class="LabelStyle Label1Style">Ngành hàng<span class="ReqiureStyle">(*)</span></label></dt>
		<dd><input type="text" id="categoryCode" style="width:216px;" class="InputTextStyle InputText1Style" /></dd>
		
		<dt><label class="LabelStyle Label1Style">Hạn mức</label></dt>
		<dd><input type="text" id="dbLevel" style="width:216px;" disabled="disabled" class="InputTextStyle InputText1Style" value="<s:property value='hanMucStr' />" /></dd>
        
        <dt><label class="LabelStyle Label1Style">Từ ngày<span class="ReqiureStyle">(*)</span></label></dt>
        <dd><input id="fDate" style="width: 196px;" class="InputTextStyle InputText1Style"> </dd>
        
        <dt><label class="LabelStyle Label1Style">Đến ngày<span class="ReqiureStyle">(*)</span></label></dt>
        <dd><input id="tDate" style="width: 196px;" class="InputTextStyle InputText1Style"></dd>
		
		<dt class="LabelTMStyle"><label class="LabelStyle Label1Style">Kiểu File</label></dt>
        <dd>
            <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
            <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
        </dd>
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="DailyFollowReport.exportVNM10_1_17();">Xuất báo cáo</button>
	</div>	
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<input type="hidden" id="hideMSG" value="1" />

<script type="text/javascript">
$(document).ready(function(){
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.loadComboTree2('shop','shopId',$('#curShopId').val(),function(node){
		$(".ErrorMsgStyle").hide();
		if (node.isLevel != 5) {
			if ($("#hideMSG").val().trim() == 0) {
				$("#errMsg").html("Đơn vị chọn không phải là một NPP cụ thể").show();
			} else {
				$("#hideMSG").val(0);
			}
			$("#isNPP").val(0);
		} else {
			$("#isNPP").val(1);
		}
	});
	$(".ErrorMsgStyle").hide();
	
	$('#categoryCode').bind('keyup', function(event){
		if (event.keyCode == keyCode_F9) {
			showDialogSearchCat();
		}
	});
	
	ReportUtils.setCurrentDateForCalendar('fDate', '01');
	ReportUtils.setCurrentDateForCalendar('tDate');
});

function showDialogSearchCat() {
	var html = '<div id="common-dialog-search-2-textbox" class="easyui-dialog">';
	html += '<div class="PopupContentMid">';
	html += '<div class="GeneralForm Search1Form" >';
	html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Mã ngành hàng</label>';
	html += '<input id="staffCode" maxlength="50" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
	html += '<label class="LabelStyle Label1Style" style=" width: 100px;">Tên ngành hàng</label>';
	html += '<input id="staffName" maxlength="250" type="text" style="width: 145px;" class="InputTextStyle InputText1Style"/>';
	html += '<div class="Clear"></div>'; 
	html += '<div class="BtnCenterSection">';
	html += '<button class="BtnGeneralStyle BtnGeneralMStyle" tabindex="3" id="common-dialog-button-search">Tìm kiếm</button>';
	html += '</div>';
	html += '<div class="Clear"></div>';
	html += '<div class="GridSection" id="common-dialog-grid-container">';
	html += '<table id="common-dialog-grid-search"></table>';
	html += '</div>';
	html += '<div class="BtnCenterSection">';
	html += '<button id ="common-dialog-button-choose" class="BtnGeneralStyle BtnGeneralMStyle">Chọn</button>';
	html += '<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$(\'#common-dialog-search-2-textbox\').dialog(\'close\');">Đóng</button>';
	html += '</div>';
	html += '<p id="dialog-search-error" style="display: none;" class="ErrorMsgStyle"></p>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	//style="display: none;"
	$('body').append(html);
	
	$('#common-dialog-search-2-textbox').dialog({
		 title: 'CHỌN NGÀNH HÀNG',
		 width: 580, 
		 height: 'auto',
		 closed: false,
		 cache: false,
		 modal: true,
		 onOpen: function() {
			 $('#common-dialog-search-2-textbox #common-dialog-button-search').unbind('click');
			 $('#common-dialog-search-2-textbox #common-dialog-button-search').bind('click', function() {
				 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
				 $('#common-dialog-grid-search').datagrid('reload');
			 });
			 $('#common-dialog-search-2-textbox input').unbind('keyup');
			 $('#common-dialog-search-2-textbox input').bind('keyup', function(e) {	
				 if(e.keyCode == keyCodes.ENTER) {
					 $('#common-dialog-search-2-textbox #common-dialog-button-search').click();
				 }
			 });
			 $('#common-dialog-search-2-textbox #common-dialog-button-choose').bind('click', function() {
				 if(ReportUtils.lstStaffId.size() == 0) {
					 $('#common-dialog-search-2-textbox #dialog-search-error').html('Bạn chưa chọn ngành hàng!').show();
				 }else{
					 $('#common-dialog-search-2-textbox #dialog-search-error').html('').hide();
					 var listStaffStr = ReportUtils.lstStaffId.valArray[0].productInfoCode;
					 for(var i = 1; i < ReportUtils.lstStaffId.valArray.length; i++) {
						 listStaffStr += ',' + ReportUtils.lstStaffId.valArray[i].productInfoCode;
					 }
					 $('.ModuleList3Form #categoryCode').val(listStaffStr);
					 $('.ModuleList3Form #categoryCode').focus();
					 $('#common-dialog-search-2-textbox').dialog('close');
				 }
			 });
			 ReportUtils.lstStaffId = new Map();
			 $('#common-dialog-grid-search').datagrid({
				 url : '/commons/category/search',
				 autoRowHeight : true,
				 rownumbers : true, 
				 pagination:true,
				 rowNum : 10,
				 pageSize:10,
				 scrollbarSize : 0,
				 pageNumber:1,
				 queryParams:{
					 page:1
				 },
				 onBeforeLoad: function(param) {
					 var code = $('#common-dialog-search-2-textbox #staffCode').val().trim();
					 var name = $('#common-dialog-search-2-textbox #staffName').val().trim();
					 param.code = code;
					 param.name = name;
					 param = $.param(param, true);
				 },
				 onCheck : function (index,row) {
					 ReportUtils.lstStaffId.put(row.id, row);
				 },
				 onUncheck : function (index,row) { 
					 ReportUtils.lstStaffId.remove(row.id);
				 },
				 onCheckAll : function(rows) {
					 for(var i = 0; i < rows.length; i++) {
						 var row = rows[i];
						 ReportUtils.lstStaffId.put(row.id, row);
					 }
				 },
				 onUncheckAll : function(rows) {
					 for(var i = 0; i < rows.length; i++) {
						 var row = rows[i];
						 ReportUtils.lstStaffId.remove(row.id);
					 }
				 },
				 fitColumns:true,
				 width : ($('#common-dialog-search-2-textbox').width() - 40),
				 columns:[[
				 	{field: 'productInfoCode', title: 'Mã ngành hàng', align:'left', width: 40, sortable : false, resizable : false, formatter: function(value, row, index) {
				 		return Utils.XSSEncode(value);         
				 	}},
				 	{field: 'productInfoName', title: 'Tên ngành hàng', align:'left', width: 40, sortable : false, resizable : false, formatter: function(value, row, index) {
				 		return Utils.XSSEncode(value);         
				 	}},
				 	{field : 'choose', checkbox : true, align: 'center', width: 20, sortable : false, resizable : false}
				 ]],
				 onLoadSuccess :function(data){							 
					 if(($.isArray(data) && data.length != 0) || (data.rows != null && data.rows != undefined && $.isArray(data.rows) && data.rows.length != 0)) {
						 var rows = (data.rows == null || data.rows == undefined) ? data : data.rows;
						 var isCheckAll=true;
						 for(var i = 0; i < rows.length; i++) {
							 if(ReportUtils.lstStaffId.get(rows[i].id)) {
								 $('#common-dialog-grid-search').datagrid('checkRow', i);
							 } else {
								 isCheckAll = false;
							 }
						 }
						 
						 if(isCheckAll) {
							 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', 'checked');
						 }else{
							 $('#common-dialog-search-2-textbox .datagrid-header-row input[type=checkbox]').attr('checked', false);
						 }
					 }
					 $('#common-dialog-search-2-textbox .datagrid-header-rownumber').html('STT');
					 var dHeight = $('#common-dialog-search-2-textbox').height();
					 var wHeight = $(window).height();
					 var top = (wHeight - dHeight) / 2 > 180 ? (wHeight - dHeight) / 2 - 180 : (wHeight - dHeight) / 2;
					 $('#common-dialog-search-2-textbox').dialog('move', {top : top});
					 setTimeout(function(){
			    			CommonSearchEasyUI.fitEasyDialog();
			    		},500);
				 }
			 });
		 },
		 onClose: function() {
			 ReportUtils.lstStaffId = null;
			 $('#common-dialog-search-2-textbox').remove();
			 $('.ModuleList3Form #categoryCode').focus();
		 }
	});
}
</script>
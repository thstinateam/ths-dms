<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>


<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
		<s:if test="%{NPP =='NPP'}">
			<input type="text" id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
		</s:if><s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVGH (F9)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		
		<dt class="LabelTMStyle">
                     <label class="LabelStyle Label1Style">Kiểu File
                     </label>
              </dt>
              <dd>
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
              </dd>
	</dl>
	<!-- me -->
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="DailyFollowReport.reportBKPTHNVGH();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>           
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
isOpenJAlert = false;
isFromBKPTHNVGH = true;
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	$('#staffCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			if($('#isNPP').val() == "false"){
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
			}else{
				$('#errMsg').html('').hide();
				var arrParam = new Array();
				var param = {};
				param.name = "shopId";
				param.value = $('#shopId').val();
				arrParam.push(param);
				CommonSearch.searchTransferStaffOnDialog(function(data) {
					$('#staffCode').val(data.code);
				}, arrParam);
			}
		}
	});
	$(document).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13' && !isOpenJAlert && isFromBKPTHNVGH){
			DailyFollowReport.reportBKPTHNVGH();
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate', '01');
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
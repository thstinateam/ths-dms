<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVGH(F9)
			</label>
		</dt>
		<dd>
			<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Kiểu file
			</label>
		</dt>
		<dd>
            <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
            <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
        </dd>
	</dl>
<!-- me -->
   	<%-- <label class="LabelStyle Label1Style">Đơn vị</label>                
   	<div class="BoxSelect BoxSelect2">
       <div class="Field2">
			<input id="shop" style="width:200px;">
			<span class="RequireStyle">(*)</span>
     	</div>
 	</div>
	<label class="LabelStyle Label2Style">NVGH(F9)</label>	                
 	<input id="staffCode" type="text" class="InputTextStyle InputText1Style" style="width:190px;"/>              
 	<div class="Clear"></div>
 	<label class="LabelStyle Label1Style">Từ ngày</label>
 	<div class="Field2">
 		<input id="fDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
 		<span class="RequireStyle">(*)</span>
 	</div>
 	<label class="LabelStyle Label2Style">Đến ngày</label>
 	<div class="Field2">
  		<input id="tDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
  		<span class="RequireStyle">(*)</span>
 	</div> --%>
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="DailyFollowReport.exportPGNTTG();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(shopId){
		$('#staffCode').val('');
	});
	$('#staffCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			var arrParam = new Array();
			var param = {};
			param.name = "shopId";
			param.value = $('#shop').combotree('getValue');
			arrParam.push(param);
			CommonSearch.searchTransferStaffOnDialog(function(data) {
				$('#staffCode').val(data.code);
			}, arrParam);
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate', '01');
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
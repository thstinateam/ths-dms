<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
		</s:if><s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVBH(F9)</label>
		</dt>
		<dd>
			<input type="text" id="staffSaleCode" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVGH(F9)
			</label>
		</dt>
		<dd>
			<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">Số đơn hàng
			</label>
		</dt>
		<dd>
			<input type="text" id="saleOrderNumber" class="InputTextStyle InputText1Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style" style="padding: 6px 0 0">Kiểu File </label>
		</dt>
		<dd>
			<input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF 
			<input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
			<input type="radio" name="formatType" id="formatTypePdfExcel" value="PDFXLS">PDF & XLS
		</dd>
	</dl>
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="DailyFollowReport.exportPXHTNVBH();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	$('.MySelectBoxClass').customStyle();
	$('.CustomStyleSelectBox').css('width','192');
	$('.MySelectBoxClass').css('width', '225');
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(shopId){
		$('#staffCode').val('');
		$('#staffSaleCode').val('');
	});
	$('#staffSaleCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			var t = $('#shop').combotree('tree');
			var node = t.tree('find', $('#shop').combotree('getValue'));
			if (node == undefined || node == null || node.isLevel == undefined || node.isLevel == null || node.isLevel != 5) {
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
				return;
			}
			VCommonJS.showDialogSearch2WithCheckbox({
				params : {
					shopId : $('#shop').combotree('getValue'),
					objectType : StaffRoleType.NVBHANDNVVS,
					//arrIdStr : shopId.toString();
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Nhân viên'},
			        {id:'name', maxlength:250, label:'Tên Nhân viên'},
			    ],
			    chooseCallback : function(listObj) {
			        //console.log(listObj);
			        if (listObj != undefined && listObj != null && listObj.length > 0) {
			        	var staffCode = listObj[0].staffCode.trim();
			        	for (var i = 1; i < listObj.length; i++) {
			        		staffCode = staffCode.trim() + ', ' + listObj[i].staffCode.trim();
			        	}
			        	$('#staffSaleCode').val(staffCode.trim());
			        } else {
			        	$('#staffSaleCode').val("");
			        }
			        $('#common-dialog-search-2-textbox').dialog("close");
			    },
			    url : '/commons/search-staff-show-list',
			    columns : [[
			        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
			    ]]
			});
		}
	});
	$('#staffCode').bind('keyup', function(event) {
		if(event.keyCode == keyCode_F9){
			if (event.keyCode == keyCode_F9) {
				var t = $('#shop').combotree('tree');
				var node = t.tree('find', $('#shop').combotree('getValue'));
				if (node == undefined || node == null || node.isLevel == undefined || node.isLevel == null || node.isLevel != 5) {
					$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
					return;
				}
				var txt = "staffCode";
				VCommonJS.showDialogSearch2({
					params : {
						shopId : $('#shop').combotree('getValue'),
						objectType : StaffRoleType.NVGH,
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã Nhân viên'},
				        {id:'name', maxlength:250, label:'Tên Nhân viên'},
				    ],
				    url : '/commons/search-staff-show-list',
				    columns : [[
				        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.id +', \''+Utils.XSSEncode(row.staffCode)+'\', \''+Utils.XSSEncode(row.staffName)+'\' ,'+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
				        }}
				    ]]
				});
			}
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$('.combo-arrow').css("width","200px");
	$('.combo-arrow').click(function(){
		$('.combo-p').css("width","230px");
		$('.combo-p .panel-body').css("width","225px");
	});
});
</script>
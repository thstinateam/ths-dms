<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="#">Báo cáo</a></li>		
	</ul>
</div>
<div class="CtnTwoColsSection">
	<div class="SidebarSection">
		<div class="SidebarInSection">
			<div class="ReportTreeSection" style="padding:10px 5px 0px">
				<div id="treeContainer" class="ScrollSection" style="min-height:471px;max-height:726px;overflow-y:auto">
					<div id="tree"></div>
				</div>
			</div>
		</div>	
	</div>
	<div class="ContentSection">
		<div class="ReportCtnSection">
			<h2 class="Title2Style" id="title"></h2>
			<input id="reportType" type="hidden" value="" />
			<div class="GeneralForm Report1Form" id="reportContainer">
			</div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<s:hidden id="isNPP"></s:hidden><!-- sontt tạo biến chung này để kiểm tra đơn vị có phải là nhà phân phối không, hay là vùng, miền.-->
<script type="text/javascript">
$(document).ready(function(){
	ReportUtils.loadTree();
	ReportUtils.bindEnterOnWindow();
	var heightContent = window.innerHeight -132;
	$('.SidebarInSection').css({height:heightContent + 'px'});
});
</script>

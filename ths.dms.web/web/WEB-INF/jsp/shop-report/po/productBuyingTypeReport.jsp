<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="Breadcrumbs">
    <span class="Breadcrumbs1Item Sprite1">Bảng kê mua hàng theo mặt hàng</span>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
			<h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin tìm kiếm</span></h3>
            <div class="GeneralMilkInBox ResearchSection">
            	<div class="ModuleList3Form">
                <label class="LabelStyle Label1Style">Đơn vị</label>                
                <div class="BoxSelect BoxSelect2">
                      <div class="Field2">
           					<input id="shopId" style="width:200px;">
           	 				<span class="RequireStyle">(*)</span>
                   	</div>
                </div>
                <label class="LabelStyle Label2Style">Từ ngày</label>
                <div class="Field2">
                	<input id="fDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
                	<span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label3Style">Đến ngày</label>
                <div class="Field2">
                 <input id="tDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
	                <span class="RequireStyle">(*)</span>
                </div>
                <div class="Clear"></div>                
                <div class="ButtonSection">                	
                	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="productReport.productBuyReport();"><span class="Sprite2">Xuất báo cáo</span></button>                	
                </div>
                <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
               </div>
            </div>
		</div>
    </div>
</div>
<s:hidden id="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shopId','shopCode',$('#curShopId').val());
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
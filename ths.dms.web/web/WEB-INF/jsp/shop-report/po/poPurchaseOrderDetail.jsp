<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="shop" class="InputTextStyle InputText1Style" style="width:226px;"/>
		</dd>
		
		<dt>
			<label class="LabelStyle Label1Style">Trạng thái</label>
		</dt>
	     <dd class="BoxSelect BoxSelect1" >
	         <select class="MySelectBoxClass" id="type" >
	         	<option value="">Tất cả</option>
				<option value="0" selected="selected" >Chưa nhập</option>
				<option value="2">Đã nhập</option>
			</select>
	     </dd>
		
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		
		<dt class="LabelTMStyle" style="margin-bottom: 0px;">
                     <label class="LabelStyle Label1Style">Kiểu File</label>
         </dt>
         <dd style="margin-bottom: 0px;">
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLSTHO">XLS Dữ liệu thô
         </dd>
        <dt class="ClearLeft LabelTMStyle">
        	<label class="LabelStyle Label1Style">Cỡ giấy</label>
        </dt>
        <dd>
        	<span>
        		<input type="radio" name="pageFormat" id="pageFormatA4" value="A4" checked="checked">&nbsp;A4
        	</span>
        	<span style="margin-left: 5px;">
        		<input type="radio" name="pageFormat" id="pageFormatA5" value="A5">&nbsp;A5
        	</span>
        </dd>
	</dl>
	<!-- me -->
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="PoReport.exportBKCTCTMH();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
    
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	$('#type').css('width', '225');
	$('.CustomStyleSelectBox').css('width','192');
	$('.CustomStyleSelectBox').css('height','21');
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');

	$("input[name=formatType]:radio").change(function () {
	    if ($("#formatTypePdf").attr("checked")) {
	        $('[name=pageFormat]:radio').attr('disabled', false);
	    } else {
	        $('[name=pageFormat]:radio').attr('disabled', true);
	    }
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã đơn vị <span class="ReqiureStyle">*</span></label>
		</dt>
		<dd>
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
		</s:if><s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
		</dd>		
	<%-- 	<tiles:insertTemplate template="/WEB-INF/jsp/shop-report/chuKy.jsp" /> --%>
		<dt><label class="LabelStyle Label1Style">Chu kỳ <span class="ReqiureStyle">*</span></label></dt>
		<dd>
			<div class="BoxSelect BoxSelect1">
		    	<select class="MySelectBoxClass" id="cycle" onchange="onChangeCycle();">
					<s:iterator value="lstCycle">
						<option value='<s:property value="cycleId"/>'><s:property value="cycleName"/></option>
					</s:iterator>
		    	</select>
			</div>
		</dd>
		<%-- <dt class="ClearLeft"><label class="LabelStyle Label1Style">Từ ngày<span class="ReqiureStyle">(*)</span></label></dt> --%>
		<dd><input onchange="checkDateForCycle(0);" id="fDate" class="InputTextStyle InputText2Style"></dd>
		
		<%-- <dt><label class="LabelStyle Label1Style">Đến ngày<span class="ReqiureStyle">(*)</span></label></dt> --%>
		<dd><input onchange="checkDateForCycle(1);" id="tDate" class="InputTextStyle InputText2Style"></dd>
	</dl>	
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return HoReport.exportSLDS2_4_NUTI();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	/* setDateTimePicker('fDate');	
	setDateTimePicker('tDate'); */
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$('#fDate').hide();
	$('#tDate').hide();
	SuperviseCustomer.mapCycle = new Map();
	var idDefault = -1;
	<s:iterator value="lstCycle">
		var obj = new Object();
		obj.cycleId = '<s:property value="cycleId"/>';
		obj.cycleCode = '<s:property value="cycleCode"/>';
		obj.cycleName = '<s:property value="cycleName"/>';
		obj.num = '<s:property value="num"/>';
		obj.year = '<s:property value="year"/>';
		obj.beginDate = '<s:property value="beginDateStr"/>';
		obj.endDate = '<s:property value="endDateStr"/>';
		SuperviseCustomer.mapCycle.put(obj.cycleId, obj);
		idDefault = obj.cycleId;
	</s:iterator>
	$('#cycle').val(idDefault).change();//setDefault là thằng cuối cùng
	$('.MySelectBoxClass').customStyle();
	ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
		$('#staffCode').val('');
		$('#staffSaleCode').val('');
	});
});
function onChangeCycle() {
	var obj = SuperviseCustomer.mapCycle.get($('#cycle').val());
	if (obj != null) {
		$('#fDate').val(obj.beginDate);
		$('#tDate').val(obj.endDate);
	}
}
function checkDateForCycle(type) {
	$('#errMsg').hide();
	var obj = SuperviseCustomer.mapCycle.get($('#cycle').val());
	if (obj != null) {
		var msg ='';
		if (msg.length == 0 && (type == undefined || type == 0)) {
			var date = $('#fDate').val();
			if(!Utils.compareDate(obj.beginDate, date) || !Utils.compareDate(date, obj.endDate)){
				msg = 'Từ ngày phải nằm trong chu kỳ';
				$('#fDate').focus();
			}
		}
		if (msg.length == 0 && (type == undefined || type == 1)) {
			var date = $('#tDate').val();
			if(!Utils.compareDate(obj.beginDate, date) || !Utils.compareDate(date, obj.endDate)){
				msg = 'Đến ngày phải nằm trong chu kỳ';
				$('#tDate').focus();
			}
		}
		if (msg.length == 0) {
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
				$('#fDate').focus();
			}
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			setTimeout(function(){$('#errMsg').hide();}, 3000);
			return false;
		}
	}
	return true;
}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt><label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle">(*)</span></label></dt>
        <dd style="height:auto;">
        	<s:if test="NPP=='NPP'">
        		<input id="shop" style="width:226px; height:auto;" disabled="disabled" class="InputTextStyle InputText1Style">
        	</s:if>
        	<s:else>
        		<input id="shop" style="width:226px; height:auto;" class="InputTextStyle InputText1Style">
        	</s:else>
        </dd>

		<dt>&nbsp;</dt>
		<dd>&nbsp;</dd>
		
		<dt class="ClearLeft" style="margin-top:-15px;"><label class="LabelStyle Label1Style">Mã CTKM</label></dt>
		<dd style="margin-top:-15px;"><input id="promotionCode" style="width:226px;" class="InputTextStyle InputText1Style" /></dd>
		
		<dt style="margin-top:-15px;"><label class="LabelStyle Label1Style">Loại KM <span class="ReqiureStyle">(*)</span></label></dt>
		<dd style="margin-top:-15px;">
			<div class="BoxSelect BoxSelect1">
				<select id="promotionType" class="MySelectBoxClass">
					<option value="">Tất cả</option>
					<option value="0">KM tự động</option>
					<option value="1,2,3,4,5">KM tay/Hủy, Đổi, Trả/Trưng bày</option>
				</select>
			</div>
		</dd>
		
		<dt><label class="LabelStyle Label1Style">Từ ngày <span class="ReqiureStyle">(*)</span></label></dt>
		<dd><input id="fDate" class="InputTextStyle InputText1Style" style="width:204px;" /></dd>
		
		<dt><label class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle">(*)</span></label></dt>
		<dd><input id="tDate" class="InputTextStyle InputText1Style" style="width:194px;" /></dd>
		
		<dt class="LabelTMStyle"><label class="LabelStyle Label1Style" style="margin-top:3px">Kiểu File</label></dt>
        <dd>
            <input type="radio" name="formatType" id="formatTypeCSV" value="CSV" checked="checked">CSV
            <input style="margin-left:46px" type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
            <input style="margin-left:46px" type="radio" name="formatType" id="formatTypeZIP" value="ZIP">*ZIP-CSV
        </dd>
        <dt><label class="LabelStyle Label1Style">Dữ liệu thô </label></dt>
		<dd style="margin-top:3px" ><input type="checkbox" id="checkExport" checked="true"></dd>
        <dt></dt>
	    <dd></dd>
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnExport" class="BtnGeneralStyle" onclick="HoReport.exportKM1_3();">Xuất báo cáo</button>
	</div>	
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>

<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");
});
</script>
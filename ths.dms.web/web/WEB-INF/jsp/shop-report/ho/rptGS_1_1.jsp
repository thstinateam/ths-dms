<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle">(*)</span></label>
	</dt>
	<dd style="height:auto; ">
		<input id="shop" type="text"  style="width:215px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt>
	</dt>
	<dd>
	</dd>
	<dt class="ClearLeft" style="margin-top:-15px;">
		<label  class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd style="margin-top:-15px;">
		<input id="fromDate" type="text" class="InputTextStyle InputText2Style"/> 
	</dd>
	<dt class="LabelTMStyle" style="margin-top:-15px;">
		<label  class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd style="margin-top:-15px;">
		<input id="toDate" type="text" class="InputTextStyle InputText2Style"/> 
	</dd>
	<dt class="ClearLeft LabelTMStyle"><label class="LabelStyle Label1Style">Kiểu File</label></dt>
    <dd>
	    <input type="radio" name="formatType" id="formatTypeCSV" value="CSV" checked="checked">CSV
	    <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
	    <input type="radio" name="formatType" id="formatTypeZIP" value="ZIP">*ZIP-CSV
    </dd>
	<dt>&nbsp;</dt>
	<dd>&nbsp;</dd>
	<dt><label class="LabelStyle Label1Style">Dữ liệu thô </label></dt>
    <dd style="margin-top:3px">
        <input type="checkbox" id="checkExport" checked="true">
   </dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="HoReport.exportGS1_1();">Xuất Excel</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
	$(document).ready( function() {
		setDateTimePicker('fromDate');
		setDateTimePicker('toDate');
		ReportUtils.setCurrentDateForCalendar('fromDate');
		ReportUtils.setCurrentDateForCalendar('toDate');
		$("#shop").kendoMultiSelect({
	        dataTextField: "shopCode",
	        dataValueField: "shopId",
	        filter: "contains",
			itemTemplate: function(data, e, s, h, q) {
				var level = data.isLevel;
				if(level == 1) {//VNM
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else if(level == 2) {
						return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 3){
					return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 4){
					return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 5){
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else {
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
	        tagTemplate:  '#: data.shopCode #',
	        change: function(e) {
	        	var lstShop=this.value();
	        	var param='';
	        	var param1='';
	        	for(var i=0;i<lstShop.length;i++){
	        		if(i==0){
	        			param+='?lstShop='+lstShop[i];
	        			param1+='?lstShopId='+lstShop[i];
	        		}else{
	        			param+='&lstShop='+lstShop[i];
	        			param1+='&lstShopId='+lstShop[i];
	        		}
	        	}
	        	//$('#staffSaleCode').val('');
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
	                }
	            }
	        },
	        value: [$('#curShopId').val()]
	    });

	    var shopKendo = $("#shop").data("kendoMultiSelect");
	    shopKendo.wrapper.attr("id", "shop-wrapper");

	    $(window).bind('keypress',function(event){
			if($('.fancybox-inner').length!=0){
				if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
					Images.showImageNextPre(0);
				}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
					Images.showImageNextPre(1);
				}
				return false;
			}
			return true;
		});
	});
</script>                	
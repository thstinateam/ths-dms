<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã đơn vị<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd style="height:auto;">
		<div class="BoxSelect BoxSelect2">				
				<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:218px;height: auto;" class="InputTextStyle InputText1Style" />
				<%-- 	<input id="shop" type="text" data-placeholder="<s:text name="image_manager_choose_unit"/>"  style="width:301px;height: auto;" class="InputTextStyle InputText1Style" /> --%>
				</div>
		</div>
		</dd>
		<tiles:insertTemplate template="/WEB-INF/jsp/shop-report/chuKy.jsp" />	
		<dt><label class="LabelStyle Label1Style">Số đơn hàng</label></dt>
		<dd>
			<input type="text" class="InputTextStyle InputText1Style" id="orderNumber"/>
		</dd>
			<dt><label class="LabelStyle Label1Style">Trạng thái</label></dt>
		<dd>
			 <div class="BoxSelect BoxSelect1">
                  <select class="MySelectBoxClass" id="orderStatus">
                          <option value="-1" selected="selected">Tất cả</option>
                          <option value="0-0" >Chưa xác nhận đơn hàng</option>
                          <option value="0-1" >Đã xác nhận đơn hàng</option>
						  <option value="1-2">Đã xác nhận in hóa đơn</option>
						  <option value="1-3">Đã cập nhật kho</option>
						  <option value="0">Đã trả</option>
						  <option value="3-">Hủy</option>
						  <option value="2-">Từ chối</option>
                  </select>
             </div>
		</dd>
		<dt><label class="LabelStyle Label1Style">Loại Đơn Hàng</label>	</dt>
		<dd>
			  <div class="BoxSelect BoxSelect2">
                          <select class="MySelectBoxClass1" id="orderType" multiple="multiple">
                                        <option value="IN">Tất cả</option>
										<option value="IN" selected="selected">Đơn bán PreSale (IN)</option>
										<option value="CM">Đơn trả PreSale (CM)</option>
										<option value="SO">Đơn bán Vansale (SO)</option>
										<option value="CO">Đơn trả Vansale (CO)</option>
										<option value="GO">Đơn trả hàng Vansale (GO)</option>
										<option value="DP">Đơn bán hàng Vansale (DP)</option>
										<option value="AI">Điều chỉnh doanh thu tăng (AI)</option>
										<option value="AD">Điều chỉnh doanh thu giảm (AD)</option>
										<option value="DC">Điều chuyển (DC)</option>
										<option value="DCT">Điều chỉnh tăng kho (DCT)</option>
										<option value="DCG">Điều chỉnh giảm kho (DCG)</option>
                          </select>
             </div>
		</dd>	
		<dt><label class="LabelStyle Label1Style">Tạo trên</label></dt>
		<dd>
			   <div class="BoxSelect BoxSelect1">
                                    <select class="MySelectBoxClass" id="orderSource">
                                        <option value="0" selected="selected">Tất cả</option>
										<option value="1" >Web</option>
										<option value="2">Tablet</option>
                                    </select>
               </div>
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">Nhân viên bán hàng</label>
		</dt>
		<dd style="height:auto;">		
			<div class="BoxSelect BoxSelect2" id="divCttb" style="margin-top:-3px">
				<div class="Field2">
					<input id="nvbh" type="text" data-placeholder="Chọn nhân viên bán hàng"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
				</div>
			</div>	
		</dd>
	</dl>	
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return HoReport.exportSLDS2_6_NUTI();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	$('.MySelectBoxClass').customStyle();
    $("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
		tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(lstShop[i] != null && lstShop[i] != ""){
	        		if(i==0){
	        			param+='?lstShop='+lstShop[i];
	        			param1+='?lstShopId='+lstShop[i];
	        		}else{
	        			param+='&lstShop='+lstShop[i];
	        			param1+='&lstShopId='+lstShop[i];
	        		}
	        	}
        	}
        	if(param != "" && param1 != ""){
				$('#divCttb').html('<input id="nvbh" type="text" data-placeholder="Chọn nhân viên bán hàng"  style="width:218px;height: auto;margin-left:0px" class="InputTextStyle InputText1Style" />');	       	
				SuperviseCustomer.loadComboNVBH("/commons/list-nvbh-by-shop"+param1);
	        }
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });
    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");
    SuperviseCustomer.loadComboNVBH("/commons/list-nvbh-by-shop");
    $('#orderType').dropdownchecklist({emptyText:'Tất cả',firstItemChecksAll: true,
		textFormatFunction: function(options) {
			if (options[0].selected) {
				return "Tất cả";
			}
			var s = null;
			for (var i = 1, sz = options.length; i < sz; i++) {
				if (options[i].selected) {
					if (s == null) {
						s = options[i].value;
					} else {
						s = s + "," + options[i].value;
					}
				}
			}
			if (s == null) {
				return "Tất cả";
			}
			return s;
		}});
});
</script>
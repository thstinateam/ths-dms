<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã đơn vị</label>
		</dt>
		<dd>
		<div class="BoxSelect BoxSelect2" style="height: auto;">				
				<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:218px;height: auto;" class="InputTextStyle InputText1Style" />
				<%-- 	<input id="shop" type="text" data-placeholder="<s:text name="image_manager_choose_unit"/>"  style="width:301px;height: auto;" class="InputTextStyle InputText1Style" /> --%>
				</div>
		</div>		
		</dd>
		<tiles:insertTemplate template="/WEB-INF/jsp/shop-report/chuKy.jsp" />
		<dt>
			<label class="LabelStyle Label1Style"  style="margin-left:10px">Câu lạc bộ</label>
		</dt>
		<dd style="height:auto;">		
		 	 <div class="BoxSelect BoxSelect2" id="divCttb" style="margin-top:-3px">
				<input id="cttb" type="text" data-placeholder="Chọn câu lạc bộ"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
		    </div>	
			<!-- <div class="BoxSelect BoxSelect2" id="divCttb" style="margin-top:-3px">
				<div class="Field2">
					<input id="nvbh" type="text" data-placeholder="Chọn nhân viên bán hàng"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
				</div>
			</div>	 -->
		</dd>		
	</dl>
	
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return HoReport.exportBCDSKS();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	$('.MySelectBoxClass').customStyle();	
    $("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
		tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(lstShop[i] != null && lstShop[i] != ""){
	        		if(i==0){
	        			param+='?lstShop='+lstShop[i];
	        			param1+='?lstShopId='+lstShop[i];
	        		}else{
	        			param+='&lstShop='+lstShop[i];
	        			param1+='&lstShopId='+lstShop[i];
	        		}
	        	}
        	}
        	if(param != "" && param1 != ""){
	       		$('#divCttb').html('<input id="cttb" type="text" data-placeholder="Chọn câu lạc bộ"  style="width:218px;height: auto;margin-left:0px" class="InputTextStyle InputText1Style" />');	       	
	       		Images.loadComboCTTB("/images/displayPrograme/getListCTTBbyListShop"+param1);
	        }
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });
    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");
    Images.loadComboCTTB("/images/displayPrograme/getListCTTBbyListShop");
});
</script>
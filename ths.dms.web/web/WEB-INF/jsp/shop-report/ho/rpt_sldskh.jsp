<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã đơn vị <span class="ReqiureStyle">*</span></label>
		</dt>
		<dd>
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
		</s:if><s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
		</dd>
		
		<tiles:insertTemplate template="/WEB-INF/jsp/shop-report/chuKy.jsp" />
		<dt>
			<label class="LabelStyle Label1Style">Ngành hàng</label>
		</dt>
		<dd>
            <div class="BoxSelect BoxSelect1" id="cat"> 
            	<select id="category" class="MySelectBoxClass" >  
           			 <option value="0">Chọn ngành hàng</option>                                      		
            			<s:iterator var="obj" value="lstCategoryType"> 
          			 <option value="<s:property value="#obj.id" />">
         				<s:property value="codeNameDisplay(#obj.productInfoCode,#obj.productInfoName)" /></option>                        		
            			</s:iterator>
         	   </select> 
            </div>
		</dd>
		<dt>
			  <label class="LabelStyle Label1Style">Ngành hàng con</label>
		</dt>
		<dd>
			<div class="BoxSelect BoxSelect1" id="subcat">
               	<select id="sub_category" class="MySelectBoxClass" >  
                   		<option value="0">Chọn ngành hàng con</option>
                           <s:iterator var="subObj" value="lstSubCategoryType"> 
                               <option value="<s:property value="#subObj.id" />">
                               <s:property value="codeNameDisplay(#subObj.productInfoCode,#subObj.productInfoName)" /></option>                        		
                           </s:iterator>                                      		
               </select> 
             </div>
		</dd>
		<dt>
			 <label class="LabelStyle Label1Style">Sản phẩm</label>
		</dt>
		<dd style="height:auto;">
			<div class="BoxSelect BoxSelect2" id="divCttb" style="margin-top:-3px">
				<div class="Field2">
					<input id="lstProduct" type="text" data-placeholder="Chọn sản phẩm"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
				</div>
			</div>	
		</dd>
	</dl>
	
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return HoReport.exportSLDSKH();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	$('.MySelectBoxClass').customStyle();
	ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
		
	});
	$('#category').val('Chọn nghành hàng').change(function(){
		var params = new Object();
		var param1 = '';
		params.idParentCat = $('#category').val();	
		Utils.getHtmlDataByAjax(params,'/report/ho/getListSubCat',
				function(data) {
					try {
						var _data = JSON.parse(data);
					} catch (e) {
						$('#sub_category').html(data);
						$('#subcat .CustomStyleSelectBoxInner').html("Chọn ngành hàng con");
						$('#sub_category option[value=0]').attr('selected', 'selected');
					}
				}, '', 'GET');
		$('#divCttb').html('<input id="lstProduct" type="text" data-placeholder="Chọn sản phẩm" style="width: 218px; height: auto; margin-left: 0px" class="InputTextStyle InputText1Style" />');	       	
		if ($('#category').val() != null && $('#category').val() != "") {
			param1 += '?idParentCat=' + $('#category').val();
		}
		HoReport.loadComboProduct("/commons/list-product-by-cat" + param1);		
	});
	$('#sub_category').val('Chọn ngành hàng con').change(function(){		
		var param1='';	
		$('#divCttb').html('<input id="lstProduct" type="text" data-placeholder="Chọn sản phẩm" style="width: 218px; height:auto; margin-left: 0px" class="InputTextStyle InputText1Style" />');	       	
		if($('#sub_category').val() != null && $('#sub_category').val() != "") {
			param1 += '?idParentSubCat=' + $('#sub_category').val();
		}
		HoReport.loadComboProduct("/commons/list-product-by-cat" + param1);		
	});
	HoReport.loadComboProduct("/commons/list-product-by-cat");
});
</script>
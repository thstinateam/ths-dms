<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Đơn vị <span class="RequireStyle">(*)</span></label>
		</dt>
		<dd style="height:auto;">
			<div class="BoxSelect BoxSelect2">
			<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:216px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>
		</dd>
		<dt class="ClearLeft" style="margin-top:-20px;">
			<label class="LabelStyle Label1Style">Ngành hàng(F9)</label>
		</dt>
		<dd style="margin-top:-20px;">
			<input type="text" class="InputTextStyle InputText1Style" id="catCode" />
		</dd>

		<dt style="margin-top:-20px;">
			<label class="LabelStyle Label1Style">Sản phẩm(F9)</label>
		</dt>
		<dd style="margin-top:-20px;">
			<input type="text" class="InputTextStyle InputText1Style" id="product" />
		</dd>
		
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ Ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input type="text" id="fromDate" class="InputTextStyle InputText2Style" />
		</dd>

		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến Ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input type="text" id="toDate" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle"><label class="LabelStyle Label1Style" style="margin-top:3px">Kiểu File</label></dt>
        <dd>
            <input type="radio" name="formatType" id="formatTypeCSV" value="CSV" checked="checked">CSV
            <!-- <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS -->
            <input type="radio" name="formatType" id="formatTypeZIP" value="ZIP">*ZIP-CSV
        </dd>
        <dt>&nbsp;</dt>
		<dd>&nbsp;</dd>
	       <dt><label style="display: none;" class="LabelStyle Label1Style">Dữ liệu thô </label></dt>
	    <dd style="margin-top:3px">
	        <input type="checkbox" style="display: none;" id="checkExport" checked="true">
	   </dd> 
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="HoReport.exportF1();">Xuất báo cáo</button>
	</div>
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>

<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fromDate');
	setDateTimePicker('toDate');
	ReportUtils.setCurrentDateForCalendar('fromDate');
	ReportUtils.setCurrentDateForCalendar('toDate');
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");

    $(window).bind('keypress',function(event){
		if($('.fancybox-inner').length!=0){
			if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
				Images.showImageNextPre(0);
			}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
				Images.showImageNextPre(1);
			}
			return false;
		}
		return true;
	});

    $('#catCode').bind('keyup', function(event) {
    	if (event.keyCode == keyCode_F9) {
    		VCommonJS.showDialogSearch2WithCheckbox({
    		    inputs : [
    		        {id:'code', maxlength:50, label:'Mã Ngành Hàng'},
    		        {id:'name', maxlength:250, label:'Tên Ngành Hàng'}
    		    ],
    		    chooseCallback : function(listObj) {
    		        console.log(listObj);
    		        var lstCat = '';
    		        if($.isArray(listObj) && listObj.length > 0) {
    		        	for(var i = 0; i < listObj.length; i++) {
        					if(lstCat.length > 0) {
        						lstCat+=",";
        					}
        					lstCat+=listObj[i].productInfoCode;
        				}
    		        }
    		        $('#catCode').val(lstCat);
    		        $('#common-dialog-search-2-textbox').dialog('close');
    		    },
    		    url : '/commons/category/search',
    		    columns : [[
    		        {field:'productInfoCode', title:'Mã Ngành Hàng', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
    		        	return Utils.XSSEncode(value);         
    		        }},
    		        {field:'productInfoName', title:'Tên Ngành Hàng', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index) {
    		        	return Utils.XSSEncode(value);         
    		        }},
    		        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
    		    ]]
    		});
    	}
    });
    $('#product').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			VCommonJS.showDialogSearch2WithCheckbox({
    		    inputs : [
    		        {id:'code', maxlength:50, label:'Mã SP'},
    		        {id:'name', maxlength:250, label:'Tên SP'}
    		    ],
    		    chooseCallback : function(listObj) {
    		        console.log(listObj);
    		        var lstProductCode = '';
    		        if($.isArray(listObj) && listObj.length > 0) {
    		        	for(var i = 0; i < listObj.length; i++) {
        					if(lstProductCode.length > 0) {
        						lstProductCode+=",";
        					}
        					lstProductCode+=listObj[i].productCode;
        				}
    		        }
    		        $('#product').val(lstProductCode);
    		        $('#common-dialog-search-2-textbox').dialog('close');
    		    },
    		    url : '/commons/product-searchAllProduct',
    		    param : {catCode : $('#catCode').val()},
    		    columns : [[
    		        {field:'productCode', title:'Mã SP', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
    		        	return Utils.XSSEncode(value);         
    		        }},
    		        {field:'productName', title:'Tên SP', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index) {
    		        	return Utils.XSSEncode(value);         
    		        }},
    		        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
    		    ]]
    		});
		}
	});
	
});
</script>
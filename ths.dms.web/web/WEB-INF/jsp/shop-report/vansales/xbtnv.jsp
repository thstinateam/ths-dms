<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!--             	<div class="ModuleList3Form"> -->
<!--                 <label class="LabelStyle Label1Style">Đơn vị</label>                 -->
<!--                 <div class="BoxSelect BoxSelect2"> -->
<!--                       <div class="Field2"> -->
<!--            					<input id="shop" style="width:200px;"> -->
<%--            	 				<span class="RequireStyle">(*)</span> --%>
<!--                    	</div> -->
<!--                 </div> -->
<!--                 <label class="LabelStyle Label1Style">NVBH (F9)</label>  -->
<!--                 <input id="staffCode" type="text" class="InputTextStyle InputText2Style" style="width:190px;" />      -->
<!--                 <div class="Clear"></div>  -->
<!--                 <label class="LabelStyle Label1Style">Từ ngày</label> -->
<!--                 <div class="Field2"> -->
<!--                 	<input id="fDate" type="text" class="InputTextStyle InputText2Style" style="width:168px;" /> -->
<%--                 	<span class="RequireStyle">(*)</span> --%>
<!--                 </div> -->
<!--                 <label class="LabelStyle Label1Style">Đến ngày</label> -->
<!--                 <div class="Field2"> -->
<!--                  <input id="tDate" type="text" class="InputTextStyle InputText2Style" style="width:168px;" /> -->
<%-- 	                <span class="RequireStyle">(*)</span> --%>
<!--                 </div> -->
<!--                 <div class="Clear"></div>                 -->
<!--                 <div class="ButtonSection">                	 -->
<%--                 	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return SaleStaff.reportSaleStaff();"><span class="Sprite2">Xuất báo cáo</span></button>                	 --%>
<!--                 </div> -->
<!--                 <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>                -->
<!--                </div> -->
<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVBH(F9)
			</label>
		</dt>
		<dd>
			<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
                     <label class="LabelStyle Label1Style" style="padding:6px 0 0">Kiểu File
                     </label>
              </dt>
              <dd>
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
              </dd>
		
	</dl>
	<!-- me -->
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="SaleStaff.reportSaleStaff();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
// 	setDateTimePicker('fDate');
// 	setDateTimePicker('tDate');	
// 	DebitPayReport._lstCustomer = new Map();
// 	DebitPayReport._lstStaff = new Map();
// 	var params = new Array();
// 	params.push('staffCode');
// 	params.push('fDate');
// 	params.push('tDate');
	
	isOpenJAlert = false;
	isFromBKXNKNVBH = true;
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	//ReportUtils.loadComboTree('shop','shopCode',$('#curShopId').val(),null,params);
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(shopId){
		$('#staffCode').val('');
	});
	
	$('#staffCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			if($('#isNPP').val() == "false"){
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
			}else{
				$('#errMsg').hide();
				ReportUtils.showDialogSearchNVBH($('#shop').combotree('getValue'));
			}
		}
	});
	
// 	$('#staffCode').bind('keyup', function(event){
// 		if(event.keyCode == keyCode_F9){
// 			var arrParam = new Array();
// 			var param1 = {};
// 			param1.name = "shopId";
// 			param1.value = $('.combo-value').val();
// 			arrParam.push(param1);
// 			CommonSearch.searchPreAndValStaffOnDialogCheckbox(function(data){},arrParam);
// 		}else{
// 			var lstStaff = $('#staffCode').val().trim().split(";");
// 			DebitPayReport._lstStaff = new Map();
// 			for(i=0;i<lstStaff.length;i++){
// 				if(lstStaff[i]!=undefined && lstStaff[i]!=''){
// 					DebitPayReport._lstStaff.put(lstStaff[i],lstStaff[i]);
// 				}
// 			}
// 		}
// 	});
	
});
</script>
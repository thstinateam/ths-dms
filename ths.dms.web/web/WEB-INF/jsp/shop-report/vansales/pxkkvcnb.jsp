<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<s:if test="%{NPP=='NPP'}" >
				<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" disabled='disabled'/>
			</s:if>			
			<s:else>
    			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
			</s:else>			
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVBH(F9)<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
		</dd>
		
		<dt class="ClearLeft">
			<label class="LabelStyle Label1Style">Giao dịch xuất <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<div class="BoxSelect BoxSelect1" style = "width: 226px">
			<select id="idTransCode" class="MySelectBoxClass" style = "width: 226px">
				<option value="-1">[-- Chọn giá trị --] </option>
			</select>
			</div>
		</dd>
	
		<dt>
			<label id="ipNgayXuatKho" class="LabelStyle Label1Style">Ngày <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>				
		<dt class="ClearLeft">
			<label class="LabelStyle Label1Style">Tên lệnh điều động <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>		
			<input id="ipNameTrans" type="text" class="InputTextStyle InputText1Style" style="width:679px"/>
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Nội dung<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<!-- <input  type="text" class="InputTextStyle InputText2Style" /> -->
			<s:textarea id="ipContentTrans" cols="128" rows="10" style="font-size: 12px" value="Vận chuyển hàng đi bán lưu động" />
		</dd>
		
		<dt class="ClearLeft LabelTMStyle" style="margin-top: 130px;">
                     <label class="LabelStyle Label1Style">Kiểu File
                     </label>
              </dt>
              <dd style="margin-top: 130px;">
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
              </dd>
	</dl>

 	<div class="Clear"></div> 
 	<div class="BtnCenterSection" style="padding-top: 18px">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="ExportTrans.exportPXKKVCNB();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>
<s:hidden id="shopId" ></s:hidden>            
<s:hidden id="shopCode"></s:hidden>
<s:hidden id="staffCode" name="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId" ></s:hidden>
<s:hidden id="customerCode"></s:hidden>
<s:hidden id="trans" ></s:hidden>

<script type="text/javascript">
function survey(selector, callback) {
	   var input = $(selector);
	   var oldvalue = input.val();
	   setInterval(function(){
	      if (input.val()!=oldvalue){
	          oldvalue = input.val();
	          callback();
	      }
	   }, 100);
	}

$(document).ready(function(){
	setDateTimePicker('fDate');
	//setDateTimePicker('tDate');	
	setDateTimePicker('ipNgayXuatKho');
	$('.MySelectBoxClass').customStyle();
	//$('#ipGiaoDichXuat').attr('disabled', true);
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());	
	//ReportUtils.loadComboTreeStockTrans('ipGiaoDichXuat', 'trans', '0000000067', $('#reportContainer input.combo-value').val());
	//ReportUtils.loadComboTreeStockTrans('ipGiaoDichXuat', 'trans', '0', '0');
	$('#ipContentTrans').val('Vận chuyển hàng đi bán lưu động');
	$('#staffCode').bind('keyup', function(event) {
		//var isNPP = $('#isNPP').val();
		if (event.keyCode == keyCode_F9) {
			var shopID =  $('#reportContainer input.combo-value').val().trim();
			$.getJSON("/report/are-you-shop?shopId="+shopID,function(result)
			{
				if(!result.isNPP) {
					msg = 'Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!';
					$('#errMsg').html(msg).show();
				} else {
					$('#errMsg').html('').hide();
					var arrParam = new Array();
					var param = {};
					param.name = "shopId";
					param.value = $('#shop').combotree('getValue');
					arrParam.push(param);
					CommonSearch.searchPreAndValStaffOnDialog(function(data){
						$('#staffCode').val(data.code);
						ExportTrans.getListStockTrans($('#staffCode').val(),$('#idTransCode'));
					},arrParam);
				
				}
			});
		}
	});
	$('#staffCode').change(function(){
		ExportTrans.getListStockTrans($('#staffCode').val(),$('#idTransCode'));
	});
	ReportUtils.setCurrentDateForCalendar('fDate');
	
	 $('#shopId').trigger('change');
	 survey('#shopId', function(){
		 $('#staffCode').val('');
		 $('#idTransCode').html("<option value='-1'> [-- Chọn giá trị --]</option>").change();
		 //alert(1);
		 //ReportUtils.loadComboTreeStockTrans('ipGiaoDichXuat', 'trans', '1', '-1');//set to empty data
	 }); 
		
});


</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<s:if test="%{NPP=='NPP'}">
				<input type="text" id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
			</s:if>
			<s:else>
				<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
			</s:else>
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVBH(F9) </label>
		</dt>
		<dd>
			<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
                     <label class="LabelStyle Label1Style" style="margin-top:3px">Kiểu File
                     </label>
              </dt>
              <dd>
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
              </dd>
          <dt> 
              <label class="LabelStyle Label1Style" >Loại phiếu</label>
            </dt>
            <dd>
					<div class="BoxSelect BoxSelect2">
						<select id="typeBill" class="MySelectBoxClass">
							<option value="0">Tất cả</option>
							<option value="1">Xuất kho nhân viên</option>
							<option value="2">Nhập kho nhân viên</option>
						</select>
					</div>
			</dd>
		
	</dl>

	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return ExportTrans.reportBCXNKNVBH();">
			<span class="Sprite2">Xuất báo cáo</span>
		</button>
	</div>
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#typeBill').customStyle();
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(), function(shopId){
		$('#staffCode').val('');
	});
	$('#staffCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			if($('#isNPP').val() == "false"){
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
			}else{
				$('#errMsg').hide();
				ReportUtils.showDialogSearchNVBH($('#shop').combotree('getValue'));
			}
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!-- vuongmq, 7-7-8.Đối chiếu HĐ hệ thống và HĐ GTGT -->
<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
	<s:if test="%{NPP=='NPP'}">
		<input type="text" id="shop" style="width:227px;" disabled="disabled" class="InputTextStyle InputText1Style" />
	</s:if>
	<s:else>
		<input type="text" id="shop" style="width:227px;" class="InputTextStyle InputText1Style" />
	</s:else>
   		
	</dd>
	<dt>
		<label class="LabelStyle Label1Style"></label>
	</dt>
	<dd>
		&nbsp;
	</dd>
	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="fDate" type="text" style="width:195px" class="InputTextStyle InputText2Style" />
	</dd>
	<dt class="LabelTMStyle">
		<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="tDate" type="text" style="width:195px" class="InputTextStyle InputText2Style" />
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">Kiểu file</label>
	</dt>
	<dd>
		<input type="radio" value="PDF" name="formatType" checked="checked">PDF
		<input type="radio" value="XLS" name="formatType">XLS
	</dd>
	
</dl>
<div class="Clear"></div> 
	<div class="BtnCenterSection">                	
		<button id="btnExport" class="BtnGeneralStyle Sprite2" onclick="TaxReport.export7_7_8();"><span class="Sprite2">Xuất báo cáo</span></button>                	
	</div>
   <p id="errMsg" class="ErrorMsgStyle " style="display: none"></p>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="type" name="type"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){	
	Utils.bindFormatOnTextfield('tax', Utils._TF_NUMBER);
	$('.MySelectBoxClass').customStyle();
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(shopId){
		$('#staffCode').val('');
	});
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');

	$('#staffCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			if($('#isNPP').val() == "false"){
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
			}else{
				$('#errMsg').hide();
				ReportUtils.showDialogSearchNVBH($('#shop').combotree('getValue'));
			}
		}
	});
	
	$('#status').combobox({
		valueField : 'value',
		textField : 'label',
		data : [ {
			label : 'Tất cả',
			value : '-1',
			selected : true
		}, {
			label : 'Đang sử dụng',
			value : '0'
		}, {
			label : 'Đã hủy',
			value : '2'
		} ]
});
});
</script>             	
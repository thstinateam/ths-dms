<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
	<div class="ModuleList3Form">
    <label class="LabelStyle Label1Style">Thuế suất</label>               
    <div class="Field2">
		<input id="tax" type="text" class="InputTextStyle InputText1Style"/>
		<span class="RequireStyle">(*)</span>
    </div>
    <label class="LabelStyle Label2Style">NVBH(F9)</label>
   	<input id="staffCode" type="text" class="InputTextStyle InputText1Style"/>
   	<div class="Clear"></div>  	
    <label class="LabelStyle Label1Style">Từ ngày</label>
    <div class="Field2">
    	<input id="fDate" type="text" class="InputTextStyle InputText2Style"/>
    	<span class="RequireStyle">(*)</span>
    </div>
    <label class="LabelStyle Label2Style">Đến ngày</label>
    <div class="Field2">
     <input id="tDate" type="text" class="InputTextStyle InputText2Style"/>
     <span class="RequireStyle">(*)</span>
    </div>
    <div class="Clear"></div>
    <label class="LabelStyle Label1Style">Hình thức thanh toán</label>
    <div class="BoxSelect BoxSelect1">
         <s:select id="custPayment" headerKey="-2" headerValue="-- Hình thức thanh toán --" cssClass="MySelectBoxClass" list="#{'1':'Tiền mặt','2':'Chuyển khoản'}"></s:select>
  	</div>
    <label class="LabelStyle Label2Style">Số đơn hàng</label>
    <input id="orderNumber" type="text" class="InputTextStyle InputText1Style"/>
   	<div class="Clear"></div> 
    <label class="LabelStyle Label1Style">Mã KH (F9)</label>
    <input id="customerCode" type="text" class="InputTextStyle InputText1Style"/>
	<label class="LabelStyle Label2Style">Số hóa đơn</label>
    <input id="invoiceNumber" type="text" class="InputTextStyle InputText1Style"/>
	<div class="Clear"></div>
    <div class="ButtonSection">                	
    	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="TaxReport.reportHDGTGT();"><span class="Sprite2">Xuất báo cáo</span></button>                	
    </div>
    <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
   </div>
   <s:hidden id="cShopCode" name="shopCode"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	Utils.bindFormatOnTextfield('tax', Utils._TF_NUMBER);
	$('.MySelectBoxClass').customStyle();
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	$('#staffCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var lstParam = new Array();
			var obj= {};
			obj.name = 'staffTypeCode';
			obj.value = 1;
			lstParam.push(obj);
			CommonSearch.searchStaffOnDialog(function(data){
				$('#staffCode').val(data.code);
			},lstParam);
		}
	});
	$('#customerCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){	
			var lstParam = new Array();
			var obj= {};
			obj.name = 'shopCode';
			obj.value = $('#cShopCode').val().trim();
			lstParam.push(obj);
			CommonSearch.searchCustomerOnDialog(function(data){
				$('#customerCode').val(data.code);				
			},lstParam);
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!-- vuongmq, 7-7-7.Bảng kê chi tiết HĐ thuế GTGT -->
<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
	<s:if test="%{NPP=='NPP'}">
		<input type="text" id="shop" style="width:227px;" disabled="disabled" class="InputTextStyle InputText1Style" />
	</s:if>
	<s:else>
		<input type="text" id="shop" style="width:227px;" class="InputTextStyle InputText1Style" />
	</s:else>
   		
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">NVBH(F9)</label>
	</dt>
	<dd>
		<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="fDate" type="text" style="width:195px" class="InputTextStyle InputText2Style" />
	</dd>
	<dt class="LabelTMStyle">
		<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="tDate" type="text" style="width:195px" class="InputTextStyle InputText2Style" />
	</dd>
	
	<dt> 
		<label class="LabelStyle Label1Style">Thuế suất</label>
	</dt>
	<dd>
		<input id="tax" type="text" style="width:216px" class="InputTextStyle InputText2Style" />
	</dd>
	
	<dt>
		<label class="LabelStyle Label1Style">Trạng thái</label>
	</dt>
	<dd>
		<input id="status" type="text" style="width:226px;" class="InputTextStyle"/>
	</dd>
</dl>
<div class="Clear"></div> 
	<div class="BtnCenterSection">                	
		<button id="btnExport" class="BtnGeneralStyle Sprite2" onclick="TaxReport.exportCTHDGTGT();"><span class="Sprite2">Xuất báo cáo</span></button>                	
	</div>
   <p id="errMsg" class="ErrorMsgStyle " style="display: none"></p>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="type" name="type"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){	
	Utils.bindFormatOnTextfield('tax', Utils._TF_NUMBER);
	$('.MySelectBoxClass').customStyle();
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(shopId){
		$('#staffCode').val('');
	});
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');

	$('#staffCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			if($('#isNPP').val() == "false"){
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
			}else{
				$('#errMsg').hide();
				//ReportUtils.showDialogSearchNVBH($('#shop').combotree('getValue'));
				VCommonJS.showDialogSearch2WithCheckbox({
					params : {
						shopId : $('#shop').combotree('getValue'),
						lstObjectType : [StaffRoleType.NVBH, StaffRoleType.NVVS].join(","),
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã Nhân viên'},
				        {id:'name', maxlength:250, label:'Tên Nhân viên'},
				    ],
				    url : '/commons/search-staff-show-list',
				    columns : [[
				        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'cb', checkbox:true, align:'center', width:40,sortable : false,resizable : false}
				    ]],
				    chooseCallback: function(lstObj) {
				    	if (!lstObj || lstObj.length == 0) {
				    		return;
				    	}
				    	var s = "";
				    	for (var i = 0, sz = lstObj.length; i < sz; i++) {
				    		s = Utils.XSSEncode(s + "," + lstObj[i].staffCode);
				    	}
				    	s = s.replace(",", "");
				    	$("#staffCode").val(s);
				    	$(".easyui-dialog").dialog("close");
				    }
				});
			}
		}
	});
	
	$('#status').combobox({
		valueField : 'value',
		textField : 'label',
		data : [ {
			label : 'Tất cả',
			value : '-1',
			selected : true
		}, {
			label : 'Đang sử dụng',
			value : '0'
		}, {
			label : 'Đã hủy',
			value : '2'
		} ]
});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã đơn vị <span class="ReqiureStyle">*</span></label>
		</dt>
		<dd>
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
		</s:if><s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
		</dd>		
		<tiles:insertTemplate template="/WEB-INF/jsp/shop-report/chuKy.jsp" />
		<%-- <dt><label class="LabelStyle Label1Style">Chọn kho</label></dt>
		<dd>
			 <div class="BoxSelect BoxSelect1">
                  <select class="MySelectBoxClass" id="orderStock">
                          <option value="0" selected="selected">Hàng bán</option>
                          <option value="1" >Khuyến mãi</option>                       
                  </select>
             </div>
		</dd>
		<dt><label class="LabelStyle Label1Style">Loại thành phẩm</label></dt>
		<dd>
			 <div class="BoxSelect BoxSelect1" id="sub">              
                  <select id="orderProduct" class="MySelectBoxClass" >  
           			 <option value="0">Tất cả</option>                                      		
            			<s:iterator var="obj" value="lstProductType"> 
          			 <option value="<s:property value="#obj.id" />">
         				<s:property value="codeNameDisplay(#obj.productInfoCode,#obj.productInfoName)" /></option>                        		
            		</s:iterator>
         	   </select> 
             </div>
		</dd> --%>		
	</dl>	
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return StockReport.report7_4_3();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	$('.MySelectBoxClass').customStyle();
	ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
		$('#staffCode').val('');
		$('#staffSaleCode').val('');
	});
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
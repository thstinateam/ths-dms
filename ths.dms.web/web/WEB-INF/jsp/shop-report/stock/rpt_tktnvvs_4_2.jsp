<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="ModuleList3Form">
<dl class="Dl3Style">
	<dt class="ClearLeft LabelTMStyle" style="margin:0;">
		<label class="LabelStyle Label1Style">Mã đơn vị <span class="ReqiureStyle">*</span></label>
	</dt>
    <dd>
	 	<s:if test="Npp=='NPP'">
	 		<input id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
	 	</s:if>
	 	<s:else>
	 		<input id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
	 	</s:else>
    </dd>
    <dt><label class="LabelStyle Label1Style"></label></dt>
	<dd>
		<div class="BoxSelect BoxSelect1"></div>
	</dd>
	<dt class="ClearLeft LabelTMStyle" style="margin:0;">
		<label class="LabelStyle Label1Style">Từ ngày <span class="ReqiureStyle">*</span></label>
	</dt>
	<dd>
		<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
	</dd>
	<dt class="LabelTMStyle" style="margin:0;">
		<label class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle">*</span></label>
	</dt>
	<dd>
		<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
	</dd>
</dl>
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return StockReport.reportTKTNVVS4_2();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePickerTrigger('fDate');	
	setDateTimePickerTrigger('tDate');
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	
	$('.MySelectBoxClass').customStyle();
	ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
		$('#staffCode').val('');
		$('#staffSaleCode').val('');
	});
});
</script>
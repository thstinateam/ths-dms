<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt><label class="LabelStyle Label1Style">Mã NPP <span class="ReqiureStyle">(*)</span></label></dt>
        <dd>
        	<s:if test="NPP=='NPP'">
        		<input id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style">
        	</s:if>
        	<s:else>
        		<input id="shop" style="width:226px;" class="InputTextStyle InputText1Style">
        	</s:else>
        </dd>
        
        <dt><label class="LabelStyle Label1Style">Loại báo cáo<span class="ReqiureStyle">(*)</span></label></dt>
        <dd>
        	<div class="BoxSelect BoxSelect1">
        		<select id="rptType" class="MySelectBoxClass">
        			<option value="TH" selected="selected">Tổng hợp</option>
        			<option value="CT">Chi tiết</option>
        		</select>
        	</div>
        </dd>
        
		<!-- <dt class="LabelTMStyle"><label class="LabelStyle Label1Style">Kiểu File</label></dt>
        <dd>
            <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
            <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
        </dd> -->
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnExport" class="BtnGeneralStyle" onclick="CustomerReport.export7_5_1TH();">Xuất báo cáo</button>
	</div>	
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>

<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val());
});
</script>
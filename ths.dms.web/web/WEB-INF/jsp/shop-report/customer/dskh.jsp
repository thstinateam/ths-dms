<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Loại báo cáo<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<div class="BoxSelect BoxSelect1" style = "width: 210px">
				<select id="kindReport" class="MySelectBoxClass" style = "width: 210px">				
						<option value="1">Danh sách khách hàng</option>
						<option value="2">Danh sách khách hàng theo NVBH</option>
				</select>
			</div>
		</dd>
		
		<dt>
			<label class="LabelStyle Label1Style">Trạng thái</label>
		</dt>
		<dd>
			<div class="BoxSelect BoxSelect1" style = "width: 210px">
				<select id="status" class="MySelectBoxClass" style = "width: 210px">				
						<option value="1">Hoạt động</option>
						<option value="0">Tạm ngưng</option>
						<option value="-2" selected="selected">Tất cả</option>
				</select>
			</div>
			
		</dd>	
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</dd>
		
		<dt>
			<label class="LabelStyle Label1Style">Mã NVBH(F9)</label>
		</dt>
		<dd>
			<input type="text" id="saleStaffCode" class="InputTextStyle InputText1Style" />
			<div class="Clear"></div>
		</dd>
		
		<dt>
			<label class="LabelStyle Label1Style">Mã KH(F9)</label>
		</dt>
		<dd>
			<input type="text" id="customerCode" class="InputTextStyle InputText1Style" />
		</dd>
		
		<dt>
			<label class="LabelStyle Label1Style">Tên KH</label>
		</dt>
		<dd>
			<input type="text" id="customerName" class="InputTextStyle InputText1Style" />
			<div class="Clear"></div>
		</dd>
		
		<dt class="LabelTMStyle"><label class="LabelStyle Label1Style">Kiểu File</label></dt>
              <dd>
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
              </dd>
		
	</dl>
	<!-- me -->
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick=" CustomerReport.exportDSKH();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>

<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<input type="hidden" id="hideMSG" value="1" />

<script type="text/javascript">
$(document).ready(function(){	
	$('#kindReport').customStyle();
	$('#status').customStyle();
	$('.CustomStyleSelectBox').css('padding', '0 15px 0 9px');
	$('.CustomStyleSelectBox').css('width', '200');
	$('.CustomStyleSelectBoxInner').css('width', '200');
	
	ReportUtils.loadComboTree2('shop', 'shopId', $('#curShopId').val(),function(node){
		$(".ErrorMsgStyle").hide();
		if (node.isLevel != 5) {
			if ($("#hideMSG").val().trim() == 0) {
				$("#errMsg").html("Đơn vị chọn không phải là một NPP cụ thể").show();
			} else {
				$("#hideMSG").val(0);
			}
			$("#isNPP").val(0);
		} else {
			$("#isNPP").val(1);
		}
		$("#customerCode").val("");
		$("#customerName").val("");
		$("#saleStaffCode").val("");
	});
	
	$('#kindReport').change(function(){
		if($(this).val() == "1"){
			//disabe Nvbh:
			$('#saleStaffCode').attr('disabled','disabled'); 
			$('#saleStaffCode').val('');
		}else{
			$('#saleStaffCode').attr('disabled',false);
		}
	});	
	$('#kindReport').change();
	$('#saleStaffCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			if ($('#isNPP').val() == 0) {
				$('#errMsg').html('Đơn vị chọn không phải là một NPP cụ thể').show();
			} else {
				VCommonJS.showDialogSearch2({
					params : {
						shopId : $('#shop').combotree('getValue'),
						lstObjectType : [StaffRoleType.NVBH, StaffRoleType.NVVS].join(","),
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã Nhân viên'},
				        {id:'name', maxlength:250, label:'Tên Nhân viên'},
				    ],
				    url : '/commons/search-staff-show-list',
				    columns : [[
				        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(v, r, i) {
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(v, r, i) {
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				            return "<a href='javascript:void(0)' onclick='chooseStaff(\""+Utils.XSSEncode(row.staffCode)+"\");'>chọn</a>";
				        }}
				    ]]
				});
			}
		}
	});
	$('#customerCode').bind('keyup', function(event) {
		$(".ErrorMsgStyle").hide();
		if (event.keyCode == keyCode_F9) {
			if ($('#isNPP').val() == 0) {
				$('#errMsg').html('Đơn vị chọn không phải là một NPP cụ thể').show();
			} else {
				var stCode = $("#saleStaffCode").val().trim();
				var url = "/commons/customer-in-shop/search";
				var params = {shopId: $('#shop').combotree('getValue'), status:$("#status").val().trim()};
				if (stCode.length > 0) {
					url = "/commons/search-Customer-Show-List-For-Routing";
					params.staffCode = stCode;
					params.checkInCurrentWeek = 1;
				}
				VCommonJS.showDialogSearch2({
				    inputs: [
				        {id:'code', maxlength:50, label:'Mã KH'},
				        {id:'name', maxlength:250, label:'Tên KH'},
				        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
				    ],
				    params: params,
				    url: url,
				    onShow: function() {
			        	$('.easyui-dialog #code').focus();
				    },
				    columns: [[
				        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter: function(v, r, i) {
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter: function(v, r, i) {
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter: function(v, r, i) {
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				        	return "<a href='javascript:void(0)' onclick='chooseCustomer(\""+ Utils.XSSEncode(row.shortCode) + "\", \""+Utils.XSSEncode(row.customerName)+"\");'>Chọn</a>";        
				        }}
				    ]]
				});
			}
		}
	});
});

function chooseCustomer(code, name) {
	$("#customerCode").val(code);
	$("#customerName").val(name);
	$(".easyui-dialog").dialog("close");
}
function chooseStaff(code) {
	$("#saleStaffCode").val(code);
	$(".easyui-dialog").dialog("close");
}
</script>
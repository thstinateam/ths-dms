<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
   	<dl class="Dl3Style">				
		<dt>
			<label class="LabelStyle Label1Style">Mã đơn vị<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</dd>
		
		<dt><label class="LabelStyle Label1Style">Mã NVBH(F9)</label></dt>
		<dd>
			<input type="text" id="saleStaffCode" class="InputTextStyle InputText1Style" />
			<div class="Clear"></div>
		</dd>
		
		<dt><label class="LabelStyle Label1Style">Mã KH(F9)</label></dt>
		<dd>
			<input type="text" id="customerCode" class="InputTextStyle InputText1Style" />
		</dd>
		
		<dt><label class="LabelStyle Label1Style">Tên KH</label></dt>
		<dd>
			<input type="text" id="customerName" class="InputTextStyle InputText1Style" />
			<div class="Clear"></div>
		</dd>
		
		<dt class="LabelTMStyle">
                     <label class="LabelStyle Label1Style">Kiểu File</label>
              </dt>
              <dd>
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
              </dd>
	</dl>
 		
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="CustomerReport.exportDSKHTTBH();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	ReportUtils.loadComboTree2('shop','shopId',$('#curShopId').val(),function(node){
		$(".ErrorMsgStyle").hide();
// 		if (node.isLevel != 5) {
// 			$("#errMsg").html("Đơn vị chọn không phải là một NPP cụ thể").show();
// 			$("#isNPP").val(0);
// 		} else {
// 			$("#isNPP").val(1);
// 		}
		$('#saleStaffCode').val('');
		$('#customerCode').val('');
		$('#customerName').val('');
	});
	$('#saleStaffCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			var t = $('#shop').combotree('tree');
			var node = t.tree('find', $('#shop').combotree('getValue'));
			if (node == undefined || node == null || node.isLevel == undefined || node.isLevel == null || node.isLevel != 5) {
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
				return;
			}
			VCommonJS.showDialogSearch2WithCheckbox({
				params : {
					shopId : $('#shop').combotree('getValue'),
					lstObjectType : [StaffRoleType.NVBH, StaffRoleType.NVVS].join(","),
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Nhân viên'},
			        {id:'name', maxlength:250, label:'Tên Nhân viên'},
			    ],
			    url : '/commons/search-staff-show-list',
			    columns : [[
			        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'cb', checkbox:true, align:'center', width:40,sortable : false,resizable : false}
			    ]],
			    chooseCallback: function(lstObj) {
			    	if (!lstObj || lstObj.length == 0) {
			    		return;
			    	}
			    	var s = "";
			    	for (var i = 0, sz = lstObj.length; i < sz; i++) {
			    		s = s + "," + lstObj[i].staffCode;
			    	}
			    	s = s.replace(",", "");
			    	$("#saleStaffCode").val(s);
			    	$(".easyui-dialog").dialog("close");
			    	//$('#customerCode').val('');
			    	//$('#customerName').val('');
			    }
			});
		}
	});
	$('#customerCode').bind('keyup', function(event) {
		$(".ErrorMsgStyle").hide();
		if (event.keyCode == keyCode_F9) {
			var t = $('#shop').combotree('tree');
			var node = t.tree('find', $('#shop').combotree('getValue'));
			if (node == undefined || node == null || node.isLevel == undefined || node.isLevel == null || node.isLevel != 5) {
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
				return;
			} else {
				VCommonJS.showDialogSearch2({
				    inputs: [
				        {id:'code', maxlength:50, label:'Mã KH'},
				        {id:'name', maxlength:250, label:'Tên KH'},
				        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
				    ],
				    params:{
				    	staffCode : $('#saleStaffCode').val().trim(),
				    	shopId: $('#shop').combotree('getValue')
				    },
				    url: '/commons/search-Customer-Show-List-For-Routing',
				    onShow: function() {
			        	$('.easyui-dialog #code').focus();
				    },
				    columns: [[
				        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false,formatter:function(v,r,i){
				        	return Utils.XSSEncode(v);
    			    	}},
				        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false,formatter:function(v,r,i){
				        	return Utils.XSSEncode(v);
    			    	}},
				        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false,formatter:function(v,r,i){
				        	return Utils.XSSEncode(v);
    			    	}},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				        	return "<a href='javascript:void(0)' onclick='chooseCustomer(\""+ Utils.XSSEncode(row.shortCode) + "\", \""+Utils.XSSEncode(row.customerName)+"\");'>Chọn</a>";        
				        }}
				    ]]
				});
			}
		}
	});
	
});

function changeSaleStaffCode(){
	$('#customerCode').val('');
	 $('#customerName').val('');
	return true;
}
function chooseCustomer(code, name) {
	$("#customerCode").val(code);
	$("#customerName").val(name);
	$(".easyui-dialog").dialog("close");
}
</script>
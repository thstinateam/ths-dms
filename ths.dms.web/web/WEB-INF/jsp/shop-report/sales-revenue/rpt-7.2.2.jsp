<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt><label class="LabelStyle Label1Style">Loại <span class="RequireStyle">(*)</span></label></dt>
        <dd>
        	<div class="BoxSelect BoxSelect1">
        		<select id="type" class="MySelectBoxClass">
        			<option value="0">Theo NVBH</option>
        			<option value="2">Theo NVBH Chi tiết</option>
        			<option value="1">Theo Sản Phẩm</option>
        		</select>
        	</div>
        </dd>
        <dt></dt>
        <dd></dd>
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVBH (F9)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Kiểu file
			</label>
		</dt>
		<dd>
            <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
            <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
        </dd>
	</dl>
	<!-- me -->
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="SalesRevenueReport.exportDSTHNVBHTN();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	$('#type').customStyle();
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(shopId){
		$('#staffCode').val('');
	});
	$('#staffCode').bind('keyup', function(event) {
		if(event.keyCode == keyCode_F9){
			if($('#isNPP').val() == "false"){
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
			}else{
				VCommonJS.showDialogSearch2WithCheckbox({
	    		    inputs : [
	    		        {id:'code', maxlength:50, label:'Mã NV'},
	    		        {id:'name', maxlength:250, label:'Tên NV'}
	    		    ],
	    		    chooseCallback : function(listObj) {
	    		    	console.log(listObj);
	    		        var lstStaffCode = '';
	    		        if($.isArray(listObj) && listObj.length > 0) {
	    		        	for(var i = 0; i < listObj.length; i++) {
	        					if(lstStaffCode.length > 0) {
	        						lstStaffCode+=",";
	        					}
	        					lstStaffCode+=listObj[i].staffCode;
	        				}
	    		        }
	    		        $('#staffCode').val(lstStaffCode);
	    		        $('#common-dialog-search-2-textbox').dialog('close');
	    		    },
	    		    url : '/commons/search-staff-show-list',
	    		    params : {
						shopId : $('#shop').combotree('getValue'),
						objectType : StaffRoleType.NVBHANDNVVS,
						//arrIdStr : shopId.toString();
						status : 1
					},
	    		    columns : [[
	    		        {field:'staffCode', title:'Mã NV', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
	    		        	return Utils.XSSEncode(value);         
	    		        }},
	    		        {field:'staffName', title:'Tên NV', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index) {
	    		        	return Utils.XSSEncode(value);         
	    		        }},
	    		        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
	    		    ]]
	    		});
			}
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate', '01');
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd class="widthComboTree">
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">Khách hàng(F9)</label>
		</dt>
		<dd>
			<input type="text" id="customerCode" class="InputTextStyle InputText1Style" />
		</dd>
		
		<dt>
			<label class="LabelStyle Label1Style">Ngành hàng</label>
		</dt>
		<dd>
             <div class="BoxSelect BoxSelect2" id="divCategory">
               <select id="category" class="MySelectBoxClass" multiple="multiple">
					<option value="-1" selected="selected">Tất cả</option>
					<s:iterator value="lstCategory">
						<option value="<s:property value="productInfoCode"/>"><s:property value="productInfoCode"/>-<s:property value="productInfoName"/></option>
					</s:iterator>
				</select>
            </div>
		</dd>
		
		
		<dt>
			<label class="LabelStyle Label1Style">Sản Phẩm(F9)</label>
		</dt>
		<dd>
			<input type="text" id="productCode" class="InputTextStyle InputText1Style" />
		</dd>
		
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		
		
		<dt class="LabelTMStyle">
                     <label class="LabelStyle Label1Style">Kiểu File</label>
         </dt>
         <dd>
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
         </dd>
	</dl>
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="SalesRevenueReport.exportDSKHTMH();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>

<s:hidden id="shopId"></s:hidden>
<s:hidden id="customerCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree2('shop','shopId',$('#curShopId').val(),function(node){
		$(".ErrorMsgStyle").hide();
		if (node.isLevel != 5) {
			$("#errMsg").html("Đơn vị chọn không phải là một NPP cụ thể").show();
			$("#isNPP").val(0);
		} else {
			$("#isNPP").val(1);
		}
		$('#customerCode').val('');
	});
	$('#customerCode').bind('keyup', function(event) {
		$(".ErrorMsgStyle").hide();
		if (event.keyCode == keyCode_F9) {
			if($('#isNPP').val() == 0){
				$('#errMsg').html('Đơn vị chọn không phải là một NPP cụ thể').show();
			}else{
				VCommonJS.showDialogSearch2WithCheckbox({
				    inputs: [
				        {id:'code', maxlength:50, label:'Mã KH'},
				        {id:'name', maxlength:250, label:'Tên KH'},
				        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
				    ],
				    params:{shopId: $('#shop').combotree('getValue')},
				    url: '/commons/customer-in-shop/search',
				    onShow: function() {
			        	$('.easyui-dialog #code').focus();
				    },
				    columns: [[
				        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter: function(v, r, i){
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter: function(v, r, i){
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter: function(v, r, i){
				        	if (v) {
				        		return Utils.XSSEncode(v);
				        	}
				        	return "";
				        }},
				        {field:'cb', checkbox:true, align:'center', width:40,sortable : false,resizable : false},
				    ]],
				    chooseCallback: function(lstObj) {
				    	if (!lstObj || lstObj.length == 0) {
				    		return;
				    	}
				    	var s = "";
				    	for (var i = 0, sz = lstObj.length; i < sz; i++) {
				    		s = s + "," + Utils.XSSEncode(lstObj[i].shortCode);
				    	}
				    	s = s.replace(",", "");
				    	$("#customerCode").val(s);
				    	$(".easyui-dialog").dialog("close");
				    }
				});
			}
		}
	});
	$('#productCode').bind('keyup', function(event) {
		$(".ErrorMsgStyle").hide();
		if (event.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2WithCheckbox({
			    inputs: [
			        {id:'code', maxlength:50, label:'Mã SP'},
			        {id:'name', maxlength:250, label:'Tên SP'},
			    ],
			    params:{lstCategoryCodes: $('#category').val().join(",")},
			    url: '/commons/search-product',
			    onShow: function() {
		        	$('.easyui-dialog #code').focus();
			    },
			    columns: [[
			        {field:'productCode', title:'Mã SP', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'productName', title:'Tên SP', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'ck', checkbox:true, align:'center', width:40,sortable : false,resizable : false},
			    ]],
			    chooseCallback: function(lstObj) {
			    	if (!lstObj || lstObj.length == 0) {
			    		return;
			    	}
			    	var s = "";
			    	for (var i = 0, sz = lstObj.length; i < sz; i++) {
			    		s = s + "," + Utils.XSSEncode(lstObj[i].productCode);
			    	}
			    	s = s.replace(",", "");
			    	$("#productCode").val(s);
			    	$(".easyui-dialog").dialog("close");
			    }
			});
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate', '01');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$('.combo-arrow').css("width","200px");
	$('.combo-arrow').click(function(){
		$('.combo-p').css("width","230px");
		$('.combo-p .panel-body').css("width","225px");
	});
	
	$('#category').dropdownchecklist({emptyText:'Tất cả',firstItemChecksAll: true});
});
</script>
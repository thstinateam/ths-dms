<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dl class="Dl3Style">
	<dt><label class="LabelStyle Label1Style">Đơn vị<span class="RequireStyle">(*)</span></label></dt>
	<dd><input type="text" style="width: 226px" class="InputTextStyle InputText1Style" id="shop"  /></dd>
	
	<dt class="ClearLeft">
		<label class="LabelStyle Label1Style">Loại báo cáo</label>
	</dt>
     <dd class="BoxSelect BoxSelect2" >
         <select class="MySelectBoxClass" id="type">
			<option value="1" selected="selected" >Tổng hợp trả hàng trưng bày</option>
			<option value="2">Chi tiết trả hàng trưng bày</option>
         </select>
     </dd>
	
	<dt>
		<label class="LabelStyle Label1Style">CTTB (F9)</label>
	</dt>
	<dd>
		<input id="multiChoiceCTTB" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
	</dd>
	<dt class="LabelTMStyle">
		<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
	</dd>
	<dt class="ClearLeft LabelTMStyle">
         <label class="LabelStyle Label1Style">Kiểu File
         </label>
    </dt>
    <dd>
         <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
         <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
    </dd>
	
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="SalesRevenueReport.exportTTCTTB();">Xuất báo cáo</button>
</div>

<div id="searchStyleDisPlayProForTreeP" style="display: none;" class="easyui-dialog">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
				<label id="seachStyleDisPlayProCodeLabel" class="LabelStyle Label2Style">Mã CTTB</label>
				<input id="seachStyleDisPlayProCode" maxlength="50" tabindex="100" type="text" class="InputTextStyle InputText5Style" />
				<label id="seachStyleDisPlayProNameLabel" class="LabelStyle Label2Style">Tên CTTB</label>
				<input id="seachStyleDisPlayProName" type="text" maxlength="250" tabindex="101" class="InputTextStyle InputText5Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" id="btnSearchCTTB" onclick="return ReportUtils.searchMultiDisplayPro();" tabindex="102" id="btnSearchStyleDisPlayPro">
						<span class="Sprite2">Tìm kiếm</span>
					</button>
				</div>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle">Danh sách tìm kiếm</h2>
				<div class="GridSection" id="searchStyleContainerGrid">
					<table id="searchCTTBGrid"></table>
				</div>
			</div>
			<div class="Clear"></div>
			<div id="bottomDisplayProDialog" class="SearchInSection SProduct1Form">
				<div class="BtnCenterSection">
<!-- 					<button id="btnSelectProgramDisplay" tabindex="103" class="BtnGeneralStyle" onclick="return ReportUtils.multiChoiceDisplayPro();">Chọn</button> -->
<!-- 					<button id="btnOnCloseDialogDisplayPro" tabindex="104" class="BtnGeneralStyle" onclick="return ReportUtils.onCloseDialogSearchMultiDisplayPro();">Bỏ qua</button> -->
					
					<button id="btnSelectProgramDisplay" tabindex="103" class="BtnGeneralStyle" >Chọn</button>
					<button id="btnOnCloseDialogDisplayPro" tabindex="104" class="BtnGeneralStyle" onclick="$(\'#searchStyleDisPlayProForTreeP\').dialog(\'close\');">Bỏ qua</button>
					
				</div>
				<div class="Clear"></div>
				<p id="errMsgDialogDisplayPro" class="ErrorMsgStyle" style="display: none; margin-top: 10px; float: left; text-align: left;" />
				<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
			</div>
		</div>
</div>

<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="curShopId2" name="shopId"></s:hidden>
<s:hidden id="multiChoiceCTTBHiddent"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	$('.CustomStyleSelectBox').css('width','192');
	$('.MySelectBoxClass').css('width', '225');
	var isLoad=1;
	ReportUtils.loadComboTree2('shop','shopCode',$('#curShopId').val(),function(node){
		$(".ErrorMsgStyle").hide();
		if (node.isLevel != 5) {
			if(isLoad!=1){
				$("#errMsg").html("Đơn vị chọn không phải là một NPP cụ thể").show();
			}
			isLoad=0;
			$("#isNPP").val(0);
		} else {
			$("#isNPP").val(1);
		}
		$('#saleStaffCode').val('');
		$('#customerCode').val('');
		$('#customerName').val('');
	});
	ReportUtils._lstChecker = new Array();
	ReportUtils._lstUnchecker = new Array();
	ReportUtils._arrMultiChoiceDisplay = new Array();
	ReportUtils._isCheckAll = false;
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.setCurrentDateForCalendar('fDate','01');
	ReportUtils.setCurrentDateForCalendar('tDate');

	
	$('#multiChoiceCTTB').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2WithCheckbox({
				params : {
					shopId : $('#shop').combotree('getValue'),
					fromDate : $('#fDate').val().trim(),
					toDate : $('#tDate').val().trim(),
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã CTTB'},
			        {id:'name', maxlength:250, label:'Tên CTTB'},
			    ],
			    chooseCallback : function(listObj) {
			        console.log(listObj);
			        if (listObj != undefined && listObj != null && listObj.length > 0) {
			        	var arrCodeStr = listObj[0].displayProgramCode.trim();
			        	for (var i = 1; i < listObj.length; i++) {
			        		arrCodeStr = Utils.XSSEncode(arrCodeStr.trim() + ', ' + listObj[i].displayProgramCode.trim());
			        	}
			        	$('#multiChoiceCTTB').val(Utils.XSSEncode(arrCodeStr.trim()));
			        } else {
			        	$('#multiChoiceCTTB').val("");
			        }
			        $('#common-dialog-search-2-textbox').dialog("close");
			    },
			    url : '/commons/search-in-tow-day',
			    columns : [[
			        {field: 'displayProgramCode', title: 'Mã CTTB', sortable: false, resizable: false, width: 60, align: 'left',
							formatter: function(val, row, idx) {
								if (val != undefined && val != null) {
									return Utils.XSSEncode(val);
								}
								return '';
							}
						},
						{field: 'displayProgramName', title: 'Tên CTTB', sortable: false, resizable: false, width: 130, align: 'left',
							formatter: function(val, row, idx) {
								if (val != undefined && val != null) {
									return Utils.XSSEncode(val);
								}
								return '';
							}
						},
						{field: 'times', title: 'Thời gian', sortable: false, resizable: false, width: 100, align: 'left',
							formatter: function(val, row, idx) {
								return GridFormatterUtils.dateFromdateTodate(row.fromDate, row.toDate);
							}
						},
			        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false}
			    ]]
			});
		}
	});
	
// 	$('#multiChoiceCTTB').bind('keyup', function(event) {		
// 		if (event.keyCode == keyCode_F9) {
// 			var urlParamater = '/commons/search-in-tow-day';
// 			ReportUtils.openDialogSearchCTTB(urlParamater);
// 		}
// 	});
});
</script>          	
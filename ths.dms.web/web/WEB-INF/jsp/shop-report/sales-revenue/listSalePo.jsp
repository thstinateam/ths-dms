<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
   <label class="LabelStyle Label1Style">Đơn vị</label>                
   <div class="BoxSelect BoxSelect2">
       <div class="Field2">
			<input id="shop" style="width:200px;">
			<span class="RequireStyle">(*)</span>
     </div>
 </div>
 <label class="LabelStyle Label2Style">NVBH(F9)</label>                
 <input id="staffSaleCode" type="text" class="InputTextStyle InputText1Style" style="width:190px;" onChange="return SuperviseCustomer.changeMyMap(2)"/>              
 <div class="Clear"></div>
 <label class="LabelStyle Label1Style">Từ ngày</label>
 <div class="Field2">
 	<input id="fDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
 	<span class="RequireStyle">(*)</span>
 </div>
 <label class="LabelStyle Label2Style">Đến ngày</label>
 <div class="Field2">
  <input id="tDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
  <span class="RequireStyle">(*)</span>
 </div>
 <div class="Clear"></div> 
 <div class="ButtonSection">                	
 	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return salePo.PoAdd();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 </div>
 <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
</div>
            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="kieuTruyen2"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	SuperviseCustomer.mapNVBH = new Map();
	$('#staffSaleCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			var arrParam = new Array();
			var param = new Object();
			param.name = 'shopId';
			param.value = $('#shop').combotree('getValue');
			arrParam.push(param);
			var param1 = new Object();
			param1.name = "caseBC";
			param1.value = "yes";
			arrParam.push(param1);
			SuperviseCustomer.mySelectfancybox(2,"Mã nhân viên", "Tên nhân viên",
			"Tìm kiếm nhân viên bán hàng", "/commons/sale-staff/search", function(){
			},"staffCode", "staffName", arrParam);
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate', '01');
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
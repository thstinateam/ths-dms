﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<s:if test="NPP.equals(\"NPP\")" >
				<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" disabled='disabled'/>
			</s:if>			
			<s:else>
    			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
			</s:else>
			
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVBH(F9)
			</label>
		</dt>
		<dd>
			<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
	</dl>

 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="SalesRevenueReport.exportBCDSDHTNTNVBH_DS31();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>
<s:hidden id="shopId"></s:hidden>            
<s:hidden id="shopCode"></s:hidden>
<s:hidden id="shopName" name = 'shopCurrent'></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="customerCode"></s:hidden>
<s:hidden id="isNPP" name = "NPP"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	
	ReportUtils.loadComboTree('shop','shopCode',$('#curShopId').val());	
		
	$('#staffCode').bind('keyup', function(event) 
	{
						
		if (event.keyCode == keyCode_F9) 
		{
			if($('#isNPP').val().trim()==="false"){
				msg = 'Đơn vị chọn phải là nhà phân phối';
				$('#errMsg').html(msg).show();
			}else{
				$('#errMsg').html('').hide();
				var arrParam = new Array();
				var param = {};
				param.name = "shopId";
				param.value = $('#shop').combotree('getValue');
				arrParam.push(param);
				ReportUtils.showDialogSearchNVBH(param.value);
			}
			/*var shopID =  $('#reportContainer input.combo-value').val().trim();
			$.getJSON("/report/are-you-shop?shopId="+shopID,function(result)
			{
				if(!result.isNPP)
				{
				}
				else
				{
				}
			});*/

		}
	
	});
	ReportUtils.setCurrentDateForCalendar('fDate', '01');
	ReportUtils.setCurrentDateForCalendar('tDate');
	
	var d = new Date();
	d.setDate(1);
	var stringfDate = ('0' + d.getDate()) + '/' + (d.getMonth() + 1) +  '/' + (d.getYear() + 1900);
	
	$('#fDate').val(stringfDate);
		
});
</script>
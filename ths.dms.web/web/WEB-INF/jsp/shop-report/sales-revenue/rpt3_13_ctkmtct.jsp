<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" disabled="disabled" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:if>
		<s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
    		
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVBH(F9)
			</label>
		</dt>
		<dd>
			<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Kiểu file
			</label>
		</dt>
		<dd>
            <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
            <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
        </dd>
	</dl>
	<!-- me -->
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="SalesRevenueReport.exportCTKMTCT();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	isOpenJAlert = false;
	isFromBKXNKNVBH = true;
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(shopId){
		$('#staffCode').val('');
	});
	
	$(window).bind('keypress',function(event){
		if($('.fancybox-inner').length!=0){
			if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
				Images.showImageNextPre(0);
			}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
				Images.showImageNextPre(1);
			}
			return false;
		}
		return true;
	});
	
	$('#staffCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			//var multiShop = $("#shop").data("kendoMultiSelect");
			$('#errMsg').html('');
			if($('#isNPP').val() == "false"){
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
				return false;
			}
			var lstShopId = $('#shop').combotree('getValue');
			VCommonJS.showDialogSearch2WithCheckbox({
				params : {						
					objectType : StaffRoleType.NVBH,
					arrIdStr: lstShopId,						
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Nhân viên'},
			        {id:'name', maxlength:250, label:'Tên Nhân viên'},
			    ],
			    chooseCallback : function(listObj) {				        
			        if (listObj != undefined && listObj != null && listObj.length > 0) {
			        	var staffCode = listObj[0].staffCode.trim();
			        	for (var i = 1; i < listObj.length; i++) {
			        		staffCode = Utils.XSSEncode(staffCode.trim() + ', ' + listObj[i].staffCode.trim());
			        	}
			        	$('#staffCode').val(Utils.XSSEncode(staffCode.trim()));
			        } else {
			        	$('#staffCode').val("");
			        }
			        $('#common-dialog-search-2-textbox').dialog("close");
			    },
			    url : '/commons/search-staff-show-list',
			    columns : [[
			        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
			    ]]
			});
		}
	});
	/* 
	$('#staffCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			if($('#isNPP').val() == "false"){
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
			}else{
				$('#errMsg').hide();
				ReportUtils.showDialogSearchNVBH($('#shop').combotree('getValue'));
			}
		}
	}); */
});

</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" disabled="disabled" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:if>
		<s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
    		
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">Ngành hàng(F9)
			</label>
		</dt>
		<dd>
			<input type="text" id="catCode" class="InputTextStyle InputText1Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
	</dl>
	<!-- me -->
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="SalesRevenueReport.exportBTHDN();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	isOpenJAlert = false;
	isFromBKXNKNVBH = true;
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.setCurrentDateForCalendar('fDate','01');
	ReportUtils.setCurrentDateForCalendar('tDate');
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(shopId){
		//$('#staffCode').val('');
	});
	$('#catCode').bind('keyup', function(event){
		if (event.keyCode == keyCode_F9) {
			var arrParam = new Array();
			CommonSearch.searchCategoryOnDialog(function(data) {
				$('#catCode').val(data.code);
			}, arrParam);
		}
	});
});

</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt><label class="LabelStyle Label1Style">Đơn vị (F9)</label></dt>
        <dd>
        	<s:if test="%{NPP=='NPP'}">
        		<input id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style"> <span class="RequireStyle">(*)</span>
        	</s:if>
        	<s:else>
        		<input id="shop" style="width:226px;" class="InputTextStyle InputText1Style"> <span class="RequireStyle">(*)</span>
        	</s:else>
        </dd>
        
        <dt><label class="LabelStyle Label1Style">NVBH(F9)</label></dt>
		<dd><input type="text" id="staffCode" class="InputTextStyle InputText1Style" /></dd>
        
        <dt><label class="LabelStyle Label1Style">Ngày</label></dt>
        <dd><input id="fDate" style="width: 196px;" class="InputTextStyle InputText1Style"> <span class="RequireStyle">(*)</span></dd>
		
		<dt>&nbsp;</dt>
		<dd>&nbsp;</dd>
		
		<dt class="LabelTMStyle"><label class="LabelStyle Label1Style">Kiểu File</label></dt>
        <dd>
           <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
           <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
        </dd>
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="SalesRevenueReport.exportTDBH8();">Xuất báo cáo</button>
	</div>	
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){
	setDateTimePicker('fDate');
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(), function(shopId){
		$('#staffCode').val('');
	});
	$('#staffCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			if($('#isNPP').val() == "false"){
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
			}else{
				$('#errMsg').hide();
				//ReportUtils.showDialogSearchNVBH($('#shop').combotree('getValue'));
				VCommonJS.showDialogSearch2WithCheckbox({
					params : {						
						lstObjectType : StaffRoleType.NVBH+","+StaffRoleType.NVVS,
						shopId: $('#shop').combotree('getValue'),						
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã Nhân viên'},
				        {id:'name', maxlength:250, label:'Tên Nhân viên'},
				    ],
				    chooseCallback : function(listObj) {				        
				        if (listObj != undefined && listObj != null && listObj.length > 0) {
				        	var staffCode = listObj[0].staffCode.trim();
				        	for (var i = 1; i < listObj.length; i++) {
				        		staffCode = Utils.XSSEncode(staffCode.trim() + ', ' + listObj[i].staffCode.trim());
				        	}
				        	$('#staffCode').val(Utils.XSSEncode(staffCode.trim()));
				        } else {
				        	$('#staffCode').val("");
				        }
				        $('#common-dialog-search-2-textbox').dialog("close");
				    },
				    url : '/commons/search-staff-show-list',
				    columns : [[
				        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
				    ]]
				});
			}
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate');
});
</script>
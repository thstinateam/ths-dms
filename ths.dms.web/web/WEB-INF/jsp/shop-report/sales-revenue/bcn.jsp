<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
			<s:if test="%{NPP=='NPP'}">
				<input type="text" id="shop" disabled="disabled" style="width:238px;" class="InputTextStyle InputText1Style" />
			</s:if>
			<s:else>
				<input type="text" id="shop" style="width:238px;" class="InputTextStyle InputText1Style" />
			</s:else>
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Ngày báo cáo<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle"><label class="LabelStyle Label1Style">Kiểu File</label></dt>
	    <dd>
	         <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
	         <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
	    </dd>
	</dl>
	<!-- me -->
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="SalesRevenueReport.exportBCN();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>
            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<input type="hidden" id="hideMSG" value="1" />

<script type="text/javascript">
$(document).ready(function(){
	setDateTimePicker('fDate');
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.loadComboTree2('shop','shopId',$('#curShopId').val(),function(node){
		$(".ErrorMsgStyle").hide();
		if (node.isLevel != 5) {
			if ($("#hideMSG").val().trim() == 0) {
				$("#errMsg").html("Đơn vị chọn không phải là một NPP cụ thể").show();
			} else {
				$("#hideMSG").val(0);
			}
			$("#isNPP").val(0);
		} else {
			$("#isNPP").val(1);
		}
	});
	$(".ErrorMsgStyle").hide();
});

</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="ModuleList3Form">
<dl class="Dl3Style">
	<dt><label class="LabelStyle Label1Style">Mã NPP<span class="RequireStyle">(*)</span></label></dt>
	<dd><input type="text" style="width: 226px" class="InputTextStyle InputText1Style" id="shop"  /></dd>
	
	<dt>
		<label class="LabelStyle Label1Style">Năm <span class="RequireStyle">(*)</span></label>
	</dt>
	<dd class="ClearRight">
		<div class="BoxSelect BoxSelect1">
			<select id="fullYearIn10" class="MySelectBoxClass" onchange=""></select>
		</div>
	</dd>
	<dt class="ClearLeft">
		<label class="LabelStyle Label1Style">CTTB (F9) <span class="RequireStyle">(*)</span></label>
	</dt>
	<dd>
		<!-- <input id="multiChoiceCTTB" type="text" class="InputTextStyle InputText1Style" readonly="readonly" /> -->
		<input id="multiChoiceCTTB" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">Mã KH (F9)</span>
		</label>
	</dt>
	<dd>
		<!-- <input type="text" id="customerCode" class="InputTextStyle InputText1Style" readonly="readonly" /> -->
		<input type="text" id="customerCode" class="InputTextStyle InputText1Style" />
	</dd>
</dl>
<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="SalesRevenueReport.exportDSTMCTTB();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>

<div id="searchStyleDisPlayProForTreeP" style="display: none;">
	<div id="searchStyleDisPlayProDL" class="easyui-dialog" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">
				<label id="seachStyleDisPlayProCodeLabel" class="LabelStyle Label2Style">Mã CTTB</label>
				<input id="seachStyleDisPlayProCode" maxlength="50" tabindex="100" type="text" class="InputTextStyle InputText5Style" />
				<label id="seachStyleDisPlayProNameLabel" class="LabelStyle Label2Style">Tên CTTB</label>
				<input id="seachStyleDisPlayProName" type="text" maxlength="250" tabindex="101" class="InputTextStyle InputText5Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" onclick="return ReportUtils.searchMultiDisplayPro();" tabindex="102" id="btnSearchStyleDisPlayPro">
						<span class="Sprite2">Tìm kiếm</span>
					</button>
				</div>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle">Danh sách tìm kiếm</h2>
				<div class="GridSection" id="searchStyleContainerGrid">
					<table id="searchCTTBGrid"></table>
				</div>
			</div>
			<div class="Clear"></div>
			<div id="bottomDisplayProDialog" class="SearchInSection SProduct1Form">
				<div class="BtnCenterSection">
					<button id="btnSelectProgramDisplay" tabindex="103" class="BtnGeneralStyle" onclick="return ReportUtils.multiChoiceDisplayPro();">Chọn</button>
					<button id="btnOnCloseDialogDisplayPro" tabindex="104" class="BtnGeneralStyle" onclick="return ReportUtils.onCloseDialogSearchMultiDisplayPro();">Bỏ qua</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgDialogDisplayPro" class="ErrorMsgStyle" style="display: none; margin-top: 10px; float: left; text-align: left;" />
				<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
			</div>
		</div>
	</div>
</div>

<s:hidden id ="shopId"></s:hidden>
<s:hidden id ="maCTTB"></s:hidden>
<s:hidden id ="curShopId" name="shopId"></s:hidden>
<s:hidden id="multiChoiceCTTBHiddent"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	var n = new Date().getFullYear();
	for(var i = n; i>=(n-10); i--) {
		$("#fullYearIn10").append('<option value="'+ i +'"> '+ i +'</option>');
	}
	$("#fullYearIn10").change();
	$('.MySelectBoxClass').customStyle('');
	ReportUtils.loadComboTree('shop','shopId', $('#curShopId').val(), function(shopCode){
		$('#customerCode').val('');
		$('#multiChoiceCTTB').val('');
	});
	$('#multiChoiceCTTB').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9) {
			if($('#isNPP').val() == "false") {
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
			} else {
				//CRMReportDSPPTNH.openDialogSearchCTTB();
				ReportUtils._objectSearchCTTB = new Object();
				ReportUtils._objectSearchCTTB.year = encodeChar($('#fullYearIn10').val());
				ReportUtils._objectSearchCTTB.shopId = $('#shop').combotree('getValue');
				var urlParamater = '/programme-display/search-Display-Program-By-Year';
				ReportUtils.openDialogSearchCTTB(urlParamater, 'searchStyleDisPlayProDL');
			}
		}
	});
	/* $('#multiChoiceCTTB').bind('keyup', function(event) {		
		if (event.keyCode == keyCode_F9) {
			CRMReportDSPPTNH.openDialogSearchCTTB();
		}
	}); */
	$('#customerCode').bind('keyup', function(event) {		
		if (event.keyCode == keyCode_F9) {
			var arrParam = new Array();
			var param = {};
			param.name = "shopId";
			param.value = $('#shop').combotree('getValue');
			arrParam.push(param);
				CommonSearch.searchCustomerByShopOnDialog(function(data) {
					$('#customerCode').val(data.code);
//					$('#customerName').val(data.name);
				}, arrParam);
		}
	});
	$('#fullYearIn10').change(function(){
		$('#multiChoiceCTTB').val("").show();
		CRMReportDSPPTNH._lstChecker = new Array();
		CRMReportDSPPTNH._lstUnchecker = new Array();
		CRMReportDSPPTNH._isCheckAll = false;
	});
	
	$('#multiChoiceCTTB').change(function(){
		$('#multiChoiceCTTB').val($('#multiChoiceCTTBHiddent').val()).show();
	});
});
</script>

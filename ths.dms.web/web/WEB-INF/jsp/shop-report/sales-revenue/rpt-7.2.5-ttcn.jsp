<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<%-- <dt><label class="LabelStyle Label1Style">Mã NPP (F9) <span class="RequireStyle">(*)</span></label></dt>
        <dd>
        	<s:if test="%{NPP=='NPP'}">
        		<input id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style">
        	</s:if>
        	<s:else>
        		<input id="shop" style="width:226px;" class="InputTextStyle InputText1Style">
        	</s:else>
        </dd> --%>
        
        <dt><label class="LabelStyle Label1Style">Loại <span class="RequireStyle">(*)</span></label></dt>
        <dd>
        	<div class="BoxSelect BoxSelect1">
        		<select id="type" class="MySelectBoxClass">
        			<option value="0">Theo NVTT</option>
        			<option value="1">Theo KH</option>
        			<option value="2">Theo phiếu thu</option>
        		</select>
        	</div>
        </dd>
        
        <dt><label class="LabelStyle Label1Style">NVBH (F9)</label></dt>
		<dd><input type="text" id="staffCode" style="width:216px;" class="InputTextStyle InputText1Style" /></dd>
		
		<dt><label class="LabelStyle Label1Style">Khách hàng (F9)</label></dt>
		<dd><input type="text" id="customerCode" style="width:216px;" class="InputTextStyle InputText1Style" /></dd>
		
		<dt><label class="LabelStyle Label1Style">NVGH (F9)</label></dt>
		<dd><input type="text" id="delivery" style="width:216px;" class="InputTextStyle InputText1Style" /></dd>
        
        <dt><label class="LabelStyle Label1Style">Từ ngày <span class="RequireStyle">(*)</span></label></dt>
        <dd><input id="fDate" style="width: 196px;" class="InputTextStyle InputText1Style"></dd>
        
        <dt><label class="LabelStyle Label1Style">Đến ngày <span class="RequireStyle">(*)</span></label></dt>
        <dd><input id="tDate" style="width: 196px;" class="InputTextStyle InputText1Style"></dd>
        <!-- <dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Kiểu file
			</label>
		</dt> -->
		 <dt><label class="LabelStyle Label1Style">Dữ liệu công nợ </label></dt>
        <dd style="margin-top:3px"><!-- <input id="fDate" style="width: 196px;" class="InputTextStyle InputText1Style"> -->
        	<input type="checkbox" id="checkExport" value="0" >
        </dd>
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="SalesRevenueReport.exportTDTTCN();">Xuất báo cáo</button>
	</div>	
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	$('#type').customStyle();
	
	$(window).bind('keypress',function(event){
		if($('.fancybox-inner').length!=0){
			if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
				Images.showImageNextPre(0);
			}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
				Images.showImageNextPre(1);
			}
			return false;
		}
		return true;
	});
	
	$('#staffCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			var lstShopId = $('#curShopId').val();
			VCommonJS.showDialogSearch2WithCheckbox({
				params : {						
					objectType : StaffRoleType.NVBHANDNVVS,
					arrIdStr: lstShopId,						
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Nhân viên'},
			        {id:'name', maxlength:250, label:'Tên Nhân viên'},
			    ],
			    chooseCallback : function(listObj) {				        
			        if (listObj != undefined && listObj != null && listObj.length > 0) {
			        	var staffCode = listObj[0].staffCode.trim();
			        	for (var i = 1; i < listObj.length; i++) {
			        		staffCode = Utils.XSSEncode(staffCode.trim() + ', ' + listObj[i].staffCode.trim());
			        	}
			        	$('#staffCode').val(Utils.XSSEncode(staffCode.trim()));
			        } else {
			        	$('#staffCode').val("");
			        }
			        $('#common-dialog-search-2-textbox').dialog("close");
			    },
			    url : '/commons/search-staff-show-list',
			    columns : [[
			        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
			    ]]
			});
		}
	});
	$('#delivery').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			var lstShopId = '';
			VCommonJS.showDialogSearch2WithCheckbox({
				params : {						
					objectType : StaffRoleType.NVGH, // 2 loai nay duoc xu ly tren action
					arrIdStr: lstShopId,
					//lstObjectType: '3,4', // StaffRoleType.NVTT:3,StaffRoleType.NVGH:4
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Nhân viên'},
			        {id:'name', maxlength:250, label:'Tên Nhân viên'},
			    ],
			    chooseCallback : function(listObj) {				        
			        if (listObj != undefined && listObj != null && listObj.length > 0) {
			        	var staffCode = listObj[0].staffCode.trim();
			        	for (var i = 1; i < listObj.length; i++) {
			        		staffCode = Utils.XSSEncode(staffCode.trim() + ', ' + listObj[i].staffCode.trim());
			        	}
			        	$('#delivery').val(Utils.XSSEncode(staffCode.trim()));
			        } else {
			        	$('#delivery').val("");
			        }
			        $('#common-dialog-search-2-textbox').dialog("close");
			    },
			    url : '/commons/search-staff-show-list',
			    columns : [[
			        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
			    ]]
			});
		}
	});
	$('#customerCode').bind('keyup', function(event) {
		$(".ErrorMsgStyle").hide();
		if (event.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2WithCheckbox({
			    inputs: [
			        {id:'code', maxlength:250, label:'Khách hàng', width:410}
// 			        {id:'name', maxlength:250, label:'Tên KH'},
// 			        {id:'address', maxlength:250, label:'Địa chỉ', width:410}
			    ],
			    params:{},
			    url: '/commons/search-Customer-Show-List',
			    onShow: function() {
		        	$('.easyui-dialog #code').focus();
			    },
			    columns: [[
			        {field:'shortCode', title:'Mã KH', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'customerName', title:'Tên KH', align:'left', width: 150, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'address', title:'Địa chỉ', align:'left', width: 170, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'cb', checkbox:true, align:'center', width:40,sortable : false,resizable : false},
			    ]],
			    chooseCallback: function(lstObj) {
			    	if (!lstObj || lstObj.length == 0) {
			    		return;
			    	}
			    	var s = "";
			    	for (var i = 0, sz = lstObj.length; i < sz; i++) {
			    		s = s + "," + lstObj[i].shortCode;
			    	}
			    	s = s.replace(",", "");
			    	$("#customerCode").val(s);
			    	$(".easyui-dialog").dialog("close");
			    }
			});
		}
	});
	$('#checkExport').change(function(){
		if($('#checkExport').is(':checked')){
			$(this).val(1);
			disabled('type');
			//$('#type').attr('disabled',true);
		} else{
			$(this).val(0);
			enable('type');
			//$('#type').attr('disabled',false);
		}
	});	
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
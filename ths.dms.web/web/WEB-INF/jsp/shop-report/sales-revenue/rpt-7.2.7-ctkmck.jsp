<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt><label class="LabelStyle Label1Style">Đơn vị (F9)</label></dt>
        <dd>        
        	<s:if test="%{NPP=='NPP'}">
        		<input id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style"> <span class="RequireStyle">(*)</span>
        	</s:if>
        	<s:else>
        		<input id="shop" style="width:226px;" class="InputTextStyle InputText1Style"> <span class="RequireStyle">(*)</span>
        	</s:else>
        </dd>
        
        <dt></dt>
        <dd></dd>
        
        <dt><label class="LabelStyle Label1Style">Từ ngày</label></dt>
        <dd><input id="fDate" style="width: 196px;" class="InputTextStyle InputText1Style"> <span class="RequireStyle">(*)</span></dd>
        
        <dt><label class="LabelStyle Label1Style">Đến ngày</label></dt>
        <dd><input id="tDate" style="width: 196px;" class="InputTextStyle InputText1Style"> <span class="RequireStyle">(*)</span></dd>
		<dt class="LabelTMStyle">
                     <label class="LabelStyle Label1Style">Kiểu File</label>
         </dt>
         <dd>
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
         </dd>
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="SalesRevenueReport.exportTDBH7();">Xuất báo cáo</button>
	</div>	
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	var date = new Date();
	var m = date.getMonth() + 1;
	var y = date.getFullYear();
	//ReportUtils.setCurrentDateForCalendar('fDate');
	$('#fDate').val('01/' + (m < 10 ? '0' + m : m) + '/' + y);
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
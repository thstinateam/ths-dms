<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
			<s:if test="%{NPP=='NPP'}">
				<input type="text" id="shop" disabled="disabled" style="width:226px;" class="InputTextStyle InputText1Style" />
			</s:if>
			<s:else>
				<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
			</s:else>
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVBH(F9)</label>
		</dt>
		<dd>
			<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
               <label class="LabelStyle Label1Style">Kiểu File
               </label>
        </dt>
        <dd>
               <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
               <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
        </dd>
	</dl>
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="SalesRevenueReport.exportRpt10_1_2();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>

<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<input type="hidden" id="hideMSG" value="1" />

<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree2('shop','shopId',$('#curShopId').val(),function(node){
		$(".ErrorMsgStyle").hide();
		if (node.isLevel != 5) {
			if ($("#hideMSG").val().trim() == 0) {
				$("#errMsg").html("Đơn vị chọn không phải là một NPP cụ thể").show();
			} else {
				$("#hideMSG").val(0);
			}
			$("#isNPP").val(0);
		} else {
			$("#isNPP").val(1);
		}
		$('#staffCode').val('');
	});
	$(".ErrorMsgStyle").hide();
	$('#staffCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			if ($('#isNPP').val() == 0) {
				$('#errMsg').html('Đơn vị chọn không phải là một NPP cụ thể').show();
			} else {
				VCommonJS.showDialogSearch2WithCheckbox({
					params : {
						shopId : $('#shop').combotree('getValue'),
						lstObjectType : [StaffRoleType.NVBH, StaffRoleType.NVVS].join(","),
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã Nhân viên'},
				        {id:'name', maxlength:250, label:'Tên Nhân viên'},
				    ],
				    url : '/commons/search-staff-show-list',
				    columns : [[
				        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'cb', checkbox:true, align:'center', width:40,sortable : false,resizable : false}
				    ]],
				    chooseCallback: function(lstObj) {
				    	if (!lstObj || lstObj.length == 0) {
				    		return;
				    	}
				    	var s = "";
				    	for (var i = 0, sz = lstObj.length; i < sz; i++) {
				    		s = s + "," + Utils.XSSEncode(lstObj[i].staffCode);
				    	}
				    	s = s.replace(",", "");
				    	$("#staffCode").val(s);
				    	$(".easyui-dialog").dialog("close");
				    }
				});
			}
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt><label class="LabelStyle Label1Style">Đơn vị (F9)</label></dt>
        <dd><input id="shop" style="width: 200px;"> <span class="RequireStyle">(*)</span></dd>
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="SalesRevenueReport.exportDayReport();">Xuất báo cáo</button>
	</div>	
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){				
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
});
</script>
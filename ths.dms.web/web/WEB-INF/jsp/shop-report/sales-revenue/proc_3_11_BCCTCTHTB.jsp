<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dl class="Dl3Style">
	<dt><label class="LabelStyle Label1Style">Đơn vị (F9)<span class="RequireStyle">(*)</span></label></dt>
	<dd><input type="text" style="width: 226px" class="InputTextStyle InputText1Style" id="shop"  /></dd>
	
	<dt>
		<label class="LabelStyle Label1Style">Năm</label>
	</dt>
	<dd class="ClearRight">
		<div class="BoxSelect BoxSelect1">
			<select id="fullYearIn10" class="MySelectBoxClass" onchange=""></select>
		</div>
	</dd>
	<dt class="ClearLeft">
		<label class="LabelStyle Label1Style">CTTB (F9)</label>
	</dt>
	<dd>
		<input id="multiChoiceCTTB" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">Mã KH(F9)</span>
		</label>
	</dt>
	<dd>
		<input type="text" id="customerCode" class="InputTextStyle InputText1Style" />
	</dd>
<!-- 	<dt class="LabelTMStyle"> -->
<!--          <label class="LabelStyle Label1Style">Kiểu File -->
<!--          </label> -->
<!--     </dt> -->
<!--     <dd> -->
<!--          <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF -->
<!--          <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS -->
<!--     </dd> -->
	
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="SalesRevenueReport.exportBCTHCTHTB_DS311();">Xuất báo cáo</button>
</div>

<div id="searchStyleDisPlayProForTreeP" style="display: none;">
	<div id="searchStyleDisPlayProDL" class="easyui-dialog" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">
				<label id="seachStyleDisPlayProCodeLabel" class="LabelStyle Label2Style">Mã CTTB</label>
				<input id="seachStyleDisPlayProCode" maxlength="50" tabindex="100" type="text" class="InputTextStyle InputText5Style" />
				<label id="seachStyleDisPlayProNameLabel" class="LabelStyle Label2Style">Tên CTTB</label>
				<input id="seachStyleDisPlayProName" type="text" maxlength="250" tabindex="101" class="InputTextStyle InputText5Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" onclick="return ReportUtils.searchMultiDisplayPro();" tabindex="102" id="btnSearchStyleDisPlayPro">
						<span class="Sprite2">Tìm kiếm</span>
					</button>
				</div>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle">Danh sách tìm kiếm</h2>
				<div class="GridSection" id="searchStyleContainerGrid">
					<table id="searchCTTBGrid"></table>
				</div>
			</div>
			<div class="Clear"></div>
			<div id="bottomDisplayProDialog" class="SearchInSection SProduct1Form">
				<div class="BtnCenterSection">
					<button id="btnSelectProgramDisplay" tabindex="103" class="BtnGeneralStyle" onclick="return ReportUtils.multiChoiceDisplayPro();">Chọn</button>
					<button id="btnOnCloseDialogDisplayPro" tabindex="104" class="BtnGeneralStyle" onclick="return ReportUtils.onCloseDialogSearchMultiDisplayPro();">Bỏ qua</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgDialogDisplayPro" class="ErrorMsgStyle" style="display: none; margin-top: 10px; float: left; text-align: left;" />
				<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
			</div>
		</div>
	</div>
</div>

<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="curShopId2" name="shopId"></s:hidden>
<s:hidden id="multiChoiceCTTBHiddent"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	var n = new Date().getFullYear();
	var i = n;
	for(i=n;i>n-11;--i){
		$('#fullYearIn10').append('<option value="'+i+'">'+i+'</option>');
	}
	$('#fullYearIn10').change();
	
	$('.MySelectBoxClass').customStyle();
	ReportUtils.loadComboTree('shop','shopCode',$('#curShopId').val(),function(shopCode){
		$('#staffCode').val('');
		$('#customerCode').val();
	}); 
	ReportUtils._lstChecker = new Array();
	ReportUtils._lstUnchecker = new Array();
	ReportUtils._arrMultiChoiceDisplay = new Array();
	ReportUtils._isCheckAll = false;
	$('#customerCode').bind('keyup', function(event) {		
		if (event.keyCode == keyCode_F9) {
			var arrParam = new Array();
			var param = {};
			param.name = "shopId";
			param.value = $('#shop').combotree('getValue');
			arrParam.push(param);
				CommonSearch.searchCustomerByShopOnDialog(function(data) {
					$('#customerCode').val(data.code);
//					$('#customerName').val(data.name);
				}, arrParam);
		}
	});
	$('#multiChoiceCTTB').bind('keyup', function(event) {		
		if (event.keyCode == keyCode_F9) {
			ReportUtils._objectSearchCTTB = new Object();
			ReportUtils._objectSearchCTTB.year = encodeChar($('#fullYearIn10').val());
			ReportUtils._objectSearchCTTB.shopId = $('#shop').combotree('getValue');
			var urlParamater = '/programme-display/search-Display-Program-By-Year';
			ReportUtils.openDialogSearchCTTB(urlParamater, 'searchStyleDisPlayProDL');
		}
	});
	$('#fullYearIn10').change(function(){
		$('#multiChoiceCTTB').val("").show();
		ReportUtils._lstChecker = new Array();
		ReportUtils._lstUnchecker = new Array();
		ReportUtils._isCheckAll = false;
	});
	
	$('#multiChoiceCTTB').change(function(){
		$('#multiChoiceCTTB').val($('#multiChoiceCTTBHiddent').val()).show();
	});
});
</script>          	
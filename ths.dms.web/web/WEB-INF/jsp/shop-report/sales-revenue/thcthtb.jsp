<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" disabled="disabled" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:if>
		<s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>	
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">Năm<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<div class="BoxSelect BoxSelect1" style = "width: 100px">
				<select id="fullYearIn10" class="MySelectBoxClass">
				</select>
			</div>
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">CTTB (F9)<span class="ReqiureStyle"></span>
			</label>
		</dt>
		<dd>
			<%-- <div class="BoxSelect BoxSelect1" style = "width: 100px">
				<select id="cttb" class="MySelectBoxClass">
				</select>
			</div> --%>
			<input id="multiChoiceCTTB" type="text" class="InputTextStyle InputText1Style" />
		</dd>
		<dt class="LabelTMStyle">
                     <label class="LabelStyle Label1Style">Kiểu File
                     </label>
              </dt>
              <dd>
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
              </dd>
	</dl>
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="SalesRevenueReport.exportTHCTHTB();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>

<!-- dialog CTTB -->
<div id="searchStyleDisPlayProForTreeP" style="display: none;">
	<div id="searchStyleDisPlayProDL" class="easyui-dialog" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">
				<label id="seachStyleDisPlayProCodeLabel" class="LabelStyle Label2Style">Mã CTTB</label>
				<input id="seachStyleDisPlayProCode" maxlength="50" tabindex="100" type="text" class="InputTextStyle InputText5Style" />
				<label id="seachStyleDisPlayProNameLabel" class="LabelStyle Label2Style">Tên CTTB</label>
				<input id="seachStyleDisPlayProName" type="text" maxlength="250" tabindex="101" class="InputTextStyle InputText5Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" onclick="return ReportUtils.searchMultiDisplayPro();" tabindex="102" id="btnSearchStyleDisPlayPro">
						<span class="Sprite2">Tìm kiếm</span>
					</button>
				</div>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle">Danh sách tìm kiếm</h2>
				<div class="GridSection" id="searchStyleContainerGrid">
					<table id="searchCTTBGrid"></table>
				</div>
			</div>
			<div class="Clear"></div>
			<div id="bottomDisplayProDialog" class="SearchInSection SProduct1Form">
				<div class="BtnCenterSection">
					<button id="btnSelectProgramDisplay" tabindex="103" class="BtnGeneralStyle" onclick="return ReportUtils.multiChoiceDisplayPro();">Lưu</button>
					<button id="btnOnCloseDialogDisplayPro" tabindex="104" class="BtnGeneralStyle" onclick="return ReportUtils.onCloseDialogSearchMultiDisplayPro();">Bỏ qua</button>
				</div>
				<div class="Clear"></div>
				<p id="errMsgDialogDisplayPro" class="ErrorMsgStyle" style="display: none; margin-top: 10px; float: left; text-align: left;" />
				<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
			</div>
		</div>
	</div>
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="isNPP" name="NPP"></s:hidden>
<s:hidden id ="maCTTB"></s:hidden>
<s:hidden id="multiChoiceCTTBHiddent"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	//$('#cttb').customStyle();
	$('#fullYearIn10').customStyle();
	var date = new Date();
	var currentYear = date.getFullYear();
	var arrHtml = new Array();
	for(var i=currentYear - 4;i<=currentYear;i++){
		arrHtml.push('<option value="'+ i +'">'+ i +'</option>');
	}
	$('#fullYearIn10').html(arrHtml.join(""));
	$('#fullYearIn10').val(currentYear);
	$('#fullYearIn10').change();
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(shopId){
		//var year = $('#year').val();
		$('#multiChoiceCTTB').val('');
		//GetCTTB(shopId,year);
	});
	$('#fullYearIn10').change(function(){
		//var year = $(this).attr('value');
		//var shopId = $('#shopId').val();
		//GetCTTB(shopId,year);
		$('#multiChoiceCTTB').val("").show();
		ReportUtils._lstChecker = new Array();
		ReportUtils._lstUnchecker = new Array();
		ReportUtils._isCheckAll = false;
	});
	$('#multiChoiceCTTB').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9) {
			if($('#isNPP').val() == "false") {
				$('#errMsg').html('Đơn vị bạn vừa chọn không phải là 1 nhà phân phối cụ thể!').show();
			} else {
				//CRMReportDSPPTNH.openDialogSearchCTTB();
				ReportUtils._objectSearchCTTB = new Object();
				ReportUtils._objectSearchCTTB.year = encodeChar($('#fullYearIn10').val());
				ReportUtils._objectSearchCTTB.shopId = $('#shopId').val();
				//var urlParamater = '/programme-display/search-Display-Program-By-Year';
				var urlParamater = '/report/sales-revenue/load-cttb';
				ReportUtils.openDialogSearchCTTB(urlParamater, 'searchStyleDisPlayProDL');
			}
		}
	});
	$('#multiChoiceCTTB').change(function(){
		$('#multiChoiceCTTB').val($('#multiChoiceCTTBHiddent').val()).show();
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dt><label class="LabelStyle Label1Style">Chu kỳ <span class="ReqiureStyle">*</span></label></dt>
<dd>
	<div class="BoxSelect BoxSelect1">
    	<select class="MySelectBoxClass" id="cycle" onchange="onChangeCycle();">
			<s:iterator value="lstCycle">
				<option value='<s:property value="cycleId"/>'><s:property value="cycleName"/></option>
			</s:iterator>
    	</select>
	</div>
</dd>
<dt class="ClearLeft"><label class="LabelStyle Label1Style">Từ ngày <span class="ReqiureStyle">*</span></label></dt>
<dd><input onchange="checkDateForCycle(0);" id="fDate" class="InputTextStyle InputText2Style"></dd>

<dt><label class="LabelStyle Label1Style">Đến ngày <span class="ReqiureStyle">*</span></label></dt>
<dd><input onchange="checkDateForCycle(1);" id="tDate" class="InputTextStyle InputText2Style"></dd>
<script type="text/javascript">
$(document).ready(function(){
	//setDateTimePicker('fDate');	
	//setDateTimePicker('tDate');
	setDateTimePickerTrigger('fDate');	
	setDateTimePickerTrigger('tDate');
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	SuperviseCustomer.mapCycle = new Map();
	var idDefault = -1;
	<s:iterator value="lstCycle">
		var obj = new Object();
		obj.cycleId = '<s:property value="cycleId"/>';
		obj.cycleCode = '<s:property value="cycleCode"/>';
		obj.cycleName = '<s:property value="cycleName"/>';
		obj.num = '<s:property value="num"/>';
		obj.year = '<s:property value="year"/>';
		obj.beginDate = '<s:property value="beginDateStr"/>';
		obj.endDate = '<s:property value="endDateStr"/>';
		SuperviseCustomer.mapCycle.put(obj.cycleId, obj);
		idDefault = obj.cycleId;
	</s:iterator>
	$('#cycle').val(idDefault).change();//setDefault là giá trị cuối cùng
});
function onChangeCycle() {
	var obj = SuperviseCustomer.mapCycle.get($('#cycle').val());
	if (obj != null) {
		$('#fDate').val(obj.beginDate);
		$('#tDate').val(obj.endDate);
	}
}
function checkDateForCycle(type) {
	console.log("checkDateForCycle " + type);
	$('#errMsg').hide();
	var obj = SuperviseCustomer.mapCycle.get($('#cycle').val());
	if (obj != null) {
		var msg ='';
		if (msg.length == 0 && (type == undefined || type == 0)) {
			var date = $('#fDate').val();
			if(!Utils.compareDate(obj.beginDate, date) || !Utils.compareDate(date, obj.endDate)){
				msg = 'Từ ngày phải nằm trong chu kỳ';
				$('#fDate').focus();
			}
		}
		if (msg.length == 0 && (type == undefined || type == 1)) {
			var date = $('#tDate').val();
			if(!Utils.compareDate(obj.beginDate, date) || !Utils.compareDate(date, obj.endDate)){
				msg = 'Đến ngày phải nằm trong chu kỳ';
				$('#tDate').focus();
			}
		}
		if (msg.length == 0) {
			var fDate = $('#fDate').val();
			var tDate = $('#tDate').val();
			if(!Utils.compareDate(fDate, tDate)){
				msg = msgErr_fromdate_greater_todate;
				$('#fDate').focus();
			}
		}
		if (msg.length > 0) {
			$('#errMsg').html(msg).show();
			setTimeout(function(){$('#errMsg').hide();}, 3000);
			return false;
		}
	}
	return true;
}
</script>
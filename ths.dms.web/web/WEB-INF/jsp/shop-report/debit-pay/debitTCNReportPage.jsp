<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>         
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<s:if test="%{NPP=='NPP'}">
				<input type="text" id="shop" style="width:200px;" disabled="disabled" class="InputTextStyle InputText1Style" />
			</s:if>
			<s:else>
				<input type="text" id="shop" style="width:200px;" class="InputTextStyle InputText1Style" />
			</s:else>
		</dd>
		
		<dt>
			<label style="width: 90px" class="LabelStyle Label1Style">Đối tượng</label>
		</dt>
		<dd>
			<div class="BoxSelect BoxSelect1">
				<select id="objectType" class="MySelectBoxClass">
					<option value="1">Khách hàng</option>
					<option value="0">NVBH</option>
				</select>
			</div>
		</dd>

		<dt>
			<label style="padding-right: 5px" class="LabelStyle Label1Style">NVBH(F9)</label>
		</dt>
		<dd>
			<input id="staffCode" type="text" class="InputTextStyle InputText1Style" style="width: 190px;" />
		</dd>

		<dt>
			<label style="width: 90px" class="LabelStyle Label1Style">Khách hàng(F9)</label>
		</dt>
		<dd>
			<input id="customerCode" type="text" class="InputTextStyle InputText1Style" style="width: 190px;" />
		</dd>
		<dt class="LabelTMStyle">
                     <label class="LabelStyle Label1Style">Kiểu File
                     </label>
              </dt>
              <dd>
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
              </dd>
		
	</dl>

	<div class="Clear"></div>
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="DebitPayReport.reportTCN();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div> 	
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>
           
<s:hidden id="shopId"></s:hidden>
<s:hidden id="shopCodeUser"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<input type="hidden" id="curDate" value="<s:property value='toDate'/>" />
<script type="text/javascript">
$(document).ready(function(){	
	$('.MySelectBoxClass').customStyle();
	$('.CustomStyleSelectBox').css('width','176');
	$('#objectType').css('width','200');
	Utils.bindAutoSearch();
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(data){
		if(data!=$('#shopId').val().trim()){
			DebitPayReport._lstCustomer = new Map();
			DebitPayReport._lstStaff = new Map();
			$('#customerCode').val('');
			$('#staffCode').val('');
		}
	});
	$(document).unbind("keypress");
	$(document).keypress(function(event){
		//alert("Tran Ngoc Sang");
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			//alert("Tran Ngoc Sang 2");
			DebitPayReport.reportTTKH();
		}
	});
	$('#staffCode').attr('disabled','disabled');
	DebitPayReport._lstCustomer = new Map();
	DebitPayReport._lstStaff = new Map();
	$('#objectType').change(function(){
		if($('#objectType').val()=='1'){
			$('#staffCode').attr('disabled','disabled');
			$('#customerCode').removeAttr('disabled');
		}else{
			$('#customerCode').attr('disabled','disabled');
			$('#staffCode').removeAttr('disabled');
		}
		DebitPayReport._lstCustomer = new Map();
		DebitPayReport._lstStaff = new Map();
		$('#customerCode').val('');
		$('#staffCode').val('');
	});	
	$('#customerCode').bind('keyup', function(event){
		$('#errMsg').html('').hide();
		if(event.keyCode == keyCode_F9){
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				$('#errMsg').html('Bạn chưa chọn đơn vị.').show();
				return;
			}
			var arrParam = new Array();
			var param = {};
			param.name = "shopId";
			param.value = $('#shopId').val();
			arrParam.push(param);
			//CommonSearch.searchCustomerWithAddressByShopOnDialogCheckbox(function(data) {}, arrParam);
			ReportUtils.showDialogSearchCustomer($('#shopId').val());
		}else{
			var lstCustomer = $('#customerCode').val().trim().split(";");
			DebitPayReport._lstCustomer = new Map();
			for(i=0;i<lstCustomer.length;i++){
				if(lstCustomer[i]!=undefined && lstCustomer[i]!=''){
					DebitPayReport._lstCustomer.put(lstCustomer[i],lstCustomer[i]);
				}
			}
		}
	});
	$('#staffCode').bind('keyup', function(event){
		$('#errMsg').html('').hide();
		if(event.keyCode == keyCode_F9){
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				$('#errMsg').html('Bạn chưa chọn đơn vị.').show();
				return;
			}
			var arrParam = new Array();
			var param1 = {};
			param1.name = "shopId";
			param1.value = $('#shopId').val();			
			arrParam.push(param1);	
			//CommonSearch.searchPreAndValStaffOnDialogCheckbox(function(data){},arrParam);
			ReportUtils.showDialogSearchPreAndVanSaleStaff($('#shopId').val());
		}else{
			var lstStaff = $('#staffCode').val().trim().split(";");
			DebitPayReport._lstStaff = new Map();
			for(i=0;i<lstStaff.length;i++){
				if(lstStaff[i]!=undefined && lstStaff[i]!=''){
					DebitPayReport._lstStaff.put(lstStaff[i],lstStaff[i]);
				}
			}
		}
	});
	$('.CustomStyleSelectBox').css('padding', '0 15px 0 9px');
});
</script>
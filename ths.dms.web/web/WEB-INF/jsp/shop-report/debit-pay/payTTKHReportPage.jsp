<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>            
<div class="ModuleList3Form">
     <dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<s:if test="NPP.equals(\"NPP\")" >
				<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" disabled='disabled'/>
			</s:if>			
			<s:else>
    			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
			</s:else>
			
		</dd>

		<dt>
			<label class="LabelStyle Label1Style">Đối tượng<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<select id="objectType" class="BoxSelect BoxSelect1">
				<option value="0">Khách hàng</option>
				<option value="1">NVBH</option>
			</select>			
		</dd>
		
		
		<dt>
			<label class="LabelStyle Label1Style">NVBH(F9)
			</label>
		</dt>
		<dd>
			<input type="text" id="staffCode" class="InputTextStyle InputText1Style" />
		</dd>
		
		
		<dt>
			<label class="LabelStyle Label1Style">Khách hàng(F9)
			</label>
		</dt>
		<dd>
			<input id="customerCode" type="text" class="InputTextStyle InputText1Style" style="width:190px;"/>
		</dd>
		
		
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		
		
		
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
                     <label class="LabelStyle Label1Style">Kiểu File
                     </label>
              </dt>
              <dd>
                     <input type="radio" name="formatType" id="formatTypePdf" value="PDF" checked="checked">PDF
                     <input type="radio" name="formatType" id="formatTypeExcel" value="XLS">XLS
              </dd>
		
	</dl>

 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="DebitPayReport.reportTTKH();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>    
</div>
           
<s:hidden id="shopId"></s:hidden>
<s:hidden id="shopCodeUser"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<input type="hidden" id="curDate" value="<s:property value='toDate'/>" />
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	
	ReportUtils.setCurrentDateForCalendar('fDate', '01');
	ReportUtils.setCurrentDateForCalendar('tDate');
	
	$('.MySelectBoxClass').customStyle();
	$('.CustomStyleSelectBox').css('width','176');
	$('#objectType').css('width','200');
	Utils.bindAutoSearch();
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(data){
		if(data!=$('#shopId').val().trim()){
			DebitPayReport._lstCustomer = new Map();
			DebitPayReport._lstStaff = new Map();
			$('#customerCode').val('');
			$('#staffCode').val('');
		}
	});
	$('#staffCode').attr('disabled','disabled');
	DebitPayReport._lstCustomer = new Map();
	DebitPayReport._lstStaff = new Map();
	$('#objectType').change(function(){
		if($('#objectType').val()=='0'){
			$('#staffCode').attr('disabled','disabled');
			$('#customerCode').removeAttr('disabled');
		}else{
			$('#customerCode').attr('disabled','disabled');
			$('#staffCode').removeAttr('disabled');
		}
		DebitPayReport._lstCustomer = new Map();
		DebitPayReport._lstStaff = new Map();
		$('#customerCode').val('');
		$('#staffCode').val('');
	});	
	$('#customerCode').bind('keyup', function(event){
		$('#errMsg').html('').hide();
		if(event.keyCode == keyCode_F9){
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				$('#errMsg').html('Bạn chưa chọn đơn vị.').show();
				return;
			}
			var arrParam = new Array();
			var param = {};
			param.name = "shopId";
			param.value = $('#shopId').val();
			arrParam.push(param);
			ReportUtils.showDialogSearchCustomer(param.value);
		}else{
			var lstCustomer = $('#customerCode').val().trim().split(";");
			DebitPayReport._lstCustomer = new Map();
			for(i=0;i<lstCustomer.length;i++){
				if(lstCustomer[i]!=undefined && lstCustomer[i]!=''){
					DebitPayReport._lstCustomer.put(lstCustomer[i],lstCustomer[i]);
				}
			}
		}
	});
	$('#staffCode').bind('keyup', function(event){
		$('#errMsg').html('').hide();
		if(event.keyCode == keyCode_F9){
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				$('#errMsg').html('Bạn chưa chọn đơn vị.').show();
				return;
			}
			var arrParam = new Array();
			var param1 = {};
			param1.name = "shopId";
			param1.value = $('#shopId').val();
			arrParam.push(param1);
			ReportUtils.showDialogSearchPreAndVanSaleStaff(param1.value);
		}else{
			var lstStaff = $('#staffCode').val().trim().split(";");
			DebitPayReport._lstStaff = new Map();
			for(i=0;i<lstStaff.length;i++){
				if(lstStaff[i]!=undefined && lstStaff[i]!=''){
					DebitPayReport._lstStaff.put(lstStaff[i],lstStaff[i]);
				}
			}
		}
	});
});
</script>
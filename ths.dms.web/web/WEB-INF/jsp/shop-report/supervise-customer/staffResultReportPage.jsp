<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã đơn vị<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
		</s:if><s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
		</dd>
		
		<tiles:insertTemplate template="/WEB-INF/jsp/shop-report/chuKy.jsp" />
	</dl>
	
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return SuperviseCustomer.resultTimeVisitCustomer();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	$('.MySelectBoxClass').customStyle();
	ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
		$('#staffCode').val('');
		$('#staffSaleCode').val('');
	});

//     $('#staffCode').bind('keyup', function(event) {
// 		if (event.keyCode == keyCode_F9) {
// 			var multiShop = $("#shop").data("kendoMultiSelect");
// 			var dataShop = multiShop.dataItems();
// 			var lstShopId = '';
// 			if (dataShop != null && dataShop.length > 0) {
// 				lstShopId = dataShop[0].id;
// 				for (var i = 1; i < dataShop.length; i++) {
// 					lstShopId += "," + dataShop[i].id;
// 				}
// 			}
// 			var staffOwner = $('#staffOwnerCode').val();
// 			VCommonJS.showDialogSearch2WithCheckbox({
// 				params : {
// 					//shopId : $('#shop').combotree('getValue'),
// 					objectType : StaffRoleType.NVBHANDNVVS,
// 					arrIdStr: lstShopId,
// 					arrParentCodeStr: staffOwner,
// 					status : 1
// 				},
// 				inputs : [
// 			        {id:'code', maxlength:50, label:'Mã Nhân viên'},
// 			        {id:'name', maxlength:250, label:'Tên Nhân viên'},
// 			    ],
// 			    chooseCallback : function(listObj) {
// 			        //console.log(listObj);
// 			        if (listObj != undefined && listObj != null && listObj.length > 0) {
// 			        	var staffCode = listObj[0].staffCode.trim();
// 			        	for (var i = 1; i < listObj.length; i++) {
// 			        		staffCode = staffCode.trim() + ', ' + listObj[i].staffCode.trim();
// 			        	}
// 			        	$('#staffCode').val(staffCode.trim());
// 			        } else {
// 			        	$('#staffCode').val("");
// 			        }
// 			        $('#common-dialog-search-2-textbox').dialog("close");
// 			    },
// 			    url : '/commons/search-staff-show-list',
// 			    columns : [[
// 			        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false},
// 			        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false},
// 			        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
// 			    ]]
// 			});
// 		}
// 	});
	
// 	$('#staffOwnerCode').bind('keyup', function(event) {
// 		if(event.keyCode == keyCode_F9){
// 			if (event.keyCode == keyCode_F9) {
// 				var multiShop = $("#shop").data("kendoMultiSelect");
// 				var dataShop = multiShop.dataItems();
// 				var lstShopId = '';
// 				if (dataShop != null && dataShop.length > 0) {
// 					lstShopId = dataShop[0].id;
// 					for (var i = 1; i < dataShop.length; i++) {
// 						lstShopId += "," + dataShop[i].id;
// 					}
// 				}
// 				var txt = "staffOwnerCode";
// 				VCommonJS.showDialogSearch2({
// 					params : {
// 						//shopId : $('#shop').combotree('getValue'),
// 						objectType : StaffRoleType.NVGS,
// 						arrIdStr: lstShopId,
// 						status : 1
// 					},
// 					inputs : [
// 				        {id:'code', maxlength:50, label:'Mã Nhân viên'},
// 				        {id:'name', maxlength:250, label:'Tên Nhân viên'},
// 				    ],
// 				    url : '/commons/search-staff-show-list',
// 				    columns : [[
// 				        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false},
// 				        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false},
// 				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
// 				            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.id +', \''+row.staffCode+'\', \''+row.staffName+'\' ,'+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
// 				        }}
// 				    ]]
// 				});
// 			}
// 		}
// 	});
// 	ReportUtils.setCurrentDateForCalendar('fDate');
// 	ReportUtils.setCurrentDateForCalendar('tDate');
});
	</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Đơn vị<span class="RequireStyle">(*)</span></label>
		</dt>
		<dd style="height:auto;">
			<!-- <input id="shop" style="width: 226px;"> -->
			<div class="BoxSelect BoxSelect2">
				<div class="Field2">
					<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:217px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
				</div>
			</div>	
		</dd>
		<dt class="ClearLeft" style="margin-top:-20px;">
			<label class="LabelStyle Label1Style">GSNPP (F9)</label>
		</dt>
		<dd style="margin-top:-20px;">
			<input type="text" class="InputTextStyle InputText1Style" id="staffOwnerCode" />
		</dd>
		<dt style="margin-top:-20px;">
			<label class="LabelStyle Label1Style">NVBH (F9)</label>
		</dt>
		<dd style="margin-top:-20px;">
			<input type="text" class="InputTextStyle InputText1Style" id="staffCode" />
		</dd>

		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input type="text" id="fDate" class="InputTextStyle InputText2Style" />
		</dd>
		
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input type="text" id="tDate" class="InputTextStyle InputText2Style" />
		</dd>
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="SuperviseCustomer.exportBCNVKGTKHTT();">Xuất báo cáo</button>
	</div>
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>
 
 <div id ="searchStyle2EasyUIDialogDiv" style="display:none; width:630px; height: auto;" >
	<div id="searchStyle2EasyUIDialog" data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">				
				<label id="searchStyle2EasyUICodeLabel" class="LabelStyle Label2Style">Mã</label>
				<input id="searchStyle2EasyUICode" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText5Style" /> 
				<label id="searchStyle2EasyUINameLabel" class="LabelStyle Label2Style">Tên</label> 
				<input id="searchStyle2EasyUIName" type="text" maxlength="250" tabindex="2"  class="InputTextStyle InputText5Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnSearchOnDialog" tabindex="3" id="btnSearchStyle2EasyUI">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
				<input type="hidden" name="" id="searchStyle2EasyUIUrl"/>
				<input type="hidden" name="" id="searchStyle2EasyUICodeText"/>
				<input type="hidden" name="" id="searchStyle2EasyUINameText"/>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle" id="searchStyle2EasyUIContainerTitle">Kết quả tìm kiếm </h2>
				<div class="GridSection" id="searchStyle2EasyUIContainerGrid">
					<table id="searchStyle2EasyUIGrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" style="display: none;" id="btnSearchStyle2EasyUIUpdate">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" id="btnSearchStyle2EasyUIClose" onclick="$('#searchStyle2EasyUIDialog').dialog('destroy');">Đóng</button>
				</div>
			</div>
			<p id="errMsgSearchStyle2EasyUI" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
		</div>
	</div>
</div>
  
<s:hidden id="shopCode"></s:hidden>
<s:hidden id="curShopCode" name="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
	$(document).ready(function(){	
		setDateTimePicker('fDate');	
		setDateTimePicker('tDate');
		$('.MySelectBoxClass').customStyle();
		$("#shop").kendoMultiSelect({
	        dataTextField: "shopCode",
	        dataValueField: "shopId",
	        filter: "contains",
			itemTemplate: function(data, e, s, h, q) {
				var level = data.isLevel;
				if(level == 1) {//VNM
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else if(level == 2) {
						return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 3){
					return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 4){
					return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 5){
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else {
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
	        tagTemplate:  '#: data.shopCode #',
	        change: function(e) {
	        	var lstShop=this.value();
	        	var param='';
	        	var param1='';
	        	for(var i=0;i<lstShop.length;i++){
	        		if(i==0){
	        			param+='?lstShop='+lstShop[i];
	        			param1+='?lstShopId='+lstShop[i];
	        		}else{
	        			param+='&lstShop='+lstShop[i];
	        			param1+='&lstShopId='+lstShop[i];
	        		}
	        	}
	        	//$('#staffSaleCode').val('');
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
	                }
	            }
	        },
	        value: [$('#curShopId').val()]
	    });

		var shopKendo = $("#shop").data("kendoMultiSelect");
	    shopKendo.wrapper.attr("id", "shop-wrapper");

	    $(window).bind('keypress',function(event){
			if($('.fancybox-inner').length!=0){
				if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
					Images.showImageNextPre(0);
				}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
					Images.showImageNextPre(1);
				}
				return false;
			}
			return true;
		});
	    $('#staffCode').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				var multiShop = $("#shop").data("kendoMultiSelect");
				var dataShop = multiShop.dataItems();
				var lstShopId = '';
				if (dataShop != null && dataShop.length > 0) {
					lstShopId = dataShop[0].id;
					for (var i = 1; i < dataShop.length; i++) {
						lstShopId += "," + dataShop[i].id;
					}
				}
				var staffOwner = $('#staffOwnerCode').val();
				VCommonJS.showDialogSearch2WithCheckbox({
					params : {
						//shopId : $('#shop').combotree('getValue'),
						objectType : StaffRoleType.NVBHANDNVVS,
						arrIdStr: lstShopId,
						arrParentCodeStr: staffOwner,
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã Nhân viên'},
				        {id:'name', maxlength:250, label:'Tên Nhân viên'},
				    ],
				    chooseCallback : function(listObj) {
				        //console.log(listObj);
				        if (listObj != undefined && listObj != null && listObj.length > 0) {
				        	var staffCode = listObj[0].staffCode.trim();
				        	for (var i = 1; i < listObj.length; i++) {
				        		staffCode = staffCode.trim() + ', ' + listObj[i].staffCode.trim();
				        	}
				        	$('#staffCode').val(staffCode.trim());
				        } else {
				        	$('#staffCode').val("");
				        }
				        $('#common-dialog-search-2-textbox').dialog("close");
				    },
				    url : '/commons/search-staff-show-list',
				    columns : [[
				        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
				    ]]
				});
			}
		});
		
		$('#staffOwnerCode').bind('keyup', function(event) {
			if(event.keyCode == keyCode_F9){
				if (event.keyCode == keyCode_F9) {
					var multiShop = $("#shop").data("kendoMultiSelect");
					var dataShop = multiShop.dataItems();
					var lstShopId = '';
					if (dataShop != null && dataShop.length > 0) {
						lstShopId = dataShop[0].id;
						for (var i = 1; i < dataShop.length; i++) {
							lstShopId += "," + dataShop[i].id;
						}
					}
					var txt = "staffOwnerCode";
					VCommonJS.showDialogSearch2({
						params : {
							//shopId : $('#shop').combotree('getValue'),
							objectType : StaffRoleType.NVGS,
							arrIdStr: lstShopId,
							status : 1
						},
						inputs : [
					        {id:'code', maxlength:50, label:'Mã Nhân viên'},
					        {id:'name', maxlength:250, label:'Tên Nhân viên'},
					    ],
					    url : '/commons/search-staff-show-list',
					    columns : [[
					        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
					        	return Utils.XSSEncode(value);         
					        }},
					        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
					        	return Utils.XSSEncode(value);         
					        }},
					        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
					            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.id +', \''+row.staffCode+'\', \''+row.staffName+'\' ,'+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
					        }}
					    ]]
					});
				}
			}
		});
		ReportUtils.setCurrentDateForCalendar('fDate');
		ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
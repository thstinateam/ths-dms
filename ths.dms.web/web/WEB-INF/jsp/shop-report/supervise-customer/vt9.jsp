<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style>
	#selectImageType .ui-dropdownchecklist-text,
	#selectImageType .ui-dropdownchecklist-selector {
    	width: 196px !important;
	}	
</style>

<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle">(*)</span></label>
	</dt>
	<dd style="height:auto;">
		<!-- <input id="shop" type="text"  style="width:216px; height: auto;" class="InputTextStyle InputText1Style" /> -->
		<div class="BoxSelect BoxSelect2" style="margin-top:-3px">
			<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>	
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">Nhân viên (F9)</label>
	</dt>
	<dd>
		<input id="staffCode" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	
	<dt class="ClearLeft LabelTMStyle" style="margin-top:-20px;">
		<label  class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd style="margin-top:-20px;">
		<input id="fromDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="fromDate"/>"/> 
	</dd>
	<dt class="LabelTMStyle" style="margin-top:-20px;">
		<label  class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd style="margin-top:-20px;">
		<input id="toDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="fromDate"/>"/> 
	</dd>
	
	<dt class="ClearLeft LabelTMStyle"><label class="LabelStyle Label1Style" style="padding: 10px 0 0;">Loại hình ảnh</label></dt>
    <dd>
        <div id="selectImageType" class="BoxSelect BoxSelect6">
                <select id="imageType" class="MySelectBoxClass" multiple="multiple">
                	<option value="-1" selected="selected">--Tất cả--</option>
                    <option value="0" selected="selected">Hình ảnh đóng cửa</option>
                    <option value="1" selected="selected">Hình ảnh trưng bày</option>
                    <option value="2" selected="selected">Hình ảnh điểm bán</option>
                    <option value="4" selected="selected">Chọn theo chương trình</option>
                </select>
		</div>
	</dd>
	
	<dt class="ClearLeft LabelTMStyle"><label class="LabelStyle Label1Style"  style="padding: 10px 0 0;">Chương trình trưng bày</label></dt>
   	<dd>
		<div id="selectImageType" class="BoxSelect BoxSelect2">
           <select id="displayProgramId" class="MySelectBoxClass" multiple="multiple">
           	   <option value="-1" selected="selected">--Tất cả--</option>
               <s:iterator value="lstSTDP">
               		<option selected="selected" value='<s:property value="id"/>'><s:property value="displayProgramCode"/> - <s:property value="displayProgramName"/></option>
               </s:iterator>
           </select>
		</div>
	</dd>    
	
	<dt>&nbsp;</dt>
	<dd>&nbsp;</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="SuperviseCustomer.exportBCVT9();">Xuất Excel</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="shopId" name="currentShopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
	$(document).ready( function() {
		setDateTimePicker('fromDate');
		setDateTimePicker('toDate');
		$("#shop").kendoMultiSelect({
	        dataTextField: "shopCode",
	        dataValueField: "shopId",
	        filter: "contains",
			itemTemplate: function(data, e, s, h, q) {
				var level = data.isLevel;
				if(level == 1) {//VNM
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else if(level == 2) {
						return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 3){
					return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 4){
					return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 5){
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else {
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
	        tagTemplate:  '#: data.shopCode #',
	        change: function(e) {
	        	var lstShop=this.value();
	        	var param='';
	        	var param1='';
	        	for(var i=0;i<lstShop.length;i++){
	        		if(i==0){
	        			param+='?lstShop='+lstShop[i];
	        			param1+='?lstShopId='+lstShop[i];
	        		}else{
	        			param+='&lstShop='+lstShop[i];
	        			param1+='&lstShopId='+lstShop[i];
	        		}
	        	}
	        	$('#staffCode').val('');
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
	                }
	            }
	        },
	        value: [$('#curShopId').val()]
	    });

	    var shopKendo = $("#shop").data("kendoMultiSelect");
	    shopKendo.wrapper.attr("id", "shop-wrapper");

	    $(window).bind('keypress',function(event){
			if($('.fancybox-inner').length!=0){
				if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
					Images.showImageNextPre(0);
				}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
					Images.showImageNextPre(1);
				}
				return false;
			}
			return true;
		});
		
		setTimeout(function() {
			$('.k-input').focus();
		}, 1000);
		
		$('#staffCode').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				var multiShop = $("#shop").data("kendoMultiSelect");
				var dataShop = multiShop.dataItems();
				var lstShopId = '';
				if (dataShop != null && dataShop.length > 0) {
					lstShopId = dataShop[0].id;
					for (var i = 1; i < dataShop.length; i++) {
						lstShopId += "," + dataShop[i].id;
					}
				}				
				VCommonJS.showDialogSearch2WithCheckbox({
					params : {						
						objectType : StaffRoleType.NVBHANDNVVS,
						arrIdStr: lstShopId,						
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã Nhân viên'},
				        {id:'name', maxlength:250, label:'Tên Nhân viên'},
				    ],
				    chooseCallback : function(listObj) {				        
				        if (listObj != undefined && listObj != null && listObj.length > 0) {
				        	var staffCode = listObj[0].staffCode.trim();
				        	for (var i = 1; i < listObj.length; i++) {
				        		staffCode = Utils.XSSEncode(staffCode.trim() + ', ' + listObj[i].staffCode.trim());
				        	}
				        	$('#staffCode').val(Utils.XSSEncode(staffCode.trim()));
				        } else {
				        	$('#staffCode').val("");
				        }
				        $('#common-dialog-search-2-textbox').dialog("close");
				    },
				    url : '/commons/search-staff-show-list',
				    columns : [[
				        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
				    ]]
				});
			}
		});
		
		$('#imageType').dropdownchecklist({ 
			forceMultiple: true,
			firstItemChecksAll: true,
			maxDropHeight: 350,
			width: 226,
			onItemClick:function(checkbox, selector){
				var justChecked = checkbox.prop("checked");
				var val = $(checkbox).val();				
				if((val == 4 || val == -1) && justChecked) {
					$("#displayProgramId").dropdownchecklist("enable");
					
				} else if((val == 4 || val == -1) && !justChecked) {
					$("#displayProgramId").dropdownchecklist("disable");
				}
			}
		});
		
		$('#displayProgramId').dropdownchecklist({ 
			forceMultiple: true,
			firstItemChecksAll: true,
			maxDropHeight: 350,
			width: 226,
			onItemClick:function(checkbox, selector){
				
			}
		});
		
		/* $("#displayProgramId").multiselect({
			show : [ "bounce", 100 ],
			hide : [ "explode", 500 ],
			checkAllText : 'chọn tất cả',
			uncheckAllText : 'bỏ tất cả',
			noneSelectedText : 'Chọn loại chương trình',
			multiple : true
		});	
		
		
		$("#imageType").multiselect({
			show : [ "bounce", 100 ],
			hide : [ "explode", 500 ],
			multiple : true,
			checkAllText : 'chọn tất cả',
			uncheckAllText : 'bỏ tất cả',
			noneSelectedText : 'Chọn loại hình ảnh',
			click: function(event, ui){
				console.log(ui.value);
				console.log(ui.checked);
				if(ui.value == 5 && ui.checked) {
					var $widget = $("#displayProgramId").multiselect();
					$widget.multiselect('enable');	
				} else if(ui.value == 5 && !ui.checked) {
					$('#displayProgramId').multiselect('uncheckAll');
					var $widget = $("#displayProgramId").multiselect();
					$widget.multiselect('disable');	
				}
			}
		}); */
		ReportUtils.setCurrentDateForCalendar('fromDate');
		ReportUtils.setCurrentDateForCalendar('toDate');
	});
</script>
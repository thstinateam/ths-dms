<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
	<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt><label class="LabelStyle Label1Style">Đơn vị (F9)<span class="RequireStyle">(*)</span></label></dt>
		<dd style="height:auto;"><!-- <input type="text" style="width: 226px" class="InputTextStyle InputText1Style" id="shop"  /> -->
			<div class="BoxSelect BoxSelect2">
			<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:216px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
			</div>
		</dd>
		
		<dt><label class="LabelStyle Label1Style">GS NPP (F9)</label></dt>
		<dd><input type="text" id="staffOwnerCode" class="InputTextStyle InputText1Style" /></dd>
		
		<dt class="ClearLeft" style="margin-top:-20px;"><label class="LabelStyle Label1Style">NVBH (F9)</label></dt>
		<dd style="margin-top:-20px;"><input type="text"  id="staffSaleCode" class="InputTextStyle InputText1Style" /></dd>
		<dt>&nbsp;</dt>
		<dd>&nbsp;</dd>
		<dt class="ClearLeft LabelTMStyle" style="margin-top:-20px;"><label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span></label></dt>
		<dd style="margin-top:-20px;"><input type="text" id="fromDate" class="InputTextStyle InputText2Style"  /></dd>
		
		<dt class="LabelTMStyle" style="margin-top:-20px;"><label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span></label></dt>
		<dd style="margin-top:-20px;"><input type="text" class="InputTextStyle InputText2Style" id="toDate" /></dd>
		
		<dt class="ClearLeft LabelTMStyle"><label class="LabelStyle Label1Style">Từ giờ<br />(0 đến 23) <span class="ReqiureStyle">(*)</span></label></dt>
		<dd>
			<input id="fromHour" type="text" class="InputTextStyle InputText1Style" maxlength="2" value="9"/>
	   		
		</dd>
		 <dt class="LabelTMStyle"><label class="LabelStyle Label1Style">Đến giờ<br />(0 đến 23) <span class="ReqiureStyle">(*)</span></label></dt>
		<dd>
			<input id="toHour" type="text" class="InputTextStyle InputText1Style" maxlength="2" value="9"/>
	   		
		</dd>
		<dt class="ClearLeft LabelTMStyle"><label class="LabelStyle Label1Style">Từ phút<br />(0 đến 59) <span class="ReqiureStyle">(*)</span></label></dt>
		<dd>
			<input id="fromMinute" type="text" class="InputTextStyle InputText1Style" maxlength="2" value="0"/>
	   		
		</dd>
		
		<dt class="LabelTMStyle"><label class="LabelStyle Label1Style">Đến phút<br />(0 đến 59) <span class="ReqiureStyle">(*)</span></label></dt>
        <dd>
        	<input id="toMinute" type="text" class="InputTextStyle InputText1Style" maxlength="2" value="0"/>
	   		
        </dd>   	
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button class="BtnGeneralStyle"  onclick="SuperviseCustomer.reportBCTGGTNVBH_1();">Xuất báo cáo</button>
	</div>
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p> 
   </div>
<s:hidden id="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="kieuTruyen2"></s:hidden>
<s:hidden id="kieuTruyen1"></s:hidden>
<s:hidden id="curShopId2" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	
	ReportUtils.setCurrentDateForCalendar('fromDate');
	ReportUtils.setCurrentDateForCalendar('toDate');	
	Utils.bindFormatOnTextfield('fromHour', Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield('fromMinute', Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield('toHour', Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield('toMinute', Utils._TF_NUMBER);
	setDateTimePicker('date');	
	setDateTimePicker('fromDate');
	setDateTimePicker('toDate');
	$('.MySelectBoxClass').customStyle();
	
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");

    $(window).bind('keypress',function(event){
		if($('.fancybox-inner').length!=0){
			if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
				Images.showImageNextPre(0);
			}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
				Images.showImageNextPre(1);
			}
			return false;
		}
		return true;
	});
	
	$('#staffSaleCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			var lstShopId = '';
			if (dataShop != null && dataShop.length > 0) {
				lstShopId = dataShop[0].id;
				for (var i = 1; i < dataShop.length; i++) {
					lstShopId += "," + dataShop[i].id;
				}
			}
			var staffOwner = $('#staffOwnerCode').val();
			VCommonJS.showDialogSearch2WithCheckbox({
				params : {
					//shopId : $('#shop').combotree('getValue'),
					objectType : StaffRoleType.NVBHANDNVVS,
					arrIdStr: lstShopId,
					arrParentCodeStr: staffOwner,
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Nhân viên'},
			        {id:'name', maxlength:250, label:'Tên Nhân viên'},
			    ],
			    chooseCallback : function(listObj) {
			        //console.log(listObj);
			        if (listObj != undefined && listObj != null && listObj.length > 0) {
			        	var staffCode = listObj[0].staffCode.trim();
			        	for (var i = 1; i < listObj.length; i++) {
			        		staffCode = staffCode.trim() + ', ' + listObj[i].staffCode.trim();
			        	}
			        	$('#staffSaleCode').val(staffCode.trim());
			        } else {
			        	$('#staffSaleCode').val("");
			        }
			        $('#common-dialog-search-2-textbox').dialog("close");
			    },
			    url : '/commons/search-staff-show-list',
			    columns : [[
			        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
			    ]]
			});
		}
	});
	
	$('#staffOwnerCode').bind('keyup', function(event) {
		if(event.keyCode == keyCode_F9){
			if (event.keyCode == keyCode_F9) {
				var multiShop = $("#shop").data("kendoMultiSelect");
				var dataShop = multiShop.dataItems();
				var lstShopId = '';
				if (dataShop != null && dataShop.length > 0) {
					lstShopId = dataShop[0].id;
					for (var i = 1; i < dataShop.length; i++) {
						lstShopId += "," + dataShop[i].id;
					}
				}
				var txt = "staffOwnerCode";
				VCommonJS.showDialogSearch2({
					params : {
						//shopId : $('#shop').combotree('getValue'),
						objectType : StaffRoleType.NVGS,
						arrIdStr: lstShopId,
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã Nhân viên'},
				        {id:'name', maxlength:250, label:'Tên Nhân viên'},
				    ],
				    url : '/commons/search-staff-show-list',
				    columns : [[
				        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false},
				        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.id +', \''+row.staffCode+'\', \''+row.staffName+'\' ,'+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
				        }}
				    ]]
				});
			}
		}
	});
	
});
</script>
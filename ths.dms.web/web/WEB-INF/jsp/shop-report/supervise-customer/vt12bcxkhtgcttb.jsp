<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle">(*)</span></label>
	</dt>
	<dd style="height: auto;">
		<input id="shop" type="text"  style="width:216px; height: auto;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="ClearLeft"><label class="LabelStyle Label1Style">Tháng</label></dt>
	<dd><input type="text" class="InputTextStyle InputText1Style" id="month" maxlength="7" style="width:193px;"/></dd>

   	<dt><label class="LabelStyle Label1Style">CTTB</label></dt>
   	<dd>
   		<div class="BoxSelect BoxSelect5">
           <select class="MySelectBoxClass" id="displayProgram">
           		<%-- <option value="">Tất cả</option>
               <s:iterator id="obj" value="listDisplayProgram">
					<option value="<s:property value="displayProgrameId"/>"><s:property value="displayProgrameCode" /></option>
				</s:iterator> --%>
           </select>
       </div>
   	</dd>                               
	
	<dt>&nbsp;</dt>
	<dd>&nbsp;</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="SuperviseCustomer.exportBCVT12();">Xuất Excel</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
	$(document).ready( function() {
		$("#shop").kendoMultiSelect({
	        dataTextField: "shopCode",
	        dataValueField: "shopId",
	        filter: "contains",
			itemTemplate: function(data, e, s, h, q) {
				var level = data.isLevel;
				if(level == 1) {//VNM
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else if(level == 2) {
						return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 3){
					return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 4){
					return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 5){
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else {
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
	        tagTemplate:  '#: data.shopCode #',
	        change: function(e) {
	        	var lstShop=this.value();
	        	var param='';
	        	var param1='';
	        	for(var i=0;i<lstShop.length;i++){
	        		if(i==0){
	        			param+='?lstShop='+lstShop[i];
	        			param1+='?lstShopId='+lstShop[i];
	        		}else{
	        			param+='&lstShop='+lstShop[i];
	        			param1+='&lstShopId='+lstShop[i];
	        		}
	        	}
	        	//$('#staffSaleCode').val('');
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
	                }
	            }
	        },
	        value: [$('#curShopId').val()]
	    });

	    var shopKendo = $("#shop").data("kendoMultiSelect");
	    shopKendo.wrapper.attr("id", "shop-wrapper");

	    $(window).bind('keypress',function(event){
			if($('.fancybox-inner').length!=0){
				if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
					Images.showImageNextPre(0);
				}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
					Images.showImageNextPre(1);
				}
				return false;
			}
			return true;
		});
		applyMonthPicker('month');
		Utils.bindFormatOnTextfield('month',Utils._TF_NUMBER_CONVFACT);		
		$('#month').val(getCurrentMonth());
		$('.MySelectBoxClass').customStyle();
		$('#month').bind('change',function(){
			loadDP();
		});	
		$('#month').change();
		setTimeout(function() {
			$('.k-input').focus();
		}, 1000);
		
		var d = new Date();		
		appendMonthAndYear('monthYear', d.getFullYear()-2, 12);							
		appendMonthAndYear('monthYear', d.getFullYear()-1, 12);
		appendMonthAndYear('monthYear', d.getFullYear(), d.getMonth()+1);
		$('#monthYear').dropdownchecklist({ 
			maxDropHeight: 350,
			width: 193,
			onItemClick:function(checkbox, selector){
			
			}
		});
	});
	function appendMonthAndYear(idTag, year, month){
		for(var i = 1; i<=month; i++){
			var itemCode = (i<10?'0'+i:i) + '/' + year;
			var itemDisplay = (i<10?'0'+i:i) + '/' + year;
			$('#'+idTag).append('<option value="'+ Utils.XSSEncode(itemCode) +'">'+Utils.XSSEncode(itemDisplay)+'</option>')
		}	
	}
	function loadDP(){
		var params = new Object();
		params.month=$('#month').val();			
		params.listShopId = $("#shop").data("kendoMultiSelect").value();
		Utils.getHtmlDataByAjaxNotOverlay(params,'/report/superivse-customer/vt12/load-display-program',
			function(data) {try {var _data = JSON.parse(data);} catch (e) {
				var t=$('#displayProgram').parent();
				t.html(data);
				$('#displayProgram').attr('multiple','multiple');
				$('#displayProgram').dropdownchecklist({ 
					forceMultiple: true,
					firstItemChecksAll: true,
					maxDropHeight: 350,
					width: 226,
					onItemClick:function(checkbox, selector){
						
					}
				});				
				$('.CustomStyleSelectBoxInner').html("Tất cả");
			}
		}, '', 'GET');
	}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dl class="Dl3Style">
	<dt><label class="LabelStyle Label1Style">Đơn vị (F9)<span class="RequireStyle">(*)</span></label></dt>
	<dd style="height:auto;">
		<!-- <input type="text" style="width: 226px" class="InputTextStyle InputText1Style" id="shop"  /> -->
		<div class="BoxSelect BoxSelect2">
			<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:216px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>
	</dd>
	
	<dt>
		<label class="LabelStyle Label1Style">GS NPP (F9)</label>
	</dt>
	<dd class="ClearRight">
		<input id="staffOwnerCode" type="text" maxlength="50" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="ClearLeft" style="margin-top:-20px;">
		<label class="LabelStyle Label1Style">Mã NVBH (F9)</label>
	</dt>
	<dd style="margin-top:-20px;">
		<input id="staffCode" type="text" maxlength="50" class="InputTextStyle InputText1Style" />
	</dd>
	<dt style="margin-top:-20px;">
		<label class="LabelStyle Label1Style">Ngày
		<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd class="ClearRight" style="margin-top:-20px;">
		<input id="fromDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="fromDate"/>"/> 
	</dd>
	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ giờ (0 đến 23)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="fromHour" type="text" maxlength="2" class="InputTextStyle InputText1Style" value="<s:property value="fromHour"/>" />
	</dd>
	
	<dt class="LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ phút (0 đến 59)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="fromMinute" type="text" maxlength="2" class="InputTextStyle InputText1Style" value="0" />
	</dd>
	
	<dt class="ClearLeft LabelTMStyle">
		<label  class="LabelStyle Label1Style">Đến giờ (0 đến 23)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="toHour" type="text" maxlength="2" class="InputTextStyle InputText1Style" value="<s:property value="toHour"/>" />
	</dd>
	
	<dt class="LabelTMStyle">
		<label  class="LabelStyle Label1Style">Đến phút(0 đến 59)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="toMinute" type="text" maxlength="2" class="InputTextStyle InputText1Style" value="0" />
	</dd>
	<dt class="ClearLeft">
		<label class="LabelStyle Label1Style">Số phút báo cáo
		<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<div class="BoxSelect BoxSelect1">
		<select id="sprTime" class="MySelectBoxClass">
			<option value="30">30 phút</option>
			<option value="60">1 tiếng</option>
			<option value="90">1 tiếng 30 phút</option>
			<option value="120">2 tiếng</option>
			<option value="150">2 tiếng 30 phút</option>
			<option value="180">3 tiếng</option>
		</select>
		</div>
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button id="bntExport" class="BtnGeneralStyle" onclick="SuperviseCustomer.reportBCTGBHHQCNVBH_DS3();">Xuất Excel</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="kieuTruyen2"></s:hidden>
<s:hidden id="kieuTruyen1"></s:hidden>
<s:hidden id="curShopId2" name="shopId"></s:hidden>
<s:hidden id="sysDate" name="toDate"></s:hidden>

<script type="text/javascript">
$(document).ready(function(){	
	var fromHourTxt = $('#fromHour').val().trim();
	if(fromHourTxt==undefined || fromHourTxt == null || fromHourTxt.length == 0){
		$('#fromHour').val('08').show();
	}
	var toHourTxt = $('#toHour').val().trim();
	if(toHourTxt==undefined || toHourTxt == null || toHourTxt.length == 0){
		$('#toHour').val('17').show();
	}
	ReportUtils.setCurrentDateForCalendar('fromDate', '01');
	Utils.bindFormatOnTextfield('fromHour', Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield('fromMinute', Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield('toHour', Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield('toMinute', Utils._TF_NUMBER);
	setDateTimePicker('fromDate');
	$('#fromDate').val($('#sysDate').val().trim());
	//setDateTimePicker('toDate');
	$('.MySelectBoxClass').customStyle();
	/* ReportUtils.loadComboTree('shop','shopCode',$('#curShopId').val(),function(shopCode){
		$('#staffCode').val('');
	}); */
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");

    $(window).bind('keypress',function(event){
		if($('.fancybox-inner').length!=0){
			if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
				Images.showImageNextPre(0);
			}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
				Images.showImageNextPre(1);
			}
			return false;
		}
		return true;
	});
    $('#staffCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			var lstShopId = '';
			if (dataShop != null && dataShop.length > 0) {
				lstShopId = dataShop[0].id;
				for (var i = 1; i < dataShop.length; i++) {
					lstShopId += "," + dataShop[i].id;
				}
			}
			var staffOwner = $('#staffOwnerCode').val();
			VCommonJS.showDialogSearch2WithCheckbox({
				params : {
					//shopId : $('#shop').combotree('getValue'),
					objectType : StaffRoleType.NVBHANDNVVS,
					arrIdStr: lstShopId,
					arrParentCodeStr: staffOwner,
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Nhân viên'},
			        {id:'name', maxlength:250, label:'Tên Nhân viên'},
			    ],
			    chooseCallback : function(listObj) {
			        //console.log(listObj);
			        if (listObj != undefined && listObj != null && listObj.length > 0) {
			        	var staffCode = listObj[0].staffCode.trim();
			        	for (var i = 1; i < listObj.length; i++) {
			        		staffCode = staffCode.trim() + ', ' + listObj[i].staffCode.trim();
			        	}
			        	$('#staffCode').val(staffCode.trim());
			        } else {
			        	$('#staffCode').val("");
			        }
			        $('#common-dialog-search-2-textbox').dialog("close");
			    },
			    url : '/commons/search-staff-show-list',
			    columns : [[
			        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
			        	return Utils.XSSEncode(value);         
			        }},
			        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
			    ]]
			});
		}
	});
	
	$('#staffOwnerCode').bind('keyup', function(event) {
		if(event.keyCode == keyCode_F9){
			if (event.keyCode == keyCode_F9) {
				var multiShop = $("#shop").data("kendoMultiSelect");
				var dataShop = multiShop.dataItems();
				var lstShopId = '';
				if (dataShop != null && dataShop.length > 0) {
					lstShopId = dataShop[0].id;
					for (var i = 1; i < dataShop.length; i++) {
						lstShopId += "," + dataShop[i].id;
					}
				}
				var txt = "staffOwnerCode";
				VCommonJS.showDialogSearch2({
					params : {
						//shopId : $('#shop').combotree('getValue'),
						objectType : StaffRoleType.NVGS,
						arrIdStr: lstShopId,
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã Nhân viên'},
				        {id:'name', maxlength:250, label:'Tên Nhân viên'},
				    ],
				    url : '/commons/search-staff-show-list',
				    columns : [[
				        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.id +', \''+row.staffCode+'\', \''+row.staffName+'\' ,'+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
				        }}
				    ]]
				});
			}
		}
	});
	$('#staffOwnerCode').bind('keypress', function(event){
		if(event.keyCode == keyCode_TAB){
			StaffCatalog.getSupervisorStaffName($('#staffOwnerCode').val().trim(),'#staffOwnerName');
		}
	});
});
</script>
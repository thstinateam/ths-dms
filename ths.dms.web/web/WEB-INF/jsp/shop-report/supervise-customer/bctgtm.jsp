<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle"> *</span></label>
		</dt>
		<dd>
			<input type="text" id="shop" class="InputTextStyle InputText1Style" style="width: 217px; margin-right: 70px;"/>
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">Số phút vượt quá<span class="ReqiureStyle"> *</span>
		</label>
		</dt>
		<dd>
            <input id="minutes" maxlength="6" type="text" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			  <label class="LabelStyle Label1Style">Từ ngày<span class="ReqiureStyle"> *</span></label>
		</dt>
		<dd>
			<input id="fDate" class="InputTextStyle InputText2Style" value="<s:property value='fromDate' />">
		</dd>
		<dt>
			 <label class="LabelStyle Label1Style">Đến ngày<span class="ReqiureStyle"> *</span></label>
		</dt>
		<dd style="height:auto;">
			<input id="tDate" class="InputTextStyle InputText2Style" value="<s:property value='toDate' />">
		</dd>
	</dl>
	
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="BtnGeneralStyle" class="BtnGeneralStyle Sprite2" onclick="return SuperviseCustomer.reportBCTGTM();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>                
</div>

<s:hidden id="shopId" name="shopId"></s:hidden>
<script type="text/javascript">
	$(document).ready(function(){	
		setDateTimePicker('fDate');	
		setDateTimePicker('tDate');
		
		ReportUtils.loadComboTree('shop', 'shopId', $('#shopId').val(),function(shopId){
			
		});

		Utils.bindFormatOnTextfield('minutes', Utils._TF_NUMBER);
});
	</script>
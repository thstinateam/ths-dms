<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã đơn vị</label>
		</dt>
		<dd>
		<div class="BoxSelect BoxSelect2">				
				<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:218px;height: auto;" class="InputTextStyle InputText1Style" />
				<%-- 	<input id="shop" type="text" data-placeholder="<s:text name="image_manager_choose_unit"/>"  style="width:301px;height: auto;" class="InputTextStyle InputText1Style" /> --%>
				</div>
		</div>
		<%-- <s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
		</s:if><s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else> --%>
		</dd>
		<tiles:insertTemplate template="/WEB-INF/jsp/shop-report/chuKy.jsp" />
		<!-- <dt class="ClearLeft">
			<label class="LabelStyle Label1Style">Giám sát</label>
		</dt>
		<dd style="height:auto;">		
			<div class="BoxSelect BoxSelect2" style="margin-top:-3px">
				<div class="Field2">
					<input id="gs" type="text" data-placeholder="Chọn giám sát"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
				</div>
			</div>	
		</dd> -->
		<dt>
			<label class="LabelStyle Label1Style">Nhân viên bán hàng</label>
		</dt>
		<dd style="height:auto;">		
			<div class="BoxSelect BoxSelect2" id="divCttb" style="margin-top:-3px">
				<div class="Field2">
					<input id="nvbh" type="text" data-placeholder="Chọn nhân viên bán hàng"  style="width:218px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
				</div>
			</div>	
		</dd>
		<%-- <dd><div class="BoxSelect BoxSelect2">
	            <select id="staffType" class="MySelectBoxClass">
	                  <option value="-1" selected="selected">Tất cả</option>
	                  <option value="1">NVBH</option>
	                  <option value="2">USM</option>
	             </select>
            </div>
		</dd> --%>
	</dl>
	
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return SuperviseCustomer.exportBCMMKH();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	$('.MySelectBoxClass').customStyle();
	/* ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
		$('#staffCode').val('');
		$('#staffSaleCode').val('');
	}); */
/* 	$("#gs").kendoMultiSelect({
        dataTextField: "gsName",
        dataValueField: "gsId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {			
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.gsId)+'" style="width:230px"><span class="tree-vnm-text">'+Utils.XSSEncode(data.gsName) +'</span></div>';			
		},
        tagTemplate:  '#: data.gsName #',	       
        dataSource: {
            transport: {
                read: {
                    dataType: "json",	                  
                    url: "/commons/list-gs-by-shop"
                }
            }
        },
        value: [$('#gsId').val()]
    }); */
    $("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:280px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
		tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(lstShop[i] != null && lstShop[i] != ""){
	        		if(i==0){
	        			param+='?lstShop='+lstShop[i];
	        			param1+='?lstShopId='+lstShop[i];
	        		}else{
	        			param+='&lstShop='+lstShop[i];
	        			param1+='&lstShopId='+lstShop[i];
	        		}
	        	}
        	}
        	if(param != "" && param1 != ""){
	        //	$('#divStaff').html('<input id="staff" type="text" data-placeholder="<s:text name="image_manager_choose_photographer"/>"  style="width:300px;height: auto;" class="InputTextStyle InputText1Style" />');
	        //	$('#divCTTB').html('<select id="cttb" name="LevelSchool" data-placeholder="<s:text name="image_manager_choose_photographer"/>" style="width:180px"></select>');
				$('#divCttb').html('<input id="nvbh" type="text" data-placeholder="Chọn nhân viên bán hàng"  style="width:218px;height: auto;margin-left:0px" class="InputTextStyle InputText1Style" />');	       	
				SuperviseCustomer.loadComboNVBH("/commons/list-nvbh-by-shop"+param1);
	        }
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });
    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");
    SuperviseCustomer.loadComboNVBH("/commons/list-nvbh-by-shop");
	/* $("#nvbh").kendoMultiSelect({
        dataTextField: "nvbhName",
        dataValueField: "nvbhId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {			
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.nvbhId)+'" style="width:230px"><span class="tree-vnm-text">'+Utils.XSSEncode(data.nvbhName) +'</span></div>';			
		},
        tagTemplate:  '#: data.nvbhName #',	       
        dataSource: {
            transport: {
                read: {
                    dataType: "json",	                  
                    url: "/commons/list-nvbh-by-shop"
                }
            }
        },
        value: [$('#nvbhId').val()]
    }); */
});
</script>
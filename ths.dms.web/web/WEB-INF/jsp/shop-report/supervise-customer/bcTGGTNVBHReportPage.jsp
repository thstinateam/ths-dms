<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã đơn vị</label>
		</dt>
		<dd>
		<s:if test="%{NPP=='NPP'}">
			<input type="text" id="shop" style="width:226px;" disabled="disabled" class="InputTextStyle InputText1Style" />
		</s:if><s:else>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</s:else>
		</dd>
		<tiles:insertTemplate template="/WEB-INF/jsp/shop-report/chuKy.jsp" />
		<dt class="ClearLeft">
			<label class="LabelStyle Label1Style">Loại nhân viên</label>
		</dt>
		<dd><div class="BoxSelect BoxSelect2">
	            <select id="staffType" class="MySelectBoxClass">
	                  <option value="1" selected="selected">NVBH</option>
	                  <option value="2">USM</option>
	             </select>
            </div>
		</dd>
	</dl>
	
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return SuperviseCustomer.reportBCTGGTNVBH();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	$('.MySelectBoxClass').customStyle();
	ReportUtils.loadComboTree('shop', 'shopId', $('#curShopId').val(), function(shopId){
		$('#staffCode').val('');
		$('#staffSaleCode').val('');
	});
});
</script>
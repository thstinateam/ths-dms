<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị</label>
	</dt>
	<dd style="height:auto;">
		<input type="text" style="width:215px; height: auto;" class="InputTextStyle InputText1Style" id="shop"  />
	</dd>
	<dt>
		<label  class="LabelStyle Label1Style">Ngày<br />(dd/MM/yyyy)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="fromDate" type="text" class="InputTextStyle InputText2Style" />
	</dd>
	<dt class="ClearLeft" style="margin-top:-15px;">
		<label  class="LabelStyle Label1Style">GSNPP (F9)</label>
	</dt>
	<dd style="margin-top:-15px;">
		<input id="staffOwnerCode" type="text" class="InputTextStyle InputText1Style" value=""/> 
	</dd>
	<dt style="margin-top:-15px;">
		<label  class="LabelStyle Label1Style">NVBH (F9)</label>
	</dt>
	<dd style="margin-top:-15px;">
		<input id="staffSaleCode" type="text" class="InputTextStyle InputText1Style" value=""/> 
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="SuperviseCustomer.exportBCVT6();">Xuất Excel</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
	$(document).ready( function() {
		setDateTimePicker('fromDate');
		ReportUtils.setCurrentDateForCalendar('fromDate');
		$("#shop").kendoMultiSelect({
	        dataTextField: "shopCode",
	        dataValueField: "shopId",
	        filter: "contains",
			itemTemplate: function(data, e, s, h, q) {
				var level = data.isLevel;
				if(level == 1) {//VNM
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else if(level == 2) {
						return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 3){
					return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 4){
					return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(level == 5){
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				} else {
					return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}
			},
	        tagTemplate:  '#: data.shopCode #',
	        change: function(e) {
	        	var lstShop=this.value();
	        	var param='';
	        	var param1='';
	        	for(var i=0;i<lstShop.length;i++){
	        		if(i==0){
	        			param+='?lstShop='+lstShop[i];
	        			param1+='?lstShopId='+lstShop[i];
	        		}else{
	        			param+='&lstShop='+lstShop[i];
	        			param1+='&lstShopId='+lstShop[i];
	        		}
	        	}
	        	$('#staffSaleCode').val('');
	        	$('#staffOwnerCode').val('');
	        },
	        dataSource: {
	            transport: {
	                read: {
	                    dataType: "json",
	                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
	                }
	            }
	        },
	        value: [$('#curShopId').val()]
	    });

	    var shopKendo = $("#shop").data("kendoMultiSelect");
	    shopKendo.wrapper.attr("id", "shop-wrapper");

	    $(window).bind('keypress',function(event){
			if($('.fancybox-inner').length!=0){
				if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
					Images.showImageNextPre(0);
				}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
					Images.showImageNextPre(1);
				}
				return false;
			}
			return true;
		});
	    $('#staffSaleCode').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				var multiShop = $("#shop").data("kendoMultiSelect");
				var dataShop = multiShop.dataItems();
				var lstShopId = '';
				if (dataShop != null && dataShop.length > 0) {
					lstShopId = dataShop[0].id;
					for (var i = 1; i < dataShop.length; i++) {
						lstShopId += "," + dataShop[i].id;
					}
				}
				var staffOwner = $('#staffOwnerCode').val();
				VCommonJS.showDialogSearch2WithCheckbox({
					params : {
						//shopId : $('#shop').combotree('getValue'),
						objectType : StaffRoleType.NVBHANDNVVS,
						arrIdStr: lstShopId,
						arrParentCodeStr: staffOwner,
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:'Mã Nhân viên'},
				        {id:'name', maxlength:250, label:'Tên Nhân viên'},
				    ],
				    chooseCallback : function(listObj) {
				        //console.log(listObj);
				        if (listObj != undefined && listObj != null && listObj.length > 0) {
				        	var staffCode = listObj[0].staffCode.trim();
				        	for (var i = 1; i < listObj.length; i++) {
				        		staffCode = Utils.XSSEncode(staffCode.trim() + ', ' + listObj[i].staffCode.trim());
				        	}
				        	$('#staffSaleCode').val(Utils.XSSEncode(staffCode.trim()));
				        } else {
				        	$('#staffSaleCode').val("");
				        }
				        $('#common-dialog-search-2-textbox').dialog("close");
				    },
				    url : '/commons/search-staff-show-list',
				    columns : [[
				        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'cb', checkbox:true, align:'center', width:80,sortable : false,resizable : false},
				    ]]
				});
			}
		});
		
		$('#staffOwnerCode').bind('keyup', function(event) {
			if(event.keyCode == keyCode_F9){
				if (event.keyCode == keyCode_F9) {
					var multiShop = $("#shop").data("kendoMultiSelect");
					var dataShop = multiShop.dataItems();
					var lstShopId = '';
					if (dataShop != null && dataShop.length > 0) {
						lstShopId = dataShop[0].id;
						for (var i = 1; i < dataShop.length; i++) {
							lstShopId += "," + dataShop[i].id;
						}
					}
					var txt = "staffOwnerCode";
					VCommonJS.showDialogSearch2({
						params : {
							//shopId : $('#shop').combotree('getValue'),
							objectType : StaffRoleType.NVGS,
							arrIdStr: lstShopId,
							status : 1
						},
						inputs : [
					        {id:'code', maxlength:50, label:'Mã Nhân viên'},
					        {id:'name', maxlength:250, label:'Tên Nhân viên'},
					    ],
					    url : '/commons/search-staff-show-list',
					    columns : [[
					        {field:'staffCode', title:'Mã Nhân viên', align:'left', width: 110, sortable:false, resizable:false},
					        {field:'staffName', title:'Tên Nhân viên', align:'left', width: 200, sortable:false, resizable:false},
					        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
					            return '<a href="javascript:void(0)" onclick="Utils.fillCodeForInputTextByDialogSearch2(\''+txt+'\', ' + row.id +', \''+Utils.XSSEncode(row.staffCode)+'\', \''+Utils.XSSEncode(row.staffName)+'\' ,'+Utils._isShowInputTextByDlSearch2.code+');">chọn</a>';         
					        }}
					    ]]
					});
				}
			}
		});
	});
</script>                	
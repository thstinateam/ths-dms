<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
            <div class="GeneralMilkInBox ResearchSection">
            	<div class="ModuleList3Form">
                <label class="LabelStyle Label2Style">Từ tháng</label>
                <div class="Field2">
                	<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
                	<span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label3Style">Đến tháng</label>
                <div class="Field2">
                 <input id="tDate" type="text" class="InputTextStyle InputText2Style" />
	                <span class="RequireStyle">(*)</span>
                </div>
                <div class="Clear"></div>                
                <div class="ButtonSection">                	
                	<button id="btnSearch" onclick="return CRMReportDSPPTNH.exportBPPXTCDDS();" class="BtnGeneralStyle Sprite2" ><span class="Sprite2">Xuất báo cáo</span></button>                	
                </div>
                <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
               </div>
            </div>
		</div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
});
</script>
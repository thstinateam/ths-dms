<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<label class="LabelStyle Label1Style">Từ tháng</label>
	<div class="Field2">
		<input id="fromMonth" type="text" style="width:168px;" class="InputTextStyle InputText1Style" value="<s:property value="fromMonth"/>"/>
 		<span class="RequireStyle">(*)</span>
 	</div>
 	<label class="LabelStyle Label2Style">Đến tháng</label>
 	<div class="Field2">
  		<input id="toMonth" type="text" style="width:168px;" class="InputTextStyle InputText1Style" value="<s:property value="toMonth"/>"/>
  		<span class="RequireStyle">(*)</span>
 	</div>
 	<div class="Clear"></div> 
 	<div class="ButtonSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD16_1();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
</div>      
<script type="text/javascript">
$(document).ready(function(){
	$('#fromMonth').bind('blur',function(){
		var txtDate = $('#fromMonth').val().trim();
		var aoDate,           // needed for creating array and object
	        txtDateTmp;
	    aoDate = txtDate.split('/');
	    if (aoDate.length >= 3) {
	    	txtDateTmp = aoDate[1] +'/'+aoDate[2];
	    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
	    		$('#fromMonth').val(txtDateTmp);
	    	}
	    } 
	});
	applyDateTimePicker("#fromMonth", "mm/yy", null, null, null, null, null, null, null,true,null);
	$('#toMonth').bind('blur',function(){
		var txtDate = $('#toMonth').val().trim();
		var aoDate,           // needed for creating array and object
	        txtDateTmp;
	    aoDate = txtDate.split('/');
	    if (aoDate.length >= 3) {
	    	txtDateTmp = aoDate[1] +'/'+aoDate[2];
	    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
	    		$('#toMonth').val(txtDateTmp);
	    	}
	    } 
	});
	applyDateTimePicker("#toMonth", "mm/yy", null, null, null, null, null, null, null, true,null);
	
	ReportUtils.setCurrentMonthForCalendar('fromMonth');
	ReportUtils.setCurrentMonthForCalendar('toMonth');	
});
</script>
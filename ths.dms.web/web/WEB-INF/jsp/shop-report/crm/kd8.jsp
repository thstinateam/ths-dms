<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
   	<label class="LabelStyle Label1Style">Đơn vị</label>
<!--     <input id="customerCode" type="text" class="InputTextStyle InputText1Style"/> -->
	<div class="BoxSelect BoxSelect2">
       <div class="Field2">
			<input id="shop" style="width:200px;">
			<span class="RequireStyle">(*)</span>
       </div>
 	</div>
<!--     <label class="LabelStyle Label2Style" style="padding-left:85px">NVBH (F9)</label> -->
<!--     <input id="staffCode" type="text" class="InputTextStyle InputText1Style" style="width:188px"/> -->
 	<div class="Clear"></div>
 	<label class="LabelStyle Label1Style">Từ ngày</label>
 	<div class="Field2">
 		<input id="fDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
 		<span class="RequireStyle">(*)</span>
 	</div>
 	<label class="LabelStyle Label2Style" style="padding-left:87px;">Đến ngày</label>
 	<div class="Field2">
  		<input id="tDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
  		<span class="RequireStyle">(*)</span>
 	</div>
 	<div class="Clear"></div> 
 	<div class="ButtonSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD8();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
</div>            
<s:hidden id="shopCode"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	DebitPayReport._lstStaff = new Map();
	ReportUtils.setCurrentDateForCalendar('tDate');
	ReportUtils.loadComboTree('shop','shopCode',$('#curShopId').val());
// 	$('#staffCode').bind('keyup', function(event){
// 		if(event.keyCode == keyCode_F9){
// 			var arrParam = new Array();
// 			var param1 = {};
// 			param1.name = "shopId";
// 			param1.value = $('.combo-value').val();
// 			arrParam.push(param1);
// 			CommonSearch.searchPreAndValStaffOnDialogCheckbox(function(data){},arrParam);
// 		}else{
// 			var lstStaff = $('#staffCode').val().trim().split(";");
// 			DebitPayReport._lstStaff = new Map();
// 			for(i=0;i<lstStaff.length;i++){
// 				if(lstStaff[i]!=undefined && lstStaff[i]!=''){
// 					DebitPayReport._lstStaff.put(lstStaff[i],lstStaff[i]);
// 				}
// 			}
// 		}
// 	});
// 	$('#saleStaffCode').bind('keyup', function(event) {
// 		if (event.keyCode == keyCode_F9) {
// 			/* var arrParam = new Array();
// 			var param = {};
// 			param.name = "shopId";
// 			param.value = $('#shop').combotree('getValue');
// 			arrParam.push(param); */
// 			CommonSearch.searchSaleStaffOnDialog(function(data) {
// 				$('#saleStaffCode').val(data.code);
// 			}, arrParam);
// 		}
// 	});
	$('#tDate').change(function(){
		var txtDate = $('#tDate').val().trim();
		var aoDate,           // needed for creating array and object
	        txtDateTmp;
	    aoDate = txtDate.split('/');
	    if (aoDate.length >= 3) {
	    	txtDateTmp = '01/'+aoDate[1] +'/'+aoDate[2];
	    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
	    		$('#fDate').val(txtDateTmp);
	    	}
	    } 
	});
	setCurrentDayOfMonth();
});
function setCurrentDayOfMonth(){
	var txtDate = $('#tDate').val().trim();
	var aoDate,           // needed for creating array and object
        txtDateTmp;
    aoDate = txtDate.split('/');
    if (aoDate.length >= 3) {
    	txtDateTmp = '01/'+aoDate[1] +'/'+aoDate[2];
    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
    		$('#fDate').val(txtDateTmp);
    	}
    } 
}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="GeneralForm SearchInSection">
	<label class="LabelStyle Label1Style">Từ ngày</label>
	<div id="fromDateDiv">
		<input id="fromDate" type="text" maxlength="10" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="fromDateFirstMonth"/>"/>
	</div>
	<label class="LabelStyle Label2Style">Đến ngày<span class="RequireStyle">(*)</span></label>
  		<input id="toDate" type="text" maxlength="10" class="InputTextStyle InputText1Style" value="<s:property value="toDate"/>"/>
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD4();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
</div>            
<script type="text/javascript">
$(document).ready(function(){
	setDateTimePicker('fromDate');
	setDateTimePicker('toDate');
	$('#fromDateDiv .ui-datepicker-trigger').unbind('click');	
	$('#toDate').change(function(){
		var txtDate = $('#toDate').val().trim();
		var aoDate,           // needed for creating array and object
	        txtDateTmp;
	    aoDate = txtDate.split('/');
	    if (aoDate.length >= 3) {
	    	txtDateTmp = '01/'+aoDate[1] +'/'+aoDate[2];
	    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
	    		$('#fromDate').val(txtDateTmp);
	    	}
	    } 
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ReportForm Report5Form">
	<label class="LabelStyle Label1Style">Nhãn hàng</label>
	<div class="Field2">
		<div class="BoxSelect BoxSelect1">
		  	<select id="productInfoCode" class="MySelectBoxClass">
				<option value="-1">---Chọn nhãn hàng---</option>
				<s:iterator value="lstSubCat">					
					<option value='<s:property value="productInfoCode"/>'><s:property value="productInfoCode"/> - <s:property value="productInfoName"/></option>
				</s:iterator>		    
		   	</select>
	   	</div>
	   	<span class="RequireStyle">(*)</span>
   	</div>
   	<div class="Clear"></div>
   	<label class="LabelStyle Label1Style">Từ ngày</label>
	<div id="fromDateDiv">
		<input id="fromDate" type="text" style="width:169px;" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="fromDateFirstMonth"/>"/>
	</div>
	<label class="LabelStyle Label2Style" style="padding-left: 40px; width: 60px">Đến ngày</label>
	<div class="Field2">
		<input id="toDate" type="text" class="InputTextStyle InputText1Style" value="<s:property value="toDate"/>" />
		<span class="RequireStyle">(*)</span>
	</div>
	<div class="Clear"></div>
	<div class="ButtonSection">                	
		<button id="btnSearch" onclick="return CRMReportDSPPTNH.exportKD10_2();" class="BtnGeneralStyle Sprite2" >
			<span class="Sprite2">Xuất báo cáo</span>
		</button>                	
	</div>
	<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>          
</div>
<script type="text/javascript">
$(document).ready(function(){	
	
	setDateTimePicker('fromDate');
	setDateTimePicker('toDate');
	$('#fromDateDiv .ui-datepicker-trigger').unbind('click');
	$('#toDate').change(function(){
		var txtDate = $('#toDate').val().trim();
		var aoDate;           // needed for creating array and object
	    var txtDateTmp;
	    aoDate = txtDate.split('/');
	    if (aoDate.length >= 3) {
	    	txtDateTmp = '01/'+aoDate[1] +'/'+aoDate[2];
	    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
	    		$('#fromDate').val(txtDateTmp);
	    	}
	    } 
	});
	$('#productInfoCode').customStyle();
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!-- <div class="ModuleList3Form"> -->
<!--    	<label class="LabelStyle Label1Style">Đơn vị</label> -->
<!-- 	<div class="BoxSelect BoxSelect2"> -->
<!-- 		<div class="Field2"> -->
<!-- 			<input id="shop" style="width:200px;"> -->
<%-- 			<span class="RequireStyle">(*)</span> --%>
<!-- 	 	</div> -->
<!--  	</div> -->
<!--     <label class="LabelStyle Label2Style" style="padding-left:85px">NVBH (F9)</label> -->
<!--     <input id="nvbh" type="text" class="InputTextStyle InputText1Style" style="width:188px"/> -->
    
<!--  	<div class="Clear"></div> -->
 	
<!--  	<label class="LabelStyle Label1Style">Từ ngày</label> -->
<!--  	<div class="Field2"> -->
<%--  		<input id="fDate" value="<s:property value='fromDateFirstMonth'/>" type="text" style="width:168px;" class="InputTextStyle InputText2Style" /> --%>
<%--  		<span class="RequireStyle">(*)</span> --%>
<!--  	</div> -->
<!--  	<label class="LabelStyle Label2Style" style="padding-left:87px;">Đến ngày</label> -->
<!--  	<div class="Field2"> -->
<!--   		<input id="tDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" /> -->
<%--   		<span class="RequireStyle">(*)</span> --%>
<!--  	</div> -->
 	
<!--  	<div class="Clear"></div> -->
 	
<!--  	<label class="LabelStyle Label1Style">Số lần</label> -->
<!--  	<div class="Field2"> -->
<!--     	<input id="solan" type="text" maxlength="4" class="InputTextStyle InputText1Style" style="width:188px"/> -->
<%--     	<span class="RequireStyle">(*)</span> --%>
<!--  	</div> -->
 	
<!--     <div class="Clear"></div> -->
<!--  	<div class="ButtonSection">                	 -->
<%--  		<button class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD_8_1();"><span class="Sprite2">Xuất báo cáo</span></button>                	 --%>
<!--  	</div> -->
<!--  	<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>                -->
<!-- </div>   -->
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Đơn vị <span class="RequireStyle">(*)</span></label>
		</dt>
		<dd>
			<!-- <input id="shop" style="width: 226px;" /> -->
			<div class="BoxSelect BoxSelect2">
			<div class="Field2">
				<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:216px;height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
			</div>
		</div>
		</dd>
		
		<dt>
			<label class="LabelStyle Label1Style">NVBH (F9)</label>
		</dt>
		<dd>
			<input type="text" class="InputTextStyle InputText1Style" id="nvbh" />
		</dd>
		
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input type="text" id="fDate" class="InputTextStyle InputText1Style" />
		</dd>
		
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input type="text" id="tDate" class="InputTextStyle InputText1Style" />
		</dd>

		<dt class="ClearLeft">
			<label class="LabelStyle Label1Style">Số lần <span class="RequireStyle">(*)</span></label>
		</dt>
		<dd>
			<input type="text" maxlength="4" class="InputTextStyle InputText1Style" id="solan" />
		</dd>
	
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="CRMReportDSPPTNH.exportKD_8_1();">Xuất báo cáo</button>
	</div>
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>          
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="nvbh"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	DebitPayReport._lstStaff = new Map();
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
// 	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(data){
// 		if(data!=$('#shopId').val().trim()){
// 			DebitPayReport._lstStaff = new Map();
// 			$('#nvbh').val('');
// 		}
// 	});
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "id",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			if(data.type.channelTypeCode == 'VNM') {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(data.type.channelTypeCode == 'MIEN'){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.id)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(data.type.channelTypeCode == 'VUNG'){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.id)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(data.type.channelTypeCode == 'NPP'){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.id)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	$('#nvbh').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendoui-combobox.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });
	var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");
    
// 	$('#nvbh').bind('keyup', function(event){
// 		$('#errMsg').html('').hide();
// 		if(event.keyCode == keyCode_F9){
// 			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
// 				$('#errMsg').html('Bạn chưa chọn đơn vị.').show();
// 				return;
// 			}
// 			var arrParam = new Array();
// 			var param1 = {};
// 			param1.name = "shopId";
// 			param1.value = $('#shopId').val();
// 			arrParam.push(param1);
// 			CommonSearch.searchPreAndValStaffOnDialogCheckbox(function(data){},arrParam,'nvbh');
// 		}else{
// 			var lstStaff = $('#nvbh').val().trim().split(";");
// 			DebitPayReport._lstStaff = new Map();
// 			for(i=0;i<lstStaff.length;i++){
// 				if(lstStaff[i]!=undefined && lstStaff[i]!=''){
// 					DebitPayReport._lstStaff.put(lstStaff[i],lstStaff[i]);
// 				}
// 			}
// 		}
// 	});
	
	$('#nvbh').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			//var shopCode = $('#shopCode').val();
			var multiShop = $("#shop").data("kendoMultiSelect");
			var dataShop = multiShop.dataItems();
			var arrParam = new Array();
			var param = new Object();
			param.name = 'strListShopId';
			//param.value = shopCode;
			if(dataShop!=null && dataShop.length>0){
				var lstShop = dataShop[0].id;
				for(var i=1;i<dataShop.length;i++){
					lstShop += "," + dataShop[i].id;
				}
				param.value = lstShop;
			}
			arrParam.push(param);
			CommonSearch.searchNVBHOnMultiSelectDialog(function(data){
				var lstStaff = "";
				if(data!=null){
					for(var i =0;i<data.length;i++){
						if(lstStaff.length>0){
							lstStaff+=",";
						}
						lstStaff+=data[i].staffCode;
					}
				}
				$('#nvbh').val(lstStaff);
				},arrParam);
			}
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

            	<div class="ModuleList3Form">
 				<label class="LabelStyle Label1Style">Đơn vị</label>                
   				<div class="BoxSelect BoxSelect2">
       				<div class="Field2">
						<input id="shop" style="width:200px;">
     				</div>
 				</div>
                <label class="LabelStyle Label2Style">Mã SP(F9)</label>
                <input id="productCode" type="text" class="InputTextStyle InputText2Style" />
                <div class="Clear"></div>  
                <label class="LabelStyle Label1Style">Từ ngày</label>
                <input id="fDate" type="text" class="InputTextStyle InputText1Style" style="width:190px;" disabled="disabled"/>
                <label class="LabelStyle Label2Style">Đến ngày</label>
                <div class="Field2">
                 <input id="tDate" type="text" class="InputTextStyle InputText2Style"/>
	                <span class="RequireStyle">(*)</span>
                </div>
                <div class="Clear"></div> 
                <div class="ButtonSection">                	
                	<button id="btnSearch" onclick="return CRMReportDSPPTNH.exportDSPPTNH();" class="BtnGeneralStyle Sprite2" ><span class="Sprite2">Xuất báo cáo</span></button>                	
                </div>
                <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>   
                <s:hidden id="shopId"></s:hidden>
				<s:hidden id="curShopId" name="shopId"></s:hidden>  
               </div>
    
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	$('#productCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			CRMReportDSPPTNH._listProductCode = new Array();
			var arrParam = new Array();
			CommonSearch.searchProductOnDialogCheckbox(function(data) {
				$('#productCode').val(data.code);
			}, arrParam);
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate','01');
	$('#tDate').bind('change',function(){
		if($('#tDate').val().length == 10){
			$('#fDate').val("01/"+ $(this).val().split("/")[1] +"/" + $(this).val().split("/")[2]);
			$('#fDate').change();
		}
	});
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
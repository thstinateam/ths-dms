<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
 	<label class="LabelStyle Label1Style">Tháng</label>
 	<div class="Field2">
  		<input id="tDate" type="text" style="width:168px;" class="InputTextStyle InputText1Style" />
  		<span class="RequireStyle">(*)</span>
 	</div>
 	<div class="Clear"></div> 
 	<div class="ButtonSection">                	
 		<button class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD16();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
</div>            
<script type="text/javascript">
$(document).ready(function(){
	$('#tDate').val(getCurrentMonth());
	applyDateTimePicker("#tDate", "mm/yy",null,null,null,null,null,null,null,true);
});
</script>
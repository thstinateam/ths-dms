<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style" style="width: 165px;">Chương trình trưng bày (F9) <span class="RequireStyle">(*)</span></label>
			
		</dt>
		<dd>
			<input id="displayProgramCode" type="text" class="InputTextStyle InputText2Style" style="width: 180px;"/>
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label2Style" style="width: 90px;">Nhãn hàng (F9)<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
			<input id="productCode" type="text" class="InputTextStyle InputText2Style" style="width: 185px;"/>
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">Mã NPP<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</dd>
	</dl>
	<!-- me -->
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" onclick="return CRMReportDSPPTNH.exportTKTHDSPPTMTG();" class="BtnGeneralStyle Sprite2" ><span class="Sprite2">Xuất báo cáo</span></button>              	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>  
 	<s:hidden id="shopId"></s:hidden>
				<s:hidden id="curShopId" name="shopId"></s:hidden>                     
</div>  
<script type="text/javascript">
$(document).ready(function(){
	setDateTimePicker('fDate');	
	setDateTimePicker('tDate');	
	$('#fromDateDiv .ui-datepicker-trigger').unbind('click');	
	$('#tDate').change(function(){
		var txtDate = $('#tDate').val().trim();
		var aoDate,           // needed for creating array and object
	        txtDateTmp;
	    aoDate = txtDate.split('/');
	    if (aoDate.length >= 3) {
	    	txtDateTmp = '01/'+aoDate[1] +'/'+aoDate[2];
	    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
	    		$('#fDate').val(txtDateTmp);
	    	}
	    } 
	});
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	$('#productCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			if ($('#displayProgramCode').val() == ""){
				$('#errMsg').html("Bạn chưa nhập mã Chương trình trưng bày").show();
				$('#displayProgramCode').focus();
			} else {
				var arrParam = new Array();
				var param1 = {};
				param1.name = "displayProgramCode";
				param1.value = $('#displayProgramCode').val();
				arrParam.push(param1);
				CommonSearch.searchSubCategoryByDisplayOnDialog(function(data) {
					$('#productCode').val(data.code);
				}, arrParam);
			}
			
		}
	});
	$('#displayProgramCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			var arrParam = new Array();
			CommonSearch.searchDisplayProgramRunningOnDialog(function(data) {
				$('#displayProgramCode').val(data.code);
			}, arrParam);
		}
	});
	ReportUtils.setCurrentDateForCalendar('fDate','01');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$('#tDate').bind('change',function(){
		if($('#tDate').val().length == 10){
			$('#fDate').val("01/"+ $(this).val().split("/")[1] +"/" + $(this).val().split("/")[2]);
			$('#fDate').change();
		}
	});
});
</script>
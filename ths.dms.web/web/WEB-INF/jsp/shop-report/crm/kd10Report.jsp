<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label style="width: 90px; float: right;" class="LabelStyle Label1Style">Ngành hàng<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
			<div class="BoxSelect BoxSelect1">
			  	<select id="subCat" class="MySelectBoxClass">
					<option value="-1">---Chọn ngành hàng---</option>
					<s:iterator value="lstSubCat">					
						<option value='<s:property value="id"/>'><s:property value="productInfoCode"/> - <s:property value="productInfoName"/></option>
					</s:iterator>		    
			   	</select>
		   	</div>
		</dd>
	
		<dt class="ClearLeft">
			<label style="padding-right: 5px" class="LabelStyle Label1Style">Từ ngày<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText1Style" value="<s:property value="toDate"/>" />
		</dd>
		
		<dt>
			<label style="padding-right: 5px" class="LabelStyle Label1Style">Đến ngày<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText1Style" value="<s:property value="toDate"/>" />
		</dd>
	</dl>

	<div class="Clear"></div>
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return CRMReportDSPPTNH.exportKD10();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div> 	
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>
<s:hidden id="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	$('.MySelectBoxClass').customStyle();
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$('#fDate').val('01/' + getCurrentMonth());
	$('#tDate').val(getCurrentDate());
});
</script>
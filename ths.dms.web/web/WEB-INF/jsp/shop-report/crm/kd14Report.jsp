<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<label class="LabelStyle Label1Style">Đơn vị</label>
	<div class="BoxSelect BoxSelect2">
        <div class="Field2">
    		<input id="shop" style="width:195px;"/>
    		<span class="RequireStyle">(*)</span>
       	</div>
    </div> 
	<div class="Clear"></div>
	<label class="LabelStyle Label1Style">Từ tháng</label>
<!-- 	<dt class="ClearLeft"><label class="LabelStyle Label1Style">Tháng</label></dt> -->
<!-- 	<dd><input type="text" class="InputTextStyle InputText1Style" id="monthFrom" maxlength="7" style="width:193px;"/></dd> -->
	<div class="Field2">
		<input id="fromDate" value='<s:property value="previousMonth"/>' type="text" class="InputTextStyle InputText1Style"/>
		<span class="RequireStyle">(*)</span>
	</div>
 	<label class="LabelStyle Label2Style">Đến tháng</label>
<!--  	<dt class="ClearLeft"><label class="LabelStyle Label1Style">Tháng</label></dt> -->
<!-- 	<dd><input type="text" class="InputTextStyle InputText1Style" id="monthTo" maxlength="7" style="width:193px;"/></dd> -->
 	<div class="Field2">
 		<input id="toDate" value='<s:property value="toMonth"/>' type="text" class="InputTextStyle InputText1Style"/>
 		<span class="RequireStyle">(*)</span>
 	</div>
 	<div class="Clear"></div> 
 	<div class="ButtonSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD14();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
</div>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="shopCode"></s:hidden>            
<script type="text/javascript">
$(document).ready(function(){	
	ReportUtils.loadComboReportTree('shop','shopCode',$('#curShopId').val());
	applyDateTimePicker("#fromDate", "mm/yy",null,null,null,null,null,null,null,true);
	applyDateTimePicker("#toDate", "mm/yy",null,null,null,null,null,null,null,true);
// 	applyMonthPicker('monthFrom');
// 	applyMonthPicker('monthTo');
});
</script>
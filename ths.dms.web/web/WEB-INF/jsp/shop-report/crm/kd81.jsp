<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
   	<label class="LabelStyle Label1Style">Đơn vị</label>
	<div class="BoxSelect BoxSelect2">
		<div class="Field2">
			<input id="shop" style="width:200px;">
			<span class="RequireStyle">(*)</span>
	 	</div>
 	</div>
    <label class="LabelStyle Label2Style" style="padding-left:85px">NVBH (F9)</label>
    <input id="nvbh" type="text" class="InputTextStyle InputText1Style" style="width:188px"/>
    
 	<div class="Clear"></div>
 	
 	<label class="LabelStyle Label1Style">Từ ngày</label>
 	<div class="Field2">
 		<input id="fDate" value="<s:property value='fromDateFirstMonth'/>" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
 		<span class="RequireStyle">(*)</span>
 	</div>
 	<label class="LabelStyle Label2Style" style="padding-left:87px;">Đến ngày</label>
 	<div class="Field2">
  		<input id="tDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
  		<span class="RequireStyle">(*)</span>
 	</div>
 	
 	<div class="Clear"></div>
 	
 	<label class="LabelStyle Label1Style">Số lần</label>
 	<div class="Field2">
    	<input id="solan" type="text" class="InputTextStyle InputText1Style" style="width:188px"/>
    	<span class="RequireStyle">(*)</span>
 	</div>
 	
    <div class="Clear"></div>
 	<div class="ButtonSection">                	
 		<button class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD81();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
</div>            
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="nvbh"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	DebitPayReport._lstStaff = new Map();
	ReportUtils.setCurrentDateForCalendar('tDate');
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(data){
		if(data!=$('#shopId').val().trim()){
			DebitPayReport._lstStaff = new Map();
			$('#nvbh').val('');
		}
	});
	$('#nvbh').bind('keyup', function(event){
		$('#errMsg').html('').hide();
		if(event.keyCode == keyCode_F9){
			if($('#shopId').val().trim().length == 0 || $('#shopId').val().trim() == activeType.ALL){
				$('#errMsg').html('Bạn chưa chọn đơn vị.').show();
				return;
			}
			var arrParam = new Array();
			var param1 = {};
			param1.name = "shopId";
			param1.value = $('#shopId').val();
			arrParam.push(param1);
			CommonSearch.searchPreAndValStaffOnDialogCheckbox(function(data){},arrParam,'nvbh');
		}else{
			var lstStaff = $('#nvbh').val().trim().split(";");
			DebitPayReport._lstStaff = new Map();
			for(i=0;i<lstStaff.length;i++){
				if(lstStaff[i]!=undefined && lstStaff[i]!=''){
					DebitPayReport._lstStaff.put(lstStaff[i],lstStaff[i]);
				}
			}
		}
	});
});
</script>
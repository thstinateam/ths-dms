<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="shop" type="text"  style="width:226px;" class="InputTextStyle InputText1Style" />
		</dd>
		
		
		<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style"  onchange="CRMReportDSPPTNH.tDateChangeByfDateIntoFirtMonth();" />
		</dd>
		
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="CRMReportDSPPTNH.exportBCTHCTHTB_KD1();">Xuất báo cáo</button>
	</div>
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>

<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>

<!-- -<div id="divProductContainer" style="display: none;">	 
	<div id="dialogProduct" class="easyui-dialog" title="Thêm sản phẩm <img id='loadingDialog' src='/resources/scripts/plugins/jquery-easyui-1.3.5/themes/default/images/loading.gif'>" data-options="closed:true,modal:true" style="width: 700px;">
   	  	<div class="GeneralForm Search1Form">
       	<label class="LabelStyle Label2Style">Mã sản phẩm</label>
       	<input id="productCodeDlg" type="text" class="InputTextStyle InputText5Style" style="padding-left: 10px"/>
       	<label class="LabelStyle Label2Style">Tên sản phẩm</label>
       	<input id="productNameDlg" type="text" class="InputTextStyle InputText5Style" style="padding-right: 90px" />
       	<div class="Clear"></div>
       	<div class="BtnCenterSection">
           <button id="btnSeachProductTree" class="BtnGeneralStyle" >Tìm kiếm</button>
       	</div>
       	<div class="Clear"></div>
   		</div>
        <div class="GeneralForm SProduct1Form">
       	  	<div class="GridSection" id="treeGridContainer">
                 <table id="treeGrid" class="easyui-treegrid"></table> 
            </div>
           	<div class="Clear"></div>
           	<div class="BtnCenterSection">
               <button id="btnChangeProduct" class="BtnGeneralStyle" onclick="return CRMReportUtils.selectOnDialog();">Chọn</button>
               <button class="BtnGeneralStyle" onclick="$('#dialogProduct').dialog('close');">Đóng</button>
           	</div>
           	<div class="Clear"></div>
           	<p id="errMsgProductDlg"  class="ErrorMsgStyle" style="display: none;"></p>
           	<p id="successMsgProductDlg"  class="SuccessMsgStyle" style="display: none;"></p>
        </div>
	</div>
</div>-->

<script type="text/javascript">

$(document).ready(function(){	
	$("#tDate").change(function(){ 
		var fDay = $('#tDate').val().trim();
		if(fDay.length > 8){
			var arr = fDay.split('/');
			var kq = "01/"+arr[1]+"/"+arr[2];
			$('#fDate').val(kq).show().change();
		}else{
			$('#fDate').val("").show().change();
		}
	});
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	disableDateTimePicker('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$("#tDate").change();
	
	CRMReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	$('#product').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			CRMReportUtils.showDialogSearchProduct();
		}
	});
	
});
</script>
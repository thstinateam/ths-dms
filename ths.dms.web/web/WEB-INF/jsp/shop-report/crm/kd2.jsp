<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Đơn vị<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
			<input id="shop" type="text"  style="width:226px;" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">TBHV (F9)</label>
		</dt>
		<dd class="ClearRight">
			<input id="tbhv" type="text" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">GS NPP (F9)</label>
		</dt>
		<dd>
			<input id="gsnpp" type="text" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">NVBH (F9)</label>
		</dt>
		<dd>
			<input id="nvbh" type="text" class="InputTextStyle InputText1Style" />
		</dd>
		
		<dt>
			<label class="LabelStyle Label1Style">Nhãn hàng (F9)</label>
		</dt>
		<dd class="ClearRight">
			<input id="subcat" type="text" class="InputTextStyle InputText1Style" />
		</dd>
		
		<dt>
			<label class="LabelStyle Label1Style">Đến ngày<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd class="ClearRight">
			<input id="tDate" type="text" class="InputTextStyle InputText2Style"/> 
		</dd>
	</dl>
	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="BtnGeneralStyle" class="BtnGeneralStyle Sprite2" onclick="return CRMReportDSPPTNH.exportKD2();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>                          
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" ></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('tDate');
	$('.MySelectBoxClass').customStyle();
	DebitPayReport._lstStaff = new Map();
	Utils.bindAutoSearch();
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),function(data){	
		if(data!=$('#shopId').val().trim()){
			DebitPayReport._lstStaff = new Map();
			$('#nvbh').val('');
			$('#gsnpp').val('');
			$('#tbhv').val('');
		}
	});
	$('#gsnpp').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var lstStaff = $('#gsnpp').val().trim().split(";");
			DebitPayReport._lstStaff = new Map();
			for(i=0;i<lstStaff.length;i++){
				if(lstStaff[i]!=undefined && lstStaff[i]!=''){
					DebitPayReport._lstStaff.put(lstStaff[i],lstStaff[i]);
				}
			}
			var arrParam = new Array();
			if($('#shopId').val().trim()!=''){
				var obj = {};
				obj.name = 'shopId';
				obj.value = $('#shopId').val().trim();
				arrParam.push(obj);
			}
			//TBHV
			var param2 = new Object();
			param2.name = 'tbhvStaffCode';
			param2.value = $('#tbhv').val().trim();
			arrParam.push(param2);
			
			CommonSearch.searchGSNPPOnDialogCheckbox(function(data){
			},arrParam,'gsnpp');
		}
	});
	$('#tbhv').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var lstStaff = $('#tbhv').val().trim().split(";");
			DebitPayReport._lstStaff = new Map();
			for(i=0;i<lstStaff.length;i++){
				if(lstStaff[i]!=undefined && lstStaff[i]!=''){
					DebitPayReport._lstStaff.put(lstStaff[i],lstStaff[i]);
				}
			}
			var arrParam = new Array();
			if($('#shopId').val().trim()!=''){
				var obj = {};
				obj.name = 'shopId';
				obj.value = $('#shopId').val().trim();
				arrParam.push(obj);
			}
			CommonSearch.searchTBHVOnDialogCheckbox(function(data){
			},arrParam,'tbhv');
		}
	});
	$('#nvbh').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var lstStaff = $('#nvbh').val().trim().split(";");
			DebitPayReport._lstStaff = new Map();
			for(i=0;i<lstStaff.length;i++){
				if(lstStaff[i]!=undefined && lstStaff[i]!=''){
					DebitPayReport._lstStaff.put(lstStaff[i],lstStaff[i]);
				}
			}
			var arrParam = new Array();
			if($('#shopId').val().trim()!=''){
				var obj = {};
				obj.name = 'shopId';
				obj.value = $('#shopId').val().trim();
				arrParam.push(obj);
			}
			//GSNPP
			var param1 = new Object();
			param1.name = 'supervisorStaffCode';
			param1.value = $('#gsnpp').val().trim();
			arrParam.push(param1);
			//TBHV
			var param2 = new Object();
			param2.name = 'tbhvStaffCode';
			param2.value = $('#tbhv').val().trim();
			arrParam.push(param2);
			
			CommonSearch.searchPreAndValStaffHasChildOnDialogCheckbox(function(data){
			},arrParam,'nvbh');
		}
	});
	DebitPayReport._lstSubCat = new Map();
	$('#subcat').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var lstSubCat = $('#subcat').val().trim().split(";");
			DebitPayReport._lstSubCat = new Map();
			for(i=0;i<lstSubCat.length;i++){
				if(lstSubCat[i]!=undefined && lstSubCat[i]!=''){
					DebitPayReport._lstSubCat.put(lstSubCat[i],lstSubCat[i]);
				}
			}
			CommonSearch.searchSubCategoryOnDialogCheckbox(function(data){
			},null,'subcat');
		}
	});
	ReportUtils.setCurrentDateForCalendar('tDate');
});
</script>
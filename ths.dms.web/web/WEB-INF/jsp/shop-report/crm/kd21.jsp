<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="GeneralForm SearchInSection">
 	<label class="LabelStyle Label1Style">Đơn vị<span class="RequireStyle">(*)</span></label>
	<div class="BoxSelect BoxSelect2">
		<input id="shop" style="width:200px;">
 	</div>
 	<label class="LabelStyle Label1Style">Từ ngày</label>
 	<div id="fromDateDiv">
	<input id="fDate" type="text"  class="InputTextStyle InputText1Style" disabled="disabled"/>
 	</div>
 	<label class="LabelStyle Label2Style" >Đến ngày<span class="RequireStyle">(*)</span></label>
	<input id="tDate" type="text"  class="InputTextStyle InputText1Style" />
 	<div class="Clear"></div>
 	<div class="BtnCenterSection">              	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD21();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>  
</div>
<s:hidden id="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	$('#fromDateDiv .ui-datepicker-trigger').unbind('click');	
	ReportUtils.setCurrentDateForCalendar('tDate');
	ReportUtils.loadComboTree('shop','shopCode',$('#curShopId').val());
	$('#tDate').change(function(){
		var txtDate = $('#tDate').val().trim();
		var aoDate,           // needed for creating array and object
	        txtDateTmp;
	    aoDate = txtDate.split('/');
	    if (aoDate.length >= 3) {
	    	txtDateTmp = '01/'+aoDate[1] +'/'+aoDate[2];
	    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
	    		$('#fDate').val(txtDateTmp);
	    	}
	    } 
	});
	setCurrentDayOfMonth();
});
function setCurrentDayOfMonth(){
	var txtDate = $('#tDate').val().trim();
	var aoDate,           // needed for creating array and object
        txtDateTmp;
    aoDate = txtDate.split('/');
    if (aoDate.length >= 3) {
    	txtDateTmp = '01/'+aoDate[1] +'/'+aoDate[2];
    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
    		$('#fDate').val(txtDateTmp);
    	}
    } 
}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã đơn vị<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">Số lần</label>
		</dt>
		<dd>
			<input id="number" type="number" style="width:215px;" class="InputTextStyle InputText2Style" maxlength="10"/>
  			<span class="RequireStyle">(*)</span>
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" style="width:215px;" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
	</dl>
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD19_1();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>             
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="nvbh"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	//setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	ReportUtils.setCurrentDateForCalendar('fDate','01');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$('#fDate').attr('disabled','disabled');
	$('#tDate').bind('change',function(){
		if($('#tDate').val().length == 10){
			$('#fDate').val("01/"+ $(this).val().split("/")[1] +"/" + $(this).val().split("/")[2]);
			$('#fDate').change();
		}
	});
});
</script>
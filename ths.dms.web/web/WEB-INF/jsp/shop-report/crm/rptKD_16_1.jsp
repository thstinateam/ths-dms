<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<!-- <dt class="LabelTMStyle"> -->
		<dt>
			<label class="LabelStyle Label1Style">Từ tháng <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input type="text" id="fromMonth" class="InputTextStyle InputText1Style" />
		</dd>
		
		<!-- <dt class="LabelTMStyle"> -->
		<dt>
			<label class="LabelStyle Label1Style">Đến tháng <span class="ReqiureStyle">(*)</span> </label>
		</dt>
		<dd>
			<input type="text" id="toMonth" class="InputTextStyle InputText1Style" />
		</dd>
	
	</dl>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="CRMReportDSPPTNH.exportKD_16_1();">Xuất báo cáo</button>
	</div>
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
</div>          
<script type="text/javascript">
$(document).ready(function(){
	$('#fromMonth').bind('blur',function(){
		var txtDate = $('#fromMonth').val().trim();
		var aoDate,           // needed for creating array and object
	        txtDateTmp;
	    aoDate = txtDate.split('/');
	    if (aoDate.length >= 3) {
	    	txtDateTmp = aoDate[1] +'/'+aoDate[2];
	    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
	    		$('#fromMonth').val(txtDateTmp);
	    	}
	    } 
	});
	applyDateTimePicker("#fromMonth", "mm/yy", null, null, null, null, null, null, null,true,null);
	$('#toMonth').bind('blur',function(){
		var txtDate = $('#toMonth').val().trim();
		var aoDate,           // needed for creating array and object
	        txtDateTmp;
	    aoDate = txtDate.split('/');
	    if (aoDate.length >= 3) {
	    	txtDateTmp = aoDate[1] +'/'+aoDate[2];
	    	if(aoDate[1] != null && aoDate[1] != undefined && aoDate[1] != '' && aoDate[2] != null && aoDate[2] != undefined && aoDate[2] != ''){
	    		$('#toMonth').val(txtDateTmp);
	    	}
	    } 
	});
	applyDateTimePicker("#toMonth", "mm/yy", null, null, null, null, null, null, null, true,null);
	ReportUtils.setCurrentMonthForCalendar('fromMonth');
	ReportUtils.setCurrentMonthForCalendar('toMonth');	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<label class="LabelStyle Label1Style">Từ tháng</label>
	<div class="Field2">
  		<input id="fromMonth" type="text" style="width:168px;" class="InputTextStyle InputText1Style" value="<s:property value="fromDate"/>"/>
  		<span class="RequireStyle">(*)</span>
 	</div>
 	<label class="LabelStyle Label2Style">Đến tháng</label>
 	<div class="Field2">
  		<input id="toMonth" type="text" style="width:168px;" class="InputTextStyle InputText1Style" value="<s:property value="toDate"/>"/>
  		<span class="RequireStyle">(*)</span>
 	</div>
 	<div class="Clear"></div> 
 	<div class="ButtonSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportBCKD18();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
</div>            
<script type="text/javascript">
$(document).ready(function(){	
	//setDateTimePicker('fromMonth');
	$('#fromMonth').val(getCurrentMonth());
	applyDateTimePicker("#fromMonth", "mm/yy",null,null,null,null,null,null,null,true);
	//setDateTimePicker('toMonth');
	$('#toMonth').val(getCurrentMonth());
	applyDateTimePicker("#toMonth", "mm/yy",null,null,null,null,null,null,null,true);
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
   <label class="LabelStyle Label1Style">Đơn vị</label>                
   <div class="BoxSelect BoxSelect2">
       <div class="Field2">
			<input id="shop" style="width:200px;">
			<span class="RequireStyle">(*)</span>
     </div>
 </div>
 <label class="LabelStyle Label2Style">Số lần</label>
 <div class="Field2">
 	<input id="badscore" type="text" style="width:188px;" maxlength ="3" class="InputTextStyle InputText3Style" />
 	<span class="RequireStyle">(*)</span>
 </div>
  <div class="Clear"></div>
  <label class="LabelStyle Label1Style">Từ ngày</label>
  <div class="Field2">
  <input id="fDate" type="text" style="width:188px;" class="InputTextStyle InputText2Style" disabled="disabled"/>
 </div>
 <label class="LabelStyle Label2Style">Đến ngày</label>
 <div class="Field2">
  <input id="tDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" onChange="myFun();"/>
  <span class="RequireStyle">(*)</span>
 </div>
 <div class="Clear"></div>
 <div class="ButtonSection">                	
 	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return CRMReportDSPPTNH.exportKD19();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 </div>
 <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
</div>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="kieuTruyen2"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	Utils.bindFormatOnTextfield('badscore',Utils._TF_NUMBER,'');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	ReportUtils.setCurrentDateForCalendar('tDate');
	var tDate = $('#tDate').val().trim();
	$('#fDate').val("01/"+tDate.substring(tDate.indexOf('/')+1,tDate.length));
});
function myFun(){
	var tDate = $('#tDate').val().trim();
	$('#fDate').val("01/"+tDate.substring(tDate.indexOf('/')+1,tDate.length));
}
</script>
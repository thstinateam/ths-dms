<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dl class="Dl3Style">
<dt>
		<label class="LabelStyle Label1Style">Đơn vị
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="shop" type="text"  style="width:226px;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">GS NPP (F9)</label>
		
	</dt>
	<dd class="ClearRight">
		<input id="staffOwnerCode" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="ClearLeft">
		<label class="LabelStyle Label1Style">Mã NVBH (F9)</label>
	</dt>
	<dd>
		<input id="saleStaffCode" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">Ngày
		<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd class="ClearRight">
		<input id="fromDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="fromDate"/>"/> 
	</dd>
	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ giờ (0 đến 23)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="fromHour" type="text" class="InputTextStyle InputText1Style" value="<s:property value="fromHour"/>" />
	</dd>
	
	<dt class="LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ phút (0 đến 59)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="fromMinute" type="text" class="InputTextStyle InputText1Style" value="0" />
	</dd>
	
	<dt class="ClearLeft LabelTMStyle">
		<label  class="LabelStyle Label1Style">Đến giờ (0 đến 23)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="toHour" type="text" class="InputTextStyle InputText1Style" value="<s:property value="toHour"/>" />
	</dd>
	
	<dt class="LabelTMStyle">
		<label  class="LabelStyle Label1Style">Đến phút(0 đến 59)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="toMinute" type="text" class="InputTextStyle InputText1Style" value="0" />
	</dd>
	<dt class="ClearLeft">
		<label class="LabelStyle Label1Style">Số phút báo cáo
		<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<div class="BoxSelect BoxSelect1">
		<select id="sprTime" class="MySelectBoxClass">
			<option value="30">30 phút</option>
			<option value="60">1 tiếng</option>
			<option value="90">1 tiếng 30 phút</option>
			<option value="120">2 tiếng</option>
			<option value="150">2 tiếng 30 phút</option>
			<option value="180">3 tiếng</option>
		</select>
		</div>
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="CRMReportDSPPTNH.exportBCDS3();">Xuất Excel</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(
		function() {
			setDateTimePicker('fromDate');
			$('#sprTime').customStyle();
			ReportUtils.loadComboReportTree('shop', 'shopCode', $(
					'#curShopId').val());
			$('#staffOwnerCode').bind('keyup', function(event) {
				if (event.keyCode == keyCode_F9) {
					var arrParam = new Array();
					var obj = {};
					obj.name = 'shopCode';
					obj.value = $('#shopCode').val().trim();
					arrParam.push(obj);
					CommonSearch.searchNVGSReportOnEasyUIDialog(function(data) {
						if($('#staffOwnerCode').val().trim()!=data.code){
							$('#saleStaffCode').val('');
						}
						$('#staffOwnerCode').val(data.code);
					}, arrParam);
				}
			});
			$('#saleStaffCode').bind('keyup', function(event) {
				if (event.keyCode == keyCode_F9) {
					// 			$('#errMsg').html('').hide();
					// 			var msg = Utils.getMessageOfRequireCheck('shopCode', 'Mã đơn vị');
					// 			if(msg.length>0){
					// 				showMsgTimeOut('errMsg',msg,1000);
					// 				return false;
					// 			}
					var shopCode = $('#shopCode').val();
					var arrParam = new Array();
					var param = new Object();
					param.name = 'shopCode';
					param.value = shopCode;
					arrParam.push(param);
					var staffOwnerCode = $('#staffOwnerCode').val();
					if (staffOwnerCode != null && staffOwnerCode != "") {
						var param1 = new Object();
						param1.name = 'staffOwnerCode';
						param1.value = staffOwnerCode;
						arrParam.push(param1);
					}
					CommonSearch.searchNVBHOnEasyUIDialog(function(data) {
						$('#saleStaffCode').val(data.code);
					}, arrParam);
				}
			});
		});
</script>                	
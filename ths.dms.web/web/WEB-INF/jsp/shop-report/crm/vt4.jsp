<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="shop" type="text"  style="width:226px;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">GS NPP (F9)</label>
		
	</dt>
	<dd class="ClearRight">
		<input id="staffOwnerCode" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="ClearLeft">
		<label class="LabelStyle Label1Style">Mã NVBH (F9)</label>
	</dt>
	<dd>
		<input id="saleStaffCode" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	<dt>&nbsp;</dt>
	<dd>&nbsp;</dd>
	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="fromDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="fromDate"/>"/> 
	</dd>
	<dt class="LabelTMStyle">
		<label  class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy)
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd>
		<input id="toDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="toDate"/>"/> 
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="CRMReportDSPPTNH.exportBCVT4();">Xuất Excel</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
	$(document).ready(
			function() {
				setDateTimePicker('fromDate');
				setDateTimePicker('toDate');
				ReportUtils.loadComboReportTree('shop', 'shopCode', $('#curShopId').val(), function(){
					$('#staffOwnerCode').val('');
					$('#saleStaffCode').val('');
				});
				$('#staffOwnerCode').bind('keyup', function(event) {
					if (event.keyCode == keyCode_F9) {
						var arrParam = new Array();
						var obj = {};
						obj.name = 'shopCode';
						obj.value = $('#shopCode').val().trim();
						arrParam.push(obj);
						CommonSearch.searchNVGSReportOnEasyUIDialog(function(data) {
							$('#staffOwnerCode').val(data.code);
							$('#saleStaffCode').val('');
						}, arrParam);
					}
				});
				$('#staffOwnerCode').bind('change', function(event) {
					$('#saleStaffCode').val('');
				});
				$('#saleStaffCode').bind('keyup', function(event) {
					if (event.keyCode == keyCode_F9) {
						var shopCode = $('#shopCode').val();
						var arrParam = new Array();
						var param = new Object();
						param.name = 'shopCode';
						param.value = shopCode;
						arrParam.push(param);
						var staffOwnerCode = $('#staffOwnerCode').val();
						if (staffOwnerCode != null && staffOwnerCode != "") {
							var param1 = new Object();
							param1.name = 'staffOwnerCode';
							param1.value = staffOwnerCode;
							arrParam.push(param1);
						}
						CommonSearch.searchNVBHOnEasyUIDialog(function(data) {
							$('#saleStaffCode').val(data.code);
						}, arrParam);
					}
				});
			});
</script>                	
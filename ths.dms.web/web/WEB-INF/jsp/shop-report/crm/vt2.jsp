<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị</label>
	</dt>
	<dd>
		<input id="shop" type="text"  style="width:226px;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">GS NPP (F9)</label>
	</dt>
	<dd class="ClearRight">
		<input id="superStaffCode" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	<dt class="ClearLeft">
		<label class="LabelStyle Label1Style">NVBH (F9)</label>
	</dt>
	<dd>
		<input id="saleStaffCode" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">Số phút tắt vượt quá		
			<span class="ReqiureStyle">(*)</span>
		</label>
	</dt>
	<dd class="ClearRight">
		<input id="minutes" maxlength="6" type="text" class="InputTextStyle InputText1Style" />
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">Từ ngày<span class="ReqiureStyle">(*)</span></label>
	</dt>
	<dd class="ClearRight">
		<input id="fromDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="fromDate"/>"/> 
	</dd>
	<dt>
		<label class="LabelStyle Label1Style">Đến ngày<span class="ReqiureStyle">(*)</span></label>
	</dt>
	<dd class="ClearRight">
		<input id="toDate" type="text" class="InputTextStyle InputText2Style" value="<s:property value="toDate"/>"/> 
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button class="BtnGeneralStyle" onclick="CRMReportDSPPTNH.exportBCVT2();">Xuất Excel</button>
</div>
<input type="hidden" id="shopId" />
<input type="hidden" id="n_minutes" value="<s:property value="n_minutes"/>" />
<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<script type="text/javascript">
	$(document).ready(function(){
			ReportUtils.loadComboTree('shop', 'shopCode', $('#curShopId').val(),function(id){
				if(id!=$('#shopId').val().trim()){
					$('#superStaffCode').val('');
					$('#saleStaffCode').val('');
				}
			});
			Utils.bindFormatOnTextfield('minutes',Utils._TF_NUMBER);
			setDateTimePicker('fromDate');
			setDateTimePicker('toDate');		
						
			$('#superStaffCode').bind('keyup', function(event) {
				if (event.keyCode == keyCode_F9) {
					var arrParam = new Array();
					var obj = {};
					obj.name = 'shopId';
					obj.value = $('#shop').combotree('getValue');
					arrParam.push(obj);
					CommonSearch.searchNVGSReportOnEasyUIDialog(function(data) {
						$('#superStaffCode').val(data.code);
					}, arrParam);
				}
			});
			$('#saleStaffCode').bind('keyup', function(event) {
				if (event.keyCode == keyCode_F9) {				
					var arrParam = new Array();
					var param = new Object();
					param.name = 'shopId';
					param.value = $('#shop').combotree('getValue');
					arrParam.push(param);
					var staffOwnerCode = $('#superStaffCode').val();
					if (staffOwnerCode != null && staffOwnerCode != "") {
						var param1 = new Object();
						param1.name = 'staffOwnerCode';
						param1.value = staffOwnerCode;
						arrParam.push(param1);
					}
					CommonSearch.searchNVBHOnEasyUIDialog(function(data) {
						$('#saleStaffCode').val(data.code);
					}, arrParam);
			}
		});
	});	
</script>                	
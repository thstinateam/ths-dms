<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<dl class="Dl3Style">
	<dt>
		<label class="LabelStyle Label1Style">Đơn vị</label>
	</dt>
	<dd>
		<input id="shop" type="text"  style="width:226px;" class="InputTextStyle InputText1Style" />
	</dd>
	<dt>&nbsp;</dt>
	<dd>&nbsp;</dd>
	<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
	</dt>
	<dd>
		<input id="fromDate" type="text"
			class="InputTextStyle InputText2Style"
			value="<s:property value="fromDate"/>" />
	</dd>
	<dt class="LabelTMStyle">
		<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span> </label>
	</dt>
	<dd>
		<input id="toDate" type="text" class="InputTextStyle InputText2Style"
			value="<s:property value="toDate"/>" />
	</dd>
</dl>
<div class="Clear"></div>
<div class="BtnCenterSection">
	<button id="btnSearch" class="BtnGeneralStyle"
		onclick="CRMReportDSPPTNH.exportBCDS1();">Xuất Excel</button>
</div>
<p id="errMsg" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fromDate');
	setDateTimePicker('toDate');
	$('#status').customStyle();
	ReportUtils.loadComboReportTree('shop','shopCode',$('#curShopId').val());
});
</script> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

            	<div class="ModuleList3Form">
                <label class="LabelStyle Label1Style">Từ ngày</label>
                <div class="Field2" id="fromDateDiv">
                	<input id="fDate" type="text" class="InputTextStyle InputText2Style" style="width:168px;" />
                	<span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label1Style">Đến ngày</label>
                <div class="Field2">
                 <input id="tDate" type="text" class="InputTextStyle InputText2Style" style="width:168px;" />
	                <span class="RequireStyle">(*)</span>
                </div>
                <div class="Clear"></div>                
                <div class="ButtonSection">                	
                	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return ImportBonus.reportImportBonus();"><span class="Sprite2">Xuất báo cáo</span></button>                	
                </div>
                <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
               </div>

<s:hidden id="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	$('#fDate').val('01/' + getCurrentMonth());
	disabled('fDate');
	$('#fromDateDiv .ui-datepicker-trigger').unbind('click');
	$('#tDate').val(getCurrentDate());
});
</script>
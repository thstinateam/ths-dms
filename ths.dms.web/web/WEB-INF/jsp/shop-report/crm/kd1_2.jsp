<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div>
	<div>
		<div class="SearchInSection PhotoSearchSection">
			<div style="margin-left: 30px; margin-top: -10px;">
				<label class="LabelStyle Label1Style">Đơn vị</label>
				<div class="BoxSelect BoxSelect2">
					<div class="Field2">
						<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:238px;height: auto;  margin-right: 70px;" class="InputTextStyle InputText1Style" />
					</div>
				</div>
				
				<label class="LabelStyle Label1Style" style="float: left;" >Sản Phẩm(F9)</label>
				<input id="product" style="width: 217px" type="text" class="InputTextStyle InputText1Style" />  
				
				<div class="Clear"></div>
				<label  class="LabelStyle Label1Style">Đến Ngày<br />(dd/MM/yyyy)
					<span class="ReqiureStyle">(*)</span>
				</label>
				<input id="tDate" type="text" style="width:217px" class="InputTextStyle InputText1Style"/>
			</div>
		</div>
	</div>
</div>
	<div class="Clear"></div>
	<div class="BtnCenterSection">
		<button id="btnSearch" class="BtnGeneralStyle" onclick="CRMReportDSPPTNH.exportKD1_2();">Xuất báo cáo</button>
	</div>
	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>

<s:hidden id="shopId"></s:hidden>
<s:hidden id="shopCode"></s:hidden>

<s:hidden id="curShopId" name="shopId"></s:hidden>
<s:hidden id="curShopCode" name="shopCode"></s:hidden>

<script type="text/javascript">

$(document).ready(function(){	
	setDateTimePicker('tDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "id",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			if(data.type.channelTypeCode == 'VNM') {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.id)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
				}else if(data.type.channelTypeCode == 'MIEN'){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.id)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(data.type.channelTypeCode == 'VUNG'){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.id)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(data.type.channelTypeCode == 'NPP'){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.id)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendoui-combobox.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");

    $(window).bind('keypress',function(event){
		if($('.fancybox-inner').length!=0){
			if(event.keyCode == keyCodes.ARROW_LEFT || event.keyCode == keyCodes.ARROW_UP){
				Images.showImageNextPre(0);
			}else if(event.keyCode == keyCodes.ARROW_RIGHT || event.keyCode == keyCodes.ARROW_DOWN){
				Images.showImageNextPre(1);
			}
			return false;
		}
		return true;
	});
    
//	CRMReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	
//	$('#staffCode').bind('keyup', function(event) {
//		if (event.keyCode == keyCode_F9) {
//			CRMReportUtils.showDialogSearchNVBH($('#shop').combotree('getValue'));
// 			var staffOwnerCode = $('#staffOwnerCode').val();
// 			var arrParam = new Array();
// 			var param = new Object();
// 			param.name = 'shopId';
// 			param.value = $('.combo-value').val();
// 			arrParam.push(param);
// 			if (staffOwnerCode != null && staffOwnerCode != "") {
// 				var param1 = new Object();
// 				param1.name = 'staffOwnerCode';
// 				param1.value = staffOwnerCode;
// 				arrParam.push(param1);
// 			}
// 			CommonSearch.searchNVBHOnDialog(function(data){
// 				$('#staffCode').val(data.code);
// 				},arrParam);
// 			}
//		});
	
//	$('#staffOwnerCode').bind('keyup', function(event){
//		if(event.keyCode == keyCode_F9){
//			CRMReportUtils.showDialogSearchGSNPP($('#shop').combotree('getValue'));
// 			var lstParam = new Array();
// 			var obj = new Object();
// 			obj.name = 'shopId';
// 			obj.value = $('.combo-value').val();
// 			lstParam.push(obj);
// 			CommonSearch.searchNVGSOnDialog(function(data){
// 				$('#staffOwnerCode').val(data.code);
// 			},lstParam);
//		}
//	});
	
// 	$('#staffTBHV').bind('keyup', function(event){
// 		if(event.keyCode == keyCode_F9){
// 			CRMReportUtils.showDialogSearchTBHV($('#shop').combotree('getValue'));
// 			var lstParam = new Array();
// 			var obj = new Object();
// 			obj.name = 'shopId';
// 			obj.value = $('.combo-value').val();
// 			lstParam.push(obj);
// 			CommonSearch.searchTBHVOnDialogCheckbox(function(data){
// 				$('#staffTBHV').val(data.code);
// 			},lstParam);
// 		}
// 	});
	
	$('#product').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			CRMReportUtils.showDialogSearchProduct();

		}
	});
});
</script>
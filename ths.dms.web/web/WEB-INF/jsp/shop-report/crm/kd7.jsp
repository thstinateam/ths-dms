<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
            	<div class="ModuleList3Form">
                <label class="LabelStyle Label1Style">Đơn vị</label>                
                <div class="BoxSelect BoxSelect2">
                      <div class="Field2">
           					<input id="shop" style="width:200px;">           	 				
                   	</div>
                </div>
                <div class="Clear"></div> 
                <label class="LabelStyle Label1Style">Từ ngày</label>
                <div class="Field2" id="fromDateDiv">
                	<input id="fDate" type="text" style="width: 168px" class="InputTextStyle InputText2Style" />
                	<span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label3Style">Đến ngày</label>
                <div class="Field2">
                 <input id="tDate" type="text" style="width: 168px" class="InputTextStyle InputText2Style" />
	                <span class="RequireStyle">(*)</span>
                </div>
                <div class="Clear"></div>                
                <div class="ButtonSection">                	
                	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD007();"><span class="Sprite2">Xuất báo cáo</span></button>                	
                </div>
                <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
               </div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val(),null,null,1);	
	ReportUtils.setCurrentDateForCalendar('tDate');
	$('#fDate').val('01/' + getCurrentMonth());
	disabled('fDate');
	$('#fromDateDiv .ui-datepicker-trigger').unbind('click');	
	$('#tDate').bind('change',function(){
		var msg = Utils.getMessageOfInvalidFormatDate('tDate','Đến ngày');
		if(msg.length > 0){
			$('#errMsg').html(msg).show();
			return false;
		}	
		if($(this).val().length>0){
			var DATE = $('#tDate').val().trim();
			var MM = DATE.split('/')[1];
			var YYYY = DATE.split('/')[2];
			$('#fDate').val('01/' + MM + '/' + YYYY);
		}		
	});
});
</script>
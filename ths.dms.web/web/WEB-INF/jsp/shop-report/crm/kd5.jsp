<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<!-- me -->
	<dl class="Dl3Style">
		<dt>
			<label class="LabelStyle Label1Style">Mã đơn vị<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input type="text" id="shop" style="width:226px;" class="InputTextStyle InputText1Style" />
		</dd>
		<dt>
			<label class="LabelStyle Label1Style">Ngành hàng</label>
		</dt>
		<dd>
			<div class="BoxSelect BoxSelect2">
				<select id="category" multiple="multiple">
  					<option value="" selected="selected">Tất cả</option>
  					<s:iterator value="lstCategory">
  						<option value="<s:property value="id"/>"><s:property value="productInfoCode"/>-<s:property value="productInfoName"/></option>
  					</s:iterator>
         		</select>
			</div>
		</dd>
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
	</dl>
 	<div class="Clear"></div> 
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD5();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
var type = '<s:property value="type"/>';
$(document).ready(function(){	
	setDateTimePicker('tDate');
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	$('#category').dropdownchecklist({
		emptyText:'Tất cả',
		forceMultiple: true,
		firstItemChecksAll: true,
		maxDropHeight: 350,
		width: 226
	});
	ReportUtils.setCurrentDateForCalendar('fDate','01');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$('#fDate').attr('disabled','disabled');
	$('#tDate').bind('change',function(){
		if($('#tDate').val().length == 10){
			$('#fDate').val("01/"+ $(this).val().split("/")[1] +"/" + $(this).val().split("/")[2]);
			$('#fDate').change();
		}
	});
});
</script>
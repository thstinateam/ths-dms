<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
   	<label class="LabelStyle Label1Style">Đơn vị</label>                
   	<div class="BoxSelect BoxSelect2">
       <div class="Field2">
			<input id="shop" style="width:200px;">
			<span class="RequireStyle">(*)</span>
     	</div>
 	</div>
 	<div class="Clear"></div>
 	<label class="LabelStyle Label1Style">Từ tháng</label>
 	<div class="Field2">
 		<input id="fDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
 		<span class="RequireStyle">(*)</span>
 	</div>
 	<label class="LabelStyle Label2Style">Đến tháng</label>
 	<div class="Field2">
  		<input id="tDate" type="text" style="width:168px;" class="InputTextStyle InputText2Style" />
  		<span class="RequireStyle">(*)</span>
 	</div>
 	<div class="Clear"></div> 
 	<div class="ButtonSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="CRMReportDSPPTNH.exportKD13();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div>
 	<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>               
</div>            
<s:hidden id="shopId"></s:hidden>
<s:hidden id="staffCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	/* setDateTimePicker('fDate');
	setDateTimePicker('tDate'); */	
	ReportUtils.loadComboTree('shop','shopId',$('#curShopId').val());
	applyDateTimePicker("#fDate", "mm/yy",null,null,null,null,null,null,null,true);
	applyDateTimePicker("#tDate", "mm/yy",null,null,null,null,null,null,null,true);
	$('#fDate').val(getCurrentMonth());
	$('#tDate').val(getCurrentMonth());
	$('#fDate').change(function(){
		if($(this).val().length > 0){
			var MM = '';
			var yyyy = '';
			if($(this).val().length == 7){
				MM = $(this).val().split("/")[0];
				yyyy = $(this).val().split("/")[1];	
			}else if($(this).val().length == 10){
				MM = $(this).val().split("/")[1];
				yyyy = $(this).val().split("/")[2];
			}		
			$('#fDate').val(MM+"/"+yyyy);
		}
	});
	$('#tDate').change(function(){
		if($(this).val().length > 0){
			var MM = '';
			var yyyy = '';
			if($(this).val().length == 7){
				MM = $(this).val().split("/")[0];
				yyyy = $(this).val().split("/")[1];	
			}else if($(this).val().length == 10){
				MM = $(this).val().split("/")[1];
				yyyy = $(this).val().split("/")[2];
			}		
			$('#tDate').val(MM+"/"+yyyy);
		}
	});
});
</script>
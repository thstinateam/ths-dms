<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<dl class="Dl3Style">
		<dt class="ClearLeft LabelTMStyle">
			<label class="LabelStyle Label1Style">Tháng<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Số tháng<span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="inputValue" type="text" class="InputTextStyle InputText2Style" />
		</dd>
</dl>
<div class="Clear"></div> 
<div class="BtnCenterSection">                	
	<button id="btnExport" class="BtnGeneralStyle" onclick="CRMReportDSPPTNH.exportKD15();" >Xuất báo cáo</button>                	
</div>
<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>  
<input type="hidden" id="tmptDate" />          
<script type="text/javascript">
$(document).ready(function(){
	Utils.bindFormatOnTextfield('inputValue', Utils._TF_NUMBER);
	$('#tDate').val(getCurrentMonth());
	applyDateTimePicker("#tDate", "mm/yy",null,null,null,null,null,null,null,true);
});
</script>
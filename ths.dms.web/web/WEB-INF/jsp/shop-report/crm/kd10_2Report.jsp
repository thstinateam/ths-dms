<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList3Form">
	<dl class="Dl3Style">
		<dt>
			<label style="width: 90px; float: right;" class="LabelStyle Label1Style">Ngành hàng<span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
			<div class="BoxSelect BoxSelect1">
			  	<select id="subCat" class="MySelectBoxClass">
					<option value="-1">---Chọn ngành hàng---</option>
					<s:iterator value="lstSubCat">					
						<option value='<s:property value="id"/>'><s:property value="productInfoCode"/> - <s:property value="productInfoName"/></option>
					</s:iterator>		    
			   	</select>
		   	</div>
		</dd>

		<dt class="ClearLeft LabelTMStyle">
		<label class="LabelStyle Label1Style">Từ ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span></label>
		</dt>
		<dd>
			<input id="fDate" type="text" class="InputTextStyle InputText2Style" />
		</dd>
		<dt class="LabelTMStyle">
			<label class="LabelStyle Label1Style">Đến ngày<br />(dd/MM/yyyy) <span class="ReqiureStyle">(*)</span>
			</label>
		</dt>
		<dd>
			<input id="tDate" type="text" class="InputTextStyle InputText2Style"  onchange="CRMReportDSPPTNH.tDateChangeByfDateIntoFirtMonth();" />
		</dd>
	</dl>

	<div class="Clear"></div>
 	<div class="BtnCenterSection">                	
 		<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return CRMReportDSPPTNH.exportKD10_2();"><span class="Sprite2">Xuất báo cáo</span></button>                	
 	</div> 	
 	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>               
</div>

<script type="text/javascript">
$(document).ready(function(){
	$("#tDate").change(function(){ 
		var fDay = $('#tDate').val().trim();
		if(fDay.length > 8){
			var arr = fDay.split('/');
			var kq = "01/"+arr[1]+"/"+arr[2];
			$('#fDate').val(kq).show().change();
		}else{
			$('#fDate').val("").show().change();
		}
	});
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	disableDateTimePicker('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');
	$("#tDate").change();
// 	setDateTimePicker('toDate',null,true);
	$('#subCat').customStyle();
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="BreadcrumbSection">
                <ul class="ResetList FixFloat BreadcrumbList">
                    <li class="Sprite1"><a href="#">Danh mục</a></li>
                    <li><span>Danh sách ngành hàng con phụ</span></li>
                </ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Mã ngành hàng</label> <input
						id="productInfoMap" maxlength="20" type="text"
						class="InputTextStyle InputText1Style" /> <label
						class="LabelStyle Label1Style">Mã NH con phụ</label> <input
						id="productInfoCode" maxlength="20" type="text"
						class="InputTextStyle InputText1Style" /> <label
						class="LabelStyle Label1Style">Tên NH con phụ</label> <input
						id="productInfoName" maxlength="50" type="text"
						class="InputTextStyle InputText1Style" />
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle cmsiscontrol"
							onclick="return SubCategorySecondary.search();">Tìm kiếm</button>
					</div>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="GeneralCntSection">
			<h2 class="Title2Style">Danh sách ngành hàng con phụ</h2>
			<div class="GridSection">
				<div class="ResultSection" id="subCategoryGrid">
					<p id="gridNoResult" style="display: none"
						class="WarningResultStyle">Không có kết quả</p>
					<table id="grid"></table>
					<div id="pager"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="Clear"></div>
<div id="divDialog" style="visibility: hidden;">           
  <div id="popup1" class="easyui-dialog" title="Thông tin ngành hàng con phụ" style="width:580px;height:210px;" data-options="closed:true,modal:true" >
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form" id="fm">
                <label class="LabelStyle Label1Style">Mã NH</label>
				<div class="BoxSelect BoxSelect4">
					<select class="MySelectBoxClass" id="productInfoMapPop">
						<option value="-1"></option>
						<s:iterator value="lstCategoryType">
							<option value="<s:property value="productInfoCode" />">
								<s:property value="productInfoCode" />
							</option>
						</s:iterator>
					</select>
				</div>
				<label class="LabelStyle LabelPLStyle"><span class="ReqiureStyle">(*)</span></label>
				<label class="LabelStyle Label2Style">Mã NH con phụ</label>
                <input id="productInfoCodePop" maxlength="20" type="text" class="InputTextStyle InputText2Style"/>
                <label class="LabelStyle LabelPLStyle"><span class="ReqiureStyle">(*)</span></label>
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style">Tên NH con phụ</label>
                <input id="productInfoNamePop" maxlength="50" type="text" class="InputTextStyle InputText3Style" style="width:411px"/>
                <label class="LabelStyle LabelPLStyle"><span class="ReqiureStyle">(*)</span></label>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnEdit" onclick="return SubCategorySecondary.save();" class="BtnGeneralStyle">Cập nhật</button>
                </div>
                <div class="Clear"></div>
	            <p id="errMsgPop" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
	            <p id="successMsg" style="display: none;" class="SuccessMsgStyle"></p>
            </div>
		</div>
    </div>
  </div>  
<s:hidden id="selId" value="0"></s:hidden>
<s:hidden id="focusTextDefault" value="code"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#productInfoMap').focus();
	var status = 1;
	$('#productInfoCode').val('');
	$('#productInfoName').val('');
	$('#productInfoMapPop, #productInfoCodePop, #productInfoNamePop').bind('keyup', function(event){
		if(event.keyCode == keyCodes.ENTER){
			$('#btnEdit').click();
		}
	});
	Utils.bindAutoButtonEx('.ContentSection','btnSearch');
	var params = new Object();
	params.productInfoMap = $('#productInfoMap').val().trim();
	params.productInfoCode = $('#productInfoCode').val().trim();
	params.productInfoName = $('#productInfoName').val().trim();
	params.status = 1;
	$('.RequireStyle').hide();
	$("#grid").datagrid({
		url : '/catalog/sub_category_sec_catalog/search',
		  pageList  : [10,20,30],
		  height:'auto',
		  scrollbarSize : 0,
		  pagination:true,			  
		  fitColumns:true,
		  singleSelect:true,
		  method : 'GET',
		  rownumbers: true,
		  width: ($('#subCategoryGrid').width()-10),
		  queryParams:params,
		  columns:[[	
			{field: 'parentCode', title: 'Mã ngành hàng',width: 80, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field: 'productInfoCode', title: 'Mã ngành hàng con phụ', width: 100, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},		    							  
		    {field: 'productInfoName', title: 'Tên ngành hàng con phụ', width: 160, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field: 'edit', title:'<a id = "btnAddControl" style="display:none;" href= "javascript:void(0)" onclick= "return SubCategorySecondary.addSubCategorySecondary();"><img src="/resources/images/icon_add.png"/></a>',
		    		width: 15, align: 'center',sortable:false,resizable:false, formatter: SubCategorySecondaryFormatter.editCellIconFormatter},							   					    
		    {field: 'productInfoId', index: 'productInfoId', hidden: true},	   
		  ]],
		  onLoadSuccess :function(data){	    	
		    	$('.datagrid-header-rownumber').html('STT');	    	
		       updateRownumWidthForJqGrid('.easyui-dialog');
		       showIconAddOfFirstControlGrid('btnAddGrid', 'btnAddControl');		       		      
		  }		  
		});		
});

</script>
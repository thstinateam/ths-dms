<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="k" uri="/kryptone" %>

<table border="0" cellspacing="0" cellpadding="0">
       <colgroup>
           <col style="width:150px;" />
           <col style="width:150px;" />
           <col style="width:380px;"/>
       </colgroup>
       <tbody>
	       <k:repeater value="listBeans" status="u">
				<k:itemTemplate>
					<tr>
				       <td class="ColsTd1 AlignCenter"><s:property value='content1'/></td>
				       <td class="ColsTd2 AlignCenter"><s:property value='content2'/></td>
				       <td class="ColsTd3 AlignLeft"><s:property value='content3'/></td>
				    </tr>
			    </k:itemTemplate>
			</k:repeater>
       </tbody>
</table>
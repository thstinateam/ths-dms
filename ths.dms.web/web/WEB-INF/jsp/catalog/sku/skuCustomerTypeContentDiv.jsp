<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="Breadcrumbs">
    <span class="Breadcrumbs1Item Sprite1">Chỉ tiêu SKU theo loại khách hàng</span>
</div>
<div class="GeneralMilkBox" id="skuCustomerDiv">
   	<div class="GeneralMilkTopBox">
       	<div class="GeneralMilkBtmBox">
			<h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin tìm kiếm</span></h3>
            <div class="GeneralMilkInBox ResearchSection">
            	<div class="ModuleList8Form">
                <label class="LabelStyle Label1Style">Mã loại KH</label>
                <div class="Field2">
                	<div class="BoxSelect BoxSelect1">
	                	<s:select id="code" headerKey="-2" headerValue="-- Chọn loại khách hàng --"  cssClass="MySelectBoxClass" list="lstChannelType" listKey="channelTypeCode" listValue="channelTypeCode" onchange="skuCodeCustomerChanged();"></s:select>                    
	                </div>
                	<span class="RequireStyle">(*)</span>
                </div>                
                <label class="LabelStyle Label2Style">Tên loại KH</label>
                <div class="Field2">
                	<input id="name" type="text" class="InputTextStyle InputText1Style" maxlength="50">
                	<span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label3Style">Chỉ tiêu SKU</label>
                <div class="Field2">
                <input id="skuAmount" type="text" class="InputTextStyle InputText1Style" maxlength="13">
                <span class="RequireStyle">(*)</span>
                </div>
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style">Trạng thái</label>
                <div class="Field2">
	                <div class="BoxSelect BoxSelect1">
	                	<s:select id="status" headerKey="-2" headerValue="-- Chọn trạng thái --"  name="status" cssClass="MySelectBoxClass" list="lstStatusBean" listKey="value" listValue="name"></s:select>                    
	                </div>
	                <span class="RequireStyle">(*)</span>
                </div>
                <div class="Clear"></div>
                <div class="ButtonSection">
                	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return SkuCatalog.search()"><span class="Sprite2">Tìm kiếm</span></button>
                	<button id="btnAdd" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return SkuCatalog.getChangedForm();"><span class="Sprite2">Thêm mới</span></button>
                	<button id="btnEdit" class="BtnGeneralStyle Sprite2" onclick="return SkuCatalog.save()" style="display: none"><span class="Sprite2">Cập nhật</span></button>
                	<button id="btnCancel" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return SkuCatalog.resetForm();" style="display: none"><span class="Sprite2">Bỏ qua</span></button>
                	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                </div>
                <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>
                <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
               </div>
            </div>            
		</div>		 
    </div>   
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Danh sách mục tiêu</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="gridContainer">                    
                    <table id="grid"></table>
					<div id="pager"></div>
                </div>
            </div>
		</div>
    </div>
</div>
<div class="FuncBtmSection">
<form action="/catalog/sku-customer-type/import-excel" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
<div class="DivInputFileSection">
		<input type="hidden" id="typeButton" name="typeButton" value="0"/> 
		<a id="exportExcel" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return SkuCatalog.exportExcel();" href="javascript:void(0)">
			<span class="Sprite2">Xuất file Excel</span>
		</a>
		<a id="ViewExcel" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return SkuCatalog.ViewFileUpload();" href="javascript:void(0)">
			<span class="Sprite2">Xem file Excel</span>
		</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		
		<p class="ExcelLink">
			<a id="downloadTemplate" href="javascript:void(0)">Tải mẫu file excel</a>
		</p>		
			<div class="DivInputFile">
	            <input type="file" class="InputFileStyle" size="22" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
	            <div class="FakeInputFile">
	                <input id="fakefilepc" type="text" readonly="readonly" class="InputTextStyle">
	            </div>
	        </div>	    
	     <button class="BtnGeneralStyle Sprite2"><span class="Sprite2">Nhập từ file</span></button>
	    <div class="Clear"></div>
</div>
</form>
</div>
<div id="viewExcelDIV" style="display: none">
		<div class="GeneralDialog General2Dialog">
        	<div class="DialogProductSearch">
            	<div class="GeneralTable Table40Section">
            	<div class="ScrollSection">
            				<div class="BoxGeneralTTitle">
	                              <table border="0" cellspacing="0" cellpadding="0">
	                                  <colgroup>
	                                     <col style="width:150px;" />
								           <col style="width:150px;" />
								           <col style="width:380px;"/>
	                                  </colgroup>
	                                  <thead>
	                                      <tr>
	                                          <th class="ColsThFirst">Mã loại KH</th>
	                                          <th class="ColsThSecond">Chỉ tiêu SKU</th>
	                                          <th class="ColsThEnd">Lỗi</th>
	                                      </tr>
	                                  </thead>
	                              </table>
	                          </div>
	                          <div class="BoxGeneralTBody">
	                          	<div id="fancy_gird" class="ScrollBodySection"> 
								</div>
	                          </div>
	                  	</div>
	                  	<div class="BoxDialogBtm">
                    		<div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
               			 </div>
	              </div>
		</div>
	</div>
</div>
<s:select id="codeTmp" headerKey="-2" headerValue="-- Chọn loại khách hàng --"  cssStyle="display:none" list="lstChannelType" listKey="channelTypeCode" listValue="channelTypeName" onchange="skuCodeCustomerChanged();"></s:select>
<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
<s:hidden id="selId" value="0"></s:hidden>
<input type="hidden" id="type" value="0"> 
<script type="text/javascript">
$(document).ready(function(){	
	$('#name').focus();
	$('#name').val('');
	$('.MySelectBoxClass').customStyle();
	$('.RequireStyle').hide();	
	Utils.bindFormatOnTextfield('skuAmount', Utils._TF_NUMBER);
	Utils.formatCurrencyFor('skuAmount');
	$("#grid").jqGrid({
	  url:SkuCatalog.getGridUrl('','',$('#status').val(),''),
	  colModel:[		
	    {name:'channelTypeCode',index:'channelTypeCode', label: 'Mã loại KH', width: 100, sortable:false,resizable:false , align: 'left'},
	    {name:'channelTypeName', index:'channelTypeName',label: 'Tên loại KH', sortable:false,resizable:false, align:'left' },
	    {name:'sku', index:'sku',label: 'Chỉ tiêu SKU', width: 70,sortable:false,resizable:false, align: 'right', formatter: SKUCatalogFormatter.skuAmountFormatter},
	    {name:'statusSku',index:'statusSku', label: 'Trạng thái', width: 60, align: 'left', sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter},
	    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: SKUCatalogFormatter.editCellIconFormatter},
	    {name:'delete', label: 'Xóa', width: 50, align: 'center', sortable:false,resizable:false, formatter: SKUCatalogFormatter.delCellIconFormatter},
	    {name:'id', index:'id',label: 'id', hidden:true },
	  ],	  
	  pager : '#pager',
	  height: 'auto',	  	  
	  width: ($('#gridContainer').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});	
	$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_SKUcholoaiKH_import.xls');
	var options = { 
	 		beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcel,  
	 		type: "POST",
	 		dataType: 'html'   
	 	}; 
	$('#importFrm').ajaxForm(options);
});
function skuCodeCustomerChanged(){
	if($('#code').val().trim()!= '-2'){
		$('#codeTmp').val($('#code').val());
		if($('#skuCustomerDiv #btnSearch').is(':hidden')){
			$('#name').val($('#codeTmp option:selected').text());	
		}		
	} else {
		$('#name').val('');
	}
	return false;
}
</script>
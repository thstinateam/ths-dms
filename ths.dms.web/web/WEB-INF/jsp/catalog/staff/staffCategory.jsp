<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="k" uri="/kryptone" %>
<div class="GeneralMilkInBox ResearchSection">
	<div class="GeneralMilkCol9">
        <label class="LabelStyle Label1Style">Mã đơn vị</label>
        <input type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="shopCode"/>"/>
        <div class="Clear"></div>
        <label class="LabelStyle Label1Style">Tên đơn vị</label>
        <input type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="shopName"/>"/>
        <div class="Clear"></div>
        <label class="LabelStyle Label1Style">Mã NV</label>
        <input type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.staffCode"/>"/>
        <div class="Clear"></div>
        <label class="LabelStyle Label1Style">Tên NV</label>
        <input type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.staffName"/>"/>
        <div class="Clear"></div>
    </div>
    <div class="GeneralMilkCol10">
        <div class="GeneralMilkBox">
            <div class="GeneralMilkTopBox">
                <div class="GeneralMilkBtmBox">
                    <h3 class="Sprite2"><span class="Sprite2">Danh sách ngành hàng</span></h3>
                    <div class="GeneralMilkInBox">
                        <div class="MultiSelectBox">
                        	<div id="scrollId" class="ScrollSection">
                                <ul class="ResetList MultiSelectList">
                                	<k:repeater value="lstCategory" status="status">
	                                	<k:itemTemplate>
	                                		<li>
	                                			<s:if test="checkEqualCategory(id, lstStaffSaleCat)">
													<label><input id="category_<s:property value="#status.index"/>" checked="checked" type="checkbox" /><s:property value="productInfoCode"/>-<s:property value="productInfoName"/></label>
	                                				<s:hidden id="categoryId_%{#status.index}" name="id"></s:hidden>	                                			
	                                			</s:if>
	                                			<s:else>
	                                				<label><input id="category_<s:property value="#status.index"/>" type="checkbox" /><s:property value="productInfoCode"/>-<s:property value="productInfoName"/></label>
	                                				<s:hidden id="categoryId_%{#status.index}" name="id"></s:hidden>
	                                			</s:else>
	                                		</li>
	                                	</k:itemTemplate>
                                	</k:repeater>
                                	<s:hidden id="lstSize" name="lstCategory.size()"></s:hidden>
                                </ul>
                            </div>
                        </div>
                        <div class="ButtonSection">
                        <s:if test="staff.status.value == 1">
                        	<button id="btnCreate" class="BtnGeneralStyle Sprite2" onclick="StaffCatalog.changeStaffCategory();"><span class="Sprite2" id="abc">Lưu</span></button>
                        </s:if>
                        	<button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="StaffCatalog.loadChanged();"><span class="Sprite2">Bỏ qua</span></button>
                        	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                        </div>
                        <p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
                		<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="Clear"></div>
</div>
<s:hidden id="categoryId" value="0"></s:hidden>
<k:repeater value="lstStaffSaleCat" status="status"><k:itemTemplate><s:hidden id="categorySelected_%{#status.index}" name="cat.id"></s:hidden></k:itemTemplate></k:repeater>
<s:hidden id="lstSaleCatSize" name="lstStaffSaleCat.size()"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#scrollId').jScrollPane();
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="#">Danh mục</a></li>
		<li><a href="/catalog/manage-staff"><span>Quản lý nhân viên</span></a></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">		
				<h2 class="Title2Style">Thông tin nhân viên</h2>		
				<div class="SearchInSection SProduct1Form">				
					<label class="LabelStyle Label1Style">Mã nhân viên <span class="ReqiureStyle">*</span></label> 
					<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="staffCode" value="<s:property value="staff.staffCode"/>" maxlength="10" /> 
					
					<label class="LabelStyle Label4Style">Tên nhân viên</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="staffName" value="<s:property value="staff.staffName"/>" maxlength="100"/>
					
					
					<label	class="LabelStyle Label14Style">Giới tính</label> 
					<div class="BoxSelect BoxSelect6">
						<select id="gender" class="MySelectBoxClass" style="width: 207px;" name="LevelSchool">							
						    <option value="-1">Chọn giới tính</option>
						    <option value="0">Nam</option>
						    <option value="1">Nữ</option>
						</select>
					</div>					
					<div class="Clear"></div>
					
					<label class="LabelStyle Label5Style">Ngày bắt đầu làm việc</label> 
					<input	type="text" class="InputTextStyle InputText1Style"  id="workStartDate" value="<s:date name="staff.startWorkingDay" format="dd/MM/yyyy"/>" maxlength="10" />
					 
					<label class="LabelStyle Label4Style">Trình độ</label> 
					<input type="text"	class="InputTextStyle InputText1Style" id="education" value="<s:property value="staff.education"/>" maxlength="250"/>
					 
					<label	class="LabelStyle Label14Style">Vị trí chức danh</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="position"  value="<s:property value="staff.position"/>"/>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label5Style">CMND</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="idCard" value="<s:property value="staff.idno"/>" maxlength="40" onkeypress="return numbersonly(event,false);"/> 
					
					<label class="LabelStyle Label4Style">Ngày cấp</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="idDate" value="<s:date name="staff.idnoDate" format="dd/MM/yyyy"/>" maxlength="10"/> 
					
					<label class="LabelStyle Label14Style">Nơi cấp</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="idPlace" value="<s:property value="staff.idnoPlace"/>" maxlength="250"/>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label5Style">Số nhà</label> 
					<input type="text" class="InputTextStyle InputText1Style" /> 
					
					<label class="LabelStyle Label4Style">Đường</label>
					<input type="text" class="InputTextStyle InputText1Style" id="street" value="<s:property value="staff.street"/>"/> 
					
					<label class="LabelStyle Label14Style">Địa chỉ</label>
					<input type="text" class="InputTextStyle InputText1Style" id="address"  value="<s:property value="staff.address"/>"/>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label5Style">TP/Tỉnh</label> 
					<input	type="text" class="InputTextStyle InputText1Style"	id="provinceCode" value="<s:property value="staff.area.province"/>"/> 
					
					<label class="LabelStyle Label4Style">Quận/Huyện</label>
					<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="districtCode" value="<s:property value="staff.area.district"/>"/>
					 
					<label class="LabelStyle Label14Style">Phường/Xã</label> 
					<input type="text" class="InputTextStyle InputText1Style" disabled="disabled" id="wardCode" value="<s:property value="staff.area.precinct"/>"/>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label5Style">Số di động</label>
					<input type="text" class="InputTextStyle InputText1Style" id="staffPhone" value="<s:property value="staff.mobilephone"/>" maxlength="20" onkeypress="return numbersonly(event,false);"/> 
					
					<label class="LabelStyle Label4Style">Số cố định</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="staffTelephone" value="<s:property value="staff.phone"/>" maxlength="20" onkeypress="return numbersonly(event,false);"/> 
					
					<label class="LabelStyle Label14Style">Email</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="email" value="<s:property value="staff.email"/>" maxlength="50">
					<div class="Clear"></div>
					
					<label class="LabelStyle Label5Style">Loại nhân viên</label> 
					<div class="BoxSelect BoxSelect6">
						<select id="staffType" class="MySelectBoxClass" style="width: 207px;" name="LevelSchool">							
						   <option value="-1">Chọn loại nhân viên</option>
							<s:iterator value="lstStaffType">
								<s:if test="id == staffType">
									<option selected="selected" value='<s:property value="id"/>'><s:property value="channelTypeCode"/> - <s:property value="channelTypeName"/></option>
								</s:if>
								<s:else>
									<option value='<s:property value="id"/>'>
									<s:property value="channelTypeCode"/> - 
									<s:property value="channelTypeName"/></option>
								</s:else>
							</s:iterator>
						</select>
					</div>	
					
					<label class="LabelStyle Label4Style">Hình thức bán hàng</label> 
					<div class="BoxSelect BoxSelect6">
						<s:select id="saleType" cssStyle="width: 207px;" list="lstSaleType" listKey="id" listValue="apParamCode" 
							headerKey="-1" headerValue="Chọn hình thức BH" name="saleType" cssClass="MySelectBoxClass" value="%{saleTypeCode}">
						</s:select>
					</div> 
					
					<label class="LabelStyle Label14Style">Nhóm bán hàng</label> 
					<div class="BoxSelect BoxSelect6">
						<s:select id="saleGroup" cssStyle="width: 207px;" list="lstsaleGroup" listKey="id" listValue="apParamCode" headerKey="-1" headerValue="Chọn nhóm bán hàng"
							name="saleGroup" cssClass="MySelectBoxClass" value="%{saleGroup}">
						</s:select>
					</div>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label5Style">Trạng thái</label> 
					<div class="BoxSelect BoxSelect6">
						<select id="status" class="MySelectBoxClass" name="LevelSchool" style="width: 207px;">
							<option value="-2">Chọn trạng thái</option>
					    	<option value="0">Tạm ngưng</option>
					    	<option value="1" selected="selected">Hoạt động</option>
						</select>
					</div>
					<div class="Clear"></div>
				</div>
				<div class="BtnCenterSection" style="padding-bottom: 10px;">
                      <button class="BtnGeneralStyle">Cập nhật</button> &nbsp;&nbsp;
                      <button class="BtnGeneralStyle">Bỏ qua</button>
                </div>				
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
	<p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
	<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
</div>
<s:hidden id="statusHidden" name="status"></s:hidden>
<s:hidden id="shopCodeOld" name="staff.shop.shopCode"></s:hidden>





<!-- 
<div class="ModuleList71Form">
<label class="LabelStyle Label1Style">Mã đơn vị(F9)</label>
<div class="Field2">
	<input id="shopCode" type="text" class="InputTextStyle InputText1Style" value="<s:property value="shopCode"/>" maxlength="22"/>
	<span class="RequireStyle">(*)</span>
</div>
<label class="LabelStyle Label2Style">Tên đơn vị</label>
<input id="shopName" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="shopName"/>"/>
<label class="LabelStyle Label3Style">Mã NV</label>
<div class="Field2">
	<input id="staffCode" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.staffCode"/>" maxlength="10" disabled="disabled"/>
	<span class="RequireStyle">(*)</span>
</div>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Tên NV</label>
<div class="Field2">
	<input id="staffName" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.staffName"/>" maxlength="100"/>
	<span class="RequireStyle">(*)</span>
</div>
<label class="LabelStyle Label2Style">Mã NVGS(F9)</label>
<input id="staffOwnerCode" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.staffOwner.staffCode"/>" maxlength="50"/>
<label class="LabelStyle Label3Style">Tên NVGS</label>
<input id="staffOwnerName" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.staffOwner.staffName"/>" maxlength="100" disabled="disabled"/>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Giới tính</label>
<div class="BoxSelect BoxSelect1">
<div class="Field2">
	<select id="gender" class="MySelectBoxClass">
	    <option value="-1">---Chọn giới tính---</option>
	    <option value="0">Nam</option>
	    <option value="1">Nữ</option>
	</select>
	<span class="RequireStyle">(*)</span>
</div>
</div>
<label class="LabelStyle Label2Style">Ngày bắt đầu làm việc</label>
<input id="workStartDate" type="text" class="InputTextStyle InputText2Style" value="<s:date name="staff.startWorkingDay" format="dd/MM/yyyy"/>" maxlength="10"/>
<label class="LabelStyle Label3Style">Trình độ</label>
<input id="education" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.education"/>" maxlength="250"/>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">CMND</label>
<input id="idCard" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.idno"/>" maxlength="40" onkeypress="return numbersonly(event,false);"/>
<label class="LabelStyle Label2Style">Ngày cấp</label>
<input id="idDate" type="text" class="InputTextStyle InputText2Style" value="<s:date name="staff.idnoDate" format="dd/MM/yyyy"/>" maxlength="10"/>
<label class="LabelStyle Label3Style">Nơi cấp</label>
<input id="idPlace" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.idnoPlace"/>" maxlength="250"/>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Tỉnh/TP(F9)</label>
<div class="Field2">
<input id="provinceCode" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.area.province"/>"/>
<span class="RequireStyle">(*)</span>
</div>
<label class="LabelStyle Label2Style">Quận/Huyện(F9)</label>
<div class="Field2">
<input id="districtCode" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.area.district"/>" disabled="disabled"/>
<span class="RequireStyle">(*)</span>
</div>
<label class="LabelStyle Label3Style">Phường/Xã(F9)</label>
<div class="Field2">
<input id="wardCode" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.area.precinct"/>" disabled="disabled"/>
<span class="RequireStyle">(*)</span>
</div>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Vị trí</label>
<input id="position" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.position"/>"/>
<label class="LabelStyle Label2Style">Đường</label>
<input id="street" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.street"/>"/>
<label class="LabelStyle Label3Style">Địa chỉ</label>
<input id="address" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.address"/>"/>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Số di động</label>
<input id="staffPhone" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.mobilephone"/>" maxlength="20" onkeypress="return numbersonly(event,false);"/>
<label class="LabelStyle Label2Style">Số cố định</label>
<input id="staffTelephone" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.phone"/>" maxlength="20" onkeypress="return numbersonly(event,false);"/>
<label class="LabelStyle Label3Style">Email</label>
<input id="email" type="text" class="InputTextStyle InputText1Style" value="<s:property value="staff.email"/>" maxlength="50"/>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Loại NV</label>
<div class="BoxSelect BoxSelect1">
	<div class="Field2">
	   <%--  <s:select id="staffType" list="lstStaffType" listKey="id" listValue="channelTypeName" headerKey="-1" headerValue="---Chọn loại nhân viên---"
			name="staffType" cssClass="MySelectBoxClass" value="%{staffType}">
		</s:select> --%>
		
		<select id="staffType" class="MySelectBoxClass" onchange="isSaleStaffValue($(this).val(),'#staffOwnerCode');">
				<option value="-1">---Chọn loại nhân viên---</option>
				<s:iterator value="lstStaffType">
					<s:if test="id == staffType">
						<option selected="selected" value='<s:property value="id"/>'><s:property value="channelTypeCode"/> - <s:property value="channelTypeName"/></option>
					</s:if>
					<s:else>
						<option value='<s:property value="id"/>'>
						<s:property value="channelTypeCode"/> - 
						<s:property value="channelTypeName"/></option>
					</s:else>
				</s:iterator>		    
		    </select>		
		
		<span class="RequireStyle">(*)</span>
	</div>
</div>
<label class="LabelStyle Label2Style">Hình thức BH</label>
<div class="BoxSelect BoxSelect1">
	<div class="Field2">
	    <s:select id="saleType" list="lstSaleType" listKey="id" listValue="apParamCode" headerKey="-1" headerValue="---Chọn hình thức BH---"
			name="saleType" cssClass="MySelectBoxClass" value="%{saleTypeCode}">
		</s:select>
		<span class="RequireStyle">(*)</span>
	</div>
</div>
<label class="LabelStyle Label3Style">Trạng thái</label>
<div class="BoxSelect BoxSelect1">
<div class="Field2">
    <select id="status" class="MySelectBoxClass">
        <option value="-2">---Chọn trạng thái---</option>
        <option value="0">Tạm ngưng</option>
        <option value="1" selected="selected">Hoạt động</option>
    </select>
    <span class="RequireStyle">(*)</span>
</div>
</div>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Nhóm BH</label>
<div class="BoxSelect BoxSelect1">
	<div class="Field2">
	    <s:select id="saleGroup" list="lstsaleGroup" listKey="id" listValue="apParamCode" headerKey="-1" headerValue="---Chọn nhóm BH---"
			name="saleGroup" cssClass="MySelectBoxClass" value="%{saleGroup}">
		</s:select>
	</div>
</div>
<div class="Clear"></div>

<tiles:insertTemplate template="/WEB-INF/jsp/catalog/attributes/attributesCommon.jsp"></tiles:insertTemplate>

<div class="ButtonSection">
	<button id="btnSave" class="BtnGeneralStyle Sprite2" onclick="return StaffCatalog.changeStaffInfo();"><span class="Sprite2">Lưu thay đổi</span></button>
	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
</div>

<p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
</div>
<s:hidden id="statusHidden" name="status"></s:hidden>
<s:hidden id="shopCodeOld" name="staff.shop.shopCode"></s:hidden>
 -->
<script type="text/javascript">
$(document).ready(function(){	
	setTimeout(function(){
		$('.MySelectBoxClass').customStyle();		
		$('.CustomStyleSelectBox').css('width','174px');
	}, 200);
	var status ='<s:property value="staff.status.value"/>';
	var gender ='<s:property value="staff.gender.value"/>';
	var isSaleStaff ='<s:property value="isSaleStaff"/>';
	var staffT = '<s:property value="staff"/>';
	if (staffT != ""){
		Utils.ChangeSelectByValueOrText('saleGroup','<s:property value="staff.saleGroup"/>',"text");
		Utils.ChangeSelectByValueOrText('saleType','<s:property value="staff.saleTypeCode"/>',"text");
	}
	Utils.bindFormatOnTextfield('staffPhone', Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield('staffTelephone', Utils._TF_NUMBER);
	$('#provinceCode').bind('blur', function(event){
		var provinceCode = $('#provinceCode').val();
		if(provinceCode == "" || provinceCode == null || provinceCode == undefined){
			$('#districtCode').val("");
			$('#wardCode').val("");
		}
	});
	if(status != null){
		$('#status').val(status);
		$('#status').change();
	}
	if(gender != null){
		$('#gender').val(gender);
		$('#gender').change();
	}
	$('#districtCode').bind('keypress', function(event){
		if(event.keyCode == keyCode_TAB){
			StaffCatalog.checkDistrictCode($('#districtCode').val().trim(),'#wardCode');
		}
	});
});
function isSaleStaffValue(value,obj){
	StaffCatalog.getStaffType(value,obj);
}
if (<s:property value="staffId"/> == 0) {
	$('#status').val(1);
	$('#status').change();
}
</script>
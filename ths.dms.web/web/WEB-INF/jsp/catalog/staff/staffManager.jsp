<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0)">Danh mục</a></li>
		<li><span>Thông tin nhân viên</span></li>
	</ul>
</div>	
<div class="CtnOneColSection">
       <div class="ContentSection">
       		<div class="ToolBarSection">
              <div class="SearchSection GeneralSSection">
<%--               	<div class="TabSection">
	                <ul class="ResetList TabSectionList">	                	
	                    <li><a href="javascript:void(0)" onclick="StaffCatalog.activeTab('tab1');" id="tabItem1" class="Sprite1 Active"><span class="Sprite1">Danh mục nhân viên</span></a></li>	                    
	                </ul>
                	<div class="Clear"></div>             	
                </div> --%>
				<div id="tab1" style="display: none;">
               		<h2 class="Title2Style">Thông tin tìm kiếm</h2>
					<div class="SearchInSection SProduct1Form">
						<label class="LabelStyle Label1Style">Mã nhân viên</label> 
						<input	type="text" class="InputTextStyle InputText1Style" id="staffCode"/> 
						
						<label	class="LabelStyle Label1Style">Tên nhân viên</label> 
						<input type="text" class="InputTextStyle InputText1Style" id="staffName"/>
						 
						<label	class="LabelStyle Label1Style">Trạng thái</label>
						<div class="BoxSelect BoxSelect6">
							<select class="MySelectBoxClass InputTextStyle" name="LevelSchool" id="status">
								<option value="-2">Chọn trạng thái</option>
							    <option value="0">Tạm ngưng</option>
							    <option value="1" selected="selected">Hoạt động</option>
							</select>
						</div>
						<div class="Clear"></div>
						<div class="SearchInSection SProduct1Form">					
						<div class="BtnCenterSection">
							<button class="BtnGeneralStyle" id="btnSearch" onclick="return StaffCatalog.search();">Tìm kiếm</button>
						</div>
						</div>
						<div class="Clear"></div>
						<p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
						<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
					</div>
					<h2 class="Title2Style">Danh sách nhân viên</h2>
					<div class="SearchInSection SProduct1Form">
						<div class="GridSection" id="gridContainer">
							<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
							<table id="grid" class="easyui-datagrid"></table>
							<div id="pager"></div>
						</div>
					</div>
               	</div> 
			</div>
		</div>
	</div>
		<div class="Clear"></div>
</div>
<s:hidden id="carTypeId" value="0"></s:hidden>
<%-- <s:hidden id="shopCodeHidden" name="staff.shop.shopCode"></s:hidden>
<s:hidden id="shopIdHidden" name="staff.shop.id"></s:hidden> --%>
<s:hidden id="shopCodeHidden" name="shop.shopCode"></s:hidden>
<s:hidden id="shopIdHidden" name="shop.id"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabItem1').addClass('Active');
	$('#tab1').show();
	
	$('#staffCode').focus();
	$('#layoutContent').removeClass('Content');
	$('#layoutContent').addClass('MainContent');
	//$('.MySelectBoxClass').customStyle();
	$('#staffCode').focus();	
	var params = new Object();
	params.shopCodeHidden = $('#shopCodeHidden').val().trim();
	params.staffCode = $('#staffCode').val().trim();
	params.staffName = encodeChar($('#staffName').val().trim());
	params.status = $('#status').val();
	$('#grid').datagrid({
		url : "/catalog_staff_manager/search",
		autoRowHeight : true,
		rownumbers : true, 
		checkOnSelect :true,
		pagination:true,
		rowNum : 10,
		singleSelect  :true,
		fitColumns:true,
		pageList  : [10,20,30],
		scrollbarSize:0,
		width: $(window).width() - 50,
		autoWidth: true,
		queryParams:params,
	    columns:[[	        
		    {field:'staffCode', title: 'Mã nhân viên', width: 100, sortable:false,resizable:true , align: 'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
		    {field:'staffName', title: 'Tên nhân viên', width:200,sortable:false,resizable:true , align: 'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			 }},
		    {field:'mobilephone', title: 'Mobilephone', width: 100, align: 'right', sortable:false, resizable:true, formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
		    {field:'staffType.channelTypeName',title: 'Loại NV', width: 80, align: 'left',sortable:false,resizable:true, formatter:function(value,row,index) {
		    		return Utils.XSSEncode(row.staffType.channelTypeCode);	
		    }},
		    {field:'status', title: 'Trạng thái', width: 80, align: 'left',sortable:false,resizable:true,formatter:StaffCatalogFormatter.statusFormat},
		    {field:'edit', 
		    	title:'<a href="/catalog_staff_manager/viewdetail?staffId=0"><img src="/resources/images/icon_add.png"/></a>',
		    	width: 20, align: 'center',sortable:false,resizable:true, formatter: StaffCatalogFormatter.editCellIconFormatter},
		    {field: 'id', index: 'id', hidden: true},
	    ]],	    
	    onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');	    	
	    	 updateRownumWidthForJqGrid('.easyui-dialog');
   		 $(window).resize();    		 
	    }
	});	
	Utils.bindAutoSearch();
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="k" uri="/kryptone" %>
<div class="GeneralMilkBox GeneralMilkExtInBox NoneMarginBox">
    <div class="GeneralMilkTopBox">
        <div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Tìm kiếm NVBH</span></h3>
            <div class="GeneralMilkInBox">
            <div class="ModuleList169Form" style="text-align: center">
                <label class="LabelStyle Label1Style">Mã đơn vị (F9)</label>
	            <input id="shopCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
                <label class="LabelStyle Label2Style">Tên đơn vị</label>
                <input id="shopName" type="text" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style" style="padding-left:85px;padding-right:10px;">Mã NV (F9)</label>
	            <input id="staffCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
                <label class="LabelStyle Label2Style" style="padding-left:130px;padding-right:10px;">Tên NV</label>
                <input id="staffName" type="text" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
                <div class="Clear"></div>
            </div>
            	<div class="ButtonSection">
                	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return StaffCatalog.searchSupervisor();"><span class="Sprite2">Tìm kiếm</span></button>
	                <img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                </div>
                <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>
                <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
            </div>
        </div>
    </div>
</div>
<div class="GeneralMilkBox GeneralMilkExtInBox">
    <div class="GeneralMilkTopBox">
        <div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Danh sách NVBH giám sát</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="supervisorGrid"  style="width:900px">
                	<div id="btnCreate" class="ButtonRSection ButtonR3Section">
                	<s:if test="staff.status.value == 1">
                		<button class="BtnGeneralStyle Sprite2" onclick="StaffCatalog.showDialogStaff();"><span class="Sprite2">Thêm</span></button>
                	</s:if>
                	</div>
                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle"></p>
                  	<table id="grid"></table>
					<div id="pager"></div>
              	</div>
              	<div class="Clear"></div>
            </div>
        </div>
    </div>
</div>
<div id="staffDialog" style="display: none;">
	<div class="GeneralDialog General2Dialog">
     	<div class="DialogProductSearch">
         	<div class="GeneralMilkBox">
                 <div class="GeneralMilkTopBox">
                     <div class="GeneralMilkBtmBox">
                         <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                        <div class="GeneralMilkInBox ResearchSection">
                        	<div class="Warehouse84Form">
                        		<label class="LabelStyle Label1Style">Mã đơn vị(F9)</label>
                                <input id="shopCodeDlg" type="text" class="InputTextStyle InputText1Style" />
                                <label class="LabelStyle Label2Style">Tên đơn vị</label>
                                <input id="shopNameDlg" type="text" class="InputTextStyle InputText1Style" />
                                <div class="Clear"></div>                                 
                                <label class="LabelStyle Label1Style">Mã NVBH(F9)</label>
                                <input id="staffCodeDlg" type="text" class="InputTextStyle InputText1Style" />
                                <label class="LabelStyle Label2Style">Tên NVBH</label>
                                <input id="staffNameDlg" type="text" class="InputTextStyle InputText1Style" />
                                <div class="Clear"></div>
                                <div class="ButtonSection">
                                    <button id="btnDlgStaff" class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" onclick=""><span class="Sprite2">Tìm kiếm</span></button>
                                </div>                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="GeneralMilkBox">
                <div class="GeneralMilkTopBox">
                    <div class="GeneralMilkBtmBox">
                        <h3 class="Sprite2"><span class="Sprite2">Thông tin NVBH</span></h3>
                        <div class="GeneralMilkInBox">
	                	<div class="ResultSection" id="staffInfoGrid">
		                  	<p id="gridStaffNoResult" style="display: none" class="WarningResultStyle"></p>
		                  	<table id="gridStaff"></table>
							<div id="pagerStaff"></div>
		              	</div>
		              	<div class="Clear"></div>
	            	</div>
                    </div>
                </div>
            </div>
            <div class="BoxDialogBtm">             	
               <div class="ButtonSection">
               	<button class="BtnGeneralStyle Sprite2 BtnSearch" onclick="return StaffCatalog.selectListStaff();"><span class="Sprite2">Chọn</span></button>
				<button onclick="$.fancybox.close();" class="BtnGeneralStyle Sprite2"><span class="Sprite2">Đóng</span></button>
               </div>
            </div>
            <p id="errMsgCustomerDlg" class="ErrorMsgStyle Sprite1" style="display: none"></p>
            <p id="successMsgCustomerDlg" class="SuccessMsgStyle" style="display: none"></p>
            <input id="staffSuper" type="hidden" value="<s:property value="staff.status.value"/>"/>
    	</div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	$('.RequireStyle').hide();
	$("#grid").jqGrid({
	  url:StaffCatalog.getSupervisorGridUrl($('#staffId').val(),'','','',''),
	  colModel:[		
	    {name:'staffCode', label: 'Mã NVBH', width: 100, sortable:false,resizable:false , align: 'left'},
	    {name:'staffName', label: 'Tên NVBH', width: 150, sortable:false,resizable:false , align: 'left'},
	    {name:'shop.shopCode', label: 'Mã đơn vị', width: 100, sortable:false,resizable:false , align: 'left'},
	    {name:'shop.shopName', label: 'Tên đơn vị', width: 150, sortable:false,resizable:false , align: 'left'},
	    {name:'delete', label: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false, formatter: StaffCatalogFormatter.delSupervisorFormatter},
	  ],	  
	  pager : '#pager',
	  height: 'auto',
	  rownumbers: true,	  
	  width: ($('#supervisorGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});
	$('#jqgh_grid_rn').prepend('STT');
	$('#shopCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			CommonSearch.searchShopOnDialog(function(data){
				$('#shopCode').val(data.code);
				$('#shopName').val(data.name);
			});
		}
	});
	$('#staffCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var arrParam = new Array();
			var param = new Object();
			param.name = 'shopCode';
			param.value = $('#shopCode').val();	
			arrParam.push(param);
			var filter = new Object();
			filter.name = 'filterStaff';
			filter.value = 1;
			arrParam.push(filter);
			CommonSearch.searchSaleStaffOnDialog(function(data){
				$('#staffCode').val(data.code);
				$('#staffName').val(data.name);
			}, arrParam);
		}
	});
	$('.ui-paging-info').hide();
});
</script>
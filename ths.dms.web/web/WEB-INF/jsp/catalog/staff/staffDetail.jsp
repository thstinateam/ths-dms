<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/catalog/manage-staff">Quản lý nhân viên</a></li>
		<li><span id="title">Thông tin nhân viên</span></li>
	</ul>
</div>	
<div class="CtnOneColSection">
       <div class="ContentSection">
       		<div class="ToolBarSection">
              <div class="SearchSection GeneralSSection">
              	<div class="Clear"></div>	
              	<!-- <div class="TabSection"> -->
	                <%-- <ul class="ResetList TabSectionList">	                	
	                    <li><a href="javascript:void(0)"  id="tabItem1" class="Sprite1 Active"><span class="Sprite1">Thông tin nhân viên</span></a></li>
	                    <li><a href="javascript:void(0)"  id="tabItem2" class="Sprite1"><span class="Sprite1">Thông tin ngành hàng</span></a></li>
	                </ul>
                	<div class="Clear"></div>  --%>
                	<div id="tab1" style="display: none;">
                		<h2 class="Title2Style">Thông tin nhân viên</h2>
                                <div class="SearchInSection SProduct1Form" id="frmStaff">
                                    <label class="LabelStyle Label1Style">Mã nhân viên <span class="ReqiureStyle">*</span></label>
                                    <input type="text" class="InputTextStyle InputText1Style" id="staffCode" value="<s:property value="staff.staffCode"/>" maxlength="10" />
                                    <label class="LabelStyle Label1Style">Tên nhân viên <span class="ReqiureStyle">*</span></label>
                                    <input type="text" class="InputTextStyle InputText2Style" id="staffName" value="<s:property value="staff.staffName"/>" maxlength="100"/>
                                    <div class="Clear"></div>
                                    <s:if test="staff != null && staff.gender.value == 0">
                                    	<label class="LabelStyle Label1Style">Giới tính <span class="ReqiureStyle">*</span></label>
	                                    <div class="BoxSelect BoxSelect2">
	                                        <select id="gender" class="MySelectBoxClass" style="width: 207px;" name="LevelSchool">							
											    <option value="0">Nữ</option>
											    <option value="1">Nam</option>
											</select>
	                                    </div>
                                    </s:if>
                                    <s:elseif test="staff && staff.gender.value == 1">
                                    	<label class="LabelStyle Label1Style">Giới tính <span class="ReqiureStyle">*</span></label>
	                                    <div class="BoxSelect BoxSelect2">
	                                        <select id="gender" class="MySelectBoxClass" style="width: 207px;" name="LevelSchool">							
											    <option value="1">Nam</option>
											    <option value="0">Nữ</option>
											</select>
	                                    </div>
                                    </s:elseif>
                                    <s:else>
                                    	<label class="LabelStyle Label1Style">Giới tính <span class="ReqiureStyle">*</span></label>
	                                    <div class="BoxSelect BoxSelect2">
	                                        <select id="gender" class="MySelectBoxClass" style="width: 207px;" name="LevelSchool">							
											    <option value="-1">Chọn giới tính</option>
											    <option value="1">Nam</option>
											    <option value="0">Nữ</option>
											</select>
	                                    </div>
                                    </s:else>
                                    <label class="LabelStyle Label1Style">Ngày bắt đầu làm việc</label>
                                    <input type="text" class="InputTextStyle InputText6Style" id="workStartDate" value="<s:date name="staff.startWorkingDay" format="dd/MM/yyyy"/>" maxlength="10" />                                   
                                    <label class="LabelStyle Label14Style">Trình độ</label>
                                    <input type="text" class="InputTextStyle InputText1Style" id="education" value="<s:property value="staff.education"/>" maxlength="50"/>
                                    <div class="Clear"></div>
                                    
                                    <label class="LabelStyle Label1Style">CMND</label>
                                    <input type="text" class="InputTextStyle InputText1Style" id="idCard" value="<s:property value="staff.idno"/>" maxlength="40" onkeypress="return numbersonly(event,false);"/>
                                    <label class="LabelStyle Label1Style">Ngày cấp</label>
                                    <input type="text" class="InputTextStyle InputText6Style" id="idDate" value="<s:date name="staff.idnoDate" format="dd/MM/yyyy"/>" maxlength="10"/> 
                                    <label class="LabelStyle Label14Style">Nơi cấp</label>
                                    <input type="text" class="InputTextStyle InputText1Style" id="idPlace" value="<s:property value="staff.idnoPlace"/>" maxlength="250"/>
                                    <div class="Clear"></div>
                                    
                                    <label class="LabelStyle Label1Style">Vị trí chức danh</label>
                                    <input type="text" class="InputTextStyle InputText1Style" id="position"  value="<s:property value="staff.position"/>" maxlength="50"/>
                                    <label class="LabelStyle Label1Style">Đường</label>
                                    <input type="text" class="InputTextStyle InputText1Style" id="street" value="<s:property value="staff.street"/>" maxlength="50"/> 
                                    <label class="LabelStyle Label14Style">Địa chỉ</label>
                                    <input type="text" class="InputTextStyle InputText1Style" id="address"  value="<s:property value="staff.address"/>" maxlength="100"/>
                                    <div class="Clear"></div>
                                    
                                    <label class="LabelStyle Label1Style">Số di động</label>
                                    <input type="text" class="InputTextStyle InputText1Style" id="staffPhone" value="<s:property value="staff.mobilephone"/>" maxlength="20" onkeypress="return numbersonly(event,false);"/> 
                                    <label class="LabelStyle Label1Style">Số cố định</label>
                                    <input type="text" class="InputTextStyle InputText1Style" id="staffTelephone" value="<s:property value="staff.phone"/>" maxlength="20" onkeypress="return numbersonly(event,false);"/> 
                                    <label class="LabelStyle Label14Style">Email</label>
                                    <input type="text" class="InputTextStyle InputText1Style" id="email" value="<s:property value="staff.email"/>" maxlength="50">
                                    <div class="Clear"></div>
                                    
                                    <label class="LabelStyle Label1Style">Chức vụ<span class="ReqiureStyle">*</span></label>
                                    <div class="BoxSelect BoxSelect2">
                                        <select id="staffType" class="MySelectBoxClass" style="width: 207px;" name="LevelSchool">							
										   <option value="-1">Chọn chức vụ</option>
											<s:iterator value="lstStaffType">
												<s:if test="id == staffType">
													<option selected="selected" value='<s:property value="id"/>'><s:property value="channelTypeCode"/> - <s:property value="channelTypeName"/></option>
												</s:if>
												<s:else>
													<option value='<s:property value="id"/>'>
													<s:property value="channelTypeCode"/> - 
													<s:property value="channelTypeName"/></option>
												</s:else>
											</s:iterator>
										</select>
                                    </div>
                                    <label class="LabelStyle Label1Style">Loại hình nhân viên</label>
                                    <div class="BoxSelect BoxSelect2">
                                        <select id="saleType" style="width: 207px;" name="saleType" class="MySelectBoxClass">
                                        	<option value="-1">Chọn loại hình nhân viên</option>
											<s:iterator value="lstSaleType">
												<s:if test="apParamCode == saleTypeCode">
													<option selected="selected" value='<s:property value="id"/>'><s:property value="apParamCode"/></option>
												</s:if>
												<s:else>
													<option value='<s:property value="id"/>'>
													<s:property value="apParamCode"/></option>
												</s:else>
											</s:iterator>
										</select>
                                    </div>
                                    <%-- <s:if test="id == null">
									<label class="LabelStyle Label14Style">Địa bàn</label>
									<div class="BoxSelect BoxSelect2">
							            <input type="text" id="areaTree" class="InputTextStyle InputText1Style" />
										<input type="hidden" id="areaId"/>
									</div>
									</s:if>--%>
                                    <div class="BoxSelect BoxSelect2" id = "saleGroup">
                                        <s:select id="saleGroup" cssStyle="width: 207px;" list="lstsaleGroup" listKey="id" listValue="apParamCode" headerKey="-1" headerValue="Chọn nhóm bán hàng"
											name="saleGroup" cssClass="MySelectBoxClass" value="%{saleGroup}">
										</s:select>
                                    </div>
                                    <div class="Clear"></div>
                                    
                                    <label class="LabelStyle Label1Style">Tỉnh/Thành phố <span class="ReqiureStyle">*</span></label> 
									<div class="BoxSelect BoxSelect2">
										<select id="province">
											<option value="-2" areaId="" centerCode=""></option>
											<s:iterator value="lstProvince" var="prov">
												<s:if test="#prov.areaCode.equals(provinceCode)">
													<option selected="selected" value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
												</s:if>
												<s:else>
													<option value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
												</s:else>
											</s:iterator>
										</select>
									</div>
									
									<label class="LabelStyle Label1Style">Quận/Huyện <span class="ReqiureStyle">*</span></label>
									<div class="BoxSelect BoxSelect2">
									<s:if test="lstDistrict == null">
										<select id="district">
										</select>
									</s:if>
									<s:else>
										<select id="district">
											<s:iterator value="lstDistrict" var="dist">
												<s:if test="#dist.areaCode.equals(districtCode)">
													<option selected="selected" value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
												</s:if>
												<s:else>
													<option value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
												</s:else>
											</s:iterator>
										</select>
									</s:else>
									</div>
									
									<label class="LabelStyle Label2Style">Phường/Xã <span class="ReqiureStyle">*</span></label>
									<div class="BoxSelect BoxSelect2">
									<s:if test="lstPrecinct == null">
										<select id="precinct">
										</select>
									</s:if>
									<s:else>
										<select id="precinct">
											<s:iterator value="lstPrecinct" var="ward">
												<s:if test="#ward.areaCode.equals(wardCode)">
													<option selected="selected" value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
												</s:if>
												<s:else>
													<option value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
												</s:else>
											</s:iterator>
										</select>
									</s:else>
									</div>
									<div class="Clear"></div>
                                    
                                    <label class="LabelStyle Label1Style">Trạng thái</label>
                                    <div class="BoxSelect BoxSelect2">
                                        <select id="status" class="MySelectBoxClass" name="LevelSchool" style="width: 207px;">											
									    	<option value="0">Tạm ngưng</option>
									    	<option value="1" selected="selected">Hoạt động</option>
										</select>
                                    </div>
                                    
                                    <label class="LabelStyle Label1Style">Trạng thái làm việc</label>
                                    <div class="BoxSelect BoxSelect2">
                                        <select id="workState" style="width: 207px;" class="MySelectBoxClass">
                                        	<option value="-1">Chọn trạng thái làm việc</option>
											<s:iterator value="lstWorkState">
												<s:if test="apParamCode == workStateCode">
													<option selected="selected" value='<s:property value="id"/>'><s:property value="apParamName"/></option>
												</s:if>
												<s:else>
													<option value='<s:property value="id"/>'>
													<s:property value="apParamName"/></option>
												</s:else>
											</s:iterator>
										</select>
                                    </div>
		                			<div class="Clear"></div>
                                    <p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
									<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                                </div>
                              	<div id="imfoCat">
									<h2 class="Title2Style">Thông tin ngành hàng</h2>
									<div class="PopupContentMid PopupContentHMid">																		
										<s:iterator value="lstProductInfo">
										<input type="checkbox" id="check<s:property value="productInfoCode" />" class="InputCbxStyle" value="<s:property value="id"/>" />
										<label class="LabelStyle Label4Style"><s:property value="productInfoCode" /> - <s:property value="productInfoName" /></label>
										<div class="Clear"></div>
										</s:iterator>
									</div>
									</div>
									<div class="Clear"></div> 
									<div class="SearchInSection SProduct1Form">                                
	                                    <div class="BtnCenterSection" style="padding-bottom: 10px;">
						                      <button id="btnCapNhat" class="BtnGeneralStyle" onclick="return StaffCatalog.changeStaffInfor();">Cập nhật</button> &nbsp;&nbsp;
						                      <button class="BtnGeneralStyle" onclick="window.location.href='/catalog/manage-staff'">Bỏ qua</button>
			                			</div>
		                			</div>									  
                            </div>
                    <!-- </div> -->
                	<div id="tab2" class="SearchInSection SProduct3Form" style="display: none;">
						 <div class="SProductForm4Cols">
							<h2 class="Title2Style">Thông tin nhân viên</h2>
							<div class="PopupContentMid">
								<label class="LabelStyle Label1Style">Mã NV</label> 
								<input type="text" id="infStaffCode" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.staffCode"/>"/>
								<input type="hidden" id="infStaffCodeHid" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.id"/>"/>
								<div class="Clear"></div>
								<label class="LabelStyle Label1Style">Tên NV</label> 
								<input type="text" id="infStaffName" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.staffName"/>"/>
								<div class="Clear"></div>
							</div>
							
						</div>						
					</div>
                	
			</div>
		</div>
	</div>
	<div class="Clear"></div>	
</div>
<p id="successMsg" class="ErrorMsgStyle" style="display: none"></p>

<s:hidden id="areaId" name="areaId"></s:hidden>
<s:hidden id="staffId" name="staffId"></s:hidden>
<s:hidden id="statusHidden" name="status"></s:hidden>
<s:hidden id="staffTypeHidden" name="staffObjectType"></s:hidden>
<s:hidden id="shopCodeOld" name="staff.shop.shopCode"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
// 	$('#areaTree').combotree('setValue',activeType.ALL);
	applyDateTimePicker("#workStartDate");
	applyDateTimePicker("#idDate");
	var staffId = $('#staffId').val();
	if (staffId != null && (staffId != 0 || staffId != "0")) {
		StaffCatalog.getListSaleCatByStaffId();
		$('#staffCode').attr('disabled',true);
	}
	var status = '<s:property value="status"/>';
	var gender ='<s:property value="staff.gender.value"/>';
	var isSaleStaff ='<s:property value="isSaleStaff"/>';
	/* setTimeout(function(){
		$('.MySelectBoxClass').customStyle();		
		$('.CustomStyleSelectBox').css('width','174px');
	}, 200); */	
	var staffT = '<s:property value="staff.staffCode"/>';
	if (staffT != ""){
		var areaIdOfShop = '<s:property value="staff.area.id"/>';
// 		if (areaIdOfShop != '') {
// 			UnitTreeCatalog.loadTreeEx(areaIdOfShop,staffId);
// 		} else {
// 			UnitTreeCatalog.loadTreeEx(null,staffId);
// 		}
		if(isSaleStaff =="true"){
			disableSelectbox('gender');
			disableSelectbox('status');
			disableSelectbox('staffType');
			$('#status').val(status);
			$('#status').change();
			$('#gender').val(gender);
			$('#gender').change();
			enableSelectbox('saleType');
			enableSelectbox('workState');
			$('#frmStaff .InputTextStyle').attr('disabled',true);
			$('#staffCode').attr('disabled',true);
			$('.CalendarLink').unbind('click');
// 			disableSelectbox('province');
// 			disableSelectbox('district');
// 			disableSelectbox('precinct');
		}
		else{
			$('#staffName').removeAttr('disabled');
			enableSelectbox('gender');
			$('#workStartDate').removeAttr('disabled');
			$('#education').removeAttr('disabled');
			$('#idCard').removeAttr('disabled');
			$('#idDate').removeAttr('disabled');
			$('#idPlace').removeAttr('disabled');
			$('#position').removeAttr('disabled');
			$('#street').removeAttr('disabled');
			$('#address').removeAttr('disabled');
			$('#staffPhone').removeAttr('disabled');
			$('#staffTelephone').removeAttr('disabled');
			$('#email').removeAttr('disabled');
			enableSelectbox('saleType');
			enableSelectbox('workState');
			$('#status').val(status);
			$('#status').change();
			$('#gender').val(gender);
			$('#gender').change();
		} 
		$('#title').html('Thông tin nhân viên');
		StaffCatalog.activeTab('tab1',true);
	}else{
		$('#frmStaff').removeClass('BoxDisSelect');
		//UnitTreeCatalog.loadTreeEx(null,staffId);
		StaffCatalog.activeTab('tab1',false);
		enableSelectbox('gender');
		enableSelectbox('staffType');
		enableSelectbox('status');
		disableSelectbox('saleType');
		disableSelectbox('workState');
		//disableSelectbox('saleGroup');
		$('#title').html('Tạo mới nhân viên');
	}
	Utils.bindFormatOnTextfield('staffCode', Utils._CODE);
	Utils.bindFormatOnTextfield('staffPhone', Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield('staffTelephone', Utils._TF_NUMBER);
	var staffType = $('#staffTypeHidden').val().trim();
	if($('#staffId').val()==0){
		$('#tabItem2').hide();
		$('#imfoCat').hide();
		$('#frmStaff').removeClass('BoxDisSelect');
	} else if (staffType == 3 || staffType == 4) {
		$('#imfoCat').hide();
		disableSelectbox('saleType');
		disableSelectbox('staffType');
	} else if (staffType == 1 || staffType == 2) {
		
	}
	else{
		$('#tabItem2').hide();
	}
	$('#saleGroup').hide();


	StaffCatalog.bindAutoUpdate();	

	focusFirstTextbox();
	
	$('#province').combobox({
		valueField : 'areaCode',
		textField : 'areaName',
		data: getDataAreaCombobox('province'),
		panelWidth:206,
		formatter: function(r) {
			return Utils.XSSEncode(r['areaName'].trim());
		},
		filter: function(q, r){
			q = q.toString().trim().toUpperCase();
			q = unicodeToEnglish(q);
			return (r['searchText'].indexOf(q)>=0);
		},
		onSelect: function(r) {
			var data = {};
			data.objectType = 2; // district
			data.areaId = r.id;
			$('#district').combobox('clear');
			$('#precinct').combobox('clear');
			Utils.getJSONDataByAjaxNotOverlay(data, '/catalog_customer_mng/get-list-sub-area',function(result) {
				if(result == undefined || result == null){
					result = [];
				}
				var obj;
				for (var i = 0, sz = result.length; i < sz; i++) {
					obj = result[i];
					obj.searchText = unicodeToEnglish(obj.areaCode + obj.areaName);
					obj.searchText = obj.searchText.toUpperCase();
				}
				$('#district').combobox('loadData', result);
			});
		}/* ,
		onLoadSuccess: function() {
			var $this = $(this);
			$this.combobox('setValue', $this.find("option:selected").attr("value"));
		} */
	});
	$('#district').combobox({
		valueField : 'areaCode',
		textField : 'areaName',
		data: getDataAreaCombobox('district'),
		panelWidth:206,
		formatter: function(r) {
			return Utils.XSSEncode(r['areaName'].trim());
		},
		filter: function(q, r){
			q = q.toString().trim().toUpperCase();
			q = unicodeToEnglish(q);
			return (r['searchText'].indexOf(q)>=0);
		},
		onSelect: function(r) {
			var data = {};
			data.objectType = 3; // ward
			data.areaId = r.id;
			$('#precinct').combobox('clear');
			Utils.getJSONDataByAjaxNotOverlay(data, '/catalog_customer_mng/get-list-sub-area',function(result) {
				if(result == undefined || result == null){
					result = [];
				}
				var obj;
				for (var i = 0, sz = result.length; i < sz; i++) {
					obj = result[i];
					obj.searchText = unicodeToEnglish(obj.areaCode + obj.areaName);
					obj.searchText = obj.searchText.toUpperCase();
				}
				$('#precinct').combobox('loadData', result);
			});
		}
	});
	$('#precinct').combobox({
		valueField : 'areaCode',
		textField : 'areaName',
		data: getDataAreaCombobox('precinct'),
		panelWidth:206,
		formatter: function(r) {
			return Utils.XSSEncode(r['areaName'].trim());
		},
		filter: function(q, r){
			q = q.toString().trim().toUpperCase();
			q = unicodeToEnglish(q);
			return (r['searchText'].indexOf(q)>=0);
		},
		onSelect: function(r) {
			$('#areaId').val(r.id);
		}
	});
});

function getDataAreaCombobox(objectId, prefix){
	var data = [];		
	var parent = '';
	if(prefix!=undefined && prefix!=null){
		parent = prefix;
	}
	var obj;
	var $this;
	$(parent + '#' + objectId +' option').each(function(){
		$this = $(this);
		obj = {};
		obj.areaCode = $this.val().trim();
		obj.areaName = $this.text().trim();
		obj.id = $this.attr('areaId').trim();
		obj.centerCode = $this.attr('centerCode').trim();
		obj.searchText = unicodeToEnglish($(this).val().trim()+ $(this).text().trim());
		obj.searchText = obj.searchText.toUpperCase();
		data.push(obj);
	});
	return data;
}
</script>
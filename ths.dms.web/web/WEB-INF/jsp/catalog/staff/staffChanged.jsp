<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="ModuleList71Form">
<label class="LabelStyle Label1Style">Mã đơn vị</label>
<input id="shopCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="shopCode"/>"/>
<label class="LabelStyle Label2Style">Tên đơn vị</label>
<input id="shopName" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="shopName"/>"/>
<label class="LabelStyle Label3Style">Mã NV</label>
<input id="staffCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.staffCode"/>"/>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Tên NV</label>
<input id="staffName" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.staffName"/>"/>
<label class="LabelStyle Label2Style">Từ ngày</label>
<input id="startDate" type="text" class="InputTextStyle InputText2Style" />
<label class="LabelStyle Label3Style">Đến ngày</label>
<input id="endDate" type="text" class="InputTextStyle InputText2Style" />
<div class="Clear"></div>
<div class="ButtonSection">
	<button class="BtnGeneralStyle Sprite2" onclick="return StaffCatalog.searchChanged();"><span class="Sprite2">Tìm kiếm</span></button>
	<img id="loading" src="/resources/images/loading.gif" class="LoadingStyle" style="display: none;"/>
</div>
<div class="GeneralMilkBox GeneralMilkExtInBox">
    <div class="GeneralMilkTopBox">
        <div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Thông tin tác động thay đổi</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="changedGrid">
                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle"></p>
                  	<table id="grid"></table>
					<div id="pager"></div>
              	</div>
            </div>
        </div>
    </div>
</div>
<div id="divDetail" class="GeneralMilkBox GeneralMilkExtInBox" style="display: none;">
    <div class="GeneralMilkTopBox">
        <div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Chi tiết thay đổi</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="detailChangedGrid">
                  	<p id="detailGridNoResult" style="display: none" class="WarningResultStyle"></p>
                  	<table id="detailGrid"></table>
					<div id="detailPager"></div>
              	</div>
            </div>
        </div>
    </div>
</div>
<div class="ButtonSection">
	<button class="BtnGeneralStyle Sprite2" onclick="StaffCatalog.exportActionLog();"><span class="Sprite2">Xuất Excel</span></button>
</div>
<p id="errMsgActionLog" class="ErrorMsgStyle Sprite1" style="display: none"></p>	
</div>
<s:hidden id="groupId" value="0"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	applyDateTimePicker("#startDate");
	applyDateTimePicker("#endDate");
	$('#startDate').val(getLastWeek());
	$('#endDate').val(getCurrentDate());
	$('.MySelectBoxClass').customStyle();
});
</script>
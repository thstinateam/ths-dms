<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
       	<ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a href="javascript:void(0);"><s:text name="catalog_sales_brand_catalogy"/></a></li>
           <li><span><s:text name="sale.in.price.manage"/></span></li>
    	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style"><s:text name="price.info.search"/></h2>
				<div class="SearchInSection SProduct1Form" id="divSearchForm">
					<label class="LabelStyle Label1Style"><s:text name="image_manager_unit"/><span class="ReqiureStyle"> *</span></label>
					<div class="BoxSelect ">
						<input  type="text" class="InputTextStyle InputText1Style" style="width: 206px;" id="cbxUnit" maxlength="50" />
					</div>
					
                    <label class="LabelStyle Label1Style"><s:text name="tho.rpt.dm.1.1.menu2"/></label>
					<div class="BoxSelect BoxSelect12">
						<input type="text" style="width: 206px;" class="easyui-combobox" id="cbxShop" maxlength="50" />
					</div>
					
					<label class="LabelStyle Label1Style"><s:text name="price.menu10"/></label>
					<div class="BoxSelect BoxSelect2" id="ddlStatusDiv">
						<select class="MySelectBoxClass" id="ddlStatus">
							<option value="-2" selected="selected"><s:text name="price_combo_all"/></option>
							<option value="1" ><s:text name="action.status.name"/></option>
							<option value="0"><s:text name="pause.status.name"/></option>
						</select>                            
					</div> 
					
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style"><s:text name="price.menu1"/></label>
					<input id="txtProductCode" type="text" class="InputTextStyle InputText1Style" maxlength="100" />
	
					<label class="LabelStyle Label1Style"><s:text name="price.menu2"/></label>
					<input id="txtProductName" type="text" class="InputTextStyle InputText1Style" maxlength="100"/>
	
	                <div class="Clear"></div>
	                
					<label class="LabelStyle Label1Style"><s:text name="price.menu8"/></label>
					<input id="txtFromDate" type="text" class="InputTextStyle InputText1Style vinput-date"/> 
	
					<label class="LabelStyle Label1Style"><s:text name="price.menu9"/></label>
					<input id="txtToDate" type="text" class="InputTextStyle InputText1Style vinput-date"/>
					<div class="Clear"></div>

					<div class="BtnCenterSection">
						<button id="btnSearchPrice" class="BtnGeneralStyle cmsdefault" onclick="return SaleInPriceManager.searchProductPrice();"><s:text name="price.btn.search"/></button>
					</div>
					<div class="Clear"></div>
				</div>
			</div>
			<p id="errMsgSaleInPriceManager" style="display: none;" class="ErrorMsgStyle"></p>
			<p id="successMsgSaleInPriceManager" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear"></div>
		</div>
		<div class="GeneralCntSection">
			<h2 class="Title2Style"><s:text name="price.list.price"/></h2>
			<div class="GridSection" id="productPriceDgDiv">
				 <table id="dgProductPrice"></table>
			</div>
			<div class="Clear"></div>
			<p id="errMsgImportPrice" style="display: none;" class="ErrorMsgStyle"></p>
			<p id="successMsgImportPrice" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear"></div>
			<div class="GeneralForm GeneralNoTP1Form" style="width:auto;">
				<button class="BtnGeneralStyle" id="btnExportPrice" style="float:right;" onclick="return SaleInPriceManager.exportBySearchPrice();"><s:text name="price_export_excel"/></button>
                <div id="group_edit_showBtnFooterGrid" class="Func1Section cmsiscontrol">
		       		<button class="BtnGeneralStyle" id="btnImportPrice" style="float:right;" onclick="return SaleInPriceManager.importPriceProduct();"><s:text name="price_import_excel"/></button>
		       		<form style="float:right; width: 345px;" action="/sale-in/importPriceProduct" name="importFrmPrice" id="importFrmPrice" method="post" enctype="multipart/form-data">
	                	<p class="DownloadSFileStyle DownloadSFile2Style">
				        	<a href="javascript:void(0)" id="downloadTemplate" class="Sprite1" onclick="SaleInPriceManager.downloadImportPriceTemplateFile();"><s:text name="price.dowload.exc.template"/></a>
				        </p>
						<input type="hidden" id="importTokenVal" name="token" value='<s:property value="token"/>'>
						<input type="file" class="InputFileStyle InputText1Style" style="width: 205px;" size="20" name="excelFile" id="excelFileFormPrice" onchange="previewImportExcelFile(this,'importFrmPrice');"> 
						<div class="FakeInputFile">
							<input type="text" id="fakefilepc" style="width: 195px;" class="InputTextStyle" />
						</div>
					</form>
                </div>
                <div class="Clear"></div>
            </div>
		</div>
		<div class="Clear" style="height: 20px;"></div>
	</div>
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
	var firstLoadPage = true;
	$(document).ready(function() {
		ReportUtils.loadComboTree('cbxUnit', 'shopId', $('#curShopId').val(), function(shopId, node) {
			$('#shopCode').val(node.code);
			var param = {};
			param.shopId = $('#shopId').val().trim();
			param.isChoiceAll = true;
			Utils.initUnitCbx('cbxShop', param, 206, function (rec) {
				if (rec != null && rec.id != null && rec.shopCode != null) {
					var shopId = rec.id;
					$('#curShopId').val(shopId);
// 					ShopParamConfig.search(shopId);
// 					$('#btnSearch').attr('onclick', 'return ShopParamConfig.search(' + shopId + ');');
		    	}	
			});
		});
		
		$('#divSearchForm input').bind('keyup', function(event) {
			if (event.keyCode == keyCodes.ENTER) {
				$('#btnSearchPrice').click();
			}
		});
		//$('#downloadTemplatePrice').attr('href', excel_template_path + 'price/Bieu_Mau_Import_Gia_San_Pham.xls');
		$('#txtFromDate, #txtToDate').width($('#txtFromDate').width()-22);
		$('#txtProductCode').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
					params: {
						status: 1
					},
					inputs: [
				        {id:'code', maxlength:50, label:'<s:text name="price_product_code"/>'},
				        {id:'name', maxlength:250, label:'<s:text name="price_product_name"/>'}
				    ],
				    url: '/commons/product-searchAllProduct',
				    columns: [[
				        {field:'productCode', title:'<s:text name="price_product_code"/>', align: 'left', width: 110, sortable: false, resizable: false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'productName', title:'<s:text name="price_product_name"/>', align: 'left', width: 200, sortable: false, resizable: false, formatter: function(value, row, index) {
				        	return Utils.XSSEncode(value);         
				        }},
				        {field:'choose', title:'', align: 'center', width:40, sortable: false, resizable: false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="SaleInPriceManager.fillTxtProductCodeByF9(\''+Utils.XSSEncode(row.productCode)+'\',\''+Utils.XSSEncode(row.productName)+'\');"><s:text name="price_choose"/></a>';         
				        }}
				    ]]
				});
			} else if (event.keyCode == keyCode_DELETE) {
				$('#txtProductCode').val('');
			}
		});
		
		$('#dgProductPrice').datagrid({
			data: [],
			//pagination : true,
	        rownumbers: true,
	        //pageNumber : 1,
	        checkOnSelect: false,
			selectOnCheck: false,
	        scrollbarSize: 0,
	        autoWidth: true,
	        //pageList: [10, 20, 50],
	        autoRowHeight: true,
	        fitColumns: true,
			columns:[[ 
			    {field: 'shopText', title: price_grid_column2, width: 90, sortable: false, align: 'left', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'productCode', title: catalog_product_com, width: 140, sortable: false, align: 'left', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'fromDateStr', title: price_grid_column7, width: 40, sortable: false, align: 'center', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'toDateStr', title: price_grid_column8, width: 40, sortable: false, align: 'center', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'pricePackage', title:price_grid_column14, width: 60, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'price', title: price_grid_column10, width: 50, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'vat', title: price_grid_column11, width: 30, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'pricePackageNotVAT', title: price_grid_column13, width: 60, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'priceNotVAT', title: price_grid_column9, width: 50, sortable: false, align: 'right', formatter: function(value, row, index) {
			    	return '';
			    }},
			    {field: 'status', title: price_grid_column12, width: 40, sortable: false, align: 'left', formatter: function(value, row, index){
			    	return '';
			    }},
			    {field: 'change', title: '', width: 20, align: 'center', formatter: function(value, row, index) {
			    	return '';
			    }}
			]],
	        onLoadSuccess :function(data) {
	        	$('.datagrid-header-rownumber').html('<s:text name="price_stt"/>');
				Utils.updateRownumWidthAndHeightForDataGrid('dgProductPrice');
	        }
		});
		//Forcus khi moi vao trang
		$('#txtProductCode').focus();
	});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style>
.InputShopText {
	border: 1px solid #c4c4c4;
    height: 20px;
    line-height: 20px;
    text-align: right; 
    width: 100px;
}
.LabelShop {
	color: #215ea2;
	padding: 0 5px;
}
.Label1Shop {
	color: #215ea2;
	padding-left: 40px;
}
.wdTable tr {
	height: 30px;
}
.titleShop {
	margin: 6px 0px;
}
.tableM1 {
	margin-left: 40px;
}
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#"><s:text name="organization_system"></s:text></a></li>
        <li><span><s:text name="shop_param_page_title"></s:text></span></li>
    </ul>
</div>

<div class="CtnOneColSection">
   <div class="ContentSection">
   		<div class="ToolBarSection">
           <div class="SearchSection GeneralSSection">
	           	<h2 class="Title2Style"><s:text name="shop_param_page_title_h2"></s:text></h2>
	           	<div class="SearchInSection SProduct1Form">
	           		<label class="LabelStyle Label2Style"><s:text name="image_manager_unit"/><span class="ReqiureStyle"> *</span></label>
					<div class="BoxSelect ">
						<input  type="text" class="InputTextStyle InputText1Style" style="width: 250px;" id="cbxUnit" maxlength="50" />
					</div>
					
                    <label class="LabelStyle Label6Style"><s:text name="tho.rpt.dm.1.1.menu2"/><span class="ReqiureStyle"> *</span></label>
					<div class="BoxSelect BoxSelect12">
						<input  type="text" style="width:226px;" class="easyui-combobox" id="cbxShop" maxlength="50" />
					</div>
					<div style="float: left; margin-top: -3px; margin-left: 30px;">
						<button id="btnSearch" class="BtnGeneralStyle" onclick="return ShopParamConfig.search();"><s:text name="jsp.common.timkiem"/></button>
					</div>
					<div class="Clear"></div>
					<p id="errMsgSearch" class="ErrorMsgStyle" style="display: none"></p>
	           	</div>
	           	<div class="Clear"></div>     	
	           	<h2 class="Title2Style"><s:text name="shop_param_page_title_info"></s:text></h2>
	           	<div class="SearchInSection SProduct1Form">
	           			<table class="wdTable" width="100%" border="0" cellspacing="0" cellpadding="0">
	                       <colgroup>
	                                <col style="width:120px">
	                                <col style="width:500px">
	                       </colgroup>
	                       <tbody>
                       		<tr>
                            	<td><label class="LabelShop">Khoảng cách ghé thăm, đặt hàng</label></td>
                                <td>
	                               <input id="shopDistanceOrder" class="InputShopText" maxlength="10" autocomplete="off"> 
		                		   <s:hidden id="hdShopDistanceOrder"></s:hidden>
		                		   <label class="LabelShop" style="text-align: left;">(mét)</label>
                                </td>
                            </tr>
                           </tbody>
                       </table>
                       <div class="Clear"></div>
                       
                       <div class="titleShop"><label class="LabelShop">Thời gian đi tuyến</label> </div>
                       <table class="wdTable" width="100%" border="0" cellspacing="0" cellpadding="0">
                       <colgroup>
                                <col style="width:120px">
                                <col style="width:500px">
                       </colgroup>
                       <tbody>
                       		<tr>
                       			<td><label class="Label1Shop">Đầu ngày (ghé thăm KH đầu)</label></td>
                                <td>
	                               <input id="shopDTStart" readonly="readonly" class="InputShopText" autocomplete="off"> 
		                		   <s:hidden id="hdShopDTStart"></s:hidden>
		                		   <label class="LabelShop"><s:text name="jsp.settings.system.hour.auto.close.date.format"/></label>
                                </td>
                            </tr>
                            <tr>
                            	<td><label class="Label1Shop">Giữa ngày (ghé thăm 50%)</label></td>
                                <td>
	                               <input id="shopDTMiddle" readonly="readonly" class="InputShopText" autocomplete="off"> 
		                		   <s:hidden id="hdShopDTMiddle"></s:hidden>
		                		   <label class="LabelShop"><s:text name="jsp.settings.system.hour.auto.close.date.format"/></label>
                                </td>
                            </tr>
                            <tr>
                            	<td><label class="Label1Shop">Cuối ngày (ghé thăm 100%)</label></td>
                                <td>
	                               <input id="shopDTEnd" readonly="readonly" class="InputShopText" autocomplete="off"> 
		                		   <s:hidden id="hdShopDTEnd"></s:hidden>
		                		   <label class="LabelShop"><s:text name="jsp.settings.system.hour.auto.close.date.format"/></label>
                                </td>
                            </tr>
                           </tbody>
                       </table>
                       <div class="Clear"></div>
                       
                       <div class="titleShop"><label class="LabelShop">Chấm công</label> </div>
                       <table class="wdTable" width="100%" border="0" cellspacing="0" cellpadding="0">
                       <colgroup>
                                <col style="width:120px">
                                <col style="width:500px">
                       </colgroup>
                       <tbody>
                       		<tr>
                            	<td><label class="Label1Shop">Khoảng cách chấm công</label></td>
                                <td>
	                               <input id="shopCCDistance" class="InputShopText" maxlength="10" autocomplete="off"> 
		                		   <s:hidden id="hdShopCCDistance"></s:hidden>
		                		   <label class="LabelShop">(mét)</label>
                                </td>
                            </tr>
                            <tr>
                            	<td><label class="Label1Shop">Thời gian chấm công từ</label></td>
                                <td>
	                               <input id="shopCCStart" readonly="readonly" class="InputShopText" autocomplete="off"> 
		                		   <s:hidden id="hdShopCCStart"></s:hidden>
		                		   <label class="LabelShop">  đến</label>
		                		   <input id="shopCCEnd" readonly="readonly" class="InputShopText" autocomplete="off"> 
		                		   <s:hidden id="hdShopCCEnd"></s:hidden>
		                		   <label class="LabelShop"><s:text name="jsp.settings.system.hour.auto.close.date.format"/></label>
                                </td>
                            </tr>
                           </tbody>
                       </table>
                       <div class="Clear"></div>
                       
                       <div class="titleShop"><label class="LabelShop">Chốt ngày</label> </div>
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       <colgroup>
                                <!-- <col style="width:120px">
                                <col style="width:500px"> -->
                       </colgroup>
                       <tbody>
                       		<tr>
                            	<td valign="top">
                            			<label class="Label1Shop">Đổi thời gian chốt trong ngày</label>
                            	</td>
                                <td>
		                		   <div class="GridSection">
				                		<div id="closeDayStartResult" class="GeneralTable">
											<div class="BoxGeneralTTitle" style="width: 100%;" id="dgGridCloseDayStart">
												<table id="gridCloseDayStart"></table>
												<div id="pager"></div>
											</div>
										</div>
						                <div class="Clear"></div>
			                     	</div>
                                </td>
                            </tr>
                            <tr>
                            	<td valign="top"><label class="Label1Shop">Tạm dừng chốt tự động</label></td>
                                <td>
		                		   <div class="GridSection">
				                		<div id="closeDayEndResult" class="GeneralTable">
											<div class="BoxGeneralTTitle" style="width: 100%;" id="dgGridCloseDayEnd">
												<table id="gridCloseDayEnd"></table>
												<div id="pager"></div>
											</div>
										</div>
						                <div class="Clear"></div>
			                     	</div>
                                </td>
                            </tr>
                           </tbody>
                       </table>
                       <div class="Clear"></div>
                       
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                       <colgroup>
	                                <!-- <col style="width:120px">
	                                <col style="width:500px"> -->
	                       </colgroup>
                       	   <tbody>
                       		<tr>
                            	<td valign="top"><label class="LabelShop">Số ngày tồn kho an toàn (PO Auto)</label></td>
                                <td>
		                		   <div class="GridSection" >
				                		<div id="productStockDayResult" class="GeneralTable" style="height: 300px;">
											<div class="BoxGeneralTTitle" style="width: 100%;" id="dgGridProductStockDay">
												<table id="gridProductStockDay"></table>
												<div id="pager"></div>
											</div>
										</div>
						                <div class="Clear"></div>
			                     	</div>
                                </td>
                            </tr>
                           </tbody>
                       </table>
	           	</div>
	           	<div class="Clear"></div>
	           	<div class="SearchInSection SProduct1Form">
		           	<div class="BtnCenterSection">
						<button id="btnUpdate" class="BtnGeneralStyle" onclick="return ShopParamConfig.saveInfo();">Cập nhật</button>
					</div>
				</div>
	            <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
           </div>
      	</div>
        
   </div>
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/catalog/shop-param/popupCloseDayStart.jsp" />
</div>
<div class="ContentSection" style="visibility: hidden;">
	<tiles:insertTemplate template="/WEB-INF/jsp/catalog/shop-param/popupCloseDayEnd.jsp" />
</div>
<s:hidden id="closeDayHM"></s:hidden>
<s:hidden id="beforeMinute"></s:hidden>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	//load Cay don vi cbx
	$('#divOverlay').addClass('Overlay');
	$('#divOverlay').show();
	$('#imgOverlay').show();
	ReportUtils.loadComboTree('cbxUnit', 'shopId', $('#curShopId').val(), function(shopId){
		var param = {};
		param.shopId = $('#shopId').val().trim();
// 		Utils.initUnitCbx('cbxShop', param, 230, null, AsoTarget.initCycleInfo);
		Utils.initUnitCbx('cbxShop', param, 230, function (rec) {
			if (rec != null && rec.id != null && rec.shopCode != null) {
				var shopId = rec.id;
				$('#curShopId').val(shopId);
				ShopParamConfig.search(shopId);
				$('#btnSearch').attr('onclick', 'return ShopParamConfig.search(' + shopId + ');');
	    	}	
		});
	});
	Utils.bindFormatOnTextfield('shopDistanceOrder', Utils._TF_NUMBER_COMMA);
	Utils.formatCurrencyFor('shopDistanceOrder');
	Utils.bindFormatOnTextfield('shopCCDistance', Utils._TF_NUMBER_COMMA);
	Utils.formatCurrencyFor('shopCCDistance');
	
	var inputShopDTStart = $('#shopDTStart').clockpicker({
	    placement: 'bottom',
	    align: 'left',
	    autoclose: true
	});
	var inputShopDTMiddle = $('#shopDTMiddle').clockpicker({
	    placement: 'bottom',
	    align: 'left',
	    autoclose: true
	});
	var inputShopDTEnd = $('#shopDTEnd').clockpicker({
	    placement: 'bottom',
	    align: 'left',
	    autoclose: true
	});
	var inputShopCCStart = $('#shopCCStart').clockpicker({
	    placement: 'bottom',
	    align: 'left',
	    autoclose: true
	});
	var inputShopCCEnd = $('#shopCCEnd').clockpicker({
	    placement: 'bottom',
	    align: 'left',
	    autoclose: true
	});
	
	
});
</script>
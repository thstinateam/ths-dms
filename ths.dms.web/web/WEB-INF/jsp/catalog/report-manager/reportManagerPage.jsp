<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.viettel.core.entities.enumtype.ShopObjectType" %>
<style>
.linkdow { text-decoration: underline;}
.linkdow: hover { text-decoration: none;}
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="#">Danh mục</a></li>
		<li><span>Quản lý mẫu báo cáo</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<!-- <h2 class="Title2Style">Thông tin tìm kiếm</h2> -->
				<div class="SearchInSection SProduct1Form">
					<div class="Clear"> <br></div>
					<s:if test="shop.getType().getObjectType() != 3">
						<div style="float: left;">
							<label class="LabelStyle Label1Style">Đơn vi <span class="RequireStyle">(*)</span></label>
							<input id="shop" style="width: 200px;" />
						</div>
					</s:if>
					<label class="LabelStyle Label1Style" >Mã BC</label>
					<input id="reportCode" tabindex="1" type="text"  class="InputTextStyle InputText4Style" maxlength="50" style="width: 150px;" />
					<label class="LabelStyle Label1Style">Tên BC</label> 
					<input id="reportName" tabindex="2"	type="text" class="InputTextStyle InputText1Style" maxlength="250" style="width: 250px;" />
					<div class="BtnLSection" style="padding-left: 20px; margin-top: -2px;">
						<button tabindex="3" id="btnSearch" class="BtnGeneralStyle" onclick="return ReportManager.search()" >Tìm kiếm</button>
					</div>		
					<div class="Clear"></div>
					<!-- <p class="ErrorMsgStyle" style="display: none;" id="errMsg" />
					<div class="Clear"></div> -->
				</div>
				<h2 class="Title2Style">Danh sách báo cáo 
					<s:if test="shop.getType().getObjectType() == 0">
						<%-- (<span class="RequireStyle" style="text-transform:none"> <s:property value="shop.getShopCode()"/> - <s:property value="shop.getShopName()"/></span>) --%>
						<span id="divShopNameView">(<span id="divShopName" class="RequireStyle" style="text-transform:none"><s:property value="shop.getShopCode()"/> - <s:property value="shop.getShopName()"/></span>) </span>
					</s:if>
					
				</h2>
				<div class="GridSection" id="dgContainer">
					<table id="dg"></table>
					<div class="Clear"></div>
				</div>
					<div class="Clear"></div>
						<p id="errMsgEdit" class="ErrorMsgStyle" style="display: none;" />
						<p id="successMsgEdit" class="SuccessMsgStyle" style="display: none; margin-top: 10px; margin-left: 10px" />
					<div class="Clear"></div>
				<div id="divReportDetail">
				<h2 class="Title2Style">Danh sách file Gốc báo cáo (<span class="RequireStyle" style="text-transform:none"> </span>)</h2> 
				<form action="/catalog/report-manager/importFileReportDetail" name="importFrmReport" id="importFrmReport" method="post" enctype="multipart/form-data">
					<div class="GridSection">
						<table id="dgReportDetail"></table>
						<div class="Clear"></div>
					</div>
					<s:hidden id="checkDefault" name="checkDefault"></s:hidden> <!--  // checkDefault: -1: khong thay doi, 0: la cai moi, ngc lai la cai khac cap nhat  -->
					<s:hidden id="reportId" name="reportId"></s:hidden>   <!--  //lay cai report can Chinh sua -->
					<s:hidden id="reportIdDefault" name="reportIdDefault"></s:hidden>   <!--  // id report can Chinh sua -->
					<s:hidden id="typeReport" name="typeReport"></s:hidden>   <!--  // typeReport: 0: VNM, 1: NPP -->
					<s:hidden id="shopIdReport" name="shopIdReport"></s:hidden>
				</form>
				</div>
					<div class="Clear"></div>
							<p id="errMsg" class="ErrorMsgStyle" style="display: none;" />
							<p id="successMsg" class="SuccessMsgStyle" style="display: none; margin-top: 10px; margin-left: 10px" />
					<div class="Clear"></div>
				<div class="SearchInSection SProduct1Form">
					<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none; float: left; margin: 0px; padding-left: 10px;"></p>
					<p id="successExcelMsg" class="SuccessMsgStyle" style="display: none; float: left; margin: 0px; padding-left: 10px;"></p>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>

<div style="display: none;">
	<div id="easyuiPopupUpload" class="easyui-dialog" title="Tạo báo cáo" data-options="closed:true,modal:true" style="width: 540px;">
		<div class="PopupContentMid2" >
			<div class="GeneralForm Search1Form" >
				<form action="/catalog/report-manager/importFileReport" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
					<input type="hidden" name="token" value="<s:property value="token" />" id="importTokenVal"  />
					<input type="hidden" id = "checkSubmit" name="checkSubmit">
					<label class="LabelStyle Label11Style" style="width: 75px">Mã BC <span class="RequireStyle">(*)</span></label>
					<input type="text" tabindex="6" style="width: 345px;" class="InputTextStyle InputText2Style" maxlength="50" id="reportCodeDlg" name="reportCode" />
					<div class="Clear"></div>
					<label class="LabelStyle Label11Style" style="width: 75px">Tên BC <span class="RequireStyle">(*)</span></label>
					<input type="text" tabindex="7" style="width: 345px;" class="InputTextStyle InputText2Style" maxlength="250" id="reportNameDlg" name="reportName" />
					<div class="Clear"></div>
					
					<label class="LabelStyle Label11Style" style="width: 75px;">File mẫu </label>
					<div class="UploadFileSection Sprite1" style="width: 424px;">
						<input id="fakefilepcVNM" name="nameFile" placeholder="File .jrxml" type="text" class="InputTextStyle InputText1Style" style="width: 345px;" readonly="readonly">
						<input type="file" tabindex="10" class="UploadFileStyle" size="1" name="reportFile" id="reportFile" onchange="uploadJasperFile(this,'importFrm','fakefilepcVNM','errPdfMsg');" style="float: left; width: 100px;" />
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label11Style" style="width: 75px">Mặc định </label>
					<input id="checkDefault" name="checkDefault" type="checkbox" class="InputTextStyle" onchange="javascript:$(this).val($(this).prop('checked') ? 1 : 0);" value="0"/>
				</form>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="saveReport" tabindex="11" class="BtnGeneralStyle" onclick="return ReportManager.importReportFile();">Lưu</button>
					<button tabindex="12" class="BtnGeneralStyle" onclick="return ReportManager.closeWindow();">Đóng</button>
				</div>
			</div> 
			<div class="Clear"></div>
				<p style="display: none; margin-top: 10px; margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errPdfMsg"></p>
				<p style="display: none; margin-top: 10px; margin-left: 10px" class="SuccessMsgStyle" id="resultPdfMsg"></p>
		</div>
	</div>
</div>

<s:hidden id="shopId" ></s:hidden>
<s:hidden id="urlVNM" name="urlVNM"></s:hidden>
<s:hidden id="urlNPP" name="urlNPP"></s:hidden>
<s:hidden id="curShopId" name="shopId" />
<s:hidden id="isNPP"/> <!-- cai nay khi click loadComboTree se co isNPP -->
<script type="text/javascript">
$(document).ready(function(){
	$('#divReportDetail').css('visibility', 'hidden');	
	$('.Label1Style').css('width','65px');
	$('.Label2Style').css('width','65px');
	$('#reportCode').focus();
	
	var stafRole = '<s:property value="staffSign.getStaffType().getObjectType()" />';
	var shopType = '<s:property value="shop.getType().getObjectType()" />';
	var shopId = $('#shopId').val().trim();
	if(shopId == null || shopId == ""){ // load lan dau hok co shop ID
		shopId = $('#curShopId').val().trim();
	}
	var titleUpload = '';
	//NHVNM:8, KTNPP:9
	if($('#urlNPP').val().trim()== '' && $('#urlVNM').val().trim() === '1'){
		ReportUtils.loadComboTreeAuthorizeEx('shop','shopId',$('#curShopId').val());
		var t = $('#shop').combotree('tree');
		var n = t.tree('getSelected');
		if(n != null || n != undefined){
			$('#divShopName').html(n.text);
			$('#divShopNameView').css('visibility', 'visible');
		}
		// danh sach bao cao
		//var VNM = "<a href= 'javascript:void(0)' title='Edit' onclick= \"return ReportManager.editReportShopMap("+rowData.id + ',' + rowIndex +");\"><img src='/resources/images/icon-edit.png' height='15'/></a>";
		titleUpload = '<a id="idAddReport"  href="javascript:void(0)" title="Add" onclick="ReportManager.uploadFileReport()"><img src="/resources/images/icon_add.png"/></a>';
		$('#dg').datagrid({
			url : '/catalog/report-manager/search',		
			rownumbers : true,	
			fitColumns:true,
			pagination:true,
			rowNum : 20,		
			pageList  : [10,20,50],
			scrollbarSize:0,
			width: $(window).width()-35,
			singleSelect:true,
			autoWidth: true,
			queryParams:{
				shopId: shopId
		 	},
		    columns:[[  
		        {field:'reportCode',title:'Mã báo cáo',align:'left', width:100,sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
		        	return Utils.XSSEncode(value);
		        }},
		        {field:'reportName',title:'Tên báo cáo',align:'left', width:210, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
		        	return Utils.XSSEncode(value);
		        },editor: 'text'}, 
		        {field:'typeDefault',title:'File gốc',align:'center', width:50, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
		        		var data = "<a href= 'javascript:void(0)' title='Edit' onclick= \"return ReportManager.editReportShopMapVNM("+rowData.id + ',' + rowIndex +");\"><img src='/resources/images/icon-edit.png' height='15'/></a>";
		        		return data;
		        	}
		        }, 
		        {field:'shopType',title:'File NPP',align:'center', width:50, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
			        var data;	
		        	if(ReportManager._isNPP == 'true'){
			        		data = "<a href= 'javascript:void(0)' title='Edit' onclick= \"return ReportManager.editReportShopMap("+rowData.id + ',' + rowIndex +");\"><img src='/resources/images/icon-edit.png' height='15'/></a>";
			        	}else{
			        		data = "<a href= 'javascript:void(0)' title='No Edit'><img src='/resources/images/icon-warning.png' height='15'/></a>";
			        	}
			        	return data;
		        	}
	        	},
		        {field:'action', title:titleUpload, 
		        	align:'center',width:30,sortable : false,resizable : false, 
		        	formatter : function(value,rowData,rowIndex) {
		        		var data = '';
		        		var dataEditName;
		        		var dataDelete = "&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)' title='Delete' onclick=\"return ReportManager.deleteReport("+rowData.id+");\"><img src='/resources/images/icon_delete.png' width='15' height='15'/></a>";
		        		if(!rowData.editName){
		        			dataEditName = "<a href= 'javascript:void(0)' title='Edit' onclick= \"return ReportManager.editReportShopMapName("+rowData.id+ ',' + rowIndex +");\"><img src='/resources/images/icon-edit.png' height='15'/></a>" 
		        		}else{
		        			dataEditName = "<a href= 'javascript:void(0)' title='Edit' onclick= \"return ReportManager.editReportShopMapName("+rowData.id+ ',' + rowIndex +");\"><img src='/resources/images/icon-save.png' width='17' height='17'/></a>"
		        			+ "&nbsp;&nbsp;&nbsp; <a href='javascript:void(0)' title='Back' onclick=\"return ReportManager.backEditReportShopMapName("+rowData.id+ ',' + rowIndex +");\"><img src='/resources/images/icon_esc.png' width='15' height='15'/></a>"
		        		}
		        		data += dataEditName;
		        		data += dataDelete;
		        		return data;
		        	}
		       	}
		    ]],
		    onLoadSuccess :function(data){
		    	var rows = data.rows;
		    	if(rows != undefined && rows != null){
		    		for(var i = 0; i<rows.length; i++){
		    			rows[i].editName = false;
		    		}
		    	}
		    	$('#errMsg').html('').hide();
		    	$('.datagrid-header-rownumber').html('STT');	    		    	
		    	 updateRownumWidthForJqGrid('.easyui-dialog');
	    		 $(window).resize();
	    		 showIconAddOfFirstControlGrid('btnAddReport','idAddReport');
		    }
		});
	 }else { //KTNPP:9
			$('#dg').datagrid({
				url : '/catalog/report-manager/search',		
				rownumbers : true,	
				fitColumns:true,
				pagination:true,
				rowNum : 20,		
				pageList  : [10,20,50],
				scrollbarSize:0,
				width: $(window).width()-35,
				singleSelect:true,
				autoWidth: true,
				queryParams:{
					shopId: shopId
			 	},
			    columns:[[  
			        {field:'reportCode',title:'Mã báo cáo',align:'left', width:100,sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
			        	return Utils.XSSEncode(value);
			        }},
			        {field:'reportName',title:'Tên báo cáo',align:'left', width:210, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
			        	return Utils.XSSEncode(value);
			        }}, 
			        {field:'typeDefault',title:'File gốc',align:'center', width:50, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
			        		var data = "<a href= 'javascript:void(0)' title='View' onclick= \"return ReportManager.editReportShopMapNPP("+rowData.id + ',' + rowIndex +");\"><img src='/resources/images/icon-view.png' height='15'/></a>";
			        		return getFormatterControlGrid('btnViewFileRootNPP',data);
			        	}
			        }, 
			        {field:'type',title:'File NPP',align:'center', width:50, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
			        		var data = "<a href= 'javascript:void(0)' title='Edit' onclick= \"return ReportManager.editReportShopMap("+rowData.id + ',' + rowIndex +");\"><img src='/resources/images/icon-edit.png' height='15'/></a>";
			        		return getFormatterControlGrid('btnViewFileNPPNPP',data);
			        	}
		        	}
			    ]],	    
			    onLoadSuccess :function(data){	 
			    	$('#errMsg').html('').hide();
			    	$('.datagrid-header-rownumber').html('STT');	    		    	
			    	 updateRownumWidthForJqGrid('.easyui-dialog');
		    		 $(window).resize();
			    }
			});
	 }
});
function loadReportDetailVNM(id, shopId, typeFile){
		//danh sach file goc
		var w = $(window).width()-35;
		var titleSave = '<a id="idSaveRoot" href="javascript:void(0)" title="Save" onclick="ReportManager.importReportFileDetail()"><img src="/resources/images/icon-save.png"  width="17" height="17"/></a>';
		$('#dgReportDetail').datagrid({
			url : '/catalog/report-manager/editReport',		
			rownumbers : true,	
			//fitColumns:true,
			pagination:true,
			rowNum : 20,		
			pageList  : [10,20,50],
			scrollbarSize:0,
			width: w,
			singleSelect:true,
			//autoWidth: true,
			queryParams:{
				reportId: id,
				shopId: shopId,
				typeReport: typeFile
		 	},
		    columns:[[  
		        {field:'createDate',title:'Ngày tải lên',align:'center', width:275,sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
		        	var createDate = rowData.createDate;
					if(createDate == null || createDate == undefined || createDate == '' || createDate == 'null'){
						createDate = '';
					}else{
						createDate = formatDate(Utils.XSSEncode(rowData.createDate));
					}
					return createDate;
		        }},
		        {field:'url',title: 'File Gốc' ,align:'center', width:w - (275 + 120 + 370 + 88 + 33), sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
		        	var data = '';
		        	if (rowData.createDate != null) {
			        	if(value == null || value == undefined || value == '' || value == 'null') {
			        		data = '<a class="linkdow" href = "' + ReportServerPath + Utils.XSSEncode(value) + '" title = "Không tìm thấy link">Download</a>';
			        	} else {
			        		data = '<a class="linkdow" href = "' + ReportServerPath + Utils.XSSEncode(value) + '" title = "' + Utils.XSSEncode(value) +'">Download</a>';
			        	}
		        	}
		        	return data;
		        }}, 
		        {field:'isDefault',title:'Mặc định',align:'center', width:120, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
		        	if(rowData.isDefault == 'DEFAULT') {
		        		return '<input type="radio" id="isdefault" name="ckDefault" value="'+rowData.id+'" checked="checked" onchange="ReportManager.changeRadio('+ rowData.id+')" />';
		        	} else { 
		        		return '<input type="radio" id="isdefault" name="ckDefault" value="0"  onchange="ReportManager.changeRadio('+ rowData.id+')" />';
		        	}
		        }}, 
		        {field:'type',title:'Tải File',align:'center', width:370, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
		        	if (rowData.createDate != null) {
		        		var nameUrl = formatStringUrl(rowData.url);
		        		return Utils.XSSEncode(nameUrl);
		        	}
        			return '<div class="UploadFileSection Sprite1" style="width: 360px; height:25px;">' 
        					+ '<input id="fakefilepc" name="nameFile" type="text" class="InputTextStyle InputText1Style" placeholder="File .jrxml" style="width: 280px;" readonly="readonly" />' 
        					+ '<input type="file" tabindex="10" class="UploadFileStyle" size="1" name="reportFile" id="reportFileDetail" onchange="uploadJasperFile(this,\'importFrmReport\');" style="float: left; width: 100px;" />'
        					+ '</div>';
	        	}},
		        {field:'action', title: titleSave, 
		        	align:'center',width: 88,sortable : false,resizable : false, 
		        	formatter : function(value,rowData,rowIndex) {
		        		var data = "<a href='javascript:void(0)' title='Delete' onclick=\"return ReportManager.deleteReportUrl("+rowData.id+  ',' + rowIndex+");\"><img src='/resources/images/icon_delete.png' width='15' height='15'/></a>";
		        		return data;
		        	}
		       	}
		    ]],	    
		    onLoadSuccess :function(data){	 
		    	$('#errMsg').html('').hide();
		    	$('.datagrid-header-rownumber').html('STT');	    		    	
		    	updateRownumWidthForJqGrid('.easyui-dialog');
	    		$(window).resize();
	    		var data = $('#dgReportDetail').datagrid('getData').rows;
	    		var flag = false;
	 	 		for(var i = 0; i< data.length ; i++) {
	 	 			if(data[i].isDefault == 'DEFAULT') {
	 	 				ReportManager._idDefaultGoc = data[i].id;
	 	 				flag = true;
	 	 			}
	 	 		}
	 	 		if(flag == false) {
	 	 			ReportManager._idDefaultGoc = null;
	 	 		}
	    		 $('#dgReportDetail').datagrid('appendRow',{
	    			 createDate: null,
	    			 url: '',
	    			 isDefault:'',
	    			 type: '',
	    			 action: ''
	    			});
	    		showIconAddOfFirstControlGrid('saveReportDetailRoot','idSaveRoot');
		    }
		});
 }
function loadReportDetailNPP(id, shopId, typeFile){
	//danh sach file goc
	var w = $(window).width()-35;
	$('#dgReportDetail').datagrid({
		url : '/catalog/report-manager/editReport',		
		rownumbers : true,	
		//fitColumns:true,
		pagination:true,
		rowNum : 20,		
		pageList  : [10,20,50],
		scrollbarSize:0,
		width: w,
		singleSelect:true,
		//autoWidth: true,
		queryParams:{
			reportId: id,
			shopId: shopId,
			typeReport: typeFile
	 	},
	    columns:[[  
	        {field:'createDate',title:'Ngày tải lên',align:'center', width:250,sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	//return Utils.XSSEncode(value);
	        	var createDate = rowData.createDate;
				if(createDate == null || createDate == undefined || createDate == '' || createDate == 'null'){
					createDate = '';
				}else{
					createDate = formatDate(Utils.XSSEncode(rowData.createDate));
				}
				return createDate;
	        }},
	        {field:'url',title: 'File Gốc' ,align:'center', width:w - (250 + 120 + 460 + 33), sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	var data = '';
	        	if (rowData.createDate != null) {
		        	if(value == null || value == undefined || value == '' || value == 'null') {
		        		data = '<a class="linkdow" href = "' + ReportServerPath + Utils.XSSEncode(value) + '" title = "Không tìm thấy link">Download</a>';
		        	} else {
		        		data = '<a class="linkdow" href = "' + ReportServerPath + Utils.XSSEncode(value) + '" title = "' + Utils.XSSEncode(value) +'">Download</a>';
		        	}
	        	}
	        	return data;
	        }}, 
	        {field:'isDefault',title:'Mặc định',align:'center', width:120, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	//return Utils.XSSEncode(value);
	        	if(rowData.isDefault == 'DEFAULT') {
	        		return '<input type="radio" id="isdefault" name="ckDefault" value="1" checked="checked" />';
	        	} else { // NOTDEFAULT
	        		return '<input type="radio" id="isdefault" name="ckDefault" value="0" />';
	        	}
	        }}, 
	        {field:'type',title:'Tên File',align:'center', width:460, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	if (rowData.createDate != null) {
	        		var nameUrl = formatStringUrl(rowData.url);
	        		return Utils.XSSEncode(nameUrl);
	        	}
        	}}
	    ]],	    
	    onLoadSuccess :function(data){	 
	    	$('#errMsg').html('').hide();
	    	$('.datagrid-header-rownumber').html('STT');	    		    	
	    	 updateRownumWidthForJqGrid('.easyui-dialog');
    		 $(window).resize();
	    }
	});
}
function loadReportDetail(id, shopId, typeFile){
//danh sach file goc
var w = $(window).width()-35;
var titleSave = '<a id="idSaveNPP" title="Save" href="javascript:void(0)" onclick="ReportManager.importReportFileDetail()"><img src="/resources/images/icon-save.png"  width="17" height="17"/></a>';
$('#dgReportDetail').datagrid({
	url : '/catalog/report-manager/editReport',		
	rownumbers : true,	
	//fitColumns:true,
	pagination:true,
	rowNum : 20,		
	pageList  : [10,20,50],
	scrollbarSize:0,
	width: w,
	singleSelect:true,
	//autoWidth: true,
	queryParams:{
		reportId: id,
		shopId: shopId,
		typeReport: typeFile
 	},
    columns:[[  
        {field:'createDate',title:'Ngày tải lên',align:'center', width:275,sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
        	//return Utils.XSSEncode(value);
        	var createDate = rowData.createDate;
			if(createDate == null || createDate == undefined || createDate == '' || createDate == 'null'){
				createDate = '';
			}else{
				createDate = formatDate(Utils.XSSEncode(rowData.createDate));
			}
			return createDate;
        }},
        {field:'url',title: 'File NPP' ,align:'center', width:w - (275 + 120 + 370 + 88 + 33), sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
        	var data = '';
        	if (rowData.createDate != null) {
	        	if(value == null || value == undefined || value == '' || value == 'null') {
	        		data = '<a class="linkdow" href = "' + ReportServerPath + Utils.XSSEncode(value) + '" title = "Không tìm thấy link">Download</a>';
	        	} else {
	        		data = '<a class="linkdow" href = "' + ReportServerPath + Utils.XSSEncode(value) + '" title = "' + Utils.XSSEncode(value) +'">Download</a>';
	        	}
        	}
        	return data;
        }}, 
        {field:'isDefault',title:'Mặc định',align:'center', width:120, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
        	//return Utils.XSSEncode(value);
        	if(rowData.isDefault == 'DEFAULT') {
	        	return '<input type="radio" id="isdefault'+ rowData.id +'" name="ckDefault" value="'+rowData.id+'" checked="checked" onchange="ReportManager.changeRadio('+ rowData.id+')" />';
	        } else { // NOTDEFAULT
	        	return '<input type="radio" id="isdefault'+ rowData.id +'" name="ckDefault" value="0"  onchange="ReportManager.changeRadio('+ rowData.id+')" />';
	        }
        }}, 
        {field:'type',title:'Tải File',align:'center', width:370, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
        	if (rowData.createDate != null) {
        		var nameUrl = formatStringUrl(rowData.url);
        		return Utils.XSSEncode(nameUrl);
        	}
			return '<div class="UploadFileSection Sprite1" style="width: 360px; height:25px;">' 
					+ '<input id="fakefilepc" name="nameFile" type="text" class="InputTextStyle InputText1Style" placeholder="File .jrxml" style="width: 280px;" readonly="readonly" />' 
					+ '<input type="file" tabindex="10" class="UploadFileStyle" size="1" name="reportFile" id="reportFileDetail" onchange="uploadJasperFile(this,\'importFrmReport\');" style="float: left; width: 100px;" />'
					+ '</div>';
    	}},
        {field:'action', title: titleSave, 
        	align:'center',width: 88,sortable : false,resizable : false, 
        	formatter : function(value,rowData,rowIndex) {
        		var data = "<a href='javascript:void(0)' title='Delete' onclick=\"return ReportManager.deleteReportUrl("+rowData.id+  ',' + rowIndex+");\"><img src='/resources/images/icon_delete.png' width='15' height='15'/></a>";
        		return data;
        	}
       	}
    ]],	    
    onLoadSuccess :function(data){	 
    	$('#errMsg').html('').hide();
    	$('.datagrid-header-rownumber').html('STT');	    		    	
    	updateRownumWidthForJqGrid('.easyui-dialog');
		$(window).resize();
		var flag = false;
		var data = $('#dgReportDetail').datagrid('getData').rows;
	 		for(var i = 0; i< data.length ; i++) {
	 			if(data[i].isDefault == 'DEFAULT') {
	 				ReportManager._idDefaultGoc = data[i].id;
	 				flag = true;
	 			}
	 	}
 		if(flag == false) {
	 		ReportManager._idDefaultGoc = null;
	 	}
		 $('#dgReportDetail').datagrid('appendRow',{
			 createDate: null,
			 url: '',
			 isDefault:'',
			 type: '',
			 action: ''
			});
		 showIconAddOfFirstControlGrid('saveReportDetailNPP','idSaveNPP');
    	}
	});
}
</script>
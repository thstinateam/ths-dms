<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<div id="divReportDetail">
	<s:if test="poNumber != null && !poNumber.isEmpty()">
		<h2 class="Title2Style"> Danh sách file gốc báo cáo <s:property value="poNumber" /> </h2>
	</s:if>
	<s:else>
		<h2 class="Title2Style"> Không có danh sách báo cáo  </h2>
	</s:else>
	
	<div class="SearchInSection SProduct1Form">
		<div class="GridSection">
			<div class="GeneralTable">	
				<div class="BoxGeneralTTitle">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
				       <colgroup>
				      	   <col style="width:70px;" />
				           <col style="width:90px;" />
				           <col style="width:324px;" />
				           <col style="width:125px;" />
				           <col style="width:125px;" />
				           <col style="width:190px;" />
				       </colgroup>
				       <thead>
				           <tr>
				               <th class="ColsThFirst">STT</th>
				               <th>Ngày tải lên</th>
				               <th>File NPP</th>
				               <th>Mặc định</th>
				               <th>Tải file</th>
				               <th>Tổng tiền</th>
				           </tr>
				       </thead>
				   </table>
				</div>
				<s:if test="lstPOAutoDetail != null && lstPOAutoDetail.size() > 0">
					<div class="BoxGeneralTBody">
						<div class="ScrollBodySection" id="listPoAutoDetail">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<colgroup>
			                        <col style="width:70px;" />
						           	<col style="width:90px;" />
						           	<col style="width:324px;" />
						           	<col style="width:125px;" />
						           	<col style="width:125px;" />
						           	<col style="width:190px;" />
			                    </colgroup>
								<tbody>						
									<s:iterator value="lstPOAutoDetail" status="status">
										<tr>
											<td class="ColsTd1 AlignColsCenter"><s:property value="#status.index+1" /></td>
			                                <td class="ColsTd2 AlignColsLeft"><s:property value="product.productCode"/></td>
			                                <td class="ColsTd3 AlignColsLeft"><s:property value="product.productName"/></td>
			                                <td class="ColsTd4 AlignColsRight">
			                                	<span class="currencyPOAutoDetail">
			                                		<s:property value="convertMoney(priceValue)"/>
			                                	</span>
			                                </td>	                              
			                                <td class="ColsTd5 AlignColsRight">	                                	
			                                	<s:property value="cellQuantityFormatter(quantity,convfact)"/>
			                                </td>
			                                <td class="ColsTd6 ColsTdEnd AlignColsRight">
			                                	<span class="currencyPOAutoDetail">
			                                		<s:property value="convertMoney(amount)"/>
			                                	</span>
			                                </td>	                                
			                            </tr>
									</s:iterator>						
								</tbody>
								<tfoot>
									<tr>
										<td colspan="5" class="ColsTd1 AlignColsRight" style="font-weight: bold; "> Tổng </td>
										<td class="ColsTd6 AlignColsRight" > <s:property value="convertMoney(totalAmountDetail)"/> VNĐ </td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</s:if>
				<s:else>
					<p class="NotData">Không có dữ liệu nào</p>
				</s:else>
			</div>
		</div>
<%-- 		<s:if test="lstPOAutoDetail != null && lstPOAutoDetail.size() > 0"> --%>
<!-- 			<label class="LabelStyle Label1Style" style="font-weight: bold;">Tổng tiền:&nbsp;</label> -->
<%-- 			<p class="ValueStyle Value2Style"><span class="currencyPOAutoDetail"><s:property value="convertMoney(totalAmountDetail)"/></span> VNĐ</p> --%>
<%-- 		</s:if> --%>
		<div class="Clear"></div>
		<div class="BtnCenterSection">
			<button class="BtnGeneralStyle" id="btnExportPOAutoReport" onclick="return POAutoManage.exportPOAutoReport(<s:property value="poAutoId"/>);">Xuất báo cáo so sánh với hiện tại</button>
			<img id="exportLoading" style="visibility: hidden;" src="/resources/images/loading.gif" class="LoadingStyle" />
		</div>
		<div class="Clear"></div>
		<p class="ErrorMsgStyle" id="errMsg" style="display: none;"></p>
	</div>
</div>
<input type="hidden" id="shopIdId" value="<s:property value="shop.id" />"/>

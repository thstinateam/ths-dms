<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/catalog/customer-type/info"><s:text name="catalog_sales_brand_catalogy"/></a>
		</li>
		<li><span id="title1"><s:text name="catalog_customer_type_title"/></span>
		</li>
	</ul>
</div>
<div class="CtnTwoColsSection">
    <div class="SidebarSection">
      	<h2 class="Title2Style"><s:text name="catalog_customer_type_customer_type_tree"/></h2>
      	<div class="SidebarInSection">
            <div class="ReportTreeSection">
  						<div id="tree"></div>                         
  			</div>
	    	<div class="Clear"></div>
 		</div>
	</div>
	<div class="ContentSection">
		<div class="ReportCtnSection">
			<h2 class="Title2Style" id="title"><s:text name="unit_tree.search_unit.search_header"/></h2> 
			<div class="GeneralForm SearchInSection">
				<div id="divParentCode" style="display:none;">
	              	  <label class="LabelStyle Label1Style"><s:text name="catalog_customer_type_parent_customer_type_code"/></label>
	                  <div class="BoxSelect BoxSelect2" id="parentCodeSelectBox"">
	                      <input id="parentCode">
	                  </div>
                  </div> 
				<label class="LabelStyle Label1Style"><s:text name="catalog_customer_type_customer_type_code"/><span class="ReqiureStyle"> * </span></label> 
				<input id="code" type="text" class="InputTextStyle InputText1Style" maxlength="50" style="width:196px"/>
				<label id="customertype" class="LabelStyle Label1Style" ><s:text name="catalog_customer_type_customer_type_name"/> <span class="ReqiureStyle"> * </span></label> 
				<input id="name" type="text" class="InputTextStyle InputText1Style" maxlength="100" style="width:196px"/>
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style" ><s:text name="catalog_customer_type_standard_sku"/> </label> 
				<input id="skuLevel" maxlength="19" type="text" class="InputTextStyle InputText1Style vinput-money" style="width:196px"/>
				<label class="LabelStyle Label1Style" ><s:text name="catalog_customer_type_target_ds"/></label> 
				<input id="saleAmount" maxlength="19" type="text" class="InputTextStyle InputText1Style vinput-money" style="width:196px"/>
				<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.shop_status"/> </label>
				<div class="BoxSelect BoxSelect3">
					<select id="status" class="MySelectBoxClass" style="width:206px">					
							<option value="-2" selected="selected" ><s:text name="jsp.common.status.all"/></option>
							<option value="0"><s:text name="jsp.common.status.stoped"/> </option>
							<option value="1" ><s:text name="jsp.common.status.active"/></option>
					</select>                    
		        </div>
				<div class="Clear"></div>
				<div class="BtnCenterSection" id = "groupDisplayByEdit" style="display: none;">
					<button id="btnEdit" class="BtnGeneralStyle cmsiscontrol" onclick="return CustomerTypeCatalog.save()" style="display: none"><s:text name="jsp.common.capnhat"/></button>
					<button id="btnSearch" class="BtnGeneralStyle" onclick="return CustomerTypeCatalog.search()""><s:text name="jsp.common.timkiem"/></button>
                    <img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
				</div>
				<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
                <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
			</div>
			<div id="contentGrid">
	              <h2 class="Title2Style"><s:text name="catalog_customer_type_list_customer_type"/></h2>
	                <div class="GridSection">
	                    <!--Đặt Grid tại đây-->
		                     <div class="ResultSection" id="customerTypeGrid">
			                    <p id="gridNoResult" style="display: none" class="WarningResultStyle"><s:text name="jsp.common.no.result"/></p>
			                    <table id="grid"></table>
								<div id="pager"></div>
			                </div>
	                </div>
              </div>
		</div>
	</div>
	<div class="Clear"></div>
</div>	
   
<s:hidden id="selId" value="0"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#code').focus();	
	$('#name').val('');	
	$('#name').removeAttr('disabled');
	$('#skuLevel').removeAttr('disabled');
	$('#saleAmount').removeAttr('disabled');
	$('.ReqiureStyle').hide();
	Utils.bindFormatOnTextfield('code', Utils._CODE);
	Utils.bindFormatOnTextfield('saleAmount', Utils._TF_NUMBER);
	Utils.formatCurrencyFor('saleAmount');
	
	Utils.bindFormatOnTextfield('skuLevel', Utils._TF_NUMBER);
	Utils.formatCurrencyFor('skuLevel');
	
	
	$("#grid").datagrid({
	  url:CustomerTypeCatalog.getGridUrl('',$('#parentCode').val(),'',$('#status').val(),0,'',''),
	  pageList  : [10,20,30],
	  width: 910,
	  height:'auto',
	  scrollbarSize : 0,
	  pagination:true,
	  fitColumns:true,
	  singleSelect  :true,
	  method : 'GET',
	  rownumbers: true,
	  width: ($('#customerTypeGrid').width()),
	  columns:[[	
		{field: 'parentChannelType', title: catalog_customer_type_parent_customer_type_code, sortable:true,resizable:true, align: 'left', formatter: GridFormatterUtils.parentChannelTypeCellFormatter},        
	    {field: 'channelTypeCode',index:'channelTypeCode', title: catalog_customer_type_customer_type_code, width: 120, sortable:true,resizable:true, align: 'left', 
			formatter:function(data, row, index){
				if(row.channelTypeCode!=undefined && row.channelTypeCode.length>0){
					return Utils.XSSEncode(row.channelTypeCode);
				}return '';
	    	}
		},
	    {field: 'channelTypeName',index:'channelTypeName', title: catalog_customer_type_customer_type_name, width: 150,sortable:true,resizable:true, align: 'left',
			formatter:function(data, row, index){
				if(row.channelTypeName!=undefined && row.channelTypeName.length>0){
					return Utils.XSSEncode(row.channelTypeName);
				}return '';
	    	}
		},
	    {field: 'sku',index:'sku', title: catalog_customer_type_standard_sku, width: 150, sortable:true,resizable:true, align: 'right',
	    	formatter:function(data, row, index){
	    		return formatCurrency(row.sku);
	    	}	    	
	    },
	    {field: 'saleAmount',index:'saleAmount', title: catalog_customer_type_target_ds, width: 150, sortable:true,resizable:true, align: 'right',
	    	formatter:function(data, row, index){
	    		return formatCurrency(row.saleAmount);
	    	}
	   	},
	    {field: 'status', index:'status',title: jsp_common_status, width: 100, align: 'left', sortable:true,resizable:true, formatter: GridFormatterUtils.statusCellIconFormatter},
	    {field: 'edit', title: catalog_customer_type_edit, width: 50, align: 'center',sortable:false,resizable:true, formatter: CustomerTypeCatalogFormatter.editCellIconFormatter},
	    {field: 'parentChannelType.channelTypeCode', index: 'parentChannelTypeCode', hidden: true},
	    {field: 'id', index: 'id', hidden: true},
	    {field: 'parentChannelType.id', index: 'parentChannelType.id', hidden: true}	   
	  ]],	  
	  onLoadSuccess:function(){
			$('.datagrid-header-rownumber').html(unit_tree_search_unit_search_grid_no); //.html('STT');  
	      	$('#grid').datagrid('resize');
	      	
	      	var arrEdit = $('#customerTypeGrid td[field="edit"]');
			if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
			  for (var i = 0, size = arrEdit.length; i < size; i++) {
			  	$(arrEdit[i]).prop("id", "btnEdit_gr_td_edit_" + i).addClass("cmsiscontrol");
			  }
			}
			Utils.functionAccessFillControl('customerTypeGrid', function(data){
				//Xu ly cac su kien lien quan den cac control phan quyen
				var arrTmpLength =  $('#customerTypeGrid td[id^="btnEdit_gr_td_edit_"]').length;
				var invisibleLenght = $('.isCMSInvisible[id^="btnEdit_gr_td_edit_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#grid').datagrid("showColumn", "edit");
				} else {
					$('#grid').datagrid("hideColumn", "edit");
				}
			});
	      }
	});
	CustomerTypeCatalog.loadTree();
	$('#parentCode').combotree({url: '/rest/catalog/customer-type/combotree/0/0.json',width:113,
	onShowPanel:function(){
		$('.combo-panel').css('width','204px');
		$('.combo-p').css('width','205px');
	}}
	);
	$('#parentCode').combotree('setValue',activeType.ALL);
	CustomerTypeCatalog.clearData();
	Utils.bindAutoButtonEx('.ContentSection','btnEdit');
	$('#status').css('width','206px');
	$('#status').next().css('width','173px');
	$('#groupDisplayByEdit').show();
});

</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
   	<ul class="ResetList FixFloat BreadcrumbList">
       <li class="Sprite1"><a href="/catalog/staff-type">Danh mục</a></li>
       <li><span id="title1">Danh sách loại nhân viên</span></li>
    </ul>
</div>
<div class="CtnTwoColsSection">
    <div class="SidebarSection">
      	<h2 class="Title2Style">Cây loại nhân viên</h2>
      	<div class="SidebarInSection" >
            <div class="ReportTreeSection">
              		<!-- <div id="treeContainer" class="ScrollSection">    -->
  						<div id="tree"></div>  <!-- class="demo" -->                        
  				<!-- 	</div> -->
  			</div>
	  		<div class="BtnCenterSection">
		        <button id="btnAdd1" class="BtnGeneralStyle" onclick="return StaffTypeCatalog.newCusType();">+ Thêm mới</button>
		    </div>
	    	<div class="Clear"></div>
 		</div>
	</div>
	<div class="ContentSection">
      	<div class="ReportCtnSection">
          	<h2 class="Title2Style" id="title">Thông tin tìm kiếm</h2>
              <div class="GeneralForm SearchInSection">
              	  <div id="divParentCode" style="display:none;">
	              	  <label class="LabelStyle Label2Style">Mã cha</label>
	                  <div class="BoxSelect BoxSelect6 BoxDisSelect" id="parentCodeSelectBox">
	                      <input id="parentCode">
	                  </div>
                  </div>
                  <label class="LabelStyle Label2Style">Mã loại</label>
                  <input id="staffTypeCode" type="text" class="InputTextStyle InputText3Style" maxlength="20"/>
                  <label class="LabelStyle LabelPLStyle"><span class="ReqiureStyle">(*)</span></label>
                  <label class="LabelStyle Label2Style">Tên loại</label>
                  <input id="staffTypeName" type="text" class="InputTextStyle InputText3Style" maxlength="100"/>
                  <label class="LabelStyle LabelPLStyle"><span class="ReqiureStyle">(*)</span></label>
                  <div class="Clear"></div>
              	<label class="LabelStyle Label2Style">Phân loại</label>
                  <div class="BoxSelect BoxSelect6">
                     <s:select id="staffObjectType" list="lstStaffObjectType" listKey="apParamCode" listValue="apParamName"
										 headerKey="-1" headerValue="Tất cả" cssClass="MySelectBoxClass" >
					 </s:select>
                  </div>
                  <label class="LabelStyle LabelPLStyle"><span class="ReqiureStyle">(*)</span></label>
                  <label class="LabelStyle Label2Style">Trạng thái</label>
                  <div class="BoxSelect BoxSelect6">
                      <select id="status" class="MySelectBoxClass" name="LevelSchool">
                          <option value="-2">Tất cả</option>
                          <option value="1" selected="selected">Hoạt động</option>
                          <option value="0">Tạm ngưng</option>
                      </select>
                  </div>
                  <label class="LabelStyle LabelPLStyle"><span class="ReqiureStyle">(*)</span></label>
                  <div class="Clear"></div>
                  
                  <div class="BtnCenterSection">
                      <button style="display: none;" id="btnUpdate" class="BtnGeneralStyle"  onclick="return StaffTypeCatalog.change();">Cập nhật</button>
                      <button id="btnSearch" class="BtnGeneralStyle" onclick="return StaffTypeCatalog.search();">Tìm kiếm</button>
                      <img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                  </div>
                  <p id="errMsg" style="display: none;" class="ErrorMsgStyle">Có lỗi xảy ra khi cập nhật dữ liệu</p>
 			      <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
              </div>
              <div id="contentGrid">
	              <h2 class="Title2Style">Danh sách loại nhân viên</h2>
	                <div class="GridSection">
	                    <!--Đặt Grid tại đây-->
<!-- 	                    <div class="GeneralMilkInBox"> -->
		                     <div class="ResultSection" id="staffTypeGrid">
			                    <p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
			                    <table id="grid"></table>
								<div id="pager"></div>
			                </div>
<!-- 			            </div> -->
	                </div>
              </div>
          </div>
      </div>
      <div class="Clear"></div>
</div>
<!-- <div class="MainInContent"> -->
<!-- 	<div class="MainInTopContent"> -->
<!-- 	   	<div class="MainInBtmContent"> -->
<!-- 	           <div class="FirstSidebar"> -->
<!-- 	     	<div class="TreeViewSection"> -->
<%-- 	          <h3 class="Sprite2"><span class="Sprite2">CÂY LOẠI NHÂN VIÊN</span></h3> --%>
<!-- 	         <div class="GeneralMilkInBox"> -->
<!-- 	         	<div id="treeContainer" class="ScrollSection">    -->
<!-- 	         		<div id="tree"></div>                          -->
<!-- 	         	</div>         -->
<!-- 	         </div> -->
<%-- 	         <button id="btnAdd1" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return StaffTypeCatalog.newCusType();"><span class="Sprite2">+ Thêm mới</span></button> --%>
<!-- 	     </div> -->
<!-- 	 </div> -->
<!-- 	 <div class="Content"> -->
<!-- 	     <div class="GeneralMilkBox"> -->
<!-- 	         <div class="GeneralMilkTopBox"> -->
<!-- 	             <div class="GeneralMilkBtmBox"> -->
<%-- 	                 <h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin tìm kiếm</span></h3> --%>
<!-- 	                 <div class="GeneralMilkInBox ResearchSection"> -->
<!-- 		                 <div class="ModuleList17Form"> -->
<!--                              <label class="LabelStyle Label1Style">Loại nhân viên</label> -->
<!--                              <div class="Field2"> -->
<!-- 	                             <input id="staffTypeCode" type="text" class="InputTextStyle InputText1Style" maxlength="20" /> -->
<%-- 	                             <span class="RequireStyle">(*)</span> --%>
<!--                              </div> -->
<!--                              <label class="LabelStyle Label2Style">Mã loại cha</label> -->
<!--                              <div class="BoxSelect BoxSelect1" id="parentCodeSelectBox"> -->
<!--                                  <input id="parentCode" style="width:200px;"> -->
<!--                              </div> -->
<!--                              <div class="Clear"></div> -->
<!--                              <label class="LabelStyle Label1Style">Tên loại</label> -->
<!--                              <div class="Field2"> -->
<!-- 			                     <input id="staffTypeName" type="text" class="InputTextStyle InputText1Style" maxlength="100"/> -->
<%-- 			                     <span class="RequireStyle">(*)</span> --%>
<!-- 		                     </div> -->
<!--                              <label class="LabelStyle Label2Style">Trạng thái</label> -->
<!-- 		                     <div class="BoxSelect BoxSelect2"> -->
<!-- 			                     <div class="Field2"> -->
<%-- 			                     	<select id="status" class="MySelectBoxClass"> --%>
<!-- 				                     	<option value="-2">---Chọn trạng thái---</option> -->
<!-- 				                     	<option value="0">Tạm ngưng</option> -->
<!-- 				                     	<option value="1" selected="selected">Hoạt động</option> -->
<%-- 			                     	</select> --%>
<%-- 			                         <span class="RequireStyle">(*)</span> --%>
<!-- 			                     </div> -->
<!-- 		                     </div> -->
<!-- 		                     <div class="Clear"></div> -->
<!-- 		                      <label class="LabelStyle Label1Style">Phân loại</label> -->
<!--                              <div class="BoxSelect BoxSelect1"> -->
<!--                              	<div class="Field2"> -->
<%-- 	                                 <s:select id="staffObjectType" list="lstStaffObjectType" listKey="apParamCode" listValue="apParamName" --%>
<!-- 										 headerKey="-1" headerValue="---Chọn mã phân loại---" cssClass="MySelectBoxClass" > -->
<%-- 					           		</s:select> --%>
<%-- 					           		<span class="RequireStyle">(*)</span> --%>
<!-- 			                     </div> -->
<!--                              </div> -->
<!--                              <div class="Clear"></div> -->
<!--                              <div class="ButtonSection"> -->
<%-- 		                     	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return StaffTypeCatalog.search();"><span class="Sprite2">Tìm kiếm</span></button> --%>
<%-- 			                	<button id="btnCreate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return Utils.getChangedForm(null,true,-1);"><span class="Sprite2">Thêm mới</span></button> --%>
<%-- 			                	<button style="display: none;" id="btnUpdate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return StaffTypeCatalog.change();"><span class="Sprite2">Cập nhật</span></button> --%>
<%-- 			                	<button style="display: none;" id="btnDismiss" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return StaffTypeCatalog.resetForm();"><span class="Sprite2">Bỏ qua</span></button> --%>
<!-- 			                	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif"> -->
<!-- 			                 </div> -->
<!-- 			                 <p id="errMsg" style="display: none;" class="ErrorMsgStyle Sprite1">Có lỗi xảy ra khi cập nhật dữ liệu</p> -->
<%-- 			                 <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" /> --%>
<!--                          </div> -->
<!-- 	                 </div> -->
<!-- 	             </div> -->
<!-- 	         </div> -->
<!-- 	     </div> -->
<!-- 	     <div class="GeneralMilkBox"> -->
<!-- 	         <div class="GeneralMilkTopBox"> -->
<!-- 	             <div id="contentGrid" class="GeneralMilkBtmBox"> -->
<%-- 	                 <h3 class="Sprite2"><span class="Sprite2">Danh sách loại nhân viên</span></h3> --%>
<!-- 	                 <div class="GeneralMilkInBox"> -->
<!-- 	                     <div class="ResultSection" id="staffTypeGrid"> -->
<!-- 		                    <p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p> -->
<!-- 		                    <table id="grid"></table> -->
<!-- 							<div id="pager"></div> -->
<!-- 		                </div> -->
<!--                      </div> -->
<!--                  </div> -->
<!--              </div> -->
<!--          </div> -->
<!--          </div> -->
<!--          <div class="Clear"></div> -->
<!-- 		</div> -->
<!--    </div> -->
<!-- </div>     -->
<s:hidden id="staffTypeId" value="0"></s:hidden>
<s:hidden id="selObjType" value="-1"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
// 	$('#layoutContent').removeClass('Content');
// 	$('#layoutContent').addClass('MainContent');
// 	$('.MySelectBoxClass').customStyle();
	$('#staffTypeCode').focus();
	$('.ReqiureStyle').hide();
	$("#grid").datagrid({
	  url:StaffTypeCatalog.getGridUrl('',-1,'',1,-1,false),
	  pageList  : [10,20,30],
	  width: 910,
	  height:'auto',
	  scrollbarSize : 0,
	  pagination:true,
	  fitColumns:true,
	  method : 'GET',
	  rownumbers: true,	  
	  width: ($('#staffTypeGrid').width()),
	  columns:[[		
	    {field:'channelTypeCode', title: 'Mã loại', width: 200, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
	    	return Utils.XSSEncode(value);
		}},
	    {field:'channelTypeName', title: 'Tên loại', width: 200, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index){
	    	return Utils.XSSEncode(value);
		}},
	    {field:'objectType', title: 'Phân loại', width: 100, sortable:false,resizable:false , align: 'left',formatter: GridFormatterUtils.staffObjectTypeCellFormatter},
	    {field:'parentChannelType', title: 'Mã cha', width: 100, align: 'left', sortable:false,resizable:false, formatter: GridFormatterUtils.parentChannelTypeCellFormatter },
	    {field: 'status', index:'status',title: 'Trạng thái', width: 100, align: 'left', sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter},
	    {field:'edit', title: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: StaffTypeCatalogFormatter.editCellIconFormatter},
	  ]],
	  onLoadSuccess:function(){
		$('.datagrid-header-rownumber').html('STT');  
      	$('#grid').datagrid('resize');
      }
	});
 	StaffTypeCatalog.loadTree();
	$('#parentCode').combotree({url: '/rest/catalog/staff-type/combotree/0/0.json',width:113,
		formatter: function(node) {
			return Utils.XSSEncode(node.text);
		},
		onShowPanel:function(){
			$('.combo-panel').css('width','204px');
			$('.combo-p').css('width','205px');
		}});
	$('#parentCode').combotree('setValue',activeType.ALL);
});
</script>
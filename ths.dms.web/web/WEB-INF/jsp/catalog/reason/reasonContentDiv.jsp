<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="Breadcrumbs">
    <span class="Breadcrumbs1Item Sprite1">Lý do</span>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
			<h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin tìm kiếm</span></h3>
            <div class="GeneralMilkInBox ResearchSection">
            	<div class="ModuleList3Form">
                <label class="LabelStyle Label1Style">Mã lý do</label>
                <div class="Field2">
                	<input id="reasonCode" type="text" class="InputTextStyle InputText1Style" maxlength="10">
                	<span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label2Style">Tên lý do</label>
                <div class="Field2">
                	<input id="reasonName" type="text" class="InputTextStyle InputText1Style" maxlength="50">
                	<span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label3Style">Nhóm lý do</label>
                 <div class="Field2">
	                <div class="BoxSelect BoxSelect1">
	                    <select id="reasonGroup" class="MySelectBoxClass">
							<option value="-2">---Chọn nhóm lý do---</option>
							<s:iterator value="lstReasonGroup">					
								<option value='<s:property value="id"/>'><s:property value="reasonGroupCode"/> - <s:property value="reasonGroupName"/></option>
							</s:iterator>		    
					    </select>
	                </div>
	                <span class="RequireStyle">(*)</span>
                </div>
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style">Ghi chú</label>
                <input id="description" type="text" class="InputTextStyle InputText1Style" maxlength="100">                
                <label class="LabelStyle Label2Style">Trạng thái</label>
                <div class="Field2">
	                <div class="BoxSelect BoxSelect1">
	                	<s:select id="status" headerKey="-2" headerValue="-- Chọn trạng thái --"  name="status" cssClass="MySelectBoxClass" list="lstStatusBean" listKey="value" listValue="name"></s:select>                    
	                </div>
	                <span class="RequireStyle">(*)</span>
                </div>
                <div class="Clear"></div>
                <div class="ButtonSection">
                	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return ReasonCatalog.searchReason()"><span class="Sprite2">Tìm kiếm</span></button>
                	<button id="btnAdd" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ReasonCatalog.getChangedForm();"><span class="Sprite2">Thêm mới</span></button>
                	<button id="btnEdit" class="BtnGeneralStyle Sprite2" onclick="return ReasonCatalog.saveReason()" style="display: none"><span class="Sprite2">Cập nhật</span></button>
                	<button id="btnCancel" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ReasonCatalog.resetForm();" style="display: none"><span class="Sprite2">Bỏ qua</span></button>
                	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                </div>
                <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none">Có lỗi xảy ra khi cập nhật dữ liệu</p>
                <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
               </div>
            </div>
		</div>
    </div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Danh sách lý do</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="reasonGrid">
                    <p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
                    <table id="grid"></table>
					<div id="pager"></div>
                </div>
            </div>
		</div>
    </div>
</div>
<div class="FuncBtmSection">
	<div class="DivInputFileSection">			
			<p class="ExcelLink">
				<a id="downloadTemplate" href="javascript:void(0)">Tải mẫu file excel</a>
			</p>		
				<div class="DivInputFile">
					<form action="/catalog/reason/import-excel" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
			            <input type="file" class="InputFileStyle" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
			            <input type="hidden" id="isView" name="isView">
			            <div class="FakeInputFile">
			                <input id="fakefilepc" type="text" class="InputTextStyle" readonly="readonly">
			            </div>
	 				</form>
		        </div>
		        <button id="btnView" class="BtnGeneralStyle Sprite2" onclick="return CarTypeCatalog.viewExcel();"><span class="Sprite2">Xem</span></button>
    			<button id="btnImport" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return CarTypeCatalog.importExcel();"><span class="Sprite2">Nhập từ file</span></button>	  
		    <div class="Clear"></div>
	</div>
</div>
<div class="FuncLeftBtmSection">
   	<button class="BtnGeneralStyle Sprite2" onclick="ReasonCatalog.exportExcelData();"><span class="Sprite2">Xuất dữ liệu</span></button>
</div>
<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
<div id="responseDiv" style="display: none"></div>
<s:hidden id="selReasonId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	$('.RequireStyle').hide();
	$('#reasonCode').focus();
	$("#grid").jqGrid({
	  url:ReasonCatalog.getGridUrl('','',$('#reasonGroup').val(),'',$('#status').val()),
	  colModel:[		
	    {name:'reasonCode',index:'reasonCode', label: 'Mã lý do', width: 100, sortable:false,resizable:false , align: 'left'},
	    {name:'reasonName', index:'reasonName',label: 'Tên lý do', sortable:false,resizable:false, align:'left' },
	    {name:'reasonGroup.reasonGroupName',index:'reasonGroup.reasonGroupName', label: 'Tên nhóm lý do', sortable:false,resizable:false, align:'left' },
	    {name:'description',index:'description', label: 'Ghi chú', sortable:false,resizable:false, align:'left' },
	    {name:'status', index:'status',label: 'Trạng thái', width: 80, align: 'left', sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter},
	    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ReasonCatalogFormatter.editCellIconFormatter},
	    {name:'delete', label: 'Xóa', width: 50, align: 'center', sortable:false,resizable:false, formatter: ReasonCatalogFormatter.delCellIconFormatter},
	    {name:'id', index:'id',hidden: true},
	    {name:'reasonGroup.id', index:'reasonGroup.id',hidden: true},
	    {name:'reasonGroup.reasonGroupCode', index:'reasonGroup.reasonGroupCode',hidden: true},
	  ],	  
	  pager : '#pager',
	  width: ($('#reasonGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});	
	ReasonCatalog._htmlReasonGroup = $('#reasonGroup').html();
});
$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_lydo_import.xls');
var options = { 
 		beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
 		success:      ProductLevelCatalog.afterImportExcelUpdate,  
 		type: "POST",
 		dataType: 'html'   
 	}; 
$('#importFrm').ajaxForm(options);
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<s:if test="lstAttribute != null && lstAttribute.size() > 0">
<s:set id="numCombo" value="0"></s:set>
<s:set id="runIndex" value="0"></s:set>
	<k:repeater value="lstAttribute" status="status">
	<k:itemTemplate>
		<label class="LabelStyle Label1Style"><s:property value="attributeName"/></label>
		<s:if test="valueType != null && (valueType.value == 1 || valueType.value == 2 || valueType.value == 3)"><!-- tu nhap -->
			<s:if test="valueType != null && valueType.value == 2"><!-- kieu so -->
				<input value='<s:property value="lstObjectValue.get(#runIndex).get(0)"/>' id="attributeCode_<s:property value="#status.index+1"/>" type="text" class="InputTextStyle Attribute InputText1Style" maxlength="20" />
				<script type="text/javascript" charset="UTF-8">
					Utils.bindFormatOnTextfield('attributeCode_'+'<s:property value="#status.index+1"/>', Utils._TF_NUMBER);
				</script>
			</s:if>
			<s:elseif test="valueType != null && valueType.value == 1"><!-- kieu chu -->
				<input value='<s:property value="lstObjectValue.get(#runIndex).get(0)"/>' id="attributeCode_<s:property value="#status.index+1"/>" type="text" class="InputTextStyle Attribute InputText1Style" maxlength="250" />
			</s:elseif>
			<s:elseif test="valueType != null && valueType.value == 3"><!-- kieu ngay -->
				<input value='<s:property value="lstObjectValue.get(#runIndex).get(0)"/>' id="attributeCode_<s:property value="#status.index+1"/>" type="text" class="InputTextStyle Attribute InputText6Style"/>
				<script type="text/javascript" charset="UTF-8">
					applyDateTimePicker('#attributeCode_'+'<s:property value="#status.index+1"/>');
				</script>
			</s:elseif>
		</s:if>
		<s:elseif test="valueType != null && valueType.value == 4"><!-- select 1 choice -->
			<div class="BoxSelect BoxSelect2">
				<select id="attributeCode_<s:property value="#status.index+1"/>" class="MySelectBoxClas easyui-combobox Attribute HasClassSelected" data-options="panelWidth : '206'"> 
					<option value="-1">---Chọn giá trị---</option>					            		
					<s:iterator id="child" value="lstObjectAttributeDetail.get(#numCombo)">
						<s:if test="lstObjectValue.get(#runIndex) != null && lstObjectValue.get(#runIndex).size() > 0">
							<s:iterator id="valueChild" value="lstObjectValue.get(#runIndex)">
								<s:if test="#child.id.toString().equals(#valueChild)">
									<option selected="selected" value='<s:property value="#child.id"/>'><s:property value="#child.code"/> - <s:property value="#child.name"/></option>
								</s:if>
								<s:else>
									<option value='<s:property value="#child.id"/>'><s:property value="#child.code"/> - <s:property value="#child.name"/></option>
								</s:else>
							</s:iterator>						
						</s:if>
						<s:else>
							<option value='<s:property value="#child.id"/>'><s:property value="#child.code"/> - <s:property value="#child.name"/></option>
						</s:else>
					</s:iterator>
				</select>
			</div>
			<s:set id="numCombo" value="#numCombo+1"></s:set> 
			<script type="text/javascript">
					$('#attributeCode_'+<s:property value="#status.index+1"/>).combobox({						
						valueField : "id",
						textField : "text",
						mode:'local',
						filter: function(q, row){
							var opts = $(this).combobox('options');
							return row[opts.valueField].indexOf(q.toUpperCase().trim())==0;
						}
					});
			</script> 
		</s:elseif>
		<s:elseif test="valueType != null && valueType.value == 5"><!-- select multi choice -->		            			
			<div class="BoxSelect BoxSelect2">
				<select multiple="multiple" id="attributeCode_<s:property value="#status.index+1"/>" class="MySelectBoxClas Attribute HasClassSelected">
				<s:iterator id="child" value="lstObjectAttributeDetail.get(#numCombo)">
					<option id="multiselect-<s:property value='#child.id'/>" value='<s:property value="#child.id"/>'><s:property value="#child.code"/> - <s:property value="#child.name"/></option>
				</s:iterator>
				</select>
				<s:if test="lstObjectValue.get(#runIndex) != null && lstObjectValue.get(#runIndex).size() > 0">
				<script type="text/javascript">
					var list<s:property value="#numCombo"/> = new Array();
					<s:iterator id="valueChild" value="lstObjectValue.get(#runIndex)">
						var value = '<s:property value="#valueChild"/>';						
						$('#multiselect-'+value).attr('selected', 'selected');
					</s:iterator>
				</script>
				</s:if>
				<script type="text/javascript">
					$('#attributeCode_'+<s:property value="#status.index+1"/>).multiselect({
						selectedText: function(numChecked, numTotal, checkedItems){
							var max = 2;
							if($(checkedItems[0]).attr('title').trim().length <= 10) {
								max = 3;
							} else if($(checkedItems[0]).attr('title').trim().length > 30){
								max = 1;
							}
							if(checkedItems.length <= max) {
								var resultStr = $(checkedItems[0]).attr('title').trim();
								for(var i = 1; i < checkedItems.length; i++) {
									var obj = checkedItems[i];
									resultStr += ', ' + $(obj).attr('title');
								}
								return resultStr;
							} else {
								return 'Đã chọn '+numChecked+'/'+numTotal;								
							}
						}
					});
					$('.ui-multiselect-menu').each(function(){
						$(this).css('width','196px');
					});
				</script> 
			</div>
			<s:set id="numCombo" value="#numCombo+1"></s:set>
		</s:elseif>
		<s:hidden id="attributeId_%{#status.index+1}" name="id"></s:hidden>		
		<s:hidden id="attributeColumnValueType_%{#status.index+1}" name="valueType.value"></s:hidden>
		<s:set id="runIndex" value="#runIndex+1"></s:set>
		<s:if test="#runIndex % 3 == 0">
			<div class="Clear"></div>
		</s:if>
	</k:itemTemplate>
	</k:repeater>
</s:if>
<div class="Clear"></div>
<s:hidden id="lstAttributeSize" name="lstAttribute.size()"></s:hidden>
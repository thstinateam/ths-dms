<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="GeneralMilkInBox ResearchSection">
  	<div class="GeneralMilkBox GeneralMilkExtInBox NoneMarginBox">
          <div class="GeneralMilkTopBox">
              <div class="GeneralMilkBtmBox">
  				<h3 class="Sprite2"><span class="Sprite2" id="titleAttr">Thông tin tìm kiếm</span></h3>
                  <div class="GeneralMilkInBox ResearchSection">
                      <div class="ModuleList183Form" id="searchForm">
                          <label class="LabelStyle Label1Style">Mã thuộc tính</label>
                          <input id="attributeCode" type="text" class="InputTextStyle InputText1Style" />
                          <label class="LabelStyle Label2Style">Tên thuộc tính</label>
                          <input id="attributeName" type="text" class="InputTextStyle InputText1Style"/>
                          <label class="LabelStyle Label3Style">Loại dữ liệu</label>
                          <div class="BoxSelect BoxSelect1">
                          	<s:select id="columnType" headerKey="-2" headerValue="-- Chọn loại dữ liệu --"  name="columnType" cssClass="MySelectBoxClass" list="lstColumnType" listKey="value" listValue="name"></s:select>
                          </div>
                          <div class="Clear"></div>
                          <label class="LabelStyle Label1Style">Trạng thái</label>
                          <div class="BoxSelect BoxSelect1">
                              <s:select id="attributeStatus" headerKey="-2" headerValue="-- Chọn trạng thái --"  name="status" cssClass="MySelectBoxClass" list="lstStatusBean" listKey="value" listValue="name"></s:select>
                          </div>
                          <div class="Clear"></div>
                          <div class="ButtonSection">
                          	<button class="BtnGeneralStyle Sprite2 BtnSearch" onclick="AttributesManager.searchAttribute();"><span class="Sprite2">Tìm kiếm</span></button>
                          	<button class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="AttributesManager.showNewOrUpdateForm()"><span class="Sprite2">Thêm mới</span></button>
                          </div>
                      </div>
                      <div class="ModuleList183Form" id="createForm" style="display: none">
                         <label class="LabelStyle Label1Style">Mã thuộc tính</label>
                         <div class="Field2">
                         		<input id="attributeCode" type="text" class="InputTextStyle InputText1Style" maxlength="40">
                         		<span class="RequireStyle">(*)</span>
                         </div>
                         <label class="LabelStyle Label2Style">Tên thuộc tính</label>
                         <div class="Field2">
                         		<input type="text" class="InputTextStyle InputText1Style" id="attributeName" maxlength="250">
                         		<span class="RequireStyle">(*)</span>
                         </div>
                         <label class="LabelStyle Label3Style">Loại dữ liệu</label>
                         <div class="Field2">
                         		<div class="BoxSelect BoxSelect1 disable">
		                             <s:select id="columnType" headerKey="-2" headerValue="-- Chọn loại dữ liệu --"  
		                             name="columnType" cssClass="MySelectBoxClass" 
		                             list="lstColumnType" listKey="value" listValue="name" 
		                             onchange="AttributesManager.getListColumnName($(this).val(),'columnName',-2);"></s:select>
		                         </div>
                         		<span class="RequireStyle">(*)</span>
                         </div>
                         <div class="Clear"></div>
                         <label class="LabelStyle Label1Style">Tên trường</label>
                         <div class="Field2">
	                         <div class="BoxSelect BoxSelect1 disable">
	                             <s:select id="columnName" headerKey="-2" headerValue="-- Chọn tên trường --"  name="columnName" cssClass="MySelectBoxClass" list="lstColumnName" listKey="name" listValue="name"></s:select>
	                         </div>
	                         <span class="RequireStyle">(*)</span>
                         </div>
                         <label class="LabelStyle Label2Style">Kiểu dữ liệu</label>
                         <div class="Field2">
	                         <div class="BoxSelect BoxSelect1">
	                             <s:select id="columnValueType" headerKey="-2" headerValue="-- Chọn loại dữ liệu --"  name="columnValueType" cssClass="MySelectBoxClass" list="lstColumnValueType" listKey="value" listValue="name"></s:select>
	                         </div>
	                         <span class="RequireStyle">(*)</span>
                         </div>
                         <label class="LabelStyle Label3Style">Trạng thái</label>
                         <div class="Field2">
	                         <div class="BoxSelect BoxSelect1">
	                             <s:select id="attributeStatus" headerKey="-2" headerValue="-- Chọn trạng thái --"  name="status" cssClass="MySelectBoxClass" list="lstStatusBean" listKey="value" listValue="name"></s:select>
	                         </div>
	                         <span class="RequireStyle">(*)</span>
                         </div>
                         <div class="Clear"></div>
                         <label class="LabelStyle Label1Style">Mô tả</label>                         
                         <input id="note" type="text" class="InputTextStyle InputText1Style" maxlength="250">                         
                         <div class="Clear"></div>
                         <div class="ButtonSection">
                         	<button class="BtnGeneralStyle Sprite2" onclick="AttributesManager.saveOrUpdate();"><span class="Sprite2">Lưu lại</span></button>
                         	<button class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="AttributesManager.showSearchForm()"><span class="Sprite2">Bỏ qua</span></button>
                         </div>
                     </div>
                     <p class="ErrorMsgStyle Sprite1" id="errMsgAttr" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
	               	 <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                  </div>
			</div>
	</div>
  </div>
   <div class="GeneralMilkBox GeneralMilkExtInBox">
       <div class="GeneralMilkTopBox">
           <div class="GeneralMilkBtmBox">
               <h3 class="Sprite2"><span class="Sprite2">Danh sách thuộc tính</span></h3>
               <div class="GeneralMilkInBox">
                   <div class="ResultSection" id="attributeContainer">
                        <table id="attributeGrid"></table>
						<div id="attributePager"></div>
                   </div>
               </div>
           </div>
       </div>
   </div>
    <div class="GeneralMilkBox GeneralMilkExtInBox">
        <div class="GeneralMilkTopBox">
            <div class="GeneralMilkBtmBox">
                <h3 class="Sprite2"><span class="Sprite2">Quản lý dữ liệu thuộc tính</span></h3>
                <div class="GeneralMilkInBox">
                    <div class="ResultSection">
                        <div class="GeneralMilkCol9">
                            <div class="GeneralTable Table43Section">
                                <div class="BoxGeneralTTitle">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <colgroup>
                                        	<col style="width:40px;" />
                                            <col style="width:100px;" />
                                            <col style="width:165px;" />
                                            <col style="width:40px;" />
                                        </colgroup>
                                        <thead>
                                            <tr>
                                            	<th class="ColsThFirst">STT</th>
                                                <th class="ColsThFirst">Mã thuộc tính</th>
                                                <th>Tên thuộc tính</th>
                                                <th class="ColsThEnd">&nbsp;</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="BoxGeneralTBody">
                                    <div class="ScrollBodySection" id="srlLstAttr">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <colgroup>
                                                <col style="width:40px;" />
	                                            <col style="width:100px;" />
	                                            <col style="width:165px;" />
	                                            <col style="width:40px;" />
                                            </colgroup>
                                            <tbody id="listAttr">                   	
                                            	<s:iterator value="lstAttribute" id="attr" status="status">
                                            		<tr id='tr_attr_<s:property value="#attr.id"/>'>
                                            			<td class="ColsTd1"><s:property value="#status.index+1"/></td>
	                                                    <td class="ColsTd1"><s:property value="#attr.attributeCode"/></td>
	                                                    <td class="ColsTd2ColsAlignLeft"><s:property value="#attr.attributeName"/></td>
	                                                    <td class="ColsTd3 ColsTdEnd"><input id="chk_attr_<s:property value="#attr.id"/>" class="chkAttribute" type="checkbox" /></td>                                                                  
	                                                </tr>
                                            	</s:iterator>                                                                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="GeneralMilkCol10">
                        	<div class="GeneralMilkBox">
                                <div class="GeneralMilkTopBox">
                                    <div class="GeneralMilkBtmBox">
                                        <h3 class="Sprite2"><span class="Sprite2">Xuất dữ liệu thuộc tính</span></h3>
                                        <div class="GeneralMilkInBox">
                                            <div class="ButtonSection">Xuất dữ liệu từ những thuộc tính đã chọn <button class="BtnGeneralStyle Sprite2" onclick="AttributesManager.exportAttribute(1)"><span class="Sprite2">Xuất dữ liệu</span></button></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        	<div class="GeneralMilkBox MarginTopBox">
                                <div class="GeneralMilkTopBox">
                                    <div class="GeneralMilkBtmBox">
                                        <h3 class="Sprite2"><span class="Sprite2">Xóa dữ liệu thuộc tính</span></h3>
                                        <div class="GeneralMilkInBox">
                                            <div class="ButtonSection">Xóa dữ liệu từ những thuộc tính đã chọn <button class="BtnGeneralStyle Sprite2" onclick="AttributesManager.deleteAttribute()"><span class="Sprite2">Xoá dữ liệu</span></button></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="GeneralMilkBox MarginTopBox">
                                <div class="GeneralMilkTopBox">
                                    <div class="GeneralMilkBtmBox">
                                        <h3 class="Sprite2"><span class="Sprite2">Nhập dữ liệu thuộc tính</span></h3>
                                        <div class="GeneralMilkInBox">
                                            <div class="FuncBtmSection">
                                                <div class="DivInputFileSection">
                                                    <p class="ExcelLink"><a href="javascript:void(0)" onclick="AttributesManager.exportAttribute(0)">Tải mẫu file excel</a></p>
                                                    <div class="DivInputFile" style="width: 238px">
                                                    	<form action="/attributes/import-excel" name="importAttrFrm" id="importAttrFrm"  method="post" enctype="multipart/form-data">
                                                    		<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>                                                    		
                                                        	<input type="file" size="20" class="InputFileStyle" name="excelFile" id="excelFileAttr" onchange="previewImportExcelFile(this,'importAttrFrm','fakefilepc2');">
                                                        	<div class="FakeInputFile">
                                                            	<input type="text" id="fakefilepc2" class="InputTextStyle"/>
<%--                                                             	<span class="RequireStyle" style="margin-left: 7px">(*)</span> --%>
                                                        	</div>
                                                        </form>
                                                    </div>
                                                    <button class="BtnGeneralStyle Sprite2" onclick="AttributesManager.importExcel(1);"><span class="Sprite2" style="margin-left: 10px">Xem</span></button>
                                                    <button class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="AttributesManager.importExcel(0);"><span class="Sprite2">Nhập từ file</span></button>
                                                    <div class="Clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="Clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dlgListAttributeValue" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralMilkBox">
                  <div class="GeneralMilkTopBox">
                      <div class="GeneralMilkBtmBox">
                          <h3 class="Sprite2"><span class="Sprite2">Nhập giá trị thuộc tính</span></h3>
                         <div class="GeneralMilkInBox ResearchSection">
                         	<div class="SearchSales4Form">                                 
                                 <label class="LabelStyle Label1Style" style="width: 50px">Giá trị</label>
                                 <div class="Field2">
                                 	<input id="attrDetailValue" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText1Style" />
                                 	<span class="RequireStyle">(*)</span>
                         		 </div>
                                 <label class="LabelStyle Label2Style" style="width: 50px">Tên</label>
                                 <div class="Field2">
                                 	<input id="attrDetailName" type="text" maxlength="250" tabindex="2" class="InputTextStyle InputText1Style" />
                                 	<span class="RequireStyle">(*)</span>
                         		 </div>
                                 <div class="Clear"></div>                                 
                                 <div class="ButtonSection">
                                     <button id="btnSaveDetail" class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" tabindex="3" onclick="AttributesManager.saveOrUpdateDetail()">
                                     	<span class="Sprite2">Thêm</span>
                                     </button>
                                 </div>                                 
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="GeneralMilkBox">
                 <div class="GeneralMilkTopBox">
                     <div class="GeneralMilkBtmBox">
                         <h3 class="Sprite2"><span class="Sprite2">Danh sách giá trị</span></h3>
                         <div class="GeneralMilkInBox">
	                         <div id="searchStyle1ScrollBody" class="">
	                         	<div class="ResultSection" id="attrDetailContainer">                                 
	                                <table id="attrDetailGrid"></table>
									<div id="attrDetailPager"></div>
	                             </div>
	                         </div>                             
                         </div>
                     </div>
                 </div>
             </div>
             <div class="BoxDialogBtm">
             		<div class="BoxDialogAlert">
                        <p id="errAttrValue" class="ErrorMsgStyle Sprite1" style="display: none"></p>
                        <p id="succAttrValue" class="SuccessMsgStyle" style="display: none"></p>
                    </div>
                 <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
             </div>
             <input type="hidden" id="selDetailId" value="0">
             <input type="hidden" id="dlgSelAttrId" value="0">
     	</div>
     </div>
 </div>
<s:hidden id="dlgColumnTypeValue"></s:hidden>
<s:hidden id="selAttrId" val="0"></s:hidden>
<s:hidden id="tableNameType" name="type"></s:hidden>
<script type="text/javascript">
var attr_col_type_number = '<s:text name="attribute.column.type.number"/>';
var attr_col_type_char = '<s:text name="attribute.column.type.char"/>';
var attr_col_type_date = '<s:text name="attribute.column.type.date"/>';
var attr_col_val_type_manual = '<s:text name="attribute.column.value.type.manual"/>';
var attr_col_val_type_list = '<s:text name="attribute.column.value.type.list"/>';
var AttributeColumnType = {NUMBER:1, CHARACTER:2, DATE_TIME:3,
		parseValue: function(value){
			if(value == 'NUMBER'){
				return AttributeColumnType.NUMBER;
			} else if(value == 'CHARACTER'){
				return AttributeColumnType.CHARACTER;
			} else if(value == 'DATE_TIME'){
				return AttributeColumnType.DATE_TIME;
			}			
			return AttributeColumnType.CHARACTER;
		}};
var AttributeColumnValue = {SELF_INPUT:1, EXIST_BEFORE:2,
		parseValue: function(value){
			if(value == 'SELF_INPUT'){
				return AttributeColumnValue.SELF_INPUT;
			} else if(value == 'EXIST_BEFORE'){
				return AttributeColumnValue.EXIST_BEFORE;
			} 			
			return AttributeColumnValue.SELF_INPUT;
		}};
$(document).ready(function(){
	$('#fakefilepc2').val('');
	$('#excelFileAttr').val('');
	$('#errExcelMsg').html('').hide();
	AttributesManager.getListAttribute();
	$('#srlLstAttr').jScrollPane();
});
</script>
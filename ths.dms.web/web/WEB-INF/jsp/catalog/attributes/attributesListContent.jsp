<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="GeneralMilkInBox ResearchSection">
  	<div class="GeneralMilkBox GeneralMilkExtInBox NoneMarginBox">
          <div class="GeneralMilkTopBox">
              <div class="GeneralMilkBtmBox">
  				<h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                  <div class="GeneralMilkInBox ResearchSection">
                      <div class="ModuleList183Form" id="searchForm">
                          <label class="LabelStyle Label1Style">Mã thuộc tính</label>
                          <input id="attributeCode" type="text" class="InputTextStyle InputText1Style" />
                          <label class="LabelStyle Label2Style">Tên thuộc tính</label>
                          <input id="attributeName" type="text" class="InputTextStyle InputText1Style"/>
                          <label class="LabelStyle Label3Style">Loại dữ liệu</label>
                          <s:select id="columnType" headerKey="-2" headerValue="-- Chọn loại dữ liệu --"  name="columnType" cssClass="MySelectBoxClass" list="lstColumnType" listKey="value" listValue="name"></s:select>
                          <div class="Clear"></div>
                          <label class="LabelStyle Label1Style">Trạng thái</label>
                          <div class="BoxSelect BoxSelect1">
                              <s:select id="attributeStatus" headerKey="-2" headerValue="-- Chọn trạng thái --"  name="status" cssClass="MySelectBoxClass" list="lstStatusBean" listKey="value" listValue="name"></s:select>
                          </div>
                          <div class="Clear"></div>
                          <div class="ButtonSection">
                          	<button class="BtnGeneralStyle Sprite2" onclick="AttributesManager.search();"><span class="Sprite2">Tìm kiếm</span></button>
                          	<button class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="$('#searchForm').hide();$('#createForm').show();"><span class="Sprite2">Thêm mới</span></button>
                          </div>
                      </div>
                      <div class="ModuleList183Form" id="createForm" style="display: none">
                         <label class="LabelStyle Label1Style">Mã thuộc tính</label>
                         <div class="Field2">
                         		<input id="code" type="text" class="InputTextStyle InputText1Style" maxlength="40">
                         		<span class="RequireStyle">(*)</span>
                         </div>
                         <label class="LabelStyle Label2Style">Tên thuộc tính</label>
                         <div class="Field2">
                         		<input type="text" class="InputTextStyle InputText1Style" id="name" maxlength="250">
                         		<span class="RequireStyle">(*)</span>
                         </div>
                         <label class="LabelStyle Label3Style">Loại dữ liệu</label>
                         <div class="Field2">
                         		<div class="BoxSelect BoxSelect1">
		                             <s:select id="columnType" headerKey="-2" headerValue="-- Chọn loại dữ liệu --"  name="columnType" cssClass="MySelectBoxClass" list="lstColumnType" listKey="value" listValue="name"></s:select>
		                         </div>
                         		<span class="RequireStyle">(*)</span>
                         </div>
                         <div class="Clear"></div>
                         <label class="LabelStyle Label1Style">Tên trường</label>
                         <div class="Field2">
	                         <div class="BoxSelect BoxSelect1">
	                             <select class="MySelectBoxClass" id="columnName">
	                                 <option>---Chọn trạng tên trường---</option>
	                                 <option>2</option>
	                             </select>
	                         </div>
	                         <span class="RequireStyle">(*)</span>
                         </div>
                         <label class="LabelStyle Label2Style">Kiểu dữ liệu</label>
                         <div class="Field2">
	                         <div class="BoxSelect BoxSelect1">
	                             <s:select id="columnValueType" headerKey="-2" headerValue="-- Chọn loại dữ liệu --"  name="columnValueType" cssClass="MySelectBoxClass" list="lstColumnValueType" listKey="value" listValue="name"></s:select>
	                         </div>
	                         <span class="RequireStyle">(*)</span>
                         </div>
                         <label class="LabelStyle Label3Style">Trạng thái</label>
                         <div class="Field2">
	                         <div class="BoxSelect BoxSelect1">
	                             <s:select id="attributeStatus" headerKey="-2" headerValue="-- Chọn trạng thái --"  name="status" cssClass="MySelectBoxClass" list="lstStatusBean" listKey="value" listValue="name"></s:select>
	                         </div>
	                         <span class="RequireStyle">(*)</span>
                         </div>
                         <div class="Clear"></div>
                         <div class="ButtonSection">
                         	<button class="BtnGeneralStyle Sprite2"><span class="Sprite2">Lưu lại</span></button>
                         	<button class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="$('#createForm').hide();$('#searchForm').show();"><span class="Sprite2">Bỏ qua</span></button>
                         </div>
                     </div>
                  </div>
			</div>
	</div>
  </div>
   <div class="GeneralMilkBox GeneralMilkExtInBox">
       <div class="GeneralMilkTopBox">
           <div class="GeneralMilkBtmBox">
               <h3 class="Sprite2"><span class="Sprite2">Danh sách thuộc tính</span></h3>
               <div class="GeneralMilkInBox">
                   <div class="ResultSection">
                       <p class="WarningResultStyle">Không có kết quả</p>
                       <!--Đặt Grid ở đây-->
                   </div>
               </div>
           </div>
       </div>
   </div>
    <div class="GeneralMilkBox GeneralMilkExtInBox">
        <div class="GeneralMilkTopBox">
            <div class="GeneralMilkBtmBox">
                <h3 class="Sprite2"><span class="Sprite2">Quản lý dữ liệu thuộc tính</span></h3>
                <div class="GeneralMilkInBox">
                    <div class="ResultSection">
                        <div class="GeneralMilkCol9">
                            <div class="GeneralTable Table43Section">
                                <div class="BoxGeneralTTitle">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <colgroup>
                                            <col style="width:100px;" />
                                            <col style="width:185px;" />
                                            <col style="width:60px;" />
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th class="ColsThFirst">Mã thuộc tính</th>
                                                <th>Tên thuộc tính</th>
                                                <th class="ColsThEnd">&nbsp;</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="BoxGeneralTBody">
                                    <div class="ScrollBodySection">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <colgroup>
                                                <col style="width:100px;" />
                                                <col style="width:185px;" />
                                                <col style="width:60px;" />
                                            </colgroup>
                                            <tbody>
                                                <tr>
                                                    <td class="ColsTd1">Location</td>
                                                    <td class="ColsTd2ColsAlignLeft">Vị trí</td>
                                                    <td class="ColsTd3 ColsTdEnd"><input type="checkbox" /></td>                                                                  
                                                </tr>                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="GeneralMilkCol10">
                        	<div class="GeneralMilkBox">
                                <div class="GeneralMilkTopBox">
                                    <div class="GeneralMilkBtmBox">
                                        <h3 class="Sprite2"><span class="Sprite2">Xuất dữ liệu thuộc tính</span></h3>
                                        <div class="GeneralMilkInBox">
                                            <div class="ButtonSection">Xuất dữ liệu từ những thuộc tính đã chọn <button class="BtnGeneralStyle Sprite2"><span class="Sprite2">Xuất dữ liệu</span></button><img src="../../../resources/vinamilk/images/loading.gif" class="LoadingStyle" /></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        	<div class="GeneralMilkBox MarginTopBox">
                                <div class="GeneralMilkTopBox">
                                    <div class="GeneralMilkBtmBox">
                                        <h3 class="Sprite2"><span class="Sprite2">Nhập dữ liệu thuộc tính</span></h3>
                                        <div class="GeneralMilkInBox">
                                            <div class="ButtonSection">Xóa dữ liệu từ những thuộc tính đã chọn <button class="BtnGeneralStyle Sprite2"><span class="Sprite2">Xoá dữ liệu</span></button><img src="../../../resources/vinamilk/images/loading.gif" class="LoadingStyle" /></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="GeneralMilkBox MarginTopBox">
                                <div class="GeneralMilkTopBox">
                                    <div class="GeneralMilkBtmBox">
                                        <h3 class="Sprite2"><span class="Sprite2">Nhập dữ liệu thuộc tính</span></h3>
                                        <div class="GeneralMilkInBox">
                                            <div class="FuncBtmSection">
                                                <div class="DivInputFileSection">
                                                    <p class="ExcelLink"><a href="#">Tải mẫu file excel</a></p>
                                                    <div class="DivInputFile">
                                                        <input type="file" size="20" class="InputFileStyle" id="avatarFile" name="avatarFile">
                                                        <div class="FakeInputFile">
                                                            <input type="text" id="fakefilepc2" class="InputTextStyle" value="Tên file" />
                                                            <span class="RequireStyle">(*)</span>
                                                        </div>
                                                    </div>
                                                    <button class="BtnGeneralStyle Sprite2"><span class="Sprite2">Xem</span></button>
                                                    <button class="BtnGeneralStyle BtnGeneralMStyle Sprite2"><span class="Sprite2">Nhập từ file</span></button>
                                                    <div class="Clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="Clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var attr_col_type_number = '<s:text name="attribute.column.type.number"/>';
var attr_col_type_char = '<s:text name="attribute.column.type.char"/>';
var attr_col_type_date = '<s:text name="attribute.column.type.date"/>';
var attr_col_val_type_manual = '<s:text name="attribute.column.value.type.manual"/>';
var attr_col_val_type_list = '<s:text name="attribute.column.value.type.list"/>';
$(document).ready(function(){
	
});
</script>
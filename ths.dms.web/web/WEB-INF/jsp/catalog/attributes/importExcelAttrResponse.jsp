<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="k" uri="/kryptone" %>
<div id="errorExcel" style="display: none"><s:property value="isError"/></div>
<div id="errorExcelMsg" style="display: none"><s:property value="errMsg"/></div>
<div id="totalRow" style="display: none"><s:property value="totalItem"/></div>
<div id="numFail" style="display: none"><s:property value="numFail"/></div>
<div id="fileNameFail" style="display: none"><s:property value="fileNameFail"/></div>
<div id="typeViewResponse" style="display: none"><s:property value="typeView"/></div>
<s:if test="typeView">
	<div id="dlgViewAttrExcel" style="display:none;">
		<div class="GeneralDialog General2Dialog">
	      	<div class="DialogProductSearch">
	          	<div class="GeneralTable Table45Section">
	          	<div class="ScrollSection" id="scrollSectionId">
	          		<div class="BoxGeneralTTitle">
				          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="classTable">
				              <colgroup>
				                  <col style="width:50px;" />
				                  <s:iterator value="lstTitle">
				                  		<col style="width:150px;" />
				                  </s:iterator>				                  
				              </colgroup>
				              <thead>
				                  <tr>
				                      <th class="ColsThFirst">STT</th>
				                      <s:iterator value="lstTitle" status="status">
				                  		 	<th class="ColsThSecond_<s:property value="#status.index"/>"><s:property/></th>
				                  	  </s:iterator>
				                  </tr>
				              </thead>
				          </table>
			        </div>
	            	<div class="BoxGeneralTBody">
		             	<div class="ScrollBodySection" id="srlLstAttrExcel">
		                     <table width="100%" border="0" cellspacing="0" cellpadding="0" class="classTable">
		                         <colgroup>
		                         	  <col style="width:50px;" />
					                  <s:iterator value="lstTitle">
				                  		<col style="width:150px;" />
				                  	  </s:iterator>					                  
		                         </colgroup>
		                         <tbody>
		                          	<k:repeater value="lstData" status="status">
										<k:itemTemplate>
				                               <tr>
				                               	   <td class="ColsTd1"><s:property value="#status.index+1"/></td>
				                                   <s:iterator status="sChild">							                  		 	
							                  		 	<td class="ColsTd2_<s:property value="#sChild.index"/>"><div class="ColsAlignLeft" style="width: 129px"><s:property escape="false"/></div></td>
							                  	  </s:iterator>
				                               </tr>
										</k:itemTemplate>
									</k:repeater>
		                         </tbody>
		                     </table>
	                    </div>
	                </div>
	            </div>
	            <div class="BoxDialogBtm">
	                <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
	            </div>
	            </div>
	    	</div>
	    </div>
	</div>
	<s:hidden id="lstSize" name="lstTitle.size()"></s:hidden>
</s:if>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="dialogParam" class="easyui-dialog" title="<s:text name="jsp.apparam.tao.tham.so"></s:text>" data-options="closed:true,modal:true">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">

         	<label class="LabelStyle Label7Style" id="labelBankCode"><s:text name="jsp.apparam.ma.tham.so"></s:text><span class="RequireStyle"> *</span></label>
           	<input id="codeDl" type="text" class="InputTextStyle InputText5Style" maxlength="50"/>
           	<label class="LabelStyle Label2Style" id="labelBankName"><s:text name="jsp.apparam.ten.tham.so"></s:text></label>
           	<input id="nameDl" type="text" class="InputTextStyle InputText5Style" maxlength="100"/>
           	<label class="LabelStyle Label2Style" id="labelBankPhone"><s:text name="jsp.common.loai"></s:text></label>
           	<input id="typeDl" type="text" class="InputTextStyle InputText5Style" maxlength="50"/>
           	<div class="Clear"></div>
           	
           	<label class="LabelStyle Label7Style" id="labelBankAddress"><s:text name="jsp.common.s.mo.ta"></s:text></label>
           	<input id="descDl" type="text" class="InputTextStyle InputText5Style" maxlength="100"/>
           	<label class="LabelStyle Label2Style" id="labelBankAccount"><s:text name="jsp.common.s.gia.tri"></s:text></label>
           	<input id="valueDl" type="text" class="InputTextStyle InputText5Style" maxlength="100"/>
           	<label class="LabelStyle Label2Style"><s:text name="jsp.common.status"></s:text></label>
				<div class="BoxSelect BoxSelect2">
					<s:select id="statusDl" name="statusDl" cssClass="MySelectBoxClass" list="lstActiveType" listKey="value" listValue="apParamName" autocomplete="off"></s:select>
                  <%-- <select id="statusDl" class="MySelectBoxClass">
                      <option value="1" selected="selected"><s:text name="jsp.common.status.active"></s:text></option>
                      <option value="0"><s:text name="jsp.common.status.stoped"></s:text></option>
                  </select> --%>
              	</div>
             <div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnSaveBank" class="BtnGeneralStyle"><s:text name="jsp.common.capnhat"></s:text></button>
				<button class="BtnGeneralStyle" onclick="$('#dialogParam').dialog('close');"><s:text name="jsp.common.dong" ></s:text></button>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="Clear"></div>
		<p id="errorMsgDl" class="ErrorMsgStyle" style="display: none;"></p>
		<p id="successMsgDl" class="SuccessMsgStyle" style="display: none"></p>
	</div>
</div>

<script type="text/javascript">
	
</script>

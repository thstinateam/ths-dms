<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#"><s:text name="organization_system"></s:text></a></li>
        <li><span><s:text name="jsp.apparam.cauhinh.tham.so.title"></s:text></span></li>
    </ul>
</div>

<div class="CtnOneColSection">
   <div class="ContentSection">
   		<div class="ToolBarSection">
           <div class="SearchSection GeneralSSection">
	           	<h2 class="Title2Style"><s:text name="jsp.common.thongtintimkiem"></s:text></h2>
	           	<div class="SearchInSection SProduct1Form" id="searchForm">								
						<label class="LabelStyle Label1Style"><s:text name="jsp.apparam.ma.tham.so"></s:text></label>
						<input type="text" autocomplete="off" maxlength="50" style="width:255px;" class="InputTextStyle InputText1Style" id="apParamCode">
								
						<label class="LabelStyle Label1Style"><s:text name="jsp.apparam.ten.tham.so"></s:text></label>
						<input type="text" autocomplete="off" maxlength="250" style="width:20%;" class="InputTextStyle InputText1Style" id="apParamName">
								
						<label class="LabelStyle Label1Style"><s:text name="jsp.common.loai"></s:text></label>
						<input type="text" autocomplete="off" maxlength="50" style="width:20%;" class="InputTextStyle InputText1Style" id="apParamType">
						
						<div class="Clear"></div>
						
						<label class="LabelStyle Label1Style"><s:text name="jsp.common.status"></s:text></label>
						<div class="BoxSelect BoxSelect7">
							<s:select id="status" name="status" cssClass="MySelectBoxClass" list="lstActiveType" listKey="value" listValue="apParamName" autocomplete="off"></s:select>
							<%-- <select autocomplete="off" id="status" class="MySelectBoxClass">
								<option selected="selected" value="-2"><s:text name="jsp.common.status.all"></s:text></option>
								<option value="1"><s:text name="jsp.common.status.active"></s:text></option>
								<option value="0"><s:text name="jsp.common.status.stoped"></s:text></option>
							</select> --%>
						</div>
						<div class="Clear"></div>				
						<div class="BtnCenterSection">
							<button onclick="ApParamSetting.search();" id="btnSearch" class="BtnGeneralStyle"><s:text name="jsp.common.timkiem"></s:text></button>
						</div>
						<div class="Clear"></div>
						<p style="display:none;" class="ErrorMsgStyle" id="errMsg"></p>
	              <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
	           	</div>
	           	<div class="SearchInSection SProduct1Form" style="padding:0;">
	           		<h2 class="Title2Style"><s:text name="jsp.apparam.ds.tham.so"></s:text></h2>
	           		<div class="GridSection" id="paramGrid">
	                  	<div id="searchResult" class="GeneralTable Table34Section">
							<p id="gridNoResult" style="display: none" class="WarningResultStyle"><s:text name="jsp.common.no.result"></s:text></p>
							<div class="BoxGeneralTTitle" id="dgGridContainer">
								<table id="grid"></table>
							</div>
						</div>
	              	</div>
	           	</div>
           </div>
      	</div>
      	<!-- <div id="divDetailCommon" class="GeneralCntSection" style="display: none;">
        </div> -->
        
   </div>
</div>

<div class="ContentSection" style="visibility: hidden;" id="dialogBankDiv">
	<tiles:insertTemplate template="/WEB-INF/jsp/catalog/apparam/popUpParam.jsp" />
</div>

<script type="text/javascript">
$(document).ready(function(){
	$("#apParamCode").focus();
	$('#status').prepend('<option value="-2">'+Utils.XSSEncode(jsp_common_status_all)+'</option>').change(); 
	$("#grid").datagrid({
		url: "/catalog/apparam/search",
		queryParams: {status: $('#status').val().trim() },
		width: ($('#dgGridContainer').width()-17),
		height: 'auto',
		singleSelect: true,
		rownumbers: true,
		fitColumns: true,
		scrollbarSize: 0,
		pagination: true,
		pageList  : [10,20,30],
		columns: [[			
			{field:'apParamCode', title: jsp_apparam_ma_tham_so, width: 100, align:'left',sortable:false,resizable:false, sortable:true, formatter:function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}},
			{field:'apParamName', title: jsp_apparam_ten_tham_so, align:'left',sortable:false,resizable:false,width : 150, sortable:true, formatter:function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}},
			{field:'type', title: jsp_common_loai,align:'left',sortable:false,resizable:false,width: 100, sortable:true, formatter:function(value, row, index){
				var type= "";
				if(row.type!=null){
					type = row.type;
				}
				return VTUtilJS.XSSEncode(type);
			}},
			{field:'description', title: jsp_common_s_mo_ta, width:300,align:'left',sortable:false,resizable:false, sortable:true, formatter:function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}},
			{field:'value', title: jsp_common_s_gia_tri, width:100,align:'left',sortable:false,resizable:false, sortable:true, formatter:function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}},
			{field:'statusStr', title: jsp_common_status, width:70,align:'left',sortable:false,resizable:false, sortable:true, formatter:function(value,row,index){
				return VTUtilJS.XSSEncode(value);
// 				if(!VTUtilJS.isNullOrEmpty(row.status)){
// 					if(row.status ==  0/* stoppedStatus */){
// 						return jsp_common_status_stoped;
// 					}else if(row.status == 1/* activeStatus */){
// 						return jsp_common_status_active;
// 					}else{
// 						return jsp_common_status_draft;
// 					}
// 				}else{
// 					return '';
// 				}
			}},			
			{field:'view', title:'<a href="javaScript:void(0);" id="dg_bank_insert" onclick="return ApParamSetting.openDialogChangeAdd();"><span style="cursor:pointer"><img title="'+Utils.XSSEncode(jsp_common_taomoi)+'" src="/resources/images/icon_add.png"/></span></a>',
				width:30, align:'center',sortable:false,resizable:false,formatter: function(value, row, index){
					return '<a href="javascript:void(0)" id="dg_bank_update" onclick="return ApParamSetting.openDialogChangeUpdate('+index+','+row.id+');"><img title="'+Utils.XSSEncode(jsp_common_capnhat)+'" src="/resources/images/icon-edit.png" width="16" heigh="16" style="padding-left: 5px;"></a>';
			}}
			
		]],
		onLoadSuccess: function(data) {
			$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
			$(window).resize();
			Utils.updateRownumWidthAndHeightForDataGrid('grid');
			var lstHeaderStyle = ["apParamCode","apParamName","type","description","value","statusStr"];
			 Utils.updateCellHeaderStyleSort('dgGridContainer',lstHeaderStyle);
		}
	});
});
</script>
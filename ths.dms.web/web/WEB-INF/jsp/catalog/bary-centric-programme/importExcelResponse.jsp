<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="k" uri="/kryptone" %>
<div id="errorExcel" style="display: none"><s:property value="isError"/></div>
<div id="errorExcelMsg" style="display: none"><s:property value="errMsg"/></div>
<div id="totalRow" style="display: none"><s:property value="totalItem"/></div>
<div id="numFail" style="display: none"><s:property value="numFail"/></div>
<div id="fileNameFail" style="display: none"><s:property value="fileNameFail"/></div>
<div id="typeView" style="display: none"><s:property value="typeView"/></div>
<div id="lstSize" style="display: none"><s:property value="lstView.size()"/></div>
<input type="hidden" id="newToken" name="newToken" value='<s:property value="token"/>'>
<!-- <div id="popup1" style="display:none;"> -->
<!-- 	<div class="GeneralDialog General2Dialog"> -->
<!--       	<div class="DialogProductSearch"> -->
<!--           	<div class="GeneralTable Table45Section"> -->
<!--           	<div class="ScrollSection"> -->
<!--           		<div class="BoxGeneralTTitle"> -->
<!-- 			          <table width="100%" border="0" cellspacing="0" cellpadding="0"> -->
<!-- 			              <colgroup> -->
<!-- 			                  <col style="width:90px;" /> -->
<!-- 			                  <col style="width:200px;" /> -->
<!-- 			                  <col style="width:200px;" /> -->
<!-- 			                  <col style="width:150px;" /> -->
<!-- 			                  <col style="width:150px;" /> -->
<!-- 			                  <col style="width:500px;" /> -->
<!-- 			              </colgroup> -->
<!-- 			              <thead> -->
<!-- 			                  <tr> -->
<!-- 			                      <th class="ColsThFirst">STT</th> -->
<!-- 			                      <th>Mã CTTT</th> -->
<!-- 			                      <th>Tên CTTT</th> -->
<!-- 			                      <th>Từ ngày</th> -->
<!-- 			                      <th>Đến ngày</th> -->
<!-- 			                      <th>Ghi chú</th> -->
<!-- 			                  </tr> -->
<!-- 			              </thead> -->
<!-- 			          </table> -->
<!-- 		        </div> -->
<!--             	<div class="BoxGeneralTBody"> -->
<!-- 	             	<div class="ScrollBodySection"> -->
<!-- 	                     <table width="100%" border="0" cellspacing="0" cellpadding="0"> -->
<!-- 	                         <colgroup> -->
<!-- 	                         	  <col style="width:90px;" /> -->
<!-- 				                  <col style="width:200px;" /> -->
<!-- 				                  <col style="width:200px;" /> -->
<!-- 				                  <col style="width:150px;" /> -->
<!-- 				                  <col style="width:150px;" /> -->
<!-- 				                  <col style="width:500px;" /> -->
<!-- 	                         </colgroup> -->
<!-- 	                         <tbody> -->
<%-- 	                          	<k:repeater value="lstView" status="status"> --%>
<%-- 									<k:itemTemplate> --%>
<!-- 			                               <tr> -->
<%-- 			                               	   <td class="ColsTd1"><div class="ColsAlignLeft" style="width: 69px"><s:property value="#status.index+1"/></div></td> --%>
<%-- 			                                   <td class="ColsTd2"><div class="ColsAlignLeft" style="width: 179px"><s:property value="content1"/></div></td>                                                   --%>
<%-- 			                                   <td class="ColsTd3"><div class="ColsAlignLeft" style="width: 179px"><s:property value="content2"/></div></td> --%>
<%-- 			                                   <td class="ColsTd4"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content3"/></div></td>    --%>
<%-- 			                                   <td class="ColsTd5"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content4"/></div></td> --%>
<%-- 											   <td style="padding-right: 10px;" class="ColsTd10 BreakLine_<s:property value="#status.index"/>"><div class="ColsAlignLeft" style="width: 479px"><s:property value="errMsg" escape="false"/></div></td>			                                    --%>
<!-- 			                               </tr> -->
<%-- 									</k:itemTemplate> --%>
<%-- 								</k:repeater> --%>
<!-- 	                         </tbody> -->
<!-- 	                     </table> -->
<!--                     </div> -->
<!--                 </div> -->
<!--             </div> -->
<!--             <div class="BoxDialogBtm"> -->
<%--                 <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div> --%>
<!--             </div> -->
<!--             </div> -->
<!--     	</div> -->
<!--     </div> -->
<!-- </div> -->
<!-- <div id="popup2" style="display:none;"> -->
<!-- 	<div class="GeneralDialog General2Dialog"> -->
<!--       	<div class="DialogProductSearch"> -->
<!--           	<div class="GeneralTable Table51Section"> -->
<!--           	<div class="ScrollSection"> -->
<!--           		<div class="BoxGeneralTTitle"> -->
<!-- 			          <table width="100%" border="0" cellspacing="0" cellpadding="0"> -->
<!-- 			              <colgroup> -->
<!-- 			                  <col style="width:50px;" /> -->
<!-- 			                  <col style="width:200px;" /> -->
<!-- 			                  <col style="width:200px;" /> -->
<!-- 			                  <col style="width:300px;" /> -->
<!-- 			              </colgroup> -->
<!-- 			              <thead> -->
<!-- 			                  <tr> -->
<!-- 			                      <th class="ColsThFirst">STT</th> -->
<!-- 			                      <th>Mã CTTT</th> -->
<!-- 			                      <th>Mã đơn vị</th> -->
<!-- 			                      <th>Thông tin lỗi</th> -->
<!-- 			                  </tr> -->
<!-- 			              </thead> -->
<!-- 			          </table> -->
<!-- 		        </div> -->
<!--             	<div class="BoxGeneralTBody"> -->
<!-- 	             	<div class="ScrollBodySection"> -->
<!-- 	                     <table width="100%" border="0" cellspacing="0" cellpadding="0"> -->
<!-- 	                         <colgroup> -->
<!-- 	                         	  <col style="width:50px;" /> -->
<!-- 				                  <col style="width:200px;" /> -->
<!-- 				                  <col style="width:200px;" /> -->
<!-- 				                  <col style="width:300px;" /> -->
<!-- 	                         </colgroup> -->
<!-- 	                         <tbody> -->
<%-- 	                          	<k:repeater value="lstView" status="status"> --%>
<%-- 									<k:itemTemplate> --%>
<!-- 			                               <tr> -->
<%-- 			                               	   <td class="ColsTd1"><div class="ColsAlignLeft" style="width: 29px"><s:property value="#status.index+1"/></div></td> --%>
<%-- 			                                   <td class="ColsTd2"><div class="ColsAlignLeft" style="width: 179px"><s:property value="content1"/></div></td>                                                   --%>
<%-- 			                                   <td class="ColsTd3"><div class="ColsAlignLeft" style="width: 179px"><s:property value="content2"/></div></td> --%>
<%-- 			                                   <td class="ColsTd6 BreakLine_<s:property value="#status.index"/>"><div class="ColsAlignLeft" style="width: 279px"><s:property value="errMsg" escape="false"/></div></td> --%>
<!-- 			                               </tr> -->
<%-- 									</k:itemTemplate> --%>
<%-- 								</k:repeater> --%>
<!-- 	                         </tbody> -->
<!-- 	                     </table> -->
<!--                     </div> -->
<!--                 </div> -->
<!--             </div> -->
<!--             <div class="BoxDialogBtm"> -->
<%--                 <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div> --%>
<!--             </div> -->
<!--             </div> -->
<!--     	</div> -->
<!--     </div> -->
<!-- </div> -->
<!-- <div id="popup3" style="display:none;"> -->
<!-- 	<div class="GeneralDialog General2Dialog"> -->
<!--       	<div class="DialogProductSearch"> -->
<!--           	<div class="GeneralTable Table51Section"> -->
<!--           	<div class="ScrollSection"> -->
<!--           		<div class="BoxGeneralTTitle"> -->
<!-- 			          <table width="100%" border="0" cellspacing="0" cellpadding="0"> -->
<!-- 			              <colgroup> -->
<!-- 			                  <col style="width:50px;" /> -->
<!-- 			                  <col style="width:100px;" /> -->
<!-- 			                  <col style="width:100px;" /> -->
<!-- 			                  <col style="width:100px;" /> -->
<!-- 			                  <col style="width:100px;" /> -->
<!-- 			                  <col style="width:300px;" /> -->
<!-- 			              </colgroup> -->
<!-- 			              <thead> -->
<!-- 			                  <tr> -->
<!-- 			                      <th class="ColsThFirst">STT</th> -->
<!-- 			                      <th>Mã CTTT</th> -->
<!-- 			                      <th>Hình thức bán hàng</th> -->
<!-- 			                      <th>Mã SP</th> -->
<!-- 			                      <th>Loại MHTT</th> -->
<!-- 			                      <th>Thông tin lỗi</th> -->
<!-- 			                  </tr> -->
<!-- 			              </thead> -->
<!-- 			          </table> -->
<!-- 		        </div> -->
<!--             	<div class="BoxGeneralTBody"> -->
<!-- 	             	<div class="ScrollBodySection"> -->
<!-- 	                     <table width="100%" border="0" cellspacing="0" cellpadding="0"> -->
<!-- 	                         <colgroup> -->
<!-- 	                         	  <col style="width:50px;" /> -->
<!-- 				                  <col style="width:100px;" /> -->
<!-- 				                  <col style="width:100px;" /> -->
<!-- 				                  <col style="width:100px;" /> -->
<!-- 				                  <col style="width:100px;" /> -->
<!-- 				                  <col style="width:300px;" /> -->
<!-- 	                         </colgroup> -->
<!-- 	                         <tbody> -->
<%-- 	                          	<k:repeater value="lstView" status="status"> --%>
<%-- 									<k:itemTemplate> --%>
<!-- 			                               <tr> -->
<%-- 			                               	   <td class="ColsTd1"><div class="ColsAlignLeft" style="width: 29px"><s:property value="#status.index+1"/></div></td> --%>
<%-- 			                                   <td class="ColsTd2"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content1"/></div></td>                                                   --%>
<%-- 			                                   <td class="ColsTd3"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content2"/></div></td> --%>
<%-- 			                                   <td class="ColsTd4"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content3"/></div></td>    --%>
<%-- 			                                   <td class="ColsTd5"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content4"/></div></td>                                                   --%>
<%-- 			                                   <td class="ColsTd6 BreakLine_<s:property value="#status.index"/>"><div class="ColsAlignLeft" style="width: 279px"><s:property value="errMsg" escape="false"/></div></td> --%>
<!-- 			                               </tr> -->
<%-- 									</k:itemTemplate> --%>
<%-- 								</k:repeater> --%>
<!-- 	                         </tbody> -->
<!-- 	                     </table> -->
<!--                     </div> -->
<!--                 </div> -->
<!--             </div> -->
<!--             <div class="BoxDialogBtm"> -->
<%--                 <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div> --%>
<!--             </div> -->
<!--             </div> -->
<!--     	</div> -->
<!--     </div> -->
<!-- </div> -->
<s:hidden id="sizeList" name="totalItem"></s:hidden>
<script type="text/javascript">     
var s = '<s:property value="importProgram"/>';
if(s == 'shop'){
	BaryCentricProgrammeCatalog.searchShop();
}
</script>
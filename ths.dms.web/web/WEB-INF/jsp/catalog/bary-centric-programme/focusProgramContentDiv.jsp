<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="javascript:void(0);">Chương trình trọng tâm</a></li>
        <li><span>Danh sách chương trình trọng tâm</span></li>
    </ul>
</div>
<div class="CtnOneColSection">
    <div class="ContentSection">
    	<div class="ToolBarSection">
            <div class="SearchSection GeneralSSection">
                <h2 class="Title2Style">Thông tin tìm kiếm</h2>
                <div class="SearchInSection SProduct1Form">
                    <label class="LabelStyle Label1Style">Mã CTTT</label>
                    <input id="code" type="text" class="InputTextStyle InputText1Style" maxlength="20"/>
                    <label class="LabelStyle Label1Style">Tên CTTT</label>
                    <input id="name" type="text" maxlength="250" class="InputTextStyle InputText1Style" style="white-space:pre;" />
                    <label id="lblUnitFocus" class="LabelStyle Label1Style">Đơn vị</label>
                    <input type="text" class="InputTextStyle InputText1Style" id="shopTree" style="width:183px;" />
                    <div class="Clear"></div>
                    <label class="LabelStyle Label1Style">Từ ngày</label>
                    <input type="text" id="fDate" class="InputTextStyle InputText6Style" />
                    <label class="LabelStyle Label1Style">Đến ngày</label>
                    <input type="text" id="tDate" class="InputTextStyle InputText6Style" />
                    <label class="LabelStyle Label1Style">Trạng thái</label>
                    <div class="BoxSelect BoxSelect2">
	                    <select id="status" class="MySelectBoxClass">
	                       <option id="statusOp_T" value="-2">-- Tất cả --</option>
	                       <option id="statusOp_0" value="0">Tạm ngưng</option>
	                       <option value="1" selected="selected">Hoạt động</option>
	                       <option id="statusOp_2" value="2">Dự thảo</option>
	                   </select>
                    </div>
                    <div class="Clear"></div>
                    <div class="BtnCenterSection">
                        <button id="btnSearch" onclick="BaryCentricProgrammeCatalog.search()" class="BtnGeneralStyle">Tìm kiếm</button>
                    </div>
                    <p style="display:none;" class="ErrorMsgStyle" id="errMsg"/>
                    <div class="Clear"></div>
                    </div>
                    <h2 class="Title2Style">Danh sách CTTT</h2>
            	<div class="GridSection" id="gridContainer">
                	<table id="listCTTTGrid"></table>
                </div>
                <div class="GeneralForm GeneralNoTP1Form">
                <div class="Clear"></div>
          		</div>
                <div class="Clear"></div>
            </div>
            <div class="Clear"></div>
        </div>
    </div>
    <div class="Clear"></div>
</div>
<s:hidden id="selId" value="0"></s:hidden>
<s:hidden id="aPer" name="activePermission"></s:hidden>
<s:hidden id="csPer" name="commercialSupportPermission"></s:hidden>
<input type="hidden" id="shopId"/>
<input type="hidden" id="curShopId" value="<s:property value="shopId" />" />

<script type="text/javascript">
$(document).ready(function() {	
	$('#code').focus();
	$('#shopName').val('');
	if ($('#status').length > 0) {
		$('#status').width(182);
		$('.BoxSelect .CustomStyleSelectBoxInner').width(150);
		setTimeout(function () {
			if ($("#status option:not(:hidden)").length == 1) {
				disableSelectbox("status");
			}
		}, 1000);
	}
	setDateTimePicker('fDate');
	setDateTimePicker('tDate');
	//load combobox don vi
	if ($("#shopTree").length > 0) {
		$('#shopTree').combotree({
			url: '/catalog/bary-centric-programme/search-unit-tree',
			lines: true,
			formatter: function(node) {
				return Utils.XSSEncode(node.text);
			},
			onSelect: function(data) {
	        	if (data != null) {
	        		$('#shopId').val($('#shopTree').combotree('getValue'));
	        	}
		    },
		    onLoadSuccess: function(node, data) {
				$('#shopTree').combotree('setValue', data[0].id);
			}
		});
	}
	var code = $('#code').val().trim();
	var name = $('#name').val().trim();
	var fromDate = $('#fDate').val();
	var toDate = $('#tDate').val();	
	var status = 1;
	if ($('#status').length > 0) {
		status = $('#status').val().trim();
	}
	var title = '<a id="btnAddGrid" title="Thêm mới" href="javascript:void(0)" onclick=\"BaryCentricProgrammeCatalog.getSelectedProgram(0);\"><img src="/resources/images/icon_add.png"/></a>';
	$('#listCTTTGrid').datagrid({
		url: BaryCentricProgrammeCatalog.getGridUrl(code,name, -2, fromDate, toDate, status),
		width: $(window).width() - 35,
		pagination: true,
		pageNumber : 1,
		scrollbarSize: 0,
		checkOnSelect: true,
		autoWidth: true,
		fitColumns: true,
		rownumbers: true,
		singleSelect: true,
		columns: [[  
		        {field: 'focusProgramCode', title: 'Mã CTTT', width: 200, sortable : false, resizable : false, align: 'left', formatter:function(value, row,index) {
		      	  	return Utils.XSSEncode(value);
		        }},  
		        {field: 'focusProgramName', title: 'Tên CTTT', width: $(window).width() - 70 - 200 - 80 - 80 - 80 - 30, sortable: false, resizable: false, align: 'left', formatter:function(value, row,index) {
		      	  	return '<p style="white-space:pre;">'+Utils.XSSEncode(row.focusProgramName)+'</p>';
		        }},  
		        {field: 'fromDate', title: 'Từ ngày', width: 80, sortable: false, align: 'center', resizable: false , formatter:function(value, row, index) {
		      	  	return toDateString(new Date(row.fromDate));
		        }},
		        {field: 'toDate', title: 'Đến ngày', width: 80, sortable: false, align: 'center', resizable: false , formatter:function(value, row, index) {
		      	  	var toDate = formatDate(row.toDate);
		 	    	if (toDate == null || toDate == undefined || toDate == '' || toDate == 'null') {
		 	    		toDate = '';
		 	    	}
		 	    	return toDate;
		        }},
		        {field: 'status', title: 'Trạng thái', width: 82, sortable: false, resizable: false, align: 'center', formatter: function(value, row,index) {
				if (row.status == activeStatus) {
		     			return "Hoạt động";
		     		} else if (row.status == stoppedStatus) {
		     			return "Tạm ngưng";
		     		} else if (row.status == waitingStatus) {
		     			return "Dự thảo";
		     		}
		        }},
		        {field: 'detail', title: title, width: 30, sortable: false, resizable: false, align: 'center', formatter: function(value, row, index) {
		      	  	return "<a id='btnEdit"+row.id+"' title='Xem' href='javascript:void(0)' onclick=\"BaryCentricProgrammeCatalog.getSelectedProgram('"+ row.id +"');\"><img src='/resources/images/icon-edit.png'/></a>";
		        }},
		        {field: 'P', hidden : true}
		]],
	  	onLoadSuccess: function(data) {
		    	$('.datagrid-header-rownumber').html('STT');
		    	$('#btnAddGrid').addClass('cmsiscontrol');
		    	Utils.functionAccessFillControl('gridContainer', function(data){
		    		
		    	});
		    	/*
		    	var arrDetail =  $('#gridContainer td[field="detail"]');
				if (arrDetail != undefined && arrDetail != null && arrDetail.length > 0) {
				  for (var i = 0, size = arrDetail.length; i < size; i++) {
				    $(arrDetail[i]).prop("id", "gr_table_edit_" + i);//Khai bao id danh cho phan quyen
					$(arrDetail[i]).addClass("cmsiscontrol");
				  }
				}
				Utils.functionAccessFillControl('gridContainer', function(data){
					//Xu ly cac su kien lien quan den cac control phan quyen
					var arrTmpLength =  $('#gridContainer td[id^="gr_table_edit_"]').length;
					var invisibleLenght = $('.isCMSInvisible[id^="gr_table_edit_"]').length;
					if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
						$('#listCTTTGrid').datagrid("showColumn", "detail");
					} else {
						$('#listCTTTGrid').datagrid("hideColumn", "detail");
					}
				});
				*/
	    },
	    method : 'GET'
	});
});
</script>
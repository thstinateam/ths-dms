<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
       	<ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a href="javascript:void(0);" class="cmsdefault">Tiện ích</a></li>
           <li><span>Mở chốt ngày</span></li>
    	</ul>
</div>
<div class="CtnOneColSection">
   <div class="ContentSection">
   		<div class="ToolBarSection">
           <div class="SearchSection GeneralSSection">
            	<div class="SearchInSection SProduct1Form" >
	            	<div class="BtnCenterSection">
		            	<label class="LabelStyle Label2Style">NPP <span class="ReqiureStyle">*</span></label>
						<div class="BoxSelect BoxSelect2">
							<select id="shop" style="width: 206px;">
							</select>
						</div>
<!-- 		            	<div style="height: auto;"> -->
<%-- 		            		<s:if test="lstShop != null && lstShop.size() == 1"> --%>
<!-- 		            			<div class="BoxSelect BoxSelect1"> -->
<%-- 									<select id="shop" class="MySelectBoxClass" disabled="disabled"> --%>
<%-- 										<s:iterator value="lstShop"> --%>
<%-- 											<option value='<s:property value="shopId"/>'><s:property value="shopCode"/> - <s:property value="shopName"/></option> --%>
<%-- 										</s:iterator> --%>
<%-- 									</select> --%>
<!-- 								</div> -->
<%-- 		            		</s:if> --%>
<%-- 		            		<s:else> --%>
<!-- 		            			<div class="BoxSelect BoxSelect1"> -->
<%-- 									<select id="shop" class="MySelectBoxClass" > --%>
<%-- 										<s:iterator value="lstShop"> --%>
<%-- 											<option value='<s:property value="shopId"/>'><s:property value="shopCode"/> - <s:property value="shopName"/></option> --%>
<%-- 										</s:iterator> --%>
<%-- 									</select> --%>
<!-- 								</div> -->
<%-- 		            		</s:else> --%>
<!-- 		            	</div> -->
		            	<label class="LabelStyle Label2Style">Ngày làm việc</label>
	                    <p id="appDate" class="ValueStyle Value1Style"></p>
	                    <label class="LabelStyle Label2Style">Ngày triển khai</label>
	                    <p id="minLockDate" class="ValueStyle Value1Style"></p>
	                    <div class="Clear"></div>
						<label class="LabelStyle Label2Style">Ngày<span class="ReqiureStyle"> *</span></label>
	                    <%-- <input type="text" id="date" maxlength="10" class="InputTextStyle InputText6Style" disabled="disabled" value="<s:property value="dateLock"/>"/> --%>	            	
	                    <input type="text" id="date" maxlength="10" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="dateLock"/>"/>	            	
                    </div>
                    <div class="Clear"></div>
	            	<div class="BtnCenterSection">
	 					<button style="margin-bottom: 20px;" id="btnOpenLock" class="BtnGeneralStyle cmsiscontrol" onclick="openDay();"><span class="Sprite2">Mở chốt</span></button>      	
	 				</div>
            	</div>
            	<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
            </div>
        </div>
  	</div>
</div>
	<div style="display:none">
		<div id="warningPopup" class="easyui-dialog" title="Xác nhận" data-options="closed:true,modal:true">
			<div class="PopupContentMid">
				<div class="GeneralForm" >
	                <div class="icon-warning" style="width: 48px; height: 48px; float: left; margin-right: 20px;"></div>
<%-- 	                <p style="font-weight: bold; padding: 2px 0px;">Ngày làm việc sẽ là ngày <span id="nextDate" style="color: #FF6600;"><s:property value="nextDate"/></span>.</p> --%>
<%-- 	                <p style="font-weight: bold; padding: 2px 0px;">Tất cả thao tác trong ngày <span id="currentDate" style="color: #FF6600;"><s:property value="dateLock"/></span> sẽ không được thực hiện nữa.</p> --%>
	                <p style="font-weight: bold; padding: 2px 0px; margin-left: 68px; margin-bottom: 10px;">Bạn có muốn thực hiện thao tác này không?</p>
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
		                <button id="btnOk" class="BtnGeneralStyle">Chọn</button>
						<button class="BtnGeneralStyle cmsdefault" id="btnCancel">Đóng</button>
	                </div>
 					<p id="errMsgSelectShop" class="ErrorMsgStyle" style="display: none"></p> 
				</div>
              </div>
		</div>
	</div>
	
	<div style="display:none">
		<div id="successPopup" class="easyui-dialog" title="Xác nhận" data-options="closed:true,modal:true">
			<div class="PopupContentMid">
				<div class="GeneralForm" >
	                <div class="icon-success" style="width: 46px; height: 46px; float: left; margin-right: 20px;"></div>
	                <p style="font-weight: bold; padding: 2px 0px;">Ngày <span id="date0" style="color: #CC0000;"></span> đã được mở chốt thành công.</p>
	                <p style="font-weight: bold; padding: 2px 0px;">Ngày làm việc sẽ là: <span id="date1" style="color: #CC0000;"></span>.</p>
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
		                <button id="btnSuccessOk" class="BtnGeneralStyle cmsdefault">Đóng</button>
	                </div>
				</div>
              </div>
		</div>
	</div>
	
<p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>
<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
<s:hidden id="appDateHide" ></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	Utils.initUnitCbx("shop", null, 206, function(rec) {
		//lấy ngày chốt và ngày triển khai
		$('#errMsg').hide();
		var params = new Object();
		params.shopId = rec.id;
		Utils.getJSONDataByAjax(params,"/catalog/utility/closing-day/get-lock-date-by-shop", function(data) {
			if (data.error) {
				$('#errMsg').html(data.errMsg).show();
			} else {
				if (data.minLockDate != undefined && data.minLockDate != null) {
					$('#minLockDate').html(data.minLockDate);
				} else {
					$('#minLockDate').html('');
				}
				if (data.appDate != undefined && data.appDate != null) {
					$('#appDate').html(data.appDate);
					$('#appDateHide').val(data.appDate);
				} else {
					$('#appDate').html('');
					$('#appDateHide').val('');
				}
				if (data.lockDate != undefined && data.lockDate != null) {
					$('#date').val(data.lockDate);
				} else {
					$('#date').val('');
				}
			}
		});
	}, null, null, null);
	
// 	$('#shop').change(function() {
// 		//lấy ngày chốt và ngày triển khai
// 		$('#errMsg').hide();
// 		var params = new Object();
// 		params.shopId = $('#shop').val();
// 		Utils.getJSONDataByAjax(params,"/catalog/utility/closing-day/get-lock-date-by-shop",function(data){
// 			if (data.error) {
// 				$('#errMsg').html(data.errMsg).show();
// 			} else {
// 				if (data.minLockDate != undefined && data.minLockDate != null) {
// 					$('#minLockDate').html(data.minLockDate);
// 				} else {
// 					$('#minLockDate').html('');
// 				}
// 				if (data.appDate != undefined && data.appDate != null) {
// 					$('#appDate').html(data.appDate);
// 				} else {
// 					$('#appDate').html('');
// 				}
// 			}
// 		});
// 	});
// 	$('#shop').change();
});


function openDay() {
	$('#errMsg').hide();
	var msg = '';
	if(msg.length == 0){
		msg = Utils.getMessageOfRequireCheck('date','Ngày chọn');
	}
	if(msg.length == 0){
		msg = Utils.getMessageOfInvalidFormatDate('date','Ngày chọn');
	}
	var fDate = $('#date').val();
	var appDate = $('#appDateHide').val();
	
	if(msg.length == 0 && !Utils.compareDate1(fDate, appDate)){
		msg = 'Ngày chọn không được lớn hơn hoặc bằng ngày làm việc. Vui lòng nhập lại';
		$('#date').focus();
	}
	if(msg.length > 0){
		$('#errMsg').html(msg).show();
		return false;
	}
	$('#warningPopup').dialog({
		title : 'Xác nhận',
		width:400,
		height:'auto',
		onOpen: function(){
			$("#errMsgSelectShop").hide();
			$("#btnCancel").bind('click',function(event) {
				$('#warningPopup').dialog('close');
			});
			
			$("#btnOk").unbind('click');
			$("#btnOk").bind('click',function(event) {
				$('#warningPopup').dialog('close');
	 			$('#errMsg').html('').hide();
	 			$('#divOverlay').show();
	 				
	 			var params = new Object();
	 			params.token = Utils.getToken();
	 			params.lockDate = fDate;
	 			params.shopCode = $('#shop').combobox('getValue');
	 			var kData = $.param(params, true);
				
	 			$.ajax({
	 				type : "POST",
	 				url : "/catalog/utility/closing-day/open/openLock",
	 				data :(kData),
	 				dataType: "json",
	 				success : function(data) {
	 					$('#divOverlay').hide();
	 					Utils.updateTokenForJSON(data);
	 					if(!data.error) {						
	 						$('#date0').html(data.appdate);
	 						$('#date1').html(data.lockdate);
	 						$('#successPopup').dialog({
	 							title : 'Mở chốt ngày thành công',
	 							width:400,
	 							height:'auto',
	 							onOpen: function(){
	 								$("#btnSuccessOk").bind('click',function(event) {
	 									$('#successPopup').dialog('close');
	 								});
	 							}
	 						});
	 						$('#successPopup').dialog('open');
// 	 						if (!data.isNotChangeLockDate) {
// 	 							$('#shopLockDate').html('Ngày làm việc: ' + data.nextdate);
// 	 						}
// 	 						$('#dateLable').html('Ngày làm việc: ' + data.nextdate);
							if (data.appdate != undefined && data.appdate != null) {
								$('#appDate').html(data.appdate);
								$('#appDateHide').val(data.appdate);
							}
							if (data.lockdate != undefined && data.lockdate != null) {
								$('#date').val(data.lockdate);
							}
// 	 						$('#currentDate').html(data.nextdate);
// 	 						$('#nextDate').html(data.nextdate1);
	 						
	 					} else {
	 						$('#errMsg').html(data.errMsg).show();
// 	 						if (data.lockDate != undefined && data.lockDate != null) {
// 								$('#appDate').html(data.lockDate);
// 							}
// 	 						if (data.errMsg) {
// 	 							data.errMsg = data.errMsg.replace(/\[r\]/g, "<span style='color:#f00;'>");
// 	 							data.errMsg = data.errMsg.replace(/\[\/\]/g, "</span>");
// 	 							data.errMsg = data.errMsg.replace(/\\n/g, "<br/>");
// 	 						}
// 	 						$('#errMsgP1').html(data.errMsg);
// 	 						$('#errorPopup').dialog({
// 	 							title : 'Mở chốt ngày không thành công',
// 	 							width:450,
// 	 							height:'auto',
// 	 							onOpen: function(){
// 	 								$("#btnErrOk").bind('click',function(event) {
// 	 									$('#errorPopup').dialog('close');
// 	 								});
// 	 							}
// 	 						});
// 	 						$('#errorPopup').dialog('open');
	 					}
	 				},
	 				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
	 					$('#divOverlay').hide();
	 					StockIssued.xhrExport = null;
	 				}
	 			});
			});
		}
	});
    $('#warningPopup').dialog('open');
	return false;
}

</script>
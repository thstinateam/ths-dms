<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">Tiện ích</a>
		</li>
		<li><span>Chốt ngày Vansale</span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div class="Clear" style="height: 20px;"></div>
				<div class="SearchInSection SProduct1Form">
				 		<label class="LabelStyle" style="padding-left:15px">Đơn vị </label>
		         		<div class="BoxSelect BoxSelect2" id ="shopIdList" >
                        <select id="shopCodeCB" style="width: 200px;">
                        </select>
                    </div>
					<div class="Clear"></div>
				</div>		
					<div class="Clear"></div>
				
			<h2 class="Title2Style">Danh sách nhân viên</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id=gridContainer>
						<table id="grid"></table>
					</div>
				</div>
				<div class="SearchInSection SProduct1Form">
						<div class="BtnCenterSection" style="padding-top: 10px;">					
							<button class="BtnGeneralStyle" id="btnLockedStock"	onclick="return LockedStock.lockStockStaff();">Khóa</button>
							<button class="BtnGeneralStyle" id="btnUnlockedStock"	onclick="return LockedStock.unlockStockStaff();">Mở khóa</button>												
						</div>	
						<div class="Clear"></div>
						<p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>	
						<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
				</div>
			</div>

		</div>
	</div>
	
	<div class="Clear"></div>
</div>	
	<s:hidden id="shopId" name="shopId"></s:hidden>	
	<s:hidden id="staffCode" name="staffCode"></s:hidden>	
<script type="text/javascript">
$(document).ready(function() {		
	LockedStock.initUnitCbx();				
});
</script>
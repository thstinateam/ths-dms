<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);">Tiện ích</a>
		</li>
		<li><span>Khóa kho nhân viên</span>
		</li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div class="Clear" style="height: 20px;"></div>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style" style="width: 90px;">Mã NV</label>
					<div class="BoxSelect BoxSelect2">
						<select class="easyui-combobox" class="MySelectBoxClass" id="saleStaff" style="width: 190px">
							<option value=""></option>
							<s:iterator value="listSaleStaff" var="obj">
								<option value="<s:property value="#obj.staffCode" />">
									<s:property value="#obj.staffName" />
								</option>
							</s:iterator>
						</select>
					</div>
					<label class="LabelStyle Label1Style" style="width: 90px;">Trạng thái</label>
					<input id="vanLock" maxlength="50" type="text" readonly="readonly" class="InputTextStyle InputText1Style" value="" />
					<label class="LabelStyle Label1Style" style="width: 15px;"></label>
					<div class="BtnCenterSection" style="float:left; width: 120px; padding: 0px; margin: 0 auto;">
						<button id="btnLockStockStaff" class="BtnGeneralStyle" onclick="return StockCategory.lockStockStaff();">Khóa kho</button>
					</div>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear" style="min-height: 200px;"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>
<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
<s:hidden id="hiddenFirtStaffCode" name="staffCodeFirt"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	//StockCategory._lstStockLock = new Map();
	/*Lay map StockLock*/
	
// 	<s:iterator value="lstStockLock" var="obj">
// 		StockCategory._lstStockLock.put('<s:property value="#obj.staff.staffCode" />', '<s:property value="#obj.vanLock" />');
// 	</s:iterator>
	Utils.bindFormatOntextfieldCurrencyFor('vanLock', Utils._TF_NUMBER);
	disabled('vanLock');
	$('#saleStaff').combobox({			
		valueField : 'staffCode',
		textField : 'staffCode',
		data : Utils.getDataComboboxStaff('saleStaff', ''),
		//width:250,
		panelWidth:206,
		formatter: function(row) {
			return '<span style="font-weight:bold">' + Utils.XSSEncode(row.staffCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.staffName) + '</span>';
		},
		filter: function(q, row){
			q = new String(q).toUpperCase().trim();
			var opts = $(this).combobox('options');
			return row['searchText'].indexOf(unicodeToEnglish(q))>=0;
		},			
		onChange:function(newvalue,oldvalue){
			$('.ErrorMsgStyle').html('').change().show();
			if(newvalue!=undefined && newvalue!=null && newvalue.length>0){
				//CategoryCatalog.getStockLockByStaffCode(newvalue);
			 	//var flag = StockCategory._lstStockLock.get(newvalue);
			 	var params = {};
				params.staffCode = newvalue.trim();
				Utils.getJSONDataByAjax(params,"/catalog/utility/closing-day/get-locked-stock-by-staffCode",function(data){
					if(data!=undefined && data!=null && data.stockLock!=undefined && data.stockLock!=null){
						CategoryCatalog._stockLock = data.stockLock;
					 	if(data.stockLock != undefined && data.stockLock != null && 1 == activeType.parseValue(data.stockLock.vanLock)){
					 		enable('btnLockStockStaff');
					 		$('#vanLock').val('Chưa khóa');
					 	}else if(data.stockLock != undefined && data.stockLock != null){
					 		disabled('btnLockStockStaff');
					 		$('#vanLock').val('Đã khóa');
					 	}else{
					 		enable('btnLockStockStaff');
					 		$('#vanLock').val('Chưa khóa');
					 	}
					}
				},null,null);
			}else{
				$('#vanLock').val('');
				disabled('btnLockStockStaff');
			}
		}
	});
	$('.combo-arrow').unbind('click');
	setTimeout(function(){
		var firtItem = $('#hiddenFirtStaffCode').val();
		if(firtItem!=undefined  && firtItem!=null && firtItem.length>0){
			$('#saleStaff').combobox('setValue',firtItem.trim()).change();
		}
	}, 1500);
	
// 	if(StockCategory._lstStockLock!=null && StockCategory._lstStockLock.size()>0){
// 		var index = $('#saleStaff').combobox('getValue');
// 		if(index!=undefined && index!=null && index.length>0){
// 		 	var flag = StockCategory._lstStockLock.get(index);
// 		 	if(flag != undefined && flag != null && 1 == activeType.parseValue(flag.trim())){
// 		 		enable('btnLockStockStaff');
// 		 		$('#vanLock').val('Chưa khóa');
// 		 	}else if(flag != undefined && flag != null){
// 		 		disabled('btnLockStockStaff');
// 		 		$('#vanLock').val('Đã khóa');
// 		 	}else{
// 		 		enable('btnLockStockStaff');
// 		 		$('#vanLock').val('Chưa khóa');
// 		 	}
// 		}
// 	}
	
});

</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
       	<ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a href="javascript:void(0);">Tiện ích</a></li>
           <li><span>Chốt ngày</span></li>
    	</ul>
</div>
<div class="CtnOneColSection">
   <div class="ContentSection">
   		<div class="ToolBarSection">
           <div class="SearchSection GeneralSSection">
            	<div class="SearchInSection SProduct1Form" >
	            	<div class="">
	            		<label class="LabelStyle Label2Style"><s:text name="unit_tree.search_unit.tab_name"/>
                        	<span class="ReqiureStyle"><font color="red">(*)</font></span>
                    	</label>
	                    <div class="BoxSelect BoxSelect2">
							<div class="Field2">
								<input id="shop" type="text" data-placeholder="Chọn đơn vị"  style="width:216px; height: auto; margin-right: 70px;" class="InputTextStyle InputText1Style" />
							</div>
						</div>
							<button style="margin-bottom: 20px;" id="btnLock" class="BtnGeneralStyle" onclick="searchShop();"><span class="Sprite2"><s:text name="jsp.common.timkiem"/></span></button>
							<div class="Clear"></div>
	            	</div>
            	</div>
            	<div class="SearchInSection SProduct1Form" style="padding:0;">
	           		<h2 class="Title2Style"><s:text name="unit_tree.search_unit.search_result_header"></s:text></h2>
	           		<div class="GridSection" id="paramGrid">
	                  	<div id="searchResult" class="GeneralTable Table34Section">
							<p id="gridNoResult" style="display: none" class="WarningResultStyle"><s:text name="jsp.common.no.result"></s:text></p>
							<div class="BoxGeneralTTitle" id="dgGridContainer">
								<table id="grid"></table>
							</div>
						</div>
	              	</div>
	           	</div>
            </div>
        </div>
  	</div>
</div>
	<div style="display:none">
		<div id="warningPopup" class="easyui-dialog" title="Xác nhận" data-options="closed: true,modal: true">
			<div class="PopupContentMid">
				<div class="GeneralForm" >
	                <div class="icon-warning" style="width: 48px; height: 48px; float: left; margin-right: 20px;"></div>
	                <p style="font-weight: bold; padding: 2px 0px;"><s:text name="chot.ngay.title.ngay.lam.viec.se.la.ngay"/> <span id="nextDate" style="color: #FF6600;"><s:property value="nextDate"/></span>.</p>
	                <p style="font-weight: bold; padding: 2px 0px;"><s:text name="chot.ngay.title.tat.ca.thao.tac.trong.ngay"/> </p>
	                <p style="font-weight: bold; padding: 2px 0px; margin-left: 68px; margin-bottom: 10px;"><s:text name="chot.ngay.title.ban.co.muon.thuc.hien.thao.tac.nay.khong"/></p>
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
		                <button id="btnOk" class="BtnGeneralStyle"><s:text name="jsp.common.chon.simple"/></button>
						<button class="BtnGeneralStyle" id="btnCancel"><s:text name="jsp.common.dong"/></button>
	                </div>
 					<p id="errMsgSelectShop" class="ErrorMsgStyle" style="display: none"></p> 
				</div>
              </div>
		</div>
	</div>
	
	<div style="display:none">
		<div id="errorPopup" class="easyui-dialog" title="Xác nhận" data-options="closed: true,modal: true">
			<div class="PopupContentMid">
				<div class="GeneralForm" >
<!-- 	                <div class="icon-error" style="width: 50px; height: 50px; float: left; margin-right: 20px;"></div> -->
<!-- 	                <p style="font-weight: bold; padding: 2px 0px;" id="errMsgP1"></p> -->
	                <div id="common-dialog-grid-container" class="GridSection">
	                	<table id="common-dialog-grid-search"></table>
	                </div>
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
		                <button id="btnErrOk" class="BtnGeneralStyle"><s:text name="jsp.common.dong"/></button>
		                <button id="btnAutoShopLock" class="BtnGeneralStyle cmsiscontrol"><s:text name="jsp.common.chot.tu.dong"/></button>
	                </div>
				</div>
              </div>
		</div>
	</div>
	
	<div style="display:none">
		<div id="errorPopup1" class="easyui-dialog" title="Chốt ngày không thành công" data-options="closed: true,modal: true">
			<div class="PopupContentMid">
				<div class="GeneralForm" >
	                <div class="icon-error" style="width: 50px; height: 50px; float: left; margin-right: 20px;"></div>
	                <p style="font-weight: bold; padding: 2px 0px;"><span id="errMsgFail1"><s:text name="chot.ngay.title.lockdate.great.than.sysdate"/></span></p>
	                <p style="font-weight: bold; padding: 2px 0px;" id="errStock"><s:text name="chot.ngay.title.nhan.vien.vansale.chua.nhap.tra.kho"/>: <br><span id="errMsgFail2" style="color: #CC0000;  margin-left: 70px;"></span></p>
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
		                <button id="btnErrOk1" class="BtnGeneralStyle"><s:text name="jsp.common.dong"/></button>
	                </div>
				</div>
              </div>
		</div>
	</div>
	
	<div style="display:none">
		<div id="successPopup" class="easyui-dialog" title="Xác nhận" data-options="closed: true,modal: true">
			<div class="PopupContentMid">
				<div class="GeneralForm" >
	                <div class="icon-success" style="width: 46px; height: 46px; float: left; margin-right: 20px;"></div>
	                <p style="font-weight: bold; padding: 2px 0px;"><s:text name="chot.ngay.title.ngay.da.duoc.chot.thanh.cong"/></p>
	                <p style="font-weight: bold; padding: 2px 0px;"><s:text name="chot.ngay.title.ngay.lam.viec.se.la.ngay"/>: <span id="date1" style="color: #CC0000;"></span>.</p>
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
		                <button id="btnSuccessOk" class="BtnGeneralStyle"><s:text name="jsp.common.dong"/></button>
	                </div>
				</div>
              </div>
		</div>
	</div>
	
	
	<!-- Chot kho nhan vien -->
	<div style="display:none">
		<div id="warningPopupStockTrans" class="easyui-dialog" title="Xác nhận" data-options="closed: true,modal: true">
			<div class="PopupContentMid">
				<div class="GeneralForm" >
	                <div class="icon-warning" style="width: 48px; height: 48px; float: left; margin-right: 20px;"></div>
	                <p style="font-weight: bold; padding: 2px 0px; margin-left: 68px; margin-bottom: 10px;"><s:text name="chot.ngay.title.ban.co.muon.thuc.hien.chot.kho.nv.khong"/></p>
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
		                <button id="btnOk1" class="BtnGeneralStyle">Chọn</button>
						<button class="BtnGeneralStyle" id="btnCancel1"><s:text name="jsp.common.dong"/></button>
	                </div>
 					<p id="errMsgStock" class="ErrorMsgStyle" style="display: none"></p> 
				</div>
              </div>
		</div>
	</div>
	
	<div style="display:none">
		<div id="successPopupStockTrans" class="easyui-dialog" title="Xác nhận" data-options="closed: true,modal: true">
			<div class="PopupContentMid">
				<div class="GeneralForm" >
	                <div class="icon-success" style="width: 46px; height: 46px; float: left; margin-right: 20px;"></div>
	                <p style="font-weight: bold; padding: 2px 0px;"><s:text name="chot.ngay.title.chot.kho.nhan.vien.thanh.cong"/></p>
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
		                <button id="btnSuccessOk1" class="BtnGeneralStyle"><s:text name="jsp.common.dong"/></button>
	                </div>
				</div>
              </div>
		</div>
	</div>
	
<p id="successMsg" class="SuccessMsgStyle" style="display: none;"></p>
<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
<%-- <s:hidden id="stockTrans" name="stockTrans"></s:hidden>      --%>
<s:hidden id="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>     
<script type="text/javascript">
$(document).ready(function() {
	initUnitCbx();
	$("#shop").kendoMultiSelect({
        dataTextField: "shopCode",
        dataValueField: "shopId",
        filter: "contains",
		itemTemplate: function(data, e, s, h, q) {
			var level = data.isLevel;
			if(level == 1) {//VNM
				return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else if(level == 2) {
					return '<div class="tree-vnm" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:230px"><span class="tree-vnm-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vnm-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 3){
				return '<div class="tree-mien" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:220px;"><span class="tree-mien-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-mien-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 4){
				return '<div class="tree-vung" node-id="'+Utils.XSSEncode(data.shopId)+'" style="width:200px"><span class="tree-vung-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-vung-text">'+Utils.XSSEncode(data.shopCode) + ' - ' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}else if(level == 5){
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			} else {
				return '<div class="tree-npp" node-id="'+Utils.XSSEncode(data.shopId)+'" style="display:-moz-grid-group"><span class="tree-npp-node"><div style="width: 15px; display: inline-block;"></div></span><span class="tree-npp-text">'+Utils.XSSEncode(data.shopCode) + '-' + Utils.XSSEncode(data.shopName)+'</span></div>';
			}
		},
        tagTemplate:  '#: data.shopCode #',
        change: function(e) {
        	var lstShop=this.value();
        	var param='';
        	var param1='';
        	for(var i=0;i<lstShop.length;i++){
        		if(i==0){
        			param+='?lstShop='+lstShop[i];
        			param1+='?lstShopId='+lstShop[i];
        		}else{
        			param+='&lstShop='+lstShop[i];
        			param1+='&lstShopId='+lstShop[i];
        		}
        	}
        	//$('#staffSaleCode').val('');
        },
        dataSource: {
            transport: {
                read: {
                    dataType: "json",
                    url: "/rest/report/shop/kendo-ui-combobox-ho.json"
                }
            }
        },
        value: [$('#curShopId').val()]
    });

    var shopKendo = $("#shop").data("kendoMultiSelect");
    shopKendo.wrapper.attr("id", "shop-wrapper");
    
    var lstShopId = [$('#curShopId').val()];
	$("#grid").datagrid({
		url: "/catalog/utility/closing-day/get-list-shop",
		queryParams: {lstShopId : lstShopId},
		width: ($('#dgGridContainer').width()-17),
		height: 'auto',
		singleSelect: true,
		rownumbers: true,
		fitColumns: true,
		scrollbarSize: 0,
		pagination: true,
		pageList  : [10,20,30],
		columns: [[			
			{field: 'shopCode', title: jsp_common_shop_code, width: 80, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}},
			{field: 'shopName', title: jsp_common_shop_name, width: 120, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}},
			{field: 'lockDateStr', title: jsp_common_ngay_lam_viec, width: 50, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index){
				value = VTUtilJS.XSSEncode(value); 
				if (value != null && value != '') {
					return VTUtilJS.XSSEncode(value);
				}
				return '';
			}},
			{field: 'view', title: '', width: 50, align: 'center', sortable: false, resizable: false, formatter: function(value, row, index){
				return '<a href="javascript:void(0)" onclick="closingDay(\'' + row.shopCode + '\',\'' + row.lockDateStr + '\',\'' + row.nextLockDateStr + '\');">Chốt ngày</a>';
			}},
			{field: 'statusLogStr', title: 'Tình trạng chốt', width: 110, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index) {
				return VTUtilJS.XSSEncode(value);
			}},
			{field: 'errorTypeStr', title: 'Ghi nhận lỗi', width: 150, align: 'left', sortable: false, resizable: false, formatter: function(value, row, index) {
				return VTUtilJS.XSSEncode(value);
			}}
			
		]],
		onLoadSuccess: function(data) {
			$('.datagrid-header-rownumber').html(jsp_common_numerical_order);
			$(window).resize();
			Utils.updateRownumWidthAndHeightForDataGrid('grid');
			var lstHeaderStyle = ["apParamCode","apParamName","type","description","value","statusStr"];
			Utils.updateCellHeaderStyleSort('dgGridContainer',lstHeaderStyle);
			
			var arrEdit =  $('#dgGridContainer td[field="view"]');
			if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
			  for (var i = 0, size = arrEdit.length; i < size; i++) {
			  	$(arrEdit[i]).prop("id", "gr_closing_day_td_view_" + i);//Khai bao id danh cho phan quyen
				$(arrEdit[i]).addClass("cmsiscontrol");
			  }
			}
			Utils.functionAccessFillControl('dgGridContainer', function(data){
				//Xu ly cac su kien lien quan den cac control phan quyen
				var arrTmpLength =  $('#dgGridContainer td[id^="gr_closing_day_td_view_"]').length;
				var invisibleLenght = $('.isCMSInvisible[id^="gr_closing_day_td_view_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#grid').datagrid("showColumn", "view");
				} else {
					$('#grid').datagrid("hideColumn", "view");
				}
			});
			
		}
	});
});

function searchShop() {
	var multiShop = $("#shop").data("kendoMultiSelect");
	var dataShop = null;
	if (multiShop != null) {
		dataShop = multiShop.dataItems();
	}
	var lst = [];
	for(var i=0;i<dataShop.length;i++){
		lst.push(dataShop[i].shopId);
	}
	$('.ErrorMsgStyle').html('').hide();
	$('.SuccessMsgStyle').html('').hide();
	$("#grid").datagrid('load',{lstShopIdStr:lst.join(',')});
}

function closingDay(shopCode, lockDate, nextLockDate) {
	General._shopCode = Utils.XSSEncode(shopCode);
	$('#warningPopup').dialog({
		title : '<s:text name="jsp.common.xacnhan"/>',
		width:400,
		height: 'auto',
		onOpen: function(){
			$("#errMsgSelectShop").hide();
			$("#btnCancel").bind('click',function(event) {
				$('#warningPopup').dialog('close');
			});
			$('#nextDate').html(nextLockDate);
			$('#currentDate').html(lockDate != 'null' ? lockDate : '');
			$("#btnOk").unbind('click');
			$("#btnOk").bind('click',function(event) {
				$('#warningPopup').dialog('close');
	 			$('#errMsg').html('').hide();
	 			$('#divOverlay').show();
	 			var params = new Object();
	 			params.token = Utils.getToken();
	 			params.shopCode = General._shopCode;
	 			params.lockDateStr = lockDate;
	 			var kData = $.param(params, true);
	 			$.ajax({
	 				type : "POST",
	 				url : "/catalog/utility/closing-day/closed",
	 				data :(kData),
	 				dataType: "json",
	 				success : function(data) {
	 					$('#divOverlay').hide();
	 					Utils.updateTokenForJSON(data);
	 					if(!data.error) {
	 						searchShop();
	 						$('#date').html(data.lockdate);
	 						$('#date1').html(data.nextdate);
	 						$('#successPopup').dialog({
	 							title : '<s:text name="chot.ngay.title.chot.ngay.thanh.cong"/>',
	 							width:400,
	 							height: 'auto',
	 							onOpen: function(){
	 								$("#btnSuccessOk").bind('click',function(event) {
	 									$('#successPopup').dialog('close');
	 								});
	 							}
	 						});
	 						$('#successPopup').dialog('open');
	 						
	 						$('#shopLockDate').html('<s:text name="chot.ngay.title.ngay.lam.viec"/>: ' + data.nextdate);
	 						$('#dateLock').html(data.nextdate);
							
	 						$('#currentDate').html(data.nextdate);
	 						$('#nextDate').html(data.nextdate1);
	 						
	 					} else {
	 						if (data.SHOPLOCKLOG) {
	 							$('#errMsg').html(data.errMsg1).show();
	 						} else {
	 							var dataGridError = data.lstErrorCloseDay;
		 						if (dataGridError == null || dataGridError == undefined) {
		 							dataGridError = [];
		 						}
// 		 						$('#errMsgP1').html(data.errMsg);
								$('#common-dialog-grid-container').html('<table id="common-dialog-grid-search"></table>');
								$('#common-dialog-grid-search').datagrid({
									data: dataGridError,
							        rownumbers : true,
							        checkOnSelect: false,
									selectOnCheck: false,
							        scrollbarSize: 0,
							        autoWidth: true,
							        autoRowHeight : true,
							        fitColumns : true,
							        width : 650,
									columns:[[
										{field: 'loaiDH', title: 'Loại ĐH', width: 110, align: 'left', formatter: function(value, row, index) {
											return Utils.XSSEncode(value);
										}},
										{field: 'soLuong', title: 'Tổng số đơn', width: 80, sortable: false, align: 'right', formatter: function (value, row, index) {
											if (value != null && value != undefined) {
												return formatCurrency(value);												
											}
											return '';
										}},  
									    {field: 'thanhTien', title: 'Tổng thành tiền', width: 100, sortable: false, align: 'right', formatter: function(value, row, index) {
									    	if (value != null && value != undefined) {
												return formatCurrency(value);												
											}
											return '';
									    }},
									    {field: 'soDH', title: 'Số đơn hàng', width: 110, sortable: false, align: 'left', formatter: function(value, row, index) {
									    	return General.subStringByListagg(Utils.XSSEncode(value), ',', 3, '...');
									    }},
									    {field: 'ghiChu', title: 'Lỗi', width: 190, sortable: false, align: 'left', formatter: function(value, row, index) {
									    	return Utils.XSSEncode(value);
									    }}
									]],
							        onLoadSuccess :function(data){
							        	$('.datagrid-header-rownumber').html('<s:text name="price_stt"/>');
										Utils.updateRownumWidthAndHeightForDataGrid('common-dialog-grid-search');
							        }
								});
								
		 						$('#errorPopup').dialog({
		 							title : '<s:text name="chot.ngay.title.chot.ngay.khong.thanh.cong"/>',
		 							width : 700,
		 							height: 'auto',
		 							onOpen: function() {
		 								$('.ErrorMsgStyle').html('').hide();
		 								$('.SuccessMsgStyle').html('').hide();
		 								$("#btnErrOk").bind('click', function(event) {
		 									$('#errorPopup').dialog('close');
		 								});
		 								$('#btnAutoShopLock').bind('click', function(event) {
		 									$('#errorPopup').dialog('close');
		 									$.messager.confirm(jsp_common_xacnhan, '<s:text name="chot.ngay.msg.forch.close.day"/>', function(r) {
		 										if (r) {
		 											var paramFc = new Object();
		 											paramFc.token = Utils.getToken();
		 											paramFc.shopCode = General._shopCode;
		 											Utils.saveData(paramFc, '/catalog/utility/closing-day/change-shop-lock-log', null, 'errMsg', function(data) {
				 										$('#btnLock').click();
				 										$('.panel-tool-close').click();
				 										if (data != undefined && data != null && data.error != undefined && !data.error) {
															$('#successMsg').html('').hide();
															var tm = setTimeout(function() {
																$('#successMsg').html('').hide();
																clearTimeout(tm);
															}, 1500);
				 										}
				 									}, null, null, null, function(dtaError) {
				 										$('#btnLock').click();
				 										if (dtaError != undefined && dtaError != null && dtaError.errMsg != undefined && dtaError.errMsg != null) {
				 											$('#errMsg').html(dtaError.errMsg).show();
				 										}
				 									}, null);
		 										}
		 									});	
		 								});
		 							}
		 						});
		 						$('#errorPopup').dialog('open');
	 						}
	 					}
	 				},
	 				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
	 					$('#divOverlay').hide();
	 					StockIssued.xhrExport = null;
	 				}
	 			});
			});
		}
	});
    $('#warningPopup').dialog('open');
	return false;
}

function closingStockTrans(){
	$('#warningPopupStockTrans').dialog({
		title : '<s:text name="jsp.common.xacnhan"/>',
		width:400,
		height: 'auto',
		onOpen: function(){
			$("#errMsgStock").hide();
			$("#btnCancel1").bind('click',function(event) {
				$('#warningPopupStockTrans').dialog('close');
			});
			
			$("#btnOk1").unbind('click');
			$("#btnOk1").bind('click',function(event) {
				$('#warningPopupStockTrans').dialog('close');
	 			$('#errMsg').html('').hide();
	 			$('#divOverlay').show();
	 			var params = new Object();
	 			params.token = Utils.getToken();
	 			var kData = $.param(params, true);
				
	 			$.ajax({
	 				type : "POST",
	 				url : "/catalog/utility/closing-day/closed-stock-trans",
	 				data :(kData),
	 				dataType: "json",
	 				success : function(data) {
	 					$('#divOverlay').hide();
	 					Utils.updateTokenForJSON(data);
	 					if(!data.error) {
 							enable('btnLock');
	 						disabled('btnStockLock');

	 						$('#successPopupStockTrans').dialog({
	 							title : '<s:text name="chot.ngay.title.chot.ngay.thanh.cong"/>',
	 							width:400,
	 							height: 'auto',
	 							onOpen: function(){
	 								$("#btnSuccessOk1").bind('click',function(event) {
	 									$('#successPopupStockTrans').dialog('close');
	 								});
	 							}
	 						});
	 						$('#successPopupStockTrans').dialog('open');
	 						
	 					} else {
	 						$('.ErrorMsgStyle').html('').hide();
	 						$('.SuccessMsgStyle').html('').hide();
	 						if (data.SHOPLOCKLOG != undefined && data.SHOPLOCKLOG != null) {
	 							$('#errMsg').html(data.errMsg1);
	 							$('#btnLock').click();
	 						} else {
		 						$('#errStock').hide(data.errMsg1);	 						
		 						$('#errMsgFail1').html(data.errMsg1);
		 						$('#errorPopup1').dialog({
		 							title : '<s:text name="chot.ngay.title.chot.ngay.khong.thanh.cong"/>',
		 							width : 400,
		 							height: 'auto',
		 							onOpen: function(){
		 								$("#btnErrOk1").bind('click',function(event) {
		 									$('#errorPopup1').dialog('close');
		 								});
		 							}
		 						});
		 						$('#errorPopup1').dialog('open');
	 						}
	 					}
	 				},
	 				error:function(XMLHttpRequest, textStatus, errorDivThrown) {
	 					$('#divOverlay').hide();
	 					StockIssued.xhrExport = null;
	 				}
	 			});
			});
		}
	});
    $('#warningPopupStockTrans').dialog('open');
	return false;
}
function initUnitCbx(){				
	var params = {};
	$.ajax({
		type : "POST",
		url: '/commons/list-child-shop',
		data :($.param(params, true)),
		dataType : "json",
		success : function(data) {
			$('#shopCodeCB').combobox({
				valueField: 'shopCode',
				textField:  'shopName',
				data: data.rows,
				panelWidth: '200',
				formatter: function(row) {
					return '<span style="font-weight:bold">' + Utils.XSSEncode(row.shopCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.shopName) + '</span>';
				},
				filter: function(q, row){
					q = new String(q).toUpperCase().trim();
					var opts = $(this).combobox('options');
					return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) == 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) == 0) ;
				},	
		        onChange: function(newValue, oldValue) {
		        	onChangeShop();
		        },
		        onLoadSuccess: function(){
		        	var arr = $('#shopCodeCB').combobox('getData');
		        	if (arr != null && arr.length > 0){
		        		$('#shopCodeCB').combobox('select', arr[0].shopCode);
		        		SPCreateOrder._currentShopCode = arr[0].shopCode;
		        		var params = new Object();
		            	params.shopCode = arr[0].shopCode;
		            	Utils.getJSONDataByAjax(params,'/catalog/utility/closing-day/check', function(data) {
		            		if (data.appDate != undefined && data.appDate != null) {
		            			updateAllDate(data.appDate, data.nextDate);
		            		}
		            	});
		        	}
		        }
			});
		}
	});
}
function onChangeShop() {
	var params = new Object();
   	params.shopCode = $('#shopCodeCB').combobox('getValue');
   	Utils.getJSONDataByAjax(params,'/catalog/utility/closing-day/check', function(data) {
   		if (data.appDate != undefined && data.appDate != null) {
   			$('#shopCodeCB').combobox('setValue', params.shopCode);
   			updateAllDate(data.appDate, data.nextDate);
   		}
   	});
}
function updateAllDate(currentDate, nextDate){
	$('#dateLock').html(currentDate);
	$('#currentDate').html(currentDate);
	$('#shopLockDate').html('<s:text name="chot.ngay.title.ngay.lam.viec"/>: ' + currentDate);
		$('#nextDate').html(nextDate);
}
</script>
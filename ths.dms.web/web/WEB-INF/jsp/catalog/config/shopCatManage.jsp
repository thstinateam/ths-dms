<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import="com.viettel.core.entities.enumtype.ProductType" %>
<div class="BreadcrumbSection">
    <ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="#"><s:text name="organization_system"/></a></li>
       	<li><span><s:text name="catalog.manager"/></span></li>
    </ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div class="Sidebar1Section" style="width: 24.9%;">
					<h2 class="Title2Style" id="title1" style="position:relative"><s:text name="catalog.manager"/></h2>
					<div class="SearchInSection SProduct1Form">
						<div class="ReportTreeSection ReportTree2Section" style="padding:10px 5px 0px">
							<div id="treeContainer" class="ScrollSection" style="min-height:471px;max-height:726px;overflow-y:auto">
								<div id="tree"></div>
							</div>
						</div>
					</div>	
				</div>
				<div style="width: 0px ! important; height: 660px;" class="Content1Section"></div>
				<div class="Content3Section Content3NBTSection" style="width: 74.9%; min-width:750px;">
					<div class="ReportCtnSection">
						<div class="SearchInSection SProduct1Form">
							<!-- Bo sung combo kho -->
							<div class="Clear"></div>
							<div id="whTypeCover" class="BoxSelect BoxSelect5" style="margin-top: 9px; margin-left:10px;" >
						    		<select id="cbWhType"  class="MySelectBoxClass" onchange="$('#btSearch').click();">
						    			<option value="0" selected="selected" ><s:text name="jsp.cb.imp.kct" /></option>
						    			<option value="1"><s:text name="jsp.cb.imp.kbh"/></option>
						    		</select>
							</div>
							<div class="SearchInSection SProduct1Form" id="divSearch" style="display: none">	
								<input type="text" class="InputTextStyle InputText1Style" id="pdCodeName" autofocus="autofocus" style="width:300px;position:relative;margin-left:5px;"  placeholder="<s:text name="cat.manage.input.search"/>" />
								<a id="btSearch" title='<s:text name="jsp.common.timkiem"/>' href="javascript:void(0)" style="position: absolute; margin-left: 10px;"  onclick="CatalogManager.searchCat();"><img src="/resources/images/icon-view.png"/></a>
							</div>
						</div>
						<div class="Clear"></div>
						<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
						<div class="Clear"></div>
						<div class="SearchInSection SProduct1Form">
			                   <div class="Clear"></div>	
							<div class="GridSection" id="datasourceDiv">	                 					
								<table id="dg"></table>
								<div class="Clear"></div>
							</div>
							<div class="Clear"></div>
							<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>	
			                <p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>					
						</div>
						<div class="Clear"></div>
					</div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>

<div id ="shopCatDialog" style="width:600px;visibility: hidden;" >
	<div id="shopCatEasyUIDialog" class="easyui-dialog" title="<s:text name="cat.manage.popup.title"/>" data-options="closed:true,modal:true, width: 800, height: 'auto', draggable: false">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<div style="width:100%">
					<label class="LabelStyle Label1Style" style=" width: 150px;"><s:text name="cat.manage.grid.tt.code"/><span class="ReqiureStyle"> *</span></label> <!-- CAT CODE -->
					<input id="catCode" autofocus maxlength="20" tabindex="1" type="text" style=" width: 194px;" class="InputTextStyle InputText1Style" /> 
					<label class="LabelStyle Label1Style" id="lblPopCatName" style=" width: 150px;"><s:text name="cat.manage.title.cat"/><span class="ReqiureStyle"> *</span></label> <!-- CAT/SUB CAT NAME --> 
					<input id="catName" type="text" maxlength="200" tabindex="2" style="width: 194px;"  class="InputTextStyle InputText1Style" />
					<input type="hidden" id="popCatType" /> 
				</div>
				<div class="Clear"></div>
				<div style="width:100%" id="coverCbParentCat" style="display: none;">
					<label class="LabelStyle Label1Style" id="lblPopupParentCat"  style="width: 150px;"><s:text name="cat.manage.title.parent"/><span class="ReqiureStyle"> *</span></label> <!-- CAT COMBO -->
					<div class="BoxSelect BoxSelect2" style="width:156px;">
						<select class="MySelectBoxClass" style="width: 205px;" tabindex="2" id="popupParentCat"></select>
					</div>
					<div class="Clear"></div>
				</div>
				<div style="width:100%" id="staffTypeContainer" style="display: none;">
					<label class="LabelStyle Label1Style" id="lblStaffType"  style="width: 150px;"><s:text name="cat.manage.staff.type.title"/><span class="ReqiureStyle"> *</span></label>
					<div class="BoxSelect BoxSelect2" style="width:156px;">
						<select class="MySelectBoxClass" style="width: 205px;" tabindex="2" id="cbxStaffType"></select>
					</div>
					<div class="Clear"></div>
				</div>
				<div class="Clear"></div>
				<div id="divDescription" style="width:100%">
					<label class="LabelStyle Label1Style" style=" width: 150px;"><s:text name="jsp.khuyenmai.ghichu"/></label> <!-- DESCRIPTION -->
					<input id="catDescript" type="text" maxlength="1000" tabindex="3" style="width: 560px;"  class="InputTextStyle InputText1Style" />
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnSearchOnDialog" onclick="return CatalogManager.saveCatalogManager()" tabindex="3" ><s:text name="jsp.save.data.import.file"/></button>
					<button class="BtnGeneralStyle BtnSearchOnDialog" onclick="$('#shopCatEasyUIDialog').dialog('close');" tabindex="3" ><s:text name="raise.error.close"/></button>
					<div class="Clear"></div>
					<p id="errMsgPopup" class="ErrorMsgStyle" style="display: none;"></p>
				</div>
				<div class="Clear"></div>

				<p id="errMsg" class="ErrorMsgStyle" style="display: none;"></p>
				<p id="sucExcelMsg" class="SuccessMsgStyle" style="display:none;"></p>
			</div>
		</div>
	</div>
</div>
<s:hidden id="catType"></s:hidden>
<input type="hidden" id="hideCatCode" />

<script type="text/javascript">
$(document).ready(function(){
// 	$('#cbWhType').customStyle();
	$('#whTypeCover').hide();
	$('#lbWhType').hide();
	$('#divSearch').hide();
	
	CatalogManager.gridTtNo = '<s:text name="jsp.common.numerical.order" />';
	CatalogManager.gridTtUnit = '<s:text name="khai.bao.danh.muc.don.vi.tinh" />';
	CatalogManager.gridTtCatCode = '<s:text name="cat.manage.title.cat" />';
	CatalogManager.gridTtSubCatCode = '<s:text name="cat.manage.title.sub.cat" />';
	CatalogManager.gridTtBrand = '<s:text name="cat.manage.title.brand" />';
	CatalogManager.gridTtFlavour = '<s:text name="cat.manage.title.flavour" />';
	CatalogManager.gridTtPackage = '<s:text name="cat.manage.title.packing" />';
	CatalogManager.gridTtCode = '<s:text name="cat.manage.grid.tt.code" />';
	CatalogManager.gridTtDescript = '<s:text name="jsp.khuyenmai.ghichu" />';
	CatalogManager.gridTtParentCat = '<s:text name="cat.manage.title.parent" />';
// 	CatalogManager.gridTtCustomerType = '<s:text name="khai.bao.danh.muc.loai.khach.hang" />';
// 	CatalogManager.gridTtAlbum = '<s:text name="khai.bao.danh.muc.album" />';
// 	CatalogManager.gridTtImportStock = '<s:text name="khai.bao.danh.muc.nhap.kho" />';
// 	CatalogManager.gridTtExportStock = '<s:text name="khai.bao.danh.muc.xuat.kho" />';
	CatalogManager.thongTin = '<s:text name="khai.bao.danh.muc.thong.tin" />';
	CatalogManager.gridBtTtAdd = '<s:text name="manager.customer.detail.add.new"/>';
	CatalogManager.gridBtTtChange = '<s:text name="stock.update.change"/>';
	CatalogManager.gridTtProblemsList = '<s:text name="jsp.van.de.list.title"/>';
	CatalogManager.gridTtProblemsListGtt = '<s:text name="jsp.van.de.grid.title"/>';
	CatalogManager.gridTtExpiryDate = '<s:text name="cat.manage.expiry.date"/>';
	CatalogManager.gridTtCustomerSalePosition = '<s:text name="cat.manage.customer.position"/>';
	CatalogManager.gridTtStaffSaleType = '<s:text name="cat.manage.staff.type.sale.title"/>';
	CatalogManager.gridTtPositionSuperviseStaffType = '<s:text name="cat.manage.staff.type.position.supervise.tree.title"/>';
	CatalogManager.gridTtStaffType = '<s:text name="cat.manage.staff.type.title"/>';
	CatalogManager.gridTtFocusProductType = '<s:text name="cat.manage.cttt.focus.product.title"/>';
	CatalogManager.gridTtReasonUpdateInvoice = '<s:text name="cat.manage.reason.update.invoice.title"/>';
	CatalogManager.gridTtOrderPriority = '<s:text name="cat.manage.order.priority"/>';

	CatalogManager.loadCatalogTree('/config/get-catalog-tree');
	var heightContent = window.innerHeight -132;
	$('.SidebarInSection').css({height:heightContent + 'px'});
	$('#pdCodeName').bind('keyup',function(event){	
		if(event.keyCode == keyCodes.ENTER){
			$('#btSearch').click();
		}
	});
	
	$(document).bind('keyup',function(event){	
		if(event.keyCode == keyCodes.F5){
			$('#catType').val(1).change();
		}
	});
});
</script>
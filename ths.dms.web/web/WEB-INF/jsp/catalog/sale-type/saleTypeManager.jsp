<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="Breadcrumbs">
    <span class="Breadcrumbs1Item Sprite1">Thông tin hình thức bán hàng</span>
</div>
<div class="MainInContent">
	<div class="MainInTopContent">
	   	<div class="MainInBtmContent">
	           <div class="FirstSidebar">
	           	<div class="TreeViewSection">
	                   <h3 class="Sprite2"><span class="Sprite2">CÂY HÌNH THỨC BÁN HÀNG</span></h3>
	         	<div class="GeneralMilkInBox">
	         	<div id="treeContainer" class="ScrollSection">   
	         	<div id="tree"></div>                         
	         	</div>           
	         </div>
	     </div>
	 </div>
	 <div class="Content">
	     <div class="GeneralMilkBox">
	         <div class="GeneralMilkTopBox">
	             <div class="GeneralMilkBtmBox">
	                 <h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin tìm kiếm</span></h3>
	                 <div class="GeneralMilkInBox ResearchSection">
		                 <div class="ModuleList17Form">
                             <label class="LabelStyle Label1Style">Mã HTBH</label>
                             <div class="Field2">
	                             <input id="staffTypeCode" type="text" class="InputTextStyle InputText1Style" maxlength="20" />
	                             <span class="RequireStyle">(*)</span>
                             </div>
                             <label class="LabelStyle Label2Style" style="width: 110px; padding-left: 40px">Mã hình thức cha</label>
                             <div class="BoxSelect BoxSelect1">
				           		<input id="parentCode" style="width:200px;">
                             </div>
                             <div class="Clear"></div>
                             <label class="LabelStyle Label1Style">Tên HTBH</label>
                             <div class="Field2">
			                     <input id="staffTypeName" value="" type="text" class="InputTextStyle InputText1Style" maxlength="100" />
			                     <span class="RequireStyle">(*)</span>
		                     </div>
                             <label class="LabelStyle Label2Style" style="width: 110px; padding-left: 40px">Trạng thái</label>
		                     <div class="BoxSelect BoxSelect2">
			                     <div class="Field2">
			                     	<select id="status" class="MySelectBoxClass">
				                     	<option value="-2">---Chọn trạng thái---</option>
				                     	<option value="0">Tạm ngưng</option>
				                     	<option value="1" selected="selected">Hoạt động</option>
			                     	</select>
			                         <span class="RequireStyle">(*)</span>
			                     </div>
		                     </div>
		                     <div class="Clear"></div>
                             <div class="ButtonSection">
		                     	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return SaleTypeCatalog.search();"><span class="Sprite2">Tìm kiếm</span></button>
			                	<button id="btnCreate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return Utils.getChangedForm(null,true,-1);"><span class="Sprite2">Thêm mới</span></button>
			                	<button style="display: none;" id="btnUpdate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return SaleTypeCatalog.change();"><span class="Sprite2">Cập nhật</span></button>
			                	<button style="display: none;" id="btnDismiss" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return SaleTypeCatalog.resetForm();"><span class="Sprite2">Bỏ qua</span></button>
			                	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
			                 </div>
			                 <p id="errMsg" style="display: none;" class="ErrorMsgStyle Sprite1">Có lỗi xảy ra khi cập nhật dữ liệu</p>
			                 <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                         </div>
	                 </div>
	             </div>
	         </div>
	     </div>
	     <div class="GeneralMilkBox">
	         <div class="GeneralMilkTopBox">
	             <div class="GeneralMilkBtmBox">
	                 <h3 class="Sprite2"><span class="Sprite2">Danh sách hình thức bán hàng</span></h3>
	                 <div class="GeneralMilkInBox">
	                     <div class="ResultSection" id="staffTypeGrid">
		                    <p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
		                    <table id="grid"></table>
							<div id="pager"></div>
		                </div>
                     </div>
                 </div>
             </div>
         </div>
         </div>
         <div class="Clear"></div>
		</div>
   </div>
</div>    
<s:hidden id="staffTypeId" value="0"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#layoutContent').removeClass('Content');
	$('#layoutContent').addClass('MainContent');
	$('.MySelectBoxClass').customStyle();
	$('.RequireStyle').hide();
	$('#staffTypeCode').focus();
	$('#staffTypeName').val('');
	$("#grid").jqGrid({
	  url:SaleTypeCatalog.getGridUrl('',-1,'',1,false),
	  colModel:[		
	    {name:'channelTypeCode', label: 'Mã hình thức bán hàng', width: 90, sortable:false,resizable:false, align: 'left' },
	    {name:'channelTypeName', label: 'Tên hình thức bán hàng', sortable:false,resizable:false , align: 'left'},
	    {name:'parentChannelType.channelTypeCode', label: 'Mã hình thức cha', width:90, align: 'left', sortable:false,resizable:false },
	    {name:'parentChannelType.channelTypeName', label: 'Tên hình thức cha', width: 140, align: 'left',sortable:false,resizable:false },
	    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: SaleTypeCatalogFormatter.editCellIconFormatter}
	  ],	  
	  pager : '#pager',
	  height: 'auto',
	  rownumbers: true,	  
	  width: ($('#staffTypeGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});
	$('#jqgh_grid_rn').prepend('STT');
	$('#parentCode').combotree({url: '/rest/catalog/sale-type/combotree/0/0.json'});
	$('#parentCode').combotree('setValue', -1);
	SaleTypeCatalog.loadTree();
	$('#treeContainer').jScrollPane();
});

</script>
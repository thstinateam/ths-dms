<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="BreadcrumbSection">
      <ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a href="#">Danh mục</a></li>
           <li><span>Quản lý xe</span></li>
      </ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 id = "titleCarId" class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Số xe <span id="errIU" class="ReqiureStyle"></span></label> 
					<input id="number" type="text" class="InputTextStyle InputText1Style" maxlength="20"/> 
					<label class="LabelStyle Label1Style">Trọng tải (kg)</label> 
					<input id="gross" type="text" class="InputTextStyle InputText1Style" onkeypress="return numbersonly(event,false);" maxlength="5"/> 
					<label class="LabelStyle Label1Style">Hiệu xe</label>
					<div class="BoxSelect BoxSelect2">
						<select id="label" class="MySelectBoxClass">
							<option value="-1" selected="selected">Chọn tất cả</option>
							<s:iterator value="lstCarLabel">
									<option value='<s:property value="id"/>'><s:property value="channelTypeCode"/> - <s:property value="channelTypeName"/></option>
							</s:iterator>
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Loại xe</label>
					<div class="BoxSelect BoxSelect2">
						<select id="type" class="MySelectBoxClass">
							<option value="-1" selected="selected">Chọn tất cả</option>
							<s:iterator value="lstCarType">
								<option value='<s:property value="apParamCode"/>'><s:property value="apParamCode"/> - <s:property value="apParamName"/></option>
							</s:iterator>
						</select>
					</div>
					<label class="LabelStyle Label1Style">Chủng loại</label>
					<div class="BoxSelect BoxSelect2">
						<select id="category" class="MySelectBoxClass">
							<option value="-1" selected="selected">Chọn tất cả</option>
							<s:iterator value="lstCarCategory">
									<option value='<s:property value="apParamCode"/>'><s:property value="apParamCode"/> - <s:property value="apParamName"/></option>
							</s:iterator>
						</select>
					</div>
					<label class="LabelStyle Label1Style">Nguồn gốc</label>
					<div class="BoxSelect BoxSelect2">
						<select id="origin" class="MySelectBoxClass">
							<option value="-1" selected="selected">Chọn tất cả</option>
							<s:iterator value="lstCarOrigin">
									<option value='<s:property value="apParamCode"/>'><s:property value="apParamCode"/> - <s:property value="apParamName"/></option>
							</s:iterator>
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Trạng thái<span id="errIU" class="ReqiureStyle"></span></label>
					<div class="BoxSelect BoxSelect2">
						<select id="status" class="MySelectBoxClass">
							<option value="-2">Chọn trạng thái</option>
							<option value="0">Tạm ngưng</option>
							<option value="1" selected="selected">Hoạt động</option>
						</select>
					</div>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle BtnMSection" onclick="return CarTypeCatalog.search();" >Tìm kiếm</button>
						<button style="display: none;" id="btnUpdate" class="BtnGeneralStyle BtnMSection" onclick="return CarTypeCatalog.change();">Cập nhật</button>
						<button style="display: none;" id="btnDismiss" class="BtnGeneralStyle" onclick="return CarTypeCatalog.resetForm();" >Bỏ qua</button>
					</div>
					<div class="Clear"></div>
					<p class="ErrorMsgStyle" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
		            <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
				</div>
				<h2 class="Title2Style">Danh sách xe</h2>
				<div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer">
						<div class="ResultSection" id="carTypeGrid">
		                    <p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
		                    <table id="grid"></table>
							<div id="pager"></div>
		                </div>
		                <div class="Clear"></div>
					</div>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
	<div id="responseDiv" style="display: none"></div>
	<div class="Clear"></div>
</div>

<s:hidden id="carTypeId" value="0"></s:hidden>
<%-- <s:hidden id="shopCodeHidden" name="staff.shop.shopCode"></s:hidden>
<s:hidden id="shopCode" name="staff.shop.shopCode"></s:hidden>
<s:hidden id="shopIdHidden" name="staff.shop.id"></s:hidden> --%>
<s:hidden id="shopCodeHidden" name="shop.shopCode"></s:hidden>
<s:hidden id="shopCode" name="shop.shopCode"></s:hidden>
<s:hidden id="shopIdHidden" name="shop.id"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#layoutContent').removeClass('Content');
	$('#layoutContent').addClass('MainContent');
	//$('.MySelectBoxClass').customStyle();
	Utils.bindAutoSearchUpdateBoxSelect();
	$('.RequireStyle').hide();
	$('#number').focus();
	var type = -1;
	var number = '';
	var label = -1;
	var gross = '';
	var category = -1;
	var origin = -1;
	var status = $('#status').val().trim();
	var params =  new Array();
	params.type = type;
	params.number =  number;
	params.label = label;
	params.gross = gross;
	params.category = category;
	params.origin = origin;
	params.status = status;
	$('#grid').datagrid({
		url : '/catalog/car-type/search',
		autoRowHeight : true,
		rownumbers : true, 
		pagination:true,
		rowNum : 10,
		fitColumns:true,
		pageList  : [10,20,30],
		singleSelect : true,
		scrollbarSize:0,
		queryParams:params,
		width: $('#gridContainer').width()-50,
	    columns:[[	 
	  		{field:'carNumber', title: 'Số xe', width: 60, align: 'left', sortable:false,resizable:true,
	  			formatter: function(cellvalue, rowObject,options){
	  				if(rowObject.carNumber!=undefined && rowObject.carNumber.length>0){
	  					return Utils.XSSEncode(rowObject.carNumber);
	  				}return '';
	  			}
	  		},
			{field:'gross', title: 'Tải trọng', width: 60, align: 'right', sortable:false,resizable:true,formatter:CarTypeCatalogFormatter.grossFormat },
			{field:'channelTypeName', title: 'Hiệu xe', width: 100, align: 'left', sortable:false,resizable:true,formatter:CarTypeCatalogFormatter.channelTypeNameFormat},
		    {field:'type.value', title: 'Loại xe', sortable:false,resizable:true , width: 120, align: 'left',formatter:CarTypeCatalogFormatter.typeFormat},
			{field:'category.value', title: 'Chủng loại', width: 100, align: 'left', sortable:false,resizable:true,formatter:CarTypeCatalogFormatter.categoryFormat},
		    {field:'origin.value', title: 'Nguồn gốc', width: 100, align: 'left', sortable:false,resizable:true,formatter:CarTypeCatalogFormatter.originFormat},	
			{field:'status.value', title: 'Trạng thái', width: 100, align: 'left', sortable:false,resizable:true,formatter:CarTypeCatalogFormatter.statusFormat},
		    {field:'edit', title:'<a title="Thêm mới" href= "javascript:void(0)" onclick= "return Utils.getChangedForm();"><img src="/resources/images/icon_add.png"/></a>',
		    	width: 20, align: 'center',sortable:false,resizable:true, formatter:CarTypeCatalogFormatter.editCellIconFormatter},
		    {field:'id',hidden: true}
	    ]],	    
	    onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');	    	
	    	 updateRownumWidthForJqGrid('.easyui-dialog');
  		 $(window).resize();    		 
	    }
	});
});

</script>
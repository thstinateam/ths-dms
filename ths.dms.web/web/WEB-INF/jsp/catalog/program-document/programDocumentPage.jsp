<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="#">Danh mục</a></li>
		<li><span>Quản lý công văn</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style" style="width: 50px;">Loại CV</label> 
					<div class="BoxSelect BoxSelect6">
						<select id="typeDocument" class="MySelectBoxClass">
							<option value="-2">Tất cả</option>
							<option value="1">Công văn CTKM</option>
							<option value="2">Công văn CTTB</option>
							<option value="3">Công văn CTTT</option>
						</select>
					 </div>
					<input id="typeDocumentHide" type="hidden"  maxlength="50" />				 
					<label class="LabelStyle Label1Style" >Mã CV</label>
					<input id="documentCode" tabindex="1" type="text"  class="InputTextStyle InputText4Style" maxlength="50" style="width: 150px;" />
					<input id="documentCodeHide" type="hidden"  maxlength="50" />					
					<label class="LabelStyle Label1Style">Tên CV</label> 
					<input	type="text" tabindex="2" id="documentName" class="InputTextStyle InputText1Style" maxlength="250" style="width: 150px;" />
					<input id="documentNameHide" type="hidden"  maxlength="50" />
					<label class="LabelStyle Label2Style">Thời gian</label>										
					<input	type="text" tabindex="3" id="fromDate" maxlength="10" class="InputTextStyle InputText6Style" value="<s:property value="fromDate" />" style="width: 75px;" />
					<input id="fromDateHide" type="hidden"  maxlength="50" />
					<input	type="text" tabindex="4" id="toDate"  maxlength="10" class="InputTextStyle InputText6Style" value="<s:property value="fromDate" />" style="width: 75px; margin-left: 20px" />
					<input id="toDateHide" type="hidden"  maxlength="50" />
					<div class="BtnLSection" style="padding-left: 20px">
						<button tabindex="5" id="btnSearch" class="BtnGeneralStyle cmsiscontrol" style="height: 24px; font-size: 11px; padding: 0 10px !important;" onclick="return ProgramDocument.search()" >Tìm kiếm</button>
					</div>		
					<div class="Clear"></div>
					<p class="ErrorMsgStyle" style="display: none;" id="errMsg" />
					<div class="Clear"></div>
				</div>
				<h2 class="Title2Style">Danh sách công văn</h2>
				<div class="GridSection">
					<table id="dg"></table>
					<div class="Clear"></div>
				</div>
				<div class="SearchInSection SProduct1Form">
					<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none; float: left; margin: 0px; padding-left: 10px;"></p>
					<p id="successExcelMsg" class="SuccessMsgStyle" style="display: none; float: left; margin: 0px; padding-left: 10px;"></p>
					<div class="BtnCSection" style="float: right; padding-bottom: 10px;">
						<button id="btnExport" class="BtnGeneralStyle cmsiscontrol" style="height: 24px; font-size: 11px; padding: 0 10px !important;" onclick="return ProgramDocument.exportExcelOfficeDocument();">Xuất Excel</button>
						<div class="Clear"></div>
					</div>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div style="display: none;">
<div id="easyuiPopup" class="easyui-dialog"	title="Xem công văn" data-options="closed:true,modal:true" style="width: 800px;">
	<div class="PopupContentMid2" style="width: 780px;">
		<div class="GeneralForm Search1Form" style="width:780px;">	
			<div id="contentImgDocument" style="width: 100%; min-height:500px; height:auto; text-align: center;">
				<img id="imgDocument" src="" />
			</div>
			<div id="buttonViewDocument" class="SearchInSection SProduct1Form">
				<div class="BtnCenterSection" style="width: 100%; text-align: center;">				
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('#easyuiPopup').window('close');">Đóng</button>
					<button id="rotateButton" class="BtnGeneralStyle BtnGeneralLStyle">Xoay</button>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div style="display: none;">
	<div id="easyuiPopupUpload" class="easyui-dialog" title="Upload công văn" data-options="closed:true,modal:true" style="width: 540px;">
		<div class="PopupContentMid2" >
			<div class="GeneralForm Search1Form" >
				<form action="/document/importPdf-OfficeDocument" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
					<input type="hidden" name="token" value="<s:property value="token" />" id="importTokenVal"  />
					<input type="hidden" id = "checkSubmit" name="checkSubmit">
					<label class="LabelStyle Label11Style" style="width: 75px">Mã CV<span class="ReqiureStyle">*</span> </label>
					<input type="text" tabindex="6" style="width: 345px;" class="InputTextStyle InputText2Style" maxlength="50" name="documentCode" id="documentCodeDlg" />
					<div class="Clear"></div>
					<label class="LabelStyle Label11Style" style="width: 75px">Tên CV<span class="ReqiureStyle">*</span> </label>
					<input type="text" tabindex="7" style="width: 345px;" class="InputTextStyle InputText2Style" maxlength="250" id="documentNameDlg" name="documentName" />
					<div class="Clear"></div>
					<label class="LabelStyle Label11Style" style="width: 75px;">Loại CV<span class="ReqiureStyle">*</span> </label>
					<div class="BoxSelect BoxSelect6" style="width: 147px; height: 23px;">
						<select name="typeDocument" id="typeDocumentDlg" class="BoxSelect BoxSelect10" style="width: 147px; cursor: pointer;" onchange="return ProgramDocument.onchangeType();">
							<option value="-2">Chọn loại CV</option>
							<option value="1">Công văn CTKM</option>
							<option value="2">Công văn CTTB</option>
							<option value="3">Công văn CTTT</option>
						</select>
					</div>
					<div class="Clear"></div>
					<label class="LabelStyle Label11Style" style="width: 75px;">Thời gian<span class="ReqiureStyle">*</span>
					</label>
					<input type="text" tabindex="8" id="fromDateDlg" style="width: 136px;" name="fromDate" maxlength="10" class="InputTextStyle InputText2Style" value="<s:property value="fromDate" />" />
					<input type="text" tabindex="9" id="toDateDlg" style="margin-left: 20px; width: 136px;" name="toDate" maxlength="10" class="InputTextStyle InputText2Style" value="<s:property value="fromDate" />" />
					<div class="Clear"></div>
					<label class="LabelStyle Label11Style" style="width: 75px;">File Pdf<span class="ReqiureStyle">*</span> </label>
					<div class="UploadFileSection Sprite1" style="width: 424px;">
						<input id="fakePdffilepc" type="text" class="InputTextStyle InputText1Style" style="width: 345px;" readonly="readonly">
						<input type="file" tabindex="10" class="UploadFileStyle" size="1" name="pdfFile" id="pdfFile" onchange="uploadPdfFile(this,'importFrm');" style="float: left; width: 100px;" />
					</div>					
				</form>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button tabindex="11" class="BtnGeneralStyle" onclick="return ProgramDocument.importPDFFile();">Tải lên</button>
					<button tabindex="12" class="BtnGeneralStyle" onclick="return ProgramDocument.closeWindow();">Đóng</button>
				</div>
			</div> 
			<div class="Clear"></div>
				<p style="display: none; margin-top: 10px; margin-left: 10px" class="ErrorMsgStyle SpriteErr" id="errPdfMsg"></p>
				<p style="display: none; margin-top: 10px; margin-left: 10px" class="SuccessMsgStyle" id="resultPdfMsg"></p>
				<p style="display: none; margin-top: 10px; margin-left: 10px" class="SuccessMsgStyle" id="resultSapce"></p>
		</div>
	</div>
</div>
<div id="searchStyleShopDisplay" style="display: none;">
	<div id="searchStyleShopDisplay1" class="easyui-dialog" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">
				<label id="seachStyleShopCodeLabel" class="LabelStyle Label2Style">Mã</label>
				<input id="seachStyleShopCode" maxlength="50" tabindex="13"
					type="text" class="InputTextStyle InputText5Style" /> <label
					id="seachStyleShopNameLabel" class="LabelStyle Label2Style">Tên</label>
				<input id="seachStyleShopName" type="text" maxlength="250"
					tabindex="14" class="InputTextStyle InputText5Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle Sprite2 BtnSearchOnDialog" tabindex="15" id="btnSearchStyleShop">
						<span class="Sprite2">Tìm kiếm</span>
					</button>
				</div>
				<s:hidden id="searchStyleShopUrl"></s:hidden>
				<s:hidden id="searchStyleShopCodeText"></s:hidden>
				<s:hidden id="searchStyleShopNameText"></s:hidden>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle">Danh sách đơn vị</h2>
<!-- 				<div id="loadingTree" class="LabelStyle Label2Style"><img src='/resources/images/loading.gif'/></div> -->
				<div class="GridSection" id="searchStyleShopContainerGrid">
					<table id="searchStyleShopGrid" class="easyui-treegrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">					
						<button id="btnSaveShopMap" tabindex="16" onclick="return ProgramDocument.updateDocumentShopMap();" class="BtnGeneralStyle">Cập nhật</button>
					<button class="BtnGeneralStyle" tabindex="17" onclick="return $('#searchStyleShopDisplay1').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgDialog" class="ErrorMsgStyle" style="display: none"></p>
				<p style="display: none; margin-left: 10px" class="SuccessMsgStyle" id="resultInsertShop"></p>
			</div>
		</div>
	</div>
</div>

<s:hidden id="selId" name="id"></s:hidden>
<s:hidden id="shopIDAd" name="shopIDAd"></s:hidden>
<s:hidden id="nodeG" name="nodeG"></s:hidden>
<input type="hidden" name="nameDocumentIDGrid" id="idDocumentIDGrid" value="0">
<script type="text/javascript">
$(document).ready(function(){
	$('#typeDocumentHide').val($('#typeDocument').val().trim());
	$('#documentCodeHide').val($('#documentCode').val().trim());
	$('#documentNameHide').val($('#documentName').val().trim());
	$('#fromDateHide').val($('#fromDate').val().trim());
	$('#toDateHide').val($('#toDate').val().trim());
	$('#resultSapce').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	$('.Label1Style').css('width','50px');
	$('.Label2Style').css('width','65px');
	$('#documentCode').focus();
	setDateTimePicker('fromDate'); 
	setDateTimePicker('toDate');
	
	var fromDate = $('#fromDate').val().trim();
	var toDate = $('#toDate').val().trim();
	var stafRole = '<s:property value="staffSign.getStaffType().getObjectType()" />';
	var title = '';
	var withL = 15;
	
	/* $('#btnSearch').bind('keyup',function(event){
		if(event.keyCode == 13){
			ProgramManager.search();
		}
	});
	
	$('#displayProgramCode').bind('keyup',function(event){
		if(event.keyCode == 13){
			ProgramManager.search();
		}
	}).focus();
	
	$('#displayProgramName').bind('keyup',function(event){
		if(event.keyCode == 13){
			ProgramManager.search();
		}
	});
	 */
	
	if(stafRole==StaffRoleType.NHVNM){
		title = '<a href="javascript:void(0)" onclick="ProgramDocument.uploadDoc()"><img src="/resources/images/icon_add.png"/></a>';
		withL = 0;
		$('#dg').datagrid({
			url : '/document/search',		
			rownumbers : true,	
			fitColumns:true,
			pagination:true,
			rowNum : 20,		
			pageList  : [10,20,30],
			scrollbarSize:0,
			width: $(window).width()-35,
			singleSelect:true,
			autoWidth: true,
			queryParams:{
				fromDate:fromDate,
				toDate:toDate
		 	},
		    columns:[[  
		        {field:'documentCode',title:'Mã công văn',align:'left', width:80,sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
		        	return '<a href="javascript:void(0)" onclick="ProgramDocument.viewDetail('+rowData.id+');" >'+Utils.XSSEncode(value) + '</a>';
		        }},
		        {field:'documentName',title:'Tên công văn',align:'left', width:230, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
		        	return Utils.XSSEncode(value);
		        }}, 
		        {field:'type',title:'Loại CV',align:'left', width:50, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
		        	return Utils.XSSEncode(rowData.type);
		        }},  
		        {field:'date', title:'Thời gian', align:'center',width:100, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
		        	var str = '';
		        	var spaceTp = $('#resultSapce').html();
		        	if(rowData.fromDate!= undefined && rowData.fromDate!=null){
		        		str+=$.datepicker.formatDate('dd/mm/yy', new Date(Utils.XSSEncode(rowData.fromDate)));
		        	}else{str+=spaceTp;}
		        	if(rowData.toDate!= undefined && rowData.toDate!=null){
		        		str+=' - ' + $.datepicker.formatDate('dd/mm/yy', new Date(Utils.XSSEncode(rowData.toDate)));
		        	}else{
		        		str+=' - ' + spaceTp;
		        	}
		        	return str;
		        	
		        }},
		        {field : 'edit', title : 'Đơn vị', align : 'center', width: 50, sortable : false, resizable : false, formatter : function(value, rowData, index){
					return "<a href= 'javascript:void(0)' onclick= \"return ProgramDocument.editOfficeDocumentShopMap("+rowData.id+");\"><img src='/resources/images/icon-edit.png' height='15'/></a>";
				}
			},
		        {field:'action', title:title, 
		        	align:'center',width:withL,sortable : false,resizable : false, 
		        	formatter : function(value,rowData,rowIndex) {
		        		return "<a href='javascript:void(0)' onclick=\"return ProgramDocument.deleteOfficeDocument("+rowData.id+");\"><img src='/resources/images/icon_delete.png' width='15' height='15'/></a>";
		        	}
		       	}
		    ]],	    
		    onLoadSuccess :function(data){	 
		    	$('#errMsg').html('').hide();
		    	$('.datagrid-header-rownumber').html('STT');	    		    	
		    	 updateRownumWidthForJqGrid('.easyui-dialog');
	    		 $(window).resize();
		    }
		});
	 }else{
	$('#dg').datagrid({
		url : '/document/search',		
		rownumbers : true,	
		fitColumns:true,
		pagination:true,
		rowNum : 20,		
		pageList  : [10,20,30],
		scrollbarSize:0,
		width: $(window).width()-35,
		singleSelect:true,
		autoWidth: true,
		queryParams:{
			fromDate:fromDate,
			toDate:toDate
	 	},
	    columns:[[  
	        {field:'documentCode',title:'Mã công văn',align:'left', width:80,sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	return '<a href="javascript:void(0)" onclick="ProgramDocument.viewDetail('+rowData.id+');" >'+Utils.XSSEncode(value) + '</a>';
	        }},
	        {field:'documentName',title:'Tên công văn',align:'left', width:230, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	return Utils.XSSEncode(rowData.documentName);
	        }}, 
	        {field:'type',title:'Loại CV',align:'left', width:50, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	return Utils.XSSEncode(rowData.type);
	        }},  
	        {field:'date', title:'Thời gian', align:'center',width:100, sortable : false,resizable : false, formatter : function(value,rowData,rowIndex) {
	        	var str = '';
	        	var spaceTp = $('#resultSapce').html();
	        	if(rowData.fromDate!= undefined && rowData.fromDate!=null){
	        		str+=$.datepicker.formatDate('dd/mm/yy', new Date(Utils.XSSEncode(rowData.fromDate)));
	        	}else{str+=spaceTp;}
	        	if(rowData.toDate!= undefined && rowData.toDate!=null){
	        		str+=' - ' + $.datepicker.formatDate('dd/mm/yy', new Date(Utils.XSSEncode(rowData.toDate)));
	        	}else{
	        		str+=' - ' + spaceTp;
	        	}
	        	return str;
	        	
	        }}/* ,
	        {field : 'edit', title : 'Đơn vị', align : 'center', width: 50, sortable : false, resizable : false, formatter : function(value, rowData, index){
					return "<a href= 'javascript:void(0)' onclick= \"return ProgramDocument.editOfficeDocumentShopMap("+rowData.id+");\"><img src='/resources/images/icon-edit.png' height='15'/></a>";
				}
			} */ 
	    ]],	    
	    onLoadSuccess :function(data){	 
	    	$('#errMsg').html('').hide();
	    	$('.datagrid-header-rownumber').html('STT');	    		    	
	    	 updateRownumWidthForJqGrid('.easyui-dialog');
    		 $(window).resize();
	    }
	});}
	
});
</script>
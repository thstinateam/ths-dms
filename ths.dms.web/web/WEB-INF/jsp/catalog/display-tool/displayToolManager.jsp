<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="Breadcrumbs">
  <span class="Breadcrumbs1Item Sprite1">Thông tin khách hàng sử dụng tủ</span>
</div>
<div class="GeneralMilkBox">
   	<div class="GeneralMilkTopBox">
       	<div class="GeneralMilkBtmBox">
          <h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin tìm kiếm</span></h3>
          <div class="GeneralMilkInBox ResearchSection">
	          <div class="ModuleList111Form">
	              <label class="LabelStyle Label1Style">Mã đơn vị (F9)</label>
	              <div class="Field2">
		              <input id="shopCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.shop.shopCode"/>"/>
		              <span class="RequireStyle">(*)</span>
				  </div>
	              <label class="LabelStyle Label2Style" style="padding-right:25px;padding-left:40px;">GSNPP(F9)</label>
	              <input id="supervisorStaffCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="currentUser.userName"/>"/>
	              <label class="LabelStyle Label3Style">Mã NVBH(F9)</label>
	              <div class="Field2">
		              <input id="saleStaffCode" type="text" class="InputTextStyle InputText1Style" />
		              <span class="RequireStyle">(*)</span>
				  </div>
	              <div class="Clear"></div>
	              <label class="LabelStyle Label1Style">Mã KH(F9)</label>
	              <div class="Field2">
		              <input id="customerCode" type="text" class="InputTextStyle InputText1Style" />
		              <span class="RequireStyle">(*)</span>
				  </div>
	              <div class="divBankHidden" style="display: none;">
		              <label class="LabelStyle Label2Style" style="padding-right:25px;padding-left:40px;">Mã tủ(F9)</label>
		              <div class="Field2">
			              <input id="displayToolCode" type="text" class="InputTextStyle InputText1Style" />
			              <span class="RequireStyle">(*)</span>
					  </div>
		              <label class="LabelStyle Label3Style">Số lượng</label>
		              <div class="Field2">
			              <input id="quantity" type="text" class="InputTextStyle InputText1Style" />
			              <span class="RequireStyle">(*)</span>
					  </div>
		              <div class="Clear"></div>
		              <label class="LabelStyle Label1Style">Doanh số</label>
		              <div class="Field2">
			              <input id="income" type="text" class="InputTextStyle InputText1Style" />
					  	  <span class="RequireStyle">(*)</span>
					  </div>
				  </div>
	              <label class="LabelStyle Label2Style" style="width:90px;padding-left:40px;">Tháng áp dụng</label>
	              <div class="Field2">
		              <input id="usedMonth" type="text" class="InputTextStyle InputText2Style" readonly="readonly"/>
		              <span class="RequireStyle">(*)</span>
				  </div>
	              <div class="Clear"></div>
	              <div class="ButtonSection">
		              	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return DisplayToolCatalog.search();"><span class="Sprite2">Tìm kiếm</span></button>
	                	<button id="btnCreate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return Utils.getChangedForm(true);"><span class="Sprite2">Thêm mới</span></button>
	                	<button style="display: none;" id="btnUpdate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return DisplayToolCatalog.change();"><span class="Sprite2">Cập nhật</span></button>
	                	<button style="display: none;" id="btnDismiss" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return DisplayToolCatalog.resetForm();"><span class="Sprite2">Bỏ qua</span></button>
		              	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
	              </div>
	              <p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
	              <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
	            </div>
            </div>
		</div>
    </div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Danh sách khách hàng sử dụng tủ</span></h3>
          	<div class="GeneralMilkInBox">
              	<div class="ResultSection" id="displayToolGrid">
                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
                  	<table id="grid"></table>
					<div id="pager"></div>
              	</div>
              	<div class="Clear"></div>
          	</div>
		</div>
    </div>
</div>
<div class="FuncBtmSection">
<form action="/catalog_display_tool/importexcelfile" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
	<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>	
	<div class="DivInputFileSection">
			<p class="ExcelLink">
				<a id="downloadTemplate" href="javascript:void(0)">Tải mẫu file excel</a>
			</p>		
				<div class="DivInputFile">
		            <input type="file" class="InputFileStyle" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
		            <div class="FakeInputFile">
		                <input id="fakefilepc" type="text" class="InputTextStyle">
		            </div>
		        </div>
		        <button class="BtnGeneralStyle Sprite2"><span class="Sprite2">Nhập từ file</span></button>	  
		    <div class="Clear"></div>
	</div>
 </form>
</div>
<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
<div id="responseDiv" style="display: none"></div>
<s:hidden id="displayToolId" value="0"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	var isShopEnable = '<s:property value="shopEnable"/>';
	var isBoth = '<s:property value="shopEnableBoth"/>';
	if(isBoth == 'false'){
		if(isShopEnable == 'true'){
			$('#shopCode').removeAttr('disabled','disabled');
		}else{
			$('#supervisorStaffCode').removeAttr('disabled','disabled');
		}
	}else{
		$('#shopCode').removeAttr('disabled','disabled');
		$('#supervisorStaffCode').removeAttr('disabled','disabled');
	}
	$('#usedMonth').val(getCurrentMonth());
	$('.RequireStyle').hide();
	applyDateTimePicker("#usedMonth", "mm/yy", null, null, null, null, null, null, null);
	$('#shopCode').focus();
	$("#grid").jqGrid({
	  url:DisplayToolCatalog.getGridUrl($('#shopCode').val(),$('#supervisorStaffCode').val(),'','',$('#usedMonth').val()),
	  colModel:[		
	    {name:'staff.staffCode', label: 'Mã NVBH', width: 100, sortable:false,resizable:false, align: 'left' },
	    {name:'staff.staffName', label: 'Tên NVBH', sortable:false,resizable:false , align: 'left'},
	    {name:'customer.shortCode', label: 'Mã KH', width: 100, align: 'right', sortable:false,resizable:false },
	    {name:'product.productCode', label: 'Mã tủ', width: 80, align: 'right',sortable:false,resizable:false},
	    {name:'quantity', label: 'Số lượng', width: 80, align: 'right',sortable:false,resizable:false},
	    {name:'amount', label: 'Doanh số', align: 'right',sortable:false,resizable:false, formatter: PromotionCatalogFormatter.moneyFormat},
	    {name:'inMonth', label: 'Tháng sử dụng', width: 100, align: 'center',sortable:false,resizable:false, formatter: DisplayToolCatalogFormatter.monthFormatter},
	    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: DisplayToolCatalogFormatter.editCellIconFormatter},
	    {name:'delete', label: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false, formatter: DisplayToolCatalogFormatter.delCellIconFormatter},
	  ],	  
	  pager : '#pager',
	  height: 'auto',
	  rownumbers: true,
	  rowNum: 10,
	  repeatitems: false, 
	  width: ($('#displayToolGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});
	$('#jqgh_grid_rn').prepend('STT');
	$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_KH_su_dung_tu.xls');
	var options = { 
			beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcel,
	 		type: "POST",
	 		dataType: 'html'   
	 	}; 
	$('#importFrm').ajaxForm(options);
	Utils.bindFormatOnTextfield('income',Utils._TF_NUMBER_COMMA);
	Utils.formatCurrencyFor('income');
	Utils.bindFormatOnTextfield('quantity',Utils._TF_NUMBER);
	$('#shopCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			CommonSearch.searchShopOnDialog(function(data){
				$('#shopCode').val(data.code);
			});
		}
	});
	$('#supervisorStaffCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var lstParam = new Array();
			var obj = {};
			obj.name = 'shopCode';
			obj.value = $('#shopCode').val().trim();
			lstParam.push(obj);
			var obj1 = {};
			obj1.name = 'typeInventory';
			if(isBoth == 'false'){
				if(isShopEnable == 'true'){
					obj1.value = 1;
				}else{
					obj1.value = 0;
				}
			}else{
				obj1.value = 1;
			}
			lstParam.push(obj1);
			CommonSearch.searchSupervisorStaffUnderOnDialog(function(data){
				$('#supervisorStaffCode').val(data.code);
			},lstParam);
		}
	});
	$('#saleStaffCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			if($('#shopCode').val().trim() != ''){
				var lstParam = new Array();
				var obj1 = {};
				obj1.name = 'shopCode';
				obj1.value = $('#shopCode').val().trim();
				lstParam.push(obj1);
				var obj2 = {};
				obj2.name = 'supervisorStaffCode';
				obj2.value = $('#supervisorStaffCode').val().trim();
				lstParam.push(obj2);
				var obj3 = {};
				obj3.name = 'typeInventory';
				if(isBoth == 'false'){
					if(isShopEnable == 'true'){
						obj3.value = 1;
					}else{
						obj3.value = 0;
					}
				}else{
					obj3.value = 1;
				}
				lstParam.push(obj3);
				CommonSearch.searchSaleStaffOnDialog(function(data){
					$('#saleStaffCode').val(data.code);
				},lstParam);
			}else{
				$('#errMsg').html('Vui lòng nhập giá trị cho trường Mã đơn vị').show();
			}
		}
	});
	$('#customerCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			if($('#shopCode').val().trim() != ''){
				$('#errMsg').html('').hide();
				var lstParam = new Array();
				var obj = {};
				obj.name = 'shopCode';
				obj.value = $('#shopCode').val().trim();
				lstParam.push(obj);
				CommonSearch.searchCustomerOnDialog(function(data){
					$('#customerCode').val(data.code);
				},lstParam);
			}else{
				$('#errMsg').html('Vui lòng nhập giá trị cho trường Mã đơn vị').show();
			}
		}
	});
	
	$('#displayToolCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			CommonSearch.searchProductOnDialog(function(data){
				$('#displayToolCode').val(data.code);
			});
		}
	});
});
</script>
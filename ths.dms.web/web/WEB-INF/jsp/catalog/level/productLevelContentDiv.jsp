<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="Breadcrumbs">
    <span class="Breadcrumbs1Item Sprite1">Danh mục mức</span>
</div>
<div class="GeneralMilkBox">
   	<div class="GeneralMilkTopBox">
       	<div class="GeneralMilkBtmBox">
			<h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin tìm kiếm</span></h3>
            <div class="GeneralMilkInBox ResearchSection">
            	<div class="ModuleList14Form">
                <label class="LabelStyle Label1Style">Mã mức</label>
                <div class="Field2">
                	<input id="code" type="text" class="InputTextStyle InputText1Style" maxlength="20">
                	<span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label2Style">Tên mức</label>
                <div class="Field2">
                	<input id="name" type="text" class="InputTextStyle InputText1Style" maxlength="150">
                	<span class="RequireStyle">(*)</span>
                </div>
	            <label class="LabelStyle Label3Style">Mã category</label>
	            <div class="Field2">
	                	<div class="BoxSelect BoxSelect1">
	                    	<s:select id="category" headerKey="-2" headerValue="-- Chọn mã category --"  cssClass="MySelectBoxClass" list="lstCat" listKey="id" listValue="productInfoCode" onchange="changedLevelCode('#code',$(this).val(),$('#subCategory').val());"></s:select>
						</div>
					<span class="RequireStyle">(*)</span>
				</div>
				<div class="Clear"></div>	
				<label class="LabelStyle Label1Style">Mã sub category</label>
	            <div class="Field2">
	                	<div class="BoxSelect BoxSelect1">
	                    	<s:select id="subCategory" headerKey="-2" headerValue="-- Chọn mã sub category --" cssClass="MySelectBoxClass" list="lstSubCat" listKey="id" listValue="productInfoCode" onchange="changedLevelCode('#code',$('#category').val(),$(this).val());"></s:select>
						</div>
					<span class="RequireStyle">(*)</span>
				</div>
				<label class="LabelStyle Label2Style">Trạng thái</label>
                <div class="Field2">
	                <div class="BoxSelect BoxSelect1">
	                	<s:select id="status" headerKey="-2" headerValue="-- Chọn mã trạng thái --" name="status" cssClass="MySelectBoxClass" list="lstStatusBean" listKey="value" listValue="name"></s:select>                    
	                </div>
	                <span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label3Style">Ghi chú</label>
                <input id="description" type="text" class="InputTextStyle InputText1Style" maxlength="100">
                <div class="Clear"></div>
                <div class="ButtonSection">
                	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return ProductLevelCatalog.search()"><span class="Sprite2">Tìm kiếm</span></button>
                	<button id="btnAdd" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ProductLevelCatalog.getChangedForm();"><span class="Sprite2">Thêm mới</span></button>
                	<button id="btnEdit" class="BtnGeneralStyle Sprite2" onclick="return ProductLevelCatalog.save()" style="display: none"><span class="Sprite2">Cập nhật</span></button>
                	<button id="btnCancel" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ProductLevelCatalog.resetForm();" style="display: none"><span class="Sprite2">Bỏ qua</span></button>
                	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                </div>
                <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>
                <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
            </div>
            </div>
		</div>
    </div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Danh sách mức</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="levelGrid">
                    <p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
                    <table id="grid"></table>
					<div id="pager"></div>
                </div>
            </div>
		</div>		
    </div>    
</div>
<div class="FuncBtmSection">
	<div class="DivInputFileSection">			
		<p class="ExcelLink">
			<a id="downloadTemplate" href="javascript:void(0)">Tải mẫu file excel</a>
		</p>		
			<div class="DivInputFile">
				<form action="/catalog/level/import-excel" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
		            <input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>
		            <input type="file" class="InputFileStyle" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
		            <input type="hidden" id="isView" name="isView">
		            <div class="FakeInputFile">
		                <input id="fakefilepc" type="text" class="InputTextStyle">
		            </div>
				</form>
	        </div>
	        <button id="btnView" class="BtnGeneralStyle Sprite2" onclick="return ProductLevelCatalog.viewExcel();"><span class="Sprite2">Xem</span></button>
       		<button id="btnImport" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ProductLevelCatalog.importExcel();"><span class="Sprite2">Nhập từ file</span></button>
	    <div class="Clear"></div>
	</div>
</div>
<div class="FuncLeftBtmSection">
   	<button class="BtnGeneralStyle Sprite2" onclick="ProductLevelCatalog.exportExcelData();"><span class="Sprite2">Xuất dữ liệu</span></button>
</div>
<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
<div id="responseDiv" style="display: none"></div>
<s:hidden id="selId" value="0"></s:hidden>
<s:hidden id="focusTextDefault" value="code"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){	
	$('#code').focus();
	$('.MySelectBoxClass').customStyle();
	$('.RequireStyle').hide();
	$("#grid").jqGrid({
	  url:ProductLevelCatalog.getGridUrl('','',$('#category').val(),$('#subCategory').val(),$('#status').val(),''),
	  colModel:[		
	    {name:'productLevelCode',index:'productLevelCode', label: 'Mã mức', width: 90, sortable:false,resizable:false , align: 'left'},
	    {name:'productLevelName',index:'productLevelName', label: 'Tên mức', width: 90, sortable:false,resizable:false , align: 'left'},
	    {name:'cat.productInfoCode',index:'cat.productInfoCode', label: 'Mã category', width: 90, sortable:false,resizable:false , align: 'left'},
	    {name:'subCat.productInfoCode',index:'subCat.productInfoCode', label: 'Mã sub category', width: 90,sortable:false,resizable:false , align: 'left'},	    
	    {name:'description',index:'description', label: 'Ghi chú', sortable:false,resizable:false, align: 'left' },
	    {name:'status',index:'status', label: 'Trạng thái', width: 78, align: 'left', sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter},
	    {name:'edit',index:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProductLevelCatalogFormatter.editCellIconFormatter},
	    {name:'delete',index:'delete', label: 'Xóa', width: 50, align: 'center', sortable:false,resizable:false, formatter: ProductLevelCatalogFormatter.delCellIconFormatter},
	    {name:'cat.id',index:'cat.id', hidden: true },
	    {name:'subCat.id',index:'subCat.id', hidden: true },
	    {name:'id',index:'id', hidden: true },
	  ],	  
	  pager : '#pager',
	  height: 'auto',	  	  
	  width: ($('#levelGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});
	$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_muc_san_pham.xls');
	var options = { 
	 		beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcelUpdate,  
	 		type: "POST",
	 		dataType: 'html'   
	 	}; 
	$('#importFrm').ajaxForm(options);
	ProductLevelCatalog._htmlCat = $('#category').html();
	ProductLevelCatalog._htmlSubCat = $('#subCategory').html();	
});
function changedLevelCode(obj,categoryValue,subCategoryValue){
	if(categoryValue != -2 && subCategoryValue != -2){
		$.getJSON('/rest/catalog/level/code/' + categoryValue + '/'+subCategoryValue+".json",function(data){
			$(obj).val(data.name);
		});
	}
};
</script>
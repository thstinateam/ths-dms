<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="Breadcrumbs">
    <span class="Breadcrumbs1Item Sprite1">Nhóm lý do</span>
</div>
<div class="GeneralMilkBox">
   	<div class="GeneralMilkTopBox">
       	<div class="GeneralMilkBtmBox">
			<h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin tìm kiếm</span></h3>
            <div class="GeneralMilkInBox ResearchSection">
            	<div class="ModuleList2Form">
                <label class="LabelStyle Label1Style">Mã nhóm lý do</label>
                <div class="Field2">
                	<input id="reasonGroupCode" type="text" class="InputTextStyle InputText1Style" maxlength="20">
                	<span class="RequireStyle">(*)</span>
                </div>                
                <label class="LabelStyle Label2Style">Tên nhóm lý do</label>
                <div class="Field2">
                	<input id="reasonGroupName" type="text" class="InputTextStyle InputText1Style" maxlength="50">
                	<span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label3Style">Ghi chú</label>
                <input id="description" type="text" class="InputTextStyle InputText1Style" maxlength="100">
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style">Trạng thái</label>
                <div class="Field2">
	                <div class="BoxSelect BoxSelect1">
	                	<s:select id="status" headerKey="-2" headerValue="-- Chọn trạng thái --"  name="status" cssClass="MySelectBoxClass" list="lstStatusBean" listKey="value" listValue="name"></s:select>                    
	                </div>
	                <span class="RequireStyle">(*)</span>
                </div>
                <div class="Clear"></div>
                <div class="ButtonSection">
                	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return ReasonGroupCatalog.searchReasonGroup()"><span class="Sprite2">Tìm kiếm</span></button>
                	<button id="btnAdd" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ReasonGroupCatalog.getChangedForm();"><span class="Sprite2">Thêm mới</span></button>
                	<button id="btnEdit" class="BtnGeneralStyle Sprite2" onclick="return ReasonGroupCatalog.saveReasonGroup()" style="display: none"><span class="Sprite2">Cập nhật</span></button>
                	<button id="btnCancel" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ReasonGroupCatalog.resetForm();" style="display: none"><span class="Sprite2">Bỏ qua</span></button>
                	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                </div>
                <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none">Có lỗi xảy ra khi cập nhật dữ liệu</p>
                <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
               </div>
            </div>            
		</div>		 
    </div>   
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Danh sách nhóm lý do</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="reasonGroupGrid">
                    <p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
                    <table id="grid"></table>
					<div id="pager"></div>
                </div>
            </div>
		</div>
    </div>
</div>

<s:hidden id="selReasonId" value="0"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#reasonGroupCode').focus();
	$('#reasonGroupName').val('');
	$('.MySelectBoxClass').customStyle();
	$('.RequireStyle').hide();
	$("#grid").jqGrid({
	  url:ReasonGroupCatalog.getGridUrl('','','',$('#status').val()),
	  colModel:[		
	    {name:'reasonGroupCode', index: 'reasonGroupCode', label: 'Mã nhóm lý do',width: 100, sortable:false,resizable:false , align: 'left'},
	    {name:'reasonGroupName', index: 'reasonGroupName',label: 'Tên nhóm lý do', sortable:false,resizable:false, align:'left'},
	    {name:'description', index: 'description',label: 'Ghi chú', sortable:false,resizable:false, align:'left' },
	    {name:'status', index: 'status',label: 'Trạng thái',width: 80, align: 'left', sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter},
	    {name:'edit', label: 'Sửa',width: 50,align: 'center',sortable:false,resizable:false, formatter: ReasonGroupCatalogFormatter.editCellIconFormatter},
	    {name:'delete', label: 'Xóa',width: 50, align: 'center', sortable:false,resizable:false, formatter: ReasonGroupCatalogFormatter.delCellIconFormatter},
	    {name: 'id', index: 'id', hidden: true}
	  ],	  
	  pager : '#pager',
	  height: 'auto',	  	  
	  width: ($('#reasonGroupGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});	
});

</script>
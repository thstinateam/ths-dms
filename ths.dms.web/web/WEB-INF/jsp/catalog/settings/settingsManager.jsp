<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style>
	.InputSetting {
		text-align: right; 
		width: 150px;
		border: 1px solid #c4c4c4;
		margin-right: 10px;
		margin-bottom: 0px;
		padding: 2px 4px;
	}
	
	.InputSettingLong {
		text-align: left; 
		width: 400px;
		border: 1px solid #c4c4c4;
		margin-bottom: 0px;
		margin-right: 10px;
		padding: 2px 4px;
	}
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#"><s:text name="organization_system"></s:text></a></li>
        <li><span><s:text name="jsp.apparam.cauhinh.tham.so.title"></s:text></span></li>
    </ul>
</div>

<div class="CtnOneColSection">
   <div class="ContentSection">
   		<div class="ToolBarSection">
           <div class="SearchSection GeneralSSection">
	           	<h2 class="Title2Style"><s:text name="jsp.settings.system.group.system.title"></s:text></h2>
	           	<div class="SearchInSection SProduct1Form">
	           		<div class="FixFloat" style="float: left; width: 90%; margin-left: 20px">
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       <colgroup>
                                   <col style="width:30%;" />
                                   <col style="width:70%;" />
                       </colgroup>
                       <tbody>
                       		<tr>
                            	<td  style="padding: 1px;">
                            		<label class="LabelStyle LabelStyle">
				                    	<s:text name="jsp.settings.system.hour.auto.close.date"/>
				                	</label>
                            	</td>
                                <td>
	                                <label class="LabelStyle LabelStyle" style="text-align: left;">
	                					<input id="hourAutoCloseDate" type="text" autocomplete="off" readonly="readonly" class="InputSetting"> 
	                					<s:hidden id="hdHourAutoCloseDate" name="settingsVo.hourAutoCloseDateId"></s:hidden>
                						<s:text name="jsp.settings.system.hour.auto.close.date.format"/>
                					</label>
                                </td>
                            </tr>
                            <tr>
                            	<td  style="padding: 1px;">
                            	<div>
	                            	<label class="LabelStyle LabelStyle">
				                    	<s:text name="jsp.settings.system.distance.check.customer"/>
				                	</label>
                            	</div>
                            	</td>
                                <td  style="padding: 1px;">
                                <div>
			                		<label class="LabelStyle LabelStyle" style="text-align: left;">
				                		<input id="distanceCheckCustomer" type="text" autocomplete="off" class="easyui-numberbox InputSetting" data-options="min:<s:text name='settingsVo.minDistanceCheckCustomerValue'/>, max:<s:text name='settingsVo.maxDistanceCheckCustomerValue'/>"> 
				                		<s:hidden id="hdDistanceCheckCustomer" name="settingsVo.distanceCheckCustomerId"></s:hidden>
			                			 Trong khoảng <s:text name="settingsVo.minDistanceCheckCustomerValue"/> - <s:text name="settingsVo.maxDistanceCheckCustomerValue"/> <s:text name="jsp.settings.system.distance.check.customer.format"/>
			                		</label>
                                </div>
                                </td>
                            </tr>
                           </tbody>
                       </table>
           			</div>
	           	</div>
	           	<div class="Clear"></div>     	
	           	<h2 class="Title2Style"><s:text name="jsp.settings.system.group.problem.title"></s:text></h2>
	           	<div class="SearchInSection SProduct1Form">
	           		<div class="FixFloat" style="float: left; width: 90%; margin-left: 20px">
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       <colgroup>
                                   <col style="width:30%;" />
                                   <col style="width:70%;" />
                       </colgroup>
                       <tbody>
                       		<tr>
                            	<td  style="padding: 1px;">
                            	<div>
	                            	<label class="LabelStyle LabelStyle LabelPadding">
				                    	<s:text name="jsp.settings.system.problem.attach.size.max"/>
				                	</label>
                            	</div>
                            	</td>
                                <td  style="padding: 1px;">
                                <div>
				                	<label class="LabelStyle LabelStyle LabelPadding" style="text-align: left;">
	                                	<input id="problemAttachSizeMax" type="text" autocomplete="off" class="easyui-numberbox InputSetting" data-options="min:<s:text name='settingsVo.minProblemAttachSizeMaxValue'/>, max: <s:text name='settingsVo.maxProblemAttachSizeMaxValue'/>"> 
					                	<s:hidden id="hdProblemAttachSizeMax" name="settingsVo.problemAttachSizeMaxId"></s:hidden>
				                		 Trong khoảng <s:text name='settingsVo.minProblemAttachSizeMaxValue'/> - <s:text name='settingsVo.maxProblemAttachSizeMaxValue'/> <s:text name="jsp.settings.system.problem.attach.size.max.format"/>
				                	</label>
                                </div>
                                </td>
                            </tr>
                            <tr>
                            	<td  style="padding: 1px;">
                            	<div>
	                            	<label class="LabelStyle LabelStyle LabelPadding" style="text-align: left;">
				                    	<s:text name="jsp.settings.system.problem.file.type.upload"/>
				                	</label>
                            	</div>
                            	</td>
                                <td  style="padding: 1px;">
                                <div>
                					<label class="LabelStyle LabelStyle LabelPadding" style="text-align: left;">
	                                	<textarea id="problemFileTypeUpload" maxlength="<s:text name='settingsVo.maxLenghtProblemFileTypeUploadValue'/>" class="InputSettingLong" rows="3"></textarea>
	                					<s:hidden id="hdProblemFileTypeUpload" name="settingsVo.problemFileTypeUploadId"></s:hidden>
                						 Tối đa <s:text name='settingsVo.maxLenghtProblemFileTypeUploadValue'/> ký tự. <s:text name="jsp.settings.system.problem.file.type.upload.format"/>
                					</label>
                                </div>
                                </td>
                            </tr>
                           </tbody>
                       </table>
           			</div>
	           	</div>
	           	<div class="Clear"></div>
	           	<h2 class="Title2Style"><s:text name="jsp.settings.system.group.tablet.title"></s:text></h2>
	           	<div class="SearchInSection SProduct1Form">
	           		<div class="FixFloat" style="float: left; width: 90%; margin-left: 20px">
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       <colgroup>
                                   <col style="width:30%;" />
                                   <col style="width:70%;" />
                       </colgroup>
                       <tbody>
                       		<tr>
                            	<td  style="padding: 1px;">
                            	<div>
	                            	<label class="LabelStyle LabelStyle LabelPadding">
				                    	<s:text name="jsp.settings.system.tablet.time.request.position"/>
				                	</label>
                            	</div>
                            	</td>
                                <td  style="padding: 1px;">
                                <div>    	
			                		<label class="LabelStyle LabelStyle LabelPadding" style="text-align: left;">
				                		<input id="timeRequestPosition" type="text" autocomplete="off" class="easyui-numberbox InputSetting" data-options="min:<s:text name='settingsVo.minTimeRequestPositionValue'/>, max:<s:text name='settingsVo.maxTimeRequestPositionValue'/>"> 
				                		<s:hidden id="hdTimeRequestPosition" name="settingsVo.timeRequestPositionId"></s:hidden>
			                			 Trong khoảng <s:text name='settingsVo.minTimeRequestPositionValue'/> - <s:text name='settingsVo.maxTimeRequestPositionValue'/> <s:text name="jsp.settings.system.tablet.time.request.position.format"/>
			                		</label>
                                </div>
                                </td>
                            </tr>
                            <tr>
                            	<td  style="padding: 1px;">
                            	<div>
	                            	<label class="LabelStyle LabelStyle LabelPadding">
				                    	<s:text name="jsp.settings.system.tablet.time.request.sync.data"/>
				                	</label>
                            	</div>
                            	</td>
                                <td  style="padding: 1px;">
                                <div>
			                		<label class="LabelStyle LabelStyle LabelPadding" style="text-align: left;">
				                		<input id="timeRequestSyncData" type="text" autocomplete="off" class="easyui-numberbox InputSetting" data-options="min:<s:text name='settingsVo.minTimeRequestSyncDataValue'/>, max:<s:text name='settingsVo.maxTimeRequestSyncDataValue'/>"> 
				                		<s:hidden id="hdTimeRequestSyncData" name="settingsVo.timeRequestSyncDataId"></s:hidden>
			                			 Trong khoảng <s:text name='settingsVo.minTimeRequestSyncDataValue'/> - <s:text name='settingsVo.maxTimeRequestSyncDataValue'/> <s:text name="jsp.settings.system.tablet.time.request.sync.data.format"/>
			                		</label>
                                </div>
                                </td>
                            </tr>
                           </tbody>
                       </table>
           			</div>
           			
	           	</div>
	           	<div class="Clear"></div>
				<div class="SearchInSection SProduct1Form">	
					<div class="BtnCenterSection">
						<button onclick="SettingsConfig.update();" id="btnUpdate" class="BtnGeneralStyle cmsiscontrol"><s:text name="jsp.common.capnhat"></s:text></button>
					</div>
				</div>
				<div class="Clear"></div>
	            <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
           </div>
      	</div>
        
   </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	
 	var hourAutoCloseDateValue = Utils.XSSEncode("<s:property value='settingsVo.hourAutoCloseDateValue' />");
	$('#hourAutoCloseDate').val(hourAutoCloseDateValue);
	var distanceCheckCustomer = Utils.XSSEncode("<s:property value='settingsVo.distanceCheckCustomerValue' />");
	$('#distanceCheckCustomer').val(distanceCheckCustomer);
	var problemAttachSizeMax = Utils.XSSEncode("<s:property value='settingsVo.problemAttachSizeMaxValue' />");
	$('#problemAttachSizeMax').val(problemAttachSizeMax);
	var problemFileTypeUpload = Utils.XSSEncode("<s:property value='settingsVo.problemFileTypeUploadValue' />");
	$('#problemFileTypeUpload').val(problemFileTypeUpload);
	var timeRequestPosition = Utils.XSSEncode("<s:property value='settingsVo.timeRequestPositionValue' />");
	$('#timeRequestPosition').val(timeRequestPosition);
	var timeRequestSyncData = Utils.XSSEncode("<s:property value='settingsVo.timeRequestSyncDataValue' />");
	$('#timeRequestSyncData').val(timeRequestSyncData);
	
	var input = $('#hourAutoCloseDate').clockpicker({
	    placement: 'bottom',
	    align: 'left',
	    autoclose: true
	});
});
</script>
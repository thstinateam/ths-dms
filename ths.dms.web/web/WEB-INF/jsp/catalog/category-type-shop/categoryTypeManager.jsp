<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/catalog/cargo-brand-distributer">Danh mục</a></li>
		<li><span id="title1">Tồn kho chuẩn theo ngành</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
   	<div class="ContentSection">
       	<div class="ToolBarSection">
          <div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
		              <label class="LabelStyle Label1Style">Mã đơn vị (F9)</label>
		              <input id="shopCode" type="text" class="InputTextStyle InputText1Style" />
		              <label class="LabelStyle Label1Style">Ngành hàng</label>
		              <div class="BoxSelect BoxSelect2">
		                <select id="category" multiple="multiple" style="display: none;">
						<option value="0">Tất cả</option>
						<s:iterator value="lstCategoryType">					
							<option value='<s:property value="id"/>'><s:property value="productInfoCode"/> - <s:property value="productInfoName"/></option>
						</s:iterator>		    
				    </select>
		              </div>
		              <label class="LabelStyle Label1Style">Trạng thái</label>
		              <div class="BoxSelect BoxSelect5">
		                  <select id="status" class="MySelectBoxClass"> 
		                      	  <option value="-2">Tất cả</option>
			                      <option value="0">Tạm ngưng</option>
			                      <option value="1" selected="selected">Hoạt động</option>
		                  </select>
		              </div>
		              <div class="Clear"></div>
		              <div class="BtnCenterSection">
			              	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return CategoryTypeCatalog.search();"><span class="Sprite2">Tìm kiếm</span></button>
<%-- 			              	<a id="btnCreate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" href="/cargo-brand-distributer/viewdetail?categoryId=0" ><span class="Sprite2">Thêm mới</span></a> --%>
			              	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
		              </div>
		              <p class="ErrorMsgStyle" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
		              <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
		              <div class="Clear"></div>
	              </div>
            </div>
            <div class="Clear"></div>
		</div>
		
		<div class=GeneralCntSection>
			<h2 class="Title2Style">Danh sách tồn kho chuẩn theo ngành</h2>
				<div class="GridSection">
					<div class="ResultSection" id="categoryGrid">
						<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
						<table id="grid"></table>
						<div id="pager"></div>
					</div>
					<div class="Clear"></div>
				</div>
				 <div class="GeneralForm GeneralNoTP1Form">
						<div class="Func1Section" id="idDivImport"> 
							<p class="DownloadSFileStyle DownloadSFile2Style">
								<a id="downloadTemplate" href="javascript:void(0)" class="Sprite1">Tải mẫu file excel</a>
							</p>
							<div class="DivInputFile">
								<form action="/cargo-brand-distributer/importexcelfile" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
									<input type="hidden" id="isView" name="isView"/>
	                 				<input type="file" class="InputFileStyle" size="22" name="excelFile" id="excelFile" style="width: 100px !important;" onchange="previewImportExcelFile(this,'importFrm');">
						            <div class="FakeInputFile">
										<input id="fakefilepc" style="margin: -3px 5px 0 0;" readonly="readonly" type="text" class="InputTextStyle InputText1Style">
									</div>
								</form>
							</div>
							<button id="btnImport" class="BtnGeneralStyle" onclick="CategoryTypeCatalog.ViewFileUpload(0);">Nhập từ Excel</button>
							<button id="btnExport" class="BtnGeneralStyle" onclick="CategoryTypeCatalog.exportExcel();">Xuất Excel</button>
						</div>
						<div class="Clear"></div>
				</div>
		</div>
    </div>
</div>
<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
<div id="responseDiv" style="display: none"></div>
<div id="viewExcelDIV" style="display: none">
		<div class="GeneralDialog General2Dialog">
        	<div class="DialogProductSearch">
            	<div class="GeneralTable Table44Section">
            	<div class="ScrollSection">
            				<div class="BoxGeneralTTitle">
	                              <table border="0" cellspacing="0" cellpadding="0">
	                                  <colgroup>
	                                     	<col style="width:85px;" />
									       <col style="width:85px;" />
									       <col style="width:85px;"/>
									       <col style="width:85px;" />
									       <col style="width:100px;" />
									       <col style="width:85px;"/>
									       <col style="width:85px;" />
									       <col style="width:85px;"/>
									       <col style="width:85px;" />
									       <col style="width:170px;" />
	                                  </colgroup>
	                                  <thead>
	                                      <tr>
	                                      	  <th class="ColsThFirst">Mã đơn vị</th>
	                                          <th class="ColsThSecond">Mã nghành hàng</th>
	                                          <th class="ColsThSecond">Tồn kho min</th>
	                                          <th class="ColsThSecond">Tồn kho max</th>
	                                          <th class="ColsThSecond">Ngày đi đường</th>
	                                          <th class="ColsThSecond">Vùng</th>
	                                          <th class="ColsThSecond">Ngày bán hàng</th>
	                                          <th class="ColsThSecond">Tuần mua hàng</th>
	                                          <th class="ColsThSecond">Tỉ lệ tăng trưởng(%)</th>
	                                          <th class="ColsThEnd">Lỗi</th>
	                                      </tr>
	                                  </thead>
	                              </table>
	                          </div>
	                          <div class="BoxGeneralTBody">
	                          	<div id="fancy_gird" class="ScrollBodySection"> 
								</div>
	                          </div>
	                  	</div>
	                  	<div class="BoxDialogBtm">
                    		<div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
               			 </div>
	              </div>
		</div>
	</div>
</div>
<div id="divPop" style="visibility:hidden">
		<div id="popup1" class="easyui-dialog" title="Ngày làm việc" data-options="closed:true,modal:true" style="width:400px;height:147px;">
        	<div class="PopupContentMid">
        		<div class="GeneralForm Search1Form">
                               <span>Thứ 2</span><input id="monday"  type="checkbox" value="2"/>
                               <span>Thứ 3</span><input id="tuesday"  type="checkbox" value="3"/>
                               <span>Thứ 4</span><input id="wednesday"  type="checkbox" value="4"/>
                               <span>Thứ 5</span><input id="thursday"  type="checkbox" value="5"/>
                               <span>Thứ 6</span><input id="friday"  type="checkbox" value="6"/>
                               <span>Thứ 7</span><input id="saturday"  type="checkbox" value="7"/>
                               <span>Chủ nhật</span><input id="sunday"  type="checkbox" value="1" />
                           <div class="Clear"></div>
                           <div class="BtnCenterSection">
                           		<button class="BtnGeneralStyle" onclick="return CategoryTypeCatalog.mapValue();"><span class="Sprite2">Chọn</span></button>
                           	</div>
                           	<div class="Clear"></div>
                           <p class="ErrorMsgStyle" id="errMsg1" style="display: none;"></p>
                 </div>
             </div>
         </div>
</div>

<s:hidden id="categoryId" name="categoryId"></s:hidden>
<s:hidden id="shopCode1" name="shopCode1"></s:hidden>
<s:hidden id="categoryTypeId" name="categoryTypeId"></s:hidden>
<s:hidden id="stockMin" name="stockMin"></s:hidden>
<s:hidden id="stockMax" name="stockMax"></s:hidden>
<s:hidden id="dateGo" name="dateGo"></s:hidden>
<s:hidden id="dateSale" name="dateSale"></s:hidden>
<s:hidden id="dateSale1" name="dateSale1"></s:hidden>
<s:hidden id="growth" name="growth"></s:hidden>
<s:hidden id="status1" name="status1"></s:hidden>
<s:hidden id="minsfOld" name="minsfOld" ></s:hidden>
<s:hidden id="shopObjectType" name="shopObjectType"></s:hidden>
<s:hidden id="shopCodeHidden" name="shopCode"></s:hidden>
<s:hidden id="shopNameHidden" name="shopName"></s:hidden>

<script type="text/javascript">
var edit = false;
var idx = -1;
$(document).ready(function(){
	/* var d = new Date();
	var yearNow = d.getFullYear();
	var i = yearNow - 5;
	var j = yearNow + 5;
	var html = new Array();
	html.push("<option value='-1'>---Chọn năm---</option>");
	for(i;i<=j;i++){
		html.push("<option value='"+i+"'>"+i+"</option>");
	}
	$('#year').html(html.join("")); */
	$('#shopCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2({
				params : {
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Đơn Vị'},
			        {id:'name', maxlength:250, label:'Tên Đơn Vị'},
			    ],
			    url : '/commons/search-shop-show-list',
			    columns : [[
			        {field:'shopCode', title:'Mã Đơn Vị', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
					  }},
			        {field:'shopName', title:'Tên Đơn Vị', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
					  }},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			            return '<a href="javascript:void(0)" onclick=" CategoryTypeCatalog.fillTxtShopCodeByF9(\''+Utils.XSSEncode(row.shopCode)+'\');">chọn</a>';         
			        }}
			    ]]
			});
		}
	});
	
	CategoryTypeCatalog.lstCategory = new Array();
	CategoryTypeCatalog.lstCategory.push(0);
	$('#category').dropdownchecklist({ 
		emptyText:'Tất cả',
		width:173,
		firstItemChecksAll: true,
		maxDropHeight: 350,
		onItemClick:function(options){
			var i=0;
			CategoryTypeCatalog.lstCategory = new Array();
			var check = $('#ddcl-category-ddw input[type=checkbox]').attr('checked');
			if(check == 'checked'){
				CategoryTypeCatalog.lstCategory.push(0);
			}else{
				$('#ddcl-category-ddw input[type=checkbox]').each(function(){
					
					var s = $(this).attr('checked');
				    if(s == 'checked'){
				    	CategoryTypeCatalog.lstCategory.push($('#ddcl-category-ddw #ddcl-category-i'+i).val());
				    }
				    i++;
				});
			}
		}
	});
// 	var tm = setTimeout(function() {
// 		$('#category').html('');
// 		$('.CustomStyleSelectBox').remove();
// 		$('#status').customStyle();
// 	}, 500);
	
	
	var status2 = [
	               {value1:'RUNNING',text1:'Hoạt động'},
	               {value1:'STOPPED',text1:'Tạm ngưng'}
	           ];
// 	$('.MySelectBoxClass').customStyle();
	/* $('#year').val(yearNow);
	$('#year').change(); */
	applyDateTimePicker("#startDate");
	applyDateTimePicker("#endDate");
	var shopObjectType = $('#shopObjectType').val();
	if(shopObjectType!=undefined && shopObjectType!=null 
			&& (parseInt(shopObjectType)== ShopObjectType.VNM || parseInt(shopObjectType)== ShopObjectType.GT)){
		$('#lblMaDonVi').html("Mã đơn vị(F9)");
		$('#shopCode').focus();
		$("#grid").datagrid({
		  url:CategoryTypeCatalog.getGridUrl('','',1,0),
		  columns:[[		
		    {field:'shop.shopCode', title: 'Mã đơn vị', width: 95, sortable:false,resizable:false, align: 'left',formatter: function(value,row,index){
		    	if(row.shop != null){
		    		return Utils.XSSEncode(row.shop.shopCode);
		    	}
		    } },
		    {field:'cat.productInfoCode', title: 'Ngành hàng', width: 80, sortable:false,resizable:false , align: 'left',formatter:function(value,row,index){
		    	if(row.cat!=null){
		    		return Utils.XSSEncode(row.cat.productInfoCode);
		    	}
		    }},
		    {field:'minsf', title: 'Tồn kho Min', width: 75, align: 'right', sortable:false,resizable:false ,  
	            editor:{  
	                type:'numberbox'
	            } },
		    {field:'maxsf', title: 'Tồn kho Max', width: 75, align: 'right',sortable:false,resizable:false ,  
	                editor:{  
	                    type:'numberbox'
	                }},
		    {field:'lead', title: 'Đi đường', width: 75, align: 'right',sortable:false,resizable:false ,  
	                    editor:{  
	                        type:'numberbox'
	                    }},
		    {field:'calendarD', title: 'Ngày BH(Thứ)', width: 110, align: 'left',sortable:false,resizable:false,formatter: CategoryTypeCatalogFormatter.saleDayFormat,
		    	editor:{ 
		    		type:'text'
	            }},
		    {field:'percentage', title: 'Tỉ lệ TT(%)', width: 60, align: 'right',sortable:false,resizable:false,editor:{  
	            type:'numberbox',options:{precision:2}
	        } },
		    {field:'status', title: 'Trạng thái', width: 170, align: 'center',sortable:false,resizable:false,formatter: CategoryTypeCatalogFormatter.statusFormat, editor:{
		    	type:'combobox',
		            options:{  
		                valueField:'value1',  
		                textField: 'text1',
		                data:status2 ,
		                editable:false,
		                panelHeight :70
			    	}
		    	}
	        },
		    {field:'edit', title: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: function(value,row,index){
		    	if (row.editing){  
	                var s = "<a href='javascript:void(0)' class='btnGridEdit' onclick='return saverow(this)'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png' title='Lưu'/></span></a> ";  
	                var c = "<a href='javascript:void(0)' class='btnGridEdit' onclick='cancelrow(this)'><span style='cursor:pointer'><img src='/resources/images/icon_esc.png' title='Quay lại'/></span></a>";  
	                return s + "&nbsp;" + c;  
	            } else {  
	                var e = "<a href='javascript:void(0)' class='btnGridEdit' onclick='editrow(this);'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png' title='Sửa'/></span></a>";  
	                return e;  
	            }  
		    }},
		    {field:'delete', title: 'Xóa', width: 30, align: 'center',sortable:false,resizable:false, formatter: CategoryTypeCatalogFormatter.delCellIconFormatter},
		  ]],	  
		  pageList  : [10,20,30],
		  height:'auto',
		  scrollbarSize : 0,
		  pagination:true,
		  fitColumns:true,
		  singleSelect  :true,
		  method : 'GET',
		  rownumbers: true,	  
		  width: ($('#categoryGrid').width() - 10),
		  onLoadSuccess:function(){
			$('.datagrid-header-rownumber').html('STT');
	      	$('#grid').datagrid('resize');
	      	edit = false;
	      	CommonFormatter.checkPermissionHideColumn("grid", "edit", "btnGridEdit");
	      	CommonFormatter.checkPermissionHideColumn("grid", "delete", "btnGridDelete");
	      },
	  	onBeforeEdit:function(index,row){  
	        row.editing = true;
	        edit =true;
	        idx = index;  
	        updateActions(index);  
	    },  
	    onAfterEdit:function(index,row){  
	        row.editing = false;
	        edit = false;
	        idx = -1;
	        $('#categoryId').val(row.id.toString());
	        $('#shopCode1').val(row.shop.shopCode.toString());
	        $('#categoryTypeId').val(row.cat.id.toString());
	        $('#stockMin').val(row.minsf.toString());
	        $('#stockMax').val(row.maxsf.toString());
	        $('#dateGo').val(row.lead);
	        $('#dateSale').val(row.calendarD);
	        $('#growth').val(row.percentage);
	        var status = 1;		
			if(row.status != activeStatus){
				status = 0;
			}
	        $('#status1').val(status);
	        updateActions(index);  
	    },  
	    onCancelEdit:function(index,row){  
	        row.editing = false;  
	        edit = false;
	        idx = -1;
	        updateActions(index);  
	    }  
		});
		$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_Ton_kho_chuan_theo_nganh_hang.xls');
		var options = { 
				beforeSubmit : CategoryTypeProductCatalog.beforeImportExcel,
				success : CategoryTypeProductCatalog.afterImportExcelNganh,  
		 		type: "POST",
		 		dataType: 'html'   
		 	}; 
		$('#importFrm').ajaxForm(options);
/* 		$('#shopCode').bind('keyup', function(event){
			if(event.keyCode == keyCode_F9){
				var arrParam = new Array();
				var param = new Object();
				param.name = 'OjectType';
				param.value = '3';
				
				arrParam.push(param);
				CommonSearch.searchShopOnDialogByChannelType(function(data){
					$('#shopCode').val(data.code);
				},arrParam);
			}
		}); */
		$('#shopCode').bind('keypress', function(event){
			if(event.keyCode == keyCode_TAB){
				PromotionCatalog.getShopName($('#shopCode').val().trim(),'#shopName');
			}
		});
	}else if(shopObjectType!=undefined && shopObjectType!=null && parseInt(shopObjectType)== ShopObjectType.NPP){
		//Phan Quyen NPP
		$('#idDivImport').html('<button id="btnExport" class="BtnGeneralStyle" onclick="return CategoryTypeCatalog.exportExcel();">Xuất Excel</button>').change().show();
		disabled('shopCode');
		$('#lblMaDonVi').html("Mã đơn vị");
		$('#shopCode').val($('#shopCodeHidden').val().trim()).show();
		$("#grid").datagrid({
			  url:CategoryTypeCatalog.getGridUrl('','',1,0),
			  columns:[[		
			    {field:'shop.shopCode', title: 'Mã đơn vị', width: 95, sortable:false,resizable:false, align: 'left',formatter: function(value,row,index){
			    	if(row.shop != null){
			    		return Utils.XSSEncode(row.shop.shopCode);
			    	}
			    } },
			    {field:'cat.productInfoCode', title: 'Ngành hàng', width: 80, sortable:false,resizable:false , align: 'left',formatter:function(value,row,index){
			    	if(row.cat!=null){
			    		return Utils.XSSEncode(row.cat.productInfoCode);
			    	}
			    }},
			    {field:'minsf', title: 'Tồn kho Min', width: 75, align: 'right', sortable:false,resizable:false},
			    {field:'maxsf', title: 'Tồn kho Max', width: 75, align: 'right',sortable:false,resizable:false},
			    {field:'lead', title: 'Đi đường', width: 75, align: 'right',sortable:false,resizable:false},
			    {field:'calendarD', title: 'Ngày BH(Thứ)', width: 110, align: 'left',sortable:false,resizable:false,formatter: CategoryTypeCatalogFormatter.saleDayFormat,
			    	editor:{ 
			    		type:'text'
		            }},
			    {field:'percentage', title: 'Tỉ lệ TT(%)', width: 60, align: 'right',sortable:false,resizable:false,editor:{  
		            type:'numberbox',options:{precision:2}
		        } },
			    {field:'status', title: 'Trạng thái', width: 170, align: 'center',sortable:false,resizable:false, formatter: CategoryTypeCatalogFormatter.statusFormat}
			  ]],	  
			  pageList  : [10,20,30],
			  height:'auto',
			  scrollbarSize : 0,
			  pagination:true,
			  fitColumns:true,
			  singleSelect  :true,
			  method : 'GET',
			  rownumbers: true,	  
			  width: ($('#categoryGrid').width() - 10),
			  onLoadSuccess:function(){
				$('.datagrid-header-rownumber').html('STT');
		      	$('#grid').datagrid('resize');
		      	edit = false;
		      	disabled('shopCode');
				$('#shopCode').val($('#shopCodeHidden').val().trim()).show();
		      }
		});
	}else{
		$('#idDivImport').html('<button id="btnExport" class="BtnGeneralStyle" onclick="return CategoryTypeCatalog.exportExcel();">Xuất Excel</button>').change().show();
	}
});
function editrow(target){
	if(edit){
		return false;
	}
    $('#grid').datagrid('beginEdit', getRowIndex(target)); 
    $('td[field=status] .datagrid-editable-input').combobox('resize', 80);
	$("td[field=maxsf] input").attr("maxlength","10");
	$("td[field=minsf] input").attr("maxlength","10");
	$("td[field=lead] input").attr("maxlength","10");
	$('td[field=percentage] input').attr('maxlength','6');
	$('#minsfOld').val($('td[field=minsf] .datagrid-editable-input').val());
	$(".datagrid-btable td[field='calendarD'] table td input[type=text]").bind("focus",
    		function(){
    			var value = $(".datagrid-btable td[field='calendarD'] table td input[type=text]").val();
    			if(value != ''){
					$('#monday,#tuesday,#wednesday,#thursday,#friday,#saturday,#sunday').removeAttr('checked');
					for(var i = 0; i < value.length; i++) {
						if(value.charAt(i) == 2) {
							$('#monday').attr('checked', 'checked');
						} else if(value.charAt(i) == 3) {
							$('#tuesday').attr('checked', 'checked');
						} else if(value.charAt(i) == 4) {
							$('#wednesday').attr('checked', 'checked');
						} else if(value.charAt(i) == 5) {
							$('#thursday').attr('checked', 'checked');
						} else if(value.charAt(i) == 6) {
							$('#friday').attr('checked', 'checked');
						} else if(value.charAt(i) == 7) {
							$('#saturday').attr('checked', 'checked');
						} else if(value.charAt(i) == 1){
							$('#sunday').attr('checked', 'checked');
						}
					}
				}
    			$("#popup1").dialog({
    				onOpen:function(){
    					$(".datagrid-btable td[field='calendarD'] table td input[type=text]").blur();
    					var tabindex = -1;
    					$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
    						if (this.type != 'hidden') {
    							$(this).attr("tabindex", tabindex);
    							tabindex -=1;
    						}
    					});
    					var tabindex = 1;
    		    		$('#popup1 input,#popup1 select,#popup1 button').each(function () {
    			    		if (this.type != 'hidden') {
    				    	    $(this).attr("tabindex", tabindex);
    							tabindex++;
    			    		}
    					});
    				},
    				onClose:function(){
    					var tabindex = 1;
    					$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
    						if (this.type != 'hidden') {
    							$(this).attr("tabindex", tabindex);
    							tabindex +=1;
    						}
    					});
    				}
    			});
    			
    		    $("#popup1").dialog("open");
    		})
}
function updateActions(index){  
    $('#grid').datagrid('updateRow',{  
        index: index,  
        row:{}  
    });  
} 
function getRowIndex(target){  
    var tr = $(target).closest('tr.datagrid-row');  
    return parseInt(tr.attr('datagrid-row-index'));  
}  
function saverow(target){  
	var msg = '';
	$('#errMsg').html('').hide();
	var minsf = $('td[field=minsf] .datagrid-editable-input').val();			
	if(minsf == ""){
		msg = Utils.getMessageOfRequireCheck('stockMin','Tồn kho Min');	
		$('#errMsg').html(msg).show();			
		$('td[field=minsf] .datagrid-editable-input').focus();
		return false;
	}	
	if(minsf.replace(/,/g,'')>9999){
		msg='Tồn kho min không được vượt quá 9999';
		$('#errMsg').html(msg).show();			
		$('td[field=minsf] .datagrid-editable-input').focus();
		return false;
	}
	var maxsf = $('td[field=maxsf] .datagrid-editable-input').val();		
	if(maxsf == ""){
		msg = Utils.getMessageOfRequireCheck('stockMax','Tồn kho Max');	
		$('#errMsg').html(msg).show();			
		$('td[field=maxsf] .datagrid-editable-input').focus();
		return false;
	}
	if(maxsf.replace(/,/g,'')>9999){
		msg='Tồn kho max không được vượt quá 9999';
		$('#errMsg').html(msg).show();			
		$('td[field=maxsf] .datagrid-editable-input').focus();
		return false;
	}
	var lead = $('td[field=lead] .datagrid-editable-input').val();
	if(lead == ""){
		msg = Utils.getMessageOfRequireCheck('dateGo','Ngày đi đường');	
		$('#errMsg').html(msg).show();			
		$('td[field=lead] .datagrid-editable-input').focus();
		return false;
	}
	if(lead.replace(/,/g,'') > 999){
		msg='Ngày đi đường không được vượt quá 999';
		$('#errMsg').html(msg).show();			
		$('td[field=lead] .datagrid-editable-input').focus();
		return false;
	}
	
	var percentage = $('td[field=percentage] .datagrid-editable-input').val();
	if(percentage == ""){
		msg = Utils.getMessageOfRequireCheck('growth','Tỉ lệ tăng trưởng');
		$('#errMsg').html(msg).show();			
		$('td[field=percentage] .datagrid-editable-input').focus();
		return false;
	}else{
		$('#growth').val(percentage);
		msg = Utils.getMessageOfFloatValidate('growth','Tỉ lệ tăng trưởng');
		if(msg!=''){
			$('#errMsg').html(msg).show();			
			$('td[field=percentage] .datagrid-editable-input').focus();
			return false;
		}
	}
	var minsfOld = $('#minsfOld').val();
	if(parseInt(minsf.trim()) >= parseInt(maxsf.trim()) && parseInt(minsf) != parseInt(minsfOld.trim())){
		msg = "Tồn kho Min phải bé hơn tồn kho Max";
		$('#errMsg').html(msg).show();
		$('td[field=minsf] .datagrid-editable-input').focus();
		return false;
	}

	if(parseInt(minsf.trim()) >= parseInt(maxsf.trim())){
		msg = "Tồn kho Min phải bé hơn tồn kho Max";
		$('#errMsg').html(msg).show();
		$('td[field=maxsf] .datagrid-editable-input').focus();
		return false;
	}
	var leadKT = lead.replace(/,/g,'');
	if( parseInt(leadKT.trim())> parseInt(maxsf.trim())){
		msg='Ngày đi đường không được vượt quá tồn kho Max';
		$('#errMsg').html(msg).show();			
		$('td[field=lead] .datagrid-editable-input').focus();
		return false;
	}
	
	
	CategoryTypeCatalog.indexedit = getRowIndex(target);
    $('#grid').datagrid('endEdit', getRowIndex(target));
    CategoryTypeCatalog.change();
    
}
function cancelrow(target){  
    $('#grid').datagrid('cancelEdit', getRowIndex(target));  
} 
</script>
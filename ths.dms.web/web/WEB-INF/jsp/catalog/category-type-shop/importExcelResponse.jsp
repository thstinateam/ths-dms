<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="errorExcel" style="display: none"><s:property value="isError"/></div>
<div id="errorExcelMsg" style="display: none"><s:property value="errMsg"/></div>
<div id="totalRow" style="display: none"><s:property value="totalItem"/></div>
<div id="numFail" style="display: none"><s:property value="numFail"/></div>
<div id="fileNameFail" style="display: none"><s:property value="fileNameFail"/></div>
<div id="typeView" style="display: none"><s:property value="typeView"/></div>
<div id="lstSize" style="display: none"><s:property value="lstView.size()"/></div>
<input type="hidden" id="newToken" value='<s:property value="getSessionToken()"/>' name="newToken">
<s:iterator var="obj" value="lstView">
	<input type="hidden" class="excel-item" 
	shortCode="<s:property value="content1"/>" 
	typect="<s:property value="content2"/>" 
	payreceiptCode="<s:property value="content3"/>" 
	money="<s:property value="content4"/>" errMsg="<s:property value="errMsg" escapeHtml="false"/>" />
</s:iterator>
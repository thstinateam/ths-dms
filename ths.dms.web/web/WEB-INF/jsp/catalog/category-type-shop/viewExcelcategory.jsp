<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="k" uri="/kryptone" %>
<table border="0" cellspacing="0" cellpadding="0">
       <colgroup>
          <col style="width:85px;" />
	        <col style="width:85px;" />
	        <col style="width:85px;"/>
	        <col style="width:85px;" />
	        <col style="width:100px;" />
	        <col style="width:85px;"/>
	        <col style="width:85px;" />
	        <col style="width:85px;"/>
	        <col style="width:85px;" />
	        <col style="width:170px;" />
       </colgroup>
       <tbody>
	       <k:repeater value="listBeans" status="u">
				<k:itemTemplate>
					<tr>
				       <td class="ColsTd1 AlignCenter"><s:property value='content1'/></td>
				       <td class="ColsTd2 AlignCenter"><s:property value='content2'/></td>
				       <td class="ColsTd3 AlignCenter"><s:property value='content3'/></td>
				       <td class="ColsTd4 AlignCenter"><s:property value='content4'/></td>
				       <td class="ColsTd5 AlignCenter"><s:property value='content5'/></td>
				       <td class="ColsTd6 AlignCenter"><s:property value='content6'/></td>
				       <td class="ColsTd7 AlignCenter"><s:property value='content7'/></td>
				       <td class="ColsTd8 AlignCenter"><s:property value='content8'/></td>
				       <td class="ColsTd9 AlignCenter"><s:property value='content9'/></td>
				       <td class="ColsTd10 AlignLeft"><s:property value='content10'/></td>
				    </tr>
			    </k:itemTemplate>
			</k:repeater>
       </tbody>
</table>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="Breadcrumbs">
   <a href="/catalog/cargo-brand-distributer" class="Breadcrumbs1Item Sprite1">Danh sách ngành hàng bán cho đơn vị</a><span class="Sprite1 Breadcrumbs2Item" id="tabTitle">Thông tin ngành hàng</span>
</div>
<div class="GeneralMilkBox">
   	<div class="GeneralMilkTopBox">
       	<div class="GeneralMilkBtmBox">
               <h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin thêm mới</span></h3>
           <div class="GeneralMilkInBox ResearchSection">
           	<div class="ModuleList18Form">
                   <label class="LabelStyle Label1Style">Mã đơn vị (F9)</label>
                   <div class="Field2">
	                   <input id="shopCode" type="text" class="InputTextStyle InputText1Style" value="<s:property value="shopProduct.shop.shopCode"/>"/>
	                   <span class="RequireStyle">(*)</span>
                   </div>
                   <label class="LabelStyle Label2Style">Ngành hàng</label>
                   <div class="BoxSelect BoxSelect1">
	                   <div class="Field2">
		                  	<select id="category" class="MySelectBoxClass">
					    	<option value="-1">---Chọn ngành hàng---</option>
					    	<s:iterator value="lstCategoryType">
					    		<s:if test="id == categoryType">
					    			<option selected="selected" value='<s:property value="id"/>'>
					    				<s:property value="productInfoCode"/> - 
					    				<s:property value="productInfoName"/></option>
					    		</s:if>
					    		<s:else>
					    			<option value='<s:property value="id"/>'>
					    			<s:property value="productInfoCode"/> - 
					    			<s:property value="productInfoName"/></option>
					    		</s:else>
					    	</s:iterator>
	   					 </select>
			           		<span class="RequireStyle">(*)</span>
	                   </div>
                   </div>
                   <div class="Clear"></div>
                   <label class="LabelStyle Label1Style">Tồn kho Min</label>
                   <div class="Field2">
                   <input id="stockMin" type="text" class="InputTextStyle InputText1Style" onkeypress="return numbersonly(event,false);" value="<s:property value="shopProduct.minsf"/>" maxlength="4"/>
                   <span class="RequireStyle">(*)</span>
                   </div>
                   <label class="LabelStyle Label2Style">Tồn kho Max</label>
                   <div class="Field2">
                   <input id="stockMax" type="text" class="InputTextStyle InputText1Style" onkeypress="return numbersonly(event,false);" value="<s:property value="shopProduct.maxsf"/>" maxlength="4"/>
                   <span class="RequireStyle">(*)</span>
                   </div>
                   <label class="LabelStyle Label3Style">Ngày đi đường</label>
                   <div class="Field2">
                   <input id="dateGo" type="text" class="InputTextStyle InputText1Style" onkeypress="return numbersonly(event,false);" value="<s:property value="shopProduct.lead"/>" maxlength="3"/>
                   <span class="RequireStyle">(*)</span>
                   </div>
                   <div class="Clear"></div>
                   <label class="LabelStyle Label1Style">Ngày bán hàng</label>
                   <div class="TTCalandarBox">
                       <div class="Field2">
	                   <input id="dateSale" type="text" class="InputTextStyle InputText1Style" value="<s:property value="shopProduct.calendarD"/>" readonly="readonly"/>
	                   <span class="RequireStyle">(*)</span>
	                   </div>
                       <div class="ToolTipCalandar" style="display: none;">
                           <ul class="ResetList ToolTipCalList">
                               <li><span>Thứ 2</span><input id="monday"  type="checkbox" value="2"/></li>
                               <li><span>Thứ 3</span><input id="tuesday"  type="checkbox" value="3"/></li>
                               <li><span>Thứ 4</span><input id="wednesday"  type="checkbox" value="4"/></li>
                               <li><span>Thứ 5</span><input id="thursday"  type="checkbox" value="5"/></li>
                               <li><span>Thứ 6</span><input id="friday"  type="checkbox" value="6"/></li>
                               <li><span>Thứ 7</span><input id="saturday"  type="checkbox" value="7"/></li>
                               <li class="ItemEnd"><span>Chủ nhật</span><input id="sunday"  type="checkbox" value="1" /></li>
                           </ul>
                           <div class="Clear"></div>
                           <div class="ButtonSection">
                           		<button class="BtnGeneralStyle Sprite2" onclick="return CategoryTypeCatalog.mapValue();"><span class="Sprite2">Chọn</span></button>
                           	</div>
                       </div>
                   </div>
                   <label class="LabelStyle Label2Style">Tỉ lệ tăng trưởng</label>
                   <div class="Field2">
                   <input id="growth" type="text" class="InputTextStyle InputText1Style" value="<s:property value="convertFloatNumberOnly(shopProduct.percentage)"/>" maxlength="6"/>
                   <span class="RequireStyle">(*)</span>
                   </div>
                   <label class="LabelStyle Label3Style">Trạng thái</label>
                   <div class="BoxSelect BoxSelect1">
                   <div class="Field2">
                       <select id="status" class="MySelectBoxClass">
	                      <option value="0">Tạm ngưng</option>
	                      <option value="1" selected="selected">Hoạt động</option>
	                  </select>
	                  <span class="RequireStyle">(*)</span>
                   </div>
                   </div>
                   <div class="Clear"></div>
                   <div class="ButtonSection">
	                   <button id="btnSave" class="BtnGeneralStyle Sprite2" onclick="return CategoryTypeCatalog.change();"><span class="Sprite2">Lưu lại</span></button>
		              	<a id="btnDismiss" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" href="/catalog/cargo-brand-distributer" ><span class="Sprite2">Bỏ qua</span></a>
		              	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                   </div>
					<p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                </div>
            </div>
		</div>
    </div>
</div>
<s:hidden id="categoryId" name="categoryId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	$('#shopCode').focus();
	$('#shopCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			CommonSearch.searchShopOnDialog(function(data){
				$('#shopCode').val(data.code);
			});
		}
	});
	if($('#dateSale').val().trim() != ''){
		var value = $('#dateSale').val();
		for(var i = 0; i < value.length; i++) {
			if(value.charAt(i) == 2) {
				$('#monday').attr('checked', 'checked');
			} else if(value.charAt(i) == 3) {
				$('#tuesday').attr('checked', 'checked');
			} else if(value.charAt(i) == 4) {
				$('#wednesday').attr('checked', 'checked');
			} else if(value.charAt(i) == 5) {
				$('#thursday').attr('checked', 'checked');
			} else if(value.charAt(i) == 6) {
				$('#friday').attr('checked', 'checked');
			} else if(value.charAt(i) == 7) {
				$('#saturday').attr('checked', 'checked');
			} else if(value.charAt(i) == 1){
				$('#sunday').attr('checked', 'checked');
			}
		}
		$('#dateSale').val(CategoryTypeCatalog.parseValueToChar($('#dateSale').val().trim()));
	}
	$('#dateSale').bind('click',function(){
		$('.ToolTipCalandar').show();
	});
	if($('#categoryId').val() != null && $('#categoryId').val() > 0){
		$('#title').html('Cập nhật thông tin');
		CategoryTypeProductCatalog.getCategoryOfProduct(-2,'<s:property value="categoryType"/>','<s:property value="categoryCode"/>');
	}else{
		$('#title').html('Thông tin thêm mới');
	}
});
</script>
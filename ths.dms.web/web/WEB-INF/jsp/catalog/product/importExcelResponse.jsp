<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="k" uri="/kryptone" %>
<div id="errorExcel" style="display: none"><s:property value="isError"/></div>
<div id="errorExcelMsg" style="display: none"><s:property value="errMsg"/></div>
<div id="totalRow" style="display: none"><s:property value="totalItem"/></div>
<div id="numFail" style="display: none"><s:property value="numFail"/></div>
<div id="fileNameFail" style="display: none"><s:property value="fileNameFail"/></div>
<div id="typeView" style="display: none"><s:property value="typeView"/></div>
<div id="lstSize" style="display: none"><s:property value="lstView.size()"/></div>
<div id="tokenValue" style="display: none"><s:property value="token" /></div>
<!-- 
<div id="popup1" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralTable Table45KHSection">
          	<div class="ScrollSection">
          		<div class="BoxGeneralTTitle">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <colgroup>
			                  	  <col style="width:50px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:120px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:100px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:300px;" />
			              </colgroup>
			              <thead>
			                  <tr>
			                      <th class="ColsThFirst">STT</th>
			                      <th>Mã SP</th>
			                      <th>Tên SP</th>
			                      <th>Mã SP gốc</th>
			                      <th>Sử dụng lô</th>
			                      <th>Mã vạch</th>
			                      <th>Mã mức</th>
			                      <th>Mã flavour</th>
			                      <th>Mã packing</th>
			                      <th>UOM1</th>
			                      <th>UOM2</th>
			                      <th>Quy đổi</th>
			                      <th>KL tịnh(kg)</th>
			                      <th>Tổng KL(kg)</th>
			                      <th>Thời hạn sử dụng</th>
			                      <th>Loại thời hạn</th>
			                      <th>Brand</th>
			                      <th>Loại SP</th>
			                      <th>Hoa hồng</th>
			                      <th>Thể tích</th>
			                      <th>Tồn kho an toàn</th>
			                      <th>Mô tả</th>
			                  </tr>
			              </thead>
			          </table>
		        </div>
            	<div class="BoxGeneralTBody">
	             	<div class="ScrollBodySection">
	                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                         <colgroup>
	                         	  <col style="width:50px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:120px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:100px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:300px;" />
	                         </colgroup>
	                         <tbody>
	                          	<k:repeater value="lstView" status="status">
									<k:itemTemplate>
			                               <tr>
			                               	   <td class="ColsTd1"><s:property value="#status.index+1"/></td>
			                                   <td class="ColsTd2"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content1"/></div></td>                                                  
			                                   <td class="ColsTd3"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content2"/></div></td>
			                                   <td class="ColsTd4"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content3"/></div></td>   
			                                   <td class="ColsTd5"><div class="ColsAlignLeft" style="width: 99px"><s:property value="content4"/></div></td>                                                  
			                                   <td class="ColsTd6"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content5"/></div></td>
			                                   <td class="ColsTd7"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content6"/></div></td>
			                                   <td class="ColsTd8"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content7"/></div></td>
			                                   <td class="ColsTd9"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content8"/></div></td>   
			                                   <td class="ColsTd2"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content9"/></div></td>                                                  
			                                   <td class="ColsTd3"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content10"/></div></td>
			                                   <td class="ColsTd4"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content11"/></div></td>   
			                                   <td class="ColsTd5"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content12"/></div></td>                                                  
			                                   <td class="ColsTd6"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content13"/></div></td>
			                                   <td class="ColsTd7"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content14"/></div></td>
			                                   <td class="ColsTd8"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content15"/></div></td>
			                                   <td class="ColsTd9"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content16"/></div></td>
			                                   <td class="ColsTd6"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content17"/></div></td>
			                                   <td class="ColsTd7"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content18"/></div></td>
			                                   <td class="ColsTd8"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content19"/></div></td>
			                                   <td class="ColsTd9"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content20"/></div></td>                                             
			                                   <td class="ColsTd10 "><div class="ColsAlignLeft" style="width: 279px"><s:property value="errMsg" escape="false"/></div></td>
			                               </tr>
									</k:itemTemplate>
								</k:repeater>
	                         </tbody>
	                     </table>
                    </div>
                </div>
            </div>
            <div class="BoxDialogBtm">
                <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
            </div>
            </div>
    	</div>
    </div>
</div>
<div id="popup3" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralTable Table45Section">
          	<div class="ScrollSection">
          		<div class="BoxGeneralTTitle">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <colgroup>
			                  <col style="width:50px;" />
			                  <col style="width:150px;" />
			                  <col style="width:80px;" />
			                  <col style="width:80px;" />
			                  <col style="width:80px;" />
			                  <col style="width:80px;" />
			                  <col style="width:80px;" />
			                  <col style="width:80px;" />
			                  <col style="width:80px;" />
			                  <col style="width:80px;" />
			                  <col style="width:80px;" />
			                  <col style="width:80px;" />
			                  <col style="width:290px;" />
			              </colgroup>
			              <thead>
			                  <tr>
			                      <th class="ColsThFirst">STT</th>
			                      <th>Mã SP</th>
			                      <th>USER1</th>
			                      <th>USER2</th>
			                      <th>USER3</th>
			                      <th>USER4</th>
			                      <th>USER5</th>
			                      <th>USER6</th>
			                      <th>USER7</th>
			                      <th>USER8</th>
			                      <th>USER9</th>
			                      <th>USER10</th>
			                      <th>Thông tin lỗi</th>
			                  </tr>
			              </thead>
			          </table>
		        </div>
            	<div class="BoxGeneralTBody">
	             	<div class="ScrollBodySection">
	                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                         <colgroup>
	                         	  <col style="width:50px;" />
				                  <col style="width:150px;" />
				                  <col style="width:80px;" />
				                  <col style="width:80px;" />
				                  <col style="width:80px;" />
				                  <col style="width:80px;" />
				                  <col style="width:80px;" />
				                  <col style="width:80px;" />
				                  <col style="width:80px;" />
				                  <col style="width:80px;" />
				                  <col style="width:80px;" />
				                  <col style="width:80px;" />
				                  <col style="width:290px;" />
	                         </colgroup>
	                         <tbody>
	                          	<k:repeater value="lstView" status="status">
									<k:itemTemplate>
			                               <tr>
			                               	   <td class="ColsTd1"><s:property value="#status.index+1"/></td>
			                                   <td class="ColsTd2"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content1"/></div></td>                                                  
			                                   <td class="ColsTd3"><div class="ColsAlignLeft" style="width: 59px"><s:property value="content2"/></div></td>
			                                   <td class="ColsTd4"><div class="ColsAlignLeft" style="width: 59px"><s:property value="content3"/></div></td>   
			                                   <td class="ColsTd5"><div class="ColsAlignLeft" style="width: 59px"><s:property value="content4"/></div></td> 
			                                   <td class="ColsTd2"><div class="ColsAlignLeft" style="width: 59px"><s:property value="content5"/></div></td>                                                  
			                                   <td class="ColsTd3"><div class="ColsAlignLeft" style="width: 59px"><s:property value="content6"/></div></td>
			                                   <td class="ColsTd4"><div class="ColsAlignLeft" style="width: 59px"><s:property value="content7"/></div></td>   
			                                   <td class="ColsTd5"><div class="ColsAlignLeft" style="width: 59px"><s:property value="content8"/></div></td>    
			                                   <td class="ColsTd4"><div class="ColsAlignLeft" style="width: 59px"><s:property value="content9"/></div></td>   
			                                   <td class="ColsTd5"><div class="ColsAlignLeft" style="width: 59px"><s:property value="content10"/></div></td>
			                                   <td class="ColsTd5"><div class="ColsAlignLeft" style="width: 59px"><s:property value="content11"/></div></td>                                               
			                                   <td class="ColsTd6"><div class="ColsAlignLeft" style="width: 269px"><s:property value="errMsg" escape="true"/></div></td>
			                               </tr>
									</k:itemTemplate>
								</k:repeater>
	                         </tbody>
	                     </table>
                    </div>
                </div>
            </div>
            <div class="BoxDialogBtm">
                <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
            </div>
            </div>
    	</div>
    </div>
</div>
<div id="popup4" style="display:none;">
	<div class="GeneralDialog General2Dialog">
      	<div class="DialogProductSearch">
          	<div class="GeneralTable Table44Section">
          	<div class="ScrollSection">
          		<div class="BoxGeneralTTitle">
			          <table width="100%" border="0" cellspacing="0" cellpadding="0">
			              <colgroup>
			                  <col style="width:50px;" />
			                  <col style="width:150px;" />
			                  <col style="width:150px;" />
			                  <col style="width:150px;" />
			                  <col style="width:100px;" />
			                  <col style="width:100px;" />
			                  <col style="width:250px;" />
			              </colgroup>
			              <thead>
			                  <tr>
			                      <th class="ColsThFirst">STT</th>
			                      <th>Mã SP</th>
			                      <th>Giá chưa thuế</th>
			                      <th>Thuế suất</th>
			                      <th>Từ ngày(dd/MM/yyyy)</th>
			                      <th>Đến ngày(dd/MM/yyyy)</th>
			                      <th>Thông tin lỗi</th>
			                  </tr>
			              </thead>
			          </table>
		        </div>
            	<div class="BoxGeneralTBody">
	             	<div class="ScrollBodySection">
	                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                         <colgroup>
	                         	  <col style="width:50px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:150px;" />
				                  <col style="width:100px;" />
				                  <col style="width:100px;" />
				                  <col style="width:250px;" />
	                         </colgroup>
	                         <tbody>
	                          	<k:repeater value="lstView" status="status">
									<k:itemTemplate>
			                               <tr>
			                               	   <td class="ColsTd1"><s:property value="#status.index+1"/></td>
			                                   <td class="ColsTd2"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content1"/></div></td>                                                  
			                                   <td class="ColsTd3"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content2"/></div></td>
			                                   <td class="ColsTd4"><div class="ColsAlignLeft" style="width: 129px"><s:property value="content3"/></div></td>   
			                                   <td class="ColsTd5"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content4"/></div></td>     
			                                   <td class="ColsTd5"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content5"/></div></td>                                                
			                                   <td class="ColsTd6"><div class="ColsAlignLeft" style="width: 229px"><s:property value="errMsg" escape="false"/></div></td>
			                               </tr>
									</k:itemTemplate>
								</k:repeater>
	                         </tbody>
	                     </table>
                    </div>
                </div>
            </div>
            <div class="BoxDialogBtm">
                <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
            </div>
            </div>
    	</div>
    </div>
</div>
 -->
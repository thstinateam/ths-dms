<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
       	<ul class="ResetList FixFloat BreadcrumbList">
           <li class="Sprite1"><a href="javascript:void(0);"><s:text name="jsp.common.danhmuc"/></a></li>
           <li><span><s:text name="jsp.product.ds.san.pham.title"/></span></li>
    	</ul>
</div>
<div class="CtnOneColSection">
  	<div id="nvtab1">
    	<div class="ContentSection">
    		<div class="ToolBarSection">
            	<div class="SearchSection GeneralSSection">
                    <h2 class="Title2Style"><s:text name="jsp.common.thongtintimkiem"/></h2>
                    <div class="SearchInSection SProduct1Form">
                        <label class="LabelStyle Label1Style"><s:text name="catalog.display.product.code"/></label>
                        <input id="productCode" type="text"  class="InputTextStyle InputText1Style" maxlength="50" />
                        <label class="LabelStyle Label1Style"><s:text name="jsp.product.ten.san.pham"/></label>
						<input id="productName" type="text"  class="InputTextStyle InputText1Style" maxlength="500" />		
                        <label class="LabelStyle Label1Style"><s:text name="catalog.product.brand"/></label>
					   	<div id="brandDiv" class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="brand">
							</select>
					   	</div>
                       	<div class="Clear"></div>
                       	<label class="LabelStyle Label1Style"><s:text name="catalog.categoryvn"/></label>
                        <div class="BoxSelect BoxSelect2" >
                            <select id="category" class="MySelectBoxClassNo" multiple="multiple" onchange="ProductCatalog.fillCategoryChildToMultiSelect();">
         					</select>
                       	</div>
                       	<label class="LabelStyle Label1Style"><s:text name="catalog.categoryvn.child"/></label>
                        <div class="BoxSelect BoxSelect2" >
                            <select id="categoryChild" class="MySelectBoxClassNo" multiple="multiple">
         					</select>
                       	</div>
					   	<label class="LabelStyle Label1Style"><s:text name="catalog.product.flavour"/></label>
					   	<div id="brandDiv" class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="flavour">
							</select>
					   	</div>
                       	<div class="Clear"></div>
                        <label class="LabelStyle Label1Style"><s:text name="jsp.common.status"/></label>
                        <div class="BoxSelect BoxSelect2">
                       		<s:select id="status" name="status" cssClass="MySelectBoxClass" list="lstActiveType" listKey="value" listValue="apParamName" ></s:select>
                        </div>
                       	<label class="LabelStyle Label1Style"><s:text name="catalog.product.uom"/></label>
                       	<div class="BoxSelect BoxSelect2">
                           	<select id="uom1" class="MySelectBoxClass">
      						</select>
                       	</div>
                       	<label class="LabelStyle Label1Style"><s:text name="catalog.product.packing"/></label>
					   	<div id="brandDiv" class="BoxSelect BoxSelect2">
							<select class="MySelectBoxClass" id="packing">
							</select>
					   	</div>
                        <div class="Clear"></div>
                        <div class="BtnCenterSection">
                            <button id="btnSearch" class="BtnGeneralStyle Sprite2 BtnSearch" onclick="return ProductCatalog.search();"><span class="Sprite2"><s:text name="jsp.common.timkiem"/></span></button>
    						<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                       	 </div>
                       	 <p class="ErrorMsgStyle" id="errMsg" style="display: none;"><s:text name="jsp.common.err.data.update"/></p>
  						 <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                         <div class="Clear"></div>
                     </div>      
                   </div> 
             	<div class="Clear"></div>
         	</div>
          	<div class=GeneralCntSection>
                <h2 class="Title2Style"><s:text name="jsp.product.ds.san.pham"/></h2>
                <div class="SearchInSection SProduct1Form">
                    <div class="GridSection" id="productGrid">
                    	<table id="grid"></table>
				 </div>
                </div>
                <div class="FuncBtmSection">
					<div class="GeneralForm GeneralNoTP1Form">
                    	<div class="Func1Section cmsiscontrol" id = "insert_group_div_form_import">
							<p class="DownloadSFileStyle DownloadSFile2Style">
								<a id="downloadTemplate" class="Sprite1" href="javascript:void(0)" onclick="ProductCatalog.downloadImportPriceTemplateFile();">Tải mẫu file excel</a>
							</p>		
							<div class="DivInputFile">
								<form action="/catalog/product/importexcelfile" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
							        <input type="file" class="InputFileStyle" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
							        <input type="hidden" id="isView" name="isView">
							        <input type="hidden" id="importTokenVal" name="token" value="<s:property value='token' />" />
							        <div class="FakeInputFile">
							        	<input type="text" id="fakefilepc" style="width: 190px;" class="InputTextStyle" />
							        </div>
								</form>
						   	</div>
						    <button id="btnImport" class="BtnGeneralStyle" onclick="ProductCatalog.importExcel();">Nhập từ file</button>
                   		</div>
                        <button id="btnExport" class="BtnGeneralStyle" onclick="ProductCatalog.exportExcelData();"><s:text name="jsp.common.xuat.ra.file"/></button>	  
                	</div>
            	</div>
            	<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
				<p id="sucExcelMsg" class="SuccessMsgStyle" style="display: none"></p>
             	<div class="Clear"></div>
             </div>
    	</div>
    </div>
    <div class="Clear"></div>
</div>
<div id="responseDiv" style="display: none"></div>
<script type="text/javascript">
$(document).ready(function(){
	ProductCatalog._listCategory = new Array();		
	$('#productCode').focus();
	ProductCatalog.fillCategoriesToMultiSelect();
	ProductCatalog.fillProductInfoToSelect('', 3, 'brand');
	ProductCatalog.fillProductInfoToSelect('', 4, 'flavour');
	ProductCatalog.fillProductInfoToSelect('', 5, 'packing');
	$('#productCode').unbind('keyup');
	$('#productName').unbind('keyup');
	$('#productCode').bind('keyup', function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$('#nvtab1 #btnSearch').click();
		}
	});
	$('#productName').bind('keyup', function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$('#nvtab1 #btnSearch').click();
		}
	});
	$('#status').prepend('<option value="-2">'+Utils.XSSEncode(jsp_common_status_all)+'</option>').change(); //hien thi cho status, da ngon ngu

	ProductCatalog.fillUoms('0', 'uom', 'uom1', 1);
	
 	var productCode = $('#productCode').val();
	var productName = $('#productName').val();
	var status = $('#status').val();
	var uom1 = $('#uom1').val();

	if(isNullOrEmpty(uom1) || uom1 == '-2'){
		uom1 = "";
	}
	if(isNullOrEmpty(productName)){
		productName = "";
	}
	if(isNullOrEmpty(productCode)){
		productCode = "";
	}
	loadGrid(productCode,productName,null,uom1,status);
});

function loadGrid(productCode, productName, categories,uom1,status){
	var addIcon = '<a title="'+Utils.XSSEncode(catalog_product_them_san_pham)+'" class="cmsiscontrol" id="insert_group_insert_product" href="/catalog/product/viewdetail" >' +
    	'<img width="15" height="15" src="/resources/images/icon_add.png"/></a>';
	$("#grid").datagrid({
		  url:ProductCatalog.getGridUrl(productCode, productName, categories, null, null, null, null, uom1, status),
		  columns:[[		
		    {field:'productCode', title: catalog_product_code, width: 100, sortable:true,
		    	formatter: function (val, row) {		    		
		    		return Utils.XSSEncode(val);
		    	}
		    },
		    {field:'productName', title: catalog_product_name, width: 200, sortable:true,
		    	formatter: function (val, row) {		    		
		    		return Utils.XSSEncode(val);
		    	}
		    },
		    {field:'uom1', title: catalog_product_dvt, width: 50, sortable: true,
		    	formatter: function (val, row) {		    		
		    		return Utils.XSSEncode(val);
		    	}
		    }, 
		    {field:'uom2', title: catalog_proudct_convfact, width: 50, sortable: true,
		    	formatter: function (val, row) {		    		
		    		return Utils.XSSEncode(val);
		    	}
		    },
		    {field:'convfact', title: catalog_proudct_convfact_sl, width: 40,align:'right', sortable:true},
		    {field:'categoryName', title: catalog_categoryvn, sortable:true,
		    	formatter: function (val, row) {		    		
		    		return Utils.XSSEncode(val);
		    	}
		    },
		    {field:'subcatName', title: catalog_categoryvn_child, sortable:true,
		    	formatter: function (val, row) {		    		
		    		return Utils.XSSEncode(val);
		    	}
		    },
		    {field:'brandName', title: catalog_product_brand, sortable:true,
		    	formatter: function (val, row) {		    		
		    		return Utils.XSSEncode(val);
		    	}
		    },
		    {field:'flavourName', title: catalog_product_flavour, sortable:true,
		    	formatter: function (val, row) {		    		
		    		return Utils.XSSEncode(val);
		    	}
		    },
		    {field:'packingName', title: catalog_product_packing, sortable:true,
		    	formatter: function (val, row) {		    		
		    		return Utils.XSSEncode(val);
		    	}
		    },
		    {field:'orderIndex', title: catalog_product_order_index, sortable:true},
		    {field:'expiryDate', title: jsp_product_han_su_dung, width: 70, sortable:true,
		    	formatter: function (val, row) {
		    		if (row.expiryDate != null && row.expiryDate != '') {
		    			return row.expiryDate + ' ' + Utils.XSSEncode(row.expiryType);
		    		}
		    		return '';
		    	}
		    },
		    {field:'status', title: jsp_common_status, width: 60, sortable:true,
		    	formatter: function (val, row) {		    		
		    		return Utils.XSSEncode(val);
		    	}
		    },
		    {field:'edit', title: addIcon,
		    	width: 30, align: 'center',sortable:false,resizable:false,
		    	formatter: function(val, row, index) {
		    		return '<a title="'+msgText6+'" href="javascript:void(0)" onclick="ProductCatalog.viewProduct('+ Utils.XSSEncode(row.productId) + ');">'+
		    			'<img src="/resources/images/icon-view.png"/></a>';
		    	}
		    },
		    {field:'P', hidden : true}
		  ]],	  
		  //pageList  : [10,20,30],
		  width: 910,
		  height:'auto',
		  scrollbarSize : 0,
		  pagination:true,
		  fitColumns:true,
		  singleSelect  :true,
		  method : 'GET',
		  rownumbers: true,	  
		  width: ($('#productGrid').width()),
		  onLoadSuccess:function(){
			  $('.datagrid-header-rownumber').html(jsp_common_numerical_order);
			  updateRownumWidthForDataGrid('#productGrid');
			  var lstHeaderStyle = ["productCode","productName","uom1","uom2","convfact","categoryName","expireDate","status"];
			  Utils.updateCellHeaderStyleSort('productGrid',lstHeaderStyle);
	       	  $('#grid').datagrid('resize');
			  Utils.functionAccessFillControl('productGrid', function (data) {
				  //Xu ly cac su kien lien quan den control phan quyen 
			  });
	      }
		});

}//load grid


</script>
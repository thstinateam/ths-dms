<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="ModuleList41Form">
<label class="LabelStyle Label1Style">Mã SP</label>
<input id="productCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="product.productCode"/>"/>
<label class="LabelStyle Label2Style">Tên SP</label>
<input id="productName" type="text" class="InputTextStyle InputText3Style" disabled="disabled" value="<s:property value="product.productName"/>"/>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Thuế suất</label>
<div class="BoxSelect BoxSelect1">
		<div class="Field2">
		    <select id="tax" class="MySelectBoxClass" onchange = "return changeVat();">
		    	<option value="-1">---Chọn loại thuế---</option>
		    	<s:iterator var="obj" value="lstApParam" >
		    		<option value="<s:property value="value"/>"><s:property value="value"/></option>
		    	</s:iterator>
		    </select>
		    <span class="RequireStyle">(*)</span>
		</div>
	</div>

<label class="LabelStyle Label2Style">Giá chưa thuế</label>
<div class="Field2">
	<input id="priceNotVat" type="text" class="InputTextStyle InputText1Style" onblur="return ProductCatalog.getPrice($(this).val(),0);" maxlength="22"/>
	<span class="RequireStyle">(*)</span>
</div>
<label class="LabelStyle Label3Style">Giá có thuế</label>
<div class="Field2">
	<input id="price" type="text" class="InputTextStyle InputText1Style" onblur="return ProductCatalog.getPrice($(this).val(),1);" maxlength="22"/>
	<span class="RequireStyle">(*)</span>
</div>

<div class="Clear"></div>
<label class="LabelStyle Label1Style">Trạng thái</label>
<div class="BoxSelect BoxSelect1">
<div class="Field2">
    <select id="status" class="MySelectBoxClass">
    	<option value="-2">---Chọn trạng thái---</option>
        <option value="0">Tạm ngưng</option>
        <option value="1" selected="selected">Hoạt động</option>
    </select>
    <span class="RequireStyle">(*)</span>
</div>
</div>
<label class="LabelStyle Label2Style">Từ ngày</label>
<div class="Field2">
<input id="startDate" type="text" class="InputTextStyle InputText2Style" value="<s:date name="product.fromDate" format="dd/MM/yyyy"/>"/>
<span class="RequireStyle">(*)</span>
</div>
<label class="LabelStyle Label3Style">Đến ngày</label>
<input id="endDate" type="text" class="InputTextStyle InputText2Style" value="<s:date name="product.toDate" format="dd/MM/yyyy"/>"/>
<div class="Clear"></div>
<div class="ButtonSection">
	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return ProductCatalog.searchPrice();"><span class="Sprite2">Tìm kiếm</span></button>
	<s:if test="isViewProduct == true">
		<button id="btnCreate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return Utils.getChangedForm();"><span class="Sprite2">Thêm mới</span></button>
		<button style="display: none;" id="btnUpdate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ProductCatalog.changeProductPrice();"><span class="Sprite2">Cập nhật</span></button>
		<button style="display: none;" id="btnDismiss" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ProductCatalog.resetFormPrice();"><span class="Sprite2">Bỏ qua</span></button>
	</s:if>
	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
</div>
<p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
<div class="GeneralMilkBox GeneralMilkExtInBox">
    <div class="GeneralMilkTopBox">
        <div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Giá sản phẩm</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="productPriceGrid">
                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle"></p>
                  	<table id="grid"></table>
					<div id="pager"></div>
              	</div>
              	<div class="Clear"></div>
            </div>
        </div>
    </div>
</div>
</div>
<s:hidden id="priceId" value="0"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	applyDateTimePicker("#startDate");
	applyDateTimePicker("#endDate");
	$('.RequireStyle').hide();
	$("#grid").jqGrid({
	  url:ProductCatalog.getPriceGridUrl($('#productId').val(),'','','',1,'','-1'),
	  colModel:[		
		{name:'priceNotVat', label: 'Giá chưa thuế', width: 100, sortable:false,resizable:false, formatter: ProductCatalogFormatter.priceFormat, align: 'right' },
	    {name:'price', label: 'Giá có thuế', width: 100, sortable:false,resizable:false, formatter: ProductCatalogFormatter.priceFormat, align: 'right' },
		{name:'vat', label: 'Thuế suất', width: 100, sortable:false,resizable:false, formatter: ProductCatalogFormatter.taxFormat, align: 'right' },
	    {name:'fromDate', label: 'Từ ngày', width: 200, sortable:false,resizable:false ,formatter:'date',formatoptions:{srcformat: 'Y-m-d H:i:s',newformat: 'd/m/Y' }, align: 'center' },
	    {name:'toDate', label: 'Đến ngày', width: 200,sortable:false,resizable:false,formatter:'date',formatoptions:{srcformat: 'Y-m-d H:i:s',newformat: 'd/m/Y' } , align: 'center' },
	    {name:'status', label: 'Trạng thái', width: 80, align: 'left', sortable:false,resizable:false, formatter: ProductCatalogFormatter.statusFormat },
		{name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProductCatalogFormatter.editPriceFormatter},
	    {name:'delete', label: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProductCatalogFormatter.delPriceFormatter},
	  ],	  
	  pager : '#pager',
	  height: 'auto',
	  rownumbers: true,	  
	  width: ($('#productPriceGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});
	$('#jqgh_grid_rn').prepend('STT');
	Utils.bindFormatOnTextfield('priceNotVat',Utils._TF_NUMBER_COMMA);
	Utils.formatCurrencyFor('priceNotVat');
	Utils.bindFormatOnTextfield('price',Utils._TF_NUMBER_COMMA);
	Utils.formatCurrencyFor('price');
});
function changeVat(){
	var vat = parseInt($('#tax').val().trim());
	if($('#priceNotVat').val().trim()!=''){
		var valuePriceNotVat = parseFloat(Utils.returnMoneyValue($('#priceNotVat').val().trim()));
		if(vat >= 0){
			var price = valuePriceNotVat + valuePriceNotVat * vat / 100;
			$('#price').val(formatCurrency(price));
		}else{
			$('#price').val('');
		}
	}else{
		$('#price').val('');
	}
	
	return true;
}
</script>
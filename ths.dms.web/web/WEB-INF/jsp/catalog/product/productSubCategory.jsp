<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="k" uri="/kryptone" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <colgroup>
        <col style="width:200px;" />
        <col style="width:297px;" />
    </colgroup>
    <tbody>
    	<k:repeater value="listSubCatVO" status="status">
	        <k:itemTemplate>
		        <tr>
		            <td class="ColsTd1 AlignLeft"><s:property value="apParamName"/></td>
		            <s:hidden id="apParamCode_%{#status.index}" name="apParamCode"></s:hidden>                                                  
		            <td class="ColsTd2 ColsTdEnd">
		            	<div class="BoxSelect BoxSelect1">
		                    <s:select id="subCategory_%{#status.index}" list="listSubCat" listKey="productInfoCode" listValue="productInfoCode" name="subCategory_%{#status.index}"
								headerKey="-1" headerValue="---Chọn ngành hàng con---" cssClass="MySelectBoxClass dlgClass" value="%{lstSubSelect.get(#status.index)}">
						    </s:select>
		                </div>
		            </td>
		        </tr>
        	</k:itemTemplate>
        </k:repeater>
        <s:hidden id="lstSize" name="listSubCatVO.size()"></s:hidden>
        <s:hidden id="lstSizeSelect" name="lstApParam.size()"></s:hidden>
    </tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){
	$('.dlgClass').customStyle();
	/* var subCategory = '<s:property value="lstSubSelect"/>';
	for(var i = 0;i<$('#lstSizeSelect').val();i++){
		$('#subCategory_'+i).val(subCategory.get(i));
		$('#subCategory_'+i).change();
	} */
});
</script>	

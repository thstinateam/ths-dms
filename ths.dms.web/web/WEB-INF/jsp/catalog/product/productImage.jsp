<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="ths.dms.helper.Configuration"%>

<style>
#player_api { width:98% !important; height: 546px; }
</style>

<div class="CtnOneColSection" id="divTabImageVideo">
   <div class="ContentSection">
       <div class="GeneralCntSection GeneralCntNPSection">
       		<h2 class="Title2Style"><s:text name="jsp.common.product.image.video"/></h2>
			<div class="GeneralMediaSection" id="divFullItem">
				<div class="GeneralMedia1Section">
					<div class="GeneralMIn MediaPlayerSection" style=" padding: 8px 0 8px 8px;" >	
						<div class="PhotoLarge">
							<span class="BoxMiddle" id="videoBox" style="text-align:center">
								<div style="display:block; max-width: 715px;max-height: 551px" id="player"></div>
								<a class="Sprite1 HideText DeleteLinkStyle" onclick="return ProductCatalog.deleteMediaItem();" href="javascript:void(0)"></a>
							</span>				
						</div>					
						<span class="BoxMiddle" id="imageBox" style="text-align:center">
							<img style="max-width: 715px;max-height: 551px;width: auto;" alt=""  src="<%=Configuration.getImgServerPath()%><s:property value="lstMediaItem.get(0).url"/>" />
							<s:if test="checkPermission==1">
								<a class="Sprite1 HideText DeleteLinkStyle" onclick="return ProductCatalog.deleteMediaItem();" href="javascript:void(0)"><s:text name="jsp.common.xoa"/></a>
							</s:if>
						</span>			
					</div>
				</div>
				<div class="GeneralMedia2Section">
					<h3 class="Title3Style"><s:text name="jsp.common.tongso"/> <s:property value="vnmTotalImage"/> <s:text name="catalog.product.hinh.anh"/> <s:text name="catalog.product.va"/> <s:property value="vnmTotalVideo"/> <s:text name="catalog.product.phim"/></h3>
					<div class="GeneralMIn" style="height: 488px;overflow: hidden;">
						<div id="pageUp" style="display: none;">
							<p class="ControlUDPhotoStyle UpPhotoStyle" onclick="return ProductCatalog.getListMediaItem(0);"><a class="Sprite1 HideText" href="javascript:void(0);"><s:text name="jsp.common.s.nhan.len"/></a>
						</div>
							<ul class="ResetList MediaThumbList FixFloat" id="divMI">
								<tiles:insertTemplate template="/WEB-INF/jsp/catalog/product/productMediaItem.jsp"></tiles:insertTemplate>
							</ul>
						<div id="pageDown">
							<p class="ControlUDPhotoStyle DownPhotoStyle" onclick="return ProductCatalog.getListMediaItem(1);"><a class="Sprite1 HideText" href="javascript:void(0);"><s:text name="jsp.common.s.nhan.xuong"/></a>
						</div>
					</div>
				</div>
				<div class="Clear"></div>
			</div>
			<h2 class="Title2Style"><s:text name="common.description"/></h2>
			<div class="TextEditorSection">
				<div class="TextEditorInSection">							
					<div class="TextEditorForm">
						<textarea id="description"  name="description" rows="50" cols="80"><s:property value="productIntroduction.content" escapeHtml="false" /></textarea>
					</div>
					<div class="GeneralForm GeneralNoPForm">
						<div class="BtnRSection">
							<button id="img_description_change" class="BtnSearchStyle BtnGeneralStyle cmsiscontrol" onclick="ProductCatalog.changeProductDescription();"><s:text name="jsp.common.luu"/></button>
						</div>
						<div class="Clear"></div>
					</div>
					<div class="Clear"></div>
					<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
				</div>
			</div>
				<h2 class="Title2Style"><s:text name="jsp.common.s.product.image.video"/></h2>
				<div class="UploadMediaSection">
					<div class="MediaBox ">
						<label class="LabelStyle Label1Style Text1Style"><s:text name="jsp.common.s.product.upload.image"/>:</label>																
						<div class="UploadMediaBox"	>
							<span class="addFileBtn" style="margin-bottom: 10px; margin-top:10px; background-color: #0299cb;"><s:text name="jsp.common.s.product.chon.image.video"/></span>
							<span class="fileupload-process">
								<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-striped active" id="total-progress" style="opacity: 0; ">
									<div data-dz-uploadprogress="" style="width: 200px;" class="progress-bar progress-bar-success"></div>
								</div>
							</span>
		
							<p id="imageMsg" class="ErrorMsgStyle SpriteErr"style="display: none;"></p>	
							<div>
								<div class="table table-striped" class="files" id="previews" style="height: 140px; overflow-x: hidden; overflow-y: scroll">
									<div id="template" class="file-row" style="margin-bottom: 10px; width: 260px;float: left">
										<!-- This is used as the file preview template -->
										<div style="margin-right: 10px; width: 50px; float: left">
											<span class="preview"><img data-dz-thumbnail /></span>
										</div>
										<div style="display: inline-block; width: 200px; clear: both">
											<div style="padding-right: 10px">
												<div>
													<p class="name" style="overflow: hidden" data-dz-name></p>
													<strong class="error text-danger" data-dz-errormessage style="color: #e50303;"></strong>
												</div>
												<div style="">
													<p class="size" data-dz-size></p>
													</div>
												<div>
													<a data-dz-remove href="javascript:void(0)"><span><img border="0" src="/resources/scripts/plugins/uploadify2.1/cancel.png"></span></a>
												</div>
											</div>								
										</div>							
									</div> 
								</div>
							</div>
						</div>						
				</div>
				<div class="GeneralForm GeneralNoTPForm">
				    <div class="BtnCenterSection">
				        <button id="img_db_video_btnUpload" class="BtnGeneralStyle cmsiscontrol" onclick="return ProductCatalog.uploadImageFile(<s:property value="productId"/>);"><s:text name="jsp.common.s.tai.len"/></button>
				   </div>                        
				   </div>
				   <div class="Clear"></div>
				</div>	
			<p id="errMsgUpload" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
			<p id="sucMsgUpload" class="SuccessMsgStyle" style="display: none"></p>
		</div>
	</div>
</div>
<s:hidden id="listSize" name="lstMediaItem.size()"></s:hidden>
<s:hidden id="mediaType" name="mediaType"></s:hidden>
<s:hidden id="errorValue" value="0"></s:hidden>
<s:hidden id="mediaItemId" name="lstMediaItem.get(0).id"></s:hidden>
<s:hidden id="numIndex" value="0"></s:hidden>
<s:hidden id="successUpload" value="0"></s:hidden>
<s:hidden id="countSuccess" value="0"></s:hidden>
<s:hidden id="countFail" value="0"></s:hidden>
<s:hidden id="introductionId" name="introductionId"></s:hidden>
<s:hidden id="productId" name="productId"></s:hidden>
<s:hidden id="vnmProductName" name="productName"></s:hidden>
<s:hidden id="numPage" name="totalMediaItem"></s:hidden>
<input type="hidden" value="<s:property value="vnmTotalImage"/>" id="vnmTotalImage" />
<script type="text/javascript">
var formatUrl = '<s:property value="mediaItem.url"/>';
$(document).ready(function(){
	Utils.functionAccessFillControl('divTabImageVideo', function (data) {
		//Xu ly cac luong lien quan den Cac control phan quyen
	});
	ProductCatalog.countArray = new Map();
	if (CKEDITOR.instances['description'] != null){
		CKEDITOR.instances['description'].destroy(true); 
	}
	//hunglm16 - không cho chỉnh sửa ckeditor
	/* var allowed = '<s:property value="checkPermission" />'; 
	if (allowed != '1') {
		CKEDITOR.config.readOnly = true;
	} else {
		CKEDITOR.config.readOnly = false;
	} */
	
	var currentUser = '<s:property value="currentUser.userName"/>';
	var firstItem = '<s:property value="lstMediaItem.get(0).mediaType.value"/>';
	var lstSize = '<s:property value="lstMediaItem.size()"/>';
	var mediaType = '<s:property value="mediaItem.mediaType"/>';
	var countSuccess = 0;
	var countFail = 0;
	if(Math.floor($('#numPage').val()/8) < 1){
		$('#pageDown').hide();
	}
	if(lstSize != '' && lstSize > 0){
		$('#divFullItem').show();	
		if(mediaType!=null && mediaType==1){
			$('#player').show();
			$('#imageBox').hide();			
			if(formatUrl.substring(formatUrl.length-3,formatUrl.length) == 'avi'){
				var url = imgServerPath + formatUrl;
				$('#player').html('<a class="media {width:715, height:551}" href="'+url+'"></a>');
				$('a.media').media();
			}else {
				flowplayer("player", "/resources/scripts/plugins/flowplayer/flowplayer-3.2.16.swf",{
					plugins: {
				        audio: {
				            url: '/resources/scripts/plugins/flowplayer/audio/flowplayer.audio-3.2.10.swf'
				        }
				    },
				    clip: {
				        url: imgServerPath + formatUrl,
				        autoPlay: false,
				        autoBuffering: true,	        
				    },
				    play: {
			            replayLabel: jsp_common_s_xem_lai,
			        }
				});
			}
		}else{
			$('#player').hide();
			$('#imageBox').show();		
		}
	}else{
		$('#divFullItem').hide();
	}
	setTimeout(function(){
		$('#divMI li img').first().click();
	},100);
	// upload images
	if(DropzonePlugin._dropzoneObject != undefined && DropzonePlugin._dropzoneObject != null){
		DropzonePlugin._dropzoneObject.destroy();	
	}
	ProductCatalog.initUploadImageElement('body', 'body', '.addFileBtn');

	var w = $('.GeneralMIn.MediaPlayerSection').width();
	/* $('#videoBox').width(w);
	$('#imageBox').width(w);
	$('#imageBox img').width(w); */
	$('#videoBox #player').width(w);
	$('#videoBox #player .media').width(w);
	$('#videoBox #player object').width(w);
	
	var options = { 
	 		beforeSubmit: ProductCatalog.beforeImportItem,   
	 		success:      ProductCatalog.afterImportItem,  
	 		type: "POST",
	 		dataType: 'html'
	 }; 
	$('#importFrm').ajaxForm(options);		
});
function getFileName(){
	$('#errMsgUpload').html('').hide();
	$('#videoFileName').html(document.getElementById("videoFile").value);
}

/* function liveUpdateImageForProduct1(object, mediaType, mediaItemId, checkPermission) { */
function liveUpdateImageForProduct1(object, mediaType, mediaItemId) {
	$('#mediaItemId').val(mediaItemId);
	if (mediaType == 0) {
		$('#player').html('').hide();
		$('#imageBox').show();
		var orgSrc = object.attr('data');
		var source = imgServerPath +orgSrc;
		$('#imageBox').html('<img style="max-width: 715px;max-height: 551px" alt="" src="'+source+'" />');
	} else {
		$('#player').show();
		$('#imageBox').hide();
		var orgSrc = object.attr('data');
		var source = imgServerPath +orgSrc;
		if (orgSrc.substring(orgSrc.length - 3,orgSrc.length) == 'avi') {
			$('#player').html('<a class="media {width:715, height:551}" href="'+source+'"></a>');
			$('a.media').media();
		} else {
			flowplayer("player", "/resources/scripts/plugins/flowplayer/flowplayer-3.2.16.swf",{
				plugins: {
					audio: {
						url: '/resources/scripts/plugins/flowplayer/audio/flowplayer.audio-3.2.10.swf'
					}
				},
				clip: {
					url: source,
					autoPlay: true,
					autoBuffering: true
				},
				play: {
					replayLabel: jsp_common_s_xem_lai,
				}
			});			
		}
	}
	var w = $('.GeneralMIn.MediaPlayerSection').width();
	/* $('#videoBox').width(w);
	$('#imageBox').width(w);
	$('#imageBox img').width(w);  */
	$('#videoBox #player').width(w);
	$('#videoBox #player .media').width(w);
	$('#videoBox #player object').width(w);
}
</script>
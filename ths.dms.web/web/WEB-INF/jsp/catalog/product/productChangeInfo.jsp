<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="ModuleList151Form">

<label class="LabelStyle Label1Style" id="labelProductCode">Mã SP</label>
<input id="productCode" disabled="disabled" type="text" class="InputTextStyle InputText1Style" value="<s:property value="product.productCode"/>" maxlength="50"/>

<label class="LabelStyle Label2Style" id="labelProductName">Tên SP</label>
<input id="productName" disabled="disabled" type="text" class="InputTextStyle InputText1Style" value="<s:property value="product.productName"/>" maxlength="100"/>

<div class="Clear"></div>

<label class="LabelStyle Label1Style">Từ ngày</label>
<input id="fromDate" type="text" class="InputTextStyle InputText2Style"/>

<label class="LabelStyle Label2Style">Đến ngày</label>
<input id="toDate" type="text" class="InputTextStyle InputText2Style" value=""/>

<div class="Clear"></div>
<div class="ButtonSection">
	<button id="btnSearch" onclick="return ProductCatalog.searchChangeInfo();" class="BtnGeneralStyle Sprite2">
		<span class="Sprite2">Tìm kiếm</span>
	</button>
	<img style="display: none;" id="loadingSearch"  src="/resources/images/loading.gif" class="LoadingStyle"  />
</div>
<div class="Clear"></div>


<div class="GeneralMilkBox GeneralMilkExtInBox">
    <div class="GeneralMilkTopBox">
        <div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Thông tin tác động thay đổi</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="productChangeInfoGrid">
                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle"></p>
                  	<table id="grid"></table>
					<div id="pager"></div>
              	</div>
              	<div class="Clear"></div>
            </div>
        </div>
    </div>
</div>
<div id="showActionDetail" style="display: none;" class="GeneralMilkBox GeneralMilkExtInBox">
    <div class="GeneralMilkTopBox">
        <div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Chi tiết thay đổi</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="productActionGrid">
                  	<p id="gridNoResultAction" style="display: none" class="WarningResultStyle"></p>
                  	<table id="gridAction"></table>
					<div id="pagerAction"></div>
              	</div>
              	<div class="Clear"></div>
            </div>
        </div>
    </div>
</div>
<div class="ButtonSection">
	<button class="BtnGeneralStyle Sprite2" onclick="ProductCatalog.exportActionLog();"><span class="Sprite2">Xuất Excel</span></button>
</div>
<p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
<%-- <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" /> --%>
</div>
<%--  <s:hidden id="proFormat" name="productProgram.proFormat"></s:hidden> --%>
<s:hidden id="productId" name="productId"></s:hidden>


<script type="text/javascript" charset="UTF-8">
$(document).ready(function(){	
	setDateTimePicker('fromDate');
	setDateTimePicker('toDate');
	$('#fromDate').val(getLastWeek());
	$('#toDate').val(getCurrentDate());
	$('#fromDate').focus();
	$("#grid").jqGrid({
		  url: ProductCatalog.getSearchChangeInfoGridUrl($('#productId').val(),$('#fromDate').val().trim(),$('#toDate').val().trim()),
		  colModel:[
			{name: 'id', index:'id', hidden: true},
		    {name:'actionUser', label: 'NV thay đổi', width: 100, sortable:false,resizable:false , align: 'left'},
		    {name:'actionType', label: 'Loại thay đổi',width: 150, sortable:false,resizable:true , align: 'left', formatter: ActionTypeGridFormater.actionTypeFormatter},
		    {name:'actionDate', label: 'Ngày thay đổi',width: 80, sortable:false,resizable:true , align: 'right', formatter : 'date', formatoptions : { srcformat : 'Y-m-d H:i:s', newformat : 'd/m/Y' } },
		    {name:'actionIp', label: 'IP máy thay đổi', width: 140, align: 'right', sortable:false,resizable:true},
		    {name:'', label: 'Xem chi tiết', width: 100, align: 'center',sortable:false,resizable:false, formatter: ProductCatalogFormatter.viewChangeDetail},		  
		  ],	  
		  pager : '#pager',
		  height: 'auto',
		  rownumbers: true,	  
		  gridComplete : function() {
			updateRownumWidthForJqGrid('#productChangeInfoGrid');
		  },
		  width: ($('#productChangeInfoGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});
	$('#jqgh_grid_rn').prepend('STT');
});
</script>
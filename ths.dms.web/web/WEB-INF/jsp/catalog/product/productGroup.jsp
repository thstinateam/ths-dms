<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="ModuleList41Form">
<label class="LabelStyle Label1Style">Mã SP</label>
<input id="productCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="product.productCode"/>"/>
<label class="LabelStyle Label2Style">Tên SP</label>
<input id="productName" type="text" class="InputTextStyle InputText3Style" disabled="disabled" value="<s:property value="product.productName"/>"/>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Mã PB sử dụng</label>
<div class="BoxSelect BoxSelect1">
	<div class="Field2">
		<s:select id="department" list="lstDepartment" listKey="apParamCode" listValue="apParamCode" value="%{departmentId}"
			headerKey="-1" headerValue="---Chọn PB sử dụng---" cssClass="MySelectBoxClass">
		</s:select>
		<span class="RequireStyle">(*)</span>
	</div>
</div>
<label class="LabelStyle Label2Style">Mức</label>
<div class="BoxSelect BoxSelect1">
	<div class="Field2">
	    <s:select id="level" list="lstLevel" listKey="id" listValue="productLevelCode"
			headerKey="-1" headerValue="---Chọn mức---" cssClass="MySelectBoxClass">
	    </s:select>
	    <span class="RequireStyle">(*)</span>
	</div>
</div>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Trạng thái</label>
<div class="BoxSelect BoxSelect1">
<div class="Field2">
    <select id="status" class="MySelectBoxClass">
    	<option value="-2">---Chọn trạng thái---</option>
        <option value="0">Tạm ngưng</option>
        <option value="1" selected="selected">Hoạt động</option>
    </select>
    <span class="RequireStyle">(*)</span>
	</div>
</div>
<div class="Clear"></div>
<div class="ButtonSection">
	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return ProductCatalog.searchGroup();"><span class="Sprite2">Tìm kiếm</span></button>
	<button id="btnCreate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ProductCatalog.changeProductGroup();"><span class="Sprite2">Thêm mới</span></button>
	<button style="display: none;" id="btnUpdate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ProductCatalog.changeProductGroup();"><span class="Sprite2">Cập nhật</span></button>
	<button style="display: none;" id="btnDismiss" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ProductCatalog.resetFormGroup();"><span class="Sprite2">Bỏ qua</span></button>
	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
</div>
<p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
<div class="GeneralMilkBox GeneralMilkExtInBox">
    <div class="GeneralMilkTopBox">
        <div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Thông tin PB sử dụng</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="productGroupGrid">
                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle"></p>
                  	<table id="grid"></table>
					<div id="pager"></div>
              	</div>
              	<div class="Clear"></div>
            </div>
        </div>
    </div>
</div>
</div>
<s:hidden id="groupId" value="0"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	$("#grid").jqGrid({
	  url:ProductCatalog.getGroupGridUrl($('#productId').val(),'','',''),
	  colModel:[		
	    {name:'departmentCode', label: 'Mã PB sử dụng', width: 100, sortable:false,resizable:false, align: 'left' },
	    {name:'departmentName', label: 'Nơi sử dụng', width: 200, sortable:false,resizable:false, align: 'left' },
	    {name:'levelCode', label: 'Mức', sortable:false,resizable:false, align: 'left' },
	    {name:'status', label: 'Trạng thái', width: 80, align: 'left', sortable:false,resizable:false,formatter:ProductCatalogFormatter.statusFormat },
	    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProductCatalogFormatter.editGroupFormatter},
	    {name:'delete', label: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProductCatalogFormatter.delGroupFormatter},
	  ],	  
	  pager : '#pager',
	  height: 'auto',
	  rownumbers: true,	  
	  width: ($('#productGroupGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});
	$('#jqgh_grid_rn').prepend('STT');
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="k" uri="/kryptone" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <colgroup>
        <col style="width:200px;" />
        <col style="width:297px;" />
    </colgroup>
    <tbody>
    	<k:repeater value="lstDepartment" status="status">
	        <k:itemTemplate>
		        <tr>
		            <td class="ColsTd1 AlignLeft"><s:property value="apParamCode"/></td>
		            <s:hidden id="apParamCode_%{#status.index}" name="apParamCode"></s:hidden>                                                  
		            <td class="ColsTd2 ColsTdEnd">
	                    <input id="apParamName_<s:property value="#status.index"/>" type="text" style="width: 260px" class="InputTextStyle InputText2Style" maxlength="200" value="<s:property value="apParamName"/>"/>
		            </td>
		        </tr>
        	</k:itemTemplate>
        </k:repeater>
        <s:hidden id="lstSize" name="lstDepartment.size()"></s:hidden>
    </tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){
});
</script>	

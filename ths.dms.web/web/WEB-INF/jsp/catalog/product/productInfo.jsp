<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style>
#group_disable_btnSaveProduct {
    background: url("/resources/images/bg_input1.jpg") repeat-x scroll left top transparent;
    border: 1px solid rgb(209, 208, 208);
    padding: 5px 10px;
    color: rgb(51, 51, 51);
}
</style>

<!-- Bao trùm bên ngoài các khu: -->
<div class="ToolBarSection"><!--  -->
	<div class="SearchSection GeneralSSection">

<!-- Khu rời nhau 1 *******************************************************************************************************	-->
	 	<h2 class="Title2Style"><s:text name="jsp.product.thong.tin.san.pham"/></h2>
		 <div class="SearchInSection SProduct1Form">
		 	<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.display.product.code"/> <span id="lbStarCode" class="ReqiureStyle">*</span></label>
			<input id="productCode" type="text" class="InputTextStyle InputText1Style "
				value="<s:property value="product.productCode"/>" maxlength="50" />
			
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="jsp.product.ten.san.pham"/> <span class="ReqiureStyle">*</span></label>
			<input id="productName" type="text" class="InputTextStyle InputText1Style  "
				value="<s:property value="product.productName"/>" maxlength="50" />							

			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.product.ten.viet.tat"/></label>
			<input id="shortName" type="text" class="InputTextStyle InputText1Style "
				value="<s:property value="product.shortName"/>" maxlength="20" />	

			<div class="Clear"></div>
			
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.categoryvn"/> <span class="ReqiureStyle">*</span></label>
			<div id="categoryDiv" class="BoxSelect BoxSelect2 ">
				<select class="MySelectBoxClass categorySelectBox" id="category">
				</select>
			</div>
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.categoryvn.child"/> <span class="ReqiureStyle">*</span></label>
			<div id="categoryChildDiv" class="BoxSelect BoxSelect2 ">
				<select class="MySelectBoxClass categorySelectBox" id="categoryChild">
				</select>
			</div>
			
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.product.order.index"/><span class="ReqiureStyle"> *</span></label>
			<input id="orderIndex" type="text" class="InputTextStyle InputText1Style" value="<s:property value="product.orderIndex"/>"
				onkeypress="return numbersonly(event,false);" maxlength="9" />

			<div class="Clear"></div>
			
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.product.brand"/></label>
			<div id="brandDiv" class="BoxSelect BoxSelect2 0">
				<select class="MySelectBoxClass brandSelectBox" id="brand">
				</select>
			</div>
			
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.product.flavour"/></label>
			<div id="brandDiv" class="BoxSelect BoxSelect2 0">
				<select class="MySelectBoxClass categorySelectBox" id="flavour">
				</select>
			</div>
			
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.product.packing"/></label>
			<div id="brandDiv" class="BoxSelect BoxSelect2 0">
				<select class="MySelectBoxClass categorySelectBox" id="packing">
				</select>
			</div>			
			
			<div class="Clear"></div>
			
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.product.barcode"/></label>
			<input id="barCode" type="text" class="InputTextStyle InputText1Style "
				value="<s:property value="product.barcode"/>" maxlength="50" onkeypress="return numbersonly(event,false);" />
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.expiredate.code"/></label>
			<input id="expireNumber" type="text" class="InputTextStyle InputText4Style" value="<s:property value="product.expiryDate"/>"
				onkeypress="return numbersonly(event,false);" maxlength="3" />
			<div id="expiryTypeDiv" class="BoxSelect BoxSelect2 " style="margin-right: 10px">
				<s:select id="expiryType"  name="expiryType" cssClass="MySelectBoxClass" style="width:97px" list="lstNgayThangNam" listKey="value" listValue="apParamName" ></s:select>
			</div>
			<div class="Clear"></div>
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.product.volumn.lit"/></label>
			<input id="volumn" type="text" type="text" class="InputTextStyle InputText1Style "
				value="<s:property value="product.volumn == 0 ? '' : product.volumn "/>" maxlength="9"
				onkeypress="return decimalBox(event, 'volumn');" onkeyup="formatThousandNumber('volumn');" />
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.netweight.kg"/></label>
			<input id="netWeight"type="text" class="InputTextStyle InputText1Style "
				value="<s:property value="product.netWeight == 0 ? '' : product.netWeight"/>" maxlength="10"
				onkeypress="return decimalBox(event, 'netWeight');" onkeyup="formatThousandNumber('netWeight');" />
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.sum.netweight.kg"/></label>
			<input id="grossWeight" type="text" class="InputTextStyle InputText1Style"
				value="<s:property value="product.grossWeight == 0 ? '' : product.grossWeight"/>" maxlength="10"
				onkeypress="return decimalBox(event, 'grossWeight');" onkeyup="formatThousandNumber('grossWeight');" />
			<div class="Clear"></div>
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.product.uom1"/></label>
			<div id="uom1Div" class="BoxSelect BoxSelect2 ">
				<select class="MySelectBoxClass uom1SelectBox" onchange="changeCbPackage();" id="uom1">
				</select>
			</div>
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.product.uom2"/></label>
			<div id="uom2Div" class="BoxSelect BoxSelect2 ">
				<select class="MySelectBoxClass uom2SelectBox" id="uom2">
				</select>
			</div>
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="catalog.proudct.convfact.sl"/><span class="ReqiureStyle"> *</span></label>
			<input id="convfact" type="text" class="InputTextStyle InputText1Style" value="<s:property value="product.convfact"/>"
				onkeypress="return numbersonly(event,false);" maxlength="10" />
			
			<div class="Clear"></div>
			<div id="statusDiv" style="visibility: hidden;">
			<label class="LabelStyle Label7Style" style="margin-left:40px"><s:text name="jsp.common.status"/></label>
	           <div class="BoxSelect BoxSelect2">
	           		<s:select id="status" name="status" cssClass="MySelectBoxClass" list="lstActiveType" listKey="value" listValue="apParamName" ></s:select>
	           </div>
	        </div>
	        <div class="Clear"></div>
		</div>
		<div class="Clear"></div>
		<h2 class="Title2Style"><s:text name="catalog.product.thuoc.tinh.mo.rong"/></h2>
		<div class="SearchInSection SProduct1Form" id="propertyTabContainer">
			<tiles:insertTemplate template="/WEB-INF/jsp/catalog/attributes-product/productAttCommon.jsp"></tiles:insertTemplate>						
		</div>
	     <div class="Clear"></div>
	     
	    <p class="ErrorMsgStyle" id="errMsg" style="display: none;"></p>
		<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
		<div class="Clear"></div>	    
		<div class="GeneralForm BtnCenterSection" style="padding-bottom:15px">
			<button id="group_disable_btnSaveProduct" class="BtnGeneralStyle cmsiscontrol" onclick="ProductCatalog.changeProductInfo();"><s:text name="jsp.common.capnhat"/></button>				
		</div>
		<div class="Clear"></div>

	</div>
</div>
<div class="easyui-dialog" data-options="closed:true,modal:true" style="width: 970px; height: 575px;" id="viewBigMap">
	<div id="fancyboxBigMap" class="GeneralDialog General2Dialog" style="width: 965px; height: 535px">
		<span id="imgBtnDeleteLatLng" style="z-index: 10000; position: absolute; cursor: pointer; margin-left: 50%; margin-top: 52%; color: #215EA2">
				<img  title="<s:text name="catalog.product.img.latlng.title"></s:text>" 
					alt="<s:text name="catalog.product.img.latlng.title">
					</s:text>" src="/resources/images/icon_delete.png">
  				&nbsp;&nbsp;<s:text name="catalog.product.img.latlng.title"></s:text>	
		</span>
		<span id="imgBtnUpdateLatLng" style="z-index: 10000; position: absolute; cursor: pointer; margin-left: 60%; margin-top: 52%; color: #215EA2">	
  				<img  title="<s:text name="jsp.common.dongy"></s:text>" 
					alt="<s:text name="jsp.common.dongy">
					</s:text>" src="/resources/images/checked_small.png">
  				&nbsp;&nbsp;<s:text name="jsp.common.dongy"></s:text>
		</span>
		<span id="imgBtnCancelLatLng" style="z-index: 10000; position: absolute; cursor: pointer; margin-left: 70%; margin-top: 52%; color: #215EA2">	
  				<img  title="<s:text name="jsp.common.bo.qua"></s:text>" 
					alt="<s:text name="jsp.common.bo.qua">
					</s:text>" src="/resources/images/icon_close.png">
  				&nbsp;&nbsp;<s:text name="jsp.common.bo.qua"></s:text>
		</span>
		<div id="bigMapContainer" style="width: 965px; height: 535px"></div>
		<div class="Clear"></div>
	</div>
</div>

<s:hidden id="isView" name="isViewProduct"></s:hidden>
<input type="hidden" id="convfactHidden" value="<s:property value="product.convfact"/>" />
<script type="text/javascript">
var allowed = true;
$(document).ready(function(){
	$('.LabelStyle').css('text-align', 'right');
	//$('.LabelStyle').css('text-align', 'left');
	//$('.MarginRight10').css('margin-right', '10px');
	$('#seachStyle1CodeLabel').css('text-align', 'right');
	$('#seachStyle1NameLabel').css('text-align', 'right');
	$('#expiryTypeDiv .CustomStyleSelectBoxInner').css('width', '65px');
 	$(document).live('keypress', function(event){
        if(event.keyCode == 116){
        	var productId = '<s:property value="productId"/>';
         	if(productId == null || productId ==""){
        		$('#productCode').val('');
        		$('#productName').val('');
        		$('#shortName').val('');        		
        		$('#barCode').val('');
        		$('#volumn').val('');
        		$('#expireNumber').val('');
        		$('#netWeight').val('');
        		$('#grossWeight').val('');
        		$('#convfact').val('');
        	} 
        }
        return true;
    }); 
	var productId = '<s:property value="productId"/>';
	if (productId != '') {
		$('#productCode').attr('disabled', 'disabled');
		//$('#lbStarCode').hide();
		$('#productName').focus();
		$('#statusDiv').css('visibility', 'visible');
		//$('#statusDiv').html(htmlStatus);
	} else {
		$('#productCode').focus();
		$('#statusDiv').css('visibility', 'hidden');
		//$('#statusDiv').html('');
	}
	if (allowed != '1') {
		$('input[type=text]').attr('disabled', 'disabled');
		$('.MySelectBoxClass').attr('disabled', 'disabled');
		$('.ReqiureStyle').hide();
		$('input:disabled').css('background', '#fff');		
	}
	$('#expiryType').prepend('<option value="-1" selected="selected">'+jsp_common_chon+'</option>');// hien thi expiryType, da ngon ngu
	
	var expiryType = '<s:property value="product.expiryType" />';
	$('#expiryType').find('option[value='+ expiryType +']').attr('selected','selected');
	$('#expiryType').change();
	
	var uom1 = '<s:property value="product.uom1" />';
	var uom2 = '<s:property value="product.uom2" />';
	var catId = '<s:property value="product.cat.id" />';
	var subCatId = '<s:property value="product.subCat.id" />';
	var brandId = '<s:property value="product.brand.id" />';
	var flavId = '<s:property value="product.flavour.id" />';
	var packingId = '<s:property value="product.packing.id" />';
	var checkLot = '<s:property value="product.checkLot" />';
	if(checkLot != null){
		$('#lot').val(checkLot).change();
	}
	var status = '<s:property value="product.status" />';
	if(status != null ){
		var statusView = activeType.parseValue(status);
		$('#status').val(statusView).change();
	}
	Utils.formatCurrencyFor('convfact');
	
	ProductCatalog.fillProductInfoToSelect(catId, 1, 'category'); // load nganh hang(category)
	ProductCatalog.fillProductInfoToSelect(brandId, 3, 'brand');
	ProductCatalog.fillProductInfoToSelect(flavId, 4, 'flavour');
	ProductCatalog.fillProductInfoToSelect(packingId, 5, 'packing');
	ProductCatalog.changeCategory('category',catId, 2, 'categoryChild', null, subCatId);
	ProductCatalog.fillUoms(uom1, 'uom', 'uom1');
	ProductCatalog.fillUoms(uom2, 'uom', 'uom2');	
	
	if($('#netWeight').val()!=''){
		var v = $('#netWeight').val();
		v = formatCurrency(v);
		$('#netWeight').val(v);		
	}
	if ($('#grossWeight').val()!='') {		
		var v = $('#grossWeight').val();
		v = formatCurrency(v);
		$('#grossWeight').val(v);
	}

	if($('#volumn').val()!=''){		
		var v = $('#volumn').val();
		if (v.length > 7) {
			v = v.substring(0, 7);
		}
		v = formatCurrency(v);
		$('#volumn').val(v);
	}
	
	if($('#convfact').val()!=''){
		var v = $('#convfact').val();
		v = formatCurrency(v);
		$('#convfact').val(v);
	}
	
});
function format_number(dec, fix) {
	fixValue = parseFloat(Math.pow(10, fix));
	retValue = parseInt(Math.round(dec * fixValue)) / fixValue;
	return retValue;
}

function decimalBox(event, objectId, fal) {
	if (objectId == undefined) {
		return false;
	}
	var key;
	if (window.event) {
		key = window.event.keyCode;
	}else if (event) {
		key = event.which;
	}else {
		return true;
	}
	if (key==null || key==0 || key==8 ||  key==9 || key==13 || key==27) {
		return true;
	} else {
		var key1 = String.fromCharCode(key);
		var s1 = $('#' + objectId).val();
		s1 = s1.replace(/,/g, '');
		var p = s1.indexOf('.');
		if (p > -1 && key1 == '.') {
			return false;
		}
		if(fal != null && fal != undefined){
			var ponterPosition = Number($('#' + objectId)[0].selectionStart);
			var al = s1.indexOf('-');
			if((key1 == '-' && al >0) || (ponterPosition>0 && key1=='-') || al >=0){
				return false;
			}
			return /[0-9.-]$/.test(key1);
		}else{
			return /[0-9.]$/.test(key1);
		}
	}
}

function formatThousandNumber(objId) {
	var val = $('#' + objId).val().trim();
	if (val == '') {
		return false;
	}
	val = val.replace(/,/g, '');
	val = formatCurrency(val);
	$('#' + objId).val(val);
}

function changeCbPackage(){
	var uom1 = $('#uom1').val();
	if(uom1 == 'CHAI'){
		enableSelectbox('baoBi');		
	}else{	
		$('#baoBi').val(-2);
		$('#baoBi').change();
		disableSelectbox('baoBi');
	}
	if(allowed != '1'){
		disableSelectbox('baoBi');
	}
}
</script>
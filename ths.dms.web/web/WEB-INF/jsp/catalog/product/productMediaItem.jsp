<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="k" uri="/kryptone"%>
<%@page import="ths.dms.helper.Configuration"%>
<s:if test="totalMediaItem > 0">
	<s:iterator value="lstMediaItem" status="status">
		<li><a href="javascript:void(0)"> 
			<div style="height: 102px;width: 142px;text-align: center;">
				<span data="<s:property value="url"/>">
					<s:if test="mediaType.getValue()==0">
						<img onclick="return liveUpdateImageForProduct1($(this).parent(),
									<s:property value="mediaType.getValue()"/>,
									<s:property value="id"/>);"
									<%-- <s:property value="checkPermission"/>);" --%> 
									src="<%=Configuration.getImgServerPath()%><s:property value="thumbUrl"/>">
					</s:if>
					<s:else>
						<input type="hidden" value="1" id="sttVideo" />																			<!-- ,<s:property value="checkPermission"/>); -->
						<img onclick="return liveUpdateImageForProduct1($(this).parent(),<s:property value="mediaType.getValue()"/>,<s:property value="id"/>);"  
								height ="102" width ="142"	 src="<%=Configuration.getStaticServerPath()%><s:property value="thumbUrl"/>">
					</s:else>						
				</span>
			</div></a>
		</li>
	</s:iterator>
</s:if>
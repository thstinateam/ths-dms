<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="ModuleList41Form">
<label class="LabelStyle Label1Style">Mã SP</label>
<input id="productCode" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="product.productCode"/>"/>
<label class="LabelStyle Label2Style">Tên SP</label>
<input id="productName" type="text" class="InputTextStyle InputText3Style" disabled="disabled"  value="<s:property value="product.productName"/>"/>
<div class="Clear"></div>
<label class="LabelStyle Label1Style">Mã đơn vị(F9)</label>
<div class="Field2">
	<input id="shopCode" type="text" class="InputTextStyle InputText1Style" value="<s:property value="psStock.shop.shopCode"/>"/>
	<span class="RequireStyle">(*)</span>
</div>
<label class="LabelStyle Label2Style">Tồn kho an toàn</label>
<div class="Field2">
	<input id="quantityMax" type="text" class="InputTextStyle InputText3Style" onkeypress="return numbersonly(event, false);" value="<s:property value="psStock.value"/>"/>
	<span class="RequireStyle">(*)</span>
</div>
<div class="Clear"></div>
<div class="ButtonSection">
	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return ProductCatalog.searchShop();"><span class="Sprite2">Tìm kiếm</span></button>
	<button id="btnCreate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ProductCatalog.changeProductShop();" ><span class="Sprite2">Thêm mới</span></button>
	<button style="display: none;" id="btnUpdate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ProductCatalog.changeProductShop();"><span class="Sprite2">Cập nhật</span></button>
	<button style="display: none;" id="btnDismiss" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return ProductCatalog.resetFormShop();"><span class="Sprite2">Bỏ qua</span></button>
	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
</div>
<p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
<div class="GeneralMilkBox GeneralMilkExtInBox">
    <div class="GeneralMilkTopBox">
        <div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Thông tin tồn kho an toàn đơn vị</span></h3>
            <div class="GeneralMilkInBox">
                <div class="ResultSection" id="productShopGrid">
                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle"></p>
                  	<table id="grid"></table>
					<div id="pager"></div>
              	</div>
              	<div class="Clear"></div>
            </div>
        </div>
    </div>
</div>
</div>
<s:hidden id="shopId" value="0"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	$("#grid").jqGrid({
	  url:ProductCatalog.getShopGridUrl($('#productId').val(),'',''),
	  colModel:[		
	    {name:'shop.shopCode', label: 'Mã đơn vị', width: 100, sortable:false,resizable:false, align: 'left' },
	    {name:'shop.shopName', label: 'Tên đơn vị', width: 200, sortable:false,resizable:false , align: 'left'},
	    {name:'value', label: 'Tồn kho an toàn', width: 100,sortable:false,resizable:false , align: 'right'},
	    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProductCatalogFormatter.editShopFormatter},
	    {name:'delete', label: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false, formatter: ProductCatalogFormatter.delShopFormatter},
	  ],	  
	  pager : '#pager',
	  height: 'auto',
	  rownumbers: true,	  
	  width: ($('#productShopGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});
	$('#jqgh_grid_rn').prepend('STT');
	$('#shopCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			CommonSearch.searchShopOnDialog(function(data){
				$('#shopCode').val(data.code);
			});
		}
	});
});
</script>
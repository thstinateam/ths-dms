<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
	    <li class="Sprite1"><a href="/catalog/product/infoindex"><s:text name="catalog.product.san.pham"/></a></li>
	    <li><span><s:text name="jsp.product.thong.tin.san.pham"/></span></li>
	</ul>
</div>
<div class="CtnOneColSection">
   	<div class="ContentSection">
       	<div class="ToolBarSection">
	    	<div class="SearchSection GeneralSSection">
	        	<div class="TabSection">
	            	<ul class="ResetList TabSectionList">
	                	<li><a id="infoTab" href="javascript:void(0);" onclick="ProductCatalog.gotoTab(0);" class="Sprite1 Active"><span class="Sprite1"><s:text name="jsp.product.thong.tin.san.pham"/></span></a></li>
		            	<s:if test="productId != null">
		            		<li><a id="imageTab" href="javascript:void(0);" onclick="ProductCatalog.gotoTab(1);" class="Sprite1"><span class="Sprite1"><s:text name="jsp.common.product.image"/></span></a></li>
		            	</s:if>
		        	</ul>
	              	<div class="Clear"></div>
	           </div>
	       </div>            
		</div>
		<div id="tabContent1">
			<tiles:insertTemplate template="/WEB-INF/jsp/catalog/product/productInfo.jsp"></tiles:insertTemplate>
		</div>
		<div id="tabContent2">
			<div id="divGeneral"  style="display: none;"></div> 
		</div>
    </div>
</div>
<s:hidden id="productId" name="productId"></s:hidden>
<s:hidden id="checkPermission" name="checkPermission"></s:hidden>

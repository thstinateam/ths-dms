<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<div id="content">
        	<div class="BreadcrumbSection">
                <ul class="ResetList FixFloat BreadcrumbList">
                    <li class="Sprite1"><a href="#">Danh mục</a></li>
                    <li><span>Nhóm sản phẩm tạo PO Auto mặc định</span></li>
                </ul>
            </div>
            <div class="CtnOneColSection">
                <div class="ContentSection">
                	<div class="ToolBarSection">
                        <div class="SearchSection GeneralSSection">
                        	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                            <div class="SearchInSection SProduct1Form">
                            	<label class="LabelStyle Label4Style">Mã nhóm</label>
                                <input id="groupCode" type="text" class="InputTextStyle InputText1Style" maxlength="20" />
                                <label class="LabelStyle Label4Style">Tên nhóm</label>
                                <input id="groupName" type="text" class="InputTextStyle InputText1Style" maxlength="250" />
                                                             
                                <label class="LabelStyle Label4Style">Trạng thái</label>                               
                                <div class="BoxSelect BoxSelect5">
									<select id="status" class="MySelectBoxClass" name="LevelSchool">
										<option value="-2">---Chọn trạng thái---</option>
									    <option value="0">Tạm ngưng</option>
									    <option value="1" selected="selected">Hoạt động</option>
									</select>
								</div>
                                
                                <div class="Clear"></div>
                                <div class="BtnCenterSection">
                                    <button class="BtnGeneralStyle cmsiscontrol" id="btnSearch" onclick="return GroupPOAuto.search();">Tìm kiếm</button>
                                </div>
                                <div class="Clear"></div>
                                <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                            </div>
                        </div>
                        <div class="Clear"></div>
                    </div>
                    <div class="GeneralCntSection">
                    	<h2 class="Title2Style">Danh sách nhóm</h2>
                    	<div class="GridSection">
                        	<div class="ResultSection" id="productGrid">
		                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
		                  	<table id="grid" class="easyui-datagrid"></table>
		              		</div>
                        </div>
                        
                    </div>
                </div>
                <div class="Clear"></div>
                <div class="GeneralCntSection">
            	
            	<div id="menuSecGoods">
            		<h2 class="Title2Style">Danh sách ngành hàng</h2>
            		<div class="GridSection">
	               		<div class="ResultSection" id="productGrid">
	                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
	                  	<table id="gridSecGoods" class="easyui-datagrid"></table>
						<div id="pager"></div>
	              		</div>
                    </div>
                </div>
            	
            	<div id="menuSecGoodsChild">
            		<h2 class="Title2Style">Danh sách ngành hàng con</h2>
            		<div class="GridSection">
                      	<div class="ResultSection" id="productGrid">
	                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
	                  	<table id="gridSecGoodsBrand" class="easyui-datagrid"></table>
						<div id="pager"></div>
              			</div>
                    </div>
            	</div>
            	<div id="menuProduct">	
            	<h2 class="Title2Style">Danh sách sản phẩm</h2>
                  	<div class="GridSection">
                      	<div class="ResultSection" id="productGrid">
		                  	<table id="gridProduct" class="easyui-datagrid"></table>
              			</div>
                    </div>
              </div>
            </div>
            </div>
            
      <div id="popupSecFoods" style="visibility: hidden;">      
    <div id="popup1" class="easyui-dialog" title="Thêm ngành hàng" style="width:616px;height:auto;"data-options="closed:true,modal:true">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
            	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                <label class="LabelStyle Label2Style">Mã NH</label>
                <input id="codeSecGoods" type="text" class="InputTextStyle InputText5Style" maxlength="20" />
                <label class="LabelStyle Label2Style">Tên NH</label>
                <input id="nameSecGoods" type="text" class="InputTextStyle InputText5Style"  maxlength="250"  />
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSearchNH" class="BtnGeneralStyle" onclick="return GroupPOAuto.searchSecGoods();">Tìm kiếm</button>
                </div>
                <div class="Clear"></div>
            </div>
            <div class="GeneralForm Search1Form">
            	<h2 class="Title2Style Title2MTStyle">Danh sách ngành hàng</h2>
                <div class="GridSection" id="gridSecGoodsPopContainer">
                <table id="gridSecGoodsPop"></table>                  
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnAddCategory" class="BtnGeneralStyle" onclick="GroupPOAuto.addCategory();">Cập nhật</button>
                </div>
                <p id="errMsgPopup1" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
            </div>
		</div>
    </div>
    </div>
    <div id="popupSecFoodsBrand" style="visibility: hidden;">      
    <div id="popup2" class="easyui-dialog" title="Thêm ngành hàng con" style="width:616px;height:auto;"data-options="closed:true,modal:true">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
            	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                <label class="LabelStyle Label2Style">Mã NHC</label>
                <input id="codeSecGoodsBrand" type="text" class="InputTextStyle InputText5Style" maxlength="20" />
                <label class="LabelStyle Label2Style">Tên NHC</label>
                <input id="nameSecGoodsBrand" type="text" class="InputTextStyle InputText5Style" maxlength="250"  />
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSearchNHC" class="BtnGeneralStyle" onclick="return GroupPOAuto.searchSecGoodsBrand();">Tìm kiếm</button>
                </div>
                <div class="Clear"></div>
            </div>
            <div class="GeneralForm Search1Form">
            	<h2 class="Title2Style Title2MTStyle">Danh sách ngành hàng con</h2>
                <div class="GridSection" id="gridSecGoodsPopBrandContainer">
                <table id="gridSecGoodsPopBrand"></table>                  
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnAddSubCategory" class="BtnGeneralStyle" onclick="GroupPOAuto.addSubCategory();">Cập nhật</button>
                </div>
                <p id="errMsgPopup2" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
            </div>
		</div>
    </div>
    </div>
    
	</div>
	 <s:hidden id="idCode1" name="shopCode1"></s:hidden>
	 <s:hidden id="idCode2" name="shopCode2"></s:hidden>
<div id="divPopProduct" style="visibility:hidden;">	 
<div id="popupProductSearch" class="easyui-dialog" title="Thêm sản phẩm" style="width:616px;height:auto;" data-options="closed:true,modal:true">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
            	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                <label class="LabelStyle Label2Style">Mã sản phẩm</label>
                <input id="productCode" type="text" class="InputTextStyle InputText5Style" maxlength="20" />
                <label class="LabelStyle Label2Style">Tên sản phẩm</label>
                <input id="productName" type="text" class="InputTextStyle InputText5Style" maxlength="250"  />
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSeachProductTree" class="BtnGeneralStyle"  onclick="return GroupPOAuto.searchProductTree();">Tìm kiếm</button>
                </div>
                <div class="Clear"></div>
            </div>
            <div class="GeneralForm Search1Form">
            	<h2 class="Title2Style Title2MTStyle">Cây sản phẩm</h2>
            	<div  style="max-height:250px;overflow:auto;">
            		<div id="treeNoResult"></div>
	                <div class="ReportTreeSection ReportTree2Section" >
	                	  <div id="rpSection"></div>
	                      <ul id="tree" class="easyui-tree" style="visibility:hidden;"></ul>
	                </div>
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSaveProduct" class="BtnGeneralStyle" onclick="return GroupPOAuto.createGroupPOAutoDetail();">Cập nhật</button>
                </div>
                 <p id="errMsgPopup3" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
            </div>
		</div>
    </div>
 </div>
	<div id ="searchStyle1EasyUIDialogDiv" style="width:550px;display: none" >
	<div id="searchStyle1EasyUIDialogCreate" class="easyui-dialog" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="seachStyle1CodeLabel" class="LabelStyle Label2Style">Mã nhóm<span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Code" maxlength="50" tabindex="1" type="text"  class="InputTextStyle InputText2Style" />
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style" >Tên nhóm<span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Name" type="text" maxlength="250" tabindex="2" class="InputTextStyle InputText2Style" />
				<div class="Clear"></div>
				<label class="LabelStyle Label2Style">Trạng thái<span class="RequireStyle" style="color:red">*</span></label>
					<div class="BoxSelect BoxSelect6">
						<select id="statusDialog" class="MySelectBoxClass">
								<option value="1" selected="selected">Hoạt động</option>
								<option value="0">Tạm ngưng</option>
							</select>
					</div>
                <label id="seachStyle1AddressLabel" style="display: none;width: 100px;" class="LabelStyle Label1Style">Địa chỉ</label>
                <input id="seachStyle1Address" type="text" style="display: none;width: 409px;"  maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
				<input type="hidden" name="" id="searchStyle1Url"/>
				<input type="hidden" name="" id="searchStyle1CodeText"/>
				<input type="hidden" name="" id="searchStyle1NameText"/>
				<input type="hidden" name="" id="searchStyle1IdText"/>
				<s:hidden id="searchStyle1AddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="BtnCenterSection" id="clickEvent">
					<button id="__btnSave" class="BtnGeneralStyle">Cập nhật</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearchCreate" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
			</div>
		</div>
	</div>
</div>
<div id ="searchStyle1EasyUIDialogDivUpdate" style="width:550px;display: none" >
	<div id="searchStyle1EasyUIDialogUpdate" class="easyui-dialog" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="seachStyle1CodeLabel" class="LabelStyle Label2Style">Mã nhóm<span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Code" maxlength="50" tabindex="1" type="text"  class="InputTextStyle InputText2Style" />
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style" >Tên nhóm<span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Name" type="text" maxlength="250" tabindex="2" class="InputTextStyle InputText2Style" />
				<div class="Clear"></div>
				<label class="LabelStyle Label2Style">Trạng thái<span class="RequireStyle" style="color:red">*</span></label>
					<div class="BoxSelect BoxSelect6" >
						<select  id="statusDialogUpdate" class="MySelectBoxClass">
								<option value="1">Hoạt động</option>
								<option value="0">Tạm ngưng</option>
							</select>
					</div>
                <label id="seachStyle1AddressLabel" style="display: none;width: 100px;" class="LabelStyle Label1Style">Địa chỉ</label>
                <input id="seachStyle1Address" type="text" style="display: none;width: 409px;"  maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
				<input type="hidden" name="" id="searchStyle1Url"/>
				<input type="hidden" name="" id="searchStyle1CodeText"/>
				<input type="hidden" name="" id="searchStyle1NameText"/>
				<input type="hidden" name="" id="searchStyle1IdText"/>
				<s:hidden id="searchStyle1AddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="BtnCenterSection" id="clickEvent">
					<button id="__btnSave1" class="BtnGeneralStyle">Cập nhật</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearchUpdate" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	GroupPOAuto._mapNode = new Map();
	GroupPOAuto._mapCheck = new Map();
	$('#groupCode').focus();
	var status = $('#status').val();
	
	//var data = '<a id="btnAdd" href="javascript:void(0);"  onclick="return GroupPOAuto.openDialogCreateGroupPOAuto(0);"><img src="/resources/images/icon-add.png"></a>';
	//var title = getFormatterControlGrid('btnAddGrid', data);
	$("#grid").datagrid({
		  url:GroupPOAuto.getGridUrl('','',status),
	  	  pageList  : [10,20,30],
		  width: ($('#productGrid').width()),
		  pageSize : 10,
		  checkOnSelect :true,
		  height:'auto',
		  scrollbarSize : 0,
		  pagination:true,			  
		  fitColumns:true,		     
	      singleSelect:true,
		  method : 'GET',
		  rownumbers: true,
		  columns:[[	
			{field: 'groupCode', title: 'Mã nhóm', width: 100, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field: 'groupName', title: 'Tên nhóm', width: 340, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field: 'status', title: 'Trạng thái', width: 80, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
		    	if(row.status == 1){
		    		return 'Hoạt động';
		    	}
		    	if(row.status == 0){
		    		return 'Tạm ngưng';
		    	}
		    }},
		    {field: 'cat', title: 'NH', width: 30, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
		    	var data = '<a href="javascript:void(0);" onclick="return GroupPOAuto.searchCat('+row.id+',0);"><img src="/resources/images/icon-view.png"></a>';
 		    	return getFormatterControlGrid('btnDetailGrid', data);
		    }},
		    {field: 'subcat', title: 'NHC', width: 30, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
		    	var data = '<a href="javascript:void(0);" onclick="return GroupPOAuto.searchSubCat('+row.id+',1);"><img src="/resources/images/icon-view.png"></a>';
		    	return getFormatterControlGrid('btnDetailGrid1', data);
		    }},
		    {field: 'product', title: 'SP', width: 30, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
		    	var data = '<a href="javascript:void(0);" onclick="return GroupPOAuto.searchProduct('+row.id+',2);"><img src="/resources/images/icon-view.png"></a>';
		    	return getFormatterControlGrid('btnDetailGrid2', data);
		    }},
		    {field: 'edit', title: '<a id="btnAdd" style="display:none;" href="javascript:void(0);"  onclick="return GroupPOAuto.openDialogCreateGroupPOAuto(0);"><img src="/resources/images/icon-add.png"></a>', width: 30, align: 'center',sortable:false,resizable:false,
		    formatter:function(value,row,index){
		    	var data = '<a href="javascript:void(0);" onclick="return GroupPOAuto.openDialogUpdateGroupPOAuto(1,'+row.id+',\''+row.groupCode+'\',\''+row.groupName+'\','+row.status+');"><img src="/resources/images/icon-edit.png"></a>';
 		    	return getFormatterControlGrid('btnEditGrid', data);
		    }},	
		    {field: 'id', index: 'id', hidden: true},	   
		  ]],
		  onLoadSuccess:function(){
				$('.datagrid-header-rownumber').html('STT');				
				showIconAddOfFirstControlGrid('btnAddGrid', 'btnAdd');
		  }
	});
	
	$('#groupCode').bind('keyup',function(event){
		if(event.keyCode == 13){
			GroupPOAuto.search();
		}
	});
	$('#groupName').bind('keyup',function(event){
		if(event.keyCode == 13){
			GroupPOAuto.search();
		}
	});
	$('#menuSecGoods').hide();
	$('#menuSecGoodsChild').hide();
	$('#menuProduct').hide();
	$('#popupProduct').hide();
	Utils.bindAutoButtonEx('.easyui-dialog','btnSeachProductTree');
	Utils.bindAutoButtonEx('.easyui-dialog','btnSearchNH');
	Utils.bindAutoButtonEx('.easyui-dialog','btnSearchNHC');
});

</script>
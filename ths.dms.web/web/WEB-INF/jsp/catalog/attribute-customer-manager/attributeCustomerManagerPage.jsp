<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="#">Danh mục</a></li>
		<li><span>Danh sách thuộc tính khách hàng</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Mã thuộc tính</label> 
					<input id="attributeCode" type="text" class="InputTextStyle InputText1Style" /> 
					<label class="LabelStyle Label1Style">Tên thuộc tính</label> 
					<input id="attributeName" type="text" class="InputTextStyle InputText1Style" /> 
					<label class="LabelStyle Label1Style">Loại giá trị</label>
					<div class="BoxSelect BoxSelect2">
					 	<select id="valueType" multiple="multiple">
					 	</select>
					</div>
<!-- 					<div class="BoxSelect BoxSelect2"> -->
<%-- 						<select  id="valueType" class="MySelectBoxClass"> --%>
<!-- 							<option value="-1" selected="selected">Tất cả</option> -->
<!-- 							<option value="1">Chữ</option> -->
<!-- 							<option value="2">Số</option> -->
<!-- 							<option value="3">Ngày tháng</option> -->
<!-- 							<option value="4">Danh sách (chọn một)</option> -->
<!-- 							<option value="5">Danh sách (chọn nhiều)</option> -->
<%-- 						</select> --%>
<!-- 					</div> -->
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
						<select id="status" class="MySelectBoxClass">
							<option value="-2"> Tất cả</option>
							<option value="0"> Tạm ngưng</option>
							<option value="1" selected="selected"> Hoạt động</option>
						</select>
					</div>
					<s:if test="lstAttributeApParam!=null && !lstAttributeApParam.isEmpty()">
						<label class="LabelStyle Label1Style">Đối tượng</label>
						<div class="BoxSelect BoxSelect5" >
							<select class="MySelectBoxClas"  id="object"  multiple="multiple">
								<option value="-1">--Tất cả--</option>
								<s:iterator value="lstAttributeApParam">
									<option value='<s:property value="apParamCode"/>'><s:property value="apParamName" /></option>
								</s:iterator>
							</select>
						</div>
					</s:if>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle" onclick="return AttributeCustomerManager.search();">Tìm kiếm</button>
					</div>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="GeneralCntSection">
			<h2 class="Title2Style">Danh sách thuộc tính</h2>
			<div class="GridSection" id="gridContainer">
				<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
				<table id="attributeDatagrid" class="easyui-datagrid"></table>
				<div id="pager"></div>
			</div>
			<div class="Clear"></div>
		</div>
		<div id="attributeDetailDataDiv" class="GeneralCntSection">
			<h2 class="Title2Style">Danh sách giá trị</h2>
			<div id="attributeDetailDataGridSection2" class="GridSection GridSection2">
				<p id="gridDetailNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
				<table id="attributeDetailDatagrid" class="easyui-datagrid"></table>
				<div id="detailPager"></div>
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div id="searchStyle1EasyUIDialogDiv" style="display: none;"></div>
<div id="popupAttribute" class="easyui-dialog" title="Thông tin thuộc tính" style="width:550px;height:220px;" data-options="closed:true,modal:true">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form">
                <label class="LabelStyle Label1Style">Mã thuộc tính <span class="ReqiureStyle">*</span></label>
                <input id="attributeCodePop" type="text" class="InputTextStyle InputText2Style" maxlength="40" />
                <label class="LabelStyle Label2Style">Tên thuộc tính <span class="ReqiureStyle">*</span></label>
                <input id="attributeNamePop" type="text" class="InputTextStyle InputText2Style" maxlength="250"/>
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style">Loại giá trị <span class="ReqiureStyle">*</span></label>
                <div id="valueTypePopDiv" class="BoxSelect BoxSelect6">
                    <select id="valueTypePop" class="MySelectBoxClass">
                        <option value="-1" selected="selected">Tất cả</option>
                        <option value="1">Chữ</option>
                        <option value="2">Số</option>
                        <option value="3">Ngày tháng</option>
                        <option value="4">Danh sách(chọn một)</option>
                        <option value="5">Danh sách(chọn nhiều)</option>
                    </select>
                </div>
                <label class="LabelStyle Label6Style">Trạng thái <span class="ReqiureStyle">*</span></label>
                <div id="statusPopDiv" class="BoxSelect BoxSelect6">
                    <select id="statusPop" class="MySelectBoxClass">
                        <option value="0">Tạm ngưng</option>
                        <option value="1" selected="selected">Hoạt động</option>
                    </select>
                </div>
                <s:if test="lstAttributeApParam!=null && !lstAttributeApParam.isEmpty()">
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Đối tượng<span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect5" >
						<select class="MySelectBoxClass"  id="objectPop">
							<option value="-1">Tất cả</option>
							<s:iterator value="lstAttributeApParam">
								<option value='<s:property value="apParamCode"/>'><s:property value="apParamName" /></option>
							</s:iterator>
						</select>
					</div>
				</s:if>
				<div class="Clear"></div>
                <label class="LabelStyle Label1Style">Mô tả</label>
                <input id="notePop" type="text" class="InputTextStyle InputText3Style" />
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSaveAttr" onclick="return AttributeCustomerManager.saveAttribute();" class="BtnGeneralStyle">Cập nhật</button>
                </div>
                <input type="hidden" id="id" value="0" />
            </div>
            <div class="Clear"></div>
            <p id="errMsgPop" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
            <p id="successMsg" style="display: none;" class="SuccessMsgStyle"></p>
		</div>
    </div>
    
    <div id="popupAttributeDetail" class="easyui-dialog" title="Thông tin giá trị" style="width:550px;height:200px;" data-options="closed:true,modal:true">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form">
                <label class="LabelStyle Label1Style">Mã GT<span class="ReqiureStyle">*</span></label>
                <input id="attributeDetailCodePop" type="text" class="InputTextStyle InputText2Style" maxlength="40" />
                <label class="LabelStyle Label2Style">Tên GT <span class="ReqiureStyle">*</span></label>
                <input id="attributeDetailNamePop" type="text" class="InputTextStyle InputText2Style" maxlength="250" />
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style">Trạng thái <span class="ReqiureStyle">*</span></label>
                <div  class="BoxSelect BoxSelect6">
                    <select id="statusDetailPop" class="MySelectBoxClass">
                        <option value="0">Tạm ngưng</option>
                        <option value="1" selected="selected">Hoạt động</option>
                    </select>
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSaveAttrDetail" onclick="return AttributeCustomerManager.saveAttributeDetail();" class="BtnGeneralStyle" onclick="return AttributeCustomerManager.saveAttributeDetail();">Cập nhật</button>
                </div>
                 <input type="hidden" id="idDetail" value="0" />
                 <input type="hidden" id="attributeId" />
            </div>
	            <div class="Clear"></div>
		        <p id="errMsgDetailPop" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
		        <p id="successMsg" style="display: none;" class="SuccessMsgStyle"></p>
		</div>
    </div>
<s:hidden id="idShowObject" name="showObjectDMSCORE"></s:hidden>
<script type="text/javascript">
	//$('.MySelectBoxClass').customStyle();
	$("#attributeCode").focus();
	Utils.bindAutoButtonEx('.ContentSection','btnSearch');
	$(document).ready(function(){
		var isShowObjectColumn = $("#idShowObject").val();
		$('#valueType').removeAttr('disabled');
		var html = '';
		html += '<option value="-1" >--Tất cả--</option>'; 
		html += '<option value="1">Chữ</option>';
		html += '<option value="2">Số</option>';
		html += '<option value="3">Ngày tháng</option>';
		html += '<option value="4">Danh sách(chọn một)</option>';
		html += '<option value="5">Danh sách(chọn nhiều)</option>';
		$('#valueType').html(html);
		setSelectBoxValue('valueType',-1);
		$('#valueType').dropdownchecklist({
			emptyText:'--Tất cả--',
	 		firstItemChecksAll: true,
			maxDropHeight: 350
		});
		setSelectBoxValue('object',-1);
		$('#object').dropdownchecklist({
			emptyText:'--Tất cả--',
	 		firstItemChecksAll: true,
			maxDropHeight: 350
		});
		lstAttributeCM = new Array();
		lstAttributeCM.push(-1);
		var params = new Object();
		params.attributeCode = '';
		params.attributeName = '',
		params.lstAttributeCM = lstAttributeCM;
		params.status = $('#status').val().trim();
		var columnAttributeDatagrids = [[	        
            			    {field:'attributeCode', title: 'Mã thuộc tính', width: 150, sortable:false,resizable:false , align: 'left',
           			    	formatter:function(cellvalue, rowObject, options){
           			    		return Utils.XSSEncode(rowObject.attributeCode);
           			    	}
           			    },
           			    {field:'attributeName', title: 'Tên thuộc tính', width:250,sortable:false,resizable:false , align: 'left',
           			    	formatter:function(cellvalue, rowObject, options){
           			    		return Utils.XSSEncode(rowObject.attributeName);
           			    	}
           			    },
           			    {field:'valueType', title: 'Loại giá trị', width: 130, align: 'left', sortable:false,resizable:false,formatter: AttributeCustemerCatalogFormatter.valueTypeFormat},
           			    {field:'status', title: 'Trạng thái', width: 80, align: 'left',sortable:false,resizable:false,formatter: AttributeCustemerCatalogFormatter.statusFormat},
           			    {field:'attributeDetailShow', title:'',
           			    	width: 20, align: 'center',sortable:false,resizable:false, formatter: AttributeCustemerCatalogFormatter.viewCellDetailIconFormatter},
           			    {field:'edit', title:'<a id="btnGridAdd" href="javascript:void(0)" onclick= "AttributeCustomerManager.openAttributeCustomer();"><img src="/resources/images/icon_add.png"/></a>',
           			    	width: 20, align: 'center',sortable:false,resizable:false, formatter: AttributeCustemerCatalogFormatter.editCellIconFormatter},
           		    ]];
		var columnObject = {field:'tableName', title: 'Đối tượng', width: 130, align: 'left', sortable:false,resizable:false,formatter: AttributeCustemerCatalogFormatter.objectFormat};
		if (isShowObjectColumn == 'true'){
			columnAttributeDatagrids[0].splice(3, 0, columnObject);
			console.log(columnAttributeDatagrids);
		}
		$('#attributeDatagrid').datagrid({
			url: '/attribute-customer-manager/search',
			autoRowHeight : true,
			rownumbers : true, 
			checkOnSelect :true,
			singleSelect : true,
			pagination:true,
			rowNum : 10,
			fitColumns:true,
			pageList  : [10,20,30],
			scrollbarSize:0,
			width: $('#gridContainer').width(),
			autoWidth: true,
			queryParams:params,
		    columns:columnAttributeDatagrids,	    
		    onLoadSuccess :function(data){
		    	$('.datagrid-header-rownumber').html('STT');	    	
		    	 updateRownumWidthForJqGrid('.easyui-dialog');
		    	 CommonFormatter.checkPermissionHideGridButton("btnGridAdd", false, "gridContainer");
		    	 CommonFormatter.checkPermissionHideGridButton("btnGridEdit", true, "gridContainer");
// 	   		 	$(window).resize();
		    }
		});
	});
	$('#attributeDetailDataDiv').hide();
</script>




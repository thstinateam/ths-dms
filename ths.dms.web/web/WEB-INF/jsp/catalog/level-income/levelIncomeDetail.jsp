<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="Breadcrumbs">
   <span class="Breadcrumbs1Item Sprite1">Mục Tiêu DS theo nghành</span><a href="/catalog/sales-brand" class="Sprite1 Breadcrumbs2Item">Mục tiêu DS theo nghành</a>
</div>
<div class="GeneralMilkBox">
   	<div class="GeneralMilkTopBox">
       	<div class="GeneralMilkBtmBox">
               <h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin thêm mới</span></h3>
           <div class="GeneralMilkInBox ResearchSection">
           		<div class="ModuleList21Form">
	              <label class="LabelStyle Label1Style">Mã loại NH</label>
	              <div class="BoxSelect BoxSelect1">
              		<div class="Field2">
	                  	<s:select id="categoryType" list="lstCategory" listKey="id" listValue="productInfoCode"  headerKey="-1" headerValue="---Chọn mã loại NH---"
							cssClass="MySelectBoxClass"  onchange="return LevelIncomeCatalog.getCategoryName($(this).val(),'#categoryName');" value="%{categoryType}">
		           		</s:select>
		           		<span class="RequireStyle">(*)</span>
                     </div>
	              </div>
	              <label class="LabelStyle Label2Style">Tên loại NH</label>
	              <input id="categoryName" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="levelIncome.cat.productInfoName"/>"/>
	              <label class="LabelStyle Label3Style">Mức</label>
	              <div class="Field2">
		              <input id="levelName" type="text" class="InputTextStyle InputText1Style" value="<s:property value="levelIncome.saleLevel"/>" maxlength="20"/>
		              <span class="RequireStyle">(*)</span>
                  </div>
                  <div class="Clear"></div>
	              <label class="LabelStyle Label1Style">Trạng thái</label>
	              <div class="BoxSelect BoxSelect1">
	              <div class="Field2">
	                  <select id="status"  class="MySelectBoxClass">
	                      <option value="-2">---Chọn  trạng thái---</option>
		                      <option value="0">Tạm ngưng</option>
		                      <option value="1" selected="selected">Hoạt động</option>
	                  </select>
	                  <span class="RequireStyle">(*)</span>
                     </div>
	              </div>	              
	              <label class="LabelStyle Label2Style">DS từ</label>
	              <div class="Field2">
	              <input id="fromIncome" maxlength="26" type="text" class="InputTextStyle InputText1Style" value="<s:property value="levelIncome.fromAmount"/>"/>
	              <span class="RequireStyle">(*)</span>
                     </div>
	              <label class="LabelStyle Label3Style">Đến DS</label>
	              <input id="toIncome" type="text" maxlength="26" class="InputTextStyle InputText1Style" value="<s:property value="levelIncome.toAmount"/>"/>
	              <div class="Clear"></div>
	              <div class="ButtonSection">
		              	<button id="btnCreate" class="BtnGeneralStyle Sprite2" onclick="return LevelIncomeCatalog.change();"><span class="Sprite2">Lưu lại</span></button>
		              	<a id="btnDismiss" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" href="/catalog/sales-brand" ><span class="Sprite2">Bỏ qua</span></a>
		              	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
	              </div>
	              <p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
	              <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
            	</div>
            </div>
		</div>
    </div>
</div>
<s:hidden id="levelIncomeId" name="levelIncomeId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	if($('#levelIncomeId').val().trim() != null && $('#levelIncomeId').val().trim() > 0){
		$('#title').html('Thông tin cập nhật');
		$('#levelName').attr('disabled','disabled');
		$('#categoryType').val('<s:property value="categoryType" />');
		$('#categoryType').attr('disabled','disabled');
		$('#categoryType').change();		
		$('#status').val('<s:property value="status" />');
		$('#status').change();
	}
	Utils.bindFormatOnTextfield('fromIncome',Utils._TF_NUMBER);
	Utils.formatCurrencyFor('fromIncome');
	Utils.bindFormatOnTextfield('toIncome',Utils._TF_NUMBER);
	Utils.formatCurrencyFor('toIncome');
	$('#fromIncome').keyup();
	$('#toIncome').keyup();
	
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/catalog/level-income/info"><s:text name="catalog_sales_brand_catalogy"/></a>
		</li>
		<li><span id="title1"><s:text name="catalog_sales_brand_seasonal_by_industry_title"/></span>
		</li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style"><s:text name="catalog_sales_brand_search_information"/></h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="catalog_sales_brand_NH_Code"/></label> 
					<div class="BoxSelect BoxSelect2" >
						<select id="categoryType" multiple="multiple">
							<option value="0"><s:text name="catalog_sales_brand_all"/></option>
							<s:iterator value="lstCategory">
								<option value="<s:property value="productInfoCode"/>"><s:property value="productInfoCode" /> - <s:property value="productInfoName" /></option>
							</s:iterator>
						</select>
					</div>
					<label class="LabelStyle Label1Style"><s:text name="catalog_sales_brand_standard_code"/></label>
					<input id="saleLevelCode" type="text" class="InputTextStyle InputText1Style" maxlength="50" /> 
					<label class="LabelStyle Label1Style"><s:text name="catalog_sales_brand_standard_name"/></label>
					<input id="saleLevelName" type="text" class="InputTextStyle InputText1Style" maxlength="250"/>
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style"><s:text name="catalog_sales_brand_from_list"/></label>
					<input id="fromIncome" maxlength="22" type="text" class="InputTextStyle InputText1Style" style="width:196px" /> 
					<label class="LabelStyle Label1Style"><s:text name="catalog_sales_brand_to_list"/></label>
					<input id="toIncome" maxlength="22" type="text" class="InputTextStyle InputText1Style" />
					<label class="LabelStyle Label1Style"><s:text name="catalog_sales_brand_status"/></label>
					<div class="BoxSelect BoxSelect2">
						<select id="status" class="MySelectBoxClass">
							<option value="-2" selected="selected" >---<s:text name="jsp.common.status.all"/>---</option>
							<option value="0"><s:text name="jsp.common.status.stoped"/></option>
							<option value="1" ><s:text name="jsp.common.status.active"/></option>
						</select>
					</div>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle Sprite2 cmsiscontrol" onclick="LevelIncomeCatalog.search();">
							<span class="Sprite2"><s:text name="jsp.common.timkiem"/></span>
						</button>
					 	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
					</div>
					<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>

		<div class=GeneralCntSection>
			<h2 class="Title2Style"><s:text name="catalog_sales_brand_seasonal_by_industry"/></h2>
				<div class="GridSection">
					<div class="ResultSection" id="levelIncomeGrid">
						<p id="gridNoResult" style="display: none"
							class="WarningResultStyle"><s:text name="jsp.common.no.result"/></p>
						<table id="grid"></table>
						<div id="pager"></div>
					</div>
					<div class="Clear"></div>
				</div>
		</div>
	</div>
</div>

<div id="viewExcelDIV" style="display: none">
		<div class="GeneralDialog General2Dialog">
        	<div class="DialogProductSearch">
            	<div class="GeneralTable Table44Section">
            	<div class="ScrollSection">
            				<div class="BoxGeneralTTitle">
	                              <table border="0" cellspacing="0" cellpadding="0">
	                                  <colgroup>
	                                     	<col style="width:125px;" />
								           <col style="width:125px;" />
								           <col style="width:125px;"/>
								           <col style="width:125px;" />
								           <col style="width:490px;" />
	                                  </colgroup>
	                                  <thead>
	                                      <tr>
	                                          <th class="ColsThFirst"><s:text name="catalog_sales_brand_line_code"/></th>
	                                          <th class="ColsThSecond"><s:text name="catalog_sales_brand_line_code"/></th>
	                                          <th class="ColsThSecond"><s:text name="catalog_sales_brand_from_seasonal"/></th>
	                                          <th class="ColsThSecond"><s:text name="catalog_sales_brand_to_seasonal"/></th>
	                                          <th class="ColsThEnd"><s:text name="catalog_sales_brand_error"/></th>
	                                      </tr>
	                                  </thead>
	                              </table>
	                          </div>
	                          <div class="BoxGeneralTBody">
	                          	<div id="fancy_gird" class="ScrollBodySection"> 
								</div>
	                          </div>
	                  	</div>
	                  	<div class="BoxDialogBtm">
                    		<div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2"><s:text name="jsp.common.dong"/></span></button></div>
               			 </div>
	              </div>
		</div>
	</div>
</div>
<div id="divDialog" style="visibility: hidden;">
<div id="popup1" class="easyui-dialog" title=<s:text name="catalog_sales_brand_update_seasonal_by_industry"/> data-options="closed:true,modal:true" style="width:960px;height:200px;">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form" id="fm">
        		<label class="LabelStyle Label1Style"><s:text name="catalog_sales_brand_NH_Code"/><span class="ReqiureStyle">(*)</span></label>
	              <div class="BoxSelect BoxSelect3">
					<select id="categoryType1" class="MySelectBoxClass">
						<option value="-1">--- <s:text name="catalog_sales_brand_chose_line"/> ---</option>
						<s:iterator value="lstCategory">
							<option value="<s:property value="id"/>">
								<s:property value="productInfoCode" />-<s:property value="productInfoName" />
							</option>
						</s:iterator>
					</select> 
	              </div>
	              <label class="LabelStyle LabelPLStyle"></label>
                <label class="LabelStyle Label1Style"><s:text name="catalog_sales_brand_standard_code"/><span class="ReqiureStyle"> (*) </span> </label>
                  <input id="saleLevelCode1" type="text" class="InputTextStyle InputText4Style" maxlength="50"/>
                  <label class="LabelStyle LabelPLStyle"></label>
                  <label class="LabelStyle Label1Style"><s:text name="catalog_sales_brand_standard_name"/><span class="ReqiureStyle"> (*) </span></label>
                  <input id="saleLevelName1" type="text" class="InputTextStyle InputText4Style" maxlength="100"/>
                  <label class="LabelStyle LabelPLStyle"></label>	
	              <div class="Clear"></div>
	              <label class="LabelStyle Label1Style"><s:text name="catalog_sales_brand_from_list"/><span class="ReqiureStyle"> (*) </span> </label> 
				<input id="fromIncome1" maxlength="20" type="text" class="InputTextStyle InputText4Style" />
				<label class="LabelStyle LabelPLStyle"><%-- <span class="ReqiureStyle">(*)</span> --%></label> 
				<label class="LabelStyle Label1Style"><s:text name="catalog_sales_brand_to_list"/> </label> 
				<input id="toIncome1" maxlength="20" type="text" class="InputTextStyle InputText4Style" />
				<label class="LabelStyle LabelPLStyle"><span class="ReqiureStyle">&nbsp;</span></label> 
	              <label class="LabelStyle Label1Style"><s:text name="report.status.label"/> <span class="ReqiureStyle"> (*) </span></label>
	              <div class="BoxSelect BoxSelect3">
	                  <select id="status1" class="MySelectBoxClass">
		                      <option value="0"><s:text name="jsp.common.status.stoped"/></option>
		                      <option value="1" selected="selected"><s:text name=""/><s:text name="jsp.common.status.active"/></option>
	                  </select>
	              </div>
	              <label class="LabelStyle LabelPLStyle"><%-- <span class="ReqiureStyle">(*)</span> --%></label>
	              <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnEdit" onclick="return LevelIncomeCatalog.change();" class="BtnGeneralStyle"><s:text name="acton.type.status.update"/> </button>
                    <img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                </div>
                <p class="ErrorMsgStyle" id="errMsg" style="display: none;"><s:text name="jsp.common.err.data.update"/> </p>
	            <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
            </div>
		</div>
 </div>
 </div>
<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
<div id="responseDiv" style="display: none"></div>
<s:hidden id="levelIncomeId" name="levelIncomeId"></s:hidden>
<s:hidden id="categoryTypeId" name="categoryTypeId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#saleLevelCode').focus();
	Utils.bindQuicklyAutoSearch();
	LevelIncomeCatalog._listCategory = new Array();
	LevelIncomeCatalog._listCategory.push(0);
	$('#categoryType').dropdownchecklist({width:227,emptyText:catalog_sales_brand_all,maxDropHeight: 300,firstItemChecksAll: true,onItemClick:function(options){
	var i=0;
	LevelIncomeCatalog._listCategory = new Array();
	var check = $('#ddcl-categoryType-ddw input[type=checkbox]').attr('checked');
	if(check == 'checked'){
		LevelIncomeCatalog._listCategory.push(0);
	}else{
		$('#ddcl-categoryType-ddw input[type=checkbox]').each(function(){
			
			var s = $(this).attr('checked');
		    if(s == 'checked'){
		    	LevelIncomeCatalog._listCategory.push($('#ddcl-categoryType-ddw #ddcl-categoryType-i'+i).val());
		    }
		    i++;
		});
	}
	}});
	$(".BoxSelect .ui-dropdownchecklist-text, .BoxSelect .ui-dropdownchecklist-selector").css({"width":"172px"});
	$("#grid").datagrid({
	  url:LevelIncomeCatalog.getGridUrl('0','','','','',1),
	  columns:[[		
	    {field:'cat.productInfoCode', title: catalog_sales_brand_NH_Code, width: 60, sortable:true,resizable:true , align: 'left',formatter: LevelIncomeCatalogFormatter.productInfoCodeFormat},
	    {field:'saleLevelCode', title: catalog_sales_brand_standard_code, width: 100, sortable:true,resizable:true, align: 'left', 
	    	formatter: function(cellvalue, row, index){
	    		if(row.saleLevelCode!=undefined && row.saleLevelCode.length>0){
	    			return Utils.XSSEncode(row.saleLevelCode);
	    		}return '';
	    	}
	    },
	    {field:'saleLevelName', title: catalog_sales_brand_standard_name, width: 170, sortable:true,resizable:true , align: 'left',
	    	formatter: function(cellvalue, row, index){
	    		if(row.saleLevelName!=undefined && row.saleLevelName.length>0){
	    			return Utils.XSSEncode(row.saleLevelName);
	    		}return '';
	    	}
	    },
	    {field:'fromAmountString', title: catalog_sales_brand_from_amount,width: 100, sortable:true,resizable:true, align: 'right',formatter: LevelIncomeCatalogFormatter.fromAmountFormat },
	    {field:'toAmountString', title: catalog_sales_brand_to_amount, width: 100, align: 'right', sortable:true,resizable:true ,formatter: LevelIncomeCatalogFormatter.toAmountFormat},
	    {field:'status', title: jsp_common_status, width: 80, align: 'left',sortable:true,resizable:true,formatter: LevelIncomeCatalogFormatter.statusFormat},
	    {field:'edit', title: "<a title= " + catalog_customer_type_add + " href='javascript:void(0);' id='btnGridAdd' onclick='LevelIncomeCatalog.addLevelIncome();'><img src='/resources/images/icon_add.png'/></a>", width: 50, align: 'center',sortable:false,resizable:true,
	    formatter: LevelIncomeCatalogFormatter.editCellIconFormatter},
	  ]],	  
	  pageList  : [10,20,30],
	  height:'auto',
	  scrollbarSize : 0,
	  pagination:true,
	  singleSelect  :true,
	  fitColumns:true,
	  method : 'GET',
	  rownumbers: true,	  
	  width: ($('#levelIncomeGrid').width() - 20),
	  onLoadSuccess:function(){
			$('.datagrid-header-rownumber').html(unit_tree_search_unit_search_grid_no); //.html('STT');
			var i = CommonFormatter.checkPermissionHideGridButton("btnGridAdd", false, "levelIncomeGrid");
			var i1 = CommonFormatter.checkPermissionHideGridButton("btnGridEdit", true, "levelIncomeGrid");
			if (i + i1 == 2) {
			 	$("#grid").datagrid("hideColumn", "edit");
			}
			$('#grid').datagrid('resize');
		 	//Phan quyen control
		   	var arrEdit =  $('#levelIncomeGrid td[field="edit"]');
			if (arrEdit != undefined && arrEdit != null && arrEdit.length > 0) {
			  for (var i = 0, size = arrEdit.length; i < size; i++) {
			  	$(arrEdit[i]).prop("id", "gr_table_td_edit_" + i);//Khai bao id danh cho phan quyen
				$(arrEdit[i]).addClass("cmsiscontrol");
			  }
			}
			Utils.functionAccessFillControl('levelIncomeGrid', function(data){
				//Xu ly cac su kien lien quan den cac control phan quyen
				var arrTmpLength =  $('#levelIncomeGrid td[id^="gr_table_td_edit_"]').length;
				var invisibleLenght = $('.isCMSInvisible[id^="gr_table_td_edit_"]').length;
				if (arrTmpLength > 0 && arrTmpLength != invisibleLenght) {
					$('#grid').datagrid("showColumn", "edit");
				} else {
					$('#grid').datagrid("hideColumn", "edit");
				}
			});
      }  
	});
});
Utils.bindFormatOnTextfield('fromIncome',Utils._TF_NUMBER);
Utils.formatCurrencyFor('fromIncome');
Utils.bindFormatOnTextfield('toIncome',Utils._TF_NUMBER);
Utils.formatCurrencyFor('toIncome');
Utils.bindFormatOnTextfield('fromIncome1',Utils._TF_NUMBER);
Utils.formatCurrencyFor('fromIncome1');
Utils.bindFormatOnTextfield('toIncome1',Utils._TF_NUMBER);
Utils.formatCurrencyFor('toIncome1');

// 	$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_muc_doanh_so_theo_nganh.xls');
// 	var options = { 
// 			beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
// 	 		success:      ProductLevelCatalog.afterImportExcel,
// 	 		type: "POST",
// 	 		dataType: 'html'   
// 	 	}; 
// 	$('#importFrm').ajaxForm(options);
//});

</script>
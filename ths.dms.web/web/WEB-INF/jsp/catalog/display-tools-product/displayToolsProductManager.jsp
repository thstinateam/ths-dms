<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="Breadcrumbs">
    <span class="Breadcrumbs1Item Sprite1">Quản lý tủ - sản phẩm</span>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
		<div class="GeneralMilkBtmBox">
			<h3 class="Sprite2">
				<span class="Sprite2">Thông tin tìm kiếm</span>
			</h3>
			<div class="GeneralMilkInBox ResearchSection">
				<div class="Web3Form">
					<div class="FormLeft">
						<label class="LabelStyle Label1Style">Mã tủ(F9)</label> 
						<div class="Field2"><input id="toolCode" type="text" class="InputTextStyle InputText1Style" /><span id="toolCodeRequire" style="display: none;" class="RequireStyle">(*)</span></div> 
						<label class="LabelStyle Label2Style">Mã SP(F9)</label> 
						<div class="Field2"><input id="productCode" type="text" class="InputTextStyle InputTextStyle" /><span id="productCodeRequire" style="display: none;" class="RequireStyle">(*)</span></div>
					</div>
					<div class="ButtonLSection">
						<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return DisplayToolProductCatalog.search();">
							<span class="Sprite2">Tìm kiếm</span>
						</button>
						<button id="btnSave" class="BtnGeneralStyle Sprite2" style="display: none;" onclick="return DisplayToolProductCatalog.add();">
							<span class="Sprite2">Lưu lại</span>
						</button>
						<button id="btnCancel" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" style="display: none;" onclick="return DisplayToolProductCatalog.changeFormCancel();">
							<span class="Sprite2">Bỏ qua</span>
						</button>
						<button id="btnAddNew" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return DisplayToolProductCatalog.changeFormAddNew();">
							<span class="Sprite2">Thêm mới</span>
						</button>
						<button id="excelExport" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return DisplayToolProductCatalog.export();">
							<span class="Sprite2">Xuất excel</span>
						</button>
					</div>
			       	<div class="Clear"></div>
			       	<p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
			       	<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
				</div>
			</div>
		</div>
	</div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Danh sách tủ với sản phẩm</span></h3>
          	<div class="GeneralMilkInBox">
              	<div class="ResultSection" id="toolProductGrid">
                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
                  	<table id="grid"></table>
					<div id="pager"></div>
              	</div>
              	<div class="Clear"></div>
          	</div>
          	<p class="ErrorMsgStyle Sprite1" id="errDelMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
		</div>
    </div>
</div>
<div class="FuncBtmSection">
<form action="/catalog/display-tool-product/importexcelfile" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
	<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>	
	<div class="DivInputFileSection">
			<p class="ExcelLink">
				<a id="downloadTemplate" href="javascript:void(0)">Tải mẫu file excel</a>
			</p>		
				<div class="DivInputFile">
		            <input type="file" class="InputFileStyle" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
		            <div class="FakeInputFile">
		                <input id="fakefilepc" type="text" class="InputTextStyle">
		            </div>
		        </div>
		        <button class="BtnGeneralStyle Sprite2"><span class="Sprite2">Nhập từ file</span></button>	  
		    <div class="Clear"></div>
	</div>
</form>
</div>
<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
<div id="responseDiv" style="display: none"></div>
<script type="text/javascript">
$(document).ready(function(){
	$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_quan_ly_tu_san_pham.xls');
	$('#toolCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			CommonSearch.searchToolOnDialog(function(data) {
				$('#toolCode').val(data.code);
			});
		}
	});
	$('#productCode').bind('keyup', function(event) {
		if(event.keyCode == keyCode_F9) {
			CommonSearch.searchProductOnDialog(function(data) {
				$('#productCode').val(data.code);
			});
		}
	});
	var options = { 
	 		beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcel,  
	 		type: "POST",
	 		dataType: 'html'   
	 	}; 
	$('#importFrm').ajaxForm(options);
	$('#grid').jqGrid({
		url:DisplayToolProductCatalog.getGridUrl('', ''),
		colModel:[
			{name: 'product', label: 'Mã tủ', width: 80, sortable:false,resizable:true , align: 'left', formatter: DisplayToolProductFormatter.getToolCodeFormatter},
			{name: 'product', label: 'Tên tủ', sortable: false, resizable:true , align: 'left', formatter: DisplayToolProductFormatter.getToolNameFormatter},
			{name: 'product', label: 'Mã SP', sortable:false,resizable:true , align: 'left', formatter: DisplayToolProductFormatter.getProductCodeFormatter},
			{name: 'product', label: 'Tên SP', width: 150, sortable:false,resizable:true , align: 'left', formatter: DisplayToolProductFormatter.getProductNameFormatter},
			{name: 'product', label: 'Xóa', width: 40, sortable:false,resizable:true, formatter: DisplayToolProductFormatter.deleteIconFormatter}
		],
		pager : '#pager',
		height: 'auto',	  	  
		width: ($('#toolProductGrid').width())
	}).navGrid('#pager', {edit:false,add:false,del:false, search: false});
});
</script>
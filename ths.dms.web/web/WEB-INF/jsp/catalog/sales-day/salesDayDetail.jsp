<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="Breadcrumbs">
   <span class="Breadcrumbs1Item Sprite1">Ngày làm việc NV</span><a href="/catalog/working-date-in-year" class="Sprite1 Breadcrumbs2Item">Danh sách số ngày làm việc trong năm</a>
</div>
<div class="GeneralMilkBox">
   	<div class="GeneralMilkTopBox">
       	<div class="GeneralMilkBtmBox">
               <h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin ngày làm việc trong năm</span></h3>
           <div class="GeneralMilkInBox ResearchSection">
           	<div class="ModuleList19Form">
                   <label class="LabelStyle Label1Style">Năm</label>
                   <div class="Field2">
	                   <input id="year" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.year"/>" maxlength="4"/>
	                   <span class="RequireStyle">(*)</span>
	               </div>
                   <label class="LabelStyle Label2Style">Trạng thái</label>
                   <div class="BoxSelect BoxSelect1">
                       	<select id="status" class="MySelectBoxClass">
	                  		<option value="-2">---Chọn trạng thái---</option>	
                      		<option value="0">Tạm ngưng</option>
                      		<option value="1" selected="selected">Hoạt động</option>
	                  	</select>
                   </div>
                   <div class="Clear"></div>
                   <label class="LabelStyle Label1Style">Tháng 1</label>
                   <input id="january" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t1"/>" maxlength="2"/>
                   <label class="LabelStyle Label2Style">Tháng 2</label>
                   <input id="february" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t2"/>" maxlength="2"/>
                   <label class="LabelStyle Label3Style">Tháng 3</label>
                   <input id="march" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t3"/>" maxlength="2"/>
                   <div class="Clear"></div>
                   <label class="LabelStyle Label1Style">Tháng 4</label>
                   <input id="april" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t4"/>" maxlength="2"/>
                   <label class="LabelStyle Label2Style">Tháng 5</label>
                   <input id="may" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t5"/>" maxlength="2"/>
                   <label class="LabelStyle Label3Style">Tháng 6</label>
                   <input id="june" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t6"/>" maxlength="2"/>
                   <div class="Clear"></div>
                   <label class="LabelStyle Label1Style">Tháng 7</label>
                   <input id="july" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t7"/>" maxlength="2"/>
                   <label class="LabelStyle Label2Style">Tháng 8</label>
                   <input id="august" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t8"/>" maxlength="2"/>
                   <label class="LabelStyle Label3Style">Tháng 9</label>
                   <input id="september" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t9"/>" maxlength="2"/>
                   <div class="Clear"></div>
                   <label class="LabelStyle Label1Style">Tháng 10</label>
                   <input id="october" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t10"/>" maxlength="2"/>
                   <label class="LabelStyle Label2Style">Tháng 11</label>
                   <input id="november" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t11"/>" maxlength="2"/>
                   <label class="LabelStyle Label3Style">Tháng 12</label>
                   <input id="december" type="text" class="InputTextStyle InputText1Style"  onkeypress="return numbersonly(event, false);" value="<s:property value="saleDays.t12"/>" maxlength="2"/>
                   <div class="Clear"></div>
                   <div class="ButtonSection">
	                   <button id="btnCreate" class="BtnGeneralStyle Sprite2" onclick="return SalesDayCatalog.change();"><span class="Sprite2">Lưu lại</span></button>
		              	<a id="btnDismiss" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" href="/catalog/working-date-in-year" ><span class="Sprite2">Bỏ qua</span></a>
		              	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                   </div>
                   <p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
                   <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                </div>
            </div>
		</div>
    </div>
</div>
<s:hidden id="salesDayId" name="salesDayId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	var salesDayId = '<s:property value="salesDayId"/>';
	if(salesDayId != '' && salesDayId > 0){
		$('#year').attr('disabled','disabled');
	}
	
});
</script>
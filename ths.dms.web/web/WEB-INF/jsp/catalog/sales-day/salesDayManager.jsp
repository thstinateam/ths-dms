<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="Breadcrumbs">
  <span class="Breadcrumbs1Item Sprite1">Số ngày làm việc trong năm</span>
</div>
<div class="GeneralMilkBox">
   	<div class="GeneralMilkTopBox">
       	<div class="GeneralMilkBtmBox">
          <h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
          <div class="GeneralMilkInBox ResearchSection">
          <div class="ModuleList19Form">
              <label class="LabelStyle Label1Style">Từ năm</label>
              <input id="fromYear" type="text" class="InputTextStyle InputText1Style" maxlength="4" onkeypress="return numbersonly(event,false);"/>
              <label class="LabelStyle Label2Style">Đến năm</label>
              <input id="toYear" type="text" class="InputTextStyle InputText1Style" maxlength="4" onkeypress="return numbersonly(event,false);"/>
              <label class="LabelStyle Label3Style">Trạng thái</label>
              <div class="BoxSelect BoxSelect1">
                  <select id="status" class="MySelectBoxClass">
                  	<option value="-2">---Chọn trạng thái---</option>	
                      <option value="0">Tạm ngưng</option>
                      <option value="1" selected="selected">Hoạt động</option>
                  </select>
              </div>
              <div class="Clear"></div>
              <div class="ButtonSection">
	              	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return SalesDayCatalog.search();"><span class="Sprite2">Tìm kiếm</span></button>
	              	<a id="btnCreate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" href="/catalog/sales-day/viewdetail?salesDayId=0" ><span class="Sprite2">Thêm mới</span></a>
	              	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
              </div>
              <p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
              <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
            </div>
            </div>
		</div>
    </div>
</div>
<div class="GeneralMilkBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
            <h3 class="Sprite2"><span class="Sprite2">Danh sách ngày làm việc trong năm</span></h3>
          	<div class="GeneralMilkInBox">
              	<div class="ResultSection" id="salesDayGrid">
                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
                  	<table id="grid"></table>
					<div id="pager"></div>
              	</div>
              	<div class="Clear"></div>
          	</div>
		</div>
    </div>
</div>
<div class="FuncBtmSection">
<form action="/catalog/sales-day/importexcelfile" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
	<div class="DivInputFileSection">
			<p class="ExcelLink">
				<a id="downloadTemplate" href="javascript:void(0)">Tải mẫu file excel</a>
			</p>		
				<div class="DivInputFile">
		            <input type="file" class="InputFileStyle" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
		            <div class="FakeInputFile">
		                <input id="fakefilepc" readonly="readonly" type="text" class="InputTextStyle">
		            </div>
		        </div>
		        <button class="BtnGeneralStyle Sprite2"><span class="Sprite2">Nhập từ file</span></button>	  
		    <div class="Clear"></div>
	</div>
 </form>
</div>
<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
<div id="responseDiv" style="display: none"></div>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	$('#fromYear').focus();
	$("#grid").jqGrid({
	  url:SalesDayCatalog.getGridUrl('','',1),
	  colModel:[		
	    {name:'year', label: 'Năm', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t1', label: 'T1', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t2', label: 'T2', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t3', label: 'T3', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t4', label: 'T4', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t5', label: 'T5', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t6', label: 'T6', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t7', label: 'T7', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t8', label: 'T8', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t9', label: 'T9', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t10', label: 'T10', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t11', label: 'T11', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'t12', label: 'T12', width: 60, sortable:false,resizable:false , align: 'right'},
	    {name:'status', label: 'Trạng thái', width: 80, align: 'center',sortable:false,resizable:false,formatter: SalesDayCatalogFormatter.statusFormat},
	    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: SalesDayCatalogFormatter.editCellIconFormatter},
	    {name:'delete', label: 'Xóa', width: 50, align: 'center',sortable:false,resizable:false, formatter: SalesDayCatalogFormatter.delCellIconFormatter},
	  ],	  
	  pager : '#pager',
	  height: 'auto',
	  rownumbers: true,	  
	  width: ($('#salesDayGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});
	$('#jqgh_grid_rn').prepend('STT');
	$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_so_ngay_lam_viec_trong_nam.xls');
	var options = { 
			beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcel, 
	 		type: "POST",
	 		dataType: 'html'   
	 	}; 
	$('#importFrm').ajaxForm(options);
});
</script>
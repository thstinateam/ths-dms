<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="errorExcel" style="display: none"><s:property value="isError"/></div>
<div id="errorExcelMsg" style="display: none"><s:property value="errMsg"/></div>
<div id="totalRow" style="display: none"><s:property value="totalItem"/></div>
<div id="numFail" style="display: none"><s:property value="numFail"/></div>
<div id="fileNameFail" style="display: none"><s:property value="fileNameFail"/></div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div class="Breadcrumbs">
    <span class="Breadcrumbs1Item Sprite1">Nhóm giao hàng</span>
</div>
<div class="MainInContent">
	<div class="MainInTopContent">
    	<div class="MainInBtmContent">
    		<div class="FirstSidebar">
              	<div class="TreeViewSection">
                      <h3 class="Sprite2"><span class="Sprite2">Cây đơn vị</span></h3>
                      <div class="GeneralMilkInBox">
                      	<div id="treeContainer" class="ScrollSection">   
                      		<div id="tree" class="jqTree"></div>          
                      	</div>                           
                      </div>
                  </div>
            </div>
            <div class="Content">
				<div class="GeneralMilkBox">
                	<div class="GeneralMilkTopBox">
                    	<div class="GeneralMilkBtmBox">
                    		<h3 class="Sprite2"><span class="Sprite2">Thông tin tìm kiếm</span></h3>
                             <div class="GeneralMilkInBox ResearchSection">
                             <div class="ModuleList6Form">                                 
                                 <label class="LabelStyle Label1Style">Mã nhóm</label>                                 
                                 <input id="code" type="text" class="InputTextStyle InputText1Style" maxlength="20"/>                                 	
                                 <label class="LabelStyle Label2Style">Tên nhóm</label>                                 
                                 <input id="name" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
                                 <div class="Clear"></div>
                                 <label class="LabelStyle Label1Style">Mã NVGH (F9)</label>                                 
                                 <input id="staffCode" type="text" class="InputTextStyle InputText1Style" maxlength="10"/>                                                                  
                                 <label class="LabelStyle Label2Style">Mã KH (F9)</label>                                 
                                 <input id="customerCode" type="text" class="InputTextStyle InputText1Style" maxlength="10"/>
                                 <div class="Clear"></div>
                                 <label class="LabelStyle Label1Style">Mã đơn vị (F9)</label>                                 
                                 <input id="shopCode" type="text" class="InputTextStyle InputText1Style" maxlength="20"/>
                                 <label class="LabelStyle Label2Style">Trạng thái</label>					             
						         	<div class="BoxSelect BoxSelect1">
						            	<s:select id="status" headerKey="-2" headerValue="-- Chọn trạng thái --"  name="status" cssClass="MySelectBoxClass" list="lstStatusBean" listKey="value" listValue="name"></s:select>                    
						            </div>
                                 <div class="Clear"></div>
                                 <label class="LabelStyle Label3Style"><input type="checkbox" id="chkAllChild">Tất cả các cấp</label>
                                 <div class="Clear"></div>
                                 <div class="ButtonSection">
				                	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return DeliveryGroupCatalog.search()"><span class="Sprite2">Tìm kiếm</span></button>
				                	<button id="btnAdd" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return DeliveryGroupCatalog.openAddForm();"><span class="Sprite2">Thêm mới</span></button>				                	
				                	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
				                </div>
                             </div>
                             <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>
                        </div>
                        </div>
                   </div>
               </div>
               <div class="GeneralMilkBox">
					<div class="GeneralMilkTopBox">
				    	<div class="GeneralMilkBtmBox">
				            <h3 class="Sprite2"><span class="Sprite2">Danh sách nhóm giao hàng</span></h3>
				            <div class="GeneralMilkInBox">
				                <div class="ResultSection" id="deliveryGroupGrid">
				                    <p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
				                    <table id="grid"></table>
									<div id="pager"></div>
				                </div>
				            </div>
						</div>
				    </div>
				</div>
				<div class="FuncBtmSection">
						<div class="DivInputFileSection">
								<div class="BoxSelect BoxSelect1">
                                     <select class="MySelectBoxClass" name="excelType" id="excelType" onchange="excelTypeChanged();">
                                         <option value="1">Nhóm giao hàng</option>
                                         <option value="2">Danh sách khách hàng</option>
                                     </select>
                                 </div>
								<p class="ExcelLink">
									<a id="downloadTemplate" href="javascript:void(0)">Tải mẫu file excel</a>
								</p>		
								<div class="DivInputFile">
									<form action="/catalog/delivery-group/import-excel" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
										<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>										
							            <input type="file" class="InputFileStyle" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
							            <input type="hidden" id="isView" name="isView">
							            <div class="FakeInputFile">
							                <input id="fakefilepc" type="text" readonly="readonly" class="InputTextStyle">
							            </div>
					 				</form>
						        </div>
							    <button id="btnView" class="BtnGeneralStyle Sprite2" onclick="return DeliveryGroupCatalog.viewExcel();"><span class="Sprite2">Xem</span></button>
       							<button id="btnImport" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return DeliveryGroupCatalog.importExcel();"><span class="Sprite2">Nhập từ file</span></button> 
							    <div class="Clear"></div>
						</div>
				</div>
				<div class="FuncLeftBtmSection">
				   	<button class="BtnGeneralStyle Sprite2" onclick="DeliveryGroupCatalog.exportExcelData();"><span class="Sprite2">Xuất dữ liệu</span></button>
				</div>
				<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
			</div>
			<div class="Clear"></div>
    	</div>
    </div>
</div>
<div id="responseDiv" style="display: none"></div>
<s:hidden id="selId" value="0"></s:hidden>
<s:hidden id="selCode"></s:hidden>
<s:hidden id="selName"></s:hidden>
<s:hidden id="shopIdHidden" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#code').focus();
	$('#name').val('');
	$('#customerCode').val('');
	$('.MySelectBoxClass').customStyle();
	$('#layoutContent').removeClass('Content').addClass('MainContent');	
	$("#grid").jqGrid({
	  url:DeliveryGroupCatalog.getGridUrl('','','','',-2,$('#status').val(),'',true),
	  colModel:[		
	    {name:'shop.shopCode', label: 'Mã đơn vị', width: 80, sortable:false,resizable:false , align: 'left'},
	    {name:'groupTransferCode', label: 'Mã nhóm', width: 80, sortable:false,resizable:false , align: 'left'},
	    {name:'groupTransferName', label: 'Tên nhóm', sortable:false,resizable:false, align:'left' },
	    {name:'staff.staffCode', label: 'Mã NVGH', width: 80, sortable:false,resizable:false , align: 'left'},
	    {name:'status', label: 'Trạng thái', width: 80, align: 'left', sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter},
	    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: DeliveryGroupCatalogFormatter.editCellIconFormatter},
	    {name:'delete', label: 'Xóa', width: 50, align: 'center', sortable:false,resizable:false, formatter: DeliveryGroupCatalogFormatter.delCellIconFormatter}
	  ],	  
	  pager : '#pager',
	  height: 'auto',	  	  
	  width: ($('#deliveryGroupGrid').width())	  
	})
	.navGrid('#pager', {edit:false,add:false,del:false, search: false});
	DeliveryGroupCatalog.loadTreeEx();
	/* loadDataForTree('/rest/catalog_customer_mng/shop/tree.json'); */	
	/* $('#tree').bind("loaded.jstree", function(event, data){
		if($('#treeContainer').data("jsp")!= undefined){
			$('#treeContainer').data("jsp").destroy();
		}
		$('#treeContainer').jScrollPane();
	});
	$('#tree').bind("select_node.jstree", function (event, data) {
		var name = data.inst.get_text(data.rslt.obj);
		var id = data.rslt.obj.attr("id");
		var code = data.rslt.obj.attr("contentItemId");
		$('#selId').val(id);
		$('#selCode').val(code);
		$('#selName').val(name);	
		var allChild = true;		
		if(!$('#chkAllChild').is(':checked')){
			allChild = false;
		}
		var url = DeliveryGroupCatalog.getGridUrl('','','','',id,$('#status').val(),'',allChild);
		$("#grid").setGridParam({url:url,page:1}).trigger("reloadGrid");
    }); */
	$('#staffCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var arrParam = new Array();
			var param = new Object();
			param.name = 'filterStaff';
			param.value = 1;
			arrParam.push(param);
			CommonSearch.searchTransferStaffOnDialog(function(data){
				$('#staffCode').val(data.code);				
			},arrParam);
		}
	});
	$('#customerCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var lstParam = new Array();
			var obj = {};
			obj.name = 'shopCode';
			obj.value = $('#shopCode').val().trim();
			lstParam.push(obj);
			CommonSearch.searchCustomerOnDialog(function(data){
				$('#customerCode').val(data.code);
			},lstParam);
		}
	});
	$('#shopCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var lstParam = new Array();
			var obj = {};
			obj.name = 'filterShop';
			obj.value = 1;
			lstParam.push(obj);
			CommonSearch.searchShopOnDialog(function(data){
				$('#shopCode').val(data.code);
				$('#shopName').val(data.name);
			},lstParam);
		}
	});
	$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_nhom_giao_hang.xls');
	var options = { 
	 		beforeSubmit: ProductLevelCatalog.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcelUpdate,  
	 		type: "POST",
	 		dataType: 'html',
	 		data:({excelType:$('#excelType').val()})
	 	}; 
	$('#importFrm').ajaxForm(options);
});
function excelTypeChanged(){
	if($('#excelType').val() == 1){
		$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_nhom_giao_hang.xls');
	} else {
		$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_nhom_giao_hang_danh_sach_khach_hang.xls');
	}
	$('#excelFile').val('');
	$('#fakefilepc').val('');
}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="Breadcrumbs">
    <a href="/catalog/delivery-group" class="Breadcrumbs1Item Sprite1">Nhóm giao hàng</a><span class="Sprite1 Breadcrumbs2Item">Tạo mới</span>
</div>
<div class="GeneralMilkBox GeneralMilkExtBox">
	<div class="GeneralMilkTopBox">
    	<div class="GeneralMilkBtmBox">
        	<div class="TabSection">
                <ul class="ResetList TabList">
                    <li id="tabInfo" class="Tab1Item"><a id="tabInfoActive" class="Sprite2 Active" href="javascript:void(0);"><span class="Sprite2">Thông tin nhóm giao hàng</span></a></li>
                    <li id="tabCus" class="Tab2Item"><a id="tabCusActive" class="Sprite2 Disable" href="javascript:void(0);"><span class="Sprite2">Danh sách khách hàng</span></a></li>
                </ul>
               	<div class="Clear"></div>
            </div>
            <div class="GeneralMilkInBox ResearchSection">
            <div class="ModuleList61Form">
                <label class="LabelStyle Label1Style">Mã nhóm</label>
                <div class="Field2">                
                	<s:textfield id="code" name="code" cssClass="InputTextStyle InputText1Style" maxlength="20"></s:textfield>
                	<span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label2Style">Tên nhóm</label>
                <div class="Field2">                
                	<s:textfield id="name" name="name" cssClass="InputTextStyle InputText1Style" maxlength="50"></s:textfield>
                	<span class="RequireStyle">(*)</span>
                </div>
                <label class="LabelStyle Label3Style">Mã đơn vị (F9)</label>
                <div class="Field2">
                <s:textfield id="shopCode" name="shopCode" cssClass="InputTextStyle InputText1Style" onchange="DeliveryGroupCatalog.shopCodeChanged();" maxlength="10"></s:textfield>
                 <span class="RequireStyle">(*)</span>
                </div>               
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style">Tên đơn vị</label>
                <s:textfield id="shopName" name="shopName" disabled="disabled" cssClass="InputTextStyle InputText1Style" maxlength="50"></s:textfield>
                <div id="staffDiv">
	                <label class="LabelStyle Label2Style">Mã NVGH (F9)</label>
	                <div class="Field2">
	                	<s:textfield id="staffCode" name="staffCode" cssClass="InputTextStyle InputText1Style" onchange="DeliveryGroupCatalog.staffCodeChanged();" maxlength="10"></s:textfield>
	                	<span class="RequireStyle">(*)</span>
	                </div>                
	                <label class="LabelStyle Label3Style">Tên NVGH</label>
	                <s:textfield id="staffName" name="staffName" disabled="disabled" cssClass="InputTextStyle InputText1Style" maxlength="50"></s:textfield>
                </div>      
                <div id="customerDiv" style="display: none">                
	                <label class="LabelStyle Label2Style">Mã KH (F9)</label>
	                <div class="Field2">
	                	<s:textfield id="customerCode" name="customerCode" cssClass="InputTextStyle InputText1Style" onchange="DeliveryGroupCatalog.customerCodeChanged();" maxlength="10"></s:textfield>
	                	<span class="RequireStyle">(*)</span>
	                </div>                
	                <label class="LabelStyle Label3Style">Tên KH</label>
	                <s:textfield id="customerName" name="customerName" disabled="disabled" cssClass="InputTextStyle InputText1Style" maxlength="50"></s:textfield>
                </div>                
                <div class="Clear"></div>
                <div id="statusDiv">
                	<label class="LabelStyle Label1Style">Trạng thái</label>
		             <div class="Field2">
			         	<div class="BoxSelect BoxSelect1">
			            	<s:select id="status" headerKey="-2" headerValue="-- Chọn trạng thái --"  name="status" cssClass="MySelectBoxClass" list="lstStatusBean" listKey="value" listValue="name"></s:select>                    
			            </div>
			            <span class="RequireStyle">(*)</span>
		             </div>
                </div>
                <div class="Clear"></div>                                
                <div class="ButtonSection">
                	<button id="btnAddInfo" class="BtnGeneralStyle Sprite2" onclick="return DeliveryGroupCatalog.saveInfo();"><span class="Sprite2">Thêm mới</span></button>
                	<button id="btnUpdateInfo" class="BtnGeneralStyle Sprite2" style="display: none" onclick="return DeliveryGroupCatalog.saveInfo();"><span class="Sprite2">Cập nhật</span></button>                	
                	<button id="btnAddCustomer" class="BtnGeneralStyle Sprite2" style="display: none" onclick="return DeliveryGroupCatalog.addCustomer();"><span class="Sprite2">Thêm mới</span></button>                	
                	<img id="loading" style="visibility: hidden;" class="LoadingStyle" src="/resources/images/loading.gif">
                </div>
                <p id="errMsg" class="ErrorMsgStyle Sprite1" style="display: none"></p>
                <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                </div>
                <div id="gridContainer" class="GeneralMilkBox GeneralMilkExtInBox" style="display: none">
                   <div class="GeneralMilkTopBox">
                       <div class="GeneralMilkBtmBox">
                           <h3 class="Sprite2"><span class="Sprite2">Danh sách KH</span></h3>
                           <div class="GeneralMilkInBox">
                               <div class="ResultSection" id="cusGrid">
                                    <p style="display: none" class="WarningResultStyle">Không có kết quả</p>
                                    <table id="grid"></table>
									<div id="pager"></div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
            </div>
		</div>
    </div>
</div>
<s:hidden id="selId" name="id"></s:hidden>
<s:hidden id="hidShopCode" name="shopCode"></s:hidden>
<s:hidden id="hidCode" name="code"></s:hidden>
<s:hidden id="hidName" name="name"></s:hidden>
<s:hidden id="hidShopName" name="shopName"></s:hidden>
<s:hidden id="hidStaffCode" name="staffCode"></s:hidden>
<s:hidden id="hidCustomerCode" name="customerCode"></s:hidden>
<s:hidden id="hidShopCodeTmp" name="shopCode"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	$('#shopName').attr('disabled','disabled');
	$('#staffName').attr('disabled','disabled');
	$('#customerName').attr('disabled','disabled');
	$('#tabInfo').bind('click', function() {
		DeliveryGroupCatalog.showInfoTab($('#code').val(), $('#name').val(), $('#shopCode').val(), $('#shopName').val());
	});
	if($('#selId').val().trim().length > 0){
		$('#tabCusActive').removeClass('Disable');
		disabled('code');
		disabled('shopCode');
		$('#btnAddInfo').hide();
		$('#btnUpdateInfo').show();
		$('#tabCus').bind('click', function() {
			DeliveryGroupCatalog.showCustomerTab($('#hidCode').val(), $('#hidName').val(), $('#hidShopCode').val(), $('#hidShopName').val());
		});
	}	
	$('#shopCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var lstParam = new Array();
			var obj = {};
			obj.name = 'filterShop';
			obj.value = 1;
			lstParam.push(obj);
			CommonSearch.searchShopOnDialog(function(data){
				$('#shopCode').val(data.code);
				$('#shopName').val(data.name);
			},lstParam);
		}
	});
	$('#staffCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var arrParam = new Array();
			var param = {};
			param.name = "staffTypeCode";
			param.value = '4';
			var param2 = {};
			param2.name = "shopCode";
			param2.value = $('#shopCode').val().trim();
			arrParam.push(param);
			arrParam.push(param2);
			CommonSearch.searchStaffVansaleOnDialog(function(data){
				$('#staffCode').val(data.code);
				$('#staffName').val(data.name);
			},arrParam);
		}
	});
	$('#customerCode').bind('keyup', function(event){
		if(event.keyCode == keyCode_F9){
			var lstParam = new Array();
			var objD = {};
			objD.name = 'shopCode';
			objD.value = $('#shopCode').val().trim();
			lstParam.push(objD);
			var objGroupTransfer = {};
			objGroupTransfer.name = 'groupTransferId';
			objGroupTransfer.value = $('#selId').val().trim();
			lstParam.push(objGroupTransfer);
			CommonSearch.searchCustomerOnDialog(function(data){
				$('#customerCode').val(data.code);
				$('#customerName').val(data.name);
			},lstParam);
		}
	});
	$('#shopCode').bind('keyup', function(event){
		if(event.keyCode == 8){
			var shopCodeValue = $('#shopCode').val().trim();
			if(shopCodeValue == ''){
				$('#shopName').val('');
			}
		}
	});
	$('#shopCode').bind('blur', function(event){
		if(event.keyCode == 8){
			var shopCodeValue = $('#shopCode').val().trim();
			if(shopCodeValue == ''){
				$('#shopName').val('');
			}
		}
	});
});
</script>
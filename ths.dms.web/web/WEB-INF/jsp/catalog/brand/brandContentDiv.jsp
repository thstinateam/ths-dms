<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<div class="BreadcrumbSection">
                <ul class="ResetList FixFloat BreadcrumbList">
                    <li class="Sprite1"><a href="#">Danh mục</a></li>
                    <li><span>Quản lý nhãn hàng</span></li>
                </ul>
</div>
<div class="CtnOneColSection">
                <div class="ContentSection">
                	<div class="ToolBarSection">
                        <div class="SearchSection GeneralSSection">
                        	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                            <div class="SearchInSection SProduct1Form">
                            	<label class="LabelStyle Label1Style">Mã nhãn hàng</label>
                                <input id="brandCode" maxlength="20" type="text" class="InputTextStyle InputText1Style" />
                                <label class="LabelStyle Label1Style">Tên nhãng hàng</label>
                                <input id="brandName" maxlength="50" type="text" class="InputTextStyle InputText1Style" />
                                
                                <div class="Clear"></div>
                                <div class="BtnCenterSection">
                                    <button id="btnSearch" class="BtnGeneralStyle"  onclick="return BrandCatalog.searchBrand();" >Tìm kiếm</button>
                                </div>
                                <div class="Clear"></div>
                            </div>
                        </div>
                        <div class="Clear"></div>
                    </div>
                    <div class="GeneralCntSection">
                    	<h2 class="Title2Style">Danh sách nhãn hàng</h2>
                    	<div class="GridSection">
                        	<div class="ResultSection" id="brandGrid">
                    			<table id="grid"></table>
                			</div>
                        </div>
                    </div>
                </div>
                <div class="Clear"></div>
            </div>

<div id="divDialog" style="visibility: hidden;">
	<div id="popup1" class="easyui-dialog" title="Cập nhật nhãn hàng" style="width:550px;height:180px;" data-options="closed:true,modal:true">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form" id="fm">
                <label class="LabelStyle Label1Style">Mã nhãn hàng</label>
                <input id="brandCodePop" maxlength="50" type="text" class="InputTextStyle InputText2Style" disabled="disabled" />
                <label class="LabelStyle Label2Style">Tên nhãn hàng</label>
                <input id="brandNamePop" maxlength="250" type="text" class="InputTextStyle InputText2Style"/>
                <label class="LabelStyle LabelPLStyle"><span class="ReqiureStyle">(*)</span></label>
                <div class="Clear"></div>                
                <div class="BtnCenterSection">
                    <button id="btnEdit" onclick="return BrandCatalog.saveBrand();" class="BtnGeneralStyle">Cập nhật</button>
                </div>
                <div class="Clear"></div>                
                <p class="ErrorMsgStyle SpriteErr" style="" id="errMsg"></p>
            </div>
		</div>
    </div>
</div>
<div id="productContainerDiv" class="GeneralCntSection" style="display: none;" >
	<h2 class="Title2Style" id="productTitle" >Danh sách sản phẩm thuộc nhãn hàng </h2>
	<div class="GridSection GridSection2">
			<table id="productGrid" ></table>
	</div>
	<div class="Clear"></div>
	<p class="ErrorMsgStyle SpriteErr" style="" id="errMsgId"></p>
	<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
</div>
<!-- Thong NM  -->
<div id ="searchStyle2EasyUIDialogDiv" style="display:none; width:630px; height: auto;" >
	<div id="searchStyle2EasyUIDialog" data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">				
				<label id="searchStyle2EasyUICodeLabel" class="LabelStyle Label2Style">Mã</label>
				<input id="searchStyle2EasyUICode" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText5Style" /> 
				<label id="searchStyle2EasyUINameLabel" class="LabelStyle Label2Style">Tên</label> 
				<input id="searchStyle2EasyUIName" type="text" maxlength="250" tabindex="2"  class="InputTextStyle InputText5Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnSearchOnDialog" tabindex="3" id="btnSearchStyle2EasyUI">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
				<s:hidden id="searchStyle2EasyUIUrl"></s:hidden>
				<s:hidden id="searchStyle2EasyUICodeText"></s:hidden>
				<s:hidden id="searchStyle2EasyUINameText"></s:hidden>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle" id="searchStyle2EasyUIContainerTitle">Kết quả tìm kiếm </h2>
				<div class="GridSection" id="searchStyle2EasyUIContainerGrid">
					<table id="searchStyle2EasyUIGrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle" style="display: none;" id="btnSearchStyle2EasyUIUpdate">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" id="btnSearchStyle2EasyUIClose" onclick="$('#searchStyle2EasyUIDialog').dialog('destroy');">Đóng</button>
				</div>
			</div>
			<p id="errMsgSearchStyle2EasyUI" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
		</div>
	</div>
</div>
<s:hidden id="selBrandId" value="0"></s:hidden>
<s:hidden id="focusTextDefault" value="brandCode"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('.MySelectBoxClass').customStyle();
	$('.RequireStyle').hide();
	$("#grid").datagrid({
		  url : '/catalog/brand/search',
		  queryParams : {
			  status : 1
		  },
	  	  pageList  : [10,20,30],
		  width: $(window).width() - 50,
		  pageSize : 10,
		  checkOnSelect :true,
		  height:'auto',
		  scrollbarSize : 0,
		  pagination:true,
		  singleSelect:true,
		  fitColumns:true,		  
		  rownumbers: true,
		  columns:[[	
			{field: 'productInfoCode', title: 'Mã nhãn hàng', width: 150, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
			}},
		    {field: 'productInfoName', title: 'Tên nhãn hàng', width: $(window).width() - 300, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
			}},
		    {field: 'view', title: '', width: 50, align: 'center',sortable:false,resizable:false,
		    	formatter: function(value,row,index){
		    		var html =  '<a href="javascript:void(0)" onclick="BrandCatalog.loadProductOfBrand({0})" ><img src="/resources/images/icon-view.png"  /></a>';
		    		return format(html,index);
		    	}},							   					    
		    {field: 'edit', title: '<img src="/resources/images/icon_add.png" style="cursor:pointer" alt="Thêm mới" title="Thêm mới" onclick="BrandCatalog.getSelectedBrand()" />', width: 50, align: 'center',sortable:false,resizable:false, 
		    	formatter:function(value, row,index){
		    		var html = '<a href="javascript:void(0)" onclick="BrandCatalog.getSelectedBrand({0})" ><img src="/resources/images/icon-edit.png" /></a>';
		    		return format(html,index);
		    	}
		   	},
		  ]],
		 
		  onLoadSuccess:function(){
		      	$('#grid').datagrid('resize');
		      	var tm = setTimeout(function(){
		      		var hg = $('.ContentSection').height() - 100; 
			      	$('.ReportTreeSection').css('height',hg);
		      	},500);
		      }
		});
});

</script>
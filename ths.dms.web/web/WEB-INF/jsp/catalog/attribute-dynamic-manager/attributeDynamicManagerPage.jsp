<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
	.Label1PopupStyle {width:107px !important;}
	.Label2PopupStyle {width:91px !important;padding-left: 75px !important;}
	.Label3PopupStyle {width:120px !important; color:#215ea2;}
	.Label4PopupStyle {width:50px !important;padding-left: 2px !important;}
	.BoxSelect1Pop .MySelectBoxClass { width:465px !important; }
	.BoxSelect1Pop .CustomStyleSelectBoxInner { width:434px; }
</style>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="#"><s:text name="organization_system" /> </a></li>
		<li><span> <s:text name="qltt_title" /> </span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style"><s:text name="jsp.common.thongtintimkiem" /></h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="qltt_ma_thuoc_tinh" /></label> 
					<input id="attributeCode" type="text" class="InputTextStyle InputText1Style" /> 
					<label class="LabelStyle Label1Style"><s:text name="qltt_ten_thuoc_tinh" /></label> 
					<input id="attributeName" type="text" class="InputTextStyle InputText1Style" /> 
					<label class="LabelStyle Label1Style"><s:text name="qltt_doi_tuong_ap_dung" /></label>
					<div class="BoxSelect BoxSelect2">
						<select  id="applyObject" class="MySelectBoxClass">
							<s:iterator var="obj" value="lstAttributeApParam"> 
          		     			<option value="<s:property value="#obj.value" />">
          		     			<s:property value="#obj.apParamName" /></option>                        		
          					</s:iterator>
							<!-- <option value="1"><s:text name="qltt_doi_tuong_khachhang" /></option>
							<option value="2" selected="selected"><s:text name="qltt_doi_tuong_sanpham" /></option>
							<option value="3" ><s:text name="qltt_doi_tuong_nhanvien" /></option> -->
						</select>
					</div>
					<div class="Clear"></div>
					
					<label class="LabelStyle Label1Style"><s:text name="qltt_loai_gia_tri" /></label>
					<div class="BoxSelect BoxSelect2">
						<select  id="valueType" class="MySelectBoxClass">
							<option value=""><s:text name="jsp.common.status.all" /></option>
							<option value="4"><s:text name="qltt_gia_tri_chon_1" /></option>
							<option value="5"><s:text name="qltt_gia_tri_chon_nhieu" /></option>
							<option value="1"><s:text name="qltt_gia_tri_chu" /></option>
							<option value="2"><s:text name="qltt_gia_tri_so" /></option>
							<option value="3"><s:text name="qltt_gia_tri_ngay_thang" /></option>
							<option value="6"><s:text name="qltt_gia_tri_ban_do" /></option>
						</select>
					</div>
					<label class="LabelStyle Label1Style"><s:text name="qltt_trang_thai" /></label>
					<div class="BoxSelect BoxSelect2">
						<select id="status" class="MySelectBoxClass">
							<option value="-1"><s:text name="jsp.common.status.all" /></option>
							<option value="1" selected="selected"><s:text name="action.status.name" /></option>
							<option value="0"><s:text name="pause.status.name" /></option>
						</select>
					</div>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle" onclick="return AttributesDynamicManager.search();">Tìm kiếm</button>
					</div>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="GeneralCntSection">
			<h2 class="Title2Style"><s:text name="qltt_danh_sach_thuoc_tinh" /></h2>
			<div class="GridSection" id="gridContainer">
				<table id="attributeGrid" ></table>				
			</div>
			<div class="Clear"></div>
		</div>
		<div id="attributeDetailDataDiv" class="GeneralCntSection">
			<h2 class="Title2Style" id="idTitleDetail"><s:text name="qltt_danh_sach_gia_tri_cua_tt" /></h2>
			<div id="attributeDetailDataGridSection2" class="GridSection GridSection2">				
				<table id="attributeDetailGrid"  ></table>				
			</div>
			<div class="Clear"></div>
		</div>
	</div>
	 <div class="Clear"></div>
     <p id="errMsg" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
     <p id="successMsg" style="display: none;" class="SuccessMsgStyle"></p>
</div>
<div id="searchStyle1EasyUIDialogDiv" style="display: none;">
	<div id="popupAttribute" class="easyui-dialog" title='<s:text name="qltt_thong_tin_thuoc_tinh" />' style="width:550px;height:220px;" data-options="closed:true,modal:true">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form">
				<label class="LabelStyle Label1Style Label1PopupStyle"><s:text name="qltt_doi_tuong_ap_dung" /></label>
				<div class="BoxSelect BoxSelect6">
					<select id="applyObjectPop" class="MySelectBoxClass">
							<s:iterator var="obj" value="lstAttributeApParam"> 
          		     			<option value="<s:property value="#obj.value" />">
          		     			<s:property value="#obj.apParamName" /></option>                        		
          					</s:iterator>
							<!-- <option value="1"><s:text name="qltt_doi_tuong_khachhang" /></option>
							<option value="2"><s:text name="qltt_doi_tuong_sanpham" /></option>
							<option value="3" ><s:text name="qltt_doi_tuong_nhanvien" /></option> -->
					</select>
				</div>
				<label class="LabelStyle Label2Style Label2PopupStyle"><s:text name="qltt_loai_gia_tri" /></label>
				<div class="BoxSelect BoxSelect6">
					<select id="valueTypePop" class="MySelectBoxClass">
						<option value="4"><s:text name="qltt_gia_tri_chon_1" /></option>
						<option value="5"><s:text name="qltt_gia_tri_chon_nhieu" /></option>
						<option value="1"><s:text name="qltt_gia_tri_chu" /></option>
						<option value="2"><s:text name="qltt_gia_tri_so" /></option>
						<option value="3"><s:text name="qltt_gia_tri_ngay_thang" /></option>
						<option value="6"><s:text name="qltt_gia_tri_ban_do" /></option>
					</select>
				</div>
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style Label1PopupStyle"><s:text name="qltt_ma_thuoc_tinh" /><span class="ReqiureStyle"> * </span></label>
                <input id="attributeCodePop" type="text" class="InputTextStyle InputText2Style" maxlength="40" />
                <label class="LabelStyle Label2Style Label2PopupStyle"><s:text name="qltt_ten_thuoc_tinh" /> <span class="ReqiureStyle"> * </span></label>
                <input id="attributeNamePop" type="text" class="InputTextStyle InputText2Style" style="width: 136px" maxlength="250"/>
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style Label1PopupStyle"><s:text name="qltt_thuoc_tinh_bat_buoc" /></label>
                <div id="mandatoryPopDiv" class="BoxSelect BoxSelect6">
                    <select id="mandatoryPop" class="MySelectBoxClass">
                    	<option value="0" selected="selected"><s:text name="qltt_bat_buoc_khong" /></option>
                        <option value="1" ><s:text name="qltt_bat_buoc_co" /></option>
                    </select>
                </div>
               <label class="LabelStyle Label2Style Label2PopupStyle"><s:text name="qltt_thu_tu_hien_thi" /></label>
                <input id="displayOrderPop"  type="text" class="InputTextStyle InputText2Style vinput-number" style="width: 136px" maxlength="2"/>
				<div class="Clear"></div>
                <label class="LabelStyle Label1Style Label1PopupStyle"><s:text name="common.description" /></label>
		        <textarea class="InputTextStyle InputArea1Style" rows="4" cols="50" id="descriptionPop" style="width: 459px;margin-left: 0px" maxlength="1000"></textarea>
                <div id="idDivStatusPop" style="display: none;">
	                <label class="LabelStyle Label1Style Label1PopupStyle"><s:text name="qltt_trang_thai" /></label>
	                <div id="statusPopDiv" class="BoxSelect BoxSelect1Pop">
	                    <select id="statusPop" class="MySelectBoxClass">
	                    	<option value="1" selected="selected"><s:text name="action.status.name" /></option>
	                        <option value="0" ><s:text name="pause.status.name" /></option>
	                    </select>
	                </div>
                </div>
                <div class="Clear"></div>
                <div id="idDivChoseText" style="display: none;">
	                <label class="Label3PopupStyle" style="margin-top: 5px" ><u><i><s:text name="qltt_rang_buoc_gia_tri_nhap" /> :</i></u></label>
	                <br />
	                <br />
	                <label class="LabelStyle Label1Style Label1PopupStyle"><s:text name="qltt_do_dai_toi_da" /></label>
	                <input id="dataLengthPop" type="text" class="InputTextStyle InputText2Style vinput-number" maxlength="4"/>
                </div>
                <div id="idDivChoseNumber" style="display: none;">
	               	<label class="LabelStyle Label7Style"><s:text name="qltt_gia_tri_hop_le_tu" /></label>
	                <input id="minValuePop" type="text" class="InputTextStyle InputText4Style vinput-number" maxlength="8" style="width: 10%" />
	                <label class="LabelStyle " style="padding:1%" ><s:text name="qltt_gia_tri_hop_le_den" /></label>
	                <input id="maxValuePop" type="text" class="InputTextStyle InputText4Style vinput-number" style="width: 10%" maxlength="8"/>
                </div>
               
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSaveAttr" onclick="return AttributesDynamicManager.saveAttribute();" class="BtnGeneralStyle"><s:text name="acton.type.status.update" /></button>
                    <button onclick="$('.easyui-dialog').dialog('close');" class="BtnGeneralStyle" ><s:text name="qltt_bo_qua" /></button>
                </div>
                <input type="hidden" id="attributeIdHid" value="0" />
            </div>
            <div class="Clear"></div>
            <p id="errMsg" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
            <p id="successMsg" style="display: none;" class="SuccessMsgStyle"></p>
		</div>
    </div>
 </div>   
 <div id="searchStyle1EasyUIDialogDiv" style="display: none;">
    <div id="popupAttributeDetail" class="easyui-dialog" title='<s:text name="qltt_thong_tin_gia_tri" />' style="width:550px;height:210px;" data-options="closed:true,modal:true">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form">
                <label class="LabelStyle Label1Style"><s:text name="qltt_ma_gia_tri" /><span class="ReqiureStyle"> * </span></label>
                <input id="attributeDetailCodePop" type="text" class="InputTextStyle InputText2Style" style="width: 375px" maxlength="40" />
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style"><s:text name="qltt_ten_gia_tri" /><span class="ReqiureStyle"> * </span></label>
                <input id="attributeDetailNamePop" type="text" class="InputTextStyle InputText2Style" style="width: 375px" maxlength="250" />
<%--                 <label class="LabelStyle Label1Style">Trạng thái <span class="ReqiureStyle">*</span></label> --%>
<!--                 <div  class="BoxSelect BoxSelect1Pop"> -->
<%--                     <select id="statusDetailPop" class="MySelectBoxClass"> --%>
<!--                         <option value="0">Tạm ngưng</option> -->
<!--                         <option value="1" selected="selected">Hoạt động</option> -->
<%--                     </select> --%>
<!--                 </div> -->
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSaveAttrDetail" class="BtnGeneralStyle" onclick="return AttributesDynamicManager.saveAttributeDetail();"><s:text name="acton.type.status.update" /></button>
                    <button onclick="$('.easyui-dialog').dialog('close');" class="BtnGeneralStyle" ><s:text name="qltt_bo_qua" /></button>
                </div>
                 <input type="hidden" id="enumIdHid" value="0" />
                 <input type="hidden" id="attributeIdHid" />
                 <input type="hidden" id="applyObjectHid" />
            </div>
	            <div class="Clear"></div>
		        <p id="errMsg" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
		        <p id="successMsg" style="display: none;" class="SuccessMsgStyle"></p>
		</div>
    </div>
</div>    
<s:hidden id="idShowObject" name="showObjectDMSCORE"></s:hidden>
<script type="text/javascript">
	//$('.MySelectBoxClass').customStyle();
	$("#attributeCode").focus();
	Utils.bindAutoButtonEx('.ContentSection','btnSearch');
	$(document).ready(function(){
		AttributesDynamicManager.loadGrid();
		$('#popupAttribute #valueTypePop').bind('change', function() {
			var type = $('#popupAttribute #valueTypePop').val();
			if(type == AttributesDynamicManager.ATTRIBUTE_TYPE.CHARACTER){
				$('#popupAttribute #idDivChoseText').show();
				$('#popupAttribute #idDivChoseNumber').hide();
			}else if(type == AttributesDynamicManager.ATTRIBUTE_TYPE.NUMBER){
				$('#popupAttribute #idDivChoseText').show();
				$('#popupAttribute #idDivChoseNumber').show();
			}else{
				$('#popupAttribute #idDivChoseText').hide();
				$('#popupAttribute #idDivChoseNumber').hide();
			}
		});
	});
	$('#attributeDetailDataDiv').hide();
	AttributesDynamicManager.HTML=$('#attributeDetailDataDiv').html(); 
</script>




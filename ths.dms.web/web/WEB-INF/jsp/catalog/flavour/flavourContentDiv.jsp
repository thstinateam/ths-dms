<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<div class="BreadcrumbSection">
                <ul class="ResetList FixFloat BreadcrumbList">
                    <li class="Sprite1"><a href="#">Danh mục</a></li>
                    <li><span>Danh sách Flavour</span></li>
                </ul>
</div>
<div class="CtnOneColSection">
                <div class="ContentSection">
                	<div class="ToolBarSection">
                        <div class="SearchSection GeneralSSection">
                        	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                            <div class="SearchInSection SProduct1Form">
                            	<label class="LabelStyle Label1Style">Mã flavour</label>
                                <input id="flavourCode" maxlength="20" type="text" class="InputTextStyle InputText1Style" />
                                <label class="LabelStyle Label1Style">Tên flavour</label>
                                <input id="flavourName" maxlength="50" type="text" class="InputTextStyle InputText1Style" />
                                <label class="LabelStyle Label1Style">Ghi chú</label>
                                <input id="description" maxlength="100" type="text" class="InputTextStyle InputText1Style" />
                                <div class="Clear"></div>
                                <div class="BtnCenterSection">
                                    <button id="btnSearch" class="BtnGeneralStyle"  onclick="return FlavourCatalog.search();" >Tìm kiếm</button>
                                </div>
                                <div class="Clear"></div>
                            </div>
                        </div>
                        <div class="Clear"></div>
                    </div>
                    <div class="GeneralCntSection">
                    	<h2 class="Title2Style">Danh sách flavour</h2>
                    	<div class="GridSection">
                        	<div class="ResultSection" id="flavourGrid">
<!--                     		<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p> -->
                    		<table id="grid"></table>
							<div id="pager"></div>
                </div>
                </div>
                	</div>
                        </div>
                    </div>

 <div id="divDialog" style="visibility: hidden;">          
  <div id="popup1" class="easyui-dialog" title="Cập nhật category" style="width:550px;height:180px;" data-options="closed:true,modal:true">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form" id="fm">
                <label class="LabelStyle Label1Style">Mã flavour</label>
                <input id="flavourCodePop" maxlength="20" type="text" class="InputTextStyle InputText2Style" disabled="disabled" />
                <label class="LabelStyle Label2Style">Tên flavour</label>
                <input id="flavourNamePop" maxlength="50" type="text" class="InputTextStyle InputText2Style"/>
                <label class="LabelStyle LabelPLStyle"><span class="ReqiureStyle">(*)</span></label>
                <div class="Clear"></div>
                <label class="LabelStyle Label1Style">Ghi chú</label>
                <input id="descriptionPop" maxlength="100" type="text" class="InputTextStyle InputText3Style" />
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnEdit" onclick="return FlavourCatalog.save();" class="BtnGeneralStyle">Cập nhật</button>
                </div>
            </div>
		</div>
    </div>
    </div>
<s:hidden id="selId" value="0"></s:hidden>
<s:hidden id="focusTextDefault" value="code"></s:hidden>
<script type="text/javascript">
$(document).ready(function(){
	$('#code').focus();
	$('#name').val('');
	var status = 1;
	$('.MySelectBoxClass').customStyle();
	$('.RequireStyle').hide();
	$("#grid").datagrid({
		url:FlavourCatalog.getGridUrl('','','',status),
		  pageList  : [10,20,30],
		  width: 910,
		  height:'auto',
		  scrollbarSize : 0,
		  pagination:true,			  
		  fitColumns:true,
		  singleSelect:true,
		  method : 'GET',
		  rownumbers: true,
		  width: ($('#areaGrid').width()),
		  columns:[[	
			{field: 'productInfoCode', title: 'Mã flavour', sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
			}},
		    {field: 'productInfoName', title: 'Tên flavour', width: 120, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
			 }},    							  
		    {field: 'description', title: 'Ghi chú', width: 120, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
			}},
		    {field: 'edit', title: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false,formatter: FlavourCatalogFormatter.editCellIconFormatter},							   					    
		    {field: 'id', index: 'id', hidden: true},	   
		  ]]
		});			
});

</script>
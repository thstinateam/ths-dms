<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!-- dungq3; 13/05/2015 -->
<s:if test="lstAttributeDynamic != null && lstAttributeDynamic.size() > 0">
<s:set id="numCombo" value="0"></s:set>
<s:set id="runIndex" value="0"></s:set>
	<k:repeater value="lstAttributeDynamic" status="status">
	<k:itemTemplate>		
		<label class="LabelStyle Label7Style" style="margin-left:40px;" >
			<s:property value="attributeName"/>
			<s:if test="mandatory == 1"> <!--  Bat buoc nhap -->
				<span class="ReqiureStyle">*</span>
			</s:if>
		</label>		
		<s:if test="type != null && (type == 1 || type == 2 || type == 3 || type==6)"><!-- tu nhap, ban do -->
			<s:if test="type != null && type == 2"><!-- kieu so -->
				<input value='<s:property value="attributeDetailVOs.get(0).value"/>'
					maxValue="<s:property value="maxValue"/>",
					minValue="<s:property value="minValue"/>"
					onkeyup="formatThousandNumber('attributeCode_<s:property value="#status.index+1"/>');" 
					onkeypress="return decimalBox(event, 'attributeCode_<s:property value="#status.index+1"/>',true);"  
					id="attributeCode_<s:property value="#status.index+1"/>" type="text" class="InputTextStyle Attribute InputText1Style vinput-number" maxlength="<s:property value="dataLength"/>" require="<s:property value="mandatory"/>" attr-name="<s:property value="attributeName"/>" />
				
			</s:if>
			
			<s:elseif test="type != null && type == 1"><!-- kieu chu -->
				<input value='<s:property value="attributeDetailVOs.get(0).value"/>' id="attributeCode_<s:property value="#status.index+1"/>" type="text" class="InputTextStyle Attribute InputText1Style" maxlength="<s:property value="dataLength"/>"  require="<s:property value="mandatory"/>" attr-name="<s:property value="attributeName"/>"/>
			</s:elseif>
			
			<s:elseif test="type != null && type == 6"><!-- kieu ban do -->
				<input id="attributeCode_<s:property value="#status.index+1"/>" type="hidden" 
						value='<s:property value="attributeDetailVOs.get(0).value"/>'
						require="<s:property value="mandatory"/>" attr-name="<s:property value="attributeName"/>" />
				<label class="LabelStyle Label1Style" style="width: 196px;text-align:left;">						
					<img  onclick="CustomerCatalog.viewBigMapOnDlgDynamic('attributeCode_<s:property value="#status.index+1"/>')"  
							src="/resources/images/Mappin/overlay.png" 
								height="20" width="20" alt="" style="cursor: pointer;float:left;"/>
					
				</label>
			</s:elseif>
			
			<s:elseif test="type != null && type == 3"><!-- kieu ngay -->
				<input value='<s:property value="attributeDetailVOs.get(0).value"/>' id="attributeCode_<s:property value="#status.index+1"/>" type="text" class="InputTextStyle Attribute InputText6Style" maxlength="<s:property value="dataLength"/>" require="<s:property value="mandatory"/>" attr-name="<s:property value="attributeName"/>" />
				<script type="text/javascript" charset="UTF-8">
					applyDateTimePicker('#attributeCode_'+'<s:property value="#status.index+1"/>');
				</script>
			</s:elseif>
		</s:if>
		
		<s:elseif test="type != null && type == 4"><!-- select 1 choice -->
			<div class="BoxSelect BoxSelect2">
				<select id="attributeCode_<s:property value="#status.index+1"/>" class="MySelectBoxClas easyui-combobox Attribute HasClassSelected" data-options="panelWidth : '206',editable:false" require="<s:property value="mandatory"/>" attr-name="<s:property value="attributeName"/>"> 
					<option value=""><s:text name="jsp.common.chon.gia.tri"/></option>					            		
					<s:iterator id="child" value="attributeEnumVOs">
						<s:if test="attributeDetailVOs.size()>0">									
							<s:iterator id="valueChild" value="attributeDetailVOs" >								
								<s:if test="#child.enumId == #valueChild.enumId">								
									<option selected="selected" value='<s:property value="#child.enumId"/>'><s:property value="#child.enumValue"/></option>
								</s:if>
								<s:else>
									<option value='<s:property value="#child.enumId"/>'><s:property value="#child.enumValue"/></option>
								</s:else>
							</s:iterator>
						</s:if>							
						<s:else>
							<option value='<s:property value="#child.enumId"/>'><s:property value="#child.enumValue"/></option>
						</s:else>
					</s:iterator>
				</select>
			</div>
			<s:set id="numCombo" value="#numCombo+1"></s:set> 
			<script type="text/javascript">
					$('#attributeCode_'+<s:property value="#status.index+1"/>).combobox({						
						valueField : "id",
						textField : "text",
						mode:'local',
						filter: function(q, row){
							var opts = $(this).combobox('options');
							return row[opts.valueField].indexOf(q.toUpperCase().trim())==0;
						}
					});
					$('#attributeCode_'+<s:property value="#status.index+1"/>).combobox('setValue',$('#attributeCode_'+<s:property value="#status.index+1"/>+' option[selected]').attr('value'));
			</script> 
		</s:elseif>
		<s:elseif test="type != null && type == 5"><!-- select multi choice -->		            			
			<div class="BoxSelect BoxSelect2">
				<select multiple="multiple" id="attributeCode_<s:property value="#status.index+1"/>" class="MySelectBoxClas Attribute HasClassSelected MultipeSelectBox" require="<s:property value="mandatory"/>" attr-name="<s:property value="attributeName"/>">
				<s:iterator id="child" value="attributeEnumVOs">
					<option id="multiselect-<s:property value='#child.enumId'/>" value='<s:property value="#child.enumId"/>'><s:property value="#child.enumValue"/></option>
				</s:iterator>
				</select>
				<s:if test="attributeDetailVOs != null && attributeDetailVOs.size() > 0">
				<script type="text/javascript">
					var list<s:property value="#numCombo"/> = new Array();
					<s:iterator id="valueChild" value="attributeDetailVOs">
						var value = '<s:property value="#valueChild.enumId"/>';						
						$('#multiselect-'+value).attr('selected', 'selected');
					</s:iterator>					
				</script>
				</s:if>
				<script type="text/javascript">
					$('#attributeCode_'+<s:property value="#status.index+1"/>).multiselect({
						selectedText: function(numChecked, numTotal, checkedItems){
							var max = 2;
							if($(checkedItems[0]).attr('title').trim().length <= 10) {
								max = 3;
							} else if($(checkedItems[0]).attr('title').trim().length > 30){
								max = 1;
							}
							if(checkedItems.length <= max) {
								var resultStr = $(checkedItems[0]).attr('title').trim();
								for(var i = 1; i < checkedItems.length; i++) {
									var obj = checkedItems[i];
									resultStr += ', ' + $(obj).attr('title');
								}
								return resultStr;
							} else {
								return 'Đã chọn '+numChecked+'/'+numTotal;								
							}
						}
					});
					$('.ui-multiselect-menu').each(function(){
						$(this).css('width','196px');
					});
				</script>
			</div>
			<s:set id="numCombo" value="#numCombo+1"></s:set>
		</s:elseif>
		<s:hidden id="attributeId_%{#status.index+1}" name="attributeId"></s:hidden>		
		<s:hidden id="attributeColumnValueType_%{#status.index+1}" name="type"></s:hidden>
		<s:set id="runIndex" value="#runIndex+1"></s:set>
		<s:if test="#runIndex % 3 == 0">
			<div class="Clear"></div>
		</s:if>
	</k:itemTemplate>
	</k:repeater>
</s:if>
<div class="Clear"></div>
<s:hidden id="lstAttributeSize" name="lstAttributeDynamic.size()"></s:hidden>
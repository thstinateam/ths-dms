<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="k" uri="/kryptone" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="ths.dms.helper.Configuration"%>

<style type="text/css">
.VinamilkTheme .tree li { margin-left:18px; background:url(/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/tree_icons.png) -286px 0px repeat-y; }
.VinamilkTheme .tree-indent.tree-join, .VinamilkTheme .tree-indent.tree-joinbottom { display:inline-block;}
.VinamilkTheme .triconline {display:none !important;}


.VinamilkTheme .tree-file {
    background-position: -200px 0;
    display: inline-block;
    width: 9px;
}
.menu-shadow {
    background: none repeat scroll 0 0 #ccc !important; 
    border-radius: 10px !important;
    box-shadow: 2px 2px 3px #cccccc !important;
    position: unset;
}

</style>
	
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

	<div class="BreadcrumbSection">
        <ul class="ResetList FixFloat BreadcrumbList">
            <li class="Sprite1"><a href="#"><s:text name="catalog_sales_brand_catalogy"/></a></li>
            <li><span><s:text name="catalog_unit_and_staff_tree_title"/></span></li>
        </ul>
    </div>
    <div class="CtnOneColSection">
        <div class="ContentSection">
        	<div class="ToolBarSection">
                <div class="SearchSection GeneralSSection">
                    <div class="Sidebar1Section" id="idDivTreeShop">	
                        <h2 class="Title2Style"><s:text name="catalog_unit_tree_title"/></h2> 
                        <p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none;"><s:text name= "jsp.common.err.data.update"/></p>
						<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
						<div style="margin: 10px 0 0 45px;">
							<input onclick="UnitTreeCatalog.cbStopChange();"  id="cbStop" type="checkbox" class="InputCbxStyle"/>
                        	<label class="LabelStyle Label12Style" title = "Hiển thị đơn vị/ nhân viên đang tạm ngưng hoạt động"><s:text name="catalog_unit_hien_thi_trang_thai_tam_ngung"/></label>
                        </div>
                        <div class="Clear"></div>
                        <s:if test="isCheckExistRowsInShop">
	                        <div id="treeDiv" class="SearchInSection SProduct1Form" style="overflow:auto;">
	                            <div class="ReportTreeSection ReportTree2Section">
	                                <ul id="tree"></ul>
	                            </div>
	                        </div>
                        </s:if>
                        <s:else>
	                        <div class="SearchInSection SProduct1Form" >
						     	<div id="idViewFirstTree" style="text-align:center">
						     		<p style="text-align: center;"><s:text name="catalog_unit_tree_first_choose"/></p>
						         	<a style="text-align: center;" href="javascript:void(0);" onclick=""><img src="/resources/images/add-organization.png"></a>
						        </div>
					     	</div>
                        </s:else>
                    </div>
                    <div class="Content1Section">
                    	<div class="ExpendContent">
                        	<ul class="ResetList ExpendContentList">
                            	<li><a id="collapseTab" href="javascript:void(0)" class="Sprite1 Item1" onclick="UnitTreeCatalog.showCollapseTab(0)"><span class="HideText">Mở</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="Content3Section" id="unitTreeContent" style="display: none;">
                	</div>
                </div>
                <div class="Clear"></div>
            </div>
        </div>
        <div class="Clear"></div>
    </div>
<div id="group_tree_dbl_right_contextMenu" class="cmsiscontrol" style="display:none; box-shadow: 2px 2px 3px #cccccc; width:200px;">
    <div id="cmChuyenDonVi" style="font-size: 12px;" onclick="UnitTreeCatalog.openPoppupMovedStaff();" data-options="iconCls:'home-go'"><s:text name="catalog_unit_tree_chuyen_don_vi"/></div>
    <div id="cmTamNgungNhanVien" style="font-size: 12px;" onclick="UnitTreeCatalog.suspendedStaff();" data-options="iconCls:'male-user-remove'"><s:text name="catalog_unit_tree_tam_ngung_doi_tuong_nay"/></div>
</div>
<!-- Liemtpt: Popup chuyen don vi -->
<div id="movedStaffEasyUIDialogDiv" style="display: none;">
    <div id="popupMovedStaff" class="easyui-dialog" title='<s:text name="catalog_unit_Tree_change_unit_tree_title"/>'; style="width:550px;height:210px;" data-options="closed:true,modal:true">
        <div class="PopupContentMid">
        	<div class="GeneralForm Search1Form">
                <label class=common.result.import.excel"LabelStyle Label1Style" style="width: 100px;"><s:text name="catalog_unit_tree_chon_don_vi_dich"/><%-- <span class="ReqiureStyle"> * </span> --%></label>
                <input id="idShopCodeChose" type="text" class="InputTextStyle InputText2Style" style="width: 250px" maxlength="40" />
                <div class="Clear"></div>
                
                <div class="BtnCenterSection" style="height: 40px; padding-top: 40px; margin: 0px;" >
                    <button id="group_tree_dbl_right_btnMoveStaff" class="BtnGeneralStyle" onclick="return UnitTreeCatalog.movedStaff();"><s:text name="acton.type.status.update" /></button>
                    <button onclick="$('.easyui-dialog').dialog('close');" class="BtnGeneralStyle" ><s:text name="qltt_bo_qua" /></button>
                </div>
            </div>
	            <div class="Clear"></div>
		        <p id="errMsg" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
		        <p id="successMsgPop" style="display: none;" class="SuccessMsgStyle"></p>
		</div>
    </div>
</div>
<!--Liemtpt: Popup chuyen don vi -->
<div id ="searchStyle2EasyUIDialogDiv" style="display:none; width:630px; height: auto;" >
	<div id="searchStyle2EasyUIDialog" data-options="closed:true,modal:true">
		<div class="PopupContentMid2">
			<div class="GeneralForm Search1Form">				
				<label id="searchStyle2EasyUICodeLabel" class="LabelStyle Label2Style">Mã</label>
				<input id="searchStyle2EasyUICode" maxlength="50" tabindex="1" type="text" class="InputTextStyle InputText5Style" /> 
				<label id="searchStyle2EasyUINameLabel" class="LabelStyle Label2Style">Tên</label> 
				<input id="searchStyle2EasyUIName" type="text" maxlength="250" tabindex="2"  class="InputTextStyle InputText5Style" />
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button class="BtnGeneralStyle BtnSearchOnDialog" tabindex="3" id="btnSearchStyle2EasyUI">Tìm kiếm</button>
				</div>
				<div class="Clear"></div>
				<s:hidden id="searchStyle2EasyUIUrl"></s:hidden>
				<s:hidden id="searchStyle2EasyUICodeText"></s:hidden>
				<s:hidden id="searchStyle2EasyUINameText"></s:hidden>
			</div>
			<div class="GeneralForm Search1Form">
				<h2 class="Title2Style Title2MTStyle" id="searchStyle2EasyUIContainerTitle">Kết quả tìm kiếm </h2>
				<div class="GridSection" id="searchStyle2EasyUIContainerGrid">
					<table id="searchStyle2EasyUIGrid"></table>
				</div>
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="group_tree_dbl_right_btnMoveShop" class="BtnGeneralStyle" style="display: none;" id="btnSearchStyle2EasyUIUpdate">Chọn</button>
					<button class="BtnGeneralStyle BtnGeneralMStyle" id="btnSearchStyle2EasyUIClose" onclick="$('#searchStyle2EasyUIDialog').dialog('destroy');">Đóng</button>
				</div>
			</div>
			<p id="errMsgSearchStyle2EasyUI" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
		</div>
	</div>
</div>
 <!-- div thong tin bigMap -->
<div class="easyui-dialog" data-options="closed:true,modal:true" style="width: 970px; height: 575px; " id="viewBigMap">
	<div id="fancyboxBigMap" class="GeneralDialog General2Dialog" style="width: 965px; height: 535px">
		<span id="imgBtnDeleteLatLng" style="z-index: 15000; position: absolute; cursor: pointer; margin-left: 50%; margin-top: 52%; color: #215EA2">
				<img  title="<s:text name="catalog.product.img.latlng.title"></s:text>" 
					alt="<s:text name="catalog.product.img.latlng.title">
					</s:text>" src="/resources/images/icon_delete.png">
  				&nbsp;&nbsp;<s:text name="catalog.product.img.latlng.title"></s:text>	
		</span>
		<span id="imgBtnUpdateLatLng" style="z-index: 15000; position: absolute; cursor: pointer; margin-left: 60%; margin-top: 52%; color: #215EA2">	
  				<img  title="<s:text name="jsp.common.dongy"></s:text>" 
					alt="<s:text name="jsp.common.dongy">
					</s:text>" src="/resources/images/checked_small.png">
  				&nbsp;&nbsp;<s:text name="jsp.common.dongy"></s:text>
		</span>
		<span id="imgBtnCancelLatLng" style="z-index: 15000; position: absolute; cursor: pointer; margin-left: 70%; margin-top: 52%; color: #215EA2">	
  				<img  title="<s:text name="jsp.common.bo.qua"></s:text>" 
					alt="<s:text name="jsp.common.bo.qua">
					</s:text>" src="/resources/images/icon_close.png">
  				&nbsp;&nbsp;<s:text name="jsp.common.bo.qua"></s:text>
		</span>
		<div id="bigMapContainer" style="width: 965px; height: 535px"></div>
		<div class="Clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	UnitTreeCatalog._contextMenuHTML = $('#group_tree_dbl_right_contextMenu').html();
	UnitTreeCatalog.loadUnitTree();// load cây và các event của nó
	var tm = setTimeout(function(){
		 UnitTreeCatalog.resize();
		 clearTimeout(tm);
	 }, 150);
	$('#group_tree_dbl_right_contextMenu').addClass('easyui-menu');
	$(window).bind('keyup',function(event){
		if(event.keyCode == keyCodes.ESCAPE && CommonSearchEasyUI._isOpenDialog == true){
			$('#searchStyle2EasyUIDialog').dialog('destroy');
 		}
		return true;
   	});
	
	$('#idShopCodeChose').combotree({
		url: '/catalog/unit-tree/search-unit-tree',
		lines: false,
		formatter: function(node) {
			return Utils.XSSEncode(node.text);
		},
		onLoadSuccess: function(node, data) {
			$('#idShopCodeChose').combotree('setValue', data[0].id);
			UnitTreeCatalog.searchShop();			
		}
	});
});  
</script>


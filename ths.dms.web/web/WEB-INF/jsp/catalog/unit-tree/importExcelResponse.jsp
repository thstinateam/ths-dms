<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="k" uri="/kryptone" %>
<div id="errorExcel" style="display: none"><s:property value="isError"/></div>
<div id="errorExcelMsg" style="display: none"><s:property value="errMsg"/></div>
<div id="totalRow" style="display: none"><s:property value="totalItem"/></div>
<div id="numFail" style="display: none"><s:property value="numFail"/></div>
<div id="fileNameFail" style="display: none"><s:property value="fileNameFail"/></div>
<div id="typeView" style="display: none"><s:property value="typeView"/></div>
<div id="lstSize" style="display: none"><s:property value="lstView.size()"/></div>
<input type="hidden" id="newToken" value='<s:property value="token"/>' name="newToken">
<div id="popup1" style="display:none;">
		<div class="GeneralDialog General1Dialog">
        	<div class="DialogProductSearch">
            	<div class="GeneralTable TableUnitTreeSection">
            	<div class="ScrollSection">
            		<div class="BoxGeneralTTitle">
				          <table width="100%" border="0" cellspacing="0" cellpadding="0">
				              <colgroup>
				              	  <col style="width:50px;" />
				                  <col style="width:100px;" />
				                  <col style="width:170px;" />
				                  <col style="width:100px;" />
				                  <col style="width:100px;" />
				                  <col style="width:100px;" />
				                  <col style="width:170px;" />
				                  <col style="width:170px;" />
				                  <col style="width:70px;" />
				                  <col style="width:100px;" />
				                  <col style="width:100px;" />
				                  <col style="width:170px;" />
				                  <col style="width:170px;" />
				                  <col style="width:170px;" />
				                  <col style="width:100px;" />
				                  <col style="width:170px;" />
				                  <col style="width:100px;" />
				                  <col style="width:100px;" />
				                  <col style="width:200px;" />
				                  <col style="width:170px;" />
				                  <col style="width:170px;" />
				                  <col style="width:350px;" />
				              </colgroup>
				              <thead>
				                  <tr>
				                      <th class="ColsThFirst">STT</th>
				                      <th>Mã đơn vị</th>
				                      <th>Tên đơn vị</th>
				                      <th>Mã đơn vị cha</th>
				                      <th>Mã số thuế</th>
				                      <th>Số tài khoản</th>
				                      <th>Tên ngân hàng</th>
				                      <th>Tên người liên hệ</th>
				                      <th>Loại đơn vị</th>
				                      <th>Số cố định</th>
				                      <th>Số di động</th>
				                      <th>Email</th>
				                      <th>Địa chỉ giao hóa đơn</th>
				                      <th>Địa chỉ giao hàng</th>
				                      <th>Số fax</th>
				                      <th>Tỉnh/thành</th>
				                      <th>Quận/huyện</th>
				                      <th>Phường/xã</th>
				                      <th>Địa chỉ</th>
				                      <th>Lattitude</th>
				                      <th>Longtitude</th>
				                      <th>Mô tả</th>
				                  </tr>
				              </thead>
				          </table>
			        </div>
                	<div class="BoxGeneralTBody">
	                	<div class="ScrollBodySection" id="scrollExcelDialog">
	                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                            <colgroup>
	                              <col style="width:50px;" />
				                  <col style="width:100px;" />
				                  <col style="width:170px;" />
				                  <col style="width:100px;" />
				                  <col style="width:100px;" />
				                  <col style="width:100px;" />
				                  <col style="width:170px;" />
				                  <col style="width:170px;" />
				                  <col style="width:70px;" />
				                  <col style="width:100px;" />
				                  <col style="width:100px;" />
				                  <col style="width:170px;" />
				                  <col style="width:170px;" />
				                  <col style="width:170px;" />
				                  <col style="width:100px;" />
				                  <col style="width:170px;" />
				                  <col style="width:100px;" />
				                  <col style="width:100px;" />
				                  <col style="width:200px;" />
				                  <col style="width:170px;" />
				                  <col style="width:170px;" />
				                  <col style="width:350px;" />
	                            </colgroup>
	                            <tbody>
		                            <k:repeater value="lstView" status="status">
										<k:itemTemplate>
			                                <tr>
			                                	<td class="ColsTd1"><s:property value="#status.index"/></td>
			                                    <td class="ColsTd2"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content1"/></div></td>                                                  
			                                    <td class="ColsTd3"><div class="ColsAlignLeft" style="width: 149px"><s:property value="content2"/></div></td>
			                                    <td class="ColsTd4"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content3"/></div></td>                                                  
			                                    <td class="ColsTd5"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content4"/></div></td>
			                                    <td class="ColsTd6"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content5"/></div></td>                                                  
			                                    <td class="ColsTd7"><div class="ColsAlignLeft" style="width: 149px"><s:property value="content6"/></div></td>
			                                    <td class="ColsTd8"><div class="ColsAlignLeft" style="width: 149px"><s:property value="content7"/></div></td>
			                                    <td class="ColsTd9"><div class="ColsAlignLeft" style="width: 49px"><s:property value="content8"/></div></td>
			                                    <td class="ColsTd10"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content9"/></div></td>
			                                    <td class="ColsTd11"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content10"/></div></td>
			                                    <td class="ColsTd12"><div class="ColsAlignLeft" style="width: 149px"><s:property value="content11"/></div></td>
			                                    <td class="ColsTd13"><div class="ColsAlignLeft" style="width: 149px"><s:property value="content12"/></div></td>
			                                    <td class="ColsTd14"><div class="ColsAlignLeft" style="width: 149px"><s:property value="content13"/></div></td>
			                                    <td class="ColsTd15"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content14"/></div></td>
			                                    <td class="ColsTd16"><div class="ColsAlignLeft" style="width: 149px"><s:property value="content15"/></div></td>
			                                    <td class="ColsTd17"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content16"/></div></td>
			                                    <td class="ColsTd18"><div class="ColsAlignLeft" style="width: 79px"><s:property value="content17"/></div></td>
			                                    <td class="ColsTd19"><div class="ColsAlignLeft" style="width: 179px"><s:property value="content18"/></div></td>
			                                    <td class="ColsTd20"><div class="ColsAlignLeft" style="width: 149px"><s:property value="content19"/></div></td>
			                                    <td class="ColsTd21"><div class="ColsAlignLeft" style="width: 149px"><s:property value="content20"/></div></td>
			                                    <td class="ColsTd22"><div class="ColsAlignLeft" style="width: 329px"><s:property value="content21" escapeHtml="false"/></div></td>
			                                </tr>
										</k:itemTemplate>
									</k:repeater>
	                            </tbody>
	                        </table>
                        </div>
                    </div>
                </div>
                <div class="BoxDialogBtm">
                    <div class="ButtonSection"><button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button></div>
                </div>
                </div>
        	</div>
        </div>
    </div>
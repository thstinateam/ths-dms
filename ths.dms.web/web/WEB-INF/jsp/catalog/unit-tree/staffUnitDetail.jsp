<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

   <div class="ReportCtnSection">
       <h2 class="Title2Style"><s:text name="catalog_unit_thong_tin_nhan_vien"/></h2>
       <div class="GeneralForm SProduct1Form">
       		<div class="SearchInSection SProduct1Form" id="divInfoBasic">
	           <label class="LabelStyle Label17Style"><s:text name="catalog.unit.tree"/></label>
	           		<input type="text" class="InputTextStyle InputText1Style classCheckChange" id="staffShopCode" disabled="disabled" value="<s:property value="staff.shop.shopName"/>" maxlength="100"/>
	           <label class="LabelStyle Label15Style"><s:text name="catalog.staff.type"/></label>
	           <div class="BoxSelect BoxSelect2">
					 <select id="staffType" class="MySelectBoxClass classCheckChange" style="width: 207px;" name="staffType">	
						<s:iterator value="lstStaffTypeView">
							<option value='<s:property value="id"/>'><s:property value="name"/></option>
						</s:iterator>
					</select>
	            </div>
	           <div class="Clear"></div>
	           <label class="LabelStyle Label17Style"><s:text name="catalog.staff.code"/> <span class="ReqiureStyle">*</span></label>
	           <input type="text" class="InputTextStyle InputText1Style classCheckChange" id="staffCode" value="<s:property value="staff.staffCode"/>" maxlength="50"/>
	           <label class="LabelStyle Label15Style"><s:text name="catalog.staff.name"/> <span class="ReqiureStyle">*</span></label>
	           <input type="text" class="InputTextStyle InputText1Style classCheckChange" id="staffName" value="<s:property value="staff.staffName"/>" maxlength="100"/>
	           <div class="Clear"></div>
	           <label class="LabelStyle Label17Style"><s:text name="catalog.gender.value"/></label>
	           <div class="BoxSelect BoxSelect2">
	               <select id="gender" class="MySelectBoxClass classCheckChange" name="LevelSchool">							
						<option value="-1"><s:text name="catalog.gender.value.choose"/></option>
						<option value="1"><s:text name="catalog_unit_gt_nam"/></option>
						<option value="0"><s:text name="catalog_unit_gt_nu"/></option>
					</select>
	           </div>
	           <label class="LabelStyle Label15Style"><s:text name="catalog.workStartDate.value"/></label>
	           <input type="text" class="InputTextStyle InputText9Style classCheckChange" id="workStartDate" value="<s:date name="staff.startWorkingDay" format="dd/MM/yyyy"/>" maxlength="10"/>
	           <div class="Clear"></div>
	           <label class="LabelStyle Label17Style"><s:text name="jsp.common.status"/></label>
	            <div class="BoxSelect BoxSelect2">
	            <s:if test="staff != null && staff.status.value == 1">
	            	<select id="status" class="MySelectBoxClass classCheckChange">											
						<option value="1"><s:text name="jsp.common.status.active"/></option>
						<option value="0"><s:text name="jsp.common.status.stoped"/></option>
					</select>
	            </s:if>
	            <s:elseif test="staff != null && staff.status.value == 0">
	            	<select id="status" class="MySelectBoxClass classCheckChange">											
						<option value="0"><s:text name="jsp.common.status.stoped"/></option>
						<option value="1"><s:text name="jsp.common.status.active"/></option>
					</select>
	            </s:elseif>
	            <s:else>
	            	 <select class="MySelectBoxClass classCheckChange" id="status">
			          <option value="-2"><s:text name="choose.status"/></option> 
			          <option value="0"><s:text name="jsp.common.status.stoped"/></option> 
			          <option value="1" selected="selected"><s:text name="jsp.common.status.active"/></option> 
			       </select>
	            </s:else>
	            </div>	      
		            <label class="LabelStyle Label15Style">Hình thức bán hàng</label>
		            <div class="BoxSelect BoxSelect1" id="subShop">              
	                	<select id="orderProduct" class="MySelectBoxClass" >      
	                		<option value="">Chọn hình thức bán hàng</option>
	                		<s:if test = "lstProductType != null">
        			   			<s:iterator var = "obj" value = "lstProductType">
        			 					<option value = "<s:property value="#obj.apParamCode" />">
        									<s:property value = "codeNameDisplay(#obj.apParamCode, #obj.description)" />
        								</option>
        			   			</s:iterator>
	                		</s:if>
	         		   	</select> 
	             	</div>
	             	<div class="Clear"></div>
		            <label class="LabelStyle Label17Style">Ngành hàng con</label>
		            <input type="text" id="manageSubStaff" style="width: 204px;" class="InputTextStyle" />
            </div>
            <!-- END THÔNG TIN NHÂN VIÊN -->
            <div class="Clear"></div>
            <!-- BEGIN THÔNG TIN LIÊN LẠC-->
            <h2 class="Title2Style"><s:text name="catalog_unit_thong_tin_lien_lac"/></h2>
			<div class="SearchInSection SProduct1Form" id="divInfoContact">
				<label class="LabelStyle Label17Style"><s:text name="organization_phone"/></label>
	            <input type="text" class="InputTextStyle InputText1Style classCheckChange" id="staffTelephone" value="<s:property value="staff.phone"/>" maxlength="20" />
				<label class="LabelStyle Label15Style"><s:text name="organization_mobile"/></label>
	            <input type="text" class="InputTextStyle InputText1Style classCheckChange" id="staffPhone" value="<s:property value="staff.mobilephone"/>" maxlength="20" />
	            <div class="Clear"></div>	
	            <label class="LabelStyle Label17Style"><s:text name="common.email"/></label>
           		<input type="text" class="InputTextStyle InputText1Style classCheckChange" id="email" value="<s:property value="staff.email"/>" maxlength="50"/>
           		<label class="LabelStyle Label15Style"><s:text name="catalog_unit_dia_ban"/></label>
	            <div class="BoxSelect BoxSelect2">
	             	<input type="text" id="areaTree" class="InputTextStyle InputText1Style" />
					<input type="hidden" id="areaId" class=""/>
				</div>	
				<div class="Clear"></div>	
				<label class="LabelStyle Label17Style"><s:text name="catalog.customer.province"/></label>
	            <input id="provinceName" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.area.provinceName"/>"/>
	            <label class="LabelStyle Label15Style"><s:text name="catalog.customer.district"/></label>
	            <input id="districtName" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.area.districtName"/>"/>
	            <div class="Clear"></div>
	            <label class="LabelStyle Label17Style"><s:text name="catalog.customer.precinct"/></label>
	            <input id="precinctName" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="staff.area.precinctName"/>"/>
	            <label class="LabelStyle Label15Style"><s:text name="catalog_unit_so_nha_duong"/></label>
	            <input type="text" class="InputTextStyle InputText1Style classCheckChange" id="houseNumber" value="<s:property value="staff.housenumber"/>" maxlength="50"/>
	            <%-- <label class="LabelStyle Label17Style">Đường</label>
	            <input type="text" class="InputTextStyle InputText1Style classCheckChange" id="street" value="<s:property value="staff.street"/>" maxlength="50"/> --%>
	            <div class="Clear"></div>			
			</div>
			<!-- END THÔNG TIN LIÊN LẠC-->
			<div class="Clear"></div>
			<!-- BEGIN THÔNG TIN MỞ RỘNG-->
            <h2 class="Title2Style"><s:text name="catalog_unit_thong_tin_mo_rong"/></h2>
			<div class="SearchInSection SProduct1Form" id="propertyTabContainer">
				<tiles:insertTemplate template="/WEB-INF/jsp/catalog/attributes-staff/staffAttCommon.jsp"></tiles:insertTemplate>						
			</div>
			<!-- END THÔNG TIN MỞ RỘNG-->
            <div class="Clear"></div>
            <div class="BtnCenterSection">
            	<button class="BtnGeneralStyle BtnMSection" onclick="return UnitTreeCatalog.showStaffSearchScreenBack();"><s:text name="jsp.common.quay.lai"/></button>
                <s:if test="staffId != null ">
                	<button id="btnUpdateStaff" class="BtnGeneralStyle cmsiscontrol" onclick="return UnitTreeCatalog.changeStaffInfo(1,'<s:property value="staffId"/>');"><s:text name="jsp.common.capnhat"/></button>
                </s:if>
                <s:else>
                	<button id="btnUpdateStaff" class="BtnGeneralStyle cmsiscontrol" onclick="return UnitTreeCatalog.changeStaffInfo(0);"><s:text name="jsp.common.capnhat"/></button>
                </s:else>
            </div>
            <div class="Clear"></div>
            <div id="staff"><p class="ErrorMsgStyle" id="errMsgStaff" style="display: none;"></p></div>
            <p class="SuccessMsgStyle" id="successMsgStaff" style="display: none;"></p>
        
        </div>
    </div>

<s:hidden id="nodeShopId"></s:hidden> <!-- id don vi cha -->
<s:hidden id="nodeTypeId"></s:hidden> <!-- id loai -->
<s:hidden id="nodeType"></s:hidden>  <!-- kieu loai: 1: don vi, 2 nhan vien -->
<s:hidden id="orgId"></s:hidden>  <!-- orgId khi them moi -->


<s:hidden id="staffCreateType" name="staffCreateType"></s:hidden>
<s:hidden id="staffId" name="staffId"></s:hidden>
<s:hidden id="statusHidden" name="status"></s:hidden>
<s:hidden id="shopCodeHidden" name="shopCode"></s:hidden>
<s:hidden id="shopNameHidden" name="shopName"></s:hidden>
<s:hidden id="staffGroupId" name="staffGroupId"></s:hidden>
<s:hidden id="staffObjectType" name="staffObjectType"></s:hidden>
<s:hidden id="province" name="province"></s:hidden>
<s:hidden id="district" name="district"></s:hidden>
<s:hidden id="ward" name="ward"></s:hidden>
<s:hidden id="parentStaffId" name="parentStaffId"></s:hidden>
<s:hidden id="saleTypeCode" name="saleTypeCode"></s:hidden>
<s:hidden id="subCatStr" name="subCatStr"></s:hidden>
<script type="text/javascript">	
	Utils.functionAccessFillControl('unitTreeContent', function (data) {
		//Xu ly cac thanh phan lien quan den control phan quyen
	});
	$('#manageSubStaff').combotree({
		url: '',
		lines: true,
		multiple: true,
		formatter: function(node) {
			return Utils.XSSEncode(node.text);
		},
		onLoadSuccess: function(node, data) {
			$('#manageShopStaff').combotree('setValue', data[0].id);
		}
	});
	UnitTreeCatalog._isLoadArea=true;
	setTimeout(function(){UnitTreeCatalog._isChange=false;},1500);
	//UnitTreeCatalog.loadParentStaff(); // load Nhân viên quản lý
	var staffId = $('#staffId').val();
	applyDateTimePicker("#workStartDate");
	applyDateTimePicker("#idDate");
	var status = '<s:property value="status"/>';
	var gender ='<s:property value="staff.gender.value"/>';
	var staffType ='<s:property value="staffType"/>';
	setTimeout(function(){
		$('.MySelectBoxClass').customStyle();		
		$('.CustomStyleSelectBox').css('width','174px');
	}, 200);	
	var staffT = '<s:property value="staff.staffCode"/>';
	if (staffT != ""){
		/** view thong tin staff*/
		$('#staffName').focus();
		var areaIdOfShop = '<s:property value="staff.area.id"/>';
		if (areaIdOfShop != '') {
			UnitTreeCatalog.loadTreeEx(areaIdOfShop,staffId);
		} else {
			UnitTreeCatalog.loadTreeEx(null,staffId);
		}
		$('#staffType').val(staffType);
		$('#staffType').change();
		$('#status').val(status);
		$('#status').change();
		$('#gender').val(gender);
		$('#gender').change();
		$('#staffCode').attr('disabled',true);
		//disabled('staffCode');
		disableSelectbox('staffType');
		if ('<s:property value="staffSpecificType"/>' != '' && '<s:property value="staffSpecificType"/>' != StaffSpecType.STAFF) {
			$('#orderProduct').attr('disabled',true);
			$('#manageSubStaff').attr('disabled',true);
		} else {
			$('#manageSubStaff').combotree({
				url: '/catalog/unit-tree/search-sub-cat-tree',
				lines: true,
				multiple: true,
				onLoadSuccess: function(node, data) {
					var lst = $('#subCatStr').val().split(',');
					$('#manageSubStaff').combotree('setValues', lst);
					$('#manageShopStaff').combotree('setValue', data[0].id);
				}
			});
		}
		var staffObjectType = $('#staffObjectType').val();
		if(staffObjectType != '' && staffObjectType != 1 && staffObjectType != 2 ){
			disableSelectbox('staffType');
		}
		$('#staffShopCode').attr('disabled',true);
		var shopId = '<s:property value="staff.shop.id"/>';
		$('#nodeShopId').val(shopId);
		//$('#staffShopName').attr('disabled',true);
		//$('.CalendarLink').unbind('click');			
		//$('#title').html('Thông tin nhân viên');
		$('#orderProduct').val($('#saleTypeCode').val());
	}else{
		/** them moi nhan vien*/
		$('#staffCode').focus();
		UnitTreeCatalog.loadTreeEx(null,staffId);
		disableSelectbox('staffType');
		disableSelectbox('status');
		if (staffType != '' && staffType != StaffSpecType.STAFF || $('#staffType').val() != '' && $('#staffType').val() != StaffSpecType.STAFF) {
			$('#orderProduct').attr('disabled',true);
			$('#manageSubStaff').attr('disabled',true);
		} else {
			$('#manageSubStaff').combotree({
				url: '/catalog/unit-tree/search-sub-cat-tree',
				lines: true,
				multiple: true,
				onLoadSuccess: function(node, data) {
					$('#manageShopStaff').combotree('setValue', data[0].id);
				}
			});
		}
	}
	/* Utils.bindFormatOnTextfield('staffPhone', Utils._TF_NUMBER);
	Utils.bindFormatOnTextfield('staffTelephone', Utils._TF_NUMBER); */
	
	$('#provinceCode').bind('blur', function(event){
		var provinceCode = $('#provinceCode').val();
		if(provinceCode == "" || provinceCode == null || provinceCode == undefined){
			$('#districtCode').val("");
			$('#wardCode').val("");
		}
	});	
	UnitTreeCatalog._isChange=false;
	$('.classCheckChange').change(function(){UnitTreeCatalog._isChange=true;});
	
	Utils.bindAutoButtonEx('.Content3Section','btnUpdateStaff');
	/** Begin ham cho thuoc tinh dong nhan vien*/
	function format_number(dec, fix) {
		fixValue = parseFloat(Math.pow(10, fix));
		retValue = parseInt(Math.round(dec * fixValue)) / fixValue;
		return retValue;
	}

	function decimalBox(event, objectId, fal) {
		if (objectId == undefined) {
			return false;
		}
		var key;
		if (window.event) {
			key = window.event.keyCode;
		}else if (event) {
			key = event.which;
		}else {
			return true;
		}
		if (key==null || key==0 || key==8 ||  key==9 || key==13 || key==27) {
			return true;
		} else {
			var key1 = String.fromCharCode(key);
			var s1 = $('#' + objectId).val();
			s1 = s1.replace(/,/g, '');
			var p = s1.indexOf('.');
			if (p > -1 && key1 == '.') {
				return false;
			}
			if(fal != null && fal != undefined){
				var ponterPosition = Number($('#' + objectId)[0].selectionStart);
				var al = s1.indexOf('-');
				if((key1 == '-' && al >0) || (ponterPosition>0 && key1=='-') || al >=0){
					return false;
				}
				return /[0-9.-]$/.test(key1);
			}else{
				return /[0-9.]$/.test(key1);
			}
		}
	}
	
	function formatThousandNumber(objId) {
		var val = $('#' + objId).val().trim();
		if (val == '') {
			return false;
		}
		val = val.replace(/,/g, '');
		val = formatCurrency(val);
		$('#' + objId).val(val);
	}
	/** End ham cho thuoc tinh dong nhan vien*/
</script>

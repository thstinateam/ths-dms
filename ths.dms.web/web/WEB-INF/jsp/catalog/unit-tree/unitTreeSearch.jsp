<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="k" uri="/kryptone"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
	a#downloadTemplate:hover, a#downloadTemplate2:hover {
		cursor: pointer;
	}

.BtnGeneralStyle {
    background: url("../images/bg_input.jpg") repeat-x scroll left top rgba(0, 0, 0, 0);
    border: 1px solid #0674b9;
    padding: 1px 9px;
}
 .BtnGeneralStyle {
    background: url("../images/bg_input1.jpg") repeat-x scroll left top rgba(0, 0, 0, 0) !important;
    border: 1px solid #d1d0d0 !important;
    color: #333;
    padding: 5px 10px !important;
}
.BtnGeneralStyle {
    cursor: pointer;
    font-size: 1em;
    font-weight: bold;
    overflow: visible;
}
</style>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="TabSection">
	<ul class="ResetList TabSectionList">
		<li><a href="javascript:void(0)" id="tabActive1" class="Sprite1 Active" onclick="UnitTreeCatalog.showTab1();"><span class="Sprite1"><s:text name="unit_tree.search_unit.tab_name" /></a></li>
		<li><a href="javascript:void(0)" id="tabActive2" class="Sprite1" onclick="UnitTreeCatalog.showTab2();"><span class="Sprite1"><s:text name="unit_tree.search_staff.tab_name" /></span></a></li>
	</ul>
	<div class="Clear"></div>
</div>
<div id="container1" >
	<div class="ReportCtnSection" >
		<h2 class="Title2Style"><s:text name="unit_tree.search_unit.search_header" /></h2>
		<%--
		<div class="GeneralForm SProduct1Form">
			<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.manage_shop" /></label>
			<input type="text" id="manageShop" style="width: 196px;" class="InputTextStyle" />			
			<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.shop_code" /></label>
			<input id="shopCode" type="text" class="InputTextStyle" maxlength="40" style="width: 196px;"/>
			<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.shop_name" /></label>
			<input id="shopName" type="text" class="InputTextStyle" maxlength="100"/>
			<div class="Clear"></div>
			<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.shop_type" /></label>
			<div class="BoxSelect BoxSelect2">
				<select class="MySelectBoxClass" id="unitType">
					<option value="-1"><s:text name="jsp.common.status.all" /></option>
					<s:if test="lstOrgUnitType != null">
						<s:iterator value="lstOrgUnitType" var="orgUnitType">
							<option value="<s:property value="#orgUnitType.name"/>"><s:property value="#orgUnitType.name"/></option>
						</s:iterator>
					</s:if>
				</select>
			</div>
			<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.shop_status" /></label>
			<div class="BoxSelect BoxSelect2">
				<select id="shopStatus" class="MySelectBoxClass">
					<option value="ALL"><s:text name="jsp.common.status.all" /></option>
					<option value="RUNNING" selected="selected"><s:text name="action.status.name" /></option>
					<option value="STOPPED"><s:text name="pause.status.name" /></option>
				</select>
			</div>
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle BtnSearch" onclick="UnitTreeCatalog.searchShop();"><s:text name="unit_tree.search_unit.search_button_text" /></button>
			</div>
			<div class="Clear"></div>
			<p id="errMsgSearchShop" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
		</div>
		--%>
		<div class="ModuleList3Form" id="divFormSearchShop" style="margin-top: 10px; margin-bottom: 10px;">
			<dl class="Dl3Style">
				<dt style="margin-left: 15px;">
					<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.manage_shop" /></label>
				</dt>
				<dd>
					<input type="text" id="manageShop" style="width: 204px;" class="InputTextStyle" />
				</dd>
				<dt style="margin-left: 15px;">
					<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.shop_code" /></label>
				</dt>
				<dd>
					<input id="shopCode" type="text" class="InputTextStyle" maxlength="40" style="width: 196px;"/>
				</dd>
				<dt style="margin-left: 15px;">
					<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.shop_name" /></label>
				</dt>
				<dd>
					<input id="shopName" type="text" class="InputTextStyle" maxlength="250"/>
				</dd>
				<div class="Clear"></div>
				
				<dt style="margin-left: 31px;">
					<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.shop_type" /></label>
				</dt>
				<dd>
					<div class="BoxSelect BoxSelect2" id="divSelectBoxUnitType">
						<select class="MySelectBoxClass" id="unitType">
							<option value="-1"><s:text name="jsp.common.status.all" /></option>
							<s:if test="lstOrgUnitType != null">
								<s:iterator value="lstOrgUnitType" var="orgUnitType">
									<option value="<s:property value="#orgUnitType.name"/>"><s:property value="#orgUnitType.name"/></option>
								</s:iterator>
							</s:if>
						</select>
					</div>
				</dd>
				<dt style="margin-left: 2px;">
					<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.shop_status" /></label>
				</dt>
				<dd>
					<div class="BoxSelect BoxSelect2" id="divSelectBoxShopStatus">
						<select id="shopStatus" class="MySelectBoxClass">
							<option value="ALL"><s:text name="jsp.common.status.all" /></option>
							<option value="RUNNING" selected="selected"><s:text name="action.status.name" /></option>
							<option value="STOPPED"><s:text name="pause.status.name" /></option>
						</select>
					</div>
				</dd>
			</dl>
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnSearchShop" class="BtnGeneralStyle BtnSearch" onclick="UnitTreeCatalog.searchShop();"><s:text name="unit_tree.search_unit.search_button_text" /></button>
			</div>
		</div>
		<div class="Clear"></div>
		<p id="errMsgSearchShop" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
		<div class="Clear"></div>
		<h2 class="Title2Style"><s:text name="unit_tree.search_unit.search_result_header" /></h2>
		<div class="GridSection" id="searchShopUnitTreeContainerGrid">
			<table id="searchShopUnitTreeGrid"></table>
		</div>
		<div class="Clear"></div>
		<!-- 	<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p> -->
			<div class="Clear"></div>
			 <div class="GeneralForm GeneralNoTP1Form" id="hideSupport">
				 <div class="Func1Section cmsiscontrol" id="group_import_shop"> 
					<p class="DownloadSFileStyle DownloadSFile2Style">
						<a id="downloadTemplate"  onclick="UnitTreeCatalog.downloadImportShopTemplateFile();" class="Sprite1"><s:text name="unit_tree.search_unit.search_grid.import.download_file"/></a>
					</p>
					<!-- href="javascript:void(0)" -->					
					<div class="DivInputFile">
							<form action="/catalog/unit-tree/importexcelShopfile" name="importShopFrm" id="importShopFrm"  method="post" enctype="multipart/form-data">
								<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>									
								<input type="hidden" id="isViewShop" name="isView"/>
                 				<input type="file" class="InputFileStyle" size="10" name="excelFile" id="excelFileShop" style="width: 100px !important;" onchange="previewImportExcelFile(this,'importFrm', 'fakefilepc_shop');">
					            <div class="FakeInputFile">
									<input id="fakefilepc_shop" style="margin: -3px 5px 0 0;" readonly="readonly" type="text" class="InputTextStyle InputText1Style">
								</div>
							</form>
					</div>
					<button id="btnImport" class="BtnGeneralStyle" onclick="return UnitTreeCatalog.uploadShop();"><s:text name="unit_tree.search_unit.search_grid.import_button_text"/></button>
				</div>
				<button id="btnExport" class="BtnGeneralStyle" onclick="return UnitTreeCatalog.exportShop();"><s:text name="unit_tree.search_unit.search_grid.export_button_text"/></button>
			<div class="Clear"></div>
			<p id="errExcelMsgShop" class="ErrorMsgStyle" style="display: none"></p>
			<p id="successMsgImportShop" class="SuccessMsgStyle" style="display:none;"></p>
			</div> 
	</div>
</div>

<div id="container2" style="display:none;">
	<div class="ReportCtnSection">
		<h2 class="Title2Style"><s:text name="unit_tree.search_unit.search_header" /></h2>
		<div class="ModuleList3Form" id="divFormSearchStaff" style="margin-top: 10px; margin-bottom: 10px;">
			<dl class="Dl3Style">
				<dt style="margin-left: 15px;">
					<label class="LabelStyle Label1Style" style="width: 80px;"><s:text name="unit_tree.search_staff.unit" /></label>
				</dt>
				<dd>
					<input type="text" id="manageShopStaff" style="width: 204px;" class="InputTextStyle" />
				</dd>
				<dt style="margin-left: 15px;">
					<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_staff.staff_code" /></label>
				</dt>
				<dd>
					<input id="staffCode" type="text" class="InputTextStyle" maxlength="40" style="width: 196px;"/>
				</dd>
				<dt style="margin-left: 15px;">
					<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_staff.staff_name" /></label>
				</dt>
				<dd>
					<input id="staffName" type="text" class="InputTextStyle" maxlength="250"/>
				</dd>
				<div class="Clear"></div>
				
				<dt style="margin-left: 15px;">
					<label class="LabelStyle Label1Style" style="width: 80px;"><s:text name="unit_tree.search_staff.staff_type" /></label>
				</dt>
				<dd>
					<div class="BoxSelect BoxSelect2" id="divSelectBoxStaffType">
						<select class="MySelectBoxClass" id="staffType">
							<option value="-1"><s:text name="jsp.common.status.all" /></option>
							<s:if test="lstOrgStaffType != null">
								<s:iterator value="lstOrgStaffType" var="orgStaffType">
									<option value="<s:property value="#orgStaffType.name"/>"><s:property value="#orgStaffType.name"/></option>
								</s:iterator>
							</s:if>
						</select>
					</div>
				</dd>
				<dt style="margin-left: 20px;">
					<label class="LabelStyle Label1Style"><s:text name="unit_tree.search_unit.shop_status" /></label>
				</dt>
				<dd>
					<div class="BoxSelect BoxSelect2" id="divSelectBoxStaffStatus">
						<select id="staffStatus" class="MySelectBoxClass">
							<option value="ALL"><s:text name="jsp.common.status.all" /></option>
							<option value="RUNNING" selected="selected"><s:text name="action.status.name" /></option>
							<option value="STOPPED"><s:text name="pause.status.name" /></option>
						</select>
					</div>
				</dd>
			</dl>
			<div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnSearchStaff" class="BtnGeneralStyle BtnSearch" onclick="UnitTreeCatalog.searchStaff();"><s:text name="unit_tree.search_unit.search_button_text" /></button>
			</div>
		</div>
		<h2 class="Title2Style">Danh sách nhân viên</h2>
		<div class="GridSection" id="searchStaffUnitContainerGrid">
			 <table id="searchStaffUnitGrid"></table>
		</div>
		<div class="Clear"></div>
			<p id="errExcelMsgStaff" class="ErrorMsgStyle" style="display: none"></p>
			<p id="errMsgStaff" class="ErrorMsgStyle" style="display: none"></p>
			<p id="successMsgImportStaff" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear"></div>
			<div class="GeneralForm GeneralNoTP1Form" id="hideSupport">
				<div class="Func1Section cmsiscontrol" id="group_import_staff"> 
					<div class="BoxSelect BoxSelect2">
				        <select class="MySelectBoxClass" name="excelType" id="excelType" onchange="UnitTreeCatalog.excelTypeChanged();" autocomplete="off">
				            <option value="1">Nhân viên</option>
				            <option value="2">Ngành hàng con</option>				         
				        </select>
					</div>
					<p class="DownloadSFileStyle DownloadSFile2Style">
						<a id="downloadTemplate2"  onclick="UnitTreeCatalog.downloadImportStaffTemplateFile();" class="Sprite1"><s:text name="unit_tree.search_unit.search_grid.import.download_file"/></a>
					</p>
					<div class="DivInputFile">
							<form action="/catalog/unit-tree/importexcelfile" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">									
								<input type="hidden" id="isViewStaff" name="isView" value="0"/>
                 				<input type="file" class="InputFileStyle" size="10" name="excelFile" id="excelFile" style="width: 100px !important;" onchange="previewImportExcelFile(this,'importFrm', 'fakefilepc_staff');">
					            <div class="FakeInputFile">
									<input id="fakefilepc_staff" style="margin: -3px 5px 0 0;" readonly="readonly" type="text" class="InputTextStyle InputText1Style">
								</div>
							</form>
							<div class="Clear"></div>
					</div>
			    	<button id="btnImport" class="BtnGeneralStyle" onclick="return UnitTreeCatalog.openImportExcelType();"><s:text name="unit_tree.search_unit.search_grid.import_button_text"/></button>
				</div>
					<button id="btnExport" class="BtnGeneralStyle" onclick="return UnitTreeCatalog.exportExcel();"><s:text name="unit_tree.search_unit.search_grid.export_button_text"/></button>
				<div class="Clear"></div>
				<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none; margin: 5px 0; padding: 2px 0;"></p>
				<div id="responseDiv" style="display: none"></div>
				<div class="Clear"></div>
			</div>
	</div>
</div>
<tiles:insertTemplate template="/WEB-INF/jsp/program/key-shop/keyShopPopup.jsp" />
<script type="text/javascript">
	$('#divSelectBoxUnitType, #divSelectBoxShopStatus').bind('keyup',function(event){
		if (event.keyCode == keyCodes.ENTER) {
			$('#btnSearchShop').click();
		}
	});
	$('#divFormSearchShop input').bind('keyup', function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$('#btnSearchShop').click();
		}
	});
	$('#divSelectBoxStaffType, #divSelectBoxStaffStatus').bind('keyup',function(event){
		if (event.keyCode == keyCodes.ENTER) {
			$('#btnSearchStaff').click();
		}
	});
	$('#divFormSearchStaff input').bind('keyup', function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$('#btnSearchStaff').click();
		}
	});
	
	Utils.functionAccessFillControl('unitTreeContent', function (data) {
		//Xu ly cac thanh phan lien quan den control phan quyen
	});
	$('.MySelectBoxClass').customStyle();
	Utils.bindAutoSearchEx('.ModuleList3Form');
	UnitTreeCatalog.initImport();
	/*var params = new Object();
	if(!$('#cbStop').is(':checked')) {
		params.status = 1;
	};*/
	var firstLoadPage = true;
	$('#searchShopUnitTreeGrid').datagrid({
		url :'/catalog/unit-tree/search-shop',
		checkOnSelect : false,
		//queryParams:params,
        pagination : true,
        rownumbers : true,
        singleSelect  :true,
        pageNumber : 1,
        scrollbarSize: 0,
        autoWidth: true,
        autoRowHeight : true,
        fitColumns : true,
		width : ($('#searchShopUnitTreeContainerGrid').width()),
		columns:[[  
        	{field: 'shopCode',title:'<s:text name="unit_tree.search_unit.shop_code" />', width:150,align:'left', sortable : true, resizable : true, formatter: function(value, row, index){
        		return Utils.XSSEncode(value);
        	}},  
        	{field: 'shopName',title:'<s:text name="unit_tree.search_unit.shop_name" />', width:200,align:'left',sortable : true, resizable : true, formatter: function(value, row, index){
        		return Utils.XSSEncode(value);
        	}},
        	{field: 'shopType',title:'<s:text name="unit_tree.search_unit.search_grid.unit_type" />', width:100,align:'left',sortable : true, resizable : true, formatter:function(value){
                if (value != null) {
                	return Utils.XSSEncode(value);
                } else {
                	return '';
    			} 
        	}},
        	{field: 'parentShopName',title:'<s:text name="unit_tree.search_unit.search_grid.parent_shop_name" />', width:200 ,align:'left', sortable : true, resizable : true, formatter: function(value,rowData){
        			return Utils.XSSEncode(value);
        		}
        	},
        	{field: 'status',title:'<s:text name="unit_tree.search_unit.shop_status" />', width:110,align:'left', sortable : true, resizable : true, 
        		formatter: function(value, row, index) {
        			if (value === activeType.RUNNING) {
        				return '<s:text name="unit_tree.search_unit.search_grid.status.active" />';
        			} else if (value === activeType.STOPPED) {
        				return '<s:text name="unit_tree.search_unit.search_grid.status.stopped" />';
        			}
        		}
        	},
        	{field: 'custom',width:50,align:'center', formatter : function(value, rowData, index) {
        		var cellContent = '<a href="javascript:void(0)" title="<s:text name="unit_tree.search_unit.search_grid.options.view.unit.on_tree" />" onclick="UnitTreeCatalog.showCollapseTab(-1,'+rowData.shopId+')"><img src="/resources/images/icon-view.png" width="15" height="16"></a>';
        		cellContent += '<a href="javascript:void(0)" style="margin-left: 10px;" title="<s:text name="unit_tree.search_unit.search_grid.options.update.unit" />" onclick="UnitTreeCatalog.showShopInfoScreen(' + rowData.parentId + ', ' + rowData.shopId + ', ' + rowData.shopTypeId +  ');"><img src="/resources/images/icon-edit.png" width="15" height="16"></a>';
        		
        		return cellContent;
        	}},
        	{field: 'P',hidden: true},
        ]],
        onBeforeLoad: function(param) {
        	if (!firstLoadPage) {        		
        		if (param) {
        			var searchParamPrefix = 'unitFilter';
        			if (param.hasOwnProperty('sort')) {
        				param[searchParamPrefix + '.sortField'] = param.sort;
        				delete param.sort;
        			}
        			if (param.hasOwnProperty('order')) {
        				param[searchParamPrefix + '.sortOrder'] = param.order;
        				delete param.order;
        			}
        		}
        	} else {
        		firstLoadPage = false;
        		return false;
        	}
        	return true;
        },
        onLoadSuccess :function(){
	    	 $('#searchShopUnitTreeContainerGrid .datagrid-header-rownumber').html('<s:text name="unit_tree.search_unit.search_grid.no" />');
	    	 updateRownumWidthForDataGrid('#searchShopUnitTreeContainerGrid');
	    	 var tm = setTimeout(function(){
	    		 UnitTreeCatalog.resize();
	    		 clearTimeout(tm);
			 }, 150);
    	}
	});

	$('#manageShop').combotree({
		url: '/catalog/unit-tree/search-unit-tree',
		lines: true,
		formatter: function(node) {
			return Utils.XSSEncode(node.text);
		},
		onLoadSuccess: function(node, data) {
			$('#manageShop').combotree('setValue', data[0].id);
			UnitTreeCatalog.searchShop();
		}
	});

	$('#manageShopStaff').combotree({
		url: '/catalog/unit-tree/search-unit-tree',
		lines: true,
		formatter: function(node) {
			return Utils.XSSEncode(node.text);
		},
		onLoadSuccess: function(node, data) {
			$('#manageShopStaff').combotree('setValue', data[0].id);
		}
	});

	$('#tabActive2').click(function () {
		var firstLoadStaffPage = true;
		//grid search staff
		$('#searchStaffUnitGrid').datagrid({
			url: "/catalog/unit-tree/search-staff-group",
			autoRowHeight: true,
			rownumbers: true, 
			checkOnSelect: true,
			pagination: true,
			singleSelect: true,
			rowNum: 10,
			scrollbarSize: 0,
			autoWidth: true,
			//queryParams:params,
	        fitColumns: true,
	        width: ($('#searchStaffUnitContainerGrid').width()),
		    columns: [[
		        {field: 'shopName', title: '<s:text name="unit_tree.search_staff.unit" />', width: 160, sortable: true, resizable: false, align: 'left',
		        	formatter: function(v, r, i) {
		        		if (r && r.hasOwnProperty('shopName')) {
		        			return Utils.XSSEncode(r.shopName);
		        		}
		        		return '';
        			}
		        },
			    {field: 'staffCode', title: '<s:text name="unit_tree.search_staff.staff_code" />', width: 120, sortable: true, resizable: false, align: 'left', 
		        	formatter: function(v, r, i) {
	                	if (r && r.hasOwnProperty('staffCode')) {
	                		return Utils.XSSEncode(r.staffCode);
	                	}
        			}
		        },
			    {field: 'staffName', title: '<s:text name="unit_tree.search_staff.staff_name" />', width: 160, sortable: true, resizable: false, align: 'left', 
			    	formatter: function(v, r) {
	                	if (r && r.hasOwnProperty('staffName')) {
	                		return Utils.XSSEncode(r.staffName);
	                	}
        			}
		        },
			    {field: 'phoneMobile', title: '<s:text name="unit_tree.search_staff.phone_mobile" />', width: 100, align: 'left', sortable: false, resizable: false,
		        	formatter: function(v, r) {
	                	if (r && r.hasOwnProperty('phone') && r.hasOwnProperty('mobilePhone')) {
	                		return isNullOrEmpty(r.phone) ? Utils.XSSEncode(r.mobilePhone) : Utils.XSSEncode(r.phone) + '<br>' + Utils.XSSEncode(r.mobilePhone);
	                	}
        			} 
		        },
			    {field: 'staffType', title: '<s:text name="unit_tree.search_staff.staff_type.short" />', width: 110, align: 'left', sortable: true, resizable: false,
		        	formatter: function(v, r, index) {
				    	if (r && r.hasOwnProperty('staffType')) {
	                		return Utils.XSSEncode(r.staffType);
	                	}
			    	}
			    },
			    {field: 'status', title: '<s:text name="unit_tree.search_unit.shop_status" />', width: 80, align: 'left', sortable: true, resizable : true, 
			    	formatter: function(value, row, index) {
			    		if (value === activeType.RUNNING) {
			    			return '<s:text name="unit_tree.search_unit.search_grid.status.active" />';
			    		} else if (value === activeType.STOPPED) {
			    			return '<s:text name="unit_tree.search_unit.search_grid.status.stopped" />';
			    		}
			    	}
			    },
			    {field: 'edit', width: 50, align: 'center', sortable: false, resizable: false, 
			    	formatter: function(val, row, index) {
			    		var cellContent = '<a href="javascript:void(0)" title="<s:text name="unit_tree.search_staff.search_grid.options.view.unit.on_tree" />" onclick="UnitTreeCatalog.showCollapseTab(-1,'+row.shopId+',null,null,null,null,'+row.staffId+')"><img src="/resources/images/icon-view.png" width="15" height="16"></a>';
			    		cellContent += '<a href="javascript:void(0)" style="margin-left: 10px;" title="<s:text name="unit_tree.search_unit.search_grid.options.update.unit" />" onclick="UnitTreeCatalog.showStaffEditScreen(' + row.staffId + ', ' + row.shopId +  ');"><img src="/resources/images/icon-edit.png" width="15" height="16"></a>';
			    		return cellContent;
			    	}
			    },
			    {field: 'P', hidden: true}
		    ]],
		    onBeforeLoad: function(param) {
		    	if (!firstLoadStaffPage) {        		
		    		if (param) {
		    			var searchParamPrefix = 'unitFilter';
		    			if (param.hasOwnProperty('sort')) {
		    				param[searchParamPrefix + '.sortField'] = param.sort;
		    				delete param.sort;
		    			}
		    			if (param.hasOwnProperty('order')) {
		    				param[searchParamPrefix + '.sortOrder'] = param.order;
		    				delete param.order;
		    			}
		    		}
		    	} else {
		    		firstLoadStaffPage = false;
		    		return false;
		    	}
		    	return true;
		    },
		    onLoadSuccess :function(data){	    	
		    	$('#searchStaffUnitGrid .datagrid-header-rownumber').html('STT');	 
		    	updateRownumWidthForDataGrid('#searchStaffUnitContainerGrid');
		    	 var tm = setTimeout(function(){
		    		 UnitTreeCatalog.resize();
		    		 clearTimeout(tm);
				 }, 150);
		    },
			method : 'POST'
	 	});
		setTimeout(function() {
			UnitTreeCatalog.searchStaff();
		}, 500);
    });
</script>
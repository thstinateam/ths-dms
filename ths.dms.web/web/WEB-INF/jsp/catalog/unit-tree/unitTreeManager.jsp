<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="k" uri="/kryptone" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="ths.dms.helper.Configuration"%>
<script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/common/vnm.mapapi.js")%>"></script>
<div class="Breadcrumbs">
    <span class="Breadcrumbs1Item Sprite1"><a href="/catalog/unit-tree"><s:text name="catalog_unit_thong_tin_nhan_vien"/></a></span>
</div>
<div class="MainInContent">
	<div class="MainInTopContent">
	   	<div class="MainInBtmContent">
           <div class="FirstSidebar">
	           	<div class="TreeViewSection">
	                   <h3 class="Sprite2"><span class="Sprite2"><s:text name="catalog_unit_tree_title"/></span></h3>
	         	<div class="GeneralMilkInBox">   
	         		<div id="treeContainer" class="ScrollSection">
		         		<div id="tree"></div>                         
		         	</div>            
		         </div>
		     </div>
		 </div>
	 <div class="Content">
	     <div class="GeneralMilkBox">
	         <div class="GeneralMilkTopBox">
	         		<div class="TabSection" id="tab" style="display: none;">
							<ul class="ResetList TabList">
								<li class="Tab1Item"><a id="infoTab" href="javascript:void(0);" onclick="UnitTreeCatalog.searchInfo();" class="Sprite2 Active"><span class="Sprite2" id="infoTabTitle">Thông tin tìm kiếm</span> </a></li>
								<li class="Tab2Item"><a id="changeInfoTab" href="javascript:void(0);" onclick="UnitTreeCatalog.loadChangeInfo();" class="Sprite2"><span class="Sprite2">Xem thông tin thay đổi</span> </a></li>
							</ul>
							<div class="Clear"></div>
					</div>
					
	             <div class="GeneralMilkBtmBox" id="productInfo">
 	                 <h3 class="Sprite2"><span class="Sprite2" id="title">Thông tin tìm kiếm</span></h3>
	                 <div class="GeneralMilkInBox ResearchSection">
	                 <div class="ModuleList1Form">
	                     <label class="LabelStyle Label1Style">Mã đơn vị</label>
                         <div class="Field2">
	                     	<input id="unitCode" type="text" class="InputTextStyle InputText1Style" maxlength="20"/>
	                     	<span class="RequireStyle">(*)</span>
	                     </div>
                         <label class="LabelStyle Label2Style">Mã cha</label>
                         <div class="BoxSelect BoxSelect2">
	                         <div class="Field2">
				           		<input id="parentCode" style="width:200px;">
				           	 <span class="RequireStyle">(*)</span>
		                     </div>
	                     </div>
                         <div class="Clear"></div>
                         <label class="LabelStyle Label1Style">Loại đơn vị</label>
                         <div class="BoxSelect BoxSelect1">
	                        <div class="Field2">
		                        <s:select id="unitType" list="lstUnitType" listKey="id" listValue="channelTypeName" value="%{unitType}"
									headerKey="-1" headerValue="---Chọn loại đơn vị---" cssClass="MySelectBoxClass">
							    </s:select>
							    <%-- <select class="MySelectBoxClass" id="unitType" name="" style="opacity: 0; height: 20px;">
								    <option value="-1" selected="selected">---Chọn loại đơn vị---</option>
								    <option value="8">VNM</option>
								    <option value="9">MIEN</option>
								    <option value="10">VUNG</option>
								    <option value="11">NPP</option>
								</select> --%>
						   		<span class="RequireStyle">(*)</span>
                     		</div>
						 </div>
                         <label class="LabelStyle Label2Style">Tên đơn vị</label>
                         <div class="Field2">
		                     <input id="unitName" type="text" class="InputTextStyle InputText2Style" maxlength="100"/>
		                     <span class="RequireStyle">(*)</span>
	                     </div>
                         <div class="Clear"></div>
                         <label class="LabelStyle Label1Style">Trạng thái</label>
                         <div class="BoxSelect BoxSelect1">
	                         <div class="Field2">
	                             <select id="status" class="MySelectBoxClass">
	                                 <option value="-2">---Chọn trạng thái---</option>
	                                 <option value="0">Tạm ngưng</option>
	                                 <option value="1" selected="selected">Hoạt động</option>
	                             </select>
	                             <span class="RequireStyle">(*)</span>
		                     </div>
                         </div>
                         <label class="LabelStyle Label2Style">Số TK</label>
                         <input id="accountNumber" type="text" class="InputTextStyle InputText2Style" maxlength="50" onkeypress="return numbersonly(event,false);"/>
                         <div class="Clear"></div>
                         <label class="LabelStyle Label1Style">Phone</label>
                         <input id="telephoneNumber" type="text" class="InputTextStyle InputText1Style" maxlength="40" onkeypress="return numbersonly(event,false);"/>
                         <label class="LabelStyle Label2Style">Mobile</label>
                         <input id="mobileNumber" type="text" class="InputTextStyle InputText2Style" maxlength="40" onkeypress="return numbersonly(event,false);"/>
                         <div class="Clear"></div>
                         <label class="LabelStyle Label1Style">Email</label>
                         <input id="email" type="text" class="InputTextStyle InputText1Style" maxlength="40"/>
                         <label class="LabelStyle Label2Style">MST</label>
                         <input id="taxNumber" type="text" class="InputTextStyle InputText2Style" maxlength="40" onkeypress="return numbersonly(event,false);"/>
                         <div class="Clear"></div>
                         <div class="divBankHidden" style="display: none;">
	                         <label class="LabelStyle Label1Style">Tên người liên hệ</label>
	                         <input id="contactName" type="text" class="InputTextStyle InputText1Style" maxlength="40"/>
	                         <label class="LabelStyle Label2Style">Tên NH</label>
	                         <input id="bankName" type="text" class="InputTextStyle InputText2Style" maxlength="250"/>
                         </div>
                         <div class="Clear"></div>
                         <div class="divBankHidden" style="display: none;">
	                         <label class="LabelStyle Label1Style">Địa chỉ giao HĐ</label>
	                         <input id="addressBillTo" type="text" class="InputTextStyle InputText1Style" maxlength="500"/>
	                         <label class="LabelStyle Label2Style">Địa chỉ giao hàng</label>
	                         <input id="addressShipTo" type="text" class="InputTextStyle InputText2Style" maxlength="500"/>
	                         <div class="Clear"></div>
	                         <label class="LabelStyle Label1Style">Số Fax</label>
	                         <input id="faxNumber" type="text" class="InputTextStyle InputText1Style" maxlength="500"/>
	                         <div class="Clear"></div>
                         </div>                         
                         <div class="divHidden">                         
                         <label class="LabelStyle Label3Style"><input id="isAll" type="checkbox">Tất cả các cấp</label>
                         </div>
                         <div class="Clear"></div>
                         <div id="divLatLng" class="GeneralMilkBox GeneralMilkBox1 divBankHidden" style="display: none;">
		                       <div class="GeneralMilkTopBox">
		                           <div class="GeneralMilkBtmBox">
		                               <h3 class="Sprite2"><span class="Sprite2">Vị trí</span></h3>
		                               <div class="GeneralMilkInBox">
		                                   <div class="GeneralMilkCol1">
		                                   	<label class="LabelStyle Label1Style">Tỉnh (F9)</label>
		                                       <div class="BoxSelect BoxSelect2">
		                                       	<div class="Field2">
		                                           	<input id="provinceCode" class="InputTextStyle InputText1Style" onchange="provinceCodeChanged();"/>
		                                           	<span class="RequireStyle">(*)</span>
		                                           </div>
		                                       </div>
		                                       <div class="Clear"></div>
		                                       <label class="LabelStyle Label1Style">Quận/huyện (F9)</label>
		                                       <div class="BoxSelect BoxSelect2">
		                                       		<div class="Field2">
		                                           		<input id="districtCode" class="InputTextStyle InputText1Style" onchange="districtCodeChanged();" />
		                                           		<span class="RequireStyle">(*)</span>
		                                           	</div>
		                                       </div>
		                                       <div class="Clear"></div>
		                                       <label class="LabelStyle Label1Style">Phường/xã (F9)</label>
		                                       <div class="BoxSelect BoxSelect2">
		                                       		<div class="Field2">
		                                           		<input id="wardCode" class="InputTextStyle InputText1Style"/> 
		                                           		<span class="RequireStyle">(*)</span>
		                                           	</div>
		                                       </div>
		                                       <div class="Clear"></div>
		                                       <label class="LabelStyle Label1Style">Địa chỉ</label>
		                                       <div class="Field2">
		                                       		<s:textfield id="address" name="address" cssClass="InputTextStyle InputText1Style" maxlength="400"></s:textfield>
		                                       		<span class="RequireStyle">(*)</span>
		                                       </div>
		                                       <div class="Clear"></div>
		                                       <label class="LabelStyle Label1Style">Lat</label>
		                                       <s:textfield id="lat" name="lat" cssClass="InputTextStyle InputText1Style"  maxlength="22"></s:textfield>
		                                       <div class="Clear"></div>
		                                       <label class="LabelStyle Label1Style">Lng</label>
		                                       <s:textfield id="lng" name="lng" cssClass="InputTextStyle InputText1Style"  maxlength="22"></s:textfield>
		                                       <div class="Clear"></div>
		                                   </div>
		                                   <div class="GeneralMilkCol2">
		                                   		<div><p class="ShowHideFunc"><a style="font-size: 13px;" href="javascript:void(0)" class="Sprite1 ShowLink" onclick="CustomerCatalog.viewBigMap()">Xem dạng đầy đủ</a></p></div>
		                                   		<div id="mapContainer" style="width:350px; height:194px">			
		                                   		</div>
		                                   </div>
		                                   <div class="Clear"></div>
		                               </div>
		                           </div>
		                       </div>
		                   	</div>
	                     	<div class="ButtonSection">
		                     	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="return UnitTreeCatalog.search();"><span class="Sprite2">Tìm kiếm</span></button>
			                	<button id="btnCreate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return Utils.getChangedForm(true,null, null,null,true);"><span class="Sprite2">Thêm mới</span></button>
			                	<button style="display: none;" id="btnUpdate" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return UnitTreeCatalog.change();"><span class="Sprite2">Cập nhật</span></button>
			                	<button style="display: none;" id="btnDismiss" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return UnitTreeCatalog.resetForm();"><span class="Sprite2">Bỏ qua</span></button>
			                	<img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
			                 </div>
			                 
			                 <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
	                 	</div>
	                 </div>
	                 <div class="Clear"></div>
	             </div>


						<div class="GeneralMilkBtmBox" id="productChangeInfo"
							style="display: none;">
							<div class="GeneralMilkInBox ResearchSection">
								<div class="ModuleList1Form">
									<label class="LabelStyle Label1Style">Mã đơn vị</label>
									<div class="Field2">
										<input id="unitCodeChangeInfo" type="text" class="InputTextStyle InputText1Style" maxlength="20" /> <span class="RequireStyle">(*)</span>
									</div>
									<label class="LabelStyle Label2Style">Tên đơn vị</label>
									<div class="Field2">
										<input id="unitNameChangeInfo" type="text" class="InputTextStyle InputText1Style" maxlength="20" /> <span class="RequireStyle">(*)</span>
									</div>

									<div class="Clear"></div>
									<label class="LabelStyle Label1Style">Từ ngày</label>
									<div class="Field2">
										<input id="startDate" type="text" class="InputTextStyle InputText3Style" />
									</div>
									<label class="LabelStyle Label2Style">Đến ngày</label>
									<div class="Field2">
										<input id="endDate" type="text" class="InputTextStyle InputText3Style" />
									</div>									
									<div class="Clear"></div>
									<div class="ButtonSection">
										<button class="BtnGeneralStyle Sprite2" onclick="return UnitTreeCatalog.searchChangeInfo();"><span class="Sprite2" >Tìm kiếm</span></button>	
									</div>
									<div class="Clear"></div>
									<div id="divShowFirst" >										
										<div class="GeneralMilkInBox">
						                     <div class="ResultSection" id="WdivShowFirst">
							                    <p id="gridNoResult2" style="display: none" class="WarningResultStyle">Không có kết quả</p>
							                    <p class="ShowHideFunc"></p>
							                    <table id="gridDivShowFirst"></table>
												<div id="pagerDivShowFirst"></div>
							                </div>
                     					</div>	
									</div>
									<div id="divShowSecond" style="display: none;">
										<div class="GeneralMilkInBox">
						                     <div class="ResultSection" id="WdivShowSecond">
							                    <p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
							                    <p class="ShowHideFunc"></p>
							                    <table id="gridDivShowSecond"></table>
												<div id="pagerDivShowSecond"></div>
							                </div>
                     					</div>	
									</div>
								</div>
							</div>
							<div class="Clear"></div>
							<div class="ButtonSection">
		                     	<button id="btnExportExecl" class="BtnGeneralStyle Sprite2" style="display: none" onclick="return UnitTreeCatalog.exportActionLog();"><span id="btnExportExecltitle" class="Sprite2">Xuất Excel</span></button>			                	
			                 </div>			                 
						</div>
						<div class="Clear"></div>
						<p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
					</div>
	     </div>
	     <div class="GeneralMilkBox">
	         <div class="GeneralMilkTopBox">
	             <div class="GeneralMilkBtmBox">
	                 <h3 class="Sprite2"><span class="Sprite2">Danh sách đơn vị</span></h3>
	                 <div class="GeneralMilkInBox">
	                     <div class="ResultSection" id="unitGrid">
		                    <p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
		                    <p class="ShowHideFunc"><a id="gridStyle" class="HideLink Sprite1" href="javascript:void(0)" onclick="getGridStyle();">Dạng mở rộng</a></p>
		                    <table id="grid"></table>
							<div id="pager"></div>
		                </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="FuncBtmSection">
	         <div class="DivInputFileSection">
				<p class="ExcelLink">
					<a id="downloadTemplate" href="javascript:void(0)">Tải mẫu file excel</a>
				</p>
				<div class="DivInputFile">
				<form action="/catalog/unit-tree/importexcelfile" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">						
					
			            <input type="file" class="InputFileStyle" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
			            <input type="hidden" id="isView" name="isView">
			            <div class="FakeInputFile">
			                <input id="fakefilepc" type="text" class="InputTextStyle">
			            </div>
			        
	 			</form>
	 			</div>
				<button class="BtnGeneralStyle Sprite2" onclick="return UnitTreeCatalog.viewExcel();"><span class="Sprite2" >Xem file</span></button>	
				<button class="BtnGeneralStyle BtnGeneralMStyle Sprite2" onclick="return UnitTreeCatalog.importExcel();"><span class="Sprite2">Nhập từ file</span></button>	  
				<div class="Clear"></div>
			</div>
		</div>
		<div class="FuncLeftBtmSection">
		   	<button class="BtnGeneralStyle Sprite2" onclick="UnitTreeCatalog.exportExcelData();"><span class="Sprite2">Xuất dữ liệu</span></button>
		</div>
		<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
		<div id="responseDiv" style="display: none"></div>
         </div>
         <div class="Clear"></div>
		</div>
   </div>
</div>    
<div style="display:none" id="viewBigMap">
	<div id="fancyboxBigMap" class="GeneralDialog General2Dialog" style="width:970px;height:575px">
		<div id="bigMapContainer" style="width:970px;height:575px"></div>
		<div class="Clear"></div>
	</div>
</div>
<s:hidden id="unitId" value="0"></s:hidden>
<s:hidden id="shopCodeHidden" name="staff.shop.shopCode"></s:hidden>
<s:hidden id="shopIdHidden" name="staff.shop.id"></s:hidden>
<s:hidden id="parentCodeS" value="-1"></s:hidden>
<input type="hidden" id="showDateId" value="<s:property  value="showDate" />" />
<script type="text/javascript">
$(document).ready(function(){
	// Huynp4
	$('#email').removeAttr('disabled');
	$('#taxNumber').removeAttr('disabled');
	$('#unitCode').removeAttr('disabled');
	$('#mobileNumber').removeAttr('disabled');
	$('#parentCode').removeAttr('disabled');
	$('#unitName').removeAttr('disabled');
	$('#accountNumber').removeAttr('disabled');
	$('#telephoneNumber').removeAttr('disabled');
	$('#contactName').removeAttr('disabled');
	$('#bankName').removeAttr('disabled');
	$('#addressBillTo').removeAttr('disabled');
	$('#faxNumber').removeAttr('disabled');
	$('#provinceCode').removeAttr('disabled');
	$('#districtCode').removeAttr('disabled');
	$('#wardCode').removeAttr('disabled');
	$('#address').removeAttr('disabled');
	$('#lat').removeAttr('disabled');
	$('#lng').removeAttr('disabled');
	
	$('#email').val('');
	$('#mobileNumber').val('');
	$('#unitCode').val('');
	$('#taxNumber').val('');
	$('#parentCode').val('');
	$('#unitName').val('');
	$('#accountNumber').val('');
	$('#telephoneNumber').val('');
	$('#contactName').val('');
	$('#bankName').val('');
	$('#addressBillTo').val('');
	$('#faxNumber').val('');
	$('#provinceCode').val('');
	$('#districtCode').val('');
	$('#wardCode').val('');
	$('#address').val('');
	$('#lat').val('');
	$('#lng').val('');
	ProductLevelCatalog._callBackAfterImport = UnitTreeCatalog.loadTreeEx;
	$('#lat,#lng').bind('change', function(){
		var lat = $('#lat').val().trim();
		var lng = $('#lng').val().trim();
		if(lat.length > 0 && lng.length > 0){
			MapUtil.loadMapResource(function(){
				Vietbando.loadMap('mapContainer',lat,lng,18, function(pt){
					$('#lat').val(pt.latitude);
					$('#lng').val(pt.longitude);
				}, true);	
			});
		}
	});
	$('#treeContainer').jScrollPane();
	//$('#tab').hide();
	applyDateTimePicker("#startDate");
	applyDateTimePicker("#endDate");
	provinceCodeChanged();
	districtCodeChanged();
	Utils.bindFormatOnTextfield('lat', Utils._TF_NUMBER_DOT);
	Utils.bindFormatOnTextfield('lng', Utils._TF_NUMBER_DOT);
	$('#layoutContent').removeClass('Content');
	$('#layoutContent').addClass('MainContent');
	$('.MySelectBoxClass').customStyle();
	$('#unitType').val("VNM");
	$('#unitCode').focus();
	$('.RequireStyle').hide();
	UnitTreeCatalog.loadTreeEx();
	$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_cay_don_vi.xls');
	var options = { 
	 		beforeSubmit: UnitTreeCatalog.beforeImportExcel,   
	 		success:      UnitTreeCatalog.afterImportExcelUpdate,  
	 		type: "POST",
	 		dataType: 'html'   
	 	}; 
	$('#importFrm').ajaxForm(options);
	
	/* $('.combo-text validatebox-text').bind('change',function(){
		UnitTreeCatalog.getListUnitType($(this).combotree('getValue'),'#unitType');
	}); */
	/* $("#grid").jqGrid({
		url:UnitTreeCatalog.getGridUrl('',-1,'',-1,1,'','','','','',true),
		  colModel:[		
		    {name:'shopCode', label: 'Mã đơn vị', width: 80, sortable:false,resizable:true , align: 'left'},
		    {name:'shopName', label: 'Tên đơn vị', sortable:false,resizable:true , align: 'left'},
		    {name:'type.channelTypeName', label: 'Loại đơn vị', width: 80, sortable:false,resizable:false, align: 'left',formatter: UnitTreeFormatter.unitTypeFormatter},
		    {name:'parentShop.shopName', label: 'Tên đơn vị cha', align: 'left',sortable:false,resizable:true },
		    {name:'taxNum', label: 'Mã số thuế', width: 80, align: 'right',sortable:false,resizable:false },
		    {name:'invoiceNumberAccount', label: 'Tài khoản NH', width: 100, align: 'right',sortable:false,resizable:false },
		    {name:'status', label: 'Trạng thái', width: 80, align: 'left',sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter },
		    {name:'add', label: 'Thêm D/V con', width: 80, align: 'center',sortable:false,resizable:false, formatter: UnitTreeFormatter.addCellIconFormatter},
		    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: UnitTreeFormatter.editCellIconFormatter}
		  ],	  
		  pager : '#pager',
		  height: 'auto',	  	  
		  width: ($('#unitGrid').width()),
		  shrinkToFit: false,
		  gridComplete: function(){
			  $('#jqgh_grid_rn').html('STT');
			  $('#treeContainer').css('height',$('.Content').height()-75);
			  $('#treeContainer').jScrollPane();
		  }
		})
		.navGrid('#pager', {edit:false,add:false,del:false, search: false});	 */
	var _url='/rest/catalog/shop-tree-ex/combotree/'+$('#shopCodeHidden').val().trim()+'/0/0/0/0.json';	
	UnitTreeCatalog.generalFunc(_url);
	$('#parentCode').combotree('setValue',-1);
	if(UnitTreeCatalog._urlLoadGrid == null) {
		UnitTreeCatalog._urlLoadGrid = UnitTreeCatalog.getGridUrl($('#unitCode').val().trim(),$('#parentCodeS').val().trim(),$('#unitName').val().trim(),$('#unitType').val().trim(),$('#status').val().trim(),$('#taxNumber').val().trim(),$('#accountNumber').val().trim(),$('#telephoneNumber').val().trim(),$('#mobileNumber').val().trim(),$('#email').val().trim(),$('#isAll').is(':checked'));
	}
	getGridStyle();
});

function getGridStyle(){
	$("#grid").jqGrid('GridUnload');
	if($('#gridStyle').hasClass('HideLink')){
		$('#gridStyle').removeClass('HideLink');
		$('#gridStyle').addClass('ShowLink');
		$('#gridStyle').text('Dạng mở rộng');
		$("#grid").jqGrid({
			  url:UnitTreeCatalog._urlLoadGrid,			
			  colModel:[		
			    {name:'shopCode', label: 'Mã đơn vị', width: 80, sortable:false,resizable:true , align: 'left'},
			    {name:'shopName', label: 'Tên đơn vị', sortable:false,resizable:true , align: 'left'},
			    {name:'type.channelTypeName', label: 'Loại đơn vị', width: 80, sortable:false,resizable:false, align: 'left',formatter: UnitTreeFormatter.unitTypeFormatter},
			    {name:'parentShop.shopName', label: 'Tên đơn vị cha', align: 'left',sortable:false,resizable:true },
			    {name:'status', label: 'Trạng thái', width: 80, align: 'left',sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter },
			    {name:'add', label: 'Thêm D/V con', width: 80, align: 'center',sortable:false,resizable:false, formatter: UnitTreeFormatter.addCellIconFormatter},
			    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: UnitTreeFormatter.editCellIconFormatter}
			  ],	  
			  pager : '#pager',
			  height: 'auto',	  	  
			  width: ($('#unitGrid').width()),			  
			  gridComplete: function(){
				  $('#jqgh_grid_rn').html('STT');
				  $('#treeContainer').css('height',$('.Content').height()-75);
				  $('#treeContainer').jScrollPane();
				  updateRownumWidthForJqGrid();
			  }
			})
			.navGrid('#pager', {edit:false,add:false,del:false, search: false});
	} else {
		$('#gridStyle').removeClass('ShowLink');
		$('#gridStyle').addClass('HideLink');
		$('#gridStyle').text('Dạng thu gọn');
		$("#grid").jqGrid({
			url:UnitTreeCatalog._urlLoadGrid,
			  colModel:[		
			    {name:'shopCode', label: 'Mã đơn vị', width: 80, sortable:false,resizable:true , align: 'left'},
			    {name:'shopName', label: 'Tên đơn vị', sortable:false,resizable:true , align: 'left'},
			    {name:'type.channelTypeName', label: 'Loại đơn vị', width: 80, sortable:false,resizable:false, align: 'left',formatter: UnitTreeFormatter.unitTypeFormatter},
			    {name:'parentShop.shopName', label: 'Tên đơn vị cha', align: 'left',sortable:false,resizable:true },
			    {name:'taxNum', label: 'Mã số thuế', width: 80, align: 'right',sortable:false,resizable:false },
			    {name:'invoiceNumberAccount', label: 'Tài khoản NH', width: 100, align: 'right',sortable:false,resizable:false },
			    {name:'status', label: 'Trạng thái', width: 80, align: 'left',sortable:false,resizable:false, formatter: GridFormatterUtils.statusCellIconFormatter },
			    {name:'add', label: 'Thêm D/V con', width: 80, align: 'center',sortable:false,resizable:false, formatter: UnitTreeFormatter.addCellIconFormatter},
			    {name:'edit', label: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: UnitTreeFormatter.editCellIconFormatter}
			  ],	  
			  pager : '#pager',
			  height: 'auto',	  	  
			  width: ($('#unitGrid').width()),
			  shrinkToFit: false,
			  gridComplete: function(){
				  $('#jqgh_grid_rn').html('STT');
				  $('#treeContainer').css('height',$('.Content').height()-75);
				  $('#treeContainer').jScrollPane();
				  updateRownumWidthForJqGrid();
			  }
			})
			.navGrid('#pager', {edit:false,add:false,del:false, search: false});	
			$("#grid").jqGrid('setFrozenColumns');
	}
}

/* $('#isAll').change(function(event) {
	UnitTreeCatalog.search();
}); */
$('#provinceCode').bind('keyup', function(event){
	if(event.keyCode == keyCode_F9){
		CommonSearch.searchProvinceOnDialog(function(data){
			$('#provinceCode').val(data.code);
			provinceCodeChanged();
			districtCodeChanged();
			$('#districtCode').focus();
		});
	}
});
$('#districtCode').bind('keyup', function(event){
	if(event.keyCode == keyCode_F9){
		var lstParam = new Array();
		var obj = {};
		obj.name = 'provinceCode';
		obj.value = $('#provinceCode').val().trim();
		lstParam.push(obj);
		CommonSearch.searchDistrictOnDialog(function(data){
			$('#districtCode').val(data.code);
			districtCodeChanged();
			$('#wardCode').focus();
		},lstParam);
	}
});
$('#wardCode').bind('keyup', function(event){
	if(event.keyCode == keyCode_F9){
		var lstParam = new Array();
		var objP = {};
		objP.name = 'provinceCode';
		objP.value = $('#provinceCode').val().trim();
		var objD = {};
		objD.name = 'districtCode';
		objD.value = $('#districtCode').val().trim();
		lstParam.push(objP);
		lstParam.push(objD);
		CommonSearch.searchWardOnDialog(function(data){
			$('#wardCode').val(data.code);
			$('#address').focus();
		},lstParam);
	}
});
function provinceCodeChanged(){
	if($('#provinceCode').val().trim().length == 0){
		$('#districtCode').val('');
		$('#districtCode').attr('disabled','disabled');
		districtCodeChanged();
	} else {
		$('#districtCode').removeAttr('disabled');
		$('#districtCode').focus();
	}
};
function districtCodeChanged(){
	if($('#districtCode').val().trim().length == 0){
		$('#wardCode').val('');
		$('#wardCode').attr('disabled','disabled');
	} else {
		$('#wardCode').removeAttr('disabled');
		$('#wardCode').focus();
	}
};
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

	<div class="ReportCtnSection">
	    <h2 class="Title2Style">Thông tin tìm kiếm</h2>
	    <div class="GeneralForm SProduct1Form">
	        <label class="LabelStyle Label16Style">Mã nhân viên</label>
	        <input type="text" class="InputTextStyle InputText5Style" id="staffCode" maxlength="40" />
	        <label class="LabelStyle Label11Style">Tên nhân viên</label>
	        <input type="text" class="InputTextStyle InputText5Style" id="staffName" maxlength="40" />
	        <label class="LabelStyle Label16Style">Trạng thái</label>
	        <div class="BoxSelect BoxSelect6"> 
	            <select class="MySelectBoxClass" id="status">
		          <option value="-2">---Tất cả---</option> 
		          <option value="0">Tạm ngưng</option> 
		          <option value="1" selected="selected">Hoạt động</option> 
		       </select>
	       </div>
	       <div class="Clear"></div>
	       <div class="BtnCenterSection">
	          <button class="BtnGeneralStyle" id="btnSearch" onclick="return UnitTreeCatalog.search(0);">Tìm kiếm</button>
	       </div>
	       <div class="Clear"></div>
	       <p class="ErrorMsgStyle Sprite1" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
				<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
	    </div>
	  	<h2 class="Title2Style">Danh sách nhân viên</h2>
	  	<div class="SearchInSection SProduct1Form">
	      <div class="GridSection" id="searchStaffUnitTreeContainerGrid">
	          <!--Đặt Grid tại đây-->
			  <table id="searchStaffUnitTreeGrid" class="easyui-datagrid"></table>
              <div class="GeneralForm GeneralNoTP1Form">
	              <div id="hideGsnpp" class="Func1Section">
<!-- 							<p class="DownloadSFileStyle DownloadSFile2Style"><a id="downloadTemplate" href="javascript:void(0)" class="Sprite1" onclick="">Tải file excel mẫu</a></p> -->
<!-- 							<button id="btnImport" class="BtnGeneralStyle" onclick= "return UnitTreeCatalog.openPopupImportStaff();">Nhập từ Excel</button> -->
<!-- 							<button id="btnExport" class="BtnGeneralStyle" onclick="UnitTreeCatalog.exportExcel();" >Xuất Excel</button> -->
						<p class="DownloadSFileStyle DownloadSFile2Style">
							<a id="downloadTemplate" href="javascript:void(0)">Tải mẫu file excel</a>
						</p>		
						<div class="DivInputFile">
				 			<form action="/catalog/unit-tree/importexcelfile" name="importFrm" id="importFrm" method="post" enctype="multipart/form-data">
					            <input type="file" class="InputFileStyle" size="20" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
					            <input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>
					            <input type="hidden" id="isView" name="isView">
					            <div class="FakeInputFile">
					                <input id="fakefilepc" type="text" class="InputTextStyle InputText1Style">
					            </div>
							</form>
				        </div>
				        <button id="btnImport" class="BtnGeneralStyle" onclick="return UnitTreeCatalog.upload();">Nhập từ file</button>
					    <button id="btnExport" class="BtnGeneralStyle" onclick="return UnitTreeCatalog.exportExcel();">Xuất ra File</button>
					    <div class="Clear"></div>
				  </div>		
                  <div class="Clear"></div>
                  <p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
          	</div>
         </div>
        </div>
    </div>
    <div id="responseDiv" style="display:none;"></div>
	<s:hidden id="carTypeId" value="0"></s:hidden>
	<s:hidden id="shopCodeHidden" name="staff.shop.shopCode"></s:hidden>
	<s:hidden id="shopIdHidden" name="staff.shop.id"></s:hidden>
	<script type="text/javascript">
	Utils.bindAutoSearchEx('.SProduct1Form');
	$('#status').customStyle();
	$('#layoutContent').removeClass('Content');
	$('#layoutContent').addClass('MainContent');
	$('#staffCode').focus();	
	$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_nhan_vien_don_vi_import.xls');
	$('.easyui-dialog #downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_nhan_vien_don_vi_import.xls');
	var node = $('#tree').tree('getSelected');
	var shopId = 0;
	var groupId = 0;
	if(node != null) {
		if(node.attributes.shop!= null) shopId=node.attributes.shop.id;
		else if(node.attributes.staff!= null) shopId=node.attributes.staff.shop.id;
		else if(node.attributes.group!= null){ 
			shopId=node.attributes.group.shop.id;
			groupId=node.attributes.group.id;
			if (isNaN(groupId)) {
				groupId = 0;
			}
		}
	}
	var params = new Object();
	params.status = $('#status').val();
	params.shopId = shopId;
	params.groupId = groupId;
	$('#searchStaffUnitTreeGrid').datagrid({
		url : "/catalog/unit-tree/search-staff",
		autoRowHeight : true,
		rownumbers : true, 
		singleSelect  :true,
		checkOnSelect :true,
		pagination:true,
		rowNum : 10,
		pageList  : [10,20,30],
		scrollbarSize:0,
		queryParams:params,
		fitColumns : true,
		width : ($('#searchStaffUnitTreeContainerGrid').width() - 20),
	    columns:[[	        
		    {field:'staffCode', title: 'Mã nhân viên', width: 40, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
		    {field:'staffName', title: 'Tên nhân viên', width:100, sortable:false,resizable:false , align: 'left', formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
		    {field:'mobilephone', title: 'Mobilephone', width: 50, align: 'right', sortable:false,resizable:false, formatter: function(value, row, index) {
		    	return Utils.XSSEncode(value);
			}},
		    {field:'staffType.channelTypeName',title: 'Loại NV', width: 50, align: 'left',sortable:false,resizable:false, formatter:function(value,row,index) {
		    		return Utils.XSSEncode(row.staffType.channelTypeCode);	
		    }},
		    {field:'status', width: 40, title: 'Trạng thái',align: 'center',sortable:false,resizable:false,formatter:StaffUnitTreeFormatter.statusFormat},
		    {field:'edit', width: 20, align: 'center',sortable:false,resizable:false, formatter: StaffUnitTreeFormatter.editCellIconFormatter},
	    ]],	    
	    onLoadSuccess :function(data){	    	
	    	$('.datagrid-header-rownumber').html('STT');	
	    	 var tm = setTimeout(function(){
	    		 UnitTreeCatalog.resize();
	    		 clearTimeout(tm);
			 }, 150);
	    },
		method : 'POST'
	});
	var options = { 
			beforeSubmit: CustomerCatalog.beforeImportExcel,   
	 		success:      ProductLevelCatalog.afterImportExcelUpdate,
	 		type: "POST",
	 		dataType: 'html'   
	 	}; 
	$('#importFrm').ajaxForm(options);
</script>
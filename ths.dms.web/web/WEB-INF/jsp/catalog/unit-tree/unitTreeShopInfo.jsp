<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<div class="ReportCtnSection">
	<h2 class="Title2Style"><s:text name="organization_information"/></h2>
    <div class="GeneralForm SProduct1Form">
		<input type="hidden" id="parentShopCode" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="parentShop.shopCode"/>" />
        <label class="LabelStyle Label17Style"><s:text name="organization_parent_Shop_Code"/></label>
        <input id="parentShopName" type="text" class="InputTextStyle InputText1Style" maxlength="100" disabled="disabled" value="<s:property value="parentShop.shopName"/>"/>
        <label class="LabelStyle Label15Style"><s:text name="organization_shop_Type"/></label>
        <div class="BoxSelect BoxSelect2">
            <select id="shopType" class="MySelectBoxClass ">
<%--             	<option value='<s:property value="shopTypeEle.id"/>'><s:property value="shopTypeEle.name"/></option> --%>
            </select>
        </div>      
        <div class="Clear"></div>
        <label class="LabelStyle Label17Style"><s:text name="organization_shop_Code"/><span class="ReqiureStyle"> * </span></label>
        <input type="text" id="shopCode" class="InputTextStyle InputText1Style classCheckChange"  maxlength="40" value="<s:property value="shop.shopCode"/>" />
        <label class="LabelStyle Label15Style"><s:text name="organization_shop_Name"/><span class="ReqiureStyle"> * </span></label>
        <input type="text" id="shopName" class="InputTextStyle InputText1Style classCheckChange" maxlength="100" value="<s:property value="shop.shopName"/>"/>
        <div class="Clear"></div>
        <label class="LabelStyle Label17Style"><s:text name="organization_status"/></label>
        <div class="BoxSelect BoxSelect2">
            <select id = "status" class="MySelectBoxClass classCheckChange">
                <option value="1" selected="selected"><s:text name="jsp.common.status.active"/></option>
                <option value="0"><s:text name="jsp.common.status.stoped"/></option>
            </select>
        </div>
         <label class="LabelStyle Label15Style"><s:text name="organization_abbreviation_name"/> </label>
        <input type="text" id="abbreviation" class="InputTextStyle InputText1Style classCheckChange"  maxlength="100" value="<s:property value="shop.abbreviation"/>" />
        <div class="Clear"></div>
        <label class="LabelStyle Label17Style"><s:text name="organization_phone"/></label>
        <input id="phoneNumber" type="text" class="InputTextStyle InputText1Style classCheckChange" maxlength="20" value="<s:property value="shop.phone"/>"/>
        <label class="LabelStyle Label15Style"><s:text name="organization_mobile"/></label>
        <input id="mobileNumber" type="text" class="InputTextStyle InputText1Style classCheckChange" maxlength="20" value="<s:property value="shop.mobiphone"/>"/>
        <div class="Clear"></div>
        <label class="LabelStyle Label17Style"><s:text name="organization_fax"/></label>
        <input id="faxNumber" type="text" class="InputTextStyle InputText1Style classCheckChange" maxlength="20" value="<s:property value="shop.fax"/>"/>
        <label class="LabelStyle Label15Style"><s:text name="organization_email"/></label>
        <input id="email" type="text" class="InputTextStyle InputText1Style classCheckChange" maxlength="40" value="<s:property value="shop.email"/>"/>
        <div class="Clear"></div>
        <label class="LabelStyle Label17Style"><s:text name="organization_tax_Number"/></label>
        <input id="taxNumber" type="text" class="InputTextStyle InputText1Style classCheckChange" maxlength="20" value="<s:property value="shop.taxNum"/>"/>
        <label class="LabelStyle Label15Style"><s:text name="organization_contact_Name"/></label>
        <input id="contactName" type="text" class="InputTextStyle InputText1Style classCheckChange" maxlength="200" value="<s:property value="shop.contactName"/>"/>
        <div class="Clear"></div>
        <label class="LabelStyle Label17Style"><s:text name="organization_address_BillTo"/></label>
        <input id="addressBillTo" type="text" class="InputTextStyle InputText1Style classCheckChange" maxlength="200" value="<s:property value="shop.billTo"/>"/>
        <label class="LabelStyle Label15Style"><s:text name="organization_address_ShipTo"/></label>
        <input id="addressShipTo" type="text" class="InputTextStyle InputText1Style classCheckChange" maxlength="200" value="<s:property value="shop.shipTo"/>"/>
        <div class="Clear"></div>
    </div>
    <h2 class="Title2Style"><s:text name="organization_position"/></h2>
    <div class="GeneralForm SProduct1Form">
    	<div class="SProductForm7Cols">
            <label class="LabelStyle Label17Style"><s:text name="organization_area_Tree"/><%-- <span class="ReqiureStyle RequireStypeOfParentShopCode"> * </span> --%></label>
            <div class="BoxSelect BoxSelect2">
				<input type="text" id="areaTree" class="InputTextStyle InputText1Style" style="width:206px;" />
				<input type="hidden" id="areaId" class="classCheckChange"/>
			</div>
            <div class="Clear"></div>
            <label class="LabelStyle Label17Style"><s:text name="organization_province_Name"/> </label>
            <input id="provinceName" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="shop.area.provinceName"/>"/>
            <div class="Clear"></div>
            <label class="LabelStyle Label17Style"><s:text name="organization_district_Name"/></label>
            <input id="districtName" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="shop.area.districtName"/>"/>
            <div class="Clear"></div>
            <label class="LabelStyle Label17Style"><s:text name="organization_precinct_Name"/></label>
            <input id="precinctName" type="text" class="InputTextStyle InputText1Style" disabled="disabled" value="<s:property value="shop.area.precinctName"/>"/>
            <div class="Clear"></div>
            <label class="LabelStyle Label17Style"><s:text name="organization_address"/><%-- <span class="ReqiureStyle RequireStypeOfParentShopCode"> * </span> --%></label>
            <input id="address" type="text" id="address"class="InputTextStyle InputText1Style classCheckChange" maxlength="200" value="<s:property value="shop.address"/>"/>
            <div class="Clear"></div>		
            <div class="BoxImageMap">
            	<a href="javascript:void(0)" class="SeeFull" onclick="UnitTreeCatalog.viewBigMap()"><s:text name="organization_view_full"/> &gt;&gt;</a>
            	<div id="mapContainerUnitTree" style="width: 348px; height: 155px;position:relative;" alt="Bản đồ"></div>
            </div>
            <div class="Clear"></div>
        </div>
        <div class="Clear"></div>
        <div class="BtnCenterSection">
         <button id ="cancelButton" class="BtnGeneralStyle" onclick="UnitTreeCatalog.cancelOrganization()"><s:text name="jsp.common.quay.lai"/></button>
         <button id ="btnUpdateShop" class="BtnGeneralStyle cmsiscontrol" onclick="UnitTreeCatalog.addOrUpdateShopInfo()"><s:text name="jsp.common.capnhat"/></button>
        </div>
        <p id="errMsgAddOrUpdate" class="ErrorMsgStyle SpriteErr" style="display: none"></p>
        <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
        <div class="Clear"></div>
    </div>
</div>
<input type="hidden" id="shopIdInfo" value="<s:property value="shop.id"/>"/>
<input type="hidden" id="lat" value="<s:property value="shop.lat"/>"/>
<input type="hidden" id="lng" value="<s:property value="shop.lng"/>"/>
<input type="hidden" id="isUpdate" value="<s:property value="isUpdate"/>"/>
<input type="hidden" id="parentShopIdHd" value="<s:property value='shop.parentShop.id' />"/>
<input type="hidden" id="parentShopNameHd" value="<s:property value='shop.parentShop.shopName' />"/>
<input type="hidden" id="organizationId" value="<s:property value='organizationId' />"/>
<input type="hidden" id="shopIdInfo" value="<s:property value="shop.id"/>"/>
<input type="hidden" id="nameTypeOfShop" value="<s:property value="shopTypeEle.name"/>"/>
<script type="text/javascript">
	Utils.functionAccessFillControl('unitTreeContent', function (data) {
		//Xu ly cac thanh phan lien quan den control phan quyen
	});
	$('#shopType').customStyle();
	$('#status').customStyle();
	UnitTreeCatalog._isLoadArea=true;
	setTimeout(function(){UnitTreeCatalog._isChange=false;},1500);
	Utils.bindFormatOnTextfield('taxNumber',Utils._TF_PHONE_NUMBER_SPECIAL); 
	var lat = '<s:property value="shop.lat"/>';
	var lng = '<s:property value="shop.lng"/>';
	var isUpdate = '<s:property value="isUpdate"/>';
	if(isUpdate =='false') {
		$('#shopCode').focus();
		disableSelectbox('status');
	} else {
		$('#shopName').focus();
	}
	var allowSelectParentShop = '<s:property value="allowSelectParentShop"/>';
	var shopTypeOfShop = '<s:property value="shopTypeEle.id"/>';
	var shopTypeOfParentShop = '<s:property value="parentShop.type.objectType"/>';
	var statusOfShop = '<s:property value="shop.status.value"/>';
	var areaIdOfShop = '<s:property value="shop.area.id"/>';
	UnitTreeCatalog.updateShopInfoScreenWhenLoad(isUpdate,allowSelectParentShop,shopTypeOfShop,statusOfShop,areaIdOfShop,shopTypeOfParentShop);
// 	var nameTypeOfShop = '<s:property value="shopTypeEle.name" escapeJavaScript="true" />';
 	var nameTypeOfShop = $('#nameTypeOfShop').val();
	var htmlCombobox = "";
	htmlCombobox += ('<option value="'+shopTypeOfShop+'">'+Utils.XSSEncode(nameTypeOfShop)+'</option>');
	$('#shopType').html(htmlCombobox).change();
	disableSelectbox('shopType');
	setTimeout(function(){
		if(lat == undefined || lat == null || lat=='') lat = null;
		if(lng == undefined || lng == null || lng=='') lng = null;
		VTMapUtil.loadMapResource(function() {
			ViettelMap.loadBigMapEx('mapContainerUnitTree', lat, lng, null, function(latitude, longitude) {
				$('#lat').val(latitude);
				$('#lng').val(longitude);
			}, true);
		});
	},500);
	UnitTreeCatalog._isChange=false;
	$('.classCheckChange').change(function(){UnitTreeCatalog._isChange=true;});
	//$('#shopName').focus();
	Utils.bindAutoButtonEx('.Content3Section','btnUpdateShop');
</script>
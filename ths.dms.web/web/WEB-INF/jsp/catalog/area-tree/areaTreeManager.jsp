<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/catalog/area-tree/info#"><s:text name="organization_system"/></a></li>
		<li><span><s:text name="area_tree_manage_area"/></span></li>
	</ul>
</div>

<div class="CtnTwoColsSection">
	<div class="SidebarSection" id="areaTreeMgrContainer">
		<h2 class="Title2Style"><s:text name="area_tree_title"/></h2>
		<div class="SidebarInSection">
			<div class="ReportTreeSection" style=" overflow: scroll;">
				<div id="tree"></div>
			</div>
		</div>
	</div>
	<div class="ContentSection">
		<div class="ReportCtnSection">
			<h2 class="Title2Style" id="findInfo"><s:text name="area_tree_information_search"/></h2>
			<div class="GeneralForm SearchInSection" id="searchForm">
				<div id="divContentEdit">
					<label class="LabelStyle Label1Style" id="lblparentCode"><s:text name="area_tree_parent_code_id"/></label>
					<input id="areaParentCode" type="text" class="InputTextStyle InputText3Style" maxlength="20" />
					<label id="lblareaNameParent" class="LabelStyle Label1Style"><s:text name="area_tree_parent_name"/></label>
					<input id="areaNameParent" maxlength="100" type="text" class="InputTextStyle InputText3Style" />
				</div>
				<div class="Clear"></div>
				<label class="LabelStyle Label1Style"><s:text name="area_tree_code"/></label>
				<input id="areaCode" maxlength="20" type="text" call-back="" class="InputTextStyle InputText3Style" />
				<label class="LabelStyle Label1Style"><s:text name="area_tree_name"/></label>
				<input id="areaName" type="text" maxlength="100" type="text" class="InputTextStyle InputText3Style" />
				<label class="LabelStyle Label1Style"><s:text name="report.status.label"/></label>
				<div class="BoxSelect BoxSelect5">
					<select id="status" class="MySelectBoxClass" name="LevelSchool">
						<option value="-2" id="allStatus"><s:text name="jsp.common.status.all"/></option>
						<option value="0"><s:text name=" "/><s:text name="jsp.common.status.stoped"/></option>
						<option value="1" selected="selected"><s:text name="jsp.common.status.active"/></option>
					</select>
				</div>
				<input type="hidden" id="areaId" value="0">
				<input type="hidden" id="parentId" value="-1">
				<div class="Clear"></div>
				<div id="groupButtonDisplay" class="BtnCenterSection">
					<button id="btnUpdate" class="BtnGeneralStyle cmsiscontrol" style="display: none;" onclick="AreaTreeCatalog.updateArea();"><s:text name="jsp.common.capnhat"/></button>
					<button id="btnSearch" class="BtnGeneralStyle" onclick="AreaTreeCatalog.search()"><s:text name="jsp.common.timkiem"/></button>
				</div>
			</div>
			<div id="titleGrid">
				<h2 class="Title2Style"><s:text name="area_tree_list"/></h2>
			</div>
			<div class="GridSection" id="GridSection">
				<div class="ResultSection" id="areaGrid">
					<p id="gridNoResult" style="display: none"
						class="WarningResultStyle"><s:text name="area_tree_no_result"/></p>
					<table id="grid"></table>
					<div id="pager"></div>
				</div>
			</div>
			<div class="Clear"></div>
			<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
			<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
			<p id="successMsgImport" class="SuccessMsgStyle" style="display:none;"></p>
			<div class="Clear"></div>
			<div class="GeneralForm GeneralNoTP1Form" id="hideSupport">
				<div class="Func1Section">
					<!-- href="javascript:void(0)" -->					
					<div class="DivInputFile cmsiscontrol" id="divImportArea" style="min-width: 405px;">
						<p class="DownloadSFileStyle DownloadSFile2Style">
							<a id="downloadTemplate" href="javascript:void(0)" onclick="AreaTreeCatalog.download();" class="Sprite1"><s:text name="unit_tree.search_unit.search_grid.import.download_file"/></a>
						</p>
						<form action="/catalog/area-tree/importexcelfile" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">									
							<input type="hidden" id="isView" name="isView"/>
                				<input type="file" class="InputFileStyle" size="22" name="excelFile" id="excelFile" style="width: 100px !important;" onchange="previewImportExcelFile(this,'importFrm');">
				            <div class="FakeInputFile">
								<input id="fakefilepc" style="margin: -3px 5px 0 0;" readonly="readonly" type="text" class="InputTextStyle InputText1Style">
							</div>
						</form>
						<button id="btnImport" class="BtnGeneralStyle" style="display: block; margin-top: -4px;" onclick="return AreaTreeCatalog.importExcel();"><s:text name="unit_tree.search_unit.search_grid.import_button_text"/></button>
					</div>
					<button id="btnExport" class="BtnGeneralStyle" onclick="return AreaTreeCatalog.exportExcelData();"><s:text name="unit_tree.search_unit.search_grid.export_button_text"/></button>
				</div>
				<div class="Clear"></div>
			</div>
		</div>
	</div>
	<div class="Clear"></div>
	<div class="Clear"></div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#layoutContent').removeClass('Content');
		$('#layoutContent').addClass('MainContent');
		$('#areaCode').focus();
		$('.RequireStyle').hide();
		$('#divContentEdit').hide();
		
		$("#grid").datagrid({
			url : '/catalog/area-tree/search',
			pageList  : [10,20,30],
			width: 910,
			height:'auto',
			scrollbarSize : 0,
			queryParams : {status : 1},
			pagination:true,			  
			fitColumns:true,
			method : 'POST',
			singleSelect:true,
			rownumbers: true,
			width: ($('#areaGrid').width()),
			columns:[[	
				{field: 'areaCode', title: area_tree_code, sortable:true,resizable:true, align: 'left', formatter: function(value, row, index){
					return VTUtilJS.XSSEncode(value);
				}},
			    {field: 'areaName', title: area_tree_name, width: 120, sortable:true,resizable:true, align: 'left', formatter: function(value, row, index){
					return VTUtilJS.XSSEncode(value);
				} },
			    {field: 'areaParentCode', title: area_tree_parent_code_id, width: 150,sortable:true,resizable:true, align: 'left',formatter : function(value, row, index){
			    	return (row.parentArea == null)? "" :VTUtilJS.XSSEncode(row.parentArea.areaCode);
			    } },
			 	{field: 'areaParentName', title: area_tree_parent_name, width: 150,sortable:true,resizable:true, align: 'left',formatter: function(value,row, index){
			 		return (row.parentArea == null)? "" :VTUtilJS.XSSEncode(row.parentArea.areaName);
			 		/* if(row.parentArea != null) {
			 			return (row.parentArea.parentArea == null)? "" :VTUtilJS.XSSEncode(row.parentArea.parentArea.areaName);
			    		//return VTUtilJS.XSSEncode(row.parentArea.parentArea.areaName);
			    	} else {
			    		return "";
			    	} */			    
			    } },
			    {field: 'status', index:'status',title: report_status_label, width: 100, align: 'left', sortable:true,resizable:true, formatter: GridFormatterUtils.statusCellIconFormatter},
			    {field: 'id', index: 'id', hidden: true},
			]],	  
			onLoadSuccess:function(){
				$('.datagrid-header-rownumber').html(area_tree_stt);  
		      	$('#grid').datagrid('resize');
		      	var tm = setTimeout(function(){
		      		var hg = $('.ContentSection').height() - 94; 
			      	$('.ReportTreeSection').css('height',hg);
		      	},500);
		      	jQuery('#status').children('option[value="-2"]').show();
		      	$('#findInfo').html(area_tree_title);
		      	$('#hideSupport').show();
			}
		});
		AreaTreeCatalog.loadAreaTree();
		//$('#downloadTemplate').attr('href', excel_template_path + 'catalog/Bieu_mau_danh_muc_cay_dia_ban.xls');
		AreaTreeCatalog.resetOnLoad();
		$("#status").bind("keydown", function(event) {
		      var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
		      if (keycode == 13) {		    	
		         
		         if(AreaTreeCatalog.isUpdate == true){
		    		 document.getElementById('btnUpdate').click();
		    	 }else{
		    		 document.getElementById('btnSearch').click();
		    	 }
		         return false;
		      } else  {
		         return true;
		      }
		});	
		Utils._functionCallback = function() {
			$('#btnUpdate').hide();
		};
	});
</script>
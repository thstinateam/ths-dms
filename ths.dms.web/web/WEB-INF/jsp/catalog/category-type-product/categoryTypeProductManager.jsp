<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
      	<div class="BreadcrumbSection">
              <ul class="ResetList FixFloat BreadcrumbList">
                  <li class="Sprite1"><a href="/catalog/product-distributer">Danh mục</a></li>
                  <li><span>Tồn kho chuẩn theo sản phẩm</span></li>
              </ul>
          </div>
          <div class="CtnOneColSection">
              <div class="ContentSection">
              	<div class="ToolBarSection">
                      <div class="SearchSection GeneralSSection">
                      	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                          <div class="SearchInSection SProduct1Form">
                          	<label class="LabelStyle Label4Style" id ="lblMaDonVi">Mã đơn vị(F9)</label>
                              <input id="shopCode" type="text" class="InputTextStyle InputText1Style" />
                              <label class="LabelStyle Label4Style">Mã sản phẩm(F9)</label>
                              <input id="productCode" type="text" class="InputTextStyle InputText1Style" />
                                                           
                              <label class="LabelStyle Label4Style">Trạng thái</label>                               
                              <div class="BoxSelect BoxSelect5">
						<select id="status" class="MySelectBoxClass" name="LevelSchool">							
							<option value="-2">---Tất cả---</option>
						    <option value="0">Tạm ngưng</option>
						    <option value="1" selected="selected">Hoạt động</option>
						</select>
		               </div>                              
                              <div class="Clear"></div>
                              <div class="BtnCenterSection">
                                  <button class="BtnGeneralStyle" id="btnSearch" onclick="return CategoryTypeProductCatalog.search();">Tìm kiếm</button>
                                  <img id="loading" class="LoadingStyle" style="visibility: hidden;" src="/resources/images/loading.gif">
                              </div>                               
                              <p class="ErrorMsgStyle" id="errMsg" style="display: none;">Có lỗi xảy ra khi cập nhật dữ liệu</p>
                              <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                          </div>
                      </div>
                      <div class="Clear"></div>
                  </div>
                  <div class="GeneralCntSection">
               			<h2 class="Title2Style">Danh sách tồn kho chuẩn theo sản phẩm</h2>
                  		<div class="GridSection">
                      		<div class="ResultSection" id="productGridV">
                  				<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
                  				<table id="grid"></table>
								<div id="pager"></div>
              				</div>
						</div>
                  	<div class="GeneralForm GeneralNoTP1Form">
						<div class="Func1Section" id="idDivImport"> 
							<p class="DownloadSFileStyle DownloadSFile2Style">
								<a id="downloadTemplate" href="javascript:void(0)" class="Sprite1">Tải mẫu file excel</a>
							</p>
	                       	<div class="DivInputFile">
								<form action="/product-distributer/importexcelfile" name="importFrm" id="importFrm"  method="post" enctype="multipart/form-data">
						            <input type="hidden" id="isView" name="isView"/>
		                				<input type="file" class="InputFileStyle" size="22" name="excelFile" id="excelFile" style="width: 100px !important;" onchange="previewImportExcelFile(this,'importFrm');">
						            <div class="FakeInputFile">
										<input id="fakefilepc" style="margin: -3px 5px 0 0;" readonly="readonly" type="text" class="InputTextStyle InputText1Style">
									</div>
								</form>
				        	</div>	    			
	                        <button id="btnImport" class="BtnGeneralStyle" onclick="CategoryTypeProductCatalog.importExcel(fakefilepc);"><span class="Sprite2">Nhập từ Excel</span></button>
	                        <button id="btnExport"class="BtnGeneralStyle" onclick="CategoryTypeProductCatalog.exportExcel();"><span class="Sprite2">Xuất Excel</span></button>
                        </div>
                        <div class="Clear"></div>
                      </div>
                  </div>
              </div>
          </div>
        <div id="weeks" style="visibility: hidden;">
           <div id="popupDate" title="Ngày làm việc trong tuần" style="width:550px;height:180px;" data-options="closed:true,modal:true">
		<label class="LabelStyle Label4Style"><input type="checkbox" name="dayinWeeks" value="2" id="T2" tabindex="1">Thứ 2</label>
		<label class="LabelStyle Label4Style"><input type="checkbox" name="dayinWeeks" value="3" id="T3" tabindex="2">Thứ 3</label>
		<label class="LabelStyle Label4Style"><input type="checkbox" name="dayinWeeks" value="4" id="T4" tabindex="3">Thứ 4</label>
		<label class="LabelStyle Label4Style"><input type="checkbox" name="dayinWeeks" value="5" id="T5" tabindex="4">Thứ 5</label>
		<label class="LabelStyle Label4Style"><input type="checkbox" name="dayinWeeks" value="6" id="T6" tabindex="5">Thứ 6</label>
		<label class="LabelStyle Label4Style"><input type="checkbox" name="dayinWeeks" value="7" id="T7" tabindex="6">Thứ 7</label>
		<label class="LabelStyle Label4Style"><input type="checkbox" name="dayinWeeks" value="1" id="CN" tabindex="7">Chủ nhật</label>
		<div class="Clear"></div>
		 <p class="ErrorMsgStyle" id="errMsg1" style="display: none;"></p>
		</div>
   	</div>
            
<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
<p id="errorExcelMsgv" class="ErrorMsgStyle" style="display: none"></p>
<p id="errorExcel" class="ErrorMsgStyle" style="display: none"></p>

<div id="responseDiv" style="display: none"></div>
<div id="divPop" style="visibility:hidden">
		<div id="popup1" class="easyui-dialog" title="Ngày làm việc" data-options="closed:true,modal:true" style="width:400px;height:147px;">
        	<div class="PopupContentMid">
        		<div class="GeneralForm Search1Form">
                               <span>Thứ 2</span><input id="monday"  type="checkbox" value="2"/>
                               <span>Thứ 3</span><input id="tuesday"  type="checkbox" value="3"/>
                               <span>Thứ 4</span><input id="wednesday"  type="checkbox" value="4"/>
                               <span>Thứ 5</span><input id="thursday"  type="checkbox" value="5"/>
                               <span>Thứ 6</span><input id="friday"  type="checkbox" value="6"/>
                               <span>Thứ 7</span><input id="saturday"  type="checkbox" value="7"/>
                               <span>Chủ nhật</span><input id="sunday"  type="checkbox" value="1" />
                           <div class="Clear"></div>
                           <div class="BtnCenterSection">
                           		<button class="BtnGeneralStyle" onclick="return CategoryTypeCatalog.mapValue();"><span class="Sprite2">Chọn</span></button>
                           	</div>
                           	<div class="Clear"></div>
                           <p class="ErrorMsgStyle" id="errMsg1" style="display: none;"></p>
                 </div>
             </div>
         </div>
</div>

<s:hidden id="productId" name="productId" ></s:hidden>
<s:hidden id="shopCodeGrid" name="shopCodeGrid"></s:hidden>
<s:hidden id="productCodeGrid" name="productCodeGrid"></s:hidden>
<s:hidden id="stockMin" name="stockMin"></s:hidden>
<s:hidden id="stockMax" name="stockMax"></s:hidden>
<s:hidden id="dateGo" name="dateGo"></s:hidden>
<s:hidden id="dateSale" name="dateSale"></s:hidden>
<s:hidden id="dateSale1" name="dateSale1"></s:hidden>
<s:hidden id="growth" name="growth"></s:hidden>
<s:hidden id="status1" name="status1"></s:hidden>
<s:hidden id="minsfOld" name="minsfOld"></s:hidden>
<s:hidden id="shopObjectType" name="shopObjectType"></s:hidden>
<s:hidden id="shopCodeHidden" name="shopCode"></s:hidden>
<s:hidden id="shopNameHidden" name="shopName"></s:hidden>

<div class="Clear" style="height: 30px;"></div>
 
<script type="text/javascript">
var edit = false;
var idx = -1;
$(document).ready(function(){
	
	$('#shopCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2({
				params : {
					status : 1
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã Đơn Vị'},
			        {id:'name', maxlength:250, label:'Tên Đơn Vị'},
			    ],
			    url : '/commons/search-shop-show-list',
			    columns : [[
			        {field:'shopCode', title:'Mã Đơn Vị', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
					  }},
			        {field:'shopName', title:'Tên Đơn Vị', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
					  }},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			            return '<a href="javascript:void(0)" onclick=" CategoryTypeProductCatalog.fillTxtShopCodeByF9(\''+Utils.XSSEncode(row.shopCode)+'\');">chọn</a>';         
			        }}
			    ]]
			});
		}
	});
	
	$('#productCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			VCommonJS.showDialogSearch2({
				params : {
					status : 1/* ,
					longType: $('#ddlProductInfo').val() */
				},
				inputs : [
			        {id:'code', maxlength:50, label:'Mã SP'},
			        {id:'name', maxlength:250, label:'Tên SP'},
			    ],
			    url : '/commons/product-searchAllProduct',
			    columns : [[
			        {field:'productCode', title:'Mã SP', align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
					  }},
			        {field:'productName', title:'Tên SP', align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index){
				    	return Utils.XSSEncode(value);
					  }},
			        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
			            return '<a href="javascript:void(0)" onclick="CategoryTypeProductCatalog.fillTxtProductCodeByF9(\''+Utils.XSSEncode(row.productCode)+'\');">chọn</a>';         
			        }}
			    ]]
			});
		}
	});
	$('#category').customStyle();
	applyDateTimePicker("#startDate");
	applyDateTimePicker("#endDate");

	var updateStatus = [
	               {valueStatus:'RUNNING',textStatus:'Hoạt động'},
	               {valueStatus:'STOPPED',textStatus:'Tạm ngưng'}
	           ];
	
	var status = $('#status').val();
	
	var shopObjectType = $('#shopObjectType').val();
	if(shopObjectType!=undefined && shopObjectType!=null 
			&& (parseInt(shopObjectType)== ShopObjectType.VNM || parseInt(shopObjectType)== ShopObjectType.GT)){
		$('#lblMaDonVi').html("Mã đơn vị(F9)");
		var options = { 
				beforeSubmit : CategoryTypeProductCatalog.beforeImportExcel,
				success : CategoryTypeProductCatalog.afterImportExcel,  
		 		type: "POST",
		 		dataType: 'html'   
		 	}; 
		$('#importFrm').ajaxForm(options);
		
		
		$('#shopCode').focus();
		$("#grid").datagrid({
			 url:CategoryTypeProductCatalog.getGridUrl('','','','',status),
			/*  url:CategoryTypeProductCatalog.getGridUrl('','',1,0), */
		  	  pageList  : [10,20,30],
			  width: ($('#productGridV').width() - 10),
			  pageSize : 10,
			  checkOnSelect :true,
			  height:'auto',
			  scrollbarSize : 0,
			  pagination:true,			  
			  fitColumns:true,	
			  pageNumber:1,
		      singleSelect:true,
			  method : 'GET',
			  rownumbers: true,
			  nowrap:false,
			  autoRowHeight: true,
			  columns:[[	
				{field: 'shop.shopCode', title: 'Mã đơn vị', width: 80, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
			    	return (row.shop == null)? "" :Utils.XSSEncode(row.shop.shopCode); 
			    }},
			    {field: 'cat.productInfoCode', title: 'Ngành hàng', width: 80, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
			    	return (row.product.cat == null)? "" :Utils.XSSEncode(row.product.cat.productInfoCode); 
			    } },		    							  
			    {field: 'product.productCode', title: 'Mã SP', width: 80, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
			    	return (row.product == null)? "" :Utils.XSSEncode(row.product.productCode); 
			    } },
			    {field: 'product.productName', title: 'Tên SP', width: 190, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
			    	return (row.product == null)? "" :Utils.XSSEncode(row.product.productName); 
			    } },
			    {field: 'minsf', title: 'Tồn kho Min', width: 80, sortable:false,resizable:false, align: 'right',editor:'numberbox' },
			    {field: 'maxsf', title: 'Tồn kho Max', width: 80, sortable:false,resizable:false, align: 'right',editor:'numberbox'},
			    
			    {field: 'lead', title: 'Đi đường', width: 80, sortable:false,resizable:false, align: 'right',editor:'numberbox' },
			    {field: 'calendarD', title: 'Ngày BH(Thứ)', width: 140, sortable:false,resizable:false, align: 'left',formatter: CategoryTypeProductCatalogFormatter.saleDayFormat, editor:{ 
		    		type:'text'
	            }},
			    {field: 'percentage', title: 'Tỉ lệ TT(%)', width: 90, sortable:false,resizable:false, align: 'right',editor:'numberbox' },
			    {field: 'status', title: 'Trạng thái', width: 110, sortable:false,resizable:false, align: 'left',formatter: CategoryTypeProductCatalogFormatter.statusFormat, 
			    	editor:{  
		                type:'combobox', 
		             	options:{                	
			            	class:'easyui-combobox',
			                valueField:'valueStatus',                  
			                panelWidth :95,
			                panelHeight :70,
			                editable:false,
			                textField:'textStatus',
							data: updateStatus
						}          	
	              	}
	            },
	            {field:'edit', title: 'Sửa', width: 50, align: 'center',sortable:false,resizable:false, formatter: function(value,row,index){
			    	if (row.editing){   
		                var s = "<a href='javascript:void(0)'  onclick='return saverow(this)'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png' title='Lưu'/></span></a> ";  
		                var c = "<a href='javascript:void(0)'  onclick='cancelrow(this)'><span style='cursor:pointer'><img src='/resources/images/icon_esc.png' title='Quay lại'/></span></a>";  
		                return s + "&nbsp;"  + c;  
		            } else {  
		                var e = "<a href='javascript:void(0)'  onclick='editrow(this);'><span style='cursor:pointer'><img src='/resources/images/icon-edit.png' title='Sửa'/></span></a>";  
		                return e;  
		            }  
			    }},
			    {field:'delete', title: 'Xóa', width: 30, align: 'center',sortable:false,resizable:false, formatter: CategoryTypeCatalogFormatter.delCellIconFormatter},

			    {field: 'id', index: 'id', hidden: true}	   
			  ]],
			  onLoadSuccess:function(){
					$('.datagrid-header-rownumber').html('STT');  
			      	$('#grid').datagrid('resize');
			      	$('.datagrid-htable .datagrid-cell').addClass('GridHeaderLargeHeight');
			    	edit = false;
			     },
			      onBeforeEdit:function(index,row){  
			          row.editing = true;
			          edit =true;		
			          idx = index;  
			          updateActions(index);  			          
			      },  
			      onAfterEdit:function(index,row){		    	 
			          row.editing = false;
			          edit = false;
			          idx = -1;
			          $('#productId').val(row.id.toString());
	 		          $('#productCodeGrid').val(row.product.productCode.toString());
	 		          $('#shopCodeGrid').val(row.shop.shopCode.toString());
			          $('#stockMin').val(row.minsf.toString());
			          $('#stockMax').val(row.maxsf.toString());
			          $('#dateGo').val(row.lead);		          
			          $('#dateSale').val(row.calendarD);
			          $('#growth').val(row.percentage);
			          var status = 1;		
			  		  if(row.status != activeStatus){
			  			   status = 0;
			  			} 
			          $('#status1').val(status);
			          updateActions(index);  
			      },  
			      onCancelEdit:function(index,row){  
			          row.editing = false;  
			          edit = false;
			          idx = -1;
			          updateActions(index);  
			      }
			});
		//Excel
		$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_Ton_kho_chuan_theo_san_pham.xls');
		CategoryTypeProductCatalog.reset();
		
	}else if(shopObjectType!=undefined && shopObjectType!=null && parseInt(shopObjectType)== ShopObjectType.NPP){
		//Phan Quyen NPP
		$('#idDivImport').html('<button class="BtnGeneralStyle" onclick="CategoryTypeProductCatalog.exportExcel();"><span class="Sprite2">Xuất Excel</span></button>').change().show();
		disabled('shopCode');
		$('#lblMaDonVi').html("Mã đơn vị");
		$('#productCode').focus();
		$('#shopCode').val($('#shopCodeHidden').val().trim()).show();
		$("#grid").datagrid({
			  url:CategoryTypeProductCatalog.getGridUrl('','','','',status),
		  	  pageList  : [10,20,30],
			  width: ($('#productGridV').width() - 10),
			  pageSize : 10,
			  checkOnSelect :true,
			  height:'auto',
			  scrollbarSize : 0,
			  pagination:true,			  
			  fitColumns:true,	
			  pageNumber:1,
		      singleSelect:true,
			  method : 'GET',
			  rownumbers: true,
			  nowrap:false,
			  autoRowHeight: true,
			  columns:[[	
				{field: 'shop.shopCode', title: 'Mã đơn vị', width: 80, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
			    	return (row.shop == null)? "" :Utils.XSSEncode(row.shop.shopCode); 
			    }},
			    {field: 'cat.productInfoCode', title: 'Ngành hàng', width: 80, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
			    	return (row.product.cat == null)? "" :Utils.XSSEncode(row.product.cat.productInfoCode); 
			    } },		    							  
			    {field: 'product.productCode', title: 'Mã SP', width: 80, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
			    	return (row.product == null)? "" :Utils.XSSEncode(row.product.productCode); 
			    } },
			    {field: 'product.productName', title: 'Tên SP', width: 190, sortable:false,resizable:false, align: 'left',formatter: function(value,row, index){
			    	return (row.product == null)? "" :Utils.XSSEncode(row.product.productName); 
			    } },
			    {field: 'minsf', title: 'Tồn kho Min', width: 80, sortable:false,resizable:false, align: 'right'},
			    {field: 'maxsf', title: 'Tồn kho Max', width: 80, sortable:false,resizable:false, align: 'right'},
			    {field: 'lead', title: 'Đi đường', width: 80, sortable:false,resizable:false, align: 'right'},
			    {field: 'calendarD', title: 'Ngày BH(Thứ)', width: 140, sortable:false,resizable:false, align: 'left',formatter: CategoryTypeProductCatalogFormatter.saleDayFormat},
			    {field: 'percentage', title: 'Tỉ lệ TT(%)', width: 90, sortable:false,resizable:false, align: 'right'},
			    {field: 'status', title: 'Trạng thái', width: 110, sortable:false,resizable:false, align: 'left',formatter: CategoryTypeProductCatalogFormatter.statusFormat},
			    {field: 'id', index: 'id', hidden: true}	   
			  ]],
			  onLoadSuccess:function(){
					$('.datagrid-header-rownumber').html('STT');  
			      	$('#grid').datagrid('resize');
			      	$('.datagrid-htable .datagrid-cell').addClass('GridHeaderLargeHeight');
			      	disabled('shopCode');
					$('#shopCode').val($('#shopCodeHidden').val().trim()).show();
			      }
			});
	}else{
		$('#idDivImport').html('<button class="BtnGeneralStyle" onclick="CategoryTypeProductCatalog.exportExcel();"><span class="Sprite2">Xuất Excel</span></button>').change().show();
	}
	//Method general
	$("#status").bind("keydown", function(event) {
	      var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
	      if (keycode == 13) {
	         document.getElementById('btnSearch').click();
	         return false;
	      } else  {
	         return true;
	      }
	});
});

function editrow(target){
	if(edit){
		return false;
	}
    $('#grid').datagrid('beginEdit', getRowIndex(target)); 
    $('td[field=status] .datagrid-editable-input').combobox('resize', 80);
	$("td[field=maxsf] input").attr("maxlength","10");
	$("td[field=minsf] input").attr("maxlength","10");
	$("td[field=lead] input").attr("maxlength","10");
	$('td[field=percentage] input').attr('maxlength','6');
	$('#minsfOld').val($('td[field=minsf] .datagrid-editable-input').val());
	$(".datagrid-btable td[field='calendarD'] table td input[type=text]").bind("focus",
    		function(){
    			var value = $(".datagrid-btable td[field='calendarD'] table td input[type=text]").val();
    			if(value != ''){
					$('#monday,#tuesday,#wednesday,#thursday,#friday,#saturday,#sunday').removeAttr('checked');
					for(var i = 0; i < value.length; i++) {
						if(value.charAt(i) == 2) {
							$('#monday').attr('checked', 'checked');
						} else if(value.charAt(i) == 3) {
							$('#tuesday').attr('checked', 'checked');
						} else if(value.charAt(i) == 4) {
							$('#wednesday').attr('checked', 'checked');
						} else if(value.charAt(i) == 5) {
							$('#thursday').attr('checked', 'checked');
						} else if(value.charAt(i) == 6) {
							$('#friday').attr('checked', 'checked');
						} else if(value.charAt(i) == 7) {
							$('#saturday').attr('checked', 'checked');
						} else if(value.charAt(i) == 1){
							$('#sunday').attr('checked', 'checked');
						}
					}
				}
    			$("#popup1").dialog({
    				onOpen:function(){
    					$(".datagrid-btable td[field='calendarD'] table td input[type=text]").blur();
    					var tabindex = -1;
    					$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
    						if (this.type != 'hidden') {
    							$(this).attr("tabindex", tabindex);
    							tabindex -=1;
    						}
    					});
    					var tabindex = 1;
    		    		$('#popup1 input,#popup1 select,#popup1 button').each(function () {
    			    		if (this.type != 'hidden') {
    				    	    $(this).attr("tabindex", tabindex);
    							tabindex++;
    			    		}
    					});
    				},
    				onClose:function(){
    					var tabindex = 1;
    					$('.InputTextStyle, select, input, button , li , tr, td, th, label, ul, a, img').each(function () {
    						if (this.type != 'hidden') {
    							$(this).attr("tabindex", tabindex);
    							tabindex +=1;
    						}
    					});
    				}
    			});
    			
    		    $("#popup1").dialog("open");
    		})
}	
	
function updateActions(index){  
    $('#grid').datagrid('updateRow',{  
        index: index,  
        row:{}  
    });  
} 
function getRowIndex(target){  
    var tr = $(target).closest('tr.datagrid-row');  
    return parseInt(tr.attr('datagrid-row-index'));  
}  
function saverow(target){  
	var msg = '';
	$('#errMsg').html('').hide();
	var minsf = $('td[field=minsf] .datagrid-editable-input').val();			
	if(minsf == ""){
		msg = Utils.getMessageOfRequireCheck('stockMin','Tồn kho Min');	
		$('#errMsg').html(msg).show();			
		$('td[field=minsf] .datagrid-editable-input').focus();
		return false;
	}	
	if(minsf.replace(/,/g,'')>9999){
		msg='Tồn kho min không được vượt quá 9999';
		$('#errMsg').html(msg).show();			
		$('td[field=minsf] .datagrid-editable-input').focus();
		return false;
	}
	var maxsf = $('td[field=maxsf] .datagrid-editable-input').val();		
	if(maxsf == ""){
		msg = Utils.getMessageOfRequireCheck('stockMax','Tồn kho Max');	
		$('#errMsg').html(msg).show();			
		$('td[field=maxsf] .datagrid-editable-input').focus();
		return false;
	}
	if(maxsf.replace(/,/g,'')>9999){
		msg='Tồn kho max không được vượt quá 9999';
		$('#errMsg').html(msg).show();			
		$('td[field=maxsf] .datagrid-editable-input').focus();
		return false;
	}
	var lead = $('td[field=lead] .datagrid-editable-input').val();
	if(lead == ""){
		msg = Utils.getMessageOfRequireCheck('dateGo','Ngày đi đường');	
		$('#errMsg').html(msg).show();			
		$('td[field=lead] .datagrid-editable-input').focus();
		return false;
	}
	if(lead.replace(/,/g,'') > 999){
		msg='Ngày đi đường không được vượt quá 999';
		$('#errMsg').html(msg).show();			
		$('td[field=lead] .datagrid-editable-input').focus();
		return false;
	}
	
	var percentage = $('td[field=percentage] .datagrid-editable-input').val();
	if(percentage == ""){
		msg = Utils.getMessageOfRequireCheck('growth','Tỉ lệ tăng trưởng');
		$('#errMsg').html(msg).show();			
		$('td[field=percentage] .datagrid-editable-input').focus();
		return false;
	}else{
		$('#growth').val(percentage);
		msg = Utils.getMessageOfFloatValidate('growth','Tỉ lệ tăng trưởng');
		if(msg!=''){
			$('#errMsg').html(msg).show();			
			$('td[field=percentage] .datagrid-editable-input').focus();
			return false;
		}
	}
	var minsfOld = $('#minsfOld').val();
	if(parseInt(minsf.trim()) >= parseInt(maxsf.trim()) && parseInt(minsf) != parseInt(minsfOld.trim())){
		msg = "Tồn kho Min phải bé hơn tồn kho Max";
		$('#errMsg').html(msg).show();
		$('td[field=minsf] .datagrid-editable-input').focus();
		return false;
	}

	if(parseInt(minsf.trim()) >= parseInt(maxsf.trim())){
		msg = "Tồn kho Min phải bé hơn tồn kho Max";
		$('#errMsg').html(msg).show();
		$('td[field=maxsf] .datagrid-editable-input').focus();
		return false;
	}
	var leadKT = lead.replace(/,/g,'');
	if( parseInt(leadKT.trim())> parseInt(maxsf.trim())){
		msg='Ngày đi đường không được vượt quá tồn kho Max';
		$('#errMsg').html(msg).show();			
		$('td[field=lead] .datagrid-editable-input').focus();
		return false;
	}
	
	CategoryTypeProductCatalog.indexedit = getRowIndex(target);
    $('#grid').datagrid('endEdit', getRowIndex(target));
    CategoryTypeProductCatalog.change();
}
function cancelrow(target){  
    $('#grid').datagrid('cancelEdit', getRowIndex(target));  
} 

</script>
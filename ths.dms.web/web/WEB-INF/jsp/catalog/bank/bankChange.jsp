<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="dialogBankChange" class="easyui-dialog" title="Tạo ngân hàng" data-options="closed:true,modal:true">
	<div class="PopupContentMid2">
		<div class="GeneralForm Search1Form">
			<label class="LabelStyle Label7Style">Đơn vị <span class="RequireStyle">(*)</span></label>
        	<!-- <div class="BoxSelect BoxSelect2"> -->
        	<div class="BoxSelect" id="shopCodeDl">
             	<input type="text" id="shopTreeDialog" class="InputTextStyle InputText5Style" />
 				<input type="hidden" id="shopCodeDialog"/>
			</div>
			<div class="Clear"></div>
			
         	<label class="LabelStyle Label7Style" id="labelBankCode">Mã ngân hàng <span class="RequireStyle">(*)</span></label>
           	<input id="bankCodeDl" type="text" class="InputTextStyle InputText5Style" maxlength="50"/>
           	<label class="LabelStyle Label2Style" id="labelBankName">Tên ngân hàng</label>
           	<input id="bankNameDl" type="text" class="InputTextStyle InputText5Style" maxlength="100"/>
           	<label class="LabelStyle Label2Style" id="labelBankPhone">Số điện thoại</label>
           	<input id="bankPhoneDl" type="text" class="InputTextStyle InputText5Style" maxlength="50"/>
           	<div class="Clear"></div>
           	
           	<label class="LabelStyle Label7Style" id="labelBankAddress">Địa chỉ</label>
           	<input id="bankAddressDl" type="text" class="InputTextStyle InputText5Style" maxlength="100"/>
           	<label class="LabelStyle Label2Style" id="labelBankAccount">Số tài khoản</label>
           	<input id="bankAccountDl" type="text" class="InputTextStyle InputText5Style" maxlength="100"/>
           	<label class="LabelStyle Label2Style">Trạng thái</label>
				<div class="BoxSelect BoxSelect2">
                  <select id="statusDl" class="MySelectBoxClass">
                      <option value="1" selected="selected">Hoạt động</option>
                      <option value="0">Tạm ngưng</option>
                  </select>
              	</div>
             <div class="Clear"></div>
			<div class="BtnCenterSection">
				<button id="btnSaveBank" class="BtnGeneralStyle">Cập nhật</button>
				<button class="BtnGeneralStyle" onclick="$('#dialogBankChange').dialog('close');">Đóng</button>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="Clear"></div>
		<p id="errorMsgDl" class="ErrorMsgStyle" style="display: none;"></p>
		<p id="successMsgDl" class="SuccessMsgStyle" style="display: none"></p>
	</div>
</div>

<script type="text/javascript">
	$(function(){
	});
</script>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
        <li class="Sprite1"><a href="#">Danh mục</a></li>
        <li><span>Danh sách ngân hàng</span></li>
    </ul>
</div>

<div class="CtnOneColSection">
   <div class="ContentSection">
   		<div class="ToolBarSection">
           <div class="SearchSection GeneralSSection">
	           	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
	           	<div class="SearchInSection SProduct1Form" id="searchForm">
	           		<label class="LabelStyle Label1Style">Đơn vị</label>
	           		<div class="BoxSelect BoxSelect2">
		              	<input type="text" id="shopTree" class="InputTextStyle InputText1Style" />
			 			<input type="hidden" id="shopCode"/>
		 			</div>
           			<label class="LabelStyle Label1Style" id="labelBankCode">Mã ngân hàng</label>
	              	<input id="bankCode" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
	              	<label class="LabelStyle Label1Style" id="labelBankName">Tên ngân hàng</label>
	              	<input id="bankName" type="text" class="InputTextStyle InputText1Style" maxlength="100"/>
	              	<div class="Clear"></div>
	              	
	              	<label class="LabelStyle Label1Style" id="labelBankPhone">Số điện thoại</label>
	              	<input id="bankPhone" type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
	              	<label class="LabelStyle Label1Style" id="labelBankAddress">Địa chỉ</label>
	              	<input id="bankAddress" type="text" class="InputTextStyle InputText1Style" maxlength="100"/>
	              	<label class="LabelStyle Label1Style">Trạng thái</label>
					<div class="BoxSelect BoxSelect2">
	                   <select id="status" class="MySelectBoxClass">
	                       <option value="-2">-- Tất cả --</option>
	                       <option value="1" selected="selected">Hoạt động</option>
	                       <option value="0">Tạm ngưng</option>
	                   </select>
	               	</div>
	               	<div class="Clear"></div>	               	
	              <div class="BtnCenterSection">
	              	<button id="btnSearch" class="BtnGeneralStyle Sprite2" onclick="BankCatalogManager.search();"><span class="Sprite2">Tìm kiếm</span></button><%--$('#grid').datagrid('load', VTUtilJS.getFormData('searchForm'));--%>
	              </div>
	              <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
	           	</div>
	           	<div class="SearchInSection SProduct1Form">
	           		<h2 class="Title2Style">Danh sách ngân hàng</h2>
	           		<div class="GridSection" id="bankGrid">
	                  	<div id="searchResult" class="GeneralTable Table34Section">
							<div id="ScrollSection" class="ScrollSection">
								<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
								<div class="BoxGeneralTTitle" id="dgGridContainer">
									<table id="grid"></table>
									<div id="pager"></div>
								</div>
							</div>
						</div>
	              	</div>
	           	</div>
           </div>
      	</div>
      	<!-- <div id="divDetailCommon" class="GeneralCntSection" style="display: none;">
        </div> -->
        
   </div>
</div>

<%-- <input type="hidden" id="proType" value="<s:property value='proType' />" /> --%>
<!-- <input type="hidden" id="shopCodeLogin" name="shopCode" /> --> 
<%-- <s:property value="shopCode"/> --%>
<s:hidden id="shopCodeLogin" name="shopCode"></s:hidden>
<div class="ContentSection" style="visibility: hidden;" id="dialogBankDiv">
	<tiles:insertTemplate template="/WEB-INF/jsp/catalog/bank/bankChange.jsp" />
</div>

<script type="text/javascript">
$(document).ready(function(){
	 
	$("#grid").datagrid({
		url: "/catalog/bank/search",
		queryParams: {status: $('#status').val().trim(), shopCode: $('#shopCodeLogin').val().trim() },
		width: $(window).width()-50,
		height: 'auto',
		singleSelect: true,
		rownumbers: true,
		fitColumns: true,
		scrollbarSize: 0,
		pagination: false,
		columns: [[			
			{field:'shopCode', title:"Đơn vị", width: 100, align:'left',sortable:false,resizable:false,formatter:function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}},
			{field:'bankCode', title:"Mã ngân hàng", align:'left',sortable:false,resizable:false,width : 150,formatter:function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}},
			{field:'bankName', title:"Tên ngân hàng",align:'left',sortable:false,resizable:false,width: 250,formatter:function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}},
			{field:'bankPhone', title:"Số điện thoại", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}},
			{field:'bankAddress', title:"Địa chỉ", width:350,align:'left',sortable:false,resizable:false,formatter:function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}}, 
			{field:'bankAccount', title:"Tài khoản", width:100,align:'left',sortable:false,resizable:false,formatter:function(value, row, index){
				return VTUtilJS.XSSEncode(value);
			}},
			{field:'status', title:"Trạng thái", width:80,align:'left',sortable:false,resizable:false,formatter:function(value,row,index){
				if(!VTUtilJS.isNullOrEmpty(row.status)){
					if(row.status == 0 ){
						return 'Tạm ngưng';
					}else if(row.status == 1){
						return 'Hoạt động';
					}else{
						return 'Dự thảo';
					}
				}else{
					return '';
				}
			}},			
			{field:'view', title:'<a href="javaScript:void(0);" id="dg_bank_insert" onclick="return BankCatalogManager.openDialogBankChangeAdd();"><span style="cursor:pointer"><img title="Thêm mới" src="/resources/images/icon_add.png"/></span></a>',
				width:50, align:'center',sortable:false,resizable:false,formatter: function(value, row, index){
					return '<a href="javascript:void(0)" id="dg_bank_update" onclick="return BankCatalogManager.openDialogBankChangeUpdate('+index+','+row.id+');"><img title="Chỉnh sửa" src="/resources/images/icon-edit.png" width="16" heigh="16" style="padding-left: 5px;"></a>';
			}}
			
		]],
		onLoadSuccess: function(data) {
			$(window).resize();
			Utils.updateRownumWidthAndHeightForDataGrid('grid');
		}
	});
	
	/* TreeUtils.loadComboTreeShopHasTitle('shopTree', 'shopCode',function(data) {
		$('.combo-panel').css('width','205');
		$('.combo-p').css('width','205');
	}, true); */
	/**TreeUtils.loadComboTreeShopHasTitle: function(controlId, storeCode,callback, returnCode)*/
	//TreeUtils.loadComboTreeShop(controlId, storeCode, defaultCode, callback, null, returnCode, true);
	var shopCodeLoad = $('#shopCodeLogin').val().trim();
	if(shopCodeLoad != undefined && shopCodeLoad != null){
		$('#shopCode').val(shopCodeLoad);
	}
	TreeUtils.loadComboTreeShop('shopTree', 'shopCode', $('#shopCodeLogin').val().trim(), function(data) {
		$('.combo-panel').css('width','205');
		$('.combo-p').css('width','205');
	}, null, true, false); 
	
	//ReportUtils.loadComboTreeAuthorizeEx('shopTree','shopCode',$('#shopCodeLogin').val().trim());
	
	$('.ContentSection .InputTextStyle,.SearchSection .InputTextStyle').unbind("keyup");
	$("#bankCode").parent().unbind("keyup");
	$("#bankCode").parent().bind("keyup", function(event) {
		if (event.keyCode == keyCodes.ENTER && $(".easyui-dialog:not(:hidden)").length == 0) {
			$("#btnSearch").click();
		}
	});
	$("#bankCodeDl").parent().unbind("keyup");
	$("#bankCodeDl").parent().bind("keyup", function(event) {
		if (event.keyCode == keyCodes.ENTER) {
			$("#btnSaveBank").click();
		}
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<div id="content">
        	<div class="BreadcrumbSection">
                <ul class="ResetList FixFloat BreadcrumbList">
                    <li class="Sprite1"><a href="#">Danh mục</a></li>
                    <li><span>Nhóm sản phẩm tạo PO Auto nhà phân phối</span></li>
                </ul>
            </div>
            <div class="CtnOneColSection">
                <div class="ContentSection">
                	<div class="ToolBarSection">
                        <div class="SearchSection GeneralSSection">
                        	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                            <div class="SearchInSection SProduct1Form">
                            	<label class="LabelStyle Label4Style">Mã NPP</label>
                                <input id="shopCode" type="text" class="InputTextStyle InputText1Style" maxlength="40" />
                                <label class="LabelStyle Label4Style">Tên NPP</label>
                                <input id="shopName" type="text" class="InputTextStyle InputText1Style" maxlength="250" />
                                                             
                                <label class="LabelStyle Label4Style" title='Đây là trạng thái của liên kết giữa "Nhà phân phối" và "Nhóm". Không phải trạng thái của Nhà phân phối! '>Trạng thái</label>                               
                                <div class="BoxSelect BoxSelect5">
									<select id="status" class="MySelectBoxClass" name="LevelSchool">
										<option value="-2">---Chọn trạng thái liên kết---</option>
									    <option value="0">Tạm ngưng</option>
									    <option value="1" selected="selected">Hoạt động</option>
									</select>
								</div>
                                
                                <div class="Clear"></div>
                                <div class="BtnCenterSection">
                                    <button class="BtnGeneralStyle cmsiscontrol" id="btnSearch" onclick="return GroupProductPOAutoNPP.search();">Tìm kiếm</button>
                                </div>
                                <div class="Clear"></div>
                                <tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />
                            </div>
                        </div>
                        <div class="Clear"></div>
                    </div>
                    <div class="GeneralCntSection">
                    	<h2 class="Title2Style">Danh sách NPP</h2>
                    	<div class="GridSection">
                        	<div class="ResultSection" id="shopMapGrid">
		                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
		                  	<table id="gridShopMap" class="easyui-datagrid"></table>
		              		</div>
                        </div>
                        
                    </div>
                     <div class="Clear"></div>
                    <div id="menuGroupProduct" class="GeneralCntSection">
                    	<h2 class="Title2Style">Danh sách nhóm <label id="NPPshopCodeLabel"></label></h2>
                    	<div class="GridSection">
                        	<div class="ResultSection" id="productGrid">
		                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
		                  	<table id="gridGroupProduct" class="easyui-datagrid"></table>
		              		</div>
                        </div>
                        
                    </div>
                </div>
                <div class="Clear"></div>
                <div class="GeneralCntSection">
            	
            	<div id="menuSecGoods">
            		<h2 class="Title2Style">Danh sách ngành hàng</h2>
            		<div class="GridSection">
	               		<div class="ResultSection" id="productGrid">
	                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
	                  	<table id="gridSecGoods"></table>
						<div id="pager"></div>
	              		</div>
                    </div>
                </div>
            	
            	<div id="menuSecGoodsChild">
            		<h2 class="Title2Style">Danh sách ngành hàng con</h2>
            		<div class="GridSection">
                      	<div class="ResultSection" id="productGrid">
	                  	<p id="gridNoResult" style="display: none" class="WarningResultStyle">Không có kết quả</p>
	                  	<table id="gridSecGoodsBrand"></table>
						<div id="pager"></div>
              			</div>
                    </div>
            	</div>
            	<div id="menuProduct">	
            	<h2 class="Title2Style">Danh sách sản phẩm</h2>
                  	<div class="GridSection">
                      	<div class="ResultSection" id="productGrid">
		                  	<table id="gridProduct" class="easyui-datagrid"></table>
              			</div>
                    </div>
              </div>
            </div>
            </div>
    <div id="popupShopMapDiv" style="visibility: hidden;">      
	    <div id="popupShopMap" class="easyui-dialog" title="Thêm NPP" style="width:616px;height:auto;" data-options="closed:true,modal:true">
	        <div class="PopupContentMid2">
	        	<div class="GeneralForm Search1Form">
<!-- 	            	<h2 class="Title2Style">Thông tin tìm kiếm</h2> -->
	                <label class="LabelStyle Label2Style">Mã đơn vị</label>
	                <input id="shopCodePop" type="text" class="InputTextStyle InputText5Style"  />
	                <label class="LabelStyle Label2Style">Tên đơn vị</label>
	                <input id="shopNamePop" type="text" class="InputTextStyle InputText5Style" />
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
	                    <button id="btnSearchShopMap" class="BtnGeneralStyle" onclick="return GroupProductPOAutoNPP.searchShopMapPop();">Tìm kiếm</button>
	                </div>
	                <div class="Clear"></div>
	            </div>
	            <div class="GeneralForm Search1Form">
	            	<h2 class="Title2Style Title2MTStyle">Danh sách đơn vị</h2>
	                <div class="GridSection" id="gridShopMapContainer">
	                <table id="gridShopMapPop"></table>                  
	                </div>
	                <div class="Clear"></div>
					<input type="checkbox" class="InputCbxStyle InputCbx2Style" id="_checkSave" style="margin-left:20px;"/>
			        <label class="LabelStyle Label8Style">Sao chép từ mặc định</label>
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
	                    <button id="btnAddShopMap" class="BtnGeneralStyle" onclick="GroupProductPOAutoNPP.addPoAutoShopMap();">Cập nhật</button>
	                </div>
	                <p id="errMsgPopShopMap" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
	            </div>
			</div>
	    </div>
    </div>
    <div id ="popupEditShopMapDiv" style="visibility: hidden;" >
	<div id="popupEditShopMap" class="easyui-dialog" title="" style="width:550px;height:210px;" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="_shopCodeLabel" class="LabelStyle Label2Style">Mã NPP<span class="ReqiureStyle">*</span></label> 
				<input id="_shopCodePop" maxlength="50" tabindex="1" type="text"  class="InputTextStyle InputText2Style" />
				<label id="_shopNameLabel" class="LabelStyle Label1Style" >Tên NPP<span class="ReqiureStyle">*</span></label> 
				<input id="_shopNamePop" type="text" maxlength="250" tabindex="2" class="InputTextStyle InputText2Style" />
				<div class="Clear"></div>
				<label class="LabelStyle Label2Style" title='Đây là trạng thái của liên kết giữa "Nhà phân phối" và "Nhóm". Không phải trạng thái của Nhà phân phối! '>Trạng thái<span class="RequireStyle" style="color:red">*</span></label>
					<div class="BoxSelect BoxSelect6" >
						<select  id="_statusPop" class="MySelectBoxClass">
								<option value="1">Hoạt động</option>
								<option value="0">Tạm ngưng</option>
							</select>
					</div>
<!--                 <label id="seachStyle1AddressLabel" style="display: none;width: 100px;" class="LabelStyle Label1Style">Địa chỉ</label> -->
<!--                 <input id="seachStyle1Address" type="text" style="display: none;width: 409px;"  maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" /> -->
                <div class="Clear"></div>
<!-- 				<input type="hidden" name="" id="searchStyle1Url"/> -->
<!-- 				<input type="hidden" name="" id="searchStyle1CodeText"/> -->
<!-- 				<input type="hidden" name="" id="searchStyle1NameText"/> -->
				<input type="hidden" name="" id="shopIdHidden"/>
<%-- 				<s:hidden id="searchStyle1AddressText"></s:hidden> --%>
				<div class="Clear"></div>
				<div class="BtnCenterSection" id="clickEvent">
					<button id="__btnSaveShopMap" class="BtnGeneralStyle" >Cập nhật</button>
				</div>
				<p id="errMsgEditShopMap" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
			</div>
		</div>
	</div>
</div>

        
    <div id="popupSecFoods" style="visibility: hidden;">      
	    <div id="popup1" class="easyui-dialog" title="Thêm ngành hàng" style="width:616px;height:auto;"data-options="closed:true,modal:true">
	        <div class="PopupContentMid2">
	        	<div class="GeneralForm Search1Form">
	            	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
	                <label class="LabelStyle Label2Style">Mã NH</label>
	                <input id="codeSecGoods" type="text" class="InputTextStyle InputText5Style" />
	                <label class="LabelStyle Label2Style">Tên NH</label>
	                <input id="nameSecGoods" type="text" class="InputTextStyle InputText5Style" />
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
	                    <button id="btnSearchNH" class="BtnGeneralStyle" onclick="return GroupProductPOAutoNPP.searchSecGoods();">Tìm kiếm</button>
	                </div>
	                <div class="Clear"></div>
	            </div>
	            <div class="GeneralForm Search1Form">
	            	<h2 class="Title2Style Title2MTStyle">Danh sách ngành hàng</h2>
	                <div class="GridSection" id="gridSecGoodsPopContainer">
	                <table id="gridSecGoodsPop"></table>                  
	                </div>
	                <div class="Clear"></div>
	                <div class="BtnCenterSection">
	                    <button id="btnAddCategory" class="BtnGeneralStyle" onclick="GroupProductPOAutoNPP.addCategory();">Cập nhật</button>
	                </div>
	                <p id="errMsgPopup1" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
	            </div>
			</div>
	    </div>
    </div>
    <div id="popupSecFoodsBrand" style="visibility: hidden;">      
    <div id="popup2" class="easyui-dialog" title="Thêm ngành hàng con" style="width:616px;height:auto;"data-options="closed:true,modal:true">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
            	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                <label class="LabelStyle Label2Style">Mã NHC</label>
                <input id="codeSecGoodsBrand" type="text" class="InputTextStyle InputText5Style" />
                <label class="LabelStyle Label2Style">Tên NHC</label>
                <input id="nameSecGoodsBrand" type="text" class="InputTextStyle InputText5Style" />
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSearchNHC" class="BtnGeneralStyle" onclick="return GroupProductPOAutoNPP.searchSecGoodsBrand();">Tìm kiếm</button>
                </div>
                <div class="Clear"></div>
            </div>
            <div class="GeneralForm Search1Form">
            	<h2 class="Title2Style Title2MTStyle">Danh sách ngành hàng con</h2>
                <div class="GridSection" id="gridSecGoodsPopBrandContainer">
                <table id="gridSecGoodsPopBrand"></table>                  
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnAddSubCategory" class="BtnGeneralStyle" onclick="GroupProductPOAutoNPP.addSubCategory();">Cập nhật</button>
                </div>
                <p id="errMsgPopup2" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
            </div>
		</div>
    </div>
    </div>
    
	</div>
	 <s:hidden id="idCode1" name="shopCode1"></s:hidden>
	 <s:hidden id="idCode2" name="shopCode2"></s:hidden>
<div id="divPopProduct" style="visibility:hidden;">	 
<div id="popupProductSearch" class="easyui-dialog" title="Thêm sản phẩm" style="width:616px;height:auto;" data-options="closed:true,modal:true">
        <div class="PopupContentMid2">
        	<div class="GeneralForm Search1Form">
            	<h2 class="Title2Style">Thông tin tìm kiếm</h2>
                <label class="LabelStyle Label2Style">Mã sản phẩm</label>
                <input id="productCode" type="text" class="InputTextStyle InputText5Style" />
                <label class="LabelStyle Label2Style">Tên sản phẩm</label>
                <input id="productName" type="text" class="InputTextStyle InputText5Style" />
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSeachProductTree" class="BtnGeneralStyle"  onclick="return GroupProductPOAutoNPP.searchProductTree();">Tìm kiếm</button>
                </div>
                <div class="Clear"></div>
            </div>
            <div class="GeneralForm Search1Form">
            	<h2 class="Title2Style Title2MTStyle">Cây sản phẩm</h2>
            	<div  style="max-height:250px;overflow:auto;">
            		<div id="treeNoResult"></div>
	                <div class="ReportTreeSection ReportTree2Section" >
	                	  <div id="rpSection"></div>
	                      <ul id="tree" class="easyui-tree" style="visibility:hidden;"></ul>
	                </div>
                </div>
                <div class="Clear"></div>
                <div class="BtnCenterSection">
                    <button id="btnSaveProduct" class="BtnGeneralStyle" onclick="return GroupProductPOAutoNPP.createGroupProductPOAutoNPPDetail();">Cập nhật</button>
                </div>
                <p id="errMsgPopup3" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
            </div>
		</div>
    </div>
 </div>
	<div id ="searchStyle1EasyUIDialogDiv" style="width:550px;display: none" >
	<div id="searchStyle1EasyUIDialogCreate" class="easyui-dialog" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="seachStyle1CodeLabel" class="LabelStyle Label2Style">Mã nhóm<span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Code" maxlength="50" tabindex="1" type="text"  class="InputTextStyle InputText2Style" />
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style" >Tên nhóm<span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Name" type="text" maxlength="250" tabindex="2" class="InputTextStyle InputText2Style" />
				<div class="Clear"></div>
				<label class="LabelStyle Label2Style">Trạng thái<span class="RequireStyle" style="color:red">*</span></label>
					<div class="BoxSelect BoxSelect6">
						<select id="statusDialog" class="MySelectBoxClass">
								<option value="1" selected="selected">Hoạt động</option>
								<option value="0">Tạm ngưng</option>
							</select>
					</div>
                <label id="seachStyle1AddressLabel" style="display: none;width: 100px;" class="LabelStyle Label1Style">Địa chỉ</label>
                <input id="seachStyle1Address" type="text" style="display: none;width: 409px;"  maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
				<input type="hidden" name="" id="searchStyle1Url"/>
				<input type="hidden" name="" id="searchStyle1CodeText"/>
				<input type="hidden" name="" id="searchStyle1NameText"/>
				<input type="hidden" name="" id="searchStyle1IdText"/>
				<s:hidden id="searchStyle1AddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="BtnCenterSection" id="clickEvent">
					<button id="__btnSave" class="BtnGeneralStyle">Cập nhật</button>
					<button id="__btnClose" class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearchCreate" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
			</div>
		</div>
	</div>
</div>
<div id ="searchStyle1EasyUIDialogDivUpdate" style="width:550px;display: none" >
	<div id="searchStyle1EasyUIDialogUpdate" class="easyui-dialog" title="" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label id="seachStyle1CodeLabel" class="LabelStyle Label2Style">Mã nhóm<span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Code" maxlength="50" tabindex="1" type="text"  class="InputTextStyle InputText2Style" />
				<label id="seachStyle1NameLabel" class="LabelStyle Label1Style" >Tên nhóm<span class="ReqiureStyle">*</span></label> 
				<input id="seachStyle1Name" type="text" maxlength="250" tabindex="2" class="InputTextStyle InputText2Style" />
				<div class="Clear"></div>
				<label class="LabelStyle Label2Style">Trạng thái<span class="RequireStyle" style="color:red">*</span></label>
					<div class="BoxSelect BoxSelect6" >
						<select  id="statusDialogUpdate" class="MySelectBoxClass">
								<option value="1">Hoạt động</option>
								<option value="0">Tạm ngưng</option>
							</select>
					</div>
                <label id="seachStyle1AddressLabel" style="display: none;width: 100px;" class="LabelStyle Label1Style">Địa chỉ</label>
                <input id="seachStyle1Address" type="text" style="display: none;width: 409px;"  maxlength="250" tabindex="3" class="InputTextStyle InputText1Style" />
                <div class="Clear"></div>
				<input type="hidden" name="" id="searchStyle1Url"/>
				<input type="hidden" name="" id="searchStyle1CodeText"/>
				<input type="hidden" name="" id="searchStyle1NameText"/>
				<input type="hidden" name="" id="searchStyle1IdText"/>
				<s:hidden id="searchStyle1AddressText"></s:hidden>
				<div class="Clear"></div>
				<div class="BtnCenterSection" id="clickEvent">
					<button id="__btnSave1" class="BtnGeneralStyle">Cập nhật</button>
					<button id="__btnClose1" class="BtnGeneralStyle BtnGeneralMStyle" onclick="$('.easyui-dialog').dialog('close');">Đóng</button>
				</div>
				<p id="errMsgSearchUpdate" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	GroupProductPOAutoNPP._mapNode = new Map();
	GroupProductPOAutoNPP._mapCheck = new Map();
	$('#shopCode').focus();
	var status = $('#status').val();
	$("#gridShopMap").datagrid({
		  url:GroupProductPOAutoNPP.getGridUrl('','',status),
	  	  pageList  : [10,20,30],
		  width: ($('#productGrid').width()),
		  pageSize : 10,
		  checkOnSelect :true,
		  height:'auto',
		  scrollbarSize : 0,
		  pagination:true,			  
		  fitColumns:true,		     
	      singleSelect:true,
		  method : 'GET',
		  rownumbers: true,
		  columns:[[	
			{field: 'shopCode', title: 'Mã NPP', width: 100, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field: 'shopName', title: 'Tên NPP', width: 340, sortable:false,resizable:false, align: 'left', formatter: function(value, row, index){
		    	return Utils.XSSEncode(value);
		    }},
		    {field: 'groupStatus', title: 'Trạng thái liên kết', width: 80, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
		    	if(row.groupStatus == 1){
		    		return 'Hoạt động';
		    	}
		    	if(row.groupStatus == 0){
		    		return 'Tạm ngưng';
		    	}
		    }},
		    {field: 'groupProduct', title: 'Nhóm SP', width: 30, sortable:false,resizable:false, align: 'center',formatter:function(value,row,index){
		    	var data = '<a href="javascript:void(0);" onclick="return GroupProductPOAutoNPP.searchGroupProduct('+row.groupId+',\''+Utils.XSSEncode(row.shopCode)+'\');"><img src="/resources/images/icon-view.png"></a>';
		    	return getFormatterControlGrid('btnDetailGrid', data);
		    	
		    }},
		    {field: 'edit', title: '<a id = "btnAdd" href="javascript:void(0);"  onclick="return GroupProductPOAutoNPP.openDialogAddShopMap();"><img src="/resources/images/icon-add.png"></a>', width: 30, align: 'center',sortable:false,resizable:false,
		    formatter:function(value,row,index){
		    	var data =  '<a href="javascript:void(0);" onclick="return GroupProductPOAutoNPP.openDialogEditShopMap('+row.groupId+',\''+Utils.XSSEncode(row.shopCode)+'\',\''+Utils.XSSEncode(row.shopName)+'\',\''+row.groupStatus+'\');"><img src="/resources/images/icon-edit.png"></a>';
		    	return getFormatterControlGrid('btnEditGrid', data);
		    }},	
// 		    {field: 'id', index: 'id', hidden: true},
// 		    {field: 'groupId', index: 'groupId', hidden: true},
// 		    {field: 'groupStatus', index: 'groupStatus', hidden: true},	
		  ]],
		  onLoadSuccess:function(){
				$('.datagrid-header-rownumber').html('STT');  
				showIconAddOfFirstControlGrid('btnAddGrid', 'btnAdd');
		  }
	});
	
	/* $('#shopCode').bind('keydown',function(event){
		if(event.keyCode == 13){
			GroupProductPOAutoNPP.search();
		}
	});
	$('#shopName').bind('keydown',function(event){
		if(event.keyCode == 13){
			GroupProductPOAutoNPP.search();
		}
	}); */
	$('#shopCodePop').bind('keydown',function(event){
		if(event.keyCode == 13){
			GroupProductPOAutoNPP.searchShopMapPop();
		}
	});
	$('#shopNamePop').bind('keydown',function(event){
		if(event.keyCode == 13){
			GroupProductPOAutoNPP.searchShopMapPop();
		}
	});
	$('#menuGroupProduct').hide();
	$('#menuSecGoods').hide();
	$('#menuSecGoodsChild').hide();
	$('#menuProduct').hide();
	$('#popupProduct').hide();
	Utils.bindAutoButtonEx('.easyui-dialog','btnSeachProductTree');
	Utils.bindAutoButtonEx('.easyui-dialog','btnSearchNH');
	Utils.bindAutoButtonEx('.easyui-dialog','btnSearchNHC');
});

</script>
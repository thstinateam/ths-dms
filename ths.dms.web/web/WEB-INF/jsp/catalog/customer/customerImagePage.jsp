<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<script src="/resources/scripts/plugins/lazyload/jquery.lazyload.min.js" type="text/javascript"></script>
<s:if test="roleViewImageCustomer==0">Bạn không có quyền xem hình ảnh khách hàng.</s:if>

<s:else>
<s:if test="status==0" >Chưa có hình ảnh</s:if>
<ul class="ResetList CustomerPhotoList FixFloat">
	<s:if test="mediaItemStore!=null">
	<li><a class="fancybox" onclick="return CustomerCatalog.showCustomerImageDetail(0)"><span class="BoxFrame"><span class="BoxMiddle">
			<img src="<s:property value="mediaItemStore.thumbUrl" />" width="192" height="126" /> </span> </span> 
		</a>
		<p class="Text1Style"><a href="JavaScript : void(0);" onclick="return CustomerCatalog.showCustomerImageDetail(0)">Hình ảnh đóng cửa</a></p>
		<p class="Text2Style"><s:property value="listMediaItemStore.size()" /> hình ảnh</p></li>
	</s:if>
		
	<s:if test="mediaItemDisplay!=null">
	<li><a class="fancybox" onclick="return CustomerCatalog.showCustomerImageDetail(1)" ><span class="BoxFrame"><span class="BoxMiddle">
			<img src="<s:property value="mediaItemDisplay.thumbUrl" />" width="192" height="126" /> </span> </span> 
		</a>
		<p class="Text1Style"><a href="JavaScript : void(0);" onclick="return CustomerCatalog.showCustomerImageDetail(1)">Hình ảnh trưng bày</a></p>
		<p class="Text2Style"><s:property value="listMediaItemDisplay.size()" /> hình ảnh</p></li>
	</s:if>
	
	<s:if test="mediaItemSalePoint!=null">
	<li><a class="fancybox" onclick="return CustomerCatalog.showCustomerImageDetail(2)" ><span class="BoxFrame"><span class="BoxMiddle">
			<img src="<s:property value="mediaItemSalePoint.thumbUrl" />" width="192" height="126" /> </span> </span> 
		</a>
		<p class="Text1Style"><a href="JavaScript : void(0);" onclick="return CustomerCatalog.showCustomerImageDetail(2)">Hình ảnh điểm bán</a></p>
		<p class="Text2Style"><s:property value="listMediaItemSalePoint.size()" /> hình ảnh</p></li>
	</s:if>
</ul>

<div style="display:none;" id="customerImageDetail">
		<div class="GeneralDialog General4Dialog">
        	<div class="DialogPhotoSection">
            	<div id="itemImage" class="PhotoCols1">
                	<div class="PhotoCols1Info1">
<!--                     	<p class="LeftStyle Sprite1"></p> -->
                    	<div class="PhotoLarge"><span class="BoxFrame"><span class="BoxMiddle">
                    		<img id="imageBig" width="580" height="426" src="">
                    		<img style="display:none" id="imageLoading" width="50" height="50" src="/resources/images/loading-large.gif">
                    	</span></span></div>                    	
<!--                         <p class="RightStyle Sprite1"></p> -->
                        <div id="itemImageMap" class="Map2Section"  style="display:none;width:360px; height:208px; right:0px; bottom:0px;">
                        	
                        </div>
                        <p id="extendMapOn" class="OnFucnStyle" onclick="CustomerCatalog.extendMap(1)"><span class="HideText Sprite1">Mở rộng</span></p>
                        <p style="display:none" id="extendMapOff" class="OffFucnStyle" onclick="CustomerCatalog.extendMap(0)"><span class="HideText Sprite1">Thu nhỏ</span></p>
                    </div>
                    <div class="PhotoCols1Info3">
                    	<p><strong id="itemImageCreateUser"></strong> chụp ngày <strong id="itemImageCreateDate"></strong></p>                    	
                    </div>
                </div>
                <div class="PhotoCols2">
                	<div id="listImage" class="DScrollpane">                		
                        
                	</div>                	
                </div>
                <div class="Clear"></div>
                <div onclick="$.fancybox.close();" class="fancybox-item fancybox-close" title="Close"></div>                
        	</div>
        </div>
    </div>
   </s:else>
   
<script type="text/javascript">
$(function(){
	var sysDate = '<s:date name="sysDate" format="dd/MM/yyyy" />';
	var customer = '  Khách hàng - (<s:property value="code" /> - <s:property value="name" />)';
	$('#tabTitle').html(sysDate+customer);
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<input type="hidden" id="function_code" value="<s:property value="systimestamp"/>" />
<s:set id="downloadToken" value="%{generateReportToken()}"></s:set>
<input type="hidden" id="report_token" value="<s:property value="#downloadToken"/>" />
<s:set id="downloadToken" value="%{putSessionValue(systimestamp, #downloadToken)}"></s:set>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);"><s:text name="catalog_sales_brand_catalogy"/></a>
		</li>
		<li><span><s:text name="common.catalog.manage-customer"/></span>
		</li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style"><s:text name="catalog.customer.search.info"/></h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style"><s:text name="catalog.customer.org"/>(F9)</label> 
					<input id="txtShopCode" type="text" class="InputTextStyle InputText1Style"  tabindex="1" autocomplete="off" maxlength="50"/> 
					<label class="LabelStyle Label1Style"><s:text name="catalog.customer.code"/></label> 
					<input id="customerCode" type="text" class="InputTextStyle InputText1Style"  tabindex="2" autocomplete="off" maxlength="50"/> 
					<label class="LabelStyle Label1Style"><s:text name="catalog.customer.name"/></label> 
					<input id="customerName" type="text" class="InputTextStyle InputText1Style" tabindex="3" autocomplete="off" maxlength="250"/> 
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style"><s:text name="catalog.customer.type"/></label>
					<div class="BoxSelect BoxSelect2">
						<select class="easyui-combobox" id="customerType" name="customerType" tabindex="4" data-options="
								panelWidth : '206'
							">
							<option value=""></option>
   							<s:iterator value="lstCustomerType" var="obj">
   								<option value="<s:property value="id" />"><s:property value="channelTypeCode" /> - <s:property value="channelTypeName" /></option>		                			
	                		</s:iterator>
						</select>
					</div>
					<label class="LabelStyle Label1Style"><s:text name="catalog.address"/></label>
					<input id="address" type="text" class="InputTextStyle InputText1Style" tabindex="5" autocomplete="off" maxlength="250"/> 
					<label class="LabelStyle Label1Style"><s:text name="catalog.customer.phone"/></label> 
					<input id="phone" type="text" class="InputTextStyle InputText1Style" tabindex="6" autocomplete="off" maxlength="50"/> 
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style"><s:text name="catalog.customer.status"/></label>
					<div class="BoxSelect BoxSelect2">
						<%-- <s:select id="status" headerKey="-2" headerValue="--Chọn trạng thái--" value="1" name="status" tabindex="7" cssClass="MySelectBoxClass" list="lstStatusBean" listKey="value" listValue="name"></s:select> --%>                                
						<s:select id="status" headerKey="-2" headerValue="--Chọn trạng thái--" value="1" name="status" tabindex="7" cssClass="MySelectBoxClass" list="lstStatus" listKey="value" listValue="name"></s:select>
					</div>
					<label class="LabelStyle Label1Style"><s:text name="catalog.customer.tansuat"/></label> 
					<input id="tansuat" type="text" class="InputTextStyle InputText1Style vinput-number" tabindex="8" autocomplete="off" maxlength="50"/>
					<div class="Clear"></div>
					<div class="BtnCenterSection">
						<button id="btnSearch" class="BtnGeneralStyle" onclick="CustomerCatalog.search();" tabindex="8"><s:text name="catalog.customer.search"/></button>
					</div>
					<div class="Clear"></div>
				</div>
			</div>
			<div class="Clear"></div>
		</div>
		<div class="GeneralCntSection">
			<h2 class="Title2Style"><s:text name="catalog.customer.list"/></h2>
			<div class="GridSection" id="customerContainerGrid">
				 <table id="customerDatagrid"></table>
			</div>
			<div class="BtnCenterSection SProduct1Form" id="divBtn">
 				<button id="group_insert_btnAgree" class="BtnGeneralStyle cmsiscontrol" onclick="CustomerCatalog.agreeCustomer(1);" tabindex="9"><s:text name="catalog.customer.approve"/></button>
 				<button id="group_insert_btnDeny" class="BtnGeneralStyle cmsiscontrol" onclick="CustomerCatalog.agreeCustomer(3);" tabindex="10"><s:text name="catalog.customer.reject"/></button>
 				<button id="group_insert_btnDelete" class="BtnGeneralStyle cmsiscontrol" onclick="CustomerCatalog.agreeCustomer(-1);" tabindex="11"><s:text name="catalog.customer.delete"/></button>
				<p id="successMsg" class="SuccessMsgStyle" style="display: none"></p>
				<p id="errMsg" class="ErrorMsgStyle" style="display: none"></p>
			</div>
			<s:if test="hasEditPermission">
				<div class="GeneralForm GeneralNoTP1Form">
                	<div class="BoxSelect BoxSelect6" style="margin-right: 10px">
                        <select class="MySelectBoxClass" name="excelType" id="excelType" onchange="excelTypeChanged();">
                            <option value="1"><s:text name="catalog.customer"/></option>
                            <option value="2"><s:text name="catalog.customer.cat"/></option>
                        </select>
                    </div>
                    <div class="Func1Section cmsiscontrol" id="group_import">
                        <p class="DownloadSFileStyle DownloadSFile2Style">
                        	<a href="javascript:void(0)" id="downloadTemplate" class="Sprite1"><s:text name="unit_tree.search_unit.search_grid.import.download_file"/></a>
                        </p> 
                        <form action="/catalog_customer_mng/import-excel" name="importFrm" style="float: left;" id="importFrm"  method="post" enctype="multipart/form-data">
                        	<input type="hidden" name="token" id="tokenImport" value='<s:property value="token"/>'>
                        	<input type="hidden" id="isView" name="isView"/>
                        	<input type="file" class="InputFileStyle" size="22" name="excelFile" id="excelFile" onchange="previewImportExcelFile(this,'importFrm');">
                        	<div class="FakeInputFile">
								<input id="fakefilepc" style="margin: -3px 5px 0 0;" readonly="readonly" type="text" class="InputTextStyle InputText1Style">
							</div>
                        </form>
                        <button id="btnImportExcel" class="BtnGeneralStyle" onclick="return CustomerCatalog.importExcel();"><s:text name="unit_tree.search_unit.search_grid.import_button_text"/></button>
                    </div>
                    <button id="btnExportExcel" onclick="return CustomerCatalog.exportExcelCustomer();" class="BtnGeneralStyle"><s:text name="unit_tree.search_unit.search_grid.export_button_text"/></button>
                    <div class="Clear"></div>
                </div>
			</s:if>
			
		</div>
	</div>
	<div class="Clear"></div>
</div>
<div style="display: none;">
	<tiles:insertTemplate template='/WEB-INF/jsp/catalog/customer/confirmDuplicateCustomerDialog.jsp'/>
</div>

<p id="errExcelMsg" class="ErrorMsgStyle" style="display: none"></p>
<div id="responseDiv" style="display: none"></div>
<s:hidden id="selId" value="0"></s:hidden>
<s:hidden id="shopIdHidden" name="shopIdHidden"></s:hidden>
<s:hidden id="hasEditPermission" name="hasEditPermission"></s:hidden>
<script type="text/javascript">
	$(document).ready(function() {
		$('#customerCode, #customerName, #customerType, #address, #phone, #status').bind('keyup',function(event){
			if(event.keyCode == keyCodes.ENTER){
				$('#btnSearch').click(); 
			}
		});
		$('#layoutContent').removeClass('Content').addClass('MainContent');

		CustomerCatalog.bindShopCodeChange();
		$('#shopCode').change();
		var excelType = $('#excelType').val();
		var options = {
			beforeSubmit : CustomerCatalog.beforeImportExcel,
			success : CustomerCatalog.afterImportExcel2,
			type : "POST",
			dataType : 'html',
			data : {
				excelType : excelType
			}
		};
		disabledSelect();
		$('#shopCode').bind('keyup', function(event) {
			console.log(event.keyCode);
			if (event.keyCode == keyCode_F9) {
				var lstParam = new Array();
				var obj = {};
				obj.name = 'filterShop';
				obj.value = 1;
				lstParam.push(obj);
				CommonSearch.searchShopOnDialog(function(data) {
					$('#shopCode').val(data.code);
					$('#shopCode').change();
				}, lstParam);
			}
		});
		disabledSelect();
		$('#shopCode').bind('keyup', function(event) {
			if (event.keyCode == 8) {
				disabledSelect();
			}
		});
		$('#shopCode').bind('change', function(event) {
			disabledSelect();
		});
		//CustomerCatalog.bindShopCodeChange();
		$('#customerCode').focus();
		$('#provinceCode').bind('keyup', function(event) {
			var value = $('#provinceCode').val();
			if (event.keyCode == 9) {
				if (value != undefined && value != '' && value != null) {
					enable('districtCode');
					$('#districtCode').val('');
					//$('#districtCode').focus();
				}
			}
		});
		$('#customerCode').focus();
		$('#importFrm').ajaxForm(options);
		
		$('#customerDatagrid').datagrid({
			url: '/catalog_customer_mng/search',
			width : $(window).width() - 35,
			queryParams:{status : $('#status').val()},
			scrollbarSize:0,
			fitColumns : true,
			singleSelect  :true,
			selectOnCheck: false,
			checkOnSelect: false,
			pagination : true,
		    rownumbers : true,
			columns:[[
				{field: 'shopName', title: 'Đơn vị', width: 100, align: 'left', sortable: true, formatter: function(value, row, index) {
					return Utils.XSSEncode(row.shop.shopName);
				}},
				{field:'shortCode',title:Utils.language_vi('catalog.customer.code'),width:100,align:'left', sortable:true,formatter: function(value, row, index){
					return Utils.XSSEncode(value);
				}},
			    {field:'customerName',title:Utils.language_vi('catalog.customer.name'),width:250,align:'left', sortable:true,formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'channelType',title:Utils.language_vi('catalog.customer.type'),width:150,align:'left', sortable:true,formatter: function(value, row, index){
			    	return Utils.XSSEncode(value? value.channelTypeName:"");
			    }},
			    {field:'mobiphone',title:Utils.language_vi('catalog.customer.phone'),width:150,align:'left', sortable:true,formatter: function(value, row, index){
			    	var str='';
			    	if(row.mobiphone!=null && row.mobiphone!='' && row.phone!=null && row.phone!=''){
			    		str=row.mobiphone+' / '+row.phone;
			    	}else if(row.mobiphone!=null && row.mobiphone!=''){
			    		str=row.mobiphone;
			    	}else if(row.phone!=null && row.phone!=''){
			    		str=row.phone;
			    	}
			    	return Utils.XSSEncode(str);
			    }},
			    {field:'address',title:Utils.language_vi('catalog.address'),width:330,align:'left', sortable:true,formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'frequency',title:catalog_customer_tansuat,width:50,align:'right', sortable:true,formatter: function(value, row, index){
			    	return Utils.XSSEncode(value);
			    }},
			    {field:'status',title:Utils.language_vi('catalog.customer.status'),width:100,align:'left', sortable:true,formatter: function(value, row, index){
			    	var str = '';
			    	if(row.status == 'RUNNING') {
			    		str = Utils.language_vi('catalog.customer.active');
			    	} else if (row.status == 'WAITING') {
			    		str = Utils.language_vi('catalog.customer.draf');
			    	} else if (row.status == 'REJECTED') {
			    		str = Utils.language_vi('catalog.customer.reject');
			    	} else if (row.status == 'STOPPED') {
			    		str = Utils.language_vi('catalog.customer.pausing');
			    	}
			    	return str;
			    }},
			    {field: 'routes', title: 'Tuyến', width: 100, align: 'left', sortable: true, formatter: function(value, row, index) {
			    	return Utils.XSSEncode(row.routes);
			    }},
			    {field: 'createUser', title: 'Người tạo', width: 100, align: 'left', sortable: true, formatter: function(value, row, index) {
			    	return Utils.XSSEncode(row.createUser) + '<br><span style="font-size:0.9em;">' + Utils.XSSEncode(row.createUserName) + '</span>';
			    }},
			    {field:'id', title:'<a class="cmsiscontrol"  id="group_insert_grid" href="/catalog_customer_mng/change"><img src="/resources/images/icon_add.png"/></a>', width:40, align:'center', formatter: function(value, row, index) {
			    	return '<a href="javascript:void(0);" onclick="changeCustomer('+row.id+');"><img src="/resources/images/icon-edit.png"></a>';
			    }},
			    {field:'checkAll', title:'<input id="checkAll" onclick="CustomerCatalog.checkAll(this);" type="checkbox" >', width:40, align:'center', formatter: function(value, row, index) {
			    	var status = $('#status').val();
			    	if(status != -2) {
			    		if(row.status == 'REJECTED' || row.status == 'WAITING' ) {
			    			return '<input class="ck" type="checkbox" value = "'+ row.id+'" onclick="isCheckAll(this)">';
			    		}
			    	}
			    }},
			    {field:'P',hidden: true}
			]],
	        onLoadSuccess :function(){
		    	$('#customerContainerGrid .datagrid-header-rownumber').html(Utils.language_vi('catalog.customer.no'));
		    	updateRownumWidthForDataGrid('#customerContainerGrid');  
		    	$("#checkAll").prop("checked", false);
		    	var status = $('#status').val();
				if(status == 3) {
					$("#divBtn").show();
					$("#group_insert_btnAgree").hide();
					$("#group_insert_btnDeny").hide();
			    } else if(status == 1 || status == -2 || status == 0) {
					$("#divBtn").hide();
			    } else if(status == 2) {
					$("#divBtn").show();
					$("#group_insert_btnAgree").show();
					$("#group_insert_btnDeny").show();
					$("#group_insert_btnDelete").show();
			    }
		    	$("#checkAll").prop("checked", false);
		    	Utils.functionAccessFillControl('content', function (data) {
		    		//Xu ly cac thanh phan lien quan den control phan quyen
		    	});
	    	}
		});
		$('#txtShopCode').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
					params : {
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:Utils.language_vi('catalog.customer.org.code')},
				        {id:'name', maxlength:250, label:Utils.language_vi('catalog.customer.org.name')},
				    ],
				    url : '/commons/search-shop-show-list',
				    columns : [[
				        {field:'shopCode', title:Utils.language_vi('catalog.customer.org.code'), align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
				        {field:'shopName', title:Utils.language_vi('catalog.customer.org.name'), align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index) {
							return Utils.XSSEncode(value);
						}},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="PriceManager.fillTxtShopCodeByF9(\''+Utils.XSSEncode(row.shopCode)+'\');"><s:text name="catalog.customer.select"/></a>';         
				        }}
				    ]]
				});
			}
		});
	});

	if ($('#hasEditPermission').val().trim() == 'true') {
		var excelType = $('#excelType').val();
		$('#downloadTemplate').attr('onclick', 'CustomerCatalog.exportExcelTemplate()');
// 				'href',
// 				excel_template_path
// 						+ 'catalog/Bieu_mau_danh_muc_khachhang_import.xls');
		var options = {
			beforeSubmit : CustomerCatalog.beforeImportExcel,
			success : CustomerCatalog.afterImportExcel2,
			type : "POST",
			dataType : 'html',
			data : {
				excelType : excelType
			}
		};
		$('#importFrm').ajaxForm(options);
	}
	$('#provinceCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			$('#seachStyle1Code').removeAttr('disabled');
			CommonSearch.searchProvinceOnDialog(function(data) {
				$('#provinceCode').val(data.code);
				provinceCodeChanged();
				//$('#provinceCode').focus();
			});
		}
	});
	$('#districtCode').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			var lstParam = new Array();
			var obj = {};
			obj.name = 'provinceCode';
			obj.value = $('#provinceCode').val().trim();
			lstParam.push(obj);
			CommonSearch.searchDistrictOnDialog(function(data) {
				$('#districtCode').val(data.code);
				//$('#districtCode').focus();
			}, lstParam);
		}
	});
	function changeCustomer(id){
		window.location.href = '/catalog_customer_mng/change?id=' + Number(id);
	};
	
	function provinceCodeChanged() {
		$('#districtCode').val('');
		if ($('#provinceCode').val().trim().length == 0) {
			$('#districtCode').attr('disabled', 'disabled');
			//$('#address').focus();
		} else {
			$('#districtCode').removeAttr('disabled');
			//$('#districtCode').focus();
		}
	};

	function excelTypeChanged() {
		var excelType = $('#excelType').val();
		if ($('#excelType').val() == 1) {
			$('#downloadTemplate').attr('onclick', 'CustomerCatalog.exportExcelTemplate()');
			$('#downloadTemplate').attr('href', 'javascript:void(0);');
// 					'href',
// 					excel_template_path + 'catalog/Bieu_mau_danh_muc_khachhang_import.xls');
			var options = {
				beforeSubmit : CustomerCatalog.beforeImportExcel,
				success : CustomerCatalog.afterImportExcel2,
				type : "POST",
				dataType : 'html',
				data : {
					excelType : excelType
				}
			};
			$('#importFrm').ajaxForm(options);
		} else {
			$("#downloadTemplate").removeAttr("onclick");
// 			$('#downloadTemplate').attr('href',excel_template_path + 'catalog/Bieu_mau_danh_muc_nganhhangcuakhachhang_import.xls');
			$('#downloadTemplate').attr('onclick', 'CustomerCatalog.exportExcelTemplateCategory()');
			$('#downloadTemplate').attr('href', 'javascript:void(0);');
			var options = {
				beforeSubmit : CustomerCatalog.beforeImportExcel,
				success : CustomerCatalog.afterImportExcel3,
				type : "POST",
				dataType : 'html',
				data : {
					excelType : excelType
				}
			};
			$('#importFrm').ajaxForm(options);
		}
		$('#excelFile').val('');
		$('#fakefilepc').val('');
	}
	
	function disabledSelect() {
		var value = $('#shopCode').val();
		if (value == "" || value == undefined || value == null) {
			disabled('groupTransfer');
			$('#groupTransfer').parent().addClass('BoxDisSelect');
		} else {
			enable('groupTransfer');
			$('#groupTransfer').parent().removeClass('BoxDisSelect');
		}
	}
	$('#customerType').combobox({
		valueField : 'customerType',
		textField : 'customerType',
		formatter: function(row) {
			return Utils.XSSEncode(row.customerType);
		},
		mode:'local',
		filter: function(q, row){
			var opts = $(this).combobox('options');
			return (row[opts.textField].toUpperCase()).indexOf(q.toUpperCase().trim())>=0;
		}
	});
	
	function isCheckAll(check) {
		if(check.checked) {
			var fullCheck = true;
			$('.ck').each(function() {
				if($(this).prop('checked') == false)
					fullCheck = false;
			});
 			$("#checkAll").prop("checked", fullCheck);
 		} else if(!check.checked) {
 			$("#checkAll").prop("checked", false);
 		}
	};
</script>

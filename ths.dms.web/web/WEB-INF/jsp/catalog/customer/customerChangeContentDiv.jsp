<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/catalog_customer_mng/info"><s:text name="catalog.customer"/></a></li>
		<s:if test="%{createNew}">
			<li><span><s:text name="catalog.customer.create.new"/></span></li>
		</s:if>
		<s:else>
			<li><span><s:text name="catalog.customer.update"/></span></li>
		</s:else>
		
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<div class="TabSection">
					<ul class="ResetList TabSectionList">
						<li><a href="javascript:void(0);" onclick="CustomerCatalog.showTab1()" id="infoTab" class="Sprite1 Active"><span class="Sprite1"><s:text name="catalog.customer.info"/></span> </a></li>
						<%-- <li><a href="javascript:void(0);" onclick="CustomerCatalog.showTab2()" id="propertyTab" class="Sprite1"><span class="Sprite1">Thuộc tính khách hàng</span> </a></li> --%>
						<li><a href="javascript:void(0);" onclick="CustomerCatalog.showTab3()" id="catInfoTab" class="Sprite1"><span class="Sprite1"><s:text name="catalog.customer.cat.info"/></span> </a></li>
					</ul>
					<div class="Clear"></div>
				</div>
				<div id="infoTabContainer">
					<h2 class="Title2Style"><s:text name="catalog.customer.common.info"/></h2>
					<div class="SearchInSection SProduct1Form">
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.org"/> (F9) <span class="ReqiureStyle">*</span></label>
						<!-- tao moi va ko phai la npp -->
						<s:if test="%{(cusShopCode == null || cusShopCode == '') && createNew}">
							<input id="txtShopCode" type="text" class="InputTextStyle InputText1Style" tabindex="1" autocomplete="off" maxlength="50" onblur="CustomerCatalog.fillTxtShopCodeByF9(null, true);" />
						</s:if>
						<!-- tao moi va la npp -->
						<s:elseif test="{(cusShopCode != null || cusShopCode != '') && createNew}" >
							<input id="txtShopCode" type="text" class="InputTextStyle InputText1Style" tabindex="1" disabled autocomplete="off" maxlength="50" value="<s:property value='shopCode' />" />
						</s:elseif>
						<!-- cap nhat va trang thai la Tao moi hoac Tu choi -->
						<s:elseif test="{(statusCustomer == 1 || statusCustomer == 0 || statusCustomer == 2 || statusCustomer == 3) && !createNew}" >
							<input id="txtShopCode" type="text" class="InputTextStyle InputText1Style" tabindex="1" disabled autocomplete="off" maxlength="50" value="<s:property value='shopCode' />" />
						</s:elseif>

						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.code"/></label>
						<s:if test="%{(statusCustomer == 1 || statusCustomer == 0) && !createNew}">
							<input value='<s:property value="customerCode"/>' disabled type="text" id="shortCode" class="InputTextStyle InputText1Style" />
 						</s:if>
 						<s:elseif test="%{statusCustomer == 2 && !createNew}">
 							<input value='<s:property value="customerCode"/>' type="text" id="shortCode" class="InputTextStyle InputText1Style" />
 						</s:elseif>
 						<s:elseif test="%{createNew}">
 							<input value='<s:property value="customerCode"/>' disabled type="text" id="shortCode" class="InputTextStyle InputText1Style" />
 						</s:elseif>
						
						<label class="LabelStyle Label6Style"><s:text name="catalog.customer.name"/> <span class="ReqiureStyle">*</span></label> 
						<input id="customerName" value='<s:property value="customerName"/>' type="text" class="InputTextStyle InputText1Style" maxlength="250"/>
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.contact"/></label> 
						<input id="contact" value='<s:property value="contact"/>' type="text" class="InputTextStyle InputText1Style" maxlength="250"/>
						 
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.house.number"/></label> 
						<input onkeyup="CustomerCatalog.addressChange();" id="addressNumber" value='<s:property value="addressNumber"/>' type="text" class="InputTextStyle InputText1Style" maxlength="100"/>
						<label class="LabelStyle Label6Style"><s:text name="catalog.customer.street.label"/></label> 
						<input onkeyup="CustomerCatalog.addressChange();" id="street" value='<s:property value="street"/>' type="text" class="InputTextStyle InputText1Style" maxlength="200"/>

						<%--<label class="LabelStyle Label1Style">Địa bàn<span class="ReqiureStyle">*</span></label> 
						<input id="areaTree" style="width: 206px" type="text" class="InputTextStyle InputText1Style" />--%>
<!-- 						<input type="hidden" id="areaId"/> -->
						<div class="Clear"></div>
						
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.provices"/> <span class="ReqiureStyle">*</span></label> 
						<div class="BoxSelect BoxSelect2">
							<select id="province" class="MySelectBoxClas">
								<option value="-2" areaId="" centerCode=""></option>
								<s:iterator value="lstProvince" var="prov">
								
									<s:if test="#ward.areaCode.equals(provinceCode)">
										<option selected="selected" value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
									</s:if>
									<s:else>
										<option value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
									</s:else>
								</s:iterator>
							</select>
						</div>
						
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.districts"/> <span class="ReqiureStyle">*</span></label>
						<div class="BoxSelect BoxSelect2">
						<s:if test="lstDistrict == null">
							<select id="district" class="MySelectBoxClas">
							</select>
						</s:if>
						<s:else>
							<select id="district" class="MySelectBoxClas">
								<s:iterator value="lstDistrict" var="dist">
									<s:if test="#dist.areaCode.equals(districtCode1)">
										<option selected="selected" value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
									</s:if>
									<s:else>
										<option value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
									</s:else>
								</s:iterator>
							</select>
						</s:else>
						</div>
						
						<label class="LabelStyle Label6Style"><s:text name="catalog.customer.areas"/> <span class="ReqiureStyle">*</span></label>
						<div class="BoxSelect BoxSelect2">
						<s:if test="lstPrecinct == null">
							<select id="precinct" class="MySelectBoxClas">
							</select>
						</s:if>
						<s:else>
							<select id="precinct" class="MySelectBoxClas">
								<s:iterator value="lstPrecinct" var="ward">
									<s:if test="#ward.areaCode.equals(wardCode)">
										<option selected="selected" value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
									</s:if>
									<s:else>
										<option value="<s:property value='areaCode' />" areaId="<s:property value='id' />" centerCode="<s:property value='centerCode' />"><s:property value="areaName" /></option>
									</s:else>
								</s:iterator>
							</select>
						</s:else>
						</div>
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.type"/> <span class="ReqiureStyle">*</span></label>
						<div class="BoxSelect BoxSelect2">
							<select id="customerType" class="MySelectBoxClas easyui-combobox" data-options="panelWidth : '206'">
								<s:iterator value="listCustomerType">
									<s:if test="channelTypeCode.equals(customerType)">
										<option value='<s:property value="channelTypeCode" />' selected="selected"><s:property value="channelTypeCode" />-<s:property value="channelTypeName" /></option>
									</s:if>
									<s:else>
										<option value='<s:property value="channelTypeCode" />'><s:property value="channelTypeCode" />-<s:property value="channelTypeName" /></option>
									</s:else>
								</s:iterator>
							</select>
						</div>
						<div class="Clear"></div>
						<div class="SProductForm4Cols SProductForm6Cols">
							<div class="PopupContentMid">
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.ngaysinh"/></label> 
								<input id="birthDay" value="<s:property value="birthDayStr"/>" type="text" class="InputTextStyle InputText1Style" maxlength="30"/>
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.tansuat"/></label> 
								<input id="tansuat" value="<s:property value="tansuat"/>" type="text" class="InputTextStyle InputText1Style vinput-number" maxlength="2"/>
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.startdate"/></label> 
								<input id="startDate" value="<s:property value="startDateStr"/>" type="text" class="InputTextStyle InputText1Style" maxlength="30"/>
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.enddate"/></label> 
								<input id="endDate" value="<s:property value="endDateStr"/>" type="text" class="InputTextStyle InputText1Style" maxlength="30"/>
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.tuyen.giao.hang"/></label>
								<div class="BoxSelect BoxSelect2"> 
									<select id="cbRouting" class="easyui-combobox" data-options="panelWidth : '206'"> 
									</select>
								</div>
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.mobile"/></label> 
								<input id="mobilePhone" value="<s:property value="mobilePhone"/>" type="text" class="InputTextStyle InputText1Style" maxlength="30"/>
								<div class="Clear"></div>
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.telephone"/></label> 
								<input id="phone" value='<s:property value="phone"/>' type="text" class="InputTextStyle InputText1Style" maxlength="30"/>
								<div class="Clear"></div>
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.email"/></label>
								<input id="email" value='<s:property value="email"/>' type="text" class="InputTextStyle InputText1Style" maxlength="50"/>
								<div class="Clear"></div>
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.house.number.cmnd"/></label> 
								<input id="idNumber" value="<s:property value="idNumber"/>" type="text" class="InputTextStyle InputText1Style" maxlength="20"/>
								<div class="Clear"></div>
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.status"/> <span class="ReqiureStyle">*</span></label>
								<div class="BoxSelect BoxSelect2">
									<s:if test="%{createNew || statusCustomer == 1 || statusCustomer == 0}">
										<s:select value="statusCustomer" id="status" name="status" cssClass="MySelectBoxClass"  list="lstStatusBean" listKey="value" listValue="name"></s:select>
									</s:if>
									<s:else>
										<s:select value="statusCustomer" id="status" name="status" cssClass="MySelectBoxClass" list="lstStatus" listKey="value" listValue="name"></s:select>
									</s:else>									
								</div>
								<div class="Clear"></div>
							</div>
						</div>
						<div class="SProductForm5Cols SProductForm6Cols">
							<div>
								<p class="ShowHideFunc" style="width: 590px;">
									<a style="font-size: 13px;" href="javascript:void(0)" class="ShowLink" onclick="CustomerCatalog.viewBigMap()"><s:text name="catalog.customer.full.view"/></a>
									<a id="xoaVitriKH" style="font-size: 13px; float: right" href="javascript:void(0)" class="cmsiscontrol ShowLink" onclick="CustomerCatalog.deleteCustomerPosition()"><s:text name="catalog.product.img.latlng.title"/></a>
								</p>
								<div id="mapContainer" style="width: 590px; height:270px;position:relative;" alt="Bản đồ"></div>
								<p id="errMsgMap" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
								<p class="SuccessMsgStyle" style="display: none;" id="successMsgMap"></p>
								<div class="Clear"></div>
							</div>
							<div class="Clear"></div>
						</div>
						<div class="Clear"></div>
					<div id="propertyTabContainer">
					<h2 class="Title2Style"><s:text name="catalog.customer.sale.info"/></h2>
					<div class="SearchInSection SProduct1Form">
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.delivery"/></label>
						<div class="BoxSelect BoxSelect2">
							<select id="deliveryCode" class="MySelectBoxClas easyui-combobox" data-options="panelWidth : '206'">
								<s:iterator value="listDeliverStaff">
									<s:if test="staffCode.equals(deliveryCode)">
										<option value='<s:property value="staffCode" />' selected="selected"><s:property value="staffCode" />-<s:property value="staffName" /></option>
									</s:if>
									<s:else>
										<option value='<s:property value="staffCode" />'><s:property value="staffCode" />-<s:property value="staffName" /></option>
									</s:else>
								</s:iterator>
							</select>
						</div>
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.cashier"/></label>
						<div class="BoxSelect BoxSelect2">
							<%-- <s:select id="cashierCode" name="cashierCode" list="listCashierStaff" cssClass="easyui-combobox" value="cashierCode" listKey="staffCode" listValue="staffName"></s:select> --%>
							<select id="cashierCode" class="easyui-combobox" data-options="panelWidth : '206'">
								<s:iterator value="listCashierStaff">
									<s:if test="staffCode.equals(cashierCode)">
										<option value='<s:property value="staffCode" />' selected="selected"><s:property value="staffCode" />-<s:property value="staffName" /></option>
									</s:if>
									<s:else>
										<option value='<s:property value="staffCode" />'><s:property value="staffCode" />-<s:property value="staffName" /></option>
									</s:else>
								</s:iterator>
							</select>
						</div>
						
						<label class="LabelStyle Label1Style">&nbsp;</label> 
						<input id="CheckapplyDebitLimited" <s:if test="applyDebitLimited==1"> checked="checked"</s:if> type="checkbox" class="InputCbxStyle InputCbx4Style" /> 
						<label class="LabelStyle Label12Style"><s:text name="catalog.customer.aply.debit.limit"/></label> 
						<div class="Clear"></div>
						<s:if test="applyDebitLimited==1">
							<label class="LabelStyle Label1Style"><s:text name="catalog.customer.debit.limit"/> <span class="ReqiureStyle  CheckapplyDebit1">*</span> </label> 
							<input id="maxDebit" value='<s:property value="convertMoney(maxDebit)"/>' name="maxDebit" class="InputTextStyle InputText1Style" maxlength="17"/> 
							<label class="LabelStyle Label1Style"><s:text name="catalog.customer.debit.date.limit"/> <span class="ReqiureStyle CheckapplyDebit1">*</span> </label> 
							<input id="debitDate" value='<s:property value="debitDate"/>' name="debitDate " class="InputTextStyle InputText1Style" maxlength="3" />
						</s:if>
						<s:else>
							<label class="LabelStyle Label1Style"><s:text name="catalog.customer.debit.limit"/><span class="ReqiureStyle  CheckapplyDebit1">*</span> </label> 
							<input id="maxDebit" disabled="disabled" name="maxDebit" class="InputTextStyle InputText1Style" maxlength="17"/> 
							<label class="LabelStyle Label1Style"><s:text name="catalog.customer.debit.date.limit"/><span class="ReqiureStyle  CheckapplyDebit1">*</span> </label> 
							<input id="debitDate" disabled="disabled" name="debitDate" class="InputTextStyle InputText1Style" maxlength="3" />
						</s:else>
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.debit.limit.now"/></label> 
						<input id="debitDateNow" disabled="disabled" value='<s:property value="debitDateNow"/>' name="debitDateNow" class="InputTextStyle InputText1Style" maxlength="30" /> 
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.amount.debit.limit"/></label> 
						<input disabled="disabled" id="maxDebitNow" value='<s:property value="convertMoney(maxDebitNow)"/>' value="convertMoney(maxDebitNow)" name="maxDebitNow" class="InputTextStyle InputText1Style" maxlength="30" />
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.sale.position"/></label>
						<div class="BoxSelect BoxSelect2">
							<select id="salePosition" class="easyui-combobox" data-options="panelWidth : '206'">
								<option value=''>Chọn vị trí bán hàng</option>
								<s:iterator value="salePositionOptions">
									<s:if test="value.equals(customerSalePosition.toString())">
										<option value='<s:property value="value" />' selected="selected"><s:property value="apParamName" /></option>
									</s:if>
									<s:else>
										<option value='<s:property value="value" />'><s:property value="apParamName" /></option>
									</s:else>
								</s:iterator>
							</select>
						</div>
						
						<div class="Clear"></div>
					</div>
					<h2 class="Title2Style"><s:text name="catalog.customer.vat.info"/></h2>
					<div class="SearchInSection SProduct1Form">
						<label class="LabelStyle Label1Style">&nbsp;</label> 
						<input id="cbxIsVat" <s:if test="isVat==1"> checked="checked"</s:if> type="checkbox" class="InputCbxStyle InputCbx4Style" /> 
						<label class="LabelStyle Label12Style"><s:text name="catalog.customer.print.vat"/></label>
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.org.name"/></label> 
						<input id="invoiceConpanyName" value='<s:property value="invoiceConpanyName"/>' name="invoiceConpanyName" class="InputTextStyle InputText1Style" maxlength="250" /> 
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.resentation"/></label> 
						<input id="invoiceOutletName" value='<s:property value="invoiceOutletName"/>' name="invoiceOutletName" class="InputTextStyle InputText1Style" maxlength="250" /> 
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.dia.chi.hoa.don"/></label> 
						<input id="custDeliveryAddr" value='<s:property value="custDeliveryAddr"/>' name="custDeliveryAddr" class="InputTextStyle InputText1Style" maxlength="250" />
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.tax.no"/></label> 
						<input id="invoiceTax" value='<s:property value="invoiceTax"/>' name="invoiceTax" class="InputTextStyle InputText1Style" maxlength="20" /> 
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.account.no"/></label> 
						<input id="invoiceNumberAccount" name="invoiceNumberAccount" value='<s:property value="invoiceNumberAccount"/>' class="InputTextStyle InputText1Style" maxlength="20" /> 
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.bank"/></label> 
						<input id="invoiceNameBank" name="invoiceNameBank" value='<s:property value="invoiceNameBank"/>' class="InputTextStyle InputText1Style" maxlength="100" />
						<div class="Clear"></div>
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.chinhanh"/></label> 
						<input id="invoiceNameBranchBank" name="invoiceNameBranchBank" value='<s:property value="invoiceNameBranchBank"/>' class="InputTextStyle InputText1Style" maxlength="100" />
						<label class="LabelStyle Label1Style"><s:text name="catalog.customer.chutk"/></label> 
						<input id="bankAccountOwner" name="bankAccountOwner" value='<s:property value="bankAccountOwner"/>' class="InputTextStyle InputText1Style" maxlength="100" />
						<div class="Clear"></div>
					</div>
					</div>
					
					</div>
					<div class="Clear"></div>
					<div class="SearchInSection SProduct1Form">
						<h2 class="Title2Style">
							<s:text name="catalog.product.thuoc.tinh.mo.rong" />
						</h2>
						<div class="SearchInSection SProduct1Form"	id="propertyTabContainer">
							<tiles:insertTemplate template="/WEB-INF/jsp/catalog/attributes-customer/customerAttCommon.jsp"></tiles:insertTemplate>
							<%-- <tiles:insertTemplate
								template="/WEB-INF/jsp/catalog/attributes-product/productAttCommon.jsp"></tiles:insertTemplate> --%>
						</div>
						<div class="Clear"></div>
						
						<div class="BtnCenterSection" style="padding-top: 10px;">
							<button id="btnCancelInfo" class="BtnGeneralStyle cmsiscontrol1" onclick="goBack();"><s:text name="catalog.customer.ignore"/></button>
							
							<s:if test="%{createNew}">
								<button id="group_insert_btnSaveInfo" class="BtnGeneralStyle cmsiscontrol1 cmsiscontrol" onclick="CustomerCatalog.saveInfo();"><s:text name="catalog.customer.create"/></button>
							</s:if>
							<s:else>
								<button id="group_update_btnEditInfo" class="BtnGeneralStyle cmsiscontrol1 cmsiscontrol" onclick="CustomerCatalog.saveInfo();"><s:text name="catalog.customer.update"/></button>
							</s:else>
						</div>
						<p id="errMsg" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
						<p id="errMsgProperty" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
						<p class="SuccessMsgStyle" style="display: none;" id="successMsg"></p>
						<div class="Clear"></div>
					</div>

				</div>
				
				<div id="catInfoTabContainer" style="display: none">
					<h2 class="Title2Style"><s:text name="catalog.customer.cat.info"/></h2>
					<div class="SearchInSection SProduct1Form">
						<div class="SProductForm4Cols SProductForm6Cols">
							<div class="PopupContentMid">
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.cat"/></label>
								<div class="BoxSelect BoxSelect2">
									<s:if test="lstCat!=null">
										<select id="cat" class="MySelectBoxClass" style="opacity: 0; height: 22px;" onchange="levelCatChanged();"> 
											<option value="-2" selected="selected">-- <s:text name="catalog.customer.cat.select"/> --</option>
											<s:iterator value="lstCat">
											<option value="<s:property value="id"/>"><s:property value="productInfoCode"/> - <s:property value="productInfoName"/></option>
											</s:iterator>
										</select>
									</s:if>
                                    <s:else>
                                       	<select id="cat" class="MySelectBoxClass" style="opacity: 0; height: 22px;">
                                           		<option value="-2">-- <s:text name="catalog.customer.cat.select"/> --</option>
                                        </select>
                                    </s:else>
								</div>
								<div class="Clear"></div>
								<label class="LabelStyle Label13Style"><s:text name="catalog.customer.amount"/></label>
								<div class="BoxSelect BoxSelect2">
									<select id="saleLevelCat" class="MySelectBoxClass">
										<option value="-2" selected="selected">-- <s:text name="catalog.customer.amount.select"/> --</option>
									</select>
								</div>
								<div class="Clear"></div>
								<div class="BtnCenterSection">
									<button id="group_insert_btnAddCat" class="BtnGeneralStyle cmsiscontrol" onclick="CustomerCatalog.saveLevelCat();"><s:text name="catalog.customer.add"/></button>
								</div>
								<div class="Clear"></div>
								<p id="errMsgLevel" class="ErrorMsgStyle" style="display: none"></p>
							</div>
						</div>
						<div class="SProductForm5Cols SProductForm6Cols">
							<div class="GridSection" id="gridContainer">
							<p id="gridNoResult" style="display: none" class="WarningResultStyle"><s:text name="catalog.customer.no.result"/></p>
							<table id="grid" class="easyui-datagrid"></table>
							<div id="pager"></div>
							</div>
						</div>
						<div class="Clear"></div>
						</div>
						<div class="Clear"></div>
					</div>
				</div>
				<div class="Clear"></div>
			</div>
		</div>
		<div class="Clear"></div>
	</div>
    <p id="successMsgLevel" class="SuccessMsgStyle" style="display: none"></p>
</div>
<div class="Clear"></div>
<!-- <div class="easyui-dialog" title="Bản đồ dạng đầy đủ" data-options="closed:true,modal:true" style="width: 970px; height: 575px;" id="viewBigMap"> -->
<!-- 	<div id="fancyboxBigMap" class="GeneralDialog General2Dialog" style="width: 965px; height: 535px"> -->
<!-- 		<div id="bigMapContainer" style="width: 965px; height: 535px"></div> -->
<!-- 		<div class="Clear"></div> -->
<!-- 	</div> -->
<!-- </div> -->
<div class="easyui-dialog" data-options="closed:true,modal:true" style="width: 970px; height: 575px;" id="viewBigMap">
	<div id="fancyboxBigMap" class="GeneralDialog General2Dialog" style="width: 965px; height: 535px">
		<span id="imgBtnDeleteLatLng" style="z-index: 10000; position: absolute; cursor: pointer; margin-left: 50%; margin-top: 52%; color: #215EA2">
				<img  title="<s:text name="catalog.product.img.latlng.title"></s:text>" 
					alt="<s:text name="catalog.product.img.latlng.title">
					</s:text>" src="/resources/images/icon_delete.png">
  				&nbsp;&nbsp;<s:text name="catalog.product.img.latlng.title"></s:text>	
		</span>
		<span id="imgBtnUpdateLatLng" style="z-index: 10000; position: absolute; cursor: pointer; margin-left: 60%; margin-top: 52%; color: #215EA2">	
  				<img  title="<s:text name="jsp.common.dongy"></s:text>" 
					alt="<s:text name="jsp.common.dongy">
					</s:text>" src="/resources/images/checked_small.png">
  				&nbsp;&nbsp;<s:text name="jsp.common.dongy"></s:text>
		</span>
		<span id="imgBtnCancelLatLng" style="z-index: 10000; position: absolute; cursor: pointer; margin-left: 70%; margin-top: 52%; color: #215EA2">	
  				<img  title="<s:text name="jsp.common.bo.qua"></s:text>" 
					alt="<s:text name="jsp.common.bo.qua">
					</s:text>" src="/resources/images/icon_close.png">
  				&nbsp;&nbsp;<s:text name="jsp.common.bo.qua"></s:text>
		</span>
		<div id="bigMapContainer" style="width: 965px; height: 535px"></div>
		<div class="Clear"></div>
	</div>
</div>
<s:hidden id="areaId" name="areaId"></s:hidden>
<s:hidden id="hidCatId" name="hidCatId"></s:hidden>
<s:hidden id="provinceCode" name="provinceCode"></s:hidden>
<s:hidden id="districtCode" name="districtCode"></s:hidden>
<s:hidden id="wardCode" name="wardCode"></s:hidden>
<s:hidden id="id" name="id"></s:hidden>
<s:hidden id="statusCustomer" name="statusCustomer"></s:hidden>
<s:hidden id="selId" name="id"></s:hidden>
<s:hidden id="selShopMapId" value="0"></s:hidden>
<s:hidden id="selChannelMapId" value="0"></s:hidden>
<s:hidden id="saleLevelCatId" value="0"></s:hidden>
<s:hidden id="tabInfo" value="1"></s:hidden>
<s:hidden id="lat" name="lat"></s:hidden>
<s:hidden id="lng" name="lng"></s:hidden>
<s:hidden id="hasEditPermission" name="hasEditPermission"></s:hidden>
<%-- <s:hidden id="address" name="address"></s:hidden> --%>

<%-- <script type="text/javascript" src="<%=Configuration.getStaticServerPath("/resources/scripts/business/common/vnm.mapapi.js")%>"></script> --%>
<script type="text/javascript">
	function levelCatChanged() {
		var catId = $('#cat').val().trim();
		var levelId = CustomerCatalog._curSaleLevel;
		if (levelId == null) {
			levelId = 0;
		}
		$.getJSON('/rest/catalog/sale-level-cat/list/' + catId + '/' + levelId
				+ '.json', function(data) {
			var arrHtml = new Array();
			arrHtml.push('<option value="-2">-- <s:text name="catalog.customer.amount.select"/> --</option>');
			if (data != null && data.length > 0) {
				for ( var i = 0; i < data.length; i++) {
					arrHtml.push('<option value="'+data[i].value+'">'
							+Utils.XSSEncode(data[i].name)+ '</option>');
				}
			}
			$('#saleLevelCat').html(arrHtml.join(""));
			if (CustomerCatalog._curSaleLevel != null) {
				$('#saleLevelCat').val(CustomerCatalog._curSaleLevel);
				CustomerCatalog._curSaleLevel = null;
			}
			$('#saleLevelCat').change();
		});
		return false;
	};
	$(document).ready(function() {
		setDateTimePicker('birthDay');
		setDateTimePicker('startDate');
		setDateTimePicker('endDate');
		
		if($('#id').val() == null || $('#id').val() == '' || $('#id').val() == undefined) {
			$('#shortCode').val('');
			$('#customerName').val('');
			$('#phone').val('');
			$('#mobilePhone').val('');
			$('#idNumber').val('');
			$('#email').val('');
			$('#contact').val('');
			$('#provinceName').val('');
			$('#wardName').val('');
			$('#districtName').val('');
			//$('#address').val('');
			$('#street').val('');
			$('#addressNumber').val('');
			$('#status').val(1);
		} else {
			var customerStatus = $('#statusCustomer').val();
			$('#status').val(customerStatus);
			if (CustomerCatalog.STATUS_WAITING == customerStatus || CustomerCatalog.STATUS_REJECTED == customerStatus) {
				disableSelectbox('status');
			}
		}
		$('#province').val($('#provinceCode').val());
		$('#district').val($('#districtCode').val());
		$('#status').change();
		$('.CheckapplyDebit1').css('visibility',$('#CheckapplyDebitLimited').is(':checked')?'visible':'hidden');
		
// 		if($('#id').val().length == 0) {
			/* var areaid = $('#areaId').val();
			if (areaid == '') { areaid = '0' };
			TreeUtils.loadComboTreeAreaForUnitTreeHasTitle('areaTree', 'areaId', areaid, function(data) {
				if(data.id != activeType.ALL) {
					if(data.type != 'WARD') {
						Alert('Thông báo','Địa bàn chọn phải là phường/xã.',function(){
							$('#areaTree').combotree('setValue',activeType.ALL);
							$('#provinceName').val('');
							$('#districtName').val('');
							$('#wardName').val('');
							//ViettelMap.clearOverlays();
						});
					} else {
						CustomerCatalog.loadAreaInfoAndMap(data);
					}
				}
			}); */
// 			CustomerCatalog.loadTree4Area('/rest/catalog/area-tree-ex1/combotree/0/0.json');							
// 		}
		var lat = '<s:property value="lat"/>';
		var lng = '<s:property value="lng"/>';
		var zoom = CustomerCatalog._DEF_ZOOM;
		if (lat.trim().length == 0 || lng.trim().length == 0 || lat=='null' || lng=='null') {
			zoom = ViettelMap._DEF_ALL_ZOOM;
		}
		VTMapUtil.loadMapResource(function(){
			ViettelMap.loadBigMapEx('mapContainer', lat, lng, zoom, function(latitude, longtitude){
				$('#lat').val(latitude);
				$('#lng').val(longtitude);
			}, true);
		});
		Utils.bindFormatOntextfieldCurrencyFor('maxDebit', Utils._TF_NUMBER_COMMA_AND_DOT);
		Utils.bindFormatOnTextfield('debitDate', Utils._TF_NUMBER);
		disabled('customerCode');
		disabled('maxDebitNow');
		disabled('debitDateNow');
		Utils.bindFormatOnTextfield('maxDebit', Utils._TF_NUMBER);
		Utils.bindFormatOnTextfield('debitDate', Utils._TF_NUMBER);
		Utils.bindFormatOnTextfield('mobilePhone', Utils._TF_NUMBER);
		Utils.bindFormatOnTextfield('phone', Utils._TF_NUMBER);
		Utils.bindFormatOnTextfield('idNumber', Utils._TF_NUMBER);
		Utils.bindFormatOnTextfield('invoiceTax', Utils._TF_NUMBER);
		Utils.bindFormatOnTextfield('invoiceNumberAccount', Utils._TF_NUMBER);
		$('#CheckapplyDebitLimited').bind('change', function() {
			$('.CheckapplyDebit1').css('visibility',$('#CheckapplyDebitLimited').is(':checked')?'visible':'hidden');
			if(!$('#CheckapplyDebitLimited').is(':checked')) {
				disabled('maxDebit');
				disabled('debitDate');
			} else {
				enable('maxDebit');
				enable('debitDate');
			}
		});
		
		Utils.bindComboboxStaffEasyUI('deliveryCode');
		Utils.bindComboboxStaffEasyUI('cashierCode');
		
// 		$('#cbRouting').combobox({
// 			valueField : 'routingId',
// 			textField : 'routingCode',
// 			mode:'local',
// 			formatter: function(r) {
// 				return Utils.XSSEncode(r['routingCode']);
// 			},
// 			filter: function(q, row){
// 				var opts = $(this).combobox('options');
// 				return row[opts.valueField].indexOf(q.toUpperCase().trim())==0;
// 			}
// 		});
		
		$('#customerType').combobox({
			valueField : 'id',
			textField : 'text',
			mode:'local',
			formatter: function(r) {
				return Utils.XSSEncode(r['text']).trim();
			},
			filter: function(q, row){
				var opts = $(this).combobox('options');
				return ((row[opts.textField]).toUpperCase()).indexOf(q.toUpperCase().trim())>=0;
			}
		});
		
		$('#province').combobox({
			valueField : 'areaCode',
			textField : 'areaName',
			data: getDataAreaCombobox('province'),
			panelWidth:206,
			formatter: function(r) {
				return Utils.XSSEncode(r['areaName'].trim());
			},
			filter: function(q, r){
				q = q.toString().trim().toUpperCase();
				q = unicodeToEnglish(q);
				return (r['searchText'].indexOf(q)>=0);
			},
			onSelect: function(r) {
				var data = {};
				data.objectType = 2; // district
				data.areaId = r.id;
				$('#district').combobox('clear');
				$('#precinct').combobox('clear');
				Utils.getJSONDataByAjaxNotOverlay(data, '/catalog_customer_mng/get-list-sub-area',function(result) {
					if(result == undefined || result == null){
						result = [];
					}
					var obj;
					for (var i = 0, sz = result.length; i < sz; i++) {
						obj = result[i];
						obj.searchText = unicodeToEnglish(obj.areaCode + obj.areaName);
						obj.searchText = obj.searchText.toUpperCase();
					}
					$('#district').combobox('loadData', result);
				});
			}
			/* ,
			onLoadSuccess: function() {
				var $this = $(this);
				$this.combobox('setValue', $this.find("option:selected").attr("value"));
			} */
		});
		$('#district').combobox({
			valueField : 'areaCode',
			textField : 'areaName',
			data: getDataAreaCombobox('district'),
			panelWidth:206,
			formatter: function(r) {
				return Utils.XSSEncode(r['areaName']).trim();
			},
			filter: function(q, r){
				q = q.toString().trim().toUpperCase();
				q = unicodeToEnglish(q);
				return (r['searchText'].indexOf(q)>=0);
			},
			onSelect: function(r) {
				var data = {};
				data.objectType = 3; // ward
				data.areaId = r.id;
				$('#precinct').combobox('clear');
				Utils.getJSONDataByAjaxNotOverlay(data, '/catalog_customer_mng/get-list-sub-area',function(result) {
					if(result == undefined || result == null){
						result = [];
					}
					var obj;
					for (var i = 0, sz = result.length; i < sz; i++) {
						obj = result[i];
						obj.searchText = unicodeToEnglish(obj.areaCode + obj.areaName);
						obj.searchText = obj.searchText.toUpperCase();
					}
					$('#precinct').combobox('loadData', result);
				});
			}
		});
		$('#precinct').combobox({
			valueField : 'areaCode',
			textField : 'areaName',
			data: getDataAreaCombobox('precinct'),
			panelWidth:206,
			formatter: function(r) {
				return Utils.XSSEncode(r['areaName']).trim();
			},
			filter: function(q, r){
				q = q.toString().trim().toUpperCase();
				q = unicodeToEnglish(q);
				return (r['searchText'].indexOf(q)>=0);
			},
			onSelect: function(r) {
				$('#areaId').val(r.id);
			}
		});

		$('#salePosition').combobox({
			valueField: 'value',
			textField: 'apParamName',
			panelWidth: 206,
			mode:'local',
			formatter: function(r) {
				return Utils.XSSEncode(r['apParamName']).trim();
			},
			filter: function(q, row) {
				var opts = $(this).combobox('options');
				return row[opts.valueField].indexOf(q.toUpperCase().trim()) == 0;
			}
		});

		$('#txtShopCode').bind('keyup', function(event) {
			if (event.keyCode == keyCode_F9) {
				VCommonJS.showDialogSearch2({
					params : {
						status : 1
					},
					inputs : [
				        {id:'code', maxlength:50, label:Utils.language_vi('catalog.customer.org.code')},
				        {id:'name', maxlength:250, label:Utils.language_vi('catalog.customer.org.name')},
				    ],
				    url : '/commons/search-shop-show-list',
				    columns : [[
				        {field:'shopCode', title:Utils.language_vi('catalog.customer.org.code'), align:'left', width: 110, sortable:false, resizable:false, formatter: function(value, row, index){
					    	return Utils.XSSEncode(value);
						  }},
				        {field:'shopName', title:Utils.language_vi('catalog.customer.org.name'), align:'left', width: 200, sortable:false, resizable:false, formatter: function(value, row, index){
					    	return Utils.XSSEncode(value);
						  }},
				        {field:'choose', title:'', align:'center', width:40, sortable:false, resizable:false, formatter:function(value,row,index) {
				            return '<a href="javascript:void(0)" onclick="CustomerCatalog.fillTxtShopCodeByF9(\''+ Utils.XSSEncode(row.shopCode) +'\');"><s:text name="catalog.customer.select"/></a>';         
				        }}
				    ]]
				});
			}
		});
		var data = new Array();
		<s:iterator value="listRouting">
			var routing = new Object();
			routing.routingId = '<s:property value="routingId"/>';
			routing.routingCode = '<s:property value="routingCode"/>';
			routing.routingName = '<s:property value="routingName"/>';
			data.push(routing);
		</s:iterator>
		loadDataComboboxRouting(data,'<s:property value="routingId" />');
		
		var dataNVGH = new Array();
		<s:iterator value="listDeliverStaff">
			var nvgh = new Object();
			nvgh.staffCode = '<s:property value="staffCode" escape="true"/>';
			nvgh.staffName = '<s:property value="staffName" escape="true"/>';
			dataNVGH.push(nvgh);
		</s:iterator>
		Utils.bindStaffCbx('#deliveryCode', dataNVGH, '<s:property value="deliveryCode" />');
		
		var dataNVTT = new Array();
		<s:iterator value="listCashierStaff">
			var nvtt = new Object();
			nvtt.staffCode = '<s:property value="staffCode" escape="true"/>';
			nvtt.staffName = '<s:property value="staffName" escape="true"/>';
			dataNVTT.push(nvtt);
		</s:iterator>
		Utils.bindStaffCbx('#cashierCode', dataNVTT, '<s:property value="cashierCode" />');
	});
	
	function loadDataComboboxRouting(data, idDefault){
		$('#cbRouting').combobox({
		    data:data,
		    valueField:'routingId',
		    textField:'routingCode',
		    formatter: function(row) {
				return '<span style="font-weight:bold">' + Utils.XSSEncode(row.routingCode) + '</span><br/>' + '<span style="color:#888">' + Utils.XSSEncode(row.routingName) + '</span>';
			},
			filter: function(q, row){
				q = new String(q).toUpperCase().trim();
				var opts = $(this).combobox('options');
				return ((unicodeToEnglish(row[opts.textField]).indexOf(unicodeToEnglish(q)) >= 0) || unicodeToEnglish(row[opts.valueField]).indexOf(unicodeToEnglish(q)) >= 0) ;
			},
			onLoadSuccess:function(){
				if (idDefault != undefined && idDefault != null) {
					$('#cbRouting').combobox('setValue',idDefault);
				}
			}
		});
	};
	
	function getDataAreaCombobox(objectId, prefix){
		var data = [];		
		var parent = '';
		if(prefix!=undefined && prefix!=null){
			parent = prefix;
		}
		var obj;
		var $this;
		$(parent + '#' + objectId +' option').each(function(){
			$this = $(this);
			obj = {};
			obj.areaCode = $this.val().trim();
			obj.areaName = $this.text().trim();
			obj.id = $this.attr('areaId').trim();
			obj.centerCode = $this.attr('centerCode').trim();
			obj.searchText = unicodeToEnglish($(this).val().trim()+ $(this).text().trim());
			obj.searchText = obj.searchText.toUpperCase();
			data.push(obj);
		});
		return data;
	};
	
	function goBack() {
		window.location.href = '/catalog_customer_mng/info';
	};
</script>
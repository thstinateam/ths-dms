<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div data-options="closed:true, modal:true" class="easyui-dialog" id="confirm-duplicate-customer-dialog">
	<div class="PopupContentMid">
		<div class="GeneralForm Search1Form">
			<div id="duplicate-customer-grid-container" class="GridSection">
				<table id="duplicate-customer-grid"></table>
			</div>
			<div class="BtnCenterSection">
				<button id="group_insert_hkh" data-button-type="1" class="BtnGeneralStyle BtnGeneralMStyle cmsiscontrol">Hủy khách hàng</button>
				<button id="group_insert_ds" data-button-type="2" class="BtnGeneralStyle BtnGeneralMStyle cmsiscontrol">Duyệt sau</button>
				<button id="group_insert_tm" data-button-type="3" class="BtnGeneralStyle BtnGeneralMStyle cmsiscontrol">Tạo mới</button>
				<button id="group_insert_tc" data-button-type="4" class="BtnGeneralStyle BtnGeneralMStyle cmsiscontrol">Từ chối</button>
				<p id="dlgErrMsg" class="ErrorMsgStyle" style="display: none"></p>
				<p id="dlgSuccessMsg" class="SuccessMsgStyle" style="display: none"></p>
			</div>
		</div>
	</div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div id="errorExcel" style="display: none"><s:property value="isError" /></div>
<div id="errorExcelMsg" style="display: none"><s:property value="errMsg" /></div>
<div id="totalRow" style="display: none"><s:property value="totalItem" /></div>
<div id="numFail" style="display: none"><s:property value="numFail" /></div>
<div id="fileNameFail" style="display: none"><s:property value="fileNameFail" /></div>
<div id="typeView" style="display: none"><s:property value="typeView" /></div>
<input type="hidden" id="newToken" value='<s:property value="token"/>' name="newToken">
<div id="lstSize" style="display: none">
	<s:if test="listCell != null"><s:property value="listCell.size()" /></s:if>
	<s:if test="listSubcat != null"><s:property value="listSubcat.size()" /></s:if>
</div>
<s:if test="listCell != null">
<div class="GeneralDialog General1Dialog">
		<div class="DialogProductSelection">
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<div class="GeneralMilkInBox">
							<div class="GeneralTable Table45KHV1Section">
								<div class="ScrollSection">
									<div class="BoxGeneralTTitle">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<colgroup>
												<col style="width: 40px;" />
												<col style="width: 90px;" />
												<col style="width: 200px;" />
												<col style="width: 100px;" />
												<col style="width: 150px;" />
												<col style="width: 200px;" />
												<col style="width: 100px;" />
												<col style="width: 90px;" />
												<col style="width: 90px;" />
												<col style="width: 90px;" />
												<col style="width: 90px;" />
												<col style="width: 90px;" />
												<col style="width: 90px;" />
												<col style="width: 90px;" />
												<col style="width: 90px;" />
												<col style="width: 90px;" />
												<col style="width: 90px;" />
												<col style="width: 90px;" />
												<col style="width: 90px;" />
												<col style="width: 200px;" />
												<col style="width: 200px;" />
												<col style="width: 200px;" />
												<col style="width: 130px;" />
												<col style="width: 100px;" />
												<col style="width: 150px;" />
												<col style="width: 130px;" />
												<col style="width: 300px;" />
											</colgroup>
											<thead>
												<tr>
													<th class="ColsThFirst">STT</th>
													<th>Mã đơn vị</th>
													<th>Tên KH</th>
													<th>Mã địa bàn</th>
													<th>Đường</th>
													<th>Địa chỉ</th>
													<th>Loại vùng miền</th>
													<th>Liên hệ</th>
													<th>CMND</th>
													<th>Phone</th>
													<th>MobilePhone</th>
													<th>Email</th>
													<th>Mã loại KH</th>
													<th>Mã nhóm GH</th>
													<th>Mã NVTT</th>
													<th>Mức nợ</th>
													<th>Hạn nợ</th>
													<th>Mã số thuế</th>
													<th>Số tài khoản</th>
													<th>Họ tên ghi trên hóa đơn</th>
													<th>Tên đơn vị ghi trên hóa đơn</th>
													<th>Địa chỉ giao hàng</th>
													<th>Tên ngân hàng</th>
													<th>Độ trung thành</th>
													<th>Số nhà</th>
													<th>Trưng bày</th>
													<th>Mô tả</th>
												</tr>
											</thead>
										</table>
									</div>
									<div class="BoxGeneralTBody">
										<div class="ScrollBodySection">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<colgroup>
													<col style="width: 40px;" />
													<col style="width: 90px;" />
													<col style="width: 200px;" />
													<col style="width: 100px;" />
													<col style="width: 150px;" />
													<col style="width: 200px;" />
													<col style="width: 100px;" />
													<col style="width: 90px;" />
													<col style="width: 90px;" />
													<col style="width: 90px;" />
													<col style="width: 90px;" />
													<col style="width: 90px;" />
													<col style="width: 90px;" />
													<col style="width: 90px;" />
													<col style="width: 90px;" />
													<col style="width: 90px;" />
													<col style="width: 90px;" />
													<col style="width: 90px;" />
													<col style="width: 90px;" />
													<col style="width: 200px;" />
													<col style="width: 200px;" />
													<col style="width: 200px;" />
													<col style="width: 130px;" />
													<col style="width: 100px;" />
													<col style="width: 150px;" />
													<col style="width: 130px;" />
													<col style="width: 300px;" />
												</colgroup>
												<tbody>
													<s:iterator value="listCell" status="status">
														<tr>
															<td class="ColsTd0"><s:property value="#status.index+1" />
															<td class="ColsTd1">
																<div class="ColsAlignLeft" style="width: 69px"><s:property value="content1" />
																</div>
															</td>
															<td class="ColsTd2">
																<div class="ColsAlignLeft" style="width: 179px"><s:property value="content2" />
																</div>
															</td>
															<td class="ColsTd3">
																<div class="ColsAlignLeft" style="width: 79px">
																	<s:property value="content3" />
																</div>
															</td>
															<td class="ColsTd4">
																<div class="ColsAlignLeft" style="width: 129px">
																	<s:property value="content4" />
																</div>
															</td>
															<td class="ColsTd5">
																<div class="ColsAlignLeft" style="width: 179px">
																	<s:property value="content5" />
																</div>
															</td>
															<td class="ColsTd6">
																<div class="ColsAlignLeft" style="width: 79px">
																	<s:property value="content6" />
																</div>
															</td>
															<td class="ColsTd7">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content7" />
																</div>
															</td>
															<td class="ColsTd8">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content8" />
																</div>
															</td>
															<td class="ColsTd9">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content9" />
																</div>
															</td>
															<td class="ColsTd10">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content10" />
																</div>
															</td>
															<td class="ColsTd11">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content11" />
																</div>
															</td>
															<td class="ColsTd12">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content12" />
																</div>
															</td>
															<td class="ColsTd13">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content13" />
																</div>
															</td>
															<td class="ColsTd14">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content14" />
																</div>
															</td>
															<td class="ColsTd15">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content15" />
																</div>
															</td>
															<td class="ColsTd16">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content16" />
																</div>
															</td>
															<td class="ColsTd17">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content17" />
																</div>
															</td>
															<td class="ColsTd18">
																<div class="ColsAlignLeft" style="width: 69px">
																	<s:property value="content18" />
																</div>
															</td>
															<td class="ColsTd19">
																<div class="ColsAlignLeft" style="width: 179px">
																	<s:property value="content19" />
																</div>
															</td>
															<td class="ColsTd20">
																<div class="ColsAlignLeft" style="width: 179px">
																	<s:property value="content20" />
																</div>
															</td>
															<td class="ColsTd21">
																<div class="ColsAlignLeft" style="width: 179px">
																	<s:property value="content21" />
																</div>
															</td>
															<td class="ColsTd22">
																<div class="ColsAlignLeft" style="width: 109px">
																	<s:property value="content22" />
																</div>
															</td>
															<td class="ColsTd23">
																<div class="ColsAlignLeft" style="width: 79px">
																	<s:property value="content23" />
																</div>
															</td>
															<td class="ColsTd24">
																<div class="ColsAlignLeft" style="width: 129px">
																	<s:property value="content24" />
																</div>
															</td>
															<td class="ColsTd25">
																<div class="ColsAlignLeft" style="width: 109px">
																	<s:property value="content25" />
																</div>
															</td>
															<td class="ColsTd26 ColsTdEnd">
																<div class="ColsAlignLeft" style="width: 279px">
																	<s:property value="errMsg" escapeHtml="false"/>
																</div>
															</td>
														</tr>
													</s:iterator>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="BoxDialogBtm">
				<div class="BoxDialogAlert">
					<p id="errMsg2" style="display: none;"class="ErrorMsgStyle Sprite1"></p>
				</div>
				<div class="ButtonSection">
					<button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();">
						<span class="Sprite2">Đóng</span>
					</button>
				</div>
			</div>
		</div>
	</div>

</s:if>
<s:if test="listSubcat != null">
	<div class="GeneralDialog General1Dialog">
		<div class="DialogProductSelection">
			<div class="GeneralMilkBox">
				<div class="GeneralMilkTopBox">
					<div class="GeneralMilkBtmBox">
						<div class="GeneralMilkInBox">
							<div class="GeneralTable Table45SubSection">
								<div class="ScrollSection">
								<div class="BoxGeneralTTitle">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<colgroup>
											<col style="width: 64px;" />
											<col style="width: 130px;" />
											<col style="width: 130px;" />
											<col style="width: 110px;" />
											<col style="width: 130px;" />
											<col style="width: 300px;" />
										</colgroup>
										<thead>
											<tr>
												<th class="ColsThFirst">STT</th>
												<th>Mã KH</th>
												<th>Ngành hàng</th>
												<th>Mức</th>
												<th>Mã đơn vị</th>
												<th class="ColsTdEnd">Mô tả</th>
											</tr>
										</thead>
									</table>
								</div>
								<div class="BoxGeneralTBody">
									<div class="ScrollBodySection">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<colgroup>
												<col style="width: 64px;" />
												<col style="width: 130px;" />
												<col style="width: 130px;" />
												<col style="width: 110px;" />
												<col style="width: 130px;" />
												<col style="width: 300px;" />
											</colgroup>
											<tbody>
												<s:iterator value="listSubcat" status="status">
													<tr>
														<td class="ColsTd1"><s:property value="#status.index+1" /></td>
														<td class="ColsTd2"><div class="ColsAlignLeft" style="width: 109px"><s:property value="content1" /></div></td>
														<td class="ColsTd3"><div class="ColsAlignLeft" style="width: 109px"><s:property value="content2" /></div></td>
														<td class="ColsTd4"><div class="ColsAlignLeft" style="width: 89px"><s:property value="content3" /></div></td>
														<td class="ColsTd5"><div class="ColsAlignLeft" style="width: 109px"><s:property value="content4" /></div></td>
														<td class="ColsTd6 ColsTdEnd"><div class="ColsAlignLeft" style="width: 279px"><s:property value="errMsg" /></div></td>
													</tr>
												</s:iterator>
											</tbody>
										</table>
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="BoxDialogBtm">
				<div class="BoxDialogAlert">
	            	<p id="errMsg2" style="display: none;" class="ErrorMsgStyle Sprite1"></p>
	         	</div>
				<div class="ButtonSection">
					<button class="BtnGeneralStyle BtnGeneralCStyle BtnGeneralMStyle Sprite2" onclick="$.fancybox.close();"><span class="Sprite2">Đóng</span></button>			
				</div>
			</div>
		</div>
	</div>
</s:if>




<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style type="text/css">
#pocscfTable tr {
	height: 22px;
}
.AlignLeftDetail {
	text-align: left; padding-left: 5px;
}
.AlignRightDetail {
	text-align: right; padding-right: 5px;
}
.AlignRightBoldDetail {
	text-align: right; padding-right: 5px;
	font-weight: bold;
}

</style>
<h2 class="Title2Style">Chi tiết đơn hàng <span style="color:#199700;"><s:property value="poNumber"/></span></h2>
<div class="Clear" style="margin-top: 10px"></div>
<div style="width: 100%;">
	<div style="width: 500px; float:left">
		<label class="LabelStyle Label2Style">Ghi chú <span class="ReqiureStyle"></span></label>
		<textarea maxlength="200" rows="3" cols="10" id="poNote" style="width: 400px; margin-left: 0px; border: 1px solid #c4c4c4; font-size: 1em;" ><s:property value="note"/></textarea>
	</div>
	<s:if test="type == 2">
		<div style="float:left">
			<label class="LabelStyle Label2Style">Số đơn gốc</label> 
			<input type="text" class="InputTextStyle InputText3Style" id="poNumber" disabled="disabled" value='<s:property value="refPoNumber"/>' autocomplete="off" />
		</div>
	</s:if>
</div>
<div class="Clear"></div>
<div class="GeneralTable General1Table" id="gridIndexDetail" style="padding:10px 10px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <colgroup>
	    	<col style="width:35px;" />
	    	<col style="width:50px;" />
	        <col style="width:150px;" />
	        <col style="width:320px;" />
	        <col style="width:70px;" />
	        <col style="width:100px;" />
	        <col style="width:100px;" />
	        <col style="width:180px;" />
	    </colgroup>
	    <thead>
	        <tr>
	        	<th class="ColsThFirst">STT</th>
	            <th>Số dòng đơn hàng</th>
	            <th>Mã sản phẩm</th>
	            <th>Tên sản phẩm</th>
	            <th>Quy cách</th>
	            <th>Đơn giá</th>
	            <th>Số lượng</th>
	            <th class="ColsThEnd">Thành tiền</th>
	        </tr>
	    </thead>
	    <tbody id="pocscfTable">	
			<s:if test="lstPoManualVODetail != null && lstPoManualVODetail.size() > 0">					
				<s:iterator value="lstPoManualVODetail" status="status">
					<tr>
						<td class="ColsTd1" style="text-align: center"><s:property value="#status.index+1" /></td>
						<td class="ColsTd2" style="text-align: center"><s:property value="poLineNumber" /></td>
						<td class="ColsTd3 AlignLeftDetail"><s:property value="productCode"/></td>
						<td class="ColsTd4 AlignLeftDetail"><s:property value="productName"/></td>
						<td class="ColsTd5 AlignRightDetail"><s:property value="displayNumber(convfact)"/></td>
						<td class="ColsTd6 AlignRightDetail"><s:property value="displayNumber(packagePrice)"/>/<s:property value="displayNumber(price)"/></td>
						<td class="ColsTd7 AlignRightDetail"><s:property value="displayNumber(packageQuantity)"/>/<s:property value="displayNumber(retailQuantity)"/></td>
						<td class="ColsTd8 ColsTdEnd AlignRightDetail"><s:property value="convertMoney(amount)"/></td>
					</tr>
				</s:iterator>
				<tr>
					<td class="ColsTd1 AlignRightBoldDetail" colspan="7">Tổng</td>
					<td class="ColsTd2 ColsTdEnd AlignRightBoldDetail"><s:property value="displayNumber(totalAmountDetail)"/></td>
				</tr>
			</s:if>
			<s:else>
				<p class="ValueStyle Value1Style">Không có dữ liệu nào</p>
			</s:else>					
		</tbody>
	</table>
</div>
<div class="Clear"></div>
<!-- <p class="TotalInfo TotalLInfo">
	<span class="Tem1Style">Tổng tiền: <span><s:property value="displayNumber(totalAmountDetail)"/></span></span>
</p> -->
<script type="text/javascript">
	$(document).ready(function() {	
		Utils.bindTRbackground();
	});
</script>



<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="ths.dms.helper.Configuration"%>
<%@taglib prefix="k" uri="/kryptone"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<style type="text/css">
</style> 
<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="/po-manual-manage/info">Mua hàng</a></li>
		<li><span>Quản lý đơn hàng</span></li>
	</ul>
</div>
<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin tìm kiếm</h2>
				<div class="SearchInSection SProduct1Form" id="search-form">
					<label class="LabelStyle Label1Style"><s:text name="image_manager_unit"/> <span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect ">
						<input  type="text" class="InputTextStyle InputText1Style" style="width: 205px;" id="cbxUnit" maxlength="50" />
					</div>
					<label class="LabelStyle Label1Style">Loại đơn</label> 
					<div class="BoxSelect BoxSelect2">
						<select id="type" class="MySelectBoxClass" onchange="POManualManage.hideAnhShowButtomPo();" autocomplete="off">
							<option value="1">Đơn đặt hàng</option>
                    		<option value="2">Đơn trả hàng</option>
						</select>
					</div>
					<label class="LabelStyle Label1Style">Trạng thái</label> 
					<div class="BoxSelect BoxSelect2">
						<select id="approvedStep" class="MySelectBoxClass" autocomplete="off">
							<option value="-1">Tất cả</option>
							<option value="1">Chờ gửi</option>
                    		<option value="2">Chờ duyệt</option>
                    		<option value="3">Đã duyệt</option>
                    		<option value="5">Từ chối</option>
                    		<option value="4">Đã nhập/xuất hàng</option>
                    		<option value="6">Hủy</option>
						</select>
					</div>
		            <div class="Clear"></div>
		            <label class="LabelStyle Label1Style">Số đơn</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="poNumber" autocomplete="off" />
					<label class="LabelStyle Label1Style">Từ ngày</label>
					<input id="fDate" class="InputTextStyle InputText6Style" value="<s:property value='fromDate' />">
					<label class="LabelStyle Label1Style">Đến ngày</label>
					<input id="tDate" class="InputTextStyle InputText6Style" value="<s:property value='toDate' />">
     				<div class="Clear"></div>
					<div class="BtnCSection">
						<button class="BtnGeneralStyle" id="btnSearch" onclick="return POManualManage.searchPo();">Tìm kiếm</button>
					</div>
					<div class="Clear"></div>
					<p class="ErrorMsgStyle SpriteErr" id="errMsgSearch" style="display: none;"></p>
					<div class="Clear"></div>
				</div>
				<div class="Clear"></div>
				<h2 class="Title2Style">Danh sách đơn hàng</h2>
				<div class="SearchInSection SProduct1Form">
                    <div class="GridSection">
                		<div id="searchPoResult" class="GeneralTable">
							<div class="BoxGeneralTTitle" style="width: 100%;" id="dgGridContainer">
								<table id="grid"></table>
								<div id="pager"></div>
							</div>
						</div>
		                <div class="Clear"></div>
		                <div class="BtnCenterSection">
		                	<button class="BtnGeneralStyle cmsiscontrol" id="btnDestroy" onclick="return POManualManage.updateStatusOrderManual(ApprovalStepPo.DESTROY);" style="margin-right:20px;">Hủy</button>
		                    <button class="BtnGeneralStyle cmsiscontrol" id="btnNew" onclick="return POManualManage.updateStatusOrderManual(ApprovalStepPo.NEW);" style="margin-right:20px;">Gửi</button>
		                    <button class="BtnGeneralStyle cmsiscontrol" id="btnRejected" onclick="return POManualManage.updateStatusOrderManual(ApprovalStepPo.REJECTED);" style="margin-right:20px;">Từ chối</button>
		                    <button class="BtnGeneralStyle cmsiscontrol" id="btnApproved" onclick="return POManualManage.updateStatusOrderManual(ApprovalStepPo.APPROVED);" style="margin-right:20px;">Duyệt</button>
		                    <div class="Clear"></div>
		                </div>
	                    <div class="Clear"></div>
	                    <p id="successMsg" class="SuccessMsgStyle" style="display: none; padding-left: 10px;"></p>
	                    <br>
	                    <p class="ErrorMsgStyle SpriteErr" id="errMsg" style="display: none; margin-left: 5px;"></p>
                      </div>
                </div>
				<div class="Clear"></div>
				<div id="divPOCSConfirmTable" style="display: none"></div>		
			</div>
		</div>
		<div class="Clear"></div>
	</div>
</div>
<div class="Clear"></div>

<!-- poup err update status -->
<div id="popup-error-container" style="display:none; width: 630px; height: auto;">
	<div id="popupErrorStatus">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form">
				<div id="gridErrorContainer">
					<div id="gridError"></div>
				</div>
				<div class="Clear"></div>
			</div>
			<div class="Clear"></div>
				<p class="ErrorMsgStyle SpriteErr" id="errMsgPopup" style="display: none;"></p>
			<div class="Clear"></div>
		<div class="SProduct1Form">
			<div class="BtnCenterSection">
				<button class="BtnGeneralStyle" id="popupBtnDestroy" onclick="return $('#popupErrorStatus').dialog('close');" >Đóng</button>
			</div>
		</div>
	</div>
	</div>
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="curShopId" name="shopId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	//load Cay don vi cbx
	ReportUtils.loadComboTree('cbxUnit', 'shopId', $('#curShopId').val(), function(shopId) {
		/*var param = {};
		param.shopId = $('#shopId').val().trim();
		param.typeCurrentUser = StaffSpecType.STAFF;
		param.isChoiceAll = true;
		Utils.initStaffCbx('cbxStaff', '/commons/load-staff-inherit', param, 208, function (rec) {

		});*/
		POManualManage.initGridPo(); 
		//disabled('btnApproved');
		$('#btnApproved').hide();
		//disabled('btnRejected');
		$('#btnRejected').hide();
	});
	
	setDateTimePicker('fDate');	
	setDateTimePicker('tDate');
	/*ReportUtils.setCurrentDateForCalendar('fDate');
	ReportUtils.setCurrentDateForCalendar('tDate');*/
	$('.MySelectBoxClass').css('width','206');
	$('.CustomStyleSelectBox').css('width','173');
	// mac dinh dat hang: POManualManage.hideAnhShowButtomPo();
});
</script>
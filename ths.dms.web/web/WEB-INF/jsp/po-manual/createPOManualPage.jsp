<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style type="text/css">
.datagrid-footer td[field=productCode], .datagrid-footer td[field=productName],
.datagrid-footer td[field=warehouseId], .datagrid-footer .datagrid-td-rownumber {
	border-right-color:transparent !important;
}
</style>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);" class="cmsdefault">Mua hàng</a></li>
		<li><span>Lập PO Manual</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
			
				<h2 class="Title2Style">Thông tin chung</h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Đơn vị <span class="ReqiureStyle">*</span></label>
                	<div class="BoxSelect BoxSelect2">
                		<input  type="text" class="InputTextStyle InputText1Style" id="cbxShop" maxlength="50" autocomplete="off"/>
                	</div>
                	<label class="LabelStyle Label2Style">Loại đơn <span class="ReqiureStyle">*</span></label>
                	<div class="BoxSelect BoxSelect2">
						<select id="type" class="MySelectBoxClass" onchange="POManualCreate.changeType()">
							<option value="1">Đặt hàng</option>
                    		<option value="2">Trả hàng</option>
						</select>
					</div>
					<label class="LabelStyle Label2Style">Số PO</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="poNumber" disabled="disabled" autocomplete="off" />
					<div class="Clear"></div>

					<label class="LabelStyle Label1Style">Kho <span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2">
						<select class="MySelectBoxClas" id="warehouse" style="width: 206px;">
							<%-- <s:iterator value="lstStaff" var="obj">
								<option value="<s:property value='#obj.staffCode' />"><s:property value='#obj.staffName' /></option>
							</s:iterator> --%>
						</select>
					</div>
					<label class="LabelStyle Label2Style">Đơn gốc (F9)</label>
					<input type="text" class="InputTextStyle InputText1Style" id="poNumberParent" onchange="return POManualCreate.changePoNumberParent(this);" disabled="disabled" autocomplete="off" />
					<label class="LabelStyle Label2Style">Ngày lập đơn</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="poDate" value="<s:property value="fromDate"/>" disabled="disabled" autocomplete="off" />
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Ngày yêu cầu giao hàng</label>
					<input id="requestDate" class="InputTextStyle InputText6Style" type="text" autocomplete="off" />
					<label class="LabelStyle Label2Style">Ghi chú <span class="ReqiureStyle"></span></label>
					<textarea maxlength="200" rows="3" cols="10" id="poNote" style="width: 528px; margin-left: 0px; border: 1px solid #c4c4c4; font-size: 1em;"></textarea>
					<div class="Clear"></div>
	                <p id="errMsgInfo" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				</div>
				
				<h2 class="Title2Style">Danh sách sản phẩm
					<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" id="loadingProduct">
				</h2>
				<div class="SearchInSection SProduct1Form" id="spSaleProductInfor" style="">                	
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div id="gridSalePOData"></div>
									<div class="Clear"></div>
									<p class="TotalInfo" style="margin-top:10px; width: 95%">
	                                	<span class="Tem1Style"><s:text name="create_order_tong_trong_luong"/> <span style="color: #FF0000" id="totalWeightSale">0</span><span style="color: #FF0000"> (Kg)</span></span>
	                                	<span id="sumPrice1">
	                                		<span class="Tem1Style"><s:text name="stock.update.title.tong.so.luong"/> <span style="color: #FF0000" id="totalQuantitySale">0</span></span>
	                                    	<span class="Tem1Style"> <s:text name="create_order_tong_tien"/> <span style="color: #FF0000" id="totalAmountSale">0</span><span style="color: #FF0000"> (VNĐ)</span></span>
                                		</span>
                                	</p>
									<!-- <div class="ButtonSection" style="margin-top:20px;text-align: right;">										
										<button style="margin-right: 10px;" class="BtnGeneralStyle BtnGeneralMStyle Sprite2" id="payment" onclick="return SPCreateOrder.paymentPrepare();">
											<span class="Sprite2"><s:text name="create_order_tinh_tien"/></span>
										</button>
										<a href="javascript:void(0)" style="margin-right: 10px;" id="backBtn" title="<s:text name="create_order_tinh_tien_lai"/>">
											<img src="/resources/images/icon-back.png" width="20" height="20" />
										</a>	
										<div style="height: 15px;">
											<p id="alertSuccess_successMsg" class="SuccessMsgStyle" style="display: none;padding: 0;" >Tính tiền thành công cho đơn hàng !</p>
											<p style="display: none;" class="ErrorMsgStyle" id="saleInputErr"></p>
										</div>										
									</div> -->		
								</div>
							</div>
						</div>
					</div>  
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnBackCreate" onClick="POManualManage.backPoManage();" title="Quay lại danh sách PO">Quay lại</button>&nbsp &nbsp
						<button class="BtnGeneralStyle" id="btnNewCreate" onClick="POManualCreate.redirectOrderManual();" title="Tạo lại 1 đơn hàng mới">Tạo đơn mới</button>&nbsp &nbsp
						<button class="BtnGeneralStyle" id="btnSaveCreate" onClick="POManualCreate.saveOrderManual(ActionType.INSERT);" title="Lưu đơn hàng">Lưu</button>
					</div>
					<div class="Clear"></div>              	
                </div>
				<!-- <div class="SearchInSection SProduct1Form">
					<div class="GridSection" id="gridContainer" style="padding:0;">
						<div id="dgrid"></div>
					</div>
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnSave" onClick="POManualCreate.saveOrderManual();" >Lưu</button>
					</div>
					<div class="Clear"></div>
				</div> -->
				<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />		
			</div>
		</div>
	</div>
</div>
<%-- pop up lay don hang goc POVNM --%>
<div id ="popupPoNumberParentDiv" style="display: none" >
	<div id="popupPoNumberParent" class="easyui-dialog" title="Import" data-options="closed:true,modal:true">
		<div class="PopupContentMid">
			<div class="GeneralForm Search1Form" >
				<label class="LabelStyle Label7Style">Số đơn hàng </label>
				<input id="popOrderNumber" maxlength="50" type="text" class="InputTextStyle InputText5Style"/>
				<label class="LabelStyle Label7Style">Số ASN </label>
				<input id="popAsn" maxlength="250" type="text" class="InputTextStyle InputText5Style"/>
				<div class="Clear"></div>
				<label class="LabelStyle Label7Style">Từ ngày </label>
					<input id="popFromDate" class="InputTextStyle InputText6Style" value="<s:property value='fromDate' />">
					<label class="LabelStyle Label7Style">Đến ngày </label>
					<input id="popToDate" class="InputTextStyle InputText6Style" value="<s:property value='toDate' />">
				<div class="Clear"></div>
				<div class="BtnCenterSection">
					<button id="popBtnSearch" onclick="return POManualCreate.searchPoParent();" class="BtnGeneralStyle">Tìm kiếm</button>
				</div>
				<!-- <div class="SearchInSection SProduct1Form"> -->
				<div class="SProduct1Form">
                    <div class="GridSection">
                		<div id="searchPoParentResult" class="GeneralTable">
							<div class="BoxGeneralTTitle" style="width: 100%;" id="dgGridContainerPop">
								<table id="gridPoParent"></table>
								<div id="pager"></div>
							</div>
						</div>
		                <div class="Clear"></div>
		                <!-- <div class="BtnCenterSection">
		                	<button class="BtnGeneralStyle BtnMSection" id="btnApproved" onclick="return FeedBack.approvedFeedback(FeedbackStatus.APPROVED);" style="float:right; margin-right:20px;">Duyệt</button>
		                </div> -->
	                    <div class="Clear"></div>
	                    <p id="errMsgPop" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
						<p id="successMsgPop" class="SuccessMsgStyle" style="display: none"></p>
                    </div>
                </div>
				<div class="BtnCenterSection">
					<button id="popBtnClose" onclick="$('#popupPoNumberParent').dialog('close');" class="BtnGeneralStyle">Bỏ qua</button>
				</div>
				<input type="hidden" id="idPo" value=""/>
			</div>
		</div>
	</div>
</div>
<s:hidden id="shopId" ></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	$('.MySelectBoxClass').css('width','206');
	$('.CustomStyleSelectBox').css('width','173');
	$('#poNumberParent').val('');
	$('#poNote').val('');
	applyDateTimePicker("#requestDate");
	ReportUtils.setCurrentDateForCalendar('requestDate');
	POManualCreate._TYPE = PoManualType.PO_IMPORT;
	GridSalePOBusiness.initSalePOGrid();
	// vuongmq; 04/12/2015; lay danh sach combobox don vi
	Utils.initUnitCbx('cbxShop', {}, null, function(rec) {
		console.log('shopId ne: ' + rec.id);
		$('#shopId').val(rec.id);
		hideAllMessage();
		var url = '/commons/change-shop-get-warehouse';
		var params = new Object();
		params.shopCode = rec.shopCode;
		Utils.changeShopGetWareHouse('#warehouse', url, params, 206, function(data) {
			POManualCreate._warehouseIdOld = data.warehouseId;
			POManualCreate.changeType();
		});
		
	});
	disabled('btnSaveComplete');
	// xu ly F9 don hang gon; Tra hang
	$('#poNumberParent').bind('keyup', function(event) {
		if (event.keyCode == keyCode_F9) {
			POManualCreate.openPopupPoParent();
		}
	});
});
</script>
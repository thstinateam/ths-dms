<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style type="text/css">
.datagrid-footer td[field=productCode], .datagrid-footer td[field=productName],
.datagrid-footer td[field=warehouseId], .datagrid-footer .datagrid-td-rownumber {
	border-right-color:transparent !important;
}
</style>

<div class="BreadcrumbSection">
	<ul class="ResetList FixFloat BreadcrumbList">
		<li class="Sprite1"><a href="javascript:void(0);" class="cmsdefault">Mua hàng</a></li>
		<li><span>Chỉnh sửa PO Manual</span></li>
	</ul>
</div>

<div class="CtnOneColSection">
	<div class="ContentSection">
		<div class="ToolBarSection">
			<div class="SearchSection GeneralSSection">
				<h2 class="Title2Style">Thông tin chung đơn hàng: <s:property value="poNumber"/></h2>
				<div class="SearchInSection SProduct1Form">
					<label class="LabelStyle Label1Style">Đơn vị <span class="ReqiureStyle">*</span></label>
                	<!-- <div class="BoxSelect BoxSelect2"> -->
                		<input  type="text" class="InputTextStyle InputText1Style" id="cbxShop" value='<s:property value="shopCode"/> - <s:property value="shopName"/>' title='<s:property value="shopCode"/> - <s:property value="shopName"/>' disabled="disabled" maxlength="50" autocomplete="off"/>
                	<!-- </div> -->
                	<label class="LabelStyle Label2Style">Loại đơn <span class="ReqiureStyle">*</span></label>
                	<div class="BoxSelect BoxSelect2">
						<s:if test="type == 1">
							<select id="type" class="MySelectBoxClass">
								<option value="1">Đặt hàng</option>
							</select>
						</s:if>
						<s:else>
							<select id="type" class="MySelectBoxClass">
								<option value="2">Trả hàng</option>
							</select>
						</s:else>
					</div>
					<label class="LabelStyle Label2Style">Số PO</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="poNumber" value='<s:property value="poNumber"/>' disabled="disabled" autocomplete="off" />
					<div class="Clear"></div>

					<label class="LabelStyle Label1Style">Kho <span class="ReqiureStyle">*</span></label>
					<div class="BoxSelect BoxSelect2">
						<select id="warehouse" class="MySelectBoxClass">
							<option value="<s:property value='warehouseId' />"><s:property value='warehouseCode' /></option>
						</select>
					</div>
					<label class="LabelStyle Label2Style">Đơn gốc (F9)</label>
					<input type="text" class="InputTextStyle InputText1Style" id="poNumberParent" value='<s:property value="refPoNumber"/>' disabled="disabled" autocomplete="off" />
					<label class="LabelStyle Label2Style">Ngày lập đơn</label> 
					<input type="text" class="InputTextStyle InputText1Style" id="poDate" value="<s:property value="fromDate"/>" disabled="disabled" autocomplete="off" />
					<div class="Clear"></div>
					<label class="LabelStyle Label1Style">Ngày yêu cầu giao hàng</label>
					<input id="requestDate" class="InputTextStyle InputText6Style" type="text" autocomplete="off" value="<s:property value="requestDate"/>" />
					<label class="LabelStyle Label2Style">Ghi chú <span class="ReqiureStyle"></span></label>
					<textarea maxlength="200" rows="3" cols="10" id="poNote" style="width: 528px; margin-left: 0px; border: 1px solid #c4c4c4; font-size: 1em;"><s:property value="note"/></textarea>
					<div class="Clear"></div>
	                <p id="errMsgInfo" style="display: none;" class="ErrorMsgStyle SpriteErr"></p>
				</div>
				
				<h2 class="Title2Style">Danh sách sản phẩm
					<img style="display: none;" src="/resources/scripts/plugins/jquery-easyui-1.3.2/themes/default/images/loading.gif" id="loadingProduct">
				</h2>
				<div class="SearchInSection SProduct1Form" id="spSaleProductInfor" style="">                	
                	<div class="GeneralMilkBox">
						<div class="GeneralMilkTopBox">
							<div class="GeneralMilkBtmBox">
								<div class="GeneralMilkInBox">
									<div id="gridSalePOData"></div>
									<div class="Clear"></div>
									<p class="TotalInfo" style="margin-top:10px; width: 95%">
	                                	<span class="Tem1Style"><s:text name="create_order_tong_trong_luong"/> <span style="color: #FF0000" id="totalWeightSale">0</span><span style="color: #FF0000"> (Kg)</span></span>
	                                	<span id="sumPrice1">
	                                		<span class="Tem1Style"><s:text name="stock.update.title.tong.so.luong"/> <span style="color: #FF0000" id="totalQuantitySale">0</span></span>
	                                    	<span class="Tem1Style"> <s:text name="create_order_tong_tien"/> <span style="color: #FF0000" id="totalAmountSale">0</span><span style="color: #FF0000"> (VNĐ)</span></span>
                                		</span>
                                	</p>	
								</div>
							</div>
						</div>
					</div>  
					<div class="BtnCenterSection">
						<button class="BtnGeneralStyle" id="btnBackEdit" onClick="POManualManage.backPoManage();" title="Quay lại danh sách PO">Quay lại</button>&nbsp &nbsp
						<button class="BtnGeneralStyle" id="btnNewEdit" onClick="POManualCreate.redirectOrderManual();" title="Tạo lại 1 đơn hàng mới">Tạo đơn mới</button>&nbsp &nbsp
						<button class="BtnGeneralStyle" id="btnSaveEdit" onClick="POManualCreate.saveOrderManual(ActionType.UPDATE);" title="Lưu đơn hàng">Lưu</button>
					</div>
					<div class="Clear"></div>              	
                </div>
				<tiles:insertTemplate template="/WEB-INF/jsp/general/successMsg.jsp" />		
			</div>
		</div>
	</div>
</div>
<s:hidden id="shopId" name="shopId"></s:hidden>
<s:hidden id="shopCode" name="shopCode"></s:hidden>
<s:hidden id="valueEdit" name="valueEdit"></s:hidden>
<s:hidden id="poId" name="poId"></s:hidden>
<script type="text/javascript">
$(document).ready(function() {
	$('.MySelectBoxClass').css('width','206');
	$('.CustomStyleSelectBox').css('width','173');
	disableSelectbox('type');
	disableSelectbox('warehouse');
	applyDateTimePicker("#requestDate");
	var type = '<s:property value="type"/>';
	POManualCreate._TYPE = type;
	GridSalePOBusiness.initSalePOGrid();
	GridSalePOBusiness.editProductPO(type);
});
</script>
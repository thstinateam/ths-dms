package ths.dms.core.common.utils;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;



public class CDateUtil {
	public static final String DATE_FORMAT_STR = "dd/MM/yyyy";

	public static final String DATETIME_FORMAT_STR = "dd/MM/yyyy HH:mm";
	
	public static final String DATE_FORMAT_STR_PRO= "dd/MM/yyyy";
	
	public static final String DATETIME_FORMAT_STR_PRO = "HH:mm dd/MM/yyyy";
	
	public static final String DATETIME_FORMAT_STR_PRO1 = "HH:mm:ss";
	
	public static final String TIME_FORMAT_STR = "HH:mm";

	public static final String ENDTIME = "endTime";

	public static final String BEGINTIME = "beginTime";

	public static final String DEFAULT_DATE = "01/01/1980";
	
	public static final String DATE_FORMAT_NOW = "dd/MM/yyyy HH:mm:ss";
	public static final String DATE_FORMAT_EXCEL_FILE = "yyyyMMddHHmmss";
	
	public static final String DATE_DD_MM = "dd/MM";
	public static final String DATE_DD_MM_YY = "dd/MM/yy";
	public static final String DATE_D_M = "d/M";
	public static final String DATE_M_Y = "MM/yyyy";
	public static final String HSSF_DATE_FORMAT_M_D_YY = "m/d/yy";
	public static final String DATA_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
	public static final String DATE_FORMAT_FILE_EXPORT = "yyyyMMddHHmmss";
	
	public static final String DATE_FORMAT_ATTRIBUTE = "yyyy-MM-dd";
	
	public static final String DATE_FORMAT_VISIT = "dd-MM-yyyy";
	public static final String DATE_FORMAT_CSV = "ddMMyyyy";
	
	public static final Integer DATE_YEAR = 2;
	public static final Integer DATE_MONTH = 1;
	public static final int[] arrDays = {31,28,31,30,31,30,31,31,30,31,30,31};
	
	public static String toYearString(Date date) {
		String dateStr = "";
		try {
			dateStr = new SimpleDateFormat("yyyy").format(date);
		} catch (Exception e) {
		}
		return dateStr;
	}

	public static String toMonthDayString(Date date) {
		String dateStr = "";
		try {
			dateStr = new SimpleDateFormat("dd/MM").format(date);
		} catch (Exception e) {
		}
		return dateStr;
	}


	public static Date getCurrentGMTDate() {
		Date now = new Date();
		Calendar calendar = new GregorianCalendar();
		int offset = calendar.getTimeZone().getOffset(now.getTime());
		Date gmtDate = new Date(now.getTime() - offset);
		return gmtDate;
	}

	

	public static String toDateString(Date date, String format) {
		String dateString = "";
		if (date == null)
			return dateString;
		Object[] params = new Object[] { date };

		try {
			dateString = MessageFormat
					.format("{0,date," + format + "}", params);
		} catch (Exception e) {

		}
		return dateString;
	}

	/**
	 * In case hour section of date object is ending in the form of "00:00", we
	 * don't display it by using Date_format string instead. 17/01/2008 00:00
	 * --> 17/01/2008
	 * 
	 * @param date
	 *            : Date
	 * @return string form of date
	 */
	public static String toDateString(Date date) {
		String dateString = "";
		String format = null;
		if (date == null)
			return dateString;
		Object[] params = new Object[] { date };
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		if (cal.get(Calendar.HOUR_OF_DAY) == 0 && cal.get(Calendar.MINUTE) == 0) {
			format = CDateUtil.DATE_FORMAT_STR;
		} else {
			format = CDateUtil.DATETIME_FORMAT_STR_PRO;
		}
		try {
			dateString = MessageFormat
					.format("{0,date," + format + "}", params);
		} catch (Exception e) {

		}
		return dateString;
	}
	
}

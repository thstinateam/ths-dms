package ths.dms.core.common.utils;
/**
 * Khai bao hang so dung chung cho web va ws
 * @author tientv11
 * @since 27/01/2015
 */
public class Constant {
	
	public static final String SORT_ASC = "asc";	// sap xep tang
	public static final String SORT_DESC = "desc";	// sap xep giam
	
	// Cau hinh ten PO
	public static final String PO = "PO";
	
	public static final String X = "X"; // loi

	// po_vnm return
	public static final String RE = "RE"; // don tra copy tu Po
	
	//Cau hinh ap_param
	public static final String SYS_SALE_ROUTE = "SYS_SALE_ROUTE";
	public static final String SYS_CAL_DATE = "SYS_CAL_DATE";
	public static final String SYS_CURRENCY = "SYS_CURRENCY";
	public static final String SYS_CURRENCY_DIVIDE = "SYS_CURRENCY_DIVIDE";
	public static final String SYS_DIGIT_DECIMAL = "SYS_DIGIT_DECIMAL";
	public static final String SYS_DECIMAL_POINT = "SYS_DECIMAL_POINT";
	public static final String SYS_MAP_TYPE = "SYS_MAP_TYPE";
	public static final String SYS_DATE_FORMAT = "SYS_DATE_FORMAT";
	public static final String MAX_SIZE_UPLOAD_ATTACH = "MAX_SIZE_UPLOAD_ATTACH";
	public static final String WHITE_LIST_FILE_TYPE_UPLOAD_ATTACH = "WHITE_LIST_FILE_TYPE_UPLOAD_ATTACH";

	public static final String SYS_SHOP_LOCK_EXEC_TIME = "SHOP_LOCK_EXEC_TIME";
	public static final String SYS_INTERVAL_GPS_FUSED_POSITION = "INTERVAL_FUSED_POSITION";
	public static final String SYS_TIME_SYNC_TO_SERVER = "TIME_SYNC_TO_SERVER";
	public static final String SYS_CHECK_DUPLICATE_CUSTOMER_DISTANCE = "SYS_CHECK_DUPLICATE_CUSTOMER_DISTANCE";
	
	//shop_param
	public static final String SYS_USE_LOCKDATE = "SYS_USE_LOCKDATE";
	public static final String SYS_RETURN_DATE = "SYS_RETURN_DATE";
	public static final String SYS_CAL_UNAPPROVED = "SYS_CAL_UNAPPROVED";
	public static final String SYS_MAXDAY_APPROVE = "SYS_MAXDAY_APPROVE";
	public static final String SYS_MAXDAY_RETURN = "SYS_MAXDAY_RETURN";
	public static final String SYS_SHOW_PRICE = "SYS_SHOW_PRICE";
	public static final String SYS_MODIFY_PRICE = "SYS_MODIFY_PRICE";
	public static final String SYS_CAL_PLAN = "SYS_CAL_PLAN";
	
	public static final String DT_START = "DT_START";
	public static final String DT_END = "DT_END";
	public static final String DT_MIDDLE = "DT_MIDDLE";
	public static final String CC_START = "CC_START";
	public static final String CC_END = "CC_END";
	public static final String CC_DISTANCE = "CC_DISTANCE";
	public static final String SHOP_LOCK_EXEC_TIME = "SHOP_LOCK_EXEC_TIME";
	public static final String SHOP_LOCK_EXEC_TIME_SYSDATE_BEFORE_MINUTE = "SHOP_LOCK_EXEC_TIME_SYSDATE_BEFORE_MINUTE";
	public static final String SHOP_LOCK_ABORT_DURATION = "SHOP_LOCK_ABORT_DURATION";
	
	/**
	 * cho phep ghe tham tao ghi chu 
	 */
	public static final String CF_IS_CUSTOMER_WAITING = "CF_IS_CUSTOMER_WAITING";
	
	/**
	 * So lan lay diem trong 1 gio de ve lo trinh giam sat 
	 */
	public static final String CF_NUMBER_POINTS_PER_HOUR = "CF_NUMBER_POINTS_PER_HOUR";
	
	/**
	 * khoang cach ngan nhat
	 */
	public static final String SHORTEST_DISTANCE = "SHORTEST_DISTANCE";
	
	//value 
	public static final String ZERO_TEXT = "0";
	public static final String ONE_TEXT = "1";
	public static final String TWO_TEXT = "2";
	
	public static final Integer ZERO_INT = 0;
	public static final Integer EIGHT_INT = 8;

	// SALE_ORDER_PROMOTION OBJECT
	public static final Integer SALE_ORDER_PROMOTION_OBJECT_SHOP = 1;
	public static final Integer SALE_ORDER_PROMOTION_OBJECT_STAFF = 2;
	public static final Integer SALE_ORDER_PROMOTION_OBJECT_CUSTOMER = 3;
	// SALE_ORDER_PROMOTION UNIT
	public static final Integer SALE_ORDER_PROMOTION_UNIT_QUANTITY = 1;
	public static final Integer SALE_ORDER_PROMOTION_UNIT_NUM = 2;
	public static final Integer SALE_ORDER_PROMOTION_UNIT_AMOUNT = 3;
	
	public static final Integer DAT_KHONG_NHAN_KM = 1;
	public static final Integer DAT_NHAN_MOT_PHAN_KM = 2;
	
	public static final Integer FEEDBACK_CREATE_AND_OWNER_STAFF_ID = 0; // tao va can thuc hien
	public static final Integer FEEDBACK_CREATE_STAFF_ID = 1; // tao
	public static final Integer FEEDBACK_OWNER_STAFF_ID = 2; // can thuc hien
	
	public static final String SYS_RETURN_DATE_VALUE_ORDER_DATE = "1";
	public static final String SYS_RETURN_DATE_VALUE_APPROVED_DATE = "2";
	public static final String SYS_RETURN_DATE_VALUE_LOCK_DATE = "3";
	
	public static final String SYS_WAREHOUSE_CONFIG_NOT_DIVIDUAL = "0";
	public static final String SYS_WAREHOUSE_CONFIG_DIVIDUAL = "1";
	
	public static final String SYS_CONVERT_QUANTITY_CONFIG_ZEROQUANTITY = "0";
	public static final String SYS_CONVERT_QUANTITY_CONFIG_QUANTITYZERO = "1";
	
	public static final String RETURN_KEYSHOP_CONFIG_NO = "0";
	public static final String RETURN_KEYSHOP_CONFIG_YES = "1";
	
	public static final String VALUE_ALL = "-1";
	public static final int MAX_NUMBER_PARAM_ID = 900;
	
}

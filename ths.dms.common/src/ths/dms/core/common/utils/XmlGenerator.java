package ths.dms.core.common.utils;

import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import ths.dms.core.entities.vo.OrderXmlDetailVO;
import ths.dms.core.entities.vo.OrderXmlHeaderVO;
import ths.dms.core.entities.vo.OrderXmlLineDataVO;
import ths.dms.core.entities.vo.OrderXmlVO;


public class XmlGenerator {
	
	static XmlGenerator intance;
	
	DocumentBuilder docBuilder;
	
	public static XmlGenerator getIntance() {
		if (intance == null) {
			try {
				intance = new XmlGenerator();
			} catch (ParserConfigurationException e) {
				return null;
			}
		}
		return intance;
	}

	public XmlGenerator() throws ParserConfigurationException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		docBuilder = docFactory.newDocumentBuilder();
	}

	public String generate(OrderXmlVO salesOrder) {
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("SalesOrder");
		doc.appendChild(rootElement);
		Element newSalesOrder = doc.createElement("NewSalesOrder");
		rootElement.appendChild(newSalesOrder);

		// Create header
		Element orderHeader = doc.createElement("SOHeader");
		newSalesOrder.appendChild(orderHeader);
		OrderXmlHeaderVO soh = salesOrder.getHeader();
		Map<String, String> data = soh.getData();
		Set<String> keys = data.keySet();
		for (Iterator<String> iterator2 = keys.iterator(); iterator2.hasNext();) {
			String key = (String) iterator2.next();
			Element element = doc.createElement(key);
			if (data.get(key) == null) {
				Attr attr = doc.createAttribute("xml:space");
				attr.setValue("preserve");
				element.setAttributeNode(attr);
				//element.appendChild(doc.createTextNode(""));
			} else {
				element.appendChild(doc.createTextNode(data.get(key)));

			}
			orderHeader.appendChild(element);
		}

		// Create Order Detail
		Element orderDetail = doc.createElement("SODetail");
		newSalesOrder.appendChild(orderDetail);
		OrderXmlDetailVO sod = salesOrder.getDetail();
		List<OrderXmlLineDataVO> lineDetails = sod.getLineDetails();
		for (Iterator<OrderXmlLineDataVO> iterator2 = lineDetails.iterator(); iterator2.hasNext();) {
			OrderXmlLineDataVO lineDetail = (OrderXmlLineDataVO) iterator2.next();

			Element line = doc.createElement("Line");
			orderDetail.appendChild(line);

			Set<String> keyLineDetail = lineDetail.getData().keySet();
			for (Iterator<String> iterator3 = keyLineDetail.iterator(); iterator3.hasNext();) {
				String key = (String) iterator3.next();

				Element element = doc.createElement(key);

				if (lineDetail.getData().get(key) == null) {
					Attr attr = doc.createAttribute("xml:space");
					attr.setValue("preserve");
					element.setAttributeNode(attr);
					//element.appendChild(doc.createTextNode(""));
				} else {
					element.appendChild(doc.createTextNode(lineDetail.getData().get(key)));
				}
				line.appendChild(element);

			}
		}

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
		}
		DOMSource source = new DOMSource(doc);
		ByteArrayOutputStream baops = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(baops);

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);
		try {
			if (transformer != null) {
				transformer.transform(source, result);
			}
			return new String(baops.toByteArray());
		} catch (TransformerException e) {
			return null;
		}
	}
}
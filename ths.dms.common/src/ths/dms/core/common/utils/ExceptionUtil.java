/*
 * Copyright 2014 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.common.utils;
/**
 * manipulate with exception
 * @author tuannd20
 * @date 06/11/2014
 */
public class ExceptionUtil {
	public static <T> Boolean isExceptionCauseBy(Throwable sourceException, Class<T> checkException) {
		if (sourceException != null) {
			if (sourceException.getClass() == checkException) {
				return true;
			}
			Throwable cause = sourceException.getCause();
			while (cause != null) {
				if (cause.getClass() == checkException) {
					return true;
				}
				cause = cause.getCause();
			}
		}
		return false;
	}
}

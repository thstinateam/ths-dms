package ths.dms.core.common.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class GroupUtility {
	//sontt customize from ThuatTQ:
	public static <T> Map<String, List<T>> collectionToMap(Collection<T> list,
			String groupBy) throws Exception {
		TreeMap<String, List<T>> hash = new TreeMap<String, List<T>>();

		for (T obj : list) {
			Class<?> klass = obj.getClass();
			String groupByGetter = "get"
					+ groupBy.substring(0, 1).toUpperCase()
					+ groupBy.substring(1);

			try {
				// dynamic method invocation
				Method m = klass.getMethod(groupByGetter);
				Object result = m.invoke(obj);
				String resultAsKey = result.toString();

				List<T> arrayList;

				if (hash.containsKey(resultAsKey)) {
					arrayList = hash.get(resultAsKey);
				} else {
					arrayList = new ArrayList<T>();

					hash.put(resultAsKey, arrayList);
				}

				arrayList.add(obj);

			} catch (SecurityException e) {
				throw e;
			} catch (NoSuchMethodException e) {
				throw e;
			} catch (IllegalAccessException e) {
				throw e;
			} catch (IllegalArgumentException e) {
				throw e;
			} catch (InvocationTargetException e) {
				throw e;
			}
		}

		return hash;
	}

	/**
	 * Nhom mot collection cac doi tuong theo mot thuoc tinh nao do, cac doi
	 * tuong nay phai tuan theo cau truc POJO (cac thuoc tinh truy cap qua get,
	 * set)
	 * 
	 * @param list
	 * @param groupBy
	 * @return
	 * @throws Exception
	 * @author ThuatTQ
	 */
	public static  <T, K> Map<K, List<T>> collectionToMap(Class<K> clazz,Collection<T> list,
			String groupBy) throws Exception {
		TreeMap<K, List<T>> hash = new TreeMap<K, List<T>>();

		for (T obj : list) {
			Class<?> klass = obj.getClass();
			String groupByGetter = "get"
					+ groupBy.substring(0, 1).toUpperCase()
					+ groupBy.substring(1);

			try {
				// dynamic method invocation
				Method m = klass.getMethod(groupByGetter);
				Object result = m.invoke(obj);
				K resultAsKey = clazz.cast(result);

				List<T> arrayList;

				if (hash.containsKey(resultAsKey)) {
					arrayList = hash.get(resultAsKey);
				} else {
					arrayList = new ArrayList<T>();

					hash.put(resultAsKey, arrayList);
				}

				arrayList.add(obj);

			} catch (SecurityException e) {
				throw e;
			} catch (NoSuchMethodException e) {
				throw e;
			} catch (IllegalAccessException e) {
				throw e;
			} catch (IllegalArgumentException e) {
				throw e;
			} catch (InvocationTargetException e) {
				throw e;
			}
		}

		return hash;
	}


	public static Map<String, List<Object>> collectionToHash(Collection list,
			String groupBy) throws Exception {
		TreeMap<String, List<Object>> hash = new TreeMap<String, List<Object>>();

		for (Object obj : list) {
			Class<?> klass = obj.getClass();
			String groupByGetter = "get"
					+ groupBy.substring(0, 1).toUpperCase()
					+ groupBy.substring(1);

			try {
				// dynamic method invocation
				Method m = klass.getMethod(groupByGetter);
				Object result = m.invoke(obj);
				String resultAsKey = result.toString();

				List<Object> arrayList;

				if (hash.containsKey(resultAsKey)) {
					arrayList = hash.get(resultAsKey);
				} else {
					arrayList = new ArrayList<Object>();

					hash.put(resultAsKey, arrayList);
				}

				arrayList.add(obj);

			} catch (SecurityException e) {
				throw e;
			} catch (NoSuchMethodException e) {
				throw e;
			} catch (IllegalAccessException e) {
				throw e;
			} catch (IllegalArgumentException e) {
				throw e;
			} catch (InvocationTargetException e) {
				throw e;
			}
		}

		return hash;
	}

	/**
	 * Nhom 1 collection cac doi tuong theo mot nhom cac thuoc tinh, cac doi
	 * tuong nay phai tuan theo cau truc POJO (cac thuoc tinh truy cap qua get,
	 * set)
	 * 
	 * @param list
	 * @param groupBy
	 * @return
	 * @throws Exception
	 * @author ThuatTQ
	 */
	public static Map<String, List<Object>> collectionToHash(Collection list,
			List<String> groupBy) throws Exception {
		TreeMap<String, List<Object>> hash = new TreeMap<String, List<Object>>();

		String key = "";
		for (Object obj : list) {
			Class<?> klass = obj.getClass();

			try {
				for (int i = 0; i < groupBy.size(); i++) {
					// dynamic method invocation
					String methodName = "get"
							+ groupBy.get(i).substring(0, 1).toUpperCase()
							+ groupBy.get(i).substring(1);
					Method m = klass.getMethod(methodName);
					Object result = m.invoke(obj);
					String resultAsKey = result.toString();

					key += resultAsKey + "-";
				}

				List<Object> arrayList;

				if (hash.containsKey(key)) {
					arrayList = hash.get(key);
				} else {
					arrayList = new ArrayList<Object>();

					hash.put(key, arrayList);
				}

				arrayList.add(obj);

			} catch (SecurityException e) {
				throw e;
			} catch (NoSuchMethodException e) {
				throw e;
			} catch (IllegalAccessException e) {
				throw e;
			} catch (IllegalArgumentException e) {
				throw e;
			} catch (InvocationTargetException e) {
				throw e;
			}
		}

		return hash;
	}
	
	//SangTN
	public static <T> Map<String, List<T>> collectionToHash2(Collection<T> list,
			List<String> groupBy) throws Exception {
		TreeMap<String, List<T>> hash = new TreeMap<String, List<T>>();

		
		for (T obj : list) {
			Class<?> klass = obj.getClass();

			String key = "";
			try {
				for (int i = 0; i < groupBy.size(); i++) {
					// dynamic method invocation
					String methodName = "get"
							+ groupBy.get(i).substring(0, 1).toUpperCase()
							+ groupBy.get(i).substring(1);
					Method m = klass.getMethod(methodName);
					Object result = m.invoke(obj);
					String resultAsKey = result.toString();

					key += resultAsKey + "-";
				}

				List<T> arrayList;

				if (hash.containsKey(key)) {
					arrayList = hash.get(key);
				} else {
					arrayList = new ArrayList<T>();

					hash.put(key, arrayList);
				}

				arrayList.add(obj);

			} catch (SecurityException e) {
				throw e;
			} catch (NoSuchMethodException e) {
				throw e;
			} catch (IllegalAccessException e) {
				throw e;
			} catch (IllegalArgumentException e) {
				throw e;
			} catch (InvocationTargetException e) {
				throw e;
			}
		}

		return hash;
	}
	
	/**
	 * @author tungtt21
	 * @param date
	 * @return
	 */
	public static String toDateSimpleFormatString(Date date){
		String dateString = "";
		String format = null;
		if (date == null)
			return dateString;
		Object[] params = new Object[] { date };
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		format = "dd/MM/yyyy";

		try {
			dateString = MessageFormat
					.format("{0,date," + format + "}", params);
		} catch (Exception e) {

		}
		return dateString;
	}
	
	 /**
	 * Convert money.
	 *
	 * @param money the money
	 * @return the string
	 * @author phut
	 * @since Aug 27, 2012
	 */
	public static String convertMoney(BigDecimal money) {
		String result = "";
		/*String _money = money.longValue() + "";*/
		String _money = money.toBigInteger() + "";
		int isDot = 1;
		for(int i = _money.length(); i > 0; i--) {
			char ch = _money.charAt(i - 1);
			if(isDot == 3 && i != 1) {
				if(_money.charAt(i - 2) == '-') {
					result = ch + result;
					isDot = 0;
				} else {
					result = "," + ch + result;
					isDot = 0;
				}
			} else {
				result = ch + result;
			}
			isDot++;
		}
		return result;
	}
}

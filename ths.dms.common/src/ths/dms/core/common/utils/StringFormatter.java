/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.common.utils;

/**
 * Lop format chuoi
 * 
 * @author nhutnn
 * @since 06/10/2015
 */
public class StringFormatter {
	public static final String ERROR_MAX_CODE = "ERROR_MAX_CODE";

	/**
	 * Formatter chuoi <b>thung/le</b>
	 * 
	 * @author nhutnn
	 * @param packing thung
	 * @param unit le
	 * @return chuoi thung/le
	 * @since 06/10/2015
	 */
	public static String formatTextStockQuantity(String packing, String unit) {
		final String DIVISION_SYMBOL = "/";
		if (packing != null && packing != "" && unit != null && unit != "") {
			return packing + DIVISION_SYMBOL + unit;
		}
		return null;
	}
	
	/**
	 * Formatter chuoi <b>thung/le</b>
	 * 
	 * @author nhutnn
	 * @param packing thung
	 * @param unit le
	 * @return chuoi thung/le
	 * @since 06/10/2015
	 */
	public static String formatTextStockQuantity(Integer packing, Integer unit) {
		if (packing != null && unit != null) {
			return formatTextStockQuantity(String.valueOf(packing), String.valueOf(unit));			
		}
		return null;
	}
	
	/**
	 * Formatter chuoi SĐT <b>co dinh - di dong</b>
	 * 
	 * @author nhutnn
	 * @param phone SĐT co dinh
	 * @param mobiphone SĐT di dong
	 * @return chuoi SĐT <b>co dinh - di dong</b>
	 * @since 07/10/2015
	 */
	public static String formatTextPhoneAndMobiphone(String phone, String mobiphone) {
		final String HYPHENS = " - ";
		String p = "";
		if (phone != null && phone != "" && mobiphone != null && mobiphone != "") {
			p = phone + HYPHENS + mobiphone;
		} else if (phone != null && phone != "") {
			p = phone;
		} else if (mobiphone != null && mobiphone != "") {
			p = mobiphone;
		}
		return p;
	}
	
	/**
	 * Formatter chuoi ma ten <b>Ma - Ten</b>
	 * 
	 * @author nhutnn
	 * @param code
	 * @param name
	 * @return chuoi ma ten <b>Ma - Ten</b>
	 * @since 06/01/2016
	 */
	public static String formatTextCodeAndName(String code, String name) {		
		return formatTextPhoneAndMobiphone(code, name);
	}
}

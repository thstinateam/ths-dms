/**
 * Copyright (c) 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.common.utils;

import java.awt.geom.Point2D;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Ham tinh toan chung
 * @author tuannd20
 * @since 28/08/2015
 */
public class MathUtil {
	
	enum DistanceMetric {
		KM("KM"),
		M("M");
		
		private String value;
		
		private DistanceMetric(String value) {
			this.value = value;
		}
	}
	
	/**
	 * Tinh khoang cach giua 2 diem dua tren toa do GPS
	 * 
	 * @author tuannd20
	 * @param firstPosition toa do diem dau
	 * @param secondPosition toa do diem sau
	 * @return Khoang cach giua 2 diem (don vi: met) theo toa do GPS
	 * @since 28/08/2015
	 */
	public static double calculateGPSDistance(Point2D.Double firstPosition, Point2D.Double secondPosition) {
		if (firstPosition == null || secondPosition == null) {
			throw new IllegalArgumentException("Null param.");
		}
		return gpsDistance(firstPosition.x, firstPosition.y, secondPosition.x, secondPosition.y, DistanceMetric.M);
	}
	
	/*
	 * This routine calculates the distance between two points (given the
	 * latitude/longitude of those points). It is being used to calculate
	 * the distance between two locations using GeoDataSource (TM) prodducts
	 *
	 * Definitions
	 *   South latitudes are negative, east longitudes are positive
	 *
	 * Passed to function
	 *   lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)
	 *   lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)
	 *   unit = the unit you desire for results
	 *          where: 'K' is kilometers
	 * Worldwide cities and other features databases with latitude longitude
	 * are available at http://www.geodatasource.com
	 *
	 * For enquiries, please contact sales@geodatasource.com
	 *
	 * Official Web site: http://www.geodatasource.com
	 *
	 *          GeoDataSource.com (C) All Rights Reserved 2015
	 */
	public static double gpsDistance(double lat1, double lon1, double lat2, double lon2, DistanceMetric unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (DistanceMetric.M == unit) {
			dist = dist * 1.609344 * 1000;
		} else {
			dist = dist * 1.609344;	// Kilomet
		}
		return dist;
	}

	/*
	 * This function converts decimal degrees to radians
	 */
	private static double deg2rad(double deg) {
	  return (deg * Math.PI / 180.0);
	}

	/*
	 * This function converts radians to decimal degrees
	 */
	private static double rad2deg(double rad) {
	  return (rad * 180 / Math.PI);
	}
	
	/**
	 * tinh khoang cach
	 * @author trietptm
	 * @param lat1
	 * @param lng1
	 * @param lat2
	 * @param lng2
	 * @return
	 */
	public static float distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371; //kilometers
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                   Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                   Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist*1000;/**metter*/
   }

	/**
	 * Xu ly getQuantityPOManual
	 * @author vuongmq
	 * @param packageQuantity
	 * @param convfact
	 * @param retailQuantity
	 * @return Integer
	 * @since Dec 8, 2015
	 */
	public static Integer getQuantityPOManual(Integer packageQuantity, Integer convfact, Integer retailQuantity) {
		if (packageQuantity != null && convfact != null) {
			packageQuantity = packageQuantity * convfact;
			if (retailQuantity != null) {
				packageQuantity = packageQuantity + retailQuantity;
			}
		}
		return packageQuantity;
	}
	
	/**
	 * Xu ly getAmountPOManual
	 * @author vuongmq
	 * @param packagePrice
	 * @param packageQuantity
	 * @param retailPrice
	 * @param quantity
	 * @return BigDecimal
	 * @since Dec 8, 2015
	 */
	public static BigDecimal getAmountPOManual(BigDecimal packagePrice, Integer packageQuantity, BigDecimal retailPrice, Integer quantity) {
		if (packagePrice != null && packageQuantity != null) {
			packagePrice = packagePrice.multiply(new BigDecimal(packageQuantity));
			if (retailPrice != null && quantity != null) {
				retailPrice = retailPrice.multiply(new BigDecimal(quantity));
				packagePrice = packagePrice.add(retailPrice);
			}
		} else {
			packagePrice = BigDecimal.ZERO;
		}
		return packagePrice;
	}
}

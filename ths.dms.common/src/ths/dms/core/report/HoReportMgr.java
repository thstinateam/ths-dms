/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.report;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.filter.ReportFilter;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.GS_1_4_VO;
import ths.dms.core.entities.vo.ProgressProgramVO;
import ths.dms.core.entities.vo.RawDataIDPVO;
import ths.dms.core.entities.vo.RawDataStockTransVO;
import ths.dms.core.entities.vo.RawDataVO;
import ths.dms.core.entities.vo.ReceivableSummaryVO;
import ths.dms.core.entities.vo.RptSLDSVO;
import ths.dms.core.entities.vo.TimeKeepingVO;
import ths.core.entities.vo.rpt.RptBCBAOMAT1_5VO;
import ths.core.entities.vo.rpt.RptBCTDTMTHVO;
import ths.core.entities.vo.rpt.RptDebitRetrieveVO;
import ths.core.entities.vo.rpt.RptDebitReturnVO;
import ths.core.entities.vo.rpt.RptEqImExVO;
import ths.core.entities.vo.rpt.RptKK1_1VO;
import ths.core.entities.vo.rpt.Rpt_3116_WVKD_03F5SC1_1VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_10_BCTHTDTM_F12_VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_3_BCDSTL11_VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_4_SC1_1VODetail;
import ths.core.entities.vo.rpt.Rpt_3_1_1_8_BCTDTMSC_F10_VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_9_DSKHMT_F11_VO;
import ths.core.entities.vo.rpt.Rpt_SLDS1_5;
import ths.dms.core.entities.vo.rpt.ho.RptCTKMVO;
import ths.dms.core.entities.vo.rpt.ho.RptDHChoKHVO;
import ths.dms.core.entities.vo.rpt.ho.RptKGTKHTTVO;
import ths.dms.core.entities.vo.rpt.ho.RptNVBHActive;
import ths.dms.core.entities.vo.rpt.ho.RptTGGTKHVO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_1_DSPSTN_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_2_TKDHGTN_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_3_GHTNVGH_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_4_G_BCDHCR_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_2_1_CTHHMV;
import ths.dms.core.entities.vo.rpt.ho.Rpt_2_2_SSSLDHMH;
import ths.dms.core.entities.vo.rpt.ho.Rpt_2_3_CTMH;
import ths.dms.core.entities.vo.rpt.ho.Rpt_5_1_TKKMVO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_ASO_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_GROWTH_SKUs_VO;
import ths.dms.core.exceptions.BusinessException;

/**
 * Bao cao HO
 * 
 * @author hunglm16
 * @since Mar 20, 2014
 */
public interface HoReportMgr {

	public final Integer ALL_INT_G = -2;//Dinh nghia cho gia tri tat ca chuc nang, bao cao
	public final int ONE_INT_G = 1;//Dinh nghia cho gia tri tat ca bao cao, hoat dong voi chuc nang
	public final int ZEZO_INT_G = 0;//Dinh nghia cho gia tri tam ngung voi chuc nang, nguoc lai voi ONE_INT_G trong bao cao
	
	/**
	 * Bao cao Xuat nhap ton thiet bi
	 * 
	 * @author thangnv31
	 * @since 2016-03-16
	 * @return
	 */
	List<RptEqImExVO> exportEQImEx(Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * Bao cao Cong no phai thu chi tiet
	 * 
	 * @author thangnv31
	 * @since Dec 22/2015
	 * @return
	 */
	List<RptDebitRetrieveVO> reportCnptCt32(Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;
	
	/**
	 * Bao cao Cong no phai thu chi tiet
	 * 
	 * @author thangnv31
	 * @since Dec 22/2015
	 * @return
	 */
	public List<RptDebitReturnVO> reportCnptrCt34(Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;
	
	/**
	 * Bao cao Cong no phai tra Tong hop
	 * 
	 * @author thangnv31
	 * @since Dec 22/2015
	 * @return
	 */
	List<RptDebitReturnVO> reportCnptCt33(Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;
	
	/**
	 * Bao cao san luong doanh so 2.1
	 * 
	 * @author hoanv25
	 * @since July 10/2015
	 * @return
	 */
	List<GS_1_4_VO> exportBCSLDS21(Long staffIdRoot, Long userId, String strListShopId, Date fDate, Date tDate, String categoryId, String subCategoryId, String strListProductId) throws BusinessException;
	
	/**
	 * Bao cao san luong doanh so theo nvbh 2.2
	 * 
	 * @author hoanv25
	 * @since July 20/2015
	 * @return
	 */
	/**
	 * Bao cao san luong doanh so theo nvbh 2.2
	 * @author trietptm
	 * @param userId
	 * @param roleId
	 * @param chooseShopId
	 * @param fDate
	 * @param tDate
	 * @param categoryId
	 * @param subCategoryId
	 * @param strListProductId
	 * @param cycleId
	 * @return
	 * @throws BusinessException
	 * @since Oct 09, 2015
	 */
	List<GS_1_4_VO> exportBCSLDS22(long userId, long roleId, long chooseShopId, Date fDate, Date tDate, String categoryId, String subCategoryId, String strListProductId, Long cycleId) throws BusinessException;

	/**
	 * Bao cao san luong doanh so theo khac hang 2.3
	 * 
	 * @author hoanv25
	 * @since July 15/2015
	 * @return
	 */
	List<GS_1_4_VO> exportBCSLDS23(Long staffIdRoot, Long userId, String strListShopId, Date fDate, Date tDate, String categoryId, String subCategoryId, String strListProductId, Long cycleId) throws BusinessException;
	
	/**
	 * Bao cao san luong doanh so cua KH
	 * @author vuongmq
	 * @param staffIdRoot
	 * @param roleId
	 * @param strListShopId
	 * @param fDate
	 * @param tDate
	 * @param categoryId
	 * @param subCategoryId
	 * @param strListProductId
	 * @param cycleId
	 * @return List<RptSLDSVO>
	 * @throws BusinessException
	 * @since 26/10/2015
	 */
	List<RptSLDSVO> exportBCSLDSKH(Long staffIdRoot, Long roleId, String strListShopId, Date fDate, Date tDate, String categoryId, String subCategoryId, String strListProductId, Long cycleId) throws BusinessException;

	/**
	 * Bao cao doanh so chuong trinh cua KH
	 * @author vuongmq
	 * @param staffIdRoot
	 * @param roleId
	 * @param strListShopId
	 * @param ksId
	 * @param cycleId
	 * @param cycleIdTo
	 * @return List<RptSLDSVO>
	 * @since 28/10/2015
	 */
	List<RptSLDSVO> exportBCDSCTKH(Long staffIdRoot, Long roleId, String strListShopId, Long ksId, Long cycleId, Long cycleIdTo) throws BusinessException;
	
	/**
	 * Bao cao aso khong hoat dong
	 * @author vuongmq
	 * @param staffIdRoot
	 * @param roleId
	 * @param strListShopId
	 * @param cycleId
	 * @return List<RptSLDSVO>
	 * @throws BusinessException
	 * @since 03/11/2015
	 */
	List<RptSLDSVO> exportBC_ASO_INACTIVE(Long staffIdRoot, Long roleId, String strListShopId, Long cycleId) throws BusinessException;
	
	/**
	 * Bao cao 2.6 Danh sach don hang
	 * 
	 * @author hoanv25
	 * @since July 22/2015
	 * @return
	 */
	List<GS_1_4_VO> exportBCSLDS26(Long staffIdRoot, Long userId, String strListShopId, String strListNvbhId, String orderNumber, String orderType, Integer orderSource, Integer approved, Integer approvedStep, Integer saleOrderType, Long cycleId,
			Date fDate, Date tDate) throws BusinessException;

	/**
	 * Bao cao 2.6 Danh sach don hang
	 * 
	 * @author hoanv25
	 * @param userId
	 * @param roleId
	 * @param strListShopId
	 * @param cycleId
	 * @since July 24/2015
	 * @return
	 */
	List<GS_1_4_VO> exportBCSLDS24(Long userId, Long roleId, String strListShopId, Long cycleId) throws BusinessException;

	/**
	 * Bao cao 2.6 Danh sach don hang
	 * 
	 * @author hoanv25
	 * @param staffIdRoot
	 * @param userId
	 * @param strListShopId
	 * @param strListKeyShopId
	 * @param cycleId
	 * @param fDate
	 * @param tDate
	 * @since July 24/2015
	 * @return
	 */
	List<GS_1_4_VO> exportBCKEYSHOP41(Long staffIdRoot, Long userId, String strListShopId, String strListKeyShopId, Long cycleId, Date fDate, Date tDate) throws BusinessException;

	/**
	 * Bao cao xuat nhap ton 3.1
	 * 
	 * @author hoanv25
	 * 
	 * @param staffIdRoot
	 * @param userId
	 * @param strListShopId
	 * @param orderStock
	 * @param orderProduct
	 * @param cycleId
	 * @param fDate
	 * @param tDate
	 * @since July 24/2015
	 * @return
	 */
	List<GS_1_4_VO> exportBCXNT31(Long staffIdRoot, Long userId, String strListShopId, Date fDate, Date tDate, String orderStock, String orderProduct, Long cycleId) throws BusinessException;

	/**
	 * Bao cao xuat nhap ton chi tiet 3.2
	 * @author vuongmq
	 * @param userId
	 * @param roleId
	 * @param strListShopId
	 * @param cycleId
	 * @param fDate
	 * @param tDate
	 * @return List<GS_1_4_VO>
	 * @throws BusinessException
	 * @since 18/09/2015
	 */
	List<GS_1_4_VO> exportBCXNT32(Long userId, Long roleId, String strListShopId, Long cycleId, Date fDate, Date tDate) throws BusinessException;
	
	/**
	 * Bao cao ton kho chi tiet theo nhan vien vansale
	 * @author dungnt19
	 * @param userId
	 * @param roleId
	 * @param strListShopId
	 * @param cycleId
	 * @param fDate
	 * @param tDate
	 * @return List<GS_1_4_VO>
	 * @throws BusinessException
	 * @since 22/01/2015
	 */
	List<GS_1_4_VO> exportBCTKTNVVS42(Long userId, Long roleId, String strListShopId, Date fDate, Date tDate) throws BusinessException;

	/**
	 * Bao cao so tinh trang don hang 3.5
	 * 
	 * @author hoanv25
	 * @version
	 * @param staffIdRoot
	 * @param userId
	 * @param strListShopId
	 * @param orderNumber
	 * @param orderStatus
	 * @param cycleId
	 * @param fDate
	 * @param tDate
	 * @since August 05/2015
	 * @return
	 */
	List<GS_1_4_VO> exportBCTTNH35(Long staffIdRoot, Long userId, String strListShopId, Date fDate, Date tDate, String orderNumber, String orderStatus, Long cycleId) throws BusinessException;

	/**
	 * Bao cao kiem ke 3.4
	 * 
	 * @author hoanv25
	 * @version
	 * @param staffIdRoot
	 * @param userId
	 * @param strListShopId
	 * @param orderStock
	 * @param orderProduct
	 * @param orderStatus
	 * @param cycleId
	 * @param fDate
	 * @param tDate
	 * @since August 06/2015
	 * @return
	 */
	List<GS_1_4_VO> exportBCKK34(Long staffIdRoot, Long userId, String strListShopId, Date fDate, Date tDate, String orderStock, String orderProduct, String orderStatus, Long cycleId) throws BusinessException;
	
	/**
	 * @author tungmt
	 * @version 1.0
	 * @param filter
	 * @return Bao cao
	 * @exception BusinessException
	 * @see
	 * @since 6/8/2015
	 * @serial
	 */
	List<RawDataVO> exportBCSLDS27(ReportFilter filter) throws BusinessException;

	/**
	 * Bao cao KM 5.1 Tong ket khuyen mai
	 * 
	 * @param shopId
	 * @param promationProgramId
	 * @param fromDate
	 * @param toDate
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return
	 * @throws BusinessException
	 * @since 18/09/2015
	 */
	List<Rpt_5_1_TKKMVO> reportKM5D1TKKM(String shopId, String promationProgramId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;
	
	/**
	 * Bao cao NVBH hoat dong
	 * @author vuongmq
	 * @param shopId
	 * @param fromDate
	 * @return List<RptNVBHActive>
	 * @throws BusinessException
	 * @since 06/10/2015
	 */
	List<RptNVBHActive> exportBCNVBHActive(String shopId, Date fromDate) throws BusinessException;
	
	/**
	 * Bao cao Don hang cua KH
	 * @author vuongmq
	 * @param shopId
	 * @param fromDate
	 * @return List<RptDHChoKHVO>
	 * @throws BusinessException
	 * @since 06/10/2015
	 */
	List<RptDHChoKHVO> exportBCDHChoKH(String shopId, Date fromDate) throws BusinessException;
	
	/**
	 * Bao cao 6.2.2 ASO
	 * @author hunglm16
	 * @param shopId
	 * @param cycleId
	 * @param flagALL
	 * @param subCatId
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return
	 * @throws BusinessException
	 * @since October 10, 2015
	 */
	Rpt_HO_ASO_VO reportASOFullData(Long shopId, Long cycleId, int flagALL, String subCatId, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;

	/**
	 * Bao cao Key Shop vuongmq
	 * 
	 * @modify hunglm16
	 * @since 18/10/2015
	 * @description Ra soat lai Packead
	 */
	List<Rpt_SLDS1_5> getListSLDS1_5(String lstShopId, Date fromDate, Date toDate) throws BusinessException;

	/**
	 * [1.1] Tong hop don hang phat sinh trong ngay
	 * 
	 * @author hunglm16
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return danh sach don hang phat sinh trong ngay
	 * @throws BusinessException
	 * @since 18/10/2015
	 */
	List<Rpt_1_1_DSPSTN_VO> report1D1DSPSTN(String shopId, Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;
	
	/**
	 * [1.2] Thong ke don hang giao trong ngay
	 * 
	 * @author hunglm16
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return danh sach don hang giao trong ngay
	 * @throws BusinessException
	 * @since 20/10/2015
	 */
	List<Rpt_1_2_TKDHGTN_VO> report1D2TKDHGTN(String shopId, Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;

	/**
	 * Bao cao raw data
	 * @author trietptm
	 * @param userId
	 * @param roleId
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return list du lieu don hang
	 * @throws BusinessException
	 * @since Nov 05, 2015
	 */
	List<RawDataIDPVO> exportRawData(Long userId, Long roleId, Long shopId, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * Bao cao raw data cho don dieu chinh
	 * @author trietptm
	 * @param userId
	 * @param roleId
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return list du lieu don stockTrans
	 * @throws BusinessException
	 * @since Nov 16, 2015
	 */
	List<RawDataStockTransVO> exportRawDataStockTrans(Long userId, Long roleId, Long shopId, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * Bao cao cham cong
	 * @author trietptm
	 * @param userId
	 * @param roleId
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param numDay
	 * @return list TimeKeepingVO
	 * @throws BusinessException
	 * @since Oct 28, 2015
	 */
	List<TimeKeepingVO> exportTimeKeeping(Long userId, Long roleId, Long shopId, Date fromDate, Date toDate, int numDay) throws BusinessException;

	/**
	 * Bc 1.3 Giao hang theo NVGH
	 * @author hunglm16
	 * @param shopId
	 * @param cycleId
	 * @param fromDate
	 * @param toDate
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return
	 * @throws BusinessException
	 * @sine 30/10/2015
	 */
	List<Rpt_1_3_GHTNVGH_VO> report1D3GHTNVGH(String shopId, Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;
	
	/**
	 * Bc 1.4 Bao cao don hang rot
	 * @author hunglm16
	 * @param shopId
	 * @param cycleId
	 * @param fromDate
	 * @param toDate
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return Danh sach du lieu va cac gia tri tong {don hang, chiet khau, thanh toan}
	 * @throws BusinessException
	 * @sine 30/10/2015
	 */
	Rpt_1_4_G_BCDHCR_VO report1D4BCDHR(String shopId, Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;

	/**
	 * Bao cao chi tiet khuyen mai
	 * @param staffIdRoot
	 * @param roleId
	 * @param shopRootId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	List<RptCTKMVO> exportBCCTKM(Long staffIdRoot, Long roleId, Long shopRootId, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * Bao cao thoi gian ghe tham KH
	 * @author vuongmq
	 * @param staffIdRoot
	 * @param roleId
	 * @param strListShopId
	 * @param fromDate
	 * @param toDate
	 * @return List<RptTGGTKHVO>
	 * @throws BusinessException
	 * @since 05/11/2015
	 */
	List<RptTGGTKHVO> exportTGGTKH(Long staffIdRoot, Long roleId, String strListShopId, Date fromDate, Date toDate) throws BusinessException;

	/**
	 * Bao cao khong ghe tham KH trong tuyen
	 * @author vuongmq
	 * @param shopId
	 * @param year
	 * @param roleId
	 * @param tDate
	 * @return List<RptKGTKHTTVO>
	 * @throws BusinessException
	 * @since 06/11/2015
	 */
	List<RptKGTKHTTVO> exportKGTKHTT(Long staffIdRoot, Long roleId, String strListShopId, Date fDate, Date tDate) throws BusinessException;
	
	/**
	 * Bao cao tang truong SKUs
	 * @author hunglm16
	 * @param shopId
	 * @param year
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return 
	 * @throws BusinessException
	 */
	Rpt_HO_GROWTH_SKUs_VO reportGrowthSKUs(Long shopId, Integer year, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;

	/**
	 * Lay header dong cua bao cao 2.4
	 * @author hunglm16
	 * @param cycleId
	 * @return
	 * @throws BusinessException
	 * @since 14/11/2015
	 */
	List<CycleVO> getHeaderBCSLDS24(String cycleId) throws BusinessException;
	
	/**
	 * Lay du lieu bao cao cong no phai thu tong hop 
	 * @author trietptm
	 * @param userId
	 * @param roleId
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return danh sach du lieu cong no phai thu tong hop
	 * @throws BusinessException
	 * @since Dec 29, 2015
	*/
	List<ReceivableSummaryVO> exportReceivableSummary(Long userId, Long roleId, Long shopId, Date fromDate, Date toDate) throws BusinessException;

//	/**
//	 * Xu ly exportPayableDetail
//	 * @author trietptm
//	 * @param userId
//	 * @param roleId
//	 * @param shopId
//	 * @param fromDate
//	 * @param toDate
//	 * @return
//	 * @throws BusinessException
//	 * @since Dec 31, 2015
//	*/
//	List<PayableDetailVO> exportPayableDetail(Long userId, Long roleId, Long shopId, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * Lay du lieu bao cao tien do thuc hien chuong trinh 5.5
	 * @author trietptm
	 * @param staffIdRoot
	 * @param roleId
	 * @param strListShopId
	 * @param ksId
	 * @param cycleId
	 * @param cycleIdTo
	 * @return danh sach du lieu bao cao tien do thuc hien chuong trinh
	 * @throws BusinessException
	 * @since Nov 28, 2015
	*/
	List<ProgressProgramVO> exportProgressProgram(Long staffIdRoot, Long roleId, String strListShopId, Long ksId, Long cycleId, Long cycleIdTo) throws BusinessException;

	/**
	 * Xu ly export2D3CTMH
	 * @author vuongmq
	 * @param staffIdRoot
	 * @param roleId
	 * @param strListShopId
	 * @param fDate
	 * @param tDate
	 * @return List<Rpt_2_3_CTMH>
	 * @since Dec 29, 2015
	*/
	List<Rpt_2_3_CTMH> export2D3CTMH(Long staffIdRoot, Long roleId, String strListShopId, Date fDate, Date tDate) throws BusinessException;
	
	/**
	 * Xu ly export2D1CTHHMV
	 * @author dungnt19
	 * @param staffIdRoot
	 * @param roleId
	 * @param strListShopId
	 * @param fDate
	 * @param tDate
	 * @return List<Rpt_2_3_CTMH>
	 * @since Dec 29, 2015
	*/
	List<Rpt_2_1_CTHHMV> export2D1CTHHMV(Long staffIdRoot, Long roleId, String strListShopId, Date fDate, Date tDate) throws BusinessException;
	
	/**
	 * Xu ly export2D2SSSLDHMH
	 * @author dungnt19
	 * @param staffIdRoot
	 * @param roleId
	 * @param strListShopId
	 * @param fDate
	 * @param tDate
	 * @return List<Rpt_2_3_CTMH>
	 * @since Dec 29, 2015
	*/
	List<Rpt_2_2_SSSLDHMH> export2D2SSSLDHMH(Long staffIdRoot, Long roleId, String strListShopId, Date fDate, Date tDate) throws BusinessException;

	/**
	 * Xu ly getRptWVKD03F11_DSKHMTB
	 * @author vuongmq
	 * @param lstShop
	 * @param reportDate
	 * @return List<Rpt_3_1_1_9_DSKHMT_F11_VO>
	 * @throws BusinessException
	 * @since Mar 3, 2016
	*/
	List<Rpt_3_1_1_9_DSKHMT_F11_VO> getRptWVKD03F11_DSKHMTB(Long staffRootId, Long roleId, Long shopRootId, String lstShop, Integer typeInfo, Date fromDate, Date toDate) throws BusinessException;

	/**
	 * Xu ly getExportKKTB11
	 * @author vuongmq
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @param lstShopId
	 * @param statisticRecord
	 * @param period
	 * @param fromDate
	 * @param toDate
	 * @return List<RptKK1_1VO>
	 * @throws BusinessException
	 * @since Mar 29, 2016
	*/
	List<RptKK1_1VO> getExportKKTB11(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, Long statisticRecord, String period, Date fromDate, Date toDate) throws BusinessException;
	/**
	 * @author vuongmq
	 * @param shopId
	 * @param lstShopId
	 * @param categoryIdMultil
	 * @return list RptBCBAOMAT1_5VO
	 * @since 18/05/2015
	 * @throws IOException
	 */
	List<RptBCBAOMAT1_5VO> getRptBCBAOMAT1_5(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, String lstEquipId, String categoryIdMultil, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * Lay data bao cao TB 3.1.1.6: Thông tin mượn thiết bị
	 * @param lstShopId
	 * @param lstEquipId
	 * @param categoryIdMultil
	 * @param periodId
	 * @since May 13,2015
	 * @throws BusinessException
	  */
	List<Rpt_3116_WVKD_03F5SC1_1VO> getRptBCDSMTDTMTK(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, String lstEquipId, String categoryIdMultil, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * Lay data bao cao TB 1.1.1.7
	 * @author hoanv25
	 * 
	 * @param lstShopId
	 * @param lstEquipId
	 * @param categoryIdMultil
	 * @param shopId
	 * @param lstIslevel
	 * @return list RptBCTDTMTHVO
	 * @since May 05,2015
	 * @throws BusinessException
	 */
	List<RptBCTDTMTHVO> getRptBCDSTDTMTH(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, String lstEquipId, String categoryIdMultil, Date fromDate, Date toDate) throws BusinessException;
	
	 /**
	 * Xu ly exportBCPSCCTH
	 * @author vuongmq
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @param lstShopId
	 * @param groupMultilId
	 * @param categoryIdMultil
	 * @param fromDate
	 * @param toDate
	 * @return List<Rpt_3_1_1_4_SC1_1VODetail>
	 * @throws BusinessException
	 * @since Apr 4, 2016
	*/
	List<Rpt_3_1_1_4_SC1_1VODetail> exportBCPSCCTH(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, String groupMultilId, String categoryIdMultil, Date fromDate, Date toDate) throws BusinessException;

	/**
	 * Bao cao [TL1.1] Danh sach thiet bi thanh ly
	 * @author trietptm
	 * @param shopId
	 * @param lstEquipId
	 * @param categoryIdMultil
	 * @param fromDate
	 * @param toDate
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return List<Rpt_3_1_1_3_BCDSTL11_VO>
	 * @throws BusinessException
	 * @since Apr 6, 2016
	*/
	List<Rpt_3_1_1_3_BCDSTL11_VO> getRptBCDSTL11(Long shopId, String lstEquipId, String categoryIdMultil, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;

	/**
	 * Xu ly getRptBCTDTMSC
	 * @author vuongmq
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @param lstShopId
	 * @param groupMultilId
	 * @param categoryIdMultil
	 * @param fromDate
	 * @param toDate
	 * @return List<Rpt_3_1_1_8_BCTDTMSC_F10_VO> 
	 * @throws BusinessException
	 * @since Apr 7, 2016
	*/
	List<Rpt_3_1_1_8_BCTDTMSC_F10_VO> getRptBCTDTMSC(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, String groupMultilId, String categoryIdMultil, Date fromDate, Date toDate) throws BusinessException;

	/**
	 * Lay du lieu bao cao F12 Xuat nhap ton thiet bi tai NPP
	 * @author trietptm
	 * @param shopId
	 * @param lstEquipId
	 * @param categoryIdMultil
	 * @param fromDate
	 * @param toDate
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return
	 * @throws BusinessException
	 * @since Apr 12, 2016
	 */
	List<Rpt_3_1_1_10_BCTHTDTM_F12_VO> getRptF12(Long shopId, String lstEquipId, String categoryIdMultil, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId)throws BusinessException ;

}
package ths.dms.core.report;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.InvoiceCustPayment;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.GS_1_2_VO;
import ths.dms.core.entities.vo.GS_1_4_VO;
import ths.dms.core.entities.vo.GS_4_2_VO;
import ths.dms.core.entities.vo.GS_KHTT;
import ths.dms.core.entities.vo.Rpt7_2_2_NVBHVO;
import ths.dms.core.entities.vo.RptCustomerVO;
import ths.dms.core.entities.vo.RptDSKHTMHCustomerAndListInfoVO;
import ths.dms.core.entities.vo.RptDTBHTNTNVBHStaffInfoVO;
import ths.dms.core.entities.vo.RptDTBHTNTNVBHStaffInfo_7_2_2VO;
import ths.dms.core.entities.vo.RptPDHVNM_5_1VO;
import ths.dms.core.entities.vo.RptTDBHTMHProductAndListInfoVO;
import ths.dms.core.entities.vo.TimeVisitCustomerVO;
import ths.core.entities.vo.rpt.PROC_3_11_BCCTCTHTB;
import ths.core.entities.vo.rpt.PT;
import ths.core.entities.vo.rpt.Rpt10_1_18_DHHH;
import ths.core.entities.vo.rpt.Rpt10_1_2;
import ths.core.entities.vo.rpt.Rpt3_11_CTCTHTB;
import ths.core.entities.vo.rpt.Rpt3_13_CTKMTCT;
import ths.core.entities.vo.rpt.Rpt3_14_CTKMTCTNV;
import ths.core.entities.vo.rpt.Rpt3_16_BTHDH;
import ths.core.entities.vo.rpt.Rpt5_5_DCPOCTDVKH;
import ths.core.entities.vo.rpt.Rpt7_2_12_PTDTBHNPPVO;
import ths.core.entities.vo.rpt.Rpt7_2_13_DetailVO;
import ths.core.entities.vo.rpt.Rpt7_2_14VO;
import ths.core.entities.vo.rpt.Rpt7_2_5_CTTT;
import ths.core.entities.vo.rpt.Rpt7_2_5_PT_Detail;
import ths.core.entities.vo.rpt.Rpt7_5_1CT;
import ths.core.entities.vo.rpt.Rpt7_5_1TH;
import ths.core.entities.vo.rpt.Rpt7_7_6VO;
import ths.core.entities.vo.rpt.RptAbsentVO;
import ths.core.entities.vo.rpt.RptActionLogVO;
import ths.core.entities.vo.rpt.RptBCCTKMVO;
import ths.core.entities.vo.rpt.RptBCDKTT_9_1VO;
import ths.core.entities.vo.rpt.RptBCDS1VO;
import ths.core.entities.vo.rpt.RptBCDS2VO;
import ths.core.entities.vo.rpt.RptBCDSDHTNTNVBH_DS3_1_VO;
import ths.core.entities.vo.rpt.RptBCNVKGTKHTTRecordVO;
import ths.core.entities.vo.rpt.RptBCNVO;
import ths.core.entities.vo.rpt.RptBCNgayVO;
import ths.core.entities.vo.rpt.RptBCPOAUTONPP_5_10;
import ths.core.entities.vo.rpt.RptBCTCNVO;
import ths.core.entities.vo.rpt.RptBCTDDSTKHNHMHLv1VO;
import ths.core.entities.vo.rpt.RptBCTGBHHQCNVBH_DS3_VO;
import ths.core.entities.vo.rpt.RptBCTGGTCNVBH_1VO;
import ths.core.entities.vo.rpt.RptBCTGTMVO;
import ths.core.entities.vo.rpt.RptBCVT1;
import ths.core.entities.vo.rpt.RptBCVT10;
import ths.core.entities.vo.rpt.RptBCVT11;
import ths.core.entities.vo.rpt.RptBCVT12;
import ths.core.entities.vo.rpt.RptBCVT5VO;
import ths.core.entities.vo.rpt.RptBCVT6;
import ths.core.entities.vo.rpt.RptBCVT7;
import ths.core.entities.vo.rpt.RptBCVT7_1;
import ths.core.entities.vo.rpt.RptBCVT9;
import ths.core.entities.vo.rpt.RptBCXNKNVBHVO2;
import ths.core.entities.vo.rpt.RptBKCTCTMHProductRecordVO;
import ths.core.entities.vo.rpt.RptBKCTCTMHPurchaseOrderVO;
import ths.core.entities.vo.rpt.RptBKCTHHMVVO;
import ths.core.entities.vo.rpt.RptBookProductVinamilkVO;
import ths.core.entities.vo.rpt.RptCTKMTCK739VO;
import ths.core.entities.vo.rpt.RptCountStoreVO;
import ths.core.entities.vo.rpt.RptCustomerByRoutingLv01VO;
import ths.core.entities.vo.rpt.RptCustomerNotVisitOfSalesVO;
import ths.core.entities.vo.rpt.RptCustomerStockVO;
import ths.core.entities.vo.rpt.RptCycleCountDiff3VO;
import ths.core.entities.vo.rpt.RptCycleCountDiffVO;
import ths.core.entities.vo.rpt.RptDDHF2VO;
import ths.core.entities.vo.rpt.RptDMVSVO;
import ths.core.entities.vo.rpt.RptDSTMCTTBVO;
import ths.core.entities.vo.rpt.RptDSTTCTDLVO;
import ths.core.entities.vo.rpt.RptDTTHNVStaffAndListInfoVO;
import ths.core.entities.vo.rpt.RptDeliveryAndPaymentOfStaffVO;
import ths.core.entities.vo.rpt.RptExImStockOfStaffLv01VO;
import ths.core.entities.vo.rpt.RptExSaleOrder2VO;
import ths.core.entities.vo.rpt.RptF1Lv01VO;
import ths.core.entities.vo.rpt.RptImExStDataVO;
import ths.core.entities.vo.rpt.RptInvDetailStatVO;
import ths.core.entities.vo.rpt.RptPDHCGVO;
import ths.core.entities.vo.rpt.RptPDTHVO;
import ths.core.entities.vo.rpt.RptPGNVTTGVO;
import ths.core.entities.vo.rpt.RptPTHCKHVO;
import ths.core.entities.vo.rpt.RptPTHVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_Stock;
import ths.core.entities.vo.rpt.RptParentDebitPaymentVO;
import ths.core.entities.vo.rpt.RptParentReturnSaleOrderAllowDeliveryVO;
import ths.core.entities.vo.rpt.RptParentSaleOrderOfDeliveryVO;
import ths.core.entities.vo.rpt.RptParentViewInvoiceVO;
import ths.core.entities.vo.rpt.RptPayDisplayProgrameVO;
import ths.core.entities.vo.rpt.RptPoCfrmFromDvkhVO;
import ths.core.entities.vo.rpt.RptPoStatusTracking2VO;
import ths.core.entities.vo.rpt.RptProductPoVnmDetailVO;
import ths.core.entities.vo.rpt.RptReturnSaleOrderByDeliveryLv01VO;
import ths.core.entities.vo.rpt.RptSaleOrderInDate2VO;
import ths.core.entities.vo.rpt.RptSaleOrderLotVO;
import ths.core.entities.vo.rpt.RptStockTransDetailVO;
import ths.core.entities.vo.rpt.RptStockTransInMoveVO;
import ths.core.entities.vo.rpt.RptTDCTDSVO;
import ths.core.entities.vo.rpt.RptTDTTCNData;
import ths.core.entities.vo.rpt.RptTDTTCN_NVTT;
import ths.core.entities.vo.rpt.RptTotalAmountParentVO;
import ths.core.entities.vo.rpt.RptVT5;
import ths.core.entities.vo.rpt.RptXBTNV;
import ths.core.entities.vo.rpt.Rpt_7_2_15;
import ths.core.entities.vo.rpt.Rpt_7_2_16;
import ths.core.entities.vo.rpt.Rpt_7_2_17;
import ths.core.entities.vo.rpt.Rpt_PO_CTTTVO;
import ths.core.entities.vo.rpt.Rpt_SHOP_7_7_8VO;
import ths.core.entities.vo.rpt.Rpt_TDXNTCT_NganhVO;
import ths.core.entities.vo.rpt.Rpt_TGLVCNVBH_VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface ShopReportMgr {
	/**
	 * Báo cáo kho - Bao cao xuat nhap ton chi tiet
	 * @author DuyNQ
	 */
	List<RptImExStDataVO> getXNTCTReport(Long shopId, Date fromDate,
			Date toDate) throws BusinessException;
	
	/**
	 * Lay du lieu cho bao cao xuat nhap ton F1
	 * @author ThuatTQ
	 */
	List<RptF1Lv01VO> getDataForF1Report(Long shopId, Date fromDate, Date toDate, Integer numSaleDayInMonth, Integer numSaleDay)
			throws BusinessException;
	
	/**
	 * 3.1.2.4 Bao cao ket qua ton kho tai diem le
	 * @author hieunq1 -> thuattq
	 */
	RptCustomerStockVO getRptCustomerStockVO(Long shopId, Date date)
			throws BusinessException;
	
	/**
	 * Bo
	 * 3.1.3.3 Phieu giao nhan va thanh toan
	 * @author hungnm -> thuattq
	 */
	List<RptSaleOrderLotVO> getListSaleOrderByDeliveryStaff(Long shopId,
			Long deliveryStaffId, Date fromDate, Date toDate,
			Integer isFreeItem, Boolean getPromotion, Long saleOrderId)
			throws BusinessException;
	
	/**
	 * Báo cáo kiểm kê kho - Báo cáo chênh lệch giữa tồn kho và thực tế
	 * @author DuyNQ
	 */
	RptCycleCountDiffVO getCLTKTTReport(Long shopId, Date fromDate,
			Date toDate, Long cycleCountCode) throws BusinessException;

	
	/**
	 * Báo cáo mua hàng : Quản lý PO Confirm từ DVKH
	 * @author duynq
	 */
	RptPoCfrmFromDvkhVO getPOConfirmDVKHReport(Long shopId,
			Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * Báo cáo mua hàng : Phiếu đặt hàng Vinamilk
	 * @author duynq
	 */
	List<RptBookProductVinamilkVO> getListProductBookFromVNM(Long shopId, Date fromDate,
			Date toDate) throws BusinessException;
	
	/**
	 * Báo cáo doanh thu bán hàng: Doanh số tổng hợp NVBH theo ngày
	 * @author duynguyen
	 * @throws BusinessException
	 */
	List<RptTotalAmountParentVO> getSummaryStaffTurnoverReport(Long shopId,
			Date fromDate, Date toDate, String staffCode)
			throws BusinessException;
	
	/**
	 * Báo cáo theo dõi hằng ngày - Phiếu trả hàng
	 * @author DuyNQ
	 */
	List<RptPTHVO> getPTHReport (Long shopId,
			Date fromDate, Date toDate, Long customerId) throws BusinessException;
	
	/**
	 * 3.1.2.1 lay danh sach bao cao dem kho thuc te
	 * @author thanhnguyen
	 * @param cycleCountCode
	 * @param shopCode
	 * @return
	 * @throws BusinessException
	 */
	List<RptCountStoreVO> getListReportCountStore(String cycleCountCode, String shopCode) throws BusinessException;
	
	/**
	 * 3.1.2.3 lay danh sach bao cao tinh trang xuat nhap dieu chinh
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	List<RptStockTransDetailVO> getListReportStockTransDetail(Long shopId, Date fromDate, Date toDate) throws BusinessException;
	//sontt:Tong hop chi tra hang trung bay
	List<RptPayDisplayProgrameVO> getListRptPayDisplayProgrameVO(
			Long shopId, String lstProgramDisplayCode, Date fDate, Date tDate)
			throws BusinessException;
	
	List<RptDSKHTMHCustomerAndListInfoVO> getDSKHTMHReport(Long shopId, String lstCustomerCode, String lstCategoryCode, String lstProductCode, Date fromDate, Date toDate)throws BusinessException;
	
	List<RptDSKHTMHCustomerAndListInfoVO> getDSKHTMHReport2(Long shopId, Long customerId,Date fromDate, Date toDate)throws BusinessException ;
	//end
	
	/**
	 * Bao cao doanh thu tong hop cua nhan vien
	 * @author tungtt
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param listStaffId
	 */
	List<RptDTTHNVStaffAndListInfoVO> getDTTHNVReport(Long shopId, String listStaffId,Date fromDate, Date toDate)throws BusinessException;
	
	
	/**
	 * doanh so theo muc CTTB
	 * @author vuongmq
	 * @param shopId
	 * @param year
	 * @param lstDisplayProgram
	 * @param lstCustomer
	 */
	List<RptDSTMCTTBVO> getDSTMCTTBReport (Long shopId, Integer year, String lstDisplayProgram, String lstCustomer) throws BusinessException;
	
	
	/**
	 * Bao cao theo doi chi tieu doanh so
	 * @author tungtt
	 */
	List<RptTDCTDSVO> getBCTDCTDSReport(Long shopId, String staffCode,Integer month, Integer year)throws BusinessException;
	
	//
		
	/**
	 * 3.1.3.8 phieu tra hang theo nvgh
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param deliveryId
	 * @return
	 * @throws BusinessException
	 */
//	List<RptParentReturnSaleOrderAllowDeliveryVO> getListRptReturnSaleOrderAllowDelivery(
//			Long shopId, Date fromDate, Date toDate, Long deliveryId)
//			throws BusinessException;
	
	/**
	 * 3.1.4.3 Bang ke mua hang theo mat hang
	 * @author hieunq1 -> thanhnn
	 * @return
	 * @throws BusinessException
	 */
	RptProductPoVnmDetailVO getRptProductPoVnmDetailVO(Long shopId)
			throws BusinessException;
	

	/**
	 * 3.1.3.6 Lay bang ke phieu tra hang theo nhan vien giao hang
	 * @author thanhnguyen -> thuattq
	 * @param shopId
	 * @param orderType
	 * @param fromDate
	 * @param toDate
	 * @param deliveryId
	 * @return
	 * @throws BusinessException
	 */
	List<RptReturnSaleOrderByDeliveryLv01VO> getListRptReturnSaleOrderByDelivery (Long shopId, OrderType orderType,
			Date fromDate, Date toDate, Long deliveryId) throws BusinessException;

	
	/**
	 * 3.1.4.2	Bảng theo dõi trạng thái đơn hàng
	 * @author HUNGNM
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	List<RptPoStatusTracking2VO> getRptPoStatusTrackingVO(Date fromDate,
			Date toDate, Long shopId) throws BusinessException;

	/**
	 * 3.1.5.8	Bảng kê chi tiết chứng từ mua hàng
	 * @author NamLB
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	List<RptBKCTCTMHPurchaseOrderVO> getRptBKCTCTMHPurchaseOrderVO(Long shopId, Date fromDate,
			Date toDate) throws BusinessException;
	
	/**
	 * 	Báo cáo không ghé thăm khách hàng trong tuyến
	 * @author NamLB
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @param listStaffId
	 * @param listOwnerStaff
	 * @return
	 * @throws BusinessException
	 */
	//List<RptBCNVKGTKHTTRecordVO> getListBCNVKGTKHTT(String shopId, String staffCode, String staffOwnerCode, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * 3.1.5.1	Báo cáo danh sách đơn hàng trong ngày theo NVBH
	 * @author HUNGNM
	 * @param fromDate
	 * @param toDate
	 * @param staffId
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	List<RptSaleOrderInDate2VO> getListRptSaleOrderInDate2VO(Date fromDate,
			Date toDate, List<Long> lstStaffId, Long shopId)
			throws BusinessException;
	
	/**
	 * Báo cáo theo dõi hằng ngày - Phiếu đổi trả hàng
	 * @author DuyNQ
	 */
	List<RptPDTHVO> getPDTHReport (Long shopId,
			Date fromDate, Date toDate, Long staffId, String deliveryCode) throws BusinessException;
	

	/**
	 * Báo cáo theo dõi hằng ngày - Phiếu đặt hàng chưa giao
	 * @author VuongHN
	 */
	List<RptPDHCGVO> getPDHCGReport(Long shopID,
			Date fromDate, Date toDate, String deliveryCode) throws BusinessException;
	
	/**
	 * 
	*  3.7.1.1 Xem thong tin hoa don GTGT
	*  @author: thanhnn
	*  @param kPaging
	*  @param vat
	*  @param customerCode
	*  @param staffId
	*  @param fromDate
	*  @param toDate
	*  @param custPayment
	*  @param selName
	*  @return
	*  @throws BusinessException
	*  @return: List<RptViewInvoiceVO>
	*  @throws:
	 */
	List<RptParentViewInvoiceVO> getRptViewInvoice(Float vat,
			String customerCode, List<Long> listStaffId, Date fromDate,
			Date toDate, InvoiceCustPayment custPayment, String selName,
			String orderNumber, Long shopId, String invoiceNumber)
			throws BusinessException;
	
	/**
	 * 
	*  3.1.2.3 phieu xuat kho kiem van chuyen noi bo
	*  @author: thanhnn
	*  @param stockTransId
	*  @return
	*  @throws BusinessException
	*  @return: List<RptStockTransInMoveVO>
	*  @throws:
	 */
	List<RptStockTransInMoveVO> getListRptStrockTransInMove(Long stockTransId)
			throws BusinessException;
	
	List<StockTrans> getListStockTransWithCondition(
			KPaging<StockTrans> kPaging, StockObjectType fromOwnerType,
			StockObjectType toOwnerType, Long staffId, Date stockTransDate,
			Long fromOwnerId, Long toOwnerId) throws BusinessException;

	/**
	 * Báo cáo bảng kê chi tiết hóa đơn GTGT
	 * @author duyNQ
	 */
//	RptInvDetailStatVO getBKCTHDGTGTReport(Long shopId, String staffCode,
//			String shortCode, Integer vat, Date fromDate, Date toDate, InvoiceStatus status)
//			throws BusinessException;
	
	/**
	 * Báo cáo bảng kê chi tiết hóa đơn GTGT
	 * @author tungtt21
	 */
	List<RptInvDetailStatVO> getBKCTHDGTGT(Long shopId, String staffCode,Integer tax, Date fromDate, Date toDate, Integer status) throws BusinessException;
	/**
	 * Bao cao xuat ban ton theo nhan vien
	 * 
	 * @author thuattq
	 */
	List<RptExImStockOfStaffLv01VO> getRptExImStockOfStaff(Long shopId,
			List<Long> listStaffId, Date fromDate, Date toDate)
			throws BusinessException;
	
	/**
	 * Bao cao tong hop phieu giao nhan va thanh toan
	 * 
	 * @author thuattq
	 */
	List<RptDeliveryAndPaymentOfStaffVO> getListDeliveryAndPaymentOfStaff(
			Long shopId, Date fromDate, Date toDate, List<Long> deliveryStaffId)
			throws BusinessException;
	
	/**
	 * 
	*  3.1.4.2 Bao cao thanh toan cua khach hang
	*  @author: thanhnn
	*  @param fromDate
	*  @param toDate
	*  @param staffId
	*  @param customerId
	*  @param type 0: NVBH, 1: KH
	*  @return
	*  @throws BusinessException
	*  @return: List<RptParentDebitPaymentVO>
	*  @throws:
	 */
	List<RptParentDebitPaymentVO> getListDebitPayment(Date fromDate, Date toDate, Long shopId, 
			List<Long> staffIds, List<Long> customerIds, Integer type) throws BusinessException;
	
	
	/**
	 * 3.1.4.3 Phiếu thu
	 * @param fromDate
	*  @param toDate
	*  @param shopId
	*  @param listCustomerId
	 * @author tungtt
	 */
	List<PT> getListPTDetail(Long shopId, String listCustomerId, Date fromDate, Date toDate) throws BusinessException;
	
	
	/**
	 * 
	*  3.1.10.1 Bao cao thoi gian ghe tham cua NVBH
	*  @author: thanhnn
	*  @param kPaging
	*  @param shopId
	*  @param staffCode
	*  @param visitPlanDate
	*  @param isHasChild
	*  @return
	*  @throws BusinessException
	*  @return: List<ActionLogVO>
	*  @throws:
	 */
	List<RptActionLogVO> getRptVisitPlan(Long shopId,
			String staffCode, String superCode, Date visitPlanDate, Boolean isHasChild) throws BusinessException;
	
	
	/**
	 * bao cao toi gian ghe tham NVBH VT1 
	 *@author vuongmq
	 **/
	
	List<RptBCVT1> getRptBCVT1(String shopId, String listGsnppCode, String listStaffCode, Date visitPlanDate) throws BusinessException;
	/**
	 * Báo cáo khách hàng - Danh sách KH
	 * @author duyNQ
	 */
	List<RptCustomerVO> getDSKHReport(Long shopId, String staffCode,
			String customerCode, String customerName, boolean isGroupByStaff,Integer status,
			Long parentStaffId, boolean checkMap) throws BusinessException;


	/**
	 * Phieu (Hoa don) giao nhan va thanh toan gop
	 * @author hungnm
	 * @param shopId
	 * @param deliveryStaffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	List<RptPGNVTTGVO> getListRptPGNVTTGVOs(Long shopId, Long deliveryStaffId,
			Date fromDate, Date toDate) throws BusinessException;

	
	
	/**
	 * Bao cao giam sat khach hang
	 * 
	 * Bao cao thoi gian ghe tham cua nhan vien ban hang _ 1
	 * 
	 * Thoi gian truyen vao co gio, phut
	 * 
	 * @author thuattq
	 */
	public List<RptBCTGGTCNVBH_1VO> getListRptBCTGGT_NVBH_1(String listShopId,
			String listNVGSId, String listNVBHId, Date fromDate,
			Date toDate) throws BusinessException;

	/**
	 * Bao cao giam sat khach hang
	 * 
	 * Bao cao khong ghe tham khach hang trong tuyen
	 * 
	 * @author thuattq
	 */
	List<RptCustomerNotVisitOfSalesVO> getListRptKHKGTTT_NVBH(
			List<Long> listShopId, List<Long> listNVGSId,
			List<Long> listNVBHId, Date fromDate, Date toDate)
			throws BusinessException;
	
	/**
	 * 
	*  bao cao thoi gian ghe tham khach hang
	*  @author: thanhnn
	*  @param fromDate
	*  @param toDate
	*  @param shopId
	*  @param superId
	*  @param staffId
	*  @param getShopOnly
	*  @return
	*  @throws BusinessException
	*  @return: List<TimeVisitCustomerVO>
	*  @throws:
	 */
	List<TimeVisitCustomerVO> getListVisitCustomer2(Date fromDate, Date toDate,
			Long shopId, Long superId, Long staffId, Boolean getShopOnly) throws BusinessException;
	
	/**
	 * Bao cao giam sat khach hang
	 * 
	 * BÁO CÁO NV KHÔNG BẬT MÁY
	 * 
	 * @author duynq
	 */
	List<RptAbsentVO> getAbsentReport2(Long shopId, Long staffId,Date fromDate,
			Date toDate) throws BusinessException;
	
	RptBCVT5VO getAbsentReport(String strListShopId, String strListStaffCode,Date fromDate,
			Date toDate) throws BusinessException;
	
	/**
	 * Bao cao giam sat khach hang
	 * 
	 * Báo cáo đi muộn về sớm của NVBH
	 * 
	 * @author duynq
	 */
	/*List<RptDMVSVO> getDMVSReport(String strListShopId, List<Long> staffOwnerId, List<Long> staffId,Date fromDate,
			Date toDate, Integer fromHour, Integer fromMinute,Integer toHour,Integer toMinute) throws BusinessException;
	*/
	/**
	 * Bao cao doanh thu ban hang
	 * 
	 * Báo cáo ngay
	 * 
	 * @author duynq
	 */
	List<RptBCNgayVO> getListBCNgayReport(Long shopId) throws BusinessException;

	/**
	 * Bao cao thoi gian tat may
	 * @author hungnm
	 * @param shopId
	 * @param gsnppId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @param minutes
	 * @return
	 * @throws BusinessException
	 */
	List<RptBCTGTMVO> getListRptBCTGTMVO(Long shopId, Long gsnppId,
			Long staffId, Date fromDate, Date toDate, Integer minutes)
			throws BusinessException;
	
	/*List<RptBCTGTMVO> getListRptBCTGTMVO2(String shopId, String lstGsnppCode, String lstStaffCode, Date fromDate, Date toDate, Integer minutes)
			throws BusinessException;
*/
	//SangTN
    /*List<RptBCDS2VO> getListRptBCDS2(String lstShopId, Date fromDate, Date toDate)
    		throws BusinessException;*/
	/**
	 * Bao cao tong cong no
	 * @param shopId
	 * @param lstObjectId
	 * @param objectType
	 * @return
	 * @throws BusinessException
	 */
	List<RptBCTCNVO> getListRptBCTCNVO(Long shopId, List<Long> lstObjectId,
			StockObjectType objectType) throws BusinessException;

	List<RptParentReturnSaleOrderAllowDeliveryVO> getListRptReturnSaleOrderAllowDeliveryGroupByProductCode(
			Long shopId, Date fromDate, Date toDate, Long deliveryId)
			throws BusinessException;

	/**
	 * Bao cao xuat nhap kho nhan vien ban hang
	 * @author hungnm
	 * @param shopId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	//SangTN	
//	List<RptBCXNKNVBHVO> getListRptBCXNKNVBHVOs(Long shopId,
//			String listStaffId, Date fromDate, Date toDate) throws BusinessException;
	List<RptBCXNKNVBHVO2> getListRptBCXNKNVBHVOs(Long shopId,
			String listStaffId, Date fromDate, Date toDate, Integer typeBill) throws BusinessException;
	/**
	 * Bao cao xuất bán tồn theo nhân viên
	 * @author tungtt
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param listStaffId
	 */
	List<RptXBTNV> getXBTNVReport(Long shopId, String listStaffId, Date fromDate, Date toDate)throws BusinessException;
	
	/**
	 * 
	 * @param shopId : ID cua nha phan phoi
	 * @param staffCode : ma code nhan vien ban hang
	 * @param stockTransCode : ma code loai giao dich xuat
	 * @param nameManeuver : ten lenh dieu dong
	 * @param content : noi dung dieu dong
	 * @return danh sach cac san pham trong don dat hang
	 * @throws DataAccessException
	 */
	public RptPXKKVCNB_Stock getPXKKVCNB(Long shopId, String staffCode,
			String stockTransCode, String nameManeuver, String content)
					throws DataAccessException;
	/**
	 * 
	 * @param shopId
	 * @param typePerson : customer or nhan vien ban hang
	 * @param codePerson : staff_code or customer_code
	 * @param fromDate
	 * @param toDate
	 * @author cangnd
	 * @return
	 * @throws DataAccessException
	 */
	List<Rpt_PO_CTTTVO> getPOCTTT(Long shopId, int typePerson, String codePerson, String fromDate, String toDate)
	 throws DataAccessException;
	
	/**
	 * @author cangnd
	 */
	public List<Rpt_TDXNTCT_NganhVO> LayDanhSachXuatNhapKhoChiTiet(Long shopId, Date fromDate, Date toDate)throws DataAccessException;
	
	/**
	 * @author vuonghn
	 * @param shopId
	 * @param lstStaffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptBCCTKMVO> getListRptCTKM(Long shopId, String lstStaffId, Date fromDate, Date toDate) throws DataAccessException;
	
	/**Lay bang ke chung tu hang hoa mua vao
	 * @author vuonghn
	 * @param shopId
	 * @param fDate
	 * @param tDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptBKCTHHMVVO> getRptBKCTHHMV(Long shopId, Date fDate, Date tDate)throws DataAccessException;
	
	/**Bao cao PO auto NPP
	 * @author namlb
	 * @param shopId
	 * @param fDate
	 * @param tDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptBCPOAUTONPP_5_10> getRptBCPOAutoNPP(Long shopId, Date fDate, Date tDate) throws DataAccessException;
	
	/**Phieu dat hang vinamilk
	 * @author phuongvm
	 * @param shopId
	 * @param fDate
	 * @param tDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptPDHVNM_5_1VO> getRptBCPDHVNM(Long shopId, Date fDate, Date tDate) throws DataAccessException;

	/**
	 * Bao cao khach hang theo tuyen ban hang
	 * @author sangtn
	 * @param shopId
	 * @param lstStaffId
	 * @param lstCustomerId
	 * @param customerName
	 * @return
	 * @throws BusinessException
	 */
	List<RptCustomerByRoutingLv01VO> getRptCustomerByRouting(Long shopId, String lstStaffCode, String lstCustomerCode, String customerName,Long staffId,Integer flagCMS)
			throws BusinessException;

	/*List<TimeVisitCustomerVO> getListVisitCustomer(String strListShopId,
			String lstStaffOwnerCode, String lstStaffSaleCode, Date fromDate,
			Date toDate, Boolean isGetShopOnly) throws BusinessException;*/

	
	/*List<RptBCVT9> exportBCVT9(String strListShopId, String strListStaffId, Date fromDate, Date toDate, String imageType, String displayProgramId,Long staffId)
					throws BusinessException;*/
	/*List<RptBCDS1VO> getListReportDS1(String strListShopId, Date fromDate,
			Date toDate) throws BusinessException;*/

	List<RptBCTDDSTKHNHMHLv1VO> getBCTDDSTKHNHMHReport(Long shopId,
			String customerCode, Date fDate, Date tDate) throws DataAccessException;
/*
	List<RptBCTGBHHQCNVBH_DS3_VO> getRptBCDS3(String strListShopId, String listGSNPPCode, String listStaffCode, Date rptDate, Long fromHour, Long fromMinute,
			Long toHour, Long toMinute, Long sprTime) throws BusinessException;*/
	
	//@author hunglm16; @since January 21, 2014; @description report Ds3: Bao cao chi tiet chi tra hang TB
	List<PROC_3_11_BCCTCTHTB> getRptBCDS3_11(Long shopId, Integer year, String lstDisProCode, String customerCode) throws BusinessException;
	
	//@author hunglm16; @since January 25, 2014; @description report Ds3: Bao cao danh sach don hang trong ngay theo NVBH
	List<RptBCDSDHTNTNVBH_DS3_1_VO> rptDSDHTNTNVBH3_1(Long shopId, String lstStaff, Date fDate, Date tDate) throws BusinessException;

	List<RptBCNVO> getBCNReport(Long shopId, Long parentStaffId, Date fDate, Date tDate, Date nextMonth, Date lastMonth, boolean checkMap) throws BusinessException;
	
	/**
	 * VT7 - Bao cao ghe tham khach hang
	 * @author vuonghn
	 */
	/*List<RptBCVT7> getBCGheThamKHVT7(String lstShopId, String lstGSNPPId, String fromDate,
			String toDate) throws BusinessException;*/
	
	/**
	 * VT10 - Bao cao ket qua cham trung bay
	 * @author vuonghn
	 */
	List<RptBCVT10> exportBCVT10(String lstShopId, String lstGSMTId,
			String fromDate, String toDate,Long shopId) throws BusinessException;
	
	/**
	 * VT11 - Bao cao nhan vien mo clip
	 * @param lstShopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	/*List<RptBCVT11> exportBCVT11(String lstShopId, String fromDate,
			String toDate,Long staffId) throws BusinessException;*/
	
	/**
	 * VT12 - Bao cao xoa khach hang tham gia CTTB
	 * @author vuonghn
	 */
	List<RptBCVT12> exportBCVT12(String lstShopId, String displayProgrameId,
			String strDate,Long staffId) throws BusinessException;

	List<RptExSaleOrder2VO> getRptBCPXHTNVGH(Long shopId, Long deliveryStaffId, Date fDate, Date tDate) throws BusinessException;

	List<RptBKCTCTMHProductRecordVO> rptMuaHang_BKCTCTMH(Long shopId, Date fromDate, Date toDate, Integer type) throws BusinessException;
	
	/**
	 * 5.5 - Doi chieu PO Confirm tu DVKH
	 * @author vuonghn
	 */
	List<Rpt5_5_DCPOCTDVKH> exportDCPOCTDVKH_5_5(Long shopId, Date fDate,
			Date tDate) throws BusinessException;
	
	/**
	 * Bang tong hop tra hang
	 * @author tungtt21
	 * @param shopId
	 * @param catCode
	 * @param fDate
	 * @param tDate
	 * @return
	 * @throws BusinessException
	 */
	List<Rpt3_16_BTHDH> getBTHDHReport(Long shopId, String catCode, Date fDate, Date tDate) throws BusinessException;
	
	/**
	 * Bao cao chi tiet khuyen mai theo chuong trinh
	 * @author tungtt21
	 * @param shopId
	 * @param staffCode
	 * @param fDate
	 * @param tDate
	 * @return
	 * @throws BusinessException
	 */
	//List<Rpt3_13_CTKMTCT> getBCCTKMTCT(Long shopId, String staffCode, Date fDate, Date tDate) throws BusinessException;
	
	/**
	 * Bao cao chi tiet khuyen mai theo chuong trinh - nhan vien
	 * @author tungtt21
	 * @param shopId
	 * @param staffCode
	 * @param fDate
	 * @param tDate
	 * @return
	 * @throws BusinessException
	 */
	List<Rpt3_14_CTKMTCTNV> getBCCTKMTCTNV(Long shopId, String staffCode, Date fDate, Date tDate) throws BusinessException;
	
	
	/**
	 * Gets the rpt bcdktt.
	 *
	 * @param shopId the shop id
	 * @param maKiemKe the ma kiem ke
	 * @return the rpt bcdktt
	 * @throws BusinessException the business exception
	 */
	List<RptBCDKTT_9_1VO> getRptBCDKTT(Long shopId, String maKiemKe)
			throws BusinessException;
	
	/**
	 * Bao cao chi tiet chi tra hang trung bay
	 * @author tungtt21
	 * @param shopId
	 * @param year
	 * @param lstDisProCode
	 * @param customerCode
	 * @return
	 * @throws BusinessException
	 */
	List<Rpt3_11_CTCTHTB> getBCCTCTHTB(Long shopId,String lstDisProCode, Date fDate, Date tDate) throws BusinessException;

	/*List<RptBCVT6> getBCChamCong(String shopId, String date, String gsnppId,
			String nvbhId) throws BusinessException;*/

	List<RptDSTTCTDLVO> Rpt7_2_8_DSTTCTDL(Long shopId, String lstStaff, Date fDate, Date tDate, Long staffRootId,Integer hasCheckStaff) throws BusinessException;
	
	List<RptDDHF2VO> rpt7_3_9_DDHF2(Long shopId, Date fDate, Date tDate) throws BusinessException;
	
	List<RptCTKMTCK739VO> rpt7_2_7_CTKMTCK(Long shopId, Date fDate, Date tDate, Long staffRootId,Integer hasCheckStaff) throws BusinessException;
	
	/**
	 * Lay bao cao 10.1.18 - doi tra hang hu hong
	 * 
	 * @author lacnv1
	 * @since Mar 11, 2014
	 */
	List<Rpt10_1_18_DHHH> export10_1_18_DHHH(Long shopId, String programCodes, String categoryCodes, Date fDate, Date tDate) throws BusinessException;

	
	/**
	 * Lay bao cao no phai thu - tong hop 7.5.1
	 * 
	 * @author lacnv1
	 * @since Mar 18, 2014
	 */
	List<Rpt7_5_1TH> getNoPhaiThuTH(Long shopId) throws BusinessException;
	
	/**
	 * Lay bao cao no phai thu - chi tiet 7.5.1
	 * 
	 * @author lacnv1
	 * @since Mar 18, 2014
	 */
	List<Rpt7_5_1CT> getNoPhaiThuCT(Long shopId) throws BusinessException;

	/*List<RptBCVT7_1> getBCVT7_1(String lstShopId, String lstGSNPPId,
			String fromDate, String toDate) throws BusinessException;*/
	List<Rpt7_2_5_CTTT> getBCCTTT_KH(Long shopId, String lstNVBHCode, String lstNVGHCode, String lstCustomerCode, Date fDate, Date tDate) throws BusinessException;

	/**
	 * Lay du lieu bao cao 7.2.5 Phieu thu
	 */
	/*List<Rpt7_2_5_PT_Detail> getRpt7_2_5PT(Long shopId, String lstSaler, String lstCustomer, String lstDelivery,
			Date fDate, Date tDate) throws BusinessException;*/
	
	/**
	 * Lay bao cao 7.5.2 - NVTT
	 * 
	 * @author lacnv1
	 * @since Apr 08, 2014
	 *//*
	List<RptTDTTCN_NVTT> getRpt7_2_5NVTT(Long shopId, String lstNVBH, String lstKH,
			String lstNVGH, Date fDate, Date tDate) throws BusinessException;*/
	
	/**
	 * Lay bao cao 7.5.2 - KH
	 * 
	 * @author lacnv1
	 * @since Apr 10, 2014
	 */
	/*List<RptTDTTCN_NVTT> getRpt7_2_5KH(Long shopId, String lstNVBH, String lstKH,
			String lstNVGH, Date fDate, Date tDate) throws BusinessException;*/
	/**
	 * Lay bao cao TDTT cong no Du lieu tho
	 * @param shopId
	 * @param lstNVBH
	 * @param lstKH
	 * @param lstNVGH
	 * @param fDate
	 * @param tDate
	 * @return
	 * @throws BusinessException
	 */
	/*List<RptTDTTCNData> getRpt7_2_5TDTTCN(Long shopId, String lstNVBH, String lstKH,
			String lstNVGH, Date fDate, Date tDate) throws BusinessException;*/
/*
	List<RptVT5> getProcVT5(String strListShopId, String strListStaffId,
			Date fromDate, Date toDate) throws BusinessException;*/

	List<Rpt10_1_2> getListRpt10_1_2(Long shopId, Long parentStaffId, String lstSaleStaffId,
			Date fromDate, Date toDate, boolean checkMap) throws BusinessException;
	
	/**
	 * Bao cao 7.2.12
	 * @author tungtt21
	 * @param strListShopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	List<Rpt7_2_12_PTDTBHNPPVO> getListReport7_2_12(String strListShopId, Date fromDate,
			Date toDate) throws BusinessException;
	/**
	 * @author vuongmq
	 * @param strListShopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @desciption: bao cao phan tich doanh thu ban hang SKU 7_2_15
	 * @throws BusinessException
	 */
	List<Rpt_7_2_15> getListPTDTBHSKU7_2_15(String strListShopId, Long parentStaffId, Date fromDate,
			Date toDate, boolean checkMap) throws BusinessException;
	
	/**
	 * @author vuongmq
	 * @param strListShopId
	 * @param fromDate
	 * @param toDate
	 * @since 14-August 2014
	 * @return
	 * @desciption: bao cao don hang tra theo gia tri
	 * @throws BusinessException
	 */
	List<Rpt_7_2_16> getListBCDHT_THEOGIATRI_7_2_16(String strListShopId, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * @author vuongmq
	 * @param strListShopId
	 * @param fromDate
	 * @param toDate
	 * @since 14-August 2014
	 * @return
	 * @desciption: bao cao don hang tra theo tong so don hang
	 * @throws BusinessException
	 */
	List<Rpt_7_2_17> getListBCDHT_THEOTONGDONHANG_7_2_17(String strListShopId, Date fromDate, Date toDate) throws BusinessException;
	
	
	/**
	 * Bao cao 7.2.14
	 * @author vuonghn
	 */
	List<Rpt7_2_14VO> getListReport7_2_14(String strListShopId, Long parentStaffId, Date fromDate, Date toDate, boolean checkMap) throws BusinessException;
	/**
	 * @author sangtn
	 * @since 13/05/2014
	 * @description Xuat bao cao 7.2.13. Phan tich doanh thu ban hang theo NVBH 
	 * */
	List<Rpt7_2_13_DetailVO> getListReport7_2_13(String strListShopId, Long parentStaffId,
			Date fromDate, Date toDate, boolean checkMap) throws BusinessException;
	/**
	 * Bao cao 7.7.6
	 * @author vuonghn
	 */
	List<Rpt7_7_6VO> getListReport7_7_6(String strListShopId, Date fromDate, Date toDate) throws BusinessException;

	List<Rpt7_2_2_NVBHVO> getRpt7_2_2SanPham(Long shopId, String listStaffId,
			Date fromDate, Date toDate,Long staffRootId,Integer hasCheckStaff) throws BusinessException;

	
	
	List<RptDTBHTNTNVBHStaffInfo_7_2_2VO> getDTBHTNTNVBH_7_2_2Report(
			Long shopId, String listStaffId, Date fromDate, Date toDate,Long staffRootId,Integer hasCheckStaff)
			throws BusinessException;

	List<Rpt_SHOP_7_7_8VO> rptShop_7_7_8VO(Long shopId, Date fromDate, Date toDate) throws BusinessException;

	/**
	 * Bao cao 7.7.1 : BC_PXHTNVGH
	 * @author hunglm16
	 * @since Ortober 2,2014
	 * @description Bao cao NPP
	 * */
	List<RptExSaleOrder2VO> getListSaleOrderByDeliveryStaffGroupByProductCode(
			Long shopId, String staffSaleCode, Integer isPrint,
			Long deliveryStaffId, String listSaleOrdernumber, Date fromDate,
			Date toDate, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	List<RptExSaleOrder2VO> getListSaleOrderByDeliveryStaffGroupByProductCodeNotApproved(
			Long shopId, String staffSaleCode, Long deliveryStaffId,
			String listSaleOrdernumber, Date fromDate, Date toDate, Long staffRootId, Integer flagCMS) throws BusinessException;
	/**
	 * Bao cao 7.7.1.A : RPT1_2_BKDHTNVGH
	 * @author hunglm16
	 * @since Ortober 2,2014
	  * @description Bao cao NPP
	 * */
	List<RptParentSaleOrderOfDeliveryVO> getListRptSaleOrderOfDelivery(
			Long shopId, String lstSaleStaffId, Integer isPrint, Date fromDate,
			Date toDate, String lstDeliveryId, String lstSaleOrderNumber,
			Long staffIdRoot, Integer flagCMS) throws BusinessException;

	List<RptParentSaleOrderOfDeliveryVO> getListRptSaleOrderOfDeliveryNotApproved(
			Long shopId, String lstSaleStaffId, Date fromDate, Date toDate,
			String lstDeliveryId, String lstSaleOrderNumber, Long staffRootId, Integer flagCMS) throws BusinessException;

	/**
	 * Lay bao cao 7_1_3 - Phieu giao nhan va thanh toan
	 * 
	 * @author hunglm16
	 * @since Ortober 2,2014
	 * @description Bao cao NPP
	 */
	List<RptDeliveryAndPaymentOfStaffVO> getListDeliveryAndPaymentOfStaffUseProcedure(
			Long shopId, Date fromDate, Date toDate,
			List<Long> deliveryStaffId, Integer isPrint, Long staffIdRoot, Integer flagCMS)
			throws BusinessException;

	List<RptDeliveryAndPaymentOfStaffVO> getListDeliveryAndPaymentOfStaffUseProcedureNotApproved(
			Long shopId, Date fromDate, Date toDate,
			List<Long> deliveryStaffId, Long staffRootId, Integer flagCMS)
			throws BusinessException;


	/**
	 * VT1 Bao cao thoi gian ghe tham cua NVBH
	 * @author tulv2
	 * @since 03.10.201
	 * */
	//List<RptBCVT1> getRptBCVT12(String strListShopId, String staffSaleCode, Date visitPlanDateTmp, Long staffIdRoot)throws BusinessException;
	List<RptBCVT1> getRptBCVT12(String strListShopId, String staffOwnerCode, String staffSaleCode, Date visitPlanDateTmp, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	/**
	 * VT1.1 Bao cao thoi gian ghe tham cua nhan vien ban hang. 
	 * @author tulv2
	 * @since 03.10.2014
	 * */
	//List<RptBCTGGTCNVBH_1VO> getListRptBCTGGT_NVBH_12(String strListShopId, String lstStaffSaleCode, Date fDate, Date tDate, Long staffRootId)throws BusinessException;
	List<RptBCTGGTCNVBH_1VO> getListRptBCTGGT_NVBH_12(String strListShopId, String lstNVGSCode, String lstNVBHCode, Date fDate, Date tDate, Long staffRootId, Integer flagCMS) throws BusinessException;

	/**
	 * VT2 Bao cao thoi gian tat may
	 * @author tulv2
	 * @since 04.10.2014
	 * */
	List<RptBCTGTMVO> getListRptBCTGTM(Long staffIdRoot, Long roleId, Long shopRootId, Date fromDate, Date toDate, Integer nMinute)throws BusinessException;

	/**
	 * VT3 Bao cao thoi gian ghe tham nhan vien ban hang
	 * @author tulv2
	 * @since 04.10.2014
	 * */
	List<GS_4_2_VO> getListVisitCustomer(Long userId, Long roleId, String lstShopId, Date fromDate, Date toDate)throws BusinessException;



	List<RptDTBHTNTNVBHStaffInfoVO> getDTBHTNTNVBHReport(Long shopId,
			String listStaffId, Date fromDate, Date toDate, Long staffIdRoot,
			Integer flagCMS) throws BusinessException;
	/**
	 * @author sangtn
	 * @since 23/05/2014
	 * @description Phiếu xuất hàng theo nhân viên bán hàng 7.1.7 
	 * 
	 * @author hunglm16
	 * @since October 2,2014
	 * @description Update theo Phan quyen CMS
	 * */
	List<RptExSaleOrder2VO> getListSaleOrderBySaleStaffGroupByProductCode(
			Long shopId, String staffSaleCode, Long deliveryStaffId,
			String listSaleOrdernumber, Date fromDate, Date toDate,
			Long staffRootId, Integer flagCMS) throws BusinessException;
	/**
	 * 7.1.8 Phieu tra hang cua Khach Hang 
	 * 
	 * @author hunglm16
	 * @since October 3,2014
	 * @description Update theo Phan quyen CMS
	 * */
	List<RptPTHCKHVO> getPTHCKH(Long shopId, String lstStaffCode, Date fromDate, Date toDate, String customerCode, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	/**
	 * VT1.2 Bao cao di muon ve som
	 * @author tulv2
	 * @since 04.10.2014
	 * */
	List<RptDMVSVO> getDMVSReport(String strListShopId,  List<Long> listOwnerId, List<Long> listStaffId, Date fDate, Date tDate, Integer fromHour, Integer fromMinute, Integer toHour, Integer toMinute, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	/**
	 * VT4 Bao cao nhan vien khong ghe tham khach hang
	 * @author tulv2
	 * @since 06.10.2014
	 * */
	//List<RptBCNVKGTKHTTRecordVO> getListBCNVKGTKHTT(String strListShopId, String staffCode, Date fDate, Date tDate, Long staffIdRoot) throws BusinessException;
	List<RptBCNVKGTKHTTRecordVO> getListBCNVKGTKHTT(String strListShopId, String staffCode, String staffOwnerCode, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	/**
	 * VT6: Bao cao cham cong
	 * @author tulv2
	 * @since 07.10.2014
	 * */
	List<RptBCVT6> getBCChamCong(String strListShopId, String fromDate, String lstStaffOwnerId, String lstStaffSaleId, Long staffIdRoot,Long flagCMS) throws BusinessException;

	/**
	 * VT7: Bao cao ghe tham khach hang
	 * @author tulv2
	 * @since 07.10.2014
	 * */
	List<RptBCVT7> getBCGheThamKHVT7(String strListShopId, String lstStaffOwnerId, String fromDate, String toDate, Long staffIdRoot, Integer flagCMS)  throws BusinessException;
	
	/**
	 * VT7.1: Bao cao chi tiet ghe tham khach hang
	 * @author tulv2
	 * @since 07.10.2014
	 * */
	List<RptBCVT7_1> getBCVT7_1(String lstShopId, String lstGSNPPId, String fromDate, String toDate, Long staffIdRoot, Integer flagCMS)  throws BusinessException;

	/**
	 * DS1: Bao cao doanh so SKUs chi tiet theo nhan vien ban hang
	 * @author tulv2
	 * @since 08.10.2014
	 * */
	List<RptBCDS1VO> getListReportDS1(String strListShopId, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	/**
	 * DS2: Bao cao doanh so SKUs chi tiet theo khach hang
	 * @author tulv2
	 * @since 08.10.2014
	 * */
	List<RptBCDS2VO> getListRptBCDS2(String lstShopId, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	/**
	 * DS3: Bao cao thoi gian ban hang hieu qua cua nhan vien ban hang
	 * @author tulv2
	 * @since 08.10.2014
	 * */
	List<RptBCTGBHHQCNVBH_DS3_VO> getRptBCDS3(String strListShopId, String staffOwnerCode, String staffSaleCode, Date fDate,Long fHour, Long fMinute, Long tHour, Long tMinute, Long sprTimes,Long staffIdRoot, Integer flagCMS)throws BusinessException;

	/**
	 * Bao cao nhan vien khong bat may
	 * @author tulv2
	 * @since 08.10.2014
	 * */
	List<RptVT5> getProcVT5(String strListShopId, String strListStaffId,Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	/**
	 * Bao cao cong no theo nhan vien thu tien
	 * @author tulv2
	 * @since 09.10.2014
	 * */
	List<RptTDTTCN_NVTT> getRpt7_2_5NVTT(Long id, String lstSalerId, String lstCustId, String lstDeliveryId, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	/**
	 * Bao cao cong no theo khach hang
	 * @author tulv2
	 * @since 09.10.2014
	 * */
	List<RptTDTTCN_NVTT> getRpt7_2_5KH(Long id, String lstSalerId, String lstCustId, String lstDeliveryId, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	/**
	 * Bao cao cong no theo phieu thu
	 * @author tulv2
	 * @since 09.10.2014
	 * */
	List<Rpt7_2_5_PT_Detail> getRpt7_2_5PT(Long id, String lstSalerId, String lstCustomerId, String lstDeliveryId, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	/**
	 * Bao cao cong no du lieu tho
	 * @author tulv2
	 * @since 09.10.2014
	 * */
	List<RptTDTTCNData> getRpt7_2_5TDTTCN(Long id, String lstSalerId, String lstCustId, String lstDeliveryId, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException;

	/**
	 * Theo doi thanh toan cong no
	 * @author tulv2
	 * @since 10.10.2014
	 * */
	List<Rpt3_13_CTKMTCT> getBCCTKMTCT(Long shopId, String staffCode, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException;
	/**
	 * 7.2.1 Theo Doi Ban Hang Theo Mat Hang
	 * 
	 * @author sangtn
	 * 
	 * @author hunglm16
	 * @since October 14,2014
	 * @description Phan Quyen Theo CMS
	 * */
	List<RptTDBHTMHProductAndListInfoVO> getTDBHTMHReport(Long shopId, String listStaffId, Date fromDate, Date toDate, Long staffRootId, Integer flagCMS) throws BusinessException;

	/**
	 * @author tulv2
	 * update 15.10.2014
	 * 
	 * */
	List<RptBCVT9> exportBCVT9(String strListShopId, String strListStaffId,Date fDate, Date tDate, String imageType, String displayProgramId,Long id, Integer flagCMS)throws BusinessException;

	/**
	 * @author tulv2
	 * update 20.10.2014
	 * Bao cao VT11
	 * */
	List<RptBCVT11> exportBCVT11(String strListShopId, String fromDate, String toDate, Integer flagCMS, Long staffId) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 26/6/2015
	 * @param userId
	 * @param roleId
	 * @param lstShopId
	 * @param fromDate
	 * @param toDate
	 * @return bao cao ghe tham cua nvbh
	 * @throws BusinessException
	 */
	List<GS_1_2_VO> getListVisitNVBH(Long userId, Long roleId, String lstShopId, Date fromDate, Date toDate, Integer staffType) throws BusinessException;

	/**
	 * @author hoanv25
	 * @since July 07/2015
	 * @param userId
	 * @param staffIdRoot
	 * @param lstShopId
	 * @param fromDate
	 * @param toDate
	 * @return bao cao ket qua di tuyen
	 * @throws BusinessException
	 */
	List<GS_4_2_VO> getListVisit(Long staffIdRoot, Long userId, String strListShopId, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * @author hoanv25
	 * @since July 08/2015
	 * @param userId
	 * @param staffIdRoot
	 * @param lstShopId
	 * @param fromDate
	 * @param toDate
	 * @param strListGsId, strListNvbhId
	 * @return bao cao mo moi khach hang
	 * @throws BusinessException
	 */
	List<GS_1_4_VO> getNewOpendCustomerByNVBH(Long staffIdRoot, Long userId, String strListShopId, Date fromDate, Date toDate, Integer check, String strListGsId, String strListNvbhId) throws BusinessException;
	
	/**
	 * @author longnh15
	 * @since July 10 2015
	 * @param cycleId
	 * @param strListShopId
	 * @param fromDate
	 * @param toDate
	 * @param product
	 * @param subCat
	 * @param Cat
	 * @return bao cao ke hoach tieu thu
	 * @throws BusinessException
	 */
	List<GS_KHTT> getListKHTT(Long cycleId, Long shopId, Date fromDate, Date toDate, String product, String subCat, String cat, String lstStaff) throws BusinessException;

	/**
	 * @author trietptm
	 * @since July 31 2015
	 * @param cycleCountId
	 * @return Bao cao chenh lech ton kho voi thuc te
	 * @throws BusinessException
	 */
	List<RptCycleCountDiff3VO> getListRptCycleCountDiff3VO(long cycleCountId) throws BusinessException;

	/**
	 * Bao cao thoi gian lam viec cua NVBH
	 * 
	 * @author hunglm16
	 * @param shopId
	 * @param cycleId
	 * @param fromDate
	 * @param toDate
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return
	 * @throws BusinessException
	 * @since 21/10/2015
	 */
	List<Rpt_TGLVCNVBH_VO> reportTGLVCNVBH(Long shopId, Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;

}

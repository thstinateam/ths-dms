package ths.dms.core.report;

import java.util.Date;
import java.util.List;

import ths.core.entities.vo.rpt.RptBCKD10;
import ths.core.entities.vo.rpt.RptBCKD10_2VO;
import ths.core.entities.vo.rpt.RptBCKD10_2_DLCPPTSkusDNVO;
import ths.core.entities.vo.rpt.RptBCKD10_3;
import ths.core.entities.vo.rpt.RptBCKD11;
import ths.core.entities.vo.rpt.RptBCKD11_1;
import ths.core.entities.vo.rpt.RptBCKD11_2VO;
import ths.core.entities.vo.rpt.RptBCKD12VO;
import ths.core.entities.vo.rpt.RptBCKD13VO;
import ths.core.entities.vo.rpt.RptBCKD14VO;
import ths.core.entities.vo.rpt.RptBCKD15VO;
import ths.core.entities.vo.rpt.RptBCKD16VO;
import ths.core.entities.vo.rpt.RptBCKD17VO;
import ths.core.entities.vo.rpt.RptBCKD18VO;
import ths.core.entities.vo.rpt.RptBCKD19VO;
import ths.core.entities.vo.rpt.RptBCKD19_1;
import ths.core.entities.vo.rpt.RptBCKD1_1;
import ths.core.entities.vo.rpt.RptBCKD1_2;
import ths.core.entities.vo.rpt.RptBCKD2;
import ths.core.entities.vo.rpt.RptBCKD21;
import ths.core.entities.vo.rpt.RptBCKD3;
import ths.core.entities.vo.rpt.RptBCKD5;
import ths.core.entities.vo.rpt.RptBCKD6;
import ths.core.entities.vo.rpt.RptBCKD7;
import ths.core.entities.vo.rpt.RptBCKD9VO;
import ths.core.entities.vo.rpt.RptDSBHMienVO;
import ths.core.entities.vo.rpt.RptDSPPNHVO;
import ths.core.entities.vo.rpt.RptDiemNVBHVO;
import ths.core.entities.vo.rpt.RptDoanhSoBanHangNVBHSKUNgayVO;
import ths.core.entities.vo.rpt.RptKD10_1VO;
import ths.core.entities.vo.rpt.RptKD1_VO;
import ths.core.entities.vo.rpt.RptKD3_VO;
import ths.core.entities.vo.rpt.RptKD9;
import ths.core.entities.vo.rpt.RptKD_16_1VO;
import ths.core.entities.vo.rpt.RptKD_8_1VO;
import ths.core.entities.vo.rpt.RptKD_General_VO;
import ths.core.entities.vo.rpt.rpt_HO_KD15VO;
import ths.dms.core.exceptions.BusinessException;
import ths.core.entities.vo.rpt.*;

public interface CrmReportMgr {
	/**
	 * KD1.3 Doanh số - phân phối theo Nhóm hàng
	 * 
	 * @author duynq
	 */
	RptDSPPNHVO getDSPPNHReport(Long shopId,  List<Long> lstSp, Date toDate) throws BusinessException;
	
	
	/**
	 * KD4 Doanh số bán hàng SKUs theo Miền
	 * 
	 * @author duynq
	 */
	RptDSBHMienVO getDSBHMienReport(Date toDate) throws BusinessException;


	/**
	 * KD1 Doanh so ban hang cua nhan vien ban hang theo SKUs tu ngay den ngay
	 * 
	 * @author thuattq
	 */
	RptDoanhSoBanHangNVBHSKUNgayVO getDoanhSoBanHangNVBHSKUNgayVOReport(
			Date toDate, 
			List<Long> tbhvId, Long shopId, List<Long> staffOwnerId,
			List<Long> staffId, List<Long> productId) throws BusinessException;
	
	
	/**
	 * KD8 Báo cáo điểm NVBH theo Tuyến và theo Doanh số từ ngày đến ngày
	 * 
	 * @author duynq
	 */
	RptDiemNVBHVO getDiemNVBHTheoTuyenReport(Long shopId, List<Long> lstStaffId,
			Date fromDate, Date toDate) throws BusinessException;

	/**
	 * KD8.1 Báo cáo NVBH bi tu x lan diem kiem tu ngay den ngay
	 * 
	 * @author duynq
	 */
	RptDiemNVBHVO getNVBHDiemKemReport(Long shopId, List<Long> listStaffId,
			Date fromDate, Date toDate, Integer soLan) throws BusinessException;

	
	/**
	 * 
	*  Bao cao KD3
	*  @author: thanhnn
	*  @param mamien
	*  @param mavung
	*  @param matbhv
	*  @param magsnpp
	*  @param manvbh
	*  @param fromDate
	*  @param toDate
	*  @return
	*  @throws BusinessException
	*  @return: RptBCKD3
	*  @throws:
	 */
	RptBCKD3 getRptBCKD3(String matbhv, String magsnpp, String manvbh,
			String manpp, Date toDate, String cat) throws BusinessException;




	/**
	 * @author hungnm
	 * @param listIdNpp
	 * @param listIdCat
	 * @param listIdSp
	 * @param from_date
	 * @param to_date
	 * @return
	 * @throws BusinessException
	 */
	List<RptBCKD9VO> getRptBCKD9(List<Long> listIdNpp, List<Long> listIdCat,
			List<Long> listIdSp, Date from_date, Date to_date)
			throws BusinessException;
	
	/**
	 * 
	*  bao cao KD10
	*  @author: thanhnn
	*  @param productInfoCode
	*  @param fromDate
	*  @param toDate
	*  @return
	*  @throws BusinessException
	*  @return: RptBCKD10
	*  @throws:
	 */
	RptBCKD10 getRptBCKD10(Long braneId, Date fromDate, Date toDate)
			throws BusinessException;
	
	/**
	 * 
	 * bao cao KD5
	 * 
	 * @author: thanhnn - thuattq
	 */
	RptBCKD5 getRptBCKD5(Long shopId,String lstCategoryId, Date toDate) throws BusinessException;


	/**
	 * Bao cao KD6
	 * @author hungnm
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	RptBCKD6 getRptBCKD6(Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * 
	*  BCKD 11_1
	*  @author: thanhnn (core AnhNH)
	*  @param displayProgramId
	*  @param fromDate
	*  @param toDate
	*  @return
	*  @throws BusinessException
	*  @return: List<RptBCKD11_1VO>
	*  @throws:
	 */
	RptBCKD11_1 getRptBCKD11_1(Long displayProgramId, Date fromDate,
			Date toDate) throws BusinessException;
	
	RptBCKD1_2 getRptBCKD1_2(String listShopId, String productCode, Date fromDate, Date toDate)
			throws BusinessException;
	
	RptBCKD1_2 getRptBCKD1_3(String listShopId, String productCode, Date fromDate, Date toDate)
	throws BusinessException;
	
	RptBCKD1_2 getRptBCKD1_1(String listShopId, String productCode, Date fromDate, Date toDate)
			throws BusinessException;
	
	/**
	 * 
	*  BCKD17
	*  @author: thanhnn (core HieuNQ)
	*  @param fromDate
	*  @param toDate
	*  @return
	*  @throws BusinessException
	*  @return: List<RptBCKD17VO>
	*  @throws:
	 */
	List<RptBCKD17VO> getRptBCKD17(Date fromDate, Date toDate) throws BusinessException;
	
	
	/**
	 * 
	*  BCKD 11_2
	*  @author: duyNQ (core AnhNH)
	*  @param displayProgramId
	*  @param fromDate
	*  @param toDate
	 */
	RptBCKD11_2VO getRptBCKD11_2(Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * Bao cao KD 12
	 * 
	 * @author thuattq
	 * @throws BusinessException
	 */
	RptBCKD12VO getRptBCKD12(Long shopId, List<Long> staffId, Date toDate)
			throws BusinessException;
	
	/**
	 * Bao cao KD 16
	 * 
	 * @author thuattq
	 * @param thangBC
	 *            Ngay dau thang bao cao
	 * @throws BusinessException
	 */
	RptBCKD16VO getRptBCKD16(Date thangBC) throws BusinessException;


	/**
	 * @author hungnm
	 * @param displayProgramId
	 * @param subCatId
	 * @param listShopId
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	RptBCKD10_3 getRptBCKD10_3(Long displayProgramId, Long subCatId,
			List<Long> listShopId, Date toDate) throws BusinessException;
	
	/**
	 * 
	*  Bao cao KD10.1
	*  @author: duynq
	 */
	RptKD10_1VO getRptBCKD10_1(Long productInfoId, Date toDate)
			throws BusinessException;
	
	/**
	 * 
	*  Bao cao KD18
	*  @author: duynq
	 */
	RptBCKD18VO getRptBCKD18(Date fromMonth, Date toMonth)
			throws BusinessException;
	
	/**
	 * 
	*  Bao cao KD7
	*  @author: thanhnn
	*  @param maMien
	*  @param fromDate
	*  @param toDate
	*  @return
	*  @throws BusinessException
	*  @return: RptBCKD7
	*  @throws:
	 */
	RptBCKD7 getRptBCKD7(String maMien, Date fromDate, Date toDate)
			throws BusinessException;
	
	/**
	 * 
	 * Bao cao KD10_2
	 * 
	 * @author thuattq
	 */
	RptBCKD10_2VO getRptBCKD10_2(String productInfoCode, Date fromDate,
			Date toDate) throws BusinessException;

	/**
	 * @author hungnm
	 * @param displayProId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	RptBCKD11 getRptBCKD11(Long displayProId, Date fromDate, Date toDate)
			throws BusinessException;


	/**
	 * @author thuattq
	 */
	RptBCKD19VO getRptBCKD19(Long shopId, Integer numTimeBadScore,
			Date fromDate, Date toDate) throws BusinessException;

	/**
	 * @author hungnm
	 * @param shopId
	 * @param numTimeBadScore
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	RptBCKD19_1 getRptBCKD19_1(Long shopId, Integer numTimeBadScore,
			Date fromDate, Date toDate) throws BusinessException;


	/**
	 * @author hungnm
	 * @param shopId
	 * @param custCode
	 * @param fromMonth
	 * @param toMonth
	 * @return
	 * @throws BusinessException
	 */
	List<RptBCKD13VO> getRptBCKD13(Long shopId, String custCode,
			Date fromMonth, Date toMonth) throws BusinessException;
	
	/**
	 * @author sangtn
	 * @since 2014-03-04
	 */
	List<RptBCKD14VO> getRptBCKD14(Long shopId, Date fromMonth, Date toMonth)
			throws BusinessException;


	/**
	 * KD1.1 Doanh số - phân phối theo SKUs
	 * @author hungnm
	 * @param listIdMien
	 * @param listIdVung
	 * @param listIdNpp
	 * @param listIdSp
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	RptBCKD1_1 getRptBCKD1_1(List<Long> listShopId, List<Long> listIdSp,
			Date toDate) throws BusinessException;


	/**
	 * KD2 Doanh số bán hàng của NVBH theo Nhãn hàng từ ngày đến ngày
	 * @author hungnm
	 * @param mien
	 * @param vung
	 * @param tbhv
	 * @param maNpp
	 * @param gsNpp
	 * @param maNvbh
	 * @param nhan
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	RptBCKD2 getRptBCKD2(List<Long> listTbhv, List<Long> listShopId,
			List<Long> listGsNpp, List<Long> listNvbh, List<Long> listNhanId,
			Date toDate) throws BusinessException;
	
	/**
	 * 
	*  BCKD 21 - core hieunq
	*  @author: thanhnn
	*  @param shopCode
	*  @param fromDate
	*  @param toDate
	*  @return
	*  @throws BusinessException
	*  @return: RptBCKD21
	*  @throws:
	 */
	RptBCKD21 getRptBCKD21(Long shopId, Date toDate)
			throws BusinessException;

	RptBCKD15VO getRptBCKD15(Date currDate, Integer numMonth)
			throws BusinessException;

	//@author hunglm16; @since January 23, 2014; @description report Ds3: Doanh so ban hang cua nhan vien ban hang theo SKUs
	List<RptKD1_VO> exportRptBCKD1(Long shopId, String lstProductCode, Date fDate, Date tDate) throws BusinessException;

	RptKD_General_VO generalKD(Date fromDate, Date toDate) throws BusinessException;

	List<RptBCKD10_2_DLCPPTSkusDNVO> getRptBCKD10_2_DLCPPTSkusDN(Long subCat, Date fromDate, Date toDate) throws BusinessException;

	RptKD_8_1VO getKD8_1(Long shopId, List<Long> listStaffId, Date fromDate, Date toDate, Integer soLan) throws BusinessException;

	List<RptKD3_VO> exportRptBCKD3(Long shopId, String lstProductCode, Date fDate, Date tDate) throws BusinessException;
	
	RptKD_16_1VO getRptBCKD16_1(Date fromDate, Date toDate) throws BusinessException;

	/**
	 * Báo cáo KD9
	 * @author tungtt21
	 * @param shopId
	 * @param productCode
	 * @param tmpDate //tai ngay -30
	 * @param fDate //tai ngay +1
	 * @param aDate //tai ngay
	 * @param tDate //den ngay
	 * @return
	 * @throws BusinessException
	 */
	List<RptKD9> getRptKD9(Long shopId, String productCode, Date tmpDate, Date fDate, Date aDate, Date tDate) throws BusinessException;


	List<rpt_HO_KD15VO> rpt_HO_KD15_SDBLCPSDDCT(Long shopId, Date fromDate, Date toDate, Integer numberMonth) throws BusinessException;
}

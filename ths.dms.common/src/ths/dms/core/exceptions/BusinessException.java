package ths.dms.core.exceptions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class BussinessException.
 */
public class BusinessException extends Exception implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6127762932224120122L;
	
	/**
	 * error code defined in 'message_*' file
	 */
	private List<String> errorCodes;

	/**
	 * Instantiates a new bussiness exception.
	 */
	public BusinessException() {
		errorCodes = new ArrayList<String>();
	}

	/**
	 * Instantiates a new bussiness exception.
	 * 
	 * @param message
	 *            the message
	 */
	public BusinessException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new bussiness exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public BusinessException(Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new bussiness exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public List<String> getErrorCodes() {
		return errorCodes;
	}

	public void setErrorCodes(List<String> errorCodes) {
		this.errorCodes = errorCodes;
		/*
		 * test
		 */
	}

}

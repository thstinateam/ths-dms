package ths.dms.core.memcached;

/**
 * 
 * @author hienpt9
 */
public interface IMemcachedClient {
	public boolean set(String key, int expried, Object value);

	public Object get(String key, long timeout);

	public boolean isAvailable();
	
	public void delete(String key);
}
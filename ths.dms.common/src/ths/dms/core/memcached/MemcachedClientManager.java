package ths.dms.core.memcached;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import net.spy.memcached.MemcachedClient;
import net.spy.memcached.internal.GetFuture;


/**
 *
 * @author hienpt9
 */
public class MemcachedClientManager implements IMemcachedClient {
//	static MemcachedClient checkKeyServer = null;
	final String CHECK_AVAILABILITY_KEY = "CHECK_AVAILABILITY_KEY";

	public static final int DEFAULT_GET_TIME_OUT = 2000;

	// Logger dung de ghi log.
	// WARN: Chu y, config cua log4j phai duoc load khi khoi tao
	// static Logger logger = Logger.getLogger(MemcachedClientManager.class);

	// Doi tuong quan ly memcached clients
	static MemcachedClientManager instance = null;

	// Mang cac memcached clients
	ArrayList<MemcachedClient> memcachedClients = null;

	// next memcached index for selection arcording to roudrobin
	final Object lock = new Object();
	int nextMemcachedIndex = 0;

	// List memcached server adress
	static String LST_MEMCACHED_SV_ADDRESS = "";

	// state
	static boolean RUNNING = false;

	private MemcachedClientManager() {

		// Khoi tao ket noi toi 2 server memcached
		List<InetSocketAddress> lstServer = new ArrayList<InetSocketAddress>();

		// Doc dia chi tu file cau hinh,
		// neu cau hinh sai hoac 1 trong so cac IP khong ket noi duoc throw
		// exception
		// String[] listServer =
		// AppSetting.getStringValue("hostMemCached").split(",");
		String[] listServer = LST_MEMCACHED_SV_ADDRESS.split(",");
		/*logger.warn("[Step 1 - OK] List memcached server configuration loading is completed.");*/


		for (String child : listServer) {
			try {
				String[] detail = child.split(":");

				boolean res = isServerValid(detail[0].trim(), Integer.parseInt(detail[1].trim()));
				if (res) {
					InetSocketAddress svAddress = new InetSocketAddress(detail[0].trim(), Integer.parseInt(detail[1].trim()));
					lstServer.add(svAddress);
				}
			} catch (Exception ex) {
				// Log loi
				return;
			}
		}

		// Tao cac memcached client connect toi tung memcached server
		memcachedClients = new ArrayList<MemcachedClient>();
		for (int i = 0; i < lstServer.size(); i++) {
			try {
				MemcachedClient memcachedClient;
				memcachedClient = new MemcachedClient(lstServer.get(i));
				memcachedClients.add(memcachedClient);
			} catch (IOException ex) {
				return;
			}
		}


		try {
			// Wait 5s for sure the memcached client has been created successfully.
			while (true) {
				if (isMemcachedUseable()) {
					break;
				}
				Thread.sleep(1);
			}
		} catch (InterruptedException ex) {

		}

	}

	/**
	 * Load cau hinh, khoi tao Memcached client manager
	 *
	 * @param lstAddress
	 * @throws Exception
	 */
	public static void start(String lstAddress) throws Exception {
		if (!RUNNING) {
			LST_MEMCACHED_SV_ADDRESS = lstAddress;
			getInstance();
			RUNNING = true;
		}
	}

	public static synchronized MemcachedClientManager getInstance() throws Exception {
		if (instance == null) {
			instance = new MemcachedClientManager();
		}

		return instance;
	}

	/**
	 *
	 * expried: number of second
	 */
	@Override
	public boolean set(String key, int expried, Object value) {
		boolean returnValue = false;
		for (MemcachedClient memcachedClient : memcachedClients) {
			if (memcachedClient.getAvailableServers().size() > 0) {
				try {
					memcachedClient.set(key, expried, value);
					returnValue = true;
					/*LogUtils.logInfo(this.getClass(), "Data cached with k = " + key + " on "
							+ memcachedClient.getAvailableServers());*/
				} catch (Exception e) {

				}
			}
		}
		return returnValue;
	}

	/**
	 *
	 * timeout : milisecond
	 */
	@Override
	public Object get(String key, long timeout) {
		Object returnValue = null;
		int numOfTrying = 0;
		boolean hitCached = false;
		// Lay memcached client theo thuat toan roudrobin
		int currentMemcachIndex = getNextMemcachedClientIndex();

		// Khi chua lay duoc key, va so lan trying < tong so memcachedClient
		while (!hitCached && numOfTrying < memcachedClients.size()) {
			// Lay ra memcached client de xu ly
			MemcachedClient memcachClient = getMemcachedClient(currentMemcachIndex, numOfTrying);

			// Neu memcached server khong available, next
			if (memcachClient.getAvailableServers().isEmpty()) {
				numOfTrying++;
				/*logger.error("[Time trying " + numOfTrying + "] Key=" + key
						+ " Server: " + memcachClient.getUnavailableServers()
						+ " is unavailable!");*/
				continue;
			}

			// returnValue
			GetFuture<Object> f = memcachClient.asyncGet(key);
			try {
				returnValue = f.get();
			} catch (Exception e) {
				// Timeout
				f.cancel(true);
				numOfTrying++;

				continue;
			}

			if (returnValue != null) {
				// Hit cached
				hitCached = true;
				/*LogUtils.logInfo(this.getClass(), "Cached hit for key = " + key + " on "
						+ memcachClient.getAvailableServers());*/
			} else {
				numOfTrying++;
			}
		}
		return returnValue;
	}

	private boolean isServerValid(String ip, int port) throws Exception {
		Socket socket = null;
		try {
			socket = new Socket();
			socket.connect(new InetSocketAddress(ip, port), 1000);
			InputStream is = socket.getInputStream();
			is.close();
		} catch (Exception e) {
			throw e;
		} finally {
			if (socket!=null) {
				socket.close();
			}
		}
		return true;
	}

	private synchronized int getNextMemcachedClientIndex() {
		// MemcachedClient memcachedClient =
		// memcachedClients.get(nextMemcachedIndex);
		// System.out.println("Current memcached client: Available="+memcachedClient.getAvailableServers()+"| unavailable="+memcachedClient.getUnavailableServers());
		if (nextMemcachedIndex == (memcachedClients.size() - 1)) {
			nextMemcachedIndex = 0;
		} else {
			nextMemcachedIndex++;
		}
		return nextMemcachedIndex;
	}

	private MemcachedClient getMemcachedClient(int currentMemcachIndex, int numOfTrying) {
		MemcachedClient memcachedClient;
		int clientSize = memcachedClients.size();
		if ((currentMemcachIndex + numOfTrying) < clientSize) {
			memcachedClient = memcachedClients.get(currentMemcachIndex + numOfTrying);
		} else {
			memcachedClient = memcachedClients.get(currentMemcachIndex + numOfTrying - clientSize);
		}
		return memcachedClient;
	}

	private boolean isMemcachedUseable() {
		if (memcachedClients != null) {
			for (MemcachedClient memcachedClient : memcachedClients) {
				if (memcachedClient.getAvailableServers().size() > 0) {
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public boolean isAvailable() {
		boolean isAvailable = false;
		try {
			this.set(CHECK_AVAILABILITY_KEY, 1000, "OK");
		} catch (Exception e) {
			return isAvailable;
		}
		Object x = null;
		try {
			x = this.get(CHECK_AVAILABILITY_KEY, 1000);
		} catch (Exception e) {
			return isAvailable;
		}

		if (x != null && x.equals("OK")) {
			isAvailable = true;
		}

		return isAvailable;
	}

	@Override
	public void delete(String key) {
        for (MemcachedClient memcachedClient : memcachedClients) {
            if (memcachedClient.getAvailableServers().size() > 0) {
                try {
                    memcachedClient.delete(key);
                } catch (Exception e) {

                }
            }
        }
	}
}

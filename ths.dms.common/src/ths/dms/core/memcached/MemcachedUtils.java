package ths.dms.core.memcached;

/**
 * Thiet lap memcached cho cac nghiep vu cua chuc nang
 * @author tientv11
 * @since 26/02/2015
 *
 */
public class MemcachedUtils {
	
	/**
	 * Put gia tri vao memcached
	 * @author tuannd20
	 * @param key Key gia tri
	 * @param value Gia tri can put
	 * @param memcachedTimeoutInSecond So giay timeout
	 * @throws Exception 
	 * @since 13/04/2015
	 */
	public static void putValueToMemcached(String key, Object value, int memcachedTimeoutInSecond) throws Exception {
		MemcachedClientManager.getInstance().set(key, memcachedTimeoutInSecond, value);
	}
	
	/**
	 * Xoa gia tri da put tren memcached
	 * @author tuannd20
	 * @param key Key cua gia tri
	 * @throws Exception 
	 * @since 14/04/2015
	 */
	public static void deleteMemcachedValue(String key) throws Exception {
		MemcachedClientManager.getInstance().delete(key);
	}
	
	/**
	 * Get gia tri tu memcached
	 * @author tungmt
	 * @param key Key gia tri
	 * @throws Exception 
	 * @since 21/04/2015
	 */
	public static Object getValueFromMemcached(String key) throws Exception {
		//timeout ko su dung trong ham get
		return MemcachedClientManager.getInstance().get(key, 0l);
	}
}

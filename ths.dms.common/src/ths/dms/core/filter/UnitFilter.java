/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.filter;

import java.util.List;

import ths.dms.core.common.utils.Constant;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.OrganizationNodeType;

/**
 * Filter tim kiem don vi, nhan vien
 * @author tuannd20
 * @since 26/02/2015
 */
public class UnitFilter extends PriviledgeInfo {
	private static final long serialVersionUID = 8548856719877921083L;

	private String unitCode;
	private String unitName;
	private String unitType;
	private OrganizationNodeType searchObjectType;
	private ActiveType status;
	private Long manageUnitId;
	private String manageUnitCode;
	private String sortField;
	private String sortOrder;
	private List<Integer> lstStaffType;
	private Boolean isCurrentUser; // lay them chinh user dang nhap, khi dung ham f_get_list_child_staff_inherit(?, sysdate, ?, ?, null)
	private String strShopId;
	
	/**
	 * lay thong tin column de sort trong DB tu sortField
	 * @author tuannd20	 
	 * @return ten column de sort trong DB
	 */
	public String transformSortFieldToDBTableColumn() {
		if (searchObjectType != null && sortField != null && sortField.length() != 0) {
			if (OrganizationNodeType.SHOP == searchObjectType) {
				switch (sortField) {
				case "shopCode":
					sortField = "shopCode";
					break;
				case "shopName":
				case "parentShopName":
					sortField = "shopName";
					break;
				case "shopType":
					sortField = "shopType";
					break;
				case "status":
					sortField = "status";
					break;
				default:
					sortField = null;
					break;
				}
			} else if (OrganizationNodeType.STAFF == searchObjectType) {
				switch (sortField) {
				case "shopName":
					sortField = "shopName";
					break;
				case "staffCode":
					sortField = "staffCode";
					break;
				case "staffName":
					sortField = "staffName";
					break;
				case "phoneMobile":
					sortField = "phone";
					break;
				case "status":
					sortField = "status";
					break;
				case "phone":
					sortField = "phone";
					break;
				case "mobilePhone":
					sortField = "mobilephone";
					break;
				default:
					sortField = null;
					break;
				}
			}			
		}
		return sortField;
	}
	
	public String getUnitCode() {
		return unitCode;
	}
	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public String getManageUnitCode() {
		return manageUnitCode;
	}
	public void setManageUnitCode(String manageUnitCode) {
		this.manageUnitCode = manageUnitCode;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public String getSortOrder() {
		if (sortOrder != null && sortOrder.length() != 0) {
			if (sortOrder.equalsIgnoreCase(Constant.SORT_ASC) || sortOrder.equalsIgnoreCase(Constant.SORT_DESC)) {
				return sortOrder;
			}
		}
		return null;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public OrganizationNodeType getSearchObjectType() {
		return searchObjectType;
	}

	public void setSearchObjectType(OrganizationNodeType searchObjectType) {
		this.searchObjectType = searchObjectType;
	}

	public Long getManageUnitId() {
		return manageUnitId;
	}

	public void setManageUnitId(Long manageUnitId) {
		this.manageUnitId = manageUnitId;
	}

	public List<Integer> getLstStaffType() {
		return lstStaffType;
	}

	public void setLstStaffType(List<Integer> lstStaffType) {
		this.lstStaffType = lstStaffType;
	}

	public final Boolean getIsCurrentUser() {
		return isCurrentUser;
	}

	public final void setIsCurrentUser(Boolean isCurrentUser) {
		this.isCurrentUser = isCurrentUser;
	}
	
	public String getStrShopId() {
		return strShopId;
	}
	
	public void setStrShopId(String strShopId) {
		this.strShopId = strShopId;
	}
}

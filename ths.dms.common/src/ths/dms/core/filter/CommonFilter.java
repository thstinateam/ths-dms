package ths.dms.core.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 * Thong tin cac cot filter thuong dung khi tim kiem
 * @author trungtm6
 * @since 29/06/2015
 *
 */
public class CommonFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4948494140913237552L;
	private Long id;
	private Long shopId;
	private Long staffId;
	private Long customerId;
	private ActiveType status;
	private Long promotionId;
	private Long cycleId;
	private Long routingId;
	private Long objectId;
	private Integer year;
	private Integer cycleNum;//So thu tu chu ky
	private Integer planType;
	private Date fromDate;
	private Date toDate;
	private boolean notIn;
	private List<Long> lstId;
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getCycleNum() {
		return cycleNum;
	}
	public void setCycleNum(Integer cycleNum) {
		this.cycleNum = cycleNum;
	}
	public Integer getPlanType() {
		return planType;
	}
	public void setPlanType(Integer planType) {
		this.planType = planType;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Long getCycleId() {
		return cycleId;
	}
	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}
	public Long getRoutingId() {
		return routingId;
	}
	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getObjectId() {
		return objectId;
	}
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
	public List<Long> getLstId() {
		return lstId;
	}
	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
	public boolean isNotIn() {
		return notIn;
	}
	public void setNotIn(boolean notIn) {
		this.notIn = notIn;
	}
	
	
}

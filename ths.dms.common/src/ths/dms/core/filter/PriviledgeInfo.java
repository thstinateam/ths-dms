/**
 * 
 */
package ths.dms.core.filter;

import java.io.Serializable;

/**
 * Du lieu phan quyen
 * @author tuannd20
 * @since 08/10/2015
 */
public class PriviledgeInfo implements Serializable {

	private static final long serialVersionUID = 6451041269522035787L;

	private Long shopRootId;
	private Long staffRootId;
	private Long roleId;
	
	public Long getShopRootId() {
		return shopRootId;
	}
	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}
	public Long getStaffRootId() {
		return staffRootId;
	}
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
}

package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DebtPaymentType;
import ths.dms.core.entities.enumtype.PaymentStatus;


@Entity
@Table(name = "PAYMENT_DETAIL_TEMP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PAYMENT_DETAIL_TEMP_SEQ", allocationSize = 1)
public class PaymentDetailTemp implements Serializable {

	private static final long serialVersionUID = 1L;
	//gia tri thanh toan
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//id
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PAYMENT_DETAIL_ID")
	private Long id;

	//id bang cha
	/*@ManyToOne(targetEntity = DebitDetail.class)
	@JoinColumn(name = "DEBIT_DETAIL_ID", referencedColumnName = "DEBIT_DETAIL_ID")
	private DebitDetail debitDetail;*/
	@Basic
	@Column(name = "FOR_OBJECT_ID")
	private Long forObjectId;

	//loai thanh toan(0: tu dong; 1: tay; 2: xoa no nho)
	@Basic
	@Column(name = "PAYMENT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.DebtPaymentType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private DebtPaymentType paymentType;

	//ngay thanh toan
	@Basic
	@Column(name = "PAY_DATE", length = 50)
	@Temporal(TemporalType.TIMESTAMP)
	private Date payDate;

	//id phieu thu
	@ManyToOne(targetEntity = PayReceivedTemp.class)
	@JoinColumn(name = "PAY_RECEIVED_ID", referencedColumnName = "PAY_RECEIVED_ID")
	private PayReceivedTemp payReceived;
	
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Basic
	@Column(name = "DISCOUNT", length = 20)
	private BigDecimal discount;
	
	@Basic
	@Column(name = "TIME", length = 2)
	private Integer time;

	//Id khach hang
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;
	
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	//trang thai thanh toan
	@Basic
	@Column(name = "PAYMENT_STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PaymentStatus"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PaymentStatus paymentStatus = PaymentStatus.NOT_PAID_YET;
	
	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*public DebitDetail getDebitDetail() {
		return debitDetail;
	}

	public void setDebit(DebitDetail debitDetail) {
		this.debitDetail = debitDetail;
	}*/

	public DebtPaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(DebtPaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public PayReceivedTemp getPayReceived() {
		return payReceived;
	}

	public void setPayReceived(PayReceivedTemp payReceived) {
		this.payReceived = payReceived;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Long getForObjectId() {
		return forObjectId;
	}

	public void setForObjectId(Long forObjectId) {
		this.forObjectId = forObjectId;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;

/**
 * 
 * Auto generate entities - HaNoi standard
 * 
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "DISPLAY_PRODUCT_GROUP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "DISPLAY_PRODUCT_GROUP_SEQ", allocationSize = 1)
public class StDisplayPdGroup implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// id nhom
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "DISPLAY_PRODUCT_GROUP_ID")
	private Long id;
	
	// id cttb
	@ManyToOne(targetEntity = StDisplayProgram.class)
	@JoinColumn(name = "DISPLAY_PROGRAM_ID", referencedColumnName = "DISPLAY_PROGRAM_ID")
	private StDisplayProgram displayProgram;
	
	// ma nhom
	@Basic
	@Column(name = "DISPLAY_DP_GROUP_CODE", length = 40)
	private String displayProductGroupCode;

	// ten nhom
	@Basic
	@Column(name = "DISPLAY_DP_GROUP_NAME", length = 750)
	private String displayProductGroupName;
	
	// trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;
	
	// trang thai
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.DisplayProductGroupType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private DisplayProductGroupType type;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATE_DATE")
	private Date createDate;

	public StDisplayProgram getDisplayProgram() {
		return displayProgram;
	}

	public void setDisplayProgram(StDisplayProgram displayProgram) {
		this.displayProgram = displayProgram;
	}

	public DisplayProductGroupType getType() {
		return type;
	}

	public void setType(DisplayProductGroupType type) {
		this.type = type;
	}

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DATE")
	private Date updateDate;
	
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public String getDisplayProductGroupCode() {
		return displayProductGroupCode;
	}

	public void setDisplayProductGroupCode(String displayProductGroupCode) {
		this.displayProductGroupCode = displayProductGroupCode;
	}

	public String getDisplayProductGroupName() {
		return displayProductGroupName;
	}

	public void setDisplayProductGroupName(String displayProductGroupName) {
		this.displayProductGroupName = displayProductGroupName;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public StDisplayPdGroup clone() {
		StDisplayPdGroup obj = new StDisplayPdGroup();
		obj.setCreateDate(createDate);
		obj.setCreateUser(createUser);
		obj.setDisplayProductGroupCode(displayProductGroupCode);
		obj.setDisplayProductGroupName(displayProductGroupName);
		obj.setDisplayProgram(displayProgram);
		obj.setType(type);
		obj.setStatus(status);
		obj.setUpdateDate(updateDate);
		obj.setUpdateUser(updateUser);
		return obj;
	}	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author loctt
 * @since 18/09/2013
 * 
 */

@Entity
@Table(name = "DISPLAY_PROGRAM_LEVEL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "DISPLAY_PROGRAM_LEVEL_SEQ", allocationSize = 1)
public class StDisplayProgramLevel implements Serializable {

	private static final long serialVersionUID = 1L;
	//so tien
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//id cttb
	@ManyToOne(targetEntity = StDisplayProgram.class)
	@JoinColumn(name = "DISPLAY_PROGRAM_ID", referencedColumnName = "DISPLAY_PROGRAM_ID")
	private StDisplayProgram displayProgram;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "DISPLAY_PROGRAM_LEVEL_ID")
	private Long id;

	//ma muc
	@Basic
	@Column(name = "LEVEL_CODE", length = 100)
	private String levelCode;

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	@Basic
	@Column(name = "NUM_SKU", length = 10)
	private Integer numSku;
	
	@Basic
	@Column(name = "QUANTITY", length = 20)
	private Integer quantity;
	
	public Integer getQuantity() {
		return quantity;
	}


	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	//ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	
	public StDisplayProgramLevel clone() {
		StDisplayProgramLevel ret = new StDisplayProgramLevel();
		ret.setAmount(amount);
		ret.setStDisplayProgram(displayProgram);
		ret.setLevelCode(levelCode);
		ret.setStatus(status);
		ret.setNumSku(numSku);
		ret.setQuantity(quantity);
		ret.setCreateDate(createDate);
		ret.setCreateUser(createUser);
		ret.setUpdateDate(updateDate);
		ret.setUpdateUser(updateUser);
		return ret;
	}


	public BigDecimal getAmount() {
		return amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public StDisplayProgram getStDisplayProgram() {
		return displayProgram;
	}


	public void setStDisplayProgram(StDisplayProgram displayProgram) {
		this.displayProgram = displayProgram;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getLevelCode() {
		return levelCode;
	}


	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}


	public ActiveType getStatus() {
		return status;
	}


	public void setStatus(ActiveType status) {
		this.status = status;
	}


	public Integer getNumSku() {
		return numSku;
	}


	public void setNumSku(Integer numSku) {
		this.numSku = numSku;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getCreateUser() {
		return createUser;
	}


	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public String getUpdateUser() {
		return updateUser;
	}


	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
}
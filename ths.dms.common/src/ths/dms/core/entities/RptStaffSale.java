package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the RPT_STAFF_SALE database table.
 * 
 */
@Entity
@Table(name="RPT_STAFF_SALE")
public class RptStaffSale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RPT_STAFF_SALE_ID_GENERATOR", sequenceName="RPT_STAFF_SALE_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RPT_STAFF_SALE_ID_GENERATOR")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="STAFF_ID")
	private Staff staff;
	
	@Column(name="STAFF_NAME")
	private String staffName;
	
	@Column(name="STAFF_CODE")
	private String staffCode;
	
	@ManyToOne
	@JoinColumn(name="PARENT_STAFF_ID")
	private Staff parentStaff;
	
	@ManyToOne
	@JoinColumn(name="SHOP_ID")
	private Shop shop;
	
	@Column(name="MONTH_AMOUNT_PLAN")
	private BigDecimal monthAmountPlan;
	
	@Column(name="MONTH_AMOUNT")
	private BigDecimal monthAmount;
	
	@Column(name="MONTH_SCORE")
	private Integer monthScore;
	
	@Column(name="DAY_AMOUNT_PLAN")
	private BigDecimal dayAmountPlan;
	
	@Column(name="DAY_AMOUNT")
	private BigDecimal dayAmount;
	
	@Column(name="DAY_SCORE")
	private Integer dayScore;
	
	@Column(name="MONTH_SKU_PLAN")
	private Integer monthSkuPlan;
	
	@Column(name="MONTH_SKU")
	private Integer monthSku;
	
	@Column(name="FOCUS1_AMOUNT_PLAN")
	private BigDecimal focus1AmountPlan;
	
	@Column(name="FOCUS1_AMOUNT")
	private BigDecimal focus1Amount;
	
	@Column(name="FOCUS2_AMOUNT")
	private BigDecimal focus2Amount;
	
	@Column(name="FOCUS2_AMOUNT_PLAN")
	private BigDecimal focus2AmountPlan;
	
	@Column(name="DAY_CUST")
	private Integer dayCust;
	
	@Column(name="DAY_CUST_ORDER_PLAN")
	private Integer dayCustOrderPlan;
	
	@Column(name="DAY_CUST_ORDER")
	private Integer dayCustOrder;

    @Temporal( TemporalType.DATE)
	@Column(name="CREATE_DATE")
	private Date createDate;

    @Temporal( TemporalType.DATE)
	@Column(name="UPDATE_DATE")
	private Date updateDate;
    
    @Column(name="USER1")
	private String user1;
    
    @Column(name="USER2")
	private String user2;
    
    @Column(name="USER3")
	private String user3;
    
    @Column(name="USER4")
	private String user4;
    
    @Column(name="USER5")
	private String user5;
    
    @Column(name="USER6")
	private BigDecimal user6;
    
    @Column(name="USER7")
	private BigDecimal user7;
    
    @Column(name="USER8")
	private BigDecimal user8;
    
    @Column(name="USER9")
	private BigDecimal user9;
    
    @Column(name="USER10")
	private BigDecimal user10;
    
    @Column(name="POINT1")
	private BigDecimal point1;
    
    @Column(name="POINT2")
	private BigDecimal point2;
    
    @Column(name="POINT3")
	private BigDecimal point3;
    
    @Column(name="POINT4")
	private BigDecimal point4;
    
    @Column(name="POINT5")
	private BigDecimal point5;
    
    @Column(name="POINT1_MONTH")
	private BigDecimal point1Month;
    
    @Column(name="POINT2_MONTH")
	private BigDecimal point2Month;
    
    @Column(name="POINT3_MONTH")
	private BigDecimal point3Month;
    
    @Column(name="POINT4_MONTH")
	private BigDecimal point4Month;
    
    @Column(name="POINT5_MONTH")
	private BigDecimal point5Month;
    
    @Column(name="DAY_SKU_PLAN")
	private BigDecimal daySkuPlan;
    
    @Column(name="DAY_SKU")
	private BigDecimal daySku;
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BigDecimal getDayAmount() {
		return dayAmount;
	}

	public void setDayAmount(BigDecimal dayAmount) {
		this.dayAmount = dayAmount;
	}

	public BigDecimal getDayAmountPlan() {
		return dayAmountPlan;
	}

	public void setDayAmountPlan(BigDecimal dayAmountPlan) {
		this.dayAmountPlan = dayAmountPlan;
	}

	public Integer getDayScore() {
		return dayScore;
	}

	public void setDayScore(Integer dayScore) {
		this.dayScore = dayScore;
	}

	public BigDecimal getFocus1Amount() {
		return focus1Amount;
	}

	public void setFocus1Amount(BigDecimal focus1Amount) {
		this.focus1Amount = focus1Amount;
	}

	public BigDecimal getFocus1AmountPlan() {
		return focus1AmountPlan;
	}

	public void setFocus1AmountPlan(BigDecimal focus1AmountPlan) {
		this.focus1AmountPlan = focus1AmountPlan;
	}

	public BigDecimal getFocus2Amount() {
		return focus2Amount;
	}

	public void setFocus2Amount(BigDecimal focus2Amount) {
		this.focus2Amount = focus2Amount;
	}

	public BigDecimal getFocus2AmountPlan() {
		return focus2AmountPlan;
	}

	public void setFocus2AmountPlan(BigDecimal focus2AmountPlan) {
		this.focus2AmountPlan = focus2AmountPlan;
	}

	public BigDecimal getMonthAmount() {
		return monthAmount;
	}

	public void setMonthAmount(BigDecimal monthAmount) {
		this.monthAmount = monthAmount;
	}

	public BigDecimal getMonthAmountPlan() {
		return monthAmountPlan;
	}

	public void setMonthAmountPlan(BigDecimal monthAmountPlan) {
		this.monthAmountPlan = monthAmountPlan;
	}

	public Integer getMonthScore() {
		return monthScore;
	}

	public void setMonthScore(Integer monthScore) {
		this.monthScore = monthScore;
	}

	public Integer getMonthSku() {
		return monthSku;
	}

	public void setMonthSku(Integer monthSku) {
		this.monthSku = monthSku;
	}

	public Integer getMonthSkuPlan() {
		return monthSkuPlan;
	}

	public void setMonthSkuPlan(Integer monthSkuPlan) {
		this.monthSkuPlan = monthSkuPlan;
	}

	public Staff getParentStaff() {
		return parentStaff;
	}

	public void setParentStaff(Staff parentStaff) {
		this.parentStaff = parentStaff;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getDayCust() {
		return dayCust;
	}

	public void setDayCust(Integer dayCust) {
		this.dayCust = dayCust;
	}

	public Integer getDayCustOrderPlan() {
		return dayCustOrderPlan;
	}

	public void setDayCustOrderPlan(Integer dayCustOrderPlan) {
		this.dayCustOrderPlan = dayCustOrderPlan;
	}

	public Integer getDayCustOrder() {
		return dayCustOrder;
	}

	public void setDayCustOrder(Integer dayCustOrder) {
		this.dayCustOrder = dayCustOrder;
	}

	public String getUser1() {
		return user1;
	}

	public void setUser1(String user1) {
		this.user1 = user1;
	}

	public String getUser2() {
		return user2;
	}

	public void setUser2(String user2) {
		this.user2 = user2;
	}

	public String getUser3() {
		return user3;
	}

	public void setUser3(String user3) {
		this.user3 = user3;
	}

	public String getUser4() {
		return user4;
	}

	public void setUser4(String user4) {
		this.user4 = user4;
	}

	public String getUser5() {
		return user5;
	}

	public void setUser5(String user5) {
		this.user5 = user5;
	}

	public BigDecimal getUser6() {
		return user6;
	}

	public void setUser6(BigDecimal user6) {
		this.user6 = user6;
	}

	public BigDecimal getUser7() {
		return user7;
	}

	public void setUser7(BigDecimal user7) {
		this.user7 = user7;
	}

	public BigDecimal getUser8() {
		return user8;
	}

	public void setUser8(BigDecimal user8) {
		this.user8 = user8;
	}

	public BigDecimal getUser9() {
		return user9;
	}

	public void setUser9(BigDecimal user9) {
		this.user9 = user9;
	}

	public BigDecimal getUser10() {
		return user10;
	}

	public void setUser10(BigDecimal user10) {
		this.user10 = user10;
	}

	public BigDecimal getPoint1() {
		return point1;
	}

	public void setPoint1(BigDecimal point1) {
		this.point1 = point1;
	}

	public BigDecimal getPoint2() {
		return point2;
	}

	public void setPoint2(BigDecimal point2) {
		this.point2 = point2;
	}

	public BigDecimal getPoint3() {
		return point3;
	}

	public void setPoint3(BigDecimal point3) {
		this.point3 = point3;
	}

	public BigDecimal getPoint4() {
		return point4;
	}

	public void setPoint4(BigDecimal point4) {
		this.point4 = point4;
	}

	public BigDecimal getPoint5() {
		return point5;
	}

	public void setPoint5(BigDecimal point5) {
		this.point5 = point5;
	}

	public BigDecimal getPoint1Month() {
		return point1Month;
	}

	public void setPoint1Month(BigDecimal point1Month) {
		this.point1Month = point1Month;
	}

	public BigDecimal getPoint2Month() {
		return point2Month;
	}

	public void setPoint2Month(BigDecimal point2Month) {
		this.point2Month = point2Month;
	}

	public BigDecimal getPoint3Month() {
		return point3Month;
	}

	public void setPoint3Month(BigDecimal point3Month) {
		this.point3Month = point3Month;
	}

	public BigDecimal getPoint4Month() {
		return point4Month;
	}

	public void setPoint4Month(BigDecimal point4Month) {
		this.point4Month = point4Month;
	}

	public BigDecimal getPoint5Month() {
		return point5Month;
	}

	public void setPoint5Month(BigDecimal point5Month) {
		this.point5Month = point5Month;
	}

	public BigDecimal getDaySkuPlan() {
		return daySkuPlan;
	}

	public void setDaySkuPlan(BigDecimal daySkuPlan) {
		this.daySkuPlan = daySkuPlan;
	}

	public BigDecimal getDaySku() {
		return daySku;
	}

	public void setDaySku(BigDecimal daySku) {
		this.daySku = daySku;
	}
}
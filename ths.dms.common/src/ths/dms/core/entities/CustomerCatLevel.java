package ths.dms.core.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "CUSTOMER_CAT_LEVEL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "CUSTOMER_CAT_LEVEL_SEQ", allocationSize = 1)
public class CustomerCatLevel implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "CUSTOMER_CAT_LEVEL_ID")
	private Long id;

	//id kh
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	//id muc
	@ManyToOne(targetEntity = SaleLevelCat.class)
	@JoinColumn(name = "SALE_LEVEL_CAT_ID", referencedColumnName = "SALE_LEVEL_CAT_ID")
	private SaleLevelCat saleLevelCat;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public SaleLevelCat getSaleLevelCat() {
		return saleLevelCat;
	}

	public void setSaleLevelCat(SaleLevelCat saleLevelCat) {
		this.saleLevelCat = saleLevelCat;
	}

	
}
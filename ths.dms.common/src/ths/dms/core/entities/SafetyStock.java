package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.SafetyStockType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "SAFETY_STOCK")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SAFETY_STOCK_SEQ", allocationSize = 1)
public class SafetyStock implements Serializable {

	private static final long serialVersionUID = 1L;
	//ngay ban hang trong tuan VD: 0000357
	@Basic
	@Column(name = "CALENDAR_D", length = 40)
	private String calendarD;

	//tuan ban hang trong thang
	@Basic
	@Column(name = "CALENDAR_W", length = 40)
	private String calendarW;

	//nganh hang
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "CAT_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo cat;

	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 40)
	private String createUser;

	//ngay di duong
	@Basic
	@Column(name = "LEAD", length = 22)
	private Integer lead;

	//ton kho max
	@Basic
	@Column(name = "MAXSF", length = 22)
	private Integer maxsf;

	//ton kho min
	@Basic
	@Column(name = "MINSF", length = 22)
	private Integer minsf;

	//ti le tang truong (VD 120%)
	@Basic
	@Column(name = "PERCENTAGE", length = 22)
	private Float percentage;

	//san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//vung - chua dung
	@Basic
	@Column(name = "REGION", length = 20)
	private String region;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SAFETY_STOCK_ID")
	private Long id;

	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	//1: theo nganh hang, 2: theo mat hang
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.SafetyStockType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private SafetyStockType type;

//	//nam
//	@Basic
//	@Column(name = "YEAR", length = 10)
//	private String year;

	public String getCalendarD() {
		return calendarD;
	}

	public void setCalendarD(String calendarD) {
		this.calendarD = calendarD;
	}

	public String getCalendarW() {
		return calendarW;
	}

	public void setCalendarW(String calendarW) {
		this.calendarW = calendarW;
	}

	public ProductInfo getCat() {
		return cat;
	}

	public void setCat(ProductInfo cat) {
		this.cat = cat;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Integer getLead() {
		if (lead == null) {
			lead = 0;
		}
		return lead;
	}

	public void setLead(Integer lead) {
		this.lead = lead;
	}

	public Integer getMaxsf() {
		if (maxsf == null) {
			maxsf = 0;
		}
		return maxsf;
	}

	public void setMaxsf(Integer maxsf) {
		this.maxsf = maxsf;
	}

	public Integer getMinsf() {
		if (minsf == null) {
			minsf = 0;
		}
		return minsf;
	}

	public void setMinsf(Integer minsf) {
		this.minsf = minsf;
	}

	public Float getPercentage() {
		return percentage == null ? 0f : percentage;
	}

	public void setPercentage(Float percentage) {
		this.percentage = percentage;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public SafetyStockType getType() {
		return type;
	}

	public void setType(SafetyStockType type) {
		this.type = type;
	}
//
//	public String getYear() {
//		return year;
//	}
//
//	public void setYear(String year) {
//		this.year = year;
//	}

	
}
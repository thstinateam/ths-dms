package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.TrainningPlanDetailStatus;


/**
 * The persistent class for the TRAINING_PLAN_DETAIL database table.
 * 
 */
@Entity
@Table(name="TRAINING_PLAN_DETAIL")
@SequenceGenerator(name="TRAINING_PLAN_DETAIL_ID_GENERATOR", sequenceName="TRAINING_PLAN_DETAIL_SEQ",allocationSize = 1)
public class TrainingPlanDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRAINING_PLAN_DETAIL_ID_GENERATOR")
	@Column(name = "TRAINING_PLAN_DETAIL_ID")
	private Long id;

	private BigDecimal amount;

	@Column(name="AMOUNT_PLAN")
	private BigDecimal amountPlan;

	@Column(name="AREA_MANAGER_ID")
	private Long areaManagerId;

    @Temporal( TemporalType.DATE)
	@Column(name="CREATE_DATE")
	private Date createDate;

	private String note;

	@Column(name="NUM_CUSTOMER_IR")
	private Integer numCustomerIr;

	@Column(name="NUM_CUSTOMER_NEW")
	private Integer numCustomerNew;

	@Column(name="NUM_CUSTOMER_ON")
	private Integer numCustomerOn;

	@Column(name="NUM_CUSTOMER_OR")
	private Integer numCustomerOr;

	@Column(name="NUM_CUSTOMER_ORDER")
	private Integer numCustomerOrder;

	@Column(name="NUM_CUSTOMER_PLAN")
	private Integer numCustomerPlan;

	private Integer score;

	@ManyToOne
	@JoinColumn(name="SHOP_ID")
	private Shop shop;
	
	@ManyToOne
	@JoinColumn(name="STAFF_ID")
	private Staff staff;

	
	//Trang thai cua lich huan luyen:
	//0: Tao moi; 
	//1: Da huan luyen; 
	//2: Huy bo; 
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.TrainningPlanDetailStatus"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private TrainningPlanDetailStatus status = TrainningPlanDetailStatus.NEW ;

    @Temporal( TemporalType.DATE)
	@Column(name="TRAINING_DATE")
	private Date trainingDate;

    @ManyToOne
	@JoinColumn(name="TRAINING_PLAN_ID")
	private TrainingPlan trainingPlan;

    @Temporal( TemporalType.DATE)
	@Column(name="UPDATE_DATE")
	private Date updateDate;
    
 // Trang thai cua lich huan luyen:
    // 0: Tao moi;
    // 1: ;
    // 2: Huy bo;
    @Basic
    @Column(name = "STATUS_MANAGER", columnDefinition = "integer")
    @Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
                @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.TrainningPlanDetailStatus"),
                @Parameter(name = "identifierMethod", value = "getValue"),
                @Parameter(name = "valueOfMethod", value = "parseValue") })
    private TrainningPlanDetailStatus statusManager;
    
    @Basic
	@Column(name = "NOTE_MANAGER", length = 1000)
	private String noteManager;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getAmountPlan() {
		return amountPlan;
	}

	public void setAmountPlan(BigDecimal amountPlan) {
		this.amountPlan = amountPlan;
	}

	public Long getAreaManagerId() {
		return areaManagerId;
	}

	public void setAreaManagerId(Long areaManagerId) {
		this.areaManagerId = areaManagerId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getNumCustomerIr() {
		return numCustomerIr;
	}

	public void setNumCustomerIr(Integer numCustomerIr) {
		this.numCustomerIr = numCustomerIr;
	}

	public Integer getNumCustomerNew() {
		return numCustomerNew;
	}

	public void setNumCustomerNew(Integer numCustomerNew) {
		this.numCustomerNew = numCustomerNew;
	}

	public Integer getNumCustomerOn() {
		return numCustomerOn;
	}

	public void setNumCustomerOn(Integer numCustomerOn) {
		this.numCustomerOn = numCustomerOn;
	}

	public Integer getNumCustomerOr() {
		return numCustomerOr;
	}

	public void setNumCustomerOr(Integer numCustomerOr) {
		this.numCustomerOr = numCustomerOr;
	}

	public Integer getNumCustomerOrder() {
		return numCustomerOrder;
	}

	public void setNumCustomerOrder(Integer numCustomerOrder) {
		this.numCustomerOrder = numCustomerOrder;
	}

	public Integer getNumCustomerPlan() {
		return numCustomerPlan;
	}

	public void setNumCustomerPlan(Integer numCustomerPlan) {
		this.numCustomerPlan = numCustomerPlan;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}


	public TrainningPlanDetailStatus getStatus() {
		return status;
	}

	public void setStatus(TrainningPlanDetailStatus status) {
		this.status = status;
	}

	public Date getTrainingDate() {
		return trainingDate;
	}

	public void setTrainingDate(Date trainingDate) {
		this.trainingDate = trainingDate;
	}

	public TrainingPlan getTrainingPlan() {
		return trainingPlan;
	}

	public void setTrainingPlan(TrainingPlan trainingPlan) {
		this.trainingPlan = trainingPlan;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public TrainningPlanDetailStatus getStatusManager() {
		return statusManager;
	}

	public void setStatusManager(TrainningPlanDetailStatus statusManager) {
		this.statusManager = statusManager;
	}

	public String getNoteManager() {
		return noteManager;
	}

	public void setNoteManager(String noteManager) {
		this.noteManager = noteManager;
	}

    
}
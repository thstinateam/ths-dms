package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_IMPORT_RECORD")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_IMPORT_SEQ", allocationSize = 1)
public class EquipImportRecord implements Serializable {

	private static final long serialVersionUID = 1L;

	//m? bi�n b?n nh?p
	@Basic
	@Column(name = "CODE", length = 400)
	private String code;

	

	@Basic
	@Column(name = "APPROVE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approveDate;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//ID nh�m thi?t b?
	@ManyToOne(targetEntity = EquipGroup.class)
	@JoinColumn(name = "EQUIP_GROUP_ID", referencedColumnName = "EQUIP_GROUP_ID")
	private EquipGroup equipGroup;

	//ID bi�n b?n nh?p
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_IMPORT_RECORD_ID")
	private Long id;

//	//K? l�m vi?c
	@ManyToOne(targetEntity = EquipPeriod.class)
	@JoinColumn(name = "EQUIP_PRIOD_ID", referencedColumnName = "EQUIP_PERIOD_ID")
	private EquipPeriod equipPriod;


	//ID nh� cung c?p
	@ManyToOne(targetEntity = EquipProvider.class)
	@JoinColumn(name = "EQUIP_PROVIDER_ID", referencedColumnName = "EQUIP_PROVIDER_ID")
	private EquipProvider equipProvider;

	//T?nh tr?ng thi?t b?. Link qua AP_PARAM v?i AP_PARAM.TYPE = EQUIP_CONDITION
	@Basic
	@Column(name = "HEALTH_STATUS", length = 500)
	private String healthStatus;

	//nguy�n gi�
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal price;

	//s? l�?ng nh?p
	@Basic
	@Column(name = "QUANTITY")
	private Integer quantity;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	//ng�y h?t h?n b?o h�nh
	@Basic
	@Column(name = "WARRANTY_EXPIRED_DATE", length = 7)
	private Date warrantyExpiredDate;

	@Basic
	@Column(name = "MANUFACTURING_YEAR")
	private Integer manuFacturingYear;

	@Basic
	@Column(name = "STOCK_ID")
	private Long stockId;
	
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public EquipGroup getEquipGroup() {
		return equipGroup;
	}

	public void setEquipGroup(EquipGroup equipGroup) {
		this.equipGroup = equipGroup;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipPeriod getEquipPriod() {
		return equipPriod;
	}

	public void setEquipPriod(EquipPeriod equipPriod) {
		this.equipPriod = equipPriod;
	}

	public EquipProvider getEquipProvider() {
		return equipProvider;
	}

	public void setEquipProvider(EquipProvider equipProvider) {
		this.equipProvider = equipProvider;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getWarrantyExpiredDate() {
		return warrantyExpiredDate;
	}

	public void setWarrantyExpiredDate(Date warrantyExpiredDate) {
		this.warrantyExpiredDate = warrantyExpiredDate;
	}

	public Integer getManuFacturingYear() {
		return manuFacturingYear;
	}

	public void setManuFacturingYear(Integer manuFacturingYear) {
		this.manuFacturingYear = manuFacturingYear;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KeyShopType;

/**
 *
 * Auto generate entities
 * @author tungmt
 * @since 30/6/2015
 * 
 */

@Entity
@Table(name = "KS")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "KS_SEQ", allocationSize = 1)
public class KS implements Serializable {

	private static final long serialVersionUID = 1L;

	//ID no
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "KS_ID")
	private Long id;
	
	@Basic
	@Column(name = "KS_CODE", length = 100)
	private String ksCode;
	
	@Basic
	@Column(name = "NAME", length = 200)
	private String name;
	
	@Basic
	@Column(name = "DESCRIPTION", length = 500)
	private String description;
	
	@ManyToOne(targetEntity = Cycle.class)
	@JoinColumn(name = "FROM_CYCLE_ID", referencedColumnName = "CYCLE_ID")
	private Cycle fromCycle;
	
	@ManyToOne(targetEntity = Cycle.class)
	@JoinColumn(name = "TO_CYCLE_ID", referencedColumnName = "CYCLE_ID")
	private Cycle toCycle;
	
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 100)
	private String createUser;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 100)
	private String updateUser;
	
	@Basic
	@Column(name = "NAME_TEXT", length = 200)
	private String nameText;
	
	@Basic
	@Column(name = "PROGRAM_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.KeyShopType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private KeyShopType programType = KeyShopType.KEY_SHOP ;
	
	@Basic
	@Column(name = "MIN_PHOTO_NUM", length = 3)
	private Integer minPhotoNum = 0;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKsCode() {
		return ksCode;
	}

	public void setKsCode(String ksCode) {
		this.ksCode = ksCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Cycle getFromCycle() {
		return fromCycle;
	}

	public void setFromCycle(Cycle fromCycle) {
		this.fromCycle = fromCycle;
	}

	public Cycle getToCycle() {
		return toCycle;
	}

	public void setToCycle(Cycle toCycle) {
		this.toCycle = toCycle;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameText() {
		return nameText;
	}

	public void setNameText(String nameText) {
		this.nameText = nameText;
	}

	public KeyShopType getProgramType() {
		return programType;
	}

	public void setProgramType(KeyShopType programType) {
		this.programType = programType;
	}

	public Integer getMinPhotoNum() {
		return minPhotoNum;
	}

	public void setMinPhotoNum(Integer minPhotoNum) {
		this.minPhotoNum = minPhotoNum;
	}

}
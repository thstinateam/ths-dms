package ths.dms.core.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "MEDIA_THUMBNAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "MEDIA_THUMBNAIL_SEQ", allocationSize = 1)
public class MediaThumbnail implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "ID")
	private Long id;


	//chính là id cua product
	@ManyToOne(targetEntity = MediaItem.class)
	@JoinColumn(name = "MEDIA_ITEM_ID", referencedColumnName = "MEDIA_ITEM_ID")
	private MediaItem mediaItem;

	@Basic
	@Column(name = "URL", length = 250)
	private String url;

	@Basic
	@Column(name = "WIDTH", length = 22)
	private Float width;
	@Basic
	@Column(name = "HEIGHT", length = 22)
	private Float height;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public MediaItem getMediaItem() {
		return mediaItem;
	}
	public void setMediaItem(MediaItem mediaItem) {
		this.mediaItem = mediaItem;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Float getWidth() {
		return width;
	}
	public void setWidth(Float width) {
		this.width = width;
	}
	public Float getHeight() {
		return height;
	}
	public void setHeight(Float height) {
		this.height = height;
	}

	
}
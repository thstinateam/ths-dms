package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author loctt
 * @since 19/9/2013
 * 
 */

@Entity
@Table(name = "DISPLAY_PL_AMT_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "DISPLAY_PL_AMT_DTL_SEQ", allocationSize = 1)
public class StDisplayPlAmtDtl implements Serializable {

	private static final long serialVersionUID = 1L;


	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "DISPLAY_PL_AMT_DTL_ID")
	private Long id;

	//id 
	@ManyToOne(targetEntity = StDisplayProgramLevel.class)
	@JoinColumn(name = "DISPLAY_PROGRAM_LEVEL_ID", referencedColumnName = "DISPLAY_PROGRAM_LEVEL_ID")
	private StDisplayProgramLevel displayProgramLevel;
	
	//id 
	@ManyToOne(targetEntity = StDisplayPdGroup.class)
	@JoinColumn(name = "DISPLAY_PRODUCT_GROUP_ID", referencedColumnName = "DISPLAY_PRODUCT_GROUP_ID")
	private StDisplayPdGroup displayProductGroup;
	
	//
	@Basic
	@Column(name = "MIN", length = 22)
	private BigDecimal min;
	
	//
	@Basic
	@Column(name = "MAX", length = 22)
	private BigDecimal max;
	
	//% chiet khau
	@Basic
	@Column(name = "PERCENT", length = 22)
	private Float precent;
	
	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	//ngay tao
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATE_DATE")
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	//ngay cap nhat
	@Basic	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DATE")
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	
	public StDisplayPlAmtDtl clone() {
		StDisplayPlAmtDtl ret = new StDisplayPlAmtDtl();
		ret.setDisplayProgramLevel(displayProgramLevel);
		ret.setDisplayProductGroup(displayProductGroup);
		ret.setMax(max);
		ret.setMin(min);
		ret.setPrecent(precent);
		ret.setStatus(status);
		ret.setCreateDate(createDate);
		ret.setCreateUser(createUser);
		ret.setUpdateDate(updateDate);
		ret.setUpdateUser(updateUser);
		return ret;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public StDisplayProgramLevel getDisplayProgramLevel() {
		return displayProgramLevel;
	}


	public void setDisplayProgramLevel(StDisplayProgramLevel displayProgramLevel) {
		this.displayProgramLevel = displayProgramLevel;
	}


	public StDisplayPdGroup getDisplayProductGroup() {
		return displayProductGroup;
	}


	public void setDisplayProductGroup(StDisplayPdGroup displayProductGroup) {
		this.displayProductGroup = displayProductGroup;
	}


	public BigDecimal getMin() {
		return min;
	}


	public void setMin(BigDecimal min) {
		this.min = min;
	}


	public BigDecimal getMax() {
		return max;
	}


	public void setMax(BigDecimal max) {
		this.max = max;
	}


	public Float getPrecent() {
		return precent;
	}


	public void setPrecent(Float precent) {
		this.precent = precent;
	}


	public ActiveType getStatus() {
		return status;
	}


	public void setStatus(ActiveType status) {
		this.status = status;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getCreateUser() {
		return createUser;
	}


	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public String getUpdateUser() {
		return updateUser;
	}


	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
}
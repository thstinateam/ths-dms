package ths.dms.core.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "CAR")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "CAR_SEQ", allocationSize = 1)
public class Car implements Serializable {

	private static final long serialVersionUID = 1L;
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "CAR_ID")
	private Long id;

	//bien so xe
	@Basic
	@Column(name = "CAR_NUMBER", length = 20)
	private String carNumber;

	//chung lo?i : kho, lanh - cau hinh gia tri vao bang ap_param
	@Basic
	@Column(name = "CATEGORY", length = 40)
	private String category;

	//trong luong cho cua xe
	@Basic
	@Column(name = "GROSS", length = 22)
	private Float gross;

	//lay thong tin tu bang channnel_type
	@ManyToOne(targetEntity = ChannelType.class)
	@JoinColumn(name = "LABEL_ID", referencedColumnName = "CHANNEL_TYPE_ID")
	private ChannelType label;

	//xuat xu: xe cua NPP, muon - cau hinh gia tri vao bang ap_param
	@Basic
	@Column(name = "ORIGIN", length = 40)
	private String origin;

	//ID NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//trang thai 0 khong hieu luc, 1 co hieu luc
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	//loai xe: xe may, o to 4 banh - cau hinh vao bang ap_param
	@Basic
	@Column(name = "TYPE", length = 20)
	private String type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Float getGross() {
//		return gross == null ? 0f : gross;
		return gross;
	}

	public void setGross(Float gross) {
		this.gross = gross;
	}

	public ChannelType getLabel() {
		return label;
	}

	public void setLabel(ChannelType label) {
		this.label = label;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
}
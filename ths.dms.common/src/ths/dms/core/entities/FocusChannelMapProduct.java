package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "FOCUS_CHANNEL_MAP_PRODUCT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "FOCUS_CHANNEL_MAP_PRODUCT_SEQ", allocationSize = 1)
public class FocusChannelMapProduct implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//id cttt
	@ManyToOne(targetEntity = FocusChannelMap.class)
	@JoinColumn(name = "FOCUS_CHANNEL_MAP_ID", referencedColumnName = "FOCUS_CHANNEL_MAP_ID")
	private FocusChannelMap focusChannelMap;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "FOCUS_CHANNEL_MAP_PRODUCT_ID")
	private Long id;

	//id sp
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Basic
	@Column(name = "TYPE", length = 50)
	private String type;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public FocusChannelMap getFocusChannelMap() {
		return focusChannelMap;
	}

	public void setFocusChannelMap(FocusChannelMap focusChannelMap) {
		this.focusChannelMap = focusChannelMap;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public FocusChannelMapProduct clone() {
		FocusChannelMapProduct m = new FocusChannelMapProduct();
		m.setCreateDate(this.getCreateDate());
		m.setCreateUser(this.getCreateUser());
		m.setFocusChannelMap(this.getFocusChannelMap());
		m.setId(this.getId());
		m.setProduct(this.getProduct());
		m.setUpdateDate(this.getUpdateDate());
		m.setUpdateUser(this.getUpdateUser());
		m.setType(this.getType());
		return m;
	}
	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;

/**
 * Auto generate entities - HCM standard
 * 
 * @author hungnm16
 * @since December 14,2014
 * @description entities thuoc kenh thiet bi
 */
@Entity
@Table(name = "EQUIP_HISTORY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_HISTORY_SEQ", allocationSize = 1)
public class EquipHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_HISTORY_ID")
	private Long id;

	@ManyToOne(targetEntity = Equipment.class)
	@JoinColumn(name = "EQUIP_ID", referencedColumnName = "EQUIP_ID")
	private Equipment equip;

	//M? kho c?a �?i t�?ng OBJECT_ID
	/*@Basic
	@Column(name = "EQUIP_WAREHOUSE_CODE", length = 400)
	private String equipWarehouseCode;
*/
	//T?nh tr?ng thi?t b?. Link qua AP_PARAM v?i AP_PARAM.TYPE = EQUIP_CONDITION
	@Basic
	@Column(name = "HEALTH_STATUS", length = 500)
	private String healthStatus;

	//ID c?a �?i t�?ng trong OBJECT_TYPE
	@Basic
	@Column(name = "OBJECT_ID")
	private Long objectId;

	@Basic
	@Column(name = "OBJECT_CODE")
	private String objectCode;
	
	//1. VNM, 2. NPP, 3. KH----
	// Gio dung: vuongmq; 20/05/2015; 1: NPP; 2 KH
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipStockTotalType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipStockTotalType objectType;

	//0: Inactive; 1: Active
	@Basic
	@Column(name = "STATUS")
	private Integer status;

	//0: �ang giao d?ch, 1: Kh�ng giao d?ch
	@Basic
	@Column(name = "TRADE_STATUS", length = 22)
	private Integer tradeStatus;

	//0: Giao nh?n, 1: Chuy?n kho, 2: S?a ch?a, 3: B�o m?t, 4: B�o m?t t? mobile, 5: Thu h?i thanh l?, 6: Thanh l?
	@Basic
	@Column(name = "TRADE_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipTradeType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipTradeType tradeType = EquipTradeType.DELIVERY;

	/*
	 * @Basic
	 * 
	 * @Column(name = "UPDATE_DATE")
	 * 
	 * @Temporal(TemporalType.TIMESTAMP) private Date updateDate;
	 * 
	 * @Basic
	 * 
	 * @Column(name = "UPDATE_USER", length = 200) private String updateUser;
	 */

	//0: �? m?t, 1: �? thanh l?, 2: �ang ? kho, 3: �ang s? d?ng, 4: �ang s?a ch?a
	@Basic
	@Column(name = "USAGE_STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipUsageStatus"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipUsageStatus usageStatus = EquipUsageStatus.SHOWING_WAREHOUSE;

	//Tên kho c?a ??i t??ng STOCK_ID
	@Basic
	@Column(name = "STOCK_NAME", length = 500)
	private String stockName;

	//Tên b?ng c?a biên b?n
	@Basic
	@Column(name = "TABLE_NAME", length = 50)
	private String tableName;
	
	//ID biên b?n
	@Basic
	@Column(name = "FORM_ID")
	private Long formId;
	
	//gia tri con lai
	@Basic
	@Column(name = "PRICE_ACTUALLY")
	private BigDecimal priceActually;
	
	//Lo?i biên b?n: 0: Giao nh?n, 1: Chuy?n kho, 2: S?a ch?a, 3: Báo m?t, 4: Báo m?t t? mobile, 5: Thu h?i, 6: Thanh lý, 7: Ki?m kê, 8: Thanh toán s?a ch?a, 9: C?p m?i
	@Basic
	@Column(name = "FORM_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipTradeType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipTradeType formType;
	
	/** gia tri thanh ly, thu hoi dung cho thanh ly tai san */
	// Gia tri thanh ly
	@Basic
	@Column(name = "LIQUIDATION_VALUE", length = 22)
	private BigDecimal liquidationValue;
	
	// Gia tri thu hoi
	@Basic
	@Column(name = "EVICTION_VALUE", length = 22)
	private BigDecimal evictionValue;
	
	/** trang thai thanh ly tai san: dung cho thiet bi cua bao mat */
	@Basic
	@Column(name = "LIQUIDATION_STATUS")
	private Integer liquidationStatus;//Enum tuong ung LinquidationStatus; khong dung enum
	
	public EquipUsageStatus getUsageStatus() {
		return usageStatus;
	}

	public void setUsageStatus(EquipUsageStatus usageStatus) {
		this.usageStatus = usageStatus;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Equipment getEquip() {
		return equip;
	}

	public void setEquip(Equipment equip) {
		this.equip = equip;
	}

	/*public String getEquipWarehouseCode() {
		return equipWarehouseCode;
	}

	public void setEquipWarehouseCode(String equipWarehouseCode) {
		this.equipWarehouseCode = equipWarehouseCode;
	}*/

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public EquipStockTotalType getObjectType() {
		return objectType;
	}

	public void setObjectType(EquipStockTotalType objectType) {
		this.objectType = objectType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(Integer tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public EquipTradeType getTradeType() {
		return tradeType;
	}

	public void setTradeType(EquipTradeType tradeType) {
		this.tradeType = tradeType;
	}

	/*
	 * public Date getUpdateDate() { return updateDate; }
	 * 
	 * public void setUpdateDate(Date updateDate) { this.updateDate =
	 * updateDate; }
	 * 
	 * public String getUpdateUser() { return updateUser; }
	 * 
	 * public void setUpdateUser(String updateUser) { this.updateUser =
	 * updateUser; }
	 */

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public EquipTradeType getFormType() {
		return formType;
	}

	public void setFormType(EquipTradeType formType) {
		this.formType = formType;
	}

	public String getObjectCode() {
		return objectCode;
	}

	public void setObjectCode(String objectCode) {
		this.objectCode = objectCode;
	}

	public BigDecimal getPriceActually() {
		return priceActually;
	}

	public void setPriceActually(BigDecimal priceActually) {
		this.priceActually = priceActually;
	}

	public BigDecimal getLiquidationValue() {
		return liquidationValue;
	}

	public void setLiquidationValue(BigDecimal liquidationValue) {
		this.liquidationValue = liquidationValue;
	}

	public BigDecimal getEvictionValue() {
		return evictionValue;
	}

	public void setEvictionValue(BigDecimal evictionValue) {
		this.evictionValue = evictionValue;
	}

	public Integer getLiquidationStatus() {
		return liquidationStatus;
	}

	public void setLiquidationStatus(Integer liquidationStatus) {
		this.liquidationStatus = liquidationStatus;
	}

	
}
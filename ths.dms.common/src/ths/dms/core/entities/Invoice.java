package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.common.utils.GroupUtility;
import ths.dms.core.entities.enumtype.InvoiceCustPayment;
import ths.dms.core.entities.enumtype.InvoiceStatus;

/**
*
* Auto generate entities - HaNoi standard
* @author nhanlt6
* @since 15/8/2014
* 
*/

@Entity
@Table(name = "INVOICE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "INVOICE_SEQ", allocationSize = 1)
public class Invoice implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//Id hoa don VAT
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "INVOICE_ID")
	private Long id;

	//So hoa don
	@Basic
	@Column(name = "INVOICE_NUMBER", length = 20)
	private String invoiceNumber;
	
	//Chiet khau
	@Basic
	@Column(name = "DISCOUNT", length = 22)
	private BigDecimal discount;
	
	//Id don hang
	@ManyToOne(targetEntity = SaleOrder.class)
	@JoinColumn(name = "SALE_ORDER_ID", referencedColumnName = "SALE_ORDER_ID")
	private SaleOrder saleOrder;
	
	//Id NVBH
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	//Id NVGH
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "DELIVERY_ID", referencedColumnName = "STAFF_ID")
	private Staff deliveryStaff;
	
	//Id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//Trang thai don hang:0 ->dang su dung; 1 ->da sua; 2 ->da huy
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.InvoiceStatus"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private InvoiceStatus status;

	//Tien thue
	@Basic
	@Column(name = "TAX_AMOUNT", length = 22)
	private BigDecimal taxAmount;

	//Thue VAT
	@Basic
	@Column(name = "VAT", length = 20)
	private Float vat;
		
	//So tien chiu thue
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//Dia chi cty
	@Basic
	@Column(name = "CPNY_ADDR", length = 600)
	private String cpnyAddr;

	//tài khoan ngân hàng cua cty
	@Basic
	@Column(name = "CPNY_BANK_ACCOUNT", length = 50)
	private String cpnyBankAccount;

	//Ten cong ty
	@Basic
	@Column(name = "CPNY_NAME", length = 600)
	private String cpnyName;

	//So dien thoai cty
	@Basic
	@Column(name = "CPNY_PHONE", length = 20)
	private String cpnyPhone;

	//Mã so thue cty
	@Basic
	@Column(name = "CPNY_TAX_NUMBER", length = 50)
	private String cpnyTaxNumber;

	//Dia chi khach hang
	@Basic
	@Column(name = "CUSTOMER_ADDR", length = 600)
	private String customerAddr;

	//Ma khach hang
	@Basic
	@Column(name = "CUSTOMER_CODE", length = 22)
	private String customerCode;

	//Tai khan ngan hang kh
	@Basic
	@Column(name = "CUST_BANK_ACCOUNT", length = 50)
	private String custBankAccount;

	//Tên ngan hang kh
	@Basic
	@Column(name = "CUST_BANK_NAME", length = 50)
	private String custBankName;

	@Basic
	@Column(name = "CPNY_BANK_NAME", length = 50)
	private String cpnyBankName;
	
	//Dia chi giao hang
	@Basic
	@Column(name = "CUST_DEL_ADDR", length = 250)
	private String custDelAddr;

	//Ten khach hang
	@Basic
	@Column(name = "CUST_NAME", length = 50)
	private String custName;

	//1: tien mat; 2: chuyen khoan
	@Basic
	@Column(name = "CUST_PAYMENT", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.InvoiceCustPayment"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private InvoiceCustPayment custPayment;

	//Ma so thue khach hang
	@Basic
	@Column(name = "CUST_TAX_NUMBER", length = 50)
	private String custTaxNumber;

	//Ten NVGH
	@Basic
	@Column(name = "DELIVERY_NAME", length = 300)
	private String deliveryName;

	//Ngày xuat hóa don (ngay gán so hoa don GTGT)
	@Basic
	@Column(name = "INVOICE_DATE", length = 7)
	private Date invoiceDate;

	//Ngay dat hang
	@Basic
	@Column(name = "ORDER_DATE", length = 7)
	private Date orderDate;

	//So don hang
	@Basic
	@Column(name = "ORDER_NUMBER", length = 50)
	private String orderNumber;

	//Ten NVBH
	@Basic
	@Column(name = "SEL_NAME", length = 300)
	private String selName;

	//Tong so mat hang trong don hang
	@Basic
	@Column(name = "SKU", length = 22)
	private Long sku;

	//Loai don hang
	@Basic
	@Column(name = "SALE_ORDER_TYPE", length = 2)
	private Integer saleOrderType;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Basic
	@Column(name = "DESTROY_USER", length = 50)
	private String destroyUser;
	
	@Basic
	@Column(name = "DESTROY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date destroyDate;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	@Transient
	private String payment;
	
	@Transient
	private String total;
	
	@Transient String taxTotal;
	
	@Transient
	private String customer;
	
	@Transient
	private String staffInfo;
	
	@Transient
	private String deliveryStaffInfo;
	
	@Transient
	private String date;
	
	@Transient
	private String stringVat;
	
	public Integer getSaleOrderType() {
		return saleOrderType;
	}

	public void setSaleOrderType(Integer saleOrderType) {
		this.saleOrderType = saleOrderType;
	}

	public String getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(String taxTotal) {
		this.taxTotal = taxTotal;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getStaffInfo() {
		return staffInfo;
	}

	public void setStaffInfo(String staffInfo) {
		this.staffInfo = staffInfo;
	}

	public String getDeliveryStaffInfo() {
		return deliveryStaffInfo;
	}

	public void setDeliveryStaffInfo(String deliveryStaffInfo) {
		this.deliveryStaffInfo = deliveryStaffInfo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStringVat() {
		return stringVat;
	}

	public void setStringVat(String stringVat) {
		this.stringVat = stringVat;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCpnyAddr() {
		return cpnyAddr;
	}

	public void setCpnyAddr(String cpnyAddr) {
		this.cpnyAddr = cpnyAddr;
	}

	public String getCpnyBankAccount() {
		return cpnyBankAccount;
	}

	public void setCpnyBankAccount(String cpnyBankAccount) {
		this.cpnyBankAccount = cpnyBankAccount;
	}

	public String getCpnyName() {
		return cpnyName;
	}

	public void setCpnyName(String cpnyName) {
		this.cpnyName = cpnyName;
	}

	public String getCpnyPhone() {
		return cpnyPhone;
	}

	public void setCpnyPhone(String cpnyPhone) {
		this.cpnyPhone = cpnyPhone;
	}

	public String getCpnyTaxNumber() {
		return cpnyTaxNumber;
	}

	public void setCpnyTaxNumber(String cpnyTaxNumber) {
		this.cpnyTaxNumber = cpnyTaxNumber;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCustomerAddr() {
		return customerAddr;
	}

	public void setCustomerAddr(String customerAddr) {
		this.customerAddr = customerAddr;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustBankAccount() {
		return custBankAccount;
	}

	public void setCustBankAccount(String custBankAccount) {
		this.custBankAccount = custBankAccount;
	}

	public String getCustBankName() {
		return custBankName;
	}

	public void setCustBankName(String custBankName) {
		this.custBankName = custBankName;
	}

	public String getCustDelAddr() {
		return custDelAddr;
	}

	public void setCustDelAddr(String custDelAddr) {
		this.custDelAddr = custDelAddr;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public InvoiceCustPayment getCustPayment() {
		return custPayment;
	}

	public void setCustPayment(InvoiceCustPayment custPayment) {
		this.custPayment = custPayment;
	}

	public String getCustTaxNumber() {
		return custTaxNumber;
	}

	public void setCustTaxNumber(String custTaxNumber) {
		this.custTaxNumber = custTaxNumber;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getSelName() {
		return selName;
	}

	public void setSelName(String selName) {
		this.selName = selName;
	}

	public Long getSku() {
		return sku == null ? Long.valueOf(0) : sku;
	}

	public void setSku(Long sku) {
		this.sku = sku;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public InvoiceStatus getStatus() {
		return status;
	}

	public void setStatus(InvoiceStatus status) {
		this.status = status;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount == null ? BigDecimal.valueOf(0) : taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Float getVat() {
		return vat == null ? 0f : vat;
	}

	public void setVat(Float vat) {
		this.vat = vat;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getCpnyBankName() {
		return cpnyBankName;
	}

	public void setCpnyBankName(String cpnyBankName) {
		this.cpnyBankName = cpnyBankName;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public String getDestroyUser() {
		return destroyUser;
	}

	public void setDestroyUser(String destroyUser) {
		this.destroyUser = destroyUser;
	}

	public Date getDestroyDate() {
		return destroyDate;
	}

	public void setDestroyDate(Date destroyDate) {
		this.destroyDate = destroyDate;
	}

	public Staff getDeliveryStaff() {
		return deliveryStaff;
	}

	public void setDeliveryStaff(Staff deliveryStaff) {
		this.deliveryStaff = deliveryStaff;
	}
	
	
	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public void formatData(){
		payment = custPayment.name().equals("MONEY")?"Tiền mặt":"Chuyển khoản";
		if(discount==null){
			discount = BigDecimal.ZERO;
		}
		BigDecimal thanhTien = amount.subtract(discount);
		total = GroupUtility.convertMoney(thanhTien);
		taxTotal = GroupUtility.convertMoney(taxAmount);
		customer = customerCode + " - " + custName;
		staffInfo = staff == null ? "" : staff.getStaffCode();
		deliveryStaffInfo = deliveryStaff == null ? "" : deliveryStaff.getStaffCode();
		date = GroupUtility.toDateSimpleFormatString(orderDate);
		stringVat = vat + "%";
	}
	
	public Invoice clone(){
		Invoice tmpInvoice = new Invoice();
		tmpInvoice.setAmount(amount);
		tmpInvoice.setCpnyAddr(cpnyAddr);
		tmpInvoice.setCpnyBankAccount(cpnyBankAccount);
		tmpInvoice.setCpnyName(cpnyName);
		tmpInvoice.setCpnyBankName(cpnyBankName);
		tmpInvoice.setCpnyPhone(cpnyPhone);
		tmpInvoice.setCpnyTaxNumber(cpnyTaxNumber);
		tmpInvoice.setCustBankAccount(custBankAccount);
		tmpInvoice.setCustBankName(custBankName);
		tmpInvoice.setCustDelAddr(custDelAddr);
		tmpInvoice.setCustName(custName);
		tmpInvoice.setCustomerAddr(customerAddr);
		tmpInvoice.setCustomerCode(customerCode);
		tmpInvoice.setCustPayment(custPayment);
		tmpInvoice.setCustTaxNumber(custTaxNumber);
		tmpInvoice.setDeliveryName(deliveryName);
		tmpInvoice.setDeliveryStaff(deliveryStaff);
		tmpInvoice.setDiscount(discount);
		tmpInvoice.setOrderDate(orderDate);
		tmpInvoice.setOrderNumber(orderNumber);
		tmpInvoice.setSelName(selName);
		tmpInvoice.setShop(shop);
		tmpInvoice.setSku(sku);
		tmpInvoice.setStaff(staff);
		tmpInvoice.setTaxAmount(taxAmount);
		tmpInvoice.setVat(vat);
		tmpInvoice.setSaleOrder(saleOrder);
		tmpInvoice.setSaleOrderType(saleOrderType);
		return tmpInvoice;
	}

}
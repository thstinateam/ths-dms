package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ApprovalStatus;

/**
 * 
 * Auto generate entities - HaNoi standard
 * 
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PO_AUTO")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PO_AUTO_SEQ", allocationSize = 1)
public class PoAuto implements Serializable {

	private static final long serialVersionUID = 1L;
	// tong tien
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	// dia chi giao hang
	@Basic
	@Column(name = "BILLTOLOCATION", length = 600)
	private String billtolocation;

	// chiet khau
	@Basic
	@Column(name = "DISCOUNT", length = 22)
	private BigDecimal discount;

	// ngay sua
	@Basic
	@Column(name = "MODIFY_DATE", length = 7)
	private Date modifyDate;

	// han thanh toan
	@Basic
	@Column(name = "PAYMENTTERM", length = 600)
	private String paymentterm;

	// ngay tao don hang
	@Basic
	@Column(name = "PO_AUTO_DATE", length = 7)
	private Date poAutoDate;

	// id
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PO_AUTO_ID")
	private Long id;

	// po number
	@Basic
	@Column(name = "PO_AUTO_NUMBER", length = 40)
	private String poAutoNumber;

	// phuong tien van chuyen
	@Basic
	@Column(name = "SHIPTOLOCATION", length = 600)
	private String shiptolocation;

	// id nha phan phoi
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	// id nhan vien thuc hien
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	// trang thai 0 chua duyet, 1 da duyet, 2 da huy
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ApprovalStatus"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ApprovalStatus status;

	// tong tien phai tra
	@Basic
	@Column(name = "TOTAL", length = 22)
	private BigDecimal total;
	
	// phuong tien van chuyen
	@Basic
	@Column(name = "DESCRIPTION", length = 250)
	private String description;

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBilltolocation() {
		return billtolocation;
	}

	public void setBilltolocation(String billtolocation) {
		this.billtolocation = billtolocation;
	}

	public BigDecimal getDiscount() {
		return discount == null ? BigDecimal.valueOf(0) : discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getPaymentterm() {
		return paymentterm;
	}

	public void setPaymentterm(String paymentterm) {
		this.paymentterm = paymentterm;
	}

	public Date getPoAutoDate() {
		return poAutoDate;
	}

	public void setPoAutoDate(Date poAutoDate) {
		this.poAutoDate = poAutoDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPoAutoNumber() {
		return poAutoNumber;
	}

	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}

	public String getShiptolocation() {
		return shiptolocation;
	}

	public void setShiptolocation(String shiptolocation) {
		this.shiptolocation = shiptolocation;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public ApprovalStatus getStatus() {
		return status;
	}

	public void setStatus(ApprovalStatus status) {
		this.status = status;
	}

	public BigDecimal getTotal() {
		return total == null ? BigDecimal.valueOf(0) : total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
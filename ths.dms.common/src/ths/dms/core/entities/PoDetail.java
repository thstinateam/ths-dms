/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Mo ta class PoDetail.java
 * @author vuongmq
 * @since Dec 7, 2015
 */

@Entity
@Table(name = "PO_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PO_DETAIL_SEQ", allocationSize = 1)
public class PoDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PO_DETAIL_ID")
	private Long id;
	
	//id Po
	@ManyToOne(targetEntity = Po.class)
	@JoinColumn(name = "PO_ID", referencedColumnName = "PO_ID")
	private Po po;
	
	//Product id
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;
	
	@Basic
	@Column(name = "AMOUNT", length = 26)
	private BigDecimal amount;

	@Basic
	@Column(name = "QUANTITY", length = 20)
	private Integer quantity;
	
	// so luong thung
	@Basic
	@Column(name = "PACKAGE_QUANTITY", length = 20)
	private Integer packageQuantity;
	
	// so luong le
	@Basic
	@Column(name = "RETAIL_QUANTITY", length = 20)
	private Integer retailQuantity;
	
	// gia thung
	@Basic
	@Column(name = "PACKAGE_PRICE", length = 26)
	private BigDecimal packagePrice;

	// gia le
	@Basic
	@Column(name = "RETAIL_PRICE", length = 26)
	private BigDecimal retailPrice;
	
	//PriceSaleIn id
	@ManyToOne(targetEntity = PriceSaleIn.class)
	@JoinColumn(name = "PRICE_SALE_IN_ID", referencedColumnName = "PRICE_SALE_IN_ID")
	private PriceSaleIn priceSaleIn;
	
	//gia tri quy doi tu Thung -> le
	@Basic
	@Column(name = "CONVFACT", length = 20)
	private Integer convfact;
	
	//po_line_number
	@Basic
	@Column(name = "PO_LINE_NUMBER", length = 20)
	private Integer poLineNumber;
	
	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Po getPo() {
		return po;
	}

	public void setPo(Po po) {
		this.po = po;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getPackageQuantity() {
		return packageQuantity;
	}

	public void setPackageQuantity(Integer packageQuantity) {
		this.packageQuantity = packageQuantity;
	}

	public Integer getRetailQuantity() {
		return retailQuantity;
	}

	public void setRetailQuantity(Integer retailQuantity) {
		this.retailQuantity = retailQuantity;
	}

	public BigDecimal getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(BigDecimal packagePrice) {
		this.packagePrice = packagePrice;
	}

	public BigDecimal getRetailPrice() {
		return retailPrice;
	}

	public void setRetailPrice(BigDecimal retailPrice) {
		this.retailPrice = retailPrice;
	}

	public PriceSaleIn getPriceSaleIn() {
		return priceSaleIn;
	}

	public void setPriceSaleIn(PriceSaleIn priceSaleIn) {
		this.priceSaleIn = priceSaleIn;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Integer getPoLineNumber() {
		return poLineNumber;
	}

	public void setPoLineNumber(Integer poLineNumber) {
		this.poLineNumber = poLineNumber;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
}
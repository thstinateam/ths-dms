package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.LinquidationStatus;
import ths.dms.core.entities.enumtype.StatusType;

/**
 * Auto generate entities - HCM standard
 * 
 * @author hungnm16
 * @since December 14,2014
 * @description entities thuoc kenh thiet bi
 */
@SuppressWarnings("deprecation")
@Entity
@Table(name = "EQUIPMENT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIPMENT_SEQ", allocationSize = 1)
public class Equipment implements Serializable {

	private static final long serialVersionUID = 1L;

	// m? n?i b?
	@Basic
	@Column(name = "CODE", length = 100)
	private String code;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	// ID class
	@ManyToOne(targetEntity = EquipGroup.class)
	@JoinColumn(name = "EQUIP_GROUP_ID", referencedColumnName = "EQUIP_GROUP_ID")
	private EquipGroup equipGroup;

	// //ID object
	// @ManyToOne(targetEntity = Equipment.class)
	// @JoinColumn(name = "EQUIP_ID", referencedColumnName = "EQUIP_ID")
	// private Equipment equip;

	// Kh�a ch�nh
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_ID")
	private Long id;

	// Nhà cung c?p nhóm thi?t b?. PK EQUIPMENT_PROVIDER
	@ManyToOne(targetEntity = EquipProvider.class)
	@JoinColumn(name = "EQUIP_PROVIDER_ID", referencedColumnName = "EQUIP_PROVIDER_ID")
	private EquipProvider equipProvider;

	// Ngày đ?u tiên đưa vào s? d?ng
	@Basic
	@Column(name = "FIRST_DATE_IN_USE", length = 7)
	private Date firstDateInUse;

	// T?nh tr?ng thi?t b?. Link qua AP_PARAM v?i AP_PARAM.TYPE =
	// EQUIP_CONDITION
	@Basic
	@Column(name = "HEALTH_STATUS", length = 500)
	private String healthStatus;

	// năm s?n xu?t
	@Basic
	@Column(name = "MANUFACTURING_YEAR", length = 5)
	private Integer manufacturingYear;

	//Bien ban cap moi
	@ManyToOne(targetEntity = EquipImportRecord.class)
	@JoinColumn(name = "EQUIP_IMPORT_RECORD_ID", referencedColumnName = "EQUIP_IMPORT_RECORD_ID")
	private EquipImportRecord equipImportRecord;

	// nguyên giá
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal price;

	// Gia tri con lai
	@Basic
	@Column(name = "RETURN_VALUE", length = 22)
	private BigDecimal priceActually;
	
	
	// m? serial
	@Basic
	@Column(name = "SERIAL", length = 100)
	private String serial;

	// 0. Inactive, 1. Active
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.StatusType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private StatusType status = StatusType.ACTIVE;

	// M? kho c?a đ?i tư?ng OBJECT_ID
	@Basic
	@Column(name = "STOCK_CODE", length = 400)
	private String stockCode;

	// Ten kho c?a đ?i tư?ng OBJECT_ID
		@Basic
		@Column(name = "STOCK_NAME", length = 400)
		private String stockName;
		
	// ID c?a đ?i tư?ng trong OBJECT_TYPE
	@Basic
	@Column(name = "STOCK_ID")
	private Long stockId;

	// 1. VNM, 2. NPP, 3. KH
	
	// vuongmq; 17/04/2015; dac ta cap nhat; 1. kho DonVi, 2. KH,
	@Basic
	@Column(name = "STOCK_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipStockTotalType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipStockTotalType stockType;

	//kho
	/*@ManyToOne(targetEntity = EquipStock.class)
	@JoinColumn(name = "STOCK_ID", referencedColumnName = "EQUIP_STOCK_ID")
	private EquipStock equipStock;*/
		
	// 0: Không giao d?ch, 1: dang giao dich
	@Basic
	@Column(name = "TRADE_STATUS")
	private Integer tradeStatus;

	// 0: Giao nh?n, 1: Chuy?n kho, 2: S?a ch?a, 3: Báo m?t, 4: Báo m?t t?
	// mobile, 5: Thu h?i thanh l?, 6: Thanh l?
	@Basic
	@Column(name = "TRADE_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipTradeType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipTradeType tradeType = EquipTradeType.DELIVERY;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	@Basic
	@Column(name = "LIQUIDATION_STATUS")
	private Integer linquidationStatus;//Enum tuong ung LinquidationStatus
	/**
	 * linquidationStatus
	 * Luu y, khong dung Enum cho truong nay
	 * @author hunglm16
	 * */
//	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.LinquidationStatus"), @Parameter(name = "identifierMethod", value = "getValue"),
//			@Parameter(name = "valueOfMethod", value = "parseValue") })
//	private LinquidationStatus linquidationStatus = LinquidationStatus.WAITING_LIQUIDATION;
	
	// 0: Đ? m?t, 1: Đ? thanh l?, 2: Đang ? kho, 3: Đang s? d?ng, 4: Đang s?a
	// ch?a
	@Basic
	@Column(name = "USAGE_STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipUsageStatus"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipUsageStatus usageStatus = EquipUsageStatus.SHOWING_WAREHOUSE;

	//EquipUsageStatus

	@Basic
	@Column(name = "WARRANTY_EXPIRED_DATE", length = 7)
	private Date warrantyExpiredDate;

	@Basic
	@Column(name = "LOST_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lostDate;
	
	/** insert khi thanh ly ở trang thái: đã thanh ly*/
	@Basic
	@Column(name = "LIQUIDATION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date liquidationDate;
	
	//Sao chep Entity
	public Equipment clone() {
		Equipment obj = new Equipment();
		obj.setCode(code);
		obj.setCreateDate(createDate);
		obj.setCreateUser(createUser);
		obj.setEquipGroup(equipGroup);
		obj.setId(id);
		obj.setEquipProvider(equipProvider);
		obj.setFirstDateInUse(firstDateInUse);
		obj.setHealthStatus(healthStatus);
		obj.setManufacturingYear(manufacturingYear);
		obj.setEquipImportRecord(equipImportRecord);
		obj.setPrice(price);
		obj.setPriceActually(priceActually);
		obj.setSerial(serial);
		obj.setStatus(status);
		obj.setStockCode(stockCode);
		obj.setStockName(stockName);
		obj.setStockId(stockId);
		obj.setStockType(stockType);
		obj.setTradeStatus(tradeStatus);
		obj.setTradeType(tradeType);		
		obj.setUpdateDate(updateDate);
		obj.setUpdateUser(updateUser);
		obj.setUsageStatus(usageStatus);		
		obj.setWarrantyExpiredDate(warrantyExpiredDate);
		obj.setLostDate(lostDate);
		obj.setLiquidationDate(liquidationDate);
		obj.setLinquidationStatus(linquidationStatus);
		return obj;
	}
	
	public EquipImportRecord getEquipImportRecord() {
		return equipImportRecord;
	}

	public void setEquipImportRecord(EquipImportRecord equipImportRecord) {
		this.equipImportRecord = equipImportRecord;
	}

	public StatusType getStatus() {
		return status;
	}

	public void setStatus(StatusType status) {
		this.status = status;
	}

	public EquipStockTotalType getStockType() {
		return stockType;
	}

	public void setStockType(EquipStockTotalType stockType) {
		this.stockType = stockType;
	}

	public Integer getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(Integer tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public EquipTradeType getTradeType() {
		return tradeType;
	}

	public void setTradeType(EquipTradeType tradeType) {
		this.tradeType = tradeType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public EquipGroup getEquipGroup() {
		return equipGroup;
	}

	public void setEquipGroup(EquipGroup equipGroup) {
		this.equipGroup = equipGroup;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipProvider getEquipProvider() {
		return equipProvider;
	}

	public void setEquipProvider(EquipProvider equipProvider) {
		this.equipProvider = equipProvider;
	}

	public Date getFirstDateInUse() {
		return firstDateInUse;
	}

	public void setFirstDateInUse(Date firstDateInUse) {
		this.firstDateInUse = firstDateInUse;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public Integer getManufacturingYear() {
		return manufacturingYear;
	}

	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getWarrantyExpiredDate() {
		return warrantyExpiredDate;
	}

	public void setWarrantyExpiredDate(Date warrantyExpiredDate) {
		this.warrantyExpiredDate = warrantyExpiredDate;
	}

	public EquipUsageStatus getUsageStatus() {
		return usageStatus;
	}

	public void setUsageStatus(EquipUsageStatus usageStatus) {
		this.usageStatus = usageStatus;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public BigDecimal getPriceActually() {
		return priceActually;
	}

	public void setPriceActually(BigDecimal priceActually) {
		this.priceActually = priceActually;
	}

	public Date getLostDate() {
		return lostDate;
	}

	public void setLostDate(Date lostDate) {
		this.lostDate = lostDate;
	}

	public Date getLiquidationDate() {
		return liquidationDate;
	}

	public void setLiquidationDate(Date liquidationDate) {
		this.liquidationDate = liquidationDate;
	}

	public Integer getLinquidationStatus() {
		return linquidationStatus;
	}

	public void setLinquidationStatus(Integer linquidationStatus) {
		this.linquidationStatus = linquidationStatus;
	}

	/*public EquipStock getEquipStock() {
		return equipStock;
	}

	public void setEquipStock(EquipStock equipStock) {
		this.equipStock = equipStock;
	}*/
	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * entity chi tiet phieu thanh toan cho phieu sua chua thiet bi
 * @author tuannd20
 */
/**
 * entity phieu thanh toan sua chua thiet bi; cap nhat 1 phieu thanh toan nhieu phieu sua chua
 * @author vuongmq
 * @date 22/06/2015
 */
@Entity
@Table(name = "EQUIP_REPAIR_PAY_FORM_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_REPAIR_PAY_FORM_DTL_SEQ", allocationSize = 1)
public class EquipRepairPayFormDtl implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_REPAIR_PAY_FORM_DTL_ID")
	private Long id;
	
	@ManyToOne(targetEntity = EquipRepairPayForm.class)
	@JoinColumn(name = "EQUIP_REPAIR_PAY_FORM_ID", referencedColumnName = "EQUIP_REPAIR_PAY_FORM_ID")
	private EquipRepairPayForm equipRepairPayForm;
	
	@ManyToOne(targetEntity = EquipRepairForm.class)
	@JoinColumn(name = "EQUIP_REPAIR_FORM_ID", referencedColumnName = "EQUIP_REPAIR_FORM_ID")
	private EquipRepairForm equipRepairForm;
	
	/*@OneToOne(targetEntity = EquipRepairFormDtl.class)
	@JoinColumn(name = "EQUIP_REPAIR_FORM_DTL_ID", referencedColumnName = "EQUIP_REPAIR_FORM_DTL_ID")
	private EquipRepairFormDtl equipRepairFormDtl;*/
	
	@Basic
	@Column(name = "TOTAL_ACTUAL_AMOUNT", length = 22)
	private BigDecimal totalActualAmount;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipRepairPayForm getEquipRepairPayForm() {
		return equipRepairPayForm;
	}

	public void setEquipRepairPayForm(EquipRepairPayForm equipRepairPayForm) {
		this.equipRepairPayForm = equipRepairPayForm;
	}

	public EquipRepairForm getEquipRepairForm() {
		return equipRepairForm;
	}

	public void setEquipRepairForm(EquipRepairForm equipRepairForm) {
		this.equipRepairForm = equipRepairForm;
	}

	public BigDecimal getTotalActualAmount() {
		return totalActualAmount;
	}

	public void setTotalActualAmount(BigDecimal totalActualAmount) {
		this.totalActualAmount = totalActualAmount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
}

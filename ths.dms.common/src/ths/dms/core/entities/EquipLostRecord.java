package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.DeliveryType;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;


/**
 * Auto generate entities - HCM standard
 * 
 * @author hungnm16
 * @since December 14,2014
 * @description entities thuoc kenh thiet bi
 */

/**
 * Auto generate entities - HCM standard
 * 
 * @author vuongmq
 * @since 27/04/2015
 * cap nhat
 * @description entities thuoc kenh thiet bi
 * entities Ql bao mat
 */
@Entity
@Table(name = "EQUIP_LOST_RECORD")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_LOST_RECORD_SEQ", allocationSize = 1)
public class EquipLostRecord implements Serializable {

	private static final long serialVersionUID = 1L;

	//m? bi�n b?n b�o m?t
	@Basic
	@Column(name = "CODE", length = 400)
	private String code;
	
	//Ghi chu
	@Basic
	@Column(name = "NOTE", length = 500)
	private String note;

	//k?t lu?n
	@Basic
	@Column(name = "CONCLUSION", length = 1000)
	private String conclusion;

	@Basic
	@Column(name = "CREATE_FORM_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createFormDate;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//trạng thái giao nhận 0: Chưa gửi, 1: Đã gửi, 2: Đã nhận
	/*@Basic
	@Column(name = "DELIVERY_STATUS")
	private Integer deliveryStatus;*/
	
	/** vuongmq: 25/04/2015; trạng thái giao nhận 0: Chưa gửi, 1: Đã gửi, 2: Đã nhận */
	// dung Enum: DeliveryType co san
	@Basic
	@Column(name = "DELIVERY_STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.DeliveryType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private DeliveryType deliveryStatus;

	//ID thi?t b?
	@ManyToOne(targetEntity = Equipment.class)
	@JoinColumn(name = "EQUIP_ID", referencedColumnName = "EQUIP_ID")
	private Equipment equip;

	//ID bi�n b?n b�o m?t
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_LOST_RECORD_ID")
	private Long id;

	//ID k? thi?t b?
	@ManyToOne(targetEntity = EquipPeriod.class)
	@JoinColumn(name = "EQUIP_PERIOD_ID", referencedColumnName = "EQUIP_PERIOD_ID")
	private EquipPeriod equipPeriod;

	//ng�y ph�t sinh doanh s? cu?i
	@Basic
	@Column(name = "LAST_ARISING_SALES_DATE", columnDefinition = "timestamp(9) default systimestamp")
	private Date lastArisingSalesDate;

	//ng�y m?t
	@Basic
	@Column(name = "LOST_DATE", columnDefinition = "timestamp(9) default systimestamp")
	private Date lostDate;

	//h�?ng x? l?
	@Basic
	@Column(name = "RECOMMENDED_TREATMENT", length = 1000)
	private String recommendedTreatment;

	//Trạng thái biên bản 0: Dự thảo, 1: Chờ duyệt, 2: Đã duyệt, 3: Không duyệt, 4: Hủy, 5: Đang sửa chữa, 6: Hoàn tất, 7: Đã thanh lý
	/*@Basic
	@Column(name = "RECORD_STATUS")
	private Integer recordStatus;*/
	
	/** Trạng thái biên bản 0: Dự thảo, 1: Chờ duyệt, 2: Đã duyệt, 3: Không duyệt, 4: Hủy, 5: Đang sửa chữa, 6: Hoàn tất, 7: Đã thanh lý */
	// dung Enum: StatusRecordsEquip
	@Basic
	@Column(name = "RECORD_STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.StatusRecordsEquip"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private StatusRecordsEquip recordStatus = StatusRecordsEquip.DRAFT;

	//ID nh�n vi�n b�o m?t
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "REPORT_STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	// tên người/tổ chức đại diện
	@Basic
	@Column(name = "REPRESENTOR", length = 200)
	private String representor;

	//m? kho
	@Basic
	@Column(name = "STOCK_CODE", length = 400)
	private String stockCode;

	//ID kho
	/*@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "STOCK_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;
	 */
	//ID kho; dua vao loai kho: Loại kho 1.NPP, 2: KH
	@Basic
	@Column(name = "STOCK_ID", length = 22)
	private Long stockId;

		
	//ID bien ban giao nhan moi nhat
	@ManyToOne(targetEntity = EquipDeliveryRecord.class)
	@JoinColumn(name = "EQUIP_DELIVERY_RECORD_ID", referencedColumnName = "EQUIP_DELIVERY_RECORD_ID")
	private EquipDeliveryRecord equipDeliveryRecord;
	
	//lo?i kho
	/*@Basic
	@Column(name = "STOCK_TYPE")
	private Integer stockType;*/
	
	/** vuongmq: 25/04/2015; 1: kho DonVi, 2: kho KH */
	// dung Enum: EquipStockTotalType
	@Basic
	@Column(name = "STOCK_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipStockTotalType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipStockTotalType stockType;
	

	//n�i truy t?m 0: t?i �?a ch? kinh doanh, 1: t?i �?a ch? th�?ng tr�
	@Basic
	@Column(name = "TRACING_PLACE")
	private Integer tracingPlace;

	//k?t qu? truy t?m 0: t?m ��?c kh�ch h�ng nh�ng kh�ng th?y t?, 1: kh�ng t?m th?y kh�ch h�ng
	@Basic
	@Column(name = "TRACING_RESULT")
	private Integer tracingResult;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	/** begin; vuongmq; 25/04/2015; them cac cot moi */
	@Basic
	@Column(name = "STOCK_NAME", length = 250)
	private String stockName;
	
	@Basic
	@Column(name = "PHONE", length = 30)
	private String phone;
	
	@Basic
	@Column(name = "ADDRESS", length = 250)
	private String address;
	
	@Basic
	@Column(name = "PRICE_ACTUALLY", length = 10)
	private BigDecimal priceActually;
	
	//Ngay duyet bien ban
	@Basic
	@Column(name = "APPROVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approveDate;
	/** end; vuongmq; 25/04/2015; them cac cot moi */
	@Transient
	private String lostDateStrByLostDate;
	
	@Transient
	private String lasdByLastArisingSalesDate;

	public EquipLostRecord clone() {
		EquipLostRecord obj = new EquipLostRecord();
		obj.setCode(code);
		obj.setConclusion(conclusion);
		obj.setCreateDate(createDate);
		obj.setCreateUser(createUser);
		obj.setDeliveryStatus(deliveryStatus);
		obj.setEquip(equip);
		obj.setId(id);
		obj.setEquipPeriod(equipPeriod);
		obj.setLastArisingSalesDate(lastArisingSalesDate);
		obj.setLostDate(lostDate);
		obj.setRecommendedTreatment(recommendedTreatment);
		obj.setRecordStatus(recordStatus);
		obj.setStaff(staff);
		obj.setRepresentor(representor);
		obj.setStockCode(stockCode);
		//obj.setCustomer(customer);
		obj.setStockId(stockId);
		obj.setStockType(stockType);
		obj.setTracingPlace(tracingPlace);
		obj.setTracingResult(tracingResult);
		obj.setUpdateDate(updateDate);
		obj.setUpdateUser(updateUser);
		obj.setEquipDeliveryRecord(equipDeliveryRecord);
		//
		obj.setStockName(stockName);
		obj.setPhone(phone);
		obj.setAddress(address);
		obj.setPriceActually(priceActually);
		obj.setApproveDate(approveDate);
		obj.setCreateFormDate(createFormDate);
		obj.setNote(note);
		return obj;
	}
	
	public String getLostDateStrByLostDate() {
		if (lostDate != null) {
			SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
			lostDateStrByLostDate = dt.format(lostDate);			
		}
		return lostDateStrByLostDate;
	}

	public void setLostDateStrByLostDate(String lostDateStrByLostDate) {
		this.lostDateStrByLostDate = lostDateStrByLostDate;
	}

	public String getLasdByLastArisingSalesDate() {
		if (lastArisingSalesDate != null) {
			SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
			lasdByLastArisingSalesDate = dt.format(lastArisingSalesDate);			
		}
		return lasdByLastArisingSalesDate;
	}

	public void setLasdByLastArisingSalesDate(String lasdByLastArisingSalesDate) {
		this.lasdByLastArisingSalesDate = lasdByLastArisingSalesDate;
	}

	/**
	 * Khai bao Getter/Setter
	 * */
	
	public Integer getTracingResult() {
		return tracingResult;
	}

	public EquipDeliveryRecord getEquipDeliveryRecord() {
		return equipDeliveryRecord;
	}

	public void setEquipDeliveryRecord(EquipDeliveryRecord equipDeliveryRecord) {
		this.equipDeliveryRecord = equipDeliveryRecord;
	}

	public void setTracingResult(Integer tracingResult) {
		this.tracingResult = tracingResult;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getConclusion() {
		return conclusion;
	}

	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Equipment getEquip() {
		return equip;
	}

	public void setEquip(Equipment equip) {
		this.equip = equip;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getLastArisingSalesDate() {
		return lastArisingSalesDate;
	}

	public void setLastArisingSalesDate(Date lastArisingSalesDate) {
		this.lastArisingSalesDate = lastArisingSalesDate;
	}

	public Date getLostDate() {
		return lostDate;
	}

	public void setLostDate(Date lostDate) {
		this.lostDate = lostDate;
	}

	public String getRecommendedTreatment() {
		return recommendedTreatment;
	}

	public void setRecommendedTreatment(String recommendedTreatment) {
		this.recommendedTreatment = recommendedTreatment;
	}

	public DeliveryType getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(DeliveryType deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public StatusRecordsEquip getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(StatusRecordsEquip recordStatus) {
		this.recordStatus = recordStatus;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getRepresentor() {
		return representor;
	}

	public void setRepresentor(String representor) {
		this.representor = representor;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	
	public EquipStockTotalType getStockType() {
		return stockType;
	}

	public void setStockType(EquipStockTotalType stockType) {
		this.stockType = stockType;
	}

	public Integer getTracingPlace() {
		return tracingPlace;
	}

	public void setTracingPlace(Integer tracingPlace) {
		this.tracingPlace = tracingPlace;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public EquipPeriod getEquipPeriod() {
		return equipPeriod;
	}

	public void setEquipPeriod(EquipPeriod equipPeriod) {
		this.equipPeriod = equipPeriod;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BigDecimal getPriceActually() {
		return priceActually;
	}

	public void setPriceActually(BigDecimal priceActually) {
		this.priceActually = priceActually;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public Date getCreateFormDate() {
		return createFormDate;
	}

	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
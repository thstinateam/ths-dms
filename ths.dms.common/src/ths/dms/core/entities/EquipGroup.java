package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_GROUP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_GROUP_SEQ", allocationSize = 1)
public class EquipGroup implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// khoa chinh
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_GROUP_ID")
	private Long id;
	
	//ma nhom thiet bi
	@Basic
	@Column(name = "CODE", length = 400)
	private String code;
	
	//ten nhom thiet bi
	@Basic
	@Column(name = "NAME", length = 400)
	private String name;

	//ten nhom thiet bi
	@Basic
	@Column(name = "NAME_TEXT", length = 400)
	private String nameText;

	// dung tich (lit)
	@Basic
	@Column(name = "CAPACITY_FROM", length = 22)
	private BigDecimal capacityFrom;
	
	// dung tich (lit)
	@Basic
	@Column(name = "CAPACITY_TO", length = 22)
	private BigDecimal capacityTo;

	// ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	// nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	@Basic
	@Column(name = "BRAND_NAME", length = 200)
	private String equipBrandName;

	// loai nhom thiet bi
	@ManyToOne(targetEntity = EquipCategory.class)
	@JoinColumn(name = "EQUIP_CATEGORY_ID", referencedColumnName = "EQUIP_CATEGORY_ID")
	private EquipCategory equipCategory;

	// trang thai: 0: off; 1: on
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;

	// ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	// nguoi cao nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	/**
	 * Khai bao phuong thuc GETTER/SETETR
	 * */

	public BigDecimal getCapacityFrom() {
		return capacityFrom;
	}

	public BigDecimal getCapacityTo() {
		return capacityTo;
	}

	public void setCapacityFrom(BigDecimal capacityFrom) {
		this.capacityFrom = capacityFrom;
	}

	public void setCapacityTo(BigDecimal capacityTo) {
		this.capacityTo = capacityTo;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public EquipCategory getEquipCategory() {
		return equipCategory;
	}

	public void setEquipCategory(EquipCategory equipCategory) {
		this.equipCategory = equipCategory;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameText() {
		return nameText;
	}

	public void setNameText(String nameText) {
		this.nameText = nameText;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getEquipBrandName() {
		return equipBrandName;
	}

	public void setEquipBrandName(String equipBrandName) {
		this.equipBrandName = equipBrandName;
	}
}
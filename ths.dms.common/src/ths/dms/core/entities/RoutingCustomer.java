package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.RoutingCustomerType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "ROUTING_CUSTOMER")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "ROUTING_CUSTOMER_SEQ", allocationSize = 1)
public class RoutingCustomer implements Serializable {

	private static final long serialVersionUID = 1L;
	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 40)
	private String createUser;

	//id khach hang
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	//0: ko di , 1: di
	@Basic
	@Column(name = "FRIDAY", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RoutingCustomerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RoutingCustomerType friday;

	//0: ko di , 1: di
	@Basic
	@Column(name = "MONDAY", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RoutingCustomerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RoutingCustomerType monday;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "ROUTING_CUSTOMER_ID")
	private Long id;

	//id tuyen
	@ManyToOne(targetEntity = Routing.class)
	@JoinColumn(name = "ROUTING_ID", referencedColumnName = "ROUTING_ID")
	private Routing routing;

	//0: ko di , 1: di
	@Basic
	@Column(name = "SATURDAY", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RoutingCustomerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RoutingCustomerType saturday;

	//so thu tu trong tuyen
	@Basic
	@Column(name = "SEQ", length = 22)
	private Integer seq;

	//0: khong hoat dong, 1: hoat dong
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status;

	//0: ko di , 1: di
	@Basic
	@Column(name = "SUNDAY", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RoutingCustomerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RoutingCustomerType sunday;

	//0: ko di , 1: di
	@Basic
	@Column(name = "THURSDAY", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RoutingCustomerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RoutingCustomerType thursday;

	//0: ko di , 1: di
	@Basic
	@Column(name = "TUESDAY", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RoutingCustomerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RoutingCustomerType tuesday;

	//0: ko di , 1: di
	@Basic
	@Column(name = "WEEK1", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RoutingCustomerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RoutingCustomerType week1;
	
	//0: ko di , 1: di
	@Basic
	@Column(name = "WEEK2", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RoutingCustomerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RoutingCustomerType week2;
	
	//0: ko di , 1: di
	@Basic
	@Column(name = "WEEK3", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RoutingCustomerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RoutingCustomerType week3;
	
	//0: ko di , 1: di
	@Basic
	@Column(name = "WEEK4", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RoutingCustomerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RoutingCustomerType week4;
	
	//tan suat
	@Basic
	@Column(name = "FREQUENCY", length = 2)
	private Integer frequency;
	
	public RoutingCustomerType getWeek1() {
		return week1;
	}

	public void setWeek1(RoutingCustomerType week1) {
		this.week1 = week1;
	}

	public RoutingCustomerType getWeek2() {
		return week2;
	}

	public void setWeek2(RoutingCustomerType week2) {
		this.week2 = week2;
	}

	public RoutingCustomerType getWeek3() {
		return week3;
	}

	public void setWeek3(RoutingCustomerType week3) {
		this.week3 = week3;
	}

	public RoutingCustomerType getWeek4() {
		return week4;
	}

	public void setWeek4(RoutingCustomerType week4) {
		this.week4 = week4;
	}

	//ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 40)
	private String updateUser;

	//0: ko di , 1: di
	@Basic
	@Column(name = "WEDNESDAY", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RoutingCustomerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RoutingCustomerType wednesday;

	//Khoang delay giua cac tuan (chua dung)
	@Basic
	@Column(name = "WEEK_INTERVAL", length = 22)
	private Integer weekInterval;
	
	@Basic
	@Column(name = "LAST_APPROVE_ORDER")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastApproveOrder;
	
	@Basic
	@Column(name = "LAST_ORDER")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastOrder;
	
	@Basic
	@Column(name = "PLAN_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date planDate;
	
	@Basic
	@Column(name = "PLAN_AVG_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date planAvgDate;
	
	@Basic
	@Column(name = "DAY_PLAN", length = 20)
	private BigDecimal dayPlan;
	
	@Basic
	@Column(name = "DAY_PLAN_AVG", length = 20)
	private BigDecimal dayPlanAvg;
	
	@Basic
	@Column(name = "SEQ2", length = 4)
	private Integer seq2;
	
	@Basic
	@Column(name = "SEQ3", length = 4)
	private Integer seq3;
	
	@Basic
	@Column(name = "SEQ4", length = 4)
	private Integer seq4;
	
	@Basic
	@Column(name = "SEQ5", length = 4)
	private Integer seq5;
	
	@Basic
	@Column(name = "SEQ6", length = 4)
	private Integer seq6;
	
	@Basic
	@Column(name = "SEQ7", length = 4)
	private Integer seq7;
	
	@Basic
	@Column(name = "SEQ8", length = 4)
	private Integer seq8;

	@Basic
	@Column(name = "START_WEEK", length = 2)
	private Integer startWeek;
	
	@Basic
	@Column(name = "START_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Basic
	@Column(name = "END_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the createUser
	 */
	public String getCreateUser() {
		return createUser;
	}

	/**
	 * @param createUser the createUser to set
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the friday
	 */
	public RoutingCustomerType getFriday() {
		return friday;
	}

	/**
	 * @param friday the friday to set
	 */
	public void setFriday(RoutingCustomerType friday) {
		this.friday = friday;
	}

	/**
	 * @return the monday
	 */
	public RoutingCustomerType getMonday() {
		return monday;
	}

	/**
	 * @param monday the monday to set
	 */
	public void setMonday(RoutingCustomerType monday) {
		this.monday = monday;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the routing
	 */
	public Routing getRouting() {
		return routing;
	}

	/**
	 * @param routing the routing to set
	 */
	public void setRouting(Routing routing) {
		this.routing = routing;
	}

	/**
	 * @return the saturday
	 */
	public RoutingCustomerType getSaturday() {
		return saturday;
	}

	/**
	 * @param saturday the saturday to set
	 */
	public void setSaturday(RoutingCustomerType saturday) {
		this.saturday = saturday;
	}

	/**
	 * @return the seq
	 */
	public Integer getSeq() {
		return seq;
	}

	/**
	 * @param seq the seq to set
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	/**
	 * @return the status
	 */
	public ActiveType getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(ActiveType status) {
		this.status = status;
	}

	/**
	 * @return the sunday
	 */
	public RoutingCustomerType getSunday() {
		return sunday;
	}

	/**
	 * @param sunday the sunday to set
	 */
	public void setSunday(RoutingCustomerType sunday) {
		this.sunday = sunday;
	}

	/**
	 * @return the thursday
	 */
	public RoutingCustomerType getThursday() {
		return thursday;
	}

	/**
	 * @param thursday the thursday to set
	 */
	public void setThursday(RoutingCustomerType thursday) {
		this.thursday = thursday;
	}

	/**
	 * @return the tuesday
	 */
	public RoutingCustomerType getTuesday() {
		return tuesday;
	}

	/**
	 * @param tuesday the tuesday to set
	 */
	public void setTuesday(RoutingCustomerType tuesday) {
		this.tuesday = tuesday;
	}

	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the updateUser
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * @param updateUser the updateUser to set
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * @return the wednesday
	 */
	public RoutingCustomerType getWednesday() {
		return wednesday;
	}

	/**
	 * @param wednesday the wednesday to set
	 */
	public void setWednesday(RoutingCustomerType wednesday) {
		this.wednesday = wednesday;
	}

	/**
	 * @return the weekInterval
	 */
	public Integer getWeekInterval() {
		return weekInterval;
	}

	/**
	 * @param weekInterval the weekInterval to set
	 */
	public void setWeekInterval(Integer weekInterval) {
		this.weekInterval = weekInterval;
	}

	public Integer getStartWeek() {
		return startWeek;
	}

	public void setStartWeek(Integer startWeek) {
		this.startWeek = startWeek;
	}

	public Date getLastApproveOrder() {
		return lastApproveOrder;
	}

	public void setLastApproveOrder(Date lastApproveOrder) {
		this.lastApproveOrder = lastApproveOrder;
	}

	public Date getLastOrder() {
		return lastOrder;
	}

	public void setLastOrder(Date lastOrder) {
		this.lastOrder = lastOrder;
	}

	public Date getPlanDate() {
		return planDate;
	}

	public void setPlanDate(Date planDate) {
		this.planDate = planDate;
	}

	public Date getPlanAvgDate() {
		return planAvgDate;
	}

	public void setPlanAvgDate(Date planAvgDate) {
		this.planAvgDate = planAvgDate;
	}

	public BigDecimal getDayPlan() {
		return dayPlan;
	}

	public void setDayPlan(BigDecimal dayPlan) {
		this.dayPlan = dayPlan;
	}

	public BigDecimal getDayPlanAvg() {
		return dayPlanAvg;
	}

	public void setDayPlanAvg(BigDecimal dayPlanAvg) {
		this.dayPlanAvg = dayPlanAvg;
	}

	public Integer getSeq2() {
		return seq2;
	}

	public void setSeq2(Integer seq2) {
		this.seq2 = seq2;
	}

	public Integer getSeq3() {
		return seq3;
	}

	public void setSeq3(Integer seq3) {
		this.seq3 = seq3;
	}

	public Integer getSeq4() {
		return seq4;
	}

	public void setSeq4(Integer seq4) {
		this.seq4 = seq4;
	}

	public Integer getSeq5() {
		return seq5;
	}

	public void setSeq5(Integer seq5) {
		this.seq5 = seq5;
	}

	public Integer getSeq6() {
		return seq6;
	}

	public void setSeq6(Integer seq6) {
		this.seq6 = seq6;
	}

	public Integer getSeq7() {
		return seq7;
	}

	public void setSeq7(Integer seq7) {
		this.seq7 = seq7;
	}

	public Integer getSeq8() {
		return seq8;
	}

	public void setSeq8(Integer seq8) {
		this.seq8 = seq8;
	}
	
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}
}
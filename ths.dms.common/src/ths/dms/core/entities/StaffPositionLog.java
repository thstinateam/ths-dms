package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * 
 * Auto generate entities - HaNoi standard
 * 
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "STAFF_POSITION_LOG")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "STAFF_POSITION_LOG_SEQ", allocationSize = 1)
public class StaffPositionLog implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "ACCURACY", length = 22)
	private Double accuracy;

	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "LAT", length = 22)
	private Double lat;

	@Basic
	@Column(name = "LNG", length = 22)
	private Double lng;

	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	@Id
	@Basic
	@Column(name = "STAFF_POSITION_LOG_ID", length = 22)
	private Long id;

	/** vuongmq; 17/08/2015; lay them thong tin wifi cua staff**/
	@Basic
	@Column(name = "PIN", length = 5)
	private Float pin;
	
	@Basic
	@Column(name = "NETWORK_TYPE")
	private String networkType;	
	
	@Basic
	@Column(name = "SPEED")
	private Integer networkSpeed;	
	
	@Transient
	private Integer pinRound;
	
	public StaffPositionLog clone() {
		StaffPositionLog obj = new StaffPositionLog();

		obj.setAccuracy(accuracy);
		obj.setCreateDate(createDate);
		obj.setLat(lat);
		obj.setLng(lng);
		obj.setStaff(staff);
		obj.setId(id);
		obj.setPin(pin);
		obj.setNetworkType(networkType);
		obj.setNetworkSpeed(networkSpeed);
		obj.setPinRound(pinRound);
		return obj;
	}

	public Double getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(Double accuracy) {
		this.accuracy = accuracy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getPin() {
		return pin;
	}

	public void setPin(Float pin) {
		this.pin = pin;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public Integer getNetworkSpeed() {
		return networkSpeed;
	}

	public void setNetworkSpeed(Integer networkSpeed) {
		this.networkSpeed = networkSpeed;
	}

	public Integer getPinRound() {
		if (pin != null) {
			return Math.round(pin);
		}
		return pinRound;
	}

	public void setPinRound(Integer pinRound) {
		this.pinRound = pinRound;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
}
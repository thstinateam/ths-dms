/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActionSaleOrder;
import ths.dms.core.entities.enumtype.SaleOrderSource;

/**
 * Mo ta class PromotionMapDelta.java
 * @author vuongmq
 * @since Jan 8, 2016
 */
@Entity
@Table(name = "PROMOTION_MAP_DELTA")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PROMOTION_MAP_DELTA_SEQ", allocationSize = 1)
public class PromotionMapDelta implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PROMOTION_MAP_DELTA_ID")
	private Long id;
	
	//id ctkm
	@ManyToOne(targetEntity = PromotionProgram.class)
	@JoinColumn(name = "PROMOTION_PROGRAM_ID", referencedColumnName = "PROMOTION_PROGRAM_ID")
	private PromotionProgram promotionProgram;
	
	//id PromotionShopMap
	@ManyToOne(targetEntity = PromotionShopMap.class)
	@JoinColumn(name = "PROMOTION_SHOP_MAP_ID", referencedColumnName = "PROMOTION_SHOP_MAP_ID")
	private PromotionShopMap promotionShopMap;
	
	//id PromotionStaffMap
	@ManyToOne(targetEntity = PromotionStaffMap.class)
	@JoinColumn(name = "PROMOTION_STAFF_MAP_ID", referencedColumnName = "PROMOTION_STAFF_MAP_ID")
	private PromotionStaffMap promotionStaffMap;
	
	//id PromotionCustomerMap
	@ManyToOne(targetEntity = PromotionCustomerMap.class)
	@JoinColumn(name = "PROMOTION_CUSTOMER_MAP_ID", referencedColumnName = "PROMOTION_CUSTOMER_MAP_ID")
	private PromotionCustomerMap promotionCustomerMap;
	
	//id npp tham gia km
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	//id staff_id
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	//id customer_id
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;
	
	//so suat
	@Basic
	@Column(name = "QUANTITY_DELTA", length = 22)
	private Integer quantityDelta;
	
	//so tien
	@Basic
	@Column(name = "AMOUNT_DELTA", length = 26)
	private BigDecimal amountDelta;
	
	//so luong
	@Basic
	@Column(name = "NUM_DELTA", length = 22)
	private BigDecimal numDelta;
	
	//id saleOrder
	@Basic
	@Column(name = "FROM_OBJECT_ID", length = 22)
	private Long fromObjectId;
	
	//0: khong loi, 1 loi
	@Basic
	@Column(name = "ERROR", length = 1)
	private Integer error;
	
	// errorMessage
	@Basic
	@Column(name = "ERROR_MESSAGE", length = 100)
	private String errorMessage;
	
	//trang thai
	@Basic
	@Column(name = "SOURCE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.SaleOrderSource"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private SaleOrderSource source;
	
	// action
	@Basic
	@Column(name = "ACTION", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActionSaleOrder"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActionSaleOrder action;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PromotionProgram getPromotionProgram() {
		return promotionProgram;
	}

	public void setPromotionProgram(PromotionProgram promotionProgram) {
		this.promotionProgram = promotionProgram;
	}

	public PromotionShopMap getPromotionShopMap() {
		return promotionShopMap;
	}

	public void setPromotionShopMap(PromotionShopMap promotionShopMap) {
		this.promotionShopMap = promotionShopMap;
	}

	public PromotionStaffMap getPromotionStaffMap() {
		return promotionStaffMap;
	}

	public void setPromotionStaffMap(PromotionStaffMap promotionStaffMap) {
		this.promotionStaffMap = promotionStaffMap;
	}

	public PromotionCustomerMap getPromotionCustomerMap() {
		return promotionCustomerMap;
	}

	public void setPromotionCustomerMap(PromotionCustomerMap promotionCustomerMap) {
		this.promotionCustomerMap = promotionCustomerMap;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Integer getQuantityDelta() {
		return quantityDelta;
	}

	public void setQuantityDelta(Integer quantityDelta) {
		this.quantityDelta = quantityDelta;
	}

	public BigDecimal getAmountDelta() {
		return amountDelta;
	}

	public void setAmountDelta(BigDecimal amountDelta) {
		this.amountDelta = amountDelta;
	}

	public BigDecimal getNumDelta() {
		return numDelta;
	}

	public void setNumDelta(BigDecimal numDelta) {
		this.numDelta = numDelta;
	}

	public Long getFromObjectId() {
		return fromObjectId;
	}

	public void setFromObjectId(Long fromObjectId) {
		this.fromObjectId = fromObjectId;
	}

	public Integer getError() {
		return error;
	}

	public void setError(Integer error) {
		this.error = error;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public SaleOrderSource getSource() {
		return source;
	}

	public void setSource(SaleOrderSource source) {
		this.source = source;
	}

	public ActionSaleOrder getAction() {
		return action;
	}

	public void setAction(ActionSaleOrder action) {
		this.action = action;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

}
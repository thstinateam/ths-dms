package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;


/**
 * The persistent class for the CUSTOMER_DISPLAY_PROGRAME database table.
 * 
 */
@Entity
@Table(name="DISPLAY_CUSTOMER_MAP")
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "DISPLAY_CUSTOMER_MAP_SEQ", allocationSize = 1)
public class CustomerDisplayPrograme implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name="DISPLAY_CUSTOMER_MAP_ID")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATE_DATE")
	private Date createDate;

	@Column(name="CREATE_USER")
	private String createUser;

	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	@Column(name="DISPLAY_PROGRAM_CODE")
	private String displayProgramCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FROM_DATE")
	private Date fromDate;

	@Column(name="LEVEL_CODE")
	private String levelCode;

	//Trang thai: 0:ko hoat dong,1: hoat dong
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

    @Temporal( TemporalType.DATE)
	@Column(name="TO_DATE")
	private Date toDate;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DATE")
	private Date updateDate;

	@Column(name="UPDATE_USER")
	private String updateUser;

	//bi-directional many-to-one association to Staff
    @ManyToOne(targetEntity = StDisplayStaffMap.class)
	@JoinColumn(name="DISPLAY_STAFF_MAP_ID" ,referencedColumnName = "DISPLAY_STAFF_MAP_ID")
	private StDisplayStaffMap stDisplayStaffMap;
    
    @ManyToOne(targetEntity = StDisplayProgramLevel.class)
	@JoinColumn(name="DISPLAY_PROGRAM_LEVEL_ID",referencedColumnName = "DISPLAY_PROGRAM_LEVEL_ID")
	private StDisplayProgramLevel stDisplayProgramLevel;
    
    @ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name="STAFF_ID")
	private Staff staff;
    
    @Column(name="IS_NOT_MIGRATE")
    private Integer isNotMigrate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}


	public String getDisplayProgramCode() {
		return displayProgramCode;
	}

	public void setDisplayProgramCode(String displayProgramCode) {
		this.displayProgramCode = displayProgramCode;
	}

	

	public StDisplayStaffMap getStDisplayStaffMap() {
		return stDisplayStaffMap;
	}

	public void setStDisplayStaffMap(StDisplayStaffMap stDisplayStaffMap) {
		this.stDisplayStaffMap = stDisplayStaffMap;
	}

	public StDisplayProgramLevel getStDisplayProgramLevel() {
		return stDisplayProgramLevel;
	}

	public void setStDisplayProgramLevel(StDisplayProgramLevel stDisplayProgramLevel) {
		this.stDisplayProgramLevel = stDisplayProgramLevel;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public String getLevelCode() {
		return levelCode;
	}

	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Integer getIsNotMigrate() {
		return isNotMigrate;
	}

	public void setIsNotMigrate(Integer isNotMigrate) {
		this.isNotMigrate = isNotMigrate;
	}
}
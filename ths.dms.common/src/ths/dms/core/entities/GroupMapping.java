package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "GROUP_MAPPING")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_GROUP_MAPPING", sequenceName = "GROUP_MAPPING_SEQ", allocationSize = 1)
public class GroupMapping implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GROUP_MAPPING")
	@Column(name = "GROUP_MAPPING_ID")
	private Long id;
	
	@ManyToOne(targetEntity = ProductGroup.class)
	@JoinColumn(name = "SALE_GROUP_ID", referencedColumnName = "PRODUCT_GROUP_ID")
	private ProductGroup saleGroup;
	
	@ManyToOne(targetEntity = GroupLevel.class)
	@JoinColumn(name = "SALE_GROUP_LEVEL_ID", referencedColumnName = "GROUP_LEVEL_ID")
	private GroupLevel saleGroupLevel;
	
	@ManyToOne(targetEntity = ProductGroup.class)
	@JoinColumn(name = "PROMO_GROUP_ID", referencedColumnName = "PRODUCT_GROUP_ID")
	private ProductGroup promotionGroup;
	
	@ManyToOne(targetEntity = GroupLevel.class)
	@JoinColumn(name = "PROMO_GROUP_LEVEL_ID", referencedColumnName = "GROUP_LEVEL_ID")
	private GroupLevel promotionGroupLevel;
	
	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductGroup getSaleGroup() {
		return saleGroup;
	}

	public void setSaleGroup(ProductGroup saleGroup) {
		this.saleGroup = saleGroup;
	}

	public GroupLevel getSaleGroupLevel() {
		return saleGroupLevel;
	}

	public void setSaleGroupLevel(GroupLevel saleGroupLevel) {
		this.saleGroupLevel = saleGroupLevel;
	}

	public ProductGroup getPromotionGroup() {
		return promotionGroup;
	}

	public void setPromotionGroup(ProductGroup promotionGroup) {
		this.promotionGroup = promotionGroup;
	}

	public GroupLevel getPromotionGroupLevel() {
		return promotionGroupLevel;
	}

	public void setPromotionGroupLevel(GroupLevel promotionGroupLevel) {
		this.promotionGroupLevel = promotionGroupLevel;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 * 
 * 
 * @author loctt
 * @since 17/9/2013
 * 
 */

@Entity
@Table(name = "DISPLAY_PROGRAM_EXCLUSION")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "DISPLAY_PROGRAM_EXCLUSION_SEQ", allocationSize = 1)
public class StDisplayProgramExclusion implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "DISPLAY_PROGRAM_EXCLUSION_ID")
	private Long id;

	@ManyToOne(targetEntity = StDisplayProgram.class)
	@JoinColumn(name = "DISPLAY_PROGRAM_ID", referencedColumnName = "DISPLAY_PROGRAM_ID")
	private StDisplayProgram displayProgram;

	@ManyToOne(targetEntity = StDisplayProgram.class)
	@JoinColumn(name = "DISPLAY_P_EXCLUSION_ID", referencedColumnName = "DISPLAY_PROGRAM_ID")
	private StDisplayProgram exDisplayProgram;

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Basic
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	public StDisplayProgramExclusion clone() {
		StDisplayProgramExclusion ret = new StDisplayProgramExclusion();
		ret.setId(id);
		ret.setStatus(status);
		ret.setUpdateDate(updateDate);
		ret.setUpdateUser(updateUser);
		ret.setCreateDate(createDate);
		ret.setCreateUser(createUser);
		ret.setDisplayProgram(displayProgram);
		ret.setExDisplayProgram(exDisplayProgram);
		return ret;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public StDisplayProgram getDisplayProgram() {
		return displayProgram;
	}

	public void setDisplayProgram(StDisplayProgram displayProgram) {
		this.displayProgram = displayProgram;
	}

	public StDisplayProgram getExDisplayProgram() {
		return exDisplayProgram;
	}

	public void setExDisplayProgram(StDisplayProgram exDisplayProgram) {
		this.exDisplayProgram = exDisplayProgram;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
}
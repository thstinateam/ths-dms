package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.PlanType;
import ths.dms.core.entities.enumtype.SalePlanOwnerType;

/**
 * 
 * Auto generate entities - HaNoi standard
 * 
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "SALE_PLAN")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SALE_PLAN_SEQ", allocationSize = 1)
public class SalePlan implements Serializable {

	private static final long serialVersionUID = 1L;
	//tong tien
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//tong tien da phan xuong
	@Basic
	@Column(name = "AMOUNT_ASSIGNED", length = 22)
	private BigDecimal amountAssign;

	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 40)
	private String createUser;

	//ghi chu
	//@Basic
	//@Column(name = "DESCRIPTION", length = 600)
	//private String description;

//	//thang
//	@Basic
//	@Column(name = "MONTH_DATE", columnDefinition = "timestamp(9) default systimestamp")
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date monthDate;

	//id doi tuong
	@Basic
	@Column(name = "OBJECT_ID", length = 22)
	private Long objectId;

	//loai doi tuong: 0 VNM,1: mien,  2: vung, 3 NPP, 4 NVBH
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.SalePlanOwnerType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private SalePlanOwnerType objectType;

	//id cha
	//@ManyToOne(targetEntity = Shop.class)
	//@JoinColumn(name = "PARENT_ID", referencedColumnName = "SHOP_ID")
	//private Shop parent;

	//Shop_Id
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "CAT_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo cat;

	//gia
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal price;

	//id san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//so luong
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;

	//so da phan xuong duoi
	@Basic
	@Column(name = "QUANTITY_ASSIGNED", length = 22)
	private Integer quantityAssign;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SALE_PLAN_ID")
	private Long id;

	//loai ke hoach 1: nam ,2 : thang
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PlanType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PlanType type;
	
	//id chu ky
	@ManyToOne(targetEntity = Cycle.class)
	@JoinColumn(name = "CYCLE_ID", referencedColumnName = "CYCLE_ID")
	private Cycle cycle;

	//version hien tai
	//@Basic
	//@Column(name = "VERSION", length = 22)
	//private Integer version;

	//nam
	//@Basic
	//@Column(name = "YEAR", length = 22)
	//private Integer year;

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public BigDecimal getAmount() {
		return amount; //return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getAmountAssign() {
		return amountAssign == null ? BigDecimal.valueOf(0) : amountAssign;
	}

	public void setAmountAssign(BigDecimal amountAssign) {
		this.amountAssign = amountAssign;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	/*public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}*/

//	public Date getMonthDate() {
//		return monthDate;
//	}
//
//	public void setMonthDate(Date month) {
//		this.monthDate = month;
//	}

	/*public Shop getParent() {
		return parent;
	}

	public void setParent(Shop parent) {
		this.parent = parent;
	}*/

	public BigDecimal getPrice() {
		return price == null ? BigDecimal.valueOf(0) : price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantityAssign() {
		if (quantityAssign == null) {
			quantityAssign = 0;
		}
		return quantityAssign;
	}

	public void setQuantityAssign(Integer quantityAssign) {
		this.quantityAssign = quantityAssign;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PlanType getType() {
		return type;
	}

	public void setType(PlanType type) {
		this.type = type;
	}

	/*public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}*/

	/*public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}*/

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public SalePlanOwnerType getObjectType() {
		return objectType;
	}

	public void setObjectType(SalePlanOwnerType objectType) {
		this.objectType = objectType;
	}

	public ProductInfo getCat() {
		return cat;
	}

	public void setCat(ProductInfo cat) {
		this.cat = cat;
	}

	public Cycle getCycle() {
		return cycle;
	}

	public void setCycle(Cycle cycle) {
		this.cycle = cycle;
	}

}
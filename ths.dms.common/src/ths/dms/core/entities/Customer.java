package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.IsAttribute;

/**
 * 
 * Auto generate entities - HaNoi standard
 * 
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "CUSTOMER")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "CUSTOMER_SEQ", allocationSize = 1)
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;
	//dia chi
	@Basic
	@Column(name = "ADDRESS", length = 400)//1200)
	private String address;

	//id dia ban
	@ManyToOne(targetEntity = Area.class)
	@JoinColumn(name = "AREA_ID", referencedColumnName = "AREA_ID")
	private Area area;

	//id NV thu tien
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "CASHIER_STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff cashierStaff;

	//id NV giao hang
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "DELIVER_ID", referencedColumnName = "STAFF_ID")
	private Staff deliver;

	//loai KH - tham chieu sang bang channel_type
	@ManyToOne(targetEntity = ChannelType.class)
	@JoinColumn(name = "CHANNEL_TYPE_ID", referencedColumnName = "CHANNEL_TYPE_ID")
	private ChannelType channelType;

	//ten lien he
	@Basic
	@Column(name = "CONTACT_NAME", length = 250)//750)
	private String contactName;

	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	//ngay bat dau hoat dong
	@Basic
	@Column(name = "OPEN_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date openDate;
	
	@Basic
	@Column(name = "CLOSE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date closeDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	@Basic
	@Column(name = "INVOICE_PAYMENT_TYPE", length = 50)
	private String invoicePaymentType;

	//ma KH - ban gom ma ngan + ma NPP
	@Basic
	@Column(name = "CUSTOMER_CODE", length = 40)
	private String customerCode;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "CUSTOMER_ID")
	private Long id;
	
	//ten KH
	@Basic
	@Column(name = "CUSTOMER_NAME", length = 250)// 750)
	private String customerName;

	//dia chi giao hàng
	@Basic
	@Column(name = "DELIVERY_ADDRESS", length = 250)//750)
	private String deliveryAddress;

	//email kh
	@Basic
	@Column(name = "EMAIL", length = 50)
	private String email;
	
	//email kh
	@Basic
	@Column(name = "FAX", length = 20)
	private String fax;
	
	//ma dau tien cua KH khi duoc tao
	@Basic
	@Column(name = "FIRST_CODE", length = 40)
	private String firstCode;

	//so nha
	@Basic
	@Column(name = "HOUSENUMBER", length = 100)// 300)
	private String housenumber;

	//thong tin anh
	@Basic
	@Column(name = "IMAGE_URL", length = 250)
	private String imageUrl;

	//ten don vi
	@Basic
	@Column(name = "INVOICE_CONPANY_NAME", length = 250)//750)
	private String invoiceConpanyName;

	//ten ngan hang
	@Basic
	@Column(name = "INVOICE_NAME_BANK", length = 250)//750)
	private String invoiceNameBank;
	
	//ten chi nhanh
	@Basic
	@Column(name = "INVOICE_NAME_BRANCH_BANK", length = 250)//750)
	private String invoiceNameBranchBank;
	
	//ten chi nhanh
	@Basic
	@Column(name = "BANK_ACCOUNT_OWNER", length = 250)
	private String bankAccountOwner;
	
	//so tai khoan
	@Basic
	@Column(name = "INVOICE_NUMBER_ACCOUNT", length = 50)
	private String invoiceNumberAccount;
	
	//ten nguoi in tren hoa don
	@Basic
	@Column(name = "INVOICE_OUTLET_NAME", length = 250)//750)
	private String invoiceOutletName;
	

	
	//MST
	@Basic
	@Column(name = "INVOICE_TAX", length = 20)
	private String invoiceTax;
	
	//ngay tao don hang thanh cong cuoi cung
	@Basic
	@Column(name = "LAST_APPROVE_ORDER", length = 7)
	private Date lastApproveOrder;

	//ngay tao don hang cuoi cung
	@Basic
	@Column(name = "LAST_ORDER", length = 7)
	private Date lastOrder;

	//do trung thanh
	//	@Basic
	//	@Column(name = "LOYALTY", length = 22)
	//	private Integer loyalty;

	//ThuatTQ Edit theo DTYC CRM
	@Basic
	@Column(name = "LOYALTY", length = 40)
	private String loyalty;

	//no toi da
	@Basic
	@Column(name = "MAX_DEBIT_AMOUNT", length = 20)//22)
	private BigDecimal maxDebitAmount;

	//so ngay no toi da
	@Basic
	@Column(name = "MAX_DEBIT_DATE", length = 3)//22)
	private Integer maxDebitDate;

	//so di dong
	@Basic
	@Column(name = "MOBIPHONE", length = 30)
	private String mobiphone;

	//so CMTND
	@Basic
	@Column(name = "IDNO", length = 20)
	private String idno;

	//so co dinh
	@Basic
	@Column(name = "PHONE", length = 30)
	private String phone;
	
	//vung
	@Basic
	@Column(name = "REGION", length = 50)
	private String region;

	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//ma ngan cua KH
	@Basic
	@Column(name = "SHORT_CODE", length = 10)
	private String shortCode;
	

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;

	//duong
	@Basic
	@Column(name = "STREET", length = 200)//600)
	private String street;
	public String getAddress() {
		return address;
	}
	
	//birthday
	@Basic
	@Column(name = "BIRTHDAY", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date birthDay;
	
	@ManyToOne(targetEntity = Routing.class)
	@JoinColumn(name = "DELIVERY_ROUTE_ID", referencedColumnName = "ROUTING_ID")
	private Routing routing;
	  
	@Basic
	@Column(name = "FREQUENCY", length = 2)
	private Integer frequency;
	
	//ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	@Basic
	@Column(name = "LAT", length = 50)
	private Double lat;

	@Basic
	@Column(name = "LNG", length = 50)
	private Double lng;

	// thong tin trung bay
	@Basic
	@Column(name = "DISPLAY", length = 50)
	private String display;
	
	// thong tin loai dia diem
	@Basic
	@Column(name = "LOCATION", length = 50)
	private String location;

	@Basic
	@Column(name = "NAME_TEXT", length = 250)
	private String nameText;
	
	@Basic
	@Column(name = "APPLY_DEBIT_LIMITED", length = 2)
	private Integer applyDebitLimited;

	//Co ra hoa don VAT hay khong
	@Basic
	@Column(name = "IS_VAT", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.IsAttribute"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private IsAttribute isVat = IsAttribute.IS;
	
	@ManyToOne(targetEntity = ApParam.class)
	@JoinColumn(name = "SALE_POSITION_ID", referencedColumnName = "AP_PARAM_ID")
	private ApParam salePosition;
	
	@Transient
	private String routes;
	
	@Transient
	private String createUserName;
		
	public void setAddress(String address) {
		this.address = address;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Staff getCashierStaff() {
		return cashierStaff;
	}

	public void setCashierStaff(Staff cashierStaff) {
		this.cashierStaff = cashierStaff;
	}

	public Staff getDeliver() {
		return deliver;
	}

	public void setDeliver(Staff deliver) {
		this.deliver = deliver;
	}

	public ChannelType getChannelType() {
		return channelType;
	}

	public void setChannelType(ChannelType channelType) {
		this.channelType = channelType;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstCode() {
		return firstCode;
	}

	public void setFirstCode(String firstCode) {
		this.firstCode = firstCode;
	}

	public String getHousenumber() {
		return housenumber;
	}

	public void setHousenumber(String housenumber) {
		this.housenumber = housenumber;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getInvoiceConpanyName() {
		return invoiceConpanyName;
	}

	public void setInvoiceConpanyName(String invoiceConpanyName) {
		this.invoiceConpanyName = invoiceConpanyName;
	}

	public String getInvoiceNameBank() {
		return invoiceNameBank;
	}

	public void setInvoiceNameBank(String invoiceNameBank) {
		this.invoiceNameBank = invoiceNameBank;
	}

	public String getInvoiceNumberAccount() {
		return invoiceNumberAccount;
	}

	public void setInvoiceNumberAccount(String invoiceNumberAccount) {
		this.invoiceNumberAccount = invoiceNumberAccount;
	}

	public String getInvoiceOutletName() {
		return invoiceOutletName;
	}

	public void setInvoiceOutletName(String invoiceOutletName) {
		this.invoiceOutletName = invoiceOutletName;
	}

	public String getInvoiceTax() {
		return invoiceTax;
	}

	public void setInvoiceTax(String invoiceTax) {
		this.invoiceTax = invoiceTax;
	}

	public Date getLastApproveOrder() {
		return lastApproveOrder;
	}

	public void setLastApproveOrder(Date lastApproveOrder) {
		this.lastApproveOrder = lastApproveOrder;
	}

	public Date getLastOrder() {
		return lastOrder;
	}

	public void setLastOrder(Date lastOrder) {
		this.lastOrder = lastOrder;
	}

	public String getLoyalty() {
		return loyalty;
	}

	public void setLoyalty(String loyalty) {
		this.loyalty = loyalty;
	}

	public BigDecimal getMaxDebitAmount() {
		return maxDebitAmount;
	}

	public void setMaxDebitAmount(BigDecimal maxDebitAmount) {
		this.maxDebitAmount = maxDebitAmount;
	}

	public Integer getMaxDebitDate() {
		return maxDebitDate;
	}

	public void setMaxDebitDate(Integer maxDebitDate) {
		this.maxDebitDate = maxDebitDate;
	}

	public String getMobiphone() {
		return mobiphone;
	}

	public void setMobiphone(String mobiphone) {
		this.mobiphone = mobiphone;
	}

	public String getIdno() {
		return idno;
	}

	public void setIdno(String idno) {
		this.idno = idno;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getInvoicePaymentType() {
		return invoicePaymentType;
	}

	public void setInvoicePaymentType(String invoicePaymentType) {
		this.invoicePaymentType = invoicePaymentType;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNameText() {
		return nameText;
	}

	public void setNameText(String nameText) {
		this.nameText = nameText;
	}

	public Integer getApplyDebitLimited() {
		return applyDebitLimited;
	}

	public void setApplyDebitLimited(Integer applyDebitLimited) {
		this.applyDebitLimited = applyDebitLimited;
	}

	public IsAttribute getIsVat() {
		return isVat;
	}

	public void setIsVat(IsAttribute isVat) {
		this.isVat = isVat;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	
	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public Routing getRouting() {
		return routing;
	}

	public void setRouting(Routing routing) {
		this.routing = routing;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public String getInvoiceNameBranchBank() {
		return invoiceNameBranchBank;
	}

	public void setInvoiceNameBranchBank(String invoiceNameBranchBank) {
		this.invoiceNameBranchBank = invoiceNameBranchBank;
	}

	public String getBankAccountOwner() {
		return bankAccountOwner;
	}

	public void setBankAccountOwner(String bankAccountOwner) {
		this.bankAccountOwner = bankAccountOwner;
	}

	public Date getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public ApParam getSalePosition() {
		return salePosition;
	}

	public void setSalePosition(ApParam salePosition) {
		this.salePosition = salePosition;
	}

	public String getRoutes() {
		return routes;
	}

	public void setRoutes(String routes) {
		this.routes = routes;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
}
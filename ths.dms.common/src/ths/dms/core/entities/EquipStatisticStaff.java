/*
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.EquipStockTotalType;
/**
* Auto generate entities - HCM standard
* 
* @author phuongvm
* @since 14/07/2015
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_STATISTIC_STAFF")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_EQUIP_STATISTIC_STAFF", sequenceName = "EQUIP_STATISTIC_STAFF_SEQ", allocationSize = 1)
public class EquipStatisticStaff implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_EQUIP_STATISTIC_STAFF")
	@Column(name = "EQUIP_STATISTIC_STAFF_ID")
	private Long id;
	
	@ManyToOne(targetEntity = EquipStatisticRecord.class)
	@JoinColumn(name = "EQUIP_STATISTIC_RECORD_ID", referencedColumnName = "EQUIP_STATISTIC_RECORD_ID")
	private EquipStatisticRecord equipStatisticRecord;
	
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	@ManyToOne(targetEntity = Equipment.class)
	@JoinColumn(name = "EQUIP_ID", referencedColumnName = "EQUIP_ID")
	private Equipment equipment;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;
	
	@Basic
	@Column(name = "STOCK_ID", length = 20)
	private Long stockId;
	
	@Basic
	@Column(name = "STOCK_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipStockTotalType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipStockTotalType stockType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipStatisticRecord getEquipStatisticRecord() {
		return equipStatisticRecord;
	}

	public void setEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord) {
		this.equipStatisticRecord = equipStatisticRecord;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public EquipStockTotalType getStockType() {
		return stockType;
	}

	public void setStockType(EquipStockTotalType stockType) {
		this.stockType = stockType;
	}

	
}
/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Entities FEEDBACK_STAFF_CUSTOMER
 * @author vuongmq
 * @since 17/11/2015
 */
@Entity
@Table(name="FEEDBACK_STAFF_CUSTOMER")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name="SEQ_STORE", sequenceName="FEEDBACK_STAFF_CUSTOMER_SEQ", allocationSize = 1)
public class FeedbackStaffCustomer implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQ_STORE")
	@Column(name="FEEDBACK_STAFF_CUSTOMER_ID")
	private Long id;
	
	//FEEDBACK_STAFF_ID voi tung loai feedback
	@ManyToOne(targetEntity = FeedbackStaff.class)
	@JoinColumn(name = "FEEDBACK_STAFF_ID", referencedColumnName = "FEEDBACK_STAFF_ID")
	private FeedbackStaff feedbackStaff;
	
	//staff_id
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	//customer_id
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;
		
	//CREATE_USER_ID
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "CREATE_USER_ID", referencedColumnName = "STAFF_ID")
	private Staff createStaff;
		
	@Basic
	@Column(name="CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
    @Column(name="UPDATE_USER", length = 100)
	private String updateUser;

	@Basic
	@Column(name="UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FeedbackStaff getFeedbackStaff() {
		return feedbackStaff;
	}

	public void setFeedbackStaff(FeedbackStaff feedbackStaff) {
		this.feedbackStaff = feedbackStaff;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Staff getCreateStaff() {
		return createStaff;
	}

	public void setCreateStaff(Staff createStaff) {
		this.createStaff = createStaff;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}

package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the MEDIA database table.
 * 
 */
@Entity
@Table(name="MEDIA")
public class Media implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MEDIA_ID_GENERATOR", sequenceName="MEDIA_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MEDIA_ID_GENERATOR")
	@Column(name="MEDIA_ID")
	private Long mediaId;
		
	@Column(name="MEDIA_CODE")
	private String mediaCode;

	@Column(name="MEDIA_NAME")
	private String mediaName;
	
	@OneToOne(targetEntity = MediaItem.class)	
	@JoinColumn(name="MEDIA_ITEM_ID", referencedColumnName="MEDIA_ITEM_ID")	
	private MediaItem mediaItemId;
		
    @Column(name="TYPE")
    private Integer type;
    
    @Column(name="STATUS")
    private Integer status;

	@Column(name="CREATE_USER")
	private String createUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CREATE_DATE")
	private Date createDate;

    @Column(name="UPDATE_USER")
	private String updateUser;
	
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DATE")
	private Date updateDate;

	public Long getMediaId() {
		return mediaId;
	}

	public void setMediaId(Long mediaId) {
		this.mediaId = mediaId;
	}

	public String getMediaCode() {
		return mediaCode;
	}

	public void setMediaCode(String mediaCode) {
		this.mediaCode = mediaCode;
	}

	public String getMediaName() {
		return mediaName;
	}

	public void setMediaName(String mediaName) {
		this.mediaName = mediaName;
	}

	

	public MediaItem getMediaItemId() {
		return mediaItemId;
	}

	public void setMediaItemId(MediaItem mediaItemId) {
		this.mediaItemId = mediaItemId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
}
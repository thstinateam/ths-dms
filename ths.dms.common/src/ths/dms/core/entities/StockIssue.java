package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "STOCK_ISSUE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "STOCK_ISSUE_SEQ", allocationSize = 1)
public class StockIssue implements Serializable {

	private static final long serialVersionUID = 1L;

	//V? vi?c
	@Basic
	@Column(name = "ABOUT", length = 2000)
	private String about;

	//Ph��ng ti?n v?n chuy?n
	@Basic
	@Column(name = "CAR", length = 400)
	private String car;

	//S? h?p �?ng
	@Basic
	@Column(name = "CONTRACT_NUMBER", length = 400)
	private String contractNumber;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//Kho xu?t
	@Basic
	@Column(name = "FROM_OWNER_STOCK", length = 200)
	private String fromOwnerStock;

	//1. ��n b�n (DB), 2. Phi?u xu?t kho �i?u ch?nh, 3. Phi?u �i?u chuy?n kho, 4. ��n tr? h�ng VNM
	@Basic
	@Column(name = "ISSUE_TYPE")
	private Integer issueType;

	//S? ch?ng t?
	@Basic
	@Column(name = "ORDER_NUMBER", length = 160)
	private String orderNumber;

	//L? do h?y
	@Basic
	@Column(name = "REASON", length = 1000)
	private String reason;

	@Basic
	@Column(name = "STAFF_CODE", length = 200)
	private String staffCode;

	//Nh�n vi�n v?n chuy?n
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	@Basic
	@Column(name = "STAFF_NAME", length = 200)
	private String staffName;

	//1. Ch�a h?y, 0. H?y
	@Basic
	@Column(name = "STATUS", length = 22)
	private BigDecimal status;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "STOCK_ISSUE_ID")
	private Long id;

	//S? phi?u
	@Basic
	@Column(name = "STOCK_ISSUE_NUMBER", length = 200)
	private String stockIssueNumber;

	@Basic
	@Column(name = "TIME_PRINTED")
	private Integer timePrinted;

	//Kho nh?p
	@Basic
	@Column(name = "TO_OWNER_STOCK", length = 200)
	private String toOwnerStock;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getCar() {
		return car;
	}

	public void setCar(String car) {
		this.car = car;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getFromOwnerStock() {
		return fromOwnerStock;
	}

	public void setFromOwnerStock(String fromOwnerStock) {
		this.fromOwnerStock = fromOwnerStock;
	}

	public Integer getIssueType() {
		return issueType;
	}

	public void setIssueType(Integer issueType) {
		this.issueType = issueType;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public BigDecimal getStatus() {
		return status;
	}

	public void setStatus(BigDecimal status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStockIssueNumber() {
		return stockIssueNumber;
	}

	public void setStockIssueNumber(String stockIssueNumber) {
		this.stockIssueNumber = stockIssueNumber;
	}

	public Integer getTimePrinted() {
		return timePrinted;
	}

	public void setTimePrinted(Integer timePrinted) {
		this.timePrinted = timePrinted;
	}

	public String getToOwnerStock() {
		return toOwnerStock;
	}

	public void setToOwnerStock(String toOwnerStock) {
		this.toOwnerStock = toOwnerStock;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
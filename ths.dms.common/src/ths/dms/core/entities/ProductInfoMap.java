package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ProductInfoMapType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PRODUCT_INFO_MAP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PRODUCT_INFO_MAP_SEQ", allocationSize = 1)
public class ProductInfoMap implements Serializable {

	private static final long serialVersionUID = 1L;
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PRODUCT_INFO_MAP_ID")
	private Long id;
	
	//ma nganh hang
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "FROM_OBJECT_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo fromObject;
	
	//ma nganh hang
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "TO_OBJECT_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo toObject;
	

	//trang thai 0 không hoat dong, 1 hoat dong
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	//Loai: 12 : Nganh hang con ; 17 : Nganh hang con phu
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProductInfoMapType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProductInfoMapType objectType;
	
	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	public ProductInfoMap clone() {
		ProductInfoMap ret = new ProductInfoMap();
		ret.setId(id);
		ret.setFromObject(fromObject);
		ret.setToObject(toObject);
		ret.setObjectType(objectType);
		ret.setStatus(status);
		ret.setCreateDate(createDate);
		ret.setCreateUser(createUser);
		ret.setUpdateDate(updateDate);
		ret.setUpdateUser(updateUser);;
		return ret;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductInfo getFromObject() {
		return fromObject;
	}

	public void setFromObject(ProductInfo fromObject) {
		this.fromObject = fromObject;
	}

	public ProductInfo getToObject() {
		return toObject;
	}

	public void setToObject(ProductInfo toObject) {
		this.toObject = toObject;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public ProductInfoMapType getObjectType() {
		return objectType;
	}

	public void setObjectType(ProductInfoMapType objectType) {
		this.objectType = objectType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ProgramType;

/**
* Auto generate entities - SALE_ORDER_DETAIL
* @author nhanlt6
* @since 15/8/2014
*/

@Entity
@Table(name = "SALE_ORDER_PROMO_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SALE_ORDER_PROMO_DETAIL_SEQ", allocationSize = 1)
public class SaleOrderPromoDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//ID chi tiet don hang, Auto number
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SALE_ORDER_PROMO_DETAIL_ID")
	private Long id;
	
	//Id don hang
	@ManyToOne(targetEntity = SaleOrderDetail.class)
	@JoinColumn(name = "SALE_ORDER_DETAIL_ID", referencedColumnName = "SALE_ORDER_DETAIL_ID")
	private SaleOrderDetail saleOrderDetail;
	
	//Id don hang
	@ManyToOne(targetEntity = SaleOrder.class)
	@JoinColumn(name = "SALE_ORDER_ID", referencedColumnName = "SALE_ORDER_ID")
	private SaleOrder saleOrder;

	//id nv
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	//So tien chiet khau
	@Basic
	@Column(name = "DISCOUNT_AMOUNT", length = 22)
	private BigDecimal discountAmount;

	//% chiet khau
	@Basic
	@Column(name = "DISCOUNT_PERCENT", length = 22)
	private Float discountPercent;

	//PROGRAM_TYPE = 0,1 thi ma la CTKM; PROGRAM_TYPE = 2 --> ma la CTTB
	@Basic
	@Column(name = "PROGRAM_CODE", length = 20)
	private String programCode;

	//0: Km tu dong, 1:KM bang tay,2: cham chung bay, 3: doi hang
	@Basic
	@Column(name = "PROGRAM_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProgramType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProgramType programType;

	//PROGRAME_TYPE_CODE
	@Basic
	@Column(name = "PROGRAME_TYPE_CODE", length = 50)
	private String programeTypeCode;
	
	//So luong KM toi da
	@Basic
	@Column(name = "MAX_AMOUNT_FREE", length = 17)
	private BigDecimal maxAmountFree;
	
	//1. Là khuyến mãi của chính sp, 0. Là khuyến mãi do chia đều (từ đơn hàng, nhóm)
	@Basic
	@Column(name = "IS_OWNER", length = 2)
	private Integer isOwner;
	
	//id npp
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	@Basic
	@Column(name = "ORDER_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderDate;
	
	/*@ManyToOne(targetEntity = ProductGroup.class)
	@JoinColumn(name = "PRODUCT_GROUP_ID", referencedColumnName = "PRODUCT_GROUP_ID")*/
	@Transient
	private ProductGroup productGroup;
	
	@Basic
	@Column(name = "PRODUCT_GROUP_ID")
	private Long productGroupId;
	
	/*@ManyToOne(targetEntity = GroupLevel.class)
	@JoinColumn(name = "GROUP_LEVEL_ID", referencedColumnName = "GROUP_LEVEL_ID")*/
	@Transient
	private GroupLevel groupLevel;
	
	@Basic
	@Column(name = "GROUP_LEVEL_ID")
	private Long groupLevelId;
	
	@Transient
	private Integer rowNum;
	
	public ProductGroup getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(ProductGroup productGroup) {
		this.productGroup = productGroup;
	}

	public GroupLevel getGroupLevel() {
		return groupLevel;
	}

	public void setGroupLevel(GroupLevel groupLevel) {
		this.groupLevel = groupLevel;
	}
	
	public SaleOrderDetail getSaleOrderDetail() {
		return saleOrderDetail;
	}

	public void setSaleOrderDetail(SaleOrderDetail saleOrderDetail) {
		this.saleOrderDetail = saleOrderDetail;
	}

	public Integer getIsOwner() {
		return isOwner;
	}

	public void setIsOwner(Integer isOwner) {
		this.isOwner = isOwner;
	}

	public Integer getRowNum() {
		return rowNum;
	}

	public void setRowNum(Integer rowNum) {
		this.rowNum = rowNum;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount == null ? BigDecimal.valueOf(0) : discountAmount;
	}
	
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Float getDiscountPercent() {
		return discountPercent == null ? 0f : discountPercent;
	}

	public void setDiscountPercent(Float discountPercent) {
		this.discountPercent = discountPercent;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public ProgramType getProgramType() {
		return programType;
	}

	public void setProgramType(ProgramType programType) {
		this.programType = programType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public BigDecimal getMaxAmountFree() {
		return maxAmountFree;
	}

	public void setMaxAmountFree(BigDecimal maxAmountFree) {
		this.maxAmountFree = maxAmountFree;
	}

	public String getProgrameTypeCode() {
		return programeTypeCode;
	}

	public void setProgrameTypeCode(String programeTypeCode) {
		this.programeTypeCode = programeTypeCode;
	}
	
	public SaleOrderPromoDetail clone() {
		SaleOrderPromoDetail temp = new SaleOrderPromoDetail();
		temp.discountAmount = discountAmount;
		temp.discountPercent = discountPercent;
		temp.id = id;
		temp.programCode = programCode;
		temp.programType = programType;
		temp.programeTypeCode = programeTypeCode;
		temp.saleOrder = saleOrder;
		temp.staff = staff;
		temp.maxAmountFree = maxAmountFree;
		return temp;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Long getGroupLevelId() {
		return groupLevelId;
	}

	public void setGroupLevelId(Long groupLevelId) {
		this.groupLevelId = groupLevelId;
	}

	public Long getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(Long productGroupId) {
		this.productGroupId = productGroupId;
	}
	
}
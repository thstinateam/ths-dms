package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * 
 * @author loctt
 * @since 25/09/2013
 * 
 */

@Entity
@Table(name = "DISPLAY_PL_DP_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "DISPLAY_PL_DP_DTL_SEQ", allocationSize = 1)
public class StDisplayPlDpDtl implements Serializable {

	private static final long serialVersionUID = 1L;


	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "DISPLAY_PL_DP_DTL_ID")
	private Long id;

	//id 
	@ManyToOne(targetEntity = StDisplayProgramLevel.class)
	@JoinColumn(name = "DISPLAY_PROGRAM_LEVEL_ID", referencedColumnName = "DISPLAY_PROGRAM_LEVEL_ID")
	private StDisplayProgramLevel displayProgramLevel;
	
	//id 
	@ManyToOne(targetEntity = StDisplayPdGroupDtl.class)
	@JoinColumn(name = "DISPLAY_PRODUCT_GROUP_DTL_ID", referencedColumnName = "DISPLAY_PRODUCT_GROUP_DTL_ID")
	private StDisplayPdGroupDtl displayProductGroupDetail;
	
	//
	@Basic
	@Column(name = "QUANTITY", length = 20)
	private BigDecimal quantity;
	
	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	//ngay tao
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATE_DATE")
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	//ngay cap nhat
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DATE")
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	
	public StDisplayPlDpDtl clone() {
		StDisplayPlDpDtl ret = new StDisplayPlDpDtl();
		ret.setDisplayProgramLevel(displayProgramLevel);
		ret.setDisplayProductGroupDetail(displayProductGroupDetail);
		ret.setQuantity(quantity);
		ret.setStatus(status);
		ret.setCreateDate(createDate);
		ret.setCreateUser(createUser);
		ret.setUpdateDate(updateDate);
		ret.setUpdateUser(updateUser);
		return ret;
	}


	public StDisplayProgramLevel getDisplayProgramLevel() {
		return displayProgramLevel;
	}


	public void setDisplayProgramLevel(StDisplayProgramLevel displayProgramLevel) {
		this.displayProgramLevel = displayProgramLevel;
	}


	public StDisplayPdGroupDtl getDisplayProductGroupDetail() {
		return displayProductGroupDetail;
	}


	public void setDisplayProductGroupDetail(
			StDisplayPdGroupDtl displayProductGroupDetail2) {
		this.displayProductGroupDetail = displayProductGroupDetail2;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public BigDecimal getQuantity() {
		return quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public ActiveType getStatus() {
		return status;
	}


	public void setStatus(ActiveType status) {
		this.status = status;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getCreateUser() {
		return createUser;
	}


	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public String getUpdateUser() {
		return updateUser;
	}


	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
}
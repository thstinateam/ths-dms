package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PO_CUSTOMER")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PO_CUSTOMER_SEQ", allocationSize = 1)
public class PoCustomer implements Serializable {

	private static final long serialVersionUID = 1L;
	//So tien don hang chua tinh khuyen mai
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//0: chua duyet; 1 dat duyet; 2 khong duyet
	@Basic
	@Column(name = "APPROVED", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.SaleOrderStatus"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private SaleOrderStatus approved = SaleOrderStatus.NOT_YET_APPROVE;
	
	//Ngay duyet don hang
	@Basic
	@Column(name = "APPROVED_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approvedDate;
	
	// 
	@Basic
	@Column(name = "ACCOUNT_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date accountDate;

	//Xe giao hang
	@ManyToOne(targetEntity = Car.class)
	@JoinColumn(name = "CAR_ID", referencedColumnName = "CAR_ID")
	private Car car;

	//Id NVTT: nhan vien thu tien
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "CASHIER_ID", referencedColumnName = "STAFF_ID")
	private Staff cashier;

	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

//	@Basic
//	@Column(name = "IMPORT_CODE", length = 50)
//	private String importCode;

	//Id khach hang
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	//ngay can giao hang
	@Basic
	@Column(name = "DELIVERY_DATE", length = 7)
	private Date deliveryDate;

	//Id NVGH
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "DELIVERY_ID", referencedColumnName = "STAFF_ID")
	private Staff delivery;

	//mo ta
	@Basic
	@Column(name = "DESCRIPTION", length = 600)
	private String description;

	//So tien khuyen mai
	@Basic
	@Column(name = "DISCOUNT", length = 22)
	private BigDecimal discount;

	//id don hang bi tra
	@ManyToOne(targetEntity = SaleOrder.class)
	@JoinColumn(name = "FROM_SALE_ORDER_ID", referencedColumnName = "SALE_ORDER_ID")
	private SaleOrder fromPoCustomer;

	//id cua hoa don VAT
//	@ManyToOne(targetEntity = Invoice.class)
//	@JoinColumn(name = "INVOICE_ID", referencedColumnName = "INVOICE_ID")
//	private Invoice invoice;

	//Ngay lap don hang
	@Basic
	@Column(name = "ORDER_DATE", length = 7)
	private Date orderDate;
	
	@Basic
	@Column(name = "TIME_PRINT", length = 7)
	private Date timePrint;
	
	
	/** So lan in */
	@Basic
	@Column(name = "PRINT_BATCH", length = 8)
	private Integer printBatch;	
	
	

	//MÃ£ don hÃ ng
	@Basic
	@Column(name = "ORDER_NUMBER", length = 50)
	private String orderNumber;

	//Loai don hang: IN: pre->kh; CM: KH->Pre;SO Van->KH;CO  KH->Van
	@Basic
	@Column(name = "ORDER_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.OrderType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private OrderType orderType = OrderType.IN ;

	//do khan cua don hang
	@Basic
	@Column(name = "PRIORITY", length = 20)
	private Integer priority;

	//ID Ã�on d?t hÃ ng, Auto number
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PO_CUSTOMER_ID")
	private Long id;

	//MÃ£ NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//Id NVBH
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	//1: don hang tren web; 2: don hang tao ra tren tablet;
	@Basic
	@Column(name = "ORDER_SOURCE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.SaleOrderSource"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private SaleOrderSource orderSource = SaleOrderSource.WEB;
	
	//So tien don hang sau khi tinh khuyen mai
	@Basic
	@Column(name = "TOTAL", length = 22)
	private BigDecimal total;

	//tong trong luong cua don hang
	@Basic
	@Column(name = "TOTAL_WEIGHT", length = 22)
	private BigDecimal totalWeight;

	//1: don hang ban nhung chua tra, 0: don hang ban da thuc hien tra lai
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.SaleOrderType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private SaleOrderType type = SaleOrderType.NOT_YET_RETURNED;

	//Ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	//thue suat
	/*@Basic
	@Column(name = "VAT", length = 22)
	private Float vat;*/
	
	@Basic
	@Column(name = "IS_VISIT_PLAN")
	private Integer isVisitPlan;
	
	@Basic
	@Column(name = "DESTROY_CODE", length = 50)
	private String destroyCode;
	
//	@Basic
//	@Column(name = "INVOICE_NUMBER", length = 50)
//	private String invoiceNumber;

	public Integer getIsVisitPlan() {
		return isVisitPlan;
	}

	public void setIsVisitPlan(Integer isVisitPlan) {
		this.isVisitPlan = isVisitPlan;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public SaleOrderStatus getApproved() {
		return approved;
	}

	public void setApproved(SaleOrderStatus approved) {
		this.approved = approved;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public Staff getCashier() {
		return cashier;
	}

	public void setCashier(Staff cashier) {
		this.cashier = cashier;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Staff getDelivery() {
		return delivery;
	}

	public void setDelivery(Staff delivery) {
		this.delivery = delivery;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getDiscount() {
		return discount == null ? BigDecimal.valueOf(0) : discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public SaleOrderSource getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(SaleOrderSource orderSource) {
		this.orderSource = orderSource;
	}

	public BigDecimal getTotal() {
		return total == null ? BigDecimal.valueOf(0) : total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getTotalWeight() {
		return totalWeight == null ? BigDecimal.valueOf(0) : totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}

	public SaleOrderType getType() {
		return type;
	}

	public void setType(SaleOrderType type) {
		this.type = type;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getDestroyCode() {
		return destroyCode;
	}

	public void setDestroyCode(String destroyCode) {
		this.destroyCode = destroyCode;
	}


	public Date getTimePrint() {
		return timePrint;
	}

	public void setTimePrint(Date timePrint) {
		this.timePrint = timePrint;
	}

	public Integer getPrintBatch() {
		return printBatch;
	}

	public void setPrintBatch(Integer printBatch) {
		this.printBatch = printBatch;
	}

	// ThuatTQ add migrate
	
	
	public SaleOrder getFromPoCustomer() {
		return fromPoCustomer;
	}

	public void setFromPoCustomer(SaleOrder fromPoCustomer) {
		this.fromPoCustomer = fromPoCustomer;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public Date getAccountDate() {
		return accountDate;
	}

	public void setAccountDate(Date accountDate) {
		this.accountDate = accountDate;
	}

	public PoCustomer clone() {
		PoCustomer s = new PoCustomer();

		s.setAmount(amount);
		s.setApproved(approved);
		s.setCar(car);
		s.setCashier(cashier);
		s.setCreateDate(createDate);
		s.setCreateUser(createUser);
		s.setCustomer(customer);
		s.setDelivery(delivery);
		s.setDeliveryDate(deliveryDate);
		s.setDescription(description);
		s.setDestroyCode(destroyCode);
		s.setDiscount(discount);
		s.setFromPoCustomer(fromPoCustomer);
		s.setIsVisitPlan(isVisitPlan);
		s.setOrderDate(orderDate);
		s.setOrderNumber(orderNumber);
		s.setOrderSource(orderSource);
		s.setOrderType(orderType);
		s.setPrintBatch(printBatch);
		s.setPriority(priority);
		s.setShop(shop);
		s.setStaff(staff);
		s.setTimePrint(timePrint);
		s.setTotal(total);
		s.setTotalWeight(totalWeight);
		s.setType(type);

		return s;
	}
}
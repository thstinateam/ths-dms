package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PROMOTION_PROGRAM_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PROMOTION_PROGRAM_DETAIL_SEQ", allocationSize = 1)
public class PromotionProgramDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//km tien
	@Basic
	@Column(name = "DISC_AMT", length = 22)
	private BigDecimal discAmt;

	//km theo %
	@Basic
	@Column(name = "DISC_PER", length = 22)
	private Float discPer;

	//id sp km
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "FREE_PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product freeProduct;

	//so luong km
	@Basic
	@Column(name = "FREE_QTY", length = 22)
	private Integer freeQty;

	//don vi sp km
	@Basic
	@Column(name = "FREE_UOM", length = 30)
	private String freeUom;

	//id san pham ban
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PROMOTION_PROGRAM_DETAIL_ID")
	private Long id;

	//id ctkm
	@ManyToOne(targetEntity = PromotionProgram.class)
	@JoinColumn(name = "PROMOTION_PROGRAM_ID", referencedColumnName = "PROMOTION_PROGRAM_ID")
	private PromotionProgram promotionProgram;

	@Basic
	@Column(name = "REQUIRED", length = 22)
	private Integer required ;

	//tong tien ban
	@Basic
	@Column(name = "SALE_AMT", length = 22)
	private BigDecimal saleAmt;

	//so luong ban
	@Basic
	@Column(name = "SALE_QTY", length = 22)
	private Integer saleQty;

	//don vi sp ban
	@Basic
	@Column(name = "SALE_UOM", length = 30)
	private String saleUom;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public BigDecimal getDiscAmt() {
//		return discAmt == null ? BigDecimal.valueOf(0) : discAmt;
		return discAmt;
	}

	public void setDiscAmt(BigDecimal discAmt) {
		this.discAmt = discAmt;
	}

	public Float getDiscPer() {
//		return discPer == null ? 0 : discPer;
		return discPer;
	}

	public void setDiscPer(Float discPer) {
		this.discPer = discPer;
	}

	public Product getFreeProduct() {
		return freeProduct;
	}

	public void setFreeProduct(Product freeProduct) {
		this.freeProduct = freeProduct;
	}

	public Integer getFreeQty() {
//		if (freeQty == null) {
//			freeQty = 0;
//		}
		return freeQty;
	}

	public void setFreeQty(Integer freeQty) {
		this.freeQty = freeQty;
	}

	public String getFreeUom() {
		return freeUom;
	}

	public void setFreeUom(String freeUom) {
		this.freeUom = freeUom;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PromotionProgram getPromotionProgram() {
		return promotionProgram;
	}

	public void setPromotionProgram(PromotionProgram promotionProgram) {
		this.promotionProgram = promotionProgram;
	}

	public Integer getRequired() {
//		if (required == null) {
//			required = 0;
//		}
		return required;
	}

	public void setRequired(Integer required) {
		this.required = required;
	}

	public BigDecimal getSaleAmt() {
//		return saleAmt == null ? BigDecimal.valueOf(0) : saleAmt;
		return saleAmt;
	}

	public void setSaleAmt(BigDecimal saleAmt) {
		this.saleAmt = saleAmt;
	}

	public Integer getSaleQty() {
//		if (saleQty == null) {
//			saleQty = 0;
//		}
		return saleQty;
	}

	public void setSaleQty(Integer saleQty) {
		this.saleQty = saleQty;
	}

	public String getSaleUom() {
		return saleUom;
	}

	public void setSaleUom(String saleUom) {
		this.saleUom = saleUom;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public PromotionProgramDetail clone() {
		PromotionProgramDetail ppd = new PromotionProgramDetail();
		ppd.setCreateDate(createDate);
		ppd.setCreateUser(createUser);
		ppd.setDiscAmt(discAmt);
		ppd.setDiscPer(discPer);
		ppd.setFreeProduct(freeProduct);
		ppd.setFreeQty(freeQty);
		ppd.setFreeUom(freeUom);
		ppd.setProduct(product);
		ppd.setId(id);
		ppd.setPromotionProgram(promotionProgram);
		ppd.setRequired(required);
		ppd.setSaleAmt(saleAmt);
		ppd.setSaleQty(saleQty);
		ppd.setSaleUom(saleUom);
		ppd.setUpdateDate(updateDate);
		ppd.setUpdateUser(updateUser);
		return ppd;
	}

	
}
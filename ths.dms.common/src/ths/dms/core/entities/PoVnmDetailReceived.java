/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.PoProductType;
import ths.dms.core.entities.enumtype.PoType;

/**
 *
 * Generate entities
 * xu ly nhap tra hang, de luu received
 * @author vuongmq
 * @since 31/08/2015
 * 
 */

@Entity
@Table(name = "PO_VNM_DETAIL_RECEIVED")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PO_VNM_DETAIL_RECEIVED_SEQ", allocationSize = 1)
public class PoVnmDetailReceived implements Serializable {

	private static final long serialVersionUID = 1L;
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PO_VNM_DETAIL_RECEIVED_ID")
	private Long id;
	
	//id don hang
	@ManyToOne(targetEntity = PoVnm.class)
	@JoinColumn(name = "PO_VNM_ID", referencedColumnName = "PO_VNM_ID")
	private PoVnm poVnm;

	//id po vnm detail
	@ManyToOne(targetEntity = PoVnmDetail.class)
	@JoinColumn(name = "PO_VNM_DETAIL_ID", referencedColumnName = "PO_VNM_DETAIL_ID")
	private PoVnmDetail poVnmDetail;
	
	//id san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//gia
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal priceValue;

	//id gia
	@ManyToOne(targetEntity = Price.class)
	@JoinColumn(name = "PRICE_ID", referencedColumnName = "PRICE_ID")
	private Price price;

	@Basic
	@Column(name = "QUANTITY_RECEIVED", length = 22)
	private Integer quantityReceived;
	
	@Basic
	@Column(name = "AMOUNT_RECEIVED")
	private BigDecimal amountReceived;

	//id warehouse
	@ManyToOne(targetEntity = Warehouse.class)
	@JoinColumn(name = "WAREHOUSE_ID", referencedColumnName = "WAREHOUSE_ID")
	private Warehouse warehouse;
	
	// 10/08/2015; dong bo ve co loai san pham
	//1: hang ban, 2: hang KM, 3: POSM
	@Basic
	@Column(name = "PRODUCT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PoProductType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PoProductType productType;
	
	//1: PO DVKH nhap, 2: PO confirm, 3: PO confirm don hang tra; 4 PO DVKH tra
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PoType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PoType type;
	
	//Ngay nhap hang vao kho
	@Basic
	@Column(name = "IMPORT_DATE", length = 7)
	private Date importDate;
	
	@Basic
	@Column(name = "RECEIVED_DATE", length = 7)
	@Temporal(TemporalType.TIMESTAMP)
	private Date receivedDate;
	
	@Basic
	@Column(name = "DELIVERY_DATE", length = 7)
	@Temporal(TemporalType.TIMESTAMP)
	private Date deliveryDate;
	
	// update_date
	@Basic
	@Column(name = "UPDATE_DATE", length = 7)
	private Date updateDate;
	
	//Lưu thông tin user thực hiện nhập/ tra kho cho PO confirm.
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	// update_date
	@Basic
	@Column(name = "CREATE_DATE", length = 7)
	private Date createDate;
	
	//Lưu thông tin user thực hiện nhập/ tra kho cho PO confirm.
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PoVnm getPoVnm() {
		return poVnm;
	}

	public void setPoVnm(PoVnm poVnm) {
		this.poVnm = poVnm;
	}

	public PoVnmDetail getPoVnmDetail() {
		return poVnmDetail;
	}

	public void setPoVnmDetail(PoVnmDetail poVnmDetail) {
		this.poVnmDetail = poVnmDetail;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public BigDecimal getPriceValue() {
		return priceValue;
	}

	public void setPriceValue(BigDecimal priceValue) {
		this.priceValue = priceValue;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Integer getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public BigDecimal getAmountReceived() {
		return amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public PoProductType getProductType() {
		return productType;
	}

	public void setProductType(PoProductType productType) {
		this.productType = productType;
	}

	public PoType getType() {
		return type;
	}

	public void setType(PoType type) {
		this.type = type;
	}

	public Date getImportDate() {
		return importDate;
	}

	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
}
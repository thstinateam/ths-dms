package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.PoDisStatus;
import ths.dms.core.entities.enumtype.PoDvStatus;
import ths.dms.core.entities.enumtype.PoObjectType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PO_VNM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PO_VNM_SEQ", allocationSize = 1)
public class PoVnm implements Serializable {
	
	private static final long serialVersionUID = 1L;
	//tong tien
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//tong tien kiem soat
	@Basic
	@Column(name = "AMOUNT_CHECK", length = 22)
	private BigDecimal amountCheck;

	//tong tien da nhan theo hoa don
	@Basic
	@Column(name = "AMOUNT_RECEIVED", length = 22)
	private BigDecimal amountReceived;

	//dia chi giao hang
	/*@Basic
	@Column(name = "BILLTOLOCATION", length = 600)
	private String billtolocation;*/

	//ngay insert vao data VT
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//chiet khau
	@Basic
	@Column(name = "DISCOUNT", length = 22)
	private BigDecimal discount;

	//id cua so don hang
	@ManyToOne(targetEntity = PoVnm.class)
	@JoinColumn(name = "FROM_PO_VNM_ID", referencedColumnName = "PO_VNM_ID")
	private PoVnm fromPoVnmId;

	//Ngay nhap hang vao kho
	@Basic
	@Column(name = "IMPORT_DATE", length = 7)
	private Date importDate;

	//ngay tao hoa don
	@Basic
	@Column(name = "INVOICE_DATE", length = 7)
	private Date invoiceDate;

	//so hoa don
	@Basic
	@Column(name = "INVOICE_NUMBER", length = 40)
	private String invoiceNumber;

	//ngay cap nhat
	@Basic
	@Column(name = "MODIFY_DATE", length = 7)
	private Date modifyDate;

	//ngay tao PO auto
	@Basic
	@Column(name = "ORDERDATE", length = 7)
	private Date orderdate;

	//po auto number
	@Basic
	@Column(name = "PO_AUTO_NUMBER", length = 40)
	private String poAutoNumber;

	//chua biet
	@Basic
	@Column(name = "PO_CO_NUMBER", length = 40)
	private String poCoNumber;

	//ngay tao po dvkh, po confirm
	@Basic
	@Column(name = "PO_VNM_DATE", length = 7)
	@Temporal(TemporalType.TIMESTAMP)
	private Date poVnmDate;
	
	//ngay tao cua po_date ben don tra
	@Basic
	@Column(name = "REQUEST_DATE", length = 7)
	@Temporal(TemporalType.TIMESTAMP)
	private Date requestDate;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PO_VNM_ID")
	private Long id;

	//tong so nhap
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;

	//sl kiem soat
	@Basic
	@Column(name = "QUANTITY_CHECK", length = 22)
	private Integer quantityCheck;

	//tong so da nhap
	@Basic
	@Column(name = "QUANTITY_RECEIVED", length = 22)
	private Integer quantityReceived;

	//ly do cap nhat
//	@ManyToOne(targetEntity = Reason.class)
//	@JoinColumn(name = "REASON_ID", referencedColumnName = "REASON_ID")
//	private Reason reason;

	//id don hang tren VNM
	@Basic
	@Column(name = "SALE_ORDER_NUMBER", length = 40)
	private String saleOrderNumber;

	//phuong tien van chuyen
	/*@Basic
	@Column(name = "SHIPTOLOCATION", length = 600)
	private String shiptolocation;*/

	//id npp
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//trang thai 0 chua nhap, 1 dang nhap hang, 2 dÃ£ nhap/tra xong, 3 co the treo, 4 treo
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PoVNMStatus"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PoVNMStatus status;

	//tong tien tra
	@Basic
	@Column(name = "TOTAL", length = 22)
	private BigDecimal total;

	//1: PO DVKH, 2: PO confirm, 3: don hang tra
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PoType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PoType type;
	
	//Lưu thông tin user thực hiện nhập kho cho PO confirm.
	@Basic
	@Column(name = "IMPORT_USER", length = 40)
	private String importUser;

	/*
	object_type       NUMBER(1,0),   [0: NCC, 1: NPP]
	dis_status        NUMBER(1,0), [0: chua chuyen NPP, 1: da chuyen [OBJECT_TYPE = 0]]
	delivery_date     DATE, [Ngay giao hang (POCONFIRM NCC)]
	podv_status       NUMBER(1,0)) [[PODVKH-NCC] 0: chua co PO Confirm, 1: dang xuat PO Confirm, 2: hoan tat, 3:treo]
	 */
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PoObjectType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PoObjectType objectType;
	
	@Basic
	@Column(name = "DIS_STATUS", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PoDisStatus"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PoDisStatus disStatus;
	
	@Basic
	@Column(name = "DELIVERY_DATE", length = 7)
	@Temporal(TemporalType.TIMESTAMP)
	private Date deliveryDate;
	
	@Basic
	@Column(name = "PODV_STATUS", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PoDvStatus"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PoDvStatus poDvStatus;
	
	@Basic
	@Column(name = "REASON_CODE", length = 50)
	private String reasonCode;
	
	//ngay huy don PO Confirm
	@Basic
	@Column(name = "CANCEL_DATE", length = 7)
	private Date cancelDate;
	
	//ngay treo PO DVKH
	@Basic
	@Column(name = "HOLD_DATE", length = 7)
	private Date holdDate;
	
	//Warehouese id
	@ManyToOne(targetEntity = Warehouse.class)
	@JoinColumn(name = "WAREHOUSE_ID", referencedColumnName = "WAREHOUSE_ID")
	private Warehouse warehouse;
	
	@Basic
	@Column(name = "NOTE", length = 200)
	private String note;
	
	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getAmountCheck() {
		return amountCheck == null ? BigDecimal.valueOf(0) : amountCheck;
	}

	public void setAmountCheck(BigDecimal amountCheck) {
		this.amountCheck = amountCheck;
	}

	public BigDecimal getAmountReceived() {
		return amountReceived == null ? BigDecimal.valueOf(0) : amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BigDecimal getDiscount() {
		return discount == null ? BigDecimal.valueOf(0) : discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public PoVnm getFromPoVnmId() {
		return fromPoVnmId;
	}

	public void setFromPoVnmId(PoVnm fromPoVnmId) {
		this.fromPoVnmId = fromPoVnmId;
	}

	public Date getImportDate() {
		return importDate;
	}

	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getOrderdate() {
		return orderdate;
	}

	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}

	public String getPoAutoNumber() {
		return poAutoNumber;
	}

	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}

	public String getPoCoNumber() {
		return poCoNumber;
	}

	public void setPoCoNumber(String poCoNumber) {
		this.poCoNumber = poCoNumber;
	}

	public Date getPoVnmDate() {
		return poVnmDate;
	}

	public void setPoVnmDate(Date poVnmDate) {
		this.poVnmDate = poVnmDate;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantityCheck() {
		if (quantityCheck == null) {
			quantityCheck = 0;
		}
		return quantityCheck;
	}

	public void setQuantityCheck(Integer quantityCheck) {
		this.quantityCheck = quantityCheck;
	}

	public Integer getQuantityReceived() {
		if (quantityReceived == null) {
			quantityReceived = 0;
		}
		return quantityReceived;
	}

	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public String getSaleOrderNumber() {
		return saleOrderNumber;
	}

	public void setSaleOrderNumber(String saleOrderNumber) {
		this.saleOrderNumber = saleOrderNumber;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public PoVNMStatus getStatus() {
		return status;
	}

	public void setStatus(PoVNMStatus status) {
		this.status = status;
	}

	public BigDecimal getTotal() {
		return total == null ? BigDecimal.valueOf(0) : total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public PoType getType() {
		return type;
	}

	public void setType(PoType type) {
		this.type = type;
	}

	public String getImportUser() {
		return importUser;
	}

	public void setImportUser(String importUser) {
		this.importUser = importUser;
	}

	public PoObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(PoObjectType objectType) {
		this.objectType = objectType;
	}

	public PoDisStatus getDisStatus() {
		return disStatus;
	}

	public void setDisStatus(PoDisStatus disStatus) {
		this.disStatus = disStatus;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public PoDvStatus getPoDvStatus() {
		return poDvStatus;
	}

	public void setPoDvStatus(PoDvStatus poDvStatus) {
		this.poDvStatus = poDvStatus;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Date getHoldDate() {
		return holdDate;
	}

	public void setHoldDate(Date holdDate) {
		this.holdDate = holdDate;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
}
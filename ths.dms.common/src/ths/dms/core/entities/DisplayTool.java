package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the DISPLAY_TOOLS database table.
 * 
 */
@Entity
@Table(name="DISPLAY_TOOLS")
public class DisplayTool implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DISPLAY_TOOLS_DISPLAYTOOLSID_GENERATOR", sequenceName="DISPLAY_TOOLS_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DISPLAY_TOOLS_DISPLAYTOOLSID_GENERATOR")
	@Column(name="DISPLAY_TOOLS_ID")
	private Long displayToolsId;

	private Long amount;

    @Temporal( TemporalType.DATE)
	@Column(name="CREATE_DATE")
	private Date createDate;

	@Column(name="CREATE_USER")
	private String createUser;

	@Column(name="CUSTOMER_ID")
	private Long customerId;

    @Temporal( TemporalType.DATE)
	@Column(name="IN_MONTH")
	private Date inMonth;

	@ManyToOne
	@JoinColumn(name="PRODUCT_ID")
	private Product product;

	private Long quantity;

	@Column(name="RESULT")
	private BigDecimal result;

	@ManyToOne
	@JoinColumn(name="SHOP_ID")
	private Shop shop;

	@ManyToOne
	@JoinColumn(name="STAFF_ID")
	private Staff staff;

    @Temporal( TemporalType.DATE)
	@Column(name="TRANSFER_DATE")
	private Date transferDate;

    @Temporal( TemporalType.DATE)
	@Column(name="UPDATE_DATE")
	private Date updateDate;

	@Column(name="UPDATE_USER")
	private String updateUser;

	public Long getDisplayToolsId() {
		return displayToolsId;
	}

	public void setDisplayToolsId(Long displayToolsId) {
		this.displayToolsId = displayToolsId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Date getInMonth() {
		return inMonth;
	}

	public void setInMonth(Date inMonth) {
		this.inMonth = inMonth;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getResult() {
		return result;
	}

	public void setResult(BigDecimal result) {
		this.result = result;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import ths.dms.core.common.utils.CDateUtil;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_REPAIR_FORM_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_REPAIR_FORM_DTL_SEQ", allocationSize = 1)
public class EquipRepairFormDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//ID h?ng m?c s?a ch?a
	@ManyToOne(targetEntity = EquipItem.class)
	@JoinColumn(name = "EQUIP_ITEM_ID", referencedColumnName = "EQUIP_ITEM_ID")
	private EquipItem equipItem;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_REPAIR_FORM_DTL_ID")
	private Long id;

	//ID phi?u s?a ch?a
	@ManyToOne(targetEntity = EquipRepairForm.class)
	@JoinColumn(name = "EQUIP_REPAIR_FORM_ID", referencedColumnName = "EQUIP_REPAIR_FORM_ID")
	private EquipRepairForm equipRepairForm;

	//Gia vat tu
	@Basic
	@Column(name = "MATERIAL_PRICE", length = 22)
	private BigDecimal materialPrice;

	//S? l�?ng
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;

	//S? l?n s?a ch?a
	@Basic
	@Column(name = "REPAIR_COUNT", length = 22)
	private Integer repairCount;

	//T?ng ti?n
	@Basic
	@Column(name = "TOTAL_AMOUNT", length = 22)
	private BigDecimal totalAmount;

	//Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	//Th?i gian b?o h�nh (th�ng)
	@Basic
	@Column(name = "WARRANTY", length = 22)
	private Long warranty;

	//Gia nhan cong
	@Basic
	@Column(name = "WORKER_PRICE", length = 22)
	private BigDecimal workerPrice;
	
	/*** vuongmq; 14/04/2015*/
	// ngay bat dau bao hanh
	@Basic
	@Column(name = "WARRANTY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date warrantyDate;
	 
	// ngay het han bao hanh
	@Basic
	@Column(name = "WARRANTY_EXPIRED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date warrantyExpireDate;
	
	//gia vat tu tu
	@Basic
	@Column(name = "FROM_MATERIAL_PRICE", length = 22)
	private BigDecimal fromMaterialPrice;
	
	//gia vat tu den
	@Basic
	@Column(name = "TO_MATERIAL_PRICE", length = 22)
	private BigDecimal toMaterialPrice;
	
	//gia nhan cong tu
	@Basic
	@Column(name = "FROM_WORKER_PRICE", length = 22)
	private BigDecimal fromWorkerPrice;
	
	// gia nhan cong den
	@Basic
	@Column(name = "TO_WORKER_PRICE", length = 22)
	private BigDecimal toWorkerPrice;
	
	// ghi chu vuot muc dinh muc
	@Basic
	@Column(name = "DESCRIPTION", length = 300)
	private String description;
	
	// ca bien tam cua Entities
	@Transient
	private String createDateString;
	
	@Transient
	private String totalActualAmount;
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public EquipItem getEquipItem() {
		return equipItem;
	}

	public void setEquipItem(EquipItem equipItem) {
		this.equipItem = equipItem;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipRepairForm getEquipRepairForm() {
		return equipRepairForm;
	}

	public void setEquipRepairForm(EquipRepairForm equipRepairForm) {
		this.equipRepairForm = equipRepairForm;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getRepairCount() {
		return repairCount;
	}

	public void setRepairCount(Integer repairCount) {
		this.repairCount = repairCount;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Long getWarranty() {
		return warranty;
	}

	public void setWarranty(Long warranty) {
		this.warranty = warranty;
	}

	public BigDecimal getWorkerPrice() {
		return workerPrice;
	}

	public void setWorkerPrice(BigDecimal workerPrice) {
		this.workerPrice = workerPrice;
	}

	public String getCreateDateString() {
		if(equipRepairForm != null && equipRepairForm.getCompleteDate()!=null){
			return CDateUtil.toDateString(equipRepairForm.getCompleteDate(), CDateUtil.DATE_FORMAT_STR);
		}
		return createDateString;
	}

	public void setCreateDateString(String createDateString) {
		this.createDateString = createDateString;
	}

	public BigDecimal getMaterialPrice() {
		return materialPrice;
	}

	public void setMaterialPrice(BigDecimal materialPrice) {
		this.materialPrice = materialPrice;
	}

	public String getTotalActualAmount() {
		return totalActualAmount;
	}

	public void setTotalActualAmount(String totalActualAmount) {
		this.totalActualAmount = totalActualAmount;
	}

	public Date getWarrantyDate() {
		return warrantyDate;
	}

	public void setWarrantyDate(Date warrantyDate) {
		this.warrantyDate = warrantyDate;
	}

	public Date getWarrantyExpireDate() {
		return warrantyExpireDate;
	}

	public void setWarrantyExpireDate(Date warrantyExpireDate) {
		this.warrantyExpireDate = warrantyExpireDate;
	}

	public BigDecimal getFromMaterialPrice() {
		return fromMaterialPrice;
	}

	public void setFromMaterialPrice(BigDecimal fromMaterialPrice) {
		this.fromMaterialPrice = fromMaterialPrice;
	}

	public BigDecimal getToMaterialPrice() {
		return toMaterialPrice;
	}

	public void setToMaterialPrice(BigDecimal toMaterialPrice) {
		this.toMaterialPrice = toMaterialPrice;
	}

	public BigDecimal getFromWorkerPrice() {
		return fromWorkerPrice;
	}

	public void setFromWorkerPrice(BigDecimal fromWorkerPrice) {
		this.fromWorkerPrice = fromWorkerPrice;
	}

	public BigDecimal getToWorkerPrice() {
		return toWorkerPrice;
	}

	public void setToWorkerPrice(BigDecimal toWorkerPrice) {
		this.toWorkerPrice = toWorkerPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
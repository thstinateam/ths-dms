package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.StockObjectType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "RPT_STOCK_TOTAL_DAY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "RPT_STOCK_TOTAL_DAY_SEQ", allocationSize = 1)

@SqlResultSetMappings({
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptImExStDataDetailVO", columns = {
				@ColumnResult(name = "catId"),
				@ColumnResult(name = "catName"),
				@ColumnResult(name = "catCode"),
				@ColumnResult(name = "productConvfact"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productPrice"),
				@ColumnResult(name = "quantityCloseSTBegin"),
				@ColumnResult(name = "quantityCloseSTEnd"),
				@ColumnResult(name = "quantitySaleToCus"),
				@ColumnResult(name = "quantitySaleOffCus"),
				@ColumnResult(name = "quantitySaleReturn"),
				@ColumnResult(name = "quantityPromotionReturn"),
				@ColumnResult(name = "quantityBuyFromVNM"),
				@ColumnResult(name = "quantityReturnToVNM"),
				@ColumnResult(name = "quantityImport"),
				@ColumnResult(name = "quantityExport") }),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptDMVSVO", columns = {
				@ColumnResult(name = "maMien"),
				@ColumnResult(name = "maVung"),
				@ColumnResult(name = "maNPP"),
				@ColumnResult(name = "tenNPP"),
				@ColumnResult(name = "maNVBH"),
				@ColumnResult(name = "tenNVBH"),
				@ColumnResult(name = "tenGSNPP"),
				@ColumnResult(name = "ngay"),
				@ColumnResult(name = "tgBatDau"),
				@ColumnResult(name = "maKHS"),
				@ColumnResult(name = "tenKHS"),
				@ColumnResult(name = "diachiKHS"),
				@ColumnResult(name = "tgKetThuc"),
				@ColumnResult(name = "maKHC"),
				@ColumnResult(name = "tenKHC"),
				@ColumnResult(name = "diachiKHC"),
				@ColumnResult(name = "numS"),
				@ColumnResult(name = "numC")
				}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCNgayVO", columns = {
				@ColumnResult(name = "tenNV"),
				@ColumnResult(name = "maNV"),
				@ColumnResult(name = "loaiNV"),
				@ColumnResult(name = "doanhSoNgay"),
				@ColumnResult(name = "doanhSoLuyKe"),
				@ColumnResult(name = "keHoachLuyKe"),
				@ColumnResult(name = "phanTramTheoTienDo"),
				@ColumnResult(name = "chenhLenhTienDo"),
				@ColumnResult(name = "doanhSoKeHoach"),
				@ColumnResult(name = "doanhSoPhaiBanNS"),
				@ColumnResult(name = "keHoachMucTieu"),
				@ColumnResult(name = "f4TrongNgay"),
				@ColumnResult(name = "f4LuyKe"),
				@ColumnResult(name = "f4ThangTruoc"),
				@ColumnResult(name = "soDiemBan"),
				@ColumnResult(name = "soDiemChuaPP"),
				@ColumnResult(name = "palmTrongTuyen"),
				@ColumnResult(name = "palmNgoaiTuyen"),
				@ColumnResult(name = "traVeHomTruoc"),
				@ColumnResult(name = "donTay"),
				@ColumnResult(name = "doanhSo"),
				@ColumnResult(name = "phanPhoi")
		}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCTGTMVO", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "maNpp"),
				@ColumnResult(name = "tenNpp"),
				@ColumnResult(name = "tenGsnpp"),
				@ColumnResult(name = "maNv"),
				@ColumnResult(name = "tenNv"),
				@ColumnResult(name = "ngay"),
				@ColumnResult(name = "khoangThoiGianTatMay")
		}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCDS1VO", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "khuVuc"),
				@ColumnResult(name = "maNPP"),
				@ColumnResult(name = "tenNPP"),
				@ColumnResult(name = "GSNPP"),
				@ColumnResult(name = "tenGSNPP"),
				@ColumnResult(name = "NVBH"),
				@ColumnResult(name = "tenNVBH"),			
				@ColumnResult(name = "nganhHang"),
				@ColumnResult(name = "sku"),
				@ColumnResult(name = "soLuong"),
				@ColumnResult(name = "doanhSo")
		}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCDS2VO", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "khuVuc"),
				@ColumnResult(name = "maNPP"),
				@ColumnResult(name = "tenNPP"),
				@ColumnResult(name = "GSNPP"),
				@ColumnResult(name = "tenGSNPP"),
				@ColumnResult(name = "NVBH"),
				@ColumnResult(name = "tenNVBH"),
				@ColumnResult(name = "maKH"),
				@ColumnResult(name = "tenKH"),
				@ColumnResult(name = "diaChi"),
				@ColumnResult(name = "nganhHang"),
				@ColumnResult(name = "sku"),
				@ColumnResult(name = "soLuong"),
				@ColumnResult(name = "doanhSo"),
				@ColumnResult(name = "isGroup")
		}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCTCNDetailVO", columns = {
				@ColumnResult(name = "orderDate"),
				@ColumnResult(name = "orderNumber"),
				@ColumnResult(name = "orderType"),
				@ColumnResult(name = "overdueDebt"),
				@ColumnResult(name = "nextDebt"),
				@ColumnResult(name = "availableDebt"),
				//@ColumnResult(name = "total"),
				@ColumnResult(name = "customerInfo"),
				@ColumnResult(name = "staffInfo"),
				@ColumnResult(name = "deliveryStaffInfo"),
				@ColumnResult(name = "paidDate")//,
				//@ColumnResult(name = "groupInfo"),
				//@ColumnResult(name = "groupOverdueDebt"),
				//@ColumnResult(name = "groupNextDebt"),
				//@ColumnResult(name = "groupAvailableDebt"),
				//@ColumnResult(name = "groupTotal"),
				//@ColumnResult(name = "soThuTu")
		}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptAbsentVO", columns = {
				@ColumnResult(name = "mienCode"),
				@ColumnResult(name = "vungCode"),
				@ColumnResult(name = "tbhv"),
				@ColumnResult(name = "shopCode"),
				@ColumnResult(name = "shopName"),
				@ColumnResult(name = "nvgs"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "summaryValue")
				}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptDSPPNHCTVO", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "khuVuc"),
				@ColumnResult(name = "maNPP"),
				@ColumnResult(name = "idNPP"),
				@ColumnResult(name = "nhomHang"),
				@ColumnResult(name = "idNhomHang"),
				@ColumnResult(name = "soDiemLe"),
				@ColumnResult(name = "doanhSo")
				}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptDSBHMienCTVO", columns = {
				@ColumnResult(name = "mienId"),
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "nhom"),
				@ColumnResult(name = "nhan"),
				@ColumnResult(name = "maSP"),
				@ColumnResult(name = "tenSP"),
				@ColumnResult(name = "keHoachTT"),
				@ColumnResult(name = "thTrongNgay"),
				@ColumnResult(name = "thLuyke"),
				@ColumnResult(name = "tongThangTruoc"),
				@ColumnResult(name = "luyKeThangTruoc"),
				@ColumnResult(name = "ptThucHien")
				}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptF1DataVO", columns = {
				@ColumnResult(name = "saleDateInMonth"),
				@ColumnResult(name = "realSaleDateInMonth"),
				@ColumnResult(name = "toDate"),
				@ColumnResult(name = "productInfoCode"),
				@ColumnResult(name = "productInfoName"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "percentage"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "price"),
				@ColumnResult(name = "openStockTotal"),
				@ColumnResult(name = "importQty"),
				@ColumnResult(name = "exportQty"),
				@ColumnResult(name = "closeStockTotal"),
				@ColumnResult(name = "monthCumulate"),
				@ColumnResult(name = "monthCumulateTen"),
				@ColumnResult(name = "monthPlan"),
				@ColumnResult(name = "min"),
				@ColumnResult(name = "max"),
				@ColumnResult(name = "lead"),
				@ColumnResult(name = "next"),
				@ColumnResult(name = "isOrderFull"),
				@ColumnResult(name = "isOrderNotFull"),
				@ColumnResult(name = "isGroup"),
				@ColumnResult(name = "poCfQty"),
				@ColumnResult(name = "poCsQty"),
				@ColumnResult(name = "convfact") }),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD3VO", columns = {
				@ColumnResult(name = "staffId"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "saleGroup"),
				@ColumnResult(name = "productInfoCode"),
				@ColumnResult(name = "currentCountCustomer"),
				@ColumnResult(name = "previorCountCustomer"),
				@ColumnResult(name = "currentAmount"),
				@ColumnResult(name = "previorAmount"),
				@ColumnResult(name = "planAmount"),
				@ColumnResult(name = "shopCode"),
				@ColumnResult(name = "superCode"),
				@ColumnResult(name = "superName"),
				@ColumnResult(name = "superShopCode"), 
				@ColumnResult(name = "parentSuperShopCode"),
				@ColumnResult(name = "tbhvCode"), 
				@ColumnResult(name = "tbhvName"), 
				@ColumnResult(name = "lkkh"), 
				@ColumnResult(name = "htkh"), 
				@ColumnResult(name = "thieu"), 
				@ColumnResult(name = "dsbqngay")}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD10VO", columns = {
				@ColumnResult(name = "percentAmount"),
				@ColumnResult(name = "shopCode"),
				@ColumnResult(name = "shopName"),
				@ColumnResult(name = "superShopCode"),
				@ColumnResult(name = "parentSuperShopCode"),
				@ColumnResult(name = "amount"),
				@ColumnResult(name = "amountPlan"),
				@ColumnResult(name = "numCustomer"),
				@ColumnResult(name = "numPlanCustomer"),
				@ColumnResult(name = "percentCustomer"),
				@ColumnResult(name = "xephangDS"),
				@ColumnResult(name = "xephangPP")}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD1_1VO", columns = {
				@ColumnResult(name = "maMien"),
				@ColumnResult(name = "maVung"),
				@ColumnResult(name = "maNPP"),
				@ColumnResult(name = "maSP"),
				@ColumnResult(name = "soDiemLe"),
				@ColumnResult(name = "doanhSo"),
				@ColumnResult(name = "isTong") }),

		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD9VO", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "maNpp"),
				@ColumnResult(name = "itemNo"),
				@ColumnResult(name = "tonDauKy1"),
				@ColumnResult(name = "totalNhap"),
				@ColumnResult(name = "totalXuat"),
				@ColumnResult(name = "cuoiKy1"),
				@ColumnResult(name = "giaTriCuoiKy1"),
				@ColumnResult(name = "soLuongBan"),
				@ColumnResult(name = "giaTriBan"),
				@ColumnResult(name = "soLuongConLai") }),
				
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptDiemNVBHCTVO", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "maNPP"),
				@ColumnResult(name = "tenNPP"),
				@ColumnResult(name = "maNVBH"),
				@ColumnResult(name = "tenNVBH"),
				@ColumnResult(name = "doi"),
				@ColumnResult(name = "diemTheoTuyen"),
				@ColumnResult(name = "diemDoanhSo"),
				@ColumnResult(name = "mucTieu"),
				@ColumnResult(name = "thucHien"),
				@ColumnResult(name = "tyLe"),
				@ColumnResult(name = "lkDiemTheoTuyen"),
				@ColumnResult(name = "lkXepHangTheoTuyen"),
				@ColumnResult(name = "lkMucTieuDoanhSo"),
				@ColumnResult(name = "lkThucHienDoanhSo"),
				@ColumnResult(name = "lkTyLe"),
				@ColumnResult(name = "lkDiemDoanhSo"),
				@ColumnResult(name = "lkXepHangDoanhSo"),
				@ColumnResult(name = "lkSoLanNhoHonBa") }),
		 
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD6VO", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "nhomHang"),
				@ColumnResult(name = "keHoachNam"),
				@ColumnResult(name = "keHoachThang"),
				@ColumnResult(name = "nhapHangTrongNgay"),
				@ColumnResult(name = "nhapHangThang"),
				@ColumnResult(name = "banHangTrongNgay"),
				@ColumnResult(name = "banHangThang"),
				@ColumnResult(name = "luyKeNamTruoc"),
				@ColumnResult(name = "soNgayBan"),
				@ColumnResult(name = "luyKeNamNay") }),
				
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD5VO", columns = {
				@ColumnResult(name = "shopName"), 
				@ColumnResult(name = "cat"),
				@ColumnResult(name = "subCat"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "planAmount"),
				@ColumnResult(name = "khLuyKe"),
				@ColumnResult(name = "khBQNgay"),
				@ColumnResult(name = "orderAmount"),
				@ColumnResult(name = "orderToDateAmount"),
				@ColumnResult(name = "previorAmount"),
				@ColumnResult(name = "duThieuLuyKe"),
				@ColumnResult(name = "phanTramThucHien"),
				@ColumnResult(name = "previorMonthAmount") }),

		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptActionLogVO", columns = {
				@ColumnResult(name = "parentSuperShopCode"),
				@ColumnResult(name = "superShopCode"), 
				@ColumnResult(name = "shopCode"),
				@ColumnResult(name = "shopName"),
				@ColumnResult(name = "superName"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "startTime"),
				@ColumnResult(name = "endTime"),
				@ColumnResult(name = "totalVisit"),
				@ColumnResult(name = "numWrongRouting"),
				@ColumnResult(name = "numCorrectRouting"),
				@ColumnResult(name = "numLess2Minute"),
				@ColumnResult(name = "numBetween2And30Minute"),
				@ColumnResult(name = "numOver30Minute"),
				@ColumnResult(name = "numOver60Minute"),
				@ColumnResult(name = "totalVisitTime") }),
		
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCVT1", columns = {
				@ColumnResult(name = "MAMIEN"),
				@ColumnResult(name = "MAVUNG"),
				@ColumnResult(name = "MANPP"),
				@ColumnResult(name = "TENNPP"),
				@ColumnResult(name = "TENGSNPP"),
				@ColumnResult(name = "MANVBH"),
				@ColumnResult(name = "TENNVBH"),
				@ColumnResult(name = "TONGSOKHACHHANG"),
				@ColumnResult(name = "SOKHACHHANGTHUCGHE"),
				@ColumnResult(name = "KHACHHANGTRONGTUYENKOGHETHAM"),
				@ColumnResult(name = "TONGTHOIGIANLAMVIEC"),
				@ColumnResult(name = "THOIGIANBATDAU"),
				@ColumnResult(name = "THOIGIANKETTHUC"),
				@ColumnResult(name = "SOLANGHETHAMDUOI5PHUT"),
				@ColumnResult(name = "SOLANGHETHAMDUOI30PHUT"),
				@ColumnResult(name = "SOLANGHETHAMDUOI60PHUT"),
				@ColumnResult(name = "SOLANGHETHAMTREN60PHUT"),
				@ColumnResult(name = "TBTHOIGIANGHETHAMKHACHHANG") 
			}),
					
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptDoanhSoBanHangNVBHSKUNgayDataVO", columns = {
				@ColumnResult(name = "maNVBH"), 
				@ColumnResult(name = "tenNVBH"), 
				@ColumnResult(name = "mien"), 
				@ColumnResult(name = "vung"), 
				@ColumnResult(name = "tbhv"), 
				@ColumnResult(name = "maNPP"), 
				@ColumnResult(name = "maGSNPP"), 
				@ColumnResult(name = "doiBanHang"), 
				@ColumnResult(name = "skus"), 
				@ColumnResult(name = "kehoachTT"), 
				@ColumnResult(name = "khachHangThangTruoc"), 
				@ColumnResult(name = "khachHangThangNay"), 
				@ColumnResult(name = "doanhSoThangTruoc"), 
				@ColumnResult(name = "doanhSoThangNay"), 
				@ColumnResult(name = "luyKeKeHoach"), 
				@ColumnResult(name = "phanTramThucHienKH"), 
				@ColumnResult(name = "thieu"), 
				@ColumnResult(name = "doanhSoBQPTHNgay") }),

		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD11_1VO", columns = {
				@ColumnResult(name = "MAMIEN"),
				@ColumnResult(name = "MAVUNG"), 
				@ColumnResult(name = "MANPP"),
				@ColumnResult(name = "MANVBH"), 
				@ColumnResult(name = "MADIEMLE"),
				@ColumnResult(name = "MUC"), 
				@ColumnResult(name = "KEHOACH"),
				@ColumnResult(name = "THUCHIEN"),
				@ColumnResult(name = "PHANTRAM") }),

		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD17VO", columns = {
				@ColumnResult(name = "maMien"), 
				@ColumnResult(name = "maTinhThanh"),
				@ColumnResult(name = "tenTinhThanh"), 
				@ColumnResult(name = "tongSoXa"), 
				@ColumnResult(name = "tongSoXaPSDS"), 
				@ColumnResult(name = "tongSoXaChuaPSDS"), 
				@ColumnResult(name = "dsto500k"),
				@ColumnResult(name = "dsto500kto1m"), 
				@ColumnResult(name = "ds1mto2m"), 
				@ColumnResult(name = "ds2mto5m"), 
				@ColumnResult(name = "ds5mto10m"),
				@ColumnResult(name = "ds10mto50m"),
				@ColumnResult(name = "ds50mto100m"),
				@ColumnResult(name = "ds100mto250m"),
				@ColumnResult(name = "ds250mto"),
				@ColumnResult(name = "tongPSDS") }),

		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD7VO", columns = {
				@ColumnResult(name = "MIEN"),
				@ColumnResult(name = "VUNG"),
				@ColumnResult(name = "TBHV"),
				@ColumnResult(name = "KHTTTHANG"),
				@ColumnResult(name = "LKTHTHANG"),
				@ColumnResult(name = "HTKHTHANG"),
				@ColumnResult(name = "XEPHANGTHANG"),
				@ColumnResult(name = "KHTTNAM"),
				@ColumnResult(name = "LKTHNAM"),
				@ColumnResult(name = "HTKHNAM"),
				@ColumnResult(name = "XEPHANGNAM"),
				@ColumnResult(name = "LKTHNAMTRUOC"),
				@ColumnResult(name = "LKTHTHANGNAMTRUOC"),
				@ColumnResult(name = "TTDSSOVOINAMTRUOC") }),

		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptKDAllowSubCattVO", columns = {
				@ColumnResult(name = "sumCustomer"),
				@ColumnResult(name = "sumAmount"), 
				@ColumnResult(name = "parentSuperShopCode"), 
				@ColumnResult(name = "superShopCode"),
				@ColumnResult(name = "shopCode"), 
				@ColumnResult(name = "productInfoCode")}),
				
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD12DataVO", columns = {
				@ColumnResult(name = "maMien"),
				@ColumnResult(name = "maVung"),
				@ColumnResult(name = "maNPP"),
				@ColumnResult(name = "maNVBH"),
				@ColumnResult(name = "maKH"),
				@ColumnResult(name = "tenKH"),
				@ColumnResult(name = "soNha"),
				@ColumnResult(name = "duong"),
				@ColumnResult(name = "phuongXa"),
				@ColumnResult(name = "quanHuyen"),
				@ColumnResult(name = "tinhThanh"),
				@ColumnResult(name = "dsLuyKe"),
				@ColumnResult(name = "dsNamTruoc"),
				@ColumnResult(name = "dsKeHoach"),
				@ColumnResult(name = "dsPhanTram"),
				@ColumnResult(name = "diem") }),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD18CTVO", columns = {
				@ColumnResult(name = "mienCode"),
				@ColumnResult(name = "catName"),
				@ColumnResult(name = "subCatName"),
				@ColumnResult(name = "numStart"),
				@ColumnResult(name = "numEnd"),
				@ColumnResult(name = "numDiff") }),
				
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD16DataVO", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "tinh"),
				@ColumnResult(name = "huyen"),
				@ColumnResult(name = "phuongXa"),
				@ColumnResult(name = "nhaPP"),
				@ColumnResult(name = "psThangTruoc"),
				@ColumnResult(name = "psThangChon"),
				@ColumnResult(name = "xaMatDi"),
				@ColumnResult(name = "xaPSDS")}),
		
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptDiemPACTTBCTVO", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "gsnpp"),
				@ColumnResult(name = "alphaKHTG"),
				@ColumnResult(name = "alphaKHDat"),
				@ColumnResult(name = "alphaPT"),
				@ColumnResult(name = "alphaDiem"),
				@ColumnResult(name = "vnmKHTG"),
				@ColumnResult(name = "vnmKHDat"),
				@ColumnResult(name = "vnmPT"),
				@ColumnResult(name = "vnmDiem"),
				@ColumnResult(name = "diemPA")}),
				
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD10_2DataVO", columns = {
				@ColumnResult(name = "areaCode"),
				@ColumnResult(name = "locationCode"),
				@ColumnResult(name = "shopCode"),
				@ColumnResult(name = "shopName"),
				@ColumnResult(name = "cusCode"),
				@ColumnResult(name = "cusName"),
				@ColumnResult(name = "cusAddress"),
				@ColumnResult(name = "amountPlan"),
				@ColumnResult(name = "amount"),
				@ColumnResult(name = "percentAmount")}),

		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD10_3VO", columns = {
				@ColumnResult(name = "maMien"),
				@ColumnResult(name = "maVung"),
				@ColumnResult(name = "maNpp"),
				@ColumnResult(name = "tenNpp"),
				@ColumnResult(name = "dsKeHoach"),
				@ColumnResult(name = "dsThucHien"),
				@ColumnResult(name = "dsKhac"),
				@ColumnResult(name = "levelCode"),
				@ColumnResult(name = "kh"),
				@ColumnResult(name = "pp"),
				@ColumnResult(name = "tongKh"),
				@ColumnResult(name = "tongPp")}),

		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD11VO", columns = {
				@ColumnResult(name = "maMien"),
				@ColumnResult(name = "maVung"),
				@ColumnResult(name = "maNpp"),
				@ColumnResult(name = "maNvbh"),
				@ColumnResult(name = "maDiemLe"),
				@ColumnResult(name = "muc"),
				@ColumnResult(name = "keHoach"),
				@ColumnResult(name = "thucHien"),
				@ColumnResult(name = "thuaThieu"),
				@ColumnResult(name = "flag")}),	

		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD16_1VO", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "maTinh"),
				@ColumnResult(name = "tinh"),
				@ColumnResult(name = "huyen"),
				@ColumnResult(name = "phuongXa"),
				@ColumnResult(name = "flag")}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD19_1VO", columns = {
				@ColumnResult(name = "maMien"),
				@ColumnResult(name = "maVung"),
				@ColumnResult(name = "maNVGS"),
				@ColumnResult(name = "tenNVGS"),
				@ColumnResult(name = "nhomNVGS"),
				@ColumnResult(name = "numBadScore"),
				@ColumnResult(name = "dayAmt"),
				@ColumnResult(name = "dayAmtPlan")}),
	
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD13VO", columns = {
				@ColumnResult(name = "maMien"),
				@ColumnResult(name = "maVung"),
				@ColumnResult(name = "maNPP"),
				@ColumnResult(name = "tenNPP"),
				@ColumnResult(name = "maKH"),
				@ColumnResult(name = "tenKH"),
				@ColumnResult(name = "soNha"),
				@ColumnResult(name = "duong"),
				@ColumnResult(name = "phuongXa"),
				@ColumnResult(name = "quanHuyen"),
				@ColumnResult(name = "tinhThanh"),
				@ColumnResult(name = "slTu"),
				@ColumnResult(name = "slDiemBan"),
				@ColumnResult(name = "kpsds1ThangTruoc"),
				@ColumnResult(name = "kpsds2ThangTruoc"),
				@ColumnResult(name = "kpsds3ThangTruoc"),
				@ColumnResult(name = "rptMonth"),
				@ColumnResult(name = "amount")}),
				
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD19DataVO", columns = {
				@ColumnResult(name = "maVung"), 
				@ColumnResult(name = "maTBHV"),
				@ColumnResult(name = "tenTBHV"),
				@ColumnResult(name = "maNVGS"),
				@ColumnResult(name = "tenNVGS"),
				@ColumnResult(name = "soWWKH"), 
				@ColumnResult(name = "soWWTH"),
				@ColumnResult(name = "diemPA"),
				@ColumnResult(name = "nho3diem"),
				@ColumnResult(name = "rptDate"), 
				@ColumnResult(name = "diem") }),

		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD14DetailVO", columns = {
				@ColumnResult(name = "maMien"), 
				@ColumnResult(name = "maVung"),
				@ColumnResult(name = "tbhv"),
				@ColumnResult(name = "maNPP"),
				@ColumnResult(name = "tenNPP"),
				@ColumnResult(name = "diemLeT12"), 
				@ColumnResult(name = "rptMonth"),
				@ColumnResult(name = "mucTieuBqNhomSKUs"),
				@ColumnResult(name = "nhomSKUs"),
				@ColumnResult(name = "bqNhomSKUs"),
				@ColumnResult(name = "diemPA") }),
				
		/**
		 * Bao cao doanh thu tong hop cua nhan vien
		 * @author tungtt
		 */
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptDTTHNVRecordVO", columns = {
				@ColumnResult(name = "staffCode"), 
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "orderDate"),
				@ColumnResult(name = "moneyForDay"),
				@ColumnResult(name = "remainForDay"), 
				@ColumnResult(name = "discountForDay"),
				@ColumnResult(name = "promotionMoneyForDay"),
				@ColumnResult(name = "promotionStockForDay"),
				@ColumnResult(name = "revenueForDay"),
				@ColumnResult(name = "salesForDay") }),
			
		/**
		 * Bao cao xuat ban ton theo nhan vien
		 * @author tungtt
		 */
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptXBTNVRecord", columns = {
				@ColumnResult(name = "staffCode"), 
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "price"),
				@ColumnResult(name = "exportQuantity"), 
				@ColumnResult(name = "exportAmount"),
				@ColumnResult(name = "sellQuantity"),
				@ColumnResult(name = "sellAmount"),
				@ColumnResult(name = "promoteQuantity"),
				@ColumnResult(name = "promoteAmount"),
				@ColumnResult(name = "stockQuantity"),
				@ColumnResult(name = "stockAmount")}),
		/**
		 * Phieu thu
		 * @author tungtt
		 */
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.PTDetail", columns = {
				@ColumnResult(name = "shortCode"), 
				@ColumnResult(name = "customerName"),
				@ColumnResult(name = "payReceivedNumber"),
				@ColumnResult(name = "createDate"),
				@ColumnResult(name = "amount")}),
		
		// vuonghn
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptSaleOrderDetail2VO", columns = {
				@ColumnResult(name = "soID"), 
				@ColumnResult(name = "customerName"), 
				@ColumnResult(name = "customerAddress"),
				@ColumnResult(name = "deliveryCode"),
				@ColumnResult(name = "deliveryName"),
				@ColumnResult(name = "orderNumber"),
				@ColumnResult(name = "orderDate"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "discount"),
				@ColumnResult(name = "total"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "numThung"),
				@ColumnResult(name = "numLe"),
				@ColumnResult(name = "quantity"),
				@ColumnResult(name = "price"),
				@ColumnResult(name = "amount"),}),
										
		
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.RptTDBHTMHProductDateStaffFollowCustomerVO", columns = {
				@ColumnResult(name = "shopId"), 
				@ColumnResult(name = "productId"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "createDate"), 
				@ColumnResult(name = "price"),
				@ColumnResult(name = "quantity"),
				@ColumnResult(name = "amount"),
				@ColumnResult(name = "customerId"),
				@ColumnResult(name = "customerCode"),
				@ColumnResult(name = "customerName"),
				@ColumnResult(name = "customerAddress"),
				@ColumnResult(name = "staffId"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "orderNumber"),
				@ColumnResult(name = "invoiceNumber"),
				@ColumnResult(name = "sumStaffQuantity"),
				@ColumnResult(name = "sumStaffAmount"),
				@ColumnResult(name = "sumDayQuantity"),
				@ColumnResult(name = "sumDayAmount"),
				@ColumnResult(name = "sumProductAndPriceQuantity"),
				@ColumnResult(name = "sumProductAndPriceAmount")}),
			
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCXNKNVBHDetailVO", columns = {
				@ColumnResult(name = "shopId"), 
				@ColumnResult(name = "stockTransDate"),
				@ColumnResult(name = "staffId"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "stockTransCode"),
				@ColumnResult(name = "productId"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productName"),						
				@ColumnResult(name = "price"),
				@ColumnResult(name = "lot"),
				@ColumnResult(name = "quantityCheckLot"),
				@ColumnResult(name = "quantityNotCheckLot"),
				@ColumnResult(name = "convfact"),
				@ColumnResult(name = "checkLot"),
				@ColumnResult(name = "soThung"),					
				@ColumnResult(name = "soLe"),					
				@ColumnResult(name = "thanhTien")}),
		/*
		 * pHIEU XUAT KHO KIEM VAN CHUYEN NOI BO
		 * */
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptPXKKVCNB_DataConvert", columns = {
				@ColumnResult(name = "ngay_hien_tai"), 
				@ColumnResult(name = "ten_nguoi_van_chuyen"),
				@ColumnResult(name = "don_vi_ban_hang"),
				@ColumnResult(name = "ma_so_thue"),
				@ColumnResult(name = "dia_chi_kho"),
				@ColumnResult(name = "dia_chi_nhan_vien"),
				@ColumnResult(name = "so_dien_thoai"),
				@ColumnResult(name = "so_tai_khoan"),
				@ColumnResult(name = "so_xe"),						
				@ColumnResult(name = "ma_san_pham"),
				@ColumnResult(name = "ten_san_pham"),
				@ColumnResult(name = "so_thung"),
				@ColumnResult(name = "so_le"),
				@ColumnResult(name = "don_gia"),
				@ColumnResult(name = "thanh_tien"),}),
	/* Doanh thu ban hang trong ngay theo NVBH
	 * namlb */
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.RptDTBHTNTHVBHRecordOrderVO", columns = {
				@ColumnResult(name = "staffId"), 
				@ColumnResult(name = "staffCode"), 
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "customerCode"),
				@ColumnResult(name = "customerName"),
				@ColumnResult(name = "customerAddress"), 
				@ColumnResult(name = "revenue"),
				@ColumnResult(name = "cash"),
				@ColumnResult(name = "debit"),
				@ColumnResult(name = "discount"),
				@ColumnResult(name = "moneyDiscount") ,
				@ColumnResult(name = "productDiscount") ,
				@ColumnResult(name = "SKU") ,
				@ColumnResult(name = "money") ,
				@ColumnResult(name = "dateOrder") ,
				@ColumnResult(name = "sumRevenueDate") ,
				@ColumnResult(name = "sumMoneyDiscountDate") ,
				@ColumnResult(name = "sumProductDiscountDate") ,
				@ColumnResult(name = "sumMoneyDate"),
				@ColumnResult(name = "sumDiscountDate"),
				@ColumnResult(name = "sumSKUDate"),
				@ColumnResult(name = "sumDebitDate"),
				@ColumnResult(name = "sumCashDate"),
				@ColumnResult(name = "sumRevenueStaff"),
				@ColumnResult(name = "sumMoneyDiscountStaff"),
				@ColumnResult(name = "sumProductDiscountStaff"),
				@ColumnResult(name = "sumMoneyStaff"),
				@ColumnResult(name = "sumDiscountStaff"),
				@ColumnResult(name = "sumSKUStaff"),
				@ColumnResult(name = "sumDebitStaff"),
				@ColumnResult(name = "sumCashStaff"),
				
		}),
		/* Phieu tra hang cua khach hang
		 * namlb */					
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptPTHCKHProductVO", columns = {
				@ColumnResult(name = "soId"),
				@ColumnResult(name = "sodId"),
				@ColumnResult(name = "orderDate"),
				@ColumnResult(name = "orderNumber"),
				@ColumnResult(name = "rootOrderNumber"),
				@ColumnResult(name = "amount"),
				@ColumnResult(name = "deliveryId"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "deliveryCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "deliveryName"),
				@ColumnResult(name = "quantity"),
				@ColumnResult(name = "detailAmount"),
				@ColumnResult(name = "discountAmount"),
				@ColumnResult(name = "priceValue"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "shortCode"),
				@ColumnResult(name = "customerCode"),
				@ColumnResult(name = "customerName"),
				@ColumnResult(name = "customerAddress"),
				@ColumnResult(name = "phone"),
				@ColumnResult(name = "mobiphone"),
				@ColumnResult(name = "adjust"),
				@ColumnResult(name = "discountOrder"),
				@ColumnResult(name = "totalPay"),
				@ColumnResult(name = "numLe"),
				@ColumnResult(name = "numThung"),
				
		}),
		/* Bao cao chi tiet chung tu mua hang
		 * namlb */					
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBKCTCTMHProductRecordVO", columns = {
				@ColumnResult(name = "saleOrderNumber"),
				@ColumnResult(name = "purchaseOrder"),
				@ColumnResult(name = "orderDate"),
				@ColumnResult(name = "poConfirm"),
				@ColumnResult(name = "invoiceNumber"),
				@ColumnResult(name = "importDate"),
				@ColumnResult(name = "status"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "quantity"),
				@ColumnResult(name = "price"),
				//@ColumnResult(name = "total"),
		}),
		/* Bao cao khong ghe tham khach hang trong tuyen
		 * namlb */					
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCNVKGTKHTTRecordVO", columns = {
				@ColumnResult(name = "areaCode"),
				@ColumnResult(name = "locationCode"),
				@ColumnResult(name = "shopCode"),
				@ColumnResult(name = "shopName"),
				@ColumnResult(name = "staffOwnerName"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "visitDate"),
				@ColumnResult(name = "visitDay"),
				@ColumnResult(name = "customerCode"),
				@ColumnResult(name = "customerName"),
				@ColumnResult(name = "customerAddress"),
		}),
		/* 
		 * Bao cao danh sach khach hang
		 * namlb */
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.CustomerVO", columns = {
				@ColumnResult(name = "shortCode"),
				@ColumnResult(name = "customerName"),
				@ColumnResult(name = "status"),
				@ColumnResult(name = "address"),
				@ColumnResult(name = "street"),
				@ColumnResult(name = "wardName"),
				@ColumnResult(name = "districtName"),
				@ColumnResult(name = "provinceName"),
				@ColumnResult(name = "areaName"),
				@ColumnResult(name = "mobiphone"),
				@ColumnResult(name = "channelTypeCode"),
				@ColumnResult(name = "maxDebitDate"),
				@ColumnResult(name = "saleAmount"),
				@ColumnResult(name = "createDate"),
				@ColumnResult(name = "updateDate"),
				@ColumnResult(name = "lastApproveOrder"),
				@ColumnResult(name = "invoiceTax"),
				@ColumnResult(name = "invoiceNumberAccount"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "shopName"),
				@ColumnResult(name = "isRouting"),
		}),		
		
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptPayDisplayProgrameRecordProductVO", columns = {
				@ColumnResult(name = "displayProgrameCode"),
				@ColumnResult(name = "displayProgrameName"),
				@ColumnResult(name = "fDate"),
				@ColumnResult(name = "tDate"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "promotionQuantity"),
				@ColumnResult(name = "price"),
		}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptKD10_1CTVO", columns = {
				@ColumnResult(name = "mien"), 
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "maNPP"), 
				@ColumnResult(name = "tenNPP"),
				@ColumnResult(name = "maKH"),
				@ColumnResult(name = "diaChi"),
				@ColumnResult(name = "dsKH"),
				@ColumnResult(name = "dsTH"),
				@ColumnResult(name = "ptTH"),
				@ColumnResult(name = "mucTieuF4"),
				@ColumnResult(name = "thucHienF4"),
				@ColumnResult(name = "lkPTTH") }),

		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD21VO", columns = {
				@ColumnResult(name = "maMien"), 
				@ColumnResult(name = "maVung"),
				@ColumnResult(name = "maNpp"), 
				@ColumnResult(name = "maCT"),
				@ColumnResult(name = "tenCT"), 
				@ColumnResult(name = "maMuc"),
				@ColumnResult(name = "maNhom"), 
				@ColumnResult(name = "dsKH"),
				@ColumnResult(name = "cpKH"),
				@ColumnResult(name = "pbKH"),
				@ColumnResult(name = "dsTH"),
				@ColumnResult(name = "cpTH"),
				@ColumnResult(name = "pbTH"),
				@ColumnResult(name = "ptDS"),
				@ColumnResult(name = "ptCP") }),
				
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD15DataVO", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "khuVuc"),
				@ColumnResult(name = "maNPP"),
				@ColumnResult(name = "maKH"),
				@ColumnResult(name = "tenKH"),
				@ColumnResult(name = "diaChiKH"),
				@ColumnResult(name = "kenh"),
				@ColumnResult(name = "thang"),
				@ColumnResult(name = "phatSinh") }),
				
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.PoAutoVO", columns = {
				@ColumnResult(name = "amount"),
				@ColumnResult(name = "convfact"),
				@ColumnResult(name = "dayPlan"),
				@ColumnResult(name = "dayReservePlan"),
				@ColumnResult(name = "dayReserveReal"),
				@ColumnResult(name = "expPovnmQty"),
				@ColumnResult(name = "expQty"),
				@ColumnResult(name = "expSaleorderQty"),
				@ColumnResult(name = "expStocktransQty"),
				@ColumnResult(name = "expVansaleQty"),
				@ColumnResult(name = "goPoConfirmQty"),
				@ColumnResult(name = "impPovnmQty"),
				@ColumnResult(name = "impQty"),
				@ColumnResult(name = "impSaleorderQty"),
				@ColumnResult(name = "impStockTransQty"),
				@ColumnResult(name = "lead"),
				@ColumnResult(name = "maxsf"),
				@ColumnResult(name = "monthCumulate"),
				@ColumnResult(name = "monthPlan"),
				@ColumnResult(name = "next"),
				@ColumnResult(name = "openStockTotal"),
				@ColumnResult(name = "percentage"),
				@ColumnResult(name = "priceId"),
				@ColumnResult(name = "priceValue"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productId"),
				@ColumnResult(name = "productInfoCode"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "quantity"),
				@ColumnResult(name = "requimentStock"),
				@ColumnResult(name = "safetyStockMin"),
				@ColumnResult(name = "stockPoDvkh"),
				@ColumnResult(name = "stockQty"),
				@ColumnResult(name = "poImport"),
				@ColumnResult(name = "poExport"),
				@ColumnResult(name = "poStock"),
				@ColumnResult(name = "poMonthCumulate"),
				@ColumnResult(name = "poBoxQuantity"),
				@ColumnResult(name = "warning") ,
				@ColumnResult(name = "grossWeight")}),
				
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCKD2Detail", columns = {
				@ColumnResult(name = "tenNvbh"),
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "truongBanHangVung"),
				@ColumnResult(name = "maNpp"),
				@ColumnResult(name = "gsNpp"),
				@ColumnResult(name = "maNvbh"),
				@ColumnResult(name = "doiBanHang"),
				@ColumnResult(name = "nhan"),
				@ColumnResult(name = "khtt"),
				@ColumnResult(name = "khPsdsThangTruoc"),
				@ColumnResult(name = "khPsdsThangNay"),
				@ColumnResult(name = "dsThangNay"),
				@ColumnResult(name = "dsThangTruoc"),
				@ColumnResult(name = "doanhSoBinhQuanNgay"),
				@ColumnResult(name = "phanTramThucHienKeHoach"),
				@ColumnResult(name = "luyKeKH"),
				@ColumnResult(name = "thieu")
				}), 
		//CangND : BAO CAO THANH TOAN CUA KHACH HANG		
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.Rpt_PO_CTTT_MAPPINGVO", columns = {
				@ColumnResult(name = "ngay_hop_dong"),
				@ColumnResult(name = "so_hop_dong"),
				@ColumnResult(name = "loai_no"),
				@ColumnResult(name = "ngay_thanh_toan"),
				@ColumnResult(name = "loai_tt"),
				@ColumnResult(name = "so_cttt"),
				@ColumnResult(name = "tien_tt"),
				@ColumnResult(name = "tien_hop_dong"),
				@ColumnResult(name = "ma_kh"),
				@ColumnResult(name = "nvtt")
				}),
			//CangND : BAO CAO XUAT NHAP TON CHI TIET	
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.Rpt_TDXNTCTVO", columns = {
				@ColumnResult(name = "ma_nganh"),
				@ColumnResult(name = "ten_hang"),
				@ColumnResult(name = "ma_hang"),
				@ColumnResult(name = "don_gia"),
				@ColumnResult(name = "so_luong_ton_dau"),
				@ColumnResult(name = "thung_le_ton_dau"),
				@ColumnResult(name = "tong_tien_ton_dau"),
				@ColumnResult(name = "so_luong_ban"),
				@ColumnResult(name = "thung_le_ban"),
				@ColumnResult(name = "tong_tien_ban"),
				@ColumnResult(name = "so_luong_khuyen_mai"),
				@ColumnResult(name = "thung_le_khuyen_mai"),
				@ColumnResult(name = "tong_tien_khuyen_mai"),
				@ColumnResult(name = "so_luong_mua_hang"),
				@ColumnResult(name = "thung_le_mua_hang"),
				@ColumnResult(name = "tong_tien_mua_hang"),
				@ColumnResult(name = "so_luong_tra_hang"),
				@ColumnResult(name = "thung_le_tra_hang"),
				@ColumnResult(name = "tong_tien_tra_hang"),
				@ColumnResult(name = "so_luong_nhap_kho"),
				@ColumnResult(name = "thung_le_nhap_kho"),
				@ColumnResult(name = "so_luong_xuat_kho"),
				@ColumnResult(name = "thung_le_xuat_kho"),
				@ColumnResult(name = "so_luong_ton_cuoi"),
				@ColumnResult(name = "thung_le_ton_cuoi"),
				@ColumnResult(name = "tong_tien_cuoi")
				}),
				

				
		//BAO CAO GIAM SAT KHACH HANG 1		
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCTGGTCNVBH_1VO", columns = {
						@ColumnResult(name = "areaCode"),
						@ColumnResult(name = "locationCode"),
						@ColumnResult(name = "shopCode"),
						@ColumnResult(name = "shopName"),
						@ColumnResult(name = "staffOwnerName"),
						@ColumnResult(name = "staffCode"),
						@ColumnResult(name = "staffName"),
						@ColumnResult(name = "afterStartTime"),
						@ColumnResult(name = "beforeEndTime"),
						@ColumnResult(name = "lowerThan5Min"),
						@ColumnResult(name = "lowerThan30Min"),
						@ColumnResult(name = "lowerThan60Min"),
						@ColumnResult(name = "largeThan60Min"),
						@ColumnResult(name = "avgTimePerCustomer")
						}),
		//VuongHN Bao cao chi tiet khuyen mai
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCCTKM_RecordDetailVO", columns={
			@ColumnResult(name = "shopId"),
			@ColumnResult(name = "shopName"),
			@ColumnResult(name = "shopAddress"),
			@ColumnResult(name = "promotionProgramCode"),
			@ColumnResult(name = "promotionProgramDecription"),
			@ColumnResult(name = "orderId"),
			@ColumnResult(name = "orderDate"),
			@ColumnResult(name = "staffName"),
			@ColumnResult(name = "staffCode"),
			@ColumnResult(name = "customerCode"),
			@ColumnResult(name = "customerType"),
			@ColumnResult(name = "productCode"),
			@ColumnResult(name = "quantity"),
			@ColumnResult(name = "freeItem"),
			@ColumnResult(name = "productCodeFree"),
			@ColumnResult(name = "quantityFree"),
		}),
		//VuongHN Bang ke chung tu hang hoa mua vao
		@SqlResultSetMapping(name="ths.dms.core.entities.vo.rpt.RptBKCTHHMV_RecordVO", columns={
				@ColumnResult(name = "shopName"),
				@ColumnResult(name = "shopAddress"),
				@ColumnResult(name = "poNumber"),
				@ColumnResult(name = "invoiceNumber"),
				@ColumnResult(name = "poDate"),
				@ColumnResult(name = "producer"),
				@ColumnResult(name = "note"),
				@ColumnResult(name = "total"),
				@ColumnResult(name = "discount"),
				@ColumnResult(name = "vat"),
				@ColumnResult(name = "amount"),
		}),
		//VuongHN Phieu doi tra hang
		@SqlResultSetMapping(name="ths.dms.core.entities.vo.rpt.RptSaleOrderDetailVO", columns={
				@ColumnResult(name = "soId"),
				@ColumnResult(name = "sodId"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "convfact"),
				@ColumnResult(name = "price"),
				@ColumnResult(name = "quantity"),
				@ColumnResult(name = "amount"),
				@ColumnResult(name = "promotionName"),
				@ColumnResult(name = "promotionCode"),
				@ColumnResult(name = "carNumber"),
				@ColumnResult(name = "customerName"),
				@ColumnResult(name = "customerCode"),
				@ColumnResult(name = "customerShortCode"),
				@ColumnResult(name = "customerAddress"),
				@ColumnResult(name = "customerPhone"),
				@ColumnResult(name = "shopName"),
				@ColumnResult(name = "shopPhone"),
				@ColumnResult(name = "shopTaxNum"),
				@ColumnResult(name = "shopAddress"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "deliveryCode"),
				@ColumnResult(name = "deliveryName"),
				@ColumnResult(name = "orderDate"),
				@ColumnResult(name = "invoiceNumber"),
				@ColumnResult(name = "invoiceId"),
				@ColumnResult(name = "numThung"),
				@ColumnResult(name = "numLe"),
		}),
		//SangTN: VT3 - Bao cao ghe tham khach hang
		@SqlResultSetMapping(name="ths.dms.core.entities.vo.TimeVisitCustomerVO", columns={
				@ColumnResult(name = "startTime"),
				@ColumnResult(name = "endTime"),
				@ColumnResult(name = "toDate"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "shopCode"),
				@ColumnResult(name = "shopName"),
				@ColumnResult(name = "superName"),
				@ColumnResult(name = "customerCode"),
				@ColumnResult(name = "customerName"),
				@ColumnResult(name = "customerAddress"),
				@ColumnResult(name = "superShopCode"),				
				@ColumnResult(name = "parentSuperShopCode"),
				@ColumnResult(name = "CLOSE"),
				@ColumnResult(name = "distance"),
				@ColumnResult(name = "tuyen"),
				@ColumnResult(name = "phut"),
				@ColumnResult(name = "giay"),
		}),
		// vuongmq bao cao VT9
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCVT9", columns = {
				@ColumnResult(name = "mien"), 
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "npp"),
				@ColumnResult(name = "tenNPP"),				
				@ColumnResult(name = "maNV"), 
				@ColumnResult(name = "tenNV"),	
				@ColumnResult(name = "maKH"),
				@ColumnResult(name = "tenKH"),				
				@ColumnResult(name = "maCT"),
				@ColumnResult(name = "tenCT"),
				@ColumnResult(name = "soLanChup"),
				@ColumnResult(name = "soHinhDat"),
				}),
		//SangTN: BCKH - Danh sach khach hang theo tuyen ban hang
		@SqlResultSetMapping(name="ths.dms.core.entities.vo.rpt.RptCustomerByRoutingDataVO", columns={
				@ColumnResult(name = "staffId"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "staffStatus"),
				@ColumnResult(name = "customerCode"),
				@ColumnResult(name = "customerName"),
				@ColumnResult(name = "customerAddress"),
				@ColumnResult(name = "customerDistrict"),
				@ColumnResult(name = "customerPrecinct"),
				@ColumnResult(name = "seq"),
				@ColumnResult(name = "mobiphone"),
				@ColumnResult(name = "dateName"),
				@ColumnResult(name = "customerType"),
				@ColumnResult(name = "customerStatus"),
				@ColumnResult(name = "displayPosition"),
				@ColumnResult(name = "displayName"),
				//@ColumnResult(name = "interval"),
				@ColumnResult(name = "week1"),
				@ColumnResult(name = "week2"),
				@ColumnResult(name = "week3"),
				@ColumnResult(name = "week4"),
		}),
		//SangTN: BCDTBH - Theo doi doanh so theo khach hang nganh hang ma hang
		@SqlResultSetMapping(name="ths.dms.core.entities.vo.rpt.RptBCTDDSTKHNHMHDetailVO", columns={
				@ColumnResult(name = "customerId"),
				@ColumnResult(name = "customerCode"),
				@ColumnResult(name = "customerName"),
				@ColumnResult(name = "shortCode"),
				@ColumnResult(name = "catId"),
				@ColumnResult(name = "catCode"),
				@ColumnResult(name = "catName"),
				@ColumnResult(name = "subCatId"),
				@ColumnResult(name = "subCatCode"),
				@ColumnResult(name = "subCatName"),
				@ColumnResult(name = "brandId"),
				@ColumnResult(name = "brandCode"),
				@ColumnResult(name = "brandName"),
				@ColumnResult(name = "flavourId"),
				@ColumnResult(name = "flavourCode"),
				@ColumnResult(name = "flavourName"),
				@ColumnResult(name = "productId"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "price"),
				@ColumnResult(name = "quantity"),
				@ColumnResult(name = "amountMoney"),				
		}),
		//TUNGTT - Bao cao bang ke chi tiet hoa don GTGT
		@SqlResultSetMapping(name="ths.dms.core.entities.vo.rpt.RptInvDetailStatDetailVO", columns={
				@ColumnResult(name = "invoiceNumber"),
				@ColumnResult(name = "invoiceDate"),
				@ColumnResult(name = "shortCode"),
				@ColumnResult(name = "customerName"),
				@ColumnResult(name = "address"),
				@ColumnResult(name = "custTaxNumber"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "quantity"),
				@ColumnResult(name = "uom1"),
				@ColumnResult(name = "vat"),
				@ColumnResult(name = "discount"),
				@ColumnResult(name = "amount"),
				@ColumnResult(name = "taxAmount"),				
				@ColumnResult(name = "finalAmount"),
				@ColumnResult(name = "status"),
				@ColumnResult(name = "orderNumber"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "createDateStr")
		}),
		//TUNGTT - Bao cao theo doi chi tieu doanh so
		@SqlResultSetMapping(name="ths.dms.core.entities.vo.rpt.RptTDCTDSVODetail", columns={
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "price"),
				@ColumnResult(name = "quantityPlan"),
				@ColumnResult(name = "amountPlan"),
				@ColumnResult(name = "quantityReal"),
				@ColumnResult(name = "amountReal"),
				@ColumnResult(name = "percent")
		}),
		//SangTN - Bao cao phieu tra hang theo nhan vien giao hang
		@SqlResultSetMapping(name="ths.dms.core.entities.vo.rpt.RptReturnSaleOrderAllowDeliveryVO", columns={
				//@ColumnResult(name = "orderDate"),
				@ColumnResult(name = "sumDiscountAmount"),//
				@ColumnResult(name = "amount"),//
				@ColumnResult(name = "discountAmount"),//
				//@ColumnResult(name = "discountPercent"),
				@ColumnResult(name = "promotionAmount"),
				@ColumnResult(name = "price"),//
				@ColumnResult(name = "deliveryCode"),//
				@ColumnResult(name = "deliveryName"),//
				@ColumnResult(name = "productCode"),//
				@ColumnResult(name = "productName"),//
				@ColumnResult(name = "quantity"),//
				@ColumnResult(name = "promotionQuantity"),//
				@ColumnResult(name = "convfact"),				
				@ColumnResult(name = "lot")				
		}),
		//TUNGTT: Bao cao ngay
		@SqlResultSetMapping(name="ths.dms.core.entities.vo.rpt.RptBCNVO", columns={
				@ColumnResult(name = "staffName"),
				@ColumnResult(name = "staffCode"),
				@ColumnResult(name = "staffType"),
				@ColumnResult(name = "amountDay"),
				@ColumnResult(name = "amountMonth"),
				@ColumnResult(name = "planMonth"),
				@ColumnResult(name = "progress"),
				@ColumnResult(name = "differenceProgress"),
				@ColumnResult(name = "amountPlanMonth"),
				@ColumnResult(name = "amountSale"),
				@ColumnResult(name = "customerTarget"),
				@ColumnResult(name = "f4Day"),
				@ColumnResult(name = "f4Month"),
				@ColumnResult(name = "f4LastMonth"),
				@ColumnResult(name = "sumCustomer"),				
				@ColumnResult(name = "notDistributed"),
				@ColumnResult(name = "inTarget"),
				@ColumnResult(name = "outTarget"),
				@ColumnResult(name = "payLastDay"),
				@ColumnResult(name = "handOrder"),
				@ColumnResult(name = "confirm"),
				@ColumnResult(name = "distribution")
		}),
		
		//vuonghn VT7 - Bao cao ghe tham khach hang
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCVT7", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "shop_code"),
				@ColumnResult(name = "shop_NAME"),
				@ColumnResult(name = "STAFF_CODE"),
				@ColumnResult(name = "NAME"),
				@ColumnResult(name = "sokhkh"),
				@ColumnResult(name = "sokhthucghe"),
				@ColumnResult(name = "thoigianlamviec"),
				@ColumnResult(name = "START_TIME"),
				@ColumnResult(name = "END_TIME"),
				@ColumnResult(name = "solanghethamduoi5phut"),
				@ColumnResult(name = "solanghethamduoi10phut"),
				@ColumnResult(name = "solanghethamduoi30phut"),
				@ColumnResult(name = "solanghethamduoi60phut"),
				@ColumnResult(name = "solanghethamhon60phut"),
				@ColumnResult(name = "THOIGIANTRUNGBINH")
		}),
		//vuonghn: VT10 - Bao cao ket qua cham trung bay
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCVT10", columns = {
				@ColumnResult(name = "mien"), 
				@ColumnResult(name = "maGSMT"),
				@ColumnResult(name = "tenGSMT"),
				@ColumnResult(name = "maKH"),				
				@ColumnResult(name = "tenKH"), 
				@ColumnResult(name = "ngay"),	
				@ColumnResult(name = "maMau"),
				@ColumnResult(name = "maCT"),
				@ColumnResult(name = "tenCT"),
				@ColumnResult(name = "loai"),
				@ColumnResult(name = "quydinh"),
				@ColumnResult(name = "thucte"),
		}),
		//vuonhgn: VT12 - Bao cao xoa khach hang tham gia CTTB
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptBCVT12", columns = {
				@ColumnResult(name = "maCTTB"), 
				@ColumnResult(name = "shopCode"),
				@ColumnResult(name = "customerCode"),
				@ColumnResult(name = "customerName"),				
				@ColumnResult(name = "address"), 
				@ColumnResult(name = "thang"),	
				@ColumnResult(name = "ngayTao"),
				@ColumnResult(name = "ngayXoa"),
		}),
		//tungtt: Bang tong hop tra hang
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.Rpt3_16_BTHDH", columns = {
				@ColumnResult(name = "orderDate"), 
				@ColumnResult(name = "customerCode"),
				@ColumnResult(name = "oddName"),
				@ColumnResult(name = "houseNumber"),
				@ColumnResult(name = "streetName"),	
				@ColumnResult(name = "district"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "amount"), 
				@ColumnResult(name = "orderNumber"),
				@ColumnResult(name = "total"),
				@ColumnResult(name = "groupAmount"),				
				@ColumnResult(name = "allowAmount"), 
				@ColumnResult(name = "suggestAmount"),
		}),
		//tungtt: BC KD9
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptKD9_Detail", columns = {
				@ColumnResult(name = "mien"), 
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "npp"),
				@ColumnResult(name = "itemNo"),
				@ColumnResult(name = "tonDauKy1"),	
				@ColumnResult(name = "totalNhap"),
				@ColumnResult(name = "totalXuat"),
				@ColumnResult(name = "cuoiKy1"), 
				@ColumnResult(name = "giaTriCKy1"),
				@ColumnResult(name = "soLuongBan"),
				@ColumnResult(name = "giaTriBan"),				
				@ColumnResult(name = "soLuongConLai"),
		}),
		//tungtt: BC Theo doi chi tiet khuyen mai theo chuong trinh
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.Rpt3_13_CTKMTCT_Detail", columns = {
				@ColumnResult(name = "programName"), 
				@ColumnResult(name = "orderDate"),
				@ColumnResult(name = "staff"),
				@ColumnResult(name = "customer"),
				@ColumnResult(name = "product"),	
				@ColumnResult(name = "quantityPromotion"),
				@ColumnResult(name = "amountPromotion"),
				@ColumnResult(name = "quantityMPromotion"),
				@ColumnResult(name = "moneyPromotion"), 
				@ColumnResult(name = "total"),
		}),
		//tungtt: BC Theo doi chi tiet khuyen mai theo chuong trinh - nhan vien
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.Rpt3_14_CTKMTCTNV_Detail", columns = {
				@ColumnResult(name = "programName"), 
				@ColumnResult(name = "staff"),
				@ColumnResult(name = "quantityPromotion"),
				@ColumnResult(name = "amountPromotion"),
				@ColumnResult(name = "moneyPromotion"), 
				@ColumnResult(name = "total"),
		}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptDM1_1TTKH", columns = {
				@ColumnResult(name = "mien"), 
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "maNPP"),
				@ColumnResult(name = "tenNPP"),
				@ColumnResult(name = "maKH"),
				@ColumnResult(name = "tenKH"), 
				@ColumnResult(name = "loaiKH"), 
				@ColumnResult(name = "soNha"),
				@ColumnResult(name = "duong"),
				@ColumnResult(name = "dienThoai"),
				@ColumnResult(name = "diDong"), 
				@ColumnResult(name = "maPhuongXa"), 
				@ColumnResult(name = "tenPhuongXa"),
				@ColumnResult(name = "maQuanHuyen"),
				@ColumnResult(name = "tenQuanHuyen"),
				@ColumnResult(name = "maTinhThanh"), 
				@ColumnResult(name = "tenTinhThanh"),
				@ColumnResult(name = "doTrungThanh"),
				@ColumnResult(name = "viTri"),
				@ColumnResult(name = "trangThai"),
				@ColumnResult(name = "ngayTao"), 
				@ColumnResult(name = "maSoThue"), 
				@ColumnResult(name = "soTaiKhoan"),
				@ColumnResult(name = "hanNo"),
				@ColumnResult(name = "mucTB"), 
				@ColumnResult(name = "mucDS"),
		}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptTDTTCN_NVTT", columns = {
				@ColumnResult(name = "maKH"),
				@ColumnResult(name = "tenKH"),
				@ColumnResult(name = "maNV"),
				@ColumnResult(name = "tenNV"),
				@ColumnResult(name = "ngayHD"),
				@ColumnResult(name = "soHD"),
				@ColumnResult(name = "loaiNo"),
				@ColumnResult(name = "ngayTT"),
				@ColumnResult(name = "loaiTT"),
				@ColumnResult(name = "soCTTT"),
				@ColumnResult(name = "tienHD"),
				@ColumnResult(name = "tienTT"),
				@ColumnResult(name = "tienCK"),
				@ColumnResult(name = "lanTT"),
				@ColumnResult(name = "tienConNo")
		}),
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.RptTDTTCNData", columns = {
				@ColumnResult(name = "maKH"),
				@ColumnResult(name = "tenKH"),
				@ColumnResult(name = "maNVTT"),
				@ColumnResult(name = "tenNVTT"),
				@ColumnResult(name = "maNVGH"),
				@ColumnResult(name = "tenNVGH"),
				@ColumnResult(name = "maNVBH"),
				@ColumnResult(name = "tenNVBH"),
				@ColumnResult(name = "idHoaDon"),
				@ColumnResult(name = "ngayHD"),
				@ColumnResult(name = "soHD"),
				@ColumnResult(name = "loaiNo"),
				@ColumnResult(name = "ngayTT"),
				@ColumnResult(name = "loaiTT"),
				@ColumnResult(name = "soCTTT"),
				@ColumnResult(name = "tienHD"),
				@ColumnResult(name = "tienTT"),
				@ColumnResult(name = "tienCK"),
				@ColumnResult(name = "lanTT")
		}),
		//VUONGHN: BAO CAO HO F1
		@SqlResultSetMapping(name = "ths.dms.core.entities.vo.rpt.Rpt_HO_Kho_F1", columns = {
				@ColumnResult(name = "mien"),
				@ColumnResult(name = "vung"),
				@ColumnResult(name = "shopCode"),
				@ColumnResult(name = "shopName"),
				@ColumnResult(name = "saleDateInMonth"),
				@ColumnResult(name = "realSaleDateInMonth"),
				@ColumnResult(name = "toDate"),
				@ColumnResult(name = "productInfoCode"),
				@ColumnResult(name = "productInfoName"),
				@ColumnResult(name = "productCode"),
				@ColumnResult(name = "percentage"),
				@ColumnResult(name = "productName"),
				@ColumnResult(name = "price"),
				@ColumnResult(name = "openStockTotal"),
				@ColumnResult(name = "importQty"),
				@ColumnResult(name = "exportQty"),
				@ColumnResult(name = "closeStockTotal"),
				@ColumnResult(name = "monthCumulate"),
				@ColumnResult(name = "monthCumulateTen"),
				@ColumnResult(name = "monthPlan"),
				@ColumnResult(name = "min"),
				@ColumnResult(name = "max"),
				@ColumnResult(name = "lead"),
				@ColumnResult(name = "next"),
				@ColumnResult(name = "isOrderFull"),
				@ColumnResult(name = "isOrderNotFull"),
				@ColumnResult(name = "isGroup"),
				@ColumnResult(name = "poCfQty"),
				@ColumnResult(name = "poCsQty"),
				@ColumnResult(name = "convfact") 
		}),
})
@NamedNativeQueries({ 	
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.XNTCT_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptImExStDataDetailVO", query = "{ call PKG_SHOP_REPORT.XNTCT_REPORT(?,:shopId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "TULV2.XNTCT_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptImExStDataDetailVO", query = "{ call TULV2.XNTCT_REPORT(?,:shopId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	//@NamedNativeQuery(name = "VUONGHN.XNTCT_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptImExStDataDetailVO", query = "{ call VUONGHN.XNTCT_REPORT(?,:shopId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	//@NamedNativeQuery(name = "PKG_SHOP_REPORT.DIMUONVESOM_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptDMVSVO", query = "{ call PKG_SHOP_REPORT.DIMUONVESOM_REPORT(?,:strListShopId,:staffOwnerId,:staffId,:fromDate,:toDate,:fromHour,:fromMinute,:toHour,:toMinute) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	//strListShopId in STRING, staffOwnerId in string, staffId in string, fromDate IN DATE, toDate in DATE, fromHour IN NUMBER, fromMinute in NUMBER,toHour IN NUMBER, toMinute in NUMBER, staffIdRoot in number, flagCMS in number)
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.DIMUONVESOM_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptDMVSVO", query = "{ call PKG_SHOP_REPORT.DIMUONVESOM_REPORT(?,:strListShopId, :staffOwnerId, :staffId,:fromDate,:toDate,:fromHour,:fromMinute,:toHour,:toMinute,:staffIdRoot,:flagCMS) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAONGAY_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCNgayVO", query = "{ call PKG_SHOP_REPORT.BAOCAONGAY_REPORT(?,:shopId,:WORKING_DAYS_IN_MONTH,:WORKED_DAYS_OF_MONTH) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAO_CAO_THOI_GIAN_TAT_MAY", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCTGTMVO", query = "{ call PKG_SHOP_REPORT.BAO_CAO_THOI_GIAN_TAT_MAY(?,:shopId,:gsnppId,:staffId,:fromDate,:toDate,:n_minutes) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_TCN", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCTCNDetailVO", query = "{ call PKG_SHOP_REPORT.BAOCAO_TCN(?,:shopId,:listStaffId,:listCustomerId, :objectType) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	//@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAO_CAO_THOI_GIAN_TAT_MAY2", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCTGTMVO", query = "{ call PKG_SHOP_REPORT.BAO_CAO_THOI_GIAN_TAT_MAY2(?,:shopId,:lstGsnppCode,:lstStaffCode,:fromDate,:toDate,:n_minutes) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	// shopId IN STRING, lstGsnppCode IN STRING, lstStaffCode IN STRING, fromDate DATE, toDate DATE, n_minutes NUMBER)
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAO_CAO_THOI_GIAN_TAT_MAY2", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCTGTMVO", query = "{ call PKG_SHOP_REPORT.BAO_CAO_THOI_GIAN_TAT_MAY2(?,:shopId, :lstGsnppCode,:lstStaffCode,:fromDate,:toDate,:n_minutes,:staffIdRoot,:flagCMS) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_DSBHSKUCTTKH", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCDS2VO", query = "{ call PKG_SHOP_REPORT.BAOCAO_DSBHSKUCTTKH(?,:listShopId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.TGGHOFNVBH_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptActionLogVO", query = "{ call PKG_SHOP_REPORT.TGGHOFNVBH_REPORT(?,:npp,:nvbh,:gsnpp,:ngay,:hasChild) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	//@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_TGGTNVBH_VT1", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCVT1", query = "{ call PKG_SHOP_REPORT.BAOCAO_TGGTNVBH_VT1(?,:shopId,:lstStaffOwnerCode,:lstStaffSaleCode,:pDate)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),   //vuongmq
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_TGGTNVBH_VT1", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCVT1", query = "{ call PKG_SHOP_REPORT.BAOCAO_TGGTNVBH_VT1(?,:shopId,:lstStaffOwnerCode,:lstStaffSaleCode,:pDate,:staffIdRoot,:flagCMS)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD1_3_PRO", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptDSPPNHCTVO", query = "{ call PKG_CRM_REPORT.KD1_3_PRO(?,:shopId,:lstSpId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD1_1_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptKDAllowSubCattVO", query = "{ call PKG_CRM_REPORT.KD1_1_REPORT(?,:shopId,:productCode,:fDate,:tDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD1_2_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptKDAllowSubCattVO", query = "{ call PKG_CRM_REPORT.KD1_2_REPORT(?,:shopId,:productCode,:fDate,:tDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD1_3_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptKDAllowSubCattVO", query = "{ call PKG_CRM_REPORT.KD1_3_REPORT(?,:shopId,:productCode,:fDate,:tDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD3_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD3VO", query = "{ call PKG_CRM_REPORT.KD3_REPORT(?,:npp,:gsnpp,:tbhvung,:nvbh,:tungay,:denngay,:cat,:songaythuchien,:songaybanhang) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD10_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD10VO", query = "{ call PKG_CRM_REPORT.KD10_REPORT(?,:braneId,:thang,:nam,:tungay,:denngay) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD11_1_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD11_1VO", query = "{ call PKG_CRM_REPORT.kd11_1_pro(?,:displayProId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD17_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD17VO", query = "{ call PKG_CRM_REPORT.proc_kd17(?,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD14_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD14DetailVO", query = "{ call PKG_CRM_REPORT.proc_kd14(?,:shopId,:fromMonth,:toMonth) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD21_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD21VO", query = "{ call PKG_CRM_REPORT.proc_kd21(?,:shopId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD7_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD7VO", query = "{ call PKG_CRM_REPORT.KD7_REPORT(?,:mien_var,:tungay,:denngay) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD2", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD2Detail", query = "{ call PKG_CRM_REPORT.KD2(?,:listTbhv,:listShopId,:listGsNpp,:listNvbh,:listNhanId,:tien_do_chuan,:so_ngay_ban_hang,:so_ngay_thuc_hien,:from_date,:to_date ) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD6", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD6VO", query = "{ call PKG_CRM_REPORT.KD6(?,:fromDate,:toDate,:soNgayBan) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD5_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD5VO", query = "{ call PKG_CRM_REPORT.KD5_REPORT(?, :tungay, :denngay, :snbhdq, :snbhtt) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD9", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD9VO", query = "{ call PKG_CRM_REPORT.KD9(?,:listIdNpp,:listIdCat,:listIdSp,:strFromDate,:strToDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.proc_kd13", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD13VO", query = "{ call PKG_CRM_REPORT.proc_kd13(?,:shopId,:custCode,:fromMonth,:toMonth) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.kd11_pro", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD11VO", query = "{ call PKG_CRM_REPORT.kd11_pro(?,:displayProId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.kd16_1_pro", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD16_1VO", query = "{ call PKG_CRM_REPORT.kd16_1_pro(?,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.proc_kd19dot1", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD19_1VO", query = "{ call PKG_CRM_REPORT.proc_kd19dot1(?,:shopId,:fromDate,:toDate,:numTimeBadScore) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD10_3_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD10_3VO", query = "{ call PKG_CRM_REPORT.KD10_3_REPORT(?,:displayProgramId,:subCatId,:listShopId,:strTuNgay,:strDenNgay) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.GET_DSBHMIEN_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptDSBHMienCTVO", query = "{ call PKG_CRM_REPORT.GET_DSBHMIEN_REPORT(?,:fromDate,:toDate, :fromLastMonth,:toLastMonth,:endLastMonth) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.GET_DIEM_NVBH_TUYEN_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptDiemNVBHCTVO", query = "{ call PKG_CRM_REPORT.GET_DIEM_NVBH_TUYEN_REPORT(?,:listShopId, :listStaffId, :fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.KOBATMAY_EX_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptAbsentVO", query = "{ call PKG_SHOP_REPORT.KOBATMAY_EX_REPORT(?,:shopId,:staffId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD1_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptDoanhSoBanHangNVBHSKUNgayDataVO", query = "{ call PKG_CRM_REPORT.KD1_REPORT(?, :shopId, :listOwnerId, :listTBHVId, :listStaffId, :listProductId, :toDate, :workingDaysInMonth, :workedDaysOfMonth) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD8_1_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptDiemNVBHCTVO", query = "{ call PKG_CRM_REPORT.KD8_1_REPORT(?,:shopId, :listStaffId, :fromDate,:toDate, :numScore) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.PROC_KD18", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD18CTVO", query = "{ call PKG_CRM_REPORT.PROC_KD18(?,:fromMonth,:toMonth) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.PROC_KD12", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD12DataVO", query = "{ call PKG_CRM_REPORT.PROC_KD12(?, :toDate, :shopId, :staffId) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD16_PRO", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD16DataVO", query = "{ call PKG_CRM_REPORT.KD16_PRO(?, :semonth) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD11_2_PRO", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptDiemPACTTBCTVO", query = "{ call PKG_CRM_REPORT.KD11_2_PRO(?, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD10_2_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD10_2DataVO", query = "{ call PKG_CRM_REPORT.KD10_2_REPORT(?, :braneId, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD10_1_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptKD10_1CTVO", query = "{ call PKG_CRM_REPORT.KD10_1_REPORT(?, :braneId, :tungay, :denngay) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.PROC_KD19", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD19DataVO", query = "{ call PKG_CRM_REPORT.PROC_KD19(?, :shopId, :fromDate, :toDate, :numSeqBadScore) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_CRM_REPORT.KD_15_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCKD15DataVO", query = "{ call PKG_CRM_REPORT.KD_15_REPORT(?, :currDate, :numMonth) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_PO_AUTO.PRO_CREATE_POAUTO", resultSetMapping = "ths.dms.core.entities.vo.PoAutoVO", query = "{ call PKG_PO_AUTO.PRO_CREATE_POAUTO(?, :shopId, :checkAll, :currSaleDay, :saleDay) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_DTTHNV", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptDTTHNVRecordVO", query = "{ call pkg_shop_report.BAOCAO_DTTHNV(?, :shopId, :listStaffId, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),//tungtt
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_PDHCG", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptSaleOrderDetail2VO", query = "{ call pkg_shop_report.BAOCAO_PDHCG(?, :shopId, :deliveryCode, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),//vuonghn
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_TDBHTMH", resultSetMapping = "ths.dms.core.entities.vo.RptTDBHTMHProductDateStaffFollowCustomerVO", query = "{ call pkg_shop_report.BAOCAO_TDBHTMH(?, :shopId, :listStaffId, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "vuonghn.BAOCAO_DTBHTNTNVBH", resultSetMapping = "ths.dms.core.entities.vo.RptDTBHTNTHVBHRecordOrderVO", query = "{ call vuonghn.BAOCAO_DTBHTNTNVBH(?, :shopId, :listStaffId, :fromDate, :toDate, :staffIdRoot, :flagCMS) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }), //namlb
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_PTHCKH", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptPTHCKHProductVO", query = "{ call pkg_shop_report.BAOCAO_PTHCKH(?, :shopId, :lstStaffCode, :customerId, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),//namlb
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_THCTHTB", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptPayDisplayProgrameRecordProductVO", query = "{ call PKG_SHOP_REPORT.BAOCAO_THCTHTB(?, :shopId, :lstProgramDisplayCode, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),//namlb
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_BKCTCTMH", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBKCTCTMHProductRecordVO", query = "{ call pkg_shop_report.BAOCAO_BKCTCTMH(?, :shopId, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),//namlb
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_DSKH", resultSetMapping = "ths.dms.core.entities.vo.CustomerVO", query = "{ call pkg_shop_report.BAOCAO_DSKH(?, :shopId, :staffCode, :customerCode, :isGroupByStaff) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),//namlb
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_BCKGTKHTT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCNVKGTKHTTRecordVO", query = "{ call pkg_shop_report.BAOCAO_BCKGTKHTT(?, :shopId, :lstStaffCode, :lstGsnppCode, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),//namlb						
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_BCKGTKHTT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCNVKGTKHTTRecordVO", query = "{ call PKG_SHOP_REPORT.BAOCAO_BCKGTKHTT(?, :shopId, :lstStaffCode, :lstGsnppCode, :fromDate, :toDate, :staffIdRoot, :flagCMS) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),//namlb
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_PT", resultSetMapping = "ths.dms.core.entities.vo.rpt.PTDetail", query = "{ call pkg_shop_report.BAOCAO_PT(?, :shopId, :listCustomerId, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),//tungtt
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_XBTNV", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptXBTNVRecord", query = "{ call pkg_shop_report.BAOCAO_XBTNV(?, :shopId, :listStaffId, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_XNKNVBH", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCXNKNVBHDetailVO", query = "{ call PKG_SHOP_REPORT.BAOCAO_XNKNVBH(?, :shopId, :listStaffId, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_XNKNVBH_XUAT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCXNKNVBHDetailVO", query = "{ call PKG_SHOP_REPORT.BAOCAO_XNKNVBH_XUAT(?, :shopId, :listStaffId, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_XNKNVBH_NHAP", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCXNKNVBHDetailVO", query = "{ call PKG_SHOP_REPORT.BAOCAO_XNKNVBH_NHAP(?, :shopId, :listStaffId, :fromDate, :toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_PXKKVCNB", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptPXKKVCNB_DataConvert", query = "{ call PKG_SHOP_REPORT.BAOCAO_PXKKVCNB(?, :shopId, :staffCode, :stockTransId) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_PO_AUTO.PRO_POAUTO_COMPARE", resultSetMapping = "ths.dms.core.entities.vo.PoAutoVO", query = "{ call PKG_PO_AUTO.PRO_POAUTO_COMPARE(?, :shopId, :poId, :checkAll, :currSaleDay, :saleDay) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
//	@NamedNativeQuery(name = "PKG_SHOP_REPORT.XNTF1_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptF1DataVO", query = "{ call PKG_SHOP_REPORT.XNTF1_REPORT(?,:var_shopid,:var_fromdate,:var_todate,:var_workdate,:var_realworkdate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.XNTF1_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptF1DataVO", query = "{ call PKG_SHOP_REPORT.XNTF1_REPORT(?,:var_shopid,:var_fromdate,:var_todate,:var_workdate,:var_realworkdate,:numExec) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "TULV2.XNTF1_REPORT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptF1DataVO", query = "{ call TULV2.XNTF1_REPORT(?,:var_shopid,:var_fromdate,:var_todate,:var_workdate,:var_realworkdate,:numExec) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_PO_TTCKH", resultSetMapping = "ths.dms.core.entities.vo.rpt.Rpt_PO_CTTT_MAPPINGVO", query = "{ call PKG_SHOP_REPORT.BAOCAO_PO_TTCKH(?,:SHOPID,:TYPEPERSON,:CODEPERSON,:FROMDATE,:TODATE) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.PRO_TDXNTCT", resultSetMapping = "ths.dms.core.entities.vo.rpt.Rpt_TDXNTCTVO", query = "{ call PKG_SHOP_REPORT.PRO_TDXNTCT(?,:SHOPID,:FROMDATE,:TODATE) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}),
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.PRO_TGGTNVBH1", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCTGGTCNVBH_1VO", query = "{ call PKG_SHOP_REPORT.PRO_TGGTNVBH1(?,:FROMDATE,:TODATE,:SHOPID,:NVGSCODE, :NVBHCODE) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}),
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_CTKM", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCCTKM_RecordDetailVO", query = "{ call pkg_shop_report.BAOCAO_CTKM(?,:shopId,:lstStaffCode,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}), //vuonghn bao cao chi tiet khuyen mai
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_BKCTHHMV", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBKCTHHMV_RecordVO", query = "{ call pkg_shop_report.BAOCAO_BKCTHHMV(?,:shopId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}), //vuonghn bang ke chung tu hang hoa mua vao
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_PDTH", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptSaleOrderDetailVO", query = "{ call pkg_shop_report.BAOCAO_PDTH(?,:shopId,:lstDeliveryId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}), //SangTN: Phieu doi tra hang
	//@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_VT3", resultSetMapping = "ths.dms.core.entities.vo.TimeVisitCustomerVO", query = "{ call PKG_SHOP_REPORT.BAOCAO_VT3(?,:strListShopId,:lstStaffOwnerCode,:lstStaffSaleCode,:fromDate,:toDate,:isGetShopOnly) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}), //SangTN: VT3 - Thoi gian ghe tham khach hang
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_VT3", resultSetMapping = "ths.dms.core.entities.vo.TimeVisitCustomerVO", query = "{ call PKG_SHOP_REPORT.BAOCAO_VT3(?,:strListShopId, :lstStaffOwnerCode, :lstStaffSaleCode,:fromDate,:toDate,:isGetShopOnly,:staffIdRoot,:flagCMS) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}), //SangTN: VT3 - Thoi gian ghe tham khach hang
	@NamedNativeQuery(name = "PKG_TIENTV.PROC_VT9", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCVT9", query = "{ call PKG_TIENTV.PROC_VT9(?,:strListShopId,:strListStaffId,:fromDate,:toDate,:typeId,:displayId,:staffId)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }), // bao cao VT9
	@NamedNativeQuery(name = "PKG_BAO_CAO_NPP.BAOCAO_DSKHTTBH", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptCustomerByRoutingDataVO", query = "{ call PKG_BAO_CAO_NPP.BAOCAO_DSKHTTBH(?,:shopId,:lstStaffCode,:lstCustomerCode,:customerName) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}), //SangTN: Danh sach khach hang theo mat hang
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_DS1", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCDS1VO", query = "{ call PKG_SHOP_REPORT.BAOCAO_DS1(?,:strListShopId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}),//SangTN: DS1 - Doanh so ban hang sku chi tiet theo nvbh
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_TDDSTKHNHMH", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCTDDSTKHNHMHDetailVO", query = "{ call PKG_SHOP_REPORT.BAOCAO_TDDSTKHNHMH(?,:shopId,:lstCustomerCode,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}),//SangTN: Theo doi doanh so theo khach hang nganh hang ma hang
	//@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_BKCTHDGTGT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptInvDetailStatDetailVO", query = "{ call pkg_shop_report.BAOCAO_BKCTHDGTGT(?,:shopId,:staffCode,:tax,:fromDate,:toDate,:status) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}),//TUNGTT: Bao cao bang ke chi tiet hoa don GTGT
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_BKCTHDGTGT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptInvDetailStatDetailVO", query = "{ call PKG_SHOP_REPORT.BAOCAO_BKCTHDGTGT(?,:shopId,:staffCode,:tax,:fromDate,:toDate,:status) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}),//TUNGTT: Bao cao bang ke chi tiet hoa don GTGT
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_TDCTDS", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptTDCTDSVODetail", query = "{ call pkg_shop_report.BAOCAO_TDCTDS(?,:shopId,:staffCode,:month,:year) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}),//TUNGTT: Bao cao theo doi chi tieu doanh so
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_PTHNVGH", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptReturnSaleOrderAllowDeliveryVO", query = "{ call pkg_shop_report.BAOCAO_PTHNVGH(?,:shopId,:lstDeliveryId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}),//SangTN: Bao cao phieu tra hang theo nhan vien giao hang
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.KOBATMAY_EX_REPORT2", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptAbsentVO", query = "{ call PKG_SHOP_REPORT.KOBATMAY_EX_REPORT2(?,:lstShopId,:lstStaffId,:fromDate,:toDate) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}),//SangTN: Bao cao phieu tra hang theo nhan vien giao hang
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_NGAY", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCNVO", query = "{ call pkg_shop_report.BAOCAO_NGAY(?,:shopId, :parentStaffId, :fromDate,:toDate,:planDay,:realDay,:nextMonth,:lastMonth, :checkMap) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true")}),//TUNGTT: Bao cao ngay
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_VT7", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCVT7", query = "{ call pkg_shop_report.BAOCAO_VT7(?,:lstShopId,:lstGSNPPId,:fromDateStr,:toDateStr)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),//Vuonghn: VT7 - Bao cao ghe tham khach hang
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_VT10", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCVT10", query = "{ call pkg_shop_report.BAOCAO_VT10(?,:lstShopId,:lstGSMTId,:fromDateStr,:toDateStr)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),//Vuonghn: VT10 - Bao cao ket qua cham trung bay
	@NamedNativeQuery(name = "pkg_shop_report.BAOCAO_VT12", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptBCVT12", query = "{ call pkg_shop_report.BAOCAO_VT12(?,:lstShopId,:cttbId,:strDate)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }), //vuonghn: vt12 - Bao cao xoa khach hang tham gia CTTB
	@NamedNativeQuery(name = "pkg_shop_report.BC_3_16_BTHDH", resultSetMapping = "ths.dms.core.entities.vo.rpt.Rpt3_16_BTHDH", query = "{ call pkg_shop_report.BC_3_16_BTHDH(?,:shopId,:catCode,:fromDate,:toDate)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }), //tungtt - Bang tong hop tra hang
	@NamedNativeQuery(name = "PKG_CRM_REPORT.RptKD9", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptKD9_Detail", query = "{ call PKG_CRM_REPORT.RptKD9(?,:shopId,:productCode,:tmpDate,:fDate,:aDate,:tDate)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }), //tungtt - BC KD 9
	@NamedNativeQuery(name = "pkg_shop_report.BC_3_13_CTKMTCT", resultSetMapping = "ths.dms.core.entities.vo.rpt.Rpt3_13_CTKMTCT_Detail", query = "{ call pkg_shop_report.BC_3_13_CTKMTCT(?,:shopId,:staffCode,:fromDate,:toDate)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }), //tungtt - BC Chi tiet khuyen mai theo chuong trinh
	@NamedNativeQuery(name = "pkg_shop_report.BC_3_14_CTKMTCTNV", resultSetMapping = "ths.dms.core.entities.vo.rpt.Rpt3_14_CTKMTCTNV_Detail", query = "{ call pkg_shop_report.BC_3_14_CTKMTCTNV(?,:shopId,:staffCode,:fromDate,:toDate)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }), //tungtt - BC Chi tiet khuyen mai theo chuong trinh - nhan vien
	@NamedNativeQuery(name = "pkg_ho_report.P_DM1_1_TTKH", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptDM1_1TTKH", query = "{ call pkg_ho_report.P_DM1_1_TTKH(?, :p_lstShopId, :p_status)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }), //lacnv - BC DM1.1 Thong Tin KH
	@NamedNativeQuery(name = "pkg_shop_report.P_CT_ThanhToan_NVTT", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptTDTTCN_NVTT", query = "{ call pkg_shop_report.P_CT_THANHTOAN_NVTT(?, :p_shopId, :p_lstStaffId, :p_lstCustId, :p_lstDeliveryId, :p_fDate, :p_tDate)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }), //lacnv - BC 7.5.2 chung tu thanh toan - NVTT
	@NamedNativeQuery(name = "pkg_shop_report.P_CT_ThanhToan_KH", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptTDTTCN_NVTT", query = "{ call pkg_shop_report.P_CT_THANHTOAN_KH(?, :p_shopId, :p_lstStaffId, :p_lstCustId, :p_lstDeliveryId, :p_fDate, :p_tDate)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }), //lacnv - BC 7.5.2 chung tu thanh toan - KH
	@NamedNativeQuery(name = "PKG_SHOP_REPORT.P_CT_THANHTOAN_CONGNO", resultSetMapping = "ths.dms.core.entities.vo.rpt.RptTDTTCNData", query = "{ call pkg_shop_report.P_CT_THANHTOAN_CONGNO(?, :p_shopId, :p_lstStaffId, :p_lstCustId, :p_lstDeliveryId, :p_fDate, :p_tDate)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }), //vuongmq - BC 7.5.2 theo doi cong no
	@NamedNativeQuery(name = "pkg_ho_report.BAOCAO_HO_KHO_F1", resultSetMapping = "ths.dms.core.entities.vo.rpt.Rpt_HO_Kho_F1", query = "{ call pkg_ho_report.BAOCAO_HO_KHO_F1(?, :VAR_LSTSHOPID, :VAR_LSTCATCODE, :VAR_LSTPRODUCTCODE, :VAR_FROMDATE, :VAR_TODATE, :VAR_WORKDATE, :VAR_REALWORKDATE) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }) //vuonghn: bao cao ho f1
	//@NamedNativeQuery(name = "PKG_HO_REPORT.BAOCAO_HO_KHO_F1", resultSetMapping = "ths.dms.core.entities.vo.rpt.Rpt_HO_Kho_F1", query = "{ call VUONGHN.BAOCAO_HO_KHO_F1(?, :VAR_LSTSHOPID, :VAR_LSTCATCODE, :VAR_LSTPRODUCTCODE, :VAR_FROMDATE, :VAR_TODATE, :VAR_WORKDATE, :VAR_REALWORKDATE) }", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") })
})
public class RptStockTotalDay implements Serializable {

	private static final long serialVersionUID = 1L;
	//tong con cuoi ngay = CLOSE_STOCK_TOTAL + IMPORT_TOTAL - EXPORT_TOTAL
	@Basic
	@Column(name = "CLOSE_STOCK_TOTAL", length = 22)
	private Integer closeStockTotal;

	//moi ngay 1 san pham se luu 1 dong
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//tong xuat trong ngay = xuat bán + xuat km + xuat doi,huy,tra + xuat vansale + tra VNM + xuat dieu chinh
	@Basic
	@Column(name = "EXPORT_TOTAL", length = 22)
	private Integer exportTotal;

	//tong tra VNM
	@Basic
	@Column(name = "EXPORT_TOTAL_VNM", length = 22)
	private Integer exportTotalVnm;

	//tong nhap trong ngay = tong nhap VNM +tong tra tu KH + nhap vansale + nhap dieu chinh
	@Basic
	@Column(name = "IMPORT_TOTAL", length = 22)
	private Integer importTotal;

	//tong nhap VNM
	@Basic
	@Column(name = "IMPORT_TOTAL_VNM", length = 22)
	private Integer importTotalVnm;

	//tong con dau ngay
	@Basic
	@Column(name = "OPEN_STOCK_TOTAL", length = 22)
	private Integer openStockTotal;

	//id doi tuong
	@Basic
	@Column(name = "OBJECT_ID", length = 22)
	private Integer objectId;

	//loai doi tuong (1:shop, 2:staff, 3:customer), hien tai se chi tong hop cho shop
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.StockObjectType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private StockObjectType objectType;

	//id sp
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//ngay tong hop
	@Basic
	@Column(name = "RPT_IN_DAY", length = 7)
	private Date rptInDay;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "RPT_STOCK_TOTAL_DAY_ID")
	private Long id;

	//tong huy, doi, tra presale
	@Basic
	@Column(name = "SALE_PROMOTION_D_PRE", length = 22)
	private Integer salePromotionDPre;

	//tong huy, doi, tra vansale
	@Basic
	@Column(name = "SALE_PROMOTION_D_VAN", length = 22)
	private Integer salePromotionDVan;

	//tong km (bao gom ca km, tra thuong, doi hang ...) presale
	@Basic
	@Column(name = "SALE_PROMOTION_PRE", length = 22)
	private Integer salePromotionPre;

	//tong km (bao gom ca km, tra thuong) vanasale
	@Basic
	@Column(name = "SALE_PROMOTION_VAN", length = 7)
	private Date salePromotionVan;

	//tong so ban trong ngay  presale
	@Basic
	@Column(name = "SALE_TOTAL_PRE", length = 22)
	private Integer saleTotalPre;

	//tong so ban trong ngay vansale
	@Basic
	@Column(name = "SALE_TOTAL_VAN", length = 7)
	private Integer saleTotalVan;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	public Integer getCloseStockTotal() {
		if (closeStockTotal == null) {
			closeStockTotal = 0;
		}
		return closeStockTotal;
	}

	public void setCloseStockTotal(Integer closeStockTotal) {
		this.closeStockTotal = closeStockTotal;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getExportTotal() {
		if (exportTotal == null) {
			exportTotal = 0;
		}
		return exportTotal;
	}

	public void setExportTotal(Integer exportTotal) {
		this.exportTotal = exportTotal;
	}

	public Integer getExportTotalVnm() {
		if (exportTotalVnm == null) {
			exportTotalVnm = 0;
		}
		return exportTotalVnm;
	}

	public void setExportTotalVnm(Integer exportTotalVnm) {
		this.exportTotalVnm = exportTotalVnm;
	}

	public Integer getImportTotal() {
		if (importTotal == null) {
			importTotal = 0;
		}
		return importTotal;
	}

	public void setImportTotal(Integer importTotal) {
		this.importTotal = importTotal;
	}

	public Integer getImportTotalVnm() {
		if (importTotalVnm == null) {
			importTotalVnm = 0;
		}
		return importTotalVnm;
	}

	public void setImportTotalVnm(Integer importTotalVnm) {
		this.importTotalVnm = importTotalVnm;
	}

	public Integer getOpenStockTotal() {
		if (openStockTotal == null) {
			openStockTotal = 0;
		}
		return openStockTotal;
	}

	public void setOpenStockTotal(Integer openStockTotal) {
		this.openStockTotal = openStockTotal;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Date getRptInDay() {
		return rptInDay;
	}

	public void setRptInDay(Date rptInDay) {
		this.rptInDay = rptInDay;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSalePromotionDPre() {
		if (salePromotionDPre == null) {
			salePromotionDPre = 0;
		}
		return salePromotionDPre;
	}

	public void setSalePromotionDPre(Integer salePromotionDPre) {
		this.salePromotionDPre = salePromotionDPre;
	}

	public Integer getSalePromotionDVan() {
		if (salePromotionDVan == null) {
			salePromotionDVan = 0;
		}
		return salePromotionDVan;
	}

	public void setSalePromotionDVan(Integer salePromotionDVan) {
		this.salePromotionDVan = salePromotionDVan;
	}

	public Integer getSalePromotionPre() {
		if (salePromotionPre == null) {
			salePromotionPre = 0;
		}
		return salePromotionPre;
	}

	public void setSalePromotionPre(Integer salePromotionPre) {
		this.salePromotionPre = salePromotionPre;
	}

	public Date getSalePromotionVan() {
		return salePromotionVan;
	}

	public void setSalePromotionVan(Date salePromotionVan) {
		this.salePromotionVan = salePromotionVan;
	}

	public Integer getSaleTotalPre() {
		if (saleTotalPre == null) {
			saleTotalPre = 0;
		}
		return saleTotalPre;
	}

	public void setSaleTotalPre(Integer saleTotalPre) {
		this.saleTotalPre = saleTotalPre;
	}

	public Integer getObjectId() {
		return objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public StockObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(StockObjectType objectType) {
		this.objectType = objectType;
	}

	public Integer getSaleTotalVan() {
		if (saleTotalVan == null) {
			saleTotalVan = 0;
		}
		return saleTotalVan;
	}

	public void setSaleTotalVan(Integer saleTotalVan) {
		this.saleTotalVan = saleTotalVan;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	
}
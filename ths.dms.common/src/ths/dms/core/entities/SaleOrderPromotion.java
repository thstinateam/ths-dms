package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "SALE_ORDER_PROMOTION")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_SALE_ORDER_PROMOTION", sequenceName = "SALE_ORDER_PROMOTION_SEQ", allocationSize = 1)
public class SaleOrderPromotion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SALE_ORDER_PROMOTION")
	@Column(name = "SALE_ORDER_PROMOTION_ID")
	private Long id;
	
	@ManyToOne(targetEntity = SaleOrder.class)
	@JoinColumn(name = "SALE_ORDER_ID", referencedColumnName = "SALE_ORDER_ID")
	private SaleOrder saleOrder;
	
	@ManyToOne(targetEntity = PromotionProgram.class)
	@JoinColumn(name = "PROMOTION_PROGRAM_ID", referencedColumnName = "PROMOTION_PROGRAM_ID")
	private PromotionProgram promotionProgram;
	
	
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	//so lan co the nhan km
	@Basic
	@Column(name = "PROMOTION_PROGRAM_CODE", length = 50)
	private String promotionCode;
	
	/*@ManyToOne(targetEntity = ProductGroup.class)
	@JoinColumn(name = "PRODUCT_GROUP_ID", referencedColumnName = "PRODUCT_GROUP_ID")*/
	@Transient
	private ProductGroup productGroup;
	
	@Basic
	@Column(name = "PRODUCT_GROUP_ID")
	private Long productGroupId;
	
	/*@ManyToOne(targetEntity = GroupLevel.class)
	@JoinColumn(name = "GROUP_LEVEL_ID", referencedColumnName = "GROUP_LEVEL_ID")*/
	@Transient
	private GroupLevel groupLevel;
	
	@Basic
	@Column(name = "GROUP_LEVEL_ID")
	private Long groupLevelId;
	
	@Basic
	@Column(name = "PROMOTION_LEVEL", length = 20)
	private Integer promotionLevel;
	
	
	@Basic
	@Column(name = "ORDER_DATE", length = 7)
	private Date orderDate;
	
	@Basic
	@Column(name = "QUANTITY_RECEIVED", length = 50)
	private Integer quantityReceived;
	
	@Basic
	@Column(name = "QUANTITY_RECEIVED_MAX", length = 50)
	private Integer maxQuantityReceived;
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	// doi tuong detail promotion
	@Basic
	@Column(name = "PROMOTION_DETAIL", length = 500)
	private String promotionDetail;

	// doi tuong het quota
	@Basic
	@Column(name = "EXCEED_OBJECT", length = 2)
	private Integer exceedObject;
	
	// dai luong het quota
	@Basic
	@Column(name = "EXCEED_UNIT", length = 2)
	private Integer exceedUnit;
	
	//so tien nhan
	@Basic
	@Column(name = "AMOUNT_RECEIVED", length = 26)
	private BigDecimal amountReceived;
	
	//so tien nhan max
	@Basic
	@Column(name = "AMOUNT_RECEIVED_MAX", length = 26)
	private BigDecimal maxAmountReceived;
	
	//so luong nhan
	@Basic
	@Column(name = "NUM_RECEIVED", length = 20)
	private BigDecimal numReceived;
	
	//so luong nhan max
	@Basic
	@Column(name = "NUM_RECEIVED_MAX", length = 20)
	private BigDecimal maxNumReceived;
	
	@Transient
	private String promotionName;
	
	@Transient
	private Integer orderPromo = 0;
	
	@Transient
	private Integer maxQuantityReceivedTotal; // neu kho du so suat, so luong, tien,: luu lai gia tri cu tinh KM; luu so suat tinh dc dau tien KM

	@Transient
	private BigDecimal maxNumReceivedTotal; // neu kho du so suat, so luong, tien,: luu lai gia tri cu tinh KM; luu so luong tinh dc dau tien KM

	@Transient
	private BigDecimal maxAmountReceivedTotal; // neu kho du so suat, so luong, tien,: luu lai gia tri cu tinh KM; luu so tien tinh dc dau tien KM
	
	@Transient
	private Integer flagPromoQuantity; // 1; dat khong an KM do het suat; 2: dat ma khong an het (chi an 1 phan) 
	
	public Integer getMaxQuantityReceived() {
		return maxQuantityReceived;
	}

	public void setMaxQuantityReceived(Integer maxQuantityReceived) {
		this.maxQuantityReceived = maxQuantityReceived;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public ProductGroup getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(ProductGroup productGroup) {
		this.productGroup = productGroup;
	}

	public GroupLevel getGroupLevel() {
		return groupLevel;
	}

	public void setGroupLevel(GroupLevel groupLevel) {
		this.groupLevel = groupLevel;
	}

	public Integer getPromotionLevel() {
		return promotionLevel;
	}

	public void setPromotionLevel(Integer promotionLevel) {
		this.promotionLevel = promotionLevel;
	}

	public Integer getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getPromotionDetail() {
		return promotionDetail;
	}

	public void setPromotionDetail(String promotionDetail) {
		this.promotionDetail = promotionDetail;
	}

	public PromotionProgram getPromotionProgram() {
		return promotionProgram;
	}

	public void setPromotionProgram(PromotionProgram promotionProgram) {
		this.promotionProgram = promotionProgram;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Long getGroupLevelId() {
		return groupLevelId;
	}

	public void setGroupLevelId(Long groupLevelId) {
		this.groupLevelId = groupLevelId;
	}

	public Long getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(Long productGroupId) {
		this.productGroupId = productGroupId;
	}
	
	

	public Integer getOrderPromo() {
		return orderPromo;
	}

	public void setOrderPromo(Integer orderPromo) {
		this.orderPromo = orderPromo;
	}

	public Integer getExceedObject() {
		return exceedObject;
	}

	public void setExceedObject(Integer exceedObject) {
		this.exceedObject = exceedObject;
	}

	public Integer getExceedUnit() {
		return exceedUnit;
	}

	public void setExceedUnit(Integer exceedUnit) {
		this.exceedUnit = exceedUnit;
	}

	public BigDecimal getAmountReceived() {
		return amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}


	public BigDecimal getMaxAmountReceived() {
		return maxAmountReceived;
	}

	public void setMaxAmountReceived(BigDecimal maxAmountReceived) {
		this.maxAmountReceived = maxAmountReceived;
	}

	public BigDecimal getNumReceived() {
		return numReceived;
	}

	public void setNumReceived(BigDecimal numReceived) {
		this.numReceived = numReceived;
	}

	public BigDecimal getMaxNumReceived() {
		return maxNumReceived;
	}

	public void setMaxNumReceived(BigDecimal maxNumReceived) {
		this.maxNumReceived = maxNumReceived;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public Integer getMaxQuantityReceivedTotal() {
		return maxQuantityReceivedTotal;
	}

	public void setMaxQuantityReceivedTotal(Integer maxQuantityReceivedTotal) {
		this.maxQuantityReceivedTotal = maxQuantityReceivedTotal;
	}

	public BigDecimal getMaxNumReceivedTotal() {
		return maxNumReceivedTotal;
	}

	public void setMaxNumReceivedTotal(BigDecimal maxNumReceivedTotal) {
		this.maxNumReceivedTotal = maxNumReceivedTotal;
	}
	
	public BigDecimal getMaxAmountReceivedTotal() {
		return maxAmountReceivedTotal;
	}

	public void setMaxAmountReceivedTotal(BigDecimal maxAmountReceivedTotal) {
		this.maxAmountReceivedTotal = maxAmountReceivedTotal;
	}

	public Integer getFlagPromoQuantity() {
		return flagPromoQuantity;
	}

	public void setFlagPromoQuantity(Integer flagPromoQuantity) {
		this.flagPromoQuantity = flagPromoQuantity;
	}

}

package ths.dms.core.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "MT_CATEGORY_GROUP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "MT_CATEGORY_GROUP_SEQ", allocationSize = 1)
public class CategoryGroup implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8884243033075673113L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "CATEGORY_GROUP_ID")
	private Long id;
	
	@Basic
	@Column(name = "CATEGORY_GROUP_CODE", length = 50)
	private String categoryGroupCode;

	@Basic
	@Column(name = "CATEGORY_GROUP_NAME", length = 100)
	private String categoryGroupName;

	@Basic
	@Column(name = "PREFIX", length = 3)
	private String prefix;

	@Basic
	@Column(name = "PARENT_GROUP", length = 50)
	private String parentGroup;

	@Basic
	@Column(name = "IS_SYSTEM", length = 2)
	private String isSystem;

	@Basic
	@Column(name = "DESCRIPTION", length = 500)
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryGroupCode() {
		return categoryGroupCode;
	}

	public void setCategoryGroupCode(String categoryGroupCode) {
		this.categoryGroupCode = categoryGroupCode;
	}

	public String getCategoryGroupName() {
		return categoryGroupName;
	}

	public void setCategoryGroupName(String categoryGroupName) {
		this.categoryGroupName = categoryGroupName;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getParentGroup() {
		return parentGroup;
	}

	public void setParentGroup(String parentGroup) {
		this.parentGroup = parentGroup;
	}

	public String getIsSystem() {
		return isSystem;
	}

	public void setIsSystem(String isSystem) {
		this.isSystem = isSystem;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}

package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "VISIT_PLAN")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "VISIT_PLAN_SEQ", allocationSize = 1)
public class VisitPlan implements Serializable {

	private static final long serialVersionUID = 1L;
	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 40)
	private String createUser;

	//tu ngay
	@Basic
	@Column(name = "FROM_DATE", length = 7)
	private Date fromDate;

	//id tuyen
	@ManyToOne(targetEntity = Routing.class)
	@JoinColumn(name = "ROUTING_ID", referencedColumnName = "ROUTING_ID")
	private Routing routing;

	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//id NVBH
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	// 0: ngung hoat dong, 1: hoat dong
	// trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;

	//den ngay
	@Basic
	@Column(name = "TO_DATE", length = 7)
	private Date toDate;

	//ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 40)
	private String updateUser;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "VISIT_PLAN_ID")
	private Long id;

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the createUser
	 */
	public String getCreateUser() {
		return createUser;
	}

	/**
	 * @param createUser the createUser to set
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the routing
	 */
	public Routing getRouting() {
		return routing;
	}

	/**
	 * @param routing the routing to set
	 */
	public void setRouting(Routing routing) {
		this.routing = routing;
	}

	/**
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * @param shop the shop to set
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * @return the staff
	 */
	public Staff getStaff() {
		return staff;
	}

	/**
	 * @param staff the staff to set
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	/**
	 * @return the status
	 */
	public ActiveType getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(ActiveType status) {
		this.status = status;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the updateUser
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * @param updateUser the updateUser to set
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
}
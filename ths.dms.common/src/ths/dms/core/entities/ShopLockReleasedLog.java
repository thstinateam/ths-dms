/*
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HCM standard
 * @author vuongmq
 * @since 14/04/2016
 * 
 */

@Entity
@Table(name = "SHOP_LOCK_RELEASED_LOG")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SHOP_LOCK_RELEASED_LOG_SEQ", allocationSize = 1)
public class ShopLockReleasedLog implements Serializable {

	private static final long serialVersionUID = 1L;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SHOP_LOCK_RELEASED_LOG_ID")
	private Long id;
	
	@Basic
	@Column(name = "SHOP_LOCK_ID", length = 20)
	private Long shopLogId;
	
	@Basic
	@Column(name = "SHOP_ID", length = 20)
	private Long shopId;
	
	@Basic
	@Column(name = "LOCK_DATE", length = 7)
	private Date lockDate;
	
	@Basic
	@Column(name = "RELEASED_DATE", length = 7)
	private Date releasedDate;

	@Basic
	@Column(name = "RELEASED_USER", length = 50)
	private String releasedUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopLogId() {
		return shopLogId;
	}

	public void setShopLogId(Long shopLogId) {
		this.shopLogId = shopLogId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Date getLockDate() {
		return lockDate;
	}

	public void setLockDate(Date lockDate) {
		this.lockDate = lockDate;
	}

	public Date getReleasedDate() {
		return releasedDate;
	}

	public void setReleasedDate(Date releasedDate) {
		this.releasedDate = releasedDate;
	}

	public String getReleasedUser() {
		return releasedUser;
	}

	public void setReleasedUser(String releasedUser) {
		this.releasedUser = releasedUser;
	}
	
	
}
/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Mo ta class ProductShopMap.java
 * @author vuongmq
 * @since Dec 2, 2015
 */

@Entity
@Table(name = "PRODUCT_SHOP_MAP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PRODUCT_SHOP_MAP_SEQ", allocationSize = 1)
public class ProductShopMap implements Serializable {

	private static final long serialVersionUID = 1L;
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PRODUCT_SHOP_MAP_ID")
	private Long id;

	//ID NPP
	@Basic
	@Column(name = "SHOP_ID", length = 20)
	private Long shopId;
	
	// id product
	@Basic
	@Column(name = "PRODUCT_ID", length = 20)
	private Long productId;

	@Basic
	@Column(name = "NESSESARY_STOCK_DAY", length = 3)
	private Integer nessesaryStockDay;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Integer getNessesaryStockDay() {
		return nessesaryStockDay;
	}

	public void setNessesaryStockDay(Integer nessesaryStockDay) {
		this.nessesaryStockDay = nessesaryStockDay;
	}
	
}
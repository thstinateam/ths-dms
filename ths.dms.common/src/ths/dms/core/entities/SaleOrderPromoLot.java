package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ProgramType;

/**
* Auto generate entities - SALE_ORDER_PROMO_LOT
* @author tungmt
* @since 7/8/2015
*/

@Entity
@Table(name = "SALE_ORDER_PROMO_LOT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SALE_ORDER_PROMO_LOT_SEQ", allocationSize = 1)
public class SaleOrderPromoLot implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SALE_ORDER_PROMO_LOT_ID")
	private Long id;
	
	//Id don hang
	@ManyToOne(targetEntity = SaleOrder.class)
	@JoinColumn(name = "SALE_ORDER_ID", referencedColumnName = "SALE_ORDER_ID")
	private SaleOrder saleOrder;
//	
//	//Id don hang
	@ManyToOne(targetEntity = SaleOrderDetail.class)
	@JoinColumn(name = "SALE_ORDER_DETAIL_ID", referencedColumnName = "SALE_ORDER_DETAIL_ID")
	private SaleOrderDetail saleOrderDetail;
//	
//	//id npp
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
//
//	//id nv
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
//	
	@Basic
	@Column(name = "ORDER_DATE", length = 7)
	private Date orderDate;
//	
//	//0: Km tu dong, 1:KM bang tay,2: cham chung bay, 3: doi hang
	@Basic
	@Column(name = "PROGRAM_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProgramType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProgramType programType;
//	
//	//PROGRAM_TYPE = 0,1 thi ma la CTKM; PROGRAM_TYPE = 2 --> ma la CTTB
	@Basic
	@Column(name = "PROGRAM_CODE", length = 50)
	private String programCode;
//	
//	//PROGRAME_TYPE_CODE
	@Basic
	@Column(name = "PROGRAME_TYPE_CODE", length = 50)
	private String programeTypeCode;
	
	//% chiet khau
	@Basic
	@Column(name = "DISCOUNT_PERCENT", length = 4)
	private Float discountPercent;

	//So tien chiet khau
	@Basic
	@Column(name = "DISCOUNT_AMOUNT", length = 26)
	private BigDecimal discountAmount;
	
	//So tien chiet khau
	@Basic
	@Column(name = "DISCOUNT_COMPUTE_AMOUNT", length = 26)
	private BigDecimal discountComputeAmount;
	
	//1. Là khuyến mãi của chính sp, 0. Là khuyến mãi do chia đều (từ đơn hàng, nhóm)
	@Basic
	@Column(name = "IS_OWNER", length = 2)
	private Integer isOwner;
	
	@Basic
	@Column(name = "SALE_ORDER_LOT_ID", length = 20)
	private Long saleOrderLotId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Integer getIsOwner() {
		return isOwner;
	}

	public void setIsOwner(Integer isOwner) {
		this.isOwner = isOwner;
	}
	
	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public SaleOrderDetail getSaleOrderDetail() {
		return saleOrderDetail;
	}

	public void setSaleOrderDetail(SaleOrderDetail saleOrderDetail) {
		this.saleOrderDetail = saleOrderDetail;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public ProgramType getProgramType() {
		return programType;
	}

	public void setProgramType(ProgramType programType) {
		this.programType = programType;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getProgrameTypeCode() {
		return programeTypeCode;
	}

	public void setProgrameTypeCode(String programeTypeCode) {
		this.programeTypeCode = programeTypeCode;
	}

	public Float getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Float discountPercent) {
		this.discountPercent = discountPercent;
	}

	public BigDecimal getDiscountComputeAmount() {
		return discountComputeAmount;
	}

	public void setDiscountComputeAmount(BigDecimal discountComputeAmount) {
		this.discountComputeAmount = discountComputeAmount;
	}

	public SaleOrderPromoLot clone() {
		SaleOrderPromoLot temp = new SaleOrderPromoLot();
		temp.saleOrder = saleOrder;
		temp.saleOrderDetail = saleOrderDetail;
		temp.shop = shop;
		temp.staff = staff;
		temp.orderDate = orderDate;
		temp.programType = programType;
		temp.programCode = programCode;
		temp.programeTypeCode = programeTypeCode;
		temp.discountPercent = discountPercent;
		temp.discountAmount = discountAmount;
		temp.discountComputeAmount = discountComputeAmount;
		temp.isOwner = isOwner;
		return temp;
	}

	public Long getSaleOrderLotId() {
		return saleOrderLotId;
	}

	public void setSaleOrderLotId(Long saleOrderLotId) {
		this.saleOrderLotId = saleOrderLotId;
	}

}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ShopProductType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "SHOP_PRODUCT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SHOP_PRODUCT_SEQ", allocationSize = 1)
public class ShopProduct implements Serializable {

	private static final long serialVersionUID = 1L;
	//ngay ban hang, vi du ban thu 3, 5, 7  thi luu 0000357
	@Basic
	@Column(name = "CALENDAR_D", length = 40)
	private String calendarD;

	//tuan ban hang - chua dung
	@Basic
	@Column(name = "CALENDAR_W", length = 40)
	private String calendarW;

	//nganh hang - lay thong tin tu bang product_info
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "CAT_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo cat;

	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "CREATE_USER", length = 40)
	private String createUser;

	//ngay ton kho toi da
	@Basic
	@Column(name = "MAXSF", length = 22)
	private Integer maxsf;

	//ngay ton kho toi thieu
	@Basic
	@Column(name = "MINSF", length = 22)
	private Integer minsf;

	//ngay di duong
	@Basic
	@Column(name = "LEAD", length = 22)
	private Integer lead;

	//ti le tang truong
	@Basic
	@Column(name = "PERCENTAGE", length = 22)
	private Float percentage;

	//id san pham neu type =2
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//chua dung
	@Basic
	@Column(name = "REGION", length = 10)
	private String region;

	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SHOP_PRODUCT_ID")
	private Long id;

	//trang thai: 1: hoat dong, 0: ko hoat dong
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	//loai 1: phan cho cat,2: phan cho sp
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ShopProductType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ShopProductType type = ShopProductType.PRODUCT;

	//ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 40)
	private String updateUser;

	public String getCalendarD() {
		return calendarD;
	}

	public void setCalendarD(String calendarD) {
		this.calendarD = calendarD;
	}

	public String getCalendarW() {
		return calendarW;
	}

	public void setCalendarW(String calendarW) {
		this.calendarW = calendarW;
	}

	public ProductInfo getCat() {
		return cat;
	}

	public void setCat(ProductInfo cat) {
		this.cat = cat;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Integer getMaxsf() {
		if (maxsf == null) {
			maxsf = 0;
		}
		return maxsf;
	}

	public void setMaxsf(Integer maxsf) {
		this.maxsf = maxsf;
	}

	public Integer getMinsf() {
		if (minsf == null) {
			minsf = 0;
		}
		return minsf;
	}

	public void setMinsf(Integer minsf) {
		this.minsf = minsf;
	}

	public Integer getLead() {
		if (lead == null) {
			lead = 0;
		}
		return lead;
	}

	public void setLead(Integer lead) {
		this.lead = lead;
	}

	public Float getPercentage() {
		return percentage == null ? 0f : percentage;
	}

	public void setPercentage(Float percentage) {
		this.percentage = percentage;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public ShopProductType getType() {
		return type;
	}

	public void setType(ShopProductType type) {
		this.type = type;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
}
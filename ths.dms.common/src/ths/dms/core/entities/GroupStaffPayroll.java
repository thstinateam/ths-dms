package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.GroupStaffPayrollObjApply;

/**
 * 
 * Auto generate entities - HaNoi standard
 * 
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "GROUP_STAFF_PAYROLL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "GROUP_STAFF_PAYROLL_SEQ", allocationSize = 1)
public class GroupStaffPayroll implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	// ma nhom nhan vien
	@Basic
	@Column(name = "GROUP_STAFF_PAYROLL_CODE", length = 50)
	private String groupStaffPrCode;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "GROUP_STAFF_PAYROLL_ID")
	private Long id;

	// ten nhom nhan vien
	@Basic
	@Column(name = "GROUP_STAFF_PAYROLL_NAME", length = 500)
	private String groupStaffPrName;

	// doi tuong ap dung: 1 don vi, 2 nhan vien
	@Basic
	@Column(name = "OBJECT_APPLY", length = 22)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.GroupStaffPayrollObjApply"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private GroupStaffPayrollObjApply objectApply;

	// id shop
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	// NULL TAT CA, LAY TRONG CHANNEL_TYPE VOI TYPE = 2
	 @ManyToOne(targetEntity = ChannelType.class)
	 @JoinColumn(name = "STAFF_TYPE_ID", referencedColumnName = "CHANNEL_TYPE_ID")
	 private ChannelType staffType;
	
//	@Basic
//	@Column(name = "STAFF_TYPE_ID", length = 22)
//	private Integer staffTypeId;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	public GroupStaffPayroll clone() {
		GroupStaffPayroll obj = new GroupStaffPayroll();

		obj.setCreateDate(createDate);
		obj.setCreateUser(createUser);
		obj.setGroupStaffPrCode(groupStaffPrCode);
		obj.setGroupStaffPrName(groupStaffPrName);
		obj.setId(id);
		obj.setObjectApply(objectApply);
		obj.setShop(shop);
		obj.setStaffType(staffType);
		obj.setUpdateDate(updateDate);
		obj.setUpdateUser(updateUser);

		return obj;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getGroupStaffPrCode() {
		return groupStaffPrCode;
	}

	public void setGroupStaffPrCode(String groupStaffPrCode) {
		this.groupStaffPrCode = groupStaffPrCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupStaffPrName() {
		return groupStaffPrName;
	}

	public void setGroupStaffPrName(String groupStaffPrName) {
		this.groupStaffPrName = groupStaffPrName;
	}

	public GroupStaffPayrollObjApply getObjectApply() {
		return objectApply;
	}

	public void setObjectApply(GroupStaffPayrollObjApply objectApply) {
		this.objectApply = objectApply;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

//	public Integer getStaffTypeId() {
//		return staffTypeId;
//	}
//
//	public void setStaffTypeId(Integer staffTypeId) {
//		this.staffTypeId = staffTypeId;
//	}
	
	
	public ChannelType getStaffType() {
		return staffType;
	}
	
	public void setStaffType(ChannelType staffType) {
		this.staffType = staffType;
	}

	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
}
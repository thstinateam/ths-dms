package ths.dms.core.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the DISPLAY_TOOLS_PRODUCT database table.
 * 
 */
@Entity
@Table(name="DISPLAY_TOOLS_PRODUCT")
public class DisplayToolsProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DISPLAY_TOOLS_PRODUCT_ID_GENERATOR", sequenceName="DISPLAY_TOOLS_PRODUCT_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DISPLAY_TOOLS_PRODUCT_ID_GENERATOR")
	private Long id;

	@ManyToOne
	@JoinColumn(name="PRODUCT_ID")
	private Product product;
	
	@ManyToOne
	@JoinColumn(name="TOOL_ID")
	private Product tool;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Product getTool() {
		return tool;
	}

	public void setTool(Product tool) {
		this.tool = tool;
	}
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "ROUTING_CAR_CUST")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "ROUTING_CAR_CUST_SEQ", allocationSize = 1)
public class RoutingCarCust implements Serializable {

	private static final long serialVersionUID = 1L;
	

	//id bang tuyen
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "ROUTING_CAR_CUST_ID")
	private Long id;

	//id KH
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;
	
	//thu tu
	@Basic
	@Column(name = "SEQ", length = 10)
	private Integer seq;

	//id tuyen
	@ManyToOne(targetEntity = RoutingCar.class)
	@JoinColumn(name = "ROUTING_CAR_ID", referencedColumnName = "ROUTING_CAR_ID")
	private RoutingCar routingCar;
	
	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", length = 7)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//ngay update
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public RoutingCar getRoutingCar() {
		return routingCar;
	}

	public void setRoutingCar(RoutingCar routingCar) {
		this.routingCar = routingCar;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.StockObjectType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "RPT_STOCK_TOTAL_MONTH")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "RPT_STOCK_TOTAL_MONTH_SEQ", allocationSize = 1)
public class RptStockTotalMonth implements Serializable {

	private static final long serialVersionUID = 1L;
	//tong cuoi thang = OPEN_STOCK_TOTAL + IMPORT_TOTAL - EXPORT_TOTAL
	@Basic
	@Column(name = "CLOSE_STOCK_TOTAL", length = 22)
	private Integer closeStockTotal;

	//ngay tao moi thang chi luu 1 lan
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//tong tra trong thang = xuat bán presale + xuat km + xuat doi,huy,tra + xuat vansale + tra VNM + xuat dieu chinh
	@Basic
	@Column(name = "EXPORT_TOTAL", length = 22)
	private Integer exportTotal;

	//tong tra VNM
	@Basic
	@Column(name = "EXPORT_TOTAL_VNM", length = 22)
	private Integer exportTotalVnm;

	//tong nhap trong thang  = tong nhap VNM +tong tra tu KH + nhap vansale + nhap dieu chinh
	@Basic
	@Column(name = "IMPORT_TOTAL", length = 22)
	private Integer importTotal;

	//tong nhap VNM
	@Basic
	@Column(name = "IMPORT_TOTAL_VNM", length = 22)
	private Integer importTotalVnm;

	//tong dau thang
	@Basic
	@Column(name = "OPEN_STOCK_TOTAL", length = 22)
	private Integer openStockTotal;

	//id doi tuong
	@Basic
	@Column(name = "OBJECT_ID")
	private Long objectId;

	//loai doi tuong (1:shop, 2:staff, 3:customer), hien tai se chi tong hop cho shop
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.StockObjectType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private StockObjectType objectType;

	//id san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//thang tong hop
	@Basic
	@Column(name = "RPT_IN_MONTH", length = 7)
	private Date rptInMonth;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "RPT_STOCK_TOTAL_MONTH_ID")
	private Long id;

	//tong huy, doi, tra presale
	@Basic
	@Column(name = "SALE_PROMOTION_D_PRE", length = 22)
	private Integer salePromotionDPre;

	//tong huy, doi, tra vansale
	@Basic
	@Column(name = "SALE_PROMOTION_D_VAN", length = 22)
	private Integer salePromotionDVan;

	//tong km (bao gom ca km, tra thuong) presale
	@Basic
	@Column(name = "SALE_PROMOTION_PRE", length = 22)
	private Integer salePromotionPre;

	//tong km (bao gom ca km, tra thuong) vansale
	@Basic
	@Column(name = "SALE_PROMOTION_VAN", length = 22)
	private Integer salePromotionVan;

	//tong ban
	@Basic
	@Column(name = "SALE_TOTAL_PRE", length = 22)
	private Integer saleTotalPre;

	//tong so ban trong thang vansale
	@Basic
	@Column(name = "SALE_TOTAL_VAN", length = 22)
	private Integer saleTotalVan;

	//ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	public Integer getCloseStockTotal() {
		if (closeStockTotal == null) {
			closeStockTotal = 0;
		}
		return closeStockTotal;
	}

	public void setCloseStockTotal(Integer closeStockTotal) {
		this.closeStockTotal = closeStockTotal;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getExportTotal() {
		if (exportTotal == null) {
			exportTotal = 0;
		}
		return exportTotal;
	}

	public void setExportTotal(Integer exportTotal) {
		this.exportTotal = exportTotal;
	}

	public Integer getExportTotalVnm() {
		if (exportTotalVnm == null) {
			exportTotalVnm = 0;
		}
		return exportTotalVnm;
	}

	public void setExportTotalVnm(Integer exportTotalVnm) {
		this.exportTotalVnm = exportTotalVnm;
	}

	public Integer getImportTotal() {
		if (importTotal == null) {
			importTotal = 0;
		}
		return importTotal;
	}

	public void setImportTotal(Integer importTotal) {
		this.importTotal = importTotal;
	}

	public Integer getImportTotalVnm() {
		if (importTotalVnm == null) {
			importTotalVnm = 0;
		}
		return importTotalVnm;
	}

	public void setImportTotalVnm(Integer importTotalVnm) {
		this.importTotalVnm = importTotalVnm;
	}

	public Integer getOpenStockTotal() {
		if (openStockTotal == null) {
			openStockTotal = 0;
		}
		return openStockTotal;
	}

	public void setOpenStockTotal(Integer openStockTotal) {
		this.openStockTotal = openStockTotal;
	}
	
	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public StockObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(StockObjectType objectType) {
		this.objectType = objectType;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Date getRptInMonth() {
		return rptInMonth;
	}

	public void setRptInMonth(Date rptInMonth) {
		this.rptInMonth = rptInMonth;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSalePromotionDPre() {
		if (salePromotionDPre == null) {
			salePromotionDPre = 0;
		}
		return salePromotionDPre;
	}

	public void setSalePromotionDPre(Integer salePromotionDPre) {
		this.salePromotionDPre = salePromotionDPre;
	}

	public Integer getSalePromotionDVan() {
		if (salePromotionDVan == null) {
			salePromotionDVan = 0;
		}
		return salePromotionDVan;
	}

	public void setSalePromotionDVan(Integer salePromotionDVan) {
		this.salePromotionDVan = salePromotionDVan;
	}

	public Integer getSalePromotionPre() {
		if (salePromotionPre == null) {
			salePromotionPre = 0;
		}
		return salePromotionPre;
	}

	public void setSalePromotionPre(Integer salePromotionPre) {
		this.salePromotionPre = salePromotionPre;
	}

	public Integer getSalePromotionVan() {
		if (salePromotionVan == null) {
			salePromotionVan = 0;
		}
		return salePromotionVan;
	}

	public void setSalePromotionVan(Integer salePromotionVan) {
		this.salePromotionVan = salePromotionVan;
	}

	public Integer getSaleTotalPre() {
		if (saleTotalPre == null) {
			saleTotalPre = 0;
		}
		return saleTotalPre;
	}

	public void setSaleTotalPre(Integer saleTotalPre) {
		this.saleTotalPre = saleTotalPre;
	}

	public Integer getSaleTotalVan() {
		if (saleTotalVan == null) {
			saleTotalVan = 0;
		}
		return saleTotalVan;
	}

	public void setSaleTotalVan(Integer saleTotalVan) {
		this.saleTotalVan = saleTotalVan;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * The persistent class for the EQUIP_STOCK_ADJUST_FORM database table.
 * 
 * @author Datpv4
 * @since July 02,2015
 * @description entities chi tiet kho dieu chinh
 * 
 */

@Entity
@Table(name = "EQUIP_STOCK_ADJUST_FORM_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_STOCK_ADJUST_FORM_DL_SEQ ", allocationSize = 1)
public class EquipStockAdjustFormDtl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_STOCK_ADJUST_FORM_DTL_ID")
	private Long id;

	@ManyToOne(targetEntity = EquipStockAdjustForm.class)
	@JoinColumn(name = "EQUIP_STOCK_ADJUST_FORM_ID", referencedColumnName = "EQUIP_STOCK_ADJUST_FORM_ID")
	private EquipStockAdjustForm equipStockAdjustForm;

	@ManyToOne(targetEntity = EquipGroup.class)
	@JoinColumn(name = "EQUIP_GROUP_ID", referencedColumnName = "EQUIP_GROUP_ID")
	private EquipGroup equipGroup;

	@Column(name = "CAPACITY_FROM", length = 22)
	private BigDecimal capacityFrom;

	@Column(name = "CAPACITY_TO", length = 22)
	private BigDecimal capacityTo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATE_DATE")
	private Date createDate;

	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Column(name = "EQUIP_CODE", length = 100)
	private String equipCode;

	@Column(name = "EQUIP_GROUP_NAME", length = 250)
	private String equipGroupName;

	@Column(name = "EQUIP_ID", length = 22)
	private Long equipId;

	@ManyToOne(targetEntity = EquipProvider.class)
	@JoinColumn(name = "EQUIP_PROVIDER_ID", referencedColumnName = "EQUIP_PROVIDER_ID")
	private EquipProvider equipProvider;

	@Column(name = "EQUIP_PROVIDER_NAME", length = 200)
	private String equipProviderName;

	@Column(name = "HEALTH_STATUS", length = 500)
	private String healthStatus;

	@Column(name = "MANUFACTURING_YEAR", length = 5)
	private Integer manufacturingYear;

	@Column(length = 22)
	private BigDecimal price;

	@Column(length = 50)
	private String serial;

	@Column(name = "STOCK_ID", length = 20)
	private Long stockId;

	@Column(name = "STOCK_NAME", length = 250)
	private String stockName;

	@Column(name = "STOCK_TYPE", length = 2)
	private Integer stockType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATE_DATE")
	private Date updateDate;

	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "WARRANTY_EXPIRED_DATE")
	private Date warrantyExpiredDate;

	@Column(name = "NOTE", length = 1000)
	private String note;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipStockAdjustForm getEquipStockAdjustForm() {
		return equipStockAdjustForm;
	}

	public void setEquipStockAdjustForm(EquipStockAdjustForm equipStockAdjustForm) {
		this.equipStockAdjustForm = equipStockAdjustForm;
	}

	public EquipGroup getEquipGroup() {
		return equipGroup;
	}

	public void setEquipGroup(EquipGroup equipGroup) {
		this.equipGroup = equipGroup;
	}

	public BigDecimal getCapacityFrom() {
		return capacityFrom;
	}

	public void setCapacityFrom(BigDecimal capacityFrom) {
		this.capacityFrom = capacityFrom;
	}

	public BigDecimal getCapacityTo() {
		return capacityTo;
	}

	public void setCapacityTo(BigDecimal capacityTo) {
		this.capacityTo = capacityTo;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getEquipGroupName() {
		return equipGroupName;
	}

	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public EquipProvider getEquipProvider() {
		return equipProvider;
	}

	public void setEquipProvider(EquipProvider equipProvider) {
		this.equipProvider = equipProvider;
	}

	public String getEquipProviderName() {
		return equipProviderName;
	}

	public void setEquipProviderName(String equipProviderName) {
		this.equipProviderName = equipProviderName;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public Integer getManufacturingYear() {
		return manufacturingYear;
	}

	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Integer getStockType() {
		return stockType;
	}

	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getWarrantyExpiredDate() {
		return warrantyExpiredDate;
	}

	public void setWarrantyExpiredDate(Date warrantyExpiredDate) {
		this.warrantyExpiredDate = warrantyExpiredDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
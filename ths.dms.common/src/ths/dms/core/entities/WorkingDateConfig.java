package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.WorkingDateType;

@Entity
@Table(name = "WORKING_DATE_CONFIG")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "WORKING_DATE_CONFIG_SEQ", allocationSize = 1)
public class WorkingDateConfig implements Serializable {

	private static final long serialVersionUID = 1L;

	//ID don vi se lay theo truong hop TYPE = 2
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "BASE_ON_SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop baseOnShop;

	//Tu ngay
	@Basic
	@Column(name = "FROM_DATE", length = 7)
	private Date fromDate;

	//Do uu tien [-99 --> 99] Cang lon cang uu tien
	@Basic
	@Column(name = "PRIORITY", length = 22)
	private Integer priority;

	//ID don vi
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	@Basic
	@Column(name = "STATUS", length = 22)
	private Integer status;

	//den ngay
	@Basic
	@Column(name = "TO_DATE", length = 7)
	private Date toDate;

	//Chiu anh huong cua cau hinh: 1: Ban than don vi; 2: Chi dinh ro don vi se lay theo; 3: de quy len cap tren
	/*@Basic
	@Column(name = "TYPE", length = 22)
	private WorkingDateType type;*/
	@Basic
	@Column(name = "TYPE",  columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.WorkingDateType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private WorkingDateType type;
	
	//ID bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "WORKING_DATE_CONFIG_ID")
	private Long id;

	public Shop getBaseOnShop() {
		return baseOnShop;
	}

	public void setBaseOnShop(Shop baseOnShop) {
		this.baseOnShop = baseOnShop;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public WorkingDateType getType() {
		return type;
	}

	public void setType(WorkingDateType type) {
		this.type = type;
	}

}
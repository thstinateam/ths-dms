package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.EquipStockTotalType;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_LIQUIDATION_FORM_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_LIQUIDATION_FORM_DTL_SEQ", allocationSize = 1)
public class EquipLiquidationFormDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//ID lo?i thi?t b?
	@ManyToOne(targetEntity = EquipCategory.class)
	@JoinColumn(name = "EQUIP_CATEGORY_ID", referencedColumnName = "EQUIP_CATEGORY_ID")
	private EquipCategory equipCategory;

	//ID nh�m thi?t b?
	@ManyToOne(targetEntity = EquipGroup.class)
	@JoinColumn(name = "EQUIP_GROUP_ID", referencedColumnName = "EQUIP_GROUP_ID")
	private EquipGroup equipGroup;

	//ID thi?t b?
	@ManyToOne(targetEntity = Equipment.class)
	@JoinColumn(name = "EQUIP_ID", referencedColumnName = "EQUIP_ID")
	private Equipment equip;

	//ID
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_LIQUIDATION_FORM_DTL_ID")
	private Long id;

	//ID phi?u thanh l?
	@ManyToOne(targetEntity = EquipLiquidationForm.class)
	@JoinColumn(name = "EQUIP_LIQUIDATION_FORM_ID", referencedColumnName = "EQUIP_LIQUIDATION_FORM_ID")
	private EquipLiquidationForm equipLiquidationForm;

	//Gi� tr? thi?t b?
	/*@Basic
	@Column(name = "EQUIP_VALUE", length = 22)
	private BigDecimal equipValue;*/

	//Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;
	
	// Gia tri thanh ly
	@Basic
	@Column(name = "LIQUIDATION_VALUE", length = 22)
	private BigDecimal liquidationValue;
	
	// Gia tri thu hoi
	@Basic
	@Column(name = "EVICTION_VALUE", length = 22)
	private BigDecimal evictionValue;
	
	// Ma kho cua thiet bi
	@Basic
	@Column(name = "STOCK_ID")
	private Long stockId;
	
	// Loai kho cua thiet bi: 1: NPP, 2: KH
	// vuongmq; 14/07/2015; dac ta cap nhat; 1. kho DonVi, 2. KH,
	@Basic
	@Column(name = "STOCK_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipStockTotalType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipStockTotalType stockType;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public EquipCategory getEquipCategory() {
		return equipCategory;
	}

	public void setEquipCategory(EquipCategory equipCategory) {
		this.equipCategory = equipCategory;
	}

	public EquipGroup getEquipGroup() {
		return equipGroup;
	}

	public void setEquipGroup(EquipGroup equipGroup) {
		this.equipGroup = equipGroup;
	}

	public Equipment getEquip() {
		return equip;
	}

	public void setEquip(Equipment equip) {
		this.equip = equip;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipLiquidationForm getEquipLiquidationForm() {
		return equipLiquidationForm;
	}

	public void setEquipLiquidationForm(EquipLiquidationForm equipLiquidationForm) {
		this.equipLiquidationForm = equipLiquidationForm;
	}

	/*public BigDecimal getEquipValue() {
		return equipValue;
	}

	public void setEquipValue(BigDecimal equipValue) {
		this.equipValue = equipValue;
	}*/

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public EquipStockTotalType getStockType() {
		return stockType;
	}

	public void setStockType(EquipStockTotalType stockType) {
		this.stockType = stockType;
	}

	public BigDecimal getLiquidationValue() {
		return liquidationValue;
	}

	public void setLiquidationValue(BigDecimal liquidationValue) {
		this.liquidationValue = liquidationValue;
	}

	public BigDecimal getEvictionValue() {
		return evictionValue;
	}

	public void setEvictionValue(BigDecimal evictionValue) {
		this.evictionValue = evictionValue;
	}

	
}
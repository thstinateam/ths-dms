package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Auto generate entities - HCM standard
 * 
 * @author tamvnm
 * @since April 01,2015
 * @description entities thuoc kenh thiet bi
 */
@Entity
@Table(name = "EQUIP_LEND_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_LEND_DETAIL_SEQ", allocationSize = 1)
public class EquipLendDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_LEND_DETAIL_ID")
	private Long id;

	//Equip Lend
	@ManyToOne(targetEntity = EquipLend.class)
	@JoinColumn(name = "EQUIP_LEND_ID", referencedColumnName = "EQUIP_LEND_ID")
	private EquipLend equipLend;

	//Equip Category
	@ManyToOne(targetEntity = EquipCategory.class)
	@JoinColumn(name = "EQUIP_CATEGORY_ID", referencedColumnName = "EQUIP_CATEGORY_ID")
	private EquipCategory equipCategory;
	
	//Equip Lend
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	//Ten khach hang
	@Basic
	@Column(name = "CUSTOMER_NAME", length = 100)
	private String customerName;

	//Ten khach hang
	@Basic
	@Column(name = "CUSTOMER_ADDRESS", length = 100)
	private String customerAddress;

	//Nguoi dai dien
	@Basic
	@Column(name = "REPRESENTATIVE", length = 100)
	private String representative;

	//Nguoi dai dien
	@Basic
	@Column(name = "RELATION", length = 100)
	private String relation;

	//Dien thoai
	@Basic
	@Column(name = "Phone", length = 30)
	private String phone;

	//Dia chi
	@Basic
	@Column(name = "ADDRESS", length = 400)
	private String address;

	//ID Tỉnh/Tp trong AREA
	@Basic
	@Column(name = "PROVINCE_ID", length = 20)
	private Long provinceId;

	//Giá trị Tỉnh/Tp
	@Basic
	@Column(name = "PROVINCE_NAME", length = 200)
	private String provinceName;

	//ID Quận/Huyện trong AREA
	@Basic
	@Column(name = "DISTRICT_ID", length = 20)
	private Long districtId;

	//Giá trị Quận/Huyện
	@Basic
	@Column(name = "DISTRICT_NAME", length = 200)
	private String districtName;

	//ID Xã/Phhường trong AREA
	@Basic
	@Column(name = "WARD_ID", length = 20)
	private Long wardId;

	//Giá trị Xã/Phường
	@Basic
	@Column(name = "WARD_NAME", length = 200)
	private String wardName;

	//Duong
	@Basic
	@Column(name = "STREET", length = 200)
	private String street;

	//So CMND
	@Basic
	@Column(name = "IDNO", length = 20)
	private String idNo;

	//Noi cap CMND
	@Basic
	@Column(name = "IDNO_PLACE", length = 200)
	private String idNoPlace;

	//ngay cap CMND
	@Basic
	@Column(name = "IDNO_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date idNoDate;

	//So ĐKKD
	@Basic
	@Column(name = "BUSINESS_NO", length = 20)
	private String businessNo;

	//Noi cap ĐKKD
	@Basic
	@Column(name = "BUSINESS_NO_PLACE", length = 200)
	private String businessNoPlace;

	//ngay cap ĐKKD
	@Basic
	@Column(name = "BUSINESS_NO_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date businessNoDate;

	//Dung tich
	@Basic
	@Column(name = "CAPACITY", length = 200)
	private String capacity;

	//Tinh trang
	@Basic
	@Column(name = "HEALTH_STATUS", length = 200)
	private String healthStatus;

	//Thoi gian muon
	@Basic
	@Column(name = "TIME_LEND", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date timeLend;

	//Kho
	@Basic
	@Column(name = "OBJECT_TYPE", length = 20)
	private Integer objectType;

	//So luong
	@Basic
	@Column(name = "QUANTITY", length = 20)
	private BigDecimal quantity;

	//Doanh so
	@Basic
	@Column(name = "AMOUNT", length = 20)
	private BigDecimal amount;

	//tran thai: 0: off, 1: on
	@Basic
	@Column(name = "STATUS", length = 22)
	private Integer status;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipLend getEquipLend() {
		return equipLend;
	}

	public void setEquipLend(EquipLend equipLend) {
		this.equipLend = equipLend;
	}

	public EquipCategory getEquipCategory() {
		return equipCategory;
	}

	public void setEquipCategory(EquipCategory equipCategory) {
		this.equipCategory = equipCategory;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public Long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public Long getWardId() {
		return wardId;
	}

	public void setWardId(Long wardId) {
		this.wardId = wardId;
	}

	public String getWardName() {
		return wardName;
	}

	public void setWardName(String wardName) {
		this.wardName = wardName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getIdNoPlace() {
		return idNoPlace;
	}

	public void setIdNoPlace(String idNoPlace) {
		this.idNoPlace = idNoPlace;
	}

	public Date getIdNoDate() {
		return idNoDate;
	}

	public void setIdNoDate(Date idNoDate) {
		this.idNoDate = idNoDate;
	}

	public String getBusinessNoPlace() {
		return businessNoPlace;
	}

	public void setBusinessNoPlace(String businessNoPlace) {
		this.businessNoPlace = businessNoPlace;
	}

	public Date getBusinessNoDate() {
		return businessNoDate;
	}

	public void setBusinessNoDate(Date businessNoDate) {
		this.businessNoDate = businessNoDate;
	}

	public Date getTimeLend() {
		return timeLend;
	}

	public void setTimeLend(Date timeLend) {
		this.timeLend = timeLend;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getRepresentative() {
		return representative;
	}

	public void setRepresentative(String representative) {
		this.representative = representative;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getBusinessNo() {
		return businessNo;
	}

	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}
}
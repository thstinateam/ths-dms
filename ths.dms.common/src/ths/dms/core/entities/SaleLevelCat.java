package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "SALE_LEVEL_CAT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SALE_LEVEL_CAT_SEQ", allocationSize = 1)
public class SaleLevelCat implements Serializable {

	private static final long serialVersionUID = 1L;
	//id nganh hang
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "CAT_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo cat;

	//tu so
	@Basic
	@Column(name = "FROM_AMOUNT", length = 22)
	private BigDecimal fromAmount;

	//muc doanh thu
	@Basic
	@Column(name = "SALE_LEVEL_CODE", length = 20)
	private String saleLevelCode;
	
	@Basic
	@Column(name = "SALE_LEVEL_NAME", length = 50)
	private String saleLevelName;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SALE_LEVEL_CAT_ID")
	private Long id;

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	//den so
	@Basic
	@Column(name = "TO_AMOUNT", length = 22)
	private BigDecimal toAmount;
	
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	public ProductInfo getCat() {
		return cat;
	}

	public void setCat(ProductInfo cat) {
		this.cat = cat;
	}

	public BigDecimal getFromAmount() {
		return fromAmount == null ? BigDecimal.valueOf(0) : fromAmount;
	}

	public void setFromAmount(BigDecimal fromAmount) {
		this.fromAmount = fromAmount;
	}

	public String getSaleLevelCode() {
		return saleLevelCode;
	}

	public void setSaleLevelCode(String saleLevelCode) {
		this.saleLevelCode = saleLevelCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public BigDecimal getToAmount() {
		return toAmount;
	}

	public void setToAmount(BigDecimal toAmount) {
		this.toAmount = toAmount;
	}
	public String getFromAmountString(){
		return fromAmount == null ? "" : fromAmount.toString();
	}
	public String getToAmountString() {
		return toAmount == null ? "" : toAmount.toString();
	}

	public String getSaleLevelName() {
		return saleLevelName;
	}

	public void setSaleLevelName(String saleLevelName) {
		this.saleLevelName = saleLevelName;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
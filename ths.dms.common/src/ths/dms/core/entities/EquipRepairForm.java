package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.common.utils.CDateUtil;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.PaymentStatusRepair;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_REPAIR_FORM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_REPAIR_FORM_SEQ", allocationSize = 1)
public class EquipRepairForm implements Serializable {

	private static final long serialVersionUID = 1L;

	//T?nh tr?ng h� h?ng
	@Basic
	@Column(name = "CONDITION", length = 1000)
	private String condition;
	
	//Ghi chu
	@Basic
	@Column(name = "NOTE", length = 500)
	private String note;

	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_FORM_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createFormDate;
	
	//Ngay lap
	@Basic
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	@Basic
	@Column(name = "COMPLETE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date completeDate;

	//Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//ID thi?t b? s?a ch?a
	@ManyToOne(targetEntity = Equipment.class)
	@JoinColumn(name = "EQUIP_ID", referencedColumnName = "EQUIP_ID")
	private Equipment equip;

	//K? c?a thi?t b?
	@ManyToOne(targetEntity = EquipPeriod.class)
	@JoinColumn(name = "EQUIP_PERIOD_ID", referencedColumnName = "EQUIP_PERIOD_ID")
	private EquipPeriod equipPeriod;

	//M? phi?u
	@Basic
	@Column(name = "EQUIP_REPAIR_FORM_CODE", length = 400)
	private String equipRepairFormCode;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_REPAIR_FORM_ID")
	private Long id;

	//Ma kho cua doi tuong Stock Id
	@Basic
	@Column(name = "STOCK_CODE", length = 400)
	private String stockCode;

	//L? do
	@Basic
	@Column(name = "REASON", length = 500)
	private String reason;
	
	@Basic
	@Column(name = "REJECT_REASON", length = 500)
	private String rejectReason;

	//S? l?n s?a ch?a
	@Basic
	@Column(name = "REPAIR_COUNT", length = 22)
	private Integer repairCount;

	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.StatusRecordsEquip"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private StatusRecordsEquip status = StatusRecordsEquip.DRAFT;

	//ID c?a �?i t�?ng trong OBJECT_TYPE
	@Basic
	@Column(name = "STOCK_ID", length = 22)
	private Long stockId;

	// 1. VNM, 2. NPP, 3. KH
	// vuongmq: 25/04/2015; 1: kho DV, 2: kho KH
	@Basic
	@Column(name = "STOCK_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipStockTotalType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipStockTotalType stockType;

	//T?ng ti?n
	@Basic
	@Column(name = "TOTAL_AMOUNT", length = 22)
	private BigDecimal totalAmount;

	// doanh so TB
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	// Gia nhan cong
	@Basic
	@Column(name = "WORKER_PRICE", length = 22)
	private BigDecimal workerPrice;
	
	// Ngay het han bao hanh
	@Basic
	@Column(name = "WARRANTY_EXPIRED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date warrantyExpireDate;
	
	
	//Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//id staff
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	//Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	//Ngay duyet bien ban
	@Basic
	@Column(name = "APPROVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approveDate;
	
	// trang thai thanh toan cua phieu sua chua 
	@Basic
	@Column(name = "PAYMENT_STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PaymentStatusRepair"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PaymentStatusRepair statusPayment = PaymentStatusRepair.NOT_PAID_YET;
	
	// tam ngay het han bao hanh
	@Transient
	private String warrantyExpireDateStr;
	
	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Equipment getEquip() {
		return equip;
	}

	public void setEquip(Equipment equip) {
		this.equip = equip;
	}

	public EquipPeriod getEquipPeriod() {
		return equipPeriod;
	}

	public void setEquipPeriod(EquipPeriod equipPeriod) {
		this.equipPeriod = equipPeriod;
	}

	public String getEquipRepairFormCode() {
		return equipRepairFormCode;
	}

	public void setEquipRepairFormCode(String equipRepairFormCode) {
		this.equipRepairFormCode = equipRepairFormCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public StatusRecordsEquip getStatus() {
		return status;
	}

	public void setStatus(StatusRecordsEquip status) {
		this.status = status;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public Integer getRepairCount() {
		return repairCount;
	}

	public void setRepairCount(Integer repairCount) {
		this.repairCount = repairCount;
	}

	public EquipStockTotalType getStockType() {
		return stockType;
	}

	public void setStockType(EquipStockTotalType stockType) {
		this.stockType = stockType;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getWorkerPrice() {
		return workerPrice;
	}

	public void setWorkerPrice(BigDecimal workerPrice) {
		this.workerPrice = workerPrice;
	}

	public Date getWarrantyExpireDate() {
		return warrantyExpireDate;
	}

	public void setWarrantyExpireDate(Date warrantyExpireDate) {
		this.warrantyExpireDate = warrantyExpireDate;
	}

	public String getWarrantyExpireDateStr() {
		if(warrantyExpireDate != null){
			warrantyExpireDateStr = CDateUtil.toDateString(warrantyExpireDate, CDateUtil.DATE_FORMAT_STR);
		}
		return warrantyExpireDateStr;
	}

	public void setWarrantyExpireDateStr(String warrantyExpireDateStr) {
		this.warrantyExpireDateStr = warrantyExpireDateStr;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public PaymentStatusRepair getStatusPayment() {
		return statusPayment;
	}

	public void setStatusPayment(PaymentStatusRepair statusPayment) {
		this.statusPayment = statusPayment;
	}

	public Date getCreateFormDate() {
		return createFormDate;
	}

	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the MEDIA_MAP database table.
 * 
 */
@Entity
@Table(name="MEDIA_MAP")
public class MediaMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MEDIA_MAP_ID_GENERATOR", sequenceName="MEDIA_MAP_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MEDIA_MAP_ID_GENERATOR")
	@Column(name="MEDIA_MAP_ID")
	private Long mediaMapId;	

	@ManyToOne
	@JoinColumn(name="MEDIA_ID")
	private Media media;
	
	@Column(name="OBJECT_TYPE")
	private Integer objectType;

	@Column(name="OBJECT_ID")
	private Long objectId;
    
	@Column(name="CREATE_USER")
	private String createUser;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="CREATE_DATE")
	private Date createDate;

	@Column(name="UPDATE_USER")
	private String updateUser;
	
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DATE")
	private Date updateDate;

    @Column(name="STATUS")
    private Integer status;
    
	public Long getMediaMapId() {
		return mediaMapId;
	}

	public void setMediaMapId(Long mediaMapId) {
		this.mediaMapId = mediaMapId;
	}	

	public Media getMedia() {
		return media;
	}

	public void setMedia(Media media) {
		this.media = media;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PROMOTION_CUSTOMER_MAP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_PROMOTION_CUSTOMER_MAP", sequenceName = "PROMOTION_CUSTOMER_MAP_SEQ", allocationSize = 1)
public class PromotionCustomerMap implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PROMOTION_CUSTOMER_MAP")
	@Column(name = "PROMOTION_CUSTOMER_MAP_ID")
	private Long id;
	
	//khoa ngoai voi bang PROMOTION_SHOP_MAP
	@ManyToOne(targetEntity = PromotionShopMap.class)
	@JoinColumn(name = "PROMOTION_SHOP_MAP_ID", referencedColumnName = "PROMOTION_SHOP_MAP_ID")
	private PromotionShopMap promotionShopMap;
	
	//id npp
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	//id kh
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;
	
	//so lan co the nhan km
	@Basic
	@Column(name = "QUANTITY_MAX", length = 22)
	private Integer quantityMax;

	//so lan da nhan km
	@Basic
	@Column(name = "QUANTITY_RECEIVED", length = 22)
	private Integer quantityReceived;
	
	//so lan co the nhan km
	@Basic
	@Column(name = "AMOUNT_MAX", length = 22)
	private BigDecimal amountMax;
	
	//so lan da nhan km
	@Basic
	@Column(name = "AMOUNT_RECEIVED", length = 22)
	private BigDecimal amountReceived;
	
	//so lan co the nhan km
	@Basic
	@Column(name = "NUM_MAX", length = 22)
	private BigDecimal numMax;
	
	//so lan da nhan km
	@Basic
	@Column(name = "NUM_RECEIVED", length = 22)
	private BigDecimal numReceived;
	
	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;


	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	//so suat tong da duyet + chua duyet
	@Basic
	@Column(name = "QUANTITY_RECEIVED_TOTAL", length = 20)
	private Integer quantityReceivedTotal;
	
	//so tien tong da duyet + chua duyet
	@Basic
	@Column(name = "AMOUNT_RECEIVED_TOTAL", length = 26)
	private BigDecimal amountReceivedTotal;
	
	//so luong tong da duyet + chua duyet
	@Basic
	@Column(name = "NUM_RECEIVED_TOTAL", length = 20)
	private BigDecimal numReceivedTotal;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PromotionShopMap getPromotionShopMap() {
		return promotionShopMap;
	}

	public void setPromotionShopMap(PromotionShopMap promotionShopMap) {
		this.promotionShopMap = promotionShopMap;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Integer getQuantityMax() {
		return quantityMax;
	}

	public void setQuantityMax(Integer quantityMax) {
		this.quantityMax = quantityMax;
	}

	public Integer getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public BigDecimal getAmountMax() {
		return amountMax;
	}

	public void setAmountMax(BigDecimal amountMax) {
		this.amountMax = amountMax;
	}

	public BigDecimal getAmountReceived() {
		return amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}

	public BigDecimal getNumMax() {
		return numMax;
	}

	public void setNumMax(BigDecimal numMax) {
		this.numMax = numMax;
	}

	public BigDecimal getNumReceived() {
		return numReceived;
	}

	public void setNumReceived(BigDecimal numReceived) {
		this.numReceived = numReceived;
	}

	public Integer getQuantityReceivedTotal() {
		return quantityReceivedTotal;
	}

	public void setQuantityReceivedTotal(Integer quantityReceivedTotal) {
		this.quantityReceivedTotal = quantityReceivedTotal;
	}

	public BigDecimal getAmountReceivedTotal() {
		return amountReceivedTotal;
	}

	public void setAmountReceivedTotal(BigDecimal amountReceivedTotal) {
		this.amountReceivedTotal = amountReceivedTotal;
	}

	public BigDecimal getNumReceivedTotal() {
		return numReceivedTotal;
	}

	public void setNumReceivedTotal(BigDecimal numReceivedTotal) {
		this.numReceivedTotal = numReceivedTotal;
	}
	
}
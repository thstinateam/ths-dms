package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.IsCheckType;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_LIQUIDATION_FORM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_LIQUIDATION_FORM_SEQ", allocationSize = 1)
public class EquipLiquidationForm implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//Ng�y duy?t bi�n b?n
	@Basic
	@Column(name = "APPROVE_DATE", length = 7)
	private Date approveDate;
	
	//�?a ch? ng�?i mua
	@Basic
	@Column(name = "EQUIP_LIQUIDATION_DOC_NUMBER", length = 500)
	private String docNumber;
	
	//�?a ch? ng�?i mua
	@Basic
	@Column(name = "BUYER_ADDRESS", length = 500)
	private String buyerAddress;

	//T�n ng�?i mua
	@Basic
	@Column(name = "BUYER_NAME", length = 500)
	private String buyerName;

	//S�T ng�?i mua
	@Basic
	@Column(name = "BUYER_PHONE_NUMBER", length = 200)
	private String buyerPhoneNumber;

	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_FORM_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createFormDate;
	
	//Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//M? bi�n b?n
	@Basic
	@Column(name = "EQUIP_LIQUIDATION_FORM_CODE", length = 200)
	private String equipLiquidationFormCode;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_LIQUIDATION_FORM_ID")
	private Long id;

	//ID k? bi�n b?n
	@ManyToOne(targetEntity = EquipPeriod.class)
	@JoinColumn(name = "EQUIP_PERIOD_ID", referencedColumnName = "EQUIP_PERIOD_ID")
	private EquipPeriod equipPeriod;

	//L? do
	@Basic
	@Column(name = "REASON", length = 500)
	private String reason;
	
	//Ghi chu
	@Basic
	@Column(name = "NOTE", length = 500)
	private String note;


	//Trạng thái biên bản 0: Dự thảo, 2: Đã duyệt,  4: Hủy, 7: Đã thanh lý
	@Basic
	@Column(name = "STATUS", length = 22)
	private Integer status;

	//Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	// Trang thai lo: 1 check, 0; khong check
	@Basic
	@Column(name = "IS_CHECK_LOT", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.IsCheckType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private IsCheckType checkLot = IsCheckType.NO;
	
	// gia tri thanh ly
	@Basic
	@Column(name = "LIQUIDATION_VALUE", length = 22)
	private BigDecimal liquidationValue;
	
	// gia tri thu hoi
	@Basic
	@Column(name = "EVICTION_VALUE", length = 22)
	private BigDecimal evictionValue;

	public String getBuyerAddress() {
		return buyerAddress;
	}

	public void setBuyerAddress(String buyerAddress) {
		this.buyerAddress = buyerAddress;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getBuyerPhoneNumber() {
		return buyerPhoneNumber;
	}

	public void setBuyerPhoneNumber(String buyerPhoneNumber) {
		this.buyerPhoneNumber = buyerPhoneNumber;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getEquipLiquidationFormCode() {
		return equipLiquidationFormCode;
	}

	public void setEquipLiquidationFormCode(String equipLiquidationFormCode) {
		this.equipLiquidationFormCode = equipLiquidationFormCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipPeriod getEquipPeriod() {
		return equipPeriod;
	}

	public void setEquipPeriod(EquipPeriod equipPeriod) {
		this.equipPeriod = equipPeriod;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public IsCheckType getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(IsCheckType checkLot) {
		this.checkLot = checkLot;
	}

	public BigDecimal getLiquidationValue() {
		return liquidationValue;
	}

	public void setLiquidationValue(BigDecimal liquidationValue) {
		this.liquidationValue = liquidationValue;
	}

	public BigDecimal getEvictionValue() {
		return evictionValue;
	}

	public void setEvictionValue(BigDecimal evictionValue) {
		this.evictionValue = evictionValue;
	}

	public Date getCreateFormDate() {
		return createFormDate;
	}

	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.StatusRecordsEquip;


/**
 * The persistent class for the EQUIP_STOCK_ADJUST_FORM database table.
 *  @author Datpv4
 * 	@since July 02,2015
 * 	@description entities kho dieu chinh
 * 
 */
@Entity
@Table(name="EQUIP_STOCK_ADJUST_FORM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_STOCK_ADJUST_FORM_SEQ", allocationSize = 1)
public class EquipStockAdjustForm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_STOCK_ADJUST_FORM_ID")
	private Long id;


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="APPROVE_DATE")
	private Date approveDate;

	@Column(length=100)
	private String code;
	
	@Basic
	@Column(name = "NOTE", length = 500)
	private String note;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATE_DATE")
	private Date createDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATE_FORM_DATE")
	private Date createFormDate;

	@Column(name="CREATE_USER", length=50)
	private String createUser;

	@Column(length=500)
	private String description;

	@Column(name="EQUIP_PERIOD_ID", nullable=false, length=22)
	private Long equipPeriodId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REFUSE_DATE")
	private Date refuseDate;

	// Trang thai duyet cua thanh toan
		@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.StatusRecordsEquip"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private StatusRecordsEquip status = StatusRecordsEquip.DRAFT;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DATE")
	private Date updateDate;

	@Column(name="UPDATE_USER", length=50)
	private String updateUser;

	public EquipStockAdjustForm() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getApproveDate() {
		return this.approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getCreateFormDate() {
		return this.createFormDate;
	}

	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Long getEquipPeriodId() {
		return equipPeriodId;
	}

	public void setEquipPeriodId(Long equipPeriodId) {
		this.equipPeriodId = equipPeriodId;
	}

	public StatusRecordsEquip getStatus() {
		return status;
	}


	public Date getRefuseDate() {
		return this.refuseDate;
	}

	public void setRefuseDate(Date refuseDate) {
		this.refuseDate = refuseDate;
	}

	public void setStatus(StatusRecordsEquip status) {
		this.status = status;
	}
	
	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */
@Entity
@Table(name = "PROMOTION_PROGRAM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_PROMOTION_PROGRAM", sequenceName = "PROMOTION_PROGRAM_SEQ", allocationSize = 1)
public class PromotionProgram implements Serializable {
	private static final long serialVersionUID = 1L;
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PROMOTION_PROGRAM")
	@Column(name = "PROMOTION_PROGRAM_ID")
	private Long id;

	//ma ctkm
	@Basic
	@Column(name = "PROMOTION_PROGRAM_CODE", length = 50)
	private String promotionProgramCode;
	
	//ten ctkm
	@Basic
	@Column(name = "PROMOTION_PROGRAM_NAME", length = 100)
	private String promotionProgramName;
	
	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.WAITING ;
	
	//loai km: ZV01 --> ZV21
	@Basic
	@Column(name = "TYPE", length = 100)
	private String type;
	
	//kieu km
	@Basic
	@Column(name = "PRO_FORMAT", length = 100)
	private String proFormat;
	
	//tu ngay
	@Basic
	@Column(name = "FROM_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromDate;
	
	//den ngay
	@Basic
	@Column(name = "TO_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date toDate;
	
	@Basic
	@Lob
	@Column(name = "DESCRIPTION")
	private String description;
	
	//kieu km
	@Basic
	@Column(name = "NAME_TEXT", length = 250)
	private String nameText;
	
	//0: khong cho phep sua so luong va so suat
	//1: cho phep sua so luong , khong hien thi so suat
	//2: cho phep sua so suat , khong duoc sua so luong
	@Basic
	@Column(name = "IS_EDITED")
	private Integer isEdited;
	
	@Basic
	@Column(name = "CAL_PRICE_VAT")
	private Integer calPriceVat;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Basic
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Basic
	@Column(name = "QUANTI_MONTH_NEW_OPEN", length = 50)
	private Integer quantiMonthNewOpen;
	
	@Basic
	@Column(name = "MD5_VALID_CODE", length = 50)
	private String md5ValidCode;
	
	@Transient
	private Integer hasPortion; // cho biet CTKM co so suat hay khong (1 co, else: khong)
	
	public String getMd5ValidCode() {
		return md5ValidCode;
	}

	public void setMd5ValidCode(String md5ValidCode) {
		this.md5ValidCode = md5ValidCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}

	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}

	public String getPromotionProgramName() {
		return promotionProgramName;
	}

	public void setPromotionProgramName(String promotionProgramName) {
		this.promotionProgramName = promotionProgramName;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProFormat() {
		return proFormat;
	}

	public void setProFormat(String proFormat) {
		this.proFormat = proFormat;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNameText() {
		return nameText;
	}

	public void setNameText(String nameText) {
		this.nameText = nameText;
	}

	public Integer getIsEdited() {
		return isEdited;
	}

	public void setIsEdited(Integer isEdited) {
		this.isEdited = isEdited;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getQuantiMonthNewOpen() {
		return quantiMonthNewOpen;
	}

	public void setQuantiMonthNewOpen(Integer quantiMonthNewOpen) {
		this.quantiMonthNewOpen = quantiMonthNewOpen;
	}

	public Integer getHasPortion() {
		return hasPortion;
	}

	public void setHasPortion(Integer hasPortion) {
		this.hasPortion = hasPortion;
	}
	
	public Integer getCalPriceVat() {
		return calPriceVat;
	}

	public void setCalPriceVat(Integer calPriceVat) {
		this.calPriceVat = calPriceVat;
	}
}

package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * @author thachnn
 * @since 16/9/2013
 * 
 */

@Entity
@Table(name = "DISPLAY_PRODUCT_GROUP_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "DISPLAY_PRODUCT_GROUP_DTL_SEQ", allocationSize = 1)
public class StDisplayPdGroupDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Temporal( TemporalType.DATE)
	@Column(name="CREATE_DATE")
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//id group dtl
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "DISPLAY_PRODUCT_GROUP_DTL_ID")
	private Long id;
	
	// id nhom cttb
	@ManyToOne(targetEntity = StDisplayPdGroup.class)
	@JoinColumn(name = "DISPLAY_PRODUCT_GROUP_ID", referencedColumnName = "DISPLAY_PRODUCT_GROUP_ID")
	private StDisplayPdGroup displayProductGroup;
	
	//id san pham ban
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//Kieu
	@Basic
	@Column(name = "CONVFACT")
	private Integer convfact;
	

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.WAITING ;
	
	

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;


	public StDisplayPdGroupDtl clone() {
		StDisplayPdGroupDtl ret = new StDisplayPdGroupDtl();
		ret.setCreateDate(createDate);
		ret.setCreateUser(createUser);
		ret.setUpdateDate(updateDate);
		ret.setUpdateUser(updateUser);
		ret.setId(id);
		ret.setStatus(status);
		ret.setDisplayProductGroup(displayProductGroup);
		ret.setConvfact(convfact);
		ret.setProduct(product);
		return ret;
	}
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public StDisplayPdGroup getDisplayProductGroup() {
		return displayProductGroup;
	}
	public void setDisplayProductGroup(StDisplayPdGroup displayProductGroup) {
		this.displayProductGroup = displayProductGroup;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	
	
	
}
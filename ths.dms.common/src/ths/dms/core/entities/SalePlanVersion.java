package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "SALE_PLAN_VERSION")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SALE_PLAN_VERSION_SEQ", allocationSize = 1)
public class SalePlanVersion implements Serializable {

	private static final long serialVersionUID = 1L;
	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//user tao
	@Basic
	@Column(name = "CREATE_USER", length = 40)
	private String createUser;

	//id san pham
	//@ManyToOne(targetEntity = Product.class)
	//@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	//private Product product;

	//so luong ke hoach
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;
	
	//doanh so ke hoach
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//id cua bang ke hoach
	@ManyToOne(targetEntity = SalePlan.class)
	@JoinColumn(name = "SALE_PLAN_ID", referencedColumnName = "SALE_PLAN_ID")
	private SalePlan salePlan;

	//id ban
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SALE_PLAN_VERSION_ID")
	private Long id;

	//thang
//	@Basic
//	@Column(name = "MONTH_DATE", columnDefinition = "timestamp(9) default systimestamp")
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date monthDate;
	
	//version cua ke hoach
	@Basic
	@Column(name = "VERSION", length = 22)
	private Integer version;
	
	//id chu ky
	@ManyToOne(targetEntity = Cycle.class)
	@JoinColumn(name = "CYCLE_ID", referencedColumnName = "CYCLE_ID")
	private Cycle cycle;

//	public Date getMonthDate() {
//		return monthDate;
//	}
//
//	public void setMonthDate(Date monthDate) {
//		this.monthDate = monthDate;
//	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	/*public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}*/

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public SalePlan getSalePlan() {
		return salePlan;
	}

	public void setSalePlan(SalePlan salePlan) {
		this.salePlan = salePlan;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Cycle getCycle() {
		return cycle;
	}

	public void setCycle(Cycle cycle) {
		this.cycle = cycle;
	}
	
}
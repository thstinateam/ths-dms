package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.OrderType;


@Entity
@Table(name = "DEBIT_DETAIL_TEMP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "DEBIT_DETAIL_TEMP_SEQ", allocationSize = 1)
public class DebitDetailTemp implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	//So tien no
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount = new BigDecimal(0) ;

	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

//	//ID khach hang (truong hop cong no KH)
//	@ManyToOne(targetEntity = Customer.class)
//	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
//	private Customer customer;

	//ID no
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "DEBIT_DETAIL_TEMP_ID")
	private Long id;

	/** Chiet khau don hang */
	@Basic
	@Column(name = "DISCOUNT", length = 22)
	private BigDecimal discount = new BigDecimal(0) ;

	//id don hang hoac id cua po confirm
	@Basic
	@Column(name = "FROM_OBJECT_ID", length = 22)
	private Long fromObjectId;

	//tong chua tra
	@Basic
	@Column(name = "REMAIN", length = 22)
	private BigDecimal remain = new BigDecimal(0) ;

//	//ID NPP
//	@ManyToOne(targetEntity = Shop.class)
//	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
//	private Shop shop;
	
	//ID NPP
	@ManyToOne(targetEntity = Debit.class)
	@JoinColumn(name = "DEBIT_ID", referencedColumnName = "DEBIT_ID")
	private Debit debit;

	//So tien phai tra sau tru chiet khau
	@Basic
	@Column(name = "TOTAL", length = 22)
	private BigDecimal total = new BigDecimal(0) ;

	//tong da tra
	@Basic
	@Column(name = "TOTAL_PAY", length = 22)
	private BigDecimal totalPay = new BigDecimal(0) ;

//	//loai: 0 cong no KH,1 cong no NPP
//	@Column(name = "TYPE", columnDefinition = "integer", nullable = true)
//	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
//			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.DebtType"),
//			@Parameter(name = "identifierMethod", value = "getValue"),
//			@Parameter(name = "valueOfMethod", value = "parseValue") })
//	private DebtType type = DebtType.CUSTOMER ;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Basic
	@Column(name = "DEBIT_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date debitDate;
	
	@Basic
	@Column(name = "type", columnDefinition = "integer")
	private Integer type;
	
	@Basic
	@Column(name = "FROM_OBJECT_NUMBER", length = 40)
	private String fromObjectNumber;

	@Basic
	@Column(name = "INVOICE_NUMBER", length = 40)
	private String invoiceNumber;
	
	/** Chiet khau khi thanh toan */
	@Basic
	@Column(name = "DISCOUNT_AMOUNT", length = 22)
	private BigDecimal discountAmount = new BigDecimal(0) ;
	
	@Basic
	@Column(name = "REASON", length = 250)
	private String reason;
	
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;
	
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	//Loai don hang: IN: pre->kh; CM: KH->Pre;SO Van->KH;CO  KH->Van
	@Basic
	@Column(name = "ORDER_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.OrderType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private OrderType orderType = OrderType.IN;
	
	//Ngay lap don hang
	@Basic
	@Column(name = "ORDER_DATE", length = 7)
	private Date orderDate;
	
	//Ngay lap don hang
	@Basic
	@Column(name = "RETURN_DATE", length = 7)
	private Date returnDate;
	
	public Date getDebitDate() {
		return debitDate;
	}

	public void setDebitDate(Date debitDate) {
		this.debitDate = debitDate;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

//	public Customer getCustomer() {
//		return customer;
//	}
//
//	public void setCustomer(Customer customer) {
//		this.customer = customer;
//	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/** Chiet khau don hang */
	public BigDecimal getDiscount() {
		return discount == null ? BigDecimal.valueOf(0) : discount;
	}

	/** Chiet khau don hang */
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public Long getFromObjectId() {
		return fromObjectId;
	}

	public void setFromObjectId(Long fromObjectId) {
		this.fromObjectId = fromObjectId;
	}

	public BigDecimal getRemain() {
		return remain == null ? BigDecimal.valueOf(0) : remain;
	}

	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}

//	public Shop getShop() {
//		return shop;
//	}
//
//	public void setShop(Shop shop) {
//		this.shop = shop;
//	}

	public BigDecimal getTotal() {
		return total == null ? BigDecimal.valueOf(0) : total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getTotalPay() {
		return totalPay == null ? BigDecimal.valueOf(0) : totalPay;
	}

	public void setTotalPay(BigDecimal totalPay) {
		this.totalPay = totalPay;
	}

//	public DebtType getType() {
//		return type;
//	}
//
//	public void setType(DebtType type) {
//		this.type = type;
//	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Debit getDebit() {
		return debit;
	}

	public void setDebit(Debit debit) {
		this.debit = debit;
	}

	public String getFromObjectNumber() {
		return fromObjectNumber;
	}

	public void setFromObjectNumber(String fromObjectNumber) {
		this.fromObjectNumber = fromObjectNumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/** Chiet khau khi thanh toan */
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	/** Chiet khau khi thanh toan */
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
}
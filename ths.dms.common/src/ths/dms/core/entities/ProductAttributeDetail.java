package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Chi tiet thuoc tinh san pham
 * @author tientv11
 * @since 15/01/2015
 * 
 */

@Entity
@Table(name = "PRODUCT_ATTRIBUTE_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PRODUCT_ATTRIBUTE_DETAIL_SEQ", allocationSize = 1)
public class ProductAttributeDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PRODUCT_ATTRIBUTE_DETAIL_ID")
	private Long id;


	@ManyToOne(targetEntity = ProductAttribute.class)
	@JoinColumn(name = "PRODUCT_ATTRIBUTE_ID", referencedColumnName = "PRODUCT_ATTRIBUTE_ID")
	private ProductAttribute productAttribute;
	
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;
	
	
	@ManyToOne(targetEntity = ProductAttributeEnum.class)
	@JoinColumn(name = "PRODUCT_ATTRIBUTE_ENUM_ID", referencedColumnName = "PRODUCT_ATTRIBUTE_ENUM_ID")
	private ProductAttributeEnum productAttributeEnum;
	
	
	@Basic
	@Column(name = "VALUE", length = 2000)
	private String value;

	

	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductAttribute getProductAttribute() {
		return productAttribute;
	}

	public void setProductAttribute(ProductAttribute productAttribute) {
		this.productAttribute = productAttribute;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public ProductAttributeEnum getProductAttributeEnum() {
		return productAttributeEnum;
	}

	public void setProductAttributeEnum(ProductAttributeEnum productAttributeEnum) {
		this.productAttributeEnum = productAttributeEnum;
	}
	
	
}
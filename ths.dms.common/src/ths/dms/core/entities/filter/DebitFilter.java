/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.DebitPaymentType;
import ths.dms.core.entities.enumtype.KPaging;

/**
 * Doi tuong chua dieu kien loc debit, debit_detail
 * 
 * @author lacnv1
 * @since Apr 08, 2015
 */
public class DebitFilter<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private KPaging<T> paging;
	
	private List<Long> lstDebitDetailId;
	private List<Long> lstSaleOrderId;
	private List<Integer> statusPayment;
	
	private Long shopId;
	private String shortCode;
	private String orderNumber;
	private BigDecimal amount;
	private BigDecimal fromAmount;
	private BigDecimal toAmount;
	private Boolean isReveived;
	private Date fromDate;
	private Date toDate;
	private Date lockDate;
	private Long deliveryId;
	private Long cashierId;
	private Long staffId;
	private String lstOrderNumbers;
	
	private String userName;
	private Staff cashierStaff;
	
	private DebitPaymentType debitPaymentType;
	
	public KPaging<T> getPaging() {
		return paging;
	}

	public void setPaging(KPaging<T> paging) {
		this.paging = paging;
	}

	public Long getShopId() {
		return shopId;
	}
	
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFromAmount() {
		return fromAmount;
	}

	public void setFromAmount(BigDecimal fromAmount) {
		this.fromAmount = fromAmount;
	}

	public BigDecimal getToAmount() {
		return toAmount;
	}

	public void setToAmount(BigDecimal toAmount) {
		this.toAmount = toAmount;
	}

	public Boolean getIsReveived() {
		return isReveived;
	}

	public void setIsReveived(Boolean isReveived) {
		this.isReveived = isReveived;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Long getCashierId() {
		return cashierId;
	}

	public void setCashierId(Long cashierId) {
		this.cashierId = cashierId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getLstOrderNumbers() {
		return lstOrderNumbers;
	}

	public void setLstOrderNumbers(String lstOrderNumbers) {
		this.lstOrderNumbers = lstOrderNumbers;
	}

	public List<Long> getLstDebitDetailId() {
		return lstDebitDetailId;
	}

	public void setLstDebitDetailId(List<Long> lstDebitDetailId) {
		this.lstDebitDetailId = lstDebitDetailId;
	}

	public Staff getCashierStaff() {
		return cashierStaff;
	}

	public void setCashierStaff(Staff cashierStaff) {
		this.cashierStaff = cashierStaff;
	}

	public List<Long> getLstSaleOrderId() {
		return lstSaleOrderId;
	}

	public void setLstSaleOrderId(List<Long> lstSaleOrderId) {
		this.lstSaleOrderId = lstSaleOrderId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public DebitPaymentType getDebitPaymentType() {
		return debitPaymentType;
	}

	public void setDebitPaymentType(DebitPaymentType debitPaymentType) {
		this.debitPaymentType = debitPaymentType;
	}

	public List<Integer> getStatusPayment() {
		return statusPayment;
	}

	public void setStatusPayment(List<Integer> statusPayment) {
		this.statusPayment = statusPayment;
	}

	public Date getLockDate() {
		return lockDate;
	}

	public void setLockDate(Date lockDate) {
		this.lockDate = lockDate;
	}
}
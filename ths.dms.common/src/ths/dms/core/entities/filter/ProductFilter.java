package ths.dms.core.entities.filter;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ProductVOEx;

public class ProductFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3343613596415261907L;
	
	private String namePro;
	private String codePro;
	private String catNamePro;
	private KPaging<ProductVOEx> kPaging;
	public String getNamePro() {
		return namePro;
	}
	public void setNamePro(String namePro) {
		this.namePro = namePro;
	}
	public String getCodePro() {
		return codePro;
	}
	public void setCodePro(String codePro) {
		this.codePro = codePro;
	}
	public String getCatNamePro() {
		return catNamePro;
	}
	public void setCatNamePro(String catNamePro) {
		this.catNamePro = catNamePro;
	}
	public KPaging<ProductVOEx> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<ProductVOEx> kPaging) {
		this.kPaging = kPaging;
	}
	
	public ProductFilter(){
		
	}
}

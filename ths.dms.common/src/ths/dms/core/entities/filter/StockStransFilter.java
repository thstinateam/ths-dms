package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransVO;

/**
 * Doi tuong loc StockStrans
 * @author phuongvm
 */
public class StockStransFilter implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private KPaging<StockTrans> kPaging;
	private KPaging<StockTransVO> kPagingStockTransVO;
	private KPaging<StockTotalVO> kPagingStockTotal;
	private Long stockTransId;
	private String stockTranCode;
	private String type;
	private String categoryDataCode; //ma kho
	private Long shopId;
	private Date startTemp;	
	private Date endTemp;
	private Long fromOwnerId;
	private Long toOwnerID;
	private Long fromOwnerType; //ma kho xuat
	private Long toOwnerType; // ma kho nhap
	private List<Long> lstFromOwnerId; //lst id cua hang xuat
	private List<Long> lstToOwnerId; //lst cua hang nhap
	
	private String productCode;
	private String productName;
	private Long category;
	private Long sub_category;
	private Integer fromAmnt;
	private Integer toAmnt;
	private Date ngayHoaDon;
	private Integer typeImport;
	private Integer warehouseId;
	private String invoiceCode;
	private Integer orderId;
	private Integer status;
	
	private List<String> lstType;
	
	private SaleOrderStatus approved;
	private SaleOrderStep approvedStep;
	
	private List<Long> lstShopId;
	
	public SaleOrderStatus getApproved() {
		return approved;
	}
	public void setApproved(SaleOrderStatus approved) {
		this.approved = approved;
	}
	public SaleOrderStep getApprovedStep() {
		return approvedStep;
	}
	public void setApprovedStep(SaleOrderStep approvedStep) {
		this.approvedStep = approvedStep;
	}
	public List<String> getLstType() {
		return lstType;
	}
	public void setLstType(List<String> lstType) {
		this.lstType = lstType;
	}

	public String getStockTranCode() {
		return stockTranCode;
	}
	public void setStockTranCode(String stockTranCode) {
		this.stockTranCode = stockTranCode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCategoryDataCode() {
		return categoryDataCode;
	}
	public void setCategoryDataCode(String categoryDataCode) {
		this.categoryDataCode = categoryDataCode;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getFromOwnerId() {
		return fromOwnerId;
	}
	public void setFromOwnerId(Long fromOwnerId) {
		this.fromOwnerId = fromOwnerId;
	}
	public Long getToOwnerID() {
		return toOwnerID;
	}
	public void setToOwnerID(Long toOwnerID) {
		this.toOwnerID = toOwnerID;
	}
	public Long getFromOwnerType() {
		return fromOwnerType;
	}
	public void setFromOwnerType(Long fromOwnerType) {
		this.fromOwnerType = fromOwnerType;
	}
	public Long getToOwnerType() {
		return toOwnerType;
	}
	public void setToOwnerType(Long toOwnerType) {
		this.toOwnerType = toOwnerType;
	}
	public List<Long> getLstFromOwnerId() {
		return lstFromOwnerId;
	}
	public void setLstFromOwnerId(List<Long> lstFromOwnerId) {
		this.lstFromOwnerId = lstFromOwnerId;
	}
	public List<Long> getLstToOwnerId() {
		return lstToOwnerId;
	}
	public void setLstToOwnerId(List<Long> lstToOwnerId) {
		this.lstToOwnerId = lstToOwnerId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Long getCategory() {
		return category;
	}
	public void setCategory(Long category) {
		this.category = category;
	}
	public Long getSub_category() {
		return sub_category;
	}
	public void setSub_category(Long sub_category) {
		this.sub_category = sub_category;
	}
	public Integer getFromAmnt() {
		return fromAmnt;
	}
	public void setFromAmnt(Integer fromAmnt) {
		this.fromAmnt = fromAmnt;
	}
	public Integer getToAmnt() {
		return toAmnt;
	}
	public void setToAmnt(Integer toAmnt) {
		this.toAmnt = toAmnt;
	}
	public Date getNgayHoaDon() {
		return ngayHoaDon;
	}
	public void setNgayHoaDon(Date ngayHoaDon) {
		this.ngayHoaDon = ngayHoaDon;
	}
	public Integer getTypeImport() {
		return typeImport;
	}
	public void setTypeImport(Integer typeImport) {
		this.typeImport = typeImport;
	}
	public Integer getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Integer warehouseId) {
		this.warehouseId = warehouseId;
	}
	public String getInvoiceCode() {
		return invoiceCode;
	}
	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public KPaging<StockTrans> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<StockTrans> kPaging) {
		this.kPaging = kPaging;
	}
	public KPaging<StockTransVO> getkPagingStockTransVO() {
		return kPagingStockTransVO;
	}
	public void setkPagingStockTransVO(KPaging<StockTransVO> kPagingStockTransVO) {
		this.kPagingStockTransVO = kPagingStockTransVO;
	}
	public Date getStartTemp() {
		return startTemp;
	}
	public void setStartTemp(Date startTemp) {
		this.startTemp = startTemp;
	}
	public Date getEndTemp() {
		return endTemp;
	}
	public void setEndTemp(Date endTemp) {
		this.endTemp = endTemp;
	}
	public KPaging<StockTotalVO> getkPagingStockTotal() {
		return kPagingStockTotal;
	}
	public void setkPagingStockTotal(KPaging<StockTotalVO> kPagingStockTotal) {
		this.kPagingStockTotal = kPagingStockTotal;
	}
	public Long getStockTransId() {
		return stockTransId;
	}
	public void setStockTransId(Long stockTransId) {
		this.stockTransId = stockTransId;
	}
	
	public List<Long> getLstShopId() {
		return lstShopId;
	}
	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}

/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;

/**
 * Filter tim kiem du lieu cho chuc nang: Danh sach san pham cho thiet bi
 * 
 * @author tuannd20
 * @version 1.0
 * @date 05/01/2015
 */
public class EquipmentGroupProductFilter<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private KPaging<T> kPaging;
	
	private String equipmentGroupId;
	private String equipmentGroupCode;
	private String equipmentGroupName;
	private Integer status;
	private ActiveType equipmentGroupStatus;
	private List<String> productCodes;
	private List<String> equipGroupCodes;
	private List<Long> equipGroupProductIds;
	
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public String getEquipmentGroupId() {
		return equipmentGroupId;
	}
	public void setEquipmentGroupId(String equipmentGroupId) {
		this.equipmentGroupId = equipmentGroupId;
	}
	public String getEquipmentGroupCode() {
		return equipmentGroupCode;
	}
	public void setEquipmentGroupCode(String equipmentGroupCode) {
		this.equipmentGroupCode = equipmentGroupCode;
	}
	public String getEquipmentGroupName() {
		return equipmentGroupName;
	}
	public void setEquipmentGroupName(String equipmentGroupName) {
		this.equipmentGroupName = equipmentGroupName;
	}
	public ActiveType getEquipmentGroupStatus() {
		return equipmentGroupStatus;
	}
	public void setEquipmentGroupStatus(ActiveType equipmentGroupStatus) {
		this.equipmentGroupStatus = equipmentGroupStatus;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<String> getProductCodes() {
		return productCodes;
	}
	public void setProductCodes(List<String> productCodes) {
		this.productCodes = productCodes;
	}
	public List<String> getEquipGroupCodes() {
		return equipGroupCodes;
	}
	public void setEquipGroupCodes(List<String> equipGroupCodes) {
		this.equipGroupCodes = equipGroupCodes;
	}
	public List<Long> getEquipGroupProductIds() {
		return equipGroupProductIds;
	}
	public void setEquipGroupProductIds(List<Long> equipGroupProductIds) {
		this.equipGroupProductIds = equipGroupProductIds;
	}
	
}

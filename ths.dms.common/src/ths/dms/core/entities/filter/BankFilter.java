package ths.dms.core.entities.filter;

import java.io.Serializable;

import ths.dms.core.entities.Bank;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;

public class BankFilter implements Serializable {

	private static final long serialVersionUID = 1L;
	private KPaging<Bank> kPaging;
	private String bankCode;
	private String bankName;
	private String bankPhone;
	private String bankAddress;
	private ActiveType status;
	private Long shopId;
	private String shopCode;
	
	public KPaging<Bank> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<Bank> kPaging) {
		this.kPaging = kPaging;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankPhone() {
		return bankPhone;
	}
	public void setBankPhone(String bankPhone) {
		this.bankPhone = bankPhone;
	}
	public String getBankAddress() {
		return bankAddress;
	}
	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	
	
}

/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.PromotionStaffVO;

public class PromotionStaffFilter implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<PromotionStaffVO> kPaging;
	private Long promotionId;
	private Long shopId;
	private String strListShopId;
	private String code;
	private String name;
	private Integer shopType;
	
	public KPaging<PromotionStaffVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<PromotionStaffVO> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}
	public String getStrListShopId() {
		return strListShopId;
	}
	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Integer getShopType() {
		return shopType;
	}
	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}
	private String address;
}

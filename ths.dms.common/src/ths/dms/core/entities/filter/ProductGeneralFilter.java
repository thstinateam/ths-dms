package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;

/**
 * Khai bao cac tham so cho lop lien quan den san pham
 * 
 * @author hunglm16
 * @since May 20,2015
 */
public class ProductGeneralFilter<T> implements Serializable {

	private static final long serialVersionUID = -1885861986995116462L;

	private KPaging<T> kPaging;

	private Long productId;
	private Long catId;
	private Long shopId;
	private Long customerTypeId;
	private Integer stockTypeId;

	private String productCode;
	private String productName;
	private Integer status;
	private Integer productInforStatus;

	private List<String> productCodes;
	private List<Long> productIds;
	private List<Long> productInforIds;
	private List<Integer> arrStatus;
	private List<Integer> productInforTypes;

	/**
	 * Khai bao GETTER/SETTER
	 * */

	public KPaging<T> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getCatId() {
		return catId;
	}

	public void setCatId(Long catId) {
		this.catId = catId;
	}

	public List<String> getProductCodes() {
		return productCodes;
	}

	public void setProductCodes(List<String> productCodes) {
		this.productCodes = productCodes;
	}

	public List<Long> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<Long> productIds) {
		this.productIds = productIds;
	}

	public List<Long> getProductInforIds() {
		return productInforIds;
	}

	public void setProductInforIds(List<Long> productInforIds) {
		this.productInforIds = productInforIds;
	}

	public List<Integer> getArrStatus() {
		return arrStatus;
	}

	public void setArrStatus(List<Integer> arrStatus) {
		this.arrStatus = arrStatus;
	}

	public Integer getProductInforStatus() {
		return productInforStatus;
	}

	public void setProductInforStatus(Integer productInforStatus) {
		this.productInforStatus = productInforStatus;
	}

	public List<Integer> getProductInforTypes() {
		return productInforTypes;
	}

	public void setProductInforTypes(List<Integer> productInforTypes) {
		this.productInforTypes = productInforTypes;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(Long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public Integer getStockTypeId() {
		return stockTypeId;
	}

	public void setStockTypeId(Integer stockTypeId) {
		this.stockTypeId = stockTypeId;
	}
}

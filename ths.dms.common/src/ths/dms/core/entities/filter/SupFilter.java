package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffSpecificType;

/**
 * Lop Filter co ban can co de lam tham so
 * 
 * @author hunglm16
 * @return BasicFilter
 * @description Chi bo sung truong o muc dung chung cho tat ca cac Table, khong tao them truong khac biet, không tạo thêm Oject ngoài <T>
 */
public class SupFilter<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private KPaging<T> kPaging;
	private T attribute;
	
	private List<Long> arrlongG;
	private List<Integer> arrIntG;
	
	private Long staffRootId;
	private Long roleId;
	private Long shopRootId;
	private Long shopId;
	private Long staffId;
	private Boolean oneNode;
	private Integer roleType;
	private Date dateTime;
	private Long loginStaffId;
	private String shopCode;
	private String shopName;
	private Staff loginStaff;
	private Long routingId;
	private String strShopId;
	private String strStaffId;
	private Boolean childShop;
	
	private String strSpecType;//list specific_type in staff_type table
	private List<StaffSpecificType> lstSpecType;
	private String strStaffTypeId;
	
	public Staff getLoginStaff() {
		return loginStaff;
	}
	public void setLoginStaff(Staff loginStaff) {
		this.loginStaff = loginStaff;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public T getAttribute() {
		return attribute;
	}
	public void setAttribute(T attribute) {
		this.attribute = attribute;
	}
	public List<Long> getArrlongG() {
		return arrlongG;
	}
	public void setArrlongG(List<Long> arrlongG) {
		this.arrlongG = arrlongG;
	}
	public List<Integer> getArrIntG() {
		return arrIntG;
	}
	public void setArrIntG(List<Integer> arrIntG) {
		this.arrIntG = arrIntG;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Boolean getOneNode() {
		return oneNode;
	}
	public void setOneNode(Boolean oneNode) {
		this.oneNode = oneNode;
	}
	public Integer getRoleType() {
		return roleType;
	}
	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}
	public Long getLoginStaffId() {
		return loginStaffId;
	}
	public void setLoginStaffId(Long loginStaffId) {
		this.loginStaffId = loginStaffId;
	}
	public Long getRoutingId() {
		return routingId;
	}
	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}
	public String getStrShopId() {
		return strShopId;
	}
	public void setStrShopId(String strShopId) {
		this.strShopId = strShopId;
	}
	public String getStrStaffId() {
		return strStaffId;
	}
	public void setStrStaffId(String strStaffId) {
		this.strStaffId = strStaffId;
	}
	public String getStrSpecType() {
		return strSpecType;
	}
	public void setStrSpecType(String strSpecType) {
		this.strSpecType = strSpecType;
	}
	public Boolean getChildShop() {
		return childShop;
	}
	public void setChildShop(Boolean childShop) {
		this.childShop = childShop;
	}
	public List<StaffSpecificType> getLstSpecType() {
		return lstSpecType;
	}
	public void setLstSpecType(List<StaffSpecificType> lstSpecType) {
		this.lstSpecType = lstSpecType;
	}
	public String getStrStaffTypeId() {
		return strStaffTypeId;
	}
	public void setStrStaffTypeId(String strStaffTypeId) {
		this.strStaffTypeId = strStaffTypeId;
	}
	public Long getStaffRootId() {
		return staffRootId;
	}
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getShopRootId() {
		return shopRootId;
	}
	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}
	
}

package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ImageFilterVO;
import ths.dms.core.entities.vo.ImageVO;

public class ImageFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5034484038781819235L;
	private List<Long> lstDisplayPrograme;
	private Long idGSNpp;
	private List<Long> lstStaff;
	private Date fromDate;
	private Date toDate;
	private String codeCustomer;
	private String nameCustomer;
	private Long idCustomer;
	private Integer roleType;
	private Integer key;
	private String shortCode;
	private KPaging<ImageVO> kPaging;
	private Long shopId;
	private List<Long> lstShop;
	private Integer objectType;
	private List<Integer> lstObjectType;//SangTN, Images for MT
	private List<ImageFilterVO> lstImageFilterVO;
	private Boolean isGroupByStaff;
	private Long staffIdGroup;
	private Long customerIdGroup;
	private String level;
	private Boolean lastSeq;
	private List<Integer> monthSeq;
	private String shopIdListStr;
	
	public List<Long> getLstDisplayPrograme() {
		return lstDisplayPrograme;
	}
	public void setLstDisplayPrograme(List<Long> lstDisplayPrograme) {
		this.lstDisplayPrograme = lstDisplayPrograme;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getCodeCustomer() {
		return codeCustomer;
	}
	public void setCodeCustomer(String codeCustomer) {
		this.codeCustomer = codeCustomer;
	}
	public String getNameCustomer() {
		return nameCustomer;
	}
	public void setNameCustomer(String nameCustomer) {
		this.nameCustomer = nameCustomer;
	}
	public Long getIdCustomer() {
		return idCustomer;
	}
	public void setIdCustomer(Long idCustomer) {
		this.idCustomer = idCustomer;
	}
	public KPaging<ImageVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<ImageVO> kPaging) {
		this.kPaging = kPaging;
	}
	public List<Long> getLstStaff() {
		return lstStaff;
	}
	public void setLstStaff(List<Long> lstStaff) {
		this.lstStaff = lstStaff;
	}
	public Integer getRoleType() {
		return roleType;
	}
	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}
	public Integer getKey() {
		return key;
	}
	public void setKey(Integer key) {
		this.key = key;
	}
	public Long getIdGSNpp() {
		return idGSNpp;
	}
	public void setIdGSNpp(Long idGSNpp) {
		this.idGSNpp = idGSNpp;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public List<ImageFilterVO> getLstImageFilterVO() {
		return lstImageFilterVO;
	}
	public void setLstImageFilterVO(List<ImageFilterVO> lstImageFilterVO) {
		this.lstImageFilterVO = lstImageFilterVO;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public List<Long> getLstShop() {
		return lstShop;
	}
	public void setLstShop(List<Long> lstShop) {
		this.lstShop = lstShop;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}	
	public List<Integer> getLstObjectType() {
		return lstObjectType;
	}
	public void setLstObjectType(List<Integer> lstObjectType) {
		this.lstObjectType = lstObjectType;
	}
	public Boolean getIsGroupByStaff() {
		return isGroupByStaff;
	}
	public void setIsGroupByStaff(Boolean isGroupByStaff) {
		this.isGroupByStaff = isGroupByStaff;
	}
	public Long getStaffIdGroup() {
		return staffIdGroup;
	}
	public void setStaffIdGroup(Long staffIdGroup) {
		this.staffIdGroup = staffIdGroup;
	}
	public Long getCustomerIdGroup() {
		return customerIdGroup;
	}
	public void setCustomerIdGroup(Long customerIdGroup) {
		this.customerIdGroup = customerIdGroup;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public Boolean getLastSeq() {
		return lastSeq;
	}
	public void setLastSeq(Boolean lastSeq) {
		this.lastSeq = lastSeq;
	}
	public List<Integer> getMonthSeq() {
		return monthSeq;
	}
	public void setMonthSeq(List<Integer> monthSeq) {
		this.monthSeq = monthSeq;
	}
	public String getShopIdListStr() {
		return shopIdListStr;
	}
	public void setShopIdListStr(String shopIdListStr) {
		this.shopIdListStr = shopIdListStr;
	}
	
}

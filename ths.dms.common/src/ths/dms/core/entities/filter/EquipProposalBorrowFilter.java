package ths.dms.core.entities.filter;

/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.ShopVO;

/**
 * Filter chuc nang quan ly de nghi muon thiet bi
 * 
 * @author hoanv25
 * @since March 09,2015
 */
public class EquipProposalBorrowFilter<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	private KPaging<T> kPaging;
	private T attribute;

	private EquipPeriod equipPeriod;

	private List<Long> lstId;
	private List<Long> lstShopId;
	private List<Long> lstEquipAttachFileId;
	private List<Long> lstLong;
	private List<Long> lstInsertId;
	private List<Long> lstStockInsertId;
	private List<Long> lstEquipId;
	private List<Long> lstObjectId;
	private List<Long> lstAddStockId;
	private List<Long> lstIdStock;
	private List<Long> lstAddStockIdInsert;
	private List<Integer> lstStatus;
	private List<Long> lstCustomerId;
	private List<String> lstCustomerCode;
	private List<FileVO> lstFileVo;
	private Map<Long, Long> mapLongLong = new HashMap<Long, Long>();

	private ShopVO shopVO;

	private Long id;
	private Long equipId;
	private Long shopId;
	private Long stockId;
	private Long objectId;
	private Long customerId;
	private Long shopRootId;
	private Long staffRootId;
	private Long roleId;

	private Integer status;
	private Integer recordStatus;
	private Integer deliveryStatus;
	private Integer statusGSNPP;
	private Integer statusTNKD;
	private Integer stockType;
	private Integer recordType;
	private Integer statusEquipRecord;
	private Integer isLevel;
	private Integer objectType;
	private Integer isUnder;

	private String code;
	private String name;
	private String serial;
	private String equipCode;
	private String superviseCode;
	private String address;
	private String shopCode;
	private String shopName;
	private String stockCode;
	private String stockName;
	private String shortCode;
	private String customerName;
	private String shopCodeName;
	private String userName;

	private Date fromDate;
	private Date toDate;

	/**
	 * Khai bao phuong thuc GETTER/SETTER
	 * */
	public Long getId() {
		return id;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public List<Long> getLstEquipAttachFileId() {
		return lstEquipAttachFileId;
	}

	public void setLstEquipAttachFileId(List<Long> lstEquipAttachFileId) {
		this.lstEquipAttachFileId = lstEquipAttachFileId;
	}

	public List<FileVO> getLstFileVo() {
		return lstFileVo;
	}

	public void setLstFileVo(List<FileVO> lstFileVo) {
		this.lstFileVo = lstFileVo;
	}

	public Integer getIsLevel() {
		return isLevel;
	}

	public void setIsLevel(Integer isLevel) {
		this.isLevel = isLevel;
	}

	public EquipPeriod getEquipPeriod() {
		return equipPeriod;
	}

	public void setEquipPeriod(EquipPeriod equipPeriod) {
		this.equipPeriod = equipPeriod;
	}
	public ShopVO getShopVO() {
		return shopVO;
	}

	public void setShopVO(ShopVO shopVO) {
		this.shopVO = shopVO;
	}

	public Map<Long, Long> getMapLongLong() {
		return mapLongLong;
	}

	public void setMapLongLong(Map<Long, Long> mapLongLong) {
		this.mapLongLong = mapLongLong;
	}

	public Long getShopRootId() {
		return shopRootId;
	}

	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}

	public List<Long> getLstLong() {
		return lstLong;
	}

	public void setLstLong(List<Long> lstLong) {
		this.lstLong = lstLong;
	}

	public Integer getStatusEquipRecord() {
		return statusEquipRecord;
	}

	public void setStatusEquipRecord(Integer statusEquipRecord) {
		this.statusEquipRecord = statusEquipRecord;
	}

	public Integer getRecordType() {
		return recordType;
	}

	public void setRecordType(Integer recordType) {
		this.recordType = recordType;
	}

	public Integer getStockType() {
		return stockType;
	}

	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public T getAttribute() {
		return attribute;
	}

	public void setAttribute(T attribute) {
		this.attribute = attribute;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public Integer getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(Integer deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public List<Long> getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}

	public KPaging<T> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Integer getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopCodeName() {
		return shopCodeName;
	}

	public void setShopCodeName(String shopCodeName) {
		this.shopCodeName = shopCodeName;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSuperviseCode() {
		return superviseCode;
	}

	public void setSuperviseCode(String superviseCode) {
		this.superviseCode = superviseCode;
	}

	public Integer getStatusGSNPP() {
		return statusGSNPP;
	}

	public Integer getStatusTNKD() {
		return statusTNKD;
	}

	public void setStatusGSNPP(Integer statusGSNPP) {
		this.statusGSNPP = statusGSNPP;
	}

	public void setStatusTNKD(Integer statusTNKD) {
		this.statusTNKD = statusTNKD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public List<Long> getLstEquipId() {
		return lstEquipId;
	}

	public void setLstEquipId(List<Long> lstEquipId) {
		this.lstEquipId = lstEquipId;
	}

	public List<Long> getLstObjectId() {
		return lstObjectId;
	}

	public void setLstObjectId(List<Long> lstObjectId) {
		this.lstObjectId = lstObjectId;
	}

	public Integer getIsUnder() {
		return isUnder;
	}

	public void setIsUnder(Integer isUnder) {
		this.isUnder = isUnder;
	}

	public List<Long> getLstInsertId() {
		return lstInsertId;
	}

	public List<Long> getLstStockInsertId() {
		return lstStockInsertId;
	}

	public void setLstInsertId(List<Long> lstInsertId) {
		this.lstInsertId = lstInsertId;
	}

	public void setLstStockInsertId(List<Long> lstStockInsertId) {
		this.lstStockInsertId = lstStockInsertId;
	}

	public List<Long> getLstAddStockId() {
		return lstAddStockId;
	}

	public void setLstAddStockId(List<Long> lstAddStockId) {
		this.lstAddStockId = lstAddStockId;
	}

	public List<Long> getLstAddStockIdInsert() {
		return lstAddStockIdInsert;
	}

	public void setLstAddStockIdInsert(List<Long> lstAddStockIdInsert) {
		this.lstAddStockIdInsert = lstAddStockIdInsert;
	}

	public List<Long> getLstIdStock() {
		return lstIdStock;
	}

	public void setLstIdStock(List<Long> lstIdStock) {
		this.lstIdStock = lstIdStock;
	}

	public List<Integer> getLstStatus() {
		return lstStatus;
	}

	public void setLstStatus(List<Integer> lstStatus) {
		this.lstStatus = lstStatus;
	}

	public List<String> getLstCustomerCode() {
		return lstCustomerCode;
	}

	public void setLstCustomerCode(List<String> lstCustomerCode) {
		this.lstCustomerCode = lstCustomerCode;
	}

	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}

	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}

	/** @return the staffRootId */
	public Long getStaffRootId() {
		return staffRootId;
	}

	/** @param staffRootId the staffRootId to set */
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}

	/** @return the roleId */
	public Long getRoleId() {
		return roleId;
	}

	/** @param roleId the roleId to set */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.vo.LogInfoVO;

/**
 * Confirm filter
 * @author trietptm
 * @since 23/10/2015 
 */
public class ConfirmFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private LogInfoVO logInfo;

	private List<Long> listId;
	private List<Long> listSaleOrderId;
	private List<Long> lstRemovedInvoice;

	private Boolean isRemovingInvoice;
	private Boolean isUpdateStock = false;
	private Boolean isReturnKeyShop;

	private Long saleOrderId;
	private Long shopId;

	private Integer defaultNumDayOrderExpire;

	private String configSysCalDate;

	private SaleOrder saleOrder;

	private Date LockDate;
	private Date currentDate;

	public LogInfoVO getLogInfo() {
		return logInfo;
	}

	public void setLogInfo(LogInfoVO logInfo) {
		this.logInfo = logInfo;
	}

	public List<Long> getListSaleOrderId() {
		return listSaleOrderId;
	}

	public void setListSaleOrderId(List<Long> listSaleOrderId) {
		this.listSaleOrderId = listSaleOrderId;
	}

	public List<Long> getLstRemovedInvoice() {
		return lstRemovedInvoice;
	}

	public void setLstRemovedInvoice(List<Long> lstRemovedInvoice) {
		this.lstRemovedInvoice = lstRemovedInvoice;
	}

	public Boolean getIsUpdateStock() {
		return isUpdateStock;
	}

	public void setIsUpdateStock(Boolean isUpdateStock) {
		this.isUpdateStock = isUpdateStock;
	}

	public Long getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Date getLockDate() {
		return LockDate;
	}

	public void setLockDate(Date lockDate) {
		LockDate = lockDate;
	}

	public Boolean getIsRemovingInvoice() {
		return isRemovingInvoice;
	}

	public void setIsRemovingInvoice(Boolean isRemovingInvoice) {
		this.isRemovingInvoice = isRemovingInvoice;
	}

	public Boolean getIsReturnKeyShop() {
		return isReturnKeyShop;
	}

	public void setIsReturnKeyShop(Boolean isReturnKeyShop) {
		this.isReturnKeyShop = isReturnKeyShop;
	}

	public Integer getDefaultNumDayOrderExpire() {
		return defaultNumDayOrderExpire;
	}

	public void setDefaultNumDayOrderExpire(Integer defaultNumDayOrderExpire) {
		this.defaultNumDayOrderExpire = defaultNumDayOrderExpire;
	}

	public String getConfigSysCalDate() {
		return configSysCalDate;
	}

	public void setConfigSysCalDate(String configSysCalDate) {
		this.configSysCalDate = configSysCalDate;
	}

	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public List<Long> getListId() {
		return listId;
	}

	public void setListId(List<Long> listId) {
		this.listId = listId;
	}

}

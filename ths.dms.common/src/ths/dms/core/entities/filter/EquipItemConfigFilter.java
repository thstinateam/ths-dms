package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.math.BigDecimal;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.EquipItemConfigVO;

/**
 * khai báo EquipItemConfigFilter 
 * @author liemtpt
 * @since 07/04/2015
 */
public class EquipItemConfigFilter implements Serializable{

	
	private static final long serialVersionUID = 1L;
	private KPaging<EquipItemConfigVO> kPaging;
	private Integer fromCapacity;
	private Integer toCapacity;
	private Integer status;
	
	public KPaging<EquipItemConfigVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<EquipItemConfigVO> kPaging) {
		this.kPaging = kPaging;
	}
	public Integer getFromCapacity() {
		return fromCapacity;
	}
	public void setFromCapacity(Integer fromCapacity) {
		this.fromCapacity = fromCapacity;
	}
	public Integer getToCapacity() {
		return toCapacity;
	}
	public void setToCapacity(Integer toCapacity) {
		this.toCapacity = toCapacity;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}

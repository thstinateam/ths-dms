/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.filter;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.CarSOVO;


/**
 * 
 * @author lacnv1
 * @since Sep 22, 2014
 */
public class CarSOFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private KPaging<CarSOVO> paging;
	private Long shopId;
	private String type;
	private String carNumber;
	private Long labelId;
	private Float gross;
	private String category;
	private String origin;
	private ActiveType status;
	private Boolean inChildShop;
	
	public KPaging<CarSOVO> getPaging() {
		return paging;
	}
	
	public void setkPaging(KPaging<CarSOVO> paging) {
		this.paging = paging;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public Long getLabelId() {
		return labelId;
	}

	public void setLabelId(Long labelId) {
		this.labelId = labelId;
	}

	public Float getGross() {
		return gross;
	}

	public void setGross(Float gross) {
		this.gross = gross;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Boolean getInChildShop() {
		return inChildShop;
	}

	public void setInChildShop(Boolean inChildShop) {
		this.inChildShop = inChildShop;
	}
}
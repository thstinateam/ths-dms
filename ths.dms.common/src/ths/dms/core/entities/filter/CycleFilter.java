/**
 * 
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;



public class CycleFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6974321976035585940L;

	private KPaging<Cycle> kPaging;
	private Long cycleId;
	private String cycleCode;
	private String cycleName;
	private Integer year;
	private Integer num;
	private Date beginDate;
	private Date endDate;
	private ActiveType status;
	private Integer lessYear;//tim kiem nho hon tinh theo nam
	private Integer moreYear;//tim kiem lon hon tinh theo nam
	
	public KPaging<Cycle> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<Cycle> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getCycleId() {
		return cycleId;
	}
	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}
	public String getCycleCode() {
		return cycleCode;
	}
	public void setCycleCode(String cycleCode) {
		this.cycleCode = cycleCode;
	}
	public String getCycleName() {
		return cycleName;
	}
	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Integer getLessYear() {
		return lessYear;
	}
	public void setLessYear(Integer lessYear) {
		this.lessYear = lessYear;
	}
	public Integer getMoreYear() {
		return moreYear;
	}
	public void setMoreYear(Integer moreYear) {
		this.moreYear = moreYear;
	}
}

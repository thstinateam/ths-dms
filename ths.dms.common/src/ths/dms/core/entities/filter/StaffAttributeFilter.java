/**
 * 
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;

/**
 * Bo loc StaffAttribute
 * @author vuongmq
 * @since 26/02/2015
 *
 */
public class StaffAttributeFilter implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1172786645448734206L;

	private Long id;
	
	private Long staffAttId;
	
	private Long staffId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Long getStaffAttId() {
		return staffAttId;
	}

	public void setStaffAttId(Long staffAttId) {
		this.staffAttId = staffAttId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	
	
	
}

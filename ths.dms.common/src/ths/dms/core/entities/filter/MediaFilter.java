package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.Media;
import ths.dms.core.entities.enumtype.KPaging;

public class MediaFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<Media> kPaging;
	private String mediaCode;
	private String mediaName;
	private Integer type;
	private Integer status;
	private Date createFromDate;
	private Date createToDate;	
	private Date updateFromDate;
	private Date updateToDate;
	
	public KPaging<Media> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<Media> kPaging) {
		this.kPaging = kPaging;
	}
	public String getMediaCode() {
		return mediaCode;
	}
	public void setMediaCode(String mediaCode) {
		this.mediaCode = mediaCode;
	}
	public String getMediaName() {
		return mediaName;
	}
	public void setMediaName(String mediaName) {
		this.mediaName = mediaName;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getCreateFromDate() {
		return createFromDate;
	}
	public void setCreateFromDate(Date createFromDate) {
		this.createFromDate = createFromDate;
	}
	public Date getCreateToDate() {
		return createToDate;
	}
	public void setCreateToDate(Date createToDate) {
		this.createToDate = createToDate;
	}
	public Date getUpdateFromDate() {
		return updateFromDate;
	}
	public void setUpdateFromDate(Date updateFromDate) {
		this.updateFromDate = updateFromDate;
	}
	public Date getUpdateToDate() {
		return updateToDate;
	}
	public void setUpdateToDate(Date updateToDate) {
		this.updateToDate = updateToDate;
	}	
}

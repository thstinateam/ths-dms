package ths.dms.core.entities.filter;

/**
 * Import thu vien
 * */
import java.io.Serializable;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.EquipmentPermissionVO;

/**
 * Filter permisson cho phan quyen nguoi dung - Thiet Bi
 * 
 * @author tamvnm 
 * @since March 24,2015
 */
public class EquipPermissionFilter implements Serializable {
	private static final long serialVersionUID = 1L;

	private KPaging<EquipmentPermissionVO> kPaging;
	private Long staffId;
	private String permissionCode;
	private String permissionName;
	private Integer permissionStatus;
	private Long roleId;
	private ActiveType status;
	public KPaging<EquipmentPermissionVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<EquipmentPermissionVO> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getPermissionCode() {
		return permissionCode;
	}
	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}
	public String getPermissionName() {
		return permissionName;
	}
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}
	public Integer getPermissionStatus() {
		return permissionStatus;
	}
	public void setPermissionStatus(Integer permissionStatus) {
		this.permissionStatus = permissionStatus;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	
	
}

/**
 * filter de lay danh sach sale_order_promotion
 * @author tuannd20
 * @version 1.0
 * @since 11/10/2014
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.List;

public class SaleOrderDetailVATFilter implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<Long> lstSOId;
	private Long productId;
	private Float vat;
	private Integer isFreeItem;
	
	public Integer getIsFreeItem() {
		return isFreeItem;
	}
	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}
	public List<Long> getLstSOId() {
		return lstSOId;
	}
	public void setLstSOId(List<Long> lstSOId) {
		this.lstSOId = lstSOId;
	}
	
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Float getVat() {
		return vat;
	}
	public void setVat(Float vat) {
		this.vat = vat;
	}
	
}

package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;

public class CustomerReportFilter implements Serializable {

	private static final long serialVersionUID = 1L;
	private String listShopCode;
	private Date fromDate;
	private Date toDate;
	private String country;
	private String province;
	public String getListShopCode() {
		return listShopCode;
	}
	public void setListShopCode(String listShopCode) {
		this.listShopCode = listShopCode;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	
}

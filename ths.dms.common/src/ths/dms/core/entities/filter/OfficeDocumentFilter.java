/**
 * 
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.enumtype.DocumentType;
import ths.dms.core.entities.enumtype.StaffObjectType;



public class OfficeDocumentFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private StaffObjectType roleType;
	
	private Long shopId;
	
	private DocumentType type;
	
	private String documentCode;
	
	private String documentName;
	
	
	private Date fromDate;
	
	
	private Date toDate;


	public StaffObjectType getRoleType() {
		return roleType;
	}


	public void setRoleType(StaffObjectType roleType) {
		this.roleType = roleType;
	}


	public Long getShopId() {
		return shopId;
	}


	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}


	

	public DocumentType getType() {
		return type;
	}


	public void setType(DocumentType type) {
		this.type = type;
	}


	public String getDocumentCode() {
		return documentCode;
	}


	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}


	public String getDocumentName() {
		return documentName;
	}


	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}


	public Date getFromDate() {
		return fromDate;
	}


	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


	public Date getToDate() {
		return toDate;
	}


	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	
	
	
}


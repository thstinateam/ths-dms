package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;

/**
 * Lop Filter co ban can co de lam tham so
 * 
 * @author hunglm16
 * @return PriceFilter
 * @description Chi bo sung truong o muc dung chung cho tat ca cac Table, khong tao them truong khac biet, không tạo thêm Oject ngoài <T>
 */
public class PriceFilter<T> implements Serializable {
	
	public static final Integer isLevelParent = 1;
	public static final Integer isLevelChid = 2;
	public static final Integer isLevelCom = 0;
	public static final Integer isLevelByChannel = 3;
	public static final Integer isLevelByShop = 4;
	public static final Integer isLevelClone = 5;
	public static final Integer isAttr = -2;
	
	private static final long serialVersionUID = 1L;
	private KPaging<T> kPaging;
	private T attribute;
	
	private List<Long> arrlongG;
	private List<Integer> arrIntG;
	
	private Boolean isArr;
	private Boolean isConnectBy;
	private Boolean isGetChildShop;
	
	private Long id;
	private Long shopId;
	private Long shopTypeId;
	private Long customerTypeId;
	private Long productId;
	private Long catId;

	private Integer shopChannelByShop;
	private Integer shopChannel;
	private Integer status;
	private Integer objectType;
	private Integer isLevel;
	
	private String productCode;
	private String productName;
	private String shopCode;
	private String shopName;
	private String customerCode;
	private String description;
	
	private Date fromDate;
	private Date toDate;
	private Date dateG;
	
	private Integer count;
	
	public Integer getShopChannelByShop() {
		return shopChannelByShop;
	}
	public void setShopChannelByShop(Integer shopChannelByShop) {
		this.shopChannelByShop = shopChannelByShop;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getIsLevel() {
		return isLevel;
	}
	public void setIsLevel(Integer isLevel) {
		this.isLevel = isLevel;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public T getAttribute() {
		return attribute;
	}
	public void setAttribute(T attribute) {
		this.attribute = attribute;
	}
	public List<Long> getArrlongG() {
		return arrlongG;
	}
	public void setArrlongG(List<Long> arrlongG) {
		this.arrlongG = arrlongG;
	}
	public List<Integer> getArrIntG() {
		return arrIntG;
	}
	public void setArrIntG(List<Integer> arrIntG) {
		this.arrIntG = arrIntG;
	}
	public Boolean getIsArr() {
		return isArr;
	}
	public void setIsArr(Boolean isArr) {
		this.isArr = isArr;
	}
	public Boolean getIsConnectBy() {
		return isConnectBy;
	}
	public void setIsConnectBy(Boolean isConnectBy) {
		this.isConnectBy = isConnectBy;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getShopTypeId() {
		return shopTypeId;
	}
	public void setShopTypeId(Long shopTypeId) {
		this.shopTypeId = shopTypeId;
	}
	public Long getCustomerTypeId() {
		return customerTypeId;
	}
	public void setCustomerTypeId(Long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}
	public Integer getShopChannel() {
		return shopChannel;
	}
	public void setShopChannel(Integer shopChannel) {
		this.shopChannel = shopChannel;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getDateG() {
		return dateG;
	}
	public void setDateG(Date dateG) {
		this.dateG = dateG;
	}
	public Long getCatId() {
		return catId;
	}
	public void setCatId(Long catId) {
		this.catId = catId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public Boolean getIsGetChildShop() {
		return isGetChildShop;
	}
	public void setIsGetChildShop(Boolean isGetChildShop) {
		this.isGetChildShop = isGetChildShop;
	}
}

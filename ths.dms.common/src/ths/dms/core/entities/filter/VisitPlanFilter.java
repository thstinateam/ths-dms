package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.VisitPlan;
import ths.dms.core.entities.enumtype.KPaging;

public class VisitPlanFilter implements Serializable {

	private static final long serialVersionUID = 1L;
	private KPaging<VisitPlan> kPaging;
	private Long staffId;
	private String staffCode;
	private Long shopId;
	private Long routingId;
	private Boolean flag;
	private String address;	
	private Date fromDate;
	private Date toDate;
	private String userName;
	private Integer status;
	private Long exceptRouteId;
	
	public Long getExceptRouteId() {
		return exceptRouteId;
	}
	public void setExceptRouteId(Long exceptRouteId) {
		this.exceptRouteId = exceptRouteId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public KPaging<VisitPlan> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<VisitPlan> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getRoutingId() {
		return routingId;
	}
	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}
	public Boolean getFlag() {
		return flag;
	}
	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}

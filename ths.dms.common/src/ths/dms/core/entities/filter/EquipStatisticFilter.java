package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.EquipStatisticRecordDetailVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;

public class EquipStatisticFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<StatisticCheckingVO> kPaging;
	private KPaging<EquipStatisticRecordDetailVO> kPagingESRD;
	private KPaging<ImageVO> kPagingImageVO;
	private Long recordId;
	private Long shopId;
	private Long productId;
	private Long stockId;
	private Long objectId;
	private Long equipGroupId;
	private Integer stepCheck;
	private Date fromDate;
	private Date toDate;
	private Date statisticDate;
	private String dateInWeek;
	private String equipCode;
	private String seri;
	private Long equipId;
	private String shopCode;
	private String customerCode;
	private String staffCode;
	private String staffName;
	private String customerNameAddress;
	private Integer status;
	private Integer notStatus;
	private Integer objectType;
	private String programCode;
	private String programName;

	public KPaging<StatisticCheckingVO> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<StatisticCheckingVO> kPaging) {
		this.kPaging = kPaging;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Integer getStepCheck() {
		return stepCheck;
	}

	public void setStepCheck(Integer stepCheck) {
		this.stepCheck = stepCheck;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getDateInWeek() {
		return dateInWeek;
	}

	public void setDateInWeek(String dateInWeek) {
		this.dateInWeek = dateInWeek;
	}

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getSeri() {
		return seri;
	}

	public void setSeri(String seri) {
		this.seri = seri;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerNameAddress() {
		return customerNameAddress;
	}

	public void setCustomerNameAddress(String customerNameAddress) {
		this.customerNameAddress = customerNameAddress;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getStatisticDate() {
		return statisticDate;
	}

	public void setStatisticDate(Date statisticDate) {
		this.statisticDate = statisticDate;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Integer getNotStatus() {
		return notStatus;
	}

	public void setNotStatus(Integer notStatus) {
		this.notStatus = notStatus;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public KPaging<EquipStatisticRecordDetailVO> getkPagingESRD() {
		return kPagingESRD;
	}

	public void setkPagingESRD(KPaging<EquipStatisticRecordDetailVO> kPagingESRD) {
		this.kPagingESRD = kPagingESRD;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public KPaging<ImageVO> getkPagingImageVO() {
		return kPagingImageVO;
	}

	public void setkPagingImageVO(KPaging<ImageVO> kPagingImageVO) {
		this.kPagingImageVO = kPagingImageVO;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

}

package ths.dms.core.entities.filter;
/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.EquipRoleDetail;
import ths.dms.core.entities.EquipSalePlan;

/**
 * Filter dung chung cho quan ly thiet bi
 * 
 * @author hoanv25
 * @since December 10,2014
 * 
 * @author nhutnn
 * @since 15/12/2014
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description ra soat va phan hoach
 */
public class EquipmentRoleDetailFilter<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Long> listmapIdStock;
	private List<Long> listmapIdEditStock;
	private List<Long> listCheckStock;
	private List<Long> listNoCheckStock;
	
	public List<Long> getListCheckStock() {
		return listCheckStock;
	}
	public List<Long> getListNoCheckStock() {
		return listNoCheckStock;
	}
	public void setListCheckStock(List<Long> listCheckStock) {
		this.listCheckStock = listCheckStock;
	}
	public void setListNoCheckStock(List<Long> listNoCheckStock) {
		this.listNoCheckStock = listNoCheckStock;
	}
	public List<Long> getListmapIdEditStock() {
		return listmapIdEditStock;
	}
	public void setListmapIdEditStock(List<Long> listmapIdEditStock) {
		this.listmapIdEditStock = listmapIdEditStock;
	}
	public List<Long> getListmapIdStock() {
		return listmapIdStock;
	}
	public void setListmapIdStock(List<Long> listmapIdStock) {
		this.listmapIdStock = listmapIdStock;
	}

}

package ths.dms.core.entities.filter;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.ProgramType;

public class SaleOrderPromoDetailFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5178634685605071531L;

	private Long saleOrderId;
	private Long saleOrderDetailId;
	private Long ShopId;
	private Long CustomerId;
	private ProgramType programType;
	private String ProgrameTypeCode;
	private Boolean isNotKeyShop;
	
	public Long getSaleOrderId() {
		return saleOrderId;
	}
	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}
	public Long getShopId() {
		return ShopId;
	}
	public void setShopId(Long shopId) {
		ShopId = shopId;
	}
	public ProgramType getProgramType() {
		return programType;
	}
	public void setProgramType(ProgramType programType) {
		this.programType = programType;
	}
	public String getProgrameTypeCode() {
		return ProgrameTypeCode;
	}
	public void setProgrameTypeCode(String programeTypeCode) {
		ProgrameTypeCode = programeTypeCode;
	}
	public Long getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(Long customerId) {
		CustomerId = customerId;
	}
	public Long getSaleOrderDetailId() {
		return saleOrderDetailId;
	}
	public void setSaleOrderDetailId(Long saleOrderDetailId) {
		this.saleOrderDetailId = saleOrderDetailId;
	}
	public Boolean getIsNotKeyShop() {
		return isNotKeyShop;
	}
	public void setIsNotKeyShop(Boolean isNotKeyShop) {
		this.isNotKeyShop = isNotKeyShop;
	}
	
	
}

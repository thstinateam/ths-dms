package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;

/**
 * Lop Filter co ban can co de lam tham so
 * 
 * @author hunglm16
 * @return BasicFilter
 * @description Chi bo sung truong o muc dung chung cho tat ca cac Table, khong tao them truong khac biet, không tạo thêm Object ngoài <T>
 */
public class PoVnmFilterBasic<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private KPaging<T> kPaging;
	private T attribute;
	
	private Long id;
	private Long shopId;
	private Integer type;
	private Integer status;
	private Long poVnmId; 
	private String poCoNumber; 
	private String invoiceNumber;
	private BigDecimal discount;
	private String userName;
	
	private Date fromDate;
	private Date toDate;
	private Date invoiceDate;
	private Date deliveryDate;
	
	private List<Long> lstId;
	private List<PoVnmDetailLotVO> lstPovnmDetail;
	/**
	 * Khai bao phuong thuc Getter/ Setter
	 * */
	
	public KPaging<T> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}

	public T getAttribute() {
		return attribute;
	}

	public void setAttribute(T attribute) {
		this.attribute = attribute;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getPoVnmId() {
		return poVnmId;
	}

	public void setPoVnmId(Long poVnmId) {
		this.poVnmId = poVnmId;
	}

	public String getPoCoNumber() {
		return poCoNumber;
	}

	public void setPoCoNumber(String poCoNumber) {
		this.poCoNumber = poCoNumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public List<PoVnmDetailLotVO> getLstPovnmDetail() {
		return lstPovnmDetail;
	}

	public void setLstPovnmDetail(List<PoVnmDetailLotVO> lstPovnmDetail) {
		this.lstPovnmDetail = lstPovnmDetail;
	}
	
}

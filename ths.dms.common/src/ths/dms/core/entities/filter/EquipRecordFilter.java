package ths.dms.core.entities.filter;

/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.ShopVO;

/**
 * Filter dung chung cho bien ban thiet bi
 * 
 * @author hunglm16
 * @since December 30,2014
 */
public class EquipRecordFilter<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	private KPaging<T> kPaging;
	private T attribute;

	private EquipPeriod equipPeriod;

	private List<Long> lstId;
	private List<Long> lstShopId;
	private List<Long> lstEquipAttachFileId;
	private List<Long> lstLong;
	private List<FileVO> lstFileVo;
	private Map<Long, Long> mapLongLong = new HashMap<Long, Long>();
	private Map<Long, Long> mapPriceActually = new HashMap<Long, Long>();

	private ShopVO shopVO;

	private Long staffRoot;
	private Long id;
	private Long equipId;
	private Long shopId;
	private Long stockId;
	private Long objectId;
	private Long customerId;
	private Long shopRootId;
	private Long eqLostRecId;
	private Long eqLostMobiRecId;

	private Integer recordStatus;
	private Integer deliveryStatus;
	private Integer stockType;
	private Integer recordType;
	private Integer statusEquipRecord;
	private Integer tracingPlace;
	private Integer tracingResult;
	private Integer flagMobile;
	private Integer isLevel;
	private Integer objectType;
	private Integer flagRecordStatus;
	private Integer status;

	private String code;
	private String name;
	private String serial;
	private String equipCode;
	private String address;
	private String shopCode;
	private String shopName;
	private String stockCode;
	private String shortCode;
	private String lostDateStr;
	private String customerName;
	private String customerSearch;
	private String shopCodeName;
	private String userName;
	private String lastArisingSalesDateStr;
	private String conclusion;
	private String recommendedTreatment;
	private String representor;
	private String strListShopId;

	private Date fromDate;
	private Date toDate;
	private Date changeDate;
	private Date lostDate;
	private Date lastArisingSalesDate;
	
	private String stockName;
	private String phone;
	private BigDecimal priceActually;
	/**
	 * Khai bao phuong thuc GETTER/SETTER
	 * */
	public Long getId() {
		return id;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public List<Long> getLstEquipAttachFileId() {
		return lstEquipAttachFileId;
	}

	public void setLstEquipAttachFileId(List<Long> lstEquipAttachFileId) {
		this.lstEquipAttachFileId = lstEquipAttachFileId;
	}

	public List<FileVO> getLstFileVo() {
		return lstFileVo;
	}

	public void setLstFileVo(List<FileVO> lstFileVo) {
		this.lstFileVo = lstFileVo;
	}

	public Long getEqLostRecId() {
		return eqLostRecId;
	}

	public void setEqLostRecId(Long eqLostRecId) {
		this.eqLostRecId = eqLostRecId;
	}

	public Integer getIsLevel() {
		return isLevel;
	}

	public void setIsLevel(Integer isLevel) {
		this.isLevel = isLevel;
	}

	public EquipPeriod getEquipPeriod() {
		return equipPeriod;
	}

	public void setEquipPeriod(EquipPeriod equipPeriod) {
		this.equipPeriod = equipPeriod;
	}

	public Integer getTracingResult() {
		return tracingResult;
	}

	public void setTracingResult(Integer tracingResult) {
		this.tracingResult = tracingResult;
	}

	public Long getEqLostMobiRecId() {
		return eqLostMobiRecId;
	}

	public void setEqLostMobiRecId(Long eqLostMobiRecId) {
		this.eqLostMobiRecId = eqLostMobiRecId;
	}

	public Integer getFlagMobile() {
		return flagMobile;
	}

	public void setFlagMobile(Integer flagMobile) {
		this.flagMobile = flagMobile;
	}

	public ShopVO getShopVO() {
		return shopVO;
	}

	public void setShopVO(ShopVO shopVO) {
		this.shopVO = shopVO;
	}

	public Long getStaffRoot() {
		return staffRoot;
	}

	public void setStaffRoot(Long staffRoot) {
		this.staffRoot = staffRoot;
	}

	public Map<Long, Long> getMapLongLong() {
		return mapLongLong;
	}

	public void setMapLongLong(Map<Long, Long> mapLongLong) {
		this.mapLongLong = mapLongLong;
	}

	public Map<Long, Long> getMapPriceActually() {
		return mapPriceActually;
	}

	public void setMapPriceActually(Map<Long, Long> mapPriceActually) {
		this.mapPriceActually = mapPriceActually;
	}

	public Long getShopRootId() {
		return shopRootId;
	}

	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}

	public List<Long> getLstLong() {
		return lstLong;
	}

	public void setLstLong(List<Long> lstLong) {
		this.lstLong = lstLong;
	}

	public Integer getStatusEquipRecord() {
		return statusEquipRecord;
	}

	public void setStatusEquipRecord(Integer statusEquipRecord) {
		this.statusEquipRecord = statusEquipRecord;
	}

	public Integer getTracingPlace() {
		return tracingPlace;
	}

	public void setTracingPlace(Integer tracingPlace) {
		this.tracingPlace = tracingPlace;
	}

	public String getConclusion() {
		return conclusion;
	}

	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}

	public String getRecommendedTreatment() {
		return recommendedTreatment;
	}

	public void setRecommendedTreatment(String recommendedTreatment) {
		this.recommendedTreatment = recommendedTreatment;
	}

	public String getRepresentor() {
		return representor;
	}

	public void setRepresentor(String representor) {
		this.representor = representor;
	}

	public Integer getRecordType() {
		return recordType;
	}

	public void setRecordType(Integer recordType) {
		this.recordType = recordType;
	}

	public Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	public Integer getStockType() {
		return stockType;
	}

	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public T getAttribute() {
		return attribute;
	}

	public void setAttribute(T attribute) {
		this.attribute = attribute;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public Integer getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(Integer deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public List<Long> getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}

	public KPaging<T> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastArisingSalesDateStr() {
		return lastArisingSalesDateStr;
	}

	public void setLastArisingSalesDateStr(String lastArisingSalesDateStr) {
		this.lastArisingSalesDateStr = lastArisingSalesDateStr;
	}

	public Date getLostDate() {
		return lostDate;
	}

	public void setLostDate(Date lostDate) {
		this.lostDate = lostDate;
	}

	public Date getLastArisingSalesDate() {
		return lastArisingSalesDate;
	}

	public void setLastArisingSalesDate(Date lastArisingSalesDate) {
		this.lastArisingSalesDate = lastArisingSalesDate;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Integer getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getLostDateStr() {
		return lostDateStr;
	}

	public void setLostDateStr(String lostDateStr) {
		this.lostDateStr = lostDateStr;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopCodeName() {
		return shopCodeName;
	}

	public void setShopCodeName(String shopCodeName) {
		this.shopCodeName = shopCodeName;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BigDecimal getPriceActually() {
		return priceActually;
	}

	public void setPriceActually(BigDecimal priceActually) {
		this.priceActually = priceActually;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getFlagRecordStatus() {
		return flagRecordStatus;
	}

	public void setFlagRecordStatus(Integer flagRecordStatus) {
		this.flagRecordStatus = flagRecordStatus;
	}

	public String getCustomerSearch() {
		return customerSearch;
	}

	public void setCustomerSearch(String customerSearch) {
		this.customerSearch = customerSearch;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.filter;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.StaffSpecificType;

/**
 * Filter cho staff type
 * @author trietptm
 * @since 23/10/2015 
 */
public class StaffTypeFilter implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private ActiveType status;
	private StaffSpecificType type;
	private String name;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public StaffSpecificType getType() {
		return type;
	}
	public void setType(StaffSpecificType type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}

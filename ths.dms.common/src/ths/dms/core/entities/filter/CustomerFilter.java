package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;
/**
 * @author hunglm16
 * @since October 2,2014
 * **/
public class CustomerFilter<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private KPaging<T> kPaging;
	
	private List<String> lstCode;
	private List<Long> lstShopId;
	private List<Long> lstCustomerId;
	private List<Long> lstNotInId;
	
	private Long customerId;
	private Long shopId;
	
	private Boolean isGetChildShop;

	private String customerCode;
	private String shortCode;
	private String customerName;
	private String shopCode;
	private String shopName;
	private String description;
	private String email;
	private String address;
	
	private Integer status;
	private Integer objectType;
	private List<String> lstStaffCode;
	private String staffCode;
	
	private Long staffId;
	private Long shopRootId;
	private Long roleId;
	private Long staffRootId;
	private Boolean isFlagCMS;
	private String strShopId;
	private Long cycleId;
	private Long ksId;
	private Date pDate;
	
	private String nganhHangParam;
	private Integer sothangParam;
	
	private Boolean isCheckStatus;
	
	public Boolean getIsFlagCMS() {
		return isFlagCMS;
	}
	public void setIsFlagCMS(Boolean isFlagCMS) {
		this.isFlagCMS = isFlagCMS;
	}
	public Long getStaffRootId() {
		return staffRootId;
	}
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public List<String> getLstStaffCode() {
		return lstStaffCode;
	}
	public void setLstStaffCode(List<String> lstStaffCode) {
		this.lstStaffCode = lstStaffCode;
	}
	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}
	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}
	public List<Long> getLstShopId() {
		return lstShopId;
	}
	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public List<String> getLstCode() {
		return lstCode;
	}
	public void setLstCode(List<String> lstCode) {
		this.lstCode = lstCode;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Boolean getIsGetChildShop() {
		return isGetChildShop;
	}
	public void setIsGetChildShop(Boolean isGetChildShop) {
		this.isGetChildShop = isGetChildShop;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public Date getpDate() {
		return pDate;
	}
	public void setpDate(Date pDate) {
		this.pDate = pDate;
	}
	public String getStrShopId() {
		return strShopId;
	}
	public void setStrShopId(String strShopId) {
		this.strShopId = strShopId;
	}
	public Long getCycleId() {
		return cycleId;
	}
	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}
	public Long getKsId() {
		return ksId;
	}
	public void setKsId(Long ksId) {
		this.ksId = ksId;
	}
	public Long getShopRootId() {
		return shopRootId;
	}
	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public List<Long> getLstNotInId() {
		return lstNotInId;
	}
	public void setLstNotInId(List<Long> lstNotInId) {
		this.lstNotInId = lstNotInId;
	}
	
	public String getNganhHangParam() {
		return nganhHangParam;
	}
	public void setNganhHangParam(String nganhHangParam) {
		this.nganhHangParam = nganhHangParam;
	}
	public Integer getSothangParam() {
		return sothangParam;
	}
	public void setSothangParam(Integer sothangParam) {
		this.sothangParam = sothangParam;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Boolean getIsCheckStatus() {
		return isCheckStatus;
	}
	public void setIsCheckStatus(Boolean isCheckStatus) {
		this.isCheckStatus = isCheckStatus;
	}
}

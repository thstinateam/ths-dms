/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.CatalogVO;

/**
 * Filter cho cac danh muc san pham
 * @author trietptm
 * @since Nov 17, 2015
 * */
public class CatalogFilter implements Serializable {
	private static final long serialVersionUID = 1L;

	private String text;
	private Long shopId;
	private String type;
	private Integer inportExportType;
	private KPaging<CatalogVO> kPaging;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public KPaging<CatalogVO> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<CatalogVO> kPaging) {
		this.kPaging = kPaging;
	}

	public Integer getInportExportType() {
		return inportExportType;
	}

	public void setInportExportType(Integer inportExportType) {
		this.inportExportType = inportExportType;
	}

}

/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
/**
 * @author vuongmq
 * @date 19/08/2015
 * @description Filter cho bang tong hop RPT_STAFF_SALE
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.Bank;
import ths.dms.core.entities.RptStaffSale;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;

public class RptStaffSaleFilter implements Serializable {

	private static final long serialVersionUID = 1L;
	private KPaging<RptStaffSale> kPaging;
	private Long staffRootId;
	private Long roleId;
	private String strShopId; // danh sach shop phan quyen
	private Long shopId;
	private Long staffId;
	private Long routingId;
	private String type;// text of object_type
	private Long customerId;
	private Integer specificType;
	private Long objectId;
	private Integer objetType;
	private List<Integer> lstObjetType;
	
	public KPaging<RptStaffSale> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<RptStaffSale> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getStaffRootId() {
		return staffRootId;
	}
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getStrShopId() {
		return strShopId;
	}
	public void setStrShopId(String strShopId) {
		this.strShopId = strShopId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Integer getSpecificType() {
		return specificType;
	}
	public void setSpecificType(Integer specificType) {
		this.specificType = specificType;
	}
	public Long getObjectId() {
		return objectId;
	}
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
	public Integer getObjetType() {
		return objetType;
	}
	public void setObjetType(Integer objetType) {
		this.objetType = objetType;
	}
	public List<Integer> getLstObjetType() {
		return lstObjetType;
	}
	public void setLstObjetType(List<Integer> lstObjetType) {
		this.lstObjetType = lstObjetType;
	}
	public Long getRoutingId() {
		return routingId;
	}
	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}

package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;
/**
 * @author hunglm16
 * @since October 2,2014
 * **/
public class OrganizationFilter<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private KPaging<T> kPaging;
	
	private List<String> lstCode;
	private List<Long> lstShopId;
	private List<Long> lstCustomerId;
	
	private Long customerId;
	private Long shopId;
	
	private Boolean isGetChildShop;

	private String customerCode;
	private String shortCode;
	private String customerName;
	private String shopCode;
	private String description;
	private String email;
	private String address;
	
	private Integer status;
	private Integer objectType;
	private List<String> lstStaffCode;
	private String staffCode;
	
	private Long staffRootId;
	private Boolean isFlagCMS;
	
	private Date pDate;
	
	public Boolean getIsFlagCMS() {
		return isFlagCMS;
	}
	public void setIsFlagCMS(Boolean isFlagCMS) {
		this.isFlagCMS = isFlagCMS;
	}
	public Long getStaffRootId() {
		return staffRootId;
	}
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public List<String> getLstStaffCode() {
		return lstStaffCode;
	}
	public void setLstStaffCode(List<String> lstStaffCode) {
		this.lstStaffCode = lstStaffCode;
	}
	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}
	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}
	public List<Long> getLstShopId() {
		return lstShopId;
	}
	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public List<String> getLstCode() {
		return lstCode;
	}
	public void setLstCode(List<String> lstCode) {
		this.lstCode = lstCode;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Boolean getIsGetChildShop() {
		return isGetChildShop;
	}
	public void setIsGetChildShop(Boolean isGetChildShop) {
		this.isGetChildShop = isGetChildShop;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public Date getpDate() {
		return pDate;
	}
	public void setpDate(Date pDate) {
		this.pDate = pDate;
	}
	
}

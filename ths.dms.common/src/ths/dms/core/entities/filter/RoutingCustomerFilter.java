package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.RoutingCustomer;
import ths.dms.core.entities.enumtype.KPaging;

public class RoutingCustomerFilter implements Serializable {

	private static final long serialVersionUID = 1L;
	private KPaging<RoutingCustomer> kPaging;

	private List<Long> lstRoutingId;
	private List<Long> lstCustomerId;

	private Long routingId;
	private Long customerId;
	private Long shopId;
	private Long id;
	private Long userId;
	private Long roleId;

	private Integer flagTime;

	private Boolean flag;

	private Date startDate;
	private Date toDate;
	private Date orderDate;

	private String shortCode;
	private String address;
	private String userName;

	
	public Integer getFlagTime() {
		return flagTime;
	}

	public void setFlagTime(Integer flagTime) {
		this.flagTime = flagTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Long> getLstRoutingId() {
		return lstRoutingId;
	}

	public void setLstRoutingId(List<Long> lstRoutingId) {
		this.lstRoutingId = lstRoutingId;
	}

	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}

	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getRoutingId() {
		return routingId;
	}

	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}

	public KPaging<RoutingCustomer> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<RoutingCustomer> kPaging) {
		this.kPaging = kPaging;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
}

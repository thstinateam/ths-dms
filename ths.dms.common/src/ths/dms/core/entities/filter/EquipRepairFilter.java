package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.EquipRepairForm;
import ths.dms.core.entities.EquipRepairFormDtl;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PaymentStatusRepair;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipRepairFormVOList;
import ths.dms.core.entities.vo.EquipRepairPayFormVO;

public class EquipRepairFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<EquipRepairFormVO> kPaging;
	private String seri;
	private String euqipCode;//ma thiet bi
	private String formCode;//ma phieu
	private Long equipCategoryId;//loai thiet bi
	private Long equipGroupId;//nhom thiet bi
	private Date fromDate;
	private Date toDate;	
	private Integer status; // trang thai
	private Integer statusPayment; // trang thai thanh toan
	private Long equipRepairId; // trang thai thanh toan
	private String strListShopId;//lst shop string
	private String customerCode;
	private String customerName;
	/**vuongmq; 15/04/2015*/
	private Long customerId;
	private String nganhHangStr;
	private Integer soThang;
	
	private Long staffRoot; /** nhan vien staffRoot*/
	private Long shopRoot; /** shopRoot chon */
	private String shopCode;
	
	private List<Long> lstId; /** cai nay print ds id cua phieu dua chua*/
	private KPaging<EquipRepairFormVOList> kPagingVOlist;
	private KPaging<EquipRepairFormDtl> kPagingDetail;
	private KPaging<EquipRepairPayFormVO> kPagingPayFormVO;
	private KPaging<EquipRepairForm> kPagingRepairForm;
	
	private Long id; 
	
	/** dung update trang thai phieu thanh toan */
	private StatusRecordsEquip statusRecordsEquip;
	private PaymentStatusRepair paymentStatusRepair;
	
	public KPaging<EquipRepairFormVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<EquipRepairFormVO> kPaging) {
		this.kPaging = kPaging;
	}
	public String getSeri() {
		return seri;
	}
	public void setSeri(String seri) {
		this.seri = seri;
	}
	public String getEuqipCode() {
		return euqipCode;
	}
	public void setEuqipCode(String euqipCode) {
		this.euqipCode = euqipCode;
	}
	public String getFormCode() {
		return formCode;
	}
	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}
	public Long getEquipCategoryId() {
		return equipCategoryId;
	}
	public void setEquipCategoryId(Long equipCategoryId) {
		this.equipCategoryId = equipCategoryId;
	}
	public Long getEquipGroupId() {
		return equipGroupId;
	}
	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getStatusPayment() {
		return statusPayment;
	}
	public void setStatusPayment(Integer statusPayment) {
		this.statusPayment = statusPayment;
	}
	public Long getEquipRepairId() {
		return equipRepairId;
	}
	public void setEquipRepairId(Long equipRepairId) {
		this.equipRepairId = equipRepairId;
	}
	public KPaging<EquipRepairFormDtl> getkPagingDetail() {
		return kPagingDetail;
	}
	public void setkPagingDetail(KPaging<EquipRepairFormDtl> kPagingDetail) {
		this.kPagingDetail = kPagingDetail;
	}
	public String getStrListShopId() {
		return strListShopId;
	}
	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getNganhHangStr() {
		return nganhHangStr;
	}
	public void setNganhHangStr(String nganhHangStr) {
		this.nganhHangStr = nganhHangStr;
	}
	public Integer getSoThang() {
		return soThang;
	}
	public void setSoThang(Integer soThang) {
		this.soThang = soThang;
	}
	public List<Long> getLstId() {
		return lstId;
	}
	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
	public KPaging<EquipRepairFormVOList> getkPagingVOlist() {
		return kPagingVOlist;
	}
	public void setkPagingVOlist(KPaging<EquipRepairFormVOList> kPagingVOlist) {
		this.kPagingVOlist = kPagingVOlist;
	}
	public Long getStaffRoot() {
		return staffRoot;
	}
	public void setStaffRoot(Long staffRoot) {
		this.staffRoot = staffRoot;
	}
	public Long getShopRoot() {
		return shopRoot;
	}
	public void setShopRoot(Long shopRoot) {
		this.shopRoot = shopRoot;
	}
	public KPaging<EquipRepairPayFormVO> getkPagingPayFormVO() {
		return kPagingPayFormVO;
	}
	public void setkPagingPayFormVO(KPaging<EquipRepairPayFormVO> kPagingPayFormVO) {
		this.kPagingPayFormVO = kPagingPayFormVO;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public KPaging<EquipRepairForm> getkPagingRepairForm() {
		return kPagingRepairForm;
	}
	public void setkPagingRepairForm(KPaging<EquipRepairForm> kPagingRepairForm) {
		this.kPagingRepairForm = kPagingRepairForm;
	}
	public StatusRecordsEquip getStatusRecordsEquip() {
		return statusRecordsEquip;
	}
	public void setStatusRecordsEquip(StatusRecordsEquip statusRecordsEquip) {
		this.statusRecordsEquip = statusRecordsEquip;
	}
	public PaymentStatusRepair getPaymentStatusRepair() {
		return paymentStatusRepair;
	}
	public void setPaymentStatusRepair(PaymentStatusRepair paymentStatusRepair) {
		this.paymentStatusRepair = paymentStatusRepair;
	}
	
}

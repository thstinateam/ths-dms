
/**
 * @author thongnm
 *
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.DisplayToolVO;

public class DisplayToolFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6492223311164902482L;

	private KPaging<DisplayToolVO> kPaging;
	
	private String displayToolCode;
	
	private String displayToolName;
	
	private ActiveType status;
	
	private Long shopId;
	
	private Long staffId;
	
	private String inMonth;
	
	private List<Long> lstExceptionalDisplayToolId; // Loại tru list TU mong muon
	
	private Boolean exceptAllDisplayToolOfProduct;  // Loai tru tat ca TU cua SP

	public KPaging<DisplayToolVO> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<DisplayToolVO> kPaging) {
		this.kPaging = kPaging;
	}

	public String getDisplayToolCode() {
		return displayToolCode;
	}

	public void setDisplayToolCode(String displayToolCode) {
		this.displayToolCode = displayToolCode;
	}

	public String getDisplayToolName() {
		return displayToolName;
	}

	public void setDisplayToolName(String displayToolName) {
		this.displayToolName = displayToolName;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public List<Long> getLstExceptionalDisplayToolId() {
		return lstExceptionalDisplayToolId;
	}

	public void setLstExceptionalDisplayToolId( List<Long> lstExceptionalDisplayToolId) {
		this.lstExceptionalDisplayToolId = lstExceptionalDisplayToolId;
	}

	public Boolean getExceptAllDisplayToolOfProduct() {
		return exceptAllDisplayToolOfProduct;
	}
	public void setExceptAllDisplayToolOfProduct(
			Boolean exceptAllDisplayToolOfProduct) {
		this.exceptAllDisplayToolOfProduct = exceptAllDisplayToolOfProduct;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getInMonth() {
		return inMonth;
	}

	public void setInMonth(String inMonth) {
		this.inMonth = inMonth;
	}
	
}

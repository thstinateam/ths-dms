package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.EquipStockAdjustFormDtl;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.EquipStockAdjustFormDtlVO;
import ths.dms.core.entities.vo.EquipStockAdjustFormVO;

public class EquipStockAdjustFormFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<EquipStockAdjustFormVO> kPaging;
	
	private KPaging<EquipStockAdjustFormDtlVO> kPagingDtl;

	private Long id;
	private String code;
	private String description;
	private Date fromDate;
	private Date toDate;
	private Integer status;
	private List<Long> lstId;
	private Long shopId;
	private Long EquipStockAdjustFormDTLID;
	private Integer isCreate;
	private Long staffRootId;
	
	public KPaging<EquipStockAdjustFormVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<EquipStockAdjustFormVO> kPaging) {
		this.kPaging = kPaging;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<Long> getLstId() {
		return lstId;
	}
	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
	public Long getEquipStockAdjustFormDTLID() {
		return EquipStockAdjustFormDTLID;
	}
	public void setEquipStockAdjustFormDTLID(Long equipStockAdjustFormDTLID) {
		EquipStockAdjustFormDTLID = equipStockAdjustFormDTLID;
	}
	public KPaging<EquipStockAdjustFormDtlVO> getkPagingDtl() {
		return kPagingDtl;
	}
	public void setkPagingDtl(KPaging<EquipStockAdjustFormDtlVO> kPagingDtl) {
		this.kPagingDtl = kPagingDtl;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Integer getIsCreate() {
		return isCreate;
	}
	public void setIsCreate(Integer isCreate) {
		this.isCreate = isCreate;
	}
	public Long getStaffRootId() {
		return staffRootId;
	}
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}
	
	
}

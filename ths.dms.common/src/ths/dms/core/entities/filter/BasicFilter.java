package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopSpecificType;

/**
 * Lop Filter co ban can co de lam tham so
 * 
 * @author hunglm16
 * @return BasicFilter
 * @description Chi bo sung truong o muc dung chung cho tat ca cac Table, khong tao them truong khac biet, không tạo thêm Object ngoài <T>
 */
public class BasicFilter<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private KPaging<T> kPaging;
	private T attribute;
	
	private List<Long> lstId;
	private List<Long> arrlongG;
	private List<Long> arrLongS;
	private List<Integer> arrIntG;
	private List<String> arrStrG;
	private List<String> arrCode;
	
	private Long id;
	private Long longG;
	private Long userId;
	private Long longType;
	private Long longP;
	private Long shopId;
	private Long longPI;
	private Long parentId;
	private Long shopRootId;
	private Long inheritUserPriv;
	private Integer isLevelShop;
	private Long objectId;
	private Long fromId;
	private Long toId;
	private Long staffRootId;
	private Long roleId;
	
	private Integer type;
	private Integer status;
	private Integer seq;
	private Integer intG;
	private Integer objectType;
	private Integer shopChannel;
	private Integer warehouseType;
	
	private String code;
	private String name;
	private String codeAndName;
	private String userName;
	private String shopCode;
	private String description;
	private String textG;
	private String equipStockCode;
	private String url;
	private String fileName;
	private String typeStr;
	private String strShopId;
	
	private Date fromDate;
	private Date toDate;
	private Date fromMonth;
	private Date toMonth;
	private Date dateG;
	
	private Boolean isArr;
	private Boolean flagCMS;
	private Boolean isConnectBy;
	private Boolean isUnionShopRoot;
	
	private String sort;
	private String order;
	
	private Boolean isExistStock;
	
	private ShopSpecificType shopType;
	
	private Long shopIdAndChilds;

	private Long productId;
	
	/**
	 * Khai bao phuong thuc Getter/ Setter
	 * */
	public List<String> getArrStrG() {
		return arrStrG;
	}
	public List<Long> getLstId() {
		return lstId;
	}
	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
	public Boolean getIsUnionShopRoot() {
		return isUnionShopRoot;
	}
	public void setIsUnionShopRoot(Boolean isUnionShopRoot) {
		this.isUnionShopRoot = isUnionShopRoot;
	}
	public Long getObjectId() {
		return objectId;
	}
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public void setArrStrG(List<String> arrStrG) {
		this.arrStrG = arrStrG;
	}
	public String getEquipStockCode() {
		return equipStockCode;
	}
	public void setEquipStockCode(String equipStockCode) {
		this.equipStockCode = equipStockCode;
	}
	public Boolean getFlagCMS() {
		return flagCMS;
	}
	public void setFlagCMS(Boolean flagCMS) {
		this.flagCMS = flagCMS;
	}
	public Long getShopRootId() {
		return shopRootId;
	}
	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}
	public Integer getIsLevelShop() {
		return isLevelShop;
	}
	public void setIsLevelShop(Integer isLevelShop) {
		this.isLevelShop = isLevelShop;
	}
	public Long getInheritUserPriv() {
		return inheritUserPriv;
	}
	public void setInheritUserPriv(Long inheritUserPriv) {
		this.inheritUserPriv = inheritUserPriv;
	}
	public Integer getShopChannel() {
		return shopChannel;
	}
	public void setShopChannel(Integer shopChannel) {
		this.shopChannel = shopChannel;
	}
	public List<Long> getArrLongS() {
		return arrLongS;
	}
	public void setArrLongS(List<Long> arrLongS) {
		this.arrLongS = arrLongS;
	}
	public List<String> getArrCode() {
		return arrCode;
	}
	public void setArrCode(List<String> arrCode) {
		this.arrCode = arrCode;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public List<String> getArrStr() {
		return arrStrG;
	}
	public void setArrStr(List<String> arrStrG) {
		this.arrStrG = arrStrG;
	}
	public Long getLongPI() {
		return longPI;
	}
	public void setLongPI(Long longPI) {
		this.longPI = longPI;
	}
	public List<Integer> getArrIntG() {
		return arrIntG;
	}
	public void setArrIntG(List<Integer> arrIntG) {
		this.arrIntG = arrIntG;
	}
	public Boolean getIsConnectBy() {
		return isConnectBy;
	}
	public void setIsConnectBy(Boolean isConnectBy) {
		this.isConnectBy = isConnectBy;
	}
	public Boolean getIsArr() {
		return isArr;
	}
	public void setIsArr(Boolean isArr) {
		this.isArr = isArr;
	}
	public Long getLongP() {
		return longP;
	}
	public void setLongP(Long longP) {
		this.longP = longP;
	}
	public Long getLongType() {
		return longType;
	}
	public void setLongType(Long longType) {
		this.longType = longType;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public T getAttribute() {
		return attribute;
	}
	public void setAttribute(T attribute) {
		this.attribute = attribute;
	}
	private Integer intFlag; 
	
	public Integer getIntFlag() {
		return intFlag;
	}
	public void setIntFlag(Integer intFlag) {
		this.intFlag = intFlag;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public List<Long> getArrlongG() {
		return arrlongG;
	}
	public void setArrlongG(List<Long> arrlongG) {
		this.arrlongG = arrlongG;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getLongG() {
		return longG;
	}
	public void setLongG(Long longG) {
		this.longG = longG;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTextG() {
		return textG;
	}
	public void setTextG(String textG) {
		this.textG = textG;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public Integer getIntG() {
		return intG;
	}
	public void setIntG(Integer intG) {
		this.intG = intG;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getDateG() {
		return dateG;
	}
	public void setDateG(Date dateG) {
		this.dateG = dateG;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getFromId() {
		return fromId;
	}
	public void setFromId(Long fromId) {
		this.fromId = fromId;
	}
	public Long getToId() {
		return toId;
	}
	public void setToId(Long toId) {
		this.toId = toId;
	}
	public Date getFromMonth() {
		return fromMonth;
	}
	public void setFromMonth(Date fromMonth) {
		this.fromMonth = fromMonth;
	}
	public Date getToMonth() {
		return toMonth;
	}
	public void setToMonth(Date toMonth) {
		this.toMonth = toMonth;
	}
	public Long getStaffRootId() {
		return staffRootId;
	}
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}
	public String getTypeStr() {
		return typeStr;
	}
	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public ShopSpecificType getShopType() {
		return shopType;
	}
	public void setShopType(ShopSpecificType shopType) {
		this.shopType = shopType;
	}
	public String getStrShopId() {
		return strShopId;
	}
	public void setStrShopId(String strShopId) {
		this.strShopId = strShopId;
	}
	public String getCodeAndName() {
		return codeAndName;
	}
	public void setCodeAndName(String codeAndName) {
		this.codeAndName = codeAndName;
	}
	public Integer getWarehouseType() {
		return warehouseType;
	}
	public void setWarehouseType(Integer warehouseType) {
		this.warehouseType = warehouseType;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Boolean getIsExistStock() {
		return isExistStock;
	}
	public void setIsExistStock(Boolean isExistStock) {
		this.isExistStock = isExistStock;
	}
	public Long getShopIdAndChilds() {
		return shopIdAndChilds;
	}
	public void setShopIdAndChilds(Long shopIdAndChilds) {
		this.shopIdAndChilds = shopIdAndChilds;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	
}

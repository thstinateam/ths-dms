package ths.dms.core.entities.filter;

/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.EquipSuggestEviction;
import ths.dms.core.entities.EquipSuggestEvictionDTL;
import ths.dms.core.entities.enumtype.KPaging;

/**
 * Filter chuc nang quan ly de nghi thu hoi
 * 
 * @author nhutnn
 * @since 14/07/2015
 */
public class EquipSuggestEvictionFilter<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	private KPaging<T> kPaging;
	private T attribute;

	private EquipPeriod equipPeriod;
	private EquipSuggestEviction equipSuggestEviction;
	
	private List<EquipSuggestEvictionDTL> lstEquipSuggestEvictionDTLs;
	private List<Long> lstId;
	private List<Long> lstShopId;
	private List<Long> lstLong;
	private List<Long> lstObjectId;
	private List<Long> lstCustomerId;
	private List<Integer> lstStatus;
	private List<String> lstCustomerCode;

	private Long id;
	private Long shopId;
	private Long objectId;
	private Long customerId;

	private Integer status;
	private Integer deliveryStatus;

	private String code;
	private String name;
	private String shopCode;
	private String shortCode;
	private String userName;
	private String equipCode;
	
	private Date fromDate;
	private Date toDate;
	
	/**
	 * Khai bao phuong thuc GETTER/SETTER
	 * */
	
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public T getAttribute() {
		return attribute;
	}
	public void setAttribute(T attribute) {
		this.attribute = attribute;
	}
	public EquipPeriod getEquipPeriod() {
		return equipPeriod;
	}
	public void setEquipPeriod(EquipPeriod equipPeriod) {
		this.equipPeriod = equipPeriod;
	}
	public List<Long> getLstId() {
		return lstId;
	}
	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
	public List<Long> getLstShopId() {
		return lstShopId;
	}
	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}
	public List<Long> getLstLong() {
		return lstLong;
	}
	public void setLstLong(List<Long> lstLong) {
		this.lstLong = lstLong;
	}
	public List<Long> getLstObjectId() {
		return lstObjectId;
	}
	public void setLstObjectId(List<Long> lstObjectId) {
		this.lstObjectId = lstObjectId;
	}
	public List<Integer> getLstStatus() {
		return lstStatus;
	}
	public void setLstStatus(List<Integer> lstStatus) {
		this.lstStatus = lstStatus;
	}
	public List<String> getLstCustomerCode() {
		return lstCustomerCode;
	}
	public void setLstCustomerCode(List<String> lstCustomerCode) {
		this.lstCustomerCode = lstCustomerCode;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getObjectId() {
		return objectId;
	}
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Integer getDeliveryStatus() {
		return deliveryStatus;
	}
	public void setDeliveryStatus(Integer deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public EquipSuggestEviction getEquipSuggestEviction() {
		return equipSuggestEviction;
	}
	public void setEquipSuggestEviction(EquipSuggestEviction equipSuggestEviction) {
		this.equipSuggestEviction = equipSuggestEviction;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<EquipSuggestEvictionDTL> getLstEquipSuggestEvictionDTLs() {
		return lstEquipSuggestEvictionDTLs;
	}
	public void setLstEquipSuggestEvictionDTLs(List<EquipSuggestEvictionDTL> lstEquipSuggestEvictionDTLs) {
		this.lstEquipSuggestEvictionDTLs = lstEquipSuggestEvictionDTLs;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}
	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}
	public String getEquipCode() {
		return equipCode;
	}
	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}	
	
}

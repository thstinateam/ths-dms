package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.EquipRepairFormDtl;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.vo.EquipItemVO;
import ths.dms.core.entities.vo.EquipRepairFormVO;

public class EquipItemFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<EquipItemVO> kPaging;
	private String name;
	private Long equipId;
	private List<Long> lstEquipItemIdExcept;
	private ActiveType status;
	/** vuongmq; 20/04/2015*/
	private Long epuipItemId;
	private boolean flagWarrantyExpriedDate;// flag lay hang muc con thoi gian bao hanh; ham getListEquipRepairDtlByFilter()
	private StatusRecordsEquip statusRepair;
	private Integer usageStatus;
	private Integer tradeStatus;
	public KPaging<EquipItemVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<EquipItemVO> kPaging) {
		this.kPaging = kPaging;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Long> getLstEquipItemIdExcept() {
		return lstEquipItemIdExcept;
	}
	public void setLstEquipItemIdExcept(List<Long> lstEquipItemIdExcept) {
		this.lstEquipItemIdExcept = lstEquipItemIdExcept;
	}
	public Long getEquipId() {
		return equipId;
	}
	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public Long getEpuipItemId() {
		return epuipItemId;
	}
	public void setEpuipItemId(Long epuipItemId) {
		this.epuipItemId = epuipItemId;
	}
	public boolean isFlagWarrantyExpriedDate() {
		return flagWarrantyExpriedDate;
	}
	public void setFlagWarrantyExpriedDate(boolean flagWarrantyExpriedDate) {
		this.flagWarrantyExpriedDate = flagWarrantyExpriedDate;
	}
	public StatusRecordsEquip getStatusRepair() {
		return statusRepair;
	}
	public void setStatusRepair(StatusRecordsEquip statusRepair) {
		this.statusRepair = statusRepair;
	}
	public Integer getUsageStatus() {
		return usageStatus;
	}
	public void setUsageStatus(Integer usageStatus) {
		this.usageStatus = usageStatus;
	}
	public Integer getTradeStatus() {
		return tradeStatus;
	}
	public void setTradeStatus(Integer tradeStatus) {
		this.tradeStatus = tradeStatus;
	}
	
}

package ths.dms.core.entities.filter;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.DisplayToolCustomerVO;

public class DisplayToolCustomerFilter implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7869249612961702123L;

	private KPaging<DisplayToolCustomerVO> kPaging;
		
	private Long shopId;
	
	private Long staffId;
	
	private String month;
	
	private Long gsnppId;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public KPaging<DisplayToolCustomerVO> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<DisplayToolCustomerVO> kPaging) {
		this.kPaging = kPaging;
	}

	public Long getGsnppId() {
		return gsnppId;
	}

	public void setGsnppId(Long gsnppId) {
		this.gsnppId = gsnppId;
	}
}

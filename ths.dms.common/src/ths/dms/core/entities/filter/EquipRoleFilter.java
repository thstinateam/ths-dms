package ths.dms.core.entities.filter;

/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.EquipmentStaffVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.ShopVO;

/**
 * Filter EquipRole - phan quyen nguoi dung - Thiet Bi
 * 
 * @author tamvnm 
 * @since March 25,2015
 */
public class EquipRoleFilter implements Serializable {
	private static final long serialVersionUID = 1L;

	private KPaging<EquipRole> kPaging;
	private Long staffId;
	private String roleCode;
	private String roleName;
	private Integer statusRole;
	
	public KPaging<EquipRole> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<EquipRole> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Integer getStatusRole() {
		return statusRole;
	}
	public void setStatusRole(Integer statusRole) {
		this.statusRole = statusRole;
	}
	
	
}

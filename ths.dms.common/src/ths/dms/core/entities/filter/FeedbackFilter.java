/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.FeedbackVO;

/**
 * Thong tin cac cot filter theo doi van de khi tim kiem
 * @author vuongmq
 * @since 11/11/2015
 *
 */
public class FeedbackFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4948494140913237552L;
	private KPaging<FeedbackVO> kPagingVO;
	private Long shopRootId;
	private Long staffRootId;
	private Long roleId;
	private Long createUserId;
	private Long id;
	private Long feedbackId;
	private Long shopId;
	private Long staffId;
	private Long customerId;
	private String staffCode;
	private String customerCode;
	private String typeParam;
	private Integer status;
	private Integer type;
	private Integer flagGetStaffId;
	private Date fromDate;
	private Date toDate;
	private List<Long> lstId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public List<Long> getLstId() {
		return lstId;
	}
	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
	public KPaging<FeedbackVO> getkPagingVO() {
		return kPagingVO;
	}
	public void setkPagingVO(KPaging<FeedbackVO> kPagingVO) {
		this.kPagingVO = kPagingVO;
	}
	public Long getShopRootId() {
		return shopRootId;
	}
	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getStaffRootId() {
		return staffRootId;
	}
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}
	public String getTypeParam() {
		return typeParam;
	}
	public void setTypeParam(String typeParam) {
		this.typeParam = typeParam;
	}
	public Long getFeedbackId() {
		return feedbackId;
	}
	public void setFeedbackId(Long feedbackId) {
		this.feedbackId = feedbackId;
	}
	public Long getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}
	public Integer getFlagGetStaffId() {
		return flagGetStaffId;
	}
	public void setFlagGetStaffId(Integer flagGetStaffId) {
		this.flagGetStaffId = flagGetStaffId;
	}
	
}

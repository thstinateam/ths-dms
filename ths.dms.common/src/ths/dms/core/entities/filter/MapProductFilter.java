/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;

/***
 * Chua cac tham so va tham bien dung chung cua Map san pham
 * @author hunglm16
 * @param <T>
 * @since 11/11/2015
 */
public class MapProductFilter<T> implements Serializable {

	private static final long serialVersionUID = -1556865548390864883L;
	
	private KPaging<T> kPaging;
	
	private Boolean pagination;
	
	private Long id;
	private Long productId;
	private Long cycleCountId;
	private Long cycleCountMapProductId;
	
	private String oum1;
	private String code;
	private String name;
	private String productCode;
	private String productName;
	
	private Integer status;
	
	private List<Long> lstOjectId;

	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * @return
	 */
	public KPaging<T> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getCycleCountId() {
		return cycleCountId;
	}

	public void setCycleCountId(Long cycleCountId) {
		this.cycleCountId = cycleCountId;
	}

	public Long getCycleCountMapProductId() {
		return cycleCountMapProductId;
	}

	public void setCycleCountMapProductId(Long cycleCountMapProductId) {
		this.cycleCountMapProductId = cycleCountMapProductId;
	}

	public String getOum1() {
		return oum1;
	}

	public void setOum1(String oum1) {
		this.oum1 = oum1;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<Long> getLstOjectId() {
		return lstOjectId;
	}

	public void setLstOjectId(List<Long> lstOjectId) {
		this.lstOjectId = lstOjectId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Boolean getPagination() {
		return pagination;
	}

	public void setPagination(Boolean pagination) {
		this.pagination = pagination;
	}
	
}

package ths.dms.core.entities.filter;

/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.EquipmentStaffVO;

/**
 * Filter staff cho phan quyen nguoi dung - Thiet Bi
 * 
 * @author tamvnm
 * @since March 24,2015
 */
public class EquipStaffFilter implements Serializable {
	private static final long serialVersionUID = 1L;

	private KPaging<EquipmentStaffVO> kPaging;
	private Long currentStaffId;
	private Long shopId;
	private Integer flagCMS;
	private String personalCode;
	private String personalName;
	private Integer personalStatus;
	private Long staffRootId;
	private Long shopRootId;
	private Long roleId;
	private String strListUserId;
	private Long userId;
	private List<String> listShopId;
	private String lstShopId;

	public KPaging<EquipmentStaffVO> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<EquipmentStaffVO> kPaging) {
		this.kPaging = kPaging;
	}

	public Long getStaffId() {
		return currentStaffId;
	}

	public void setStaffId(Long staffId) {
		this.currentStaffId = staffId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getFlagCMS() {
		return flagCMS;
	}

	public void setFlagCMS(Integer flagCMS) {
		this.flagCMS = flagCMS;
	}

	public String getPersonalCode() {
		return personalCode;
	}

	public void setPersonalCode(String personalCode) {
		this.personalCode = personalCode;
	}

	public String getPersonalName() {
		return personalName;
	}

	public void setPersonalName(String personalName) {
		this.personalName = personalName;
	}

	public Integer getPersonalStatus() {
		return personalStatus;
	}

	public void setPersonalStatus(Integer personalStatus) {
		this.personalStatus = personalStatus;
	}

	public Long getCurrentStaffId() {
		return currentStaffId;
	}

	public void setCurrentStaffId(Long currentStaffId) {
		this.currentStaffId = currentStaffId;
	}

	public Long getStaffRootId() {
		return staffRootId;
	}

	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}

	public Long getShopRootId() {
		return shopRootId;
	}

	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getStrListUserId() {
		return strListUserId;
	}

	public void setStrListUserId(String strListUserId) {
		this.strListUserId = strListUserId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<String> getListShopId() {
		return listShopId;
	}

	public void setListShopId(List<String> listShopId) {
		this.listShopId = listShopId;
	}

	public String getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(String lstShopId) {
		this.lstShopId = lstShopId;
	}

}


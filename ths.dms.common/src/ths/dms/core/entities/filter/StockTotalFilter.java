/**
 * 
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.WarehouseType;
import ths.dms.core.entities.vo.StockTotalVO;

/**
 *  Toi doi tuong loc cho cac doi tuong kho
 * @author tientv11
 * @since October 1,214
 */
public class StockTotalFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	KPaging<StockTotalVO> kPaging;
	
	private Long productId;
	
	private String productCode;
	
	private String productName;
	
	private String promotionProgramCode;
	
	private Long ownerId;
	
	private StockObjectType ownerType;
	
	private Long orderId;
	
	private Long catId;
	
	private Long subCatId;
	
	private Integer fromQuantity;	
	
	private Integer toQuantity;
	
	private List<Long> exceptProductId;
	
	private Long staffId;
	
	private Integer seq;
	
	private Long warehouseId;
	
	private Boolean isExport;
	
	private WarehouseType orderByType;
	
	private Integer typeUpdate;
	
	private Long idChecking;

	private WarehouseType warehouseType;
	
	private Integer status;
	
	public Integer getTypeUpdate() {
		return typeUpdate;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public void setTypeUpdate(Integer typeUpdate) {
		this.typeUpdate = typeUpdate;
	}

	public Boolean getIsExport() {
		return isExport;
	}

	public void setIsExport(Boolean isExport) {
		this.isExport = isExport;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public KPaging<StockTotalVO> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<StockTotalVO> kPaging) {
		this.kPaging = kPaging;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}

	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public StockObjectType getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(StockObjectType ownerType) {
		this.ownerType = ownerType;
	}

	public Long getCatId() {
		return catId;
	}

	public void setCatId(Long catId) {
		this.catId = catId;
	}

	public Long getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(Long subCatId) {
		this.subCatId = subCatId;
	}

	public Integer getFromQuantity() {
		return fromQuantity;
	}

	public void setFromQuantity(Integer fromQuantity) {
		this.fromQuantity = fromQuantity;
	}

	public Integer getToQuantity() {
		return toQuantity;
	}

	public void setToQuantity(Integer toQuantity) {
		this.toQuantity = toQuantity;
	}

	public List<Long> getExceptProductId() {
		return exceptProductId;
	}

	public void setExceptProductId(List<Long> exceptProductId) {
		this.exceptProductId = exceptProductId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getIdChecking() {
		return idChecking;
	}

	public void setIdChecking(Long idChecking) {
		this.idChecking = idChecking;
	}

	public WarehouseType getWarehouseType() {
		return warehouseType;
	}

	public void setWarehouseType(WarehouseType warehouseType) {
		this.warehouseType = warehouseType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public WarehouseType getOrderByType() {
		return orderByType;
	}

	public void setOrderByType(WarehouseType orderByType) {
		this.orderByType = orderByType;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
}


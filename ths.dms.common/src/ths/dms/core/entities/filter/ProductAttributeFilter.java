/**
 * 
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;

/**
 * Bo loc ProductAttribute
 * @author tientv11
 * @since 16/01/2015
 *
 */
public class ProductAttributeFilter implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1172786645448734206L;

	private Long id;
	
	private Long productAttId;
	
	private Long productId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductAttId() {
		return productAttId;
	}

	public void setProductAttId(Long productAttId) {
		this.productAttId = productAttId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}
	
	
	
}

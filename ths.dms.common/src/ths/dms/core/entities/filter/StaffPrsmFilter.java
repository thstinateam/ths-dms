package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;
/**
 * Lop Filter Staff an theo phan quyen
 * 
 * @author hunglm16
 * @since September 28,2014
 * @description Chi bo sung truong o muc dung chung cho tat ca cac Table, khong tao them truong khac biet, không tạo thêm Oject ngoài <T>
 */
public class StaffPrsmFilter<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	private KPaging<T> kPaging;
	
	private List<String> lstCode;
	private List<String> lstStr;
	private List<String> lstParentStaffCode;
	private List<Long> lstLong;
	private List<Long> lstId;
	private List<Long> lstParentStaffId;
	private List<Long> lstShopId;
	private List<Integer> lstObjectType;
	private List<Integer> lstSpecType;//Nutifood join staff_type, use specific_type
	
	private Long id;
	private Long inheritUserPriv;
	private Long parentStaffId;
	private Long staffOwnerId;
	private Long staffId;
	private Long shopId;
	private Long roleId;
	private Long userId;
	
	private Boolean isGetChildShop;
	private Boolean isGetShopStopped;
	private Boolean isLstChildStaffRoot;
	
	private Integer status;
	private Integer staffType;
	private Integer statusPrsm;
	private Integer objectType;
	
	private String staffCode;
	private String staffName;
	private String saleTypeCode;	
	private String staffTypeCode;
	private String shopCode;
	private String address;
	private String staffIdListStr;
	
	private Date fromDate;
	private Date toDate;
	
	private Integer gsmt;
	
	public Boolean getIsLstChildStaffRoot() {
		return isLstChildStaffRoot;
	}
	public void setIsLstChildStaffRoot(Boolean isLstChildStaffRoot) {
		this.isLstChildStaffRoot = isLstChildStaffRoot;
	}
	public List<Long> getLstShopId() {
		return lstShopId;
	}
	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}
	public List<Long> getLstParentStaffId() {
		return lstParentStaffId;
	}
	public void setLstParentStaffId(List<Long> lstParentStaffId) {
		this.lstParentStaffId = lstParentStaffId;
	}
	public List<String> getLstParentStaffCode() {
		return lstParentStaffCode;
	}
	public void setLstParentStaffCode(List<String> lstParentStaffCode) {
		this.lstParentStaffCode = lstParentStaffCode;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public Integer getStatusPrsm() {
		return statusPrsm;
	}
	public void setStatusPrsm(Integer statusPrsm) {
		this.statusPrsm = statusPrsm;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getParentStaffId() {
		return parentStaffId;
	}
	public void setParentStaffId(Long parentStaffId) {
		this.parentStaffId = parentStaffId;
	}
	public Long getStaffOwnerId() {
		return staffOwnerId;
	}
	public void setStaffOwnerId(Long staffOwnerId) {
		this.staffOwnerId = staffOwnerId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Integer getStaffType() {
		return staffType;
	}
	public void setStaffTypeId(Integer staffType) {
		this.staffType = staffType;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public List<String> getLstCode() {
		return lstCode;
	}
	public void setLstCode(List<String> lstCode) {
		this.lstCode = lstCode;
	}
	public List<String> getLstStr() {
		return lstStr;
	}
	public void setLstStr(List<String> lstStr) {
		this.lstStr = lstStr;
	}
	public List<Long> getLstLong() {
		return lstLong;
	}
	public void setLstLong(List<Long> lstLong) {
		this.lstLong = lstLong;
	}
	public List<Long> getLstId() {
		return lstId;
	}
	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getInheritUserPriv() {
		return inheritUserPriv;
	}
	public void setInheritUserPriv(Long inheritUserPriv) {
		this.inheritUserPriv = inheritUserPriv;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getStaffTypeCode() {
		return staffTypeCode;
	}
	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Boolean getIsGetChildShop() {
		return isGetChildShop;
	}
	public void setIsGetChildShop(Boolean isGetChildShop) {
		this.isGetChildShop = isGetChildShop;
	}
	public String getSaleTypeCode() {
		return saleTypeCode;
	}
	public void setSaleTypeCode(String saleTypeCode) {
		this.saleTypeCode = saleTypeCode;
	}
	public List<Integer> getLstObjectType() {
		return lstObjectType;
	}
	public void setLstObjectType(List<Integer> lstObjectType) {
		this.lstObjectType = lstObjectType;
	}
	public String getStaffIdListStr() {
		return staffIdListStr;
	}
	public void setStaffIdListStr(String staffIdListStr) {
		this.staffIdListStr = staffIdListStr;
	}
	public List<Integer> getLstSpecType() {
		return lstSpecType;
	}
	public void setLstSpecType(List<Integer> lstSpecType) {
		this.lstSpecType = lstSpecType;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public Boolean getIsGetShopStopped() {
		return isGetShopStopped;
	}
	public void setIsGetShopStopped(Boolean isGetShopStopped) {
		this.isGetShopStopped = isGetShopStopped;
	}
	
	public Integer getGsmt() {
		return gsmt;
	}
	public void setGsmt(Integer gsmt) {
		this.gsmt = gsmt;
	}
}

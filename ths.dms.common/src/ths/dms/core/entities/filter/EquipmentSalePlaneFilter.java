package ths.dms.core.entities.filter;

/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.EquipSalePlan;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;

/**
 * Filter dung chung cho quan ly thiet bi
 * 
 * @author hoanv25
 * @since December 10,2014
 * 
 * @author nhutnn
 * @since 15/12/2014
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description ra soat va phan hoach
 */
public class EquipmentSalePlaneFilter<T> implements Serializable {

	private static final long serialVersionUID = 552504298955594226L;

	private KPaging<T> kPaging;
	private T attribute;

	private Long equipSalePlanId;
	private Long equipGroupId;

	private List<String> lstEquipDelete;
	private List<String> lstEquipAdd;
	private List<Long> lstDelete;
	private List<EquipSalePlan> lstInsert;
	private List<EquipSalePlan> lstChange;

	private String arrDelStr;
	private String arrAmountStr;
	private String arrToMonthStr;
	private String arrFromMonthStr;
	private String arrCustomerTypeStr;

	private String userName;

	LogInfoVO logInfoVO;

	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	private List<Long> lstIdRecord;

	public String getArrDelStr() {
		return arrDelStr;
	}

	public void setArrDelStr(String arrDelStr) {
		this.arrDelStr = arrDelStr;
	}

	public String getArrAmountStr() {
		return arrAmountStr;
	}

	public void setArrAmountStr(String arrAmountStr) {
		this.arrAmountStr = arrAmountStr;
	}

	public String getArrToMonthStr() {
		return arrToMonthStr;
	}

	public void setArrToMonthStr(String arrToMonthStr) {
		this.arrToMonthStr = arrToMonthStr;
	}

	public String getArrFromMonthStr() {
		return arrFromMonthStr;
	}

	public void setArrFromMonthStr(String arrFromMonthStr) {
		this.arrFromMonthStr = arrFromMonthStr;
	}

	public String getArrCustomerTypeStr() {
		return arrCustomerTypeStr;
	}

	public void setArrCustomerTypeStr(String arrCustomerTypeStr) {
		this.arrCustomerTypeStr = arrCustomerTypeStr;
	}

	public T getAttribute() {
		return attribute;
	}

	public void setAttribute(T attribute) {
		this.attribute = attribute;
	}

	public List<Long> getLstIdRecord() {
		return lstIdRecord;
	}

	public void setLstIdRecord(List<Long> lstIdRecord) {
		this.lstIdRecord = lstIdRecord;
	}

	public List<String> getLstEquipDelete() {
		return lstEquipDelete;
	}

	public void setLstEquipDelete(List<String> lstEquipDelete) {
		this.lstEquipDelete = lstEquipDelete;
	}

	public List<String> getLstEquipAdd() {
		return lstEquipAdd;
	}

	public void setLstEquipAdd(List<String> lstEquipAdd) {
		this.lstEquipAdd = lstEquipAdd;
	}

	public List<Long> getLstDelete() {
		return lstDelete;
	}

	public void setLstDelete(List<Long> lstDelete) {
		this.lstDelete = lstDelete;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<EquipSalePlan> getLstInsert() {
		return lstInsert;
	}

	public void setLstInsert(List<EquipSalePlan> lstInsert) {
		this.lstInsert = lstInsert;
	}

	public List<EquipSalePlan> getLstChange() {
		return lstChange;
	}

	public void setLstChange(List<EquipSalePlan> lstChange) {
		this.lstChange = lstChange;
	}

	public Long getEquipSalePlanId() {
		return equipSalePlanId;
	}

	public void setEquipSalePlanId(Long equipSalePlanId) {
		this.equipSalePlanId = equipSalePlanId;
	}

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public KPaging<T> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}

	public LogInfoVO getLogInfoVO() {
		return logInfoVO;
	}

	public void setLogInfoVO(LogInfoVO logInfoVO) {
		this.logInfoVO = logInfoVO;
	}

}

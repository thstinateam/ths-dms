/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.PayReceived;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.PayDetailVO;

/**
 * dieu kien tim kiem pay_received
 * 
 * @author lacnv1
 * @since Mar 19, 2015
 */
public class PayReceivedFilter<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private KPaging<T> paging;

	private List<PayDetailVO> lstPayDetailVOs;
	private List<Long> lstId;
	
	private Staff cashierStaff;
	
	private String payReceivedNumber;
	private Integer receiptType;
	private Integer ownerType;
	private String createDateStr;
	private Integer status;
	private String refNumber;
	private String bankName;
	private BigDecimal fromAmount;
	private BigDecimal toAmount;
	private Date fromDate;
	private Date toDate;
	private Long shopId;
	private Long customerId;
	private PayReceived payReceived;
	private Long payId;
	private String username;

	public KPaging<T> getPaging() {
		return paging;
	}

	public void setPaging(KPaging<T> paging) {
		this.paging = paging;
	}

	public String getPayReceivedNumber() {
		return payReceivedNumber;
	}

	public void setPayReceivedNumber(String payReceivedNumber) {
		this.payReceivedNumber = payReceivedNumber;
	}

	public Integer getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(Integer receiptType) {
		this.receiptType = receiptType;
	}

	public Integer getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(Integer ownerType) {
		this.ownerType = ownerType;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRefNumber() {
		return refNumber;
	}

	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public BigDecimal getFromAmount() {
		return fromAmount;
	}

	public void setFromAmount(BigDecimal fromAmount) {
		this.fromAmount = fromAmount;
	}

	public BigDecimal getToAmount() {
		return toAmount;
	}

	public void setToAmount(BigDecimal toAmount) {
		this.toAmount = toAmount;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public PayReceived getPayReceived() {
		return payReceived;
	}

	public void setPayReceived(PayReceived payReceived) {
		this.payReceived = payReceived;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getPayId() {
		return payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}

	public List<PayDetailVO> getLstPayDetailVOs() {
		return lstPayDetailVOs;
	}

	public void setLstPayDetailVOs(List<PayDetailVO> lstPayDetailVOs) {
		this.lstPayDetailVOs = lstPayDetailVOs;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public Staff getCashierStaff() {
		return cashierStaff;
	}

	public void setCashierStaff(Staff cashierStaff) {
		this.cashierStaff = cashierStaff;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
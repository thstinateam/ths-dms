package ths.dms.core.entities.filter;

import java.io.Serializable;

/**
 * khai báo EquipItemConfigFilter 
 * @author liemtpt
 * @since 07/04/2015
 */
public class EquipItemConfigDtlFilter implements Serializable{

	
	private static final long serialVersionUID = 1L;
	private Long equipItemConfigId;
	private String equipItemCode;
	private Integer status;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getEquipItemCode() {
		return equipItemCode;
	}
	public void setEquipItemCode(String equipItemCode) {
		this.equipItemCode = equipItemCode;
	}
	public Long getEquipItemConfigId() {
		return equipItemConfigId;
	}
	public void setEquipItemConfigId(Long equipItemConfigId) {
		this.equipItemConfigId = equipItemConfigId;
	}
	
	
}

package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;
/**
 * @author vuongmq
 * @date Feb 11,2015
 * **/
public class OrganizationSystemFilter<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private KPaging<T> kPaging;
	
	private List<String> lstCode;
	private List<Long> lstShopId;
	private List<Long> lstCustomerId;
	
	private Long id;
	private Boolean isGetChildShop;

	private String name;
	private String code;
	private String description;
	private String email;
	private String address;
	
	private Integer status;
	private Integer nodeType;
	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}
	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}
	public List<Long> getLstShopId() {
		return lstShopId;
	}
	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public List<String> getLstCode() {
		return lstCode;
	}
	public void setLstCode(List<String> lstCode) {
		this.lstCode = lstCode;
	}
	public Boolean getIsGetChildShop() {
		return isGetChildShop;
	}
	public void setIsGetChildShop(Boolean isGetChildShop) {
		this.isGetChildShop = isGetChildShop;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getNodeType() {
		return nodeType;
	}
	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}
	
}

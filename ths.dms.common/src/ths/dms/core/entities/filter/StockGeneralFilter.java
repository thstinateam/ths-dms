package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;

/**
 * Lop Filter chung cho cac chuc nang lien quan den Stock (Warehouse) de lam tham so
 * 
 * @author hunglm16
 * @return BasicFilter
 * @description Co the bo sung them cac tham so, nhung yeu cau Han che bo sung tham so dang Object
 */
public class StockGeneralFilter<T> implements Serializable {
	
	//TODO dinh nghia doi tuong
	public static final Integer ISPRINT = 2;
	public static final String POVNM = "POVNM";
	public static final String DP = "DP";
	public static final String DC = "DC";
	public static final int INDEXGRID = 20;
	
	private static final long serialVersionUID = 1L;
	private KPaging<T> kPaging;
	private T attribute;
	
	private List<Long> lstId;
	private List<Long> lstExcId;
	
	private Long shopId;
	private Long staffId;
	private Long parentId;
	private Long productId;
	private Long wareHouseId;
	private Long stockTotalId;
	private Long stockTransId;
	private Long stockTransDetailId;
	
	private Integer type;
	private Integer seq;
	private Integer status;
	private Integer isPrint;
	private Integer convfact;
	private Integer quantity;
	private Integer objectType;
	private Integer shopChannel;
	private Integer approved;
	private Integer approvedStep;
	private Integer availableQuantity;
	private Integer statusShop;
	private Integer statusWareHouse;
	
	private BigDecimal price;
	
	private String userName;
	private String shopCode;
	private String description;
	private String productCode;
	private String productName;
	private String wareHouseCode;
	private String wareHouseName;
	private String transType;
	private String staffCode;
	private String stockTransCode;
	private String stockIssueNumberStr;
	private String about;
	private String staffText;
	private String contractNumber;
	private String car;
	private String warehouseInputName;
	private String warehouseOutPutName;
	private String isIdAttrStr;
	
	public Integer getStatusShop() {
		return statusShop;
	}
	public void setStatusShop(Integer statusShop) {
		this.statusShop = statusShop;
	}
	public Integer getStatusWareHouse() {
		return statusWareHouse;
	}
	public void setStatusWareHouse(Integer statusWareHouse) {
		this.statusWareHouse = statusWareHouse;
	}
	public String getIsIdAttrStr() {
		return isIdAttrStr;
	}
	public void setIsIdAttrStr(String isIdAttrStr) {
		this.isIdAttrStr = isIdAttrStr;
	}
	public String getStockIssueNumberStr() {
		return stockIssueNumberStr;
	}
	public void setStockIssueNumberStr(String stockIssueNumberStr) {
		this.stockIssueNumberStr = stockIssueNumberStr;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	public String getStaffText() {
		return staffText;
	}
	public void setStaffText(String staffText) {
		this.staffText = staffText;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getCar() {
		return car;
	}
	public void setCar(String car) {
		this.car = car;
	}
	public String getWarehouseInputName() {
		return warehouseInputName;
	}
	public void setWarehouseInputName(String warehouseInputName) {
		this.warehouseInputName = warehouseInputName;
	}
	public String getWarehouseOutPutName() {
		return warehouseOutPutName;
	}
	public void setWarehouseOutPutName(String warehouseOutPutName) {
		this.warehouseOutPutName = warehouseOutPutName;
	}
	private Date fromDate;
	private Date toDate;
	
	public Integer getIsPrint() {
		return isPrint;
	}
	public void setIsPrint(Integer isPrint) {
		this.isPrint = isPrint;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStockTransCode() {
		return stockTransCode;
	}
	public void setStockTransCode(String stockTransCode) {
		this.stockTransCode = stockTransCode;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public Integer getApprovedStep() {
		return approvedStep;
	}
	public void setApprovedStep(Integer approvedStep) {
		this.approvedStep = approvedStep;
	}
	public Integer getApproved() {
		return approved;
	}
	public void setApproved(Integer approved) {
		this.approved = approved;
	}
	public List<Long> getLstId() {
		return lstId;
	}
	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
	public List<Long> getLstExcId() {
		return lstExcId;
	}
	public void setLstExcId(List<Long> lstExcId) {
		this.lstExcId = lstExcId;
	}
	
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public T getAttribute() {
		return attribute;
	}
	public void setAttribute(T attribute) {
		this.attribute = attribute;
	}
	public Long getWareHouseId() {
		return wareHouseId;
	}
	public void setWareHouseId(Long wareHouseId) {
		this.wareHouseId = wareHouseId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getStockTotalId() {
		return stockTotalId;
	}
	public void setStockTotalId(Long stockTotalId) {
		this.stockTotalId = stockTotalId;
	}
	public Long getStockTransId() {
		return stockTransId;
	}
	public void setStockTransId(Long stockTransId) {
		this.stockTransId = stockTransId;
	}
	public Long getStockTransDetailId() {
		return stockTransDetailId;
	}
	public void setStockTransDetailId(Long stockTransDetailId) {
		this.stockTransDetailId = stockTransDetailId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public Integer getShopChannel() {
		return shopChannel;
	}
	public void setShopChannel(Integer shopChannel) {
		this.shopChannel = shopChannel;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getAvailableQuantity() {
		return availableQuantity;
	}
	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getWareHouseCode() {
		return wareHouseCode;
	}
	public void setWareHouseCode(String wareHouseCode) {
		this.wareHouseCode = wareHouseCode;
	}
	public String getWareHouseName() {
		return wareHouseName;
	}
	public void setWareHouseName(String wareHouseName) {
		this.wareHouseName = wareHouseName;
	}
	
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
}

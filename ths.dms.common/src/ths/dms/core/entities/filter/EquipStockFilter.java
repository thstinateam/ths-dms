package ths.dms.core.entities.filter;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.EquipStockVO;

public class EquipStockFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<EquipStockVO> kPaging;
	private String customerCode;
	private String customerNameOrAdrress;
	private String toShopCode;
	private String shopCode;
	private String shopName;
	private Long shopRoot;
	private Long staffRoot;
	private String strListShopId;
	private String code;
	private String name;
	private String equipStockId;
	private String equipStockCode;
	private Long shopId;
	public KPaging<EquipStockVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<EquipStockVO> kPaging) {
		this.kPaging = kPaging;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerNameOrAdrress() {
		return customerNameOrAdrress;
	}
	public void setCustomerNameOrAdrress(String customerNameOrAdrress) {
		this.customerNameOrAdrress = customerNameOrAdrress;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Long getShopRoot() {
		return shopRoot;
	}
	public void setShopRoot(Long shopRoot) {
		this.shopRoot = shopRoot;
	}
	public Long getStaffRoot() {
		return staffRoot;
	}
	public void setStaffRoot(Long staffRoot) {
		this.staffRoot = staffRoot;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEquipStockId() {
		return equipStockId;
	}
	public void setEquipStockId(String equipStockId) {
		this.equipStockId = equipStockId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getEquipStockCode() {
		return equipStockCode;
	}
	public void setEquipStockCode(String equipStockCode) {
		this.equipStockCode = equipStockCode;
	}
	public String getToShopCode() {
		return toShopCode;
	}
	public void setToShopCode(String toShopCode) {
		this.toShopCode = toShopCode;
	}
	public String getStrListShopId() {
		return strListShopId;
	}
	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}
	
}

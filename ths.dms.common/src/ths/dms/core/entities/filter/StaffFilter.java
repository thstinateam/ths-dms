package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.StaffVO;

public class StaffFilter implements Serializable {

	private static final long serialVersionUID = 1L;
	private KPaging<Staff> kPaging;
	private KPaging<StaffVO> kPagingStockVO;
	private String staffCode;
	private String staffName;
	private List<String> lstStaffOwnerCode;
	private Long shopId;
	private Boolean isGetChildShop;
	private List<String> lstTBHVCode;
	private String saleTypeCode;	
	private String staffTypeCode;
	private String shopCode;
	private ActiveType status;
	private Date startTemp;	
	private String strListUserId;
	private Long staffId;
	private Date checkDate;
	private Integer actionLogObjectType;
	
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getStaffTypeCode() {
		return staffTypeCode;
	}
	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}
	public KPaging<Staff> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<Staff> kPaging) {
		this.kPaging = kPaging;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public List<String> getLstStaffOwnerCode() {
		return lstStaffOwnerCode;
	}
	public void setLstStaffOwnerCode(List<String> lstStaffOwnerCode) {
		this.lstStaffOwnerCode = lstStaffOwnerCode;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Boolean getIsGetChildShop() {
		return isGetChildShop;
	}
	public void setIsGetChildShop(Boolean isGetChildShop) {
		this.isGetChildShop = isGetChildShop;
	}
	public List<String> getLstTBHVCode() {
		return lstTBHVCode;
	}
	public void setLstTBHVCode(List<String> lstTBHVCode) {
		this.lstTBHVCode = lstTBHVCode;
	}
	public String getSaleTypeCode() {
		return saleTypeCode;
	}
	public void setSaleTypeCode(String saleTypeCode) {
		this.saleTypeCode = saleTypeCode;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public KPaging<StaffVO> getkPagingStockVO() {
		return kPagingStockVO;
	}
	public void setkPagingStockVO(KPaging<StaffVO> kPagingStockVO) {
		this.kPagingStockVO = kPagingStockVO;
	}
	public Date getStartTemp() {
		return startTemp;
	}
	public void setStartTemp(Date startTemp) {
		this.startTemp = startTemp;
	}
	public String getStrListUserId() {
		return strListUserId;
	}
	public void setStrListUserId(String strListUserId) {
		this.strListUserId = strListUserId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Date getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}
	public Integer getActionLogObjectType() {
		return actionLogObjectType;
	}
	public void setActionLogObjectType(Integer actionLogObjectType) {
		this.actionLogObjectType = actionLogObjectType;
	}
}

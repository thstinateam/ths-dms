/**
 * 
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.enumtype.ObjectReportType;
import ths.dms.core.entities.enumtype.StaffObjectType;

/**
 * 
 * @author hunglm16
 * @since October 1,214
 */
public class ReportFilter implements Serializable{

	private static final long serialVersionUID = 1L;

	private StaffObjectType roleType;
	private Long shopId;
	private Long reportId;
	
	private String reportCode;
	private String reportName;
	
	private ObjectReportType typeReport;

	private String name;
	private String shopName;
	private String staffName;
	private String saleOrdernumber;
	private Integer isPrint;
	private Long deliveryStaffId;
	private String listSaleOrdernumber;

	private Long staffIdRoot;
	private Long userId;
	private Long roleId;
	private String strListShopId;
	private Date fDate;
	private Date tDate;
	private String categoryId;
	private String subCategoryId;
	private String strListProductId;
	private Long cycleId;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getSaleOrdernumber() {
		return saleOrdernumber;
	}

	public void setSaleOrdernumber(String saleOrdernumber) {
		this.saleOrdernumber = saleOrdernumber;
	}

	public Integer getIsPrint() {
		return isPrint;
	}

	public void setIsPrint(Integer isPrint) {
		this.isPrint = isPrint;
	}

	public Long getDeliveryStaffId() {
		return deliveryStaffId;
	}

	public void setDeliveryStaffId(Long deliveryStaffId) {
		this.deliveryStaffId = deliveryStaffId;
	}

	public String getListSaleOrdernumber() {
		return listSaleOrdernumber;
	}

	public void setListSaleOrdernumber(String listSaleOrdernumber) {
		this.listSaleOrdernumber = listSaleOrdernumber;
	}

	public StaffObjectType getRoleType() {
		return roleType;
	}

	public void setRoleType(StaffObjectType roleType) {
		this.roleType = roleType;
	}


	public Long getShopId() {
		return shopId;
	}


	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	public String getReportCode() {
		return reportCode;
	}


	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}


	public String getReportName() {
		return reportName;
	}


	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public ObjectReportType getTypeReport() {
		return typeReport;
	}

	public void setTypeReport(ObjectReportType typeReport) {
		this.typeReport = typeReport;
	}

	public Long getStaffIdRoot() {
		return staffIdRoot;
	}

	public void setStaffIdRoot(Long staffIdRoot) {
		this.staffIdRoot = staffIdRoot;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public Date getfDate() {
		return fDate;
	}

	public void setfDate(Date fDate) {
		this.fDate = fDate;
	}

	public Date gettDate() {
		return tDate;
	}

	public void settDate(Date tDate) {
		this.tDate = tDate;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public String getStrListProductId() {
		return strListProductId;
	}

	public void setStrListProductId(String strListProductId) {
		this.strListProductId = strListProductId;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	
	
}


package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.KS;
import ths.dms.core.entities.KSCusProductReward;
import ths.dms.core.entities.KSCustomer;
import ths.dms.core.entities.KSCustomerLevel;
import ths.dms.core.entities.KSProduct;
import ths.dms.core.entities.KSShopMap;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.KeyShopVO;

public class KeyShopFilter implements Serializable {

	private static final long serialVersionUID = 1L;
	private KPaging<KS> kPaging;
	private KPaging<KeyShopVO> kPagingVO;
	private KPaging<KSProduct> kPagingP;
	private KPaging<KSCustomer> kPagingC;
	private KPaging<KSCustomerLevel> kPagingCL;
	private KPaging<KSCusProductReward> kPagingCPR;
	private KPaging<KSShopMap> kPagingS;
	private String order;
	private String sort;
	
	private List<Long> lstProductInfoId;
	private List<Long> lstProductInfoChildId;
	private List<Long> lstKsId;
	private List<Long> lstKsCustomerId;
	private List<Long> lstParentShopId;
	
	private Long ksId;
	private Long exceptKSId;
	private Long ksLevelId;
	private Long productId;
	private Long shopId;
	private Long customerId;
	private Long cycleId;
	private Long ksCustomerId;
	private Long saleOrderId;
	private Long staffRootId;
	private Long roleId;
	private Long shopRootId;
	
	private String codeAndName;
	private String code;
	private String name;
	private String ksCode;
	private String ksName;
	private String createUser;
	private String updateUser;
	private String strLstShopId;
	
	private Boolean isAdmin;
	private Boolean isGetChild;
	private Boolean isGetParentShop;
	private Boolean isReturn;
	
	private Date beginDate;
	private Date endDate;
	
	private Integer year;
	private Integer num;
	private Integer status;
	private Integer ksType;
	private Integer result;
	
	public KPaging<KS> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<KS> kPaging) {
		this.kPaging = kPaging;
	}
	public KPaging<KeyShopVO> getkPagingVO() {
		return kPagingVO;
	}
	public void setkPagingVO(KPaging<KeyShopVO> kPagingVO) {
		this.kPagingVO = kPagingVO;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public Long getKsId() {
		return ksId;
	}
	public void setKsId(Long ksId) {
		this.ksId = ksId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getKsCode() {
		return ksCode;
	}
	public void setKsCode(String ksCode) {
		this.ksCode = ksCode;
	}
	public String getKsName() {
		return ksName;
	}
	public void setKsName(String ksName) {
		this.ksName = ksName;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public String getCodeAndName() {
		return codeAndName;
	}
	public void setCodeAndName(String codeAndName) {
		this.codeAndName = codeAndName;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Boolean getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getStrLstShopId() {
		return strLstShopId;
	}
	public void setStrLstShopId(String strLstShopId) {
		this.strLstShopId = strLstShopId;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Long getKsLevelId() {
		return ksLevelId;
	}
	public void setKsLevelId(Long ksLevelId) {
		this.ksLevelId = ksLevelId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public KPaging<KSProduct> getkPagingP() {
		return kPagingP;
	}
	public void setkPagingP(KPaging<KSProduct> kPagingP) {
		this.kPagingP = kPagingP;
	}
	public Integer getKsType() {
		return ksType;
	}
	public void setKsType(Integer ksType) {
		this.ksType = ksType;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getCycleId() {
		return cycleId;
	}
	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}
	public KPaging<KSCustomer> getkPagingC() {
		return kPagingC;
	}
	public void setkPagingC(KPaging<KSCustomer> kPagingC) {
		this.kPagingC = kPagingC;
	}
	public KPaging<KSCustomerLevel> getkPagingCL() {
		return kPagingCL;
	}
	public void setkPagingCL(KPaging<KSCustomerLevel> kPagingCL) {
		this.kPagingCL = kPagingCL;
	}
	public Long getKsCustomerId() {
		return ksCustomerId;
	}
	public void setKsCustomerId(Long ksCustomerId) {
		this.ksCustomerId = ksCustomerId;
	}
	public KPaging<KSCusProductReward> getkPagingCPR() {
		return kPagingCPR;
	}
	public void setkPagingCPR(KPaging<KSCusProductReward> kPagingCPR) {
		this.kPagingCPR = kPagingCPR;
	}
	public KPaging<KSShopMap> getkPagingS() {
		return kPagingS;
	}
	public void setkPagingS(KPaging<KSShopMap> kPagingS) {
		this.kPagingS = kPagingS;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public Boolean getIsGetChild() {
		return isGetChild;
	}
	public void setIsGetChild(Boolean isGetChild) {
		this.isGetChild = isGetChild;
	}
	public List<Long> getLstProductInfoId() {
		return lstProductInfoId;
	}
	public void setLstProductInfoId(List<Long> lstProductInfoId) {
		this.lstProductInfoId = lstProductInfoId;
	}
	public List<Long> getLstProductInfoChildId() {
		return lstProductInfoChildId;
	}
	public void setLstProductInfoChildId(List<Long> lstProductInfoChildId) {
		this.lstProductInfoChildId = lstProductInfoChildId;
	}
	public Long getExceptKSId() {
		return exceptKSId;
	}
	public void setExceptKSId(Long exceptKSId) {
		this.exceptKSId = exceptKSId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getResult() {
		return result;
	}
	public void setResult(Integer result) {
		this.result = result;
	}
	public List<Long> getLstKsId() {
		return lstKsId;
	}
	public void setLstKsId(List<Long> lstKsId) {
		this.lstKsId = lstKsId;
	}
	public Boolean getIsGetParentShop() {
		return isGetParentShop;
	}
	public void setIsGetParentShop(Boolean isGetParentShop) {
		this.isGetParentShop = isGetParentShop;
	}
	public Long getSaleOrderId() {
		return saleOrderId;
	}
	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}
	public Boolean getIsReturn() {
		return isReturn;
	}
	public void setIsReturn(Boolean isReturn) {
		this.isReturn = isReturn;
	}
	public List<Long> getLstKsCustomerId() {
		return lstKsCustomerId;
	}
	public void setLstKsCustomerId(List<Long> lstKsCustomerId) {
		this.lstKsCustomerId = lstKsCustomerId;
	}
	public List<Long> getLstParentShopId() {
		return lstParentShopId;
	}
	public void setLstParentShopId(List<Long> lstParentShopId) {
		this.lstParentShopId = lstParentShopId;
	}
	public Long getStaffRootId() {
		return staffRootId;
	}
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getShopRootId() {
		return shopRootId;
	}
	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}
	
}

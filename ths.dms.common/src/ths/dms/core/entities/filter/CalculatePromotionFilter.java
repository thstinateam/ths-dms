/**
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.vo.SaleOrderDetailVO;
import ths.dms.core.entities.vo.SaleOrderVO;

public class CalculatePromotionFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long shopId;
	private Long saleOrderId;
	private Long staffId;
	private long customerId;
	private long customerTypeId;
	private Integer shopChannel;
	private Date orderDate;
	private Boolean isDividualWarehouse;
	private SaleOrderVO orderVO;
	private Map<Long, List<SaleOrderLot>> mapSaleOrderLot;
	private List<SaleOrderDetailVO> lstSalesOrderDetail;
	
	public Map<Long, List<SaleOrderLot>> getMapSaleOrderLot() {
		return mapSaleOrderLot;
	}
	public void setMapSaleOrderLot(Map<Long, List<SaleOrderLot>> mapSaleOrderLot) {
		this.mapSaleOrderLot = mapSaleOrderLot;
	}
	public List<SaleOrderDetailVO> getLstSalesOrderDetail() {
		return lstSalesOrderDetail;
	}
	public void setLstSalesOrderDetail(List<SaleOrderDetailVO> lstSalesOrderDetail) {
		this.lstSalesOrderDetail = lstSalesOrderDetail;
	}
	public long getShopId() {
		return shopId;
	}
	public void setShopId(long shopId) {
		this.shopId = shopId;
	}
	public Integer getShopChannel() {
		return shopChannel;
	}
	public void setShopChannel(Integer shopChannel) {
		this.shopChannel = shopChannel;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public long getCustomerTypeId() {
		return customerTypeId;
	}
	public void setCustomerTypeId(long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Long getSaleOrderId() {
		return saleOrderId;
	}
	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}
	public SaleOrderVO getOrderVO() {
		return orderVO;
	}
	public void setOrderVO(SaleOrderVO orderVO) {
		this.orderVO = orderVO;
	}
	public Boolean getIsDividualWarehouse() {
		return isDividualWarehouse;
	}
	public void setIsDividualWarehouse(Boolean isDividualWarehouse) {
		this.isDividualWarehouse = isDividualWarehouse;
	}

}

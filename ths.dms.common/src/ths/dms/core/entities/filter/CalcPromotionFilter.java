/**
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.vo.SaleOrderDetailVO;
import ths.dms.core.entities.vo.SaleOrderVO;

public class CalcPromotionFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal amtOrder;
	private List<PromotionProgram> lstPromotionProEntity;
	private SortedMap<Long, SaleOrderDetailVO> sortListProductSales;
	private List<SaleOrderDetailVO> listProductPromotionsale;
	private Long keyList;
	private SortedMap<Long, List<SaleOrderDetailVO>> sortListOutPut;
	private Long shopId;
	private int promotionType;
	private SaleOrderVO orderVO;
	private Long customerId;
	private Long staffId;
	private Date orderDate;
	private Map<Long, List<Long>> mapCTKMProduct;
	private Boolean isDividualWarehouse;
	
	public BigDecimal getAmtOrder() {
		return amtOrder;
	}
	public void setAmtOrder(BigDecimal amtOrder) {
		this.amtOrder = amtOrder;
	}
	public List<PromotionProgram> getLstPromotionProEntity() {
		return lstPromotionProEntity;
	}
	public void setLstPromotionProEntity(List<PromotionProgram> lstPromotionProEntity) {
		this.lstPromotionProEntity = lstPromotionProEntity;
	}
	public SortedMap<Long, SaleOrderDetailVO> getSortListProductSales() {
		return sortListProductSales;
	}
	public void setSortListProductSales(SortedMap<Long, SaleOrderDetailVO> sortListProductSales) {
		this.sortListProductSales = sortListProductSales;
	}
	public List<SaleOrderDetailVO> getListProductPromotionsale() {
		return listProductPromotionsale;
	}
	public void setListProductPromotionsale(List<SaleOrderDetailVO> listProductPromotionsale) {
		this.listProductPromotionsale = listProductPromotionsale;
	}
	public Long getKeyList() {
		return keyList;
	}
	public void setKeyList(Long keyList) {
		this.keyList = keyList;
	}
	public SortedMap<Long, List<SaleOrderDetailVO>> getSortListOutPut() {
		return sortListOutPut;
	}
	public void setSortListOutPut(SortedMap<Long, List<SaleOrderDetailVO>> sortListOutPut) {
		this.sortListOutPut = sortListOutPut;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public int getPromotionType() {
		return promotionType;
	}
	public void setPromotionType(int promotionType) {
		this.promotionType = promotionType;
	}
	public SaleOrderVO getOrderVO() {
		return orderVO;
	}
	public void setOrderVO(SaleOrderVO orderVO) {
		this.orderVO = orderVO;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Map<Long, List<Long>> getMapCTKMProduct() {
		return mapCTKMProduct;
	}
	public void setMapCTKMProduct(Map<Long, List<Long>> mapCTKMProduct) {
		this.mapCTKMProduct = mapCTKMProduct;
	}
	public Boolean getIsDividualWarehouse() {
		return isDividualWarehouse;
	}
	public void setIsDividualWarehouse(Boolean isDividualWarehouse) {
		this.isDividualWarehouse = isDividualWarehouse;
	}

}

/**
 * filter de lay danh sach sale_order_promotion
 * @author tuannd20
 * @version 1.0
 * @since 11/10/2014
 */
package ths.dms.core.entities.filter;

import java.io.Serializable;

public class SaleOrderPromotionFilter implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long saleOrderId;

	public Long getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}
}

package ths.dms.core.entities.filter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Media;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.EquipmentEvictionVO;

public class EquipmentEvictionFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<EquipmentEvictionVO> kPaging;
	private String customerCode;
	private String customerName;
	private String address;
	private String euqipCode;
	private String seri;
	private Integer statusForm;
	private Integer statusAction;
	private Date fromDate;
	private Date toDate;	
	
	/**Mã biên bản đề nghị thu hồi thiết bị*/
	private String equipSugEvictionCode;
	private List<Long> lstShopId = new ArrayList<Long>();
	public KPaging<EquipmentEvictionVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<EquipmentEvictionVO> kPaging) {
		this.kPaging = kPaging;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEuqipCode() {
		return euqipCode;
	}
	public void setEuqipCode(String euqipCode) {
		this.euqipCode = euqipCode;
	}
	public String getSeri() {
		return seri;
	}
	public void setSeri(String seri) {
		this.seri = seri;
	}
	public Integer getStatusForm() {
		return statusForm;
	}
	public void setStatusForm(Integer statusForm) {
		this.statusForm = statusForm;
	}
	public Integer getStatusAction() {
		return statusAction;
	}
	public void setStatusAction(Integer statusAction) {
		this.statusAction = statusAction;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public List<Long> getLstShopId() {
		return lstShopId;
	}
	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}
	public String getEquipSugEvictionCode() {
		return equipSugEvictionCode;
	}
	public void setEquipSugEvictionCode(String equipSugEvictionCode) {
		this.equipSugEvictionCode = equipSugEvictionCode;
	}
	
}

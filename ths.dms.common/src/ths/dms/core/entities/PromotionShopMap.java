package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PROMOTION_SHOP_MAP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_PROMOTION_SHOP_MAP", sequenceName = "PROMOTION_SHOP_MAP_SEQ", allocationSize = 1)
public class PromotionShopMap implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PROMOTION_SHOP_MAP")
	@Column(name = "PROMOTION_SHOP_MAP_ID")
	private Long id;
	
	//id ctkm
	@ManyToOne(targetEntity = PromotionProgram.class)
	@JoinColumn(name = "PROMOTION_PROGRAM_ID", referencedColumnName = "PROMOTION_PROGRAM_ID")
	private PromotionProgram promotionProgram;
	
	//id npp tham gia km
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	//so lan co the nhan km
	@Basic
	@Column(name = "QUANTITY_MAX", length = 22)
	private Integer quantityMax;
	
	//so lan da nhan km
	@Basic
	@Column(name = "QUANTITY_RECEIVED", length = 22)
	private Integer quantityReceived;
	
	//so lan co the nhan km
	@Basic
	@Column(name = "AMOUNT_MAX", length = 22)
	private BigDecimal amountMax;
	
	//so lan da nhan km
	@Basic
	@Column(name = "AMOUNT_RECEIVED", length = 22)
	private BigDecimal amountReceived;
	
	//so lan co the nhan km
	@Basic
	@Column(name = "NUM_MAX", length = 22)
	private BigDecimal numMax;
	
	//so lan da nhan km
	@Basic
	@Column(name = "NUM_RECEIVED", length = 22)
	private BigDecimal numReceived;
	
	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Basic
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	//so xuat ap dung cho KH voi TH CTKM chi den NPP
	@Basic
	@Column(name = "QUANTITY_CUSTOMER", length = 22)
	private Integer quantityCustomer;
	
	@Basic
	@Column(name = "FROM_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromDate;
	
	@Basic
	@Column(name = "TO_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date toDate;
	
	@Basic
	@Column(name = "IS_QUANTITY_MAX_EDIT")
	private Integer isQuantityMaxEdit;
	
	//so suat tong da duyet + chua duyet
	@Basic
	@Column(name = "QUANTITY_RECEIVED_TOTAL", length = 20)
	private Integer quantityReceivedTotal;
	
	//so tien tong da duyet + chua duyet
	@Basic
	@Column(name = "AMOUNT_RECEIVED_TOTAL", length = 26)
	private BigDecimal amountReceivedTotal;
	
	//so luong tong da duyet + chua duyet
	@Basic
	@Column(name = "NUM_RECEIVED_TOTAL", length = 20)
	private BigDecimal numReceivedTotal;
	
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public PromotionProgram getPromotionProgram() {
		return promotionProgram;
	}

	public void setPromotionProgram(PromotionProgram promotionProgram) {
		this.promotionProgram = promotionProgram;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantityCustomer() {
		return quantityCustomer;
	}

	public void setQuantityCustomer(Integer quantityCustomer) {
		this.quantityCustomer = quantityCustomer;
	}

	public Integer getQuantityMax() {
		return quantityMax;
	}

	public void setQuantityMax(Integer quantityMax) {
		this.quantityMax = quantityMax;
	}

	public Integer getQuantityReceived() {
		if (quantityReceived == null) {
			quantityReceived = 0;
		}
		return quantityReceived;
	}
	
	public Integer getQuantityReceived1() {
		return quantityReceived;
	}

	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getIsQuantityMaxEdit() {
		return isQuantityMaxEdit;
	}

	public void setIsQuantityMaxEdit(Integer isQuantityMaxEdit) {
		this.isQuantityMaxEdit = isQuantityMaxEdit;
	}

	public BigDecimal getAmountMax() {
		return amountMax;
	}

	public void setAmountMax(BigDecimal amountMax) {
		this.amountMax = amountMax;
	}

	public BigDecimal getAmountReceived() {
		return amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}

	public BigDecimal getNumMax() {
		return numMax;
	}

	public void setNumMax(BigDecimal numMax) {
		this.numMax = numMax;
	}

	public BigDecimal getNumReceived() {
		return numReceived;
	}

	public void setNumReceived(BigDecimal numReceived) {
		this.numReceived = numReceived;
	}

	public Integer getQuantityReceivedTotal() {
		return quantityReceivedTotal;
	}

	public void setQuantityReceivedTotal(Integer quantityReceivedTotal) {
		this.quantityReceivedTotal = quantityReceivedTotal;
	}

	public BigDecimal getAmountReceivedTotal() {
		return amountReceivedTotal;
	}

	public void setAmountReceivedTotal(BigDecimal amountReceivedTotal) {
		this.amountReceivedTotal = amountReceivedTotal;
	}

	public BigDecimal getNumReceivedTotal() {
		return numReceivedTotal;
	}

	public void setNumReceivedTotal(BigDecimal numReceivedTotal) {
		this.numReceivedTotal = numReceivedTotal;
	}

	public PromotionShopMap clone() {
		PromotionShopMap temp = new PromotionShopMap();
		temp.setCreateDate(createDate);
		temp.setCreateUser(createUser);
		temp.setPromotionProgram(promotionProgram);
		temp.setId(id);
		temp.setQuantityCustomer(quantityCustomer);
		temp.setQuantityMax(quantityMax);
		temp.setQuantityReceived(quantityReceived);
		temp.setAmountMax(amountMax);
		temp.setAmountReceived(amountReceived);
		temp.setNumMax(numMax);
		temp.setNumReceived(numReceived);
		temp.setShop(shop);
		temp.setStatus(status);
		temp.setUpdateDate(updateDate);
		temp.setUpdateUser(updateUser);
		temp.setQuantityReceivedTotal(quantityReceivedTotal);
		temp.setAmountReceivedTotal(amountReceivedTotal);
		temp.setNumReceivedTotal(numReceivedTotal);
		return temp;
	}

}
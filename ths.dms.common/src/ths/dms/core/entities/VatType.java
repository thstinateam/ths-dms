package ths.dms.core.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "VAT_TYPE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "VAT_TYPE_SEQ", allocationSize = 1)
public class VatType implements Serializable {

	private static final long serialVersionUID = 1L;
	//ghi chu
	@Basic
	@Column(name = "DESCRIPTION", length = 3000)
	private String description;

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	//ti le thue
	@Basic
	@Column(name = "VALUE", length = 22)
	private Float value;

	//ma loai thue
	@Basic
	@Column(name = "VAT_TYPE_CODE", length = 20)
	private String vatTypeCode;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "VAT_TYPE_ID")
	private Long id;

	//ten loai thue
	@Basic
	@Column(name = "VAT_TYPE_NAME", length = 300)
	private String vatTypeName;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Float getValue() {
		return value == null ? 0f : value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public String getVatTypeCode() {
		return vatTypeCode;
	}

	public void setVatTypeCode(String vatTypeCode) {
		this.vatTypeCode = vatTypeCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVatTypeName() {
		return vatTypeName;
	}

	public void setVatTypeName(String vatTypeName) {
		this.vatTypeName = vatTypeName;
	}

	
}
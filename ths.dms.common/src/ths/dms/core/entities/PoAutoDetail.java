package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PO_AUTO_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PO_AUTO_DETAIL_SEQ", allocationSize = 1)
public class PoAutoDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	//tong tien
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//quy cach
	@Basic
	@Column(name = "CONVFACT", length = 22)
	private Integer convfact;

	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//dinh muc ke hoach
	@Basic
	@Column(name = "DAY_PLAN", length = 22)
	private Integer dayPlan;

	//ngay du tru ke hoach
	@Basic
	@Column(name = "DAY_RESERVE_PLAN", length = 22)
	private Double dayReservePlan;

	//ngay du tru thuc te
	@Basic
	@Column(name = "DAY_RESERVE_REAL", length = 22)
	private BigDecimal dayReserveReal;

	//xuat
	@Basic
	@Column(name = "EXPORT", length = 22)
	private Integer export;

	//nhap
	@Basic
	@Column(name = "IMPORT", length = 22)
	private Integer importQuantity;

	//thoi gian giao hang
	@Basic
	@Column(name = "LEAD", length = 22)
	private Integer lead;

	//luy ke tieu thu thang
	@Basic
	@Column(name = "MONTH_CUMULATE", length = 22)
	private Integer monthCumulate;

	//ke hoach tieu thu thang
	@Basic
	@Column(name = "MONTH_PLAN", length = 22)
	private Integer monthPlan;

	//khoang cach ngay giao hang
	@Basic
	@Column(name = "NEXT", length = 22)
	private Integer next;

	//ton dau ky
	@Basic
	@Column(name = "OPEN_STOCK", length = 22)
	private Integer openStock;

	//ngay tao don hang po auto
	@Basic
	@Column(name = "PO_AUTO_DATE", length = 7)
	private Date poAutoDate;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PO_AUTO_DETAIL_ID")
	private Long id;

	//id po auto
	@ManyToOne(targetEntity = PoAuto.class)
	@JoinColumn(name = "PO_AUTO_ID", referencedColumnName = "PO_AUTO_ID")
	private PoAuto poAuto;

	//gia
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal priceValue;

	//id gia
	@ManyToOne(targetEntity = Price.class)
	@JoinColumn(name = "PRICE_ID", referencedColumnName = "PRICE_ID")
	private Price price;

	//id san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//so luong thung mua
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;

	//yeu cau ton kho
	@Basic
	@Column(name = "REQUIMENT_STOCK", length = 22)
	private Integer requimentStock;

	//ngay ton kho an toan min
	@Basic
	@Column(name = "SAFETY_STOCK_MIN", length = 22)
	private Integer safetyStockMin;

	//ton kho hien tai
	@Basic
	@Column(name = "STOCK", length = 22)
	private Integer stock;

	//so hang dang van chuyen
	@Basic
	@Column(name = "STOCK_PO_CONFIRM", length = 22)
	private Integer stockPoConfirm;

	//so hang cho van chuyen
	@Basic
	@Column(name = "STOCK_PO_DVKH", length = 22)
	private Integer stockPoDvkh;

	//canh bao
	@Basic
	@Column(name = "WARNING", length = 40)
	private String warning;
	
	//so luong nhan (PO DVKH)
//	@Basic
//	@Column(name = "QUANTITY_RECEIVED", length = 20)
//	private Integer quantityReveived;
	
//	//gia tri nhan (PO DVKH)
//	@Basic
//	@Column(name = "AMOUNT_RECEIVED", length = 20)
//	private BigDecimal amountReveived;
	

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getConvfact() {
		if (convfact == null) {
			convfact = 0;
		}
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getDayPlan() {
		if (dayPlan == null) {
			dayPlan = 0;
		}
		return dayPlan;
	}

	public void setDayPlan(Integer dayPlan) {
		this.dayPlan = dayPlan;
	}

	public Double getDayReservePlan() {
		return dayReservePlan;
	}

	public void setDayReservePlan(Double dayReservePlan) {
		this.dayReservePlan = dayReservePlan;
	}

	public BigDecimal getDayReserveReal() {
		return dayReserveReal == null ? BigDecimal.valueOf(0) : dayReserveReal;
	}

	public void setDayReserveReal(BigDecimal dayReserveReal) {
		this.dayReserveReal = dayReserveReal;
	}

	public Integer getExport() {
		if (export == null) {
			export = 0;
		}
		return export;
	}

	public void setExport(Integer export) {
		this.export = export;
	}

	public Integer getImportQuantity() {
		if (importQuantity == null) {
			importQuantity = 0;
		}
		return importQuantity;
	}

	public void setImportQuantity(Integer importQuantity) {
		this.importQuantity = importQuantity;
	}

	public Integer getLead() {
		if (lead == null) {
			lead = 0;
		}
		return lead;
	}

	public void setLead(Integer lead) {
		this.lead = lead;
	}

	public Integer getMonthCumulate() {
		if (monthCumulate == null) {
			monthCumulate = 0;
		}
		return monthCumulate;
	}

	public void setMonthCumulate(Integer monthCumulate) {
		this.monthCumulate = monthCumulate;
	}

	public Integer getMonthPlan() {
		if (monthPlan == null) {
			monthPlan = 0;
		}
		return monthPlan;
	}

	public void setMonthPlan(Integer monthPlan) {
		this.monthPlan = monthPlan;
	}

	public Integer getNext() {
		if (next == null) {
			next = 0;
		}
		return next;
	}

	public void setNext(Integer next) {
		this.next = next;
	}

	public Integer getOpenStock() {
		if (openStock == null) {
			openStock = 0;
		}
		return openStock;
	}

	public void setOpenStock(Integer openStock) {
		this.openStock = openStock;
	}

	public Date getPoAutoDate() {
		return poAutoDate;
	}

	public void setPoAutoDate(Date poAutoDate) {
		this.poAutoDate = poAutoDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PoAuto getPoAuto() {
		return poAuto;
	}

	public void setPoAuto(PoAuto poAuto) {
		this.poAuto = poAuto;
	}

	public BigDecimal getPriceValue() {
		return priceValue == null ? BigDecimal.valueOf(0) : priceValue;
	}

	public void setPriceValue(BigDecimal priceValue) {
		this.priceValue = priceValue;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getRequimentStock() {
		if (requimentStock == null) {
			requimentStock = 0;
		}
		return requimentStock;
	}

	public void setRequimentStock(Integer requimentStock) {
		this.requimentStock = requimentStock;
	}

	public Integer getSafetyStockMin() {
		if (safetyStockMin == null) {
			safetyStockMin = 0;
		}
		return safetyStockMin;
	}

	public void setSafetyStockMin(Integer safetyStockMin) {
		this.safetyStockMin = safetyStockMin;
	}

	public Integer getStock() {
		if (stock == null) {
			stock = 0;
		}
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Integer getStockPoConfirm() {
		if (stockPoConfirm == null) {
			stockPoConfirm = 0;
		}
		return stockPoConfirm;
	}

	public void setStockPoConfirm(Integer stockPoConfirm) {
		this.stockPoConfirm = stockPoConfirm;
	}

	public Integer getStockPoDvkh() {
		if (stockPoDvkh == null) {
			stockPoDvkh = 0;
		}
		return stockPoDvkh;
	}

	public void setStockPoDvkh(Integer stockPoDvkh) {
		this.stockPoDvkh = stockPoDvkh;
	}

	public String getWarning() {
		return warning;
	}

	public void setWarning(String warning) {
		this.warning = warning;
	}

//	public Integer getQuantityReveived() {
//		return quantityReveived;
//	}
//
//	public void setQuantityReveived(Integer quantityReveived) {
//		this.quantityReveived = quantityReveived;
//	}

//	public BigDecimal getAmountReveived() {
//		return amountReveived;
//	}
//
//	public void setAmountReveived(BigDecimal amountReveived) {
//		this.amountReveived = amountReveived;
//	}
}
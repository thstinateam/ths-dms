package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.StockTransLotOwnerType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "STOCK_TRANS_LOT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "STOCK_TRANS_LOT_SEQ", allocationSize = 1)
public class StockTransLot implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//So lo
	@Basic
	@Column(name = "LOT", length = 6)
	private String lot;

	//ID Product
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//So luong
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;

	@Basic
	@Column(name = "STOCK_TRANS_DATE", length = 7)
	private Date stockTransDate;

	//Stock_trans_detail_id
	@ManyToOne(targetEntity = StockTransDetail.class)
	@JoinColumn(name = "STOCK_TRANS_DETAIL_ID", referencedColumnName = "STOCK_TRANS_DETAIL_ID")
	private StockTransDetail stockTransDetail;

	//Stock_trans_id
	@ManyToOne(targetEntity = StockTrans.class)
	@JoinColumn(name = "STOCK_TRANS_ID", referencedColumnName = "STOCK_TRANS_ID")
	private StockTrans stockTrans;

	//Id
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "STOCK_TRANS_LOT_ID")
	private Long id;
	
	//Id doi tuong xuat
	@Basic
	@Column(name = "FROM_OWNER_ID", length = 50)
	private Long fromOwnerId;

	//1: NPP; 2: nhan vien; 3 xuat nhap dieu chinh
	@Basic
	@Column(name = "FROM_OWNER_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.StockTransLotOwnerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private StockTransLotOwnerType fromOwnerType;
	
	//Id doi tuong nhap
	@Basic
	@Column(name = "TO_OWNER_ID", length = 50)
	private Long toOwnerId;

	//1: NPP; 2: nhan vien; 3 xuat nhap dieu chinh
	@Basic
	@Column(name = "TO_OWNER_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.StockTransLotOwnerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private StockTransLotOwnerType toOwnerType;
	
	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getStockTransDate() {
		return stockTransDate;
	}

	public void setStockTransDate(Date stockTransDate) {
		this.stockTransDate = stockTransDate;
	}

	public StockTransDetail getStockTransDetail() {
		return stockTransDetail;
	}

	public void setStockTransDetail(StockTransDetail stockTransDetail) {
		this.stockTransDetail = stockTransDetail;
	}

	public StockTrans getStockTrans() {
		return stockTrans;
	}

	public void setStockTrans(StockTrans stockTrans) {
		this.stockTrans = stockTrans;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFromOwnerId() {
		return fromOwnerId;
	}

	public void setFromOwnerId(Long fromOwnerId) {
		this.fromOwnerId = fromOwnerId;
	}

	public StockTransLotOwnerType getFromOwnerType() {
		return fromOwnerType;
	}

	public void setFromOwnerType(StockTransLotOwnerType fromOwnerType) {
		this.fromOwnerType = fromOwnerType;
	}

	public Long getToOwnerId() {
		return toOwnerId;
	}

	public void setToOwnerId(Long toOwnerId) {
		this.toOwnerId = toOwnerId;
	}

	public StockTransLotOwnerType getToOwnerType() {
		return toOwnerType;
	}

	public void setToOwnerType(StockTransLotOwnerType toOwnerType) {
		this.toOwnerType = toOwnerType;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	
}
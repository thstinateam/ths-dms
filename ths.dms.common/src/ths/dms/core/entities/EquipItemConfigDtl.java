package ths.dms.core.entities;

/**
 * The entity equip item config
 * 
 * @author hunglm16
 * @since May 05,2015
 */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

@Entity
@Table(name = "EQUIP_ITEM_CONFIG_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_ITEM_CONFIG_DTL_SEQ", allocationSize = 1)
public class EquipItemConfigDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	//Id tang tu dong
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_ITEM_CONFIG_DTL_ID")
	private Long id;

	//Id danh muc hang muc
	@ManyToOne(targetEntity = EquipItemConfig.class)
	@JoinColumn(name = "EQUIP_ITEM_CONFIG_ID", referencedColumnName = "EQUIP_ITEM_CONFIG_ID")
	private EquipItemConfig equipItemConfig;

	//Id hang muc
	@ManyToOne(targetEntity = EquipItem.class)
	@JoinColumn(name = "EQUIP_ITEM_ID", referencedColumnName = "EQUIP_ITEM_ID")
	private EquipItem equipItem;

	//Don gia vat tu tu
	@Basic
	@Column(name = "FROM_MATERIAL_PRICE", length = 17)
	private BigDecimal fromMaterialPrice;

	//Don gia nhan cong tu
	@Basic
	@Column(name = "FROM_WORKER_PRICE", length = 17)
	private BigDecimal fromWorkerPrice;

	//Trang thai: 0: off; 1: on
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;

	//Don gia vat tu den
	@Basic
	@Column(name = "TO_MATERIAL_PRICE", length = 17)
	private BigDecimal toMaterialPrice;

	//Don gia nhan cong den
	@Basic
	@Column(name = "TO_WORKER_PRICE", length = 17)
	private BigDecimal toWorkerPrice;

	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//Ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipItemConfig getEquipItemConfig() {
		return equipItemConfig;
	}

	public void setEquipItemConfig(EquipItemConfig equipItemConfig) {
		this.equipItemConfig = equipItemConfig;
	}

	public EquipItem getEquipItem() {
		return equipItem;
	}

	public void setEquipItem(EquipItem equipItem) {
		this.equipItem = equipItem;
	}

	public BigDecimal getFromMaterialPrice() {
		return fromMaterialPrice;
	}

	public void setFromMaterialPrice(BigDecimal fromMaterialPrice) {
		this.fromMaterialPrice = fromMaterialPrice;
	}

	public BigDecimal getFromWorkerPrice() {
		return fromWorkerPrice;
	}

	public void setFromWorkerPrice(BigDecimal fromWorkerPrice) {
		this.fromWorkerPrice = fromWorkerPrice;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public BigDecimal getToMaterialPrice() {
		return toMaterialPrice;
	}

	public void setToMaterialPrice(BigDecimal toMaterialPrice) {
		this.toMaterialPrice = toMaterialPrice;
	}

	public BigDecimal getToWorkerPrice() {
		return toWorkerPrice;
	}

	public void setToWorkerPrice(BigDecimal toWorkerPrice) {
		this.toWorkerPrice = toWorkerPrice;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
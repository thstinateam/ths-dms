package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_SALE_PLAN")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_SALE_PLAN_SEQ", allocationSize = 1)
public class EquipSalePlan implements Serializable {

	private static final long serialVersionUID = 1L;

	//Doanh s? trong th?i k?
	@Basic
	@Column(name = "AMOUNT")
	private BigDecimal amount;

	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//Lo?i kh�ch h�ng: 0: ch�a x�c �?nh; 1: th�nh th?; 2: n�ng th�n
	@Basic
	@Column(name = "CUSTOMER_TYPE_ID", length = 20)
	private Long customerTypeId;

	//Nh�m thi?t b?. FK: EQUIP_GROUP
	@ManyToOne(targetEntity = EquipGroup.class)
	@JoinColumn(name = "EQUIP_GROUP_ID", referencedColumnName = "EQUIP_GROUP_ID")
	private EquipGroup equipGroup;

	//Kh�a ch�nh
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_SALE_PLAN_ID")
	private Long id;

	//T? th�ng
	@Basic
	@Column(name = "FROM_MONTH", length = 22)
	private Integer fromMonth;

	//�?n th�ng
	@Basic
	@Column(name = "TO_MONTH", length = 22)
	private Integer toMonth;

	//Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	@Transient
	private Integer indexRow;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(Long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public EquipGroup getEquipGroup() {
		return equipGroup;
	}

	public void setEquipGroup(EquipGroup equipGroup) {
		this.equipGroup = equipGroup;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	public Integer getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(Integer fromMonth) {
		this.fromMonth = fromMonth;
	}

	public Integer getToMonth() {
		return toMonth;
	}

	public void setToMonth(Integer toMonth) {
		this.toMonth = toMonth;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getIndexRow() {
		return indexRow;
	}

	public void setIndexRow(Integer indexRow) {
		this.indexRow = indexRow;
	}

}
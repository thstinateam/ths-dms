package ths.dms.core.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "SALE_DAY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SALE_DAY_SEQ", allocationSize = 1)
public class SaleDays implements Serializable {

	private static final long serialVersionUID = 1L;
	//ngay tao
	/*@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 40)
	private String createUser;
*/
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SALE_DAY_ID")
	private Long id;

	//trang thai
	/*@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
*/
	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	@Basic
	@Column(name = "YEAR", length = 40)
	private Integer year;
	
	//thang 1
	@Basic
	@Column(name = "T1", length = 22)
	private Integer t1;

	//thang 10
	@Basic
	@Column(name = "T10", length = 22)
	private Integer t10;

	//thang 11
	@Basic
	@Column(name = "T11", length = 22)
	private Integer t11;

	//thang 12
	@Basic
	@Column(name = "T12", length = 22)
	private Integer t12;

	//thang 2
	@Basic
	@Column(name = "T2", length = 22)
	private Integer t2;

	//thang 3
	@Basic
	@Column(name = "T3", length = 22)
	private Integer t3;

	//thang 4
	@Basic
	@Column(name = "T4", length = 22)
	private Integer t4;

	//thang 5
	@Basic
	@Column(name = "T5", length = 22)
	private Integer t5;

	//thang 6
	@Basic
	@Column(name = "T6", length = 22)
	private Integer t6;

	//thang 7
	@Basic
	@Column(name = "T7", length = 22)
	private Integer t7;

	//thang 8
	@Basic
	@Column(name = "T8", length = 22)
	private Integer t8;

	//thang 9
	@Basic
	@Column(name = "T9", length = 22)
	private Integer t9;

	//nam
	/*@Basic
	@Column(name = "YEAR", length = 10)
	private String year;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}*/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}*/

	public Integer getT1() {
		return t1;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public void setT1(Integer t1) {
		this.t1 = t1;
	}

	public Integer getT10() {
		return t10;
	}

	public void setT10(Integer t10) {
		this.t10 = t10;
	}

	public Integer getT11() {
		return t11;
	}

	public void setT11(Integer t11) {
		this.t11 = t11;
	}

	public Integer getT12() {
		return t12;
	}

	public void setT12(Integer t12) {
		this.t12 = t12;
	}

	public Integer getT2() {
		return t2;
	}

	public void setT2(Integer t2) {
		this.t2 = t2;
	}

	public Integer getT3() {
		return t3;
	}

	public void setT3(Integer t3) {
		this.t3 = t3;
	}

	public Integer getT4() {
		return t4;
	}

	public void setT4(Integer t4) {
		this.t4 = t4;
	}

	public Integer getT5() {
		return t5;
	}

	public void setT5(Integer t5) {
		this.t5 = t5;
	}

	public Integer getT6() {
		return t6;
	}

	public void setT6(Integer t6) {
		this.t6 = t6;
	}

	public Integer getT7() {
		return t7;
	}

	public void setT7(Integer t7) {
		this.t7 = t7;
	}

	public Integer getT8() {
		return t8;
	}

	public void setT8(Integer t8) {
		this.t8 = t8;
	}

	public Integer getT9() {
		return t9;
	}

	public void setT9(Integer t9) {
		this.t9 = t9;
	}
/*
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}*/
}
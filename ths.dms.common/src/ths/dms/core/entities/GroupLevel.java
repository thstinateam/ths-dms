package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "GROUP_LEVEL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_GROUP_LEVEL", sequenceName = "GROUP_LEVEL_SEQ", allocationSize = 1)
public class GroupLevel implements Serializable {
private static final long serialVersionUID = 1L;
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GROUP_LEVEL")
	@Column(name = "GROUP_LEVEL_ID")
	private Long id;

	@ManyToOne(targetEntity = ProductGroup.class)
	@JoinColumn(name = "PRODUCT_GROUP_ID", referencedColumnName = "PRODUCT_GROUP_ID")
	private ProductGroup productGroup;

	//so lan co the nhan km
	@Basic
	@Column(name = "GROUP_TEXT", length = 100)
	private String groupText;

	@Basic
	@Column(name = "ORDER_NUMBER")
	private Integer order;

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	@Basic
	@Column(name = "MIN_QUANTITY")
	private Integer minQuantity;

	@Basic
	@Column(name = "MAX_QUANTITY")
	private Integer maxQuantity;

	@Basic
	@Column(name = "MIN_AMOUNT")
	private BigDecimal minAmount;

	@Basic
	@Column(name = "MAX_AMOUNT")
	private BigDecimal maxAmount;

	@Basic
	@Column(name = "PROMOTION_PERCENT")
	private Float percent;

	@Basic
	@Column(name = "HAS_PRODUCT")
	private Integer hasProduct;

	@ManyToOne(targetEntity = GroupLevel.class)
	@JoinColumn(name = "PARENT_GROUP_LEVEL_ID", referencedColumnName = "GROUP_LEVEL_ID")
	private GroupLevel parentLevel;

	@Basic
	@Column(name = "GROUP_LEVEL_CODE")
	private String levelCode;

	@Basic
	@Column(name = "PROMOTION_PROGRAM_ID")
	private Long promotionProgramId;

	@Override
	public GroupLevel clone() throws CloneNotSupportedException {
		GroupLevel cloneObj = new GroupLevel();
		cloneObj.setGroupText(this.getGroupText());
		cloneObj.setHasProduct(this.getHasProduct());
		cloneObj.setMaxAmount(this.getMaxAmount());
		cloneObj.setMaxQuantity(this.getMaxQuantity());
		cloneObj.setMinAmount(this.getMinAmount());
		cloneObj.setMinQuantity(this.getMinQuantity());
		cloneObj.setOrder(this.getOrder());
		cloneObj.setPercent(this.getPercent());
		cloneObj.setProductGroup(this.getProductGroup());
		cloneObj.setPromotionProgramId(this.promotionProgramId);
		cloneObj.setStatus(this.getStatus());
		return cloneObj;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductGroup getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(ProductGroup productGroup) {
		this.productGroup = productGroup;
	}

	public String getGroupText() {
		return groupText;
	}

	public void setGroupText(String groupText) {
		this.groupText = groupText;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Integer getMinQuantity() {
		if (minQuantity == null)
			return 0;
		else
			return minQuantity;
	}

	public Integer getRawMinQuantity() {
		return minQuantity;
	}

	public void setMinQuantity(Integer minQuantity) {
		this.minQuantity = minQuantity;
	}

	public Integer getMaxQuantity() {
		if (maxQuantity == null)
			return 0;
		else
			return maxQuantity;
	}

	public Integer getRawMaxQuantity() {
		return maxQuantity;
	}

	public void setMaxQuantity(Integer maxQuantity) {
		this.maxQuantity = maxQuantity;
	}

	public BigDecimal getMinAmount() {
		if (minAmount == null)
			return BigDecimal.ZERO;
		else
			return minAmount;
	}

	public BigDecimal getRawMinAmount() {
		return minAmount;
	}

	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}

	public BigDecimal getMaxAmount() {
		if (maxAmount == null)
			return BigDecimal.ZERO;
		else
			return maxAmount;
	}

	public BigDecimal getRawMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}

	public Float getPercent() {
		return percent;
	}

	public void setPercent(Float percent) {
		this.percent = percent;
	}

	public Integer getHasProduct() {
		return hasProduct;
	}

	public void setHasProduct(Integer hasProduct) {
		this.hasProduct = hasProduct;
	}

	public GroupLevel getParentLevel() {
		return parentLevel;
	}

	public void setParentLevel(GroupLevel parentLevel) {
		this.parentLevel = parentLevel;
	}

	public String getLevelCode() {
		return levelCode;
	}

	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}

	public Long getPromotionProgramId() {
		return promotionProgramId;
	}

	public void setPromotionProgramId(Long promotionProgramId) {
		this.promotionProgramId = promotionProgramId;
	}
}
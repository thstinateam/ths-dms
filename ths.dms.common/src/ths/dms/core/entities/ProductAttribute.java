package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.ProductAttributeEnumType;
import ths.dms.core.entities.enumtype.ProductAttributeReqType;
import ths.dms.core.entities.enumtype.ProductAttributeSearchType;

/**
 * 
 * Quan ly thuoc tinh san pham
 * @author tientv11
 * @since 15/01/2015
 * 
 */

@Entity
@Table(name = "PRODUCT_ATTRIBUTE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PRODUCT_ATTRIBUTE_SEQ", allocationSize = 1)
public class ProductAttribute implements Serializable {

	private static final long serialVersionUID = 1L;
	// id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PRODUCT_ATTRIBUTE_ID")
	private Long id;
	

	// ma thuoc tinh
	@Basic
	@Column(name = "CODE", length = 40)
	private String attributeCode;

	// ten thuoc tinh
	@Basic
	@Column(name = "NAME", length = 500)
	private String attributeName;	
	
	@Basic
	@Column(name = "DESCRIPTION", length = 500)
	private String description;
	
	

	// trang thai: 0: ngung: 1:hoat dong
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;
	
	
	@Basic
	@Column(name = "IS_ENUMERATION", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProductAttributeEnumType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProductAttributeEnumType attributeEnumType = ProductAttributeEnumType.NO;
	
	
	@Basic
	@Column(name = "IS_SEARCH", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProductAttributeSearchType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProductAttributeSearchType attributeSearchType = ProductAttributeSearchType.NO;
	
	@Basic
	@Column(name = "MANDATORY", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProductAttributeReqType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProductAttributeReqType attributeReqType = ProductAttributeReqType.NOT_REQUIRE;


	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.AttributeColumnType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private AttributeColumnType valueType;
	
	
	@Basic
	@Column(name = "DATA_LENGTH",length=2000)
	private Integer dataLength;
	
	
	@Basic
	@Column(name = "MIN_VALUE")
	private Integer minValue;
	
	
	@Basic
	@Column(name = "MAX_VALUE")
	private Integer maxValue;
	
	@Basic
	@Column(name = "DISPLAY_ORDER")
	private Integer displayOrder;	
	

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttributeCode() {
		return attributeCode;
	}

	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public ProductAttributeEnumType getAttributeEnumType() {
		return attributeEnumType;
	}

	public void setAttributeEnumType(ProductAttributeEnumType attributeEnumType) {
		this.attributeEnumType = attributeEnumType;
	}

	public ProductAttributeSearchType getAttributeSearchType() {
		return attributeSearchType;
	}

	public void setAttributeSearchType(
			ProductAttributeSearchType attributeSearchType) {
		this.attributeSearchType = attributeSearchType;
	}

	public ProductAttributeReqType getAttributeReqType() {
		return attributeReqType;
	}

	public void setAttributeReqType(ProductAttributeReqType attributeReqType) {
		this.attributeReqType = attributeReqType;
	}

	public AttributeColumnType getValueType() {
		return valueType;
	}

	public void setValueType(AttributeColumnType valueType) {
		this.valueType = valueType;
	}

	public Integer getDataLength() {
		return dataLength;
	}

	public void setDataLength(Integer dataLength) {
		this.dataLength = dataLength;
	}

	public Integer getMinValue() {
		return minValue;
	}

	public void setMinValue(Integer minValue) {
		this.minValue = minValue;
	}

	public Integer getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Integer maxValue) {
		this.maxValue = maxValue;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	
	
	

}
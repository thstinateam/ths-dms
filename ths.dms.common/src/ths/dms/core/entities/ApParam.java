package ths.dms.core.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "AP_PARAM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "AP_PARAM_SEQ", allocationSize = 1)
public class ApParam implements Serializable {

	private static final long serialVersionUID = 1L;
	//ma
	@Basic
	@Column(name = "AP_PARAM_CODE", length = 50)
	private String apParamCode;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "AP_PARAM_ID")
	private Long id;

	//ten
	@Basic
	@Column(name = "AP_PARAM_NAME", length = 750)
	private String apParamName;

	//ghi chu
	@Basic
	@Column(name = "DESCRIPTION", length = 100)
	private String description;

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	@Basic
	@Column(name = "TYPE", columnDefinition = "varchar2(50)", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ApParamType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ApParamType type;
	@Transient
	private String typeStr;

	//gia tri
	@Basic
	@Column(name = "VALUE", length = 50)
	private String value;

	public String getApParamCode() {
		return apParamCode;
	}

	public void setApParamCode(String apParamCode) {
		this.apParamCode = apParamCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApParamName() {
		return apParamName;
	}

	public void setApParamName(String apParamName) {
		this.apParamName = apParamName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}



	public ApParamType getType() {
		return type;
	}

	public void setType(ApParamType type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getTypeStr() {
		return typeStr;
	}

	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}
	
	//TODO Ho tro gan Code va Value
	public String getApParamCodeValue() {
		return apParamCode + " - " + value;
	}
	
}
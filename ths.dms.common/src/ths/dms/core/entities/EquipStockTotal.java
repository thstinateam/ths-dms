package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.EquipStockTotalType;

/**
 * Auto generate entities - HCM standard
 * 
 * @author hungnm16
 * @since December 14,2014
 * @description entities thuoc kenh thiet bi
 */
@Entity
@Table(name = "EQUIP_STOCK_TOTAL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_STOCK_TOTAL_SEQ", allocationSize = 1)
public class EquipStockTotal implements Serializable {

	private static final long serialVersionUID = 1L;

	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//ID nh�m thi?t b?
	@ManyToOne(targetEntity = EquipGroup.class)
	@JoinColumn(name = "EQUIP_GROUP_ID", referencedColumnName = "EQUIP_GROUP_ID")
	private EquipGroup equipGroup;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_STOCK_TOTAL_ID")
	private Long id;

	//T?n kho hi?n t?i
	@Basic
	@Column(name = "QUANTITY")
	private Integer quantity;

	//ID �?i t�?ng theo STOCK_TYPE
	@Basic
	@Column(name = "STOCK_ID")
	private Long stockId;

	//1. Kho VNM, 2. Kho NPP, 3. Kho KH, Null: tr�?ng h?p t?n kho chung theo nh�m thi?t b?
	// cap nhat DB: 27/04/2015; 1. Kho,2. Kho KH,0: trường hợp tồn kho chung theo nhóm thiết bị
	@Basic
	@Column(name = "STOCK_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipStockTotalType"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipStockTotalType stockType ;

	//Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public EquipGroup getEquipGroup() {
		return equipGroup;
	}

	public void setEquipGroup(EquipGroup equipGroup) {
		this.equipGroup = equipGroup;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public EquipStockTotalType getStockType() {
		return stockType;
	}

	public void setStockType(EquipStockTotalType stockType) {
		this.stockType = stockType;
	}

}
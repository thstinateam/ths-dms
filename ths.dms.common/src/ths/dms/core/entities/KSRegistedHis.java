/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Auto generate entities KSShopTarget
 * 
 * @author hunglm16
 * @since 23/11/2015
 */
@Entity
@Table(name = "KS_REGISTED_HIS")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "KS_REGISTED_HIS_SEQ", allocationSize = 1)
public class KSRegistedHis implements Serializable {

	private static final long serialVersionUID = 669684430682578071L;

	//Id khoa chinh
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "KS_REGISTED_HIS_ID")
	private Long id;
	//Ma Chuong trinh
	@Basic
	@Column(name = "KS_ID", length = 20)
	private Long ksId;
	//Ma chu ky
	@Basic
	@Column(name = "CYCLE_ID", length = 20)
	private Long cycleId;
	//Ma Chuong trinh voi Khach hang
	@Basic
	@Column(name = "KS_CUSTOMER_ID", length = 20)
	private Long ksCustomerId;
	//Ma khach hang
	@Basic
	@Column(name = "CUSTOMER_ID", length = 20)
	private Long customerId;
	//Ma Muc
	@Basic
	@Column(name = "KS_LEVEL_ID", length = 20)
	private Long ksLevelId;
	//Thanh tien
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;
	//So luong
	@Basic
	@Column(name = "QUANTITY", length = 20)
	private BigDecimal quantity;
	//Boi so
	@Basic
	@Column(name = "MULTIPLYER", length = 6)
	private Integer multiPlyer;

//	//Ngay tao
//	@Basic
//	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date createDate;
//
//	//Nguoi tao
//	@Basic
//	@Column(name = "CREATE_USER", length = 100)
//	private String createUser;

	//Ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 100)
	private String updateUser;
	
	/**
	 * Them phuong thuc GETTER/SETTER
	 * @return
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getKsId() {
		return ksId;
	}

	public void setKsId(Long ksId) {
		this.ksId = ksId;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public Long getKsCustomerId() {
		return ksCustomerId;
	}

	public void setKsCustomerId(Long ksCustomerId) {
		this.ksCustomerId = ksCustomerId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getKsLevelId() {
		return ksLevelId;
	}

	public void setKsLevelId(Long ksLevelId) {
		this.ksLevelId = ksLevelId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Integer getMultiPlyer() {
		return multiPlyer;
	}

	public void setMultiPlyer(Integer multiPlyer) {
		this.multiPlyer = multiPlyer;
	}

//	public Date getCreateDate() {
//		return createDate;
//	}
//
//	public void setCreateDate(Date createDate) {
//		this.createDate = createDate;
//	}
//
//	public String getCreateUser() {
//		return createUser;
//	}
//
//	public void setCreateUser(String createUser) {
//		this.createUser = createUser;
//	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
}
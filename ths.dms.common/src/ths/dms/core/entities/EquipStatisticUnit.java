package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.poi.hssf.record.chart.UnitsRecord;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
/**
* Auto generate entities - HCM standard
* 
* @author phut
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_STATISTIC_UNIT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_STATISTIC_UNIT_SEQ", allocationSize = 1)
public class EquipStatisticUnit implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//ID bi�n b?n ki?m k�
	@ManyToOne(targetEntity = EquipStatisticRecord.class)
	@JoinColumn(name = "EQUIP_STATISTIC_RECORD_ID", referencedColumnName = "EQUIP_STATISTIC_RECORD_ID")
	private EquipStatisticRecord equipStatisticRecord;

	//ID ��n v? ki?m k�
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_STATISTIC_UNIT_ID")
	private Long id;

	//ID Don vi tham gia kiem khong
	@Basic
	@Column(name = "UNIT_ID")
	private Long unitId;

	//Loai: 1: VNM; 2: NPP
	@Basic
	@Column(name = "UNIT_TYPE", length = 2)
	private Integer unitType;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	//Trang thai: 0: off; 1: on
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public EquipStatisticRecord getEquipStatisticRecord() {
		return equipStatisticRecord;
	}

	public void setEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord) {
		this.equipStatisticRecord = equipStatisticRecord;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUnitId() {
		return unitId;
	}

	public void setUnitId(Long unitId) {
		this.unitId = unitId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Integer getUnitType() {
		return unitType;
	}

	public void setUnitType(Integer unitType) {
		this.unitType = unitType;
	}
}
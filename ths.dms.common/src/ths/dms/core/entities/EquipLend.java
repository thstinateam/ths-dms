package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author tamvnm
* @since April 01,2015
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_LEND")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_LEND_SEQ", allocationSize = 1)
public class EquipLend implements Serializable {

	private static final long serialVersionUID = 1L;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_LEND_ID")
	private Long equipLendId;

	//ma
	@Basic
	@Column(name = "CODE", length = 50)
	private String code;
	
	//ghi chu
	@Basic
	@Column(name = "NOTE", length = 50)
	private String note;
	
	//ma GS
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "USER_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	//shop
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	//ki
	@ManyToOne(targetEntity = EquipPeriod.class)
	@JoinColumn(name = "EQUIP_PERIOD_ID", referencedColumnName = "EQUIP_PERIOD_ID")
	private EquipPeriod equipPeriod;
	
	//tran thai muon: 0: Du thao, 1: Cho duyet, 2: Da duyet, 3: Khong duyet, 4: Huy
	@Basic
	@Column(name = "STATUS", length = 2)
	private Integer status;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	//trang thai giao nhan: 0:  Chua xuat, 1: Da xuat
	@Basic
	@Column(name = "DELIVERY_STATUS", length = 2)
	private Integer deliveryStatus;

	@Basic
	@Column(name = "CREATE_FORM_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createFormDate;
	
	@Basic
	@Column(name = "APPROVE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approveDate;
	
	public Long getEquipLendId() {
		return equipLendId;
	}

	public void setEquipLendId(Long equipLendId) {
		this.equipLendId = equipLendId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public EquipPeriod getEquipPeriod() {
		return equipPeriod;
	}

	public void setEquipPeriod(EquipPeriod equipPeriod) {
		this.equipPeriod = equipPeriod;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(Integer deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public Date getCreateFormDate() {
		return createFormDate;
	}

	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
}
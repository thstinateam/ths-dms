package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ProgramType;

/**
* Auto generate entities - SALE_ORDER_DETAIL
* @author nhanlt6
* @since 15/8/2014
*/

@Entity
@Table(name = "SALE_ORDER_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SALE_ORDER_DETAIL_SEQ", allocationSize = 1)
public class SaleOrderDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//ID chi tiet don hang, Auto number
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SALE_ORDER_DETAIL_ID")
	private Long id;
	
	//Id don hang
	@ManyToOne(targetEntity = SaleOrder.class)
	@JoinColumn(name = "SALE_ORDER_ID", referencedColumnName = "SALE_ORDER_ID")
	private SaleOrder saleOrder;

	//id npp
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//id nv
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	//So tien
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//So tien chiet khau
	@Basic
	@Column(name = "DISCOUNT_AMOUNT", length = 22)
	private BigDecimal discountAmount;

	//% chiet khau
	@Basic
	@Column(name = "DISCOUNT_PERCENT", length = 22)
	private Float discountPercent;

	//0: hang ban; 1 khuyen mai
	@Basic
	@Column(name = "IS_FREE_ITEM", length = 22)
	private Integer isFreeItem = 0 ;
	
	@Basic
	@Column(name = "JOIN_PROGRAM_CODE", length = 50)
	private String joinProgramCode;
	
	@Basic
	@Column(name = "PROMOTION_ORDER_NUMBER", length = 50)
	private Integer promotionOrderNumber;

	//Ngay dat
	@Basic
	@Column(name = "ORDER_DATE", length = 7)
	private Date orderDate;

	//Gia ban da co VAT
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal priceValue;
	
	//Gia chua thue
	@Basic
	@Column(name = "PRICE_NOT_VAT", length = 22)
	private BigDecimal priceNotVat;
	
	//Gia thung da co VAT
	@Basic
	@Column(name = "PACKAGE_PRICE", length = 22)
	private BigDecimal packagePrice;
	
	//Gia thung chua VAT
	@Basic
	@Column(name = "PACKAGE_PRICE_NOT_VAT", length = 22)
	private BigDecimal packagePriceNotVAT;

	//Id gia ban
	@ManyToOne(targetEntity = Price.class)
	@JoinColumn(name = "PRICE_ID", referencedColumnName = "PRICE_ID")
	private Price price;

	//Ma san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;
	
	//Ma nganh hang
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "CAT_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo productInfo;

	//PROGRAM_TYPE = 0,1 thi ma la CTKM; PROGRAM_TYPE = 2 --> ma la CTTB
	@Basic
	@Column(name = "PROGRAM_CODE", length = 50)
	private String programCode;

	//0: Km tu dong, 1:KM bang tay,2: cham chung bay, 3: doi hang
	@Basic
	@Column(name = "PROGRAM_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProgramType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProgramType programType;

	//So luong
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;
	
	//So luong le
	@Basic
	@Column(name = "QUANTITY_RETAIL", length = 22)
	private Integer quantityRetail;
	
	//So luong thung
	@Basic
	@Column(name = "QUANTITY_PACKAGE", length = 22)
	private Integer quantityPackage;
	
	//PROGRAME_TYPE_CODE
	@Basic
	@Column(name = "PROGRAME_TYPE_CODE", length = 50)
	private String programeTypeCode;

	//Trong luong
	@Basic
	@Column(name = "TOTAL_WEIGHT", length = 22)
	private BigDecimal totalWeight;

	//Thue VAT
	@Basic
	@Column(name = "VAT", length = 22)
	private Float vat;
	
	//Id hoa don
	@ManyToOne(targetEntity = Invoice.class)
	@JoinColumn(name = "INVOICE_ID", referencedColumnName = "INVOICE_ID")
	private Invoice invoice;

	//So luong KM toi da
	@Basic
	@Column(name = "MAX_QUANTITY_FREE", length = 17)
	private Integer maxQuantityFree;
	
//	//CK KM toi da
//	@Basic
//	@Column(name = "MAX_AMOUNT_FREE", length = 17)
//	private BigDecimal maxAmountFree;
	
	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	//Ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi tao
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

/*	// ThuatTQ add migrate
	@Basic
	@Column(name = "IS_NOT_MIGRATE", length = 8)
	private Integer isNotMigrate;	
*/
	// ThuatTQ add trigger
/*	@Basic
	@Column(name = "IS_NOT_TRIGGER", length = 8)
	private Integer isNotTrigger;*/
	
	
	/*@ManyToOne(targetEntity = ProductGroup.class)
	@JoinColumn(name = "PRODUCT_GROUP_ID", referencedColumnName = "PRODUCT_GROUP_ID")*/
	@Transient
	private ProductGroup productGroup;
	
	@Basic
	@Column(name = "PRODUCT_GROUP_ID")
	private Long productGroupId;
	
	/*@ManyToOne(targetEntity = GroupLevel.class)
	@JoinColumn(name = "GROUP_LEVEL_ID", referencedColumnName = "GROUP_LEVEL_ID")*/
	@Transient
	private GroupLevel groupLevel;
	
	@Basic
	@Column(name = "GROUP_LEVEL_ID")
	private Long groupLevelId;
	
	//so thu tu lan dat
	@Basic
	@Column(name = "PAYING_ORDER", length = 10)
	private Integer payingOrder;
	
	@Basic
	@Column(name = "CONVFACT", length = 22)
	private Integer convfact;
	
	/** 
	 * Nhung thuoc tinh khong co trong DB. Phuc vu cho tinh khuyen mai 
	 * @author tientv11
	 */
	
	@Transient
	private Integer rowNum;
	
	
	@Transient
	private Integer orderNumber;
	
	@Transient
	private BigDecimal maxAmountFree;
	
	@Transient 
	private Boolean hasPromotion;
	
	@Transient
	private Integer levelPromo;
	
	@Transient
	private String lstProgramCode;
	
	@Transient
	private Integer maxQuantityFreeTotal; // neu kho du so suat, so luong, tien,: luu lai gia tri cu tinh KM
	
	@Transient
	private Integer promotionQuotation;
	
	public ProductGroup getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(ProductGroup productGroup) {
		this.productGroup = productGroup;
	}

	public GroupLevel getGroupLevel() {
		return groupLevel;
	}

	public void setGroupLevel(GroupLevel groupLevel) {
		this.groupLevel = groupLevel;
	}
	
	public Integer getLevelPromo() {
		return levelPromo;
	}

	public void setLevelPromo(Integer levelPromo) {
		this.levelPromo = levelPromo;
	}

	public Boolean getHasPromotion() {
		return hasPromotion;
	}

	public void setHasPromotion(Boolean hasPromotion) {
		this.hasPromotion = hasPromotion;
	}

	public BigDecimal getMaxAmountFree() {
		return maxAmountFree;
	}

	public void setMaxAmountFree(BigDecimal maxAmountFree) {
		this.maxAmountFree = maxAmountFree;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Integer getRowNum() {
		return rowNum;
	}

	public void setRowNum(Integer rowNum) {
		this.rowNum = rowNum;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount == null ? BigDecimal.valueOf(0) : discountAmount;
	}
	
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Float getDiscountPercent() {
		return discountPercent == null ? 0f : discountPercent;
	}

	public void setDiscountPercent(Float discountPercent) {
		this.discountPercent = discountPercent;
	}

	public Integer getIsFreeItem() {
		return isFreeItem;
	}

	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public BigDecimal getPriceValue() {
		return priceValue == null ? BigDecimal.valueOf(0) : priceValue;
	}

	public void setPriceValue(BigDecimal priceValue) {
		this.priceValue = priceValue;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public ProgramType getProgramType() {
		return programType;
	}

	public void setProgramType(ProgramType programType) {
		this.programType = programType;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}
	
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public BigDecimal getTotalWeight() {
		return totalWeight == null ? BigDecimal.valueOf(0) : totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Float getVat() {
		return vat == null ? 0f : vat;
	}

	public void setVat(Float vat) {
		this.vat = vat;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Integer getMaxQuantityFree() {
		return maxQuantityFree;
	}

	public void setMaxQuantityFree(Integer maxQuantityFree) {
		this.maxQuantityFree = maxQuantityFree;
	}

	public BigDecimal getPriceNotVat() {
		return priceNotVat;
	}
	

	public void setPriceNotVat(BigDecimal priceNotVat) {
		this.priceNotVat = priceNotVat;
	}

//	public BigDecimal getMaxAmountFree() {
//		return maxAmountFree;
//	}
//
//	public void setMaxAmountFree(BigDecimal maxAmountFree) {
//		this.maxAmountFree = maxAmountFree;
//	}

	public String getProgrameTypeCode() {
		return programeTypeCode;
	}

	public void setProgrameTypeCode(String programeTypeCode) {
		this.programeTypeCode = programeTypeCode;
	}
/*	
	public Integer getIsNotMigrate() {
		return isNotMigrate;
	}

	public void setIsNotMigrate(Integer isNotMigrate) {
		this.isNotMigrate = isNotMigrate;
	}
*/
/*	public Integer getIsNotTrigger() {
		return isNotTrigger;
	}

	public void setIsNotTrigger(Integer isNotTrigger) {
		this.isNotTrigger = isNotTrigger;
	}*/

//	public BigDecimal getMyTime() {
//		return myTime;
//	}
//
//	public void setMyTime(BigDecimal myTime) {
//		this.myTime = myTime;
//	}
	
	public String getDiscountAmountStr() {
		if(discountAmount == null) {
			return "0";
		}
		String result = "";
		String _money = discountAmount.toBigInteger() + "";
		int isDot = 1;
		for(int i = _money.length(); i > 0; i--) {
			char ch = _money.charAt(i - 1);
			if(isDot == 3 && i != 1) {
				if(_money.charAt(i - 2) == '-') {
					result = ch + result;
					isDot = 0;
				} else {
					result = "." + ch + result;
					isDot = 0;
				}
			} else {
				result = ch + result;
			}
			isDot++;
		}
		return result;
	}
	
	public String getQuantityStr() {
		if(quantity == null) {
			return "0";
		}
		String _number = quantity + "";
		String result = "";
		int isDot = 1;
		for(int i = _number.length(); i > 0; i--) {
			char ch = _number.charAt(i - 1);
			if(isDot == 3 && i != 1) {
				if(_number.charAt(i - 2) == '-') {
					result = ch + result;
					isDot = 0;
				} else {
					result = "." + ch + result;
					isDot = 0;
				}
			} else {
				result = ch + result;
			}
			isDot++;
		}
		return result;
	}
	
	public String getPriceNotVatStr() {
		if(priceNotVat == null) {
			return "0";
		}
		String result = "";
		String _money = priceNotVat.toBigInteger() + "";
		int isDot = 1;
		for(int i = _money.length(); i > 0; i--) {
			char ch = _money.charAt(i - 1);
			if(isDot == 3 && i != 1) {
				if(_money.charAt(i - 2) == '-') {
					result = ch + result;
					isDot = 0;
				} else {
					result = "." + ch + result;
					isDot = 0;
				}
			} else {
				result = ch + result;
			}
			isDot++;
		}
		return result;
	}
	
	public String getJoinProgramCode() {
		return joinProgramCode;
	}

	public void setJoinProgramCode(String joinProgramCode) {
		this.joinProgramCode = joinProgramCode;
	}

	public Integer getPromotionOrderNumber() {
		return promotionOrderNumber;
	}

	public void setPromotionOrderNumber(Integer promotionOrderNumber) {
		this.promotionOrderNumber = promotionOrderNumber;
	}

	public Long getGroupLevelId() {
		return groupLevelId;
	}

	public void setGroupLevelId(Long groupLevelId) {
		this.groupLevelId = groupLevelId;
	}

	public Long getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(Long productGroupId) {
		this.productGroupId = productGroupId;
	}

	
	public Integer getPayingOrder() {
		return payingOrder;
	}

	public void setPayingOrder(Integer payingOrder) {
		this.payingOrder = payingOrder;
	}

	public BigDecimal getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(BigDecimal packagePrice) {
		this.packagePrice = packagePrice;
	}

	public BigDecimal getPackagePriceNotVAT() {
		return packagePriceNotVAT;
	}

	public void setPackagePriceNotVAT(BigDecimal packagePriceNotVAT) {
		this.packagePriceNotVAT = packagePriceNotVAT;
	}

	public ProductInfo getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}

	public Integer getQuantityRetail() {
		return quantityRetail;
	}

	public void setQuantityRetail(Integer quantityRetail) {
		this.quantityRetail = quantityRetail;
	}

	public Integer getQuantityPackage() {
		return quantityPackage;
	}

	public void setQuantityPackage(Integer quantityPackage) {
		this.quantityPackage = quantityPackage;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public SaleOrderDetail clone() {
		SaleOrderDetail temp = new SaleOrderDetail();
		temp.amount = amount;
		temp.createDate = createDate;
		temp.createUser = createUser;
		temp.discountAmount = discountAmount;
		temp.discountPercent = discountPercent;
		temp.isFreeItem = isFreeItem;
		//temp.isNotMigrate = isNotMigrate;
		//temp.isNotTrigger = isNotTrigger;
		temp.id = id;
		temp.orderDate = orderDate;
		temp.priceValue = priceValue;
		temp.price = price;
		temp.product = product;
		temp.programCode = programCode;
		temp.programType = programType;
		temp.programeTypeCode = programeTypeCode;
		temp.quantity = quantity;
		temp.saleOrder = saleOrder;
		temp.shop = shop;
		temp.staff = staff;
		temp.totalWeight = totalWeight;
		temp.updateDate = updateDate;
		temp.updateUser = updateUser;
		temp.vat = vat;
		temp.priceNotVat = priceNotVat;
//		temp.maxAmountFree = maxAmountFree;
		temp.maxQuantityFree = maxQuantityFree;
		temp.joinProgramCode = joinProgramCode;
		temp.promotionOrderNumber = promotionOrderNumber;
		temp.groupLevelId = groupLevelId;
		temp.payingOrder = payingOrder;
		temp.promotionQuotation = promotionQuotation;
		return temp;
	}

	public String getLstProgramCode() {
		return lstProgramCode;
	}

	public void setLstProgramCode(String lstProgramCode) {
		this.lstProgramCode = lstProgramCode;
	}

	public Integer getMaxQuantityFreeTotal() {
		return maxQuantityFreeTotal;
	}

	public void setMaxQuantityFreeTotal(Integer maxQuantityFreeTotal) {
		this.maxQuantityFreeTotal = maxQuantityFreeTotal;
	}

	public Integer getPromotionQuotation() {
		return promotionQuotation;
	}

	public void setPromotionQuotation(Integer promotionQuotation) {
		this.promotionQuotation = promotionQuotation;
	}
	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;


/**
 * Phan cap nguoi dung - them moi
 * 
 * @author hunglm16
 * @since August 20,2014
 * 
 */

@Entity
@Table(name = "PARENT_STAFF_MAP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PARENT_STAFF_MAP_SEQ", allocationSize = 1)
public class ParentStaffMap implements Serializable {

	private static final long serialVersionUID = 1L;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PARENT_STAFF_MAP_ID")
	private Long id;

	//Trang thai: 0:ko hoat dong,1: hoat dong
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "PARENT_STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff parentStaff;
	
	//Ng�y tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//Ng�y tao
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Nguoi tao
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	//Hieu luc tu ngay
	@Basic
	@Column(name = "FROM_DATE", length = 7)
	private Date fromDate;
	
	//Hieu luc den ngay
	@Basic
	@Column(name = "TO_DATE", length = 7)
	private Date toDate;
	
	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Staff getParentStaff() {
		return parentStaff;
	}

	public void setParentStaff(Staff parentStaff) {
		this.parentStaff = parentStaff;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	public ParentStaffMap clone() {
		ParentStaffMap obj = new ParentStaffMap();
		obj.setCreateDate(createDate);
		obj.setCreateUser(createUser);
		obj.setFromDate(fromDate);
		obj.setId(id);
		obj.setParentStaff(parentStaff);
		obj.setStaff(staff);
		obj.setStatus(status);
		obj.setToDate(toDate);
		obj.setUpdateDate(updateDate);
		obj.setUpdateUser(updateUser);
		return obj;
	}
}
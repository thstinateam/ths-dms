package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.RptDebitMonthObjectType;


/**
 * The persistent class for the RPT_DEBIT_MONTH database table.
 * 
 */
@Entity
@Table(name = "RPT_DEBIT_MONTH")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "RPT_DEBIT_MONTH_SEQ", allocationSize = 1)
public class RptDebitMonth implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "RPT_DEBIT_MONTH_ID")
	private Long id;

	@Basic
	@Column(name = "CLOSE_DEBIT")
	private BigDecimal closeDebit;

	@Basic
	@Column(name = "IN_DEBIT")
	private BigDecimal inDebit;

	@Basic
	@Column(name = "OPEN_DEBIT")
	private BigDecimal openDebit;

	@Basic
	@Column(name = "OBJECT_ID")
	private Long objectId;

	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RptDebitMonthObjectType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RptDebitMonthObjectType objectType;

	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "RPT_IN_MONTH")
	private Date rptInMonth;

	public RptDebitMonth() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getCloseDebit() {
		return closeDebit;
	}

	public void setCloseDebit(BigDecimal closeDebit) {
		this.closeDebit = closeDebit;
	}

	public BigDecimal getInDebit() {
		return inDebit;
	}

	public void setInDebit(BigDecimal inDebit) {
		this.inDebit = inDebit;
	}

	public BigDecimal getOpenDebit() {
		return openDebit;
	}

	public void setOpenDebit(BigDecimal openDebit) {
		this.openDebit = openDebit;
	}

	public Date getRptInMonth() {
		return rptInMonth;
	}

	public void setRptInMonth(Date rptInMonth) {
		this.rptInMonth = rptInMonth;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public RptDebitMonthObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(RptDebitMonthObjectType objectType) {
		this.objectType = objectType;
	}

}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.PerFormStatus;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_STOCK_TRANS_FORM_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_STOCK_TRANS_FORM_DTL_SEQ", allocationSize = 1)
public class EquipStockTransFormDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "COMPLETE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date completeDate;
	
	//Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	@ManyToOne(targetEntity = Equipment.class)
	@JoinColumn(name = "EQUIP_ID", referencedColumnName = "EQUIP_ID")
	private Equipment equip;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_STOCK_TRANS_FORM_DTL_ID")
	private Long id;

	@ManyToOne(targetEntity = EquipStockTransForm.class)
	@JoinColumn(name = "EQUIP_STOCK_TRANS_FORM_ID", referencedColumnName = "EQUIP_STOCK_TRANS_FORM_ID")
	private EquipStockTransForm equipStockTransForm;

	//T?nh tr?ng thi?t b?. Link qua AP_PARAM v?i AP_PARAM.TYPE = EQUIP_CONDITION
	@Basic
	@Column(name = "HEALTH_STATUS", length = 500)
	private String healthStatus;

	//Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;
	
	//M? kho ngu?n
	@Basic
	@Column(name = "FROM_STOCK_ID")
	private Long fromStockId;
	
	@Basic
	@Column(name = "FROM_STOCK_NAME", length = 200)
	private String fromStockName;
	
	@Basic
	@Column(name = "TO_STOCK_NAME", length = 200)
	private String toStockName;
	
	//M? kho dich
	@Basic
	@Column(name = "TO_STOCK_ID")
	private Long toStockId;
	
	// m? serial
	@Basic
	@Column(name = "SERIAL", length = 100)
	private String serial;
	
	// ID class
	@ManyToOne(targetEntity = EquipGroup.class)
	@JoinColumn(name = "EQUIP_GROUP_ID", referencedColumnName = "EQUIP_GROUP_ID")
	private EquipGroup equipGroup;
	
	@Basic
	@Column(name = "EQUIP_GROUP_NAME", length = 200)
	private String equipGroupName;
	
	// dung tich (lit)
	@Basic
	@Column(name = "CAPACITY_FROM", length = 22)
	private BigDecimal capacityFrom;
	
	// dung tich (lit)
	@Basic
	@Column(name = "CAPACITY_TO", length = 22)
	private BigDecimal capacityTo;
	
	@Basic
	@Column(name = "FROM_SHOP_NAME", length = 200)
	private String fromShopName;
	
	@Basic
	@Column(name = "TO_SHOP_NAME", length = 200)
	private String toShopName;
	
	// 2. dang thuc hien, 1. hoan thanh , -1: huy
	@Basic
	@Column(name = "PERFORM_STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PerFormStatus"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PerFormStatus performStatus = PerFormStatus.ONGOING;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Equipment getEquip() {
		return equip;
	}

	public void setEquip(Equipment equip) {
		this.equip = equip;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipStockTransForm getEquipStockTransForm() {
		return equipStockTransForm;
	}

	public void setEquipStockTransForm(EquipStockTransForm equipStockTransForm) {
		this.equipStockTransForm = equipStockTransForm;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Long getFromStockId() {
		return fromStockId;
	}

	public void setFromStockId(Long fromStockId) {
		this.fromStockId = fromStockId;
	}

	public String getFromStockName() {
		return fromStockName;
	}

	public void setFromStockName(String fromStockName) {
		this.fromStockName = fromStockName;
	}

	public String getToStockName() {
		return toStockName;
	}

	public void setToStockName(String toStockName) {
		this.toStockName = toStockName;
	}

	public Long getToStockId() {
		return toStockId;
	}

	public void setToStockId(Long toStockId) {
		this.toStockId = toStockId;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public EquipGroup getEquipGroup() {
		return equipGroup;
	}

	public void setEquipGroup(EquipGroup equipGroup) {
		this.equipGroup = equipGroup;
	}

	public String getEquipGroupName() {
		return equipGroupName;
	}

	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}

	public BigDecimal getCapacityFrom() {
		return capacityFrom;
	}

	public void setCapacityFrom(BigDecimal capacityFrom) {
		this.capacityFrom = capacityFrom;
	}

	public BigDecimal getCapacityTo() {
		return capacityTo;
	}

	public void setCapacityTo(BigDecimal capacityTo) {
		this.capacityTo = capacityTo;
	}

	public String getFromShopName() {
		return fromShopName;
	}

	public void setFromShopName(String fromShopName) {
		this.fromShopName = fromShopName;
	}

	public String getToShopName() {
		return toShopName;
	}

	public void setToShopName(String toShopName) {
		this.toShopName = toShopName;
	}

	public PerFormStatus getPerformStatus() {
		return performStatus;
	}

	public void setPerformStatus(PerFormStatus performStatus) {
		this.performStatus = performStatus;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}
	
}
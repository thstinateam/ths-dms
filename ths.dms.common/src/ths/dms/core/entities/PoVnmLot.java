package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PO_VNM_LOT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PO_VNM_LOT_SEQ", allocationSize = 1)
public class PoVnmLot implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	@Basic
	@Column(name = "AMOUNT_PAY", length = 22)
	private BigDecimal amountPay;

	@Basic
	@Column(name = "LOT", length = 50)
	private String lot;

	@Basic
	@Column(name = "PO_VNM_DATE", length = 7)
	private Date poVnmDate;

	@ManyToOne(targetEntity = PoVnmDetail.class)
	@JoinColumn(name = "PO_VNM_DETAIL_ID", referencedColumnName = "PO_VNM_DETAIL_ID")
	private PoVnmDetail poVnmDetail;

	@ManyToOne(targetEntity = PoVnm.class)
	@JoinColumn(name = "PO_VNM_ID", referencedColumnName = "PO_VNM_ID")
	private PoVnm poVnm;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PO_VNM_LOT_ID")
	private Long id;

	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal priceValue;

	@ManyToOne(targetEntity = Price.class)
	@JoinColumn(name = "PRICE_ID", referencedColumnName = "PRICE_ID")
	private Price price;

	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;

	@Basic
	@Column(name = "QUANTITY_PAY", length = 22)
	private Integer quantityPay;

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getAmountPay() {
		return amountPay == null ? BigDecimal.valueOf(0) : amountPay;
	}

	public void setAmountPay(BigDecimal amountPay) {
		this.amountPay = amountPay;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public Date getPoVnmDate() {
		return poVnmDate;
	}

	public void setPoVnmDate(Date poVnmDate) {
		this.poVnmDate = poVnmDate;
	}

	public PoVnmDetail getPoVnmDetail() {
		return poVnmDetail;
	}

	public void setPoVnmDetail(PoVnmDetail poVnmDetail) {
		this.poVnmDetail = poVnmDetail;
	}

	public PoVnm getPoVnm() {
		return poVnm;
	}

	public void setPoVnm(PoVnm poVnm) {
		this.poVnm = poVnm;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPriceValue() {
		return priceValue == null ? BigDecimal.valueOf(0) : priceValue;
	}

	public void setPriceValue(BigDecimal priceValue) {
		this.priceValue = priceValue;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantityPay() {
		if (quantityPay == null) {
			quantityPay = 0;
		}
		return quantityPay;
	}

	public void setQuantityPay(Integer quantityPay) {
		this.quantityPay = quantityPay;
	}
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "MT_MEDIA_ALBUM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "MT_MEDIA_ALBUM_SEQ", allocationSize = 1)
public class MtMediaAlbum implements Serializable {

	private static final long serialVersionUID = 1L;
	//null
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//null
	@Basic
	@Column(name = "CREATE_USER", length = 500)
	private String createUser;

	//null
	@Basic
	@Column(name = "DESCRIPTION", length = 2000)
	private String description;

	//null
	@Basic
	@Column(name = "MT_MEDIA_ALBUM_CODE", length = 1000)
	private String mtMediaAlbumCode;

	//null
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "MT_MEDIA_ALBUM_ID")
	private Long id;

	//null
	@Basic
	@Column(name = "MT_MEDIA_ALBUM_NAME", length = 250)
	private String mtMediaAlbumName;

	//null
	@Basic
	@Column(name = "STATUS", length = 22)
	private Integer status;

	//null
	@Basic
	@Column(name = "TITLE", length = 250)
	private String title;
	
	@Basic
	@Column(name = "NAME_TEXT", length = 250)
	private String nameText;

	//null
	@Basic
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//null
	@Basic
	@Column(name = "UPDATE_USER", length = 500)
	private String updateUser;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMtMediaAlbumCode() {
		return mtMediaAlbumCode;
	}

	public void setMtMediaAlbumCode(String mtMediaAlbumCode) {
		this.mtMediaAlbumCode = mtMediaAlbumCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMtMediaAlbumName() {
		return mtMediaAlbumName;
	}

	public void setMtMediaAlbumName(String mtMediaAlbumName) {
		this.mtMediaAlbumName = mtMediaAlbumName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getNameText() {
		return nameText;
	}

	public void setNameText(String nameText) {
		this.nameText = nameText;
	}
	
	
}
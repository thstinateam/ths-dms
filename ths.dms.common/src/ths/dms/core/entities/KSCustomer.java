package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.RewardType;

/**
*
* Auto generate entities
* @author tungmt
* @since 30/6/2015
* 
*/

@Entity
@Table(name = "KS_CUSTOMER")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "KS_CUSTOMER_SEQ", allocationSize = 1)
public class KSCustomer implements Serializable {

	private static final long serialVersionUID = 1L;

	//ID no
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "KS_CUSTOMER_ID")
	private Long id;
	
	@ManyToOne(targetEntity = KS.class)
	@JoinColumn(name = "KS_ID", referencedColumnName = "KS_ID")
	private KS ks;
	
	@ManyToOne(targetEntity = Cycle.class)
	@JoinColumn(name = "CYCLE_ID", referencedColumnName = "CYCLE_ID")
	private Cycle cycle;
	
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;
	
	@Basic
	@Column(name = "AMOUNT_DISCOUNT", length = 26)
	private BigDecimal amountDiscount = new BigDecimal(0) ;
	
	@Basic
	@Column(name = "OVER_AMOUNT_DISCOUNT", length = 26)
	private BigDecimal overAmountDiscount = new BigDecimal(0) ;
	
	@Basic
	@Column(name = "DISPLAY_MONEY", length = 26)
	private BigDecimal displayMoney = new BigDecimal(0) ;
	
	@Basic
	@Column(name = "SUPPORT_MONEY", length = 26)
	private BigDecimal supportMoney = new BigDecimal(0) ;
	
	@Basic
	@Column(name = "ODD_DISCOUNT", length = 26)
	private BigDecimal oddDicount = new BigDecimal(0) ;
	
	@Basic
	@Column(name = "ADD_MONEY", length = 26)
	private BigDecimal addMoney = new BigDecimal(0) ;
	
	@Basic
	@Column(name = "TOTAL_REWARD_MONEY", length = 26)
	private BigDecimal totalRewardMoney = new BigDecimal(0) ;
	
	@Basic
	@Column(name = "TOTAL_REWARD_MONEY_DONE", length = 26)
	private BigDecimal totalRewardMoneyDone = new BigDecimal(0) ;
	
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	@Basic
	@Column(name = "REWARD_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.RewardType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private RewardType rewardType;
	
	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 100)
	private String createUser;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 100)
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public KS getKs() {
		return ks;
	}

	public void setKs(KS ks) {
		this.ks = ks;
	}

	public Cycle getCycle() {
		return cycle;
	}

	public void setCycle(Cycle cycle) {
		this.cycle = cycle;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public BigDecimal getAmountDiscount() {
		return amountDiscount;
	}

	public void setAmountDiscount(BigDecimal amountDiscount) {
		this.amountDiscount = amountDiscount;
	}

	public BigDecimal getOverAmountDiscount() {
		return overAmountDiscount;
	}

	public void setOverAmountDiscount(BigDecimal overAmountDiscount) {
		this.overAmountDiscount = overAmountDiscount;
	}

	public BigDecimal getDisplayMoney() {
		return displayMoney;
	}

	public void setDisplayMoney(BigDecimal displayMoney) {
		this.displayMoney = displayMoney;
	}

	public BigDecimal getOddDicount() {
		return oddDicount;
	}

	public void setOddDicount(BigDecimal oddDicount) {
		this.oddDicount = oddDicount;
	}

	public BigDecimal getAddMoney() {
		return addMoney;
	}

	public void setAddMoney(BigDecimal addMoney) {
		this.addMoney = addMoney;
	}

	public BigDecimal getTotalRewardMoney() {
		if (totalRewardMoney == null) {
			return BigDecimal.ZERO;
		}
		return totalRewardMoney;
	}

	public void setTotalRewardMoney(BigDecimal totalRewardMoney) {
		this.totalRewardMoney = totalRewardMoney;
	}

	public BigDecimal getTotalRewardMoneyDone() {
		if (totalRewardMoneyDone == null) {
			return BigDecimal.ZERO;
		}
		return totalRewardMoneyDone;
	}

	public void setTotalRewardMoneyDone(BigDecimal totalRewardMoneyDone) {
		this.totalRewardMoneyDone = totalRewardMoneyDone;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public RewardType getRewardType() {
		return rewardType;
	}

	public void setRewardType(RewardType rewardType) {
		this.rewardType = rewardType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public BigDecimal getSupportMoney() {
		return supportMoney;
	}

	public void setSupportMoney(BigDecimal supportMoney) {
		this.supportMoney = supportMoney;
	}

}
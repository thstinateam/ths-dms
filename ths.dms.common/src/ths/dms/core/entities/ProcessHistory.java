/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ProcessHistoryType;

/**
 * Luu thong tin chuyen trang thai doi tuong
 * @author tuannd20
 * @since 24/01/2014
 */
@Entity
@Table(name = "PROCESS_HISTORY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PROCESS_HISTORY_SEQ", allocationSize = 1)
public class ProcessHistory implements Serializable {
	private static final long serialVersionUID = 1L;
	
	// id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PROCESS_HISTORY_ID")
	private Long id;
	
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProcessHistoryType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProcessHistoryType type;
	
	/*@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProcessHistoryObjectType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProcessHistoryObjectType objectType;*/
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = false)
	private Integer objectType;
	
	@Basic
	@Column(name = "OBJECT_ID")
	private Long objectId;
	
	@Basic
	@Column(name = "LOG_TIME", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date logTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
/*
	public ProcessHistoryObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(ProcessHistoryObjectType objectType) {
		this.objectType = objectType;
	}
*/
	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public Date getLogTime() {
		return logTime;
	}

	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	public ProcessHistoryType getType() {
		return type;
	}

	public void setType(ProcessHistoryType type) {
		this.type = type;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
}

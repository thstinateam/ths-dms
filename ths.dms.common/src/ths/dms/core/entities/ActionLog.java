package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActionLogObjectType;

/**
 * 
 * Auto generate entities - HaNoi standard
 * 
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "ACTION_LOG")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "ACTION_LOG_SEQ", allocationSize = 1)
public class ActionLog implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "ACTION_LOG_ID")
	private Long id;

	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	@Basic
	@Column(name = "END_TIME", length = 7)
	private Date endTime;

	// Is out of routing, luu gia tri ghe tham co phai la KH ngoai tuyen hay
	// khong.
	@Basic
	@Column(name = "IS_OR", length = 22)
	private Long isOr = 0l;

	@Basic
	@Column(name = "LAT", length = 22)
	private Float lat;

	@Basic
	@Column(name = "LNG", length = 22)
	private Float lng;

	@Basic
	@Column(name = "OBJECT_ID", length = 22)
	private BigDecimal objectId;

	// 0: ghe tham,1: ket thuc ghe tham,2: cham trung bay,3: kiem hang ton, 4:
	// dat hang
	@Basic
	@Column(name = "OBJECT_TYPE", length = 22)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActionLogObjectType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActionLogObjectType objectType;

	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	@Basic
	@Column(name = "START_TIME", length = 7)
	private Date startTime;

	public ActionLog clone() {
		ActionLog obj = new ActionLog();

		obj.setId(id);
		obj.setCustomer(customer);
		obj.setEndTime(endTime);
		obj.setIsOr(isOr);
		obj.setLat(lat);
		obj.setLng(lng);
		obj.setObjectId(objectId);
		obj.setObjectType(objectType);
		obj.setStaff(staff);
		obj.setStartTime(startTime);

		return obj;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getIsOr() {
		return isOr;
	}

	public void setIsOr(Long isOr) {
		this.isOr = isOr;
	}

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLng() {
		return lng;
	}

	public void setLng(Float lng) {
		this.lng = lng;
	}

	public BigDecimal getObjectId() {
		return objectId;
	}

	public void setObjectId(BigDecimal objectId) {
		this.objectId = objectId;
	}

	public ActionLogObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(ActionLogObjectType objectType) {
		this.objectType = objectType;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "INVOICE_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "INVOICE_DETAIL_SEQ", allocationSize = 1)
public class InvoiceDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	//Id 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "INVOICE_DETAIL_ID")
	private Long id;

	//Ma san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;
	
	//So luong
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;
	
	//Gia ban da co VAT
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal priceValue;
	
	//So tien chiet khau
	@Basic
	@Column(name = "DISCOUNT_AMOUNT", length = 22)
	private BigDecimal discountAmount;

	//% chiet khau
	@Basic
	@Column(name = "DISCOUNT_PERCENT", length = 22)
	private Float discountPercent;
	
	//So tien
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;
	
	//0: hang ban; 1 khuyen mai
	@Basic
	@Column(name = "IS_FREE_ITEM", length = 22)
	private Integer isFreeItem = 0 ;
	
	//Thue VAT
	@Basic
	@Column(name = "VAT", length = 22)
	private Float vat;
	
	//Id gia ban
	@ManyToOne(targetEntity = Price.class)
	@JoinColumn(name = "PRICE_ID", referencedColumnName = "PRICE_ID")
	private Price price;
	
	@ManyToOne(targetEntity = SaleOrderDetail.class)
	@JoinColumn(name = "SALE_ORDER_DETAIL_ID", referencedColumnName = "SALE_ORDER_DETAIL_ID")
	private SaleOrderDetail saleOrderDetail;
	
	@ManyToOne(targetEntity = Invoice.class)
	@JoinColumn(name = "INVOICE_ID", referencedColumnName = "INVOICE_ID")
	private Invoice invoice;
	
	//Ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi tao
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Basic
	@Column(name = "PRICE_NOT_VAT", length = 22)
	private BigDecimal priceNotVat;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPriceValue() {
		return priceValue;
	}

	public void setPriceValue(BigDecimal priceValue) {
		this.priceValue = priceValue;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Float getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Float discountPercent) {
		this.discountPercent = discountPercent;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getIsFreeItem() {
		return isFreeItem;
	}

	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}

	public Float getVat() {
		return vat;
	}

	public void setVat(Float vat) {
		this.vat = vat;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public SaleOrderDetail getSaleOrderDetail() {
		return saleOrderDetail;
	}

	public void setSaleOrderDetail(SaleOrderDetail saleOrderDetail) {
		this.saleOrderDetail = saleOrderDetail;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public BigDecimal getPriceNotVat() {
		return priceNotVat;
	}

	public void setPriceNotVat(BigDecimal priceNotVat) {
		this.priceNotVat = priceNotVat;
	}
	
}
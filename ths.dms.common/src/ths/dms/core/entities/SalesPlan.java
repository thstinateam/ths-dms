package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "SALES_PLAN")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SALES_PLAN_SEQ", allocationSize = 1)
public class SalesPlan implements Serializable {

	private static final long serialVersionUID = 1L;
	//tong tien
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//ma nganh hang
	@Basic
	@Column(name = "CATEGORY_CODE", length = 50)
	private String categoryCode;

	//ngay tao
	@Basic
	@Column(name = "CREATE_TIME", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//chua dung
	@Basic
	@Column(name = "DESCRIPTION", length = 200)
	private String description;

	//tu ngay
	@Basic
	@Column(name = "FROM_DATE", length = 7)
	private Date fromDate;

	//mat hang trong tam: 0:ko trong tam, 1: trong tam
	@Basic
	@Column(name = "IS_FOCUS_ITEM", length = 22)
	private Integer isFocusItem;

	//id san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//so luong ban
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SALE_PLAN_ID", length = 22)
	private Long id;

	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//id nhan vien
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	//den ngay
	@Basic
	@Column(name = "TO_DATE", length = 7)
	private Date toDate;

	//ngay cap nhat
	@Basic
	@Column(name = "UPDATE_TIME", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Integer getIsFocusItem() {
		return isFocusItem;
	}

	public void setIsFocusItem(Integer isFocusItem) {
		this.isFocusItem = isFocusItem;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	
}
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum PlanTypeKpi {
	    NOT_APPLY (1),
	    
	    APPLY (2);
	    
	    /** The value. */
	    private Integer value;
	    
	    /** The values. */
	    private static Map<Integer, PlanTypeKpi> values = null;
	    
	    /**
	     * Gets the value.
	     * 
	     * @return the value
	     */
	    public Integer getValue() {
	        return value;
	    }
	    
	    /**
	     * Instantiates a new gender type.
	     * 
	     * @param value
	     *            the value
	     */
	    PlanTypeKpi(Integer value) {
	        this.value = value;
	    }
	    
	    /**
	     * Parses the value.
	     * 
	     * @param value
	     *            the value
	     * @return the gender type
	     */
	    public static PlanTypeKpi parseValue(Integer value) {
	        if (values == null) {
	            values = new HashMap<Integer, PlanTypeKpi>(
	            		PlanTypeKpi.values().length);
	            for (PlanTypeKpi e : PlanTypeKpi.values())
	                values.put(e.getValue(), e);
	        }
	        return values.get(value);
	    }
}

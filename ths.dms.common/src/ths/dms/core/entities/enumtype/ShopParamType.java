/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Mo ta class ShopParamType.java
 * @author vuongmq
 * @since Nov 28, 2015
 */
public enum ShopParamType {
    
    SYS_CONFIG("SYS_CONFIG"),
    
    ;
    
    
    /** The value. */
    private String value;
    
    /** The values. */
    private static Map<String, ShopParamType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ShopParamType(String value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ShopParamType parseValue(String value) {
        if (values == null) {
            values = new HashMap<String, ShopParamType>(ShopParamType.values().length);
            for (ShopParamType e : ShopParamType.values()) {
                values.put(e.getValue(), e);
            }
        }
        return values.get(value);
    }
}

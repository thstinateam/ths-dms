/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum GenderType.
 * 
 * @author huytran
 */
public enum OrderSaleType {
    
    /** PRE --> KH */
	SALE_RETURNED (0),
    
    /** KH --> PRE*/
    SALE_NOT_RETURNED (1);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, OrderSaleType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    OrderSaleType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static OrderSaleType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, OrderSaleType>(
            		OrderSaleType.values().length);
            for (OrderSaleType e : OrderSaleType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

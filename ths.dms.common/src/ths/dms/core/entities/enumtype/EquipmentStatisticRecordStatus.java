/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * trang thai cua bien ban kiem ke
 * @author tuannd20
 */
public enum EquipmentStatisticRecordStatus {
	DRAFT (0),
	
    RUNNING (1),
    
    DONE (2),
    
    DELETED (3)
    ;
    
    /** The value. */
    private Integer value;
    /** The values. */
    private static Map<Integer, EquipmentStatisticRecordStatus> values = null;
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    EquipmentStatisticRecordStatus(Integer value) {
        this.value = value;
    }
    /**
     * Parses the value.
     *
     * @author tuannd20
     * @param value the value
     * @return the gender type
     */
    public static EquipmentStatisticRecordStatus parseValue(Integer value) {
        if(value == null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, EquipmentStatisticRecordStatus>(EquipmentStatisticRecordStatus.values().length);
            for (EquipmentStatisticRecordStatus e : EquipmentStatisticRecordStatus.values()) {
            	values.put(e.getValue(), e);            	
            }
        }
        return values.get(value);
    }
}

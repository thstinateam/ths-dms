/**
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
 /**
 * @author vuongmq
 * @since 09/09/2015
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Dinh nghia loai chuc nang ghi log
 * 
 * @author hunglm16
 * @since August 13, 2015
 */
public enum FuncType {
	CREATE("CREATE"),
	READ("READ"),
	UPDATE("UPDATE"),
	DELETE("DELETE")
	;

	private String value;

	private static Map<String, FuncType> values = null;

	public String getValue() {
		return value;
	}

	FuncType(String value) {
		this.value = value;
	}

	public static FuncType parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, FuncType>(FuncType.values().length);
			for (FuncType e : FuncType.values())
				values.put(e.getValue(), e);
		}

		return values.get(value);
	}
}

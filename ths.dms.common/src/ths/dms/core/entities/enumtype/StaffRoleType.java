/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum StaffRoleType {
    
	NVBH_PRE(1),
	
	NVBH_ON_VAN (2),
	
	NVTT (3),
	
	NVGH(4),
	
	GSNPP(5),
	
	GDM(6),    
	
	TBHV(7),
	
	VNM(8),
	
	KTNPP(9),
	
	TNKD(10);

    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, StaffRoleType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    StaffRoleType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static StaffRoleType parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, StaffRoleType>(
                    StaffRoleType.values().length);
            for (StaffRoleType e : StaffRoleType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

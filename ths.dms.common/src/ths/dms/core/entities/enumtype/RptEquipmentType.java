/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum RptEquipmentType {
    
	FAIL (0),

	SUCCESS (1);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, RptEquipmentType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    RptEquipmentType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static RptEquipmentType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, RptEquipmentType>(
                    RptEquipmentType.values().length);
            for (RptEquipmentType e : RptEquipmentType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum TableHasAttribute {

	PRODUCT("PRODUCT", new String[] { "PRODUCT_CODE" }),

	CUSTOMER("CUSTOMER", new String[] { "CUSTOMER_CODE" }),

	STAFF("STAFF", new String[] { "STAFF_CODE" }),

	PROMOTION_PROGRAM("PROMOTION_PROGRAM",
			new String[] { "PROMOTION_PROGRAM_CODE" }),

	FOCUS_PROGRAM("FOCUS_PROGRAM", new String[] { "FOCUS_PROGRAM_CODE" }),

	DISPLAY_PROGRAM("DISPLAY_PROGRAM", new String[] { "DISPLAY_PROGRAM_CODE" }),

	DISPLAY_GROUP("DISPLAY_GROUP", new String[] { "DISPLAY_PROGRAM_CODE", "DISPLAY_GROUP_CODE" }),

	DISPLAY_PROGRAM_LEVEL("DISPLAY_PROGRAM_LEVEL", new String[] {
			"DISPLAY_PROGRAM_CODE", "LEVEL_CODE" }),

	INCENTIVE_PROGRAM("INCENTIVE_PROGRAM",
			new String[] { "INCENTIVE_PROGRAM_CODE" }),

	INCENTIVE_PROGRAM_LEVEL("INCENTIVE_PROGRAM_LEVEL", new String[] {
			"INCENTIVE_PROGRAM_CODE", "LEVEL_CODE" }),
	
	DP_PAY_PERIOD_RESULT("DP_PAY_PERIOD_RESULT", new String[] {
			"SHOP_CODE", "CUSTOMER_CODE", "LEVEL_CODE" });

	private String tableName;
	private String[] tableKey;

	private static Map<String, TableHasAttribute> values = null;

	public String getTableName() {
		return tableName;
	}

	public String[] getTableKey() {
		return this.tableKey;
	}

	private TableHasAttribute(String value, String[] tableKey) {
		this.tableName = value;
		this.tableKey = tableKey;
	}

	public static TableHasAttribute parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, TableHasAttribute>(
					TableHasAttribute.values().length);

			for (TableHasAttribute e : TableHasAttribute.values()) {
				values.put(e.getTableName(), e);
			}
		}

		return values.get(value);
	}

	public boolean isEqual(TableHasAttribute item) {
		return this.getTableName().equalsIgnoreCase(item.getTableName());
	}
}
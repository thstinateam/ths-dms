/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum RelationType {
    
    AND (0),

    OR (1);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, RelationType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    RelationType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static RelationType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, RelationType>(
                    RelationType.values().length);
            for (RelationType e : RelationType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

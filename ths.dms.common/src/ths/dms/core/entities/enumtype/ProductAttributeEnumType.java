/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum ProductAttributeEnumType {
    
	YES (1),

    NO (0);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ProductAttributeEnumType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ProductAttributeEnumType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ProductAttributeEnumType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ProductAttributeEnumType>(
                    ProductAttributeEnumType.values().length);
            for (ProductAttributeEnumType e : ProductAttributeEnumType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

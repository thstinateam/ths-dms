/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum PoDvStatus {
    
    CHUA_CO_PO_CONFIRM (0),

    DANG_XUAT_PO_CONFIRM (1),
    
    HOAN_TAT (2),
    
    TREO (3);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, PoDvStatus> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    PoDvStatus(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static PoDvStatus parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, PoDvStatus>(
                    PoDvStatus.values().length);
            for (PoDvStatus e : PoDvStatus.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

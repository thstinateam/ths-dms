/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum PromotionCustomerMapType {
    
    APPLY_TO_CUSTOMER_TYPE (1),

    APPLY_TO_CUSTOMER (2);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, PromotionCustomerMapType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    PromotionCustomerMapType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static PromotionCustomerMapType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, PromotionCustomerMapType>(
                    PromotionCustomerMapType.values().length);
            for (PromotionCustomerMapType e : PromotionCustomerMapType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

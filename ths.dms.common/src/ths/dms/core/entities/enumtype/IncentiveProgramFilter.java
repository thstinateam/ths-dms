/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;



public class IncentiveProgramFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6492223311164902482L;

	private String programCode;
	
	private String programName;
	
	private IncentiveType incentiveType;
	
	private Long shopId;
	
	private ApplyType applyType;
	
	private Date fromDate;
	
	private Date toDate;
	
	private ActiveType status;
	
	private String shopCode;
	
	public String getProgramCode() {
		return programCode;
	}
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	public IncentiveType getIncentiveType() {
		return incentiveType;
	}
	public void setIncentiveType(IncentiveType incentiveType) {
		this.incentiveType = incentiveType;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public ApplyType getApplyType() {
		return applyType;
	}
	public void setApplyType(ApplyType applyType) {
		this.applyType = applyType;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	/**
	 * @return the shopCode
	 */
	public String getShopCode() {
		return shopCode;
	}
	/**
	 * @param shopCode the shopCode to set
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	
}

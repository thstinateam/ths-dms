/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Dinh nghia loai chuc nang ghi log
 * 
 * @author hunglm16
 * @since August 13, 2015
 */
public enum AppLogTypeEnum {
	LOGIN(1), LOGOUT(2), FORM(3), REPORT(4);

	private Integer value;

	/** The values. */
	private static Map<Integer, AppLogTypeEnum> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	AppLogTypeEnum(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * @author hunglm16
	 * @param value the value
	 * @return the gender type
	 * @since August 13, 2015
	 */
	public static AppLogTypeEnum parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, AppLogTypeEnum>(AppLogTypeEnum.values().length);
			for (AppLogTypeEnum e : AppLogTypeEnum.values()) {
				values.put(e.getValue(), e);
			}
		}
		return values.get(value);
	}
}

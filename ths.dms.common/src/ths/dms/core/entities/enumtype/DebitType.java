/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum DebitType {
    THU (0),
    
    CHI (1);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, DebitType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    DebitType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static DebitType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, DebitType>(
                    DebitType.values().length);
            for (DebitType e : DebitType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

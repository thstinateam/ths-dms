/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum VisitPlanType.
 * 
 * @author huytran
 */
public enum TreeVOType {
    
    CATEGORY (0),

    SUB_CATEGORY (1),
    
    PRODUCT (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, TreeVOType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    TreeVOType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static TreeVOType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, TreeVOType>(
                    TreeVOType.values().length);
            for (TreeVOType e : TreeVOType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ProductType.
 * 
 * @author huytran
 */
public enum ProductType {
    
    
    CAT (1),
    
    SUB_CAT (2),
    
    BRAND (3),
    
    FLAVOUR (4),
    
    PACKING (5),
    
    PRODUCT (6),
    
    SECOND_SUB_CAT (7);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ProductType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ProductType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ProductType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ProductType>(
                    ProductType.values().length);
            for (ProductType e : ProductType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.vo.PoAutoGroupShopMapVO;



public class PoAutoGroupShopMapFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6974321976035585940L;

	private KPaging<PoAutoGroupShopMapVO> kPaging;
	private String shopCode;
	private String shopName;
	private List<Long> lstParentId;
	private ActiveType status;
	private String taxNum; 
	private String invoiceNumberAccount;
	private String phone;
	private String mobilePhone;
	private String email;
	private Long shopTypeId;
	private Boolean isGetOneChildLevel;
	private List<Integer> lstObjectType;
	
	
	public KPaging<PoAutoGroupShopMapVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<PoAutoGroupShopMapVO> kPaging) {
		this.kPaging = kPaging;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public List<Long> getLstParentId() {
		return lstParentId;
	}
	public void setLstParentId(List<Long> lstParentId) {
		this.lstParentId = lstParentId;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public String getTaxNum() {
		return taxNum;
	}
	public void setTaxNum(String taxNum) {
		this.taxNum = taxNum;
	}
	public String getInvoiceNumberAccount() {
		return invoiceNumberAccount;
	}
	public void setInvoiceNumberAccount(String invoiceNumberAccount) {
		this.invoiceNumberAccount = invoiceNumberAccount;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getShopTypeId() {
		return shopTypeId;
	}
	public void setShopTypeId(Long shopTypeId) {
		this.shopTypeId = shopTypeId;
	}
	public Boolean getIsGetOneChildLevel() {
		return isGetOneChildLevel;
	}
	public void setIsGetOneChildLevel(Boolean isGetOneChildLevel) {
		this.isGetOneChildLevel = isGetOneChildLevel;
	}
	public List<Integer> getLstObjectType() {
		return lstObjectType;
	}
	public void setLstObjectType(List<Integer> lstObjectType) {
		this.lstObjectType = lstObjectType;
	}
	
}

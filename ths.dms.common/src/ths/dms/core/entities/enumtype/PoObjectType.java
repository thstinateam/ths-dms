/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum PoObjectType {
    
    NCC (0),

    NPP (1);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, PoObjectType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    PoObjectType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static PoObjectType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, PoObjectType>(
                    PoObjectType.values().length);
            for (PoObjectType e : PoObjectType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

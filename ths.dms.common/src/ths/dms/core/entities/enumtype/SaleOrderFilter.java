/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



public class SaleOrderFilter<T> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private KPaging<T> kPaging;
	
	private OrderByType orderByType;
	private OrderType orderType;

	private SaleOrderStatus approved;
	private List<SaleOrderStatus> lstApprovedStatus;

	private List<Long> listSaleOrderId = new ArrayList<Long>();
	private List<Long> lstFromSaleOrderId = new ArrayList<Long>();
	
	private Boolean isCompareEqual; 
	private Boolean getBothINandSO;
	
	private Long saleOrderId;
	private Long customerId;
	private Long staffId;
	private Long shopId;
	private Long programId;
	private Long productId;
	private Long userId;
	private Long roleId;
	
	private Integer status;
	private Integer type;
	
	private Date fromDate;
	private Date toDate;
	private Date orderDate;
	private Date lockDate;
	
	private String shopCode;
	private String saleOderNumber;
	private String orderTypeStr;
	private String isJoinSaleOderNumber;
	private String fromDateStr;
	private String toDateStr;
	private String staffCode;
	private String deliveryCode;
	private String shortCode;
	private String customerName;
	private String orderNumber;
	/*
	 * number of day different from now
	 */
	private Integer diffDaysFromDate;
	
	/*
	 * anchor date to check date different
	 */
	private Date anchorDateDiff;
	
	private Boolean isPositiveAmountSaleOrder;
	
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public Long getSaleOrderId() {
		return saleOrderId;
	}
	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getSaleOderNumber() {
		return saleOderNumber;
	}
	public void setSaleOderNumber(String saleOderNumber) {
		this.saleOderNumber = saleOderNumber;
	}
	public String getOrderTypeStr() {
		return orderTypeStr;
	}
	public void setOrderTypeStr(String orderTypeStr) {
		this.orderTypeStr = orderTypeStr;
	}
	public String getIsJoinSaleOderNumber() {
		return isJoinSaleOderNumber;
	}
	public void setIsJoinSaleOderNumber(String isJoinSaleOderNumber) {
		this.isJoinSaleOderNumber = isJoinSaleOderNumber;
	}
	public String getFromDateStr() {
		return fromDateStr;
	}
	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}
	public String getToDateStr() {
		return toDateStr;
	}
	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}
	
	public List<Long> getListSaleOrderId() {
		return listSaleOrderId;
	}
	public void setListSaleOrderId(List<Long> listSaleOrderId) {
		this.listSaleOrderId = listSaleOrderId;
	}
	public OrderByType getOrderByType() {
		return orderByType;
	}
	public void setOrderByType(OrderByType orderByType) {
		this.orderByType = orderByType;
	}
	public Boolean getGetBothINandSO() {
		return getBothINandSO;
	}
	public void setGetBothINandSO(Boolean getBothINandSO) {
		this.getBothINandSO = getBothINandSO;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public SaleOrderStatus getApproved() {
		return approved;
	}
	public void setApproved(SaleOrderStatus approved) {
		this.approved = approved;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	private Integer state;

	public Boolean getIsCompareEqual() {
		return isCompareEqual;
	}
	public void setIsCompareEqual(Boolean isCompareEqual) {
		this.isCompareEqual = isCompareEqual;
	}
	public List<SaleOrderStatus> getLstApprovedStatus() {
		return lstApprovedStatus;
	}
	public void setLstApprovedStatus(List<SaleOrderStatus> lstApprovedStatus) {
		this.lstApprovedStatus = lstApprovedStatus;
	}
	public Integer getDiffDaysFromDate() {
		return diffDaysFromDate;
	}
	public void setDiffDaysFromDate(Integer diffDaysFromDate) {
		this.diffDaysFromDate = diffDaysFromDate;
	}
	public Date getAnchorDateDiff() {
		return anchorDateDiff;
	}
	public void setAnchorDateDiff(Date anchorDateDiff) {
		this.anchorDateDiff = anchorDateDiff;
	}
	public Boolean getIsPositiveAmountSaleOrder() {
		return isPositiveAmountSaleOrder;
	}
	public void setIsPositiveAmountSaleOrder(Boolean isPositiveAmountSaleOrder) {
		this.isPositiveAmountSaleOrder = isPositiveAmountSaleOrder;
	}
	public Long getProgramId() {
		return programId;
	}
	public void setProgramId(Long programId) {
		this.programId = programId;
	}
	public List<Long> getLstFromSaleOrderId() {
		return lstFromSaleOrderId;
	}
	public void setLstFromSaleOrderId(List<Long> lstFromSaleOrderId) {
		this.lstFromSaleOrderId = lstFromSaleOrderId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Date getLockDate() {
		return lockDate;
	}
	public void setLockDate(Date lockDate) {
		this.lockDate = lockDate;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * enum nay dung cho shop type code
 * @author phut
 */
public enum ShopChannelType {
    
    VNM ("VNM"),
    MIEN("MIEN"),
    VUNG("VUNG"),
    NPP("NPP");
    
    /** The value. */
    private String value;
    
    /** The values. */
    private static Map<String, ShopChannelType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ShopChannelType(String value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ShopChannelType parseValue(String value) {
        if (values == null) {
            values = new HashMap<String, ShopChannelType>(
                    ShopChannelType.values().length);
            for (ShopChannelType e : ShopChannelType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

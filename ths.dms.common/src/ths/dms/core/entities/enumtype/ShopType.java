/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum SaleObjectType.
 * 
 * @author huytran
 */
public enum ShopType {
    
	/** vinamilk */
    VINAMILK (0),

    /** mien */
    REGION(1),
    
    /** vung */
    AREA(2),
    
    /** NPP */
    SHOP(3),
    
    /** NVBH */
    SALEMAN(4),
    
    MT_STORE(4),
    
    MT_VUNG(5),
    
    MT_MIEN(6),
    
    MT_VNM(7)
    ;
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ShopType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ShopType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ShopType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ShopType>(
                    ShopType.values().length);
            for (ShopType e : ShopType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

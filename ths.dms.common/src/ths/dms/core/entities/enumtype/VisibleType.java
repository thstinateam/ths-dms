/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum VisibleType {
    
    VISIBLE (1),

    INVISIBLE (0);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, VisibleType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    VisibleType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static VisibleType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, VisibleType>(
                    VisibleType.values().length);
            for (VisibleType e : VisibleType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum EquipStockTotalType {

	KHO(1),

	KHO_KH(2),

	KHO_TONG(0);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, EquipStockTotalType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 */
	EquipStockTotalType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 * @return the gender type
	 */
	public static EquipStockTotalType parseValue(Integer value) {
		if (value != null && value == -1) {
			value = -2;
		}
		if (values == null) {
			values = new HashMap<Integer, EquipStockTotalType>(EquipStockTotalType.values().length);
			for (EquipStockTotalType e : EquipStockTotalType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai tinh trang quy dinh.
	 * 
	 * @author hunglm16
	 * @param value
	 * @return flag
	 */
	public static boolean checkIsRecode(Integer value) {
    	if(value == null){
    		return false;
    	}
    	if(!EquipStockTotalType.KHO_TONG.getValue().equals(value) && !EquipStockTotalType.KHO.getValue().equals(value)
				&& !EquipStockTotalType.KHO_KH.getValue().equals(value)){
    		return false;
    	}
        return true;
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum StaffRole {
	VNM_SALESONLINE_HO ("VNM_SALESONLINE_HO"),
	
	VNM_SALESONLINE_GS ("VNM_SALESONLINE_GS"),
	
	VNM_SALESONLINE_DISTRIBUTOR ("VNM_SALESONLINE_DISTRIBUTOR");
	
	/** The value. */
	private String value;

	/** The values. */
	private static Map<String, StaffRole> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	StaffRole(String value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static StaffRole parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, StaffRole>(StaffRole.values().length);
			for (StaffRole e : StaffRole.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

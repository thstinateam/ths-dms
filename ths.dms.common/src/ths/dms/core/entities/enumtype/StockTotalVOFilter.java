/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.List;



public class StockTotalVOFilter implements Serializable{

	/**
	 * @author hunglm16
	 * @since 16-10-2013
	 */
	private static final long serialVersionUID = 6974321976035585940L;

	private String productName;
	private String promotionProgramCode;
	private Long ownerId;
	private StockObjectType ownerType;
	private Long catId;
	private Long subCatId;
	private Integer fromQuantity;
	private Integer toQuantity;
	private Long cycleCountId;
	private String productCode;
	private List<Long> lstProductId;
	private List<Long> exceptProductId;
	private Long staffSalerId;//nvbhId
	private Long shopId;
	
	private Long productId;
	private Long warehouseId;
	private Long stockTotalId;
	private ActiveType status;
	
	private WarehouseType orderByType;
	
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public List<Long> getLstProductId() {
		return lstProductId;
	}
	public void setLstProductId(List<Long> lstProductId) {
		this.lstProductId = lstProductId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public List<Long> getExceptProductId() {
		return exceptProductId;
	}
	public void setExceptProductId(List<Long> exceptProductId) {
		this.exceptProductId = exceptProductId;
	}
	public Long getStaffSalerId() {
		return staffSalerId;
	}
	public void setStaffSalerId(Long staffSalerId) {
		this.staffSalerId = staffSalerId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}
	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public StockObjectType getOwnerType() {
		return ownerType;
	}
	public void setOwnerType(StockObjectType ownerType) {
		this.ownerType = ownerType;
	}
	public Long getCatId() {
		return catId;
	}
	public void setCatId(Long catId) {
		this.catId = catId;
	}
	public Long getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(Long subCatId) {
		this.subCatId = subCatId;
	}
	public Integer getFromQuantity() {
		return fromQuantity;
	}
	public void setFromQuantity(Integer fromQuantity) {
		this.fromQuantity = fromQuantity;
	}
	public Integer getToQuantity() {
		return toQuantity;
	}
	public void setToQuantity(Integer toQuantity) {
		this.toQuantity = toQuantity;
	}
	public Long getCycleCountId() {
		return cycleCountId;
	}
	public void setCycleCountId(Long cycleCountId) {
		this.cycleCountId = cycleCountId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}
	public Long getStockTotalId() {
		return stockTotalId;
	}
	public void setStockTotalId(Long stockTotalId) {
		this.stockTotalId = stockTotalId;
	}
	public WarehouseType getOrderByType() {
		return orderByType;
	}
	public void setOrderByType(WarehouseType orderByType) {
		this.orderByType = orderByType;
	}
}

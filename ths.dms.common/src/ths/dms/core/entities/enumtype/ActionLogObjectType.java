/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum ActionLogObjectType {

	VISIT(0),

	END_VISIT(1),

	MARK_DISPLAY(2),

	CYCLE_COUNT(3),

	ORDER(4);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, ActionLogObjectType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	ActionLogObjectType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static ActionLogObjectType parseValue(Integer value) {
		if (value != null && value == -1) {
			value = -2;
		}
		if (values == null) {
			values = new HashMap<Integer, ActionLogObjectType>(
					ActionLogObjectType.values().length);
			for (ActionLogObjectType e : ActionLogObjectType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

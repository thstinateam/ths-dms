package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.List;

public class AttributeDynamicVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long attributeId;
	private String attributeCode;
	private String attributeName;
	private Integer applyObject;
	private String description;
	private Integer type;
	private Integer displayOrder;
	private Integer dataLength;
	private Integer isEnumeration;
	private Integer isSearch;
	private Integer mandatory;
	private Integer maxValue;
	private Integer minValue;
	private Integer status;
	private List<AttributeEnumVO> attributeEnumVOs;
	private List<AttributeDetailVO> attributeDetailVOs;

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
	public Integer getIsEnumeration() {
		return isEnumeration;
	}
	public void setIsEnumeration(Integer isEnumeration) {
		this.isEnumeration = isEnumeration;
	}
	public Integer getIsSearch() {
		return isSearch;
	}
	public void setIsSearch(Integer isSearch) {
		this.isSearch = isSearch;
	}
	public Integer getMandatory() {
		return mandatory;
	}
	public void setMandatory(Integer mandatory) {
		this.mandatory = mandatory;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}
	public String getAttributeCode() {
		return attributeCode;
	}
	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public Integer getApplyObject() {
		return applyObject;
	}
	public void setApplyObject(Integer applyObject) {
		this.applyObject = applyObject;
	}
	public Integer getDataLength() {
		return dataLength;
	}
	public void setDataLength(Integer dataLength) {
		this.dataLength = dataLength;
	}
	public Integer getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(Integer maxValue) {
		this.maxValue = maxValue;
	}
	public Integer getMinValue() {
		return minValue;
	}
	public void setMinValue(Integer minValue) {
		this.minValue = minValue;
	}
	public List<AttributeDetailVO> getAttributeDetailVOs() {
		return attributeDetailVOs;
	}
	public void setAttributeDetailVOs(List<AttributeDetailVO> attributeDetailVOs) {
		this.attributeDetailVOs = attributeDetailVOs;
	}
	public List<AttributeEnumVO> getAttributeEnumVOs() {
		return attributeEnumVOs;
	}
	public void setAttributeEnumVOs(List<AttributeEnumVO> attributeEnumVOs) {
		this.attributeEnumVOs = attributeEnumVOs;
	}
}
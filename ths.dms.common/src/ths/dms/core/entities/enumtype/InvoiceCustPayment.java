/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum InvoiceCustPayment {
    
    MONEY (1),

    BANK_TRANSFER (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, InvoiceCustPayment> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    InvoiceCustPayment(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static InvoiceCustPayment parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, InvoiceCustPayment>(
                    InvoiceCustPayment.values().length);
            for (InvoiceCustPayment e : InvoiceCustPayment.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

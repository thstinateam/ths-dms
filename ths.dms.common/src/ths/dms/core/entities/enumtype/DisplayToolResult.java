/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum DisplayToolResult {
    
    ZERO (0),

    PASSED (1),
    
    NOT_PASSED (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, DisplayToolResult> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    DisplayToolResult(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static DisplayToolResult parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, DisplayToolResult>(
                    DisplayToolResult.values().length);
            for (DisplayToolResult e : DisplayToolResult.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
    
}

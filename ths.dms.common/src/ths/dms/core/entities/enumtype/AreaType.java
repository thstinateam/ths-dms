/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum AreaType {
    
    COUNTRY (0),

    PROVINCE (1),
    
    DISTRICT (2),
    
    WARD (3);
    
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, AreaType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    AreaType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static AreaType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, AreaType>(
                    AreaType.values().length);
            for (AreaType e : AreaType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * Khai bao Import thu vien
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Status Records Equipment.
 * 
 * @author nhutnn
 * @since 06/07/2015
 * @description 0:  Chua xuat, 1: Da xuat
 */
public enum StatusDeliveryEquip {

	NOT_EXPORT(0),// Chua xuat

	EXPORTED(1) // Da xuat

	;

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, StatusDeliveryEquip> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	StatusDeliveryEquip(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author hunglm16
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static StatusDeliveryEquip parseValue(Integer value) {
		if (value == null) {
			value = -1;
		}
		if (values == null) {
			values = new HashMap<Integer, StatusDeliveryEquip>(StatusDeliveryEquip.values().length);
			for (StatusDeliveryEquip e : StatusDeliveryEquip.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai tinh trang quy dinh.
	 * 
	 * @author hunglm16
	 * @param value
	 * @return flag
	 */
	public static boolean checkIsRecode(Integer value) {
		if (value == null) {
			return false;
		}
		if (!StatusDeliveryEquip.NOT_EXPORT.getValue().equals(value) && !StatusDeliveryEquip.EXPORTED.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

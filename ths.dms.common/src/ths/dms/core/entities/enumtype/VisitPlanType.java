/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum VisitPlanType.
 * 
 * @author huytran
 */
public enum VisitPlanType {
    
    NOTGO (0),

    GO (1);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, VisitPlanType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    VisitPlanType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static VisitPlanType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, VisitPlanType>(
                    VisitPlanType.values().length);
            for (VisitPlanType e : VisitPlanType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

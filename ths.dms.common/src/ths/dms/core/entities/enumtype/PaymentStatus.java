/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author hunglm16
 * @description Quy dinh tinh trang tren toan bo he thong SO
 */
public enum PaymentStatus {
    
	DELETED(-1),
	
    NOT_PAID_YET (0), // Chua thanh toan
    
    PAID (1), // Da thanh toan
    
    RETURN_PAID (2) // da dao thanh toan
    ;
    
    /** The value. */
    private Integer value;
    /** The values. */
    private static Map<Integer, PaymentStatus> values = null;
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    PaymentStatus(Integer value) {
        this.value = value;
    }
    /**
     * Parses the value.
     * 
     * @author hunglm16
     * @param value the value
     * @return the gender type
     */
    public static PaymentStatus parseValue(Integer value) {
        if(value==null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, PaymentStatus>(PaymentStatus.values().length);
            for (PaymentStatus e : PaymentStatus.values()) {
                values.put(e.getValue(), e);
            }
        }
        return values.get(value);
    }
}
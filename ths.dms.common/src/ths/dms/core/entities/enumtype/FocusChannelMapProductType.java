/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum FocusChannelMapProductType {
    
    MHTT1 (1),

    MHTT2 (2);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, FocusChannelMapProductType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    FocusChannelMapProductType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static FocusChannelMapProductType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, FocusChannelMapProductType>(
                    FocusChannelMapProductType.values().length);
            for (FocusChannelMapProductType e : FocusChannelMapProductType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

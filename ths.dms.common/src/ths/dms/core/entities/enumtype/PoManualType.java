/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Mo ta class PoManualType.java
 * @author vuongmq
 * @since Dec 8, 2015
 */
public enum PoManualType {

	PO_IMPORT(1), // PO nhap

	PO_RETURN(2); // PO tra
	
	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, PoManualType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	PoManualType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static PoManualType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, PoManualType>(PoManualType.values().length);
			for (PoManualType e : PoManualType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
	
	/**
	 * Kiem tra co ton tai theo quy dinh.
	 * 
	 * Xu ly checkPoManualStatus
	 * @author vuongmq
	 * @param value
	 * @return boolean
	 * @since Dec 8, 2015
	 */
	public static boolean checkPoManualStatus(Integer value) {
		if (value == null) {
			return false;
		}
		if (!PoManualType.PO_IMPORT.getValue().equals(value) && !PoManualType.PO_RETURN.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

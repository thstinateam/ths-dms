/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum Under Shop
 * 
 * @author loctt1
 */
public enum UnderShop {
    
    VINAMILK (0),
	
	OTHER_COMPANY (1);

    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, UnderShop> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a under shop
     * 
     * @param value
     *            the value
     */
    UnderShop(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the under shop
     */
    public static UnderShop parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, UnderShop>(
                    UnderShop.values().length);
            for (UnderShop e : UnderShop.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;

import java.io.Serializable;

public class AttributeDetailVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long attributeId;
	private Long id;
	private String code;
	private String name;
	private int status;
	private Long enumId;
	//sontt them field de xu ly tren web:
	private boolean checked;
	private String value;
	//
	
	public Long getId() {
		return id;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Long getEnumId() {
		return enumId;
	}
	public void setEnumId(Long enumId) {
		this.enumId = enumId;
	}
	
}

package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.vo.PrintOrderVO;

public class PrintOrderFilter implements Serializable {
	public Long shopId;
	public Date lockDay;
	public String customerCode;
	public String customeName;
	public String refOrderNumber;
	public OrderType orderType;
	public Date fromDate;
	public Date toDate;
	public String staffCode;
	public String deliveryCode;
	public SaleOrderSource orderSource;
	public KPaging<PrintOrderVO> kPaging;
	private Integer isValueOrder;
	private Long numberValueOrder;
	private String strListUserId;
	
	public Integer getIsValueOrder() {
		return isValueOrder;
	}
	public void setIsValueOrder(Integer isValueOrder) {
		this.isValueOrder = isValueOrder;
	}
	public Long getNumberValueOrder() {
		return numberValueOrder;
	}
	public void setNumberValueOrder(Long numberValueOrder) {
		this.numberValueOrder = numberValueOrder;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Date getLockDay() {
		return lockDay;
	}
	public void setLockDay(Date lockDay) {
		this.lockDay = lockDay;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomeName() {
		return customeName;
	}
	public void setCustomeName(String customeName) {
		this.customeName = customeName;
	}
	public String getRefOrderNumber() {
		return refOrderNumber;
	}
	public void setRefOrderNumber(String refOrderNumber) {
		this.refOrderNumber = refOrderNumber;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public SaleOrderSource getOrderSource() {
		return orderSource;
	}
	public void setOrderSource(SaleOrderSource orderSource) {
		this.orderSource = orderSource;
	}
	public KPaging<PrintOrderVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<PrintOrderVO> kPaging) {
		this.kPaging = kPaging;
	}
	public String getStrListUserId() {
		return strListUserId;
	}
	public void setStrListUserId(String strListUserId) {
		this.strListUserId = strListUserId;
	}
}

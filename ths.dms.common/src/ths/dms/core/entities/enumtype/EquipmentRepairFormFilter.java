package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Filter dung chung cho lich su sua chua thiet bi
 * 
 * @author hunglm16
 * @since December 20,2014
 */
public class EquipmentRepairFormFilter<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	private KPaging<T> kPaging;
	private Long id;
	private Long equipId;
	private Long stockId;
	private Long equipPeriodId;
	private Long equipRepairFormDtlId;
	private Long equipItemId;
	
	private String equipItemCode;
	private String equipItemName;
	
	private Integer warranty;
	private Integer quantity;
	private Integer status;
	private Integer repairCount;
	
	private BigDecimal materialPrice;
	private BigDecimal workerPrice;
	private BigDecimal totalAmount;
	
	private String completeDateStr;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public String getCompleteDateStr() {
		return completeDateStr;
	}
	public void setCompleteDateStr(String completeDateStr) {
		this.completeDateStr = completeDateStr;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEquipId() {
		return equipId;
	}
	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	public Long getEquipPeriodId() {
		return equipPeriodId;
	}
	public void setEquipPeriodId(Long equipPeriodId) {
		this.equipPeriodId = equipPeriodId;
	}
	public Long getEquipRepairFormDtlId() {
		return equipRepairFormDtlId;
	}
	public void setEquipRepairFormDtlId(Long equipRepairFormDtlId) {
		this.equipRepairFormDtlId = equipRepairFormDtlId;
	}
	public Long getEquipItemId() {
		return equipItemId;
	}
	public void setEquipItemId(Long equipItemId) {
		this.equipItemId = equipItemId;
	}
	public String getEquipItemCode() {
		return equipItemCode;
	}
	public void setEquipItemCode(String equipItemCode) {
		this.equipItemCode = equipItemCode;
	}
	public String getEquipItemName() {
		return equipItemName;
	}
	public void setEquipItemName(String equipItemName) {
		this.equipItemName = equipItemName;
	}
	public Integer getWarranty() {
		return warranty;
	}
	public void setWarranty(Integer warranty) {
		this.warranty = warranty;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getMaterialPrice() {
		return materialPrice;
	}
	public void setMaterialPrice(BigDecimal materialPrice) {
		this.materialPrice = materialPrice;
	}
	public BigDecimal getWorkerPrice() {
		return workerPrice;
	}
	public void setWorkerPrice(BigDecimal workerPrice) {
		this.workerPrice = workerPrice;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Integer getRepairCount() {
		return repairCount;
	}
	public void setRepairCount(Integer repairCount) {
		this.repairCount = repairCount;
	}
}

/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Cac loai doi tuong
 * @author tuannd20
 */
public enum ProcessHistoryType {
	SALE_ORDER(1),
	PO(2),
	STOCK_TRANS(3)
    ;
    
    /** The value. */
    private Integer value;
    /** The values. */
    private static Map<Integer, ProcessHistoryType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    ProcessHistoryType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @author tuannd20
     * @param value the value
     * @return the gender type
     */
    public static ProcessHistoryType parseValue(Integer value) {
        if(value==null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, ProcessHistoryType>(
                    ProcessHistoryType.values().length);
            for (ProcessHistoryType e : ProcessHistoryType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

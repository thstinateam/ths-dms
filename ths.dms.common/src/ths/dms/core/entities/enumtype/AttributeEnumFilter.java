package ths.dms.core.entities.enumtype;

import java.io.Serializable;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffAttribute;

public class AttributeEnumFilter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long attributeId;
	
	private String code;
	
	private String value;

	private ActiveType status;
	
	private Staff staff;
	
	private StaffAttribute staffAttribute;
	
	private Integer applyObject;
	
	private String order;
	
	private String sort;
	
	private ActiveType attStatus;
	
	
	

	public ActiveType getAttStatus() {
		return attStatus;
	}

	public void setAttStatus(ActiveType attStatus) {
		this.attStatus = attStatus;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public StaffAttribute getStaffAttribute() {
		return staffAttribute;
	}

	public void setStaffAttribute(StaffAttribute staffAttribute) {
		this.staffAttribute = staffAttribute;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}

	public Integer getApplyObject() {
		return applyObject;
	}

	public void setApplyObject(Integer applyObject) {
		this.applyObject = applyObject;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	

}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum RoutingCustomerType.
 * 
 * @author huytran
 */
public enum RoutingCustomerType {
    
    NOTGO (0),

    GO (1);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, RoutingCustomerType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    RoutingCustomerType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static RoutingCustomerType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, RoutingCustomerType>(
                    RoutingCustomerType.values().length);
            for (RoutingCustomerType e : RoutingCustomerType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

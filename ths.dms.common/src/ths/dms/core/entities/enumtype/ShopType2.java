/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum Shop Type 2.
 * 
 * @author loctt1
 */
public enum ShopType2 {
    
    HIGH_CLASS (0),
	
	GENERAL (1);

    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ShopType2> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new shop rank
     * 
     * @param value
     *            the value
     */
    ShopType2(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the shop location
     */
    public static ShopType2 parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ShopType2>(
            		ShopType2.values().length);
            for (ShopType2 e : ShopType2.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

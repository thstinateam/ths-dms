/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum PlanType.
 * 
 * @author huytran
 */
public enum PlanType {
    
    YEAR (1),
    
    MONTH (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, PlanType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    PlanType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static PlanType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, PlanType>(
                    PlanType.values().length);
            for (PlanType e : PlanType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum Ob.
 * 
 * @author vuongmq
 */
public enum ObjectReportType {
    
	VNM (0),
	
    NPP (1);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ObjectReportType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ObjectReportType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ObjectReportType parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, ObjectReportType>(
                    ObjectReportType.values().length);
            for (ObjectReportType e : ObjectReportType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

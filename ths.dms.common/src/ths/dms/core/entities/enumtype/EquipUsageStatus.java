/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum EquipUsageStatus.
 * 
 * @author hunglm16
 * @description 0: Da mat, 1: Da thanh ly, 2: Dang o kho, 3: Dang su dung, 4: Dang sua chua
 */
public enum EquipUsageStatus {

	LOST(0), // Da mat

	LIQUIDATED(1), // Da thanh ly

	SHOWING_WAREHOUSE(2), // Dang o kho

	IS_USED(3), // Dang su dung

	SHOWING_REPAIR(4) //Dang sua chua
	;

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, EquipUsageStatus> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	EquipUsageStatus(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author hunglm16
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static EquipUsageStatus parseValue(Integer value) {
		if (value == null) {
			value = -1;
		}
		if (values == null) {
			values = new HashMap<Integer, EquipUsageStatus>(EquipUsageStatus.values().length);
			for (EquipUsageStatus e : EquipUsageStatus.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai tinh trang quy dinh.
	 * 
	 * @author hunglm16
	 * @param value
	 * @return flag
	 */
	public static boolean checkIsRecode(Integer value) {
		if (value == null) {
			return false;
		}
		if (!EquipUsageStatus.LOST.getValue().equals(value) && !EquipUsageStatus.LIQUIDATED.getValue().equals(value) && !EquipUsageStatus.SHOWING_WAREHOUSE.getValue().equals(value) && !EquipUsageStatus.IS_USED.getValue().equals(value)
				&& !EquipUsageStatus.SHOWING_REPAIR.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liemtpt
 *
 */
public enum WorkingDateProcedureType {
	THIET_LAP_RIENG(1),
	THUA_KE_CHA(2),
	THEM_NGAY_NGAY_NGHI(3),
	XOA_NGAY_NGAY_NGHI(4);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, WorkingDateProcedureType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    WorkingDateProcedureType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static WorkingDateProcedureType parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, WorkingDateProcedureType>(
                    WorkingDateProcedureType.values().length);
            for (WorkingDateProcedureType e : WorkingDateProcedureType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

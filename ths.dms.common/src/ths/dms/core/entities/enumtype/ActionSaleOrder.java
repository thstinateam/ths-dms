/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Mo ta class ActionSaleOrder.java
 * @author vuongmq
 * @since Jan 8, 2016
 */
public enum ActionSaleOrder {
    
    INSERT (1),

    UPDATE (2),

    REJECTED (3),
    
    CANCEL (4),
    
    CANCEL_APPROVED(5),
    
    APPROVED(6),
    
    CANCEL_CHOT_NGAY(7);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ActionSaleOrder> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ActionSaleOrder(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ActionSaleOrder parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ActionSaleOrder>(
                    ActionSaleOrder.values().length);
            for (ActionSaleOrder e : ActionSaleOrder.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

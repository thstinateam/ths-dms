/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum ApParamType {
	/**Begin vuongmq*/
	STATUS_TYPE("STATUS_TYPE"),
	
	NGAY_THANG_NAM("NGAY_THANG_NAM"),
	
	CHECK_LOT("CHECK_LOT"),
    /**End vuongmq*/
    PROMOTION ("PROMOTION"),
    
    CARTYPE("CARTYPE"),
    
    CARORG("CARORG"),
    
    UOM("UOM"),
    
    UOM1("UOM1"),
    
    UOM2("UOM2"),
    
    CARCAT("CARCAT"),
    
    PROMOTYPE("PROMOTYPE"),
    
    DEP("DEP"),
    
    OFFDATE("NGHI"),
    
    PROMOTION_MANUAL ("PROMOTION_MANUAL"),
    
    STAFF_TYPE ("STAFF_TYPE"),
    
    WORK_STATE("WORK_STATE"),
    
    ORDER_PIRITY("ORDER_PIRITY"),
    
    PRODUCT_SUBCAT_MAP("PRODUCT_SUBCAT_MAP"),
    
    DISPLAY("DISPLAY"),
    
    LOCATION("LOCATION"),
    
    LOYALTY("LOYALTY"),
    
    FC_INCENTIVE("FC_INCENTIVE"),
    
    DISPLAY_PROGRAM("DP_TYPE"),
    
    STAFF_SALE_TYPE("STAFF_SALE_TYPE"),
    
    WORK_STATE_TYPE("WORK_STATE"),
    
    STAFF_SALE_GROUP("STAFF_SALE_GROUP"),
    
    FOCUS_PRODUCT_TYPE("FOCUS_PRODUCT_TYPE"),
    
    VAT_TYPE("VAT_TYPE"),
    
    PO_REASON("REASON"),
    
    PO_REASON_INVOICE("SYS_REASON_UPDATE_INVOICE"), // ly do dieu chinh hoa don
    
    SALE_ORDER_APPROVED("FXT"),
    
    SALE_ORDER_APPLOT("LOT"),
    
    TH("TH"),
    
    ALLOW_EDIT_PROMOTION("ALLOW_EDIT_PROMOTION"), /** Cau hinh cho phep doi KM */
    
    AUTO_PO_XML("AUTO_PO_XML"),
    
    AUTO_PO_ZIP_PASS("AUTO_PO_ZIP_PASS"),
    
    VT2_MNMM("VT2_MNMM"),
    
    WAREHOUSE_TYPE("WAREHOUSE_TYPE"),
    
    DMS_CORE_ATTR("DMS_CORE_ATTR"),
    
    SYS_MAP_TYPE("SYS_MAP_TYPE"),
    
    SALE_ORDER_CONFIG("SALE_ORDER_CONFIG"),
    
    SYS_MODIFY_PRICE("SYS_MODIFY_PRICE"),
    
    VANSALE("VANSALE"),
    
    STAFF_TYPE_SHOW_SUP("STAFF_TYPE_SHOW_SUP"),//STAFF_TYPE ALLOWED SHOW IN SUPERVISE SALE 
	
    CUSTOMER_SALE_POSITION("CUSTOMER_SALE_POSITION"),
    
    SYS("SYS"), // SYS
    
    SYS_CONFIG("SYS_CONFIG"),
    
    CUSTOMER_CONFIG("CUSTOMER_CONFIG"),
    
    SYS_WAREHOUSE_CONFIG("SYS_WAREHOUSE_CONFIG"), //cau hinh co phan biet kho ban vs kho km
    
    SYS_CONVERT_QUANTITY_CONFIG("SYS_CONVERT_QUANTITY_CONFIG"), //cau hinh co convert quantity
    
    RETURN_KEYSHOP_CONFIG("RETURN_KEYSHOP_CONFIG"), //cau hinh tra hang keyshop
    
    SHORTEST_DISTANCE("SHORTEST_DISTANCE"),
    
    FEEDBACK_TYPE("FEEDBACK_TYPE"), // lay loai theo doi van de
    
    EQUIP_CONDITION("EQUIP_CONDITION"),
    
    EQUIP_EVICTION_PARAM("EQUIP_EVICTION_PARAM"),
    
    ACTIVE_TYPE_STATUS("ACTIVE_TYPE_STATUS"),
    
    LOSE_EQUIPMENT_RECORD("LOSE_EQUIPMENT_RECORD"),
    
    PROMOTION_DEFAULT ("PROMOTION_DEFAULT"),
    ;
    
    
    /** The value. */
    private String value;
    
    /** The values. */
    private static Map<String, ApParamType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ApParamType(String value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ApParamType parseValue(String value) {
        if (values == null) {
            values = new HashMap<String, ApParamType>(
                    ApParamType.values().length);
            for (ApParamType e : ApParamType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;

/**
 * 
 */
public class PromotionShopMapFilter implements Serializable {
	private static final long serialVersionUID = 7457477733939615641L;

	private Long programId;
	
	private Long shopRootId;
	
	private Long promotionId;
	
	private String shopCode;

	private String shopName;

	private ActiveType status;

	private Integer quantityMax;

	private Integer quantity;

	private Long parentShopId;
	
	private Integer isLevel;
	
	private String cusCode;
	
	private String cusName;
	
	private String address;

	public Long getProgramId() {
		return programId;
	}

	public void setProgramId(Long programId) {
		this.programId = programId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Long getParentShopId() {
		return parentShopId;
	}

	public void setParentShopId(Long parentShopId) {
		this.parentShopId = parentShopId;
	}

	public Integer getQuantityMax() {
		return quantityMax;
	}

	public void setQuantityMax(Integer quantityMax) {
		this.quantityMax = quantityMax;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}

	public Integer getIsLevel() {
		return isLevel;
	}

	public void setIsLevel(Integer isLevel) {
		this.isLevel = isLevel;
	}

	public Long getShopRootId() {
		return shopRootId;
	}

	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}

	public String getCusCode() {
		return cusCode;
	}

	public void setCusCode(String cusCode) {
		this.cusCode = cusCode;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}

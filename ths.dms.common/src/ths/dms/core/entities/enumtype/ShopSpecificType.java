/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cuonglt3
 *
 */
public enum ShopSpecificType {
	NPP(1), NPP_MT(2);
	
	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, ShopSpecificType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value the value
	 */
	ShopSpecificType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value the value
	 * @return the gender type
	 */
	public static ShopSpecificType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, ShopSpecificType>(ShopSpecificType.values().length);
			for (ShopSpecificType e : ShopSpecificType.values()) {
				values.put(e.getValue(), e);				
			}
		}
		return values.get(value);
	}
}

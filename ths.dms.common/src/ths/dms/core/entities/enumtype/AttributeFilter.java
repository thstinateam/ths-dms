package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.List;

public class AttributeFilter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String attributeCode;
	private String attributeName;
	private List<AttributeColumnType> lstValueType;
	private ActiveType status;
	private String listObject;
	
	public String getListObject() {
		return listObject;
	}
	public void setListObject(String listObject) {
		this.listObject = listObject;
	}
	public String getAttributeCode() {
		return attributeCode;
	}
	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public List<AttributeColumnType> getLstValueType() {
		return lstValueType;
	}
	public void setLstValueType(List<AttributeColumnType> lstValueType) {
		this.lstValueType = lstValueType;
	}
	
	
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum SaleOrderStep {
    
	//0: chua xac nhan, 
	//1: da xac nhan,
	//2: da xac nhan in,
	//3: da ghi nhan no va giam kho,
	//4: da ap no
	
    
    
    NOT_YET_CONFIRM (0),
    
    CONFIRMED (1),
    
    PRINT_CONFIRMED (2),

    DEBIT_CONFIRMED_AND_STOCK_OUT (3),
    
    DEBIT_ASSIGNED (4);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, SaleOrderStep> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    SaleOrderStep(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static SaleOrderStep parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, SaleOrderStep>(
                    SaleOrderStep.values().length);
            for (SaleOrderStep e : SaleOrderStep.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

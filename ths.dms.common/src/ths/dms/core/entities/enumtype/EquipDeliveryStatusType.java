package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;
/**
 * The Enum DeliveryType.
 * 
 * @author tamvnm
 * @description Quy dinh noi dung thiet bi trong Module Quan ly thiet bi
 */
public enum EquipDeliveryStatusType {

	NULL(null),

	NOT_EXPORT(0),
	
	EXPORTED(1);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, EquipDeliveryStatusType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 */
	EquipDeliveryStatusType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 * @return the gender type
	 */
	public static EquipDeliveryStatusType parseValue(Integer value) {
		if (value != null && value == -1) {
			value = -2;
		}
		if (values == null) {
			values = new HashMap<Integer, EquipDeliveryStatusType>(
					EquipDeliveryStatusType.values().length);
			for (EquipDeliveryStatusType e : EquipDeliveryStatusType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum FormType.
 * 
 * @author hunglm16
 */
public enum FormTypeCMS {
    
    MODULE (1),

    COMPONENT (2),
    
    SUBCOMPONENT (3)
    ;
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, FormTypeCMS> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    FormTypeCMS(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static FormTypeCMS parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, FormTypeCMS>(
            		FormTypeCMS.values().length);
            for (FormTypeCMS e : FormTypeCMS.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

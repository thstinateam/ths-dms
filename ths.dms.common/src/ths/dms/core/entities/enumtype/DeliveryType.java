package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DeliveryType.
 * 
 * @author nhutnn
 * @since December 24,2014
 * @description 0: Chua gui, 1: Da gui, 2: Da nhan
 */
public enum DeliveryType {

	NOTSEND(0),//Chua gui

	SENT(1),//Da gui

	RECEIVED(2)//Da nhan
	;

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, DeliveryType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 */
	DeliveryType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 * @return the gender type
	 */
	public static DeliveryType parseValue(Integer value) {
		if (value != null && value == -1) {
			value = -2;
		}
		if (values == null) {
			values = new HashMap<Integer, DeliveryType>(DeliveryType.values().length);
			for (DeliveryType e : DeliveryType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai trong enum hay khong
	 * 
	 * @author hunglm16
	 * @param value
	 * @return flag
	 */
	public static boolean checkIsRecode(Integer value) {
		if (value == null) {
			return false;
		}
		if (!DeliveryType.NOTSEND.getValue().equals(value) && !DeliveryType.SENT.getValue().equals(value) && !DeliveryType.RECEIVED.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

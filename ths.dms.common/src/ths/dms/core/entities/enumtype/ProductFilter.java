package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.Product;

// TODO: Auto-generated Javadoc
/**
 * The Class ProductFilter.
 */
public class ProductFilter implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The k paging. */
	private KPaging<Product> kPaging;

	/** The status. */
	private ActiveType status;

	/** The lst category id. */
	private List<Long> lstCategoryId;
	
	/** The lst product code selected. */
	private List<String> lstProductCodeSelected;
	
	/** The lst category codes. */
	private List<String> lstCategoryCodes;

	/** The cat id. */
	private Long catId;

	/** The is price valid. */
	private Boolean isPriceValid;
	
	/** The is except z cat. */
	private Boolean isExceptZCat;

	/** The status int. */
	private Integer statusInt;
	
	/** The check lot. */
	private Integer checkLot;

	/** The product code. */
	private String productCode;
	
	/** The product name. */
	private String productName;
	
	/** The category code. */
	private String categoryCode;
	
	/** The product type code. */
	private String productTypeCode;
	
	/** The sub cat code. */
	private String subCatCode;
	
	/** The promotion code. */
	private String promotionCode;
	
	/** The user. */
	private String user;
	
	private Long brandId;
	
	private Boolean checkBrand;
	
	/** The uom1. */
	private String uom1;
	
	private String order;
	
	private String sort;
	
	private List<Long> lstSubCategoryId;
	
	private Long flavourId;
	
	private Long packingId;
	
	public Boolean getCheckBrand() {
		return checkBrand;
	}

	public void setCheckBrand(Boolean checkBrand) {
		this.checkBrand = checkBrand;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	/**
	 * Gets the cat id.
	 *
	 * @return the cat id
	 */
	public Long getCatId() {
		return catId;
	}
	
	/**
	 * Sets the cat id.
	 *
	 * @param catId the new cat id
	 */
	public void setCatId(Long catId) {
		this.catId = catId;
	}
	
	/**
	 * Gets the status int.
	 *
	 * @return the status int
	 */
	public Integer getStatusInt() {
		return statusInt;
	}
	
	/**
	 * Sets the status int.
	 *
	 * @param statusInt the new status int
	 */
	public void setStatusInt(Integer statusInt) {
		this.statusInt = statusInt;
	}
	
	/**
	 * Gets the lst product code selected.
	 *
	 * @return the lst product code selected
	 */
	public List<String> getLstProductCodeSelected() {
		return lstProductCodeSelected;
	}
	
	/**
	 * Sets the lst product code selected.
	 *
	 * @param lstProductCodeSelected the new lst product code selected
	 */
	public void setLstProductCodeSelected(List<String> lstProductCodeSelected) {
		this.lstProductCodeSelected = lstProductCodeSelected;
	}
	
	/**
	 * Gets the k paging.
	 *
	 * @return the k paging
	 */
	public KPaging<Product> getkPaging() {
		return kPaging;
	}
	
	/**
	 * Sets the k paging.
	 *
	 * @param kPaging the new k paging
	 */
	public void setkPaging(KPaging<Product> kPaging) {
		this.kPaging = kPaging;
	}
	
	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return productCode;
	}
	
	/**
	 * Sets the product code.
	 *
	 * @param productCode the new product code
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return productName;
	}
	
	/**
	 * Sets the product name.
	 *
	 * @param productName the new product name
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	/**
	 * Gets the category code.
	 *
	 * @return the category code
	 */
	public String getCategoryCode() {
		return categoryCode;
	}
	
	/**
	 * Sets the category code.
	 *
	 * @param categoryCode the new category code
	 */
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	
	/**
	 * Gets the product type code.
	 *
	 * @return the product type code
	 */
	public String getProductTypeCode() {
		return productTypeCode;
	}
	
	/**
	 * Sets the product type code.
	 *
	 * @param productTypeCode the new product type code
	 */
	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ActiveType getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	
	/**
	 * Gets the sub cat code.
	 *
	 * @return the sub cat code
	 */
	public String getSubCatCode() {
		return subCatCode;
	}
	
	/**
	 * Sets the sub cat code.
	 *
	 * @param subCatCode the new sub cat code
	 */
	public void setSubCatCode(String subCatCode) {
		this.subCatCode = subCatCode;
	}
	
	/**
	 * Gets the promotion code.
	 *
	 * @return the promotion code
	 */
	public String getPromotionCode() {
		return promotionCode;
	}
	
	/**
	 * Sets the promotion code.
	 *
	 * @param promotionCode the new promotion code
	 */
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	
	/**
	 * Gets the check lot.
	 *
	 * @return the check lot
	 */
	public Integer getCheckLot() {
		return checkLot;
	}
	
	/**
	 * Sets the check lot.
	 *
	 * @param checkLot the new check lot
	 */
	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}
	
	/**
	 * Gets the lst category id.
	 *
	 * @return the lst category id
	 */
	public List<Long> getLstCategoryId() {
		return lstCategoryId;
	}
	
	/**
	 * Sets the lst category id.
	 *
	 * @param lstCategoryId the new lst category id
	 */
	public void setLstCategoryId(List<Long> lstCategoryId) {
		this.lstCategoryId = lstCategoryId;
	}
	
	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	
	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(String user) {
		this.user = user;
	}
	
	/**
	 * Gets the checks if is price valid.
	 *
	 * @return the checks if is price valid
	 */
	public Boolean getIsPriceValid() {
		return isPriceValid;
	}
	
	/**
	 * Sets the checks if is price valid.
	 *
	 * @param isPriceValid the new checks if is price valid
	 */
	public void setIsPriceValid(Boolean isPriceValid) {
		this.isPriceValid = isPriceValid;
	}
	
	/**
	 * Gets the checks if is except z cat.
	 *
	 * @return the checks if is except z cat
	 */
	public Boolean getIsExceptZCat() {
		return isExceptZCat;
	}
	
	/**
	 * Sets the checks if is except z cat.
	 *
	 * @param isExceptZCat the new checks if is except z cat
	 */
	public void setIsExceptZCat(Boolean isExceptZCat) {
		this.isExceptZCat = isExceptZCat;
	}
	
	/**
	 * Gets the lst category codes.
	 *
	 * @return the lst category codes
	 */
	public List<String> getLstCategoryCodes() {
		return lstCategoryCodes;
	}
	
	/**
	 * Sets the lst category codes.
	 *
	 * @param lstCategoryCodes the new lst category codes
	 */
	public void setLstCategoryCodes(List<String> lstCategoryCodes) {
		this.lstCategoryCodes = lstCategoryCodes;
	}

	public String getUom1() {
		return uom1;
	}

	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public List<Long> getLstSubCategoryId() {
		return lstSubCategoryId;
	}

	public void setLstSubCategoryId(List<Long> lstSubCategoryId) {
		this.lstSubCategoryId = lstSubCategoryId;
	}

	public Long getFlavourId() {
		return flavourId;
	}

	public void setFlavourId(Long flavourId) {
		this.flavourId = flavourId;
	}

	public Long getPackingId() {
		return packingId;
	}

	public void setPackingId(Long packingId) {
		this.packingId = packingId;
	}
}

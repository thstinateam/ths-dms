package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SaleOrderDetailFilter<T> implements Serializable{

	private static final long serialVersionUID = 1L;

	private KPaging<T> kPaging;
	private List<Long> arrLongId = new ArrayList<Long>();
	
	private Boolean isCompareEqual; 
	private Boolean flag;
	private Boolean isNotKeyShop;
	
	private Long productId;
	private Long saleOrderId;
	private Long shopId;

	private String userName;
	private String productCode;
	private String orderNumber;
	private String productName;
	private String textG;
	
	private Integer type;
	private Integer status;
	private Integer isAttr;
	
	private String programCode;
	private Integer promoLevel;
	private Long levelId;
	
	private Integer isFreeItem;
	
	private ProgramType programType;
	
	private String programeTypeCode;
	
	public Integer getIsFreeItem() {
		return isFreeItem;
	}

	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}

	public Integer getIsAttr() {
		return isAttr;
	}

	public void setIsAttr(Integer isAttr) {
		this.isAttr = isAttr;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	private List<String> arrStr;
	
	
	public List<String> getArrStr() {
		return arrStr;
	}

	public void setArrStr(List<String> arrStr) {
		this.arrStr = arrStr;
	}

	public String getTextG() {
		return textG;
	}

	public void setTextG(String textG) {
		this.textG = textG;
	}
	
	public Boolean getFlag() {
		return flag;
	}
	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	private Boolean isArrLongId; 
	
	public Boolean getIsArrLongId() {
		return isArrLongId;
	}
	public void setIsArrLongId(Boolean isArrLongId) {
		this.isArrLongId = isArrLongId;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public List<Long> getArrLongId() {
		return arrLongId;
	}
	public void setArrLongId(List<Long> arrLongId) {
		this.arrLongId = arrLongId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public Boolean getIsCompareEqual() {
		return isCompareEqual;
	}
	public void setIsCompareEqual(Boolean isCompareEqual) {
		this.isCompareEqual = isCompareEqual;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getSaleOrderId() {
		return saleOrderId;
	}
	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public Integer getPromoLevel() {
		return promoLevel;
	}

	public void setPromoLevel(Integer promoLevel) {
		this.promoLevel = promoLevel;
	}

	public Long getLevelId() {
		return levelId;
	}

	public void setLevelId(Long levelId) {
		this.levelId = levelId;
	}

	public ProgramType getProgramType() {
		return programType;
	}

	public void setProgramType(ProgramType programType) {
		this.programType = programType;
	}

	public String getProgrameTypeCode() {
		return programeTypeCode;
	}

	public void setProgrameTypeCode(String programeTypeCode) {
		this.programeTypeCode = programeTypeCode;
	}

	public Boolean getIsNotKeyShop() {
		return isNotKeyShop;
	}

	public void setIsNotKeyShop(Boolean isNotKeyShop) {
		this.isNotKeyShop = isNotKeyShop;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}	
}

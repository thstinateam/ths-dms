/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * FeedbackStatus theo doi van de trang thai
 * @author vuongmq
 * @since 11/11/2015 
 */
public enum FeedbackStatus {

	NEW(0),
	
	COMPLETE(1),

	APPROVED(2);

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, FeedbackStatus> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value the value
	 */
	FeedbackStatus(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author vuongmq
	 * @param value the value
	 * @return the aso plan type
	 * @since 11/11/2015 
	 */
	public static FeedbackStatus parseValue(Integer value) {
		if (value == null) {
			value = 0;
		}
		if (values == null) {
			values = new HashMap<Integer, FeedbackStatus>(FeedbackStatus.values().length);
			for (FeedbackStatus e : FeedbackStatus.values()) {
				values.put(e.getValue(), e);
			}
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai theo quy dinh.
	 * 
	 * @author vuongmq
	 * @param value
	 * @return flag
	 */
	public static boolean checkFeedbackStatus(Integer value) {
		if (value == null) {
			return false;
		}
		if (!FeedbackStatus.NEW.getValue().equals(value) && !FeedbackStatus.COMPLETE.getValue().equals(value) && !FeedbackStatus.APPROVED.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

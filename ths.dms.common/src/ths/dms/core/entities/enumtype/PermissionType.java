/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum Ob.
 * 
 * @author hunglm16
 */
public enum PermissionType {
    
	FUNCTION (1),
	
	ORGACESS(2),
    
	REPORT (3);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, PermissionType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    PermissionType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static PermissionType parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, PermissionType>(
                    PermissionType.values().length);
            for (PermissionType e : PermissionType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

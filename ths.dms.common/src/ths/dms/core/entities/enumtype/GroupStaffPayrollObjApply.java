/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum GroupStaffPayrollObjApply {
    
    SHOP (1),

    STAFF (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, GroupStaffPayrollObjApply> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    GroupStaffPayrollObjApply(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static GroupStaffPayrollObjApply parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, GroupStaffPayrollObjApply>(
                    GroupStaffPayrollObjApply.values().length);
            for (GroupStaffPayrollObjApply e : GroupStaffPayrollObjApply.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

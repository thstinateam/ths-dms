/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;



public class PoAutoGroupDetailVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7457477733939615641L;

	private Long id;
	
	private Long objectId;
	
	private Integer objectType;
	
	private String code;
	
	private String parentCode;
	
	private String name;
	
	// ThuatTQ Add
	
	private Long groupId;
	
	private String groupCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}
}

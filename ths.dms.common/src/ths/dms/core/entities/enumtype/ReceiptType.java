/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ReceiptType.
 * 
 * @author huytran
 */
public enum ReceiptType {
    
    RECEIVED (0),

    PAID (1),
    
    PHIEU_UY_NHIEM_CHI(2),
    PHIEU_CHI_UY_NHIEM_CHI(3),
    RECEIVED_REVERSE(4),
    PAID_REVERSE(5)
    ;    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ReceiptType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ReceiptType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ReceiptType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ReceiptType>(
                    ReceiptType.values().length);
            for (ReceiptType e : ReceiptType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

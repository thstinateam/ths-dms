package ths.dms.core.entities.enumtype;

/**
 * @author vuongmq
 * @date 13/03/2015
 * @description: object type cho quan ly kho thiet bi Equip_Stock
 * cai nay hien tai chua xai, cot type trong Equip_Stock, đã bỏ, dụa vào shop_id xác định type
 */
import java.util.HashMap;
import java.util.Map;

public enum EquipStockObjectType {

	VNM(0),

	MIEN(1),

	VUNG(2),
	
	NPP(3);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, EquipStockObjectType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 */
	EquipStockObjectType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 * @return the gender type
	 */
	public static EquipStockObjectType parseValue(Integer value) {
		if (value != null && value == -1) {
			value = -2;
		}
		if (values == null) {
			values = new HashMap<Integer, EquipStockObjectType>(EquipStockObjectType.values().length);
			for (EquipStockObjectType e : EquipStockObjectType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai tinh trang quy dinh.
	 * @author vuongmq
	 * @param value
	 * @return flag
	 */
	public static boolean checkIsEquipStockObject(Integer value) {
    	if(value == null){
    		return false;
    	}
    	if(!EquipStockObjectType.VNM.getValue().equals(value) 
    			&& !EquipStockObjectType.MIEN.getValue().equals(value)
				&& !EquipStockObjectType.VUNG.getValue().equals(value)
				&& !EquipStockObjectType.NPP.getValue().equals(value)){
    		return false;
    	}
        return true;
    }
}

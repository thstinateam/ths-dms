/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;

public class DisplayProgramExclusionFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5719535609658660659L;

	private Long displayProgramId;
	
	private String displayProgramExclusionCode;
	
	private String displayProgramExclusionName;
	
	private ActiveType status;
	
	private Long shopId;

	public Long getDisplayProgramId() {
		return displayProgramId;
	}

	public void setDisplayProgramId(Long displayProgramId) {
		this.displayProgramId = displayProgramId;
	}

	public String getDisplayProgramExclusionCode() {
		return displayProgramExclusionCode;
	}

	public void setDisplayProgramExclusionCode(String displayProgramExclusionCode) {
		this.displayProgramExclusionCode = displayProgramExclusionCode;
	}

	public String getDisplayProgramExclusionName() {
		return displayProgramExclusionName;
	}

	public void setDisplayProgramExclusionName(String displayProgramExclusionName) {
		this.displayProgramExclusionName = displayProgramExclusionName;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
}

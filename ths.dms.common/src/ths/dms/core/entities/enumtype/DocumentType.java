/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum DocumentType {

	CTKM(1),

	CTTB(2),
	
	CTTT(3);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, DocumentType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	DocumentType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static DocumentType parseValue(Integer value) {
		if (value != null && value == -1) {
			value = -2;
		}
		if (values == null) {
			values = new HashMap<Integer, DocumentType>(
					DocumentType.values().length);
			for (DocumentType e : DocumentType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

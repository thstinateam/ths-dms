package ths.dms.core.entities.enumtype;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The organization filter 
 * @author liemtpt
 * @since 12/02/2015
 * */
public class OrganizationFilter implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private Long organizationId;
	private Long parentOrgId;
	private Long nodeTypeId;
	private Integer nodeOrdinal;
	private Integer nodeType;
	private Integer status;
	private String typeCode; //phuocdh2 add typeCode (prefix) and typeName
	private String typeName; // ShopName or StaffName
	private Integer statusType; 
	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
	public Long getParentOrgId() {
		return parentOrgId;
	}
	public void setParentOrgId(Long parentOrgId) {
		this.parentOrgId = parentOrgId;
	}
	public Long getNodeTypeId() {
		return nodeTypeId;
	}
	public void setNodeTypeId(Long nodeTypeId) {
		this.nodeTypeId = nodeTypeId;
	}
	public Integer getNodeType() {
		return nodeType;
	}
	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getNodeOrdinal() {
		return nodeOrdinal;
	}
	public void setNodeOrdinal(Integer nodeOrdinal) {
		this.nodeOrdinal = nodeOrdinal;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public Integer getStatusType() {
		return statusType;
	}
	public void setStatusType(Integer statusType) {
		this.statusType = statusType;
	}
	
}

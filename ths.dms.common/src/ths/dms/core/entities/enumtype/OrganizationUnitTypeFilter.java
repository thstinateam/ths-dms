package ths.dms.core.entities.enumtype;

import java.io.Serializable;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.vo.OrganizationUnitTypeVO;

// TODO: Auto-generated Javadoc
/**
 * Class OrganizationUnitTypeFilter luu tru cac param cua don vi va chuc vu 
 * @author phuocdh2
 * @since 11.02.2015
 * */
public class OrganizationUnitTypeFilter implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private Long shopId;

	/** The Type Code Or Name. */
	String codeOrName;
	
	/** The ap param name. */
	String apParamName;
	
	/** The status. */
	ActiveType statusStaffType;
	
	/** The status. */
	ActiveType statusShopType;
	
	/** The description. */
	String description;
	
	/** The value. */
	String value;
	
	/** The type str. */
	String typeStr;
	
	OrganizationNodeType nodeType;
	
	/** The k paging. */
	KPaging<ApParam> kPaging;
	
	/** The paging vo. */
	private KPaging<OrganizationUnitTypeVO> pagingVO;
	
	private String order;
	private String sort;
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getApParamName() {
		return apParamName;
	}

	public void setApParamName(String apParamName) {
		this.apParamName = apParamName;
	}

	public ActiveType getStatusStaffType() {
		return statusStaffType;
	}

	public void setStatusStaffType(ActiveType statusStaffType) {
		this.statusStaffType = statusStaffType;
	}

	public ActiveType getStatusShopType() {
		return statusShopType;
	}

	public void setStatusShopType(ActiveType statusShopType) {
		this.statusShopType = statusShopType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTypeStr() {
		return typeStr;
	}

	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

	public KPaging<ApParam> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<ApParam> kPaging) {
		this.kPaging = kPaging;
	}

	public KPaging<OrganizationUnitTypeVO> getPagingVO() {
		return pagingVO;
	}

	public void setPagingVO(KPaging<OrganizationUnitTypeVO> pagingVO) {
		this.pagingVO = pagingVO;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public OrganizationNodeType getNodeType() {
		return nodeType;
	}

	public void setNodeType(OrganizationNodeType nodeType) {
		this.nodeType = nodeType;
	}

	public String getCodeOrName() {
		return codeOrName;
	}

	public void setCodeOrName(String codeOrName) {
		this.codeOrName = codeOrName;
	}

	
	
}

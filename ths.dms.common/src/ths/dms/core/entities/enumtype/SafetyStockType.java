/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum SafetyStockType {
    
    CAT (1),

    PRODUCT (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, SafetyStockType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    SafetyStockType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static SafetyStockType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, SafetyStockType>(
                    SafetyStockType.values().length);
            for (SafetyStockType e : SafetyStockType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

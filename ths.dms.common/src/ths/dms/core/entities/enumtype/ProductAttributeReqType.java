/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum ProductAttributeReqType {
    
	REQUIRE (1),

    NOT_REQUIRE (0);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ProductAttributeReqType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ProductAttributeReqType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ProductAttributeReqType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ProductAttributeReqType>(
                    ProductAttributeReqType.values().length);
            for (ProductAttributeReqType e : ProductAttributeReqType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.vo.PoVnmManualVO;
import ths.dms.core.entities.vo.PoVnmStockInVO;

/**
 * Nhập hàng về NPP
 * Mo ta class PoVnmFilter.java
 * @author vuongmq
 * @since Dec 15, 2015
 */
public class PoVnmFilter implements Serializable {
	
	private static final long serialVersionUID = -2684675320589968763L;

	private KPaging<PoVnm> kPaging;
	private KPaging<PoVnmManualVO> kPagingManualVO;
	private KPaging<PoVnmStockInVO> kPagingPoStockInVO;
	private PoType poType;
	private Long shopId; 
	private String poAutoNumber;
	private Date fromDate;
	private Date toDate;
	private Date importToDate;
	private Date importFromDate;
	private Date tDate;
	private Date fDate;
	private PoVNMStatus poStatus;
	private String invoiceNumber;
	private Boolean hasFindChildShop;
	private String sortField;
	private Boolean isLikePoNumber;
	private PoObjectType objectType = PoObjectType.NPP;
	
	private String productCode;
	private String productName;
	private Long poVnmId;//poDVKH
	private Long poConfirmId;
	private Long warehouseId;
	private Long productId;
	private Long fromPoVnmId;
	
	
	private String orderNumber;
	private String poCoNumber;
	private List<Integer> lstPoVnmStatus;
	private List<Integer> lstPoVnmType;
	
	public PoType getPoType() {
		return poType;
	}
	public void setPoType(PoType poType) {
		this.poType = poType;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getPoAutoNumber() {
		return poAutoNumber;
	}
	public KPaging<PoVnm> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<PoVnm> kPaging) {
		this.kPaging = kPaging;
	}
	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public PoObjectType getObjectType() {
		return objectType;
	}
	public void setObjectType(PoObjectType objectType) {
		this.objectType = objectType;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public PoVNMStatus getPoStatus() {
		return poStatus;
	}
	public void setPoStatus(PoVNMStatus poStatus) {
		this.poStatus = poStatus;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getSortField() {
		return sortField;
	}
	public void setSortField(String sortField) {
		this.sortField = sortField;
	}
	public Boolean getHasFindChildShop() {
		return hasFindChildShop;
	}
	public void setHasFindChildShop(Boolean hasFindChildShop) {
		this.hasFindChildShop = hasFindChildShop;
	}
	public Boolean getIsLikePoNumber() {
		return isLikePoNumber;
	}
	public void setIsLikePoNumber(Boolean isLikePoNumber) {
		this.isLikePoNumber = isLikePoNumber;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Long getPoVnmId() {
		return poVnmId;
	}
	public void setPoVnmId(Long poVnmId) {
		this.poVnmId = poVnmId;
	}
	public Long getPoConfirmId() {
		return poConfirmId;
	}
	public void setPoConfirmId(Long poConfirmId) {
		this.poConfirmId = poConfirmId;
	}
	public Long getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getFromPoVnmId() {
		return fromPoVnmId;
	}
	public void setFromPoVnmId(Long fromPoVnmId) {
		this.fromPoVnmId = fromPoVnmId;
	}
	public Date getImportToDate() {
		return importToDate;
	}
	public void setImportToDate(Date importToDate) {
		this.importToDate = importToDate;
	}
	public Date getImportFromDate() {
		return importFromDate;
	}
	public void setImportFromDate(Date importFromDate) {
		this.importFromDate = importFromDate;
	}
	public Date gettDate() {
		return tDate;
	}
	public void settDate(Date tDate) {
		this.tDate = tDate;
	}
	public Date getfDate() {
		return fDate;
	}
	public void setfDate(Date fDate) {
		this.fDate = fDate;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getPoCoNumber() {
		return poCoNumber;
	}
	public void setPoCoNumber(String poCoNumber) {
		this.poCoNumber = poCoNumber;
	}
	public List<Integer> getLstPoVnmStatus() {
		return lstPoVnmStatus;
	}
	public void setLstPoVnmStatus(List<Integer> lstPoVnmStatus) {
		this.lstPoVnmStatus = lstPoVnmStatus;
	}
	public List<Integer> getLstPoVnmType() {
		return lstPoVnmType;
	}
	public void setLstPoVnmType(List<Integer> lstPoVnmType) {
		this.lstPoVnmType = lstPoVnmType;
	}
	public KPaging<PoVnmManualVO> getkPagingManualVO() {
		return kPagingManualVO;
	}
	public void setkPagingManualVO(KPaging<PoVnmManualVO> kPagingManualVO) {
		this.kPagingManualVO = kPagingManualVO;
	}
	public KPaging<PoVnmStockInVO> getkPagingPoStockInVO() {
		return kPagingPoStockInVO;
	}
	public void setkPagingPoStockInVO(KPaging<PoVnmStockInVO> kPagingPoStockInVO) {
		this.kPagingPoStockInVO = kPagingPoStockInVO;
	}
	
	
}

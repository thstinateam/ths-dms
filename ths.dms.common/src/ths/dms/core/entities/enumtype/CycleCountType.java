/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum CycleCountType.
 * 
 * @author huytran
 */
public enum CycleCountType {
    
    ONGOING (0),

    COMPLETED (1),
    
    CANCELLED (2),
    
    REJECTED (3),
    
    WAIT_APPROVED (4),
    
    NOT_COUNTING (5);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, CycleCountType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    CycleCountType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static CycleCountType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, CycleCountType>(
                    CycleCountType.values().length);
            for (CycleCountType e : CycleCountType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum PoProductType.
 * 
 * @author vuongmq
 * @date 04/08/2015
 */
public enum PoProductType {

	SALE(1), // Hang ban

	PROMOTION(2), // Hang km

	POSM(3); // Posm

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, PoProductType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	PoProductType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static PoProductType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, PoProductType>(PoProductType.values().length);
			for (PoProductType e : PoProductType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum ProgramType {
    
    AUTO_PROM (0),

    MANUAL_PROM (1),
    
    DISPLAY_SCORE (2),
    
    PRODUCT_EXCHANGE (3),
    
    DESTROY (4),
    
    RETURN (5),
    
    KEY_SHOP(6)
    ;
    
    
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ProgramType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ProgramType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ProgramType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ProgramType>(
                    ProgramType.values().length);
            for (ProgramType e : ProgramType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
    
    public static Boolean checkIfProgramTypeInOldProductExchange(ProgramType type) {
    	if (ProgramType.PRODUCT_EXCHANGE.equals(type))
    		return true;
    	else if (ProgramType.DESTROY.equals(type))
    		return true;
    	else if (ProgramType.RETURN.equals(type))
    		return true;
    	return false;
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum OrganizationNodeType.
 * @author liemtpt
 * @date: 11/02/2015
 */
public enum OrganizationNodeType {
    
    SHOP (1),
    
    STAFF (2);
    
    /** The value. */
    private Integer value;
    /** The values. */
    private static Map<Integer, OrganizationNodeType> values = null;
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    OrganizationNodeType(Integer value) {
        this.value = value;
    }
    /**
     * Parses the value.
     * 
     * @author liemtpt
     * @param value the value
     * @return the gender type
     */
    public static OrganizationNodeType parseValue(Integer value) {
        if(value==null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, OrganizationNodeType>(
                    OrganizationNodeType.values().length);
            for (OrganizationNodeType e : OrganizationNodeType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
    
}

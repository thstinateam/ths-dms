/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum DebtPaymentType {
    
    AUTO (0),

    MANUAL (1),
    
    REMOVE_SMALL_DEBT (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, DebtPaymentType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    DebtPaymentType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static DebtPaymentType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, DebtPaymentType>(
                    DebtPaymentType.values().length);
            for (DebtPaymentType e : DebtPaymentType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;

import java.io.Serializable;

public class AttributeDetailFilter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long attributeId;
	
	private String code;
	
	private String name;
	
	private Long customerID;

	private ActiveType status;

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Long getAttributeId() {
		return attributeId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}

	public Long getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Long customerID) {
		this.customerID = customerID;
	}

}

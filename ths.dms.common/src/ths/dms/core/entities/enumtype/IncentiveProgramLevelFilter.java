/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.math.BigDecimal;

public class IncentiveProgramLevelFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7090200630435998151L;

	private Long programId;
	
	private String programCode;
	
	private String programName;
	
	private String levelCode;
	
	private String levelCodeEqual;
	
	private BigDecimal amount;
	
	private BigDecimal freeAmount;
	
	private Long freeProductId;
	
	private String freeProductCode;
	
	private Integer freeQuantity;
	
	private ActiveType status;

	public Long getProgramId() {
		return programId;
	}

	public void setProgramId(Long programId) {
		this.programId = programId;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public String getLevelCode() {
		return levelCode;
	}

	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFreeAmount() {
		return freeAmount;
	}

	public void setFreeAmount(BigDecimal freeAmount) {
		this.freeAmount = freeAmount;
	}

	public Long getFreeProductId() {
		return freeProductId;
	}

	public void setFreeProductId(Long freeProductId) {
		this.freeProductId = freeProductId;
	}

	public Integer getFreeQuantity() {
		return freeQuantity;
	}

	public void setFreeQuantity(Integer freeQuantity) {
		this.freeQuantity = freeQuantity;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public String getFreeProductCode() {
		return freeProductCode;
	}

	public void setFreeProductCode(String freeProductCode) {
		this.freeProductCode = freeProductCode;
	}

	public String getLevelCodeEqual() {
		return levelCodeEqual;
	}

	public void setLevelCodeEqual(String levelCodeEqual) {
		this.levelCodeEqual = levelCodeEqual;
	}
	
}

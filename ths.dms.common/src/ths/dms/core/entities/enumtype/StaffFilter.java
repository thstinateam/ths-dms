package ths.dms.core.entities.enumtype;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffGroupDetail;
import ths.dms.core.entities.vo.StaffExportVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.filter.PriviledgeInfo;

public class StaffFilter extends PriviledgeInfo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<Staff> kPaging;
	private KPaging<StaffExportVO> exPaging;
	private KPaging<StaffGroupDetail> sgdPaging;
	private KPaging<StaffVO> staffVOPaging;
	private Long staffId;
	private Long catId;
	private String staffCode;
	private String staffTypeCode;
	private String staffTypeName;
	private Long shopId;
	private Long userId;
	private String shopGroupCode;
	private String staffName;
	private String mobilePhoneNum;
	private ActiveType status;
	private String shopCode;
	private StaffObjectType staffType;
	private List<Integer> lstStaffType;
	private ChannelTypeType channelTypeType;
	private List<Integer> lstChannelObjectType;
	private String saleTypeCode;
	private String shopName;
	private Boolean isGetShopOnly;
	private Boolean isGetAll;
	private String shopCodeStaff;
	private String shopNameStaff;
	private String staffOwnerCode;
	private String strListUserId;
	private String strListShopId;
	private Boolean notInStaffGroup;
	private Long vungId;
	private Long staffGroupId;
	private Long typeId;
	private List<Long> lstExceptId;
	private List<Long> lstMandatoryId;
	
	private Boolean applyUnitTree;
	private Boolean isInAnyStaffGroup;
	private Boolean isGetParent;
	private Boolean isGetAllChild;
	private Boolean isStaffTypeId;
	private StaffSpecificType specType;
	private List<StaffSpecificType> lstSpecType;
	private Date checkDate;
	private Integer actionLogObjectType;
	
	private String strSpecType;//string specific_type in staff_type table
	
	private Boolean allStaffChildShop;
	
	private Boolean isCurrentUser; // lay them chinh user dang nhap, khi dung ham f_get_list_child_staff_inherit(?, sysdate, ?, ?, null)
	private List<Long> lstNotInId;
	
	public Boolean getIsGetAll() {
		return isGetAll;
	}
	public void setIsGetAll(Boolean isGetAll) {
		this.isGetAll = isGetAll;
	}
	public Boolean getIsGetAllChild() {
		return isGetAllChild;
	}
	public void setIsGetAllChild(Boolean isGetAllChild) {
		this.isGetAllChild = isGetAllChild;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Boolean getIsGetParent() {
		return isGetParent;
	}
	public void setIsGetParent(Boolean isGetParent) {
		this.isGetParent = isGetParent;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShopGroupCode() {
		return shopGroupCode;
	}
	public void setShopGroupCode(String shopGroupCode) {
		this.shopGroupCode = shopGroupCode;
	}

	public KPaging<Staff> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<Staff> kPaging) {
		this.kPaging = kPaging;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffTypeCode() {
		return staffTypeCode;
	}
	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getMobilePhoneNum() {
		return mobilePhoneNum;
	}
	public void setMobilePhoneNum(String mobilePhoneNum) {
		this.mobilePhoneNum = mobilePhoneNum;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public StaffObjectType getStaffType() {
		return staffType;
	}
	public void setStaffType(StaffObjectType staffType) {
		this.staffType = staffType;
	}
	public List<Integer> getLstStaffType() {
		return lstStaffType;
	}
	public void setLstStaffType(List<Integer> lstStaffType) {
		this.lstStaffType = lstStaffType;
	}
	public ChannelTypeType getChannelTypeType() {
		return channelTypeType;
	}
	public void setChannelTypeType(ChannelTypeType channelTypeType) {
		this.channelTypeType = channelTypeType;
	}
	public List<Integer> getLstChannelObjectType() {
		return lstChannelObjectType;
	}
	public void setLstChannelObjectType(List<Integer> lstChannelObjectType) {
		this.lstChannelObjectType = lstChannelObjectType;
	}
	public String getSaleTypeCode() {
		return saleTypeCode;
	}
	public void setSaleTypeCode(String saleTypeCode) {
		this.saleTypeCode = saleTypeCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Boolean getIsGetShopOnly() {
		return isGetShopOnly;
	}
	public void setIsGetShopOnly(Boolean isGetShopOnly) {
		this.isGetShopOnly = isGetShopOnly;
	}
	public String getShopCodeStaff() {
		return shopCodeStaff;
	}
	public void setShopCodeStaff(String shopCodeStaff) {
		this.shopCodeStaff = shopCodeStaff;
	}
	public String getShopNameStaff() {
		return shopNameStaff;
	}
	public void setShopNameStaff(String shopNameStaff) {
		this.shopNameStaff = shopNameStaff;
	}
	public String getStaffOwnerCode() {
		return staffOwnerCode;
	}
	public void setStaffOwnerCode(String staffOwnerCode) {
		this.staffOwnerCode = staffOwnerCode;
	}
	public String getStrListUserId() {
		return strListUserId;
	}
	public void setStrListUserId(String strListUserId) {
		this.strListUserId = strListUserId;
	}
	public Boolean getNotInStaffGroup() {
		return notInStaffGroup;
	}
	public void setNotInStaffGroup(Boolean notInStaffGroup) {
		this.notInStaffGroup = notInStaffGroup;
	}
	public Long getVungId() {
		return vungId;
	}
	public void setVungId(Long vungId) {
		this.vungId = vungId;
	}
	public Long getStaffGroupId() {
		return staffGroupId;
	}
	public void setStaffGroupId(Long staffGroupId) {
		this.staffGroupId = staffGroupId;
	}
	public Boolean getIsInAnyStaffGroup() {
		return isInAnyStaffGroup;
	}
	public void setIsInAnyStaffGroup(Boolean isInAnyStaffGroup) {
		this.isInAnyStaffGroup = isInAnyStaffGroup;
	}
	public List<Long> getLstExceptId() {
		return lstExceptId;
	}
	public void setLstExceptId(List<Long> lstExceptId) {
		this.lstExceptId = lstExceptId;
	}
	public List<Long> getLstMandatoryId() {
		return lstMandatoryId;
	}
	public void setLstMandatoryId(List<Long> lstMandatoryId) {
		this.lstMandatoryId = lstMandatoryId;
	}
	public KPaging<StaffGroupDetail> getSgdPaging() {
		return sgdPaging;
	}
	public void setSgdPaging(KPaging<StaffGroupDetail> sgdPaging) {
		this.sgdPaging = sgdPaging;
	}
	public Boolean getApplyUnitTree() {
		return applyUnitTree;
	}
	public void setApplyUnitTree(Boolean applyUnitTree) {
		this.applyUnitTree = applyUnitTree;
	}
	public KPaging<StaffExportVO> getExPaging() {
		return exPaging;
	}
	public void setExPaging(KPaging<StaffExportVO> exPaging) {
		this.exPaging = exPaging;
	}
	public KPaging<StaffVO> getStaffVOPaging() {
		return staffVOPaging;
	}
	public void setStaffVOPaging(KPaging<StaffVO> staffVOPaging) {
		this.staffVOPaging = staffVOPaging;
	}
	public Boolean getIsStaffTypeId() {
		return isStaffTypeId;
	}
	public void setIsStaffTypeId(Boolean isStaffTypeId) {
		this.isStaffTypeId = isStaffTypeId;
	}
	public String getStaffTypeName() {
		return staffTypeName;
	}
	public void setStaffTypeName(String staffTypeName) {
		this.staffTypeName = staffTypeName;
	}
	public StaffSpecificType getSpecType() {
		return specType;
	}
	public void setSpecType(StaffSpecificType specType) {
		this.specType = specType;
	}
	public List<StaffSpecificType> getLstSpecType() {
		return lstSpecType;
	}
	public void setLstSpecType(List<StaffSpecificType> lstSpecType) {
		this.lstSpecType = lstSpecType;
	}
	public Long getCatId() {
		return catId;
	}
	public void setCatId(Long catId) {
		this.catId = catId;
	}
	public String getStrSpecType() {
		return strSpecType;
	}
	public void setStrSpecType(String strSpecType) {
		this.strSpecType = strSpecType;
	}
	public Boolean getAllStaffChildShop() {
		return allStaffChildShop;
	}
	public void setAllStaffChildShop(Boolean allStaffChildShop) {
		this.allStaffChildShop = allStaffChildShop;
	}
	public Date getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}
	public Integer getActionLogObjectType() {
		return actionLogObjectType;
	}
	public void setActionLogObjectType(Integer actionLogObjectType) {
		this.actionLogObjectType = actionLogObjectType;
	}
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public final Boolean getIsCurrentUser() {
		return isCurrentUser;
	}
	public final void setIsCurrentUser(Boolean isCurrentUser) {
		this.isCurrentUser = isCurrentUser;
	}
	public List<Long> getLstNotInId() {
		return lstNotInId;
	}
	public void setLstNotInId(List<Long> lstNotInId) {
		this.lstNotInId = lstNotInId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getStrListShopId() {
		return strListShopId;
	}
	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}
	
	
}

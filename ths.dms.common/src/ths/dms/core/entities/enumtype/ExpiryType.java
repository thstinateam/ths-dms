/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum ExpiryType {
    
    DATE (0),

    MONTH (1),
    
    YEAR (3);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ExpiryType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ExpiryType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ExpiryType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ExpiryType>(
                    ExpiryType.values().length);
            for (ExpiryType e : ExpiryType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

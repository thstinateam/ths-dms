/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum CycleType {
    
    ALL (0),

    SELECT (1);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, CycleType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    CycleType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static CycleType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, CycleType>(
                    CycleType.values().length);
            for (CycleType e : CycleType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

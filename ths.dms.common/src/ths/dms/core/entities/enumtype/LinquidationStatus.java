/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum Trang Thai Thanh Ly.
 * 
 * @author hunglm16
 * @since December 22,2014
 * @description Trang thai thanh ly: 2: cho thanh ly; 1: da thanh ly; 0: khong thanh ly
 */
public enum LinquidationStatus {

	PROCESS_LIQUIDATION(3), //dang thanh ly; dung ben bien ban thanh ly
	WAITING_LIQUIDATION(2), //Cho thanh ly
	FINISH_LIQUIDATION(1), // Da thanh ly
	NO_LIQUIDATION(0) //Khong thanh ly
	;
	

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, LinquidationStatus> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	LinquidationStatus(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static LinquidationStatus parseValue(Integer value) {
		if (value != null && value == -1) {
			value = -2;
		}
		if (values == null) {
			values = new HashMap<Integer, LinquidationStatus>(LinquidationStatus.values().length);
			for (LinquidationStatus e : LinquidationStatus.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai tinh trang quy dinh.
	 * 
	 * @author hunglm16
	 * @param value
	 * @return flag
	 */
	public static boolean checkIsRecode(Integer value) {
		if (value == null) {
			return false;
		}
		if (!LinquidationStatus.WAITING_LIQUIDATION.getValue().equals(value) && !LinquidationStatus.FINISH_LIQUIDATION.getValue().equals(value) && !LinquidationStatus.NO_LIQUIDATION.getValue().equals(value)) {
			return false;
		}
		return true;
	}

}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Mo ta class ApprovalStepPo.java
 * @author vuongmq
 * @since Dec 9, 2015
 */
public enum ApprovalStepPo {
    
    NEW (1), // cho gui
    
    WAITING(2), // cho duyet
    
    APPROVED (3), // da duyet
    
    COMPLETED(4), // da nhap or da xuat
    
    REJECTED(5), // status = 0, approve_step = 2
    
    DESTROY (6), //status = 0, approve_step = 1
    ;
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ApprovalStepPo> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ApprovalStepPo(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ApprovalStepPo parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ApprovalStepPo>(
                    ApprovalStepPo.values().length);
            for (ApprovalStepPo e : ApprovalStepPo.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
    
    /**
     * Check co thuoc cac trang thai cua ApprovalStepPo
     * Xu ly checkApprovalStepPo
     * @author vuongmq
     * @param value
     * @return boolean
     * @since Dec 18, 2015
     */
	public static boolean checkApprovalStepPo(Integer value) {
		if (value == null) {
			return false;
		}
		if (!ApprovalStepPo.NEW.getValue().equals(value) && !ApprovalStepPo.WAITING.getValue().equals(value)
				&& !ApprovalStepPo.APPROVED.getValue().equals(value) && !ApprovalStepPo.COMPLETED.getValue().equals(value)
				&& !ApprovalStepPo.REJECTED.getValue().equals(value) && !ApprovalStepPo.DESTROY.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

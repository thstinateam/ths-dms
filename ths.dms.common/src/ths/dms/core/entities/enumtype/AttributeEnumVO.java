package ths.dms.core.entities.enumtype;

import java.io.Serializable;

public class AttributeEnumVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer applyObject;
	private Long attributeId;
	private Long attributeDetailId;
	private Long enumId;
	private Integer status;
	private String enumCode;
	private String enumValue;

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getEnumId() {
		return enumId;
	}
	public void setEnumId(Long enumId) {
		this.enumId = enumId;
	}
	public String getEnumCode() {
		return enumCode;
	}
	public void setEnumCode(String enumCode) {
		this.enumCode = enumCode;
	}
	public String getEnumValue() {
		return enumValue;
	}
	public void setEnumValue(String enumValue) {
		this.enumValue = enumValue;
	}
	public Long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}
	public Long getAttributeDetailId() {
		return attributeDetailId;
	}
	public void setAttributeDetailId(Long attributeDetailId) {
		this.attributeDetailId = attributeDetailId;
	}
	public Integer getApplyObject() {
		return applyObject;
	}
	public void setApplyObject(Integer applyObject) {
		this.applyObject = applyObject;
	}
	
}

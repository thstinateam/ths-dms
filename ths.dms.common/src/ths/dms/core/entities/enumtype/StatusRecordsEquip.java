/**
 * Khai bao Import thu vien
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Status Records Equipment.
 * 
 * @author hunglm16
 * @description 0: Du thao, 1: Cho duyet, 2: Da duyet, 3: Khong duyet, 4: Huy, 5: Dang sua chua, 6: Hoan tat sua chua.
 */
public enum StatusRecordsEquip {

	DRAFT(0),// Du thao

	WAITING_APPROVAL(1), //Cho duyet

	APPROVED(2),// Da duyet

	NO_APPROVAL(3),// Khong duyet

	CANCELLATION(4),// Huy

	ARE_CORRECTED(5),// Dang sua chua

	COMPLETION_REPAIRS(6),// Hoan tat sua chua
	
	LIQUIDATED(7)// Thanh ly
	;

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, StatusRecordsEquip> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	StatusRecordsEquip(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author hunglm16
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static StatusRecordsEquip parseValue(Integer value) {
		if (value == null) {
			value = -1;
		}
		if (values == null) {
			values = new HashMap<Integer, StatusRecordsEquip>(StatusRecordsEquip.values().length);
			for (StatusRecordsEquip e : StatusRecordsEquip.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai tinh trang quy dinh.
	 * 
	 * @author hunglm16
	 * @param value
	 * @return flag
	 */
	public static boolean checkIsRecode(Integer value) {
		if (value == null) {
			return false;
		}
		if (!StatusRecordsEquip.DRAFT.getValue().equals(value) && !StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(value) && !StatusRecordsEquip.APPROVED.getValue().equals(value) && !StatusRecordsEquip.NO_APPROVAL.getValue().equals(value)
				&& !StatusRecordsEquip.CANCELLATION.getValue().equals(value) && !StatusRecordsEquip.ARE_CORRECTED.getValue().equals(value) && !StatusRecordsEquip.COMPLETION_REPAIRS.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;



public class PoAutoGroupDetailFilter implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7457477733939615641L;

	private Long poAutoGroupId;
	
	private Long objectId;
	
	private Integer objectType;
		
	public Long getPoAutoGroupId() {
		return poAutoGroupId;
	}

	public void setPoAutoGroupId(Long poAutoGroupId) {
		this.poAutoGroupId = poAutoGroupId;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	
}

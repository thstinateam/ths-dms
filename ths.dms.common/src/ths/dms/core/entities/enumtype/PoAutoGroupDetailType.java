/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ProductType.
 * 
 * @author huytran
 */
public enum PoAutoGroupDetailType {
    
    
    CAT (0),
    
    SUB_CAT (1),
    
    PRODUCT (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, PoAutoGroupDetailType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    PoAutoGroupDetailType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static PoAutoGroupDetailType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, PoAutoGroupDetailType>(
                    PoAutoGroupDetailType.values().length);
            for (PoAutoGroupDetailType e : PoAutoGroupDetailType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

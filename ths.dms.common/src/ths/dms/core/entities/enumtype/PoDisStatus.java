/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum PoDisStatus {
    
    CHUA_CHUYEN (0),

    DA_CHUYEN (1);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, PoDisStatus> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    PoDisStatus(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static PoDisStatus parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, PoDisStatus>(
                    PoDisStatus.values().length);
            for (PoDisStatus e : PoDisStatus.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;
/**
 * EquipSalePlanCustomerType
 * @author liemtpt
 * @since 17/03/2015
 * @description customer_type in table equip_sale_plan
 */
public enum EquipSalePlanCustomerType {

	//chua xac dinh
	UNDEFINED(0),

	// Thanh thi
	CITY(1),
	
	// Nong thon
	GARDEN_CITY(2);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, EquipSalePlanCustomerType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 */
	EquipSalePlanCustomerType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 * @return the gender type
	 */
	public static EquipSalePlanCustomerType parseValue(Integer value) {
		if (value != null && value == -1) {
			value = -2;
		}
		if (values == null) {
			values = new HashMap<Integer, EquipSalePlanCustomerType>(EquipSalePlanCustomerType.values().length);
			for (EquipSalePlanCustomerType e : EquipSalePlanCustomerType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai tinh trang quy dinh.
	 * 
	 * @author hunglm16
	 * @param value
	 * @return flag
	 */
	public static boolean checkIsRecode(Integer value) {
    	if(value == null){
    		return false;
    	}
    	if(!EquipSalePlanCustomerType.UNDEFINED.getValue().equals(value) && !EquipSalePlanCustomerType.CITY.getValue().equals(value)
				&& !EquipSalePlanCustomerType.GARDEN_CITY.getValue().equals(value)){
    		return false;
    	}
        return true;
    }
}

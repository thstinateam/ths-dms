/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cuonglt3
 *
 */
public enum WarehouseType {
	SALES(0),
	PROMOTION(1);
	
	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, WarehouseType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value the value
	 */
	WarehouseType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value the value
	 * @return the gender type
	 */
	public static WarehouseType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, WarehouseType>(WarehouseType.values().length);
			for (WarehouseType e : WarehouseType.values()) {
				values.put(e.getValue(), e);				
			}
		}
		return values.get(value);
	}
}

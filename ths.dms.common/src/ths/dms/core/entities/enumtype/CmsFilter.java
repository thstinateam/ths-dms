package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.vo.CmsVO;

/**
 *
 * @author hunglm16
 * @since September 10,2014
 */
public class CmsFilter implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private KPaging<CmsVO> kPaging;
	private List<Long> lstId;

	private Long userId;
	private Long formId;
	private Long roleId;
	private Long appId;
	private Long id;
	private Long parentId;
	private Long controlId;
	private Long functionId;
	private Long shopId;
	private Long staffRootId;
	private Long shopRootId;

	private Integer type;

	private String formCode;
	private String formName;
	private String controlCode;
	private String controlName;
	private String userName;
	private String appCode;
	private Integer specificType;
	
	private Boolean flagCMSChildrent;
	
	public Long getShopRootId() {
		return shopRootId;
	}
	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}
	public Boolean getFlagCMSChildrent() {
		return flagCMSChildrent;
	}
	public void setFlagCMSChildrent(Boolean flagCMSChildrent) {
		this.flagCMSChildrent = flagCMSChildrent;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getAppId() {
		return appId;
	}
	public void setAppId(Long appId) {
		this.appId = appId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public List<Long> getLstId() {
		return lstId;
	}
	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
	public KPaging<CmsVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<CmsVO> kPaging) {
		this.kPaging = kPaging;
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getFormId() {
		return formId;
	}
	public void setFormId(Long formId) {
		this.formId = formId;
	}
	public Long getControlId() {
		return controlId;
	}
	public void setControlId(Long controlId) {
		this.controlId = controlId;
	}
	public Long getFunctionId() {
		return functionId;
	}
	public void setFunctionId(Long functionId) {
		this.functionId = functionId;
	}
	public String getFormCode() {
		return formCode;
	}
	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getControlCode() {
		return controlCode;
	}
	public void setControlCode(String controlCode) {
		this.controlCode = controlCode;
	}
	public String getControlName() {
		return controlName;
	}
	public void setControlName(String controlName) {
		this.controlName = controlName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAppCode() {
		return appCode;
	}
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	public Long getStaffRootId() {
		return staffRootId;
	}
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}
	public Integer getSpecificType() {
		return specificType;
	}
	public void setSpecificType(Integer specificType) {
		this.specificType = specificType;
	}
	
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author nald
 */
public enum ProductInfoObjectType {
    
	/** San pham */
	SP(0),
	
	/** Nganh hang bao bi */
    BAO_BI (1),

    /** Vat pham khuyen mai */
    VP_KM (2),
    
    /** Nganh hang thiet bi */
    Z(3),
    
    /** tiền */
    TIEN (4),
    
    /** Bảng hiệu */
	BANG_HIEU(5),
	
	/** Loại hỗ trợ khác */
	LOAI_HO_TRO_KHAC(6);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ProductInfoObjectType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ProductInfoObjectType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ProductInfoObjectType parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, ProductInfoObjectType>(
                    ProductInfoObjectType.values().length);
            for (ProductInfoObjectType e : ProductInfoObjectType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

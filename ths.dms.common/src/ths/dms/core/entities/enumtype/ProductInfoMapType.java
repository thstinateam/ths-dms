/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ProductType.
 * 
 * @author huytran
 */
public enum ProductInfoMapType {
	
	SUB_CAT (12),
    
    SECOND_SUB_CAT (17);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ProductInfoMapType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ProductInfoMapType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ProductInfoMapType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ProductInfoMapType>(
                    ProductInfoMapType.values().length);
            for (ProductInfoMapType e : ProductInfoMapType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum DebitOwnerType {
    SHOP (1),
    
    CUSTOMER (3);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, DebitOwnerType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    DebitOwnerType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static DebitOwnerType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, DebitOwnerType>(
                    DebitOwnerType.values().length);
            for (DebitOwnerType e : DebitOwnerType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

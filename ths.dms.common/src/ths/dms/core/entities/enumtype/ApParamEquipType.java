
/**
 * 
 */
package ths.dms.core.entities.enumtype;
/**
 * @author vuongmq
 * @date 15/04/2015
 * cac equip_param cho thiet bi
 */
import java.util.HashMap;
import java.util.Map;


public enum ApParamEquipType {
    
	/** Begin vuongmq; ql sua chua cau hinh*/
	NGAY_HET_HAN_BAO_HANH ("NGAY_HET_HAN_BAO_HANH"),
    
	CODE_THANG_DOANH_SO ("THANG_DOANH_SO"),
    
    CODE_NGANH_HANG_DOANH_SO ("NGANH_HANG_DOANH_SO"),
    
    DOANH_SO_PHIEU_SUA_CHUA ("DOANH_SO_PHIEU_SUA_CHUA"),
    
    EQUIPMENT_CAT ("EQUIPMENT_CAT"), // cau hinh ma nganh hang Z
    
    EMAIL_SEND_LOST_RECORD ("EMAIL_SEND_LOST_RECORD"), // nguoi send
    
    EMAIL_TO_LOST_RECORD ("EMAIL_TO_LOST_RECORD"), // nguoi nhan
    
    EMAIL_SEND_REPAIR ("EMAIL_SEND_REPAIR"), // nguoi send
    
    EMAIL_TO_REPAIR ("EMAIL_TO_REPAIR"), // nguoi nhan
    
    
    /** End vuongmq; ql sua chua cau hinh*/
    
    /** Quan ly giao nhan va hop dong
     *  @author tamvnm
     * */
    EQUIP_DELIVERY_RECORD ("EQUIP_DELIVERY_RECORD"),
    
    FROM_OBJECT_CODE ("FROM_OBJECT_CODE"),
    
    FROM_OBJECT_ADDRESS ("FROM_OBJECT_ADDRESS"),
    
    FROM_OBJECT_TAX ("FROM_OBJECT_TAX"),
    
    FROM_OBJECT_PHONE ("FROM_OBJECT_PHONE"),
    
    FROM_REPRESENTATIVE ("FROM_REPRESENTATIVE"),
    
    FROM_POSITION ("FROM_POSITION"),
    
    FROM_PAGE ("FROM_PAGE"),
    
    FROM_PAGE_PLACE ("FROM_PAGE_PLACE"),
    
    FROM_PAGE_DATE ("FROM_PAGE_DATE"),
    
    FROM_FAX ("FROM_FAX"),
    
    /** Noi dung muon tu*/
    DELIVERY_CONTENT ("DELIVERY_CONTENT"),
    
    /** Cap moi*/
    DELIVERY_CONTENT_NEW ("DELIVERY_CONTENT_NEW"),
    
    /** Dieu chuyen tu khach hang khac*/
    DELIVERY_CONTENT_CUSTOMER_NEW ("DELIVERY_CONTENT_CUSTOMER_NEW"),
    
    /** Khac*/
    DELIVERY_CONTENT_OTHER ("DELIVERY_CONTENT_OTHER"),
    
    /** NPP cho muon*/
    DELIVERY_CONTENT_SHOP ("DELIVERY_CONTENT_SHOP"),
     /** END Quan ly giao nhan va hop dong*/
      
    /** Thu hoi va thanh ly
     *  @author tamvnm
     * */
    EQUIP_EVICTION_REASON_TYPE ("EQUIP_EVICTION_REASON"),
    REASON_DCKH ("REASON_DCKH"),
    REASON_HU_HONG ("REASON_HU_HONG"),
    REASON_MAT_TU ("REASON_MAT_TU"),
    REASON_KHAC ("REASON_R_KHAC"), // Thêm chữ R để đảm bảo lý do khác ở cuối cùng.
    EQUIP_EVICTION_FROM_SHOP_TYPE ("EQUIP_EVICTION_FROM_SHOP"),
    FROM_SHOP ("FROM_SHOP"), // Ben cho mượn
    EQUIP_EVICTON_PRINT_TYPE ("EQUIP_EVICTON_PRINT"),
    //So khau hao
    EQUIP_NUMBER_DEPRECIATION("EQUIP_NUMBER_DEPRECIATION")
    
     /** END Quan ly giao nhan va hop dong*/
    ;
    
    
    /** The value. */
    private String value;
    
    /** The values. */
    private static Map<String, ApParamEquipType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ApParamEquipType(String value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ApParamEquipType parseValue(String value) {
        if (values == null) {
            values = new HashMap<String, ApParamEquipType>(
                    ApParamEquipType.values().length);
            for (ApParamEquipType e : ApParamEquipType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

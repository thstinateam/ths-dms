/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum DebitPaymentType {
    
    CHUA_TOI_HAN (0),

    TOI_HAN (1),
    
    QUA_HAN (2),
    
    DA_TAT_TOAN(3);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, DebitPaymentType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    DebitPaymentType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static DebitPaymentType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, DebitPaymentType>(
                    DebitPaymentType.values().length);
            for (DebitPaymentType e : DebitPaymentType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

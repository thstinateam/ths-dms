/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActionAuditType.
 * 
 * @author huytran
 */
public enum ActionAuditType {
    
    CUSTOMER ("CUSTOMER"),

    STAFF ("STAFF"),
    
    SHOP ("SHOP"),
    
    PRODUCT ("PRODUCT"),
    
    CUSTOMERCATLEVEL ("CUSTOMER_CAT_LEVEL"),
    
    STAFFCATLEVEL ("STAFF_CAT_LEVEL"),
    
    PROMOTION_PROGRAM ("PROMOTION_PROGRAM"),
    
    INCENTIVE_PROGRAM ("INCENTIVE_PROGRAM"),
    
    FOCUS_PROGRAM ("FOCUS_PROGRAM"),
    
    DISPLAY_PROGRAM ("DISPLAY_PROGRAM");
    
    
    /** The value. */
    private String value;
    
    /** The values. */
    private static Map<String, ActionAuditType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ActionAuditType(String value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ActionAuditType parseValue(String value) {
        if (values == null) {
            values = new HashMap<String, ActionAuditType>(
                    ActionAuditType.values().length);
            for (ActionAuditType e : ActionAuditType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;



public class PoAutoGroupFilter implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7457477733939615641L;

	private Long id;
	
	private String groupName;
	
	private String groupCode;
		
	private ApprovalStatus status;
	
	private Long shopId;
	
	private Long poAutoGroupShopMapId;

	
	public Long getPoAutoGroupShopMapId() {
		return poAutoGroupShopMapId;
	}

	public void setPoAutoGroupShopMapId(Long poAutoGroupShopMapId) {
		this.poAutoGroupShopMapId = poAutoGroupShopMapId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public ApprovalStatus getStatus() {
		return status;
	}

	public void setStatus(ApprovalStatus status) {
		this.status = status;
	}

}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * Cac tinh trang cua bang ghi lock chot ngay
 * 
 * @author hunglm16
 * @since 21/10/2015
 */
public enum ShopLockLogStatus {
    
	RUNNING(1), PENDING(2), DONE(3), ERROR(4);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, ShopLockLogStatus> values = null;

    /**
     * Gets the value.
     * 
     * @return the value
     */
	public Integer getValue() {
		return value;
	}
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
	ShopLockLogStatus(Integer value) {
		this.value = value;
	}
    
    /**
     * Parses the value.
     * 
     * @author hunglm16
     * @param value the value
     * @return the gender type
     * @since 21/10/2015
     */
	public static ShopLockLogStatus parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, ShopLockLogStatus>(ShopLockLogStatus.values().length);
			for (ShopLockLogStatus e : ShopLockLogStatus.values()) {
				values.put(e.getValue(), e);
			}
		}
		return values.get(value);
	}

}

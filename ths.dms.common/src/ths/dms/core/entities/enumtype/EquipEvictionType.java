/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Type ly do thu hoi
 * 
 * @author tamvnm
 */
public enum EquipEvictionType{
    
    DIEU_CHUYEN_KHACH_HANG_KHAC (0),

    HU_HONG (1),
    
    MAT_TU (2),
    
    KHAC (3);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, EquipEvictionType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    EquipEvictionType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static EquipEvictionType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, EquipEvictionType>(
                    EquipEvictionType.values().length);
            for (EquipEvictionType e : EquipEvictionType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

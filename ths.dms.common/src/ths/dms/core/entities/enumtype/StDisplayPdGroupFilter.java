package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.StDisplayProgram;

public class StDisplayPdGroupFilter implements Serializable{

private static final long serialVersionUID = 1L;
	
	// id nhom sp
	
	private Long id;
	
	
	private StDisplayProgram displayProgram;
	
	
	private String displayProductGroupCode;

	
	private String displayProductGroupName;
	
	
	private ActiveType status = ActiveType.RUNNING;
	
	
	private DisplayProductGroupType type;
	
	
	private Date createDate;

	
	private String createUser;

	
	private Date updateDate;
	
	
	private String updateUser;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public StDisplayProgram getDisplayProgram() {
		return displayProgram;
	}


	public void setDisplayProgram(StDisplayProgram displayProgram) {
		this.displayProgram = displayProgram;
	}


	public String getDisplayProductGroupCode() {
		return displayProductGroupCode;
	}


	public void setDisplayProductGroupCode(String displayProductGroupCode) {
		this.displayProductGroupCode = displayProductGroupCode;
	}


	public String getDisplayProductGroupName() {
		return displayProductGroupName;
	}


	public void setDisplayProductGroupName(String displayProductGroupName) {
		this.displayProductGroupName = displayProductGroupName;
	}


	public ActiveType getStatus() {
		return status;
	}


	public void setStatus(ActiveType status) {
		this.status = status;
	}


	public DisplayProductGroupType getType() {
		return type;
	}


	public void setType(DisplayProductGroupType type) {
		this.type = type;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getCreateUser() {
		return createUser;
	}


	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public String getUpdateUser() {
		return updateUser;
	}


	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	
}

package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffGroupDetail;
import ths.dms.core.entities.vo.StaffExportVO;
import ths.dms.core.entities.vo.StaffVO;

public class StaffFilterNew implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<Staff> kPaging;
	private KPaging<StaffExportVO> exPaging;
	private KPaging<StaffGroupDetail> sgdPaging;
	private KPaging<StaffVO> staffVOPaging;
	private Long staffId;
	private String staffCode;
	private String staffTypeCode;
	private Long shopId;
	private String shopGroupCode;
	private String staffName;
	private String mobilePhoneNum;
	private ActiveType status;
	private String shopCode;
	private StaffObjectType[] staffType;
	private List<Integer> lstStaffType;
	private ChannelTypeType channelTypeType;
	private List<Integer> lstChannelObjectType;
	private String saleTypeCode;
	private String shopName;
	private Boolean isGetShopOnly;
	private String shopCodeStaff;
	private String shopNameStaff;
	private String staffOwnerCode;
	private Boolean notInStaffGroup;
	private Long vungId;
	private Long staffGroupId;
	private List<Long> lstExceptId;
	private List<Long> lstMandatoryId;
	
	private Boolean applyUnitTree;
	private Boolean isInAnyStaffGroup;
	private Boolean isGetParent;
	private Boolean isGetAllChild;
	
	
	public Boolean getIsGetAllChild() {
		return isGetAllChild;
	}
	public void setIsGetAllChild(Boolean isGetAllChild) {
		this.isGetAllChild = isGetAllChild;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Boolean getIsGetParent() {
		return isGetParent;
	}
	public void setIsGetParent(Boolean isGetParent) {
		this.isGetParent = isGetParent;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShopGroupCode() {
		return shopGroupCode;
	}
	public void setShopGroupCode(String shopGroupCode) {
		this.shopGroupCode = shopGroupCode;
	}

	public KPaging<Staff> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<Staff> kPaging) {
		this.kPaging = kPaging;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffTypeCode() {
		return staffTypeCode;
	}
	public void setStaffTypeCode(String staffTypeCode) {
		this.staffTypeCode = staffTypeCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getMobilePhoneNum() {
		return mobilePhoneNum;
	}
	public void setMobilePhoneNum(String mobilePhoneNum) {
		this.mobilePhoneNum = mobilePhoneNum;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public StaffObjectType[] getStaffType() {
		return staffType;
	}
	public void setStaffType(StaffObjectType... staffType) {
		this.staffType = staffType;
	}
	public List<Integer> getLstStaffType() {
		return lstStaffType;
	}
	public void setLstStaffType(List<Integer> lstStaffType) {
		this.lstStaffType = lstStaffType;
	}
	public ChannelTypeType getChannelTypeType() {
		return channelTypeType;
	}
	public void setChannelTypeType(ChannelTypeType channelTypeType) {
		this.channelTypeType = channelTypeType;
	}
	public List<Integer> getLstChannelObjectType() {
		return lstChannelObjectType;
	}
	public void setLstChannelObjectType(List<Integer> lstChannelObjectType) {
		this.lstChannelObjectType = lstChannelObjectType;
	}
	public String getSaleTypeCode() {
		return saleTypeCode;
	}
	public void setSaleTypeCode(String saleTypeCode) {
		this.saleTypeCode = saleTypeCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Boolean getIsGetShopOnly() {
		return isGetShopOnly;
	}
	public void setIsGetShopOnly(Boolean isGetShopOnly) {
		this.isGetShopOnly = isGetShopOnly;
	}
	public String getShopCodeStaff() {
		return shopCodeStaff;
	}
	public void setShopCodeStaff(String shopCodeStaff) {
		this.shopCodeStaff = shopCodeStaff;
	}
	public String getShopNameStaff() {
		return shopNameStaff;
	}
	public void setShopNameStaff(String shopNameStaff) {
		this.shopNameStaff = shopNameStaff;
	}
	public String getStaffOwnerCode() {
		return staffOwnerCode;
	}
	public void setStaffOwnerCode(String staffOwnerCode) {
		this.staffOwnerCode = staffOwnerCode;
	}
	public Boolean getNotInStaffGroup() {
		return notInStaffGroup;
	}
	public void setNotInStaffGroup(Boolean notInStaffGroup) {
		this.notInStaffGroup = notInStaffGroup;
	}
	public Long getVungId() {
		return vungId;
	}
	public void setVungId(Long vungId) {
		this.vungId = vungId;
	}
	public Long getStaffGroupId() {
		return staffGroupId;
	}
	public void setStaffGroupId(Long staffGroupId) {
		this.staffGroupId = staffGroupId;
	}
	public Boolean getIsInAnyStaffGroup() {
		return isInAnyStaffGroup;
	}
	public void setIsInAnyStaffGroup(Boolean isInAnyStaffGroup) {
		this.isInAnyStaffGroup = isInAnyStaffGroup;
	}
	public List<Long> getLstExceptId() {
		return lstExceptId;
	}
	public void setLstExceptId(List<Long> lstExceptId) {
		this.lstExceptId = lstExceptId;
	}
	public List<Long> getLstMandatoryId() {
		return lstMandatoryId;
	}
	public void setLstMandatoryId(List<Long> lstMandatoryId) {
		this.lstMandatoryId = lstMandatoryId;
	}
	public KPaging<StaffGroupDetail> getSgdPaging() {
		return sgdPaging;
	}
	public void setSgdPaging(KPaging<StaffGroupDetail> sgdPaging) {
		this.sgdPaging = sgdPaging;
	}
	public Boolean getApplyUnitTree() {
		return applyUnitTree;
	}
	public void setApplyUnitTree(Boolean applyUnitTree) {
		this.applyUnitTree = applyUnitTree;
	}
	public KPaging<StaffExportVO> getExPaging() {
		return exPaging;
	}
	public void setExPaging(KPaging<StaffExportVO> exPaging) {
		this.exPaging = exPaging;
	}
	public KPaging<StaffVO> getStaffVOPaging() {
		return staffVOPaging;
	}
	public void setStaffVOPaging(KPaging<StaffVO> staffVOPaging) {
		this.staffVOPaging = staffVOPaging;
	}
	
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum GroupStaffPayrollObjectApplyType {
    
    SHOP (1),

    STAFF (2);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, GroupStaffPayrollObjectApplyType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    GroupStaffPayrollObjectApplyType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static GroupStaffPayrollObjectApplyType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, GroupStaffPayrollObjectApplyType>(
                    GroupStaffPayrollObjectApplyType.values().length);
            for (GroupStaffPayrollObjectApplyType e : GroupStaffPayrollObjectApplyType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;



public class ChannelTypeFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6974321976035585940L;

	private String channelTypeCode;
	private String channelTypeName;
	private Long parentId;
	private ChannelTypeType type;
	private ActiveType status;
	private Integer isEdit;
	private ActiveType statusSku;
	private ActiveType statusAmount;
	private Integer objectType;
	private List<Integer> lstObjectType;
	private BigDecimal saleAmount;
	private BigDecimal sku;
	private Boolean isGetOneChildLevel;
	private Boolean isGetParent;
	private Boolean isGetParentAndChild;
	private String order;
	private String sort;
	
	private Boolean isOrderByChannelTypeName;
	
	public String getChannelTypeCode() {
		return channelTypeCode;
	}
	public void setChannelTypeCode(String channelTypeCode) {
		this.channelTypeCode = channelTypeCode;
	}
	public String getChannelTypeName() {
		return channelTypeName;
	}
	public void setChannelTypeName(String channelTypeName) {
		this.channelTypeName = channelTypeName;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public ChannelTypeType getType() {
		return type;
	}
	public void setType(ChannelTypeType type) {
		this.type = type;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public Integer getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}
	public ActiveType getStatusSku() {
		return statusSku;
	}
	public void setStatusSku(ActiveType statusSku) {
		this.statusSku = statusSku;
	}
	public ActiveType getStatusAmount() {
		return statusAmount;
	}
	public void setStatusAmount(ActiveType statusAmount) {
		this.statusAmount = statusAmount;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public BigDecimal getSaleAmount() {
		return saleAmount;
	}
	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}
	public BigDecimal getSku() {
		return sku;
	}
	public void setSku(BigDecimal sku) {
		this.sku = sku;
	}
	public Boolean getIsGetOneChildLevel() {
		return isGetOneChildLevel;
	}
	public void setIsGetOneChildLevel(Boolean isGetOneChildLevel) {
		this.isGetOneChildLevel = isGetOneChildLevel;
	}
	public Boolean getIsGetParent() {
		return isGetParent;
	}
	public void setIsGetParent(Boolean isGetParent) {
		this.isGetParent = isGetParent;
	}
	public Boolean getIsOrderByChannelTypeName() {
		return isOrderByChannelTypeName;
	}
	public void setIsOrderByChannelTypeName(Boolean isOrderByChannelTypeName) {
		this.isOrderByChannelTypeName = isOrderByChannelTypeName;
	}
	public List<Integer> getLstObjectType() {
		return lstObjectType;
	}
	public void setLstObjectType(List<Integer> lstObjectType) {
		this.lstObjectType = lstObjectType;
	}
	public Boolean getIsGetParentAndChild() {
		return isGetParentAndChild;
	}
	public void setIsGetParentAndChild(Boolean isGetParentAndChild) {
		this.isGetParentAndChild = isGetParentAndChild;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
}

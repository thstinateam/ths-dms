package ths.dms.core.entities.enumtype;

import java.io.Serializable;

public class HTBHFilter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private Long focusProgramId;
	private String code;
	private String name;
	private ActiveType status;
	private Boolean isHO ;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getFocusProgramId() {
		return focusProgramId;
	}
	public void setFocusProgramId(Long focusProgramId) {
		this.focusProgramId = focusProgramId;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public Boolean getIsHO() {
		return isHO;
	}
	public void setIsHO(Boolean isHO) {
		this.isHO = isHO;
	}
}

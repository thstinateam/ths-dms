package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author hunglm16
 * @description Quy dinh Co hieu doi tuong
 */
public enum IsAttribute {

	IS(1),

	UN(0);

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, IsAttribute> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value the value
	 */
	IsAttribute(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author hunglm16
	 * @param value the value
	 * @return the gender type
	 */
	public static IsAttribute parseValue(Integer value) {
		if (value == null) {
			value = 0;
		}
		if (values == null) {
			values = new HashMap<Integer, IsAttribute>(IsAttribute.values().length);
			for (IsAttribute e : IsAttribute.values()) {
				values.put(e.getValue(), e);
			}
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai tinh trang quy dinh.
	 * 
	 * @author hunglm16
	 * @param value
	 * @return flag
	 */
	public static boolean checkIsRecode(Integer value) {
		if (value == null) {
			return false;
		}
		if (!IsAttribute.IS.getValue().equals(value) && !IsAttribute.UN.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

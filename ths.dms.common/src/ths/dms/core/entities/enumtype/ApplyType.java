/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum ApplyType {
    
	QUANTITY (1),
	
    AMOUNT (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ApplyType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ApplyType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ApplyType parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, ApplyType>(
                    ApplyType.values().length);
            for (ApplyType e : ApplyType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

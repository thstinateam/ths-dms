/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum EquipTradeType.
 * 
 * @author hunglm16
 * @since December 22,2014
 * @description 0: Giao nhan, 1: Chuyen kho, 2: Sua chua, 3: Bao mat, 4: Bao mat
 *              tu mobile, 5: Thu hoi thanh ly, 6: Thanh ly, 7: Kiem ke, 8:
 *              thanh toan sua chua
 */
public enum EquipTradeType {

	DELIVERY(0), // Giao nhan

	WAREHOUSE_TRANSFER(1), // Chuyen kho

	CORRECTED(2), // Sua chua

	NOTICE_LOST(3), // Bao mat

	NOTICE_LOSS_MOBILE(4), // Bao mat tu Mobile

	WITHDRAWAL_LIQUIDATION(5), // thu hoi thanh ly

	LIQUIDATION(6), // Thanh ly

	INVENTORY(7), // Kiem ke

	PAYMENT_CORRECTION(8),// thanh toan sua chua
	
	EQUIP_ALLOCATION(9), // Cap moi
	
	EQUIP_STOCK_ADJUST(10), // Cap nhat kho dieu chinh
	
	EQUIP_LEND(11), // De nghi muon thiet bi
	
	EQUIP_SUGGEST_EVICTION(12) // De nghi thu hoi thiet bi
	;

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, EquipTradeType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	EquipTradeType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static EquipTradeType parseValue(Integer value) {
		if (value != null && value == -1) {
			value = -2;
		}
		if (values == null) {
			values = new HashMap<Integer, EquipTradeType>(EquipTradeType.values().length);
			for (EquipTradeType e : EquipTradeType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai tinh trang quy dinh.
	 * 
	 * @author hunglm16
	 * @param value
	 * @return flag
	 */
	public static boolean checkIsRecode(Integer value) {
		if (value == null) {
			return false;
		}
		if (!EquipTradeType.DELIVERY.getValue().equals(value) && !EquipTradeType.WAREHOUSE_TRANSFER.getValue().equals(value) && !EquipTradeType.CORRECTED.getValue().equals(value) && !EquipTradeType.NOTICE_LOST.getValue().equals(value) && !EquipTradeType.NOTICE_LOSS_MOBILE.getValue().equals(value) && !EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue().equals(value) && !EquipTradeType.LIQUIDATION.getValue().equals(value) && !EquipTradeType.PAYMENT_CORRECTION.getValue().equals(value)) {
			return false;
		}
		return true;
	}

}

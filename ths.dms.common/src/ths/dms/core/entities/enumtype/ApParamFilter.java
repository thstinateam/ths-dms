package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.vo.ApParamEquipVO;
import ths.dms.core.entities.vo.ApParamVO;

// TODO: Auto-generated Javadoc
/**
 * Class ApParamFilter duoc su dung de lam dieu kien loc cho Table ApParam khi viet cau truy van.
 * @author tulv2
 * @since 17.09.2014
 * */
public class ApParamFilter implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private Long shopId;

	/** The ap param code. */
	String apParamCode;
	
	/** The ap param name. */
	String apParamName;
	
	/** The type. */
	ApParamType type;
	
	/** The status. */
	ActiveType status;
	
	/** The description. */
	String description;
	
	/** The value. */
	String value;
	
	/** The type str. */
	String typeStr;
	
	/** The k paging. */
	KPaging<ApParam> kPaging;
	
	/** The paging vo. */
	private KPaging<ApParamVO> pagingVO;
	
	private String order;
	private String sort;
	
	private List<String> lstApCode;
	
	/** vuongmq; 15/04/2015*/
	private KPaging<ApParamEquip> KPagingParamEquip;
	private KPaging<ApParamEquipVO> pagingVOEquip;
	
	
	public List<String> getLstApCode() {
		return lstApCode;
	}

	public void setLstApCode(List<String> lstApCode) {
		this.lstApCode = lstApCode;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the ap param code.
	 *
	 * @return the ap param code
	 */
	public String getApParamCode() {
		return apParamCode;
	}

	/**
	 * Sets the ap param code.
	 *
	 * @param apParamCode the new ap param code
	 */
	public void setApParamCode(String apParamCode) {
		this.apParamCode = apParamCode;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public ApParamType getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(ApParamType type) {
		this.type = type;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ActiveType getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ActiveType status) {
		this.status = status;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the ap param name.
	 *
	 * @return the ap param name
	 */
	public String getApParamName() {
		return apParamName;
	}

	/**
	 * Sets the ap param name.
	 *
	 * @param apParamName the new ap param name
	 */
	public void setApParamName(String apParamName) {
		this.apParamName = apParamName;
	}

	/**
	 * Gets the type str.
	 *
	 * @return the type str
	 */
	public String getTypeStr() {
		return typeStr;
	}

	/**
	 * Sets the type str.
	 *
	 * @param typeStr the new type str
	 */
	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

	/**
	 * Gets the k paging.
	 *
	 * @return the k paging
	 */
	public KPaging<ApParam> getkPaging() {
		return kPaging;
	}

	/**
	 * Sets the k paging.
	 *
	 * @param kPaging the new k paging
	 */
	public void setkPaging(KPaging<ApParam> kPaging) {
		this.kPaging = kPaging;
	}

	/**
	 * Gets the paging vo.
	 *
	 * @return the paging vo
	 */
	public KPaging<ApParamVO> getPagingVO() {
		return pagingVO;
	}

	/**
	 * Sets the paging vo.
	 *
	 * @param pagingVO the new paging vo
	 */
	public void setPagingVO(KPaging<ApParamVO> pagingVO) {
		this.pagingVO = pagingVO;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	/** @return the kPagingParamEquip */
	public KPaging<ApParamEquip> getKPagingParamEquip() {
		return KPagingParamEquip;
	}

	/** @param kPagingParamEquip the kPagingParamEquip to set */
	public void setKPagingParamEquip(KPaging<ApParamEquip> kPagingParamEquip) {
		KPagingParamEquip = kPagingParamEquip;
	}

	/** @return the pagingVOEquip */
	public KPaging<ApParamEquipVO> getPagingVOEquip() {
		return pagingVOEquip;
	}

	/** @param pagingVOEquip the pagingVOEquip to set */
	public void setPagingVOEquip(KPaging<ApParamEquipVO> pagingVOEquip) {
		this.pagingVOEquip = pagingVOEquip;
	}
	
	
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum PaymentType {
    
    MONEY (0),

    BANK_TRANSFER (1);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, PaymentType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    PaymentType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static PaymentType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, PaymentType>(
                    PaymentType.values().length);
            for (PaymentType e : PaymentType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author phut
 * @description nhom thiet bi hoac u ke
 */
public enum EquipObjectType {
	ALL(0),
	
    GROUP(1),
    
    SHELF(2),
    
    EQUIP(3)
    ;
    
    /** The value. */
    private Integer value;
    /** The values. */
    private static Map<Integer, EquipObjectType> values = null;
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    EquipObjectType(Integer value) {
        this.value = value;
    }
    /**
     * Parses the value.
     * 
     * @author hunglm16
     * @param value the value
     * @return the gender type
     */
    public static EquipObjectType parseValue(Integer value) {
        if(value==null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, EquipObjectType>(
                    EquipObjectType.values().length);
            for (EquipObjectType e : EquipObjectType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
    /**
     * Kiem tra co ton tai tinh trang quy dinh.
     * 
     * @author hunglm16
     * @param value
     * @return flag
     */
    public static boolean checkIsRecode(Integer value) {
    	if(value == null){
    		return false;
    	}
    	if(!EquipObjectType.GROUP.getValue().equals(value) && !EquipObjectType.SHELF.getValue().equals(value)){
    		return false;
    	}
        return true;
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum MediaType {
    
	IMAGE (0),
	
    VIDEO (1);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, MediaType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    MediaType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static MediaType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, MediaType>(
                    MediaType.values().length);
            for (MediaType e : MediaType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum Decentralization Shop
 * 
 * @author hunglm16
 * @since August 18,2014
 * @description Phu thuoc Function : decode_islevel_vnm duoc tao trong Database 
 * Dung de kiem tra specific
 */
public enum ShopDecentralizationSTT {

	VNM(1),
	
	KENH(2),

	MIEN(3),
	
	VUNG(4),

	NPP(5),
	
	UNALL(6),
	
	ALL(-2) //Dung cho specific trong form, neu ton tai co nghia la lay het Shop trong Role duoc chon
	;

	///// Tat ca con lai tra ra 6

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, ShopDecentralizationSTT> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	ShopDecentralizationSTT(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static ShopDecentralizationSTT parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, ShopDecentralizationSTT>(ShopDecentralizationSTT.values().length);
			for (ShopDecentralizationSTT e : ShopDecentralizationSTT.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

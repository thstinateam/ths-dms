/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum ApprovalType {
    
    NOT_YET (1),

    APPROVED (2),
    
    DESTROY (3);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ApprovalType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ApprovalType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ApprovalType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ApprovalType>(
                    ApprovalType.values().length);
            for (ApprovalType e : ApprovalType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

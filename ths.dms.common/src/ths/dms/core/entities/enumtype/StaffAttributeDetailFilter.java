package ths.dms.core.entities.enumtype;

import java.io.Serializable;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffAttribute;

public class StaffAttributeDetailFilter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long staffAttrId;
	
	private Long staffAttrDetailId;
	
	private String code;
	
	private String name;

	private ActiveType status;
	
	private Staff staff;
	
	private StaffAttribute staffAttribute;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public StaffAttribute getStaffAttribute() {
		return staffAttribute;
	}

	public void setStaffAttribute(StaffAttribute staffAttribute) {
		this.staffAttribute = staffAttribute;
	}

	public Long getStaffAttrId() {
		return staffAttrId;
	}

	public void setStaffAttrId(Long staffAttrId) {
		this.staffAttrId = staffAttrId;
	}

	public Long getStaffAttrDetailId() {
		return staffAttrDetailId;
	}

	public void setStaffAttrDetailId(Long staffAttrDetailId) {
		this.staffAttrDetailId = staffAttrDetailId;
	}

	

}

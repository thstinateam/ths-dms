package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;


/**
 * enum type: type debit_detail
 * 
 * @author lacnv1
 * @since Apr 4, 2014
 */
public enum DebitDetailType {
	NHAP_VNM(0),
	TRA_VNM(1),
	TANG_NO_VNM(2),
	GIAM_NO_VNM(3),
	BAN_HANG(4),
	KH_TRA_HANG(5),
	TANG_NO_KH(6),
	GIAM_NO_KH(7);
	
    private Integer value;
    
    private static Map<Integer, DebitDetailType> values = null;
    
    public Integer getValue() {
        return value;
    }
    
    DebitDetailType(Integer value) {
        this.value = value;
    }
    
    public static DebitDetailType parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, DebitDetailType>(
            		DebitDetailType.values().length);
            for (DebitDetailType e : DebitDetailType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}
/**
 * Khai bao Import thu vien
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Type Stock Equip Lend
 * 
 * @author nhutnn
 * @since 06/07/2015
 * @description 1:  Kho NCC, 2: Kho cong ty
 */
public enum TypeStockEquipLend {

	NCC(1),// Kho NCC

	COMPANY(2) // Kho cong ty

	;

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, TypeStockEquipLend> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	TypeStockEquipLend(Integer value) {
		this.value = value;
	}

	public static TypeStockEquipLend parseValue(Integer value) {
		if (value == null) {
			value = -1;
		}
		if (values == null) {
			values = new HashMap<Integer, TypeStockEquipLend>(TypeStockEquipLend.values().length);
			for (TypeStockEquipLend e : TypeStockEquipLend.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	public static boolean checkIsRecode(Integer value) {
		if (value == null) {
			return false;
		}
		if (!TypeStockEquipLend.NCC.getValue().equals(value) && !TypeStockEquipLend.COMPANY.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

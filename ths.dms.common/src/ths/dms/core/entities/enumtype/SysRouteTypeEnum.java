/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum SysRouteTypeEnum in param. Phuc vu cho KPI, ASO
 * 
 * @author hunglm16
 * @since October 12,2015
 */
public enum SysRouteTypeEnum {

	CODE("SYS_ROUTE_TYPE"),

	TYPE("SYS_CONFIG"),

	IN_ROUTE("1"),

	OUT_ROUTE("2"),

	ALL("3");

	/** The value. */
	private String value;

	/** The values. */
	private static Map<String, SysRouteTypeEnum> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	SysRouteTypeEnum(String value) {
		this.value = value;
	}

	/**
	 * Lay gia tri enum
	 * 
	 * @author hunglm16
	 * @param value
	 * @return
	 * @since October 12,2015
	 */
	public static SysRouteTypeEnum parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, SysRouteTypeEnum>(SysRouteTypeEnum.values().length);
			for (SysRouteTypeEnum e : SysRouteTypeEnum.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra gia Value cau hinh hop le
	 * 
	 * @author hunglm16
	 * @param val
	 * @param fixed
	 *            1:
	 * @return true: thoa man
	 * @return false: khong hop le
	 * 
	 * @since October 12, 2015
	 */
	public static boolean checkValueApp(Integer val) {
		if (val == null || (!val.equals(Integer.valueOf(SysRouteTypeEnum.IN_ROUTE.getValue())) && !val.equals(Integer.valueOf(SysRouteTypeEnum.OUT_ROUTE.getValue())) && !val.equals(Integer.valueOf(SysRouteTypeEnum.ALL.getValue())))) {
			return false;
		}
		return true;
	}
}

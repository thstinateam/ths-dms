/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum ChannelTypeType {
    
    SHOP (1),

    STAFF (2),
    
    CUSTOMER (3),
    
    CAR (4),
    
    SALE_MAN (6),
    
    CAR_LABEL (7),
    
    ORIGIN (8),
    
    PRODUCT (9),   //@tientv11
    
    CARD_SALEMT(11),//TUNGTT
    
    CUSTOMER_SALEMT(10);//LocTT1
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ChannelTypeType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ChannelTypeType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ChannelTypeType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ChannelTypeType>(
                    ChannelTypeType.values().length);
            for (ChannelTypeType e : ChannelTypeType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

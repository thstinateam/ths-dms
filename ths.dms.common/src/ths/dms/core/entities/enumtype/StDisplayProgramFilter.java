
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;

public class StDisplayProgramFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5719535609658660659L;

	private String displayProgramCode;
	
	private String displayProgramName;
	
	private ActiveType status;
	
	private Date fromDate;
	
	private Date toDate;
	
	private Long displayProgramId;
	
	private Long shopId;

	public String getDisplayProgramCode() {
		return displayProgramCode;
	}

	public void setDisplayProgramCode(String displayProgramCode) {
		this.displayProgramCode = displayProgramCode;
	}

	public String getDisplayProgramName() {
		return displayProgramName;
	}

	public void setDisplayProgramName(String displayProgramName) {
		this.displayProgramName = displayProgramName;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getDisplayProgramId() {
		return displayProgramId;
	}

	public void setDisplayProgramId(Long displayProgramId) {
		this.displayProgramId = displayProgramId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
}

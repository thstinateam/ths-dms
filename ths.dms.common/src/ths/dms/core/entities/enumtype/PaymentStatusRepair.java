/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author vuongmq
 * @date 19/06/2015
 * @description Thanh toan phieu sua chua cua module Thiet bi
 */
public enum PaymentStatusRepair {
    
	// Trạng thái thanh toán: 0: chưa tham gia thanh toán, 1: Chờ thanh toán, 2: Đã thanh toán
	
    NOT_PAID_YET (0), // Chua thanh toan
    
    PAID_TRANSACTION(1), // cho thanh toan
    
    PAID (2), // Da thanh toan
    ;
    
    /** The value. */
    private Integer value;
    /** The values. */
    private static Map<Integer, PaymentStatusRepair> values = null;
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    PaymentStatusRepair(Integer value) {
        this.value = value;
    }
    /**
     * Parses the value.
     * 
     * @author hunglm16
     * @param value the value
     * @return the gender type
     */
    public static PaymentStatusRepair parseValue(Integer value) {
        if(value==null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, PaymentStatusRepair>(PaymentStatusRepair.values().length);
            for (PaymentStatusRepair e : PaymentStatusRepair.values()) {
                values.put(e.getValue(), e);
            }
        }
        return values.get(value);
    }
}
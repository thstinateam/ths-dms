package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.RoutingCar;
import ths.dms.core.entities.vo.CarVO;

public class CarFilter implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<CarVO> kPaging;
	private KPaging<RoutingCar> kPRoutingCar;
	private Long shopId;
	private List<Long> lstShopId;
	private Long carId;
	private String shopCode;
	private String shopName;
	private String carNumber;
	private Long parentShopId;
	private Boolean isWarning;
	private Date deliveryDate;
	private Integer rownum;
	private Date fromDate;
	private Date toDate;
	private Float latLngDistance;//VT14
	
	public KPaging<CarVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<CarVO> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getCarId() {
		return carId;
	}
	public void setCarId(Long carId) {
		this.carId = carId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getCarNumber() {
		return carNumber;
	}
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Long getParentShopId() {
		return parentShopId;
	}
	public void setParentShopId(Long parentShopId) {
		this.parentShopId = parentShopId;
	}
	public Boolean getIsWarning() {
		return isWarning;
	}
	public void setIsWarning(Boolean isWarning) {
		this.isWarning = isWarning;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Integer getRownum() {
		return rownum;
	}
	public void setRownum(Integer rownum) {
		this.rownum = rownum;
	}
	public KPaging<RoutingCar> getkPRoutingCar() {
		return kPRoutingCar;
	}
	public void setkPRoutingCar(KPaging<RoutingCar> kPRoutingCar) {
		this.kPRoutingCar = kPRoutingCar;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public List<Long> getLstShopId() {
		return lstShopId;
	}
	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}
	public Float getLatLngDistance() {
		return latLngDistance;
	}
	public void setLatLngDistance(Float latLngDistance) {
		this.latLngDistance = latLngDistance;
	}
	
	
}

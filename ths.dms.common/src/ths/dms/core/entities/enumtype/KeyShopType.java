/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum KeyShopType {
    
    KEY_SHOP (1),

    DISPLAY (2);
    
    
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, KeyShopType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    KeyShopType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static KeyShopType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, KeyShopType>(
                    KeyShopType.values().length);
            for (KeyShopType e : KeyShopType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
    
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum SaleOrderStatus {
    
	//-2: don hang tao tren tablet chua yeu cau xac nhan, --> 2 
	//-1: don hang tao tren tablet yeu cau xac nhan, 	  --> 3
	// 0: don hang chua duyet,
	// 1: don hang da duyet 							  
	//-3: huy do qua ngay khong phe duyet				  --> 4
	
	//2 đơn hàng tạo trên tablet chưa yêu cầu xác nhận, 
	//3 đơn hàng trên tablet yêu cầu xác nhận (Huy)
	//0 đơn hàng chưa duyệt, 
	//1 đơn hàng đã duyệt, 
	//4 hủy do qua ngày không phê duyệt
	
    
    
    NOT_YET_APPROVE (0),
    
    APPROVED (1),
    
    TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM (2),

    TABLET_CREATED_AND_REQUIRED_CONFIRM (3),
    
    CANCEL (4);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, SaleOrderStatus> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    SaleOrderStatus(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static SaleOrderStatus parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, SaleOrderStatus>(
                    SaleOrderStatus.values().length);
            for (SaleOrderStatus e : SaleOrderStatus.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Type vi tri thiet bi
 * 
 * @author tamvnm
 */
public enum EquipEvictionPositionType{
    
    NPP (1),

    DIEM_BAN (2),
        
    CHO_CHUYEN_KHACH_HANG_KHAC (3);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, EquipEvictionPositionType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    EquipEvictionPositionType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static EquipEvictionPositionType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, EquipEvictionPositionType>(
                    EquipEvictionPositionType.values().length);
            for (EquipEvictionPositionType e : EquipEvictionPositionType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

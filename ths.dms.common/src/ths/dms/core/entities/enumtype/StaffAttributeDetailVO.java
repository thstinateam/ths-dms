package ths.dms.core.entities.enumtype;

import java.io.Serializable;

public class StaffAttributeDetailVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long staffAttrId;
	private Long staffAttrDetailId;
	private Long enumId;
	private Integer status;
	private String enumCode;
	private String enumValue;

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getStaffAttrId() {
		return staffAttrId;
	}
	public void setStaffAttrId(Long staffAttrId) {
		this.staffAttrId = staffAttrId;
	}
	public Long getStaffAttrDetailId() {
		return staffAttrDetailId;
	}
	public void setStaffAttrDetailId(Long staffAttrDetailId) {
		this.staffAttrDetailId = staffAttrDetailId;
	}
	public Long getEnumId() {
		return enumId;
	}
	public void setEnumId(Long enumId) {
		this.enumId = enumId;
	}
	public String getEnumCode() {
		return enumCode;
	}
	public void setEnumCode(String enumCode) {
		this.enumCode = enumCode;
	}
	public String getEnumValue() {
		return enumValue;
	}
	public void setEnumValue(String enumValue) {
		this.enumValue = enumValue;
	}
	
}

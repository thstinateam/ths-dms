package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;

public class WorkingDateConfigFilter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long shopId;
	
	private WorkingDateType type;
	
	private Date fromDate;
	
	private Date toDate;

	private ActiveType status;

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public WorkingDateType getType() {
		return type;
	}

	public void setType(WorkingDateType type) {
		this.type = type;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	

}

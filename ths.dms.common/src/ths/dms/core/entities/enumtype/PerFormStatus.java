/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum PerFormStatus {

	ONGOING(2),

	SUCCESS(1),

	CANCEL(0);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, PerFormStatus> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	PerFormStatus(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static PerFormStatus parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, PerFormStatus>(PerFormStatus.values().length);
			for (PerFormStatus e : PerFormStatus.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum SaleObjectType.
 * 
 * @author huytran
 */
public enum SalePlanOwnerType {
    
    /** NPP */
    SHOP(3),
    
    /** NVBH */
    SALEMAN(1);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, SalePlanOwnerType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    SalePlanOwnerType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static SalePlanOwnerType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, SalePlanOwnerType>(
                    SalePlanOwnerType.values().length);
            for (SalePlanOwnerType e : SalePlanOwnerType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

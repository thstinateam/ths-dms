/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DefaultType.
 * 
 * @author vuongmq
 */
public enum DefaultType {
    
	DEFAULT (1),
    
	NOTDEFAULT (0);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, DefaultType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    DefaultType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static DefaultType parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, DefaultType>(
                    DefaultType.values().length);
            for (DefaultType e : DefaultType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

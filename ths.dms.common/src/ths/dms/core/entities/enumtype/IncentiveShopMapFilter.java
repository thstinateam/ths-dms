/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;



public class IncentiveShopMapFilter implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7457477733939615641L;

	private Long programId;
	
	private String shopCode;
	
	private String shopName;
	
	private ActiveType status;
	
	private Long parentShopId;

	public Long getProgramId() {
		return programId;
	}

	public void setProgramId(Long programId) {
		this.programId = programId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Long getParentShopId() {
		return parentShopId;
	}

	public void setParentShopId(Long parentShopId) {
		this.parentShopId = parentShopId;
	}
	
}

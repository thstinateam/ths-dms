package ths.dms.core.entities.enumtype;
/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ths.dms.core.entities.vo.FileVO;

/**
 * Filter dung chung cho quan ly thiet bi
 * 
 * @author hoanv25
 * @since December 10,2014
 * 
 * @author nhutnn
 * @since 15/12/2014
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description ra soat va phan hoach
 */
public class EquipmentFilter<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	private KPaging<T> kPaging;
	
	private List<Long> lstLong;
	private List<Long> lstEquipId;
	private List<Long> lstId;

	private List<String> lstEquipDelete;
	private List<String> lstEquipAdd;
	private List<FileVO> lstFileVo;
	
	private Map<Long, Long> mapLongLong = new HashMap<Long, Long>();
	
	private Long id;
	private Long equipGroupId;
	private List<Long> equipGroupIds;
	private List<Long> lstShopId;
	private List<Integer> lstRecordStatus;
	private List<String> lstEquipmentCode;
	private Long equipCategoryId;
	private Long equipProviderId;
	private Long idRecordDelivery;
	private Long idLiquidation;
	private Long shopParentId;
	private Long idExcepted;
	private Long equipStockTransFormId;
	private Long equipLostId;
	private Long equipId;
	private Long stockId;
	private Long eqDeliveryRecId;
	private Long shopRoot;
	private Long staffRoot;
	private Long EquipLendId;
	private Long periodId;
	private Long equipSuggestEvictionId;
	private Long customerId;
	private Long staffRootId;
	private Long shopRootId;
	private Long roleId;
	
	private Integer status;
	private Integer statusPerform;
	private Integer type;
	private Integer quantity;	
	private Integer fromMonth;
	private Integer toMonth;
	private Integer customerType;
	private Integer statusRecord;
	private Integer recordStatus;
	private Integer statusDelivery;
	private Integer statusEquip;
	private Integer statusStock;
	private Integer stockType;
	private Integer tradeType;
	private Integer tradeStatus;
	private Integer usageStatus;
	private Integer yearManufacture;
	private Integer statusPrint;
	private Integer flagCms;

	private BigDecimal toCapacity;
	private BigDecimal fromCapacity;
	private Float aMount;
	
	private BigDecimal price;

	private String code;	
	private String name;
	private String equipCategoryCode;
	private Date createDate;
	private Date updateDate;
	private Date fromDate;
	private Date toDate;
	private Date createFromDate;
	private Date createToDate;
	private Date fromContractDate;
	private Date toContractDate;

	private String brand;	
	private String groupCode;
	private String staffCode;
	private String stockCode;
	private String createUser;
	private String updateUser;
	private String recordCode;
	private String seriNumber;
	private String categoryCode;
	private String providerCode;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String numberContract;
	private String docNumber;
	private String healthStatus;
	private String firstDateInUseStr; 
	private String manufacturingYearStr; 
	private String warrantyExpiredDateStr;
	private String healthStatusNew;
	private String fromStock;
	private Long fromStockId;
	private String toStock;
	private Long shopId;
	private String shopCode;
	private String shopName;
	private String equipCode;
	private String strListShopId;
	private String equipLendCode;
	private String curShopCode;
	private String stockName;
	private String equipGroupName;
	private String arrShop;
	
	private Boolean firstRow;
	private String equipImportRecordCode;
	
	private Integer flagMobileLost; // 1: bao mat mobile; 0: bao mat truc tiep web
	private Integer flagUpdate;
	private Integer flagStockCom;
	
	private boolean flagNotUsageStatus; // vuongmq; 03/07/2015; dung import sua chua; khong lay trang thai su dung va o kho
	private String address;
	private Boolean isCheckStatus;
	private String orderStatistic;
	/**
	 * Khai bao Getter/Setter
	 * */
	
	public List<Long> getLstId() {
		return lstId;
	}

	public Long getEqDeliveryRecId() {
		return eqDeliveryRecId;
	}

	public void setEqDeliveryRecId(Long eqDeliveryRecId) {
		this.eqDeliveryRecId = eqDeliveryRecId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

	public Map<Long, Long> getMapLongLong() {
		return mapLongLong;
	}

	public void setMapLongLong(Map<Long, Long> mapLongLong) {
		this.mapLongLong = mapLongLong;
	}

	public Integer getRecordStatus() {
		return recordStatus;
	}

	public List<Long> getLstEquipId() {
		return lstEquipId;
	}

	public void setLstEquipId(List<Long> lstEquipId) {
		this.lstEquipId = lstEquipId;
	}

	public List<Long> getLstLong() {
		return lstLong;
	}

	public void setLstLong(List<Long> lstLong) {
		this.lstLong = lstLong;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}

	public String getHealthStatusNew() {
		return healthStatusNew;
	}

	public void setHealthStatusNew(String healthStatusNew) {
		this.healthStatusNew = healthStatusNew;
	}

	public String getFirstDateInUseStr() {
		return firstDateInUseStr;
	}

	public void setFirstDateInUseStr(String firstDateInUseStr) {
		this.firstDateInUseStr = firstDateInUseStr;
	}

	public String getManufacturingYearStr() {
		return manufacturingYearStr;
	}

	public void setManufacturingYearStr(String manufacturingYearStr) {
		this.manufacturingYearStr = manufacturingYearStr;
	}

	public String getWarrantyExpiredDateStr() {
		return warrantyExpiredDateStr;
	}

	public void setWarrantyExpiredDateStr(String warrantyExpiredDateStr) {
		this.warrantyExpiredDateStr = warrantyExpiredDateStr;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getFirstRow() {
		return firstRow;
	}

	public Long getEquipCategoryId() {
		return equipCategoryId;
	}

	public void setEquipCategoryId(Long equipCategoryId) {
		this.equipCategoryId = equipCategoryId;
	}

	public void setFirstRow(Boolean firstRow) {
		this.firstRow = firstRow;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public Long getEquipProviderId() {
		return equipProviderId;
	}

	public void setEquipProviderId(Long equipProviderId) {
		this.equipProviderId = equipProviderId;
	}

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public Integer getStockType() {
		return stockType;
	}

	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}

	public Integer getTradeType() {
		return tradeType;
	}

	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}

	public Integer getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(Integer tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public Integer getUsageStatus() {
		return usageStatus;
	}

	public void setUsageStatus(Integer usageStatus) {
		this.usageStatus = usageStatus;
	}

	private List<Long> lstIdRecord;
	
	public Date getCreateDate() {
		return createDate;
	}

	public KPaging<T> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}



	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getNumberContract() {
		return numberContract;
	}

	public void setNumberContract(String numberContract) {
		this.numberContract = numberContract;
	}

	public String getRecordCode() {
		return recordCode;
	}

	public void setRecordCode(String recordCode) {
		this.recordCode = recordCode;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getIdRecordDelivery() {
		return idRecordDelivery;
	}

	public void setIdRecordDelivery(Long idRecordDelivery) {
		this.idRecordDelivery = idRecordDelivery;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public Integer getYearManufacture() {
		return yearManufacture;
	}

	public void setYearManufacture(Integer yearManufacture) {
		this.yearManufacture = yearManufacture;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public Integer getStatusRecord() {
		return statusRecord;
	}
	public void setStatusRecord(Integer statusRecord) {
		this.statusRecord = statusRecord;
	}
	public Integer getStatusDelivery() {
		return statusDelivery;
	}
	public void setStatusDelivery(Integer statusDelivery) {
		this.statusDelivery = statusDelivery;
	}
	public Integer getStatusStock() {
		return statusStock;
	}

	public void setStatusStock(Integer statusStock) {
		this.statusStock = statusStock;
	}

	public Integer getStatusEquip() {
		return statusEquip;
	}
	public void setStatusEquip(Integer statusEquip) {
		this.statusEquip = statusEquip;
	}
	public List<Long> getLstIdRecord() {
		return lstIdRecord;
	}
	public void setLstIdRecord(List<Long> lstIdRecord) {
		this.lstIdRecord = lstIdRecord;
	}
		
	public Integer getQuantity() {
		return quantity;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}	
	public BigDecimal getToCapacity() {
		return toCapacity;
	}

	public BigDecimal getFromCapacity() {
		return fromCapacity;
	}

	public void setToCapacity(BigDecimal toCapacity) {
		this.toCapacity = toCapacity;
	}

	public void setFromCapacity(BigDecimal fromCapacity) {
		this.fromCapacity = fromCapacity;
	}

	public Integer getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(Integer fromMonth) {
		this.fromMonth = fromMonth;
	}

	public Integer getToMonth() {
		return toMonth;
	}

	public void setToMonth(Integer toMonth) {
		this.toMonth = toMonth;
	}

	public Integer getCustomerType() {
		return customerType;
	}

	public void setCustomerType(Integer customerType) {
		this.customerType = customerType;
	}

	public Float getaMount() {
		return aMount;
	}

	public void setaMount(Float aMount) {
		this.aMount = aMount;
	}

	public Long getShopParentId() {
		return shopParentId;
	}

	public void setShopParentId(Long shopParentId) {
		this.shopParentId = shopParentId;
	}

	public List<String> getLstEquipDelete() {
		return lstEquipDelete;
	}

	public void setLstEquipDelete(List<String> lstEquipDelete) {
		this.lstEquipDelete = lstEquipDelete;
	}

	public List<String> getLstEquipAdd() {
		return lstEquipAdd;
	}

	public void setLstEquipAdd(List<String> lstEquipAdd) {
		this.lstEquipAdd = lstEquipAdd;
	}
	public String getFromStock() {
		return fromStock;
	}

	public void setFromStock(String fromStock) {
		this.fromStock = fromStock;
	}

	public String getToStock() {
		return toStock;
	}

	public void setToStock(String toStock) {
		this.toStock = toStock;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public Long getIdLiquidation() {
		return idLiquidation;
	}

	public void setIdLiquidation(Long idLiquidation) {
		this.idLiquidation = idLiquidation;
	}


	public Long getEquipStockTransFormId() {
		return equipStockTransFormId;
	}

	public void setEquipStockTransFormId(Long equipStockTransFormId) {
		this.equipStockTransFormId = equipStockTransFormId;
	}


	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public Long getEquipLostId() {
		return equipLostId;
	}

	public void setEquipLostId(Long equipLostId) {
		this.equipLostId = equipLostId;
	}

	public List<Long> getEquipGroupIds() {
		return equipGroupIds;
	}

	public void setEquipGroupIds(List<Long> equipGroupIds) {
		this.equipGroupIds = equipGroupIds;
	}

	public Long getFromStockId() {
		return fromStockId;
	}

	public void setFromStockId(Long fromStockId) {
		this.fromStockId = fromStockId;
	}

	public String getEquipImportRecordCode() {
		return equipImportRecordCode;
	}

	public void setEquipImportRecordCode(String equipImportRecordCode) {
		this.equipImportRecordCode = equipImportRecordCode;
	}

	public List<FileVO> getLstFileVo() {
		return lstFileVo;
	}

	public void setLstFileVo(List<FileVO> lstFileVo) {
		this.lstFileVo = lstFileVo;
	}

	public Integer getStatusPerform() {
		return statusPerform;
	}

	public void setStatusPerform(Integer statusPerform) {
		this.statusPerform = statusPerform;
	}

	public Long getShopRoot() {
		return shopRoot;
	}

	public void setShopRoot(Long shopRoot) {
		this.shopRoot = shopRoot;
	}

	public Long getStaffRoot() {
		return staffRoot;
	}

	public void setStaffRoot(Long staffRoot) {
		this.staffRoot = staffRoot;
	}

	public String getEquipLendCode() {
		return equipLendCode;
	}

	public void setEquipLendCode(String equipLendCode) {
		this.equipLendCode = equipLendCode;
	}

	public Date getFromContractDate() {
		return fromContractDate;
	}

	public void setFromContractDate(Date fromContractDate) {
		this.fromContractDate = fromContractDate;
	}

	public Date getToContractDate() {
		return toContractDate;
	}

	public void setToContractDate(Date toContractDate) {
		this.toContractDate = toContractDate;
	}

	public Integer getStatusPrint() {
		return statusPrint;
	}

	public void setStatusPrint(Integer statusPrint) {
		this.statusPrint = statusPrint;
	}

	public String getCurShopCode() {
		return curShopCode;
	}

	public void setCurShopCode(String curShopCode) {
		this.curShopCode = curShopCode;
	}

	public Long getEquipLendId() {
		return EquipLendId;
	}

	public void setEquipLendId(Long equipLendId) {
		EquipLendId = equipLendId;
	}

	public List<String> getLstEquipmentCode() {
		return lstEquipmentCode;
	}

	public void setLstEquipmentCode(List<String> lstEquipmentCode) {
		this.lstEquipmentCode = lstEquipmentCode;
	}

	public String getEquipCategoryCode() {
		return equipCategoryCode;
	}

	public void setEquipCategoryCode(String equipCategoryCode) {
		this.equipCategoryCode = equipCategoryCode;
	}

	public Long getIdExcepted() {
		return idExcepted;
	}

	public void setIdExcepted(Long idExcepted) {
		this.idExcepted = idExcepted;
	}

	public Long getPeriodId() {
		return periodId;
	}

	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	public Integer getFlagCms() {
		return flagCms;
	}

	public void setFlagCms(Integer flagCms) {
		this.flagCms = flagCms;
	}

	public Integer getFlagMobileLost() {
		return flagMobileLost;
	}

	public void setFlagMobileLost(Integer flagMobileLost) {
		this.flagMobileLost = flagMobileLost;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public boolean isFlagNotUsageStatus() {
		return flagNotUsageStatus;
	}

	public void setFlagNotUsageStatus(boolean flagNotUsageStatus) {
		this.flagNotUsageStatus = flagNotUsageStatus;
	}

	public Integer getFlagUpdate() {
		return flagUpdate;
	}

	public void setFlagUpdate(Integer flagUpdate) {
		this.flagUpdate = flagUpdate;
	}

	public String getEquipGroupName() {
		return equipGroupName;
	}

	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}

	public Integer getFlagStockCom() {
		return flagStockCom;
	}

	public void setFlagStockCom(Integer flagStockCom) {
		this.flagStockCom = flagStockCom;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getEquipSuggestEvictionId() {
		return equipSuggestEvictionId;
	}

	public void setEquipSuggestEvictionId(Long equipSuggestEvictionId) {
		this.equipSuggestEvictionId = equipSuggestEvictionId;
	}

	public String getArrShop() {
		return arrShop;
	}

	public void setArrShop(String arrShop) {
		this.arrShop = arrShop;
	}

	public List<Long> getLstShopId() {
		return lstShopId;
	}

	public void setLstShopId(List<Long> lstShopId) {
		this.lstShopId = lstShopId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Date getCreateFromDate() {
		return createFromDate;
	}

	public void setCreateFromDate(Date createFromDate) {
		this.createFromDate = createFromDate;
	}

	public Date getCreateToDate() {
		return createToDate;
	}

	public void setCreateToDate(Date createToDate) {
		this.createToDate = createToDate;
	}

	public Boolean getIsCheckStatus() {
		return isCheckStatus;
	}

	public void setIsCheckStatus(Boolean isCheckStatus) {
		this.isCheckStatus = isCheckStatus;
	}

	/** @return the staffRootId */
	public Long getStaffRootId() {
		return staffRootId;
	}

	/** @param staffRootId the staffRootId to set */
	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}

	/** @return the shopRootId */
	public Long getShopRootId() {
		return shopRootId;
	}

	/** @param shopRootId the shopRootId to set */
	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}

	/** @return the roleId */
	public Long getRoleId() {
		return roleId;
	}

	/** @param roleId the roleId to set */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public List<Integer> getLstRecordStatus() {
		return lstRecordStatus;
	}

	public void setLstRecordStatus(List<Integer> lstRecordStatus) {
		this.lstRecordStatus = lstRecordStatus;
	}

	public String getOrderStatistic() {
		return orderStatistic;
	}

	public void setOrderStatistic(String orderStatistic) {
		this.orderStatistic = orderStatistic;
	}
	
}

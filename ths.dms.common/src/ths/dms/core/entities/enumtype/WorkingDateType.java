/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liemtpt
 *
 */
public enum WorkingDateType {
	BAN_THAN_DON_VI(1),
	CHI_DINH_DON_VI(2),
	DE_QUY(3);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, WorkingDateType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    WorkingDateType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static WorkingDateType parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, WorkingDateType>(
                    WorkingDateType.values().length);
            for (WorkingDateType e : WorkingDateType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

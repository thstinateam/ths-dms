/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum GenderType.
 * 
 * @author huytran
 */
public enum GenderType {
    
    /** The FEMALE. */
    FEMALE (0),
    
    /** The MALE. */
    MALE (1),
    
    /** The NONE. */
    BOTH (2),
    
    /** The BOTH. */
    NONE (3),
    
    /** The GAY. */
    GAY (4),
    
    /** The LESBIAN. */
    LESBIAN (5);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, GenderType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    GenderType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static GenderType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, GenderType>(
                    GenderType.values().length);
            for (GenderType e : GenderType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

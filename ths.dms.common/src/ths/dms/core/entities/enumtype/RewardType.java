/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum RewardType {
    
    CHUYEN_KHOAN (0),

    KHOA (1),
    
    MO_KHOA_CHUA_TRA (2),
    
    DA_TRA_MOT_PHAN (3),
    
    DA_TRA_TOAN_BO (4);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, RewardType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    RewardType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static RewardType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, RewardType>(
                    RewardType.values().length);
            for (RewardType e : RewardType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

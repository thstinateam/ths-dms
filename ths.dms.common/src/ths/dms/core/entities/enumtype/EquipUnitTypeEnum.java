/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author hunglm16
 * @description Quy dinh tinh trang tren toan bo he thong SO
 * Kenh thiet bi
 */
public enum EquipUnitTypeEnum {

	VNM(1),

	MIEN(2),

	VUNG(3),

	NPP(4);

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, EquipUnitTypeEnum> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	EquipUnitTypeEnum(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author hunglm16
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static EquipUnitTypeEnum parseValue(Integer value) {
		if (value == null) {
			value = -1;
		}
		if (values == null) {
			values = new HashMap<Integer, EquipUnitTypeEnum>(EquipUnitTypeEnum.values().length);
			for (EquipUnitTypeEnum e : EquipUnitTypeEnum.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Dinh nghia loai phep tinh toan hoc
 * 
 * @author hunglm16
 * @since 29/10/2015
 */
public enum ArithmeticEnum {
	PLUS(1), //Cong
	MINUS(2), //Tru
	multiplication(3), //Nhan
	division(4) //Chia
	;

	private Integer value;

	/** The values. */
	private static Map<Integer, ArithmeticEnum> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value the value
	 */
	ArithmeticEnum(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author hunglm16
	 * @param value the value
	 * @return the gender type
	 * @since 29/10/2015
	 */
	public static ArithmeticEnum parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, ArithmeticEnum>(ArithmeticEnum.values().length);
			for (ArithmeticEnum e : ArithmeticEnum.values()) {
				values.put(e.getValue(), e);
			}
		}
		return values.get(value);
	}
}

/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * trang thai khi kiem ke
 * @author nhutnn
 */
public enum EquipStatisticStatus {
	NOT_INVENTORY (0),

	INVENTORIED (1),
	
    STILL (2),
    
    LOST (3),
    ;
    
    /** The value. */
    private Integer value;
    /** The values. */
    private static Map<Integer, EquipStatisticStatus> values = null;
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    EquipStatisticStatus(Integer value) {
        this.value = value;
    }
    /**
     * Parses the value.
     *
     * @author nhutnn
     * @param value the value
     * @return the gender type
     */
    public static EquipStatisticStatus parseValue(Integer value) {
        if(value == null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, EquipStatisticStatus>(EquipStatisticStatus.values().length);
            for (EquipStatisticStatus e : EquipStatisticStatus.values()) {
            	values.put(e.getValue(), e);            	
            }
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum PoVNMStatus {
	// Chua nhap/ tra
	NOT_IMPORT(0),
	// Dang nhap/ tra
	IMPORTING(1),
	// Da nhap/ tra xong
	IMPORTED(2),
	// Co the treo
	PENDING(3),
	// Da treo
	SUSPEND(4);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, PoVNMStatus> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	PoVNMStatus(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static PoVNMStatus parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, PoVNMStatus>(
					PoVNMStatus.values().length);
			for (PoVNMStatus e : PoVNMStatus.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

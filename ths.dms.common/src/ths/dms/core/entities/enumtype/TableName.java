
/**
 * 
 */
package ths.dms.core.entities.enumtype;
/**
 * @author tamvnm
 * @date 07/05/2015
 * cac equip_param cho thiet bi
 */
import java.util.HashMap;
import java.util.Map;


public enum TableName {
    
	EQUIP_DELIVERY_TABLE ("EQUIP_DELIVERY_RECORD"),
	EQUIP_EVICTION_FORM_TABLE ("EQUIP_EVICTION_FORM"),
	
	/** vuongmq; 20/05/2015; */
	EQUIP_LOST_RECORD ("EQUIP_LOST_RECORD"), // bao mat 
	EQUIP_LOST_MOBILE_REC ("EQUIP_LOST_MOBILE_REC"), // bao mat mobile
	EQUIP_REPAIR_FORM ("EQUIP_REPAIR_FORM"), // sua chua
	
	/** vuongmq; 08/07/2015; thanh ly tai san*/
	EQUIP_LIQUIDATION_FORM("EQUIP_LIQUIDATION_FORM"), // thanh ly tai san
	
    ;
    
    
    /** The value. */
    private String value;
    
    /** The values. */
    private static Map<String, TableName> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    TableName(String value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static TableName parseValue(String value) {
        if (values == null) {
            values = new HashMap<String, TableName>(
                    TableName.values().length);
            for (TableName e : TableName.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

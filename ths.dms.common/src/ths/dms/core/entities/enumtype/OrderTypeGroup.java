/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author hunglm16
 * @since 03/09/2015
 */
public enum OrderTypeGroup {

	B1(1),

	B2(2),

	B3(3),

	B4(4),

	ALL(-2);

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, OrderTypeGroup> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value the value
	 */
	OrderTypeGroup(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author hunglm16
	 * @param value the value
	 * @return the gender type
	 */
	public static OrderTypeGroup parseValue(Integer value) {
		if (value == null) {
			value = -1;
		}
		if (values == null) {
			values = new HashMap<Integer, OrderTypeGroup>(OrderTypeGroup.values().length);
			for (OrderTypeGroup e : OrderTypeGroup.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

}

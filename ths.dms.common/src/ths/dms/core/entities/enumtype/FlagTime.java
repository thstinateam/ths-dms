/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum OrderType.
 * 
 * @author huytran
 */
public enum FlagTime {

	BACKTIME(0),

	SYSTEM(1),

	NEXTTIME(2),

	SYSANDNEXTTIME(3);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, FlagTime> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	FlagTime(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static FlagTime parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, FlagTime>(FlagTime.values().length);
			for (FlagTime e : FlagTime.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author hunglm16
 * @since 03/09/2015
 */
public enum StatusShopLockLog {

	RUNNING(1),

	PENDING(2),

	DONE(3),

	ERROR(4),

	ALL(-2);

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, StatusShopLockLog> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	StatusShopLockLog(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author hunglm16
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static StatusShopLockLog parseValue(Integer value) {
		if (value == null) {
			value = -1;
		}
		if (values == null) {
			values = new HashMap<Integer, StatusShopLockLog>(StatusShopLockLog.values().length);
			for (StatusShopLockLog e : StatusShopLockLog.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai tinh trang.
	 * 
	 * @author hunglm16
	 * @param value
	 * @return true Ton tai
	 * @return fale Khong ton tai
	 */
	public static boolean isValidValue(Integer value) {
		if (value == null) {
			return false;
		}
		if (!StatusShopLockLog.RUNNING.getValue().equals(value) && !StatusShopLockLog.PENDING.getValue().equals(value) && !StatusShopLockLog.DONE.getValue().equals(value) && !StatusShopLockLog.ERROR.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

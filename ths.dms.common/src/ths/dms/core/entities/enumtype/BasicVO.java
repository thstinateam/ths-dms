package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Lop VO co ban can co de lam tham so
 * 
 * @author hunglm16
 * @return BasicVOFilter
 * @description Chi bo sung truong o muc dung chung cho tat ca cac Table, khong tao them truong khac biet, không tạo thêm Oject ngoài <T>
 */
public class BasicVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long longG;
	private String code;
	private String name;
	private String userName;
	private String shopCode;
	private String description;
	private String textG;
	private Integer type;
	private Integer status;
	private Integer seq;
	private Integer intG;
	private Integer count;
	private BigDecimal amount;
	private Date fromDate;
	private Date toDate;
	private Date dateG;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getLongG() {
		return longG;
	}
	public void setLongG(Long longG) {
		this.longG = longG;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTextG() {
		return textG;
	}
	public void setTextG(String textG) {
		this.textG = textG;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public Integer getIntG() {
		return intG;
	}
	public void setIntG(Integer intG) {
		this.intG = intG;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getDateG() {
		return dateG;
	}
	public void setDateG(Date dateG) {
		this.dateG = dateG;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
}

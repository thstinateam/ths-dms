package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum EquipTradeStatus {
	TRADING(1),
	NOT_TRADE(0);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, EquipTradeStatus> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    EquipTradeStatus(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static EquipTradeStatus parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, EquipTradeStatus>(
            		EquipTradeStatus.values().length);
            for (EquipTradeStatus e : EquipTradeStatus.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum StockObjectType.
 * 
 * @author huytran
 */
public enum StockTransType {
    
    EXPORT_TO_STAFF (0),

    IMPORT_FROM_STAFF (1),
    
    EXPORT_MODIFIED (2),
    
    IMPORT_MODIFIED (3);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, StockTransType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    StockTransType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static StockTransType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, StockTransType>(
                    StockTransType.values().length);
            for (StockTransType e : StockTransType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

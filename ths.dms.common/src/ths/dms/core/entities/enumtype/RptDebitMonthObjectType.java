/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum SaleObjectType.
 * 
 * @author huytran
 */
public enum RptDebitMonthObjectType {
    
    /** shop */
    SHOP(1),
    
    /** staff */
    STAFF(2),
    
    /** NPP */
    CUSTOMER(3);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, RptDebitMonthObjectType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    RptDebitMonthObjectType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static RptDebitMonthObjectType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, RptDebitMonthObjectType>(
                    RptDebitMonthObjectType.values().length);
            for (RptDebitMonthObjectType e : RptDebitMonthObjectType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

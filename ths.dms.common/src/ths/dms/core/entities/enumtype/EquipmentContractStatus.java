/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum EquipmentContractStatus {
	
	ALLOCATION (1),

    RECOVERY (2),
    
    LOSS (3);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, EquipmentContractStatus> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    EquipmentContractStatus(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static EquipmentContractStatus parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, EquipmentContractStatus>(
                    EquipmentContractStatus.values().length);
            for (EquipmentContractStatus e : EquipmentContractStatus.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

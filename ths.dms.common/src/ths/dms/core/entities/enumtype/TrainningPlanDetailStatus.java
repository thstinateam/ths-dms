/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum TrainningPlanDetailStatus {
    
	DELETED(-1),
	
	NEW(0),
	
    TRAINED (1),

    CANCLED (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, TrainningPlanDetailStatus> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    TrainningPlanDetailStatus(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static TrainningPlanDetailStatus parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, TrainningPlanDetailStatus>(
                    TrainningPlanDetailStatus.values().length);
            for (TrainningPlanDetailStatus e : TrainningPlanDetailStatus.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

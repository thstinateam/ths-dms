package ths.dms.core.entities.enumtype;

public class CommonContanst {

	/*
	 * process_history
	 */
	public final static Integer SALE_ORDER_CONFIRM_STEP = 1;
	public final static Integer SALE_ORDER_APPROVED_STEP = 2;
	public final static Integer SALE_ORDER_STOCKUPDATING_STEP = 3;
	public final static Integer PO_IMPORT_STEP = 1;
	public final static Integer PO_RETURN_STEP = 2;
	public final static Integer STOCK_TRANS_STOCKUPDATING_STEP = 1;
}
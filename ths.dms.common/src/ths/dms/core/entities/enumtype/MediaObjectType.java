/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum MediaObjectType {
	//0: hinh anh dong cua, 1: hinh anh trung bay, 2: hinh anh diem ban,  3: hinh anh san pham  (chính là luu tru hình, video cho product)
	IMAGE_STORE (0),
	
    IMAGE_DISPLAY (1),
    
    IMAGE_SALE_POINT (2),
    
    IMAGE_PRODUCT (3),
    
    IMAGE_FEEDBACK (5), // image theo doi van de
    
    IMAGE_OFFICE_DOCUMENT(6),
    
    VIDEO_UPLOAD(7);
    
	
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, MediaObjectType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    MediaObjectType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static MediaObjectType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, MediaObjectType>(
                    MediaObjectType.values().length);
            for (MediaObjectType e : MediaObjectType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.Invoice;

public class InvoiceFilter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public KPaging<Invoice> kPaging;

	public Float taxValue;
	public String orderNumber;
	public Date fromDate;
	public Date toDate;

	public String staffCode;
	public String shortCode;
	public InvoiceCustPayment custPayment;
	
	public String customerAddr;
	public InvoiceStatus status;
	public Boolean isSOPrint;
	public Boolean isInvoicePrint;
	
	public Integer isValueOrder;
	
	public Integer getIsValueOrder() {
		return isValueOrder;
	}
	public void setIsValueOrder(Integer isValueOrder) {
		this.isValueOrder = isValueOrder;
	}
	private Long shopId;
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public KPaging<Invoice> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<Invoice> kPaging) {
		this.kPaging = kPaging;
	}
	public Float getTaxValue() {
		return taxValue;
	}
	public void setTaxValue(Float taxValue) {
		this.taxValue = taxValue;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public InvoiceCustPayment getCustPayment() {
		return custPayment;
	}
	public void setCustPayment(InvoiceCustPayment custPayment) {
		this.custPayment = custPayment;
	}
	public String getCustomerAddr() {
		return customerAddr;
	}
	public void setCustomerAddr(String customerAddr) {
		this.customerAddr = customerAddr;
	}
	public InvoiceStatus getStatus() {
		return status;
	}
	public void setStatus(InvoiceStatus status) {
		this.status = status;
	}
	public Boolean getIsSOPrint() {
		return isSOPrint;
	}
	public void setIsSOPrint(Boolean isSOPrint) {
		this.isSOPrint = isSOPrint;
	}
	public Boolean getIsInvoicePrint() {
		return isInvoicePrint;
	}
	public void setIsInvoicePrint(Boolean isInvoicePrint) {
		this.isInvoicePrint = isInvoicePrint;
	}
}

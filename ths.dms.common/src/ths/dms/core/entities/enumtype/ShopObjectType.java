/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum OrderType.
 * 
 * @author huytran
 */
public enum ShopObjectType {

	VNM(14),
	
	GT(0),

	MIEN(1),
	
	VUNG(2),

	NPP(3),
	
	CH(4),	//loctt - Nov4, 2013
	
	MIEN_SALEMT(6),//loctt - Nov15, 2013
	
	SALEMT(7),
	
	VUNG_SALEMT(5),	//loctt - Nov15, 2013
	
	KA(8),
	
	MIEN_KA(9),

	VUNG_KA(10),
	
	NPP_KA(11),
	
	ST(12),

	MIEN_ST(13);


	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, ShopObjectType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	ShopObjectType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static ShopObjectType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, ShopObjectType>(ShopObjectType.values().length);
			for (ShopObjectType e : ShopObjectType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

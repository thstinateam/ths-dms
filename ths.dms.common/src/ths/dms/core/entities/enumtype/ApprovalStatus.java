/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum ApprovalStatus {
    
    NOT_YET (0),

    APPROVED (1),
    
    DESTROY (2),
    
    REJECT(3),
    
    NCC_APPROVED(4);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ApprovalStatus> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ApprovalStatus(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ApprovalStatus parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ApprovalStatus>(
                    ApprovalStatus.values().length);
            for (ApprovalStatus e : ApprovalStatus.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;



public class IncentiveProgramVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7457477733939615641L;

	private Long id;
	
	private String programCode;
	
	private String programName;
	
	private Integer incentiveType;
	
	private Integer type;
	
	private Date fromDate;
	
	private Date toDate;
	
	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public Integer getIncentiveType() {
		return incentiveType;
	}

	public void setIncentiveType(Integer incentiveType) {
		this.incentiveType = incentiveType;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	

}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum OrderType.
 * 
 * @author vuongmq
 * @since 27 August, 2014
 * @description Loai don hang ORDER_TYPE
 */
public enum OrderType {

	
	IN("IN"),  /** Đơn bán presale */

	CM("CM"),	/** Đơn trả presale */
	
	SO("SO"),	/** Đơn bán vansale (NVBH -> KH)*/
	
	CO("CO"),  /** Đơn trả vansale (NVBH -> KH)*/
	
	GO("GO"),  /** Đơn trả hàng vansale(GO) KTNPP ->NVBH vansale */
	
	DP("DP"),  /** Đơn bán hàng vansale(DP) KTNPP ->NVBH vansale */
	
	DC("DC"),  /** Điều chuyển */
	
	DCT("DCT"), /** Điều chỉnh tăng */
	
	DCG("DCG"), /** Điều chỉnh giảm */
	
	AI("AI"), /** Điều chỉnh tăng - update cho Dieu chinh doanh thu @author hunglm16 */
	
	AD("AD"), /** Điều chỉnh giảm - update cho Dieu chinh doanh thu @author hunglm16 */
	
	TT("TT"); // chua xai

	/** The value. */
	private String value;

	/** The values. */
	private static Map<String, OrderType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	OrderType(String value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static OrderType parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, OrderType>(OrderType.values().length);
			for (OrderType e : OrderType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum PayReceivedType {
	THANH_TOAN(0),
	CHIET_KHAU(1); // giam no

    private Integer value;

    private static Map<Integer, PayReceivedType> values = null;

    public Integer getValue() {
        return value;
    }

    PayReceivedType(Integer value) {
        this.value = value;
    }

    public static PayReceivedType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, PayReceivedType>(
            		PayReceivedType.values().length);
            for (PayReceivedType e : PayReceivedType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}
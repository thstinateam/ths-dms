/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author hunglm16
 * @description Quy dinh tinh trang tren toan bo he thong SO
 */
public enum EquipPeriodType {
    
	CREATED(1),
	
    OPENED (2),
    
    CLOSED (3);
    
    /** The value. */
    private Integer value;
    /** The values. */
    private static Map<Integer, EquipPeriodType> values = null;
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    EquipPeriodType(Integer value) {
        this.value = value;
    }
    /**
     * Parses the value.
     * 
     * @author hunglm16
     * @param value the value
     * @return the gender type
     */
    public static EquipPeriodType parseValue(Integer value) {
        if(value==null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, EquipPeriodType>(
                    EquipPeriodType.values().length);
            for (EquipPeriodType e : EquipPeriodType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
   
}

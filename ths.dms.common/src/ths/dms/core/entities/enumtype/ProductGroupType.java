package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum ProductGroupType {
	MUA(1),

	KM(2);

	private Integer value;

	private static Map<Integer, ProductGroupType> values = null;
	
	public Integer getValue() {
		return value;
	}

	ProductGroupType(Integer value) {
		this.value = value;
	}

	public static ProductGroupType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, ProductGroupType>(ProductGroupType.values().length);
			for (ProductGroupType e : ProductGroupType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

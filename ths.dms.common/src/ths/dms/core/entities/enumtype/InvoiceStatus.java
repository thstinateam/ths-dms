/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum InvoiceStatus {
    
    USING (0),

    MODIFIED (1),
    
    CANCELED (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, InvoiceStatus> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    InvoiceStatus(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static InvoiceStatus parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, InvoiceStatus>(
                    InvoiceStatus.values().length);
            for (InvoiceStatus e : InvoiceStatus.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum ValueType {
    QUANTITY (1),
    
    AMOUNT (2);
    
    /** The value. */
    private Integer value;
    /** The values. */
    private static Map<Integer, ValueType> values = null;
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    ValueType(Integer value) {
        this.value = value;
    }
    /**
     * Parses the value.
     * 
     * @author hunglm16
     * @param value the value
     * @return the gender type
     */
    public static ValueType parseValue(Integer value) {
        if(value==null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, ValueType>(
                    ValueType.values().length);
            for (ValueType e : ValueType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;
/**
 * The Enum DeliveryType.
 * 
 * @author nhutnn
 * @description Quy dinh noi dung thiet bi trong Module Quan ly thiet bi
 */
public enum EquipDeliveryContentType {

	NEW(0),

	TRANSFER_FROM_OTHER_STOCK(1),
	
	OTHER(2);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, EquipDeliveryContentType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 */
	EquipDeliveryContentType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 * @return the gender type
	 */
	public static EquipDeliveryContentType parseValue(Integer value) {
		if (value != null && value == -1) {
			value = -2;
		}
		if (values == null) {
			values = new HashMap<Integer, EquipDeliveryContentType>(
					EquipDeliveryContentType.values().length);
			for (EquipDeliveryContentType e : EquipDeliveryContentType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

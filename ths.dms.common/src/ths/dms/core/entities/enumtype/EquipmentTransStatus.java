/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum EquipmentTransStatus.
 * 
 * @author thongnm
 */
public enum EquipmentTransStatus {
    
	ALLOCATION (1),

    RECOVERY (2),
    
    LOSS (3),
    
    REPAIR(4);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, EquipmentTransStatus> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    EquipmentTransStatus(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static EquipmentTransStatus parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, EquipmentTransStatus>(
                    EquipmentTransStatus.values().length);
            for (EquipmentTransStatus e : EquipmentTransStatus.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;
import java.io.Serializable;
import java.util.List;

/**
 * @author hunglm16
 * @since May 18,2015
 * @description Lien quan den Data Modeler 
 * 01. Doanh muc nhom thiet bi
 * 02. Nhom, doanh so thiet bi
 * 03. Thiet bi
 */
public class EquipGFilter<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	private KPaging<T> kPaging;
	
	private Long equipGroupId;
	private Long equipCategoryId;
	
	private String equipGroupCode;
	private String equipGroupName;
	
	private Integer equipGroupStatus;
	private List<Integer> lstStatus;
	
	/**
	 * Khai bao Getter/Setter
	 * */
	
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getEquipGroupId() {
		return equipGroupId;
	}
	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}
	public Long getEquipCategoryId() {
		return equipCategoryId;
	}
	public void setEquipCategoryId(Long equipCategoryId) {
		this.equipCategoryId = equipCategoryId;
	}
	public String getEquipGroupCode() {
		return equipGroupCode;
	}
	public void setEquipGroupCode(String equipGroupCode) {
		this.equipGroupCode = equipGroupCode;
	}
	public String getEquipGroupName() {
		return equipGroupName;
	}
	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}
	public Integer getEquipGroupStatus() {
		return equipGroupStatus;
	}
	public void setEquipGroupStatus(Integer equipGroupStatus) {
		this.equipGroupStatus = equipGroupStatus;
	}
	public List<Integer> getLstStatus() {
		return lstStatus;
	}
	public void setLstStatus(List<Integer> lstStatus) {
		this.lstStatus = lstStatus;
	}
	
}

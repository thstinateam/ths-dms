package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum PromotionType {
	
	ZV01("ZV01"),
	ZV02("ZV02"),
	ZV03("ZV03"),
	ZV04("ZV04"),
	ZV05("ZV05"),
	ZV06("ZV06"),
	ZV07("ZV07"),
	ZV08("ZV08"),
	ZV09("ZV09"),
	ZV10("ZV10"),
	ZV11("ZV11"),
	ZV12("ZV12"),
	ZV13("ZV13"),
	ZV14("ZV14"),
	ZV15("ZV15"),
	ZV16("ZV16"),
	ZV17("ZV17"),
	ZV18("ZV18"),
	ZV19("ZV19"),
	ZV20("ZV20"),
	ZV21("ZV21"),
	ZV22("ZV22"),
	ZV23("ZV23"),
	ZV24("ZV24"),
	KS("KS")
	;
	
	private String value;
	
	private static Map<String, PromotionType> values = null;
	
	
	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	PromotionType(String value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static PromotionType parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, PromotionType>(PromotionType.values().length);
			for (PromotionType e : PromotionType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

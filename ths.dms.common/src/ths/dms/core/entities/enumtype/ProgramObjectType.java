/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum ProgramObjectType {
    
    FOCUS_PROGRAM (0),

    DISPLAY_PROGRAM (1),
    
    PROMOTION_PROGRAM (2),
    
    INCENTIVE_PROGRAM (3),
    
    
    GROUP_PO (4),
    
    EQUIPMENT (5),
    
    GROUP_PO_NPP (6);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ProgramObjectType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ProgramObjectType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ProgramObjectType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ProgramObjectType>(
                    ProgramObjectType.values().length);
            for (ProgramObjectType e : ProgramObjectType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
    
}

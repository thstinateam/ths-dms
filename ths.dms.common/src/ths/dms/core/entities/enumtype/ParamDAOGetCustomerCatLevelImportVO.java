package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.vo.CustomerCatLevelImportVO;

public class ParamDAOGetCustomerCatLevelImportVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private KPaging<CustomerCatLevelImportVO> kPaging;
	private String shortCode;
	private String customerName;
	private List<String> lstAreaCode;
	private String phone;
	private String mobiPhone;
	private Long customerTypeId;
	private ActiveType status;
	private String region;
	private String loyalty;
	private Long groupTransferId;
	private Long cashierId;
	private String address;
	private String street;
	private Long shopId;
	private String directShopCode;
	private String shopCodeLike;
	private String display;
	private String location;
	private Long notInGroupTransferId;

	public KPaging<CustomerCatLevelImportVO> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<CustomerCatLevelImportVO> kPaging) {
		this.kPaging = kPaging;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public List<String> getLstAreaCode() {
		return lstAreaCode;
	}

	public void setLstAreaCode(List<String> lstAreaCode) {
		this.lstAreaCode = lstAreaCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobiPhone() {
		return mobiPhone;
	}

	public void setMobiPhone(String mobiPhone) {
		this.mobiPhone = mobiPhone;
	}

	public Long getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(Long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getLoyalty() {
		return loyalty;
	}

	public void setLoyalty(String loyalty) {
		this.loyalty = loyalty;
	}

	public Long getGroupTransferId() {
		return groupTransferId;
	}

	public void setGroupTransferId(Long groupTransferId) {
		this.groupTransferId = groupTransferId;
	}

	public Long getCashierId() {
		return cashierId;
	}

	public void setCashierId(Long cashierId) {
		this.cashierId = cashierId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getDirectShopCode() {
		return directShopCode;
	}

	public void setDirectShopCode(String directShopCode) {
		this.directShopCode = directShopCode;
	}

	public String getShopCodeLike() {
		return shopCodeLike;
	}

	public void setShopCodeLike(String shopCodeLike) {
		this.shopCodeLike = shopCodeLike;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Long getNotInGroupTransferId() {
		return notInGroupTransferId;
	}

	public void setNotInGroupTransferId(Long notInGroupTransferId) {
		this.notInGroupTransferId = notInGroupTransferId;
	}
}

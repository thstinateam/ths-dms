/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;

public class DisplayCustomerMapFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5719535609658660659L;

	private String shortCode;
	
	private String customerName;
	
	private Long displayStaffMapId;
	
	private Long displayProgramLevelId;
	
	private ActiveType status;
	
	private Date fromDate;
	
	private Date toDate;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Long getDisplayStaffMapId() {
		return displayStaffMapId;
	}

	public void setDisplayStaffMapId(Long displayStaffMapId) {
		this.displayStaffMapId = displayStaffMapId;
	}

	public Long getDisplayProgramLevelId() {
		return displayProgramLevelId;
	}

	public void setDisplayProgramLevelId(Long displayProgramLevelId) {
		this.displayProgramLevelId = displayProgramLevelId;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

}

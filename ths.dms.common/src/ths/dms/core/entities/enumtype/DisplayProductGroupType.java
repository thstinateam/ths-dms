/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum DisplayProductGroupType {

    DS (1), /** Doanh so */
    
    SL(2), /** San luong */
    
    SKU (3), /** SKU */
    
    CB(4);/** Trung bay */
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, DisplayProductGroupType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    DisplayProductGroupType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static DisplayProductGroupType parseValue(Integer value) {
    	if (value != null && value == -1){
    		value = -2;
    	}    		
        if (values == null) {
            values = new HashMap<Integer, DisplayProductGroupType>(
                    DisplayProductGroupType.values().length);
            for (DisplayProductGroupType e : DisplayProductGroupType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

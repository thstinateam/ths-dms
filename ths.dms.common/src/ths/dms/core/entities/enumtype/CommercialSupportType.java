/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum OrderType.
 * 
 * @author huytran
 */
public enum CommercialSupportType {

	PROMOTION_PROGRAM(1),

	DISPLAY_PROGRAM(2);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, CommercialSupportType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	CommercialSupportType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static CommercialSupportType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, CommercialSupportType>(CommercialSupportType.values().length);
			for (CommercialSupportType e : CommercialSupportType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

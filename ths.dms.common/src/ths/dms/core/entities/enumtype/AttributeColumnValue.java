/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum AttributeColumnValue {
    
    SELF_INPUT (1),

    EXIST_BEFORE (2);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, AttributeColumnValue> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    AttributeColumnValue(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static AttributeColumnValue parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, AttributeColumnValue>(
                    AttributeColumnValue.values().length);
            for (AttributeColumnValue e : AttributeColumnValue.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

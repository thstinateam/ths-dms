package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.vo.ToolVO;

public class ToolFilter implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private KPaging<ToolVO> kPaging;
	private Long parentShopId;
	private Integer typeSup;
	private String toolCode;
	private String customerCode;
	private String customerName;
	private Boolean isWarning;
	private Integer distance;
	private Long chipId;
	private String chipCode;
	private Float minTemp;
	private Float maxTemp;
	private Date fromDate;
	private Date toDate;
	
	
	public KPaging<ToolVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<ToolVO> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getParentShopId() {
		return parentShopId;
	}
	public void setParentShopId(Long parentShopId) {
		this.parentShopId = parentShopId;
	}
	public Integer getTypeSup() {
		return typeSup;
	}
	public void setTypeSup(Integer typeSup) {
		this.typeSup = typeSup;
	}
	public String getToolCode() {
		return toolCode;
	}
	public void setToolCode(String toolCode) {
		this.toolCode = toolCode;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public Boolean getIsWarning() {
		return isWarning;
	}
	public void setIsWarning(Boolean isWarning) {
		this.isWarning = isWarning;
	}
	public Integer getDistance() {
		return distance;
	}
	public void setDistance(Integer distance) {
		this.distance = distance;
	}
	public String getChipCode() {
		return chipCode;
	}
	public void setChipCode(String chipCode) {
		this.chipCode = chipCode;
	}
	public Float getMinTemp() {
		return minTemp;
	}
	public void setMinTemp(Float minTemp) {
		this.minTemp = minTemp;
	}
	public Float getMaxTemp() {
		return maxTemp;
	}
	public void setMaxTemp(Float maxTemp) {
		this.maxTemp = maxTemp;
	}
	public Long getChipId() {
		return chipId;
	}
	public void setChipId(Long chipId) {
		this.chipId = chipId;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	
}

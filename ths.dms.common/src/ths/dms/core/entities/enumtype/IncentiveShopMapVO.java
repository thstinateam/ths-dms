/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;



public class IncentiveShopMapVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7457477733939615641L;

	private Long id;
	
	private String shopCode;
	
	private String shopName;
	
	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}

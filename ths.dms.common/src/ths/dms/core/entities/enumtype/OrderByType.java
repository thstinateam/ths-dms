/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum OrderByType {
    
    ASC ("asc"),
    
    DESC("desc");
    
    /** The value. */
    private String value;
    
    /** The values. */
    private static Map<String, OrderByType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    OrderByType(String value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static OrderByType parseValue(String value) {
        if (values == null) {
            values = new HashMap<String, OrderByType>(
                    OrderByType.values().length);
            for (OrderByType e : OrderByType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

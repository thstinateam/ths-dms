/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author hunglm16
 * @since September 17,2014
 */
public enum SaleOrderIsAttr {

    AIORAD (0);//Phieu dieu dieu chinh
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, SaleOrderIsAttr> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    SaleOrderIsAttr(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static SaleOrderIsAttr parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, SaleOrderIsAttr>(
                    SaleOrderIsAttr.values().length);
            for (SaleOrderIsAttr e : SaleOrderIsAttr.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Aso plan type (loai phan bo Aso)
 * @author vuongmq
 * @since 04/09/2015 
 */
public enum AsoPlanType {

	SKU(1),

	SUB_CAT(2);

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, AsoPlanType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value the value
	 */
	AsoPlanType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author vuongmq
	 * @param value the value
	 * @return the aso plan type
	 */
	public static AsoPlanType parseValue(Integer value) {
		if (value == null) {
			value = 0;
		}
		if (values == null) {
			values = new HashMap<Integer, AsoPlanType>(AsoPlanType.values().length);
			for (AsoPlanType e : AsoPlanType.values()) {
				values.put(e.getValue(), e);
			}
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai theo quy dinh.
	 * 
	 * @author vuongmq
	 * @param value
	 * @return flag
	 */
	public static boolean checkAsoPlanType(Integer value) {
		if (value == null) {
			return false;
		}
		if (!AsoPlanType.SKU.getValue().equals(value) && !AsoPlanType.SUB_CAT.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

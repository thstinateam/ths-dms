package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum GeneralStatus {
	NOT_PASS (5),
    
    PASS (2);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, GeneralStatus> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    GeneralStatus(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static GeneralStatus parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, GeneralStatus>(
            		GeneralStatus.values().length);
            for (GeneralStatus e : GeneralStatus.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.SaleOrderVOEx2;

public class SoFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public KPaging<SaleOrder> kPaging;
	public KPaging<SaleOrderVO> kPagingVO;
	public KPaging<SaleOrderDetail> kPagingDetail;
	public KPaging<SaleOrderVOEx2> kPagingVOEx2;
	public List<Long> lstSaleOrderId;
	public List<Long> lstPOCustomerId;
	public List<Long> lstCustomerId;
	public List<Long> lstPayReceivedId;
	public List<Long> lstProductId;
	public List<Long> lstId;
	public Long saleOrderId;
	public String shortCode;
	public String customerCode;
	public String customeName;
	public String orderNumber;
	public String refOrderNumber;
	public SaleOrderStatus approval;
	public List<OrderType> listOrderType;
	private List<String> lstOrderTypeStr;
	public OrderType orderType;
	public Date fromDate;
	public Date orderDate;
	public Date toDate;
	public Long staffId;
	public String staffCode;
	public Long deliveryId;
	public String deliveryCode;
	public String cashierCode;
	public String priorityCode;
	public SaleOrderType saleOrderType;
	public Boolean checkDefault;
	public Boolean isFromSaleOrderNull;
	public Long shopId;
	public Long shopTypeId;
	public Long customerId;
	public Long userId;
	public Long roleId;
	public SaleOrderSource orderSource;
	public Boolean checkDueDate;
	public Date deliveryDate;
	public Long carId;
	public Boolean isPrint;
	public Boolean isConfirm;
	public Integer approveVan;
	public Date lockDay;
	public SaleOrderStep approvedStep;

	public Integer printBatch;
	public Integer valueOrder;

	private Boolean isCheckOrderDateDesc;
	private Integer isFreeItem;
	private String description;
	private Integer objectType;
	private List<Long> lstPassOrderId;
	private Integer isValueOrder;
	private BigDecimal numberValueOrder;
	private String sort;
	private String order;
	private String strListShopId;
	private String strListUserId;
	private Boolean approvedAndStep1;
	
	private Integer printVATStatus;//Trang thai IN VAT
	
	private boolean exactStaffSearching = false;
	
	private Boolean isDividualWarehouse;
	
	public Integer getIsValueOrder() {
		return isValueOrder;
	}

	public void setIsValueOrder(Integer isValueOrder) {
		this.isValueOrder = isValueOrder;
	}

	public BigDecimal getNumberValueOrder() {
		return numberValueOrder;
	}

	public void setNumberValueOrder(BigDecimal numberValueOrder) {
		this.numberValueOrder = numberValueOrder;
	}

	public Boolean getIsConfirm() {
		return isConfirm;
	}

	public void setIsConfirm(Boolean isConfirm) {
		this.isConfirm = isConfirm;
	}

	public List<Long> getLstProductId() {
		return lstProductId;
	}

	public void setLstProductId(List<Long> lstProductId) {
		this.lstProductId = lstProductId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getRefOrderNumber() {
		return refOrderNumber;
	}

	public void setRefOrderNumber(String refOrderNumber) {
		this.refOrderNumber = refOrderNumber;
	}

	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}

	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getIsFreeItem() {
		return isFreeItem;
	}

	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}

	public Integer getValueOrder() {
		return valueOrder;
	}

	public void setValueOrder(Integer valueOrder) {
		this.valueOrder = valueOrder;
	}

	public Date printTime;

	public Integer getPrintBatch() {
		return printBatch;
	}

	public void setPrintBatch(Integer printBatch) {
		this.printBatch = printBatch;
	}

	public Date getPrintTime() {
		return printTime;
	}

	public void setPrintTime(Date printTime) {
		this.printTime = printTime;
	}

	public Boolean getIsPrint() {
		return isPrint;
	}

	public void setIsPrint(Boolean isPrint) {
		this.isPrint = isPrint;
	}

	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId) {
		this.carId = carId;
	}

	public KPaging<SaleOrder> getkPaging() {
		return kPaging;
	}

	public void setkPaging(KPaging<SaleOrder> kPaging) {
		this.kPaging = kPaging;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomeName() {
		return customeName;
	}

	public void setCustomeName(String customeName) {
		this.customeName = customeName;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public SaleOrderStatus getApproval() {
		return approval;
	}

	public void setApproval(SaleOrderStatus approval) {
		this.approval = approval;
	}

	public List<OrderType> getListOrderType() {
		return listOrderType;
	}

	public void setListOrderType(List<OrderType> listOrderType) {
		this.listOrderType = listOrderType;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getCashierCode() {
		return cashierCode;
	}

	public void setCashierCode(String cashierCode) {
		this.cashierCode = cashierCode;
	}

	public SaleOrderType getSaleOrderType() {
		return saleOrderType;
	}

	public void setSaleOrderType(SaleOrderType saleOrderType) {
		this.saleOrderType = saleOrderType;
	}

	public Boolean getCheckDefault() {
		return checkDefault;
	}

	public void setCheckDefault(Boolean checkDefault) {
		this.checkDefault = checkDefault;
	}

	public Boolean getIsFromSaleOrderNull() {
		return isFromSaleOrderNull;
	}

	public void setIsFromSaleOrderNull(Boolean isFromSaleOrderNull) {
		this.isFromSaleOrderNull = isFromSaleOrderNull;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public SaleOrderSource getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(SaleOrderSource orderSource) {
		this.orderSource = orderSource;
	}

	public Boolean getCheckDueDate() {
		return checkDueDate;
	}

	public void setCheckDueDate(Boolean checkDueDate) {
		this.checkDueDate = checkDueDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getPriorityCode() {
		return priorityCode;
	}

	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}

	public Integer getApproveVan() {
		return approveVan;
	}

	public void setApproveVan(Integer approveVan) {
		this.approveVan = approveVan;
	}

	public Date getLockDay() {
		return lockDay;
	}

	public void setLockDay(Date lockDay) {
		this.lockDay = lockDay;
	}

	public SaleOrderStep getApprovedStep() {
		return approvedStep;
	}

	public void setApprovedStep(SaleOrderStep approvedStep) {
		this.approvedStep = approvedStep;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public List<Long> getLstPOCustomerId() {
		return lstPOCustomerId;
	}

	public void setLstPOCustomerId(List<Long> lstPOCustomerId) {
		this.lstPOCustomerId = lstPOCustomerId;
	}

	public List<Long> getLstSaleOrderId() {
		return lstSaleOrderId;
	}

	public void setLstSaleOrderId(List<Long> lstSaleOrderId) {
		this.lstSaleOrderId = lstSaleOrderId;
	}

	public Boolean getIsCheckOrderDateDesc() {
		return isCheckOrderDateDesc;
	}

	public void setIsCheckOrderDateDesc(Boolean isCheckOrderDateDesc) {
		this.isCheckOrderDateDesc = isCheckOrderDateDesc;
	}

	public KPaging<SaleOrderDetail> getkPagingDetail() {
		return kPagingDetail;
	}

	public void setkPagingDetail(KPaging<SaleOrderDetail> kPagingDetail) {
		this.kPagingDetail = kPagingDetail;
	}

	public KPaging<SaleOrderVO> getkPagingVO() {
		return kPagingVO;
	}

	public void setkPagingVO(KPaging<SaleOrderVO> kPagingVO) {
		this.kPagingVO = kPagingVO;
	}

	public List<Long> getLstPayReceivedId() {
		return lstPayReceivedId;
	}

	public void setLstPayReceivedId(List<Long> lstPayReceivedId) {
		this.lstPayReceivedId = lstPayReceivedId;
	}

	public KPaging<SaleOrderVOEx2> getkPagingVOEx2() {
		return kPagingVOEx2;
	}

	public void setkPagingVOEx2(KPaging<SaleOrderVOEx2> kPagingVOEx2) {
		this.kPagingVOEx2 = kPagingVOEx2;
	}

	public List<Long> getLstPassOrderId() {
		return lstPassOrderId;
	}

	public void setLstPassOrderId(List<Long> lstPassOrderId) {
		this.lstPassOrderId = lstPassOrderId;
	}

	public List<String> getLstOrderTypeStr() {
		return lstOrderTypeStr;
	}

	public void setLstOrderTypeStr(List<String> lstOrderTypeStr) {
		this.lstOrderTypeStr = lstOrderTypeStr;
	}

	public Integer getPrintVATStatus() {
		return printVATStatus;
	}

	public void setPrintVATStatus(Integer printVATStatus) {
		this.printVATStatus = printVATStatus;
	}

	public boolean getExactStaffSearching() {
		return exactStaffSearching;
	}

	public void setExactStaffSearching(boolean exactStaffSearching) {
		this.exactStaffSearching = exactStaffSearching;
	}

	public Long getShopTypeId() {
		return shopTypeId;
	}

	public void setShopTypeId(Long shopTypeId) {
		this.shopTypeId = shopTypeId;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Boolean getApprovedAndStep1() {
		return approvedAndStep1;
	}

	public void setApprovedAndStep1(Boolean approvedAndStep1) {
		this.approvedAndStep1 = approvedAndStep1;
	}

	public Boolean getIsDividualWarehouse() {
		return isDividualWarehouse;
	}

	public void setIsDividualWarehouse(Boolean isDividualWarehouse) {
		this.isDividualWarehouse = isDividualWarehouse;
	}

	public String getStrListUserId() {
		return strListUserId;
	}

	public void setStrListUserId(String strListUserId) {
		this.strListUserId = strListUserId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
	
}

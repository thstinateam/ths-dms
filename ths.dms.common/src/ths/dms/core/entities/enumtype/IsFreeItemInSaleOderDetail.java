/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum OrderType.
 * 
 * @author hunglm16
 */
public enum IsFreeItemInSaleOderDetail {

	SALE(0), //Don hang ban

	PROMOTION(1), // Don hang khuyen mai

	RETURNSO(2);

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, IsFreeItemInSaleOderDetail> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	IsFreeItemInSaleOderDetail(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static IsFreeItemInSaleOderDetail parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, IsFreeItemInSaleOderDetail>(IsFreeItemInSaleOderDetail.values().length);
			for (IsFreeItemInSaleOderDetail e : IsFreeItemInSaleOderDetail.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

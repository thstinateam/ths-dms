/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum AttributeColumnType {
    
	CHARACTER (1),

    NUMBER (2),
    
    DATE_TIME (3),
	
	CHOICE (4),
	
	MULTI_CHOICE (5),
	
	LOCATION(6);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, AttributeColumnType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    AttributeColumnType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static AttributeColumnType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, AttributeColumnType>(
                    AttributeColumnType.values().length);
            for (AttributeColumnType e : AttributeColumnType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

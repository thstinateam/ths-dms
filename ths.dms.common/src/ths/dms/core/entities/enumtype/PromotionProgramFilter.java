package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.PromotionProgram;

public class PromotionProgramFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private KPaging<PromotionProgram> kPaging;
	private Long id;
	private String ppCode;
	private String ppName;
	private List<String> lstType;
	private String shopCode;
	private String customerTypeCode;
	private Date fromDate;
	private Date toDate;
	private ActiveType status;
	private Boolean isAutoPromotion;
	private String promotionName;
	private String createUser;
	private Integer check;
	private Boolean isVNM;
	private List<Integer> lstStatus;
	private String strListShopId;
	private boolean flag; 
	private Integer isDefaultDiscountType;
	private Integer calPriceVat;
	
	
	public Integer getCalPriceVat() {
		return calPriceVat;
	}
	public void setCalPriceVat(Integer calPriceVat) {
		this.calPriceVat = calPriceVat;
	}
	public Integer getIsDefaultDiscountType() {
		return isDefaultDiscountType;
	}
	public void setIsDefaultDiscountType(Integer isDefaultDiscountType) {
		this.isDefaultDiscountType = isDefaultDiscountType;
	}
	public boolean getFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public Boolean getIsVNM() {
		return isVNM;
	}
	public void setIsVNM(Boolean isVNM) {
		this.isVNM = isVNM;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public KPaging<PromotionProgram> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<PromotionProgram> kPaging) {
		this.kPaging = kPaging;
	}
	public String getPpCode() {
		return ppCode;
	}
	public void setPpCode(String ppCode) {
		this.ppCode = ppCode;
	}
	public String getPpName() {
		return ppName;
	}
	public void setPpName(String ppName) {
		this.ppName = ppName;
	}
	public List<String> getLstType() {
		return lstType;
	}
	public void setLstType(List<String> lstType) {
		this.lstType = lstType;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getCustomerTypeCode() {
		return customerTypeCode;
	}
	public void setCustomerTypeCode(String customerTypeCode) {
		this.customerTypeCode = customerTypeCode;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public Boolean getIsAutoPromotion() {
		return isAutoPromotion;
	}
	public void setIsAutoPromotion(Boolean isAutoPromotion) {
		this.isAutoPromotion = isAutoPromotion;
	}
	public String getPromotionName() {
		return promotionName;
	}
	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Integer getCheck() {
		return check;
	}
	public void setCheck(Integer check) {
		this.check = check;
	}
	public List<Integer> getLstStatus() {
		return lstStatus;
	}
	public void setLstStatus(List<Integer> lstStatus) {
		this.lstStatus = lstStatus;
	}
	public String getStrListShopId() {
		return strListShopId;
	}
	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}
}

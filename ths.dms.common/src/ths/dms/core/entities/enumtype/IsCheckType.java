package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author liemtpt
 * @description  is check type dung de kiem tra gia tri la co hay khong
 */
public enum IsCheckType {

	YES(1),

	NO(0);

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, IsCheckType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value the value
	 */
	IsCheckType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author hunglm16
	 * @param value the value
	 * @return the gender type
	 */
	public static IsCheckType parseValue(Integer value) {
		if (value == null) {
			value = 0;
		}
		if (values == null) {
			values = new HashMap<Integer, IsCheckType>(IsCheckType.values().length);
			for (IsCheckType e : IsCheckType.values()) {
				values.put(e.getValue(), e);
			}
		}
		return values.get(value);
	}

}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum GoodsType.
 * 
 * @author huytran
 */
public enum GoodsType {
    
    /** The FEMALE. */
    NORMAL (0),
    
    /** The MALE. */
    FREE (1);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, GoodsType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    GoodsType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static GoodsType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, GoodsType>(
                    GoodsType.values().length);
            for (GoodsType e : GoodsType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum StockObjectType.
 * 
 * @author huytran
 */
public enum PoType {

	PO_CUSTOMER_SERVICE(1), // PO DV nhap

	PO_CONFIRM(2), // PO confirm nhap

	RETURNED_SALES_ORDER(3), // PO confirm tra

	PO_CUSTOMER_SERVICE_RETURN(4); // PO DV tra
	
	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, PoType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	PoType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static PoType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, PoType>(PoType.values().length);
			for (PoType e : PoType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

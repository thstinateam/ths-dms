/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum SortByType {
    
    PRODUCT_CODE (0),

    PRODUCT_NAME (1);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, SortByType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    SortByType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static SortByType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, SortByType>(
                    SortByType.values().length);
            for (SortByType e : SortByType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

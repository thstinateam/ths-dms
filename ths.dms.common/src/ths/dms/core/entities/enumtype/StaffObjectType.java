/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum OrderType.
 * 
 * @author huytran
 */
public enum StaffObjectType {
	VIETTEL_ADMIN (0),
	NV (1),
	GS (2),
	QL (3),
	NVGH (4),
	NVTT (5),
	NVBH(12),

	NVVS(21),
	
	NVGS(51),
	
	TBHM(6),
	
	TBHV(7),
	
	NHVNM(8),
	
	KTNPP(9),
	
	TNKD(10),
	
	DRIVER(11),
	
	NVBHANDNVVS(1012) //Bo sung cho chuc nang tim kiem
	;

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, StaffObjectType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	StaffObjectType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static StaffObjectType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, StaffObjectType>(StaffObjectType.values().length);
			for (StaffObjectType e : StaffObjectType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}
}

package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.ShopProduct;

public class ShopProductFilter implements Serializable {

	/**
	 * @author hunglm16
	 * @since JUNE 17,2014
	 * @class Parameter Shop Product
	 */
	private static final long serialVersionUID = 1L;

	private KPaging<ShopProduct> kPaging;
	
	private Long shopId;
	private Long productInfoId;//Cat_id
	private Long productId;
	private String userName;
	
	private List<Long> lstId;
	
	public List<Long> getLstId() {
		return lstId;
	}
	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}
	public KPaging<ShopProduct> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<ShopProduct> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getProductInfoId() {
		return productInfoId;
	}
	public void setProductInfoId(Long productInfoId) {
		this.productInfoId = productInfoId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	private Integer type;
}

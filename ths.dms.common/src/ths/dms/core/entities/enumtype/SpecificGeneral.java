/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Dung cho phan quyen tuong tac man hinh
 * @author hunglm16
 * @since 26/09/2015
 */
public enum SpecificGeneral {
	NPP(1);
	
	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, SpecificGeneral> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value the value
	 */
	SpecificGeneral(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value the value
	 * @return the gender type
	 */
	public static SpecificGeneral parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, SpecificGeneral>(SpecificGeneral.values().length);
			for (SpecificGeneral e : SpecificGeneral.values()) {
				values.put(e.getValue(), e);				
			}
		}
		return values.get(value);
	}
}

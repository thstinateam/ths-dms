/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum SourceType.
 * 
 * @author huytran
 */
public enum SourceType {
    
    WEB (0),

    TABLET (1);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, SourceType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    SourceType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static SourceType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, SourceType>(
                    SourceType.values().length);
            for (SourceType e : SourceType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author hunglm16
 * @since 01/09/2015
 */
public enum StatusType {
	All(-2),
	DELETED(-1),

	INACTIVE(0),

	ACTIVE(1),
	CANCALLED(2)
	;

	/** The value. */
	private Integer value;
	/** The values. */
	private static Map<Integer, StatusType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	StatusType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @author hunglm16
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static StatusType parseValue(Integer value) {
		if (value == null) {
			value = -1;
		}
		if (values == null) {
			values = new HashMap<Integer, StatusType>(StatusType.values().length);
			for (StatusType e : StatusType.values())
				values.put(e.getValue(), e);
		}
		return values.get(value);
	}

	/**
	 * Kiem tra co ton tai tinh trang quy dinh.
	 * 
	 * @author hunglm16
	 * @param value
	 * @return flag
	 */
	public static boolean isValidValue(Integer value) {
		if (value == null) {
			return false;
		}
		if (!StatusType.ACTIVE.getValue().equals(value) && !StatusType.INACTIVE.getValue().equals(value)) {
			return false;
		}
		return true;
	}
}

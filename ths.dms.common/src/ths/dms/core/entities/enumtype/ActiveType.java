/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author hunglm16
 * @description Quy dinh tinh trang tren toan bo he thong SO
 */
public enum ActiveType {
    
	DELETED(-1),

	STOPPED(0),

	RUNNING(1),

	WAITING(2), 
	
	REJECTED(3),
	
	KHONG_DAT(5),
	
	DAT(6),

	PRODUCTTYPE(7),
	
	HET_HAN(8); 
	
	 
    
    /** The value. */
    private Integer value;
    /** The values. */
    private static Map<Integer, ActiveType> values = null;
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    ActiveType(Integer value) {
        this.value = value;
    }
    /**
     * Parses the value.
     * 
     * @author hunglm16
     * @param value the value
     * @return the gender type
     */
    public static ActiveType parseValue(Integer value) {
        if(value==null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, ActiveType>(
                    ActiveType.values().length);
            for (ActiveType e : ActiveType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
    /**
     * Kiem tra co ton tai tinh trang quy dinh.
     * 
     * @author hunglm16
     * @param value
     * @return flag
     */
    public static boolean isValidValue(Integer value) {
    	if(value == null){
    		return false;
    	}
    	if(!ActiveType.DELETED.getValue().equals(value) && !ActiveType.STOPPED.getValue().equals(value)
				&& !ActiveType.WAITING.getValue().equals(value) && !ActiveType.RUNNING.getValue().equals(value)){
    		return false;
    	}
        return true;
    }
}

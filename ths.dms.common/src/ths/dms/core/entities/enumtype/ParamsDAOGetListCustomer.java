/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.Customer;

/**
 * 
 * @author hungnm
 */
/**
 * @author hungnm
 *
 */
public class ParamsDAOGetListCustomer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    
	private KPaging<Customer> kPaging;
	private String shortCode;
	private String customerName;
	private List<String> lstAreaCode; 
	private String phone; 
	private String mobiPhone;
	private Long customerTypeId;
	private ActiveType status; 
	private String region; 
	private String loyalty;
	private Long deliverId; 
	private Long cashierId; 
	private String address; 
	private String street; 
	private Long shopId; 
	private Long cycleId; 
	private Long ksId; 
	private String directShopCode; 
	private String shopCodeLike;
	private String shopCode;
	private String display; 
	private String location; 
	private Long notInDeliverId;
	private String customerTypeCode;
	private String strShopId;
	private boolean allSubShop;
	private String order;
	private String sort;
	private Integer tansuat;
	public KPaging<Customer> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<Customer> kPaging) {
		this.kPaging = kPaging;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public List<String> getLstAreaCode() {
		return lstAreaCode;
	}
	public void setLstAreaCode(List<String> lstAreaCode) {
		this.lstAreaCode = lstAreaCode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobiPhone() {
		return mobiPhone;
	}
	public void setMobiPhone(String mobiPhone) {
		this.mobiPhone = mobiPhone;
	}
	public Long getCustomerTypeId() {
		return customerTypeId;
	}
	public void setCustomerTypeId(Long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getLoyalty() {
		return loyalty;
	}
	public void setLoyalty(String loyalty) {
		this.loyalty = loyalty;
	}
	public Long getDeliverId() {
		return deliverId;
	}
	public void setDeliverId(Long deliverId) {
		this.deliverId = deliverId;
	}
	public Long getCashierId() {
		return cashierId;
	}
	public void setCashierId(Long cashierId) {
		this.cashierId = cashierId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getDirectShopCode() {
		return directShopCode;
	}
	public void setDirectShopCode(String directShopCode) {
		this.directShopCode = directShopCode;
	}
	public String getShopCodeLike() {
		return shopCodeLike;
	}
	public void setShopCodeLike(String shopCodeLike) {
		this.shopCodeLike = shopCodeLike;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Long getNotInDeliverId() {
		return notInDeliverId;
	}
	public void setNotInDeliverId(Long notInDeliverId) {
		this.notInDeliverId = notInDeliverId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getCustomerTypeCode() {
		return customerTypeCode;
	}
	public void setCustomerTypeCode(String customerTypeCode) {
		this.customerTypeCode = customerTypeCode;
	}
	public boolean isAllSubShop() {
		return allSubShop;
	}
	public void setAllSubShop(boolean allSubShop) {
		this.allSubShop = allSubShop;
	}
	public String getStrShopId() {
		return strShopId;
	}
	public void setStrShopId(String strShopId) {
		this.strShopId = strShopId;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public Integer getTansuat() {
		return tansuat;
	}
	public void setTansuat(Integer tansuat) {
		this.tansuat = tansuat;
	}
	public Long getCycleId() {
		return cycleId;
	}
	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}
	public Long getKsId() {
		return ksId;
	}
	public void setKsId(Long ksId) {
		this.ksId = ksId;
	}
	
	
    
}

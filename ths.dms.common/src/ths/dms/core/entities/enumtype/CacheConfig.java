package ths.dms.core.entities.enumtype;

/**
 * @author thanhtc
 *
 */
public class CacheConfig {
	private String regionKey;
	private boolean isCache;

	public CacheConfig(String regionKey) {
		this.regionKey = regionKey;
		this.isCache = true;
	}

	public CacheConfig(boolean isCache) {
		this.isCache = isCache;
	}

	public String getRegionKey() {
		return regionKey;
	}

	public void setRegionKey(String regionKey) {
		this.regionKey = regionKey;
	}

	public boolean isCache() {
		return isCache;
	}

	public void setCache(boolean isCache) {
		this.isCache = isCache;
	}

}

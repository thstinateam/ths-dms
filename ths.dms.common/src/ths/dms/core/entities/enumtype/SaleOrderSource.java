/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum SaleOrderSource {

    WEB (1),
    
    TABLET (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, SaleOrderSource> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    SaleOrderSource(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static SaleOrderSource parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, SaleOrderSource>(
                    SaleOrderSource.values().length);
            for (SaleOrderSource e : SaleOrderSource.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author huytran
 */
public enum SaleOrderType {

    RETURNED (0),
    
    NOT_YET_RETURNED (1),
    
    RETURNED_ORDER (2);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, SaleOrderType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    SaleOrderType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static SaleOrderType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, SaleOrderType>(
                    SaleOrderType.values().length);
            for (SaleOrderType e : SaleOrderType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

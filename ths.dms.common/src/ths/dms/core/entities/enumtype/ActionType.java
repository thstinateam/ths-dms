/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum ActionType {
    
    INSERT (0),

    UPDATE (1),
    
    DELETE (2);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ActionType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    ActionType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static ActionType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ActionType>(
                    ActionType.values().length);
            for (ActionType e : ActionType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cuonglt3
 *
 */
public enum StaffSpecificType {
	VIETTEL_ADMIN(0),
	STAFF(1),
	SUPERVISOR(2),
	MANAGER(3), 
	NVGH(4),
	NVTT(5),
	RSM(6),
	GSMT(7)
	;
	
	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, StaffSpecificType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value the value
	 */
	StaffSpecificType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value the value
	 * @return the gender type
	 */
	public static StaffSpecificType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, StaffSpecificType>(StaffSpecificType.values().length);
			for (StaffSpecificType e : StaffSpecificType.values()) {
				values.put(e.getValue(), e);				
			}
		}
		return values.get(value);
	}
}

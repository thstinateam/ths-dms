/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;

/**
 * Tap tham so chuont trinh trong tam
 * @modify hunglm16
 * @since 02/11/2015
 */
public class FocusProgramFilter implements Serializable{

	private static final long serialVersionUID = 5719535609658660659L;

	private Long shopId;
	private Long staffRootId;
	private Long shopRootId;
	private Long rollId;
	private Long id;
	
	private String shopCode;
	private String focusProgramCode;
	private String focusProgramName;
	private String strListShopId;
	private String createUser;
	
	private Boolean isAdmin;
	private Boolean flagAll;
	
	private Date fromDate;
	
	private Date toDate;
	
	private ActiveType status;
	
	/**
	 * Khai bao GETTER/SETTER
	 * @return
	 */
	public String getFocusProgramCode() {
		return focusProgramCode;
	}

	public void setFocusProgramCode(String focusProgramCode) {
		this.focusProgramCode = focusProgramCode;
	}

	public String getFocusProgramName() {
		return focusProgramName;
	}

	public void setFocusProgramName(String focusProgramName) {
		this.focusProgramName = focusProgramName;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getStrListShopId() {
		return strListShopId;
	}

	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStaffRootId() {
		return staffRootId;
	}

	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}

	public Long getShopRootId() {
		return shopRootId;
	}

	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}

	public Long getRollId() {
		return rollId;
	}

	public void setRollId(Long rollId) {
		this.rollId = rollId;
	}

	public Boolean getFlagAll() {
		return flagAll;
	}

	public void setFlagAll(Boolean flagAll) {
		this.flagAll = flagAll;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}

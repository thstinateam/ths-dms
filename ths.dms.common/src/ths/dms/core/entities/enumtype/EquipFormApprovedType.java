/**
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum loai phe duyet
 * 
 * @author tamvnm
 * @since Jan 13, 2016
 */
public enum EquipFormApprovedType {
	EQUIPMENT(0), // thiet bi
	DELIVERY(1), // giao nhan
	REPAIR(2); // sua chua

	private Integer value;

	private static Map<Integer, EquipFormApprovedType> values = null;

	public Integer getValue() {
		return value;
	}

	EquipFormApprovedType(Integer value) {
		this.value = value;
	}

	public static EquipFormApprovedType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, EquipFormApprovedType>(EquipFormApprovedType.values().length);
			for (EquipFormApprovedType e : EquipFormApprovedType.values()) {
				values.put(e.getValue(), e);
			}
		}
		return values.get(value);
	}
}
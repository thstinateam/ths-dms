/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * Loai giao dich kho
 * @author tientv11
 *
 */
public enum StockTransLotOwnerType {
    
	WAREHOUSE (1),

    STAFF (2),
    
    UPDATED (3);
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, StockTransLotOwnerType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    StockTransLotOwnerType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static StockTransLotOwnerType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, StockTransLotOwnerType>(
                    StockTransLotOwnerType.values().length);
            for (StockTransLotOwnerType e : StockTransLotOwnerType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;

/**
 * Filter dung chung cho kho thiet bi
 * 
 * @author hunglm16
 * @since December 21,2014
 */
public class EquipStockEquipFilter<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	private KPaging<T> kPaging;

	private Long id;
	private Long equipGroupId;
	private Long fromStockId;
	private Long toStockId;
	private Long stockId;
	
	private Integer stockType; 
	
	private Date createFormDate;
	private Date approveDate;
	private Date refuseDate;

	private String healthStatus;
	private Integer status;
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	public Integer getStockType() {
		return stockType;
	}
	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEquipGroupId() {
		return equipGroupId;
	}
	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}
	public Long getFromStockId() {
		return fromStockId;
	}
	public void setFromStockId(Long fromStockId) {
		this.fromStockId = fromStockId;
	}
	public Long getToStockId() {
		return toStockId;
	}
	public void setToStockId(Long toStockId) {
		this.toStockId = toStockId;
	}
	public String getHealthStatus() {
		return healthStatus;
	}
	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}
	public Date getCreateFormDate() {
		return createFormDate;
	}
	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}
	public Date getApproveDate() {
		return approveDate;
	}
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}
	public Date getRefuseDate() {
		return refuseDate;
	}
	public void setRefuseDate(Date refuseDate) {
		this.refuseDate = refuseDate;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}

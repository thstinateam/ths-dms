/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.math.BigDecimal;



public class IncentiveProgramLevelVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8326706057557119156L;

	private Long id;
	
	private Long programId;
	
	private String programCode;
	
	private String programName;
	
	private String levelCode;
	
	private BigDecimal amount;
	
	private BigDecimal freeAmount;
	
	private Long freeProductId;
	
	private String freeProductName;
	
	private String freeProductCode;
	
	private Integer freeQuantity;
	
	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProgramId() {
		return programId;
	}

	public void setProgramId(Long programId) {
		this.programId = programId;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public String getLevelCode() {
		return levelCode;
	}

	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFreeAmount() {
		return freeAmount;
	}

	public void setFreeAmount(BigDecimal freeAmount) {
		this.freeAmount = freeAmount;
	}

	public Long getFreeProductId() {
		return freeProductId;
	}

	public void setFreeProductId(Long freeProductId) {
		this.freeProductId = freeProductId;
	}

	public String getFreeProductCode() {
		return freeProductCode;
	}

	public void setFreeProductCode(String freeProductCode) {
		this.freeProductCode = freeProductCode;
	}

	public String getFreeProductName() {
		return freeProductName;
	}

	public void setFreeProductName(String freeProductName) {
		this.freeProductName = freeProductName;
	}

	public Integer getFreeQuantity() {
		return freeQuantity;
	}

	public void setFreeQuantity(Integer freeQuantity) {
		this.freeQuantity = freeQuantity;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}

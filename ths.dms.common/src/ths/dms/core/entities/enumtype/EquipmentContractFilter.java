/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;

public class EquipmentContractFilter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6492223311164902482L;
	
	
	private String contractNo; 
	
	private String prevContractNo;
	
	private String shopCode;
	
	private String customerCode;
	
	private String equipmentCode;
	
	private Date fromDate;
	
	private Date toDate;
	
	private String series;
	
	private EquipmentContractStatus status;

//	private Long parentShopId; 
	
	
	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getPrevContractNo() {
		return prevContractNo;
	}

	public void setPrevContractNo(String prevContractNo) {
		this.prevContractNo = prevContractNo;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public EquipmentContractStatus getStatus() {
		return status;
	}

	public void setStatus(EquipmentContractStatus status) {
		this.status = status;
	}
	
}

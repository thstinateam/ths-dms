package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.vo.CycleCountSearchVO;

public class CycleCountFilter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private KPaging<CycleCountSearchVO> kPaging;	
	private Long shopId;
	private String cycleCountCode;
	private CycleCountType status;
	private String description;
	private Date fromDate;
	private Date toDate;
	private Date fromCreate;
	private Date toCreate;
	private Long wareHouseId;
	private String strListShopId;
	private Integer yearPeriod; 
	private Integer numPeriod;
	
	public KPaging<CycleCountSearchVO> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<CycleCountSearchVO> kPaging) {
		this.kPaging = kPaging;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getCycleCountCode() {
		return cycleCountCode;
	}
	public void setCycleCountCode(String cycleCountCode) {
		this.cycleCountCode = cycleCountCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getFromCreate() {
		return fromCreate;
	}
	public void setFromCreate(Date fromCreate) {
		this.fromCreate = fromCreate;
	}
	public Date getToCreate() {
		return toCreate;
	}
	public void setToCreate(Date toCreate) {
		this.toCreate = toCreate;
	}
	public Long getWareHouseId() {
		return wareHouseId;
	}
	public void setWareHouseId(Long wareHouseId) {
		this.wareHouseId = wareHouseId;
	}
	public String getStrListShopId() {
		return strListShopId;
	}
	public void setStrListShopId(String strListShopId) {
		this.strListShopId = strListShopId;
	}
	public CycleCountType getStatus() {
		return status;
	}
	public void setStatus(CycleCountType status) {
		this.status = status;
	}
	public Integer getYearPeriod() {
		return yearPeriod;
	}
	public void setYearPeriod(Integer yearPeriod) {
		this.yearPeriod = yearPeriod;
	}
	public Integer getNumPeriod() {
		return numPeriod;
	}
	public void setNumPeriod(Integer numPeriod) {
		this.numPeriod = numPeriod;
	}
	
	
}

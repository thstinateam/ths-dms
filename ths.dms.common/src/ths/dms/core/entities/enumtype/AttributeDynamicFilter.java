package ths.dms.core.entities.enumtype;

import java.io.Serializable;
import java.math.BigDecimal;

public class AttributeDynamicFilter implements Serializable {
	/**
	 * @author liemtpt
	 * @since 21/01/2015
	 */
	private static final long serialVersionUID = 1L;

	private Long attributeId;
	private String attributeCode;
	private String attributeName;
	private Integer applyObject;
	private String description;
	private Integer type;
	private Integer displayOrder;
	private BigDecimal dataLength;
	private Integer isEnumeration;
	private Integer isSearch;
	private Integer mandatory;
	private BigDecimal maxValue;
	private BigDecimal minValue;
	private Integer status;
	private String order;
	private String sort;
	
	public Long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}
	public String getAttributeCode() {
		return attributeCode;
	}
	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getApplyObject() {
		return applyObject;
	}
	public void setApplyObject(Integer applyObject) {
		this.applyObject = applyObject;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
	public BigDecimal getDataLength() {
		return dataLength;
	}
	public void setDataLength(BigDecimal dataLength) {
		this.dataLength = dataLength;
	}
	public Integer getIsEnumeration() {
		return isEnumeration;
	}
	public void setIsEnumeration(Integer isEnumeration) {
		this.isEnumeration = isEnumeration;
	}
	public Integer getIsSearch() {
		return isSearch;
	}
	public void setIsSearch(Integer isSearch) {
		this.isSearch = isSearch;
	}
	public Integer getMandatory() {
		return mandatory;
	}
	public void setMandatory(Integer mandatory) {
		this.mandatory = mandatory;
	}
	public BigDecimal getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(BigDecimal maxValue) {
		this.maxValue = maxValue;
	}
	public BigDecimal getMinValue() {
		return minValue;
	}
	public void setMinValue(BigDecimal minValue) {
		this.minValue = minValue;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum Shop Location.
 * 
 * @author loctt1
 */
public enum ShopLocation {
    
    NORMAL_SHOP (0),
	
	TRADE_CENTER (1);

    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, ShopLocation> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new shop location
     * 
     * @param value
     *            the value
     */
    ShopLocation(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the shop location
     */
    public static ShopLocation parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, ShopLocation>(
                    ShopLocation.values().length);
            for (ShopLocation e : ShopLocation.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

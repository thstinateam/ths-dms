/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum RequestAPI.
 * 
 * @author LuanDV.
 * @since 29/10/2010 - Created.
 */
public enum RequestAPI {
	
	/** FROM STREET TO LAT LONG */
	FROM_STREET_TO_LAT_LONG (1);
	
	/** The value. */
	private Integer value;
    
    /** The values. */
    private static Map<Integer, RequestAPI> values = null;
    
    /**
	 * Gets the value.
	 * 
	 * @return the value
	 */
    public Integer getValue() {
        return value;
    }
    
    /**
	 * Instantiates a new account type.
	 * 
	 * @param value
	 *            the value
	 */
    RequestAPI(Integer value) {
        this.value = value;
    }
    
    /**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the account type
	 */
    public static RequestAPI parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, RequestAPI>(
                    RequestAPI.values().length);
            for (RequestAPI e : RequestAPI.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

/**
 * 
 */
package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum StockObjectType.
 * 
 * @author huytran
 */
public enum StockObjectType {
    
    SHOP (1),

    STAFF (2),
    
    CUSTOMER (3),
    
    VANSALE(4)
    ;
    
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static Map<Integer, StockObjectType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    StockObjectType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static StockObjectType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, StockObjectType>(
                    StockObjectType.values().length);
            for (StockObjectType e : StockObjectType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}

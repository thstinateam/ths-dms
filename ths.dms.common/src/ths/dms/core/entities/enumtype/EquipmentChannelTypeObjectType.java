package ths.dms.core.entities.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum EquimentChannelType.
 * 
 * @author hunglm16
 * @since December 18,2014
 * @description Type = 13, Object Type nhan gia tri value, ChannelTypeCode nhan gia tri Name
 */
public enum EquipmentChannelTypeObjectType {

	EQM_LTB(1), // Loai thiet bi

	EQM_NCC(2), // Nha cung cap

	EQM_HIEU(3), // Hieu

	EQM_HMSC(4) // Hang muc sua chua
	;

	/** The value. */
	private Integer value;

	/** The values. */
	private static Map<Integer, EquipmentChannelTypeObjectType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 */
	EquipmentChannelTypeObjectType(Integer value) {
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static EquipmentChannelTypeObjectType parseValue(Integer value) {
		if (values == null) {
			values = new HashMap<Integer, EquipmentChannelTypeObjectType>(EquipmentChannelTypeObjectType.values().length);
			for (EquipmentChannelTypeObjectType e : EquipmentChannelTypeObjectType.values()) {
				values.put(e.getValue(), e);
			}
		}
		return values.get(value);
	}
}

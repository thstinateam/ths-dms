package ths.dms.core.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * 
 * Auto generate entities - HaNoi standard
 * 
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "GROUP_STAFF_PAYROLL_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "GROUP_STAFF_PAYROLL_DETAIL_SEQ", allocationSize = 1)
public class GroupStaffPayrollDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "GROUP_STAFF_PAYROLL_DETAIL_ID")
	private Long id;

	// ID NHOM NV
	@ManyToOne(targetEntity = GroupStaffPayroll.class)
	@JoinColumn(name = "GROUP_STAFF_PAYROLL_ID", referencedColumnName = "GROUP_STAFF_PAYROLL_ID")
	private GroupStaffPayroll groupStaffPayroll;

	// ID NV
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	public GroupStaffPayrollDetail clone() {
		GroupStaffPayrollDetail obj = new GroupStaffPayrollDetail();

		obj.setGroupStaffPayroll(groupStaffPayroll);
		obj.setId(id);
		obj.setStaff(staff);

		return obj;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GroupStaffPayroll getGroupStaffPayroll() {
		return groupStaffPayroll;
	}

	public void setGroupStaffPayroll(GroupStaffPayroll groupStaffPayroll) {
		this.groupStaffPayroll = groupStaffPayroll;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}
}
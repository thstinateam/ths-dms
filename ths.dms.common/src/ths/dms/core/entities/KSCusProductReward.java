package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
*
* Auto generate entities
* @author tungmt
* @since 30/6/2015
* 
*/

@Entity
@Table(name = "KS_CUS_PRODUCT_REWARD")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "KS_CUS_PRODUCT_REWARD_SEQ", allocationSize = 1)
public class KSCusProductReward implements Serializable {

	private static final long serialVersionUID = 1L;

	//ID no
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "KS_CUS_PRODUCT_REWARD_ID")
	private Long id;
	
	
	@ManyToOne(targetEntity = KSCustomer.class)
	@JoinColumn(name = "KS_CUSTOMER_ID", referencedColumnName = "KS_CUSTOMER_ID")
	private KSCustomer ksCustomer;
	
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;
	
	@Basic
	@Column(name = "PRODUCT_NUM", length = 6)
	private Integer productNum = 0 ;
	
	@Basic
	@Column(name = "PRODUCT_NUM_DONE", length = 6)
	private Integer productNumDone = 0 ;
	
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 100)
	private String createUser;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 100)
	private String updateUser;
	
	@ManyToOne(targetEntity = KS.class)
	@JoinColumn(name = "KS_ID", referencedColumnName = "KS_ID")
	private KS ks;
	
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public KSCustomer getKsCustomer() {
		return ksCustomer;
	}

	public void setKsCustomer(KSCustomer ksCustomer) {
		this.ksCustomer = ksCustomer;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getProductNum() {
		return productNum;
	}

	public void setProductNum(Integer productNum) {
		this.productNum = productNum;
	}

	public Integer getProductNumDone() {
		if (productNumDone == null) {
			return 0;
		}
		return productNumDone;
	}

	public void setProductNumDone(Integer productNumDone) {
		this.productNumDone = productNumDone;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public KS getKs() {
		return ks;
	}

	public void setKs(KS ks) {
		this.ks = ks;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

}
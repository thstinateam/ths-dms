package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.MediaType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "MEDIA_ITEM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "MEDIA_ITEM_SEQ", allocationSize = 1)
public class MediaItem implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic
	@Column(name = "CLIENT_THUMB_URL", length = 250)
	private String clientThumbUrl;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 500)
	private String createUser;

	@Basic
	@Column(name = "DESCRIPTION", length = 3000)
	private String description;

	@Basic
	@Column(name = "FILE_SIZE", length = 22)
	private Float fileSize;

	@Basic
	@Column(name = "HEIGHT", length = 22)
	private Float height;

	@Basic
	@Column(name = "LAT", length = 22)
	private Float lat = 0.0F;

	@Basic
	@Column(name = "LNG", length = 22)
	private Float lng = 0.0F;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "MEDIA_ITEM_ID")
	private Long id;

	//0:image; 1: video
	@Basic
	@Column(name = "MEDIA_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.MediaType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private MediaType mediaType;

	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	
	@Basic
	@Column(name = "OBJECT_ID")
	private Long objectId;

	//0: hinh anh dong cua, 1: hinh anh trung bay, 2: hinh anh diem ban,  3: hinh anh san pham  (chính là luu tru hình, video cho product)
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.MediaObjectType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private MediaObjectType objectType;

//	@Column(name="RESULT")
//	private Integer result;
	
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

//	@ManyToOne(targetEntity = StDisplayProgram.class)
//	@JoinColumn(name = "DISPLAY_PROGRAME_ID", referencedColumnName = "DISPLAY_PROGRAM_ID")
//	private StDisplayProgram displayProgram;
	
	@ManyToOne
	@JoinColumn(name="SHOP_ID")
	private Shop shop;
	
	@Basic
	@Column(name = "THUMB_URL", length = 250)
	private String thumbUrl;

	@Basic
	@Column(name = "TITLE", length = 250)
	private String title;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 500)
	private String updateUser;

	@Basic
	@Column(name = "URL", length = 250)
	private String url;

	@Basic
	@Column(name = "WIDTH", length = 22)
	private Float width;

	public String getClientThumbUrl() {
		return clientThumbUrl;
	}

	public void setClientThumbUrl(String clientThumbUrl) {
		this.clientThumbUrl = clientThumbUrl;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getFileSize() {
		return fileSize;
	}

	public void setFileSize(Float fileSize) {
		this.fileSize = fileSize;
	}

	public Float getHeight() {
		return height;
	}

	public void setHeight(Float height) {
		this.height = height;
	}

	public Float getLat() {
		return lat == null ? 0f : lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLng() {
		return lng == null ? 0f : lng;
	}

	public void setLng(Float lng) {
		this.lng = lng;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}

	public MediaObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(MediaObjectType objectType) {
		this.objectType = objectType;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getThumbUrl() {
		return thumbUrl;
	}

	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Float getWidth() {
		return width == null ? 0f : width;
	}

	public void setWidth(Float width) {
		this.width = width;
	}

	/**
	 * @return the objectId
	 */
	public Long getObjectId() {
		return objectId;
	}

	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

//	public Integer getResult() {
//		return result;
//	}
//
//	public void setResult(Integer result) {
//		this.result = result;
//	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

}
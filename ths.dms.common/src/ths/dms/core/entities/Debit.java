package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.DebitOwnerType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "DEBIT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "DEBIT_SEQ", allocationSize = 1)
public class Debit implements Serializable {

	private static final long serialVersionUID = 1L;
	//ID
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "DEBIT_ID")
	private Long id;

	//Han muc tien no cua KH
	@Basic
	@Column(name = "MAX_DEBIT_AMOUNT", length = 22)
	private BigDecimal maxDebitAmount;

	//Han no cua KH
	@Basic
	@Column(name = "MAX_DEBIT_DATE", length = 22)
	private Integer maxDebitDate;

	//id doi tuong shop/customer
	@Basic
	@Column(name = "OBJECT_ID", length = 22)
	private Long objectId;

	//1 shop, 3 customer
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.DebitOwnerType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private DebitOwnerType objectType;

	//Tong no don tich
	@Basic
	@Column(name = "TOTAL_AMOUNT", length = 22)
	private BigDecimal totalAmount;

	//No hien tai
	@Basic
	@Column(name = "TOTAL_DEBIT", length = 22)
	private BigDecimal totalDebit;

	//Tong tra don tich
	@Basic
	@Column(name = "TOTAL_PAY", length = 22)
	private BigDecimal totalPay;
	
	@Basic
	@Column(name = "TOTAL_DISCOUNT", length = 20)
	private BigDecimal totalDiscount = BigDecimal.ZERO;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getMaxDebitAmount() {
		return maxDebitAmount;
	}

	public void setMaxDebitAmount(BigDecimal maxDebitAmount) {
		this.maxDebitAmount = maxDebitAmount;
	}

	public Integer getMaxDebitDate() {
		return maxDebitDate;
	}

	public void setMaxDebitDate(Integer maxDebitDate) {
		this.maxDebitDate = maxDebitDate;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public DebitOwnerType getObjectType() {
		return objectType;
	}

	public void setObjectType(DebitOwnerType objectType) {
		this.objectType = objectType;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount == null ? BigDecimal.ZERO : totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalDebit() {
		return totalDebit == null ? BigDecimal.ZERO : totalDebit;
	}

	public void setTotalDebit(BigDecimal totalDebit) {
		this.totalDebit = totalDebit;
	}

	public BigDecimal getTotalPay() {
		return totalPay == null ? BigDecimal.ZERO : totalPay;
	}

	public void setTotalPay(BigDecimal totalPay) {
		this.totalPay = totalPay;
	}

	public BigDecimal getTotalDiscount() {
		return totalDiscount == null ? BigDecimal.ZERO : totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
}
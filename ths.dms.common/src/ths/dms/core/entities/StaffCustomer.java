package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "STAFF_CUSTOMER")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "STAFF_CUSTOMER_SEQ", allocationSize = 1)
public class StaffCustomer implements Serializable {

	private static final long serialVersionUID = 1L;
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	//Doanh số kế hoạch ngày tương ứng với KH
	@Basic
	@Column(name = "DAY_PLAN", length = 22)
	private BigDecimal dayPlan;

	//Doanh số kế hoạch trung bình của KH
	@Basic
	@Column(name = "DAY_PLAN_AVG", length = 22)
	private BigDecimal dayPlanAvg;

	//Ngày có cập nhật doanh số mục tiêu trung bình
	@Basic
	@Column(name = "DAY_PLAN_AVG_DATE", length = 7)
	private Date dayPlanAvgDate;

	//Ngày có cập nhật doanh số mục tiêu của KH
	@Basic
	@Column(name = "DAY_PLAN_DATE", length = 7)
	private Date dayPlanDate;

	//Ngày cho phép đặt hàng từ xa
	@Basic
	@Column(name = "EXCEPTION_ORDER_DATE", length = 7)
	private Date exceptionOrderDate;

	//Ngày cuối cùng có đơn hàng được duyệt
	@Basic
	@Column(name = "LAST_APPROVE_ORDER", length = 7)
	private Date lastApproveOrder;

	//Ngày cuối cùng có đơn hàng
	@Basic
	@Column(name = "LAST_ORDER", length = 7)
	private Date lastOrder;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "STAFF_CUSTOMER_ID")
	private Long id;

	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public BigDecimal getDayPlan() {
		return dayPlan;
	}

	public void setDayPlan(BigDecimal dayPlan) {
		this.dayPlan = dayPlan;
	}

	public BigDecimal getDayPlanAvg() {
		return dayPlanAvg;
	}

	public void setDayPlanAvg(BigDecimal dayPlanAvg) {
		this.dayPlanAvg = dayPlanAvg;
	}

	public Date getDayPlanAvgDate() {
		return dayPlanAvgDate;
	}

	public void setDayPlanAvgDate(Date dayPlanAvgDate) {
		this.dayPlanAvgDate = dayPlanAvgDate;
	}

	public Date getDayPlanDate() {
		return dayPlanDate;
	}

	public void setDayPlanDate(Date dayPlanDate) {
		this.dayPlanDate = dayPlanDate;
	}

	public Date getExceptionOrderDate() {
		return exceptionOrderDate;
	}

	public void setExceptionOrderDate(Date exceptionOrderDate) {
		this.exceptionOrderDate = exceptionOrderDate;
	}

	public Date getLastApproveOrder() {
		return lastApproveOrder;
	}

	public void setLastApproveOrder(Date lastApproveOrder) {
		this.lastApproveOrder = lastApproveOrder;
	}

	public Date getLastOrder() {
		return lastOrder;
	}

	public void setLastOrder(Date lastOrder) {
		this.lastOrder = lastOrder;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}
	

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
}
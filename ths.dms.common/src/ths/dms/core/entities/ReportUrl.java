package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.DefaultType;
import ths.dms.core.entities.enumtype.ObjectReportType;


/**
 * The persistent class for the REPORT_URL database table.
 * 
 */
@Entity
@Table(name="REPORT_URL")
public class ReportUrl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="REPORT_URL_GENERATOR", sequenceName="REPORT_URL_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REPORT_URL_GENERATOR")
	@Column(name = "REPORT_URL_ID")
	private Long id;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATE_DATE")
	private Date createDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="UPDATE_DATE")
	private Date updateDate;
	
	@Column(name="CREATE_USER")
	private String createUser;
	
	@Column(name="UPDATE_USER")
	private String updateUser;
	
	// thuoc tinh object_id trong bang la reportId
	@Column(name="OBJECT_ID")
	private Long objectId; 
	
	// 0: template bao cao VNM, 1: template bao cao NPP
	@Basic
	@Column(name="OBJECT_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ObjectReportType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ObjectReportType type;
	
	@Column(name="URL")
	private String url;
	
	// 1: template mac dinh, 0: template khong mac dinh
	@Basic
	@Column(name="IS_DEFAULT", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.DefaultType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private DefaultType isDefault = DefaultType.DEFAULT;
	//end vuongmq
   

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public ObjectReportType getType() {
		return type;
	}

	public void setType(ObjectReportType type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public DefaultType getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(DefaultType isDefault) {
		this.isDefault = isDefault;
	}

    
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

@Entity
@Table(name = "CUSTOMER_ATTRIBUTE_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "CUSTOMER_ATTRIBUTE_DETAIL_SEQ", allocationSize = 1)
public class CustomerAttributeDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 400)
	private String createUser;

	//ID
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "CUSTOMER_ATTRIBUTE_DETAIL_ID")
	private Long id;

	//ID Enum neu cho tu danh sach Enum. (NULL) neu khong chon
	@ManyToOne(targetEntity = CustomerAttributeEnum.class)
	@JoinColumn(name = "CUSTOMER_ATTRIBUTE_ENUM_ID", referencedColumnName = "CUSTOMER_ATTRIBUTE_ENUM_ID")
	private CustomerAttributeEnum customerAttributeEnum;

	//ID mau thuoc tinh
	@ManyToOne(targetEntity = CustomerAttribute.class)
	@JoinColumn(name = "CUSTOMER_ATTRIBUTE_ID", referencedColumnName = "CUSTOMER_ATTRIBUTE_ID")
	private CustomerAttribute customerAttribute;

	//ID khach hang
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	//Trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 400)
	private String updateUser;

	//Gia tri
	@Basic
	@Column(name = "VALUE", length = 4000)
	private String value;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CustomerAttributeEnum getCustomerAttributeEnum() {
		return customerAttributeEnum;
	}

	public void setCustomerAttributeEnum(CustomerAttributeEnum customerAttributeEnum) {
		this.customerAttributeEnum = customerAttributeEnum;
	}

	public CustomerAttribute getCustomerAttribute() {
		return customerAttribute;
	}

	public void setCustomerAttribute(CustomerAttribute customerAttribute) {
		this.customerAttribute = customerAttribute;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
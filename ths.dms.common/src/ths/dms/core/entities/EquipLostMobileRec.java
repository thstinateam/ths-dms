package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_LOST_MOBILE_REC")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_LOST_MOBILE_REC_SEQ", allocationSize = 1)
public class EquipLostMobileRec implements Serializable {

	private static final long serialVersionUID = 1L;

	//m? phi?u b�o m?t
	@Basic
	@Column(name = "CODE", length = 400)
	private String code;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//ID thi?t b?
	@ManyToOne(targetEntity = Equipment.class)
	@JoinColumn(name = "EQUIP_ID", referencedColumnName = "EQUIP_ID")
	private Equipment equip;

	//ID phi?u b�o m?t
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_LOST_MOBILE_REC_ID")
	private Long id;

	//ID phi?u b�o m?t, n?u ch�a t?o phi?u b�o m?t th? �? -1
	@Basic
	@Column(name = "EQUIP_LOST_REC_ID")
	private Long equipLostRecId;


	//ng�y ph�t sinh doanh s? cu?i
	@Basic
	@Column(name = "LAST_ARISING_SALES_DATE", length = 7)
	private Date lastArisingSalesDate;

	//ng�y m?t
	@Basic
	@Column(name = "LOST_DATE", length = 7)
	private Date lostDate;

	//tr?ng th�i phi?u b�o m?t 0: ch�a x? l?, 1:�? x? l?, 2: �? h?y
	@Basic
	@Column(name = "RECORD_STATUS", length = 22)
	private Integer recordStatus;

	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "REPORT_STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	//m? kho kh�ch h�ng
	@Basic
	@Column(name = "STOCK_CODE", length = 400)
	private String stockCode;

	//ID kho kh�ch h�ng
	@Basic
	@Column(name = "STOCK_ID")
	private Long stockId;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;
	
	

	public Long getEquipLostRecId() {
		return equipLostRecId;
	}

	public void setEquipLostRecId(Long equipLostRecId) {
		this.equipLostRecId = equipLostRecId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Equipment getEquip() {
		return equip;
	}

	public void setEquip(Equipment equip) {
		this.equip = equip;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	public EquipLostRecord getEquipLostRec() {
//		return equipLostRec;
//	}
//
//	public void setEquipLostRec(EquipLostRecord equipLostRec) {
//		this.equipLostRec = equipLostRec;
//	}

	public Date getLastArisingSalesDate() {
		return lastArisingSalesDate;
	}

	public void setLastArisingSalesDate(Date lastArisingSalesDate) {
		this.lastArisingSalesDate = lastArisingSalesDate;
	}

	public Date getLostDate() {
		return lostDate;
	}

	public void setLostDate(Date lostDate) {
		this.lostDate = lostDate;
	}

	

	public Integer getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	

}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author phut
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_STATISTIC_RECORD")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_STATISTIC_RECORD_SEQ", allocationSize = 1)
public class EquipStatisticRecord implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_STATISTIC_RECORD_ID")
	private Long id;
	
	@Basic
	@Column(name = "APPROVE_DATE", length = 7)
	private Date approveDate;
	
	@Basic
	@Column(name = "CODE", length = 200)
	private String code;
	
	@Basic
	@Column(name = "NAME", length = 400)
	private String name;
	
	@Basic
	@Column(name = "NAME_TEXT", length = 400)
	private String nameText;
	
	@Basic
	@Column(name = "FROM_DATE")
	private Date fromDate;
	
	@Basic
	@Column(name = "TO_DATE", length = 7)
	private Date toDate;

	@Basic
	@Column(name = "RECORD_STATUS", length = 22)
	private Integer recordStatus;

	@ManyToOne(targetEntity = EquipPeriod.class)
	@JoinColumn(name = "EQUIP_PRIOD_ID", referencedColumnName = "EQUIP_PERIOD_ID")
	private EquipPeriod equipPriod;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	@Basic
	@Column(name = "TYPE", length = 200)
	private Integer type;
	
	@Basic
	@Column(name = "QUANTITY", length = 200)
	private String quantity;
	
	@Basic
	@Column(name = "TYPE_CUSTOMER", length = 200)
	private Integer typeCustomer;
	
	@Basic
	@Column(name = "PROMOTION_CODE", length = 200)
	private String promotionCode;
	
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	@Transient
	String fromDateStr;
	@Transient
	String toDateStr;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}

	public EquipPeriod getEquipPriod() {
		return equipPriod;
	}

	public void setEquipPriod(EquipPeriod equipPriod) {
		this.equipPriod = equipPriod;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getNameText() {
		return nameText;
	}

	public void setNameText(String nameText) {
		this.nameText = nameText;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public Integer getTypeCustomer() {
		return typeCustomer;
	}

	public void setTypeCustomer(Integer typeCustomer) {
		this.typeCustomer = typeCustomer;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	
	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}

	public String getFromDateStr() {
		if(fromDate==null){
			return "";
		}else{
			String ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy").format(this.getFromDate());
			return ddmmyyyy;
		}
	}

	public String getToDateStr() {
		if(toDate==null){
			return "";
		}else{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			toDateStr = sdf.format(this.toDate);
			return toDateStr;
		}
	}

	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	
}
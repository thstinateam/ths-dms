package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Auto generate entities - HCM standard
 * 
 * @author tamvnm
 * @since may 04,2015
 * @description entities thuoc kenh thiet bi
 */
@Entity
@Table(name = "EQUIP_SUGGEST_EVICTION_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_SUGGEST_EVICTION_DTL_SEQ", allocationSize = 1)
public class EquipSuggestEvictionDTL implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_SUGGEST_EVICTION_DTL_ID")
	private Long id;

	//bb de nghi thu hoi
	@ManyToOne(targetEntity = EquipSuggestEviction.class)
	@JoinColumn(name = "EQUIP_SUGGEST_EVICTION_ID", referencedColumnName = "EQUIP_SUGGEST_EVICTION_ID")
	private EquipSuggestEviction equipSugEviction;

	//Mien
	@Basic
	@Column(name = "REGION", length = 250)
	private String region;

	//Kenh
	@Basic
	@Column(name = "CHANNEL", length = 250)
	private String channel;

	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	@Basic
	@Column(name = "SHOP_NAME", length = 250)
	private String shopName;

	//Ma bien ban
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	//Ma bien ban
	@Basic
	@Column(name = "CUSTOMER_CODE", length = 400)
	private String customerCode;

	//Ma bien ban
	@Basic
	@Column(name = "CUSTOMER_NAME", length = 400)
	private String customerName;

	//thiet bi
	@ManyToOne(targetEntity = Equipment.class)
	@JoinColumn(name = "EQUIP_ID", referencedColumnName = "EQUIP_ID")
	private Equipment equipment;

	//Ma thiet bi
	@Basic
	@Column(name = "EQUIP_CODE", length = 50)
	private String equipCode;

	//seri thiet bi
	@Basic
	@Column(name = "SERIAL", length = 100)
	private String serial;

	//Thoi gian de nghi
	@Basic
	@Column(name = "SUGGEST_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date suggestDate;

	//Kho
	@Basic
	@Column(name = "OBJECT_TYPE", length = 20)
	private Integer objectType;

	//Ly do de nghi thu hoi
	@Basic
	@Column(name = "SUGGEST_REASON", length = 250)
	private String suggestReason;

	//Giam sat
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	//Ma giam sat
	@Basic
	@Column(name = "STAFF_CODE", length = 250)
	private String staffCode;

	//Ten giam sat
	@Basic
	@Column(name = "STAFF_NAME", length = 250)
	private String staffName;

	//Dien thoai
	@Basic
	@Column(name = "PHONE", length = 250)
	private String phone;

	//Người đúng tên mượn thiết bị
	@Basic
	@Column(name = "REPRESENTATIVE", length = 250)
	private String refresentative;

	//Quan he voi chu cua hang
	@Basic
	@Column(name = "RELATION", length = 250)
	private String relation;

	//Dien thoai vi tri dat tu 
	@Basic
	@Column(name = "MOBILE", length = 250)
	private String mobile;

	//So nha
	@Basic
	@Column(name = "HOUSE_NUMBER", length = 250)
	private String housenumber;

	//Id tinh/tp
//	@Basic
//	@Column(name = "PROVINCE_ID")
//	private Long provinceId;

	//Ten tinh/tp
	@Basic
	@Column(name = "PROVINCE_NAME", length = 250)
	private String provinceName;

	//Id quan/huyen
//	@Basic
//	@Column(name = "DISTRICT_ID")
//	private Long districtId;

	//Ten quan/huyen
	@Basic
	@Column(name = "DISTRICT_NAME", length = 250)
	private String districtName;

	//Id xa/phuong
//	@Basic
//	@Column(name = "WARD_ID")
//	private Long wardId;

	//Ten xa/phuong
	@Basic
	@Column(name = "WARD_NAME", length = 250)
	private String wardName;

	//Duong
	@Basic
	@Column(name = "STREET")
	private String street;

	//loai thiet bi
	@Basic
	@Column(name = "EQUIP_CATEGORY_ID")
	private Long equipCategoryId;

	//Ten loai thiet bi
	@Basic
	@Column(name = "EQUIP_CATEGORY_NAME", length = 250)
	private String equipCategoryName;

	// T?nh tr?ng thi?t b?. Link qua AP_PARAM v?i AP_PARAM.TYPE =
	// EQUIP_CONDITION
	@Basic
	@Column(name = "HEALTH_STATUS", length = 500)
	private String healthStatus;

	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	@Basic
	@Column(name = "DELIVERY_STATUS", length = 22)
	private Integer deliveryStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipSuggestEviction getEquipSugEviction() {
		return equipSugEviction;
	}

	public void setEquipSugEviction(EquipSuggestEviction equipSugEviction) {
		this.equipSugEviction = equipSugEviction;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public Date getSuggestDate() {
		return suggestDate;
	}

	public void setSuggestDate(Date suggestDate) {
		this.suggestDate = suggestDate;
	}

	public String getSuggestReason() {
		return suggestReason;
	}

	public void setSuggestReason(String suggestReason) {
		this.suggestReason = suggestReason;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRefresentative() {
		return refresentative;
	}

	public void setRefresentative(String refresentative) {
		this.refresentative = refresentative;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getHousenumber() {
		return housenumber;
	}

	public void setHousenumber(String housenumber) {
		this.housenumber = housenumber;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getWardName() {
		return wardName;
	}

	public void setWardName(String wardName) {
		this.wardName = wardName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Long getEquipCategoryId() {
		return equipCategoryId;
	}

	public void setEquipCategoryId(Long equipCategoryId) {
		this.equipCategoryId = equipCategoryId;
	}

	public String getEquipCategoryName() {
		return equipCategoryName;
	}

	public void setEquipCategoryName(String equipCategoryName) {
		this.equipCategoryName = equipCategoryName;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(Integer deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

}
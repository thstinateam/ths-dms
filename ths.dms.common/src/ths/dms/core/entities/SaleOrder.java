package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.SaleOrderType;

/**
* Auto generate entities - SALE_ORDER
* @author nhanlt6
* @since 15/8/2014
*/

@Entity
@Table(name = "SALE_ORDER")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SALE_ORDER_SEQ", allocationSize = 1)
public class SaleOrder implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//So tien don hang chua tinh khuyen mai
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//0: chua duyet; 1 dat duyet; 2 khong duyet
	@Basic
	@Column(name = "APPROVED", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.SaleOrderStatus"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private SaleOrderStatus approved = SaleOrderStatus.NOT_YET_APPROVE;

	//Xe giao hang
	@ManyToOne(targetEntity = Car.class)
	@JoinColumn(name = "CAR_ID", referencedColumnName = "CAR_ID")
	private Car car;

	//Id NVTT: nhan vien thu tien
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "CASHIER_ID", referencedColumnName = "STAFF_ID")
	private Staff cashier;
	
	@ManyToOne(targetEntity = Routing.class)
	@JoinColumn(name = "ROUTING_ID", referencedColumnName = "ROUTING_ID")
	private Routing routing;
	
	@Basic
	@Column(name = "SHOP_CODE", length = 100)
	private String shopCode;

	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	

	//Id khach hang
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	//ngay can giao hang
	@Basic
	@Column(name = "DELIVERY_DATE", length = 7)
	private Date deliveryDate;

	//Id NVGH
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "DELIVERY_ID", referencedColumnName = "STAFF_ID")
	private Staff delivery;

	//mo ta
	@Basic
	@Column(name = "DESCRIPTION", length = 600)
	private String description;

	//So tien khuyen mai
	@Basic
	@Column(name = "DISCOUNT", length = 22)
	private BigDecimal discount;
	
	//id don hang bi tra
	@ManyToOne(targetEntity = SaleOrder.class)
	@JoinColumn(name = "FROM_SALE_ORDER_ID", referencedColumnName = "SALE_ORDER_ID")
	private SaleOrder fromSaleOrder;

	//Ngay lap don hang
	@Basic
	@Column(name = "ORDER_DATE", length = 7)
	private Date orderDate;
	
	//Thoi gian In
	@Basic
	@Column(name = "TIME_PRINT", length = 7)
	private Date timePrint;
	
	//So lan in
	@Basic
	@Column(name = "PRINT_BATCH", length = 8)
	private Integer printBatch;	
	
	//Ma don hang
	@Basic
	@Column(name = "ORDER_NUMBER", length = 50)
	private String orderNumber;
	
	//Ma don hang tham chieu
	@Basic
	@Column(name = "REF_ORDER_NUMBER", length = 50)
	private String refOrderNumber;

	//Loai don hang: IN: pre->kh; CM: KH->Pre;SO Van->KH;CO  KH->Van
	@Basic
	@Column(name = "ORDER_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.OrderType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private OrderType orderType = OrderType.IN ;

	//do khan cua don hang
	@Basic
	@Column(name = "PRIORITY", length = 20)
	private Integer priority;
	
	@Basic
	@Column(name = "QUANTITY")
	private Long quantity;
	
	@Basic
	@Column(name = "STOCK_DATE", length = 7)
	private Date stockDate;

	//ID Ã�on d?t hÃ ng, Auto number
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SALE_ORDER_ID")
	private Long id;

	//Ma NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//Id NVBH
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	//1: don hang tren web; 2: don hang tao ra tren tablet;
	@Basic
	@Column(name = "ORDER_SOURCE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.SaleOrderSource"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private SaleOrderSource orderSource = SaleOrderSource.WEB;
	
	//So tien don hang sau khi tinh khuyen mai
	@Basic
	@Column(name = "TOTAL", length = 22)
	private BigDecimal total;

	//tong trong luong cua don hang
	@Basic
	@Column(name = "TOTAL_WEIGHT", length = 22)
	private BigDecimal totalWeight;

	//1: don hang ban nhung chua tra, 0: don hang ban da thuc hien tra lai
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.SaleOrderType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private SaleOrderType type = SaleOrderType.NOT_YET_RETURNED;

	//Ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	/*//thue suat
	@Basic
	@Column(name = "VAT", length = 22)
	private Float vat;*/
	
	//don hang trong tuyen ( 1 ) hoac ngoai tuyen ( 0 )
	@Basic
	@Column(name = "IS_VISIT_PLAN")
	private Integer isVisitPlan;
	
	//Ma user huy dung cho xoa don hang tablet: neu xoa tu dong code = AUTO_DEL
	@Basic
	@Column(name = "DESTROY_CODE", length = 50)
	private String destroyCode;
	
	//Id PO Customer
	@Basic
	@Column(name = "FROM_PO_CUSTOMER_ID", length = 20)
	private Long fromPoCustomerId;
	
	//0:chua duyet 1:da duyet don vansale
	@Basic
	@Column(name = "APPROVED_VAN", length = 2)
	private Integer approvedVan;

	//0: chua duyet; 1 dat duyet; 2 khong duyet
	@Basic
	@Column(name = "APPROVED_STEP", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.SaleOrderStep"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private SaleOrderStep approvedStep = SaleOrderStep.NOT_YET_CONFIRM;
	
	//Ngay duyet don hang
	@Basic
	@Column(name = "APPROVED_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approvedDate;
	
	// 
	@Basic
	@Column(name = "ACCOUNT_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date accountDate;
	
	//Id cua don tong
	@Basic
	@Column(name = "PARENT_CP_SALE_ORDER_ID", length = 20)
	private Long parentCP;
	
	//Co la hoa don chinh trong hoa don tong nao khong
	@Basic
	@Column(name = "IS_PRIMARY", length = 2)
	private Integer isPrimary;
	
	//Dung cho dong bo
	/*@Basic
	@Column(name = "VERSION_DATA")
	private Integer versionData;*/
	
	//So luong SKU cua don hang nay
	@Basic
	@Column(name = "TOTAL_DETAIL")
	private Long totalDetail;
	
	@ManyToOne(targetEntity = Cycle.class)
	@JoinColumn(name = "CYCLE_ID", referencedColumnName = "CYCLE_ID")
	private Cycle cycle;
	
	@Basic
	@Column(name = "IS_REWARD_KS", length = 2)
	private Integer isRewardKs;

	@Transient
	private String message;
	
	@Transient
	private String priorityStr;
	
	@Transient
	private BigDecimal saleOrderDiscount;
	
	@Transient
	private BigDecimal totalDiscount;
	
	@Transient
	private Integer isShowPrice;
	
	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public BigDecimal getSaleOrderDiscount() {
		return saleOrderDiscount;
	}

	public void setSaleOrderDiscount(BigDecimal saleOrderDiscount) {
		this.saleOrderDiscount = saleOrderDiscount;
	}

	public Long getTotalDetail() {
		return totalDetail;
	}

	public void setTotalDetail(Long totalDetail) {
		this.totalDetail = totalDetail;
	}

	public String getPriorityStr() {
		return priorityStr;
	}

	public void setPriorityStr(String priorityStr) {
		this.priorityStr = priorityStr;
	}

	public SaleOrderStep getApprovedStep() {
		return approvedStep;
	}

	public void setApprovedStep(SaleOrderStep approvedStep) {
		this.approvedStep = approvedStep;
	}

	public Long getParentCP() {
		return parentCP;
	}

	public void setParentCP(Long parentCP) {
		this.parentCP = parentCP;
	}

	public Integer getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Integer isPrimary) {
		this.isPrimary = isPrimary;
	}

/*	public Integer getVersionData() {
		return versionData;
	}

	public void setVersionData(Integer versionData) {
		this.versionData = versionData;
	}*/

	public Integer getIsVisitPlan() {
		return isVisitPlan;
	}

	public void setIsVisitPlan(Integer isVisitPlan) {
		this.isVisitPlan = isVisitPlan;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public SaleOrderStatus getApproved() {
		return approved;
	}

	public void setApproved(SaleOrderStatus approved) {
		this.approved = approved;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public Staff getCashier() {
		return cashier;
	}

	public void setCashier(Staff cashier) {
		this.cashier = cashier;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Staff getDelivery() {
		return delivery;
	}

	public void setDelivery(Staff delivery) {
		this.delivery = delivery;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getDiscount() {
		return discount == null ? BigDecimal.valueOf(0) : discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public SaleOrder getFromSaleOrder() {
		return fromSaleOrder;
	}

	public void setFromSaleOrder(SaleOrder fromSaleOrder) {
		this.fromSaleOrder = fromSaleOrder;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public SaleOrderSource getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(SaleOrderSource orderSource) {
		this.orderSource = orderSource;
	}

	public BigDecimal getTotal() {
		return total == null ? BigDecimal.valueOf(0) : total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getTotalWeight() {
		return totalWeight == null ? BigDecimal.valueOf(0) : totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}

	public SaleOrderType getType() {
		return type;
	}

	public void setType(SaleOrderType type) {
		this.type = type;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

/*	public Float getVat() {
		return vat == null ? 0f : vat;
	}

	public void setVat(Float vat) {
		this.vat = vat;
	}*/

	public String getDestroyCode() {
		return destroyCode;
	}

	public void setDestroyCode(String destroyCode) {
		this.destroyCode = destroyCode;
	}


	public Date getTimePrint() {
		return timePrint;
	}

	public void setTimePrint(Date timePrint) {
		this.timePrint = timePrint;
	}

	public Integer getPrintBatch() {
		return printBatch;
	}

	public void setPrintBatch(Integer printBatch) {
		this.printBatch = printBatch;
	}

	// ThuatTQ add migrate
	
	public String getRefOrderNumber() {
		return refOrderNumber;
	}

	public void setRefOrderNumber(String refOrderNumber) {
		this.refOrderNumber = refOrderNumber;
	}

	public Long getFromPoCustomerId() {
		return fromPoCustomerId;
	}

	public void setFromPoCustomerId(Long fromPoCustomerId) {
		this.fromPoCustomerId = fromPoCustomerId;
	}
	
	public Integer getApprovedVan() {
		return approvedVan;
	}

	public void setApprovedVan(Integer approvedVan) {
		this.approvedVan = approvedVan;
	}
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SaleOrder clone() {
		SaleOrder s = new SaleOrder();

		s.setAmount(amount);
		s.setApproved(approved);
		s.setCar(car);
		s.setCashier(cashier);
		s.setCreateDate(createDate);
		s.setCreateUser(createUser);
		s.setCustomer(customer);
		s.setDelivery(delivery);
		s.setDeliveryDate(deliveryDate);
		s.setDescription(description);
		s.setDestroyCode(destroyCode);
		s.setDiscount(discount);
		s.setFromSaleOrder(fromSaleOrder);
		//s.setIsNotMigrate(isNotMigrate);
		//s.setIsNotTrigger(isNotTrigger);
		s.setIsVisitPlan(isVisitPlan);
		s.setOrderDate(orderDate);
		s.setOrderNumber(orderNumber);
		s.setRefOrderNumber(refOrderNumber);
		s.setOrderSource(orderSource);
		s.setOrderType(orderType);
		s.setPrintBatch(printBatch);
		s.setPriority(priority);
		s.setShop(shop);
		s.setStaff(staff);
		s.setTimePrint(timePrint);
		s.setTotal(total);
		s.setTotalWeight(totalWeight);
		s.setType(type);
	//	s.setVat(vat);
		s.setMessage(message);
		return s;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Integer getIsShowPrice() {
		return isShowPrice;
	}

	public void setIsShowPrice(Integer isShowPrice) {
		this.isShowPrice = isShowPrice;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public Date getAccountDate() {
		return accountDate;
	}

	public void setAccountDate(Date accountDate) {
		this.accountDate = accountDate;
//		if (accountDate != null) {
//			this.accountDate = DateUtils.truncate(accountDate, Calendar.DATE);
//		} else {
//			this.accountDate = accountDate;
//		}
	}

	public Routing getRouting() {
		return routing;
	}

	public void setRouting(Routing routing) {
		this.routing = routing;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Date getStockDate() {
		return stockDate;
	}

	public void setStockDate(Date stockDate) {
		this.stockDate = stockDate;
	}

	public Cycle getCycle() {
		return cycle;
	}

	public void setCycle(Cycle cycle) {
		this.cycle = cycle;
	}

	public Integer getIsRewardKs() {
		return isRewardKs;
	}

	public void setIsRewardKs(Integer isRewardKs) {
		this.isRewardKs = isRewardKs;
	}
	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "CHANNEL_TYPE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "CHANNEL_TYPE_SEQ", allocationSize = 1)
public class ChannelType implements Serializable {

	private static final long serialVersionUID = 1L;
	//ma
	@Basic
	@Column(name = "CHANNEL_TYPE_CODE", length = 40)
	private String channelTypeCode;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "CHANNEL_TYPE_ID")
	private Long id;

	//ten
	@Basic
	@Column(name = "CHANNEL_TYPE_NAME", length = 750)
	private String channelTypeName;

	//duoc phep sua hay ko
	@Basic
	@Column(name = "IS_EDIT", length = 22)
	private Integer isEdit;

	//phan loai theo tung doi tuong: voi doi tuong NV 1:preSale, 2: VanSale,3:NVTT,4: NVGH, ....
	@Basic
	@Column(name = "OBJECT_TYPE", length = 2)
	private Integer objectType;

	//id cha
	@ManyToOne(targetEntity = ChannelType.class)
	@JoinColumn(name = "PARENT_CHANNEL_TYPE_ID", referencedColumnName = "CHANNEL_TYPE_ID")
	private ChannelType parentChannelType;

	//chi tieu doanh so
	@Basic
	@Column(name = "SALE_AMOUNT", length = 22)
	private BigDecimal saleAmount;

	//chi tieu SKU
	@Basic
	@Column(name = "SKU", length = 22)
	private BigDecimal sku;

	//trang thai loai DV, NV, KH
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	//1: dung cho loai don vi, 2: dung cho loai NV, 3: dung cho loai KH,4: Loai xe, 6: loai nhan vien ban hang, 7: Hieu xe, 8:Nguon goc
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ChannelTypeType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ChannelTypeType type ;
	
	//ten
	@Basic
	@Column(name = "NAME_TEXT", length = 250)
	private String nametext;
	
	public String getChannelTypeCode() {
		return channelTypeCode;
	}

	public void setChannelTypeCode(String channelTypeCode) {
		this.channelTypeCode = channelTypeCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getChannelTypeName() {
		return channelTypeName;
	}

	public void setChannelTypeName(String channelTypeName) {
		this.channelTypeName = channelTypeName;
	}

	public Integer getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public ChannelType getParentChannelType() {
		return parentChannelType;
	}

	public void setParentChannelType(ChannelType parentChannelType) {
		this.parentChannelType = parentChannelType;
	}

	public BigDecimal getSaleAmount() {
		return saleAmount==null ? BigDecimal.valueOf(0) : saleAmount;
	}

	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}

	public BigDecimal getSku() {
		return sku == null ? BigDecimal.valueOf(0) : sku;
	}

	public void setSku(BigDecimal sku) {
		this.sku = sku;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public ChannelTypeType getType() {
		return type;
	}

	public void setType(ChannelTypeType type) {
		this.type = type;
	}

	public String getNametext() {
		return nametext;
	}

	public void setNametext(String nametext) {
		this.nametext = nametext;
	}

	
}
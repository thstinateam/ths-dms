/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
/**
 * phan bo ke hoach aso (active sale Order)
 * @author vuongmq
 * @since 19/10/2015 
 */
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.AsoPlanType;

@Entity
@Table(name="ASO_PLAN")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name="SEQ_STORE", sequenceName="ASO_PLAN_SEQ", allocationSize = 1)
public class AsoPlan implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQ_STORE")
	@Column(name="ASO_PLAN_ID")
	private Long id;
	
	//routing_id
	@ManyToOne(targetEntity = Routing.class)
	@JoinColumn(name = "ROUTING_ID", referencedColumnName = "ROUTING_ID")
	private Routing routing;
	
	//object_type
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.AsoPlanType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private AsoPlanType objectType;
	
	//object Id voi tung loai aso
	@Basic
	@Column(name = "OBJECT_ID", length = 20)
	private Long objectId;
	
	// chu ky
	@ManyToOne(targetEntity = Cycle.class)
	@JoinColumn(name = "CYCLE_ID", referencedColumnName = "CYCLE_ID")
	private Cycle cycle;
	
	// shop
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name="SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	@Basic
	@Column(name="PLAN", length = 10)
    private BigDecimal plan;
	
	@Basic
	@Column(name="CREATE_USER", length = 100)
	private String createUser;

	@Basic
	@Column(name="CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
    @Column(name="UPDATE_USER", length = 100)
	private String updateUser;

	@Basic
	@Column(name="UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Routing getRouting() {
		return routing;
	}

	public void setRouting(Routing routing) {
		this.routing = routing;
	}

	public AsoPlanType getObjectType() {
		return objectType;
	}

	public void setObjectType(AsoPlanType objectType) {
		this.objectType = objectType;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public Cycle getCycle() {
		return cycle;
	}

	public void setCycle(Cycle cycle) {
		this.cycle = cycle;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public BigDecimal getPlan() {
		return plan;
	}

	public void setPlan(BigDecimal plan) {
		this.plan = plan;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}

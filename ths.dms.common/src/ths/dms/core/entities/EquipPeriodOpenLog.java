package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since July 24,2015
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_PERIOD_OPEN_LOG")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_PERIOD_OPEN_LOG_SEQ", allocationSize = 1)
public class EquipPeriodOpenLog implements Serializable {

	private static final long serialVersionUID = 1L;

	//Khoa chinh
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_PERIOD_OPEN_LOG_ID")
	private Long id;
	
	//thao tac: 1: mo; 2 dong
	@Basic
	@Column(name = "ACTION")
	private Integer action;
	
	//id ky. fk equip_period
	@Basic
	@Column(name = "EQUIP_PERIOD_ID")
	private Long equipPeriodId;

	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;


	//ngay dong, mo ky
	@Basic
	@Column(name = "LOG_DATE", length = 7)
	private Date lockDate;

	/**
	 * Khai bao GETTER/SETTER
	 * */

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Integer getAction() {
		return action;
	}


	public void setAction(Integer action) {
		this.action = action;
	}


	public Long getEquipPeriodId() {
		return equipPeriodId;
	}


	public void setEquipPeriodId(Long equipPeriodId) {
		this.equipPeriodId = equipPeriodId;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getCreateUser() {
		return createUser;
	}


	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public Date getLockDate() {
		return lockDate;
	}


	public void setLockDate(Date lockDate) {
		this.lockDate = lockDate;
	}


}
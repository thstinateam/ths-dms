/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Auto generate entities - HoChiMinh standard
 * 
 * @author hunglm16
 * @since September 01, 2015
 */
@Entity
@Table(name = "SHOP_LOCK_LOG")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SHOP_LOCK_LOG_SEQ", allocationSize = 1)
public class ShopLockLog implements Serializable {

	private static final long serialVersionUID = -4969697944202104061L;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SHOP_LOCK_LOG_ID")
	private Long id;

	//id Don vi
	@Basic
	@Column(name = "SHOP_ID")
	private Long shopId;

	//Ngay lam viec
	@Basic
	@Column(name = "WORKING_DATE")
	@Temporal(TemporalType.DATE)
	private Date workingDate;

	//0: loi, 1: Thanh cong
	@Basic
	@Column(name = "status")
	private Integer status;

	//0: Loai loi
	@Basic
	@Column(name = "ERROR_TYPE")
	private Integer errorType;

	//So luong don thanh cong
	@Basic
	@Column(name = "QUANTITY_SUCCESS")
	private Integer quantitySuccess;
	
	//Tong so luong don xu ly
	@Basic
	@Column(name = "QUANTITY")
	private Integer quantity;
	
	//Tong so luong don xu ly
	@Basic
	@Column(name = "PASSED")
	private Integer passed;
	
	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Date getWorkingDate() {
		return workingDate;
	}

	public void setWorkingDate(Date workingDate) {
		this.workingDate = workingDate;
	}

	public Integer getErrorType() {
		return errorType;
	}

	public void setErrorType(Integer errorType) {
		this.errorType = errorType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getQuantitySuccess() {
		return quantitySuccess;
	}

	public void setQuantitySuccess(Integer quantitySuccess) {
		this.quantitySuccess = quantitySuccess;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getPassed() {
		return passed;
	}

	public void setPassed(Integer passed) {
		this.passed = passed;
	}
	
}

package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "ACTION_AUDIT_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "ACTION_AUDIT_DETAIL_SEQ", allocationSize = 1)
public class ActionAuditDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "ACTION_AUDIT_DETAIL_ID")
	private Long id;

	//ngay tac dong
	@Basic
	@Column(name = "ACTION_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date actionDate;

	//id action_log tuong ung
	@ManyToOne(targetEntity = ActionAudit.class)
	@JoinColumn(name = "ACTION_AUDIT_ID", referencedColumnName = "ACTION_AUDIT_ID")
	private ActionAudit action;

	//cot thay doi
	@Basic
	@Column(name = "COLUMN_NAME", length = 50)
	private String columnName;

	//ghi chu
	@Basic
	@Column(name = "DESCRIPTION", length = 500)
	private String description;

	//gia tri moi
	@Basic
	@Column(name = "NEW_VALUE", length = 800)
	private String newValue;

	//gia tri cu
	@Basic
	@Column(name = "OLD_VALUE", length = 500)
	private String oldValue;

	//bang tac dong
	@Basic
	@Column(name = "TABLE_NAME", length = 50)
	private String tableName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public ActionAudit getAction() {
		return action;
	}

	public void setAction(ActionAudit action) {
		this.action = action;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	
}
/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 * 
 * Auto generate entities - HMC standard
 * 
 * @author hunglm16
 * @since 16/11/2015
 */
@Entity
@Table(name = "SHOP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SHOP_SEQ", allocationSize = 1)
public class Shop implements Serializable {

	private static final long serialVersionUID = 1L;
	// dia chi ghi tren hoa don
	@Basic
	@Column(name = "ADDRESS", length = 500)
	private String address;

	// ma dia ban
	@Basic
	@Column(name = "AREA_CODE", length = 40)
	private String areaCode;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	// email lien he
	@Basic
	@Column(name = "EMAIL", length = 40)
	private String email;

	// ten ngan hang
	@Basic
	@Column(name = "INVOICE_BANK_NAME", length = 250)
	private String invoiceBankName;

	// tai khoan ngan hàng
	@Basic
	@Column(name = "INVOICE_NUMBER_ACCOUNT", length = 50)
	private String invoiceNumberAccount;

	// so di dong
	@Basic
	@Column(name = "MOBIPHONE", length = 40)
	private String mobiphone;

	// id cha
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "PARENT_SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop parentShop;

	// so co dinh
	@Basic
	@Column(name = "PHONE", length = 40)
	private String phone;

	// ma NPP
	@Basic
	@Column(name = "SHOP_CODE", length = 40)
	private String shopCode;

	// id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SHOP_ID")
	private Long id;

	// ten NPP
	@Basic
	@Column(name = "SHOP_NAME", length = 250)
	private String shopName;

	@ManyToOne(targetEntity = ShopType.class)
	@JoinColumn(name = "SHOP_TYPE_ID", referencedColumnName = "SHOP_TYPE_ID")
	private ShopType type;

	@ManyToOne(targetEntity = Organization.class)
	@JoinColumn(name = "ORGANIZATION_ID", referencedColumnName = "ORGANIZATION_ID")
	private Organization organization;
	
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;

	// ma so thue
	@Basic
	@Column(name = "TAX_NUM", length = 40)
	private String taxNum;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	@ManyToOne(targetEntity = Area.class)
	@JoinColumn(name = "AREA_ID", referencedColumnName = "AREA_ID")
	private Area area;

	@Basic
	@Column(name = "BILL_TO", length = 250)
	private String billTo;

	@Basic
	@Column(name = "SHIP_TO", length = 250)
	private String shipTo;

	@Basic
	@Column(name = "CONTACT_NAME", length = 200)
	private String contactName;

	@Basic
	@Column(name = "FAX", length = 50)
	private String fax;

	@Basic
	@Column(name = "DISTANCE_ORDER", length = 10)
	private Long distanceOrder;
	
	@Basic
	@Column(name = "LAT")
	private Float lat;

	@Basic
	@Column(name = "LNG")
	private Float lng;

	@Basic
	@Column(name = "NAME_TEXT", length = 250)
	private String nameText;

	// 4 new fields added by LocTT1 - Nov1, 2013
	@Basic
	@Column(name = "SHOP_TYPE", length = 100)
	private String shopType;

	@Basic
	@Column(name = "SHOP_LOCATION", length = 100)
	private String shopLocation;

	@Basic
	@Column(name = "UNDER_SHOP", length = 2)
	private Integer underShop;

	@Basic
	@Column(name = "PRICE_TYPE", length = 20)
	private Integer priceType;

	// Kenh 1: KENH GT; 2: KENH KA; 3: KENH MT
	@Basic
	@Column(name = "SHOP_CHANNEL", length = 2)
	private Integer shopChannel;
	
	@Basic
	@Column(name = "ABBREVIATION", length = 250)
	private String abbreviation; 
	
	@Transient
	private Integer quantity;
	
	//Dinh muc han no
	@Basic
	@Column(name = "DEBIT_DATE_LIMIT", length = 10)
	private Integer debitDateLimit;

	public Integer getShopChannel() {
		return shopChannel;
	}

	public void setShopChannel(Integer shopChannel) {
		this.shopChannel = shopChannel;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public String getBillTo() {
		return billTo;
	}

	public void setBillTo(String billTo) {
		this.billTo = billTo;
	}

	public String getShipTo() {
		return shipTo;
	}

	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLng() {
		return lng;
	}

	public void setLng(Float lng) {
		this.lng = lng;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInvoiceBankName() {
		return invoiceBankName;
	}

	public void setInvoiceBankName(String invoiceBankName) {
		this.invoiceBankName = invoiceBankName;
	}

	public String getInvoiceNumberAccount() {
		return invoiceNumberAccount;
	}

	public void setInvoiceNumberAccount(String invoiceNumberAccount) {
		this.invoiceNumberAccount = invoiceNumberAccount;
	}

	public String getMobiphone() {
		return mobiphone;
	}

	public void setMobiphone(String mobiphone) {
		this.mobiphone = mobiphone;
	}

	public Shop getParentShop() {
		return parentShop;
	}

	public void setParentShop(Shop parentShop) {
		this.parentShop = parentShop;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public String getTaxNum() {
		return taxNum;
	}

	public void setTaxNum(String taxNum) {
		this.taxNum = taxNum;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getNameText() {
		return nameText;
	}

	public void setNameText(String nameText) {
		this.nameText = nameText;
	}

	public String getShopType() {
		return shopType;
	}

	public void setShopType(String shopType) {
		this.shopType = shopType;
	}

	public String getShopLocation() {
		return shopLocation;
	}

	public void setShopLocation(String shopLocation) {
		this.shopLocation = shopLocation;
	}

	public Integer getUnderShop() {
		return underShop;
	}

	public void setUnderShop(Integer underShop) {
		this.underShop = underShop;
	}

	public Integer getPriceType() {
		return priceType;
	}

	public void setPriceType(Integer priceType) {
		this.priceType = priceType;
	}

	public ShopType getType() {
		return type;
	}

	public void setType(ShopType type) {
		this.type = type;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getDistanceOrder() {
		return distanceOrder;
	}

	public void setDistanceOrder(Long distanceOrder) {
		this.distanceOrder = distanceOrder;
	}

	public Integer getDebitDateLimit() {
		return debitDateLimit;
	}

	public void setDebitDateLimit(Integer debitDateLimit) {
		this.debitDateLimit = debitDateLimit;
	}

}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ProgramType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PO_CUSTOMER_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PO_CUSTOMER_DETAIL_SEQ", allocationSize = 1)
public class PoCustomerDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	//So tien
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//So tien chiet khau
	@Basic
	@Column(name = "DISCOUNT_AMOUNT", length = 22)
	private BigDecimal discountAmount;

	//% chiet khau
	@Basic
	@Column(name = "DISCOUNT_PERCENT", length = 22)
	private Float discountPercent;

	//0: hang ban; 1 khuyen mai
	@Basic
	@Column(name = "IS_FREE_ITEM", length = 22)
	private Integer isFreeItem = 0 ;

	//Ngay dat
	@Basic
	@Column(name = "ORDER_DATE", length = 7)
	private Date orderDate;

	//Gia ban da co VAT
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal priceValue;
	
	@Basic
	@Column(name = "PRICE_NOT_VAT", length = 22)
	private BigDecimal priceNotVat;

	//Id gia ban
	@ManyToOne(targetEntity = Price.class)
	@JoinColumn(name = "PRICE_ID", referencedColumnName = "PRICE_ID")
	private Price price;
	
	@Basic
	@Column(name = "JOIN_PROGRAM_CODE", length = 50)
	private String joinProgramCode;

	//Ma san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//PROGRAM_TYPE = 0,1 thi ma la CTKM; PROGRAM_TYPE = 2 --> ma la CTTB
	@Basic
	@Column(name = "PROGRAM_CODE", length = 20)
	private String programCode;

	//0: Km tu dong, 1:KM bang tay,2: cham chung bay, 3: doi hang
	@Basic
	@Column(name = "PROGRAM_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProgramType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProgramType programType;

	//So luong
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;
	
	//PROGRAME_TYPE_CODE
	@Basic
	@Column(name = "PROGRAME_TYPE_CODE", length = 50)
	private String programeTypeCode;


	//ID chi tiet don hang, Auto number
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PO_CUSTOMER_DETAIL_ID")
	private Long id;

	
	
	//Id don hang
	@ManyToOne(targetEntity = PoCustomer.class)
	@JoinColumn(name = "PO_CUSTOMER_ID", referencedColumnName = "PO_CUSTOMER_ID")
	private PoCustomer poCustomer;

	//id npp
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//id nv
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	//Trong luong
	@Basic
	@Column(name = "TOTAL_WEIGHT", length = 22)
	private BigDecimal totalWeight;

	//Ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi tao
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	//Thue VAT
	@Basic
	@Column(name = "VAT", length = 22)
	private Float vat;
	
	@ManyToOne(targetEntity = Invoice.class)
	@JoinColumn(name = "INVOICE_ID", referencedColumnName = "INVOICE_ID")
	private Invoice invoice;

	@Basic
	@Column(name = "MAX_QUANTITY_FREE", length = 17)
	private Integer maxQuantityFree;
	
	@Basic
	@Column(name = "MAX_AMOUNT_FREE", length = 17)
	private BigDecimal maxAmountFree;
	
	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount == null ? BigDecimal.valueOf(0) : discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Float getDiscountPercent() {
		return discountPercent == null ? 0f : discountPercent;
	}

	public void setDiscountPercent(Float discountPercent) {
		this.discountPercent = discountPercent;
	}

	public Integer getIsFreeItem() {
		return isFreeItem;
	}

	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public BigDecimal getPriceValue() {
		return priceValue == null ? BigDecimal.valueOf(0) : priceValue;
	}

	public void setPriceValue(BigDecimal priceValue) {
		this.priceValue = priceValue;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public ProgramType getProgramType() {
		return programType;
	}

	public void setProgramType(ProgramType programType) {
		this.programType = programType;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PoCustomer getPoCustomer() {
		return poCustomer;
	}

	public void setPoCustomer(PoCustomer poCustomer) {
		this.poCustomer = poCustomer;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public BigDecimal getTotalWeight() {
		return totalWeight == null ? BigDecimal.valueOf(0) : totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Float getVat() {
		return vat == null ? 0f : vat;
	}

	public void setVat(Float vat) {
		this.vat = vat;
	}

	public String getJoinProgramCode() {
		return joinProgramCode;
	}

	public void setJoinProgramCode(String joinProgramCode) {
		this.joinProgramCode = joinProgramCode;
	}

	public PoCustomerDetail clone() {
		PoCustomerDetail temp = new PoCustomerDetail();
		temp.amount = amount;
		temp.createDate = createDate;
		temp.createUser = createUser;
		temp.discountAmount = discountAmount;
		temp.discountPercent = discountPercent;
		temp.isFreeItem = isFreeItem;
		temp.id = id;
		temp.orderDate = orderDate;
		temp.priceValue = priceValue;
		temp.price = price;
		temp.product = product;
		temp.programCode = programCode;
		temp.programType = programType;
		temp.programeTypeCode = programeTypeCode;
		temp.quantity = quantity;
		temp.poCustomer = poCustomer;
		temp.shop = shop;
		temp.staff = staff;
		temp.totalWeight = totalWeight;
		temp.updateDate = updateDate;
		temp.updateUser = updateUser;
		temp.vat = vat;
		temp.maxAmountFree = maxAmountFree;
		temp.maxQuantityFree = maxQuantityFree;
		temp.joinProgramCode = joinProgramCode;
		return temp;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Integer getMaxQuantityFree() {
		return maxQuantityFree;
	}

	public void setMaxQuantityFree(Integer maxQuantityFree) {
		this.maxQuantityFree = maxQuantityFree;
	}

	public BigDecimal getPriceNotVat() {
		return priceNotVat;
	}

	public void setPriceNotVat(BigDecimal priceNotVat) {
		this.priceNotVat = priceNotVat;
	}

	public BigDecimal getMaxAmountFree() {
		return maxAmountFree;
	}

	public void setMaxAmountFree(BigDecimal maxAmountFree) {
		this.maxAmountFree = maxAmountFree;
	}

	public String getProgrameTypeCode() {
		return programeTypeCode;
	}

	public void setProgrameTypeCode(String programeTypeCode) {
		this.programeTypeCode = programeTypeCode;
	}
	
	// ThuatTQ add migrate
}
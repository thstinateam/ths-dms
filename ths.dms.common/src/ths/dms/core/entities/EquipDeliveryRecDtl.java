package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_DELIVERY_REC_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_DELIVERY_REC_DTL_SEQ", allocationSize = 1)
public class EquipDeliveryRecDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	//N?i dung: 0: C?p m?i, 1: �i?u chuy?n t? KH kh�c, 2:Kh�c
	@Basic
	@Column(name = "CONTENT", length = 22)
	private String content;

	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//ID bi�n b?n giao nh?n
	@ManyToOne(targetEntity = EquipDeliveryRecord.class)
	@JoinColumn(name = "EQUIP_DELIVERY_RECORD_ID", referencedColumnName = "EQUIP_DELIVERY_RECORD_ID")
	private EquipDeliveryRecord equipDeliveryRecord;

	//ID chi ti?t b?n b?n giao nh?n
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_DELIVERY_REC_DTL_ID")
	private Long id;

	//ID thi?t b?
	@ManyToOne(targetEntity = Equipment.class)
	@JoinColumn(name = "EQUIP_ID", referencedColumnName = "EQUIP_ID")
	private Equipment equipment;

	//T?nh tr?ng thi?t b?. Link qua AP_PARAM v?i AP_PARAM.TYPE = EQUIP_CONDITION
	@Basic
	@Column(name = "HEALTH_STATUS", length = 500)
	private String healthStatus;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	@Basic
	@Column(name = "FROM_STOCK_ID", length = 20)
	private Long fromStockId;

	//Số tháng khấu hao
	@Basic
	@Column(name = "DEPRECIATION", length = 5)
	private Integer depreciation;
	
	//Giá trị TBBH
	@Basic
	@Column(name = "PRICE", length = 5)
	private BigDecimal price;
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public EquipDeliveryRecord getEquipDeliveryRecord() {
		return equipDeliveryRecord;
	}

	public void setEquipDeliveryRecord(EquipDeliveryRecord equipDeliveryRecord) {
		this.equipDeliveryRecord = equipDeliveryRecord;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getDepreciation() {
		return depreciation;
	}

	public void setDepreciation(Integer depreciation) {
		this.depreciation = depreciation;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getFromStockId() {
		return fromStockId;
	}

	public void setFromStockId(Long fromStockId) {
		this.fromStockId = fromStockId;
	}
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 * 
 * Auto generate entities - HaNoi standard
 * 
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "DISPLAY_STAFF_MAP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "DISPLAY_STAFF_MAP_SEQ", allocationSize = 1)
public class StDisplayStaffMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@ManyToOne(targetEntity = StDisplayProgram.class)
	@JoinColumn(name = "DISPLAY_PROGRAM_ID", referencedColumnName = "DISPLAY_PROGRAM_ID")
	private StDisplayProgram displayProgram;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "DISPLAY_STAFF_MAP_ID")
	private Long id;

	//so luong kh phat trien duoc
	@Basic
	@Column(name = "QUANTITY_MAX", length = 22)
	private Integer quantityMax = 0;
	
	//so luong kh phat trien duoc
	@Basic
	@Column(name = "QUANTITY_RECEIVED", length = 22)
	private Integer quantityReceived = 0;

	//id nv
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	@Basic
	@Column(name = "STATUS", length = 1)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;
	
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPLOAD_DATE")	
	private Date uploadDate;

	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATE_DATE")	
	private Date updateDate;
	
	

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "MONTH")
	private Date month;
	
	

	public StDisplayStaffMap clone() {
		StDisplayStaffMap obj = new StDisplayStaffMap();
		obj.setCreateDate(createDate);
		obj.setCreateUser(createUser);
		obj.setDisplayProgram(displayProgram);
		obj.setId(id);
		obj.setQuantityMax(quantityMax);
		obj.setQuantityReceived(quantityReceived);
		obj.setStaff(staff);
		obj.setStatus(status);
		obj.setUpdateDate(updateDate);
		obj.setUpdateUser(updateUser);
		obj.setUploadDate(uploadDate);

		return obj;
	}
	
	
	

	public Date getUploadDate() {
		return uploadDate;
	}




	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}




	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public StDisplayProgram getDisplayProgram() {
		return displayProgram;
	}

	public void setDisplayProgram(StDisplayProgram displayProgram) {
		this.displayProgram = displayProgram;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public Integer getQuantityMax() {
		return quantityMax;
	}

	public void setQuantityMax(Integer quantityMax) {
		this.quantityMax = quantityMax;
	}

	public Integer getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getMonth() {
		return month;
	}

	public void setMonth(Date month) {
		this.month = month;
	}
	
}
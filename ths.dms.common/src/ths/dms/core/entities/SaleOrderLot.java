package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Auto generate entities - SALE_ORDER_LOT
 * @author nhanlt6
 * @since 15/8/2014
 */

@Entity
@Table(name = "SALE_ORDER_LOT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "SALE_ORDER_LOT_SEQ", allocationSize = 1)
public class SaleOrderLot implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//Id chi tiet lo hàng, Auto Number
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "SALE_ORDER_LOT_ID")
	private Long id;
	
	//Id Sales_order_detail_id
	@ManyToOne(targetEntity = SaleOrderDetail.class)
	@JoinColumn(name = "SALE_ORDER_DETAIL_ID", referencedColumnName = "SALE_ORDER_DETAIL_ID")
	private SaleOrderDetail saleOrderDetail;

	//Id Sales_order_id
	@ManyToOne(targetEntity = SaleOrder.class)
	@JoinColumn(name = "SALE_ORDER_ID", referencedColumnName = "SALE_ORDER_ID")
	private SaleOrder saleOrder;

	//id npp
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//id nv
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	//id nv
	@ManyToOne(targetEntity = Warehouse.class)
	@JoinColumn(name = "WAREHOUSE_ID", referencedColumnName = "WAREHOUSE_ID")
	private Warehouse warehouse;
	
	//id nv
	@ManyToOne(targetEntity = StockTotal.class)
	@JoinColumn(name = "STOCK_TOTAL_ID", referencedColumnName = "STOCK_TOTAL_ID")
	private StockTotal stockTotal;
	
	//So lo
	@Basic
	@Column(name = "LOT", length = 6)
	private String lot;

	//Gia le da co VAT
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal priceValue;
	
	//Gia thung da co VAT
	@Basic
	@Column(name = "PACKAGE_PRICE", length = 22)
	private BigDecimal packagePrice;
	
	//Id gia ban
	@ManyToOne(targetEntity = Price.class)
	@JoinColumn(name = "PRICE_ID", referencedColumnName = "PRICE_ID")
	private Price price;

	//Id mat hang
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//So luong
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;
	
	//So luong le
	@Basic
	@Column(name = "QUANTITY_RETAIL", length = 22)
	private Integer quantityRetail;
	
	//So luong thung
	@Basic
	@Column(name = "QUANTITY_PACKAGE", length = 22)
	private Integer quantityPackage;
	
	//So tien chiet khau
	@Basic
	@Column(name = "DISCOUNT_AMOUNT", length = 22)
	private BigDecimal discountAmount;

	//% chiet khau
	@Transient
	private Float discountPercent;
	
	//Ngay het han cua lo hàng
	@Basic
	@Column(name = "EXPIRY_DATE", length = 7)
	private Date expirationDate;

	//Ngay dat hang
	@Basic
	@Column(name = "ORDER_DATE", length = 7)
	private Date orderDate;

	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Transient
	private Integer hasPromotion;
	
	@Transient
	private Integer levelPromo;
	
	public Integer getLevelPromo() {
		return levelPromo;
	}

	public void setLevelPromo(Integer levelPromo) {
		this.levelPromo = levelPromo;
	}

	public Integer getHasPromotion() {
		return hasPromotion;
	}

	public void setHasPromotion(Integer hasPromotion) {
		this.hasPromotion = hasPromotion;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public BigDecimal getPriceValue() {
		return priceValue == null ? BigDecimal.valueOf(0) : priceValue;
	}

	public void setPriceValue(BigDecimal price) {
		this.priceValue = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public SaleOrderDetail getSaleOrderDetail() {
		return saleOrderDetail;
	}

	public void setSaleOrderDetail(SaleOrderDetail saleOrderDetail) {
		this.saleOrderDetail = saleOrderDetail;
	}

	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public StockTotal getStockTotal() {
		return stockTotal;
	}

	public void setStockTotal(StockTotal stockTotal) {
		this.stockTotal = stockTotal;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Float getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Float discountPercent) {
		this.discountPercent = discountPercent;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Integer getQuantityRetail() {
		return quantityRetail;
	}

	public void setQuantityRetail(Integer quantityRetail) {
		this.quantityRetail = quantityRetail;
	}

	public Integer getQuantityPackage() {
		return quantityPackage;
	}

	public void setQuantityPackage(Integer quantityPackage) {
		this.quantityPackage = quantityPackage;
	}

	public BigDecimal getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(BigDecimal packagePrice) {
		this.packagePrice = packagePrice;
	}

	
}
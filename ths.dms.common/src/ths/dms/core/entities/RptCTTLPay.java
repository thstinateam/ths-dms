package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "RPT_CTTL_PAY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "RPT_CTTL_PAY_SEQ", allocationSize = 1)
public class RptCTTLPay implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "RPT_CTTL_PAY_ID")
	private Long id;
	
	@Basic
	@Column(name = "CREATE_DATE", length = 7)
	private Date createDate;
	
	@ManyToOne(targetEntity = PromotionProgram.class)
	@JoinColumn(name = "PROMOTION_PROGRAM_ID", referencedColumnName = "PROMOTION_PROGRAM_ID")
	private PromotionProgram promotionProgram;
	
	@Basic
	@Column(name = "PROMOTION_FROM_DATE", length = 7)
	private Date promotionFromDate;
	
	@Basic
	@Column(name = "PROMOTION_TO_DATE", length = 7)
	private Date promotionToDate;
	
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;
	
	@Basic
	@Column(name = "TOTAL_QUANTITY_PAY_PROMOTION", length = 22)
	private BigDecimal totalQuantityPayPromotion;
	
	@Basic
	@Column(name = "TOTAL_AMOUNT_PAY_PROMOTION", length = 22)
	private BigDecimal totalAmountPayPromotion;
	
	//So suat tra thuong
	@Basic
	@Column(name = "PROMOTION", length = 22)
	private Integer promotion;
	
	@Basic
	@Column(name = "IS_MIGRATE", length = 2)
	private Integer isMigrate=0;
	
	@Basic
	@Column(name = "APPROVED", length = 2)
	private Integer approved;
	
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	@ManyToOne(targetEntity = SaleOrder.class)
	@JoinColumn(name = "SALE_ORDER_ID", referencedColumnName = "SALE_ORDER_ID")
	private SaleOrder saleOrder;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public PromotionProgram getPromotionProgram() {
		return promotionProgram;
	}

	public void setPromotionProgram(PromotionProgram promotionProgram) {
		this.promotionProgram = promotionProgram;
	}

	public Date getPromotionFromDate() {
		return promotionFromDate;
	}

	public void setPromotionFromDate(Date promotionFromDate) {
		this.promotionFromDate = promotionFromDate;
	}

	public Date getPromotionToDate() {
		return promotionToDate;
	}

	public void setPromotionToDate(Date promotionToDate) {
		this.promotionToDate = promotionToDate;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public BigDecimal getTotalQuantityPayPromotion() {
		return totalQuantityPayPromotion;
	}

	public void setTotalQuantityPayPromotion(BigDecimal totalQuantityPayPromotion) {
		this.totalQuantityPayPromotion = totalQuantityPayPromotion;
	}

	public BigDecimal getTotalAmountPayPromotion() {
		return totalAmountPayPromotion;
	}

	public void setTotalAmountPayPromotion(BigDecimal totalAmountPayPromotion) {
		this.totalAmountPayPromotion = totalAmountPayPromotion;
	}

	public Integer getPromotion() {
		return promotion;
	}

	public void setPromotion(Integer promotion) {
		this.promotion = promotion;
	}

	public Integer getIsMigrate() {
		return isMigrate;
	}

	public void setIsMigrate(Integer isMigrate) {
		this.isMigrate = isMigrate;
	}

	public Integer getApproved() {
		return approved;
	}

	public void setApproved(Integer approved) {
		this.approved = approved;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

}

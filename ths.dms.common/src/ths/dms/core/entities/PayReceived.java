package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.PaymentStatus;
import ths.dms.core.entities.enumtype.PaymentType;
import ths.dms.core.entities.enumtype.ReceiptType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PAY_RECEIVED")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PAY_RECEIVED_SEQ", allocationSize = 1)
public class PayReceived implements Serializable {

	private static final long serialVersionUID = 1L;
	//tong tien thanh toan
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_DATE_SYS", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDateSys;
	
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//Id khach hang
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	//loai thanh toan: tien mat, chuyen khoan: 0 Tien mat, 1 chuyen khoan
	@Basic
	@Column(name = "PAYMENT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PaymentType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PaymentType paymentType;
	
	//id chung tu
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PAY_RECEIVED_ID")
	private Long id;

	//so chung tu
	@Basic
	@Column(name = "PAY_RECEIVED_NUMBER", length = 40)
	private String payReceivedNumber;

	//Lý do thu/chi
//	@ManyToOne(targetEntity = Reason.class)
//	@JoinColumn(name = "REASON_ID", referencedColumnName = "REASON_ID")
//	private Reason reason;

	//0: phieu thu; 1 phieu chi
	@Basic
	@Column(name = "RECEIPT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ReceiptType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ReceiptType receiptType = ReceiptType.RECEIVED;
	
	//so tien du cua phieu thu sau khi thanh toan
//	@Basic
//	@Column(name = "REMAIN", length = 22)
//	private BigDecimal remain = new BigDecimal(0);

	//ID NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	//ID NPP
	@ManyToOne(targetEntity = Bank.class)
	@JoinColumn(name = "BANK_ID", referencedColumnName = "BANK_ID")
	private Bank bank;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Basic
	@Column(name = "type", columnDefinition = "integer")
	private Integer type;
	
	//trang thai thanh toan
	@Basic
	@Column(name = "PAYMENT_STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PaymentStatus"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PaymentStatus paymentStatus = PaymentStatus.NOT_PAID_YET;
	
	@Basic
	@Column(name = "PAYER_NAME", length = 250)
	private String payerName;
	
	@Basic
	@Column(name = "PAYER_ADDRESS", length = 250)
	private String payerAddress;
	
	@Basic
	@Column(name = "PAYMENT_REASON", length = 250)
	private String paymentReason;

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPayReceivedNumber() {
		return payReceivedNumber;
	}

	public void setPayReceivedNumber(String payReceivedNumber) {
		this.payReceivedNumber = payReceivedNumber;
	}

	public ReceiptType getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(ReceiptType receiptType) {
		this.receiptType = receiptType;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCreateDateSys() {
		return createDateSys;
	}

	public void setCreateDateSys(Date createDateSys) {
		this.createDateSys = createDateSys;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPayerName() {
		return payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getPayerAddress() {
		return payerAddress;
	}

	public void setPayerAddress(String payerAddress) {
		this.payerAddress = payerAddress;
	}

	public String getPaymentReason() {
		return paymentReason;
	}

	public void setPaymentReason(String paymentReason) {
		this.paymentReason = paymentReason;
	}
}
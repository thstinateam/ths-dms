package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_EVICTION_FORM_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_EVICTION_FORM_DTL_SEQ", allocationSize = 1)
public class EquipEvictionFormDtl implements Serializable {

	private static final long serialVersionUID = 1L;

	//Dung tich tu
	@Basic
	@Column(name = "CAPACITY_FROM", length = 22)
	private BigDecimal capacityFrom;
	
	//Dung tich den
	@Basic
	@Column(name = "CAPACITY_TO", length = 22)
	private BigDecimal capacityTo;
	
	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	// loai thiet bi
	@ManyToOne(targetEntity = EquipCategory.class)
	@JoinColumn(name = "EQUIP_CATEGORY_ID", referencedColumnName = "EQUIP_CATEGORY_ID")
	private EquipCategory equipCategory;
	
	//Ten loai thiet bi
	@Basic
	@Column(name = "EQUIP_CATEGORY_NAME", length = 500)
	private String equipCategoryName;
	
	//Bien ban giao nhan
	@ManyToOne(targetEntity = EquipDeliveryRecord.class)
	@JoinColumn(name = "EQUIP_DELIVERY_RECORD_ID", referencedColumnName = "EQUIP_DELIVERY_RECORD_ID")
	private EquipDeliveryRecord equipDeliveryRecord;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_EVICTION_FORM_DTL_ID")
	private Long id;

	@ManyToOne(targetEntity = EquipEvictionForm.class)
	@JoinColumn(name = "EQUIP_EVICTION_FORM_ID", referencedColumnName = "EQUIP_EVICTION_FORM_ID")
	private EquipEvictionForm equipEvictionForm;

	// Nhom thiet bi
	@ManyToOne(targetEntity = EquipGroup.class)
	@JoinColumn(name = "EQUIP_GROUP_ID", referencedColumnName = "EQUIP_GROUP_ID")
	private EquipGroup equipGroup;
	
	//Ten ngom thiet bi
	@Basic
	@Column(name = "EQUIP_GROUP_NAME", length = 500)
	private String equipGroupName;
	
	@ManyToOne(targetEntity = Equipment.class)
	@JoinColumn(name = "EQUIP_ID", referencedColumnName = "EQUIP_ID")
	private Equipment equip;

	//T?nh tr?ng thi?t b?. Link qua AP_PARAM v?i AP_PARAM.TYPE = EQUIP_CONDITION
	@Basic
	@Column(name = "HEALTH_STATUS", length = 500)
	private String healthStatus;

	// năm s?n xu?t
	@Basic
	@Column(name = "MANUFACTURING_YEAR", length = 5)
	private Integer manufacturingYear;
	
	// m? serial
	@Basic
	@Column(name = "SERIAL", length = 100)
	private String serial;
	
	//S? l�?ng. Defaul = 1
	@Basic
	@Column(name = "TOTAL", length = 22)
	private Integer total;
	
	//Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipEvictionForm getEquipEvictionForm() {
		return equipEvictionForm;
	}

	public void setEquipEvictionForm(EquipEvictionForm equipEvictionForm) {
		this.equipEvictionForm = equipEvictionForm;
	}

	public Equipment getEquip() {
		return equip;
	}

	public void setEquip(Equipment equip) {
		this.equip = equip;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public BigDecimal getCapacityFrom() {
		return capacityFrom;
	}

	public void setCapacityFrom(BigDecimal capacityFrom) {
		this.capacityFrom = capacityFrom;
	}

	public BigDecimal getCapacityTo() {
		return capacityTo;
	}

	public void setCapacityTo(BigDecimal capacityTo) {
		this.capacityTo = capacityTo;
	}

	public EquipCategory getEquipCategory() {
		return equipCategory;
	}

	public void setEquipCategory(EquipCategory equipCategory) {
		this.equipCategory = equipCategory;
	}

	public String getEquipCategoryName() {
		return equipCategoryName;
	}

	public void setEquipCategoryName(String equipCategoryName) {
		this.equipCategoryName = equipCategoryName;
	}

	public EquipDeliveryRecord getEquipDeliveryRecord() {
		return equipDeliveryRecord;
	}

	public void setEquipDeliveryRecord(EquipDeliveryRecord equipDeliveryRecord) {
		this.equipDeliveryRecord = equipDeliveryRecord;
	}

	public EquipGroup getEquipGroup() {
		return equipGroup;
	}

	public void setEquipGroup(EquipGroup equipGroup) {
		this.equipGroup = equipGroup;
	}

	public String getEquipGroupName() {
		return equipGroupName;
	}

	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}

	public Integer getManufacturingYear() {
		return manufacturingYear;
	}

	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}
	
}
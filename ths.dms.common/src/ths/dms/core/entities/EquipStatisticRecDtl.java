package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author phut
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_STATISTIC_REC_DTL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_STATISTIC_REC_DTL_SEQ", allocationSize = 1)
public class EquipStatisticRecDtl implements Serializable {
	private static final long serialVersionUID = 1L;

	//ID chi ti?t bi�n b?n ki?m k�
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_STATISTIC_REC_DTL_ID")
	private Long id;
	
	//ID bi�n b?n ki?m k�
	@ManyToOne(targetEntity = EquipStatisticRecord.class)
	@JoinColumn(name = "EQUIP_STATISTIC_RECORD_ID", referencedColumnName = "EQUIP_STATISTIC_RECORD_ID")
	private EquipStatisticRecord equipStatisticRecord;


	//ID shop kh�ch h�ng
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	@Basic
	@Column(name = "OBJECT_STOCK_ID")
	private Long objectStockId;
	
	@Basic
	@Column(name = "OBJECT_ID")
	private Long objectId;

	//t?nh tr?ng ki?m k�: 0 :ch�a ki?m k�, 1: c?n, 2: m?t
	@Basic
	@Column(name = "STATUS", length = 22)
	private Integer status;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;
	
	@Basic
	@Column(name = "OBJECT_TYPE")
	private Integer objectType;
	
	@Basic
	@Column(name = "STATISTIC_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date statisticDate;
	
	@Basic
	@Column(name = "STATISTIC_TIME")
	private Integer statisticTime;
	
	@Basic
	@Column(name = "QUANTITY")
	private Integer quantity;
	
	@Basic
	@Column(name = "QUANTITY_ACTUALLY")
	private Integer quantityActually;
	
	@Basic
	@Column(name = "OBJECT_STOCK_TYPE")
	private Integer objectStockType;
	
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	@Basic
	@Column(name = "DATE_IN_WEEK")
	private String dateInWeek;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipStatisticRecord getEquipStatisticRecord() {
		return equipStatisticRecord;
	}

	public void setEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord) {
		this.equipStatisticRecord = equipStatisticRecord;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Long getObjectStockId() {
		return objectStockId;
	}

	public void setObjectStockId(Long objectStockId) {
		this.objectStockId = objectStockId;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public Date getStatisticDate() {
		return statisticDate;
	}

	public void setStatisticDate(Date statisticDate) {
		this.statisticDate = statisticDate;
	}

	public Integer getStatisticTime() {
		return statisticTime;
	}

	public void setStatisticTime(Integer statisticTime) {
		this.statisticTime = statisticTime;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantityActually() {
		return quantityActually;
	}

	public void setQuantityActually(Integer quantityActually) {
		this.quantityActually = quantityActually;
	}

	public Integer getObjectStockType() {
		return objectStockType;
	}

	public void setObjectStockType(Integer objectStockType) {
		this.objectStockType = objectStockType;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public String getDateInWeek() {
		return dateInWeek;
	}

	public void setDateInWeek(String dateInWeek) {
		this.dateInWeek = dateInWeek;
	}
}
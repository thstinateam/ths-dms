package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "MT_CATEGORY_DATA")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "MT_CATEGORY_DATA_SEQ", allocationSize = 1)
public class CategoryData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8884243033075673113L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "CATEGORY_DATA_ID")
	private Long id;
	
	@Basic
	@Column(name = "CATEGORY_CODE", length = 10)
	private String categoryCode;

	@Basic
	@Column(name = "CATEGORY_NAME", length = 100)
	private String categoryName;

	@ManyToOne(targetEntity = CategoryGroup.class)
	@JoinColumn(name = "CATEGORY_GROUP_ID", referencedColumnName = "CATEGORY_GROUP_ID")
	private CategoryGroup categoryGroup;

	@Transient
	private String categoryGroupCode;
	
	@Basic
	@Column(name = "REMARKS", length = 200)
	private String remarks;

	@Basic
	@Column(name = "PARENT_CODE", length = 10)
	private String parentCode;

	@Basic
	@Column(name = "FREE_FIELD1", length = 100)
	private String freeField1;

	@Basic
	@Column(name = "FREE_FIELD2", length = 100)
	private String freeField2;

	@Basic
	@Column(name = "CREATE_DATE", length = 7)
	private Date createDate;
	
	@Basic
	@Column(name = "CREATE_BY", length = 20)
	private String createBy;

	@Basic
	@Column(name = "UPDATE_DATE", length = 7)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_BY", length = 20)
	private String updateBy;

	@Basic
	@Column(name = "SYN_ACTION", length = 2)
	private Integer synAction;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public CategoryGroup getCategoryGroup() {
		return categoryGroup;
	}

	public void setCategoryGroup(CategoryGroup categoryGroup) {
		this.categoryGroup = categoryGroup;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getFreeField1() {
		return freeField1;
	}

	public void setFreeField1(String freeField1) {
		this.freeField1 = freeField1;
	}

	public String getFreeField2() {
		return freeField2;
	}

	public void setFreeField2(String freeField2) {
		this.freeField2 = freeField2;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Integer getSynAction() {
		return synAction;
	}

	public void setSynAction(Integer synAction) {
		this.synAction = synAction;
	}

	public String getCategoryGroupCode() {
		return categoryGroupCode;
	}

	public void setCategoryGroupCode(String categoryGroupCode) {
		this.categoryGroupCode = categoryGroupCode;
	}
	
	
}

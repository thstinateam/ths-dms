/**
 * 
 */
package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import ths.dms.core.entities.enumtype.RequestAPI;

/**
 * The Class REQUEST_LOG.
 * 
 * @author HuyTQ
 */
@Entity
@Table(name = "REQUEST_LOG")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "REQUEST_LOG_SEQ", allocationSize = 1)
public class RequestLog implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5857917436005548826L;

	/** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
    private Integer id;
    
    /** The user. */
    @Basic
    @Column(name = "USER_NAME", nullable = false) 
    private String userName;
    
    /** The api. */
    @Basic
    @Column(name = "API", nullable = false)    
    @Enumerated(EnumType.STRING)
    private RequestAPI api;
    
    /** The ip. */
    @Basic
    @Column(name = "IP", nullable = false) 
    private String ip;
    
    /** The request time. */
    @Basic
    @Column(name = "REQUEST_TIME", columnDefinition = "timestamp(9) default systimestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestTime;	
    
    /** The client. */
    @Basic
    @Column(name = "CLIENT", nullable = false)    
    private String client = "WEB";

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public RequestAPI getApi() {
		return api;
	}

	public void setApi(RequestAPI api) {
		this.api = api;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}
        
}

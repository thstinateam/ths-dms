package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "INCENTIVE_PROGRAM_LEVEL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "INCENTIVE_PROGRAM_LEVEL_SEQ", allocationSize = 1)
public class IncentiveProgramLevel implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Basic
	@Column(name = "FREE_AMOUNT", length = 22)
	private BigDecimal freeAmount;

	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "FREE_PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product freeProduct;

	@Basic
	@Column(name = "FREE_QUANTITY")
	private Integer freeQuantity;

	@ManyToOne(targetEntity = IncentiveProgram.class)
	@JoinColumn(name = "INCENTIVE_PROGRAM_ID", referencedColumnName = "INCENTIVE_PROGRAM_ID")
	private IncentiveProgram incentiveProgram;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "INCENTIVE_PROGRAM_LEVEL_ID")
	private Long id;

	@Basic
	@Column(name = "LEVEL_CODE", length = 100)
	private String levelCode;

	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.WAITING ;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public BigDecimal getFreeAmount() {
		return freeAmount;
	}

	public void setFreeAmount(BigDecimal freeAmount) {
		this.freeAmount = freeAmount;
	}

	public Product getFreeProduct() {
		return freeProduct;
	}

	public void setFreeProduct(Product freeProduct) {
		this.freeProduct = freeProduct;
	}

	public Integer getFreeQuantity() {
		return freeQuantity;
	}

	public void setFreeQuantity(Integer freeQuantity) {
		this.freeQuantity = freeQuantity;
	}

	public IncentiveProgram getIncentiveProgram() {
		return incentiveProgram;
	}

	public void setIncentiveProgram(IncentiveProgram incentiveProgram) {
		this.incentiveProgram = incentiveProgram;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLevelCode() {
		return levelCode;
	}

	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	

	public IncentiveProgramLevel clone() {
		IncentiveProgramLevel obj = new IncentiveProgramLevel();

		obj.setAmount(amount);
		obj.setCreateDate(createDate);
		obj.setCreateUser(createUser);
		obj.setFreeAmount(freeAmount);
		obj.setFreeProduct(freeProduct);
		obj.setFreeQuantity(freeQuantity);
		obj.setIncentiveProgram(incentiveProgram);
		obj.setId(id);
		obj.setLevelCode(levelCode);
		obj.setStatus(status);
		obj.setUpdateDate(updateDate);
		obj.setUpdateUser(updateUser);
	

		return obj;
	}
}
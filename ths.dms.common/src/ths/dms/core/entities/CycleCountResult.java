package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "CYCLE_COUNT_RESULT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "CYCLE_COUNT_RESULT_SEQ", allocationSize = 1)
public class CycleCountResult implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//Cycle_count_id
	@ManyToOne(targetEntity = CycleCount.class)
	@JoinColumn(name = "CYCLE_COUNT_ID", referencedColumnName = "CYCLE_COUNT_ID")
	private CycleCount cycleCount;

	//Cycle_count_map_id
	@ManyToOne(targetEntity = CycleCountMapProduct.class)
	@JoinColumn(name = "CYCLE_COUNT_MAP_PRODUCT_ID", referencedColumnName = "CYCLE_COUNT_MAP_PRODUCT_ID")
	private CycleCountMapProduct cycleCountMapProduct;

	//ID ket qua kiem ke, Auto number
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "CYCLE_COUNT_RESULT_ID")
	private Long id;

	//Lo hang
	@Basic
	@Column(name = "LOT", length = 6)
	private String lot;

	//ID hang hoa
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//So luong chot khi bat dau kiem kho
	@Basic
	@Column(name = "QUANTITY_BEFORE_COUNT", length = 22)
	private Integer quantityBeforeCount;

	//So luong kiem ke thuc te
	@Basic
	@Column(name = "QUANTITY_COUNTED", length = 22)
	private Integer quantityCounted;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Basic
	@Column(name = "COUNT_DATE")
	private Date countDate;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public CycleCount getCycleCount() {
		return cycleCount;
	}

	public void setCycleCount(CycleCount cycleCount) {
		this.cycleCount = cycleCount;
	}

	public CycleCountMapProduct getCycleCountMapProduct() {
		return cycleCountMapProduct;
	}

	public void setCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct) {
		this.cycleCountMapProduct = cycleCountMapProduct;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantityBeforeCount() {
		if (quantityBeforeCount == null) {
			quantityBeforeCount = 0;
		}
		return quantityBeforeCount;
	}

	public void setQuantityBeforeCount(Integer quantityBeforeCount) {
		this.quantityBeforeCount = quantityBeforeCount;
	}

	public Integer getQuantityCounted() {
		if (quantityCounted == null) {
			quantityCounted = 0;
		}
		return quantityCounted;
	}

	public void setQuantityCounted(Integer quantityCounted) {
		this.quantityCounted = quantityCounted;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getCountDate() {
		return countDate;
	}

	public void setCountDate(Date countDate) {
		this.countDate = countDate;
	}

}
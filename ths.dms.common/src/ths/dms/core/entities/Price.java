package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
* Auto generate entities - PRICE
* @author hunglm16
* @since August 30,2014
*/

@Entity
@Table(name = "PRICE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PRICE_SEQ", allocationSize = 1)
public class Price implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//id gia
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PRICE_ID")
	private Long id;
	
	//Ma san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;
	
	//Gia chua VAT
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal price;
	
	//Gia chua vat
	@Basic
	@Column(name = "PRICE_NOT_VAT", length = 22)
	private BigDecimal priceNotVat;
	
	//Gia thung chua VAT
	@Basic
	@Column(name = "PACKAGE_PRICE", length = 22)
	private BigDecimal packagePrice;
	
	//Gia thung chua vat
	@Basic
	@Column(name = "PACKAGE_PRICE_NOT_VAT", length = 22)
	private BigDecimal packagePriceNotVat;
	
	//Hieu luc tu ngay
	@Basic
	@Column(name = "FROM_DATE", length = 7)
	private Date fromDate;
	
	//Hieu luc den ngay
	@Basic
	@Column(name = "TO_DATE", length = 7)
	private Date toDate;
	
	//1: hoat dong, 0: ngung
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	//Ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//Ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	//Loai thue
	@Basic
	@Column(name = "VAT")
	private Float vat;
	
	//Id Nha Phan Phoi
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	

	//Id loai DV - tham chieu sang bang shop_type
	@ManyToOne(targetEntity = ShopType.class)
	@JoinColumn(name = "SHOP_TYPE_ID", referencedColumnName = "SHOP_TYPE_ID")
	private ShopType shopType;
	
/*	//Id Nha Phan Phoi
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID_TEMP", referencedColumnName = "SHOP_ID")
	private Shop shopTemp;
	
	public Shop getShopTemp() {
		return shopTemp;
	}

	public void setShopTemp(Shop shopTemp) {
		this.shopTemp = shopTemp;
	}*/

	/*//Kenh NPP
	@Basic
	@Column(name = "SHOP_CHANNEL", length = 2)
	private Integer shopChannel;*/
	
	//Id Khach Hang
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;
	
	//Id loai KH - tham chieu sang bang channel_type
	@ManyToOne(targetEntity = ChannelType.class)
	@JoinColumn(name = "CUSTOMER_TYPE_ID", referencedColumnName = "CHANNEL_TYPE_ID")
	private ChannelType customerTypeId;

	/*@Basic
	@Column(name = "SYN_ACTION", length = 2)
	private Integer synAction;
	*/
	/*@Basic
	@Column(name = "IS_NOT_MIGRATE", length = 8)
	private Integer isNotMigrate;	
*/
	/*@Basic
	@Column(name = "IS_NOT_TRIGGER", length = 8)
	private Integer isNotTrigger;
	*/
/*	public Integer getSynAction() {
		return synAction;
	}

	public void setSynAction(Integer synAction) {
		this.synAction = synAction;
	}
*/
/*	public Integer getIsNotMigrate() {
		return isNotMigrate;
	}

	public void setIsNotMigrate(Integer isNotMigrate) {
		this.isNotMigrate = isNotMigrate;
	}
*/
	/*public Integer getIsNotTrigger() {
		return isNotTrigger;
	}

	public void setIsNotTrigger(Integer isNotTrigger) {
		this.isNotTrigger = isNotTrigger;
	}*/

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

/*	public Integer getShopChannel() {
		return shopChannel;
	}

	public void setShopChannel(Integer shopChannel) {
		this.shopChannel = shopChannel;
	}*/

	public ChannelType getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(ChannelType customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public BigDecimal getPrice() {
		return price == null ? BigDecimal.valueOf(0) : price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getPriceNotVat() {
		return priceNotVat == null ? BigDecimal.valueOf(0) : priceNotVat;
	}

	public void setPriceNotVat(BigDecimal priceNotVat) {
		this.priceNotVat = priceNotVat;
	}

	public BigDecimal getPackagePrice() {
		return packagePrice == null ? BigDecimal.valueOf(0) : packagePrice;
	}

	public void setPackagePrice(BigDecimal packagePrice) {
		this.packagePrice = packagePrice;
	}

	public BigDecimal getPackagePriceNotVat() {
		return packagePriceNotVat;
	}

	public void setPackagePriceNotVat(BigDecimal packagePriceNotVat) {
		this.packagePriceNotVat = packagePriceNotVat;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Float getVat() {
		return vat;
	}

	public void setVat(Float vat) {
		this.vat = vat;
	}

	public ShopType getShopType() {
		return shopType;
	}

	public void setShopType(ShopType shopType) {
		this.shopType = shopType;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}
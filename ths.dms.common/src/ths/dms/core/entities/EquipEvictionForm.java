package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_EVICTION_FORM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_EVICTION_FORM_SEQ", allocationSize = 1)
public class EquipEvictionForm implements Serializable {

	private static final long serialVersionUID = 1L;

	//Tr?ng th�i giao nh?n
	@Basic
	@Column(name = "ACTION_STATUS", length = 22)
	private Integer actionStatus;
	
	//Dia chi
	@Basic
	@Column(name = "ADDRESS", length = 250)
	private String address;

	//Ngay duyet bien ban
	@Basic
	@Column(name = "APPROVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approveDate;
	
	//M? bi�n b?n thu h?i, thanh l?
	@Basic
	@Column(name = "CODE", length = 400)
	private String code;

	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	//Ngay tao
	@Basic
	@Column(name = "CREATE_FORM_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createFormDate;

	//Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

//	//V? tr� thi?t b? hi?n t?i: 0: kh�ng x�c �?nh; 1: NPP; 2: KH
//	@Basic
//	@Column(name = "CURRENT_POSTION", length = 22)
//	private Integer currentPostion;

	//Ma khach hang
	@Basic
	@Column(name = "CUSTOMER_CODE", length = 400)
	private String customerCode;
	
	//Ten khach hang
	@Basic
	@Column(name = "CUSTOMER_NAME", length = 400)
	private String customerName;
		
	//Th�ng tin kh�ch h�ng
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_EVICTION_FORM_ID")
	private Long id;

	//K? l?p. FK: EQUIPMENT_PRIOD
	@ManyToOne(targetEntity = EquipPeriod.class)
	@JoinColumn(name = "EQUIP_PRIOD_ID", referencedColumnName = "EQUIP_PERIOD_ID")
	private EquipPeriod equipPriod;

	@ManyToOne(targetEntity = EquipSuggestEviction.class)
	@JoinColumn(name = "EQUIP_SUGGEST_EVICTION_ID", referencedColumnName = "EQUIP_SUGGEST_EVICTION_ID")
	private EquipSuggestEviction equipSugEviction;
	
	//Tr?ng th�i bi�n b?n
	@Basic
	@Column(name = "FORM_STATUS", length = 22)
	private Integer formStatus;

	//Ng�?i �?i di?n kh�ch h�ng
	@Basic
	@Column(name = "FROM_REPRESENTOR", length = 400)
	private String fromRepresentor;

//	//T?nh tr?ng chuy?n tr?ng th�i: 0: kh�ng chuy?n; 1: �?i chuy?n
//	@Basic
//	@Column(name = "IS_WAITING_TO_MOVE", length = 22)
//	private Integer isWaitingToMove;

//	//Kh�ch h�ng ti?p theo
//	@ManyToOne(targetEntity = Customer.class)
//	@JoinColumn(name = "NEW_CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
//	private Customer customerNew;

//	//Dia chi khach hang moi
//	@Basic
//	@Column(name = "NEW_ADDRESS", length = 250)
//	private String newAddress;
	
	//Ng�?i �?i di?n thu h?i
	@Basic
	@Column(name = "TO_REPRESENTATOR", length = 400)
	private String toRepresentator;

	//Kho thu h?i
	@Basic
	@Column(name = "TO_STOCK_ID")
	private Long toStockId;
	
	//Ten Kho thu h?i 
	@Basic
	@Column(name = "TO_STOCK_NAME", length = 250)
	private String toStockName;

	//Lo?i kho thu h?i: 1: kho VNM; 2: kho kh�ch h�ng; 3: kho NPP
	@Basic
	@Column(name = "TO_STOCK_TYPE", length = 22)
	private Integer toStockType;

	//Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;
	
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
//	@ManyToOne(targetEntity = Shop.class)
//	@JoinColumn(name = "NEW_SHOP_ID", referencedColumnName = "SHOP_ID")
//	private Shop newShop;
	
	
	//So dien thoai
	@Basic
	@Column(name = "PHONE", length = 30)
	private String phone;
	
	//Ly do
	@Basic
	@Column(name = "REASON", length = 50)
	private String reason;
	
	//ghi chu
	@Basic
	@Column(name = "NOTE", length = 500)
	private String note;

	public Integer getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(Integer actionStatus) {
		this.actionStatus = actionStatus;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipPeriod getEquipPriod() {
		return equipPriod;
	}

	public void setEquipPriod(EquipPeriod equipPriod) {
		this.equipPriod = equipPriod;
	}

	public Integer getFormStatus() {
		return formStatus;
	}

	public void setFormStatus(Integer formStatus) {
		this.formStatus = formStatus;
	}

	public String getFromRepresentor() {
		return fromRepresentor;
	}

	public void setFromRepresentor(String fromRepresentor) {
		this.fromRepresentor = fromRepresentor;
	}

	public String getToRepresentator() {
		return toRepresentator;
	}

	public void setToRepresentator(String toRepresentator) {
		this.toRepresentator = toRepresentator;
	}

	public Long getToStockId() {
		return toStockId;
	}

	public void setToStockId(Long toStockId) {
		this.toStockId = toStockId;
	}

	public String getToStockName() {
		return toStockName;
	}

	public void setToStockName(String toStockName) {
		this.toStockName = toStockName;
	}

	public Integer getToStockType() {
		return toStockType;
	}

	public void setToStockType(Integer toStockType) {
		this.toStockType = toStockType;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public EquipSuggestEviction getEquipSugEviction() {
		return equipSugEviction;
	}

	public void setEquipSugEviction(EquipSuggestEviction equipSugEviction) {
		this.equipSugEviction = equipSugEviction;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public Date getCreateFormDate() {
		return createFormDate;
	}

	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
}
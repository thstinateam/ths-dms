package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.ProductAttributeEnumType;
import ths.dms.core.entities.enumtype.ProductAttributeReqType;
import ths.dms.core.entities.enumtype.ProductAttributeSearchType;

@Entity
@Table(name = "STAFF_ATTRIBUTE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "STAFF_ATTRIBUTE_SEQ", allocationSize = 1)
public class StaffAttribute implements Serializable {

	private static final long serialVersionUID = 1L;

	//Ma thuoc tinh
	@Basic
	@Column(name = "CODE", length = 400)
	private String code;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 400)
	private String createUser;

	//Do dai du lieu
	@Basic
	@Column(name = "DATA_LENGTH", length = 22)
	private Integer dataLength;

	//Mo ta
	@Basic
	@Column(name = "DESCRIPTION", length = 400)
	private String description;

	//Thu tu hien thi
	@Basic
	@Column(name = "DISPLAY_ORDER", length = 22)
	private Integer displayOrder;

	//Gia tri lon nhat
	@Basic
	@Column(name = "MAX_VALUE", length = 22)
	private Integer maxValue;

	//Gia tri nho nhat
	@Basic
	@Column(name = "MIN_VALUE", length = 22)
	private Integer minValue;

	//Ten thuoc tinh
	@Basic
	@Column(name = "NAME", length = 200)
	private String name;

	//ID
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "STAFF_ATTRIBUTE_ID")
	private Long id;

	// trang thai: 0: ngung: 1:hoat dong
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;
	
	//Co phai kieu danh sach hien thi hay khong
	@Basic
	@Column(name = "IS_ENUMERATION", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProductAttributeEnumType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProductAttributeEnumType isEnumeration = ProductAttributeEnumType.NO;
	
	//Thuoc tinh tim kiem
	@Basic
	@Column(name = "IS_SEARCH", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProductAttributeSearchType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProductAttributeSearchType isSearch = ProductAttributeSearchType.NO;
	
	//Bat buoc
	@Basic
	@Column(name = "MANDATORY", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ProductAttributeReqType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ProductAttributeReqType mandatory = ProductAttributeReqType.NOT_REQUIRE;

	//Loai thuoc tinh
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.AttributeColumnType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private AttributeColumnType type;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 400)
	private String updateUser;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Integer getDataLength() {
		return dataLength;
	}

	public void setDataLength(Integer dataLength) {
		this.dataLength = dataLength;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}


	public Integer getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Integer maxValue) {
		this.maxValue = maxValue;
	}

	public Integer getMinValue() {
		return minValue;
	}

	public void setMinValue(Integer minValue) {
		this.minValue = minValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public ProductAttributeEnumType getIsEnumeration() {
		return isEnumeration;
	}

	public void setIsEnumeration(ProductAttributeEnumType isEnumeration) {
		this.isEnumeration = isEnumeration;
	}

	public ProductAttributeSearchType getIsSearch() {
		return isSearch;
	}

	public void setIsSearch(ProductAttributeSearchType isSearch) {
		this.isSearch = isSearch;
	}

	public ProductAttributeReqType getMandatory() {
		return mandatory;
	}

	public void setMandatory(ProductAttributeReqType mandatory) {
		this.mandatory = mandatory;
	}

	public AttributeColumnType getType() {
		return type;
	}

	public void setType(AttributeColumnType type) {
		this.type = type;
	}

}
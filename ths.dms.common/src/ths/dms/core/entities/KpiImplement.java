package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="KPI_IMPLEMENT")
public class KpiImplement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5659204778904442315L;
	

	@Id
	@SequenceGenerator(name="KPI_IMPLEMENT_ID_GENERATOR", sequenceName="KPI_IMPLEMENT_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="KPI_IMPLEMENT_ID_GENERATOR")
	@Column(name="KPI_IMPLEMENT_ID")
	private Long id;
	
	@ManyToOne(targetEntity = Kpi.class)
	@JoinColumn(name = "KPI_ID", referencedColumnName = "KPI_ID")
	private Kpi kpi;
	
	@ManyToOne(targetEntity = Cycle.class)
	@JoinColumn(name = "CYCLE_ID", referencedColumnName = "CYCLE_ID")
	private Cycle cycle;
	
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	@ManyToOne
	@JoinColumn(name="SHOP_ID")
	private Shop shop;

	@Column(name="PLAN")
    private BigDecimal plan;
	
	@Column(name="CREATE_USER")
	private String createUser;

    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CREATE_DATE")
	private Date createDate;

    @Column(name="UPDATE_USER")
	private String updateUser;
	
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DATE")
	private Date updateDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Kpi getKpi() {
		return kpi;
	}

	public void setKpi(Kpi kpi) {
		this.kpi = kpi;
	}

	public Cycle getCycle() {
		return cycle;
	}

	public void setCycle(Cycle cycle) {
		this.cycle = cycle;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public BigDecimal getPlan() {
		return plan;
	}

	public void setPlan(BigDecimal plan) {
		this.plan = plan;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
    
    
}

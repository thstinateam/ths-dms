package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * @author trietptm
 * @since 05/06/2015
 * 
 */

@Entity
@Table(name = "CYCLE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "CYCLE_SEQ", allocationSize = 1)
public class Cycle implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "CYCLE_ID")
	private Long id;
	
	@Basic
	@Column(name = "CYCLE_CODE", length = 100)
	private String cycleCode;
	
	@Basic
	@Column(name = "CYCLE_NAME", length = 200)
	private String cycleName;

	//ngay bat dau chu ky
	@Basic
	@Column(name = "YEAR", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date year;
	
	//so chu ky trong nam
	@Basic
	@Column(name = "NUM", length = 2)
	private Integer num;

	//ngay bat dau chu ky
	@Basic
	@Column(name = "BEGIN_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date beginDate;
	
	//ngay ket thuc chu ky
	@Basic
	@Column(name = "END_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	
	//status chu ky: tinh trang chu ky: (1: hoat dong; 2: tam ngung; 3: huy)
	@Basic
	@Column(name = "STATUS", length = 2)
	private Integer status;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getYear() {
		return year;
	}

	public void setYear(Date year) {
		this.year = year;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getCycleCode() {
		return cycleCode;
	}

	public void setCycleCode(String cycleCode) {
		this.cycleCode = cycleCode;
	}

	public String getCycleName() {
		return cycleName;
	}

	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
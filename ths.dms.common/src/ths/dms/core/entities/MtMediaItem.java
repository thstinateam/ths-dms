package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "MT_MEDIA_ITEM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "MT_MEDIA_ITEM_SEQ", allocationSize = 1)
public class MtMediaItem implements Serializable {

	private static final long serialVersionUID = 1L;
	//null
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//null
	@Basic
	@Column(name = "CREATE_USER", length = 2000)
	private String createUser;

	//null
	@Basic
	@Column(name = "DESCRIPTION", length = 2000)
	private String description;

	//null
	@Basic
	@Column(name = "FILE_SIZE", length = 22)
	private BigDecimal fileSize;

	//null
	@Basic
	@Column(name = "HEIGHT", length = 22)
	private BigDecimal height;

	//1: hinh anh, 2: video
	@Basic
	@Column(name = "MEDIA_TYPE", length = 22)
	private Integer mediaType;

	//ID Album
	@ManyToOne(targetEntity = MtMediaAlbum.class)
	@JoinColumn(name = "MT_MEDIA_ALBUM_ID", referencedColumnName = "MT_MEDIA_ALBUM_ID")
	private MtMediaAlbum mtMediaAlbumId;

	//null
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "MT_MEDIA_ITEM_ID")
	private Long id;

	//null
	@Basic
	@Column(name = "OBJECT_ID", length = 22)
	private Long objectId;

	//null
	@Basic
	@Column(name = "OBJECT_TYPE", length = 22)
	private BigDecimal objectType;

	//Id cua hang
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shopId;

	//null
	@Basic
	@Column(name = "STATUS", length = 22)
	private BigDecimal status;

	//null
	@Basic
	@Column(name = "THUMB_URL", length = 1000)
	private String thumbUrl;

	//null
	@Basic
	@Column(name = "TITLE", length = 1000)
	private String title;

	//null
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//null
	@Basic
	@Column(name = "UPDATE_USER", length = 2000)
	private String updateUser;

	//null
	@Basic
	@Column(name = "URL", length = 1000)
	private String url;

	//null
	@Basic
	@Column(name = "WIDTH", length = 22)
	private BigDecimal width;
	
	@Transient
	String createDateStr;
	
	public String getCreateDateStr() {
		if(this.getCreateDate() !=  null) {
			String ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy").format(this.getCreateDate());
			String hhmm = new SimpleDateFormat("HH:mm").format(this.getCreateDate());
			return ddmmyyyy +"-"+hhmm;
		}
		return "";
	}
	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getFileSize() {
		return fileSize;
	}

	public void setFileSize(BigDecimal fileSize) {
		this.fileSize = fileSize;
	}

	public BigDecimal getHeight() {
		return height;
	}

	public void setHeight(BigDecimal height) {
		this.height = height;
	}

	public Integer getMediaType() {
		return mediaType;
	}

	public void setMediaType(Integer mediaType) {
		this.mediaType = mediaType;
	}

	public MtMediaAlbum getMtMediaAlbumId() {
		return mtMediaAlbumId;
	}

	public void setMtMediaAlbumId(MtMediaAlbum mtMediaAlbumId) {
		this.mtMediaAlbumId = mtMediaAlbumId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public BigDecimal getObjectType() {
		return objectType;
	}

	public void setObjectType(BigDecimal objectType) {
		this.objectType = objectType;
	}

	public Shop getShopId() {
		return shopId;
	}

	public void setShopId(Shop shopId) {
		this.shopId = shopId;
	}

	public BigDecimal getStatus() {
		return status;
	}

	public void setStatus(BigDecimal status) {
		this.status = status;
	}

	public String getThumbUrl() {
		return thumbUrl;
	}

	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public BigDecimal getWidth() {
		return width;
	}

	public void setWidth(BigDecimal width) {
		this.width = width;
	}
	

}
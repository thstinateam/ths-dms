package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PRODUCT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PRODUCT_SEQ", allocationSize = 1)
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;
	//ma vach cua san pham
	@Basic
	@Column(name = "BARCODE", length = 50)
	private String barcode;

	//nhan hieu sp
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "BRAND_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo brand;

	//ma nganh hang
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "CAT_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo cat;
	
	public String getCategoryCode() {
		if(cat == null) {
			return "";
		}
		return cat.getProductInfoCode();
	}

	//null,0: khong chek lot,1 : check lot
	@Basic
	@Column(name = "CHECK_LOT", length = 9)
	private Integer checkLot = 0;

	//hoa hong - chua dung
	@Basic
	@Column(name = "COMMISSION", length = 10)
	private Double commission;

	//gia tri quy doi tu UOM2 -> UOM1
	@Basic
	@Column(name = "CONVFACT", length = 9)
	private Integer convfact;

	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//thoi gian het han su dung cua san pham
	@Basic
	@Column(name = "EXPIRY_DATE", length = 9)
	private Integer expiryDate;

	//loai han su dung: 1:ngay, 2:thang
	@Basic
	@Column(name = "EXPIRY_TYPE", length = 9)
	private Integer expiryType;
	
	//Nhom VAT
	/*@Basic
	@Column(name = "GROUP_VAT", length = 22)
	private String groupVat;*/

	//huong thom
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "FLAVOUR_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo flavour;

	//tong trong luong
	@Basic
	@Column(name = "GROSS_WEIGHT", length = 22)
	private BigDecimal grossWeight;

	//trong luong tinh
	@Basic
	@Column(name = "NET_WEIGHT", length = 22)
	private BigDecimal netWeight;

	//loai bao bi
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "PACKING_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo packing;

	//ma cha cua SP
	@Basic
	@Column(name = "PARENT_PRODUCT_CODE", length = 50)
	private String parentProductCode;

	//ma mat hang
	@Basic
	@Column(name = "PRODUCT_CODE", length = 50)
	private String productCode;

	//id san pham
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PRODUCT_ID")
	private Long id;

	//ten mat hang
	@Basic
	@Column(name = "PRODUCT_NAME", length = 750)
	private String productName;

	//loai SP - tam thoi chua dung
	@Basic
	@Column(name = "PRODUCT_TYPE", length = 50)
	private String productType;

	//ton kho an toan - chua dung
	@Basic
	@Column(name = "SAFETY_STOCK", length = 9)
	private Integer safetyStock;

	//1: hoat dong, 0: ngung
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	//nganh hang con
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "SUB_CAT_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo subCat;
	
	//nganh hang con
	@ManyToOne(targetEntity = ProductInfo.class)
	@JoinColumn(name = "SUB_CAT_T_ID", referencedColumnName = "PRODUCT_INFO_ID")
	private ProductInfo subCatTemp;


	//don vi tinh nho nhat (hop ..)
	@Basic
	@Column(name = "UOM1", length = 60)
	private String uom1;

	//package(thung..)
	@Basic
	@Column(name = "UOM2", length = 60)
	private String uom2;

	//ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	//thuoc tinh cua mat hang - the tich - chua dung
	@Basic
	@Column(name = "VOLUMN", length = 22)
	private Double volumn;
	@Basic
	@Column(name = "NAME_TEXT", length = 250)
	private String nameText;
	
	
	/*@Basic
	@Column(name = "IS_NOT_MIGRATE", length = 22)
	private Integer isNotMigrate = 0;*/
	
	//Ten viet tat cua mat hang
	@Basic
	@Column(name = "SHORT_NAME", length = 50)
	private String shortName;
	
	@Basic
	@Column(name = "ORDER_INDEX", length = 9)
	private Integer orderIndex;
	
	public String getNameText() {
		return nameText;
	}

	public void setNameText(String nameText) {
		this.nameText = nameText;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public ProductInfo getBrand() {
		return brand;
	}

	public void setBrand(ProductInfo brand) {
		this.brand = brand;
	}

	public ProductInfo getCat() {
		return cat;
	}

	public void setCat(ProductInfo cat) {
		this.cat = cat;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Integer getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Integer expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Integer getExpiryType() {
		return expiryType;
	}

	public void setExpiryType(Integer expiryType) {
		this.expiryType = expiryType;
	}

	public ProductInfo getFlavour() {
		return flavour;
	}

	public void setFlavour(ProductInfo flavour) {
		this.flavour = flavour;
	}

	public BigDecimal getGrossWeight() {
		return grossWeight == null ? BigDecimal.valueOf(0) : grossWeight;
	}

	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}

	public BigDecimal getNetWeight() {
		return netWeight == null ? BigDecimal.valueOf(0) : netWeight;
	}

	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}

	public ProductInfo getPacking() {
		return packing;
	}

	public void setPacking(ProductInfo packing) {
		this.packing = packing;
	}

	public String getParentProductCode() {
		return parentProductCode;
	}

	public void setParentProductCode(String parentProductCode) {
		this.parentProductCode = parentProductCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	public ProductLevel getProductLevel() {
//		return productLevel;
//	}
//
//	public void setProductLevel(ProductLevel productLevel) {
//		this.productLevel = productLevel;
//	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public Integer getSafetyStock() {
		return safetyStock;
	}

	public void setSafetyStock(Integer safetyStock) {
		this.safetyStock = safetyStock;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public ProductInfo getSubCat() {
		return subCat;
	}

	public void setSubCat(ProductInfo subCat) {
		this.subCat = subCat;
	}

	public String getUom1() {
		return uom1;
	}

	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	public String getUom2() {
		return uom2;
	}

	public void setUom2(String uom2) {
		this.uom2 = uom2;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Double getVolumn() {
		return volumn;
	}

	public void setVolumn(Double volumn) {
		this.volumn = volumn;
	}

	public ProductInfo getSubCatTemp() {
		return subCatTemp;
	}

	public void setSubCatTemp(ProductInfo subCatTemp) {
		this.subCatTemp = subCatTemp;
	}

	public Product clone() {
		Product prd = new Product();
		prd.barcode = barcode;
		prd.brand = brand;
		prd.cat = cat;
		prd.checkLot = checkLot;
		prd.commission = commission;
		prd.convfact = convfact;
		prd.createDate = createDate;
		prd.createUser = createUser;
		prd.expiryDate = expiryDate;
		prd.expiryType = expiryType;
		prd.flavour = flavour;
		prd.grossWeight = grossWeight;
		prd.netWeight = netWeight;
		prd.packing = packing;
		prd.parentProductCode = parentProductCode;
		prd.productCode = productCode;
		prd.id = id;
		prd.productName = productName;
		prd.productType = productType;
		prd.safetyStock = safetyStock;
		prd.status = status;
		prd.subCat = subCat;
		prd.uom1 = uom1;
		prd.uom2 = uom2;
		prd.updateDate = updateDate;
		prd.updateUser = updateUser;
		prd.volumn = volumn;
		prd.subCatTemp = subCatTemp;
		prd.shortName = shortName;
		return prd;
	}
	/*public String getGroupVat() {
		return groupVat;
	}

	public void setGroupVat(String groupVat) {
		this.groupVat = groupVat;
	}*/

	/*public Integer getIsNotMigrate() {
		return isNotMigrate;
	}

	public void setIsNotMigrate(Integer isNotMigrate) {
		this.isNotMigrate = isNotMigrate;
	}*/

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Integer getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(Integer orderIndex) {
		this.orderIndex = orderIndex;
	}

}
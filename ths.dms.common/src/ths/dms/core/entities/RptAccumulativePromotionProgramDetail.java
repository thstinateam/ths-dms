package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "RPT_CTTL_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "RPT_CTTL_DETAIL_SEQ", allocationSize = 1)
public class RptAccumulativePromotionProgramDetail implements Serializable {
private static final long serialVersionUID = 1L;
	
	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "RPT_CTTL_DETAIL_ID")
	private Long id;
	
	@ManyToOne(targetEntity = RptAccumulativePromotionProgram.class)
	@JoinColumn(name = "RPT_CTTL_ID", referencedColumnName = "RPT_CTTL_ID")
	private RptAccumulativePromotionProgram rptAccumulativePromotionProgram;
	
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;
	
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private BigDecimal quantity;
	
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;
	
	@Basic
	@Column(name = "QUANTITY_PAY_PROMOTION", length = 22)
	private BigDecimal quantityPayPromotion;
	
	@Basic
	@Column(name = "AMOUNT_PAY_PROMOTION", length = 22)
	private BigDecimal amountPayPromotion;
	
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RptAccumulativePromotionProgram getRptAccumulativePromotionProgram() {
		return rptAccumulativePromotionProgram;
	}

	public void setRptAccumulativePromotionProgram(
			RptAccumulativePromotionProgram rptAccumulativePromotionProgram) {
		this.rptAccumulativePromotionProgram = rptAccumulativePromotionProgram;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getQuantityPayPromotion() {
		return quantityPayPromotion;
	}

	public void setQuantityPayPromotion(BigDecimal quantityPayPromotion) {
		this.quantityPayPromotion = quantityPayPromotion;
	}

	public BigDecimal getAmountPayPromotion() {
		return amountPayPromotion;
	}

	public void setAmountPayPromotion(BigDecimal amountPayPromotion) {
		this.amountPayPromotion = amountPayPromotion;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	public RptAccumulativePromotionProgramDetail clone(){
		RptAccumulativePromotionProgramDetail detail = new RptAccumulativePromotionProgramDetail();
		detail.setAmount(amount);
		detail.setAmountPayPromotion(amountPayPromotion);
		detail.setId(id);
		detail.setProduct(product);
		detail.setQuantity(quantity);
		detail.setQuantityPayPromotion(quantityPayPromotion);
		detail.setRptAccumulativePromotionProgram(rptAccumulativePromotionProgram);
		detail.setShop(shop);
		return detail;
	}
}

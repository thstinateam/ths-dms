package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApplyType;
import ths.dms.core.entities.enumtype.IncentiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "INCENTIVE_PROGRAM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "INCENTIVE_PROGRAM_SEQ", allocationSize = 1)
public class IncentiveProgram implements Serializable {

	private static final long serialVersionUID = 1L;
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	@Basic
	@Column(name = "FROM_DATE", length = 7)
	private Date fromDate;

	@Basic
	@Column(name = "INCENTIVE_PROGRAM_CODE", length = 50)
	private String incentiveProgramCode;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "INCENTIVE_PROGRAM_ID")
	private Long id;

	@Basic
	@Column(name = "INCENTIVE_PROGRAM_NAME", length = 200)
	private String incentiveProgramName;

	//tinh boi so
	@Basic
	@Column(name = "MULTIPLE", length = 1)
	private Integer multiple;

	// tinh toi uu
	@Basic
	@Column(name = "RECURSIVE", length = 1)
	private Integer recursive;

	//Doi tuong ap dung 1:NVBH, 2:NPP
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.IncentiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private IncentiveType objectType = IncentiveType.STAFF ;

	//Trang thai 2: khoi tao, 1: hoat dong, 0: ngung hoat dong
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.WAITING ;

	@Basic
	@Column(name = "TO_DATE", length = 7)
	private Date toDate;

	//Kieu ap dung cho muc 1:so luong, 2 tong tien
	@Basic
	@Column(name = "TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ApplyType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ApplyType type = ApplyType.QUANTITY ;
	
//	@Basic
//	@Column(name = "INCENTIVE_PROGRAM_TYPE", length = 50)
//	private String incentiveProgramType;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getCreateUser() {
		return createUser;
	}


	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}


	public Date getFromDate() {
		return fromDate;
	}


	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}


	public String getIncentiveProgramCode() {
		return incentiveProgramCode;
	}


	public void setIncentiveProgramCode(String incentiveProgramCode) {
		this.incentiveProgramCode = incentiveProgramCode;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getIncentiveProgramName() {
		return incentiveProgramName;
	}


	public void setIncentiveProgramName(String incentiveProgramName) {
		this.incentiveProgramName = incentiveProgramName;
	}


	public Integer getMultiple() {
		return multiple;
	}


	public void setMultiple(Integer multiple) {
		this.multiple = multiple;
	}


	public Integer getRecursive() {
		return recursive;
	}


	public void setRecursive(Integer recursive) {
		this.recursive = recursive;
	}


	public ActiveType getStatus() {
		return status;
	}


	public void setStatus(ActiveType status) {
		this.status = status;
	}


	public Date getToDate() {
		return toDate;
	}


	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


	public Date getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public String getUpdateUser() {
		return updateUser;
	}


	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}


	public IncentiveType getObjectType() {
		return objectType;
	}


	public void setObjectType(IncentiveType objectType) {
		this.objectType = objectType;
	}


	public ApplyType getType() {
		return type;
	}


	public void setType(ApplyType type) {
		this.type = type;
	}


	public IncentiveProgram clone() {
		IncentiveProgram obj = new IncentiveProgram();

		obj.setCreateDate(createDate);
		obj.setCreateUser(createUser);
		obj.setFromDate(fromDate);
		obj.setIncentiveProgramCode(incentiveProgramCode);
		obj.setId(id);
		obj.setIncentiveProgramName(incentiveProgramName);
		obj.setMultiple(multiple);
		obj.setRecursive(recursive);
		obj.setStatus(status);
		obj.setToDate(toDate);
//		obj.setType(type);
		obj.setUpdateDate(updateDate);
		obj.setUpdateUser(updateUser);
		return obj;
	}
}
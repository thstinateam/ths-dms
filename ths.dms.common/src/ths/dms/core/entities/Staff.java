package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.GenderType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm16
 * @since 16/7/2014
 * 
 */

@Entity
@Table(name = "STAFF")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "STAFF_SEQ", allocationSize = 1)
public class Staff implements Serializable {

	private static final long serialVersionUID = 1L;
	//Dia chi
	@Basic
	@Column(name = "ADDRESS", length = 500)
	private String address;

	//Ma dia ban
	@ManyToOne(targetEntity = Area.class)
	@JoinColumn(name = "AREA_ID", referencedColumnName = "AREA_ID")
	private Area area;

	//ngay sinh
	@Basic
	@Column(name = "BIRTHDAY", length = 7)
	private Date birthday;

	//noi sinh
	@Basic
	@Column(name = "BIRTHDAY_PLACE", length = 750)
	private String birthdayPlace;

	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//trinh do
	@Basic
	@Column(name = "EDUCATION", length = 50)
	private String education;

	//email nv
	@Basic
	@Column(name = "EMAIL", length = 50)
	private String email;

	//gioi tinh
	@Basic
	@Column(name = "GENDER", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.GenderType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private GenderType gender = GenderType.FEMALE;

	//so nha
	@Basic
	@Column(name = "HOUSENUMBER", length = 150)
	private String housenumber;

	//so CMT
	@Basic
	@Column(name = "IDNO", length = 40)
	private String idno;

	//ngay cap
	@Basic
	@Column(name = "IDNO_DATE", length = 7)
	private Date idnoDate;

	//noi cap
	@Basic
	@Column(name = "IDNO_PLACE", length = 750)
	private String idnoPlace;

	//imei may tinh nhan vien su dung
	@Basic
	@Column(name = "IMEI", length = 40)
	private String imei;

	//don hang cuoi cung duoc duyet
	@Basic
	@Column(name = "LAST_APPROVED_ORDER", length = 7)
	private Date lastApprovedOrder;

	//don hang cuoi cung da dat
	@Basic
	@Column(name = "LAST_ORDER", length = 7)
	private Date lastOrder;

	//di dong
	@Basic
	@Column(name = "MOBILEPHONE", length = 20)
	private String mobilephone;

	//co dinh
	@Basic
	@Column(name = "PHONE", length = 20)
	private String phone;

	//ke hoach ngay
	@Basic
	@Column(name = "PLAN", length = 22)
	private BigDecimal plan;

	//vi tri
	@Basic
	@Column(name = "POSITION", length = 50)
	private String position;

	//so lan dong bo -- lan heo dat ten
	@Basic
	@Column(name = "RESET_THRESHOLD", length = 22)
	private Integer resetThreshold = 1
 ;

	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//sku ke hoach
	@Basic
	@Column(name = "SKU", length = 22)
	private Integer sku;

	//ma nv
	@Basic
	@Column(name = "STAFF_CODE", length = 50)
	private String staffCode;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "STAFF_ID")
	private Long id;

	//ten nv
	@Basic
	@Column(name = "STAFF_NAME", length = 300)
	private String staffName;
	
	//id nv quan ly
	@Transient
	private Staff staffOwner;

	//loai nhan vien ban hang
	@Basic
	@Column(name = "SALE_TYPE_CODE", length = 50)
	private String saleTypeCode;

	//trang thai lam viec
	@Basic
	@Column(name = "WORK_STATE_CODE", length = 50)
	private String workStateCode;
	
	//loai nv
	@ManyToOne(targetEntity = StaffType.class)
	@JoinColumn(name = "STAFF_TYPE_ID", referencedColumnName = "STAFF_TYPE_ID")
	private StaffType staffType;

	//ngay bat dau lam
	@Basic
	@Column(name = "START_WORKING_DAY", length = 7)
	private Date startWorkingDay;

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;

	//duong
	@Basic
	@Column(name = "STREET", length = 300)
	private String street;

	//ngay cap nhat ke hoach ngay
	@Basic
	@Column(name = "UPDATE_PLAN", length = 7)
	private Date updatePlan;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;
	
	@Basic
	@Column(name = "SALE_GROUP", length = 100)
	private String saleGroup;
	
	@Basic
	@Column(name = "PASSWORD", length = 100)
	private String password;

	@Basic
	@Column(name = "NAME_TEXT", length = 250)
	private String nameText;
	
	/** orgId cho nhan vien**/
	@ManyToOne(targetEntity = Organization.class)
	@JoinColumn(name = "ORGANIZATION_ID", referencedColumnName = "ORGANIZATION_ID")
	private Organization organization;
	
	
	public String getNameText() {
		return nameText;
	}

	public void setNameText(String nameText) {
		this.nameText = nameText;
	}

	public String getSaleGroup() {
		return saleGroup;
	}

	public void setSaleGroup(String saleGroup) {
		this.saleGroup = saleGroup;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getBirthdayPlace() {
		return birthdayPlace;
	}

	public void setBirthdayPlace(String birthdayPlace) {
		this.birthdayPlace = birthdayPlace;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public GenderType getGender() {
		return gender;
	}

	public void setGender(GenderType gender) {
		this.gender = gender;
	}

	public String getHousenumber() {
		return housenumber;
	}

	public void setHousenumber(String housenumber) {
		this.housenumber = housenumber;
	}

	public String getIdno() {
		return idno;
	}

	public void setIdno(String idno) {
		this.idno = idno;
	}

	public Date getIdnoDate() {
		return idnoDate;
	}

	public void setIdnoDate(Date idnoDate) {
		this.idnoDate = idnoDate;
	}

	public String getIdnoPlace() {
		return idnoPlace;
	}

	public void setIdnoPlace(String idnoPlace) {
		this.idnoPlace = idnoPlace;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public Date getLastApprovedOrder() {
		return lastApprovedOrder;
	}

	public void setLastApprovedOrder(Date lastApprovedOrder) {
		this.lastApprovedOrder = lastApprovedOrder;
	}

	public Date getLastOrder() {
		return lastOrder;
	}

	public void setLastOrder(Date lastOrder) {
		this.lastOrder = lastOrder;
	}

	public String getMobilephone() {
		return mobilephone;
	}

	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BigDecimal getPlan() {
		if (plan == null) {
			plan = BigDecimal.ZERO;
		}
		return plan;
	}

	public void setPlan(BigDecimal plan) {
		this.plan = plan;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Integer getResetThreshold() {
		return resetThreshold;
	}

	public void setResetThreshold(Integer resetThreshold) {
		this.resetThreshold = resetThreshold;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Integer getSku() {
		if (sku == null) {
			sku = 0;
		}
		return sku;
	}

	public void setSku(Integer sku) {
		this.sku = sku;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public StaffType getStaffType() {
		return staffType;
	}

	public void setStaffType(StaffType staffType) {
		this.staffType = staffType;
	}

	public Date getStartWorkingDay() {
		return startWorkingDay;
	}

	public void setStartWorkingDay(Date startWorkingDay) {
		this.startWorkingDay = startWorkingDay;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Date getUpdatePlan() {
		return updatePlan;
	}

	public void setUpdatePlan(Date updatePlan) {
		this.updatePlan = updatePlan;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getSaleTypeCode() {
		return saleTypeCode;
	}

	public void setSaleTypeCode(String saleTypeCode) {
		this.saleTypeCode = saleTypeCode;
	}

	
	public String getWorkStateCode() {
		return workStateCode;
	}

	public void setWorkStateCode(String workStateCode) {
		this.workStateCode = workStateCode;
	}
	
	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Staff getStaffOwner() {
		return staffOwner;
	}

	public void setStaffOwner(Staff staffOwner) {
		this.staffOwner = staffOwner;
	}

	public Staff clone() {
		Staff obj = new Staff();

		obj.setAddress(address);
		obj.setArea(area);
		obj.setBirthday(birthday);
		obj.setBirthdayPlace(birthdayPlace);
		// obj.setCheckType(checkType);
		obj.setCreateDate(createDate);
		obj.setCreateUser(createUser);
		obj.setEducation(education);
		obj.setEmail(email);
		obj.setGender(gender);
		obj.setHousenumber(housenumber);
		obj.setIdno(idno);
		obj.setIdnoDate(idnoDate);
		obj.setIdnoPlace(idnoPlace);
		obj.setImei(imei);
		// obj.setIsDeleteData(isDeleteData);
		// obj.setIsGetDeviceInfo(isGetDeviceInfo);
		obj.setLastApprovedOrder(lastApprovedOrder);
		obj.setLastOrder(lastOrder);
		// obj.setLockStatus(lockStatus);
		obj.setMobilephone(mobilephone);
		// obj.setNameText(nameText);
		// obj.setNumLoginFail(numLoginFail);
		obj.setPassword(password);
		obj.setPhone(phone);
		obj.setPlan(plan);
		obj.setPosition(position);
		obj.setResetThreshold(resetThreshold);
		obj.setSaleGroup(saleGroup);
		obj.setSaleTypeCode(saleTypeCode);
		obj.setWorkStateCode(workStateCode);
		obj.setShop(shop);
		obj.setSku(sku);
		obj.setStaffCode(staffCode);
		obj.setId(id);
		obj.setStaffName(staffName);
		obj.setStaffType(staffType);
		obj.setStartWorkingDay(startWorkingDay);
		obj.setStatus(status);
		obj.setStreet(street);
		obj.setUpdateDate(updateDate);
		obj.setUpdatePlan(updatePlan);
		obj.setUpdateUser(updateUser);
		obj.setStaffOwner(staffOwner);
		return obj;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
}
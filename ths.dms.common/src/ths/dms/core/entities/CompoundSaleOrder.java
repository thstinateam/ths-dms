package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * 
 * @author phuongvm
 * @since 22/8/2014
 * 
 */

@Entity
@Table(name = "COMPOUND_SALE_ORDER")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "COMPOUND_SALE_ORDER_SEQ", allocationSize = 1)
public class CompoundSaleOrder implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "COMPOUND_SALE_ORDER_ID")
	private Long id;

	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//ma npp
	@Basic
	@Column(name = "SHOP_CODE", length = 10)
	private String shopCode;
	
	//id saleOrder
	@ManyToOne(targetEntity = SaleOrder.class)
	@JoinColumn(name = "PRI_SALE_ORDER_ID", referencedColumnName = "SALE_ORDER_ID")
	private SaleOrder priSaleOrder;
	
	//Ngay lap don hang chinh
	@Basic
	@Column(name = "PRI_ORDER_DATE", length = 7)
	private Date priOrderDate;
	
	//Ma don hang chinh
	@Basic
	@Column(name = "PRI_ORDER_NUMBER", length = 50)
	private String priOrderNumber;
	
	//ngay cap nhat
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public SaleOrder getPriSaleOrder() {
		return priSaleOrder;
	}

	public void setPriSaleOrder(SaleOrder priSaleOrder) {
		this.priSaleOrder = priSaleOrder;
	}

	public Date getPriOrderDate() {
		return priOrderDate;
	}

	public void setPriOrderDate(Date priOrderDate) {
		this.priOrderDate = priOrderDate;
	}

	public String getPriOrderNumber() {
		return priOrderNumber;
	}

	public void setPriOrderNumber(String priOrderNumber) {
		this.priOrderNumber = priOrderNumber;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	
}
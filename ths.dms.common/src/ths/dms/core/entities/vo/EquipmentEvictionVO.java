package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Class EquipmentEvictionVO
 * 
 *@author phuongvm
 *@since December 15,2014
 *@description 
 */
public class EquipmentEvictionVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String ngay;
	private String maPhieu;
	private String maNPP;
	private String tenNPP;
	private String maKH;
	private String tenKH;
	private String diaChi;
	private String dienThoai;
	private String trangThaiBienBan;
	private String trangThaiGiaoNhan;
	private Integer recordStatus;
	private Integer deliveryStatus;
	private String maNPPNew;
	private String maKHNew;
	private String tenKHNew;
	private String diaChiNew;
	private String soHopDong;
	private String loaiTB;
	private String tenTB;
	private String soSeri;
	private String tinhTrang;
	private Integer soLuong;
	private Integer viTri;
	private String ngDaiDienKhachHang;
	private String ngDaiDienThuHoi;
	private String maThietBi;
	private String lyDo;
	private Integer viTriHienTai;
	private String choChuyen;
	private String customerStockCode;
	private String shopCustomerStockcode;
	private String shopStockcode;
	private Integer numberFile;
	private Long customerId;
	private Date createFormDate;
	private String customerCode;
	private String note;
	
	/**Mã biên bản đề nghị thu hồi thiết bị*/
	private String equipSugEvictionCode;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNgay() {
		return ngay;
	}
	public void setNgay(String ngay) {
		this.ngay = ngay;
	}
	public String getMaPhieu() {
		return maPhieu;
	}
	public void setMaPhieu(String maPhieu) {
		this.maPhieu = maPhieu;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getTrangThaiBienBan() {
		return trangThaiBienBan;
	}
	public void setTrangThaiBienBan(String trangThaiBienBan) {
		this.trangThaiBienBan = trangThaiBienBan;
	}
	public String getTrangThaiGiaoNhan() {
		return trangThaiGiaoNhan;
	}
	public void setTrangThaiGiaoNhan(String trangThaiGiaoNhan) {
		this.trangThaiGiaoNhan = trangThaiGiaoNhan;
	}
	public String getMaNPPNew() {
		return maNPPNew;
	}
	public void setMaNPPNew(String maNPPNew) {
		this.maNPPNew = maNPPNew;
	}
	public String getMaKHNew() {
		return maKHNew;
	}
	public void setMaKHNew(String maKHNew) {
		this.maKHNew = maKHNew;
	}
	public String getNgDaiDienKhachHang() {
		return ngDaiDienKhachHang;
	}
	public void setNgDaiDienKhachHang(String ngDaiDienKhachHang) {
		this.ngDaiDienKhachHang = ngDaiDienKhachHang;
	}
	public String getNgDaiDienThuHoi() {
		return ngDaiDienThuHoi;
	}
	public void setNgDaiDienThuHoi(String ngDaiDienThuHoi) {
		this.ngDaiDienThuHoi = ngDaiDienThuHoi;
	}
	public String getMaThietBi() {
		return maThietBi;
	}
	public void setMaThietBi(String maThietBi) {
		this.maThietBi = maThietBi;
	}
	
	public String getLyDo() {
		return lyDo;
	}
	public void setLyDo(String lyDo) {
		this.lyDo = lyDo;
	}
	public Integer getViTriHienTai() {
		return viTriHienTai;
	}
	public void setViTriHienTai(Integer viTriHienTai) {
		this.viTriHienTai = viTriHienTai;
	}
	public String getChoChuyen() {
		return choChuyen;
	}
	public void setChoChuyen(String choChuyen) {
		this.choChuyen = choChuyen;
	}
	public String getCustomerStockCode() {
		return customerStockCode;
	}
	public void setCustomerStockCode(String customerStockCode) {
		this.customerStockCode = customerStockCode;
	}
	public String getShopCustomerStockcode() {
		return shopCustomerStockcode;
	}
	public void setShopCustomerStockcode(String shopCustomerStockcode) {
		this.shopCustomerStockcode = shopCustomerStockcode;
	}
	public String getShopStockcode() {
		return shopStockcode;
	}
	public void setShopStockcode(String shopStockcode) {
		this.shopStockcode = shopStockcode;
	}
	public String getDienThoai() {
		return dienThoai;
	}
	public void setDienThoai(String dienThoai) {
		this.dienThoai = dienThoai;
	}
	public String getTenKHNew() {
		return tenKHNew;
	}
	public void setTenKHNew(String tenKHNew) {
		this.tenKHNew = tenKHNew;
	}
	public String getDiaChiNew() {
		return diaChiNew;
	}
	public void setDiaChiNew(String diaChiNew) {
		this.diaChiNew = diaChiNew;
	}
	public String getSoHopDong() {
		return soHopDong;
	}
	public void setSoHopDong(String soHopDong) {
		this.soHopDong = soHopDong;
	}
	public String getLoaiTB() {
		return loaiTB;
	}
	public void setLoaiTB(String loaiTB) {
		this.loaiTB = loaiTB;
	}
	public String getTenTB() {
		return tenTB;
	}
	public void setTenTB(String tenTB) {
		this.tenTB = tenTB;
	}
	public String getSoSeri() {
		return soSeri;
	}
	public void setSoSeri(String soSeri) {
		this.soSeri = soSeri;
	}
	public String getTinhTrang() {
		return tinhTrang;
	}
	public void setTinhTrang(String tinhTrang) {
		this.tinhTrang = tinhTrang;
	}
	public Integer getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(Integer soLuong) {
		this.soLuong = soLuong;
	}
	public Integer getViTri() {
		return viTri;
	}
	public void setViTri(Integer viTri) {
		this.viTri = viTri;
	}
	public String getEquipSugEvictionCode() {
		return equipSugEvictionCode;
	}
	public void setEquipSugEvictionCode(String equipSugEvictionCode) {
		this.equipSugEvictionCode = equipSugEvictionCode;
	}
	public Integer getNumberFile() {
		return numberFile;
	}
	public void setNumberFile(Integer numberFile) {
		this.numberFile = numberFile;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Date getCreateFormDate() {
		return createFormDate;
	}
	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}
	public Integer getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}
	public Integer getDeliveryStatus() {
		return deliveryStatus;
	}
	public void setDeliveryStatus(Integer deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	
	
}

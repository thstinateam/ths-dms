package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptCustomerVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String shopName;
	private String staffName;
	private String areaName;
	private List<RptCustomerVO> lstArea = new ArrayList<RptCustomerVO>();
	private List<CustomerVO> lstCustomer;
	
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public List<CustomerVO> getLstCustomer() {
		return lstCustomer;
	}
	public void setLstCustomer(List<CustomerVO> lstCustomer) {
		this.lstCustomer = lstCustomer;
	}
	public List<RptCustomerVO> getLstArea() {
		return lstArea;
	}
	public void setLstArea(List<RptCustomerVO> lstArea) {
		this.lstArea = lstArea;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import ths.dms.core.entities.Staff;

/**
 * VO pay_detail
 * 
 * @author nhutnn
 * @since 23/03/2015
 */
public class PayDetailVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Staff cashier;

	private Long paymentDetailId;
	private String fromObjectNumber;
	private Long orderId;
	private String customerCode;
	private String deliveryCode;
	private String staffCode;
	private String cashierCode;
	private String cashierName;
	private String createDateStr;
	private BigDecimal amount;
	private String bankName;
	private BigDecimal discount;
	private BigDecimal total;

	public String getFromObjectNumber() {
		return fromObjectNumber;
	}

	public void setFromObjectNumber(String fromObjectNumber) {
		this.fromObjectNumber = fromObjectNumber;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getCashierCode() {
		return cashierCode;
	}

	public void setCashierCode(String cashierCode) {
		this.cashierCode = cashierCode;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Long getPaymentDetailId() {
		return paymentDetailId;
	}

	public void setPaymentDetailId(Long paymentDetailId) {
		this.paymentDetailId = paymentDetailId;
	}

	public String getCashierName() {
		return cashierName;
	}

	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Staff getCashier() {
		return cashier;
	}

	public void setCashier(Staff cashier) {
		this.cashier = cashier;
	}
}
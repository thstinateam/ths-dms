package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class LevelMappingVO implements Serializable {
	Long mappingId;
	Long idLevelMua;
	String groupMuaCode;
	String groupMuaName;
	String groupMuaText;
	Integer orderLevelMua;
	Integer minQuantityMua;
	BigDecimal minAmountMua;
	
	Long idLevelKM;
	String groupKMCode;
	String groupKMName;
	String groupKMText;
	Integer orderLevelKM;
	Integer maxQuantityKM;
	BigDecimal maxAmountKM;
	Float percentKM;
	
	public Long getMappingId() {
		return mappingId;
	}
	public void setMappingId(Long mappingId) {
		this.mappingId = mappingId;
	}
	public Long getIdLevelMua() {
		return idLevelMua;
	}
	public void setIdLevelMua(Long idLevelMua) {
		this.idLevelMua = idLevelMua;
	}
	public String getGroupMuaCode() {
		return groupMuaCode;
	}
	public void setGroupMuaCode(String groupMuaCode) {
		this.groupMuaCode = groupMuaCode;
	}
	public String getGroupMuaName() {
		return groupMuaName;
	}
	public void setGroupMuaName(String groupMuaName) {
		this.groupMuaName = groupMuaName;
	}
	public Integer getOrderLevelMua() {
		return orderLevelMua;
	}
	public void setOrderLevelMua(Integer orderLevelMua) {
		this.orderLevelMua = orderLevelMua;
	}
	public Integer getMinQuantityMua() {
		return minQuantityMua;
	}
	public void setMinQuantityMua(Integer minQuantityMua) {
		this.minQuantityMua = minQuantityMua;
	}
	public BigDecimal getMinAmountMua() {
		return minAmountMua;
	}
	public void setMinAmountMua(BigDecimal minAmountMua) {
		this.minAmountMua = minAmountMua;
	}
	public Long getIdLevelKM() {
		return idLevelKM;
	}
	public void setIdLevelKM(Long idLevelKM) {
		this.idLevelKM = idLevelKM;
	}
	public String getGroupKMCode() {
		return groupKMCode;
	}
	public void setGroupKMCode(String groupKMCode) {
		this.groupKMCode = groupKMCode;
	}
	public String getGroupKMName() {
		return groupKMName;
	}
	public void setGroupKMName(String groupKMName) {
		this.groupKMName = groupKMName;
	}
	public Integer getOrderLevelKM() {
		return orderLevelKM;
	}
	public void setOrderLevelKM(Integer orderLevelKM) {
		this.orderLevelKM = orderLevelKM;
	}
	public Integer getMaxQuantityKM() {
		return maxQuantityKM;
	}
	public void setMaxQuantityKM(Integer maxQuantityKM) {
		this.maxQuantityKM = maxQuantityKM;
	}
	public Float getPercentKM() {
		return percentKM;
	}
	public void setPercentKM(Float percentKM) {
		this.percentKM = percentKM;
	}
	public String getGroupMuaText() {
		return groupMuaText;
	}
	public void setGroupMuaText(String groupMuaText) {
		this.groupMuaText = groupMuaText;
	}
	public String getGroupKMText() {
		return groupKMText;
	}
	public void setGroupKMText(String groupKMText) {
		this.groupKMText = groupKMText;
	}
	public BigDecimal getMaxAmountKM() {
		return maxAmountKM;
	}
	public void setMaxAmountKM(BigDecimal maxAmountKM) {
		this.maxAmountKM = maxAmountKM;
	}
}

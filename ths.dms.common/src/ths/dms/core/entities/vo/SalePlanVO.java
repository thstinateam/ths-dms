package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class SalePlanVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long salePlanId;
	private Long staffId;
	private String staffCode;
	private String shopCode;
	private Long productId;
	private String productCode;
	private String productName;
	private String productInfoCode;
	private String productInfoName;
	private Integer month;
	private Integer year;
	private Integer staffQuantity;
	private Integer quantity;
	// private Integer staffQuantity1;
	private BigDecimal price;
	private BigDecimal amount;
	private Integer shopQuantity;
	private Integer shopQuantityAssign;
	private Integer shopLeftQuantity;
	private Long id;
	private Long cycleId;
	private Long routingId;
	private String routingCode;
	private String routingName;

	private Integer totalQuantity;
	private BigDecimal totalAmount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSalePlanId() {
		return salePlanId;
	}

	public void setSalePlanId(Long salePlanId) {
		this.salePlanId = salePlanId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getStaffQuantity() {
		/*if (staffQuantity == null) {
			staffQuantity = 0;
		}*/
		return staffQuantity;
	}

	public Integer getStaffQuantity1() {
		return staffQuantity;
	}

	public void setStaffQuantity(Integer staffQuantity) {
		this.staffQuantity = staffQuantity;
	}

	public BigDecimal getPrice() {
		return price == null ? BigDecimal.valueOf(0) : price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getShopQuantity() {
		if (shopQuantity == null) {
			shopQuantity = 0;
		}
		return shopQuantity;
	}

	public String getProductInfoCode() {
		return productInfoCode;
	}

	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}

	public String getProductInfoName() {
		return productInfoName;
	}

	public void setProductInfoName(String productInfoName) {
		this.productInfoName = productInfoName;
	}

	public void setShopQuantity(Integer shopQuantity) {
		this.shopQuantity = shopQuantity;
	}

	public Integer getShopQuantityAssign() {
		if (shopQuantityAssign == null) {
			shopQuantityAssign = 0;
		}
		return shopQuantityAssign;
	}

	public void setShopQuantityAssign(Integer shopQuantityAssign) {
		this.shopQuantityAssign = shopQuantityAssign;
	}

	public Integer getShopLeftQuantity() {
		if (shopLeftQuantity == null) {
			shopLeftQuantity = 0;
		}
		return shopLeftQuantity;
	}

	public void setShopLeftQuantity(Integer shopLeftQuantity) {
		this.shopLeftQuantity = shopLeftQuantity;
	}

	/**
	 * @return the shopCode
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * @param shopCode
	 *            the shopCode to set
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public String getRoutingCode() {
		return routingCode;
	}

	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}

	public String getRoutingName() {
		return routingName;
	}

	public void setRoutingName(String routingName) {
		this.routingName = routingName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getRoutingId() {
		return routingId;
	}

	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}

}

package ths.dms.core.entities.vo;

import java.io.Serializable;
/**
 * Quan ly thuoc tinh san pham voi nhieu gia tri
 * @author tientv11
 * @since 16/01/2015
 */
public class ProductAttributeEnumVO implements Serializable {

	
	private static final long serialVersionUID = -4931975943896321113L;
	
	private Long id;
	
	private Long productAttId;
	
	private String code;
	
	private String value;
	
	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductAttId() {
		return productAttId;
	}

	public void setProductAttId(Long productAttId) {
		this.productAttId = productAttId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;

import ths.dms.core.entities.CycleCountMapProduct;

public class CycleCountMapProductVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private CycleCountMapProduct cycleCountMapProduct;
	
	private Boolean isUpdateQuantBfrCount;

	public CycleCountMapProduct getCycleCountMapProduct() {
		return cycleCountMapProduct;
	}

	public void setCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct) {
		this.cycleCountMapProduct = cycleCountMapProduct;
	}

	public Boolean getIsUpdateQuantBfrCount() {
		return isUpdateQuantBfrCount;
	}

	public void setIsUpdateQuantBfrCount(Boolean isUpdateQuantBfrCount) {
		this.isUpdateQuantBfrCount = isUpdateQuantBfrCount;
	}
}

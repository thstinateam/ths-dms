/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/***
 * @author vuongmq
 * @date 18/08/2015
 * @description Lay danh sach ghe tham cua Staff voi Customer o Action_Log 
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Transient;

public class ActionLogCustomerVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id; //
	private Long customerId;
	private Long staffId;
	private Long objectId;
	private Integer objectType;
	private Date startTime;
	private Date endTime;

	private String startTimeHHMM;
	private String endTimeHHMM;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Long getObjectId() {
		return objectId;
	}
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public String getStartTimeHHMM() {
		if (startTime != null) {
			System.out.println(new SimpleDateFormat("HH").format(startTime)+':'+new SimpleDateFormat("mm").format(startTime));
			return new SimpleDateFormat("HH").format(startTime)+':'+new SimpleDateFormat("mm").format(startTime);
		} else {
			return "";
		}
	}
	public void setStartTimeHHMM(String startTimeHHMM) {
		this.startTimeHHMM = startTimeHHMM;
	}
	public String getEndTimeHHMM() {
		if (endTime != null) {
			System.out.println(new SimpleDateFormat("HH").format(endTime)+':'+new SimpleDateFormat("mm").format(endTime));
			return new SimpleDateFormat("HH").format(endTime)+':'+new SimpleDateFormat("mm").format(endTime);
		} else {
			return "";
		}
	}
	public void setEndTimeHHMM(String endTimeHHMM) {
		this.endTimeHHMM = endTimeHHMM;
	}
}

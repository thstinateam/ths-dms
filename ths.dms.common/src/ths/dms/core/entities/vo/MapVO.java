package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;

public class MapVO<K, V> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	TreeMap<String, V> map = new TreeMap<String, V>();
	ArrayList<K> keys = new ArrayList<K>();

	public V put(K key, V value) {
		keys.add(key);
		return map.put(key.toString(), value);
	}

	public ArrayList<K> keyList() {
		return keys;
	}
	
	public Set<String> keySet() {
		return map.keySet();
	}
	
	public V remove(Object key) {
		keys.remove(key);
		return map.remove(key.toString());
	}

	public V get(Object key) {
		return map.get(key.toString());
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Class EquipmentRepairFormVO
 * 
 * @author hunglm16
 * @since December 19,2014
 * @description Them thuoc tinh co trong bang EquipmentRepairForm va cac bang lien quan
 */
public class EquipmentRepairFormDtlVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	
	private String hangMuc;
	private String tenHangMuc; //vuongmq; 28/05/2015; lay de in phieu
	private String ngayBatDauBaoHanh; //
	private Long baoHanh;
	private String ngayHetHanBaoHanh; //
	private String donGiaVatTuDinhMuc; //
	private String donGiaNhanCongDinhMuc; //
	private Integer soLuong;
	private Integer lanSua;
	
	private BigDecimal donGiaVatTu;
	private BigDecimal donGiaNhanCong;
	private BigDecimal tongTien;
	private BigDecimal tongTienVatTu;
	private String description; // 01/07/2015; them ghi chu cho nhap vuot dinh muc
	private EquipItemVO dataView;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getHangMuc() {
		return hangMuc;
	}
	public void setHangMuc(String hangMuc) {
		this.hangMuc = hangMuc;
	}
	public String getTenHangMuc() {
		return tenHangMuc;
	}
	public void setTenHangMuc(String tenHangMuc) {
		this.tenHangMuc = tenHangMuc;
	}
	public Long getBaoHanh() {
		return baoHanh;
	}
	public void setBaoHanh(Long baoHanh) {
		this.baoHanh = baoHanh;
	}
	public Integer getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(Integer soLuong) {
		this.soLuong = soLuong;
	}
	public Integer getLanSua() {
		return lanSua;
	}
	public void setLanSua(Integer lanSua) {
		this.lanSua = lanSua;
	}
	public BigDecimal getDonGiaVatTu() {
		return donGiaVatTu;
	}
	public void setDonGiaVatTu(BigDecimal donGiaVatTu) {
		this.donGiaVatTu = donGiaVatTu;
	}
	public BigDecimal getDonGiaNhanCong() {
		return donGiaNhanCong;
	}
	public void setDonGiaNhanCong(BigDecimal donGiaNhanCong) {
		this.donGiaNhanCong = donGiaNhanCong;
	}
	public BigDecimal getTongTien() {
		return tongTien;
	}
	public void setTongTien(BigDecimal tongTien) {
		this.tongTien = tongTien;
	}
	public String getNgayBatDauBaoHanh() {
		return ngayBatDauBaoHanh;
	}
	public void setNgayBatDauBaoHanh(String ngayBatDauBaoHanh) {
		this.ngayBatDauBaoHanh = ngayBatDauBaoHanh;
	}
	public String getNgayHetHanBaoHanh() {
		return ngayHetHanBaoHanh;
	}
	public void setNgayHetHanBaoHanh(String ngayHetHanBaoHanh) {
		this.ngayHetHanBaoHanh = ngayHetHanBaoHanh;
	}
	public String getDonGiaVatTuDinhMuc() {
		return donGiaVatTuDinhMuc;
	}
	public void setDonGiaVatTuDinhMuc(String donGiaVatTuDinhMuc) {
		this.donGiaVatTuDinhMuc = donGiaVatTuDinhMuc;
	}
	public String getDonGiaNhanCongDinhMuc() {
		return donGiaNhanCongDinhMuc;
	}
	public void setDonGiaNhanCongDinhMuc(String donGiaNhanCongDinhMuc) {
		this.donGiaNhanCongDinhMuc = donGiaNhanCongDinhMuc;
	}
	public EquipItemVO getDataView() {
		return dataView;
	}
	public void setDataView(EquipItemVO dataView) {
		this.dataView = dataView;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getTongTienVatTu() {
		return tongTienVatTu;
	}
	public void setTongTienVatTu(BigDecimal tongTienVatTu) {
		this.tongTienVatTu = tongTienVatTu;
	}
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

public class DynamicStaffKpiSaveVO implements Serializable  {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long shopId;
	private Long staffId;
	private String shopCode;
	private String shopName;
	private String staffCode;
	private String staffName;
	private String areaCode;
	private Long cycleId;
	private String cycleText;
	
	private Long kiId;
	private Long kpiId;
	private Integer year;
	private Integer period;
	
	List<DynamicBigVO> lstKpiData;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public String getCycleText() {
		return cycleText;
	}

	public void setCycleText(String cycleText) {
		this.cycleText = cycleText;
	}

	public Long getKiId() {
		return kiId;
	}

	public void setKiId(Long kiId) {
		this.kiId = kiId;
	}

	public Long getKpiId() {
		return kpiId;
	}

	public void setKpiId(Long kpiId) {
		this.kpiId = kpiId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public List<DynamicBigVO> getLstKpiData() {
		return lstKpiData;
	}

	public void setLstKpiData(List<DynamicBigVO> lstKpiData) {
		this.lstKpiData = lstKpiData;
	}
	
	

}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;


public class StockTransLotVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String productCode;
	
	private String productName;
	
	private Integer quantity;
	
	private Integer convfact;
	
	private String lot;
	
	private BigDecimal price;
	
	private BigDecimal total;

	private String qtyDisplay;
	
	private String bigUnit;
	
	private String smallUnit;
	
	private String fromWarehouse;
	
	private String toWarehouse;

	private Long fromOwnerId;

	private Long toOwnerId;

	public String getBigUnit() {
		return bigUnit;
	}

	public void setBigUnit(String bigUnit) {
		this.bigUnit = bigUnit;
	}

	public String getSmallUnit() {
		return smallUnit;
	}

	public void setSmallUnit(String smallUnit) {
		this.smallUnit = smallUnit;
	}

	public String getQtyDisplay() {
		int bigUnit = 0;
		int smallUnit = 0;
		
		if(convfact <= 0){
			convfact = 1;
		}
		bigUnit = quantity/convfact;
		smallUnit = quantity%convfact;
		qtyDisplay = String.valueOf(bigUnit) + '/' + String.valueOf(smallUnit);
		return qtyDisplay;
	}
	
	public void setQtyDisplay(String qtyDisplay) {
		this.qtyDisplay = qtyDisplay;
	}
	
	public String getThungLe() {
		int bigUnit = 0;
		int smallUnit = 0;
		
		if(convfact <= 0){
			convfact = 1;
		}
		bigUnit = quantity/convfact;
		smallUnit = quantity%convfact;
		return String.valueOf(bigUnit) + '/' + String.valueOf(smallUnit);
	}
	
	public String getThung() {
		int bigUnit = 0;
		if(convfact <= 0){
			convfact = 1;
		}
		bigUnit = quantity/convfact;
		return String.valueOf(bigUnit);
	}
	
	public String getLe() {
		int smallUnit = 0;
		if(convfact <= 0){
			convfact = 1;
		}
		smallUnit = quantity%convfact;
		return String.valueOf(smallUnit);
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getConvfact() {
		if (convfact == null) {
			convfact = 0;
		}
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public BigDecimal getPrice() {
		return price == null ? BigDecimal.valueOf(0) : price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getTotal() {
		return total == null ? BigDecimal.valueOf(0) : total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getFromWarehouse() {
		return fromWarehouse;
	}

	public void setFromWarehouse(String fromWarehouse) {
		this.fromWarehouse = fromWarehouse;
	}

	public String getToWarehouse() {
		return toWarehouse;
	}

	public void setToWarehouse(String toWarehouse) {
		this.toWarehouse = toWarehouse;
	}

	public Long getToOwnerId() {
		return toOwnerId;
	}

	public void setToOwnerId(Long toOwnerId) {
		this.toOwnerId = toOwnerId;
	}
	
	public Long getFromOwnerId() {
		return fromOwnerId;
	}

	public void setFromOwnerId(Long fromOwnerId) {
		this.fromOwnerId = fromOwnerId;
	}

}
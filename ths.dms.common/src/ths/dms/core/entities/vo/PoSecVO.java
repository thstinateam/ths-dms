package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PoSecVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long poVnmId;
	private String poConfirmNumber;
	private Date poVnmDate;
	private BigDecimal amount;
	private BigDecimal discount;
	private BigDecimal total; // tong tien chiet khau
	private BigDecimal remain;// con no
	private BigDecimal totalPay; // thanh toan(da tra)
	private Long debitDetailId;
	public String getPoConfirmNumber() {
		return poConfirmNumber;
	}
	public void setPoConfirmNumber(String poConfirmNumber) {
		this.poConfirmNumber = poConfirmNumber;
	}
	public Date getPoVnmDate() {
		return poVnmDate;
	}
	public void setPoVnmDate(Date poVnmDate) {
		this.poVnmDate = poVnmDate;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getRemain() {
		return remain;
	}
	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}
	public BigDecimal getTotalPay() {
		return totalPay;
	}
	public void setTotalPay(BigDecimal totalPay) {
		this.totalPay = totalPay;
	}
	public Long getPoVnmId() {
		return poVnmId;
	}
	public void setPoVnmId(Long poVnmId) {
		this.poVnmId = poVnmId;
	}
	public Long getDebitDetailId() {
		return debitDetailId;
	}
	public void setDebitDetailId(Long debitDetailId) {
		this.debitDetailId = debitDetailId;
	}
}

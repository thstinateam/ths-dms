package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

public class CycleCountSearchVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	private String vungshopName;
	private String areaShopName;
	private String shopName;
	private String shopPhone;
	private String shopAddress;
	private String wareHouseName;
	private String cycleCountCode;
	private Date startDate;
	private String createUser;
	private Date approvedDate;
	private String approvedUser;
	private String status;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopPhone() {
		return shopPhone;
	}
	public void setShopPhone(String shopPhone) {
		this.shopPhone = shopPhone;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String getWareHouseName() {
		return wareHouseName;
	}
	public void setWareHouseName(String wareHouseName) {
		this.wareHouseName = wareHouseName;
	}
	public String getCycleCountCode() {
		return cycleCountCode;
	}
	public void setCycleCountCode(String cycleCountCode) {
		this.cycleCountCode = cycleCountCode;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getApprovedDate() {
		return approvedDate;
	}
	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}
	public String getApprovedUser() {
		return approvedUser;
	}
	public void setApprovedUser(String approvedUser) {
		this.approvedUser = approvedUser;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getVungshopName() {
		return vungshopName;
	}
	public void setVungshopName(String vungshopName) {
		this.vungshopName = vungshopName;
	}
	public String getAreaShopName() {
		return areaShopName;
	}
	public void setAreaShopName(String areaShopName) {
		this.areaShopName = areaShopName;
	}
	
	
	
}

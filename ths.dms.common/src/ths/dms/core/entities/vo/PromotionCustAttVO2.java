package ths.dms.core.entities.vo;

import java.io.Serializable;

public class PromotionCustAttVO2 implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6377309448532271010L;
	private Long promotionCustAttrId;
	private Long attributeId;
	private Long attributeEnumId;
	private String fromValue;
	private String toValue;
	private Long attributeDetailId;
	private Long promotionCustAttrDetailId;
	private String attributeDetailCode;
	private String attributeDetailName;
	private Integer valueType;
	
	public Long getPromotionCustAttrId() {
		return promotionCustAttrId;
	}
	public void setPromotionCustAttrId(Long promotionCustAttrId) {
		this.promotionCustAttrId = promotionCustAttrId;
	}
	public Long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}
	public String getFromValue() {
		return fromValue;
	}
	public void setFromValue(String fromValue) {
		this.fromValue = fromValue;
	}
	public String getToValue() {
		return toValue;
	}
	public void setToValue(String toValue) {
		this.toValue = toValue;
	}
	public Long getAttributeDetailId() {
		return attributeDetailId;
	}
	public void setAttributeDetailId(Long attributeDetailId) {
		this.attributeDetailId = attributeDetailId;
	}
	public Long getPromotionCustAttrDetailId() {
		return promotionCustAttrDetailId;
	}
	public void setPromotionCustAttrDetailId(Long promotionCustAttrDetailId) {
		this.promotionCustAttrDetailId = promotionCustAttrDetailId;
	}
	public String getAttributeDetailCode() {
		return attributeDetailCode;
	}
	public void setAttributeDetailCode(String attributeDetailCode) {
		this.attributeDetailCode = attributeDetailCode;
	}
	public String getAttributeDetailName() {
		return attributeDetailName;
	}
	public void setAttributeDetailName(String attributeDetailName) {
		this.attributeDetailName = attributeDetailName;
	}
	public Integer getValueType() {
		return valueType;
	}
	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}
	public Long getAttributeEnumId() {
		return attributeEnumId;
	}
	public void setAttributeEnumId(Long attributeEnumId) {
		this.attributeEnumId = attributeEnumId;
	}
}

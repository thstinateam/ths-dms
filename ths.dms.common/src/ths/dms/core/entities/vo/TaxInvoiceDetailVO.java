package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class TaxInvoiceDetailVO implements Serializable {

	private static final long serialVersionUID = 1L;
	// ma hang hoa
	private String productCode;
	// ten hang hoa
	private String productName;
	// don vi tinh
	private String unit;
	// so luong
	private Integer quantity;
	// don gia
	private BigDecimal price;
	// thanh tien
	private BigDecimal amount;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price == null ? BigDecimal.valueOf(0) : price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}

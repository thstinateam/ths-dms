package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptTDBHTMHProductDateStaffFollowCustomerVO implements Serializable{

	/**
	 * @author sangtn
	 * 
	 */
	//Các thông tin:
	//Mã k/h, tên k/h, địa chỉ k/h: CUSTOMER_CODE, CUSTOMER_NAME, ADDRESS trong bảng CUSTOMER
	//Đơn hàng: ORDER_NUMBER trong SALE_ORDER
	//Số hóa đơn: Không thấy, recommend trong SALE_ORDER_DETAIL
	//Số lượng: QUANTITY trong SALE_ORDER_DETAIL
	//Thành tiền: AMOUNT lấy tạm thời trong SALE_ORDER
	
	//Trong sql tra ve: so.shop_id, p.product_code, p.product_name, sod.price,so.create_date, s.staff_id, s.staff_name,   so.amount, so.order_number, cu.customer_name, cu.address, cu.short_code, sod.quantity,
	//s.staff_code
	
				
	
	private BigDecimal shopId;
	private BigDecimal productId;
	private String productCode;
	
	private String productName;
	private String createDate;
	private BigDecimal price;
	
	private BigDecimal quantity;
	private BigDecimal amount;
	private BigDecimal customerId;
	
	private String customerCode;
	private String customerName;
	private String customerAddress;
	
	private BigDecimal staffId;
	private String staffCode;
	private String staffName;	
	
	private String orderNumber;
	private String invoiceNumber;
	
	private BigDecimal sumStaffQuantity; 
	private BigDecimal sumStaffAmount;
	private BigDecimal sumDayQuantity; 
	private BigDecimal sumDayAmount;
	private BigDecimal sumProductAndPriceQuantity; 
	private BigDecimal sumProductAndPriceAmount;
	
	public BigDecimal getShopId() {
		return shopId;
	}
	public void setShopId(BigDecimal shopId) {
		this.shopId = shopId;
	}
	public BigDecimal getProductId() {
		return productId;
	}
	public void setProductId(BigDecimal productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getCreateDate() {		
		return createDate;
	}
	public void setCreateDate(String createDate) {
		//Samg, chỉnh định dạng ngày	
		//Date date = parse(createDate, "yyyy/MM/dd");	
		//this.createDate = toDateString(date, "dd/MM/yyyy");;
		this.createDate = createDate;
	}
	
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getCustomerId() {
		return customerId;
	}
	public void setCustomerId(BigDecimal customerId) {
		this.customerId = customerId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public BigDecimal getStaffId() {
		return staffId;
	}
	public void setStaffId(BigDecimal staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public BigDecimal getSumStaffQuantity() {
		return sumStaffQuantity;
	}
	public void setSumStaffQuantity(BigDecimal sumStaffQuantity) {
		this.sumStaffQuantity = sumStaffQuantity;
	}
	public BigDecimal getSumStaffAmount() {
		return sumStaffAmount;
	}
	public void setSumStaffAmount(BigDecimal sumStaffAmount) {
		this.sumStaffAmount = sumStaffAmount;
	}
	public BigDecimal getSumDayQuantity() {
		return sumDayQuantity;
	}
	public void setSumDayQuantity(BigDecimal sumDayQuantity) {
		this.sumDayQuantity = sumDayQuantity;
	}
	public BigDecimal getSumDayAmount() {
		return sumDayAmount;
	}
	public void setSumDayAmount(BigDecimal sumDayAmount) {
		this.sumDayAmount = sumDayAmount;
	}
	public BigDecimal getSumProductAndPriceQuantity() {
		return sumProductAndPriceQuantity;
	}
	public void setSumProductAndPriceQuantity(BigDecimal sumProductAndPriceQuantity) {
		this.sumProductAndPriceQuantity = sumProductAndPriceQuantity;
	}
	public BigDecimal getSumProductAndPriceAmount() {
		return sumProductAndPriceAmount;
	}
	public void setSumProductAndPriceAmount(BigDecimal sumProductAndPriceAmount) {
		this.sumProductAndPriceAmount = sumProductAndPriceAmount;
	}	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.ActiveType;

public class SaleTypeFocusProgramGridVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8751751818763357283L;
	private Long id;
	private String saleTypeCode;
	private String apParamName;
	private ActiveType status = ActiveType.RUNNING ;
	private Long focusChannelMapId ;
	
	public Long getFocusChannelMapId() {
		return focusChannelMapId;
	}
	public void setFocusChannelMapId(Long focusChannelMapId) {
		this.focusChannelMapId = focusChannelMapId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSaleTypeCode() {
		return saleTypeCode;
	}
	public void setSaleTypeCode(String saleTypeCode) {
		this.saleTypeCode = saleTypeCode;
	}
	public String getApParamName() {
		return apParamName;
	}
	public void setApParamName(String apParamName) {
		this.apParamName = apParamName;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	
	
}

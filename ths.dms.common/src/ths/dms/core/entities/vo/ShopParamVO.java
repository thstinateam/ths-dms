/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;


/**
 * Mo ta class ShopParamVO.java
 * @author vuongmq
 * @since Nov 30, 2015
 */
public class ShopParamVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private Long id;
	private Long shopId;
	
	private String code;
	private String name;
	private String value;
	private String valueDate;
	private String valueTime;
	
	private String valueFromDateStr;
	private String valueToDateStr;
	private String tmpView;

	private Integer updateFlag;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	public String getValueTime() {
		return valueTime;
	}

	public void setValueTime(String valueTime) {
		this.valueTime = valueTime;
	}

	public Integer getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(Integer updateFlag) {
		this.updateFlag = updateFlag;
	}

	public String getValueFromDateStr() {
		return valueFromDateStr;
	}

	public void setValueFromDateStr(String valueFromDateStr) {
		this.valueFromDateStr = valueFromDateStr;
	}

	public String getValueToDateStr() {
		return valueToDateStr;
	}

	public void setValueToDateStr(String valueToDateStr) {
		this.valueToDateStr = valueToDateStr;
	}

	public String getTmpView() {
		return tmpView;
	}

	public void setTmpView(String tmpView) {
		this.tmpView = tmpView;
	}

}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Class Equipment VO
 * 
 *@author hunglm16
 *@since December 14,2014
 *@description Han che dung bien Object
 */
public class EquipmentManagerVO implements Serializable {

	private static final long serialVersionUID = -8033734306114133018L;
	
	private Long id;
	private Long stockId;
	private Long staffId;
	private Long equipmentId;
	
	private Integer quantity;
	
	private String stockCode;
	private String staffCode;
	private String equipmentCode;
	private String seriNumber;
	private String healthStatus;
	private String stockName;
	private String typeEquipment;
	private String groupEquipmentCode;
	private String groupEquipmentName;
	private String capacity;
	private String equipmentBrand;
	private String equipmentProvider;
	private String yearManufacture;
	private String shortCode;
	private String customerName;
	private String address;
	private String shopCode;
	
	
	private BigDecimal price;

	/**
	 * Khai bao GETTER/SETTER
	 * */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(Long equipmentId) {
		this.equipmentId = equipmentId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getTypeEquipment() {
		return typeEquipment;
	}

	public void setTypeEquipment(String typeEquipment) {
		this.typeEquipment = typeEquipment;
	}

	public String getGroupEquipmentCode() {
		return groupEquipmentCode;
	}

	public void setGroupEquipmentCode(String groupEquipmentCode) {
		this.groupEquipmentCode = groupEquipmentCode;
	}

	public String getGroupEquipmentName() {
		return groupEquipmentName;
	}

	public void setGroupEquipmentName(String groupEquipmentName) {
		this.groupEquipmentName = groupEquipmentName;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getEquipmentBrand() {
		return equipmentBrand;
	}

	public void setEquipmentBrand(String equipmentBrand) {
		this.equipmentBrand = equipmentBrand;
	}

	public String getEquipmentProvider() {
		return equipmentProvider;
	}

	public void setEquipmentProvider(String equipmentProvider) {
		this.equipmentProvider = equipmentProvider;
	}

	public String getYearManufacture() {
		return yearManufacture;
	}

	public void setYearManufacture(String yearManufacture) {
		this.yearManufacture = yearManufacture;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	
}

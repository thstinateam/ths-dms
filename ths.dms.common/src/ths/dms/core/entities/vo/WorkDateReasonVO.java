package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;



public class WorkDateReasonVO implements Serializable {

	private static final long serialVersionUID = 1L;

	List<WorkReasonVO> lstWorkReason;
	Map<String, Integer> mapSaleDate;
	public List<WorkReasonVO> getLstWorkReason() {
		return lstWorkReason;
	}
	public void setLstWorkReason(List<WorkReasonVO> lstWorkReason) {
		this.lstWorkReason = lstWorkReason;
	}
	public Map<String, Integer> getMapSaleDate() {
		return mapSaleDate;
	}
	public void setMapSaleDate(Map<String, Integer> mapSaleDate) {
		this.mapSaleDate = mapSaleDate;
	}
}
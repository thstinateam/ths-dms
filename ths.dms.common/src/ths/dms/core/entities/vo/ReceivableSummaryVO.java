/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VO bao cao cong no phai thu tong hop
 * @author trietptm
 * @since Dec 29, 2015
 */
public class ReceivableSummaryVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String regionCode;
	private String areaCode;
	private String shopCode;
	private String shopName;
	private String customerCode;
	private String customerName;
	private String address;
	private BigDecimal openDebit;
	private BigDecimal increase;
	private BigDecimal discountReceivable;
	private BigDecimal payment;
	private BigDecimal decrease;
	private BigDecimal discountPayable;
	private BigDecimal paymentRevert;
	private BigDecimal closeDebit;
	private BigDecimal maxDebitAmount;
	
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public BigDecimal getOpenDebit() {
		return openDebit;
	}
	public void setOpenDebit(BigDecimal openDebit) {
		this.openDebit = openDebit;
	}
	public BigDecimal getIncrease() {
		return increase;
	}
	public void setIncrease(BigDecimal increase) {
		this.increase = increase;
	}
	public BigDecimal getDiscountReceivable() {
		return discountReceivable;
	}
	public void setDiscountReceivable(BigDecimal discountReceivable) {
		this.discountReceivable = discountReceivable;
	}
	public BigDecimal getPayment() {
		return payment;
	}
	public void setPayment(BigDecimal payment) {
		this.payment = payment;
	}
	public BigDecimal getDecrease() {
		return decrease;
	}
	public void setDecrease(BigDecimal decrease) {
		this.decrease = decrease;
	}
	public BigDecimal getDiscountPayable() {
		return discountPayable;
	}
	public void setDiscountPayable(BigDecimal discountPayable) {
		this.discountPayable = discountPayable;
	}
	public BigDecimal getPaymentRevert() {
		return paymentRevert;
	}
	public void setPaymentRevert(BigDecimal paymentRevert) {
		this.paymentRevert = paymentRevert;
	}
	public BigDecimal getCloseDebit() {
		return closeDebit;
	}
	public void setCloseDebit(BigDecimal closeDebit) {
		this.closeDebit = closeDebit;
	}
	public BigDecimal getMaxDebitAmount() {
		return maxDebitAmount;
	}
	public void setMaxDebitAmount(BigDecimal maxDebitAmount) {
		this.maxDebitAmount = maxDebitAmount;
	}
}

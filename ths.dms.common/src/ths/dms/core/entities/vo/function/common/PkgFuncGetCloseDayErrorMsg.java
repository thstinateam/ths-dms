/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.function.common;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

/**
 * Lay thong tin du lieu loi khi thuc hien kiem tra chot ngay
 * 
 * @author hunglm16
 * @since 21/10/2015
 */
public class PkgFuncGetCloseDayErrorMsg implements Serializable {

	private static final long serialVersionUID = -131343869617979289L;
	
	private Long idDV; 
	private BigDecimal thanhTien; 
	private Integer soLuong;
	private String loaiDH;
	private String ghiChu;
	private String ngayChot;
	private String soDH;
	private String soDHSub3;
	
	/**
	 * Xy ly null cho cac gia tri mac dinh
	 * @author hunglm16
	 * @since October 12,2015
	 */
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */

	public Long getIdDV() {
		return idDV;
	}

	public void setIdDV(Long idDV) {
		this.idDV = idDV;
	}

	public BigDecimal getThanhTien() {
		return thanhTien;
	}

	public void setThanhTien(BigDecimal thanhTien) {
		this.thanhTien = thanhTien;
	}

	public Integer getSoLuong() {
		return soLuong;
	}

	public void setSoLuong(Integer soLuong) {
		this.soLuong = soLuong;
	}

	public String getLoaiDH() {
		return loaiDH;
	}

	public void setLoaiDH(String loaiDH) {
		this.loaiDH = loaiDH;
	}

	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	public String getNgayChot() {
		return ngayChot;
	}

	public void setNgayChot(String ngayChot) {
		this.ngayChot = ngayChot;
	}
	public String getSoDH() {
		return soDH;
	}
	public void setSoDH(String soDH) {
		this.soDH = soDH;
	}
	public String getSoDHSub3() {
		return soDHSub3;
	}
	public void setSoDHSub3(String soDHSub3) {
		this.soDHSub3 = soDHSub3;
	}
	
}
package ths.dms.core.entities.vo;

import java.io.Serializable;

public class DisplayToolVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long displayToolId; // Ma tu (tuong ung la productId trong bang Product)
	
	private String displayToolCode; 
	
	private String displayToolName;
	
	public Long getDisplayToolId() {
		return displayToolId;
	}

	public void setDisplayToolId(Long displayToolId) {
		this.displayToolId = displayToolId;
	}

	public String getDisplayToolCode() {
		return displayToolCode;
	}

	public void setDisplayToolCode(String displayToolCode) {
		this.displayToolCode = displayToolCode;
	}

	public String getDisplayToolName() {
		return displayToolName;
	}

	public void setDisplayToolName(String displayToolName) {
		this.displayToolName = displayToolName;
	}
}

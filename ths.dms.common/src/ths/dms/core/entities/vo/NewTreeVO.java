package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NewTreeVO<T,V> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1878562251044796712L;

	private T object;
	
	private V detail;
	
	private List<NewTreeVO<T,V>> listChildren;

	public NewTreeVO() {
		listChildren = new ArrayList<NewTreeVO<T,V>>();
	}
	
	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

	public List<NewTreeVO<T,V>> getListChildren() {
		return listChildren;
	}

	public void setListChildren(List<NewTreeVO<T,V>> listChildren) {
		this.listChildren = listChildren;
	}

	public V getDetail() {
		return detail;
	}

	public void setDetail(V detail) {
		this.detail = detail;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;



public class WorkReasonVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date dayOff;
	private String reason;
	public Date getDayOff() {
		return dayOff;
	}
	public void setDayOff(Date dayOff) {
		this.dayOff = dayOff;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
}
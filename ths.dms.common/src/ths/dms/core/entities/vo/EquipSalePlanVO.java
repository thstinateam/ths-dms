package ths.dms.core.entities.vo;

/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.EquipSalePlan;
import ths.dms.core.entities.enumtype.EquipSalePlanCustomerType;

/**
 * Class Equip Sale Plan VO
 * 
 * @author liemtpt
 * @since 03/04/2015
 * 
 */
public class EquipSalePlanVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long equipGroupId;
	private String fromMonth;
	private String toMonth;
	private String customerTypeStr;
	private Long customerTypeId;
	private String customerTypeCode;
	private String amount;
	private Integer indexRow;
	private Boolean isRowEmpty;
	private Boolean flagCheckExistSP;
	private List<String> row;

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public String getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(String fromMonth) {
		this.fromMonth = fromMonth;
	}

	public String getToMonth() {
		return toMonth;
	}

	public void setToMonth(String toMonth) {
		this.toMonth = toMonth;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Integer getIndexRow() {
		return indexRow;
	}

	public void setIndexRow(Integer indexRow) {
		this.indexRow = indexRow;
	}

	public String getCustomerTypeStr() {
		return customerTypeStr;
	}

	public void setCustomerTypeStr(String customerTypeStr) {
		this.customerTypeStr = customerTypeStr;
	}

	public Long getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(Long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public Boolean getIsRowEmpty() {
		return isRowEmpty;
	}

	public void setIsRowEmpty(Boolean isRowEmpty) {
		this.isRowEmpty = isRowEmpty;
	}

	public List<String> getRow() {
		return row;
	}

	public void setRow(List<String> row) {
		this.row = row;
	}

	public Boolean getFlagCheckExistSP() {
		return flagCheckExistSP;
	}

	public void setFlagCheckExistSP(Boolean flagCheckExistSP) {
		this.flagCheckExistSP = flagCheckExistSP;
	}

	public String getCustomerTypeCode() {
		return customerTypeCode;
	}

	public void setCustomerTypeCode(String customerTypeCode) {
		this.customerTypeCode = customerTypeCode;
	}
	
	
}

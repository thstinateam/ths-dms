/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * Settings VO
 * @author duongdt3
 * @since 28/11/2015 
 */
public class SettingsVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long hourAutoCloseDateId;
	private String hourAutoCloseDateValue;

	private Long distanceCheckCustomerId;
	private Long distanceCheckCustomerValue;
	private Long minDistanceCheckCustomerValue;
	private Long maxDistanceCheckCustomerValue;

	private Long problemAttachSizeMaxId;
	private Long problemAttachSizeMaxValue;
	private Long minProblemAttachSizeMaxValue;
	private Long maxProblemAttachSizeMaxValue;

	private Long problemFileTypeUploadId;
	private String problemFileTypeUploadValue;
	private Long maxLenghtProblemFileTypeUploadValue;

	private Long timeRequestPositionId;
	private Long timeRequestPositionValue;
	private Long minTimeRequestPositionValue;
	private Long maxTimeRequestPositionValue;

	private Long timeRequestSyncDataId;
	private Long timeRequestSyncDataValue;
	private Long minTimeRequestSyncDataValue;
	private Long maxTimeRequestSyncDataValue;

	public Long getHourAutoCloseDateId() {
		return hourAutoCloseDateId;
	}

	public void setHourAutoCloseDateId(Long hourAutoCloseDateId) {
		this.hourAutoCloseDateId = hourAutoCloseDateId;
	}

	public String getHourAutoCloseDateValue() {
		return hourAutoCloseDateValue;
	}

	public void setHourAutoCloseDateValue(String hourAutoCloseDateValue) {
		this.hourAutoCloseDateValue = hourAutoCloseDateValue;
	}

	public Long getDistanceCheckCustomerId() {
		return distanceCheckCustomerId;
	}

	public void setDistanceCheckCustomerId(Long distanceCheckCustomerId) {
		this.distanceCheckCustomerId = distanceCheckCustomerId;
	}

	public Long getDistanceCheckCustomerValue() {
		return distanceCheckCustomerValue;
	}

	public void setDistanceCheckCustomerValue(Long distanceCheckCustomerValue) {
		this.distanceCheckCustomerValue = distanceCheckCustomerValue;
	}

	public Long getProblemAttachSizeMaxId() {
		return problemAttachSizeMaxId;
	}

	public void setProblemAttachSizeMaxId(Long problemAttachSizeMaxId) {
		this.problemAttachSizeMaxId = problemAttachSizeMaxId;
	}

	public Long getProblemAttachSizeMaxValue() {
		return problemAttachSizeMaxValue;
	}

	public void setProblemAttachSizeMaxValue(Long problemAttachSizeMaxValue) {
		this.problemAttachSizeMaxValue = problemAttachSizeMaxValue;
	}

	public Long getProblemFileTypeUploadId() {
		return problemFileTypeUploadId;
	}

	public void setProblemFileTypeUploadId(Long problemFileTypeUploadId) {
		this.problemFileTypeUploadId = problemFileTypeUploadId;
	}

	public String getProblemFileTypeUploadValue() {
		return problemFileTypeUploadValue;
	}

	public void setProblemFileTypeUploadValue(String problemFileTypeUploadValue) {
		this.problemFileTypeUploadValue = problemFileTypeUploadValue;
	}

	public Long getTimeRequestPositionId() {
		return timeRequestPositionId;
	}

	public void setTimeRequestPositionId(Long timeRequestPositionId) {
		this.timeRequestPositionId = timeRequestPositionId;
	}

	public Long getTimeRequestPositionValue() {
		return timeRequestPositionValue;
	}

	public void setTimeRequestPositionValue(Long timeRequestPositionValue) {
		this.timeRequestPositionValue = timeRequestPositionValue;
	}

	public Long getTimeRequestSyncDataId() {
		return timeRequestSyncDataId;
	}

	public void setTimeRequestSyncDataId(Long timeRequestSyncDataId) {
		this.timeRequestSyncDataId = timeRequestSyncDataId;
	}

	public Long getTimeRequestSyncDataValue() {
		return timeRequestSyncDataValue;
	}

	public void setTimeRequestSyncDataValue(Long timeRequestSyncDataValue) {
		this.timeRequestSyncDataValue = timeRequestSyncDataValue;
	}

	public Long getMinDistanceCheckCustomerValue() {
		return minDistanceCheckCustomerValue;
	}

	public void setMinDistanceCheckCustomerValue(Long minDistanceCheckCustomerValue) {
		this.minDistanceCheckCustomerValue = minDistanceCheckCustomerValue;
	}

	public Long getMaxDistanceCheckCustomerValue() {
		return maxDistanceCheckCustomerValue;
	}

	public void setMaxDistanceCheckCustomerValue(Long maxDistanceCheckCustomerValue) {
		this.maxDistanceCheckCustomerValue = maxDistanceCheckCustomerValue;
	}

	public Long getMinProblemAttachSizeMaxValue() {
		return minProblemAttachSizeMaxValue;
	}

	public void setMinProblemAttachSizeMaxValue(Long minProblemAttachSizeMaxValue) {
		this.minProblemAttachSizeMaxValue = minProblemAttachSizeMaxValue;
	}

	public Long getMaxProblemAttachSizeMaxValue() {
		return maxProblemAttachSizeMaxValue;
	}

	public void setMaxProblemAttachSizeMaxValue(Long maxProblemAttachSizeMaxValue) {
		this.maxProblemAttachSizeMaxValue = maxProblemAttachSizeMaxValue;
	}

	public Long getMaxLenghtProblemFileTypeUploadValue() {
		return maxLenghtProblemFileTypeUploadValue;
	}

	public void setMaxLenghtProblemFileTypeUploadValue(Long maxLenghtProblemFileTypeUploadValue) {
		this.maxLenghtProblemFileTypeUploadValue = maxLenghtProblemFileTypeUploadValue;
	}

	public Long getMinTimeRequestPositionValue() {
		return minTimeRequestPositionValue;
	}

	public void setMinTimeRequestPositionValue(Long minTimeRequestPositionValue) {
		this.minTimeRequestPositionValue = minTimeRequestPositionValue;
	}

	public Long getMaxTimeRequestPositionValue() {
		return maxTimeRequestPositionValue;
	}

	public void setMaxTimeRequestPositionValue(Long maxTimeRequestPositionValue) {
		this.maxTimeRequestPositionValue = maxTimeRequestPositionValue;
	}

	public Long getMinTimeRequestSyncDataValue() {
		return minTimeRequestSyncDataValue;
	}

	public void setMinTimeRequestSyncDataValue(Long minTimeRequestSyncDataValue) {
		this.minTimeRequestSyncDataValue = minTimeRequestSyncDataValue;
	}

	public Long getMaxTimeRequestSyncDataValue() {
		return maxTimeRequestSyncDataValue;
	}

	public void setMaxTimeRequestSyncDataValue(Long maxTimeRequestSyncDataValue) {
		this.maxTimeRequestSyncDataValue = maxTimeRequestSyncDataValue;
	}

}

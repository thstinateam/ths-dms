package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ExMapping implements Serializable {
	Long levelId;
	Long parentId;
	String textCode;
	Integer minQuantityMua;
	BigDecimal minAmountMua;
	BigDecimal percent;
	Integer maxQuantityKM;
	BigDecimal maxAmountKM;
	List<SubLevelMapping> listSubLevel;
	public Long getLevelId() {
		return levelId;
	}
	public void setLevelId(Long levelId) {
		this.levelId = levelId;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getTextCode() {
		return textCode;
	}
	public void setTextCode(String textCode) {
		this.textCode = textCode;
	}
	public Integer getMinQuantityMua() {
		return minQuantityMua;
	}
	public void setMinQuantityMua(Integer minQuantityMua) {
		this.minQuantityMua = minQuantityMua;
	}
	public BigDecimal getMinAmountMua() {
		return minAmountMua;
	}
	public void setMinAmountMua(BigDecimal minAmountMua) {
		this.minAmountMua = minAmountMua;
	}
//	public Float getPercent() {
//		return percent;
//	}
//	public void setPercent(Float percent) {
//		this.percent = percent;
//	}
	public Integer getMaxQuantityKM() {
		return maxQuantityKM;
	}
	public void setMaxQuantityKM(Integer maxQuantityKM) {
		this.maxQuantityKM = maxQuantityKM;
	}
	public BigDecimal getMaxAmountKM() {
		return maxAmountKM;
	}
	public void setMaxAmountKM(BigDecimal maxAmountKM) {
		this.maxAmountKM = maxAmountKM;
	}
	public List<SubLevelMapping> getListSubLevel() {
		return listSubLevel;
	}
	public void setListSubLevel(List<SubLevelMapping> listSubLevel) {
		this.listSubLevel = listSubLevel;
	}
	public BigDecimal getPercent() {
		return percent;
	}
	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}
	
}

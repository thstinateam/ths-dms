package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class PoDetailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	private Long id;
	
	private Integer productId;
	
	private String productCode;
	
	private String productName;
	
	private Integer convfact;
	
	private Integer totalQuantity;//tong
	
	private Integer quantity;//so luong con lai
	
	private BigDecimal price;//gia
	
	private BigDecimal amount;//tong tien
	
	private Integer checkLot;
	
	
	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public Long getId() {
		return productId.longValue();
	}

	public void setId(Long id) {
//		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author sangtn
 * @see Bao cao theo doi ban hang theo mat hang
 *
 */
public class RptTDBHTMHProductDateFollowStaffVO implements Serializable{

	/**
	 * 
	 */
	//Các thông tin
	//Ma nv, ten nv: STAFF_CODE, STAFF_NAME trong STAFF
	//Tong so luong ban/tra , Tong thanh tien: Tinh toan tu RptTDBHTMHProductDateStaffFollowCustomerVO
		
	private String staffCode;
	private String staffName;	
	private BigDecimal sumStaffQuantity;
	private BigDecimal sumStaffAmount;
	private List<RptTDBHTMHProductDateStaffFollowCustomerVO> lstRptTDBHTMHProductDateStaffFollowCustomerVO = new ArrayList<RptTDBHTMHProductDateStaffFollowCustomerVO>();
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
	public BigDecimal getSumStaffQuantity() {
		return sumStaffQuantity;
	}
	public void setSumStaffQuantity(BigDecimal sumStaffQuantity) {
		this.sumStaffQuantity = sumStaffQuantity;
	}
	public BigDecimal getSumStaffAmount() {
		return sumStaffAmount;
	}
	public void setSumStaffAmount(BigDecimal sumStaffAmount) {
		this.sumStaffAmount = sumStaffAmount;
	}
	public List<RptTDBHTMHProductDateStaffFollowCustomerVO> getLstRptTDBHTMHProductDateStaffFollowCustomerVO() {
		return lstRptTDBHTMHProductDateStaffFollowCustomerVO;
	}
	public void setLstRptTDBHTMHProductDateStaffFollowCustomerVO(
			List<RptTDBHTMHProductDateStaffFollowCustomerVO> lstRptTDBHTMHProductDateStaffFollowCustomerVO) {
		this.lstRptTDBHTMHProductDateStaffFollowCustomerVO = lstRptTDBHTMHProductDateStaffFollowCustomerVO;
	}
	
}

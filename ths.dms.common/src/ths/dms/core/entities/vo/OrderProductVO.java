package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class OrderProductVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long saleOrderId;
	private String orderNumber;
	private Long productId;
	private String productCode;
	private String productName;
	private String promotionProgramCode;
	private String ksCode;
	private Long priceId;
	private Long priceSaleInId;
	private BigDecimal price;
	private BigDecimal priceNotVat;
	private BigDecimal packagePrice;
	private BigDecimal packagePriceNotVat;
	private Float vat;
	private Integer poLineNumber; // de edit po; po line number
	private Integer availableQuantity;
	private Integer packageQuantity; // de edit po; sl thung
	private Integer retailQuantity; // de edit po; sl le
	private Integer poDetailQuantity; // de edit po; sl quantity po
	private Integer quantity;
	private Integer oldQuantity;
	private Integer stock;
	private Integer convfact;
	private Long isFocus;
	private Float netWeight;
	private Float grossWeight;
	private Integer checkLot;
	private BigDecimal total;
	private BigDecimal amount;
	private String warehouseCode;
	private String warehouseName;
	private Long warehouseId;
	private Integer warehouseType;
	private Integer isFreeItem;
	private Integer countSOD;
	private Integer numDetail;
	private Integer totalDetail;
	
	
	public Float getVat() {
		return vat;
	}
	public void setVat(Float vat) {
		this.vat = vat;
	}
	public Integer getPoLineNumber() {
		return poLineNumber;
	}
	public void setPoLineNumber(Integer poLineNumber) {
		this.poLineNumber = poLineNumber;
	}
	public BigDecimal getPriceNotVat() {
		return priceNotVat;
	}
	public void setPriceNotVat(BigDecimal priceNotVat) {
		this.priceNotVat = priceNotVat;
	}
	public Integer getTotalDetail() {
		return totalDetail;
	}
	public void setTotalDetail(Integer totalDetail) {
		this.totalDetail = totalDetail;
	}
	public Integer getCountSOD() {
		return countSOD;
	}
	public void setCountSOD(Integer countSOD) {
		this.countSOD = countSOD;
	}
	public Integer getNumDetail() {
		return numDetail;
	}
	public void setNumDetail(Integer numDetail) {
		this.numDetail = numDetail;
	}
	public Long getPriceId() {
		return priceId;
	}
	public void setPriceId(Long priceId) {
		this.priceId = priceId;
	}
	public Long getWarehouseId() {
		return warehouseId;
	}
	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public Float getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Float netWeight) {
		this.netWeight = netWeight;
	}

	public Float getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Float grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}

	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public Integer getPackageQuantity() {
		return packageQuantity;
	}
	public void setPackageQuantity(Integer packageQuantity) {
		this.packageQuantity = packageQuantity;
	}
	public Integer getRetailQuantity() {
		return retailQuantity;
	}
	public void setRetailQuantity(Integer retailQuantity) {
		this.retailQuantity = retailQuantity;
	}
	public Integer getPoDetailQuantity() {
		return poDetailQuantity;
	}
	public void setPoDetailQuantity(Integer poDetailQuantity) {
		this.poDetailQuantity = poDetailQuantity;
	}
	/**
	 * @return the convfact
	 */
	public Integer getConvfact() {
		return convfact;
	}

	/**
	 * @param convfact the convfact to set
	 */
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	/**
	 * @return the isFocus
	 */
	public Long getIsFocus() {
		return isFocus;
	}

	/**
	 * @param isFocus the isFocus to set
	 */
	public void setIsFocus(Long isFocus) {
		this.isFocus = isFocus;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getOldQuantity() {
		return oldQuantity;
	}
	public void setOldQuantity(Integer oldQuantity) {
		this.oldQuantity = oldQuantity;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public BigDecimal getPackagePrice() {
		return packagePrice;
	}
	public void setPackagePrice(BigDecimal packagePrice) {
		this.packagePrice = packagePrice;
	}
	public BigDecimal getPackagePriceNotVat() {
		return packagePriceNotVat;
	}
	public void setPackagePriceNotVat(BigDecimal packagePriceNotVat) {
		this.packagePriceNotVat = packagePriceNotVat;
	}
	public Integer getWarehouseType() {
		return warehouseType;
	}
	public void setWarehouseType(Integer warehouseType) {
		this.warehouseType = warehouseType;
	}
	public Integer getIsFreeItem() {
		return isFreeItem;
	}
	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}
	public String getKsCode() {
		return ksCode;
	}
	public void setKsCode(String ksCode) {
		this.ksCode = ksCode;
	}
	public Long getPriceSaleInId() {
		return priceSaleInId;
	}
	public void setPriceSaleInId(Long priceSaleInId) {
		this.priceSaleInId = priceSaleInId;
	}

}

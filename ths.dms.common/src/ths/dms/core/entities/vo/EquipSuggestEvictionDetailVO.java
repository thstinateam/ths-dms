/*
* Copyright YYYY Viettel Telecom. All rights reserved.
* VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
*/
package ths.dms.core.entities.vo;

/**
 * Import thu vien
 * */
import java.io.Serializable;

/**
 * Class Equip Borrow VO
 * 
 * @author nhutnn
 * @since 02/07/2015
 */
public class EquipSuggestEvictionDetailVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long equipSuggestEvictionId;
	private Long id;
	private String equipSuggestEvictionCode;
	private String note;
	private String shopCodeMien;
	private String shopCodeKenh;
	private Long customerId;
	private String customerCode;
	private String customerCodeLong;
	private String customerName;
	private String representative;
	private String relation;
	private String phone;
	private String mobile;
	private String address;
	private String street;
	private String ward;
	private String district;
	private String province;
	private String equipCategory;
	private String capacity;
	private String equipCategoryCode;
	private String healthStatus;
	private String healthStatusCode;
	private String timeEviction;
	private String reason;
	private String equipCode;
	private Long equipId;
	private String equipSeri;
	private String equipBrand;
	private String equipGroup;
	private String staffName;
	private String staffCode;
	private String staffPhone;
	private String staffMobile;
	private Integer stockTypeValue;

	private String shopName;
	private String shopCode;
	
	public Long getEquipSuggestEvictionId() {
		return equipSuggestEvictionId;
	}

	public void setEquipSuggestEvictionId(Long equipSuggestEvictionId) {
		this.equipSuggestEvictionId = equipSuggestEvictionId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEquipSuggestEvictionCode() {
		return equipSuggestEvictionCode;
	}

	public void setEquipSuggestEvictionCode(String equipSuggestEvictionCode) {
		this.equipSuggestEvictionCode = equipSuggestEvictionCode;
	}

	public String getShopCodeMien() {
		return shopCodeMien;
	}

	public void setShopCodeMien(String shopCodeMien) {
		this.shopCodeMien = shopCodeMien;
	}

	public String getShopCodeKenh() {
		return shopCodeKenh;
	}

	public void setShopCodeKenh(String shopCodeKenh) {
		this.shopCodeKenh = shopCodeKenh;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerCodeLong() {
		return customerCodeLong;
	}

	public void setCustomerCodeLong(String customerCodeLong) {
		this.customerCodeLong = customerCodeLong;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getRepresentative() {
		return representative;
	}

	public void setRepresentative(String representative) {
		this.representative = representative;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getEquipCategory() {
		return equipCategory;
	}

	public void setEquipCategory(String equipCategory) {
		this.equipCategory = equipCategory;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getEquipCategoryCode() {
		return equipCategoryCode;
	}

	public void setEquipCategoryCode(String equipCategoryCode) {
		this.equipCategoryCode = equipCategoryCode;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public String getHealthStatusCode() {
		return healthStatusCode;
	}

	public void setHealthStatusCode(String healthStatusCode) {
		this.healthStatusCode = healthStatusCode;
	}

	public String getTimeEviction() {
		return timeEviction;
	}

	public void setTimeEviction(String timeEviction) {
		this.timeEviction = timeEviction;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public String getEquipSeri() {
		return equipSeri;
	}

	public void setEquipSeri(String equipSeri) {
		this.equipSeri = equipSeri;
	}

	public String getEquipBrand() {
		return equipBrand;
	}

	public void setEquipBrand(String equipBrand) {
		this.equipBrand = equipBrand;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffPhone() {
		return staffPhone;
	}

	public void setStaffPhone(String staffPhone) {
		this.staffPhone = staffPhone;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Integer getStockTypeValue() {
		return stockTypeValue;
	}

	public void setStockTypeValue(Integer stockTypeValue) {
		this.stockTypeValue = stockTypeValue;
	}

	public String getStaffMobile() {
		return staffMobile;
	}

	public void setStaffMobile(String staffMobile) {
		this.staffMobile = staffMobile;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getEquipGroup() {
		return equipGroup;
	}

	public void setEquipGroup(String equipGroup) {
		this.equipGroup = equipGroup;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}

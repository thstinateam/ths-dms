package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class Rpt7_2_2_NVBHVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String maNVBH;
	private String tenNVBH;
	
	ArrayList<Rpt7_2_2_NganhHangVO> listRpt7_2_2_NganhHangVO = new ArrayList<Rpt7_2_2_NganhHangVO>();
	private BigDecimal tongSL;
	private BigDecimal tongTien;
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public ArrayList<Rpt7_2_2_NganhHangVO> getListRpt7_2_2_NganhHangVO() {
		return listRpt7_2_2_NganhHangVO;
	}
	public void setListRpt7_2_2_NganhHangVO(
			ArrayList<Rpt7_2_2_NganhHangVO> listRpt7_2_2_NganhHangVO) {
		this.listRpt7_2_2_NganhHangVO = listRpt7_2_2_NganhHangVO;
	}
	public BigDecimal getTongSL() {
		return tongSL;
	}
	public void setTongSL(BigDecimal tongSL) {
		this.tongSL = tongSL;
	}
	public BigDecimal getTongTien() {
		return tongTien;
	}
	public void setTongTien(BigDecimal tongTien) {
		this.tongTien = tongTien;
	} 
	
}

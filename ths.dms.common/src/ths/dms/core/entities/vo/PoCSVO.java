package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PoCSVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// id povnm
	private Long poVNMId;
	// shop code
	private String shopCode;
	// shop name
	private String shopName;
	// sale order number
	private String saleOrderNumber;
	// po auto number
	private String poAutoNumber;
	// discount
	private BigDecimal discount;
	// tong tien total
	private BigDecimal total;
	// tong tien
	private BigDecimal amount;
	// tong tien da nhan theo hop dong
	private BigDecimal amountReceived;
	// tong so luong
	private Integer quantity;
	// tong so luong da nhan
	private Integer quantityReceived;
	// trang thai
	private Integer status;
	// ngay tao
	private Date poVNMDate;

	private Integer type;
	
	public Long getPoVNMId() {
		return poVNMId;
	}

	public void setPoVNMId(Long poVNMId) {
		this.poVNMId = poVNMId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public BigDecimal getAmountReceived() {
		return amountReceived == null ? BigDecimal.valueOf(0) : amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantityReceived() {
		if (quantityReceived == null) {
			quantityReceived = 0;
		}
		return quantityReceived;
	}

	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getPoVNMDate() {
		return poVNMDate;
	}

	public void setPoVNMDate(Date poVNMDate) {
		this.poVNMDate = poVNMDate;
	}

	/**
	 * @param saleOrderNumber the saleOrderNumber to set
	 */
	public void setSaleOrderNumber(String saleOrderNumber) {
		this.saleOrderNumber = saleOrderNumber;
	}

	/**
	 * @return the saleOrderNumber
	 */
	public String getSaleOrderNumber() {
		return saleOrderNumber;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getPoAutoNumber() {
		return poAutoNumber;
	}

	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VO: promotion_staff_map
 * 
 * @author lacnv1
 * @since Aug 18, 2014
 *
 */
public class PromotionStaffVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long mapId;
	private Long id;
	private Long parentId;
	private String code;
	private String name;
	private Integer isSaler;
	private BigDecimal quantity;
	private BigDecimal receivedQtt;
	private BigDecimal amountMax;
	private BigDecimal receivedAmt;
	private BigDecimal numMax;
	private BigDecimal receivedNum;
	
	public Long getMapId() {
		return mapId;
	}
	
	public void setMapId(Long mapId) {
		this.mapId = mapId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getReceivedQtt() {
		return receivedQtt;
	}

	public void setReceivedQtt(BigDecimal receivedQtt) {
		this.receivedQtt = receivedQtt;
	}

	public Integer getIsSaler() {
		return isSaler;
	}

	public void setIsSaler(Integer isSaler) {
		this.isSaler = isSaler;
	}

	public BigDecimal getAmountMax() {
		return amountMax;
	}

	public void setAmountMax(BigDecimal amountMax) {
		this.amountMax = amountMax;
	}

	public BigDecimal getReceivedAmt() {
		return receivedAmt;
	}

	public void setReceivedAmt(BigDecimal receivedAmt) {
		this.receivedAmt = receivedAmt;
	}

	public BigDecimal getNumMax() {
		return numMax;
	}

	public void setNumMax(BigDecimal numMax) {
		this.numMax = numMax;
	}

	public BigDecimal getReceivedNum() {
		return receivedNum;
	}

	public void setReceivedNum(BigDecimal receivedNum) {
		this.receivedNum = receivedNum;
	}
	
}
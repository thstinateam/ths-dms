package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class PrintDeliveryGroupVO1 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String deliveryCode;
	private String deliveryName;
	private String customerShortCode;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String productCode;
	private String productName;
	private Integer convfact;
	private Integer isFreeItem;
	private Integer quantity;
	private BigDecimal price;
	private BigDecimal amount;
	private BigDecimal discountAmount;
	private String promotionProgramCode;
	private String promotionProgramName;
	private String orderNumber;

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getCustomerShortCode() {
		return customerShortCode;
	}

	public void setCustomerShortCode(String customerShortCode) {
		this.customerShortCode = customerShortCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Integer getIsFreeItem() {
		return isFreeItem;
	}

	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the promotionProgramCode
	 */
	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}

	/**
	 * @param promotionProgramCode the promotionProgramCode to set
	 */
	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}

	public String getPromotionProgramName() {
		return promotionProgramName;
	}

	public void setPromotionProgramName(String promotionProgramName) {
		this.promotionProgramName = promotionProgramName;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}
}

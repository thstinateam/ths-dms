package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

public class ProductsForPoAutoParams implements Serializable {
private static final long serialVersionUID = 1L;
	
	private String productCode;
	
	private String productName;
	
	private Long poAutoId;
	
	private List<Long> lstExceptProductId; 

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getPoAutoId() {
		return poAutoId;
	}

	public void setPoAutoId(Long poAutoId) {
		this.poAutoId = poAutoId;
	}

	public List<Long> getLstExceptProductId() {
		return lstExceptProductId;
	}

	public void setLstExceptProductId(List<Long> lstExceptProductId) {
		this.lstExceptProductId = lstExceptProductId;
	}
}

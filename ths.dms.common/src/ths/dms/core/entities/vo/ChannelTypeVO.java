package ths.dms.core.entities.vo;

import java.io.Serializable;

public class ChannelTypeVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idChannelType;
	private String nameChannelType;
	private String codeChannelType;
	//sontt them field de xu ly tren web:
	private boolean checked;
	//
	
	public Long getIdChannelType() {
		return idChannelType;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public void setIdChannelType(Long idChannelType) {
		this.idChannelType = idChannelType;
	}
	public String getNameChannelType() {
		return nameChannelType;
	}
	public void setNameChannelType(String nameChannelType) {
		this.nameChannelType = nameChannelType;
	}
	public String getCodeChannelType() {
		return codeChannelType;
	}
	public void setCodeChannelType(String codeChannelType) {
		this.codeChannelType = codeChannelType;
	}
}

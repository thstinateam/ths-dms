package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SuperVisorInfomationVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long staffId;
	
	private String staffCode;
	
	private BigDecimal lat;
	
	private BigDecimal lng;
	
	private BigDecimal accuracy;
	
	private BigDecimal plan;
	
	private BigDecimal amount;
	
	private Integer pointInRouting;
	
	private Integer pointAmount;
	
	private Date lastDatePosition;
	
	private String strLastDatePosition;

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public BigDecimal getLat() {
		return lat;
	}

	public void setLat(BigDecimal lat) {
		this.lat = lat;
	}

	public BigDecimal getLng() {
		return lng;
	}

	public void setLng(BigDecimal lng) {
		this.lng = lng;
	}

	public BigDecimal getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(BigDecimal accuracy) {
		this.accuracy = accuracy;
	}

	public BigDecimal getPlan() {
		return plan;
	}

	public void setPlan(BigDecimal plan) {
		this.plan = plan;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getPointInRouting() {
		return pointInRouting;
	}

	public void setPointInRouting(Integer pointInRouting) {
		this.pointInRouting = pointInRouting;
	}

	public Integer getPointAmount() {
		return pointAmount;
	}

	public void setPointAmount(Integer pointAmount) {
		this.pointAmount = pointAmount;
	}

	/**
	 * @return the lastDatePosition
	 */
	public Date getLastDatePosition() {
		return lastDatePosition;
	}

	/**
	 * @param lastDatePosition the lastDatePosition to set
	 */
	public void setLastDatePosition(Date lastDatePosition) {
		this.lastDatePosition = lastDatePosition;
		if(lastDatePosition!=null){
			this.strLastDatePosition = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a").format(lastDatePosition);
		}
	}

	public String getStrLastDatePosition() {
		return strLastDatePosition;
	}

	public void setStrLastDatePosition(String strLastDatePosition) {
		this.strLastDatePosition = strLastDatePosition;
	}
	
	
}

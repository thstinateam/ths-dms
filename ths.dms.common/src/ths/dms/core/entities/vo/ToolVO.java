package ths.dms.core.entities.vo;

import java.io.Serializable;

public class ToolVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long shopId;
	private Long toolId;
	private Long parentShopId;
	private String shopCode;
	private String shopName;
	private String mien;
	private String vung;
	private Integer shopType;
	private String toolCode;
	private Float lat;
	private Float lng;
	private Float latCustomer;
	private Float lngCustomer;
	private Integer countTool;
	private Integer countWarning;
	private Long distance;
	private String createDate;
	private Float accuracy;
	private String address;
	private String productYear;
	private String warranty;
	private String imei;
	private String serial;
	private String customerCode;
	private String customerName;
	private Float temperature;
	private Float minTemp;
	private Float maxTemp;
	private String minMaxTemp;
	private Boolean isBold;
	private Integer isWarning;
	private String chipCode;
	private String chipVer;
	private Long chipId;
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getParentShopId() {
		return parentShopId;
	}
	public String getChipVer() {
		return chipVer;
	}
	public void setChipVer(String chipVer) {
		this.chipVer = chipVer;
	}
	public void setParentShopId(Long parentShopId) {
		this.parentShopId = parentShopId;
	}
	public Long getToolId() {
		return toolId;
	}
	public void setToolId(Long toolId) {
		this.toolId = toolId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Integer getShopType() {
		return shopType;
	}
	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}
	public Float getLat() {
		return lat;
	}
	public void setLat(Float lat) {
		this.lat = lat;
	}
	public Float getLng() {
		return lng;
	}
	public void setLng(Float lng) {
		this.lng = lng;
	}
	public Integer getCountTool() {
		return countTool;
	}
	public void setCountTool(Integer countTool) {
		this.countTool = countTool;
	}
	public Integer getCountWarning() {
		return countWarning;
	}
	public void setCountWarning(Integer countWarning) {
		this.countWarning = countWarning;
	}
	public Long getDistance() {
		return distance;
	}
	public void setDistance(Long distance) {
		this.distance = distance;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public Float getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(Float accuracy) {
		this.accuracy = accuracy;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Float getLatCustomer() {
		return latCustomer;
	}
	public void setLatCustomer(Float latCustomer) {
		this.latCustomer = latCustomer;
	}
	public Float getLngCustomer() {
		return lngCustomer;
	}
	public void setLngCustomer(Float lngCustomer) {
		this.lngCustomer = lngCustomer;
	}
	public String getProductYear() {
		return productYear;
	}
	public void setProductYear(String productYear) {
		this.productYear = productYear;
	}
	public String getWarranty() {
		return warranty;
	}
	public void setWarranty(String warranty) {
		this.warranty = warranty;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Float getTemperature() {
		return temperature;
	}
	public void setTemperature(Float temperature) {
		this.temperature = temperature;
	}
	public Float getMinTemp() {
		return minTemp;
	}
	public void setMinTemp(Float minTemp) {
		this.minTemp = minTemp;
	}
	public Float getMaxTemp() {
		return maxTemp;
	}
	public void setMaxTemp(Float maxTemp) {
		this.maxTemp = maxTemp;
	}
	public String getToolCode() {
		return toolCode;
	}
	public void setToolCode(String toolCode) {
		this.toolCode = toolCode;
	}
	public Boolean getIsBold() {
		return isBold;
	}
	public void setIsBold(Boolean isBold) {
		this.isBold = isBold;
	}
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public Integer getIsWarning() {
		return isWarning;
	}
	public void setIsWarning(Integer isWarning) {
		this.isWarning = isWarning;
	}
	public String getChipCode() {
		return chipCode;
	}
	public void setChipCode(String chipCode) {
		this.chipCode = chipCode;
	}
	public Long getChipId() {
		return chipId;
	}
	public void setChipId(Long chipId) {
		this.chipId = chipId;
	}
	public String getMinMaxTemp() {
		return minMaxTemp;
	}
	public void setMinMaxTemp(String minMaxTemp) {
		this.minMaxTemp = minMaxTemp;
	}
	
	
}

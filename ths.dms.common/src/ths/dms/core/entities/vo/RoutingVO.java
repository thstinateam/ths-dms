package ths.dms.core.entities.vo;

import java.io.Serializable;

public class RoutingVO implements Serializable{
	/**
	 * @author hunglm16
	 * @since March 25, 2014
	 */
	private static final long serialVersionUID = 1L;
	private Long visitPlanId;
	private Long staffId;
	private String staffCode;
	private String staffName;
	private String routingCode;
	private String routingName;
	private Long routingId;
	private Integer status;
	private String shopCode;
	
	private String customerShortCode;
	private String customerName;
	private String customerAddress;
	private String statusRoutingCustomer;
	private String monDayStr;
	private String tuesDayStr;
	private String wednesDayStr;
	private String thursDayStr;
	private String firtDayStr;
	private String saturDayStr;
	private String sunDayStr;
	private Integer weekInterval;
	private String startDateStr;
	private String endDateStr;
	private String seq2Str;
	private String seq3Str;
	private String seq4Str;
	private String seq5Str;
	private String seq6Str;
	private String seq7Str;
	private String seq8Str;
	private String week1Str;
	private String week2Str;
	private String week3Str;
	private Integer tansuat;
	public String getWeek1Str() {
		return week1Str;
	}
	public void setWeek1Str(String week1Str) {
		this.week1Str = week1Str;
	}
	public String getWeek2Str() {
		return week2Str;
	}
	public void setWeek2Str(String week2Str) {
		this.week2Str = week2Str;
	}
	public String getWeek3Str() {
		return week3Str;
	}
	public void setWeek3Str(String week3Str) {
		this.week3Str = week3Str;
	}
	public String getWeek4Str() {
		return week4Str;
	}
	public void setWeek4Str(String week4Str) {
		this.week4Str = week4Str;
	}


	private String week4Str;
	
	private Integer seq2;
	private Integer seq3;
	private Integer seq4;
	private Integer seq5;
	private Integer seq6;
	private Integer seq7;
	private Integer seq8;
	
	public Integer getSeq2() {
		return seq2;
	}
	public void setSeq2(Integer seq2) {
		this.seq2 = seq2;
	}
	public Integer getSeq3() {
		return seq3;
	}
	public void setSeq3(Integer seq3) {
		this.seq3 = seq3;
	}
	public Integer getSeq4() {
		return seq4;
	}
	public void setSeq4(Integer seq4) {
		this.seq4 = seq4;
	}
	public Integer getSeq5() {
		return seq5;
	}
	public void setSeq5(Integer seq5) {
		this.seq5 = seq5;
	}
	public Integer getSeq6() {
		return seq6;
	}
	public void setSeq6(Integer seq6) {
		this.seq6 = seq6;
	}
	public Integer getSeq7() {
		return seq7;
	}
	public void setSeq7(Integer seq7) {
		this.seq7 = seq7;
	}
	public Integer getSeq8() {
		return seq8;
	}
	public void setSeq8(Integer seq8) {
		this.seq8 = seq8;
	}
	
	
	private String updateDateStr;
	
	public String getUpdateDateStr() {
		return updateDateStr;
	}
	public void setUpdateDateStr(String updateDateStr) {
		this.updateDateStr = updateDateStr;
	}
	public String getSeq2Str() {
		return seq2Str;
	}
	public void setSeq2Str(String seq2Str) {
		this.seq2Str = seq2Str;
	}
	public String getSeq3Str() {
		return seq3Str;
	}
	public void setSeq3Str(String seq3Str) {
		this.seq3Str = seq3Str;
	}
	public String getSeq4Str() {
		return seq4Str;
	}
	public void setSeq4Str(String seq4Str) {
		this.seq4Str = seq4Str;
	}
	public String getSeq5Str() {
		return seq5Str;
	}
	public void setSeq5Str(String seq5Str) {
		this.seq5Str = seq5Str;
	}
	public String getSeq6Str() {
		return seq6Str;
	}
	public void setSeq6Str(String seq6Str) {
		this.seq6Str = seq6Str;
	}
	public String getSeq7Str() {
		return seq7Str;
	}
	public void setSeq7Str(String seq7Str) {
		this.seq7Str = seq7Str;
	}
	public String getSeq8Str() {
		return seq8Str;
	}
	public void setSeq8Str(String seq8Str) {
		this.seq8Str = seq8Str;
	}
	
	public Integer getWeekInterval() {
		return weekInterval;
	}
	public void setWeekInterval(Integer weekInterval) {
		this.weekInterval = weekInterval;
	}
	public String getStartDateStr() {
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	public String getEndDateStr() {
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	
	public String getCustomerShortCode() {
		return customerShortCode;
	}
	public void setCustomerShortCode(String customerShortCode) {
		this.customerShortCode = customerShortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getStatusRoutingCustomer() {
		return statusRoutingCustomer;
	}
	public void setStatusRoutingCustomer(String statusRoutingCustomer) {
		this.statusRoutingCustomer = statusRoutingCustomer;
	}
	
	public String getMonDayStr() {
		return monDayStr;
	}
	public void setMonDayStr(String monDayStr) {
		this.monDayStr = monDayStr;
	}
	public String getTuesDayStr() {
		return tuesDayStr;
	}
	public void setTuesDayStr(String tuesDayStr) {
		this.tuesDayStr = tuesDayStr;
	}
	public String getWednesDayStr() {
		return wednesDayStr;
	}
	public void setWednesDayStr(String wednesDayStr) {
		this.wednesDayStr = wednesDayStr;
	}
	public String getThursDayStr() {
		return thursDayStr;
	}
	public void setThursDayStr(String thursDayStr) {
		this.thursDayStr = thursDayStr;
	}
	public String getFirtDayStr() {
		return firtDayStr;
	}
	public void setFirtDayStr(String firtDayStr) {
		this.firtDayStr = firtDayStr;
	}
	public String getSaturDayStr() {
		return saturDayStr;
	}
	public void setSaturDayStr(String saturDayStr) {
		this.saturDayStr = saturDayStr;
	}
	public String getSunDayStr() {
		return sunDayStr;
	}
	public void setSunDayStr(String sunDayStr) {
		this.sunDayStr = sunDayStr;
	}
	/**
	 * @return the visitPlanId
	 */
	public Long getVisitPlanId() {
		return visitPlanId;
	}
	/**
	 * @param visitPlanId the visitPlanId to set
	 */
	public void setVisitPlanId(Long visitPlanId) {
		this.visitPlanId = visitPlanId;
	}
	/**
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}
	/**
	 * @param staffCode the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	/**
	 * @return the staffName
	 */
	public String getStaffName() {
		return staffName;
	}
	/**
	 * @param staffName the staffName to set
	 */
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	/**
	 * @return the routingCode
	 */
	public String getRoutingCode() {
		return routingCode;
	}
	/**
	 * @param routingCode the routingCode to set
	 */
	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}
	/**
	 * @return the routingName
	 */
	public String getRoutingName() {
		return routingName;
	}
	/**
	 * @param routingName the routingName to set
	 */
	public void setRoutingName(String routingName) {
		this.routingName = routingName;
	}
	/**
	 * @return the shopCode
	 */
	public String getShopCode() {
		return shopCode;
	}
	/**
	 * @param shopCode the shopCode to set
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	/**
	 * @return the routingId
	 */
	public Long getRoutingId() {
		return routingId;
	}
	/**
	 * @param routingId the routingId to set
	 */
	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getTansuat() {
		return tansuat;
	}
	public void setTansuat(Integer tansuat) {
		this.tansuat = tansuat;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	
}

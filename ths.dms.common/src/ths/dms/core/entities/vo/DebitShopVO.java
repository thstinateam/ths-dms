package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class DebitShopVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long debitId;
	private String invoiceNumber;
	private String shopCode;
	private String poCoNumber;
	private Timestamp createDate;
	private BigDecimal total;
	private BigDecimal remain;
	private BigDecimal userPaid;/* this field is used for web */
	private BigDecimal remainAfterPaid;/* this field is used for web */
	private BigDecimal amount;
	private BigDecimal discount;

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public BigDecimal getUserPaid() {
		return userPaid == null ? BigDecimal.valueOf(0) : userPaid;
	}

	public void setUserPaid(BigDecimal userPaid) {
		this.userPaid = userPaid;
	}

	public long getDebitId() {
		return debitId;
	}

	public void setDebitId(long debitId) {
		this.debitId = debitId;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getPoCoNumber() {
		return poCoNumber;
	}

	public void setPoCoNumber(String poCoNumber) {
		this.poCoNumber = poCoNumber;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public BigDecimal getTotal() {
		return total == null ? BigDecimal.valueOf(0) : total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getRemain() {
		return remain == null ? BigDecimal.valueOf(0) : remain;
	}

	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}

	public void setRemainAfterPaid(BigDecimal remainAfterPaid) {
		this.remainAfterPaid = remainAfterPaid;
	}

	public BigDecimal getRemainAfterPaid() {
		return remainAfterPaid == null ? BigDecimal.valueOf(0)
				: remainAfterPaid;
	}

}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptDSKHTMHCatAndListInfoVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String catCode;
	List<RptDSKHTMHRecordProductVO> listDetail = new ArrayList<RptDSKHTMHRecordProductVO>();
	private BigDecimal sumQuantitySaleForCategory;
	private BigDecimal sumQuantityDisplayForCategory;
	private BigDecimal sumMoneyForCategory;

	public String getCatCode() {
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public List<RptDSKHTMHRecordProductVO> getListDetail() {
		return listDetail;
	}

	public void setListDetail(List<RptDSKHTMHRecordProductVO> listDetail) {
		this.listDetail = listDetail;
	}

	public BigDecimal getSumQuantitySaleForCategory() {
		return sumQuantitySaleForCategory;
	}

	public void setSumQuantitySaleForCategory(BigDecimal sumQuantitySaleForCategory) {
		this.sumQuantitySaleForCategory = sumQuantitySaleForCategory;
	}

	public BigDecimal getSumQuantityDisplayForCategory() {
		return sumQuantityDisplayForCategory;
	}

	public void setSumQuantityDisplayForCategory(
			BigDecimal sumQuantityDisplayForCategory) {
		this.sumQuantityDisplayForCategory = sumQuantityDisplayForCategory;
	}

	public BigDecimal getSumMoneyForCategory() {
		return sumMoneyForCategory;
	}

	public void setSumMoneyForCategory(BigDecimal sumMoneyForCategory) {
		this.sumMoneyForCategory = sumMoneyForCategory;
	}

}
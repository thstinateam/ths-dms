package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import ths.dms.core.entities.enumtype.ProgramType;

/**
 * Luu SaleOrderPromotionDetail cho don hang.
 *
 * @author tientv11
 * @since 04/10/2014
 */
public class SaleOrderPromotionDetailVO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The program code. */
	private String programCode;
	
	/** The discount amount. */
	private BigDecimal discountAmount;
	
	/** The discount percent. */
	private BigDecimal discountPercent;
	
	/** The program type. */
	private ProgramType programType;
	
	/** The program type. */
	private String programTypeCode;
	
	/** The max amount free. */
	private BigDecimal maxAmountFree;
	
	/** The is owner. */
	private Integer isOwner;

	/** The product code. */
	private String productCode;
	
	/** The product id. */
	private Long productId;
	
	private Long productGroupId;
	
	private Long groupLevelId;
	
	public Long getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(Long productGroupId) {
		this.productGroupId = productGroupId;
	}

	public Long getGroupLevelId() {
		return groupLevelId;
	}

	public void setGroupLevelId(Long groupLevelId) {
		this.groupLevelId = groupLevelId;
	}
	public String getProgramTypeCode() {
		return programTypeCode;
	}

	public void setProgramTypeCode(String programTypeCode) {
		this.programTypeCode = programTypeCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * Gets the program code.
	 *
	 * @return the program code
	 */
	public String getProgramCode() {
		return programCode;
	}

	/**
	 * Sets the program code.
	 *
	 * @param programCode the new program code
	 */
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	/**
	 * Gets the discount amount.
	 *
	 * @return the discount amount
	 */
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	/**
	 * Sets the discount amount.
	 *
	 * @param discountAmount the new discount amount
	 */
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	/**
	 * Gets the discount percent.
	 *
	 * @return the discount percent
	 */
	public BigDecimal getDiscountPercent() {
		return discountPercent;
	}

	/**
	 * Sets the discount percent.
	 *
	 * @param discountPercent the new discount percent
	 */
	public void setDiscountPercent(BigDecimal discountPercent) {
		this.discountPercent = discountPercent;
	}

	/**
	 * Gets the program type.
	 *
	 * @return the program type
	 */
	public ProgramType getProgramType() {
		return programType;
	}

	/**
	 * Sets the program type.
	 *
	 * @param programType the new program type
	 */
	public void setProgramType(ProgramType programType) {
		this.programType = programType;
	}

	/**
	 * Gets the max amount free.
	 *
	 * @return the max amount free
	 */
	public BigDecimal getMaxAmountFree() {
		return maxAmountFree;
	}

	/**
	 * Sets the max amount free.
	 *
	 * @param maxAmountFree the new max amount free
	 */
	public void setMaxAmountFree(BigDecimal maxAmountFree) {
		this.maxAmountFree = maxAmountFree;
	}

	/**
	 * Gets the checks if is owner.
	 *
	 * @return the checks if is owner
	 */
	public Integer getIsOwner() {
		return isOwner;
	}

	/**
	 * Sets the checks if is owner.
	 *
	 * @param isOwner the new checks if is owner
	 */
	public void setIsOwner(Integer isOwner) {
		this.isOwner = isOwner;
	}
	
	
	
	
}

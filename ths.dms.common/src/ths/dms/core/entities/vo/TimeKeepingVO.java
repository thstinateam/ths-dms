/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * VO cham cong
 * @author trietptm
 * @since Oct 28, 2015
 */
public class TimeKeepingVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String regionCode;
	private String areaCode;
	private String shopCode;
	private String shopName;
	private String staffCode;
	private String staffName;
	
	private Long shopId;
	private Long staffId;
	
	private Integer numHouse;
	private Integer numPlanDay;
	private Integer numCustomer;
	private Integer quantity;
	
	private Date day;

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Integer getNumHouse() {
		return numHouse;
	}

	public void setNumHouse(Integer numHouse) {
		this.numHouse = numHouse;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public Integer getNumPlanDay() {
		return numPlanDay;
	}

	public void setNumPlanDay(Integer numPlanDay) {
		this.numPlanDay = numPlanDay;
	}

	public Integer getNumCustomer() {
		return numCustomer;
	}

	public void setNumCustomer(Integer numCustomer) {
		this.numCustomer = numCustomer;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
}

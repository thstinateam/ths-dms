package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.SaleOrder;

public class SaleOrderParams implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private SaleOrder salesOrder;
	private List<SaleProductVO> lstSaleProductVO;
	private List<SaleProductVO> lstPromoProductVO;
	private LogInfoVO logInfo;
	public SaleOrder getSalesOrder() {
		return salesOrder;
	}
	public void setSalesOrder(SaleOrder salesOrder) {
		this.salesOrder = salesOrder;
	}
	public List<SaleProductVO> getLstSaleProductVO() {
		return lstSaleProductVO;
	}
	public void setLstSaleProductVO(List<SaleProductVO> lstSaleProductVO) {
		this.lstSaleProductVO = lstSaleProductVO;
	}
	
	public List<SaleProductVO> getLstPromoProductVO() {
		return lstPromoProductVO;
	}
	public void setLstPromoProductVO(List<SaleProductVO> lstPromoProductVO) {
		this.lstPromoProductVO = lstPromoProductVO;
	}
	public LogInfoVO getLogInfo() {
		return logInfo;
	}
	public void setLogInfo(LogInfoVO logInfo) {
		this.logInfo = logInfo;
	}
	
}

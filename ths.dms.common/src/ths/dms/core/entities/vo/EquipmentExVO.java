package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.management.loading.PrivateClassLoader;

/**
 * Class Equipment VO
 * 
 * @author hoanv25
 * @since December 10,2014
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description Them thuoc tinh co trong bang Equiment
 */
public class EquipmentExVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id; 
	private String soHopDong; 
	private String loaiThietBi;
	private String nhomThietBi;
	private String maThietBi;
	private String soSeri;
	private Integer soLuong;
	private String tinhTrangThietBi;
	private Integer manufacturingYear;
	private Long equipDeliveryId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSoHopDong() {
		return soHopDong;
	}
	public void setSoHopDong(String soHopDong) {
		this.soHopDong = soHopDong;
	}
	public String getLoaiThietBi() {
		return loaiThietBi;
	}
	public void setLoaiThietBi(String loaiThietBi) {
		this.loaiThietBi = loaiThietBi;
	}
	public String getNhomThietBi() {
		return nhomThietBi;
	}
	public void setNhomThietBi(String nhomThietBi) {
		this.nhomThietBi = nhomThietBi;
	}
	public String getMaThietBi() {
		return maThietBi;
	}
	public void setMaThietBi(String maThietBi) {
		this.maThietBi = maThietBi;
	}
	public String getSoSeri() {
		return soSeri;
	}
	public void setSoSeri(String soSeri) {
		this.soSeri = soSeri;
	}
	public Integer getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(Integer soLuong) {
		this.soLuong = soLuong;
	}
	public String getTinhTrangThietBi() {
		return tinhTrangThietBi;
	}
	public void setTinhTrangThietBi(String tinhTrangThietBi) {
		this.tinhTrangThietBi = tinhTrangThietBi;
	}
	public Integer getManufacturingYear() {
		return manufacturingYear;
	}
	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}
	public Long getEquipDeliveryId() {
		return equipDeliveryId;
	}
	public void setEquipDeliveryId(Long equipDeliveryId) {
		this.equipDeliveryId = equipDeliveryId;
	}
	
}

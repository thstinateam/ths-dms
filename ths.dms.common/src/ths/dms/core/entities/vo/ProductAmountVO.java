package ths.dms.core.entities.vo;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.StockObjectType;


// TODO: Auto-generated Javadoc
/**
 * The Class ProductAmountVO.
 */
public class ProductAmountVO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The product id. */
	private long productId;
	
	/** The pakage. */
	private int pakage = 0;
	
	/** The unit. */
	private int unit = 0;
	
	/** The owner id. */
	private long ownerId;
	
	/** The owner type. */
	private StockObjectType ownerType;
	
	/** The warehouse id. */
	private Long warehouseId;
	
	
	
	
	/**
	 * Gets the warehouse id.
	 *
	 * @return the warehouse id
	 */
	public Long getWarehouseId() {
		return warehouseId;
	}

	/**
	 * Sets the warehouse id.
	 *
	 * @param warehouseId the new warehouse id
	 */
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	/**
	 * Gets the product id.
	 *
	 * @return the product id
	 */
	public long getProductId() {
		return productId;
	}
	
	/**
	 * Sets the product id.
	 *
	 * @param productId the new product id
	 */
	public void setProductId(long productId) {
		this.productId = productId;
	}
	
	/**
	 * Gets the pakage.
	 *
	 * @return the pakage
	 */
	public int getPakage() {
		return pakage;
	}
	
	/**
	 * Sets the pakage.
	 *
	 * @param pakage the new pakage
	 */
	public void setPakage(int pakage) {
		this.pakage = pakage;
	}
	
	/**
	 * Gets the unit.
	 *
	 * @return the unit
	 */
	public int getUnit() {
		return unit;
	}
	
	/**
	 * Sets the unit.
	 *
	 * @param unit the new unit
	 */
	public void setUnit(int unit) {
		this.unit = unit;
	}
	
	/**
	 * Gets the owner id.
	 *
	 * @return the owner id
	 */
	public long getOwnerId() {
		return ownerId;
	}
	
	/**
	 * Sets the owner id.
	 *
	 * @param ownerId the new owner id
	 */
	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}
	
	/**
	 * Gets the owner type.
	 *
	 * @return the owner type
	 */
	public StockObjectType getOwnerType() {
		return ownerType;
	}
	
	/**
	 * Sets the owner type.
	 *
	 * @param ownerType the new owner type
	 */
	public void setOwnerType(StockObjectType ownerType) {
		this.ownerType = ownerType;
	}
}
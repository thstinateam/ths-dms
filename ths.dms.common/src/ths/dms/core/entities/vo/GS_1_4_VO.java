/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import ths.core.entities.vo.rpt.DynamicVO;

public class GS_1_4_VO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	
	private String maMien;
	private String maKhuVuc;
	private String maVung;
	private String maNPP;
	private String tenNPP;
	private String maGSNPP;
	private String tenGSNPP;
	private String maNVBH;
	private String tenNVBH;
	private String ngay;
	private String maTuyen;
	private String tenTuyen;
	private String maKH;
	private String tenKH;
	private String maSP;
	private String tenSp;
	private String nhanHang;
	private String nganhHang;
	private String nganhHangCon;
	private String dongGoi;
	private String huongVi;
	private String donViTinh;
	private String keyShop;
	private String muc;
	private String thudatsl;
	private String thudatds;
	private String slthudatsl;
	private String slthudatds;
	private String diaChi;
	private String loaiKH;
	private String xaPhuong;
	private String quanHuyen;
	private String tinhTp;
	private String phoneNumber;
	private String oderDateC; //ngay ban hang
	private String thoiGianBatDau;
	private String thoiGianKetThuc;
	private String close;
	private String ngayDatHang;
	private String ngayDuyetDon;
	private String soDH;
	private String taoTren;
	private String TrangThai;
	private String giaTriDH;
	private String chietKhau;
	private String loai;
	private String ghiChu;
	private String loaiDH;
	private String soNha;
	private String huyen;
	private String tinh;
	private String xa;
	private String duong;
	private String diDong;
	private String taiKhoan;
	private String nganHang;
	private String chiNhanh;
	private String chuTk;
	private String chuKy;
	private String maKho;
	private String tenKho;
	private String soPODVKH;
	private String nghiepVu;
	private String poConfirm;
	private String ngayOrder;
	private String ngayXK;
	private String ngayNhan;
	private String maKiemKe;
	private String ngayKiemKe;
	private String ngayDuyet;
	private String nguoiDuyet;
	private String tuNpp;
	private String denNpp;
	
	private Integer quyCach;
	private Integer sanLuong;
	private Integer quantityRetail;
	private Integer le;
	private Integer sumQuantityRetail;
	private Integer slle;
	private Integer quantityPackage;
	private Integer thung; 
	private Integer sumQuantityPackage;
	private Integer slthung;
	private Integer quantity;
	private Integer leDoi; //san luong quy doi ra le
	private Integer targetSl; //san luong bc 2.2 
	private Integer sltargetSl;
	private Integer soLuong;
	private Integer sumQuantity;
	private Integer slqd;// sum theo san luong quy doi
	private Integer totalCustomer;
	private Integer slsoDH;
	private Integer slOrder;
	private Integer slNhap;
	private Integer conLai;
	private Integer slTonCuoi;
	private Integer slKiemKe;
	private Integer chenhLech;
	private Integer nhapNuti;
	private Integer traNuti;
	private Integer xuatNuti; // Ban hang
	private Integer xuatKM;
	private Integer xuatVansale;
	private Integer nhapVansale;
	private Integer traNutiKH; // Tra hang
	private Integer hangDiDuong;
	private Integer xuatDc;
	private Integer nhapDc;
	
	private BigDecimal targetDs;
	private BigDecimal sltargetDs;
	private BigDecimal thanhTienSauThue;
	private BigDecimal thanhTienTruocThue;
	private BigDecimal giaChuaThue;
	private BigDecimal giaSauThue;
	private BigDecimal visitLat;
	private BigDecimal visitLng;
	private BigDecimal soPhutGheTham;
	private BigDecimal khoangCach;
	private BigDecimal doanhSoTruocVAT;
	private BigDecimal sldoanhSoTruocVAT;
	private BigDecimal amount;
	private BigDecimal doanhSoSauVAT;
	private BigDecimal sumAmount;
	private BigDecimal sldoanhSoSauVAT;
	private BigDecimal slthanhTienSauThue;
	private BigDecimal slchietKhau;
	private BigDecimal slgiaTriDH;
	private BigDecimal tonDau;
	private BigDecimal tonCuoi;
	private BigDecimal tongNhap;
	private BigDecimal tongXuat;
	private BigDecimal donGia;
	private BigDecimal thanhTien;
	
	private List<DynamicVO> lstColumns;
	
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaGSNPP() {
		return maGSNPP;
	}
	public void setMaGSNPP(String maGSNPP) {
		this.maGSNPP = maGSNPP;
	}
	public String getTenGSNPP() {
		return tenGSNPP;
	}
	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getNgay() {
		return ngay;
	}
	public void setNgay(String ngay) {
		this.ngay = ngay;
	}
	public String getMaTuyen() {
		return maTuyen;
	}
	public void setMaTuyen(String maTuyen) {
		this.maTuyen = maTuyen;
	}
	public String getTenTuyen() {
		return tenTuyen;
	}
	public void setTenTuyen(String tenTuyen) {
		this.tenTuyen = tenTuyen;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getLoaiKH() {
		return loaiKH;
	}
	public void setLoaiKH(String loaiKH) {
		this.loaiKH = loaiKH;
	}
	public BigDecimal getVisitLat() {
		return visitLat;
	}
	public void setVisitLat(BigDecimal visitLat) {
		this.visitLat = visitLat;
	}
	public BigDecimal getVisitLng() {
		return visitLng;
	}
	public void setVisitLng(BigDecimal visitLng) {
		this.visitLng = visitLng;
	}
	public String getThoiGianBatDau() {
		return thoiGianBatDau;
	}
	public void setThoiGianBatDau(String thoiGianBatDau) {
		this.thoiGianBatDau = thoiGianBatDau;
	}
	public String getThoiGianKetThuc() {
		return thoiGianKetThuc;
	}
	public void setThoiGianKetThuc(String thoiGianKetThuc) {
		this.thoiGianKetThuc = thoiGianKetThuc;
	}
	public String getClose() {
		return close;
	}
	public void setClose(String close) {
		this.close = close;
	}
	public BigDecimal getSoPhutGheTham() {
		return soPhutGheTham;
	}
	public void setSoPhutGheTham(BigDecimal soPhutGheTham) {
		this.soPhutGheTham = soPhutGheTham;
	}
	public BigDecimal getKhoangCach() {
		return khoangCach;
	}
	public void setKhoangCach(BigDecimal khoangCach) {
		this.khoangCach = khoangCach;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getOderDateC() {
		return oderDateC;
	}
	public void setOderDateC(String oderDateC) {
		this.oderDateC = oderDateC;
	}
	public String getXaPhuong() {
		return xaPhuong;
	}
	public void setXaPhuong(String xaPhuong) {
		this.xaPhuong = xaPhuong;
	}
	public String getQuanHuyen() {
		return quanHuyen;
	}
	public void setQuanHuyen(String quanHuyen) {
		this.quanHuyen = quanHuyen;
	}
	public String getTinhTp() {
		return tinhTp;
	}
	public void setTinhTp(String tinhTp) {
		this.tinhTp = tinhTp;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Integer getTotalCustomer() {
		return totalCustomer;
	}
	public void setTotalCustomer(Integer totalCustomer) {
		this.totalCustomer = totalCustomer;
	}
	public String getMaKhuVuc() {
		return maKhuVuc;
	}
	public void setMaKhuVuc(String maKhuVuc) {
		this.maKhuVuc = maKhuVuc;
	}
	public String getMaSP() {
		return maSP;
	}
	public void setMaSP(String maSP) {
		this.maSP = maSP;
	}
	public String getTenSp() {
		return tenSp;
	}
	public void setTenSp(String tenSp) {
		this.tenSp = tenSp;
	}
	public String getNhanHang() {
		return nhanHang;
	}
	public void setNhanHang(String nhanHang) {
		this.nhanHang = nhanHang;
	}
	public String getNganhHang() {
		return nganhHang;
	}
	public void setNganhHang(String nganhHang) {
		this.nganhHang = nganhHang;
	}
	public String getNganhHangCon() {
		return nganhHangCon;
	}
	public void setNganhHangCon(String nganhHangCon) {
		this.nganhHangCon = nganhHangCon;
	}
	public String getDongGoi() {
		return dongGoi;
	}
	public void setDongGoi(String dongGoi) {
		this.dongGoi = dongGoi;
	}
	public String getHuongVi() {
		return huongVi;
	}
	public void setHuongVi(String huongVi) {
		this.huongVi = huongVi;
	}
	public String getDonViTinh() {
		return donViTinh;
	}
	public void setDonViTinh(String donViTinh) {
		this.donViTinh = donViTinh;
	}
	public Integer getQuyCach() {
		return quyCach;
	}
	public void setQuyCach(Integer quyCach) {
		this.quyCach = quyCach;
	}
	public Integer getSanLuong() {
		return sanLuong;
	}
	public void setSanLuong(Integer sanLuong) {
		this.sanLuong = sanLuong;
	}
	public BigDecimal getThanhTienSauThue() {
		return thanhTienSauThue;
	}
	public void setThanhTienSauThue(BigDecimal thanhTienSauThue) {
		this.thanhTienSauThue = thanhTienSauThue;
	}
	public BigDecimal getThanhTienTruocThue() {
		return thanhTienTruocThue;
	}
	public void setThanhTienTruocThue(BigDecimal thanhTienTruocThue) {
		this.thanhTienTruocThue = thanhTienTruocThue;
	}
	public BigDecimal getGiaChuaThue() {
		return giaChuaThue;
	}
	public void setGiaChuaThue(BigDecimal giaChuaThue) {
		this.giaChuaThue = giaChuaThue;
	}
	public BigDecimal getGiaSauThue() {
		return giaSauThue;
	}
	public void setGiaSauThue(BigDecimal giaSauThue) {
		this.giaSauThue = giaSauThue;
	}
	public Integer getLeDoi() {
		return leDoi;
	}
	public void setLeDoi(Integer leDoi) {
		this.leDoi = leDoi;
	}
	public Integer getTargetSl() {
		return targetSl;
	}
	public void setTargetSl(Integer targetSl) {
		this.targetSl = targetSl;
	}
	public BigDecimal getTargetDs() {
		return targetDs;
	}
	public void setTargetDs(BigDecimal targetDs) {
		this.targetDs = targetDs;
	}
	public Integer getThung() {
		return thung;
	}
	public void setThung(Integer thung) {
		this.thung = thung;
	}
	public Integer getLe() {
		return le;
	}
	public void setLe(Integer le) {
		this.le = le;
	}
	public String getThudatsl() {
		return thudatsl;
	}
	public void setThudatsl(String thudatsl) {
		this.thudatsl = thudatsl;
	}
	public String getThudatds() {
		return thudatds;
	}
	public void setThudatds(String thudatds) {
		this.thudatds = thudatds;
	}
	public Integer getSlqd() {
		return slqd;
	}
	public void setSlqd(Integer slqd) {
		this.slqd = slqd;
	}
	public BigDecimal getDoanhSoTruocVAT() {
		return doanhSoTruocVAT;
	}
	public void setDoanhSoTruocVAT(BigDecimal doanhSoTruocVAT) {
		this.doanhSoTruocVAT = doanhSoTruocVAT;
	}
	public BigDecimal getDoanhSoSauVAT() {
		return doanhSoSauVAT;
	}
	public void setDoanhSoSauVAT(BigDecimal doanhSoSauVAT) {
		this.doanhSoSauVAT = doanhSoSauVAT;
	}
	public Integer getSlle() {
		return slle;
	}
	public void setSlle(Integer slle) {
		this.slle = slle;
	}
	public Integer getSlthung() {
		return slthung;
	}
	public void setSlthung(Integer slthung) {
		this.slthung = slthung;
	}
	public Integer getSltargetSl() {
		return sltargetSl;
	}
	public void setSltargetSl(Integer sltargetSl) {
		this.sltargetSl = sltargetSl;
	}
	public BigDecimal getSltargetDs() {
		return sltargetDs;
	}
	public void setSltargetDs(BigDecimal sltargetDs) {
		this.sltargetDs = sltargetDs;
	}
	public BigDecimal getSldoanhSoTruocVAT() {
		return sldoanhSoTruocVAT;
	}
	public void setSldoanhSoTruocVAT(BigDecimal sldoanhSoTruocVAT) {
		this.sldoanhSoTruocVAT = sldoanhSoTruocVAT;
	}
	public BigDecimal getSldoanhSoSauVAT() {
		return sldoanhSoSauVAT;
	}
	public void setSldoanhSoSauVAT(BigDecimal sldoanhSoSauVAT) {
		this.sldoanhSoSauVAT = sldoanhSoSauVAT;
	}
	public String getKeyShop() {
		return keyShop;
	}
	public void setKeyShop(String keyShop) {
		this.keyShop = keyShop;
	}
	public String getMuc() {
		return muc;
	}
	public void setMuc(String muc) {
		this.muc = muc;
	}
	public Integer getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(Integer soLuong) {
		this.soLuong = soLuong;
	}
	public String getSlthudatsl() {
		return slthudatsl;
	}
	public void setSlthudatsl(String slthudatsl) {
		this.slthudatsl = slthudatsl;
	}
	public String getSlthudatds() {
		return slthudatds;
	}
	public void setSlthudatds(String slthudatds) {
		this.slthudatds = slthudatds;
	}
	public String getNgayDatHang() {
		return ngayDatHang;
	}
	public void setNgayDatHang(String ngayDatHang) {
		this.ngayDatHang = ngayDatHang;
	}
	public String getNgayDuyetDon() {
		return ngayDuyetDon;
	}
	public void setNgayDuyetDon(String ngayDuyetDon) {
		this.ngayDuyetDon = ngayDuyetDon;
	}
	public String getSoDH() {
		return soDH;
	}
	public void setSoDH(String soDH) {
		this.soDH = soDH;
	}
	public String getTaoTren() {
		return taoTren;
	}
	public void setTaoTren(String taoTren) {
		this.taoTren = taoTren;
	}
	public String getTrangThai() {
		return TrangThai;
	}
	public void setTrangThai(String trangThai) {
		TrangThai = trangThai;
	}
	public String getGiaTriDH() {
		return giaTriDH;
	}
	public void setGiaTriDH(String giaTriDH) {
		this.giaTriDH = giaTriDH;
	}
	public String getChietKhau() {
		return chietKhau;
	}
	public void setChietKhau(String chietKhau) {
		this.chietKhau = chietKhau;
	}
	public String getLoai() {
		return loai;
	}
	public void setLoai(String loai) {
		this.loai = loai;
	}
	public String getGhiChu() {
		return ghiChu;
	}
	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}
	public BigDecimal getSlthanhTienSauThue() {
		return slthanhTienSauThue;
	}
	public void setSlthanhTienSauThue(BigDecimal slthanhTienSauThue) {
		this.slthanhTienSauThue = slthanhTienSauThue;
	}
	public BigDecimal getSlchietKhau() {
		return slchietKhau;
	}
	public void setSlchietKhau(BigDecimal slchietKhau) {
		this.slchietKhau = slchietKhau;
	}
	public BigDecimal getSlgiaTriDH() {
		return slgiaTriDH;
	}
	public void setSlgiaTriDH(BigDecimal slgiaTriDH) {
		this.slgiaTriDH = slgiaTriDH;
	}
	public Integer getSlsoDH() {
		return slsoDH;
	}
	public void setSlsoDH(Integer slsoDH) {
		this.slsoDH = slsoDH;
	}
	public List<DynamicVO> getLstColumns() {
		return lstColumns;
	}
	public void setLstColumns(List<DynamicVO> lstColumns) {
		this.lstColumns = lstColumns;
	}
	public String getLoaiDH() {
		return loaiDH;
	}
	public void setLoaiDH(String loaiDH) {
		this.loaiDH = loaiDH;
	}
	public String getSoNha() {
		return soNha;
	}
	public void setSoNha(String soNha) {
		this.soNha = soNha;
	}
	public String getDuong() {
		return duong;
	}
	public void setDuong(String duong) {
		this.duong = duong;
	}
	public String getDiDong() {
		return diDong;
	}
	public void setDiDong(String diDong) {
		this.diDong = diDong;
	}
	public String getTaiKhoan() {
		return taiKhoan;
	}
	public void setTaiKhoan(String taiKhoan) {
		this.taiKhoan = taiKhoan;
	}
	public String getNganHang() {
		return nganHang;
	}
	public void setNganHang(String nganHang) {
		this.nganHang = nganHang;
	}
	public String getChiNhanh() {
		return chiNhanh;
	}
	public void setChiNhanh(String chiNhanh) {
		this.chiNhanh = chiNhanh;
	}
	public String getChuTk() {
		return chuTk;
	}
	public void setChuTk(String chuTk) {
		this.chuTk = chuTk;
	}
	public String getChuKy() {
		return chuKy;
	}
	public void setChuKy(String chuKy) {
		this.chuKy = chuKy;
	}
	public String getHuyen() {
		return huyen;
	}
	public void setHuyen(String huyen) {
		this.huyen = huyen;
	}
	public String getTinh() {
		return tinh;
	}
	public void setTinh(String tinh) {
		this.tinh = tinh;
	}
	public String getXa() {
		return xa;
	}
	public void setXa(String xa) {
		this.xa = xa;
	}
	public String getMaKho() {
		return maKho;
	}
	public void setMaKho(String maKho) {
		this.maKho = maKho;
	}
	public BigDecimal getTonDau() {
		return tonDau;
	}
	public void setTonDau(BigDecimal tonDau) {
		this.tonDau = tonDau;
	}
	public BigDecimal getTonCuoi() {
		return tonCuoi;
	}
	public void setTonCuoi(BigDecimal tonCuoi) {
		this.tonCuoi = tonCuoi;
	}
	public BigDecimal getTongNhap() {
		return tongNhap;
	}
	public void setTongNhap(BigDecimal tongNhap) {
		this.tongNhap = tongNhap;
	}
	public BigDecimal getTongXuat() {
		return tongXuat;
	}
	public void setTongXuat(BigDecimal tongXuat) {
		this.tongXuat = tongXuat;
	}
	public String getTenKho() {
		return tenKho;
	}
	public void setTenKho(String tenKho) {
		this.tenKho = tenKho;
	}
	public String getSoPODVKH() {
		return soPODVKH;
	}
	public void setSoPODVKH(String soPODVKH) {
		this.soPODVKH = soPODVKH;
	}
	public String getNghiepVu() {
		return nghiepVu;
	}
	public void setNghiepVu(String nghiepVu) {
		this.nghiepVu = nghiepVu;
	}
	public String getPoConfirm() {
		return poConfirm;
	}
	public void setPoConfirm(String poConfirm) {
		this.poConfirm = poConfirm;
	}
	public String getNgayOrder() {
		return ngayOrder;
	}
	public void setNgayOrder(String ngayOrder) {
		this.ngayOrder = ngayOrder;
	}
	public String getNgayXK() {
		return ngayXK;
	}
	public void setNgayXK(String ngayXK) {
		this.ngayXK = ngayXK;
	}
	public String getNgayNhan() {
		return ngayNhan;
	}
	public void setNgayNhan(String ngayNhan) {
		this.ngayNhan = ngayNhan;
	}
	public Integer getSlOrder() {
		return slOrder;
	}
	public void setSlOrder(Integer slOrder) {
		this.slOrder = slOrder;
	}
	public Integer getSlNhap() {
		return slNhap;
	}
	public void setSlNhap(Integer slNhap) {
		this.slNhap = slNhap;
	}
	public Integer getConLai() {
		return conLai;
	}
	public void setConLai(Integer conLai) {
		this.conLai = conLai;
	}
	public BigDecimal getDonGia() {
		return donGia;
	}
	public void setDonGia(BigDecimal donGia) {
		this.donGia = donGia;
	}
	public BigDecimal getThanhTien() {
		return thanhTien;
	}
	public void setThanhTien(BigDecimal thanhTien) {
		this.thanhTien = thanhTien;
	}
	public String getMaKiemKe() {
		return maKiemKe;
	}
	public void setMaKiemKe(String maKiemKe) {
		this.maKiemKe = maKiemKe;
	}
	public String getNgayKiemKe() {
		return ngayKiemKe;
	}
	public void setNgayKiemKe(String ngayKiemKe) {
		this.ngayKiemKe = ngayKiemKe;
	}
	public String getNgayDuyet() {
		return ngayDuyet;
	}
	public void setNgayDuyet(String ngayDuyet) {
		this.ngayDuyet = ngayDuyet;
	}
	public String getNguoiDuyet() {
		return nguoiDuyet;
	}
	public void setNguoiDuyet(String nguoiDuyet) {
		this.nguoiDuyet = nguoiDuyet;
	}
	public Integer getSlTonCuoi() {
		return slTonCuoi;
	}
	public void setSlTonCuoi(Integer slTonCuoi) {
		this.slTonCuoi = slTonCuoi;
	}
	public Integer getSlKiemKe() {
		return slKiemKe;
	}
	public void setSlKiemKe(Integer slKiemKe) {
		this.slKiemKe = slKiemKe;
	}
	public Integer getNhapNuti() {
		return nhapNuti;
	}
	public void setNhapNuti(Integer nhapNuti) {
		this.nhapNuti = nhapNuti;
	}
	public Integer getTraNuti() {
		return traNuti;
	}
	public void setTraNuti(Integer traNuti) {
		this.traNuti = traNuti;
	}
	public Integer getXuatNuti() {
		return xuatNuti;
	}
	public void setXuatNuti(Integer xuatNuti) {
		this.xuatNuti = xuatNuti;
	}
	public Integer getTraNutiKH() {
		return traNutiKH;
	}
	public void setTraNutiKH(Integer traNutiKH) {
		this.traNutiKH = traNutiKH;
	}
	public Integer getChenhLech() {
		return chenhLech;
	}
	public void setChenhLech(Integer chenhLech) {
		this.chenhLech = chenhLech;
	}
	public Integer getHangDiDuong() {
		return hangDiDuong;
	}
	public void setHangDiDuong(Integer hangDiDuong) {
		this.hangDiDuong = hangDiDuong;
	}
	public Integer getXuatDc() {
		return xuatDc;
	}
	public void setXuatDc(Integer xuatDc) {
		this.xuatDc = xuatDc;
	}
	public Integer getNhapDc() {
		return nhapDc;
	}
	public void setNhapDc(Integer nhapDc) {
		this.nhapDc = nhapDc;
	}
	public String getTuNpp() {
		return tuNpp;
	}
	public void setTuNpp(String tuNpp) {
		this.tuNpp = tuNpp;
	}
	public String getDenNpp() {
		return denNpp;
	}
	public void setDenNpp(String denNpp) {
		this.denNpp = denNpp;
	}
	public Integer getQuantityRetail() {
		return quantityRetail;
	}
	public void setQuantityRetail(Integer quantityRetail) {
		this.quantityRetail = quantityRetail;
	}
	public Integer getSumQuantityRetail() {
		return sumQuantityRetail;
	}
	public void setSumQuantityRetail(Integer sumQuantityRetail) {
		this.sumQuantityRetail = sumQuantityRetail;
	}
	public Integer getQuantityPackage() {
		return quantityPackage;
	}
	public void setQuantityPackage(Integer quantityPackage) {
		this.quantityPackage = quantityPackage;
	}
	public Integer getSumQuantityPackage() {
		return sumQuantityPackage;
	}
	public void setSumQuantityPackage(Integer sumQuantityPackage) {
		this.sumQuantityPackage = sumQuantityPackage;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getSumQuantity() {
		return sumQuantity;
	}
	public void setSumQuantity(Integer sumQuantity) {
		this.sumQuantity = sumQuantity;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getSumAmount() {
		return sumAmount;
	}
	public void setSumAmount(BigDecimal sumAmount) {
		this.sumAmount = sumAmount;
	}
	public Integer getXuatKM() {
		return xuatKM;
	}
	public void setXuatKM(Integer xuatKM) {
		this.xuatKM = xuatKM;
	}
	public Integer getXuatVansale() {
		return xuatVansale;
	}
	public void setXuatVansale(Integer xuatVansale) {
		this.xuatVansale = xuatVansale;
	}
	public Integer getNhapVansale() {
		return nhapVansale;
	}
	public void setNhapVansale(Integer nhapVansale) {
		this.nhapVansale = nhapVansale;
	}
}

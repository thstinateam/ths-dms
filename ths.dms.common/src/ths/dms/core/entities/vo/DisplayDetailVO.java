package ths.dms.core.entities.vo;

import java.io.Serializable;

public class DisplayDetailVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 35238790802725829L;
	private Long idDisplayDetail;
	private String CTTB;
	private String displayCode;
	private String productCode;
	private String nameProduct;
	private String countCTTB;
	private Integer isTrue;
	
	public Long getIdDisplayDetail() {
		return idDisplayDetail;
	}
	public void setIdDisplayDetail(Long idDisplayDetail) {
		this.idDisplayDetail = idDisplayDetail;
	}
	public String getNameProduct() {
		return nameProduct;
	}
	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}
	public String getCTTB() {
		return CTTB;
	}
	public void setCTTB(String cTTB) {
		CTTB = cTTB;
	}
	public String getCountCTTB() {
		return countCTTB;
	}
	public void setCountCTTB(String countCTTB) {
		this.countCTTB = countCTTB;
	}
	public Integer getIsTrue() {
		return isTrue;
	}
	public void setIsTrue(Integer isTrue) {
		this.isTrue = isTrue;
	}
	public String getDisplayCode() {
		return displayCode;
	}
	public void setDisplayCode(String displayCode) {
		this.displayCode = displayCode;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;


public class GroupStaffPayrollVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String groupStaffPrCode;
	private String groupStaffPrName;
	private Integer objectApply;
	private Integer isHasUsed;
	private String channelTypeCode;
	private String channelTypeName;
	private Integer staffTypeId;
	private Long id;
	
	public String getGroupStaffPrCode() {
		return groupStaffPrCode;
	}
	public void setGroupStaffPrCode(String groupStaffPrCode) {
		this.groupStaffPrCode = groupStaffPrCode;
	}
	public String getGroupStaffPrName() {
		return groupStaffPrName;
	}
	public void setGroupStaffPrName(String groupStaffPrName) {
		this.groupStaffPrName = groupStaffPrName;
	}
	public Integer getObjectApply() {
		return objectApply;
	}
	public void setObjectApply(Integer objectApply) {
		this.objectApply = objectApply;
	}
	public Integer getIsHasUsed() {
		return isHasUsed;
	}
	public void setIsHasUsed(Integer isHasUsed) {
		this.isHasUsed = isHasUsed;
	}
	public String getChannelTypeCode() {
		return channelTypeCode;
	}
	public void setChannelTypeCode(String channelTypeCode) {
		this.channelTypeCode = channelTypeCode;
	}
	public String getChannelTypeName() {
		return channelTypeName;
	}
	public void setChannelTypeName(String channelTypeName) {
		this.channelTypeName = channelTypeName;
	}
	public Integer getStaffTypeId() {
		return staffTypeId;
	}
	public void setStaffTypeId(Integer staffTypeId) {
		this.staffTypeId = staffTypeId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}

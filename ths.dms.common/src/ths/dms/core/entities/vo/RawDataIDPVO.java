/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VO bao cao raw data
 * @author trietptm
 * @since Nov 05, 2015
 */
public class RawDataIDPVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String regionCode;
	private String areaCode;
	private String shopCode;
	private String shopName;
	private String staffCode;
	private String staffName;
	private String routingCode;
	private String routingName;
	private String shortCode;
	private String customerName;
	private String houseNumber;
	private String street;
	private String precinct;
	private String districtName;
	private String provinceName;
	private String orderNumber;
	private String refOrderNumber;
	private String orderType;
	private String orderSource;
	private String orderDate;
	private String deliveryDate;
	private String warehouseCode;
	private String warehouseName;
	private String productCode;
	private String productName;
	private BigDecimal price;
	private Integer convfact;
	private Integer quantityPackage;
	private Integer quantityRetail;
	private BigDecimal amount;
	private String programCode;
	private String programType;
	private BigDecimal discountAmount;
	private Integer promoPackage;
	private Integer promoRetail;
	private Integer approved;
	private Integer approvedStep;
	private String status;
	private Integer type;
	
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getRoutingCode() {
		return routingCode;
	}
	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}
	public String getRoutingName() {
		return routingName;
	}
	public void setRoutingName(String routingName) {
		this.routingName = routingName;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getPrecinct() {
		return precinct;
	}
	public void setPrecinct(String precinct) {
		this.precinct = precinct;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getRefOrderNumber() {
		return refOrderNumber;
	}
	public void setRefOrderNumber(String refOrderNumber) {
		this.refOrderNumber = refOrderNumber;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getOrderSource() {
		return orderSource;
	}
	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getWarehouseCode() {
		return warehouseCode;
	}
	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}
	public String getWarehouseName() {
		return warehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public Integer getQuantityPackage() {
		return quantityPackage;
	}
	public void setQuantityPackage(Integer quantityPackage) {
		this.quantityPackage = quantityPackage;
	}
	public Integer getQuantityRetail() {
		return quantityRetail;
	}
	public void setQuantityRetail(Integer quantityRetail) {
		this.quantityRetail = quantityRetail;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getProgramCode() {
		return programCode;
	}
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}
	public String getProgramType() {
		return programType;
	}
	public void setProgramType(String programType) {
		this.programType = programType;
	}
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Integer getPromoPackage() {
		return promoPackage;
	}
	public void setPromoPackage(Integer promoPackage) {
		this.promoPackage = promoPackage;
	}
	public Integer getPromoRetail() {
		return promoRetail;
	}
	public void setPromoRetail(Integer promoRetail) {
		this.promoRetail = promoRetail;
	}
	public Integer getApproved() {
		return approved;
	}
	public void setApproved(Integer approved) {
		this.approved = approved;
	}
	public Integer getApprovedStep() {
		return approvedStep;
	}
	public void setApprovedStep(Integer approvedStep) {
		this.approvedStep = approvedStep;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * @author hunglm16
 * @since AUGUST 14,2014
 * @description Dung lay du lieu co ban cua Shop, yeu cau khong duoc them doi thuong la Object vao day
 * */
public class ShopVO implements Serializable {
	
	private static final long serialVersionUID = -2293374973367476361L;
	
	private Long id;
	private Long shopId;
	private Long parentId;
	private Long shopTypeId;
	private Long parentShopId;
	private Long ksShopTagetId;
	
	private Integer isLevel;
	private Integer status;
	private Integer objectType;
	private Integer shopChannel;
	private Integer quantityTarget;
	private Integer quantity;
	
	private String shopCode;
	private String shopName;
	private String abbreviation;
	private String orgName;
	private String parentShopCode;
	private String phone;
	private String mobiphone;
	private String fax;
	private String email;
	private String taxNum;
	private String contactName;
	private String billTo;
	private String shipTo;
	private String precinctName;
	private String areaName;
	private String address;
	private String latlng;//
	private String shopType;
	private String parentShopName;
	private String lockDateStr;
	private String nextLockDateStr;
	
	private Float lng;
	private Float lat;
	
	private Integer levelShowPrice;    // dh2 return shop_param
	private Integer value; //

	private String errorTypeStr;
	private String statusLogStr;
	
	public ShopVO clone(ShopVO vo) {
		ShopVO a = new ShopVO();
		a.shopId = vo.shopId;
		a.shopCode = vo.shopCode;
		a.shopName = vo.shopName;
		a.parentId = vo.parentId;
		a.isLevel = vo.isLevel;
		a.status = vo.status;
		a.objectType = vo.objectType;
		a.shopChannel = vo.shopChannel;
		return a;
	}

	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * @return
	 */
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getShopTypeId() {
		return shopTypeId;
	}

	public void setShopTypeId(Long shopTypeId) {
		this.shopTypeId = shopTypeId;
	}

	public Long getParentShopId() {
		return parentShopId;
	}

	public void setParentShopId(Long parentShopId) {
		this.parentShopId = parentShopId;
	}

	public Long getKsShopTagetId() {
		return ksShopTagetId;
	}

	public void setKsShopTagetId(Long ksShopTagetId) {
		this.ksShopTagetId = ksShopTagetId;
	}

	public Integer getIsLevel() {
		return isLevel;
	}

	public void setIsLevel(Integer isLevel) {
		this.isLevel = isLevel;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public Integer getShopChannel() {
		return shopChannel;
	}

	public void setShopChannel(Integer shopChannel) {
		this.shopChannel = shopChannel;
	}

	public Integer getQuantityTarget() {
		return quantityTarget;
	}

	public void setQuantityTarget(Integer quantityTarget) {
		this.quantityTarget = quantityTarget;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getParentShopCode() {
		return parentShopCode;
	}

	public void setParentShopCode(String parentShopCode) {
		this.parentShopCode = parentShopCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobiphone() {
		return mobiphone;
	}

	public void setMobiphone(String mobiphone) {
		this.mobiphone = mobiphone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTaxNum() {
		return taxNum;
	}

	public void setTaxNum(String taxNum) {
		this.taxNum = taxNum;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getBillTo() {
		return billTo;
	}

	public void setBillTo(String billTo) {
		this.billTo = billTo;
	}

	public String getShipTo() {
		return shipTo;
	}

	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}

	public String getPrecinctName() {
		return precinctName;
	}

	public void setPrecinctName(String precinctName) {
		this.precinctName = precinctName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLatlng() {
		return latlng;
	}

	public void setLatlng(String latlng) {
		this.latlng = latlng;
	}

	public String getShopType() {
		return shopType;
	}

	public void setShopType(String shopType) {
		this.shopType = shopType;
	}

	public String getParentShopName() {
		return parentShopName;
	}

	public void setParentShopName(String parentShopName) {
		this.parentShopName = parentShopName;
	}

	public String getLockDateStr() {
		return lockDateStr;
	}

	public void setLockDateStr(String lockDateStr) {
		this.lockDateStr = lockDateStr;
	}

	public String getNextLockDateStr() {
		return nextLockDateStr;
	}

	public void setNextLockDateStr(String nextLockDateStr) {
		this.nextLockDateStr = nextLockDateStr;
	}

	public Float getLng() {
		return lng;
	}

	public void setLng(Float lng) {
		this.lng = lng;
	}

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Integer getLevelShowPrice() {
		return levelShowPrice;
	}

	public void setLevelShowPrice(Integer levelShowPrice) {
		this.levelShowPrice = levelShowPrice;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getErrorTypeStr() {
		return errorTypeStr;
	}

	public void setErrorTypeStr(String errorTypeStr) {
		this.errorTypeStr = errorTypeStr;
	}

	public String getStatusLogStr() {
		return statusLogStr;
	}

	public void setStatusLogStr(String statusLogStr) {
		this.statusLogStr = statusLogStr;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
}

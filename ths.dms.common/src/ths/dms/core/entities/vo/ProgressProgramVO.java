/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VO bao cao tien do thuc hien chuong trinh
 * @author trietptm
 * @since Nov 28, 2015
 */
public class ProgressProgramVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String regionCode;
	private String areaCode;
	private String shopCode;
	private String shopName;
	private String staffCode;
	private String staffName;
	private Integer quantity;
	private Integer quantityCustomer;
	private Double quantityPercent;
	private BigDecimal amountTarget;
	private BigDecimal amount;
	private BigDecimal amountRemain;
	private Double amountPercent;
	
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getQuantityCustomer() {
		return quantityCustomer;
	}
	public void setQuantityCustomer(Integer quantityCustomer) {
		this.quantityCustomer = quantityCustomer;
	}
	public Double getQuantityPercent() {
		return quantityPercent;
	}
	public void setQuantityPercent(Double quantityPercent) {
		this.quantityPercent = quantityPercent;
	}
	public BigDecimal getAmountTarget() {
		return amountTarget;
	}
	public void setAmountTarget(BigDecimal amountTarget) {
		this.amountTarget = amountTarget;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getAmountRemain() {
		return amountRemain;
	}
	public void setAmountRemain(BigDecimal amountRemain) {
		this.amountRemain = amountRemain;
	}
	public Double getAmountPercent() {
		return amountPercent;
	}
	public void setAmountPercent(Double amountPercent) {
		this.amountPercent = amountPercent;
	}

}

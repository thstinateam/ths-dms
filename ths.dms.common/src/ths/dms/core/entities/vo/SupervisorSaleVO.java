package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class SupervisorSaleVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3163638741517621531L;
	Long shopId;
	String shopCode;
	String shopName;
	Long staffId;
	String staffCode;
	String staffName;
	Float lat;
	Float lng;
	BigDecimal dayAmountPlan;
	BigDecimal dayAmount;
	BigDecimal dayAmountApproved;
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Float getLat() {
		return lat;
	}
	public void setLat(Float lat) {
		this.lat = lat;
	}
	public Float getLng() {
		return lng;
	}
	public void setLng(Float lng) {
		this.lng = lng;
	}
	public BigDecimal getDayAmountPlan() {
		return dayAmountPlan;		
	}
	public void setDayAmountPlan(BigDecimal dayAmountPlan) {
		this.dayAmountPlan = dayAmountPlan;
	}
	public BigDecimal getDayAmount() {
		if(dayAmount != null) {
			BigDecimal __dayAmount = dayAmount.divide(BigDecimal.valueOf(1000)).setScale(0, RoundingMode.HALF_DOWN);
			return __dayAmount;
		} else {
			return BigDecimal.ZERO;
		}
	}
	public void setDayAmount(BigDecimal dayAmount) {
		this.dayAmount = dayAmount;
	}
	
	public BigDecimal getDayAmountApproved() {
		if(dayAmountApproved != null) {
			BigDecimal __dayAmountApproved = dayAmountApproved.divide(BigDecimal.valueOf(1000)).setScale(0, RoundingMode.HALF_DOWN);
			return __dayAmountApproved;
		} else {
			return BigDecimal.ZERO;
		}
	}
	public void setDayAmountApproved(BigDecimal dayAmountApproved) {
		this.dayAmountApproved = dayAmountApproved;
	}
	public BigDecimal getDayAmountLeft() {
		BigDecimal dayAmountLeft = null;
		dayAmountLeft = dayAmountPlan != null && dayAmount != null && dayAmountPlan.compareTo(dayAmount) > 0 ? dayAmountPlan.subtract(dayAmount) : BigDecimal.ZERO;
		BigDecimal __dayAmountLeft = dayAmountLeft.divide(BigDecimal.valueOf(1000)).setScale(0, RoundingMode.HALF_DOWN);
		return __dayAmountLeft;
	}
}

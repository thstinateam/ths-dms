package ths.dms.core.entities.vo;

import java.io.Serializable;

public class ReportVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long reportId;
	private String catCode;
	private String catName;
	
	private Long subCatId;
	private String subCatCode;
	private String subCatName;
	
	private Long productId;
	private String productCode;
	private String productName;
	private String catelogyCode;
	private Integer quantity;
	private Integer convfact;
	
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the catelogyCode
	 */
	public String getCatelogyCode() {
		return catelogyCode;
	}
	/**
	 * @param catelogyCode the catelogyCode to set
	 */
	public void setCatelogyCode(String catelogyCode) {
		this.catelogyCode = catelogyCode;
	}
	public Long getReportId() {
		return reportId;
	}
	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}
	public String getCatCode() {
		return catCode;
	}
	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	public Long getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(Long subCatId) {
		this.subCatId = subCatId;
	}
	public String getSubCatCode() {
		return subCatCode;
	}
	public void setSubCatCode(String subCatCode) {
		this.subCatCode = subCatCode;
	}
	public String getSubCatName() {
		return subCatName;
	}
	public void setSubCatName(String subCatName) {
		this.subCatName = subCatName;
	}
	
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	
}

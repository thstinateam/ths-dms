package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author hunglm16
 * @since AUGUST 14,2014
 * @description Tao cay co ban dung cho tich hop, yeu cau ke thua khong tao them truong moi
 * */
public class TreeVOBasic<T> implements Serializable 
{
	//TREE BASIC
	private Long id;
	private String code;
	private String name;
	private Long parentId;
	private Integer isLevel;
	private Integer isNode;
	private Boolean flag;
	private T attribute;
	private List<TreeVOBasic<T>> children;
	private String data;
	private String text;
	private String state;
	private Integer objectType;
	private String url;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getState() {
		if(state==null || state.trim().length() == 0){
			state = "closed";
		}
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public List<TreeVOBasic<T>> getChildren() {
		return children;
	}
	public void setChildren(List<TreeVOBasic<T>> children) {
		this.children = children;
	}
	public T getAttribute() {
		return attribute;
	}
	public void setAttribute(T attribute) {
		this.attribute = attribute;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Integer getIsLevel() {
		return isLevel;
	}
	public void setIsLevel(Integer isLevel) {
		this.isLevel = isLevel;
	}
	public Integer getIsNode() {
		return isNode;
	}
	public void setIsNode(Integer isNode) {
		this.isNode = isNode;
	}
	public Boolean getFlag() {
		return flag;
	}
	public void setFlag(Boolean flag) {
		this.flag = flag;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
}

/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * VO map chi tiet sale_order de xuat ra file xml
 * 
 * @author lacnv1
 * @since Mar 14, 2015
 */
public class OrderXmlDetailVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<OrderXmlLineDataVO> lineDetails;
	
	public OrderXmlDetailVO() {
		lineDetails = new ArrayList<OrderXmlLineDataVO>();
	}

	public void addLine(OrderXmlLineDataVO lineDetail) {
		lineDetails.add(lineDetail);
	}

	public List<OrderXmlLineDataVO> getLineDetails() {
		return lineDetails;
	}

	public void setLineDetails(List<OrderXmlLineDataVO> lineDetails) {
		this.lineDetails = lineDetails;
	}
}
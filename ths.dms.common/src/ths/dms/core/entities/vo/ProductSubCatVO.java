package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;

import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;

public class ProductSubCatVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ProductInfo productInfo;
	private ArrayList<Product> lstProduct = new ArrayList<Product>();

	public ProductInfo getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}

	public ArrayList<Product> getLstProduct() {
		return lstProduct;
	}

	public void setLstProduct(ArrayList<Product> lstProduct) {
		this.lstProduct = lstProduct;
	}

}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.common.utils.GroupUtility;
import ths.dms.core.entities.Invoice;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.enumtype.InvoiceCustPayment;

public class SaleOrderVOEx implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4970295328021146021L;
	
	private String rowId;
	
	private SaleOrder saleOrder;
	
	private List<SaleOrderDetail> lstSaleOrderDetail;
	
	private Integer conLai; //so dong sale order detail con co the chua dc trong 1 hoa don VAT
	
	private boolean hasFreeItem = false; // don co chua san pham khuyen mai (isFreeItem = 1)
	
	private Invoice invoice;
	
	private InvoiceCustPayment custPayment;
	
	private BigDecimal amount;
	
	private BigDecimal taxAmount;
	
	private BigDecimal discount;
	
	private List<Long> lstSaleOrderId; //chi dung cho don gop
	
	private Long sku;
	
	private String orderNumber;
	private String invoiceNumber;
	private String payment;
	private String total;
	private String customer;
	private String customerAddr;
	private String staffInfo;
	private String deliveryStaffInfo;
	private String date;
	private String stringVat;
	private String diachiKH;
	private String diachiHD;
	private String taxTotal;
	private String invoiceCompanyName;
	private String cpnyName;
	private boolean check;
	
	
	public boolean isHasFreeItem() {
		return hasFreeItem;
	}

	public void setHasFreeItem(boolean hasFreeItem) {
		this.hasFreeItem = hasFreeItem;
	}

	public String getRowId() {
		return rowId;
	}

	public Integer getConLai() {
		return conLai;
	}

	public void setConLai(Integer conLai) {
		this.conLai = conLai;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	public List<SaleOrderDetail> getLstSaleOrderDetail() {
		return lstSaleOrderDetail;
	}

	public void setLstSaleOrderDetail(List<SaleOrderDetail> lstSaleOrderDetail) {
		this.lstSaleOrderDetail = lstSaleOrderDetail;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public InvoiceCustPayment getCustPayment() {
		return custPayment;
	}

	public void setCustPayment(InvoiceCustPayment custPayment) {
		this.custPayment = custPayment;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setSku(Long sku) {
		this.sku = sku;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getCustomerAddr() {
		return customerAddr;
	}

	public void setCustomerAddr(String customerAddr) {
		this.customerAddr = customerAddr;
	}

	public String getStaffInfo() {
		return staffInfo;
	}

	public void setStaffInfo(String staffInfo) {
		this.staffInfo = staffInfo;
	}

	public String getDeliveryStaffInfo() {
		return deliveryStaffInfo;
	}

	public void setDeliveryStaffInfo(String deliveryStaffInfo) {
		this.deliveryStaffInfo = deliveryStaffInfo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStringVat() {
		return stringVat;
	}

	public void setStringVat(String stringVat) {
		this.stringVat = stringVat;
	}

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	public Long getSku() {
		this.sku = 0L;
		if(lstSaleOrderDetail != null) {
			for(int i = 0; i < lstSaleOrderDetail.size();i++) {
				if(lstSaleOrderDetail.get(i).getIsFreeItem() == 0) {
					sku = sku + 1;
				}
			}
		}
		return sku;
	}
	
	public String getDiachiKH() {
		return diachiKH;
	}

	public void setDiachiKH(String diachiKH) {
		this.diachiKH = diachiKH;
	}

	public String getDiachiHD() {
		return diachiHD;
	}

	public void setDiachiHD(String diachiHD) {
		this.diachiHD = diachiHD;
	}

	public String getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(String taxTotal) {
		this.taxTotal = taxTotal;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getInvoiceCompanyName() {
		return invoiceCompanyName;
	}

	public void setInvoiceCompanyName(String invoiceCompanyName) {
		this.invoiceCompanyName = invoiceCompanyName;
	}
	
	
	public List<Long> getLstSaleOrderId() {
		return lstSaleOrderId;
	}

	public void setLstSaleOrderId(List<Long> lstSaleOrderId) {
		this.lstSaleOrderId = lstSaleOrderId;
	}

	public void formatData(){
		orderNumber = saleOrder.getOrderNumber();
		invoiceNumber = "";
		payment = "Tiền mặt";
		if(discount==null){
			discount = BigDecimal.ZERO;
		}
		BigDecimal thanhTien = amount.subtract(discount);
		total = GroupUtility.convertMoney(thanhTien);
		taxTotal = GroupUtility.convertMoney(taxAmount);
		customer = saleOrder.getCustomer().getShortCode() + " - " + saleOrder.getCustomer().getCustomerName();
		customerAddr = saleOrder.getCustomer().getAddress();
		staffInfo =  saleOrder.getStaff() == null ? "" : saleOrder.getStaff().getStaffCode();
		deliveryStaffInfo = saleOrder.getDelivery() == null ? "" : saleOrder.getDelivery().getStaffCode();
		date = GroupUtility.toDateSimpleFormatString(saleOrder.getOrderDate());
//		if(stringVat == null || stringVat.isEmpty()){
//			stringVat = (saleOrder.getVat() == null ? "" : (lstSaleOrderDetail == null || lstSaleOrderDetail.size() == 0) ? "" : this.getVATListSaleOrderDetail(lstSaleOrderDetail)) + "%";
//		}
		invoiceCompanyName = saleOrder.getCustomer().getInvoiceConpanyName() == null ? "" : saleOrder.getCustomer().getInvoiceConpanyName();
		check = false;
	}
	
	public Float getVATListSaleOrderDetail(List<SaleOrderDetail> listDetail){
		for (SaleOrderDetail saleOrderDetail : listDetail) {
			if(saleOrderDetail.getIsFreeItem().equals(0) && saleOrderDetail.getVat() > 0){
				return saleOrderDetail.getVat();
			}
		}
		return 0F;
	}

	public String getCpnyName() {
		return cpnyName;
	}

	public void setCpnyName(String cpnyName) {
		this.cpnyName = cpnyName;
	}
}

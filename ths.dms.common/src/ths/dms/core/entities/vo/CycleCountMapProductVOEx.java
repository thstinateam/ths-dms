package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.CycleCountMapProduct;
import ths.dms.core.entities.CycleCountResult;

public class CycleCountMapProductVOEx implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private CycleCountMapProduct cycleCountMapProduct;
	
	private List<CycleCountResult> lstCycleCountResult;

	public CycleCountMapProduct getCycleCountMapProduct() {
		return cycleCountMapProduct;
	}

	public void setCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct) {
		this.cycleCountMapProduct = cycleCountMapProduct;
	}

	public List<CycleCountResult> getLstCycleCountResult() {
		return lstCycleCountResult;
	}

	public void setLstCycleCountResult(List<CycleCountResult> lstCycleCountResult) {
		this.lstCycleCountResult = lstCycleCountResult;
	}

}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class GS_1_2_VO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	
	private String shopLevel2Code;
	private String shopLevel1Code;
	private String maNPP;
	private String tenNPP;
	private String maGSNPP;
	private String tenGSNPP;
	private String maNVBH;
	private String tenNVBH;
	private String ngay;
	private String batDauLamViec;
	private String ketThucLamViec;
	private String thoiGianChamCong;
	private String dungGioOrTreGio;
	private String khoangCachChamCong;
	private String batDauChamCong;
	private String ketThucChamCong;
	private String khoangCachQuyDinh;
	public String getShopLevel2Code() {
		return shopLevel2Code;
	}
	public void setShopLevel2Code(String shopLevel2Code) {
		this.shopLevel2Code = shopLevel2Code;
	}
	public String getShopLevel1Code() {
		return shopLevel1Code;
	}
	public void setShopLevel1Code(String shopLevel1Code) {
		this.shopLevel1Code = shopLevel1Code;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaGSNPP() {
		return maGSNPP;
	}
	public void setMaGSNPP(String maGSNPP) {
		this.maGSNPP = maGSNPP;
	}
	public String getTenGSNPP() {
		return tenGSNPP;
	}
	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getNgay() {
		return ngay;
	}
	public void setNgay(String ngay) {
		this.ngay = ngay;
	}
	public String getBatDauLamViec() {
		return batDauLamViec;
	}
	public void setBatDauLamViec(String batDauLamViec) {
		this.batDauLamViec = batDauLamViec;
	}
	public String getKetThucLamViec() {
		return ketThucLamViec;
	}
	public void setKetThucLamViec(String ketThucLamViec) {
		this.ketThucLamViec = ketThucLamViec;
	}
	public String getThoiGianChamCong() {
		return thoiGianChamCong;
	}
	public void setThoiGianChamCong(String thoiGianChamCong) {
		this.thoiGianChamCong = thoiGianChamCong;
	}
	public String getDungGioOrTreGio() {
		return dungGioOrTreGio;
	}
	public void setDungGioOrTreGio(String dungGioOrTreGio) {
		this.dungGioOrTreGio = dungGioOrTreGio;
	}
	public String getKhoangCachChamCong() {
		return khoangCachChamCong;
	}
	public void setKhoangCachChamCong(String khoangCachChamCong) {
		this.khoangCachChamCong = khoangCachChamCong;
	}
	public String getBatDauChamCong() {
		return batDauChamCong;
	}
	public void setBatDauChamCong(String batDauChamCong) {
		this.batDauChamCong = batDauChamCong;
	}
	public String getKetThucChamCong() {
		return ketThucChamCong;
	}
	public void setKetThucChamCong(String ketThucChamCong) {
		this.ketThucChamCong = ketThucChamCong;
	}
	public String getKhoangCachQuyDinh() {
		return khoangCachQuyDinh;
	}
	public void setKhoangCachQuyDinh(String khoangCachQuyDinh) {
		this.khoangCachQuyDinh = khoangCachQuyDinh;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}

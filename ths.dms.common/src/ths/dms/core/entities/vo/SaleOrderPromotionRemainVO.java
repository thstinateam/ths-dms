/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * Mo ta class SaleOrderPromotionRemainVO.java
 * @author vuongmq
 * @since Jan 11, 2016
 */
public class SaleOrderPromotionRemainVO<T> implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private T remain;// gia tri shop
	
	private T remain1; // gia tri customer
	
	private T remain2; // gia tri staff
	
	private Integer typeObject; // 1: shop, 2: Staff, 3: Customner

	public T getRemain() {
		return remain;
	}

	public void setRemain(T remain) {
		this.remain = remain;
	}

	public T getRemain1() {
		return remain1;
	}

	public void setRemain1(T remain1) {
		this.remain1 = remain1;
	}

	public T getRemain2() {
		return remain2;
	}

	public void setRemain2(T remain2) {
		this.remain2 = remain2;
	}

	public Integer getTypeObject() {
		return typeObject;
	}

	public void setTypeObject(Integer typeObject) {
		this.typeObject = typeObject;
	}
	
}

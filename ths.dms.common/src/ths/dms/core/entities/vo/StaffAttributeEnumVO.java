package ths.dms.core.entities.vo;

import java.io.Serializable;
/**
 * Quan ly thuoc tinh nhan vien voi nhieu gia tri
 * @author vuongmq
 * @since 26/02/2015
 */
public class StaffAttributeEnumVO implements Serializable {

	
	private static final long serialVersionUID = -4931975943896321113L;
	
	private Long id;
	
	private Long staffAttId;
	
	private String code;
	
	private String value;
	
	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStaffAttId() {
		return staffAttId;
	}

	public void setStaffAttId(Long staffAttId) {
		this.staffAttId = staffAttId;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	
	
	
	
}

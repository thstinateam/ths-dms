package ths.dms.core.entities.vo;

import java.io.Serializable;

public class ZV03View implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Long productId;
	
	public String productCode;
	
	public String productName;
	
	public Integer quantity;
	
	public Long amount;
	
	public String freeProductCodeStr;
	
	public String pgDetailIdStr;
	
	public Long pgDetailId;
	
	public String freeProductNameStr;

	public String getFreeProductNameStr() {
		return freeProductNameStr;
	}

	public void setFreeProductNameStr(String freeProductNameStr) {
		this.freeProductNameStr = freeProductNameStr;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	

	public String getFreeProductCodeStr() {
		return freeProductCodeStr;
	}

	public void setFreeProductCodeStr(String freeProductCodeStr) {
		this.freeProductCodeStr = freeProductCodeStr;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getPgDetailIdStr() {
		return pgDetailIdStr;
	}

	public void setPgDetailIdStr(String pgDetailIdStr) {
		this.pgDetailIdStr = pgDetailIdStr;
	}

	public Long getPgDetailId() {
		return pgDetailId;
	}

	public void setPgDetailId(Long pgDetailId) {
		this.pgDetailId = pgDetailId;
	}
	
}

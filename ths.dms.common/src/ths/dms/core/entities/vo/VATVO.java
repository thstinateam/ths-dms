package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class VATVO implements Serializable {
	Integer rowNUm;
	String product;
	String uom;
	Integer quantity;
	BigDecimal price;//price not vat
	BigDecimal discount;
	String description;
	public Integer getRowNUm() {
		return rowNUm;
	}
	public void setRowNUm(Integer rowNUm) {
		this.rowNUm = rowNUm;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getQuantityStr() {
		String quantityStr = "";
		if(quantity == null) {
			return "";
		}
		String _quantity = quantity.intValue() + "";
		int isDot = 1;
		for(int i = _quantity.length(); i > 0; i--) {
			char ch = _quantity.charAt(i - 1);
			if(isDot == 3 && i != 1) {
				if(_quantity.charAt(i - 2) == '-') {
					quantityStr = ch + quantityStr;
					isDot = 0;
				} else {
					quantityStr = "." + ch + quantityStr;
					isDot = 0;
				}
			} else {
				quantityStr = ch + quantityStr;
			}
			isDot++;
		}
		return quantityStr;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public String getStrPrice() {
		String strPrice = "";
		if(price == null) {
			return "";
		}
		String _price = price.toBigInteger() + "";
		int isDot = 1;
		for(int i = _price.length(); i > 0; i--) {
			char ch = _price.charAt(i - 1);
			if(isDot == 3 && i != 1) {
				if(_price.charAt(i - 2) == '-') {
					strPrice = ch + strPrice;
					isDot = 0;
				} else {
					strPrice = "." + ch + strPrice;
					isDot = 0;
				}
			} else {
				strPrice = ch + strPrice;
			}
			isDot++;
		}
		return strPrice;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public String getStrDiscount() {
		String strDiscount = "";
		if(discount == null) {
			return "";
		}
		String _discount = discount.toBigInteger() + "";
		int isDot = 1;
		for(int i = _discount.length(); i > 0; i--) {
			char ch = _discount.charAt(i - 1);
			if(isDot == 3 && i != 1) {
				if(_discount.charAt(i - 2) == '-') {
					strDiscount = ch + strDiscount;
					isDot = 0;
				} else {
					strDiscount = "." + ch + strDiscount;
					isDot = 0;
				}
			} else {
				strDiscount = ch + strDiscount;
			}
			isDot++;
		}
		return strDiscount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}

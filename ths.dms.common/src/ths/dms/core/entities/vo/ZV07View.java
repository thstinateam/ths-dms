package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class ZV07View implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String productCodeStr;
	
	public Integer quantity;
	
	public BigDecimal amount;
	
	//-------------------------------
	
	public Float discountPercent;
	
	public BigDecimal discountAmount;
	
	public String freeProductCodeStr;

	//For detail
	
	public Long productId;
	
	public String productCode;
	
	public String productName;
	
	public Integer required;
	
	public String pgDetailIdStr;
	
	public Long pgDetailId;
	
	public String freeProductNameStr;
	
	public String productNameStr;

	public String getProductNameStr() {
		return productNameStr;
	}

	public void setProductNameStr(String productNameStr) {
		this.productNameStr = productNameStr;
	}

	public String getFreeProductNameStr() {
		return freeProductNameStr;
	}

	public void setFreeProductNameStr(String freeProductNameStr) {
		this.freeProductNameStr = freeProductNameStr;
	}

	public String getProductCodeStr() {
		return productCodeStr;
	}

	public void setProductCodeStr(String productCodeStr) {
		this.productCodeStr = productCodeStr;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Float getDiscountPercent() {
		return discountPercent;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public void setDiscountPercent(Float discountPercent) {
		this.discountPercent = discountPercent;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getRequired() {
		return required;
	}

	public void setRequired(Integer required) {
		this.required = required;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getPgDetailIdStr() {
		return pgDetailIdStr;
	}

	public void setPgDetailIdStr(String pgDetailIdStr) {
		this.pgDetailIdStr = pgDetailIdStr;
	}

	public Long getPgDetailId() {
		return pgDetailId;
	}

	public void setPgDetailId(Long pgDetailId) {
		this.pgDetailId = pgDetailId;
	}

	public String getFreeProductCodeStr() {
		return freeProductCodeStr;
	}

	public void setFreeProductCodeStr(String freeProductCodeStr) {
		this.freeProductCodeStr = freeProductCodeStr;
	}
}

/*
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * VO PROMOTION_MAP_DELTA
 *
 * @author agile_dungdq3
 * @since: 3:30:17 PM Jan 6, 2016
 */
public class PromotionMapDeltaVO implements Serializable {

	private static final long serialVersionUID = 6846868849986382406L;

	public final int SOURCE_WEB = 1;
	
	public final int SOURCE_MOBILE = 2;

	private Long promotionShopMapID;

	private Long promotionStaffMapID;
	
	private Long promotionCustomerMapID;

	/**
	 * So luong khuyen mai
	 */
	private BigDecimal quantity;
	
	/**
	 * So tien khuyen mai
	 */
	private BigDecimal amount;
	
	/**
	 * So suat khuyen mai
	 */
	private BigDecimal num;

	/**
	 * Nguồn tác động: 1: Web; 2: Mobile
	 */
	private BigDecimal source;
	
	/**
	 * Tác động: 1: Tạo đơn; 2: Chỉnh đơn; 3: Từ chối đơn; 4: Hủy đơn
	 */
	private BigDecimal action;
	
	/**
	 * ID don hang
	 */
	private Long fromObjectID;

	//Ngay cap nhat
	private Date createDate;

	//Nguoi cap nhat
	private String createUser;
	
	private Long promotionProgramID;
	
	private Long shopID;
	
	private Long staffID;
	
	private Long customerID;
	
	private String orderType;

	public Long getPromotionShopMapID() {
		return promotionShopMapID;
	}

	public void setPromotionShopMapID(Long promotionShopMap) {
		this.promotionShopMapID = promotionShopMap;
	}

	public Long getPromotionStaffMapID() {
		return promotionStaffMapID;
	}

	public void setPromotionStaffMapID(Long promotionStaffMap) {
		this.promotionStaffMapID = promotionStaffMap;
	}

	public Long getPromotionCustomerMapID() {
		return promotionCustomerMapID;
	}

	public void setPromotionCustomerMapID(Long promotionCustomerMap) {
		this.promotionCustomerMapID = promotionCustomerMap;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getNum() {
		return num;
	}

	public void setNum(BigDecimal num) {
		this.num = num;
	}

	public BigDecimal getSource() {
		return source;
	}

	public void setSource(BigDecimal source) {
		this.source = source;
	}

	public BigDecimal getAction() {
		return action;
	}

	public void setAction(BigDecimal action) {
		this.action = action;
	}

	public Long getFromObjectID() {
		return fromObjectID;
	}

	public void setFromObjectID(Long fromObjectID) {
		this.fromObjectID = fromObjectID;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getPromotionProgramID() {
		return promotionProgramID;
	}

	public void setPromotionProgramID(Long promotionProgramID) {
		this.promotionProgramID = promotionProgramID;
	}

	public Long getShopID() {
		return shopID;
	}

	public void setShopID(Long shopID) {
		this.shopID = shopID;
	}

	public Long getStaffID() {
		return staffID;
	}

	public void setStaffID(Long staffID) {
		this.staffID = staffID;
	}

	public Long getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Long customerID) {
		this.customerID = customerID;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
}

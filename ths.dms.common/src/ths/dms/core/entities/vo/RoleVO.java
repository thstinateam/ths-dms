package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hunglm16
 * @since September 10, 2014
 */
public class RoleVO implements Serializable {
	private Long roleId;
	private String roleCode;
	private String roleName;
	private List<ShopVO> listShop;
	private Long objectType;
	private Long type;
	private Long inheritUserId;
	private Integer inheritStaffSpecificType;
	private Long inheritStaffShopId;
	private String appversion;
	private StaffVO profile;
	private String inheritStaffTypeName;

	public RoleVO() {
		this.roleId = 0L;
		this.roleCode = "";
		this.roleName = "";
		this.inheritUserId = 0L;
		this.listShop = new ArrayList<ShopVO>();
		this.inheritStaffTypeName = "";
		this.inheritStaffSpecificType = 0;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<ShopVO> getListShop() {
		return listShop;
	}

	public void setListShop(List<ShopVO> listShop) {
		this.listShop = listShop;
	}

	public Long getObjectType() {
		return objectType;
	}

	public void setObjectType(Long objectType) {
		this.objectType = objectType;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getInheritUserId() {
		return inheritUserId;
	}

	public void setInheritUserId(Long inheritUserId) {
		this.inheritUserId = inheritUserId;
	}

	public Long getInheritStaffShopId() {
		return inheritStaffShopId;
	}

	public void setInheritStaffShopId(Long inheritStaffShopId) {
		this.inheritStaffShopId = inheritStaffShopId;
	}

	public String getAppversion() {
		return appversion;
	}

	public void setAppversion(String appversion) {
		this.appversion = appversion;
	}

	public StaffVO getProfile() {
		return profile;
	}

	public void setProfile(StaffVO profile) {
		this.profile = profile;
	}

	public Integer getInheritStaffSpecificType() {
		return inheritStaffSpecificType;
	}

	public void setInheritStaffSpecificType(Integer inheritStaffSpecificType) {
		this.inheritStaffSpecificType = inheritStaffSpecificType;
	}

	public String getInheritStaffTypeName() {
		return inheritStaffTypeName;
	}

	public void setInheritStaffTypeName(String inheritStaffTypeName) {
		this.inheritStaffTypeName = inheritStaffTypeName;
	}

}

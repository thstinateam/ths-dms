package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/*
 * Doi tuong khuyen mai mo rong
 * ThuatTQ
 * loctt 20sep2013
 */
public class DisplayProgrameExVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String displayProgrameCode;
	

	private Float percentMin;
	private Float percentMax;
	private String productCode;
	private BigDecimal levelAmount;
	private Integer quantity;
	private Integer numSku;
	private String shopCode;
	private Date fromDate;
	private Date toDate;
	
	//new 
	private Long displayProductGroupId;
	private Long displayProgramLevelDTLId;
	private String displayProductGroupCode;
	private String displayProductGroupName;
	private BigDecimal min;
	private BigDecimal max;
	private Float precent;
	private String levelCode;
	
	
	public Integer getNumSku() {
		return numSku;
	}
	public void setNumSku(Integer numSku) {
		this.numSku = numSku;
	}
	public String getDisplayProgrameCode() {
		return displayProgrameCode;
	}
	public void setDisplayProgrameCode(String displayProgrameCode) {
		this.displayProgrameCode = displayProgrameCode;
	}
	public String getLevelCode() {
		return levelCode;
	}
	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Float getPercentMin() {
		return percentMin;
	}
	public void setPercentMin(Float percentMin) {
		this.percentMin = percentMin;
	}
	public Float getPercentMax() {
		return percentMax;
	}
	public void setPercentMax(Float percentMax) {
		this.percentMax = percentMax;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	public BigDecimal getLevelAmount() {
		return levelAmount;
	}

	public void setLevelAmount(BigDecimal levelAmount) {
		this.levelAmount = levelAmount;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	
	public String getDisplayProductGroupCode() {
		return displayProductGroupCode;
	}
	public void setDisplayProductGroupCode(String displayProductGroupCode) {
		this.displayProductGroupCode = displayProductGroupCode;
	}
	public String getDisplayProductGroupName() {
		return displayProductGroupName;
	}
	public void setDisplayProductGroupName(String displayProductGroupName) {
		this.displayProductGroupName = displayProductGroupName;
	}
	
	public BigDecimal getMin() {
		return min;
	}
	public void setMin(BigDecimal min) {
		this.min = min;
	}
	public BigDecimal getMax() {
		return max;
	}
	public void setMax(BigDecimal max) {
		this.max = max;
	}
	public Float getPrecent() {
		return precent;
	}
	public void setPrecent(Float precent) {
		this.precent = precent;
	}
	public Long getDisplayProductGroupId() {
		return displayProductGroupId;
	}
	public void setDisplayProductGroupId(Long displayProductGroupId) {
		this.displayProductGroupId = displayProductGroupId;
	}
	public Long getDisplayProgramLevelDTLId() {
		return displayProgramLevelDTLId;
	}
	public void setDisplayProgramLevelDTLId(Long displayProgramLevelDTLId) {
		this.displayProgramLevelDTLId = displayProgramLevelDTLId;
	}
}

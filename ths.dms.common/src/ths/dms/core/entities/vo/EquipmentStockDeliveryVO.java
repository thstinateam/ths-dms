package ths.dms.core.entities.vo;

import java.io.Serializable;

public class EquipmentStockDeliveryVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Ma don vi */
	private String shopCode;
	
	/** Ten don vi */
	private String shopName;
		
	/** Id don vi */
	private Long shopId;
	
	/** Id kho */
	private Long stockId;

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
}

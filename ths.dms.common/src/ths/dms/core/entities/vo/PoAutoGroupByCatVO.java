package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PoAutoGroupByCatVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String subCat;
	private List<PoAutoVO> data;

	public PoAutoGroupByCatVO() {
		data = new ArrayList<PoAutoVO>();
	}

	public String getSubCat() {
		return subCat;
	}

	public void setSubCat(String subCat) {
		this.subCat = subCat;
	}

	public List<PoAutoVO> getData() {
		return data;
	}

	public void setData(List<PoAutoVO> data) {
		this.data = data;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * 
 * @author vuongmq
 * @since 27- August, 2014
 * @description Load grid Cap nhat phai thu ca kho
 *
 */
public class SaleOrderStockVOTemp implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long id;//saleOrderId ma don hang
	private Long shopId; // shopId
	private Long staffId; // nvbh
	private String orderNumber;
	private String orderType;
	private Integer orderSource;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShopId() {
		return shopId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public Integer getOrderSource() {
		return orderSource;
	}
	public void setOrderSource(Integer orderSource) {
		this.orderSource = orderSource;
	}
	
	
}
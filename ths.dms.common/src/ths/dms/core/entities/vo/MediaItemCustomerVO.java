package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class MediaItemCustomerVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1956654986662515109L;
	private String customerCode;
	private HashMap<String, MediaItemProgramVO> mapProgram;
	private List<MediaItemProgramVO> lstMediaItemProgram;
	
	public HashMap<String, MediaItemProgramVO> getMapProgram() {
		return mapProgram;
	}
	public void setMapProgram(HashMap<String, MediaItemProgramVO> mapProgram) {
		this.mapProgram = mapProgram;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public List<MediaItemProgramVO> getLstMediaItemProgram() {
		return lstMediaItemProgram;
	}
	public void setLstMediaItemProgram(List<MediaItemProgramVO> lstMediaItemProgram) {
		this.lstMediaItemProgram = lstMediaItemProgram;
	}
	
	
}

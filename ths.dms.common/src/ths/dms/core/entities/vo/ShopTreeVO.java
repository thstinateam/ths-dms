package ths.dms.core.entities.vo;

import java.io.Serializable;

import ths.dms.core.entities.ShopType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.OrganizationNodeType;

public class ShopTreeVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private ShopTreeVO parentShop;
	private String shopCode;
	private String shopName;
	private ShopType type;
	private ActiveType status;
	/**vuongmq*/
	private OrganizationNodeType nodeType;// nodeType cay to chuc
	private Long typeId;
	private Integer nodeOrdinal; //sap sep
	private Long organizationId;
	private Integer isManage; // La quan ly
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ShopTreeVO getParentShop() {
		return parentShop;
	}
	public void setParentShop(ShopTreeVO parentShop) {
		this.parentShop = parentShop;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public OrganizationNodeType getNodeType() {
		return nodeType;
	}
	public void setNodeType(OrganizationNodeType nodeType) {
		this.nodeType = nodeType;
	}
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public Integer getNodeOrdinal() {
		return nodeOrdinal;
	}
	public void setNodeOrdinal(Integer nodeOrdinal) {
		this.nodeOrdinal = nodeOrdinal;
	}
	public ShopType getType() {
		return type;
	}
	public void setType(ShopType type) {
		this.type = type;
	}
	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
	public Integer getIsManage() {
		return isManage;
	}
	public void setIsManage(Integer isManage) {
		this.isManage = isManage;
	}
	
}

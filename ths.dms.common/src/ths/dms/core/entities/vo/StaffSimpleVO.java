package ths.dms.core.entities.vo;

import java.io.Serializable;

public class StaffSimpleVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5497197485816488581L;

	private Long staffId;
	private String staffCode;
	private String staffName;
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
	
}

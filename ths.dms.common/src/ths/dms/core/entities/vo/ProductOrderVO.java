package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProductOrderVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final String ERR_PRODUCT_NULL = "ERR_PRODUCT_NULL";
	public static final String ERR_WAREHOUSE_NULL = "ERR_WAREHOUSE_NULL";
	public static final String ERR_PRODUCT_NOT_BELONG_WAREHOUSE = "ERR_PRODUCT_NOT_BELONG_WAREHOUSE";
	public static final String ERR_WAREHOUSE_NOT_BELONG_SHOP = "ERR_WAREHOUSE_NOT_BELONG_SHOP";
	public static final String ERR_NOT_ENOUGH = "ERR_NOT_ENOUGH";

	private Long productId;
	private Long warehouseId;
	private Integer quantity;
	private BigDecimal price;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
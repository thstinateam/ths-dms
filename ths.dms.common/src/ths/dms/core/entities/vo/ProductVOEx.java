package ths.dms.core.entities.vo;

import java.io.Serializable;

public class ProductVOEx implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6746983940085551395L;
	
	private Long id;
	private String productName;
	private String productCode;
	private String productCat;
	private Integer convfact;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductCat() {
		return productCat;
	}
	public void setProductCat(String productCat) {
		this.productCat = productCat;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
}

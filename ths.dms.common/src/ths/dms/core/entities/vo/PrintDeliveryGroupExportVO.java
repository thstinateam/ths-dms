package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PrintDeliveryGroupExportVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String orderNumber;
	private String deliveryCode;
	private String deliveryName;
	private ArrayList<PrintDeliveryGroupExportVO1> lstDeliveryGroup = new ArrayList<PrintDeliveryGroupExportVO1>();
	private ArrayList<PrintDeliveryGroupExportVO1> lstDeliveryGroupProMoney = new ArrayList<PrintDeliveryGroupExportVO1>();
	private List<String> lstSaleOrder = new ArrayList<String>();

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public ArrayList<PrintDeliveryGroupExportVO1> getLstDeliveryGroup() {
		return lstDeliveryGroup;
	}

	public void setLstDeliveryGroup(
			ArrayList<PrintDeliveryGroupExportVO1> lstDeliveryGroup) {
		this.lstDeliveryGroup = lstDeliveryGroup;
	}

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @param orderNumber
	 *            the orderNumber to set
	 */
	public void setOrderNumber(String orderNumber) {
		if(orderNumber != null && !orderNumber.equalsIgnoreCase("")) {
			String __orderNumber = orderNumber.replaceAll(" ", "");
			String[] __lstSO = __orderNumber.split(",");
			if(__lstSO.length == 5) {
				lstSaleOrder.add(orderNumber);
			} else {
				int index = 1;
				String tmp = "";
				for(int i = 0; i < __lstSO.length; i++) {
					if(index == 5) {
						tmp = tmp.equalsIgnoreCase("") ? __lstSO[i] : tmp + ", " + __lstSO[i];
						lstSaleOrder.add(tmp);
						tmp = new String("");
						index = 1;
					} else {
						tmp = tmp.equalsIgnoreCase("") ? __lstSO[i] : tmp + ", " + __lstSO[i];
						index++;
					}
				}
				if(!tmp.equals("")) {
					lstSaleOrder.add(tmp);
				}
			}
		}
		this.orderNumber = orderNumber;
	}
	

	/**
	 * @return the lstDeliveryGroupProMoney
	 */
	public ArrayList<PrintDeliveryGroupExportVO1> getLstDeliveryGroupProMoney() {
		return lstDeliveryGroupProMoney;
	}

	/**
	 * @param lstDeliveryGroupProMoney
	 *            the lstDeliveryGroupProMoney to set
	 */
	public void setLstDeliveryGroupProMoney(
			ArrayList<PrintDeliveryGroupExportVO1> lstDeliveryGroupProMoney) {
		this.lstDeliveryGroupProMoney = lstDeliveryGroupProMoney;
	}

	public List<String> getLstSaleOrder() {
		return lstSaleOrder;
	}

	public void setLstSaleOrder(List<String> lstSaleOrder) {
		this.lstSaleOrder = lstSaleOrder;
	}

}

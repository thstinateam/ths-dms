/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;

public class StaffPositionLogVO implements Serializable {
	
	private static final long serialVersionUID = 4488396665144328036L;
	private Long id;
	private Shop shop;
	private Staff staff;
	private Date createDate;
	private Float lat;
	private Float lng;
	private Float accuracy;
	private Float pin;
	private String networkType;
	private Integer networkSpeed;
	private Integer pinRound;
	
	public void safeSetNull() throws Exception {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw new Exception(e);
		} 
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLng() {
		return lng;
	}

	public void setLng(Float lng) {
		this.lng = lng;
	}

	public Float getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(Float accuracy) {
		this.accuracy = accuracy;
	}

	public Float getPin() {
		return pin;
	}

	public void setPin(Float pin) {
		this.pin = pin;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public Integer getNetworkSpeed() {
		return networkSpeed;
	}

	public void setNetworkSpeed(Integer networkSpeed) {
		this.networkSpeed = networkSpeed;
	}

	public Integer getPinRound() {
		return pinRound;
	}

	public void setPinRound(Integer pinRound) {
		this.pinRound = pinRound;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Class EquipItemVO
 * 
 * @author liemtpt
 * @since 07/04/2015
 * 
 * @author hunglm16
 * @since May 07,2015
 * @description Ra soat va Update
 */
public class EquipItemConfigDtlVO implements Serializable {

	private static final long serialVersionUID = 7425917930437165646L;

	private Long id;
	// Id  hang muc
	private Long equipItemId;
	
	private String equipItemConfigCode;
	
	private String equipItemConfigName;
	// Ma  hang muc
	private String equipItemCode;
	// Ten  hang muc
	private String equipItemName;

	//Don gia vat tu tu
	private BigDecimal fromMaterialPrice;
	//Don gia vat tu den
	private BigDecimal toMaterialPrice;
	// Don gia cong nhan tu
	private BigDecimal fromWorkerPrice;
	// Don gia cong nhan den
	private BigDecimal toWorkerPrice;
	
	/**
	 * Khai bao phuong thuc GETTER/SETTER
	 * */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEquipItemConfigCode() {
		return equipItemConfigCode;
	}
	public void setEquipItemConfigCode(String equipItemConfigCode) {
		this.equipItemConfigCode = equipItemConfigCode;
	}
	public String getEquipItemConfigName() {
		return equipItemConfigName;
	}
	public void setEquipItemConfigName(String equipItemConfigName) {
		this.equipItemConfigName = equipItemConfigName;
	}
	public String getEquipItemCode() {
		return equipItemCode;
	}
	public void setEquipItemCode(String equipItemCode) {
		this.equipItemCode = equipItemCode;
	}
	public String getEquipItemName() {
		return equipItemName;
	}
	public void setEquipItemName(String equipItemName) {
		this.equipItemName = equipItemName;
	}
	public BigDecimal getFromMaterialPrice() {
		return fromMaterialPrice;
	}
	public void setFromMaterialPrice(BigDecimal fromMaterialPrice) {
		this.fromMaterialPrice = fromMaterialPrice;
	}
	public BigDecimal getToMaterialPrice() {
		return toMaterialPrice;
	}
	public void setToMaterialPrice(BigDecimal toMaterialPrice) {
		this.toMaterialPrice = toMaterialPrice;
	}
	public BigDecimal getFromWorkerPrice() {
		return fromWorkerPrice;
	}
	public void setFromWorkerPrice(BigDecimal fromWorkerPrice) {
		this.fromWorkerPrice = fromWorkerPrice;
	}
	public BigDecimal getToWorkerPrice() {
		return toWorkerPrice;
	}
	public void setToWorkerPrice(BigDecimal toWorkerPrice) {
		this.toWorkerPrice = toWorkerPrice;
	}
	public Long getEquipItemId() {
		return equipItemId;
	}
	public void setEquipItemId(Long equipItemId) {
		this.equipItemId = equipItemId;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EquipmentLiquidationFormVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Id bien ban */
	private Long idLiquidation;
	
	/** Ma bien ban */
	private String liquidationCode;
	
	/** so to trinh thanh ly */
	private String docNumber;
	
	/** gia tri */
	private Long equipValue;
	
	/** dia chi nguoi mua */
	private String buyerAddress;
	
	/** so dien thoai nguoi mua */
	private String buyerPhoneNumber;
	
	/** ten nguoi mua */
	private String buyerName;	
	
	/** ly do */
	private String reasonLiquidation;
	
	/** Ngay tao bien ban */
	private String createDate;
	
	/** danh sach ma thiet bi */
	private String equipCode;
	
	/** danh sach so seri */
	private String equipSeri;
	
	/** Ten file dinh kem*/
	private String attachFilesName;
	
	/** Url file dinh kem */
	private String urlAttachFiles;
	
	/** Trang thai bien ban */
	private Integer status;
	
	/** Ma cong van */
	private String equip_liquidation_doc;
	
	/** Gia tri thanh ly: ben thanh ly tai san*/
	private BigDecimal liquidationValue;
	
	/** Gia tri thu hoi: ben thanh ly tai san*/
	private BigDecimal evictionValue;
	
	/** Gia tri lo: ben thanh ly tai san*/
	private Integer checkLot;
	
	/** Tong Gia tri thanh ly: ben thanh ly tai san*/
	private BigDecimal liquidationValueSum;
	
	/** Tong Gia tri thu hoi: ben thanh ly tai san*/
	private BigDecimal evictionValueSum;
	
	// dung cho tim kiem danh sach thanh ly bien ban
	private Date createDateTmp;
	
	/** danh sach equip VO */
	private List<EquipmentDeliveryVO> lstEquipmentLiquidationVOs;

	/** danh sach  EquipmentLiquidationFormVO*/
	private List<EquipmentLiquidationFormVO> lstequipmentLiquidationFormVOs;
	
	private Date createFormDate;
	private String note;
	
	public EquipmentLiquidationFormVO(){
		this.lstequipmentLiquidationFormVOs = new ArrayList<EquipmentLiquidationFormVO>();
	}
	
	public Long getIdLiquidation() {
		return idLiquidation;
	}

	public void setIdLiquidation(Long idLiquidation) {
		this.idLiquidation = idLiquidation;
	}

	public String getLiquidationCode() {
		return liquidationCode;
	}

	public void setLiquidationCode(String liquidationCode) {
		this.liquidationCode = liquidationCode;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getBuyerAddress() {
		return buyerAddress;
	}

	public void setBuyerAddress(String buyerAddress) {
		this.buyerAddress = buyerAddress;
	}

	public String getBuyerPhoneNumber() {
		return buyerPhoneNumber;
	}

	public void setBuyerPhoneNumber(String buyerPhoneNumber) {
		this.buyerPhoneNumber = buyerPhoneNumber;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getReasonLiquidation() {
		return reasonLiquidation;
	}

	public void setReasonLiquidation(String reasonLiquidation) {
		this.reasonLiquidation = reasonLiquidation;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getEquipSeri() {
		return equipSeri;
	}

	public void setEquipSeri(String equipSeri) {
		this.equipSeri = equipSeri;
	}

	public String getAttachFilesName() {
		return attachFilesName;
	}

	public void setAttachFilesName(String attachFilesName) {
		this.attachFilesName = attachFilesName;
	}

	public String getUrlAttachFiles() {
		return urlAttachFiles;
	}

	public void setUrlAttachFiles(String urlAttachFiles) {
		this.urlAttachFiles = urlAttachFiles;
	}

	public List<EquipmentDeliveryVO> getLstEquipmentLiquidationVOs() {
		return lstEquipmentLiquidationVOs;
	}

	public void setLstEquipmentLiquidationVOs(List<EquipmentDeliveryVO> lstEquipmentLiquidationVOs) {
		this.lstEquipmentLiquidationVOs = lstEquipmentLiquidationVOs;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getEquipValue() {
		return equipValue;
	}

	public void setEquipValue(Long equipValue) {
		this.equipValue = equipValue;
	}

	public List<EquipmentLiquidationFormVO> getLstequipmentLiquidationFormVOs() {
		return lstequipmentLiquidationFormVOs;
	}

	public void setLstequipmentLiquidationFormVOs(
			List<EquipmentLiquidationFormVO> lstequipmentLiquidationFormVOs) {
		this.lstequipmentLiquidationFormVOs = lstequipmentLiquidationFormVOs;
	}

	public String getEquip_liquidation_doc() {
		return equip_liquidation_doc;
	}

	public void setEquip_liquidation_doc(String equip_liquidation_doc) {
		this.equip_liquidation_doc = equip_liquidation_doc;
	}

	public BigDecimal getLiquidationValue() {
		return liquidationValue;
	}

	public void setLiquidationValue(BigDecimal liquidationValue) {
		this.liquidationValue = liquidationValue;
	}

	public BigDecimal getEvictionValue() {
		return evictionValue;
	}

	public void setEvictionValue(BigDecimal evictionValue) {
		this.evictionValue = evictionValue;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public Date getCreateDateTmp() {
		return createDateTmp;
	}

	public void setCreateDateTmp(Date createDateTmp) {
		this.createDateTmp = createDateTmp;
	}

	public BigDecimal getLiquidationValueSum() {
		return liquidationValueSum;
	}

	public void setLiquidationValueSum(BigDecimal liquidationValueSum) {
		this.liquidationValueSum = liquidationValueSum;
	}

	public BigDecimal getEvictionValueSum() {
		return evictionValueSum;
	}

	public void setEvictionValueSum(BigDecimal evictionValueSum) {
		this.evictionValueSum = evictionValueSum;
	}

	public Date getCreateFormDate() {
		return createFormDate;
	}

	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}

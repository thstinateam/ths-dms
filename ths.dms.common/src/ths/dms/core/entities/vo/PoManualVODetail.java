/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Mo ta class PoManualVODetail.java
 * @author vuongmq
 * @since Dec 21, 2015
 */
public class PoManualVODetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private Long id;
	private Long productId;
	private Long priceSaleInId;
	
	private String productCode;
	private String productName;
	
	private Integer packageQuantity;
	private Integer retailQuantity;
	private Integer quantity;
	private Integer convfact;
	private Integer poLineNumber;
	
	private BigDecimal packagePrice;
	private BigDecimal price; // retailPrice; gia le
	private BigDecimal amount;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getPriceSaleInId() {
		return priceSaleInId;
	}
	public void setPriceSaleInId(Long priceSaleInId) {
		this.priceSaleInId = priceSaleInId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getPackageQuantity() {
		return packageQuantity;
	}
	public void setPackageQuantity(Integer packageQuantity) {
		this.packageQuantity = packageQuantity;
	}
	public Integer getRetailQuantity() {
		return retailQuantity;
	}
	public void setRetailQuantity(Integer retailQuantity) {
		this.retailQuantity = retailQuantity;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public Integer getPoLineNumber() {
		return poLineNumber;
	}
	public void setPoLineNumber(Integer poLineNumber) {
		this.poLineNumber = poLineNumber;
	}
	public BigDecimal getPackagePrice() {
		return packagePrice;
	}
	public void setPackagePrice(BigDecimal packagePrice) {
		this.packagePrice = packagePrice;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;

import ths.dms.core.entities.Staff;

/**
 * vo chua du lieu cac danh muc san pham
 * @author trietptm
 * @since Nov 17, 2015
 * */
public class CatalogVO implements Serializable {

	private static final long serialVersionUID = 3356702502499942801L;

	private Long id;
	private String code;
	private String name;
	private String description;
	private Long parentId;
	private String parentName;
	private Long staffTypeId;
	private String value;
	private String type;
	private Staff staff;
	private Boolean isEdit;
	private String multiLanguage;
	private LogInfoVO logInfoVO;
	private Integer importExportType;
	private String newCode;

	public String getNewCode() {
		return newCode;
	}

	public void setNewCode(String newCode) {
		this.newCode = newCode;
	}

	public LogInfoVO getLogInfoVO() {
		return logInfoVO;
	}

	public void setLogInfoVO(LogInfoVO logInfoVO) {
		this.logInfoVO = logInfoVO;
	}

	public String getMultiLanguage() {
		return multiLanguage;
	}

	public void setMultiLanguage(String multiLanguage) {
		this.multiLanguage = multiLanguage;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Boolean getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getType() {
		return type;
	}

	public Integer getImportExportType() {
		return importExportType;
	}

	public void setImportExportType(Integer importExportType) {
		this.importExportType = importExportType;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getStaffTypeId() {
		return staffTypeId;
	}

	public void setStaffTypeId(Long staffTypeId) {
		this.staffTypeId = staffTypeId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}

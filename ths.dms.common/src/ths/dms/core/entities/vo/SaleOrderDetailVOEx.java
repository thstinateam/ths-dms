package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import ths.dms.core.entities.GroupLevel;
import ths.dms.core.entities.ProductGroup;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ProgramType;


public class SaleOrderDetailVOEx  implements Serializable {
	/**
	 * lap doi tuong SaleOrderDetailVO Co ban theo cac truong trong Sale Order Detail
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long productId;
	private Long saleOrderLotId;
	private Long warehouseId;
	
	private Float discountPercent;
	private Float grossWeight;
	private BigDecimal totalWeight;

	private Integer stockQuantity;
	private Integer availableQuantity;
	private Integer convfact;
	private Integer quantity;
	private Integer quantityPackage;
	private Integer quantityRetail;
	private Integer oldQuantity;
	private Integer programType;
	private String programTypeCode;
	private Integer checkLot;
	private Integer maxQuantityFree;
	private Integer maxQuantityFreeCk;
	private Integer maxQuantityFreeTotal; /** vuongmq; 11/01/2015; hien thi maxQuantityFreeTotal so luong tinh khuyen mai*/
	private Integer isFreeItem;
	private Integer ppType;	/** Tham so nay chua gia tri de kiem tra no la KM ZV01 - ZV18 hay KM ZV19-ZV21*/
	private Integer outOfStock;
	private Integer isFailAmount;
	private Integer type;/** Loai KM */
	private Integer relation;
	private Integer isEdited;
	private Integer isAttr;
	private Integer isChangeQuantity;
	private Integer isChangeRation;
	private Integer isInvalidPrice;//sai gia
	private Integer isExceedKeyShop;
	private Integer warehouseType;
	
	private Boolean canChaneMultiProduct;
	
	private BigDecimal myTime;
	private BigDecimal price;
	private BigDecimal priceProduct;
	private BigDecimal amount;
	private BigDecimal discountAmount;
	private BigDecimal priceNotVat;
	private BigDecimal maxAmountFree;
	
	private String productCode;
	private String productName;
	private String programCode;
	private String uom1;
	private String promotionType;
	private String warehouseName;
	private String promotionProgramCode;
	private String customerCode;
	
	private Warehouse warehouse;
	
	private Integer changeProduct;// 1: la duoc lua chon doi SPKM, 0: ko duoc chon doi SPKM
	private Integer levelPromo;
	private String staffCode;
	
	private String productInfoCode;
	private String joinProgramCode;
	
	private Long productGroupId;
	private Long groupLevelId;
	
	private Integer openPromo = 0;
	private Integer hasPortion;
	private Integer accumulation = 0;
	
	private ProductGroup productGroup;
	private GroupLevel groupLevel;
	
	private Integer isVansale;
	private Integer isOwner;
	
	private BigDecimal pkPrice;
	private BigDecimal pkPriceNotVat;
	
	private BigDecimal pkPriceView;
	private BigDecimal priceView;
	
	

	public String getProgramTypeCode() {
		return programTypeCode;
	}

	public void setProgramTypeCode(String programTypeCode) {
		this.programTypeCode = programTypeCode;
	}

	public Long getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(Long productGroupId) {
		this.productGroupId = productGroupId;
	}

	public Long getGroupLevelId() {
		return groupLevelId;
	}

	public void setGroupLevelId(Long groupLevelId) {
		this.groupLevelId = groupLevelId;
	}

	public ProductGroup getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(ProductGroup productGroup) {
		this.productGroup = productGroup;
	}

	public GroupLevel getGroupLevel() {
		return groupLevel;
	}

	public void setGroupLevel(GroupLevel groupLevel) {
		this.groupLevel = groupLevel;
	}
	public String getProductInfoCode() {
		return productInfoCode;
	}

	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Integer getIsEdited() {
		return isEdited;
	}

	public void setIsEdited(Integer isEdited) {
		this.isEdited = isEdited;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getIsAttr() {
		return isAttr;
	}

	public void setIsAttr(Integer isAttr) {
		this.isAttr = isAttr;
	}

	public Integer getIsFailAmount() {
		return isFailAmount;
	}

	public void setIsFailAmount(Integer isFailAmount) {
		this.isFailAmount = isFailAmount;
	}

	public Integer getOutOfStock() {
		return outOfStock;
	}

	public void setOutOfStock(Integer outOfStock) {
		this.outOfStock = outOfStock;
	}
	
	public Long getWarehouseId() {
		return warehouseId;
	}

	
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public Long getSaleOrderLotId() {
		return saleOrderLotId;
	}

	public void setSaleOrderLotId(Long saleOrderLotId) {
		this.saleOrderLotId = saleOrderLotId;
	}

	public Integer getIsFreeItem() {
		return isFreeItem;
	}

	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getPpType() {
		return ppType;
	}

	public void setPpType(Integer ppType) {
		this.ppType = ppType;
	}

	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}

	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}

	public Integer getMaxQuantityFree() {
		return maxQuantityFree;
	}

	public void setMaxQuantityFree(Integer maxQuantityFree) {
		this.maxQuantityFree = maxQuantityFree;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPrice() {
		return price == null ? BigDecimal.ZERO : price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public BigDecimal getPriceProduct() {
		return priceProduct == null ? BigDecimal.ZERO : priceProduct;
	}

	public void setPriceProduct(BigDecimal priceProduct) {
		this.priceProduct = priceProduct;
	}

	public Integer getStockQuantity() {
		if (stockQuantity == null) {
			stockQuantity = 0;
		}
		return stockQuantity;
	}

	public void setStockQuantity(Integer stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public Integer getConvfact() {
		if (convfact == null) {
			convfact = 0;
		}
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.ZERO : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public ProgramType getProgramType() {
		return ProgramType.parseValue(programType);
	}

	public void setProgramType(Integer programType) {
		this.programType = programType;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Float getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Float discountPercent) {
		this.discountPercent = discountPercent;
	}

	public String getUom1() {
		return uom1;
	}

	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	public String getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}

	public Float getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Float grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public BigDecimal getPriceNotVat() {
		return priceNotVat;
	}

	public void setPriceNotVat(BigDecimal priceNotVat) {
		this.priceNotVat = priceNotVat;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getMaxAmountFree() {
		return maxAmountFree;
	}

	public void setMaxAmountFree(BigDecimal maxAmountFree) {
		this.maxAmountFree = maxAmountFree;
	}

	public Integer getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public Boolean getCanChaneMultiProduct() {
		return canChaneMultiProduct;
	}

	public void setCanChaneMultiProduct(Boolean canChaneMultiProduct) {
		this.canChaneMultiProduct = canChaneMultiProduct;
	}

	public Integer getMaxQuantityFreeCk() {
		return maxQuantityFreeCk;
	}

	public void setMaxQuantityFreeCk(Integer maxQuantityFreeCk) {
		this.maxQuantityFreeCk = maxQuantityFreeCk;
	}

	public Integer getMaxQuantityFreeTotal() {
		return maxQuantityFreeTotal;
	}

	public void setMaxQuantityFreeTotal(Integer maxQuantityFreeTotal) {
		this.maxQuantityFreeTotal = maxQuantityFreeTotal;
	}

	public Integer getRelation() {
		return relation;
	}

	public void setRelation(Integer relation) {
		this.relation = relation;
	}

	public BigDecimal getMyTime() {
		return myTime;
	}

	public void setMyTime(BigDecimal myTime) {
		this.myTime = myTime;
	}

	public Integer getOldQuantity() {
		return oldQuantity;
	}

	public void setOldQuantity(Integer oldQuantity) {
		this.oldQuantity = oldQuantity;
	}

	public Integer getChangeProduct() {
		return changeProduct;
	}

	public void setChangeProduct(Integer changeProduct) {
		this.changeProduct = changeProduct;
	}

	public Integer getLevelPromo() {
		return levelPromo;
	}

	public void setLevelPromo(Integer levelPromo) {
		this.levelPromo = levelPromo;
	}

	public String getJoinProgramCode() {
		return joinProgramCode;
	}

	public void setJoinProgramCode(String joinProgramCode) {
		this.joinProgramCode = joinProgramCode;
	}

	public Integer getIsVansale() {
		return isVansale;
	}

	public void setIsVansale(Integer isVansale) {
		this.isVansale = isVansale;
	}

	public Integer getOpenPromo() {
		return openPromo;
	}

	public void setOpenPromo(Integer openPromo) {
		this.openPromo = openPromo;
	}

	public Integer getIsOwner() {
		return isOwner;
	}

	public void setIsOwner(Integer isOwner) {
		this.isOwner = isOwner;
	}

	public Integer getAccumulation() {
		return accumulation;
	}

	public void setAccumulation(Integer accumulation) {
		this.accumulation = accumulation;
	}

	public BigDecimal getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}

	public Integer getHasPortion() {
		return hasPortion;
	}

	public void setHasPortion(Integer hasPortion) {
		this.hasPortion = hasPortion;
	}

	public Integer getIsChangeQuantity() {
		return isChangeQuantity;
	}

	public void setIsChangeQuantity(Integer isChangeQuantity) {
		this.isChangeQuantity = isChangeQuantity;
	}

	public Integer getIsChangeRation() {
		return isChangeRation;
	}

	public void setIsChangeRation(Integer isChangeRation) {
		this.isChangeRation = isChangeRation;
	}

	public BigDecimal getPkPrice() {
		return pkPrice;
	}

	public void setPkPrice(BigDecimal pkPrice) {
		this.pkPrice = pkPrice;
	}

	public BigDecimal getPkPriceNotVat() {
		return pkPriceNotVat;
	}

	public void setPkPriceNotVat(BigDecimal pkPriceNotVat) {
		this.pkPriceNotVat = pkPriceNotVat;
	}

	public BigDecimal getPkPriceView() {
		return pkPriceView;
	}

	public void setPkPriceView(BigDecimal pkPriceView) {
		this.pkPriceView = pkPriceView;
	}

	public BigDecimal getPriceView() {
		return priceView;
	}

	public void setPriceView(BigDecimal priceView) {
		this.priceView = priceView;
	}

	public Integer getQuantityPackage() {
		return quantityPackage;
	}

	public void setQuantityPackage(Integer quantityPackage) {
		this.quantityPackage = quantityPackage;
	}

	public Integer getQuantityRetail() {
		return quantityRetail;
	}

	public void setQuantityRetail(Integer quantityRetail) {
		this.quantityRetail = quantityRetail;
	}

	public Integer getWarehouseType() {
		return warehouseType;
	}

	public void setWarehouseType(Integer warehouseType) {
		this.warehouseType = warehouseType;
	}

	public Integer getIsInvalidPrice() {
		return isInvalidPrice;
	}

	public void setIsInvalidPrice(Integer isInvalidPrice) {
		this.isInvalidPrice = isInvalidPrice;
	}

	public Integer getIsExceedKeyShop() {
		return isExceedKeyShop;
	}

	public void setIsExceedKeyShop(Integer isExceedKeyShop) {
		this.isExceedKeyShop = isExceedKeyShop;
	}



}
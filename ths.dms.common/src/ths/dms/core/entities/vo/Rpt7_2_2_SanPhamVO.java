package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class Rpt7_2_2_SanPhamVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String maNVBH;
	private String tenNVBH;
	private String maNganhHang;
	private String maSP;
	private String tenSP;
	private BigDecimal donGia;
	private BigDecimal soLuong;
	private BigDecimal thanhTien;
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getMaNganhHang() {
		return maNganhHang;
	}
	public void setMaNganhHang(String maNganhHang) {
		this.maNganhHang = maNganhHang;
	}
	public String getMaSP() {
		return maSP;
	}
	public void setMaSP(String maSP) {
		this.maSP = maSP;
	}
	public String getTenSP() {
		return tenSP;
	}
	public void setTenSP(String tenSP) {
		this.tenSP = tenSP;
	}
	public BigDecimal getDonGia() {
		return donGia;
	}
	public void setDonGia(BigDecimal donGia) {
		this.donGia = donGia;
	}
	public BigDecimal getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(BigDecimal soLuong) {
		this.soLuong = soLuong;
	}
	public BigDecimal getThanhTien() {
		return thanhTien;
	}
	public void setThanhTien(BigDecimal thanhTien) {
		this.thanhTien = thanhTien;
	}

}

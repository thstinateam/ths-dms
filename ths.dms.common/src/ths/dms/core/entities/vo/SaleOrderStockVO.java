package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author vuongmq
 * @since 27- August, 2014
 * @description Load grid Cap nhat phai thu ca kho
 *
 */
public class SaleOrderStockVO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long id;//saleOrderId ma don hang
	private Long shopId; // npp
	private Long staffId; // nvbh
	private String shopCode;
	private String orderNumber;
	private String refOrderNumber;
	private Date orderDate;
	private String strOrderDate;
	private String shortCode;
	private String customerName;
	private String customerAddress;
	private BigDecimal total;	
	private BigDecimal quantity;
	private Integer convfact;
	private String orderType;
	private String staffCode;
	private String staffName;
	private String deliveryCode;
	private String deliveryName;
	private Integer orderSource;
	private String fromWarehouse; 
	private String fromShop;
	private String toWarehouse;
	private String toShop;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getRefOrderNumber() {
		return refOrderNumber;
	}
	public void setRefOrderNumber(String refOrderNumber) {
		this.refOrderNumber = refOrderNumber;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public Integer getOrderSource() {
		return orderSource;
	}
	public void setOrderSource(Integer orderSource) {
		this.orderSource = orderSource;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public String getFromWarehouse() {
		return fromWarehouse;
	}
	public void setFromWarehouse(String fromWarehouse) {
		this.fromWarehouse = fromWarehouse;
	}
	public String getFromShop() {
		return fromShop;
	}
	public void setFromShop(String fromShop) {
		this.fromShop = fromShop;
	}
	public String getToWarehouse() {
		return toWarehouse;
	}
	public void setToWarehouse(String toWarehouse) {
		this.toWarehouse = toWarehouse;
	}
	public String getToShop() {
		return toShop;
	}
	public void setToShop(String toShop) {
		this.toShop = toShop;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public String getStrOrderDate() {
		return strOrderDate;
	}
	public void setStrOrderDate(String strOrderDate) {
		this.strOrderDate = strOrderDate;
	}
	
	
}
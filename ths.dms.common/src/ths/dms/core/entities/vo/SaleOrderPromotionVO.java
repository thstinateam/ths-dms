package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.entities.Product;

/**
 * The Class SaleOrderPromotionVO.
 *
 * @author  ?
 * @editby tientv11
 */
public class SaleOrderPromotionVO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id. */
	private Long id;
	
	/** The sale order id. */
	private Long saleOrderId;
	
	/** The promotion id. */
	private Long promotionId;
	
	/** The promotion code. */
	private String promotionCode;
	
	/** The group id. */
	private Long groupId;
	
	/** The level id. */
	private Long levelId;
	
	/** The quantity. */
	private Integer quantity;
	
	private Integer maxQuantityReceived;
	
	private Integer quantityReceived;
	
	private Integer promotionLevel;
	
	private Boolean isNotReceiveQuantity = false;
	
	private Product product;
	
	// begin vuongmq; 08/01/2015; them cac bien theo xu ly save: sale_order_promotion
	private Integer exceedObject;
	
	private Integer exceedUnit;
	
	private BigDecimal numReceived;
	
	private BigDecimal maxNumReceived;
	
	private BigDecimal amountReceived;
	
	private BigDecimal maxAmountReceived;
	
	private Integer maxQuantityReceivedTotal; // neu kho du so suat, so luong, tien,: luu lai gia tri cu tinh KM; luu so suat tinh dc dau tien KM

	private BigDecimal maxNumReceivedTotal; // neu kho du so suat, so luong, tien,: luu lai gia tri cu tinh KM; luu so luong tinh dc dau tien KM

	private BigDecimal maxAmountReceivedTotal; // neu kho du so suat, so luong, tien,: luu lai gia tri cu tinh KM; luu so tien tinh dc dau tien KM
	
	//private Integer flagPromoQuantity; // 1; dat khong an KM do het suat; 2: dat ma khong an het (chi an 1 phan); duoi client thong bao thoi 
	
	// end vuongmq; 08/01/2015; them cac bien theo xu ly save: sale_order_promotion
	public Integer getMaxQuantityReceived() {
		return maxQuantityReceived;
	}

	public void setMaxQuantityReceived(Integer maxQuantityReceived) {
		this.maxQuantityReceived = maxQuantityReceived;
	}
	public Boolean getIsNotReceiveQuantity() {
		return isNotReceiveQuantity;
	}

	public void setIsNotReceiveQuantity(Boolean isNotReceiveQuantity) {
		this.isNotReceiveQuantity = isNotReceiveQuantity;
	}

	/** The detail v os. */
	private List<SaleOrderPromotionDetailVO> detailVOs;
	
	
	public Integer getPromotionLevel() {
		return promotionLevel;
	}

	public void setPromotionLevel(Integer promotionLevel) {
		this.promotionLevel = promotionLevel;
	}

	public Integer getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	/**
	 * Gets the detail v os.
	 *
	 * @return the detail v os
	 */
	public List<SaleOrderPromotionDetailVO> getDetailVOs() {
		return detailVOs;
	}

	/**
	 * Sets the detail v os.
	 *
	 * @param detailVOs the new detail v os
	 */
	public void setDetailVOs(List<SaleOrderPromotionDetailVO> detailVOs) {
		this.detailVOs = detailVOs;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Gets the sale order id.
	 *
	 * @return the sale order id
	 */
	public Long getSaleOrderId() {
		return saleOrderId;
	}
	
	/**
	 * Sets the sale order id.
	 *
	 * @param saleOrderId the new sale order id
	 */
	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}
	
	/**
	 * Gets the promotion id.
	 *
	 * @return the promotion id
	 */
	public Long getPromotionId() {
		return promotionId;
	}
	
	/**
	 * Sets the promotion id.
	 *
	 * @param promotionId the new promotion id
	 */
	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}
	
	/**
	 * Gets the promotion code.
	 *
	 * @return the promotion code
	 */
	public String getPromotionCode() {
		return promotionCode;
	}
	
	/**
	 * Sets the promotion code.
	 *
	 * @param promotionCode the new promotion code
	 */
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	
	/**
	 * Gets the group id.
	 *
	 * @return the group id
	 */
	public Long getGroupId() {
		return groupId;
	}
	
	/**
	 * Sets the group id.
	 *
	 * @param groupId the new group id
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	
	/**
	 * Gets the level id.
	 *
	 * @return the level id
	 */
	public Long getLevelId() {
		return levelId;
	}
	
	/**
	 * Sets the level id.
	 *
	 * @param levelId the new level id
	 */
	public void setLevelId(Long levelId) {
		this.levelId = levelId;
	}
	
	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	
	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getExceedObject() {
		return exceedObject;
	}

	public void setExceedObject(Integer exceedObject) {
		this.exceedObject = exceedObject;
	}

	public Integer getExceedUnit() {
		return exceedUnit;
	}

	public void setExceedUnit(Integer exceedUnit) {
		this.exceedUnit = exceedUnit;
	}

	public BigDecimal getNumReceived() {
		return numReceived;
	}

	public void setNumReceived(BigDecimal numReceived) {
		this.numReceived = numReceived;
	}

	public BigDecimal getMaxNumReceived() {
		return maxNumReceived;
	}

	public void setMaxNumReceived(BigDecimal maxNumReceived) {
		this.maxNumReceived = maxNumReceived;
	}

	public BigDecimal getAmountReceived() {
		return amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}

	public BigDecimal getMaxAmountReceived() {
		return maxAmountReceived;
	}

	public void setMaxAmountReceived(BigDecimal maxAmountReceived) {
		this.maxAmountReceived = maxAmountReceived;
	}

	public Integer getMaxQuantityReceivedTotal() {
		return maxQuantityReceivedTotal;
	}

	public void setMaxQuantityReceivedTotal(Integer maxQuantityReceivedTotal) {
		this.maxQuantityReceivedTotal = maxQuantityReceivedTotal;
	}

	public BigDecimal getMaxNumReceivedTotal() {
		return maxNumReceivedTotal;
	}

	public void setMaxNumReceivedTotal(BigDecimal maxNumReceivedTotal) {
		this.maxNumReceivedTotal = maxNumReceivedTotal;
	}

	public BigDecimal getMaxAmountReceivedTotal() {
		return maxAmountReceivedTotal;
	}

	public void setMaxAmountReceivedTotal(BigDecimal maxAmountReceivedTotal) {
		this.maxAmountReceivedTotal = maxAmountReceivedTotal;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}

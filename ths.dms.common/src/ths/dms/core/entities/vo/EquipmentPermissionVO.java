package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.management.loading.PrivateClassLoader;

/**
 * Class EquipmentPermission VO
 * @author tamvnm
 * @since March 26,2015
 * @description Cac quyen hien tai cua nguoi dung - Gan quyen cho nguoi dung
 */
public class EquipmentPermissionVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id; 
	private String roleCode;
	private String roleName;
	private String discription;
	private Integer status;
	private Long equipRoleUserId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getEquipRoleUserId() {
		return equipRoleUserId;
	}
	public void setEquipRoleUserId(Long equipRoleUserId) {
		this.equipRoleUserId = equipRoleUserId;
	}
	
}

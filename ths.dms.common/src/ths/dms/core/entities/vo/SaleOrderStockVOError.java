package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * 
 * @author vuongmq
 * @since 27- August, 2014
 * @description Load grid Cap nhat phai thu ca kho
 *
 */
public class SaleOrderStockVOError implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long id;//saleOrderId ma don hang
	private String orderNumber; // so don hang
	private String stockQuantity; // ton kho
	private String errOrder; // loi don hang
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(String stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public String getErrOrder() {
		return errOrder;
	}
	public void setErrOrder(String errOrder) {
		this.errOrder = errOrder;
	}
	
	
}
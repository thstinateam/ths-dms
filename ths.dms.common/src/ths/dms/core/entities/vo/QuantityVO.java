package ths.dms.core.entities.vo;

import java.io.Serializable;

public class QuantityVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long shopId;
	
	private Integer quantityMax;
	
	private Integer quantityReceived;
	
	private Integer isNPP;
	
	private Integer isJoin;
	
	private Long promotionShopMapId;
	
	public Integer getQuantityMax() {
		return quantityMax;
	}

	public void setQuantityMax(Integer quantityMax) {
		this.quantityMax = quantityMax;
	}

	public Integer getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getIsNPP() {
		return isNPP;
	}

	public void setIsNPP(Integer isNPP) {
		this.isNPP = isNPP;
	}

	public Integer getIsJoin() {
		return isJoin;
	}

	public void setIsJoin(Integer isJoin) {
		this.isJoin = isJoin;
	}

	public Long getPromotionShopMapId() {
		return promotionShopMapId;
	}

	public void setPromotionShopMapId(Long promotionShopMapId) {
		this.promotionShopMapId = promotionShopMapId;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class StockTransVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long stockTransId;
	
	private String stockTransCode;
	
	private String shopCode;
	private String shopName;
	
	private Integer fromOwnerType;
	
	private Integer toOwnerType;
	
	private Date stockTransDate;
	
	private String stockTransDateStr;
	
	private String fromStockCode;
	
	private String toStockCode;
	
	private BigDecimal amount;

	private Integer status;
	
	public Long getStockTransId() {
		return stockTransId;
	}

	public void setStockTransId(Long stockTransId) {
		this.stockTransId = stockTransId;
	}

	public String getStockTransCode() {
		return stockTransCode;
	}

	public void setStockTransCode(String stockTransCode) {
		this.stockTransCode = stockTransCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Integer getFromOwnerType() {
		return fromOwnerType;
	}

	public void setFromOwnerType(Integer fromOwnerType) {
		this.fromOwnerType = fromOwnerType;
	}

	public Integer getToOwnerType() {
		return toOwnerType;
	}

	public void setToOwnerType(Integer toOwnerType) {
		this.toOwnerType = toOwnerType;
	}

	public Date getStockTransDate() {
		return stockTransDate;
	}

	public void setStockTransDate(Date stockTransDate) {
		this.stockTransDate = stockTransDate;
	}

	public String getFromStockCode() {
		return fromStockCode;
	}

	public void setFromStockCode(String fromStockCode) {
		this.fromStockCode = fromStockCode;
	}

	public String getToStockCode() {
		return toStockCode;
	}

	public void setToStockCode(String toStockCode) {
		this.toStockCode = toStockCode;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getStockTransDateStr() {
		return stockTransDateStr;
	}

	public void setStockTransDateStr(String stockTransDateStr) {
		this.stockTransDateStr = stockTransDateStr;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
}

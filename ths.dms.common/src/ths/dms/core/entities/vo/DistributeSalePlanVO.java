package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class DistributeSalePlanVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long salePlanId;
	private Long productId;
	private String productCode;
	private String productName;
	private String catName;
	private BigDecimal price;
	private Integer quantity;
	private Integer quantityAssign;
	private Integer ownerQuantity;
	private Integer convfact;
	private Integer ownerQuantityAssign;
	private BigDecimal amount;
	private String spMonth;
	private String shopCode;
	/**
	 * @return the salePlanId
	 */
	public Long getSalePlanId() {
		return salePlanId;
	}
	/**
	 * @param salePlanId the salePlanId to set
	 */
	public void setSalePlanId(Long salePlanId) {
		this.salePlanId = salePlanId;
	}
	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the quantityAssign
	 */
	public Integer getQuantityAssign() {
		return quantityAssign;
	}
	/**
	 * @param quantityAssign the quantityAssign to set
	 */
	public void setQuantityAssign(Integer quantityAssign) {
		this.quantityAssign = quantityAssign;
	}
	/**
	 * @return the ownerQuantity
	 */
	public Integer getOwnerQuantity() {
		return ownerQuantity;
	}
	/**
	 * @param ownerQuantity the ownerQuantity to set
	 */
	public void setOwnerQuantity(Integer ownerQuantity) {
		this.ownerQuantity = ownerQuantity;
	}
	/**
	 * @return the ownerQuantityAssign
	 */
	public Integer getOwnerQuantityAssign() {
		return ownerQuantityAssign;
	}
	/**
	 * @param ownerQuantityAssign the ownerQuantityAssign to set
	 */
	public void setOwnerQuantityAssign(Integer ownerQuantityAssign) {
		this.ownerQuantityAssign = ownerQuantityAssign;
	}
	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		/*if (quantity != null && quantity > 0 && price != null) {
			return price.multiply(new BigDecimal(quantity));
		}
		if (amount == null) {
			return new BigDecimal(0);
		}*/
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * @return the catName
	 */
	public String getCatName() {
		return catName;
	}
	/**
	 * @param catName the catName to set
	 */
	public void setCatName(String catName) {
		this.catName = catName;
	}
	public String getSpMonth() {
		return spMonth;
	}
	public void setSpMonth(String spMonth) {
		this.spMonth = spMonth;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	
	
}

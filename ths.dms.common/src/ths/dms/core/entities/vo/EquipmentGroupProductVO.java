/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * doi tuong hien thi len view, luu du lieu trong bang equip_group_product
 * @author tuannd20
 * @date 05/01/2015
 */
public class EquipmentGroupProductVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long equipmentGroupId;
	private Long equipGroupProductId;
	
	private String productCode;
	private String productName;
	private String fromDateStr;
	private String toDateStr;
	
	private Date fromDate;
	private Date toDate;
	
	private Integer flagSys;
	
	/**
	 * Khai bao GETTET/SETER
	 * */
	
	public Long getEquipmentGroupId() {
		return equipmentGroupId;
	}
	public void setEquipmentGroupId(Long equipmentGroupId) {
		this.equipmentGroupId = equipmentGroupId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getFromDateStr() {
		return fromDateStr;
	}
	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}
	public String getToDateStr() {
		return toDateStr;
	}
	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}
	public Long getEquipGroupProductId() {
		return equipGroupProductId;
	}
	public void setEquipGroupProductId(Long equipGroupProductId) {
		this.equipGroupProductId = equipGroupProductId;
	}
	public Integer getFlagSys() {
		return flagSys;
	}
	public void setFlagSys(Integer flagSys) {
		this.flagSys = flagSys;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
}

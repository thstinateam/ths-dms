/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */



package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 
 * @author lacnv1
 * @since Nov 04, 2014
 */
public class CycleCountDiffVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer rowNo;
	private String cycleCountCode;
	private String stockCardNumber;
	private String productCode;
	private String lot;
	private String startDateStr;
	private Integer quantityCounted;
	private Integer quantityBeforeCount;
	private BigDecimal price;
	private Integer checkLot;
	private Integer quantityDiff;
	private BigDecimal amountDiff;
	private Integer convfact;
	private String quantityCountedConvfact;
	private String quantityBeforeCountConvfact;
	private String quantityDiffConvfact;

	public String getCycleCountCode() {
		return cycleCountCode;
	}

	public void setCycleCountCode(String cycleCountCode) {
		this.cycleCountCode = cycleCountCode;
	}

	public String getStockCardNumber() {
		return stockCardNumber;
	}

	public void setStockCardNumber(String stockCardNumber) {
		this.stockCardNumber = stockCardNumber;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public Integer getQuantityCounted() {
		return quantityCounted;
	}

	public void setQuantityCounted(Integer quantityCounted) {
		this.quantityCounted = quantityCounted;
	}

	public Integer getQuantityBeforeCount() {
		return quantityBeforeCount;
	}

	public void setQuantityBeforeCount(Integer quantityBeforeCount) {
		this.quantityBeforeCount = quantityBeforeCount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

//	public Integer getQuantityDiff() {
//		return quantityDiff;
//	}
//
//	public void setQuantityDiff(Integer quantityDiff) {
//		if (quantityDiff == null) {
//			this.quantityDiff = null;
//		} else {
//			this.quantityDiff = Math.abs(quantityDiff);
//		}
//	}
//
//	public BigDecimal getAmountDiff() {
//		return amountDiff;
//	}
//
//	public void setAmountDiff(BigDecimal amountDiff) {
//		if (amountDiff == null) {
//			this.amountDiff = null;
//		} else {
//			this.amountDiff = amountDiff.abs();
//		}
//	}

	public Integer getQuantityDiff() {
		return quantityDiff;
	}

	public void setQuantityDiff(Integer quantityDiff) {
		this.quantityDiff = quantityDiff;
	}

	public BigDecimal getAmountDiff() {
		return amountDiff;
	}

	public void setAmountDiff(BigDecimal amountDiff) {
		this.amountDiff = amountDiff;
	}

	public String getStartDateStr() {
		return startDateStr;
	}

	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public Integer getRowNo() {
		return rowNo;
	}

	public void setRowNo(Integer rowNo) {
		this.rowNo = rowNo;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public String getQuantityCountedConvfact() {
		return quantityCountedConvfact;
	}

	public void setQuantityCountedConvfact(String quantityCountedConvfact) {
		this.quantityCountedConvfact = quantityCountedConvfact;
	}

	public String getQuantityBeforeCountConvfact() {
		return quantityBeforeCountConvfact;
	}

	public void setQuantityBeforeCountConvfact(String quantityBeforeCountConvfact) {
		this.quantityBeforeCountConvfact = quantityBeforeCountConvfact;
	}

	public String getQuantityDiffConvfact() {
		return quantityDiffConvfact;
	}

	public void setQuantityDiffConvfact(String quantityDiffConvfact) {
		this.quantityDiffConvfact = quantityDiffConvfact;
	}
}
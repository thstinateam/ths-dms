package ths.dms.core.entities.vo;

import java.io.Serializable;

public class CustomerCatLevelImportVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String customerCode;
	private String productInfoName;
	private String saleLevel;
	private String shopCode;

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getProductInfoName() {
		return productInfoName;
	}

	public void setProductInfoName(String productInfoName) {
		this.productInfoName = productInfoName;
	}

	public String getSaleLevel() {
		return saleLevel;
	}

	public void setSaleLevel(String saleLevel) {
		this.saleLevel = saleLevel;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
}

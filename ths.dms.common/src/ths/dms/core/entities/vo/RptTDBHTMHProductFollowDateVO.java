package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author sangtn
 * @see Bao cao theo doi ban hang theo mat hang
 *
 */
public class RptTDBHTMHProductFollowDateVO implements Serializable{

	/**
	 * 
	 */
	//Cac thong tin: 
	//Ngay tao la CREATE_DATE trong SALE_ORDER
	//Tong so luong ban/tra , Tong thanh tien: Tinh toan tu RptTDBHTMHProductDateFollowStaffVO
	
	private String createDate;
	private BigDecimal sumDayQuantity;
	private BigDecimal sumDayAmount;
	private List<RptTDBHTMHProductDateFollowStaffVO> lstRptTDBHTMHProductDateFollowStaffVO = new ArrayList<RptTDBHTMHProductDateFollowStaffVO>();

	
	public String getCreateDate() {
		if(createDate != null && createDate.length() != 0) {
			String[] temp = createDate.split("/");
			return temp[2] + "/" + temp[1] + "/" + temp[0];
		} else return "";		
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;		
	}
	public BigDecimal getSumDayQuantity() {
		return sumDayQuantity;
	}
	public void setSumDayQuantity(BigDecimal sumDayQuantity) {
		this.sumDayQuantity = sumDayQuantity;
	}
	public BigDecimal getSumDayAmount() {
		return sumDayAmount;
	}
	public void setSumDayAmount(BigDecimal sumDayAmount) {
		this.sumDayAmount = sumDayAmount;
	}
	public List<RptTDBHTMHProductDateFollowStaffVO> getLstRptTDBHTMHProductDateFollowStaffVO() {
		return lstRptTDBHTMHProductDateFollowStaffVO;
	}
	public void setLstRptTDBHTMHProductDateFollowStaffVO(
			List<RptTDBHTMHProductDateFollowStaffVO> lstRptTDBHTMHProductDateFollowStaffVO) {
		this.lstRptTDBHTMHProductDateFollowStaffVO = lstRptTDBHTMHProductDateFollowStaffVO;
	}	
}

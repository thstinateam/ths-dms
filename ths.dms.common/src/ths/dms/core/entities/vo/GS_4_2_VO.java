package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class GS_4_2_VO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	
	private String maMien;
	private String maVung;
	private String maNPP;
	private String tenNPP;
	private String maGSNPP;
	private String tenGSNPP;
	private String maNVBH;
	private String tenNVBH;
	private String ngay;
	private String maTuyen;
	private String tenTuyen;
	private String maKH;
	private String tenKH;
	private String diaChi;
	private String loaiKH;
	private String oderDateC; //ngay ban hang
	private BigDecimal visitLat;
	private BigDecimal visitLng;
	private String thoiGianBatDau;
	private String thoiGianKetThuc;
	private String close;
	private BigDecimal soPhutGheTham;
	private BigDecimal khoangCach;
	private Integer haveVisitCustomer; //KH phai ghe tham trong ngay
	private Integer haveOrderCustomer; //KH ghe tham co don hang
	private Integer noOrderCustomer; //KH ghe tham ko co don hang
	private Integer giveOrderCustomer; //KH ghe tham co don hang tra
	private Integer closeCustomer; //KH dong cua
	private Integer noVisitCustomer; //KH ko ghe tham
	private Integer overCustomer; //Kh ngoai tuyen
	private Integer overOrderCustomer; //KH ngoai tuyen ko dh
	
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaGSNPP() {
		return maGSNPP;
	}
	public void setMaGSNPP(String maGSNPP) {
		this.maGSNPP = maGSNPP;
	}
	public String getTenGSNPP() {
		return tenGSNPP;
	}
	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getNgay() {
		return ngay;
	}
	public void setNgay(String ngay) {
		this.ngay = ngay;
	}
	public String getMaTuyen() {
		return maTuyen;
	}
	public void setMaTuyen(String maTuyen) {
		this.maTuyen = maTuyen;
	}
	public String getTenTuyen() {
		return tenTuyen;
	}
	public void setTenTuyen(String tenTuyen) {
		this.tenTuyen = tenTuyen;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getLoaiKH() {
		return loaiKH;
	}
	public void setLoaiKH(String loaiKH) {
		this.loaiKH = loaiKH;
	}
	public BigDecimal getVisitLat() {
		return visitLat;
	}
	public void setVisitLat(BigDecimal visitLat) {
		this.visitLat = visitLat;
	}
	public BigDecimal getVisitLng() {
		return visitLng;
	}
	public void setVisitLng(BigDecimal visitLng) {
		this.visitLng = visitLng;
	}
	public String getThoiGianBatDau() {
		return thoiGianBatDau;
	}
	public void setThoiGianBatDau(String thoiGianBatDau) {
		this.thoiGianBatDau = thoiGianBatDau;
	}
	public String getThoiGianKetThuc() {
		return thoiGianKetThuc;
	}
	public void setThoiGianKetThuc(String thoiGianKetThuc) {
		this.thoiGianKetThuc = thoiGianKetThuc;
	}
	public String getClose() {
		return close;
	}
	public void setClose(String close) {
		this.close = close;
	}
	public BigDecimal getSoPhutGheTham() {
		return soPhutGheTham;
	}
	public void setSoPhutGheTham(BigDecimal soPhutGheTham) {
		this.soPhutGheTham = soPhutGheTham;
	}
	public BigDecimal getKhoangCach() {
		return khoangCach;
	}
	public void setKhoangCach(BigDecimal khoangCach) {
		this.khoangCach = khoangCach;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Integer getHaveVisitCustomer() {
		return haveVisitCustomer;
	}
	public void setHaveVisitCustomer(Integer haveVisitCustomer) {
		this.haveVisitCustomer = haveVisitCustomer;
	}
	public Integer getHaveOrderCustomer() {
		return haveOrderCustomer;
	}
	public void setHaveOrderCustomer(Integer haveOrderCustomer) {
		this.haveOrderCustomer = haveOrderCustomer;
	}
	public Integer getNoOrderCustomer() {
		return noOrderCustomer;
	}
	public void setNoOrderCustomer(Integer noOrderCustomer) {
		this.noOrderCustomer = noOrderCustomer;
	}
	public Integer getGiveOrderCustomer() {
		return giveOrderCustomer;
	}
	public void setGiveOrderCustomer(Integer giveOrderCustomer) {
		this.giveOrderCustomer = giveOrderCustomer;
	}
	public Integer getCloseCustomer() {
		return closeCustomer;
	}
	public void setCloseCustomer(Integer closeCustomer) {
		this.closeCustomer = closeCustomer;
	}
	public Integer getNoVisitCustomer() {
		return noVisitCustomer;
	}
	public void setNoVisitCustomer(Integer noVisitCustomer) {
		this.noVisitCustomer = noVisitCustomer;
	}
	public Integer getOverCustomer() {
		return overCustomer;
	}
	public void setOverCustomer(Integer overCustomer) {
		this.overCustomer = overCustomer;
	}
	public Integer getOverOrderCustomer() {
		return overOrderCustomer;
	}
	public void setOverOrderCustomer(Integer overOrderCustomer) {
		this.overOrderCustomer = overOrderCustomer;
	}
	public String getOderDateC() {
		return oderDateC;
	}
	public void setOderDateC(String oderDateC) {
		this.oderDateC = oderDateC;
	}
	
	
}

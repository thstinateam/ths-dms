/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.StatusRecordsEquip;

/**
 * thong tin phieu thanh toan sua chua thiet bi
 * @author tuannd20
 * @version 1.0
 */
public class EquipmentRepairPaymentRecord implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String paymentRecordCode;
	private Date paymentDate;
	private StatusRecordsEquip status;
	private String paymentDateInDDMMYYYFormat; // temp de day tu client len action
	/** vuongmq 19/04/2015; them cac loai tien*/
	private BigDecimal workerPrice;
	private BigDecimal materialPrice;
	private BigDecimal totalAmount;
	
	private Long shopRoot; // shopRoot de phan quyen
	private List<RepairItemPaymentVO> paymentRecordDetails;
	
	public String getPaymentRecordCode() {
		return paymentRecordCode;
	}
	public void setPaymentRecordCode(String paymentRecordCode) {
		this.paymentRecordCode = paymentRecordCode;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public StatusRecordsEquip getStatus() {
		return status;
	}
	public void setStatus(StatusRecordsEquip status) {
		this.status = status;
	}
	public List<RepairItemPaymentVO> getPaymentRecordDetails() {
		return paymentRecordDetails;
	}
	public void setPaymentRecordDetails(List<RepairItemPaymentVO> paymentRecordDetails) {
		this.paymentRecordDetails = paymentRecordDetails;
	}
	public String getPaymentDateInDDMMYYYFormat() {
		return paymentDateInDDMMYYYFormat;
	}
	public void setPaymentDateInDDMMYYYFormat(String paymentDateInDDMMYYYFormat) {
		this.paymentDateInDDMMYYYFormat = paymentDateInDDMMYYYFormat;
	}
	public BigDecimal getWorkerPrice() {
		return workerPrice;
	}
	public void setWorkerPrice(BigDecimal workerPrice) {
		this.workerPrice = workerPrice;
	}
	public BigDecimal getMaterialPrice() {
		return materialPrice;
	}
	public void setMaterialPrice(BigDecimal materialPrice) {
		this.materialPrice = materialPrice;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Long getShopRoot() {
		return shopRoot;
	}
	public void setShopRoot(Long shopRoot) {
		this.shopRoot = shopRoot;
	}
}

/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * chi tiet hang muc sua chua
 * @author tuannd20
 */
public class RepairItemPaymentVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long itemId;
	private BigDecimal totalRepairAmount;
	
	public Long getItemId() {
		return itemId;
	}
	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}
	public BigDecimal getTotalRepairAmount() {
		return totalRepairAmount;
	}
	public void setTotalRepairAmount(BigDecimal totalRepairAmount) {
		this.totalRepairAmount = totalRepairAmount;
	}
}

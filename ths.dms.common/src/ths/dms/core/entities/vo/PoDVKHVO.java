package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PoDVKHVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5548977473815698560L;
	private String saleOrderNumber;
	private Integer poType;
	private List<PoConfirmVO> listPoConfirmVo;

	public PoDVKHVO() {
		listPoConfirmVo = new ArrayList<PoConfirmVO>();
	}

	public String getSaleOrderNumber() {
		return saleOrderNumber;
	}

	public void setSaleOrderNumber(String saleOrderNumber) {
		this.saleOrderNumber = saleOrderNumber;
	}

	public Integer getPoType() {
		return poType;
	}

	public void setPoType(Integer poType) {
		this.poType = poType;
	}
	
	public List<PoConfirmVO> getListPoConfirmVo() {
		return listPoConfirmVo;
	}

	public void setListPoConfirmVo(List<PoConfirmVO> listPoConfirmVo) {
		this.listPoConfirmVo = listPoConfirmVo;
	}

}

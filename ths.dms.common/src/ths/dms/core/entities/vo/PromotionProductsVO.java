package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.SaleOrderPromotion;


public class PromotionProductsVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<SaleOrderDetailVO> listProductPromotionSale;
	
	private SortedMap<Long, List<SaleOrderDetailVO>> sortListOutPut;
	//Danh sach sp ban huong CKMH
	private List<SaleOrderDetailVO> listSaleProductReceivePromo;
	
	private List<SaleOrderPromotion> listQuantityReceivedOrder;
	//moi them vao
	private List<SaleOrderDetailVO> listPromotionForOrderChange;
	private List<SaleOrderDetailVO> listOpenPromotionForOrder;
	
	//tungmt
	private List<SaleOrderDetailVO> listPromoDetailByProduct;//danh sach KM theo tung san pham
	
	public List<SaleOrderPromoDetail> listPromoDetailForProduct;
	
	public List<SaleOrderPromoDetail> listPromoDetailForOrder;
	
	//Danh sach so suat cua KM sp
	public List<SaleOrderPromotion> productQuantityReceivedList = new ArrayList<SaleOrderPromotion>();
	
	//Danh sach so suat cua KM don hang
	public List<SaleOrderPromotion> orderQuantityReceivedList = new ArrayList<SaleOrderPromotion>();
	
	private SaleOrderDetailVO promotionOrder;
	
	public SaleOrderDetailVO getPromotionOrder() {
		return promotionOrder;
	}
	public void setPromotionOrder(SaleOrderDetailVO promotionOrder) {
		this.promotionOrder = promotionOrder;
	}
	public List<SaleOrderPromotion> getProductQuantityReceivedList() {
		return productQuantityReceivedList;
	}
	public void setProductQuantityReceivedList(
			List<SaleOrderPromotion> productQuantityReceivedList) {
		this.productQuantityReceivedList = productQuantityReceivedList;
	}
	public List<SaleOrderPromotion> getOrderQuantityReceivedList() {
		return orderQuantityReceivedList;
	}
	public void setOrderQuantityReceivedList(
			List<SaleOrderPromotion> orderQuantityReceivedList) {
		this.orderQuantityReceivedList = orderQuantityReceivedList;
	}
	public List<SaleOrderPromoDetail> getListPromoDetailForProduct() {
		return listPromoDetailForProduct;
	}
	public void setListPromoDetailForProduct(
			List<SaleOrderPromoDetail> listPromoDetailForProduct) {
		this.listPromoDetailForProduct = listPromoDetailForProduct;
	}
	public List<SaleOrderPromoDetail> getListPromoDetailForOrder() {
		return listPromoDetailForOrder;
	}
	public void setListPromoDetailForOrder(
			List<SaleOrderPromoDetail> listPromoDetailForOrder) {
		this.listPromoDetailForOrder = listPromoDetailForOrder;
	}
	public List<SaleOrderPromotion> getListQuantityReceivedOrder() {
		return listQuantityReceivedOrder;
	}
	public void setListQuantityReceivedOrder(
			List<SaleOrderPromotion> listQuantityReceivedOrder) {
		this.listQuantityReceivedOrder = listQuantityReceivedOrder;
	}
	public List<SaleOrderDetailVO> getListProductPromotionSale() {
		return listProductPromotionSale;
	}
	public void setListProductPromotionSale(
			List<SaleOrderDetailVO> listProductPromotionSale) {
		this.listProductPromotionSale = listProductPromotionSale;
	}
	public SortedMap<Long, List<SaleOrderDetailVO>> getSortListOutPut() {
		return sortListOutPut;
	}
	public void setSortListOutPut(
			SortedMap<Long, List<SaleOrderDetailVO>> sortListOutPut) {
		this.sortListOutPut = sortListOutPut;
	}
	public List<SaleOrderDetailVO> getListPromotionForOrderChange() {
		return listPromotionForOrderChange;
	}
	public void setListPromotionForOrderChange(
			List<SaleOrderDetailVO> listPromotionForOrderChange) {
		this.listPromotionForOrderChange = listPromotionForOrderChange;
	}
	public List<SaleOrderDetailVO> getListSaleProductReceivePromo() {
		return listSaleProductReceivePromo;
	}

	public void setListSaleProductReceivePromo(
			List<SaleOrderDetailVO> listSaleProductReceivePromo) {
		this.listSaleProductReceivePromo = listSaleProductReceivePromo;
	}
	public List<SaleOrderDetailVO> getListOpenPromotionForOrder() {
		return listOpenPromotionForOrder;
	}
	public void setListOpenPromotionForOrder(
			List<SaleOrderDetailVO> listOpenPromotionForOrder) {
		this.listOpenPromotionForOrder = listOpenPromotionForOrder;
	}
	public List<SaleOrderDetailVO> getListPromoDetailByProduct() {
		return listPromoDetailByProduct;
	}
	public void setListPromoDetailByProduct(
			List<SaleOrderDetailVO> listPromoDetailByProduct) {
		this.listPromoDetailByProduct = listPromoDetailByProduct;
	}
	
}
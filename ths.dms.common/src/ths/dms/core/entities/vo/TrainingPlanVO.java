package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TrainingPlanVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String codeGSNPP;
	private String nameGSNPP;
	private String codeNPP;
	private String codeNVBH;
	private String nameNVBH;
	private Date trainingDate;
	private String dateStr;
	private String strHidden;
	private Long trainingPlanId;
	
	

	public String getCodeGSNPP() {
		return codeGSNPP;
	}
	public void setCodeGSNPP(String codeGSNPP) {
		this.codeGSNPP = codeGSNPP;
	}
	public String getNameGSNPP() {
		return nameGSNPP;
	}
	public Long getTrainingPlanId() {
		return trainingPlanId;
	}
	public void setTrainingPlanId(Long trainingPlanId) {
		this.trainingPlanId = trainingPlanId;
	}
	public void setNameGSNPP(String nameGSNPP) {
		this.nameGSNPP = nameGSNPP;
	}
	public String getCodeNPP() {
		return codeNPP;
	}
	public void setCodeNPP(String codeNPP) {
		this.codeNPP = codeNPP;
	}
	public String getCodeNVBH() {
		return codeNVBH;
	}
	public void setCodeNVBH(String codeNVBH) {
		this.codeNVBH = codeNVBH;
	}
	public String getNameNVBH() {
		return nameNVBH;
	}
	public void setNameNVBH(String nameNVBH) {
		this.nameNVBH = nameNVBH;
	}
	public Date getTrainingDate() {
		return trainingDate;
	}
	public void setTrainingDate(Date trainingDate) {
		this.trainingDate = trainingDate;
	}
	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
	public String getDateStr() {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		if(this.trainingDate!=null) {
			this.dateStr = df.format(this.trainingDate);
		}
		return dateStr;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setStrHidden(String strHidden) {
		this.strHidden = strHidden;
	}
	public String getStrHidden() {
		return strHidden;
	}
}

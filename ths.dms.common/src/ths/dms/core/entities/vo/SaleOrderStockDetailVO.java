package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * 
 * @author vuongmq
 * @since 27- August, 2014
 * @description Load grid Cap nhat phai thu ca kho
 *
 */
public class SaleOrderStockDetailVO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long id;//saleOrderId ma don hang // hoac cac don STOCK_TRANS_ID
	private Long shopId;
	private Long staffId;
	private Long detailId;
	private Long productId;
	private Integer quatity;
	private Long stockId;
	private Long warehouseId;
	private String orderType;
	// don hang prresale
	private Long fromOwnerId;
	private Integer fromOwnerType;
	private Long toOwnerId;
	private Integer toOwnerType;
	
	public Long getDetailId() {
		return detailId;
	}
	public void setDetailId(Long detailId) {
		this.detailId = detailId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Integer getQuatity() {
		return quatity;
	}
	public void setQuatity(Integer quatity) {
		this.quatity = quatity;
	}
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	public Long getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public Long getFromOwnerId() {
		return fromOwnerId;
	}
	public void setFromOwnerId(Long fromOwnerId) {
		this.fromOwnerId = fromOwnerId;
	}
	public Integer getFromOwnerType() {
		return fromOwnerType;
	}
	public void setFromOwnerType(Integer fromOwnerType) {
		this.fromOwnerType = fromOwnerType;
	}
	public Long getToOwnerId() {
		return toOwnerId;
	}
	public void setToOwnerId(Long toOwnerId) {
		this.toOwnerId = toOwnerId;
	}
	public Integer getToOwnerType() {
		return toOwnerType;
	}
	public void setToOwnerType(Integer toOwnerType) {
		this.toOwnerType = toOwnerType;
	}
	
}
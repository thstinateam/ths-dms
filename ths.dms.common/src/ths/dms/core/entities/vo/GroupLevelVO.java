package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * edit by tientv11
 * The Class GroupLevelVO.
 */
public class GroupLevelVO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id. */
	Long id;
	
	/** The group text. */
	String groupText;
	
	/** The order number. */
	Integer orderNumber;
	
	/** The min quantity. */
	Integer minQuantity;
	
	/** The max quantity. */
	Integer maxQuantity;
	
	/** The min amount. */
	BigDecimal minAmount;
	
	/** The max amount. */
	BigDecimal maxAmount;
	
	/** The promotion percent. */
	Float promotionPercent;
	
	/** The has product. */
	Integer hasProduct;
	
	/** The group level detail id. */
	Long groupLevelDetailId;
	
	/** The product id. */
	Long productId;
	
	/** The value type. */
	Integer valueType;
	
	/** The value. */
	BigDecimal value;
	
	/** The is required. */
	Integer isRequired;
	
	/** The detail. */
	List<GroupLevelDetailVO> detail;
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Gets the group text.
	 *
	 * @return the group text
	 */
	public String getGroupText() {
		return groupText;
	}
	
	/**
	 * Sets the group text.
	 *
	 * @param groupText the new group text
	 */
	public void setGroupText(String groupText) {
		this.groupText = groupText;
	}
	
	/**
	 * Gets the min quantity.
	 *
	 * @return the min quantity
	 */
	public Integer getMinQuantity() {
		if (minQuantity == null) return 0;
		else return minQuantity;
	}
	
	/**
	 * Sets the min quantity.
	 *
	 * @param minQuantity the new min quantity
	 */
	public void setMinQuantity(Integer minQuantity) {
		this.minQuantity = minQuantity;
	}
	
	/**
	 * Gets the max quantity.
	 *
	 * @return the max quantity
	 */
	public Integer getMaxQuantity() {
		if (maxQuantity == null) return 0;
		else return maxQuantity;
	}
	
	/**
	 * Sets the max quantity.
	 *
	 * @param maxQuantity the new max quantity
	 */
	public void setMaxQuantity(Integer maxQuantity) {
		this.maxQuantity = maxQuantity;
	}
	
	/**
	 * Gets the min amount.
	 *
	 * @return the min amount
	 */
	public BigDecimal getMinAmount() {
		if (minAmount == null) return BigDecimal.ZERO;
		else return minAmount;
	}
	
	/**
	 * Sets the min amount.
	 *
	 * @param minAmount the new min amount
	 */
	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}
	
	/**
	 * Gets the max amount.
	 *
	 * @return the max amount
	 */
	public BigDecimal getMaxAmount() {
		if (maxAmount == null) return BigDecimal.ZERO;
		else return maxAmount;
	}
	
	/**
	 * Sets the max amount.
	 *
	 * @param maxAmount the new max amount
	 */
	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}
	
	/**
	 * Gets the detail.
	 *
	 * @return the detail
	 */
	public List<GroupLevelDetailVO> getDetail() {
		return detail;
	}
	
	/**
	 * Sets the detail.
	 *
	 * @param detail the new detail
	 */
	public void setDetail(List<GroupLevelDetailVO> detail) {
		this.detail = detail;
	}
	
	/**
	 * Gets the order number.
	 *
	 * @return the order number
	 */
	public Integer getOrderNumber() {
		return orderNumber;
	}
	
	/**
	 * Sets the order number.
	 *
	 * @param orderNumber the new order number
	 */
	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	/**
	 * Gets the promotion percent.
	 *
	 * @return the promotion percent
	 */
	public Float getPromotionPercent() {
		return promotionPercent;
	}
	
	/**
	 * Sets the promotion percent.
	 *
	 * @param promotionPercent the new promotion percent
	 */
	public void setPromotionPercent(Float promotionPercent) {
		this.promotionPercent = promotionPercent;
	}

	public Integer getHasProduct() {
		return hasProduct;
	}

	public void setHasProduct(Integer hasProduct) {
		this.hasProduct = hasProduct;
	}

	public Long getGroupLevelDetailId() {
		return groupLevelDetailId;
	}

	public void setGroupLevelDetailId(Long groupLevelDetailId) {
		this.groupLevelDetailId = groupLevelDetailId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Integer getValueType() {
		return valueType;
	}

	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public Integer getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(Integer isRequired) {
		this.isRequired = isRequired;
	}
	
	
}

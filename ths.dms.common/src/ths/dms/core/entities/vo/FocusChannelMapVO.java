package ths.dms.core.entities.vo;

import java.io.Serializable;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.FocusChannelMap;


public class FocusChannelMapVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private FocusChannelMap focusChannelMap;
	
	private ApParam apParam;

	public FocusChannelMap getFocusChannelMap() {
		return focusChannelMap;
	}

	public void setFocusChannelMap(FocusChannelMap focusChannelMap) {
		this.focusChannelMap = focusChannelMap;
	}

	public ApParam getApParam() {
		return apParam;
	}

	public void setApParam(ApParam apParam) {
		this.apParam = apParam;
	}
	
	
}

package ths.dms.core.entities.vo;
/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Class Equip Borrow VO
 * 
 * @author nhutnn
 * @since 02/07/2015
 */
public class EquipLendDetailVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long equipLendId;
	private String equipLendCode;
	private String note;
	private String shopCode;
	private String shopName;
	private String shopCodeVung;
	private String shopCodeMien;
	private String shopCodeKenh;
	private String customerCode;
	private String customerCodeLong;
	private String customerName;
	private String representative;
	private String relation;
	private String phone;
	private String address;
	private String street;
	private String wardCode;
	private String ward;
	private String districtCode;
	private String district;
	private String provinceCode;
	private String province;
	private String idNo;
	private String idNoPlace;
	private String idNoDate;
	private String businessNo;
	private String businessNoPlace;
	private String businessNoDate;
	private String shopAddress;
	private BigDecimal quantity;
	private String equipCategory;
	private String equipCategoryCode;
	private String healthStatus;
	private String healthStatusCode;
	private String timeLend;
	private String stockType;
	private Integer stockTypeValue;
	private BigDecimal amount;
	private String staffCode;
	private String staffName;
	private String staffPhone;
	private String staffMobile;
	private String capacity;
	
	public Long getEquipLendId() {
		return equipLendId;
	}
	public void setEquipLendId(Long equipLendId) {
		this.equipLendId = equipLendId;
	}
	public String getShopCodeMien() {
		return shopCodeMien;
	}
	public void setShopCodeMien(String shopCodeMien) {
		this.shopCodeMien = shopCodeMien;
	}
	public String getShopCodeKenh() {
		return shopCodeKenh;
	}
	public void setShopCodeKenh(String shopCodeKenh) {
		this.shopCodeKenh = shopCodeKenh;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getRepresentative() {
		return representative;
	}
	public void setRepresentative(String representative) {
		this.representative = representative;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}	
	public String getBusinessNo() {
		return businessNo;
	}
	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}
	public String getBusinessNoPlace() {
		return businessNoPlace;
	}
	public void setBusinessNoPlace(String businessNoPlace) {
		this.businessNoPlace = businessNoPlace;
	}
	public String getBusinessNoDate() {
		return businessNoDate;
	}
	public void setBusinessNoDate(String businessNoDate) {
		this.businessNoDate = businessNoDate;
	}
	public String getTimeLend() {
		return timeLend;
	}
	public void setTimeLend(String timeLend) {
		this.timeLend = timeLend;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getStaffPhone() {
		return staffPhone;
	}
	public void setStaffPhone(String staffPhone) {
		this.staffPhone = staffPhone;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public String getHealthStatus() {
		return healthStatus;
	}
	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}
	public String getEquipLendCode() {
		return equipLendCode;
	}
	public void setEquipLendCode(String equipLendCode) {
		this.equipLendCode = equipLendCode;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getWardCode() {
		return wardCode;
	}
	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}
	public String getDistrictCode() {
		return districtCode;
	}
	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}
	public String getProvinceCode() {
		return provinceCode;
	}
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	public String getIdNoPlace() {
		return idNoPlace;
	}
	public void setIdNoPlace(String idNoPlace) {
		this.idNoPlace = idNoPlace;
	}
	public String getIdNoDate() {
		return idNoDate;
	}
	public void setIdNoDate(String idNoDate) {
		this.idNoDate = idNoDate;
	}
	public String getEquipCategory() {
		return equipCategory;
	}
	public void setEquipCategory(String equipCategory) {
		this.equipCategory = equipCategory;
	}
	public String getEquipCategoryCode() {
		return equipCategoryCode;
	}
	public void setEquipCategoryCode(String equipCategoryCode) {
		this.equipCategoryCode = equipCategoryCode;
	}
	public String getHealthStatusCode() {
		return healthStatusCode;
	}
	public void setHealthStatusCode(String healthStatusCode) {
		this.healthStatusCode = healthStatusCode;
	}
	public String getStockType() {
		return stockType;
	}
	public void setStockType(String stockType) {
		this.stockType = stockType;
	}
	public Integer getStockTypeValue() {
		return stockTypeValue;
	}
	public void setStockTypeValue(Integer stockTypeValue) {
		this.stockTypeValue = stockTypeValue;
	}
	public String getWard() {
		return ward;
	}
	public void setWard(String ward) {
		this.ward = ward;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCustomerCodeLong() {
		return customerCodeLong;
	}
	public void setCustomerCodeLong(String customerCodeLong) {
		this.customerCodeLong = customerCodeLong;
	}
	public String getStaffMobile() {
		return staffMobile;
	}
	public void setStaffMobile(String staffMobile) {
		this.staffMobile = staffMobile;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopCodeVung() {
		return shopCodeVung;
	}
	public void setShopCodeVung(String shopCodeVung) {
		this.shopCodeVung = shopCodeVung;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class DisplayToolCustomerVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id; // DISPLAY_TOOL_ID
	
	private Long shopId;
	
	private String shopCode;
	
	private Long staffId;
	
	private String staffCode;
	
	private String staffName;
	
	private String staffCodeName;
	
	private String toolCode;
	
	private String custom;
	
	private String shorCode; // 3 ky tu dau cua customer_code
	
	private String customerCode; 
	
	private String customerCodeName; //shor_code + customer_name
	
	private String customerName;
	
	private BigDecimal amount;
	
	private Integer quantity;
	
	private String month;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getToolCode() {
		return toolCode;
	}

	public void setToolCode(String toolCode) {
		this.toolCode = toolCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getCustom() {
		return custom;
	}

	public void setCustom(String custom) {
		this.custom = custom;
	}

	public String getShorCode() {
		return shorCode;
	}

	public void setShorCode(String shorCode) {
		this.shorCode = shorCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerCodeName() {
		return customerCodeName;
	}

	public void setCustomerCodeName(String customerCodeName) {
		this.customerCodeName = customerCodeName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffCodeName() {
		return staffCodeName;
	}

	public void setStaffCodeName(String staffCodeName) {
		this.staffCodeName = staffCodeName;
	}
	
	
	
	
	
}

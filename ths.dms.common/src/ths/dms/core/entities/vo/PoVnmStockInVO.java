/*
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Mo ta class PoVnmStockInVO.java
 * @author vuongmq
 * @since Jan 27, 2016
 */
public class PoVnmStockInVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String saleOrderNumber;
	private String poCoNumber;
	private String invoiceNumber;
	private String poAutoNumber;
	
	private Integer type;
	private Integer status;
	
	private Date orderDate;
	private String orderDateStr;
	private Date importDate;
	private String importDateStr;
	private Date poVnmDate;
	private String poVnmDateStr;
	private Date requestedDate; // ngay yeu cau giao hang
	private String requestedDateStr;
	private Date receivedDate; // ngay nhap (lay cuoi cung trong PO_VNM_DETAIL_RECEIVED)
	private String receivedDateStr;

	private BigDecimal amount;
	private BigDecimal discount;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSaleOrderNumber() {
		return saleOrderNumber;
	}
	public void setSaleOrderNumber(String saleOrderNumber) {
		this.saleOrderNumber = saleOrderNumber;
	}
	public String getPoCoNumber() {
		return poCoNumber;
	}
	public void setPoCoNumber(String poCoNumber) {
		this.poCoNumber = poCoNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getPoAutoNumber() {
		return poAutoNumber;
	}
	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderDateStr() {
		return orderDateStr;
	}
	public void setOrderDateStr(String orderDateStr) {
		this.orderDateStr = orderDateStr;
	}
	public Date getImportDate() {
		return importDate;
	}
	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}
	public String getImportDateStr() {
		return importDateStr;
	}
	public void setImportDateStr(String importDateStr) {
		this.importDateStr = importDateStr;
	}
	public Date getPoVnmDate() {
		return poVnmDate;
	}
	public void setPoVnmDate(Date poVnmDate) {
		this.poVnmDate = poVnmDate;
	}
	public String getPoVnmDateStr() {
		return poVnmDateStr;
	}
	public void setPoVnmDateStr(String poVnmDateStr) {
		this.poVnmDateStr = poVnmDateStr;
	}
	public Date getRequestedDate() {
		return requestedDate;
	}
	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}
	public String getRequestedDateStr() {
		return requestedDateStr;
	}
	public void setRequestedDateStr(String requestedDateStr) {
		this.requestedDateStr = requestedDateStr;
	}
	public Date getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
	public String getReceivedDateStr() {
		return receivedDateStr;
	}
	public void setReceivedDateStr(String receivedDateStr) {
		this.receivedDateStr = receivedDateStr;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
}
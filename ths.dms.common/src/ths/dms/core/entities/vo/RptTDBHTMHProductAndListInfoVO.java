package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author sangtn
 * @see Bao cao theo doi ban hang theo mat hang
 *
 */
public class RptTDBHTMHProductAndListInfoVO implements Serializable{

	/**
	 * 
	 */
	//Cac thong tin
	//Ma mat hang, ten mat hang PRODUCT_ID, PRODUCT_NAME trong PRODUCT
	//Don gia PRICE trong SALE_ORDER_DETAIL
	//Tong so luong ban/tra , Tong thanh tien: Tinh toan tu RptTDBHTMHProductFollowDateVO
	
	private BigDecimal productId;
	private String productName;
	private String productCode;
	private BigDecimal price;
	private BigDecimal sumProductAndPriceQuantity;
	private BigDecimal sumProductAndPriceAmount;
	private List<RptTDBHTMHProductFollowDateVO> lstRptTDBHTMHProductFollowDateVO = new ArrayList<RptTDBHTMHProductFollowDateVO>();
	public BigDecimal getProductId() {
		return productId;
	}
	public void setProductId(BigDecimal productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	
	public BigDecimal getSumProductAndPriceQuantity() {
		return sumProductAndPriceQuantity;
	}
	public void setSumProductAndPriceQuantity(BigDecimal sumProductAndPriceQuantity) {
		this.sumProductAndPriceQuantity = sumProductAndPriceQuantity;
	}
	public BigDecimal getSumProductAndPriceAmount() {
		return sumProductAndPriceAmount;
	}
	public void setSumProductAndPriceAmount(BigDecimal sumProductAndPriceAmount) {
		this.sumProductAndPriceAmount = sumProductAndPriceAmount;
	}
	public List<RptTDBHTMHProductFollowDateVO> getLstRptTDBHTMHProductFollowDateVO() {
		return lstRptTDBHTMHProductFollowDateVO;
	}
	public void setLstRptTDBHTMHProductFollowDateVO(
			List<RptTDBHTMHProductFollowDateVO> lstRptTDBHTMHProductFollowDateVO) {
		this.lstRptTDBHTMHProductFollowDateVO = lstRptTDBHTMHProductFollowDateVO;
	}
	
}

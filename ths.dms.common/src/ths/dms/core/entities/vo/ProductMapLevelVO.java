package ths.dms.core.entities.vo;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.ActiveType;

public class ProductMapLevelVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String departmentCode;
	
	private String departmentName;
	
	private String levelCode;
	
	private Long levelId;
	
	private Integer status;

	public Long getLevelId() {
		return levelId;
	}

	public void setLevelId(Long levelId) {
		this.levelId = levelId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getLevelCode() {
		return levelCode;
	}

	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}

	public ActiveType getStatus() {
		return ActiveType.parseValue(status);
	}
	
	public void setStatus(Integer status) {
		this.status = status;
	}

}

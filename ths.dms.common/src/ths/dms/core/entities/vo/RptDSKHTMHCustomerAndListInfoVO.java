package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptDSKHTMHCustomerAndListInfoVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String customerCode;
	private String customerName;
	List<RptDSKHTMHCatAndListInfoVO> listStock = new ArrayList<RptDSKHTMHCatAndListInfoVO>();
	private BigDecimal sumQuantitySaleForCustomer;
	private BigDecimal sumQuantityDisplayForCustomer;
	private BigDecimal sumMoneyForCustomer;
	private String customerAddress;

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public List<RptDSKHTMHCatAndListInfoVO> getListStock() {
		return listStock;
	}

	public void setListStock(List<RptDSKHTMHCatAndListInfoVO> listStock) {
		this.listStock = listStock;
	}

	public BigDecimal getSumQuantitySaleForCustomer() {
		return sumQuantitySaleForCustomer;
	}

	public void setSumQuantitySaleForCustomer(
			BigDecimal sumQuantitySaleForCustomer) {
		this.sumQuantitySaleForCustomer = sumQuantitySaleForCustomer;
	}

	public BigDecimal getSumQuantityDisplayForCustomer() {
		return sumQuantityDisplayForCustomer;
	}

	public void setSumQuantityDisplayForCustomer(
			BigDecimal sumQuantityDisplayForCustomer) {
		this.sumQuantityDisplayForCustomer = sumQuantityDisplayForCustomer;
	}

	public BigDecimal getSumMoneyForCustomer() {
		return sumMoneyForCustomer;
	}

	public void setSumMoneyForCustomer(BigDecimal sumMoneyForCustomer) {
		this.sumMoneyForCustomer = sumMoneyForCustomer;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	
}

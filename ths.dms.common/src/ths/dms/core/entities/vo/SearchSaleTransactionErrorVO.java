package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchSaleTransactionErrorVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String orderNumber = "";
	
	private String carNumber = "";
	
	private String cashierStaff = "";
	
	private String deliveryStaff = "";
	
	private String saleStaff = "";
	
	private String orderNow = "";
	
	private String approve = "";
	
	private String debitMax = "";
	
	private String debitMaxDate = "";
	private String quantityErr = "";
	private String promotionErr = "";
	private String amountErr = "";
	private String priceErr = "";
	private String changeQuantity = "";
	private String changeRation = "";
	private String keyShopErr = "";
	private String orderNotIntegrityErr = "";
	private String openPromo = "";
	private String lostOpenPromo = "";
	private String accumulation = "";
	private String SOCOErr = "";
	private String confirmedErr = "";
	private String systemError = "";
	private String notExist;
	private String customerNotActive = "";
	
	private String customerCode;
	private String customerName;
	private List<String> productCode = new ArrayList<String>();
	
	private List<String> productLot = new ArrayList<String>();
	
	private List<String> productLotDiff = new ArrayList<String>();
	
	private List<String> productChooseLot = new ArrayList<String>();
	
	private List<String> promotionShop = new ArrayList<String>();
	
	private List<String> promotionReceived = new ArrayList<String>();
	
	private String notEnoughPortion = "";
	
	
	public String getChangeQuantity() {
		return changeQuantity;
	}
	public void setChangeQuantity(String changeQuantity) {
		this.changeQuantity = changeQuantity;
	}
	public String getChangeRation() {
		return changeRation;
	}
	public void setChangeRation(String changeRation) {
		this.changeRation = changeRation;
	}
	public String getSOCOErr() {
		return SOCOErr;
	}
	public void setSOCOErr(String sOCOErr) {
		SOCOErr = sOCOErr;
	}
	public String getQuantityErr() {
		return quantityErr;
	}
	public void setQuantityErr(String quantityErr) {
		this.quantityErr = quantityErr;
	}
	public String getPromotionErr() {
		return promotionErr;
	}
	public void setPromotionErr(String promotionErr) {
		this.promotionErr = promotionErr;
	}
	public String getAmountErr() {
		return amountErr;
	}
	public void setAmountErr(String amountErr) {
		this.amountErr = amountErr;
	}
	public String getPriceErr() {
		return priceErr;
	}
	public void setPriceErr(String priceErr) {
		this.priceErr = priceErr;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getDebitMaxDate() {
		return debitMaxDate;
	}
	public void setDebitMaxDate(String debitMaxDate) {
		this.debitMaxDate = debitMaxDate;
	}
	public String getDebitMax() {
		return debitMax;
	}
	public void setDebitMax(String debitMax) {
		this.debitMax = debitMax;
	}
	public String getApprove() {
		return approve;
	}
	public void setApprove(String approve) {
		this.approve = approve;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getCarNumber() {
		return carNumber;
	}
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}
	public String getCashierStaff() {
		return cashierStaff;
	}
	public void setCashierStaff(String cashierStaff) {
		this.cashierStaff = cashierStaff;
	}
	public String getDeliveryStaff() {
		return deliveryStaff;
	}
	public void setDeliveryStaff(String deliveryStaff) {
		this.deliveryStaff = deliveryStaff;
	}
	public String getSaleStaff() {
		return saleStaff;
	}
	public void setSaleStaff(String saleStaff) {
		this.saleStaff = saleStaff;
	}
	public List<String> getProductCode() {
		return productCode;
	}
	public void setProductCode(List<String> productCode) {
		this.productCode = productCode;
	}
	public List<String> getProductLot() {
		return productLot;
	}
	public void setProductLot(List<String> productLot) {
		this.productLot = productLot;
	}
	public List<String> getProductLotDiff() {
		return productLotDiff;
	}
	public void setProductLotDiff(List<String> productLotDiff) {
		this.productLotDiff = productLotDiff;
	}
	public List<String> getProductChooseLot() {
		return productChooseLot;
	}
	public void setProductChooseLot(List<String> productChooseLot) {
		this.productChooseLot = productChooseLot;
	}
	public List<String> getPromotionShop() {
		return promotionShop;
	}
	public void setPromotionShop(List<String> promotionShop) {
		this.promotionShop = promotionShop;
	}
	public List<String> getPromotionReceived() {
		return promotionReceived;
	}
	public void setPromotionReceived(List<String> promotionReceived) {
		this.promotionReceived = promotionReceived;
	}
	
	public String getOrderNow() {
		return orderNow;
	}
	public void setOrderNow(String orderNow) {
		this.orderNow = orderNow;
	}
	public boolean containError(){
		if(	orderNumber.length()>0 || carNumber.length()>0 
			|| orderNow.length()>0 || cashierStaff.length()>0 || deliveryStaff.length()>0 || debitMax.length()>0		
			|| productCode.size()>0 || productLot.size()>0 
			|| productLotDiff.size()>0 || productLotDiff.size()>0
			|| productChooseLot.size()>0 || productChooseLot.size()>0
			|| promotionShop.size()>0 || promotionReceived.size()>0){
			return true;
		}				
		return false;
	}
	public String getNotExist() {
		return notExist;
	}
	public void setNotExist(String notExist) {
		this.notExist = notExist;
	}
	public String getOpenPromo() {
		return openPromo;
	}
	public void setOpenPromo(String openPromo) {
		this.openPromo = openPromo;
	}
	public String getAccumulation() {
		return accumulation;
	}
	public void setAccumulation(String accumulation) {
		this.accumulation = accumulation;
	}
	public String getLostOpenPromo() {
		return lostOpenPromo;
	}
	public void setLostOpenPromo(String lostOpenPromo) {
		this.lostOpenPromo = lostOpenPromo;
	}
	public String getNotEnoughPortion() {
		return notEnoughPortion;
	}
	public void setNotEnoughPortion(String notEnoughPortion) {
		this.notEnoughPortion = notEnoughPortion;
	}
	public String getKeyShopErr() {
		return keyShopErr;
	}
	public void setKeyShopErr(String keyShopErr) {
		this.keyShopErr = keyShopErr;
	}
	public String getOrderNotIntegrityErr() {
		return orderNotIntegrityErr;
	}
	public void setOrderNotIntegrityErr(String orderNotIntegrityErr) {
		this.orderNotIntegrityErr = orderNotIntegrityErr;
	}
	public String getConfirmedErr() {
		return confirmedErr;
	}
	public void setConfirmedErr(String confirmedErr) {
		this.confirmedErr = confirmedErr;
	}
	public String getSystemError() {
		return systemError;
	}
	public void setSystemError(String systemError) {
		this.systemError = systemError;
	}
	public String getCustomerNotActive() {
		return customerNotActive;
	}
	public void setCustomerNotActive(String customerNotActive) {
		this.customerNotActive = customerNotActive;
	}
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Class Staff VO
 * 
 *@author hunglm16
 *@since September 24,2014 
 */
public class StaffVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long shopId;
	private Long parentStaffMapId;
	private Long parentStaffId;
	private Long staffOwnerId;
	private Long staffId;
	private Long inheritUserPriv;	
	private Long catId;

	private Integer objectType;
	private Integer hasPosition;
	private Integer isLevel;
	private Integer status;

	private String shopCode;
	private String shopName;
	private String channelTypeCode;
	private String channelTypeName;
	private String staffCode;
	private String staffName;
	private String vanLock;
	private String lockDate;
	private String deliveryStaff;
	private String saleStaff;
	private String statusStr;
	private String address;
	private String gsName;
	private Date fromDate;
	private Date toDate;
	
	private String phone;
	private String mobilePhone;
	private String staffType;
	
	public Long getInheritUserPriv() {
		return inheritUserPriv;
	}
	public void setInheritUserPriv(Long inheritUserPriv) {
		this.inheritUserPriv = inheritUserPriv;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getParentStaffMapId() {
		return parentStaffMapId;
	}
	public void setParentStaffMapId(Long parentStaffMapId) {
		this.parentStaffMapId = parentStaffMapId;
	}
	public Long getParentStaffId() {
		return parentStaffId;
	}
	public void setParentStaffId(Long parentStaffId) {
		this.parentStaffId = parentStaffId;
	}
	public Long getStaffOwnerId() {
		return staffOwnerId;
	}
	public void setStaffOwnerId(Long staffOwnerId) {
		this.staffOwnerId = staffOwnerId;
	}
	public Integer getIsLevel() {
		return isLevel;
	}
	public void setIsLevel(Integer isLevel) {
		this.isLevel = isLevel;
	}
	
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getChannelTypeCode() {
		return channelTypeCode;
	}
	public void setChannelTypeCode(String channelTypeCode) {
		this.channelTypeCode = channelTypeCode;
	}
	public String getChannelTypeName() {
		return channelTypeName;
	}
	public void setChannelTypeName(String channelTypeName) {
		this.channelTypeName = channelTypeName;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
		this.id = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public Integer getHasPosition() {
		if (hasPosition != null && hasPosition.intValue() > 1) {
			return 1;
		}
		return hasPosition;
	}
	public void setHasPosition(Integer hasPosition) {
		this.hasPosition = hasPosition;
	}
	public String getDeliveryStaff() {
		return deliveryStaff;
	}
	public void setDeliveryStaff(String deliveryStaff) {
		this.deliveryStaff = deliveryStaff;
	}
	public String getSaleStaff() {
		return saleStaff;
	}
	public void setSaleStaff(String saleStaff) {
		this.saleStaff = saleStaff;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	public String getVanLock() {
		return vanLock;
	}
	public void setVanLock(String vanLock) {
		this.vanLock = vanLock;
	}
	public String getLockDate() {
		return lockDate;
	}
	public void setLockDate(String lockDate) {
		this.lockDate = lockDate;
	}
	public String getStaffType() {
		return staffType;
	}
	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getGsName() {
		return gsName;
	}
	public void setGsName(String gsName) {
		this.gsName = gsName;
	}
	public Long getCatId() {
		return catId;
	}
	public void setCatId(Long catId) {
		this.catId = catId;
	}
}

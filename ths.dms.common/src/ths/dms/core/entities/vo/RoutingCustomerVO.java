package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

public class RoutingCustomerVO implements Serializable{
	/**
	 * @author hunglm16
	 * @description Thong tin tuyen
	 */
	private static final long serialVersionUID = 1L;
	
	private Long routingCustomerId;
	private Integer weekInterval;
	private Integer startWeek;
	private Integer monday;
	private Integer tuesday;
	private Integer wednesday;
	private Integer thursday;
	private Integer friday;
	private Integer saturday;
	private Integer sunday;
	private Date startDate;
	private Date endDate;
	private Long routingId;
	private String routingName;
	private Long customerId;
	private String shortCode;
	private String customerName;
	private String customerCode;
	private String address;
	private Double lat;
	private Double lng;
	private Integer status;
	private Integer isDel;
	private String startDateStr;
	private String routingCode;
	private Integer seq2;
	private Integer seq3;
	private Integer seq4;
	private Integer seq5;
	private Integer seq6;
	private Integer seq7;
	private Integer seq8;
	private Integer tansuat;
	public Integer getWeek1() {
		return week1;
	}
	public void setWeek1(Integer week1) {
		this.week1 = week1;
	}
	public Integer getWeek2() {
		return week2;
	}
	public void setWeek2(Integer week2) {
		this.week2 = week2;
	}
	public Integer getWeek3() {
		return week3;
	}
	public void setWeek3(Integer week3) {
		this.week3 = week3;
	}
	public Integer getWeek4() {
		return week4;
	}
	public void setWeek4(Integer week4) {
		this.week4 = week4;
	}
	private Integer seq;
	private Integer week1;
	private Integer week2;
	private Integer week3;
	private Integer week4;
	
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public Integer getSeq2() {
		return seq2;
	}
	public void setSeq2(Integer seq2) {
		this.seq2 = seq2;
	}
	public Integer getSeq3() {
		return seq3;
	}
	public void setSeq3(Integer seq3) {
		this.seq3 = seq3;
	}
	public Integer getSeq4() {
		return seq4;
	}
	public void setSeq4(Integer seq4) {
		this.seq4 = seq4;
	}
	public Integer getSeq5() {
		return seq5;
	}
	public void setSeq5(Integer seq5) {
		this.seq5 = seq5;
	}
	public Integer getSeq6() {
		return seq6;
	}
	public void setSeq6(Integer seq6) {
		this.seq6 = seq6;
	}
	public Integer getSeq7() {
		return seq7;
	}
	public void setSeq7(Integer seq7) {
		this.seq7 = seq7;
	}
	public Integer getSeq8() {
		return seq8;
	}
	public void setSeq8(Integer seq8) {
		this.seq8 = seq8;
	}
	public String getRoutingCode() {
		return routingCode;
	}
	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}
	public String getStartDateStr() {
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLng() {
		return lng;
	}
	public void setLng(Double lng) {
		this.lng = lng;
	}
	public String getEndDateStr() {
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	private String endDateStr;

	
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
	/*public Integer getLat() {
		return lat;
	}
	public void setLat(Integer lat) {
		this.lat = lat;
	}
	public Integer getLng() {
		return lng;
	}
	public void setLng(Integer lng) {
		this.lng = lng;
	}*/
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Long getRoutingCustomerId() {
		return routingCustomerId;
	}
	public void setRoutingCustomerId(Long routingCustomerId) {
		this.routingCustomerId = routingCustomerId;
	}
	public Integer getWeekInterval() {
		return weekInterval;
	}
	public void setWeekInterval(Integer weekInterval) {
		this.weekInterval = weekInterval;
	}
	public Integer getStartWeek() {
		return startWeek;
	}
	public void setStartWeek(Integer startWeek) {
		this.startWeek = startWeek;
	}
	public Integer getMonday() {
		return monday;
	}
	public void setMonday(Integer monday) {
		this.monday = monday;
	}
	public Integer getTuesday() {
		return tuesday;
	}
	public void setTuesday(Integer tuesday) {
		this.tuesday = tuesday;
	}
	public Integer getWednesday() {
		return wednesday;
	}
	public void setWednesday(Integer wednesday) {
		this.wednesday = wednesday;
	}
	public Integer getThursday() {
		return thursday;
	}
	public void setThursday(Integer thursday) {
		this.thursday = thursday;
	}
	public Integer getFriday() {
		return friday;
	}
	public void setFriday(Integer friday) {
		this.friday = friday;
	}
	public Integer getSaturday() {
		return saturday;
	}
	public void setSaturday(Integer saturday) {
		this.saturday = saturday;
	}
	public Integer getSunday() {
		return sunday;
	}
	public void setSunday(Integer sunday) {
		this.sunday = sunday;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Long getRoutingId() {
		return routingId;
	}
	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}
	public String getRoutingName() {
		return routingName;
	}
	public void setRoutingName(String routingName) {
		this.routingName = routingName;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		if(address!=null && address.length()>0){
			return address;
		}
		return "";
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getIsDel() {
		return isDel;
	}
	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}
	public Integer getTansuat() {
		return tansuat;
	}
	public void setTansuat(Integer tansuat) {
		this.tansuat = tansuat;
	}
	
}

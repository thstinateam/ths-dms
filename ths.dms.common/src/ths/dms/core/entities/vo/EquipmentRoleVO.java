package ths.dms.core.entities.vo;
/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Class Equipment VO
 * 
 * @author hoanv25
 * @since December 10,2014
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description Them thuoc tinh co trong bang Equiment
 */
public class EquipmentRoleVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long shopId;
	
	private Integer status;
	private Integer fromMonth;
	private Integer toMonth;
			
	private Date createDate;
	private Date updateDate;

	private String code;
	private String name;	
	private String shopCode;
	private String shopName;
	private String description;
	private String lstEquipId;
	private String checkListStock;
	private String noCheckListStock;
	private String lstObjectId;
	private String stockCode; 
	private String createUser;
	private String updateUser;
	
	public Long getId() {
		return id;
	}
	public Long getShopId() {
		return shopId;
	}
	public Integer getStatus() {
		return status;
	}
	public Integer getFromMonth() {
		return fromMonth;
	}
	public Integer getToMonth() {
		return toMonth;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public String getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	public String getShopCode() {
		return shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public String getDescription() {
		return description;
	}
	public String getLstEquipId() {
		return lstEquipId;
	}
	public String getCheckListStock() {
		return checkListStock;
	}
	public String getNoCheckListStock() {
		return noCheckListStock;
	}
	public String getLstObjectId() {
		return lstObjectId;
	}
	public String getStockCode() {
		return stockCode;
	}
	public String getCreateUser() {
		return createUser;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public void setFromMonth(Integer fromMonth) {
		this.fromMonth = fromMonth;
	}
	public void setToMonth(Integer toMonth) {
		this.toMonth = toMonth;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setLstEquipId(String lstEquipId) {
		this.lstEquipId = lstEquipId;
	}
	public void setCheckListStock(String checkListStock) {
		this.checkListStock = checkListStock;
	}
	public void setNoCheckListStock(String noCheckListStock) {
		this.noCheckListStock = noCheckListStock;
	}
	public void setLstObjectId(String lstObjectId) {
		this.lstObjectId = lstObjectId;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	/**
	 * Khai bao GETTER/SETTER
	 * */
	
}

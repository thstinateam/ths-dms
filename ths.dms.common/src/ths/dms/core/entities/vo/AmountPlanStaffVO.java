package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

public class AmountPlanStaffVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer dsBHKH;
	private Integer snBHDQ;
	private Integer tienDoChuan;
	private List<AmountPlanStaffDetailVO> detail;
	
	public Integer getDsBHKH() {
		return dsBHKH;
	}
	public void setDsBHKH(Integer dsBHKH) {
		this.dsBHKH = dsBHKH;
	}
	public Integer getSnBHDQ() {
		return snBHDQ;
	}
	public void setSnBHDQ(Integer snBHDQ) {
		this.snBHDQ = snBHDQ;
	}
	public Integer getTienDoChuan() {
		return tienDoChuan;
	}
	public void setTienDoChuan(Integer tienDoChuan) {
		this.tienDoChuan = tienDoChuan;
	}
	public List<AmountPlanStaffDetailVO> getDetail() {
		return detail;
	}
	public void setDetail(List<AmountPlanStaffDetailVO> detail) {
		this.detail = detail;
	}
	
	
	
}

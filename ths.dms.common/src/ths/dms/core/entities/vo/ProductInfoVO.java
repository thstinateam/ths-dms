package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

public class ProductInfoVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3356702502499942801L;
	private Long idProductInfoVO;
	private String codeProductInfoVO;
	//sontt them vao 1 field:
	private List<SaleCatLevelVO> listSaleCatLevelVO;
	//
	public Long getIdProductInfoVO() {
		return idProductInfoVO;
	}
	public void setIdProductInfoVO(Long idProductInfoVO) {
		this.idProductInfoVO = idProductInfoVO;
	}
	public String getCodeProductInfoVO() {
		return codeProductInfoVO;
	}
	public void setCodeProductInfoVO(String codeProductInfoVO) {
		this.codeProductInfoVO = codeProductInfoVO;
	}
	public List<SaleCatLevelVO> getListSaleCatLevelVO() {
		return listSaleCatLevelVO;
	}
	public void setListSaleCatLevelVO(List<SaleCatLevelVO> listSaleCatLevelVO) {
		this.listSaleCatLevelVO = listSaleCatLevelVO;
	}
	
}

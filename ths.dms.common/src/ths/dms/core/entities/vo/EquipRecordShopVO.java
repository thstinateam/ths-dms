package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * VO don vi thuoc kiem ke thiet bi
 * 
 * @author phut
 * @since Aug 14, 2014
 */
public class EquipRecordShopVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long parentId; // id cua shop cha
	private String shopCode;
	private String shopName;
	private String nameText;
	private Integer status;
	private Integer isNPP;
	private Integer isCheck;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Integer getIsNPP() {
		return isNPP;
	}

	public void setIsNPP(Integer isNPP) {
		this.isNPP = isNPP;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getNameText() {
		return nameText;
	}

	public void setNameText(String nameText) {
		this.nameText = nameText;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIsCheck() {
		return isCheck;
	}

	public void setIsCheck(Integer isCheck) {
		this.isCheck = isCheck;
	}
	
}
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class RptDTBHTNTNVBHDateInfoVO implements Serializable {
	private String staffCode;
	private String staffName;
	private String dateOrder;
	ArrayList<RptDTBHTNTHVBHRecordOrderVO> listRptDTBHTNTHVBHRecordOrderVO = new ArrayList<RptDTBHTNTHVBHRecordOrderVO>();
	private BigDecimal sumRevenueDate;
	private BigDecimal sumMoneyDiscountDate;
	private BigDecimal sumProductDiscountDate;
	private BigDecimal sumMoneyDate;
	private BigDecimal sumDiscountDate;
	private BigDecimal sumSKUDate;
	private BigDecimal sumDebitDate;
	private BigDecimal sumCashDate;
	
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public ArrayList<RptDTBHTNTHVBHRecordOrderVO> getListRptDTBHTNTHVBHRecordOrderVO() {
		return listRptDTBHTNTHVBHRecordOrderVO;
	}
	public void setListRptDTBHTNTHVBHRecordOrderVO(
			ArrayList<RptDTBHTNTHVBHRecordOrderVO> listRptDTBHTNTHVBHRecordOrderVO) {
		this.listRptDTBHTNTHVBHRecordOrderVO = listRptDTBHTNTHVBHRecordOrderVO;
	}
	public String getDateOrder() {
		return dateOrder;
	}
	public void setDateOrder(String dateOrder) {
		this.dateOrder = dateOrder;
	}
	public BigDecimal getSumRevenueDate() {
		return sumRevenueDate;
	}
	public void setSumRevenueDate(BigDecimal sumRevenueDate) {
		this.sumRevenueDate = sumRevenueDate;
	}
	public BigDecimal getSumMoneyDiscountDate() {
		return sumMoneyDiscountDate;
	}
	public void setSumMoneyDiscountDate(BigDecimal sumMoneyDiscountDate) {
		this.sumMoneyDiscountDate = sumMoneyDiscountDate;
	}
	public BigDecimal getSumProductDiscountDate() {
		return sumProductDiscountDate;
	}
	public void setSumProductDiscountDate(BigDecimal sumProductDiscountDate) {
		this.sumProductDiscountDate = sumProductDiscountDate;
	}
	public BigDecimal getSumMoneyDate() {
		return sumMoneyDate;
	}
	public void setSumMoneyDate(BigDecimal sumMoneyDate) {
		this.sumMoneyDate = sumMoneyDate;
	}
	public BigDecimal getSumDiscountDate() {
		return sumDiscountDate;
	}
	public void setSumDiscountDate(BigDecimal sumDiscountDate) {
		this.sumDiscountDate = sumDiscountDate;
	}
	public BigDecimal getSumSKUDate() {
		return sumSKUDate;
	}
	public void setSumSKUDate(BigDecimal sumSKUDate) {
		this.sumSKUDate = sumSKUDate;
	}
	public BigDecimal getSumDebitDate() {
		return sumDebitDate;
	}
	public void setSumDebitDate(BigDecimal sumDebitDate) {
		this.sumDebitDate = sumDebitDate;
	}
	public BigDecimal getSumCashDate() {
		return sumCashDate;
	}
	public void setSumCashDate(BigDecimal sumCashDate) {
		this.sumCashDate = sumCashDate;
	}
	
	public String getDateOrderTmp() {
		if(this.dateOrder != null && this.dateOrder.length() != 0) {
			String[] temp = this.dateOrder.split("/");
			return temp[1] + "/" + temp[0] + "/" + temp[2];
		} else return "";
	}
}

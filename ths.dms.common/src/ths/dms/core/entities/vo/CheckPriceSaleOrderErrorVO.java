package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

public class CheckPriceSaleOrderErrorVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String orderNumber;
	private List<String> listProductCode;
	private String productCodeString;
	
	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public List<String> getListProductCode() {
		return listProductCode;
	}

	public void setListProductCode(List<String> listProductCode) {
		this.listProductCode = listProductCode;
	}
	
	public String getProductCodeString() {
		if(listProductCode.size() > 0){
			productCodeString = listProductCode.get(0);
			for(int i = 1, size = listProductCode.size(); i < size; i++){
				productCodeString += ", " + listProductCode.get(i);
			}
		}
		return productCodeString;
	}

	public void setProductCodeString(String productCodeString) {
		this.productCodeString = productCodeString;
	}

	public boolean containError(){
		if(	orderNumber.length()>0 
			|| listProductCode.size()>0){
			return true;
		}				
		return false;
	}

}

package ths.dms.core.entities.vo;

import java.io.Serializable;

public class ParentStaffMapVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long staffId;
	private Long parentStaffId;
	
	private String parentStaffStr;

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getParentStaffId() {
		return parentStaffId;
	}

	public void setParentStaffId(Long parentStaffId) {
		this.parentStaffId = parentStaffId;
	}

	public String getParentStaffStr() {
		return parentStaffStr;
	}

	public void setParentStaffStr(String parentStaffStr) {
		this.parentStaffStr = parentStaffStr;
	}
	
	
}

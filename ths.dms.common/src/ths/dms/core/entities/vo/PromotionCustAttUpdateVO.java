package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.PromotionCustAttr;
import ths.dms.core.entities.PromotionCustAttrDetail;

public class PromotionCustAttUpdateVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PromotionCustAttr promotionCustAttr;
	private List<PromotionCustAttrDetail> lstPromotionCustAttrDetail;
	
	public PromotionCustAttr getPromotionCustAttr() {
		return promotionCustAttr;
	}
	public void setPromotionCustAttr(PromotionCustAttr promotionCustAttr) {
		this.promotionCustAttr = promotionCustAttr;
	}
	public List<PromotionCustAttrDetail> getLstPromotionCustAttrDetail() {
		return lstPromotionCustAttrDetail;
	}
	public void setLstPromotionCustAttrDetail(
			List<PromotionCustAttrDetail> lstPromotionCustAttrDetail) {
		this.lstPromotionCustAttrDetail = lstPromotionCustAttrDetail;
	}
}

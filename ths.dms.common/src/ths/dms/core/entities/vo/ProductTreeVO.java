package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.entities.enumtype.TreeVOType;

public class ProductTreeVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String name;
	
	private Long parentId;
	
	private List<ProductTreeVO> listChildren = new ArrayList<ProductTreeVO>();
	
	private TreeVOType type;
	
	private Integer convfact;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ProductTreeVO> getListChildren() {
		return listChildren;
	}

	public void setListChildren(List<ProductTreeVO> listChildren) {
		this.listChildren = listChildren;
	}

	public TreeVOType getType() {
		return type;
	}

	public void setType(TreeVOType type) {
		this.type = type;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
}

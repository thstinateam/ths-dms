package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * VO don vi cay
 * 
 * @author vuongmq
 * @date Mar 19, 2015
 */
public class ShopViewParentVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long parentId; // id cua shop cha
	private String shopCode;
	private String shopName;
	private Integer quantity;
	private Integer receivedQtt;
	private Integer isNPP;
	private Integer isEdit;
	private Integer isExists;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getReceivedQtt() {
		return receivedQtt;
	}

	public void setReceivedQtt(Integer receivedQtt) {
		this.receivedQtt = receivedQtt;
	}

	public Integer getIsNPP() {
		return isNPP;
	}

	public void setIsNPP(Integer isNPP) {
		this.isNPP = isNPP;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getIsExists() {
		return isExists;
	}

	public void setIsExists(Integer isExists) {
		this.isExists = isExists;
	}

	public Integer getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}
}
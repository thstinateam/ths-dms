package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class TotalRewardMoneyVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4755467015311882389L;
	private String ksCode;
	private BigDecimal totalRewardMoney;
	private BigDecimal totalRewardMoneyDone;
	
	public String getKsCode() {
		return ksCode;
	}
	public void setKsCode(String ksCode) {
		this.ksCode = ksCode;
	}
	public BigDecimal getTotalRewardMoney() {
		return totalRewardMoney;
	}
	public void setTotalRewardMoney(BigDecimal totalRewardMoney) {
		this.totalRewardMoney = totalRewardMoney;
	}
	public BigDecimal getTotalRewardMoneyDone() {
		return totalRewardMoneyDone;
	}
	public void setTotalRewardMoneyDone(BigDecimal totalRewardMoneyDone) {
		this.totalRewardMoneyDone = totalRewardMoneyDone;
	}
}

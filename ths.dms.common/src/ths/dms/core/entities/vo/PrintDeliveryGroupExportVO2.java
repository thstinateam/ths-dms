package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class PrintDeliveryGroupExportVO2 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String orderNumber;
	private String deliveryCode;
	private String deliveryName;
	private String productCode;
	private String productName;
	private String lot;
	private Integer convfact;
	private BigDecimal price;
	private Integer quantity;
	private BigDecimal amount;
	private Integer proQuantity;
	private BigDecimal proAmount;
	private BigDecimal proMoney;
	private String strQuantity;
	private String strProQuatity;

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public Integer getProQuantity() {
		return proQuantity;
	}

	public void setProQuantity(Integer proQuantity) {
		this.proQuantity = proQuantity;
	}

	public BigDecimal getProAmount() {
		return proAmount;
	}

	public void setProAmount(BigDecimal proAmount) {
		this.proAmount = proAmount;
	}

	public BigDecimal getProMoney() {
		return proMoney;
	}

	public void setProMoney(BigDecimal proMoney) {
		this.proMoney = proMoney;
	}

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * them ham nay vao de do phai tinh toan
	 * @author phut
	 * @return the str quantity
	 */
	public String getStrQuantity() {
		if(quantity == null || convfact == null || convfact == 0) {
			return "0/0";
		}
		int left = quantity / convfact;
		int right = quantity % convfact;
		strQuantity = left + "/" + right;
		return strQuantity;
	}


	/**
	 * them ham nay vao de do phai tinh toan
	 * @author phut
	 * @return the str pro quatity
	 */
	public String getStrProQuatity() {
		if(proQuantity == null || convfact == null || convfact == 0) {
			return "0/0";
		}
		int left = proQuantity / convfact;
		int right = proQuantity % convfact;
		strProQuatity = left + "/" + right;
		return strProQuatity;
	}
	
}

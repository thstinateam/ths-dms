package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExcelPromotionDetail implements Serializable {
	public String promotionCode;
	public String type;
	public String productCode;
	public Integer saleQuantity;
	public BigDecimal saleAmount;
	public BigDecimal discountAmount;
	public Float discountPercent;
	public String freeProductCode;
	public Integer freeQuantity;
	public Boolean andOr;
}
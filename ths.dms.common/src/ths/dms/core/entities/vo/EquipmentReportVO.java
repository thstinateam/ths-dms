package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

public class EquipmentReportVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**id bien ban*/
	private Long recordId;

	/**ngay cap nhat hop dong */
	private Date updateDate;

	/**so hop dong */
	private String contractNumber;

	/**mien */
	private  String mien;

	/**kenh */
	private String kenh;

	/**ten TBHV */
	private String tbhvName;

	/**Ma NPP */
	private String shopCode;

	/**Ten NPP */
	private String shopName;

	/**Ten GSNPP */
	private String superviseName;

	/**Ma thiet bi */
	private String equipmentCode;

	/**So seri */
	private String seri;

	/**Ma KH */
	private String customerCode;

	/**Ten KH */
	private String customerName;

	/**Dien thoai */
	private String phone;

	/**So nha */
	private String houseNumber;

	/**Duong */
	private String street;

	/**Phuong */
	private String ward;

	/**Quan */
	private String district;

	/**TP */
	private String city;

	/**Tu mat */
	private Integer freezer;

	/**Tu dong */
	private Integer refrigerator;

	/**Tong so luong tu */
	private Integer totalEquipment;

	/**Ten loai tu */
	private String categoryName;

	/**Ten hieu tu */
	private String brandName;

	/**Dung tich */
	private String capacity;

	/**Nam cap lan dau */
	private Integer firstYearInUse;

	/**Nam ky hop dong muon tu */
	private Integer contractYear;
	
	/**Vi tri: 1:kho NPP, 2: tai diem ban */
	private Integer position;

	/**So luong tren he thong */
	private Integer quantity;

	/**So luong thuc te */
	private Integer quantityActually;

	/**trang thai giao nhan: 0: chua gui, 1: da gui, 2: da nhan  */
	private Integer deliveryStatus;

	/**Ngay chuyen trang thai da gui */
	private Date sentDate;
	
	/**Ngay chuyen trang thai da nhan */
	private Date receivedDate;

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getKenh() {
		return kenh;
	}

	public void setKenh(String kenh) {
		this.kenh = kenh;
	}

	public String getTbhvName() {
		return tbhvName;
	}

	public void setTbhvName(String tbhvName) {
		this.tbhvName = tbhvName;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getSuperviseName() {
		return superviseName;
	}

	public void setSuperviseName(String superviseName) {
		this.superviseName = superviseName;
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getSeri() {
		return seri;
	}

	public void setSeri(String seri) {
		this.seri = seri;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getFreezer() {
		return freezer;
	}

	public void setFreezer(Integer freezer) {
		this.freezer = freezer;
	}

	public Integer getRefrigerator() {
		return refrigerator;
	}

	public void setRefrigerator(Integer refrigerator) {
		this.refrigerator = refrigerator;
	}

	public Integer getTotalEquipment() {
		return totalEquipment;
	}

	public void setTotalEquipment(Integer totalEquipment) {
		this.totalEquipment = totalEquipment;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public Integer getFirstYearInUse() {
		return firstYearInUse;
	}

	public void setFirstYearInUse(Integer firstYearInUse) {
		this.firstYearInUse = firstYearInUse;
	}

	public Integer getContractYear() {
		return contractYear;
	}

	public void setContractYear(Integer contractYear) {
		this.contractYear = contractYear;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantityActually() {
		return quantityActually;
	}

	public void setQuantityActually(Integer quantityActually) {
		this.quantityActually = quantityActually;
	}

	public Integer getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(Integer deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
	
	
	
}

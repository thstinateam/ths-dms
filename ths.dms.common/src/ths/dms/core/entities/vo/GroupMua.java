package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.entities.enumtype.PromotionType;

public class GroupMua implements Serializable {
		public List<GroupSP> lstLevel = new ArrayList<GroupSP>();
		public String groupCode;
		public int order;
		
		public List<GroupSP> searchIndex(List<Long> lstIndex){
			List<GroupSP> lstGroupSP=new ArrayList<GroupSP>();
			if(lstIndex!=null){
				for(GroupSP groupSP : this.lstLevel) {
					if(lstIndex.contains(groupSP.index)){
						lstGroupSP.add(groupSP);
					}
				}
			}
			return lstGroupSP;
		}
		
		public GroupSP searchNode(Node node) {
			for(GroupSP groupSP : this.lstLevel) {
				for(int i = 0; i < groupSP.lstSP.size(); i++) {
					if(node.productCode != null && !node.productCode.equals("") && node.productCode.equals(groupSP.lstSP.get(i).productCode) && node.quantity != null && node.quantity.equals(groupSP.lstSP.get(i).quantity)) {
						return groupSP;
					} else if(node.productCode != null && !node.productCode.equals("") && node.productCode.equals(groupSP.lstSP.get(i).productCode) && node.amount != null && node.amount.equals(groupSP.lstSP.get(i).amount)) {
						return groupSP;
					} else if(node.amount != null && node.amount.equals(groupSP.lstSP.get(i).amount)) {
						return groupSP;
					}
				}
			}
			return null;
		}
		
		
		public boolean getAndOr(String type){
			boolean andOr = false;
			if(PromotionType.ZV01.getValue().equals(type) || PromotionType.ZV02.getValue().equals(type) || PromotionType.ZV03.getValue().equals(type) ||
					PromotionType.ZV04.getValue().equals(type) || PromotionType.ZV05.getValue().equals(type) || PromotionType.ZV06.getValue().equals(type) ||
					PromotionType.ZV13.getValue().equals(type) || PromotionType.ZV14.getValue().equals(type) || PromotionType.ZV15.getValue().equals(type) ||
					PromotionType.ZV16.getValue().equals(type) || PromotionType.ZV17.getValue().equals(type) || PromotionType.ZV18.getValue().equals(type)) {
				andOr = true;
			}
			return andOr;
		}
		
		/**
		 * cho biet co tao level moi hay khong
		 * @param type
		 * @return
		 */
		public boolean checkCreateLevelPromotionLineQty(String type,String productCode,Integer quantity){
			if(PromotionType.ZV01.getValue().equals(type) || PromotionType.ZV02.getValue().equals(type) || PromotionType.ZV03.getValue().equals(type) ||
					PromotionType.ZV04.getValue().equals(type) || PromotionType.ZV05.getValue().equals(type) || PromotionType.ZV06.getValue().equals(type)) {
				if(lstLevel.isEmpty()){
					return true;
				}else{
					for(GroupSP groupSP : lstLevel) {
						for(Node node : groupSP.lstSP) {
							if(productCode.equals(node.productCode) && quantity.equals(node.quantity)) {
								return false;
							}
						}
					}
					return true;
				}
			}else{
				return false;
			}
		}
		
		public boolean checkCreateLevelPromotionLineAmt(String type,String productCode,BigDecimal amount){
			if(PromotionType.ZV01.getValue().equals(type) || PromotionType.ZV02.getValue().equals(type) || PromotionType.ZV03.getValue().equals(type) ||
					PromotionType.ZV04.getValue().equals(type) || PromotionType.ZV05.getValue().equals(type) || PromotionType.ZV06.getValue().equals(type)) {
				if(lstLevel.isEmpty()){
					return true;
				}else{
					for(GroupSP groupSP : lstLevel) {
						for(Node node : groupSP.lstSP) {
							if(productCode.equals(node.productCode) && amount.equals(node.amount)) {
								return false;
							}
						}
					}
					return true;
				}
			}else{
				return false;
			}
		}
		
		public GroupSP add2Level(String type, String productCode, Integer quantity, Long indexMua, Integer iRun, String[] arrProduct, Integer[] arrQuantity, String[] arrFreeProduct, Integer[] arrFreeQuantity) {
			boolean isCreateNewLevel = false;
			//Kiem tra co tao nhom moi hay khong doi voi dang line
			isCreateNewLevel=checkCreateLevelPromotionLineQty(type,productCode,quantity);
			if(arrFreeProduct!=null && arrFreeQuantity!=null){
				boolean flag=true;
				for(int i=0;i<iRun;i++){//Chi can co spmua+slmua giong voi ben tren hoac spkm+slkm giong ben tren thi khong tao level moi
					if(arrProduct[i]!=null && ((arrProduct[i].equals(arrProduct[iRun]) && arrQuantity[i].equals(arrQuantity[iRun]))
							|| (arrFreeProduct[i].equals(arrFreeProduct[iRun]) && arrFreeQuantity[i].equals(arrFreeQuantity[iRun])))){
						flag=false;
						break;
					}
				}
				if(flag){
					isCreateNewLevel=true;
				}
			}
			boolean andOr = this.getAndOr(type);
			if(lstLevel.isEmpty()) {
				NodeSP node = new NodeSP(productCode, quantity, andOr);
				NodeSP nodeRef = null;
				if(arrFreeProduct!=null && arrFreeQuantity!=null){
					 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
				}
				GroupSP groupSP = new GroupSP();
				groupSP.order = lstLevel.size() + 1;
				groupSP.index = indexMua;
				groupSP.lstSP.add(node);
				groupSP.lstSPRef.add(nodeRef);
				lstLevel.add(groupSP);
				return groupSP;
			} else {
				//kiem tra neu cung sp va sl thi return nhom do luon
				for(GroupSP groupSP : lstLevel) {
					for(Node node : groupSP.lstSP) {
						if(productCode.equals(node.productCode) && quantity.equals(node.quantity)) {
							NodeSP nodeRef = null;
							if(arrFreeProduct!=null && arrFreeQuantity!=null){
								 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
							}
							groupSP.lstSPRef.add(nodeRef);
							return groupSP;
						}
					}
				}
				if(lstLevel.size() == 1) {
					//moi chi co 1 level thoi => add sp nay vao danh sach sp cua level luon
					for(int i = 1; i <= lstLevel.get(0).lstSP.size(); i++) {
						if(productCode.equals(arrProduct[iRun-i])) {
							isCreateNewLevel = true;
							break;
						}
					}
					if(isCreateNewLevel) {
						NodeSP node = new NodeSP(productCode, quantity, andOr);
						NodeSP nodeRef = null;
						if(arrFreeProduct!=null && arrFreeQuantity!=null){
							 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
						}
						GroupSP groupSP = new GroupSP();
						groupSP.order = lstLevel.size() + 1;
						groupSP.index = indexMua;
						groupSP.lstSP.add(node);
						groupSP.lstSPRef.add(nodeRef);
						lstLevel.add(groupSP);
						return groupSP;
					} else {
						NodeSP node = new NodeSP(productCode, quantity, andOr);
						NodeSP nodeRef = null;
						if(arrFreeProduct!=null && arrFreeQuantity!=null){
							 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
						}
						lstLevel.get(lstLevel.size() - 1).lstSP.add(node);
						lstLevel.get(lstLevel.size() - 1).lstSPRef.add(nodeRef);
						return lstLevel.get(lstLevel.size() - 1);
					}
				} else {
					GroupSP lastGroup = null;
					if(!isCreateNewLevel) {
						for(GroupSP groupSP : lstLevel) {
							//ben tren kiem tra chua co nhom nao co sp & sl nay
							//o day kiem tra tiep neu trong nhom do co sp roi thi qua nhom khac, chua co thi them vao nhom nay
							//sp mua phai ko co trong level
							boolean flagMua=true;
							for(Node node : groupSP.lstSP) {
								if(productCode.equals(node.productCode)) {
									flagMua=false;
									break;
								}
							}
							//sp km phai co trong level
							boolean flagKM=false;
							if(arrFreeProduct!=null){
								for(Node node : groupSP.lstSPRef) {
									if(arrFreeProduct[iRun].equals(node.productCode)) {
										flagKM=true;
										break;
									}
								}
							}else{//CTKM ko co SP KM thi cho qua
								flagKM=true;
							}
							if(flagMua && flagKM){//sp mua ko co va spkm phai co trong level thi moi add vao level
								lastGroup=groupSP;
								break;
							}
						}
						if(lastGroup==null){//cac muc deu co sp nay roi` thi tao muc moi
							isCreateNewLevel=true;
						}
					}
					if(isCreateNewLevel) {
						NodeSP node = new NodeSP(productCode, quantity, andOr);
						NodeSP nodeRef = null;
						if(arrFreeProduct!=null && arrFreeQuantity!=null){
							 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
						}
						GroupSP groupSP = new GroupSP();
						groupSP.order = lstLevel.size() + 1;
						groupSP.index = indexMua;
						groupSP.lstSP.add(node);
						groupSP.lstSPRef.add(nodeRef);
						lstLevel.add(groupSP);
						return groupSP;
					} else {
						NodeSP node = new NodeSP(productCode, quantity, andOr);
						NodeSP nodeRef = null;
						if(arrFreeProduct!=null && arrFreeQuantity!=null){
							 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
						}
						lastGroup.lstSP.add(node);
						lastGroup.lstSPRef.add(nodeRef);
						return lastGroup;
					}
				}
			}
		}
		
		public GroupSP add2Level(String type, String productCode, BigDecimal amount, Long indexMua, Integer iRun, String[] arrProduct, BigDecimal[] arrAmount,String[] arrFreeProduct,Integer[] arrFreeQuantity) {
			boolean isCreateNewLevel = false;
			//Kiem tra co tao nhom moi hay khong doi voi dang line
			isCreateNewLevel=checkCreateLevelPromotionLineAmt(type,productCode,amount);
			if(arrFreeProduct!=null && arrFreeQuantity!=null){
				boolean flag=true;
				for(int i=0;i<iRun;i++){//Chi can co spmua+slmua giong voi ben tren hoac spkm+slkm giong ben tren thi khong tao level moi
					if(arrProduct[i]!=null && ((arrProduct[i].equals(arrProduct[iRun]) && arrAmount[i].equals(arrAmount[iRun]))
							|| (arrFreeProduct[i].equals(arrFreeProduct[iRun]) && arrFreeQuantity[i].equals(arrFreeQuantity[iRun])))){
						flag=false;
						break;
					}
				}
				if(flag){
					isCreateNewLevel=true;
				}
			}
			boolean andOr = this.getAndOr(type);
			if(lstLevel.isEmpty()) {
				NodeSP node = new NodeSP(productCode, amount, andOr);
				NodeSP nodeRef = null;
				if(arrFreeProduct!=null && arrFreeQuantity!=null){
					 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
				}
				GroupSP groupSP = new GroupSP();
				groupSP.order = lstLevel.size() + 1;
				groupSP.index = indexMua;
				groupSP.lstSP.add(node);
				groupSP.lstSPRef.add(nodeRef);
				lstLevel.add(groupSP);
				return groupSP;
			} else {
				//kiem tra neu cung sp va amount thi return nhom do luon
				for(GroupSP groupSP : lstLevel) {
					for(Node node : groupSP.lstSP) {
						if(productCode.equals(node.productCode) && amount.equals(node.amount)) {
							NodeSP nodeRef = null;
							if(arrFreeProduct!=null && arrFreeQuantity!=null){
								 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
							}
							groupSP.lstSPRef.add(nodeRef);
							return groupSP;
						}
					}
				}
				if(lstLevel.size() == 1) {
					//moi chi co 1 level thoi => add sp nay vao danh sach sp cua level luon
					for(int i = 1; i <= lstLevel.get(0).lstSP.size(); i++) {
						if(productCode.equals(arrProduct[iRun-i])) {
							isCreateNewLevel = true;
							break;
						}
					}
					if(isCreateNewLevel) {
						NodeSP node = new NodeSP(productCode, amount, andOr);
						NodeSP nodeRef = null;
						if(arrFreeProduct!=null && arrFreeQuantity!=null){
							 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
						}
						GroupSP groupSP = new GroupSP();
						groupSP.order = lstLevel.size() + 1;
						groupSP.index = indexMua;
						groupSP.lstSP.add(node);
						groupSP.lstSPRef.add(nodeRef);
						lstLevel.add(groupSP);
						return groupSP;
					} else {
						NodeSP node = new NodeSP(productCode, amount, andOr);
						NodeSP nodeRef = null;
						if(arrFreeProduct!=null && arrFreeQuantity!=null){
							 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
						}
						lstLevel.get(lstLevel.size() - 1).lstSP.add(node);
						lstLevel.get(lstLevel.size() - 1).lstSPRef.add(nodeRef);
						return lstLevel.get(lstLevel.size() - 1);
					}
				} else {
					GroupSP lastGroup = null;
					if(!isCreateNewLevel) {
						for(GroupSP groupSP : lstLevel) {
							//ben tren kiem tra chua co nhom nao co sp & sl nay
							//o day kiem tra tiep neu trong nhom do co sp roi thi qua nhom khac, chua co thi them vao nhom nay
							//sp mua phai ko co trong level
							boolean flagMua=true;
							for(Node node : groupSP.lstSP) {
								if(productCode.equals(node.productCode)) {
									flagMua=false;
									break;
								}
							}
							//sp km phai co trong level
							boolean flagKM=false;
							if(arrFreeProduct!=null){
								for(Node node : groupSP.lstSPRef) {
									if(arrFreeProduct[iRun].equals(node.productCode)) {
										flagKM=true;
										break;
									}
								}
							}else{//CTKM ko co SP KM thi cho qua
								flagKM=true;
							}
							if(flagMua && flagKM){//sp mua ko co va spkm phai co trong level thi moi add vao level
								lastGroup=groupSP;
								break;
							}
						}
						if(lastGroup==null){//cac muc deu co sp nay roi` thi tao muc moi
							isCreateNewLevel=true;
						}
					}
					if(isCreateNewLevel) {
						NodeSP node = new NodeSP(productCode, amount, andOr);
						NodeSP nodeRef = null;
						if(arrFreeProduct!=null && arrFreeQuantity!=null){
							 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
						}
						GroupSP groupSP = new GroupSP();
						groupSP.order = lstLevel.size() + 1;
						groupSP.index = indexMua;
						groupSP.lstSP.add(node);
						groupSP.lstSPRef.add(nodeRef);
						lstLevel.add(groupSP);
						return groupSP;
					} else {
						NodeSP node = new NodeSP(productCode, amount, andOr);
						NodeSP nodeRef = null;
						if(arrFreeProduct!=null && arrFreeQuantity!=null){
							 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], andOr);
						}
						lastGroup.lstSP.add(node);
						lastGroup.lstSPRef.add(nodeRef);
						return lastGroup;
					}
				}
			}
		}
		
		public GroupSP add2Level(BigDecimal amount, Long indexMua, Integer iRun, BigDecimal[] arrAmount,String[] arrFreeProduct,Integer[] arrFreeQuantity) {
			if(lstLevel.isEmpty()) {
				NodeAmount node = new NodeAmount(amount);
				NodeSP nodeRef = null;
				if(arrFreeProduct!=null && arrFreeQuantity!=null){
					 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], true);
				}
				GroupSP groupSP = new GroupSP();
				groupSP.order = lstLevel.size() + 1;
				groupSP.index = indexMua;
				groupSP.lstSP.add(node);
				groupSP.lstSPRef.add(nodeRef);
				lstLevel.add(groupSP);
				return groupSP;
			} else {
				for(GroupSP __groupSP : lstLevel) {
					for(int i = 0; i < __groupSP.lstSP.size(); i++) {
						if(amount.equals(__groupSP.lstSP.get(i).amount)) {
							return __groupSP;
						}
					}
				}
				NodeAmount node = new NodeAmount(amount);
				NodeSP nodeRef = null;
				if(arrFreeProduct!=null && arrFreeQuantity!=null){
					 nodeRef = new NodeSP(arrFreeProduct[iRun], arrFreeQuantity[iRun], true);
				}
				GroupSP groupSP = new GroupSP();
				groupSP.order = lstLevel.size() + 1;
				groupSP.index = indexMua;
				groupSP.lstSP.add(node);
				groupSP.lstSPRef.add(nodeRef);
				lstLevel.add(groupSP);
				return groupSP;
			}
		}
	}
package ths.dms.core.entities.vo;

import java.io.Serializable;

public class SaleCatLevelVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5996426570040054770L;
	
	private Long idSaleCatLevel;
	private String codeSaleCatLevel;
	private String nameSaleCatLevel;
	
	public Long getIdSaleCatLevel() {
		return idSaleCatLevel;
	}
	public void setIdSaleCatLevel(Long idSaleCatLevel) {
		this.idSaleCatLevel = idSaleCatLevel;
	}
	public String getCodeSaleCatLevel() {
		return codeSaleCatLevel;
	}
	public void setCodeSaleCatLevel(String codeSaleCatLevel) {
		this.codeSaleCatLevel = codeSaleCatLevel;
	}
	public String getNameSaleCatLevel() {
		return nameSaleCatLevel;
	}
	public void setNameSaleCatLevel(String nameSaleCatLevel) {
		this.nameSaleCatLevel = nameSaleCatLevel;
	}
}

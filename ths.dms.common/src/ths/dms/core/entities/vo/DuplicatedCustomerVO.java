/**
 * Copyright (c) 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * Doi tuong chua thong tin khach hang trung
 * @author tuannd20
 * @since 28/08/2015
 */
public class DuplicatedCustomerVO implements Serializable {
	private static final long serialVersionUID = -4220760913233842952L;

	private String regionShopCode;
	private String regionShopName;
	private String areaShopCode;
	private String areaShopName;
	private String shopCode;
	private String shopName;
	private String customerCode;
	private String shortCode;
	private String customerName;
	private String address;
	private Integer status;
	private Integer no;
	private Double lat;
	private Double lng;
	private Double distanceFromApprovingCustomer;
	
	public String getRegionShopCode() {
		return regionShopCode;
	}
	public void setRegionShopCode(String regionShopCode) {
		this.regionShopCode = regionShopCode;
	}
	public String getRegionShopName() {
		return regionShopName;
	}
	public void setRegionShopName(String regionShopName) {
		this.regionShopName = regionShopName;
	}
	public String getAreaShopCode() {
		return areaShopCode;
	}
	public void setAreaShopCode(String areaShopCode) {
		this.areaShopCode = areaShopCode;
	}
	public String getAreaShopName() {
		return areaShopName;
	}
	public void setAreaShopName(String areaShopName) {
		this.areaShopName = areaShopName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Double getDistanceFromApprovingCustomer() {
		return distanceFromApprovingCustomer;
	}
	public void setDistanceFromApprovingCustomer(
			Double distanceFromApprovingCustomer) {
		this.distanceFromApprovingCustomer = distanceFromApprovingCustomer;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLng() {
		return lng;
	}
	public void setLng(Double lng) {
		this.lng = lng;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public Integer getNo() {
		return no;
	}
	public void setNo(Integer no) {
		this.no = no;
	}
}

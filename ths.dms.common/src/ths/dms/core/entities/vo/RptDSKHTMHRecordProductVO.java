package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptDSKHTMHRecordProductVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String customerCode;
	private String customerName;
	private String catCode;
	private String catName;
	private String productCode;
	private String productName;
	private BigDecimal quantitySale;
	private BigDecimal quantityDisplay;
	private BigDecimal money;
	private String customerHouseNumber;
	private String customerStreet;

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCatCode() {
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public BigDecimal getQuantitySale() {
		return quantitySale;
	}

	public void setQuantitySale(BigDecimal quantitySale) {
		this.quantitySale = quantitySale;
	}

	public BigDecimal getQuantityDisplay() {
		return quantityDisplay;
	}

	public void setQuantityDisplay(BigDecimal quantityDisplay) {
		this.quantityDisplay = quantityDisplay;
	}

	public String getCustomerHouseNumber() {
		return customerHouseNumber;
	}

	public void setCustomerHouseNumber(String customerHouseNumber) {
		this.customerHouseNumber = customerHouseNumber;
	}

	public String getCustomerStreet() {
		return customerStreet;
	}

	public void setCustomerStreet(String customerStreet) {
		this.customerStreet = customerStreet;
	}
	
}

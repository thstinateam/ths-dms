package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

public class ExcelPromotionHeader implements Serializable {
	public String promotionCode;
	public String description;
	public String type;
	public String format;
	public Date fromDate;
	public Date toDate;
}
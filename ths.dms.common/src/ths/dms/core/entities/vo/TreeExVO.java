package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TreeExVO<T, T2> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1878562251044796712L;

	private T object;
	
	private List<TreeExVO<T, T2>> listChildren;
	
	private List<T2> listLeaves;

	public TreeExVO() {
		listChildren = new ArrayList<TreeExVO<T, T2>>();
		listLeaves = new ArrayList<T2>();
	}
	
	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

	public List<TreeExVO<T, T2>> getListChildren() {
		return listChildren;
	}

	public void setListChildren(List<TreeExVO<T, T2>> listChildren) {
		this.listChildren = listChildren;
	}

	public List<T2> getListLeaves() {
		return listLeaves;
	}

	public void setListLeaves(List<T2> listLeaves) {
		this.listLeaves = listLeaves;
	}
}

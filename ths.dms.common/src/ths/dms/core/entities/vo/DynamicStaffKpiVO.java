package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

import ths.core.entities.vo.rpt.DynamicVO;


public class DynamicStaffKpiVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6966133410933977207L;
	private Long shopId;
	private Long staffId;
	private String shopCode;
	private String shopName;
	private String staffCode;
	private String staffName;
	private String areaCode;
	private Long cycleId;
	private String cycleText;
	
	private Long kiId;
	private Long kpiId;
	private Integer year;
	private Integer period;
	
	
	List<DynamicVO> lstKpiData;

	public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}


	public List<DynamicVO> getLstKpiData() {
		return lstKpiData;
	}

	public void setLstKpiData(List<DynamicVO> lstKpiData) {
		this.lstKpiData = lstKpiData;
	}

	public String getCycleText() {
		return cycleText;
	}

	public void setCycleText(String cycleText) {
		this.cycleText = cycleText;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public Long getKpiId() {
		return kpiId;
	}

	public void setKpiId(Long kpiId) {
		this.kpiId = kpiId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public Long getKiId() {
		return kiId;
	}

	public void setKiId(Long kiId) {
		this.kiId = kiId;
	}
	
	
}

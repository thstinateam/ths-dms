package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.PoVnmDetail;
import ths.dms.core.entities.PoVnmLot;


public class CreatePoVnmDetailVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PoVnmDetail poVnmDetail;
	private List<PoVnmLot> listPoVnmLot;
	
	public PoVnmDetail getPoVnmDetail() {
		return poVnmDetail;
	}
	public void setPoVnmDetail(PoVnmDetail poVnmDetail) {
		this.poVnmDetail = poVnmDetail;
	}
	public List<PoVnmLot> getListPoVnmLot() {
		return listPoVnmLot;
	}
	public void setListPoVnmLot(List<PoVnmLot> listPoVnmLot) {
		this.listPoVnmLot = listPoVnmLot;
	}
	
}
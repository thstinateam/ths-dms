package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Class EquipRepairFormVOList
 * 
 *@author vuongmq
 *@since 22/104/2015
 *@description xuat print file PDF; sua chua
 */
public class EquipRepairFormVOList implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String maPhieu;
	private String loaiThietBi;
	private String nhomThietBi;
	private String maThietBi;
	private String soSeri;
	private String kho;
	private String ngayTao;
	private String maKH; // vuongmq; 28/05/2015; in cap nhat
	private String khachHang; // vuongmq; 28/05/2015; in cap nhat
	private String donVi;
	private String diaChi; // vuongmq; 28/05/2015; in cap nhat
	private String dienThoai; // vuongmq; 28/05/2015; in cap nhat
	private Integer trangThai;
	private String tinhTrangHuHong;
	private String lyDoDeNghi;
	private BigDecimal tongTien;
	private String lyDoTuChoi;
	private Integer kyMo;
	private Integer daThanhToan;
	private Integer paymentPeriodOpen;
	
	/** vuongmq; them cac cot in*/
	private BigDecimal doanhSoTB;
	private String tenNPP;
	private String tenMien;
	
	private String capNam; // vuongmq; 28/05/2015; in cap nhat
	private String contractNumber; // vuongmq; 28/05/2015; in cap nhat
	private BigDecimal giaNhanCong; // vuongmq; 28/05/2015; in cap nhat
	
	private List<EquipmentRepairFormDtlVO> lstDetail; /** cai nay dung de in*/
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMaPhieu() {
		return maPhieu;
	}
	public void setMaPhieu(String maPhieu) {
		this.maPhieu = maPhieu;
	}
	public String getLoaiThietBi() {
		return loaiThietBi;
	}
	public void setLoaiThietBi(String loaiThietBi) {
		this.loaiThietBi = loaiThietBi;
	}
	public String getNhomThietBi() {
		return nhomThietBi;
	}
	public void setNhomThietBi(String nhomThietBi) {
		this.nhomThietBi = nhomThietBi;
	}
	public String getMaThietBi() {
		return maThietBi;
	}
	public void setMaThietBi(String maThietBi) {
		this.maThietBi = maThietBi;
	}
	public String getSoSeri() {
		return soSeri;
	}
	public void setSoSeri(String soSeri) {
		this.soSeri = soSeri;
	}
	public String getKho() {
		return kho;
	}
	public void setKho(String kho) {
		this.kho = kho;
	}
	public String getNgayTao() {
		return ngayTao;
	}
	public void setNgayTao(String ngayTao) {
		this.ngayTao = ngayTao;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public Integer getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(Integer trangThai) {
		this.trangThai = trangThai;
	}
	public String getTinhTrangHuHong() {
		return tinhTrangHuHong;
	}
	public void setTinhTrangHuHong(String tinhTrangHuHong) {
		this.tinhTrangHuHong = tinhTrangHuHong;
	}
	public String getLyDoDeNghi() {
		return lyDoDeNghi;
	}
	public void setLyDoDeNghi(String lyDoDeNghi) {
		this.lyDoDeNghi = lyDoDeNghi;
	}
	public BigDecimal getTongTien() {
		return tongTien;
	}
	public void setTongTien(BigDecimal tongTien) {
		this.tongTien = tongTien;
	}
	public String getLyDoTuChoi() {
		return lyDoTuChoi;
	}
	public void setLyDoTuChoi(String lyDoTuChoi) {
		this.lyDoTuChoi = lyDoTuChoi;
	}
	public Integer getKyMo() {
		return kyMo;
	}
	public void setKyMo(Integer kyMo) {
		this.kyMo = kyMo;
	}
	public Integer getDaThanhToan() {
		return daThanhToan;
	}
	public void setDaThanhToan(Integer daThanhToan) {
		this.daThanhToan = daThanhToan;
	}
	public String getKhachHang() {
		return khachHang;
	}
	public void setKhachHang(String khachHang) {
		this.khachHang = khachHang;
	}
	public String getDonVi() {
		return donVi;
	}
	public void setDonVi(String donVi) {
		this.donVi = donVi;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getDienThoai() {
		return dienThoai;
	}
	public void setDienThoai(String dienThoai) {
		this.dienThoai = dienThoai;
	}
	public Integer getPaymentPeriodOpen() {
		return paymentPeriodOpen;
	}
	public void setPaymentPeriodOpen(Integer paymentPeriodOpen) {
		this.paymentPeriodOpen = paymentPeriodOpen;
	}
	public List<EquipmentRepairFormDtlVO> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<EquipmentRepairFormDtlVO> lstDetail) {
		this.lstDetail = lstDetail;
	}
	public String getTenMien() {
		return tenMien;
	}
	public void setTenMien(String tenMien) {
		this.tenMien = tenMien;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public BigDecimal getDoanhSoTB() {
		return doanhSoTB;
	}
	public void setDoanhSoTB(BigDecimal doanhSoTB) {
		this.doanhSoTB = doanhSoTB;
	}
	public String getCapNam() {
		return capNam;
	}
	public void setCapNam(String capNam) {
		this.capNam = capNam;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public BigDecimal getGiaNhanCong() {
		return giaNhanCong;
	}
	public void setGiaNhanCong(BigDecimal giaNhanCong) {
		this.giaNhanCong = giaNhanCong;
	}
	
}

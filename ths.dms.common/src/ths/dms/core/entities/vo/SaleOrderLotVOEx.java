/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VO sale_order_lot cho man hinh tra hang
 * 
 * @author lacnv1
 * @since Mar 26, 2015
 */
public class SaleOrderLotVOEx implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long saleOrderDetailId;
	private String productCode;
	private String productName;
	private Integer convfact;
	private BigDecimal price;
	private BigDecimal packagePrice;
	private BigDecimal quantityPackage;
	private BigDecimal quantityRetail;
	private String warehouseName;
	private Integer quantity;
	private BigDecimal discountAmount;
	private String programCode;
	private Integer isFreeItem;
	private Integer programType;
	private String programTypeCode;
	private Integer maxQuantityFree;
	private Integer levelPromo;
	private String joinProgramCode;
	private Integer quantityReceived;
	private Integer maxQuantityReceived;
	private Long productGroupId;
	private Integer payingOrder;
	private BigDecimal maxAmountFree;
	
	public Long getSaleOrderDetailId() {
		return saleOrderDetailId;
	}

	public void setSaleOrderDetailId(Long saleOrderDetailId) {
		this.saleOrderDetailId = saleOrderDetailId;
	}

	public String getProductCode() {
		return productCode;
	}
	
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public Integer getIsFreeItem() {
		return isFreeItem;
	}

	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}

	public Integer getProgramType() {
		return programType;
	}

	public void setProgramType(Integer programType) {
		this.programType = programType;
	}

	public String getProgramTypeCode() {
		return programTypeCode;
	}

	public void setProgramTypeCode(String programTypeCode) {
		this.programTypeCode = programTypeCode;
	}

	public Integer getMaxQuantityFree() {
		return maxQuantityFree;
	}

	public void setMaxQuantityFree(Integer maxQuantityFree) {
		this.maxQuantityFree = maxQuantityFree;
	}

	public Integer getLevelPromo() {
		return levelPromo;
	}

	public void setLevelPromo(Integer levelPromo) {
		this.levelPromo = levelPromo;
	}

	public String getJoinProgramCode() {
		return joinProgramCode;
	}

	public void setJoinProgramCode(String joinProgramCode) {
		this.joinProgramCode = joinProgramCode;
	}

	public Integer getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public Integer getMaxQuantityReceived() {
		return maxQuantityReceived;
	}

	public void setMaxQuantityReceived(Integer maxQuantityReceived) {
		this.maxQuantityReceived = maxQuantityReceived;
	}

	public Long getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(Long productGroupId) {
		this.productGroupId = productGroupId;
	}

	public Integer getPayingOrder() {
		return payingOrder;
	}

	public void setPayingOrder(Integer payingOrder) {
		this.payingOrder = payingOrder;
	}

	public BigDecimal getMaxAmountFree() {
		return maxAmountFree;
	}

	public void setMaxAmountFree(BigDecimal maxAmountFree) {
		this.maxAmountFree = maxAmountFree;
	}

	public BigDecimal getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(BigDecimal packagePrice) {
		this.packagePrice = packagePrice;
	}

	public BigDecimal getQuantityPackage() {
		return quantityPackage;
	}

	public void setQuantityPackage(BigDecimal quantityPackage) {
		this.quantityPackage = quantityPackage;
	}

	public BigDecimal getQuantityRetail() {
		return quantityRetail;
	}

	public void setQuantityRetail(BigDecimal quantityRetail) {
		this.quantityRetail = quantityRetail;
	}
}
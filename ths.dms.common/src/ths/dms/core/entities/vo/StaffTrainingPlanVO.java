package ths.dms.core.entities.vo;

import java.io.Serializable;

public class StaffTrainingPlanVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long trainingPlanId;
	private Long trainingPlanDetailId;
	private Long gsId;
	private String gsCode;
	private String gsName;
	private Long nvId;
	private String nvCode;
	private String nvName;

	public Long getTrainingPlanId() {
		return trainingPlanId;
	}

	public void setTrainingPlanId(Long trainingPlanId) {
		this.trainingPlanId = trainingPlanId;
	}

	public Long getTrainingPlanDetailId() {
		return trainingPlanDetailId;
	}

	public void setTrainingPlanDetailId(Long trainingPlanDetailId) {
		this.trainingPlanDetailId = trainingPlanDetailId;
	}

	public Long getGsId() {
		return gsId;
	}

	public void setGsId(Long gsId) {
		this.gsId = gsId;
	}

	public String getGsCode() {
		return gsCode;
	}

	public void setGsCode(String gsCode) {
		this.gsCode = gsCode;
	}

	public String getGsName() {
		return gsName;
	}

	public void setGsName(String gsName) {
		this.gsName = gsName;
	}

	public Long getNvId() {
		return nvId;
	}

	public void setNvId(Long nvId) {
		this.nvId = nvId;
	}

	public String getNvCode() {
		return nvCode;
	}

	public void setNvCode(String nvCode) {
		this.nvCode = nvCode;
	}

	public String getNvName() {
		return nvName;
	}

	public void setNvName(String nvName) {
		this.nvName = nvName;
	}
}

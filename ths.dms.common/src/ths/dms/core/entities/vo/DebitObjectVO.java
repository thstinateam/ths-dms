package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import ths.dms.core.entities.Customer;

public class DebitObjectVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long shopId;
	private String debitNumber;
	private BigDecimal amount;
	private String reason;
	private Long customerId;
	private Customer customer;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getDebitNumber() {
		return debitNumber;
	}

	public void setDebitNumber(String debitNumber) {
		this.debitNumber = debitNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
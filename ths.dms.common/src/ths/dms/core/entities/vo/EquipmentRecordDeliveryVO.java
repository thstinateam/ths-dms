package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import sun.nio.cs.ext.Big5;

public class EquipmentRecordDeliveryVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Id bien ban */
	private Long idRecord;
	
	/** Ma bien ban */
	private String recordCode;
	
	/** Ma bien ban de nghi muon thiet bi */
	private String equipLendCode;
	
	/** Id bien ban de nghi muon thiet bi */
	private Long equipLendId;
	
	/** Ma nhan vien */
	private String staffCode;
	
	/** Ten nhan vien */
	private String staffName;
	
	/** Chuc vu */
	private String staffType;
	
	/** Ma khach hang */
	private String customerCode;
	
	/** Ten khach hang */
	private String customerName;
	
	/** Dia chi khach hang */
	private String customerAddress;
	
	/** Ma don vi */
	private String shopCode;
	
	/** Ten don vi */
	private String shopName;
	
	/** Ngay tao bien ban */
	private String createDate;
	
	/** So hop dong */
	private String numberContract;
	
	/** Ngay hop dong */
	private String dateContract;
	
	/** Ten file dinh kem*/
	private String attachFilesName;
	
	/** Url file dinh kem */
	private String urlAttachFiles;
	
	/** Trang thai bien ban */
	private Integer statusRecord;
	
	/** Trang thai giao nhan */
	private Integer statusDelivery;
	
	private String cusPhone;
	
	/** Trang thai in 0: chua in - 1: da in */
	private Integer statusPrint;
	/** thong tin ben cho muon*/
	private String fromShopName;
	private String fromShopAddess;
	private String fromPhone;
	private String fromFax;
	private String fromRepresentative;
	private String fromPosition;
	
	private String toCustomerName;
	private String toCustomerCode;
	/**  Đại chỉ cư ngụ trụ sở chỉnh*/
	private String toAddress;
	/** Dia chi dat tu */
	private String toObjectAddress;
	
	/** So nha*/
	private String soNha;
	
	/** duong-thon ap*/
	private String duong;
	
	/**tinh - tp */
	private String tp;
	
	/** quan-huyen */
	private String quan;
	
	/** phuong - xa*/
	private String phuong;
	
	private String toPhone;
	/** So Dang ky kinh doanh */
	private String toBusinessLicense;
	
	private String toBusinessPlace;
	
	/** Ngay cap Dang ky kinh doanh */
	private Date toBusinessDate;
	
	/** Nguoi dai dien */
	private String toRepresentative;
	
	/** Chuc vu */
	private String toPosition;
	
	/** So CMND */
	private String idNO;
	
	/** Noi cap CMND */
	private String idNOPlace;
	
	/** Ngay cap CMND  */
	private Date idNODate;
	
	private Date contractDate;
	
	/** Ma thiet bi*/
	private String equipmentCode;
	
	/** So seri */
	private String seriNumber;
	
	/** Dia chi thuong tru */
	private String toPermanentAddress;
	
	/** So thang khau hao*/
	private Integer depreciation;
	
	
	/** Dso tu mat */
	private BigDecimal freezer;
	
	/** Dso tu dong */
	private BigDecimal refrigerator;
	
	/**Nội dung: Equip_Param */
	private String content;
	
	/** Ngay tao hop dong */
	private Date contractCreateDate;
	
	/** Trang thai giao nhan */
	private List<EquipmentDeliveryVO> lstEquipmentDeliveryVOs;
	
	private String equipGroup;
	private String equipGroupCode;
	
	private String equipProvider;
	private BigDecimal price;
	private String health;
	private Integer manufacturingYear;
	private Date createFormDate;
	
	/** kenh*/
	private String kenh;
	/** mien*/
	private String mien;
	// dien thoai khach hang
	private String customerPhone;
		
	// ngay hop dong
//	private String dayContract;
//	private String monthContract;
//	private String yearContract;
	
	// so thiet bi
	private Integer numberEquip;
	
	// So tap tin dinh kem
	private Integer numberFile;
	
	private String toRelation;
	private String toHouseNumber;
	private String toStreet;
	private String toProvinceName;
	private String toDistrictName;
	private String toWardName;
	private Long customerId;
	private String note;
	
	public String getRecordCode() {
		return recordCode;
	}
	public void setRecordCode(String recordCode) {
		this.recordCode = recordCode;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}	
	public String getStaffType() {
		return staffType;
	}
	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getNumberContract() {
		return numberContract;
	}
	public void setNumberContract(String numberContract) {
		this.numberContract = numberContract;
	}
	public String getAttachFilesName() {
		return attachFilesName;
	}
	public void setAttachFilesName(String attachFilesName) {
		this.attachFilesName = attachFilesName;
	}
	public String getUrlAttachFiles() {
		return urlAttachFiles;
	}
	public void setUrlAttachFiles(String urlAttachFiles) {
		this.urlAttachFiles = urlAttachFiles;
	}
	public Integer getStatusRecord() {
		return statusRecord;
	}
	public void setStatusRecord(Integer statusRecord) {
		this.statusRecord = statusRecord;
	}
	public Integer getStatusDelivery() {
		return statusDelivery;
	}
	public void setStatusDelivery(Integer statusDelivery) {
		this.statusDelivery = statusDelivery;
	}
	public Long getIdRecord() {
		return idRecord;
	}
	public void setIdRecord(Long idRecord) {
		this.idRecord = idRecord;
	}
	
	public List<EquipmentDeliveryVO> getLstEquipmentDeliveryVOs() {
		return lstEquipmentDeliveryVOs;
	}
	public void setLstEquipmentDeliveryVOs(List<EquipmentDeliveryVO> lstEquipmentDeliveryVOs) {
		this.lstEquipmentDeliveryVOs = lstEquipmentDeliveryVOs;
	}
	public String getDateContract() {
		return dateContract;
	}
	public void setDateContract(String dateContract) {
		this.dateContract = dateContract;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public Integer getNumberEquip() {
		return numberEquip;
	}
	public void setNumberEquip(Integer numberEquip) {
		this.numberEquip = numberEquip;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getEquipLendCode() {
		return equipLendCode;
	}
	public void setEquipLendCode(String equipLendCode) {
		this.equipLendCode = equipLendCode;
	}
	public Long getEquipLendId() {
		return equipLendId;
	}
	public void setEquipLendId(Long equipLendId) {
		this.equipLendId = equipLendId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Integer getStatusPrint() {
		return statusPrint;
	}
	public void setStatusPrint(Integer statusPrint) {
		this.statusPrint = statusPrint;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getSeriNumber() {
		return seriNumber;
	}
	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getIdNO() {
		return idNO;
	}
	public void setIdNO(String idNO) {
		this.idNO = idNO;
	}
	public String getIdNOPlace() {
		return idNOPlace;
	}
	public void setIdNOPlace(String idNOPlace) {
		this.idNOPlace = idNOPlace;
	}
	public Date getIdNODate() {
		return idNODate;
	}
	public void setIdNODate(Date idNODate) {
		this.idNODate = idNODate;
	}
	public String getToBusinessLicense() {
		return toBusinessLicense;
	}
	public void setToBusinessLicense(String toBusinessLicense) {
		this.toBusinessLicense = toBusinessLicense;
	}
	public Date getToBusinessDate() {
		return toBusinessDate;
	}
	public void setToBusinessDate(Date toBusinessDate) {
		this.toBusinessDate = toBusinessDate;
	}
	public String getToObjectAddress() {
		return toObjectAddress;
	}
	public void setToObjectAddress(String toObjectAddress) {
		this.toObjectAddress = toObjectAddress;
	}
	public String getToRepresentative() {
		return toRepresentative;
	}
	public void setToRepresentative(String toRepresentative) {
		this.toRepresentative = toRepresentative;
	}
	public String getToPosition() {
		return toPosition;
	}
	public void setToPosition(String toPosition) {
		this.toPosition = toPosition;
	}
	public BigDecimal getFreezer() {
		return freezer;
	}
	public void setFreezer(BigDecimal freezer) {
		this.freezer = freezer;
	}
	public BigDecimal getRefrigerator() {
		return refrigerator;
	}
	public void setRefrigerator(BigDecimal refrigerator) {
		this.refrigerator = refrigerator;
	}
	public Date getContractCreateDate() {
		return contractCreateDate;
	}
	public void setContractCreateDate(Date contractCreateDate) {
		this.contractCreateDate = contractCreateDate;
	}
	public String getFromShopName() {
		return fromShopName;
	}
	public void setFromShopName(String fromShopName) {
		this.fromShopName = fromShopName;
	}
	public String getFromShopAddess() {
		return fromShopAddess;
	}
	public void setFromShopAddess(String fromShopAddess) {
		this.fromShopAddess = fromShopAddess;
	}
	public String getFromPhone() {
		return fromPhone;
	}
	public void setFromPhone(String fromPhone) {
		this.fromPhone = fromPhone;
	}
	public String getFromFax() {
		return fromFax;
	}
	public void setFromFax(String fromFax) {
		this.fromFax = fromFax;
	}
	public String getFromRepresentative() {
		return fromRepresentative;
	}
	public void setFromRepresentative(String fromRepresentative) {
		this.fromRepresentative = fromRepresentative;
	}
	public String getFromPosition() {
		return fromPosition;
	}
	public void setFromPosition(String fromPosition) {
		this.fromPosition = fromPosition;
	}
	public String getToCustomerName() {
		return toCustomerName;
	}
	public void setToCustomerName(String toCustomerName) {
		this.toCustomerName = toCustomerName;
	}
	public String getToCustomerCode() {
		return toCustomerCode;
	}
	public void setToCustomerCode(String toCustomerCode) {
		this.toCustomerCode = toCustomerCode;
	}
	public String getToAddress() {
		return toAddress;
	}
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}
	public String getToPhone() {
		return toPhone;
	}
	public void setToPhone(String toPhone) {
		this.toPhone = toPhone;
	}
	public String getToBusinessPlace() {
		return toBusinessPlace;
	}
	public void setToBusinessPlace(String toBusinessPlace) {
		this.toBusinessPlace = toBusinessPlace;
	}
	public Date getContractDate() {
		return contractDate;
	}
	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}
	public String getKenh() {
		return kenh;
	}
	public void setKenh(String kenh) {
		this.kenh = kenh;
	}
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public Integer getNumberFile() {
		return numberFile;
	}
	public void setNumberFile(Integer numberFile) {
		this.numberFile = numberFile;
	}
	public Integer getDepreciation() {
		return depreciation;
	}
	public void setDepreciation(Integer depreciation) {
		this.depreciation = depreciation;
	}
	public String getEquipGroup() {
		return equipGroup;
	}
	public void setEquipGroup(String equipGroup) {
		this.equipGroup = equipGroup;
	}
	public String getEquipProvider() {
		return equipProvider;
	}
	public void setEquipProvider(String equipProvider) {
		this.equipProvider = equipProvider;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getHealth() {
		return health;
	}
	public void setHealth(String health) {
		this.health = health;
	}
	public Integer getManufacturingYear() {
		return manufacturingYear;
	}
	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}
	public Date getCreateFormDate() {
		return createFormDate;
	}
	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}
	public String getToRelation() {
		return toRelation;
	}
	public void setToRelation(String toRelation) {
		this.toRelation = toRelation;
	}
	public String getToHouseNumber() {
		return toHouseNumber;
	}
	public void setToHouseNumber(String toHouseNumber) {
		this.toHouseNumber = toHouseNumber;
	}
	public String getToStreet() {
		return toStreet;
	}
	public void setToStreet(String toStreet) {
		this.toStreet = toStreet;
	}
	public String getToProvinceName() {
		return toProvinceName;
	}
	public void setToProvinceName(String toProvinceName) {
		this.toProvinceName = toProvinceName;
	}
	public String getToDistrictName() {
		return toDistrictName;
	}
	public void setToDistrictName(String toDistrictName) {
		this.toDistrictName = toDistrictName;
	}
	public String getToWardName() {
		return toWardName;
	}
	public void setToWardName(String toWardName) {
		this.toWardName = toWardName;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getSoNha() {
		return soNha;
	}
	public void setSoNha(String soNha) {
		this.soNha = soNha;
	}
	public String getDuong() {
		return duong;
	}
	public void setDuong(String duong) {
		this.duong = duong;
	}
	public String getTp() {
		return tp;
	}
	public void setTp(String tp) {
		this.tp = tp;
	}
	public String getQuan() {
		return quan;
	}
	public void setQuan(String quan) {
		this.quan = quan;
	}
	public String getPhuong() {
		return phuong;
	}
	public void setPhuong(String phuong) {
		this.phuong = phuong;
	}
	public String getToPermanentAddress() {
		return toPermanentAddress;
	}
	public void setToPermanentAddress(String toPermanentAddress) {
		this.toPermanentAddress = toPermanentAddress;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getEquipGroupCode() {
		return equipGroupCode;
	}
	public void setEquipGroupCode(String equipGroupCode) {
		this.equipGroupCode = equipGroupCode;
	}

	public String getCusPhone() {
		return cusPhone;
	}

	public void setCusPhone(String cusPhone) {
		this.cusPhone = cusPhone;
	}
	
}

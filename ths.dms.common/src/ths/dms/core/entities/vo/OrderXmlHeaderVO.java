/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * VO map header cua sale_order xuat ra file xml
 * 
 * @author lacnv1
 * @since Mar 14, 2015
 */
public class OrderXmlHeaderVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<String, String> data;
	
	public OrderXmlHeaderVO() {
		data = new LinkedHashMap<String, String>();
		
		setDistCode(null);
		setOrderType(null);
		setOrderNbr(null);
		setInvoiceNbr(null);
		setOrderDate(null);
		setRetailerCode(null);
		setSRCode(null);
		setDeliveryCode(null);
		setAmount(null);
		setDocDiscount(null);
		setDocPromoCode(null);
		setTotal(null);
		/*setPriority(null);
		setDeliveryDate(null);*/
	}
	
	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}

	public String getDistCode() {
		return data.get("DistCode");
	}

	public void setDistCode(String distCode) {
		data.put("DistCode", distCode);
	}

	public String getOrderType() {
		return data.get("OrderType");
	}

	public void setOrderType(String orderType) {
		data.put("OrderType", orderType);
	}

	public String getOrderNbr() {
		return data.get("OrderNbr");
	}

	public void setOrderNbr(String orderNbr) {
		data.put("OrderNbr", orderNbr);
	}

	public String getInvoiceNbr() {
		return data.get("InvoiceNbr");
	}

	public void setInvoiceNbr(String invoiceNbr) {
		data.put("InvoiceNbr", invoiceNbr);
	}

	public String getOrderDate() {
		return data.get("OrderDate");
	}

	public void setOrderDate(String orderDate) {
		data.put("OrderDate", orderDate);
	}

	public String getRetailerCode() {
		return data.get("RetailerCode");
	}

	public void setRetailerCode(String retailerCode) {
		data.put("RetailerCode", retailerCode);
	}

	public String getSRCode() {
		return data.get("SRCode");
	}

	public void setSRCode(String sRCode) {
		data.put("SRCode", sRCode);
	}

	public String getDeliveryCode() {
		return data.get("DeliveryCode");
	}

	public void setDeliveryCode(String deliveryCode) {
		data.put("DeliveryCode", deliveryCode);
	}

	public String getAmount() {
		return data.get("Amount");
	}

	public void setAmount(String amount) {
		data.put("Amount", amount);
	}

	public String getDocDiscount() {
		return data.get("DocDiscount");
	}

	public void setDocDiscount(String docDiscount) {
		data.put("DocDiscount", docDiscount);
	}

	public String getDocPromoCode() {
		return data.get("DocPromoCode");
	}

	public void setDocPromoCode(String docPromoCode) {
		data.put("DocPromoCode", docPromoCode);
	}

	public String getTotal() {
		return data.get("Total");
	}

	public void setTotal(String total) {
		data.put("Total", total);
	}

	public String getPriority() {
		return data.get("Priority");
	}

	public void setPriority(String priority) {
		data.put("Priority", priority);
	}

	public String getDeliveryDate() {
		return data.get("DeliveryDate");
	}

	public void setDeliveryDate(String deliveryDate) {
		data.put("DeliveryDate", deliveryDate);
	}
}
package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * The OrganizationUnitTypeVO
 * @author liemtpt
 * @since: 11/02/2015
 */
public class OrganizationUnitTypeVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long organizationId ;
	private Long parentOrgId;
	private Long nodeOrdinal;
	//Loai to chuc: DONVI(1), CHUCVU(2)
	private Integer nodeType;
	private Long nodeTypeId;
	private String prefix;
	private String name;
	private String iconUrl;
	private String description;
	private Integer isManage;

	public Integer getNodeType() {
		return nodeType;
	}
	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
	public Long getParentOrgId() {
		return parentOrgId;
	}
	public void setParentOrgId(Long parentOrgId) {
		this.parentOrgId = parentOrgId;
	}
	public Long getNodeOrdinal() {
		return nodeOrdinal;
	}
	public void setNodeOrdinal(Long nodeOrdinal) {
		this.nodeOrdinal = nodeOrdinal;
	}
	public Long getNodeTypeId() {
		return nodeTypeId;
	}
	public void setNodeTypeId(Long nodeTypeId) {
		this.nodeTypeId = nodeTypeId;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getIsManage() {
		return isManage;
	}
	public void setIsManage(Integer isManage) {
		this.isManage = isManage;
	}
	
	
	
}

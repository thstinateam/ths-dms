package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromotionCustomerVO implements Serializable {

	private static final long serialVersionUID = 1L;

	Long mapId;
	Long id;
	Long parentId;
	Long customerId;
	String customerCode;
	String customerName;
	String address;
	private BigDecimal quantity;
	private BigDecimal receivedQtt;
	private BigDecimal amountMax;
	private BigDecimal receivedAmt;
	private BigDecimal numMax;
	private BigDecimal receivedNum;
	
	Integer isCustomer;
	public Long getMapId() {
		return mapId;
	}
	public void setMapId(Long mapId) {
		this.mapId = mapId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
//	public Integer getQuantity() {
//		return quantity;
//	}
//	public void setQuantity(Integer quantity) {
//		this.quantity = quantity;
//	}
//	public Integer getReceivedQtt() {
//		return receivedQtt;
//	}
//	public void setReceivedQtt(Integer receivedQtt) {
//		this.receivedQtt = receivedQtt;
//	}
	public Integer getIsCustomer() {
		return isCustomer;
	}
	public void setIsCustomer(Integer isCustomer) {
		this.isCustomer = isCustomer;
	}
	public BigDecimal getAmountMax() {
		return amountMax;
	}
	public void setAmountMax(BigDecimal amountMax) {
		this.amountMax = amountMax;
	}
	public BigDecimal getReceivedAmt() {
		return receivedAmt;
	}
	public void setReceivedAmt(BigDecimal receivedAmt) {
		this.receivedAmt = receivedAmt;
	}
	public BigDecimal getNumMax() {
		return numMax;
	}
	public void setNumMax(BigDecimal numMax) {
		this.numMax = numMax;
	}
	public BigDecimal getReceivedNum() {
		return receivedNum;
	}
	public void setReceivedNum(BigDecimal receivedNum) {
		this.receivedNum = receivedNum;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getReceivedQtt() {
		return receivedQtt;
	}
	public void setReceivedQtt(BigDecimal receivedQtt) {
		this.receivedQtt = receivedQtt;
	}

}
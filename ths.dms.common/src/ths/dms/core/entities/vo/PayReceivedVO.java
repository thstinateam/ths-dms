/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VO pay_received
 * 
 * @author lacnv1
 * @since Mar 19, 2015
 */
public class PayReceivedVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String payReceivedNumber;
	private Integer receiptType;
	private Integer ownerType;
	private String createDateStr;
	private BigDecimal amount;
	private Integer status;
	private String bankName;
	private String payerName;
	private String payerAddress;
	private String payingReason;
	private BigDecimal discount;
	private BigDecimal remain;
	private String fromObjectNumbers;
	private Long payId;
	
	public String getPayReceivedNumber() {
		return payReceivedNumber;
	}

	public void setPayReceivedNumber(String payReceivedNumber) {
		this.payReceivedNumber = payReceivedNumber;
	}

	public Integer getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(Integer receiptType) {
		this.receiptType = receiptType;
	}

	public Integer getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(Integer ownerType) {
		this.ownerType = ownerType;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getPayerName() {
		return payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getPayerAddress() {
		return payerAddress;
	}

	public void setPayerAddress(String payerAddress) {
		this.payerAddress = payerAddress;
	}
	
	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public String getPayingReason() {
		return payingReason;
	}

	public void setPayingReason(String payingReason) {
		this.payingReason = payingReason;
	}

	public BigDecimal getRemain() {
		return remain;
	}

	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}

	public String getFromObjectNumbers() {
		return fromObjectNumbers;
	}

	public void setFromObjectNumbers(String fromObjectNumbers) {
		this.fromObjectNumbers = fromObjectNumbers;
	}

	public Long getPayId() {
		return payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}
	
}
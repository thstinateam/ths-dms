package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class KSCusProductRewardDoneVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3429663424031241431L;
	private Long ksCustomerId;
	private Boolean isDone;
	private BigDecimal totalProductNumDone;
	
	public Long getKSCustomerId() {
		return ksCustomerId;
	}
	public void setKSCustomerId(Long ksCustomerId) {
		this.ksCustomerId = ksCustomerId;
	}
	public Boolean getIsDone() {
		return isDone;
	}
	public void setIsDone(Boolean isDone) {
		this.isDone = isDone;
	}
	public BigDecimal getTotalProductNumDone() {
		return totalProductNumDone;
	}
	public void setTotalProductNumDone(BigDecimal totalProductNumDone) {
		this.totalProductNumDone = totalProductNumDone;
	}
}

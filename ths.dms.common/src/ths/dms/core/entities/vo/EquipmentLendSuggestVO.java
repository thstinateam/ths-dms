package ths.dms.core.entities.vo;

import java.io.Serializable;

public class EquipmentLendSuggestVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String donVi;
	private String maBienBan;
	private String ngay;
	private String giamSat;
	private String trangThaiBienBan;
	private String trangThaiGiaoNhan;
	
	public String getDonVi() {
		return donVi;
	}
	public void setDonVi(String donVi) {
		this.donVi = donVi;
	}
	public String getMaBienBan() {
		return maBienBan;
	}
	public void setMaBienBan(String maBienBan) {
		this.maBienBan = maBienBan;
	}
	public String getNgay() {
		return ngay;
	}
	public void setNgay(String ngay) {
		this.ngay = ngay;
	}
	public String getGiamSat() {
		return giamSat;
	}
	public void setGiamSat(String giamSat) {
		this.giamSat = giamSat;
	}
	public String getTrangThaiBienBan() {
		return trangThaiBienBan;
	}
	public void setTrangThaiBienBan(String trangThaiBienBan) {
		this.trangThaiBienBan = trangThaiBienBan;
	}
	public String getTrangThaiGiaoNhan() {
		return trangThaiGiaoNhan;
	}
	public void setTrangThaiGiaoNhan(String trangThaiGiaoNhan) {
		this.trangThaiGiaoNhan = trangThaiGiaoNhan;
	}
	
	

}

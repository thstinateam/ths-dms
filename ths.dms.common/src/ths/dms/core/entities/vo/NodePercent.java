package ths.dms.core.entities.vo;
public class NodePercent extends Node {
	public NodePercent(Node n){
		this.productCode=n.productCode;
		this.quantity=n.quantity;
		this.isRequired=n.isRequired;
		this.amount=n.amount;
		this.percent=n.percent;
	}
		public NodePercent(Float __percent) {
			this.percent = __percent;
		}
	}
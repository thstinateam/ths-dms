/**
 * Copyright (c) 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Thong tin thuoc tinh dong
 * @author tuannd20
 * @since 03/09/2015
 */
public class DynamicAttributeVO implements Serializable {
	private static final long serialVersionUID = -1695884548828735022L;

	private Long attributeId;
	private String attributeCode;
	private String attributeName;
	private Integer attributeType;
	private Integer displayOrder;
	private Long attributeDetailId;
	private String attributeDetailCode;
	private String attributeDetailName;
	private Integer attibuteValueMaxLength;
	private BigDecimal attributeMinValue;
	private BigDecimal attributeMaxValue;
	private Integer attributeMandatoy;
	
	public String getAttributeCode() {
		return attributeCode;
	}
	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public Integer getAttributeType() {
		return attributeType;
	}
	public void setAttributeType(Integer attributeType) {
		this.attributeType = attributeType;
	}
	public String getAttributeDetailCode() {
		return attributeDetailCode;
	}
	public void setAttributeDetailCode(String attributeDetailCode) {
		this.attributeDetailCode = attributeDetailCode;
	}
	public String getAttributeDetailName() {
		return attributeDetailName;
	}
	public void setAttributeDetailName(String attributeDetailName) {
		this.attributeDetailName = attributeDetailName;
	}
	public Integer getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
	public Integer getAttibuteValueMaxLength() {
		return attibuteValueMaxLength;
	}
	public void setAttibuteValueMaxLength(Integer attibuteValueMaxLength) {
		this.attibuteValueMaxLength = attibuteValueMaxLength;
	}
	public BigDecimal getAttributeMinValue() {
		return attributeMinValue;
	}
	public void setAttributeMinValue(BigDecimal attributeMinValue) {
		this.attributeMinValue = attributeMinValue;
	}
	public BigDecimal getAttributeMaxValue() {
		return attributeMaxValue;
	}
	public void setAttributeMaxValue(BigDecimal attributeMaxValue) {
		this.attributeMaxValue = attributeMaxValue;
	}
	public Integer getAttributeMandatoy() {
		return attributeMandatoy;
	}
	public void setAttributeMandatoy(Integer attributeMandatoy) {
		this.attributeMandatoy = attributeMandatoy;
	}
	public Long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}
	public Long getAttributeDetailId() {
		return attributeDetailId;
	}
	public void setAttributeDetailId(Long attributeDetailId) {
		this.attributeDetailId = attributeDetailId;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;

public class ReasonVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long reasonId;
	private String reasonCode;
	private String reasonGroupCode;
	
	
	
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public String getReasonGroupCode() {
		return reasonGroupCode;
	}
	public void setReasonGroupCode(String reasonGroupCode) {
		this.reasonGroupCode = reasonGroupCode;
	}
	public Long getReasonId() {
		return reasonId;
	}
	public void setReasonId(Long reasonId) {
		this.reasonId = reasonId;
	}
	public ReasonVO(Long reasonId, String reasonCode, String reasonGroupCode) {
		super();
		this.reasonId = reasonId;
		this.reasonCode = reasonCode;
		this.reasonGroupCode = reasonGroupCode;
	}
	
	
}

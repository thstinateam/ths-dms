package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.core.entities.vo.rpt.DynamicVO;

public class StaffExportVO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String shopCode;
	private String staffCode;
	private String staffName;
	private Integer gender;
	private String genderStr;
	private String startDate;
	private String education;
	private String position;
	private String idNumber;
	private String idDate;
	private String idPlace;
	private String houseNumber;
	private String street;
	private String address;
	private String provinceCode;
	private String provinceName;
	private String districtCode;
	private String districtName;
	private String precinctCode;
	private String precinctName;
	private String mobile;
	private String phone;
	private String email;
	private String staffType;
	private String saleTypeCode;
	private String saleGroup;
	private Integer status;
	private String statusStr;
	private String shopName;
	private String areaName;
	private List<DynamicVO> dynamicAttributes;
	
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getIdDate() {
		return idDate;
	}
	public void setIdDate(String idDate) {
		this.idDate = idDate;
	}
	public String getIdPlace() {
		return idPlace;
	}
	public void setIdPlace(String idPlace) {
		this.idPlace = idPlace;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getProvinceCode() {
		return provinceCode;
	}
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getDistrictCode() {
		return districtCode;
	}
	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getPrecinctCode() {
		return precinctCode;
	}
	public void setPrecinctCode(String precinctCode) {
		this.precinctCode = precinctCode;
	}
	public String getPrecinctName() {
		return precinctName;
	}
	public void setPrecinctName(String precinctName) {
		this.precinctName = precinctName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStaffType() {
		return staffType;
	}
	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}
	public String getSaleTypeCode() {
		return saleTypeCode;
	}
	public void setSaleTypeCode(String saleTypeCode) {
		this.saleTypeCode = saleTypeCode;
	}
	public String getSaleGroup() {
		return saleGroup;
	}
	public void setSaleGroup(String saleGroup) {
		this.saleGroup = saleGroup;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getGenderStr() {
		return genderStr;
	}
	public void setGenderStr(String genderStr) {
		this.genderStr = genderStr;
	}
	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public List<DynamicVO> getDynamicAttributes() {
		return dynamicAttributes;
	}
	public void setDynamicAttributes(List<DynamicVO> dynamicAttributes) {
		this.dynamicAttributes = dynamicAttributes;
	}
	
}
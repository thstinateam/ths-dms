package ths.dms.core.entities.vo;
/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.entities.EquipLendDetail;

/**
 * Class Equip Borrow VO
 * 
 * @author hutnn
 * @since 02/07/2015
 */
public class EquipBorrowVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String code;
	private String note;
	private String shopCode;
	private String shopName;
	private String staffCode;
	private String staffName;
	private Integer status;
	private String createFormDate;
	private Integer statusDelivery;
	
	private BigDecimal totalEquip;
	private List<EquipLendDetailVO> lstEquipLendDetailVOs;
	private List<EquipLendDetail> lstEquipLendDetails;
	/**
	 * Khai bao phuong thuc GETTER/SETTER
	 * */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getStatusDelivery() {
		return statusDelivery;
	}
	public void setStatusDelivery(Integer statusDelivery) {
		this.statusDelivery = statusDelivery;
	}
	public String getCreateFormDate() {
		return createFormDate;
	}
	public void setCreateFormDate(String createFormDate) {
		this.createFormDate = createFormDate;
	}
	public List<EquipLendDetailVO> getLstEquipLendDetailVOs() {
		return lstEquipLendDetailVOs;
	}
	public void setLstEquipLendDetailVOs(List<EquipLendDetailVO> lstEquipLendDetailVOs) {
		this.lstEquipLendDetailVOs = lstEquipLendDetailVOs;
	}
	public BigDecimal getTotalEquip() {
		return totalEquip;
	}
	public void setTotalEquip(BigDecimal totalEquip) {
		this.totalEquip = totalEquip;
	}
	public List<EquipLendDetail> getLstEquipLendDetails() {
		return lstEquipLendDetails;
	}
	public void setLstEquipLendDetails(List<EquipLendDetail> lstEquipLendDetails) {
		this.lstEquipLendDetails = lstEquipLendDetails;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
}

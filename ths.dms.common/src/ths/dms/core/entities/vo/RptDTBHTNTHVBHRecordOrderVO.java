package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptDTBHTNTHVBHRecordOrderVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal staffId;
	private String staffName;
	private String staffCode;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private BigDecimal revenue;
	private BigDecimal cash;
	private BigDecimal debit;
	private BigDecimal discount;
	private BigDecimal moneyDiscount;
	private BigDecimal productDiscount;
	private BigDecimal SKU;
	private BigDecimal money;
	private String dateOrder;
	private BigDecimal sumRevenueDate;
	private BigDecimal sumMoneyDiscountDate;
	private BigDecimal sumProductDiscountDate;
	private BigDecimal sumMoneyDate;
	private BigDecimal sumDiscountDate;
	private BigDecimal sumSKUDate;
	private BigDecimal sumDebitDate;
	private BigDecimal sumCashDate;
	private BigDecimal sumRevenueStaff;
	private BigDecimal sumMoneyDiscountStaff;
	private BigDecimal sumProductDiscountStaff;
	private BigDecimal sumMoneyStaff;
	private BigDecimal sumDiscountStaff;
	private BigDecimal sumSKUStaff;
	private BigDecimal sumDebitStaff;
	private BigDecimal sumCashStaff;
	
	/*public String getAddress() {
		String address = "";
		if(this.customerCode.length() > 0 && this.customerCode != null)
			address += this.customerCode;
		if(address.length() > 0 && this.customerName.length() > 0 && this.customerName != null)
			address += " - " + this.customerName;
		if(address.length() > 0 && this.customerAddress.length() > 0 && this.customerAddress != null)
			address += " - " + this.customerAddress;
		return address;
	}*/
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getstaffCode() {
		return staffCode;
	}
	public void setstaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	
	public BigDecimal getCash() {
		return cash;
	}
	public void setCash(BigDecimal cash) {
		this.cash = cash;
	}
	public BigDecimal getDebit() {
		return debit;
	}
	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getMoneyDiscount() {
		return moneyDiscount;
	}
	public void setMoneyDiscount(BigDecimal moneyDiscount) {
		this.moneyDiscount = moneyDiscount;
	}
	public BigDecimal getProductDiscount() {
		return productDiscount;
	}
	public void setProductDiscount(BigDecimal productDiscount) {
		this.productDiscount = productDiscount;
	}
	public BigDecimal getSKU() {
		return SKU;
	}
	public void setSKU(BigDecimal sKU) {
		SKU = sKU;
	}
	public BigDecimal getRevenue() {
		return revenue;
	}
	public void setRevenue(BigDecimal revenue) {
		this.revenue = revenue;
	}
	public BigDecimal getMoney() {
		return money;
	}
	public void setMoney(BigDecimal money) {
		this.money = money;
	}
	
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getDateOrder() {
		return dateOrder;
	}
	public void setDateOrder(String dateOrder) {
		this.dateOrder = dateOrder;
	}
	public BigDecimal getSumRevenueDate() {
		return sumRevenueDate;
	}
	public void setSumRevenueDate(BigDecimal sumRevenueDate) {
		this.sumRevenueDate = sumRevenueDate;
	}
	public BigDecimal getSumMoneyDiscountDate() {
		return sumMoneyDiscountDate;
	}
	public void setSumMoneyDiscountDate(BigDecimal sumMoneyDiscountDate) {
		this.sumMoneyDiscountDate = sumMoneyDiscountDate;
	}
	public BigDecimal getSumProductDiscountDate() {
		return sumProductDiscountDate;
	}
	public void setSumProductDiscountDate(BigDecimal sumProductDiscountDate) {
		this.sumProductDiscountDate = sumProductDiscountDate;
	}
	public BigDecimal getSumMoneyDate() {
		return sumMoneyDate;
	}
	public void setSumMoneyDate(BigDecimal sumMoneyDate) {
		this.sumMoneyDate = sumMoneyDate;
	}
	public BigDecimal getSumDiscountDate() {
		return sumDiscountDate;
	}
	public void setSumDiscountDate(BigDecimal sumDiscountDate) {
		this.sumDiscountDate = sumDiscountDate;
	}
	public BigDecimal getSumSKUDate() {
		return sumSKUDate;
	}
	public void setSumSKUDate(BigDecimal sumSKUDate) {
		this.sumSKUDate = sumSKUDate;
	}
	public BigDecimal getSumDebitDate() {
		return sumDebitDate;
	}
	public void setSumDebitDate(BigDecimal sumDebitDate) {
		this.sumDebitDate = sumDebitDate;
	}
	public BigDecimal getSumCashDate() {
		return sumCashDate;
	}
	public void setSumCashDate(BigDecimal sumCashDate) {
		this.sumCashDate = sumCashDate;
	}
	public BigDecimal getSumRevenueStaff() {
		return sumRevenueStaff;
	}
	public void setSumRevenueStaff(BigDecimal sumRevenueStaff) {
		this.sumRevenueStaff = sumRevenueStaff;
	}
	public BigDecimal getSumMoneyDiscountStaff() {
		return sumMoneyDiscountStaff;
	}
	public void setSumMoneyDiscountStaff(BigDecimal sumMoneyDiscountStaff) {
		this.sumMoneyDiscountStaff = sumMoneyDiscountStaff;
	}
	public BigDecimal getSumProductDiscountStaff() {
		return sumProductDiscountStaff;
	}
	public void setSumProductDiscountStaff(BigDecimal sumProductDiscountStaff) {
		this.sumProductDiscountStaff = sumProductDiscountStaff;
	}
	public BigDecimal getSumMoneyStaff() {
		return sumMoneyStaff;
	}
	public void setSumMoneyStaff(BigDecimal sumMoneyStaff) {
		this.sumMoneyStaff = sumMoneyStaff;
	}
	public BigDecimal getSumDiscountStaff() {
		return sumDiscountStaff;
	}
	public void setSumDiscountStaff(BigDecimal sumDiscountStaff) {
		this.sumDiscountStaff = sumDiscountStaff;
	}
	public BigDecimal getSumSKUStaff() {
		return sumSKUStaff;
	}
	public void setSumSKUStaff(BigDecimal sumSKUStaff) {
		this.sumSKUStaff = sumSKUStaff;
	}
	public BigDecimal getSumDebitStaff() {
		return sumDebitStaff;
	}
	public void setSumDebitStaff(BigDecimal sumDebitStaff) {
		this.sumDebitStaff = sumDebitStaff;
	}
	public BigDecimal getSumCashStaff() {
		return sumCashStaff;
	}
	public void setSumCashStaff(BigDecimal sumCashStaff) {
		this.sumCashStaff = sumCashStaff;
	}
	public BigDecimal getStaffId() {
		return staffId;
	}
	public void setStaffId(BigDecimal staffId) {
		this.staffId = staffId;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;

public class RoutingTreeVO implements Serializable{
	/**
	 * @author hunglm16
	 * @since MAY 17, 2014
	 */
	private static final long serialVersionUID = 1L;
	private String routingCode;
	private String routingName;
	private Long routingId;
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	private Integer status;
	private Long shopId;
	private Integer isStatus;
	private Integer isNotMigrate;	
	private Integer isNotTrigger;
	
	public String getRoutingCode() {
		return routingCode;
	}
	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}
	public String getRoutingName() {
		return routingName;
	}
	public void setRoutingName(String routingName) {
		this.routingName = routingName;
	}
	public Long getRoutingId() {
		return routingId;
	}
	public void setRoutingId(Long routingId) {
		this.routingId = routingId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Integer getIsStatus() {
		return isStatus;
	}
	public void setIsStatus(Integer isStatus) {
		this.isStatus = isStatus;
	}
	public Integer getIsNotMigrate() {
		return isNotMigrate;
	}
	public void setIsNotMigrate(Integer isNotMigrate) {
		this.isNotMigrate = isNotMigrate;
	}
	public Integer getIsNotTrigger() {
		return isNotTrigger;
	}
	public void setIsNotTrigger(Integer isNotTrigger) {
		this.isNotTrigger = isNotTrigger;
	}

}

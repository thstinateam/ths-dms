package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

public class ErrorVO implements Serializable{
	/**
	 * @author hunglm16
	 * @description Thong tin loi
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer key;
	private String value;
	private String value1;
	private Long id;
	private Date date;
	private String descripton;
	
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	public String getDescripton() {
		return descripton;
	}
	public void setDescripton(String descripton) {
		this.descripton = descripton;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getKey() {
		return key;
	}
	public void setKey(Integer key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}

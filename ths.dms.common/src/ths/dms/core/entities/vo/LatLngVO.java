package ths.dms.core.entities.vo;

import java.io.Serializable;

public class LatLngVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long shopId;
	private Long customerId;
	private Double lat;
	private Double lng;
	private Double goc;
	
	public LatLngVO(){
		this.shopId=null;
		this.customerId=null;
		this.lat=null;
		this.lng=null;
		this.goc=null;
	}
	public LatLngVO(LatLngVO a){
		this.shopId=a.getShopId();
		this.customerId=a.getCustomerId();
		this.lat=a.getLat();
		this.lng=a.getLng();
		this.goc=a.getGoc();
	}
	
	public LatLngVO(Long shopId,Long customerId,Double lat,Double lng){
		this.shopId=shopId;
		this.customerId=customerId;
		this.lat=lat;
		this.lng=lng;
	}
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLng() {
		return lng;
	}
	public void setLng(Double lng) {
		this.lng = lng;
	}
	public Double getGoc() {
		return goc;
	}
	public void setGoc(Double goc) {
		this.goc = goc;
	}
	
	
}

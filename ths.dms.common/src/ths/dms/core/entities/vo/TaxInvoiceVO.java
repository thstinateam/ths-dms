package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.Invoice;

public class TaxInvoiceVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Invoice invoice;
	private List<TaxInvoiceDetailVO> listDetail;

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public List<TaxInvoiceDetailVO> getListDetail() {
		return listDetail;
	}

	public void setListDetail(List<TaxInvoiceDetailVO> listDetail) {
		this.listDetail = listDetail;
	}
}

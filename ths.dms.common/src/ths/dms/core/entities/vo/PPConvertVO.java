package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

public class PPConvertVO implements Serializable {
	Long id;
	String name;
	Long promotionId;
	List<PPConvertDetailVO> listDetail;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}
	public List<PPConvertDetailVO> getListDetail() {
		return listDetail;
	}
	public void setListDetail(List<PPConvertDetailVO> listDetail) {
		this.listDetail = listDetail;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author sangtn
 * @since 10-06-2014
 * @description Load grid phan quan ly don hang
 *
 */
public class SaleOrderVOEx2 implements Serializable{
	
	private static final long serialVersionUID = 1L;
	/**
	 * id	checkbox
	 * edit 	id
	 * refOrderNumber	refOrderNumber
	 * orderNumber	approved orderNumber
	 * nvbh	staffCode ***staff.staffCode
	 * customer ***customer.id, customer.short_code, customer.customer_name
	 * totalOrder total
	 * 
	 * orderDate cellValue
	 * deliveryDate cellValue
	 * destroyCode         
	 * address ***customer.address
	 * statusOrder type approved
	 * orderType orderType
	 * fromSaleOrder fromSaleOrder.orderNumber
	 * description   
	 * nvgh delivery.staffCode
	 */
	
	private Long id;//saleOrderId ma don hang
	private String refOrderNumber;
	private Integer approved;
	private String orderNumber;
	
	private Long staffId;
	private String staffCode;
	private String staffName;
	
	private Long customerId;
	private String shortCode;
	private String customerName;
	private String customerAddress;
	
	private BigDecimal total;	
	private Date orderDate;
	private Date deliveryDate;
	
	private Integer type;	
	private String orderType;
	private String fromSaleOrderCode;
	private Integer orderStatus;
	private Long deliveryId;
	private String deliveryCode;
	private String deliveryName;
	private String destroyCode;
	private String description;
	private Long priority;
	private String priorityStr;
	private String message;
	private BigDecimal saleOrderDiscount;
	private BigDecimal totalDiscount;
	private String customerCode;
	private String address;
	private BigDecimal totalValue;
	
	private Integer lockedStock;
	private String fromSaleOrderNumber;
	private Integer saleOrderType;
	private Long fromOrderId;
	private Integer approvedStep;
	
	public Integer getSaleOrderType() {
		return saleOrderType;
	}
	public void setSaleOrderType(Integer saleOrderType) {
		this.saleOrderType = saleOrderType;
	}
	public Integer getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getFromSaleOrderNumber() {
		return fromSaleOrderNumber;
	}
	public void setFromSaleOrderNumber(String fromSaleOrderNumber) {
		this.fromSaleOrderNumber = fromSaleOrderNumber;
	}
	public BigDecimal getSaleOrderDiscount() {
		return saleOrderDiscount;
	}
	public void setSaleOrderDiscount(BigDecimal saleOrderDiscount) {
		this.saleOrderDiscount = saleOrderDiscount;
	}
	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public BigDecimal getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPriorityStr() {
		return priorityStr;
	}
	public void setPriorityStr(String priorityStr) {
		this.priorityStr = priorityStr;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRefOrderNumber() {
		return refOrderNumber;
	}
	public void setRefOrderNumber(String refOrderNumber) {
		this.refOrderNumber = refOrderNumber;
	}
	public Integer getApproved() {
		return approved;
	}
	public void setApproved(Integer approved) {
		this.approved = approved;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getFromSaleOrderCode() {
		return fromSaleOrderCode;
	}
	public void setFromSaleOrderCode(String fromSaleOrderCode) {
		this.fromSaleOrderCode = fromSaleOrderCode;
	}
	public Long getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public String getDestroyCode() {
		return destroyCode;
	}
	public void setDestroyCode(String destroyCode) {
		this.destroyCode = destroyCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getPriority() {
		return priority;
	}
	public void setPriority(Long priority) {
		this.priority = priority;
	}
	public Integer getLockedStock() {
		return lockedStock;
	}
	public void setLockedStock(Integer lockedStock) {
		this.lockedStock = lockedStock;
	}
	public Long getFromOrderId() {
		return fromOrderId;
	}
	public void setFromOrderId(Long fromOrderId) {
		this.fromOrderId = fromOrderId;
	}
	public Integer getApprovedStep() {
		return approvedStep;
	}
	public void setApprovedStep(Integer approvedStep) {
		this.approvedStep = approvedStep;
	}
	
}
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

public class CycleCountVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id;
	private Date createDate;
	private Date startDate;
	private String cycleCountCode;
	private String description;
	private String status;
	private Integer canUpdate;
	private String createDateStr;
	private String wareHouseCode;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCycleCountCode() {
		return cycleCountCode;
	}
	public void setCycleCountCode(String cycleCountCode) {
		this.cycleCountCode = cycleCountCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCanUpdate() {
		return canUpdate;
	}
	public void setCanUpdate(Integer canUpdate) {
		this.canUpdate = canUpdate;
	}
	public String getCreateDateStr() {
		return createDateStr;
	}
	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getWareHouseCode() {
		return wareHouseCode;
	}
	public void setWareHouseCode(String wareHouseCode) {
		this.wareHouseCode = wareHouseCode;
	}
	
	
}

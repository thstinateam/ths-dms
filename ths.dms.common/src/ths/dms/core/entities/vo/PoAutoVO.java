package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ths.dms.core.common.utils.GroupUtility;

public class PoAutoVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal amount;
	private Integer convfact;
	private Integer dayPlan;
	private Double dayReservePlan;
	private Double dayReserveReal;
	private Integer expPovnmQty;
	private Integer expQty;
	private Integer expSaleorderQty;
	private Integer expStocktransQty;
	private Integer expVansaleQty;
	private Integer goPoConfirmQty;
	private Integer impPovnmQty;
	private Integer impQty;
	private Integer impSaleorderQty;
	private Integer impStockTransQty;
	private Integer lead;
	private Integer maxsf;
	private Integer monthCumulate;
	private Integer monthPlan;
	private Integer next;
	private Integer openStockTotal;
	private Float percentage;
	private Long priceId;
	private BigDecimal priceValue;
	private String productCode;
	private Long productId;
	private String productInfoCode;
	private String productName;
	private Integer quantity;
	private Integer requimentStock;
	private Integer safetyStockMin;
	private Integer stockPoDvkh;
	private Integer stockQty;
	private String warning;
	
	private Integer poImport;
	private Integer poExport;
	private Integer poStock;
	private Integer poMonthCumulate;
	private Integer poBoxQuantity;
	private Float grossWeight;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(BigDecimal convfact) {
		if (null != convfact) {
			this.convfact = convfact.intValue();
		} else {
			this.convfact = 0;
		}
	}

	public Integer getDayPlan() {
		return dayPlan;
	}

	public void setDayPlan(BigDecimal dayPlan) {
		if (null != dayPlan) {
			this.dayPlan = dayPlan.intValue();
		} else {
			this.dayPlan = 0;
		}
	}

	public Double getDayReservePlan() {
		return dayReservePlan;
	}

	public void setDayReservePlan(BigDecimal dayReservePlan) {
		if (null != dayReservePlan) {
			this.dayReservePlan = dayReservePlan.doubleValue();
		} else {
			this.dayReservePlan = new Double(0);
		}
	}

	public Double getDayReserveReal() {
		return dayReserveReal;
	}

	public void setDayReserveReal(BigDecimal dayReserveReal) {
		if (null != dayReserveReal) {
			this.dayReserveReal = dayReserveReal.doubleValue();
		} else {
			this.dayReserveReal = 0d;
		}
	}

	public Integer getExpPovnmQty() {
		return expPovnmQty;
	}

	public void setExpPovnmQty(BigDecimal expPovnmQty) {
		if (null != expPovnmQty) {
			this.expPovnmQty = expPovnmQty.intValue();
		} else {
			this.expPovnmQty = 0;
		}
	}

	public Integer getExpQty() {
		return expQty;
	}

	public void setExpQty(BigDecimal expQty) {
		if (null != expQty) {
			this.expQty = expQty.intValue();
		} else {
			this.expQty = 0;
		}
	}

	public Integer getExpSaleorderQty() {
		return expSaleorderQty;
	}

	public void setExpSaleorderQty(BigDecimal expSaleorderQty) {
		if (null != expSaleorderQty) {
			this.expSaleorderQty = expSaleorderQty.intValue();
		} else {
			this.expSaleorderQty = 0;
		}
	}

	public Integer getExpStocktransQty() {
		return expStocktransQty;
	}

	public void setExpStocktransQty(BigDecimal expStocktransQty) {
		if (null != expStocktransQty) {
			this.expStocktransQty = expStocktransQty.intValue();
		} else {
			this.expStocktransQty = 0;
		}
	}

	public Integer getExpVansaleQty() {
		return expVansaleQty;
	}

	public void setExpVansaleQty(BigDecimal expVansaleQty) {
		if (null != expVansaleQty) {
			this.expVansaleQty = expVansaleQty.intValue();
		} else {
			this.expVansaleQty = 0;
		}
	}

	public Integer getGoPoConfirmQty() {
		return goPoConfirmQty;
	}

	public void setGoPoConfirmQty(BigDecimal goPoConfirmQty) {
		if (null != goPoConfirmQty) {
			this.goPoConfirmQty = goPoConfirmQty.intValue();
		} else {
			this.goPoConfirmQty = 0;
		}

	}

	public Integer getImpPovnmQty() {
		return impPovnmQty;
	}

	public void setImpPovnmQty(BigDecimal impPovnmQty) {
		if (null != impPovnmQty) {
			this.impPovnmQty = impPovnmQty.intValue();
		} else {
			this.impPovnmQty = 0;
		}
	}

	public Integer getImpQty() {
		return impQty;
	}

	public void setImpQty(BigDecimal impQty) {
		if (null != impQty) {
			this.impQty = impQty.intValue();
		} else {
			this.impQty = 0;
		}
	}

	public Integer getImpSaleorderQty() {
		return impSaleorderQty;
	}

	public void setImpSaleorderQty(BigDecimal impSaleorderQty) {
		if (null != impSaleorderQty) {
			this.impSaleorderQty = impSaleorderQty.intValue();
		} else {
			this.impSaleorderQty = 0;
		}
	}

	public Integer getImpStockTransQty() {
		return impStockTransQty;
	}

	public void setImpStockTransQty(BigDecimal impStockTransQty) {
		if (null != impStockTransQty) {
			this.impStockTransQty = impStockTransQty.intValue();
		} else {
			this.impStockTransQty = 0;
		}
	}

	public Integer getLead() {
		return lead;
	}

	public void setLead(BigDecimal lead) {
		if (null != lead) {
			this.lead = lead.intValue();
		} else {
			this.lead = 0;
		}
	}

	public Integer getMaxsf() {
		return maxsf;
	}

	public void setMaxsf(BigDecimal maxsf) {
		if (null != maxsf) {
			this.maxsf = maxsf.intValue();
		} else {
			this.maxsf = 0;
		}
	}

	public Integer getMonthCumulate() {
		return monthCumulate;
	}

	public void setMonthCumulate(BigDecimal monthCumulate) {
		if (null != monthCumulate) {
			this.monthCumulate = monthCumulate.intValue();
		} else {
			this.monthCumulate = 0;
		}
	}

	public Integer getMonthPlan() {
		return monthPlan;
	}

	public void setMonthPlan(BigDecimal monthPlan) {
		if (null != monthPlan) {
			this.monthPlan = monthPlan.intValue();
		} else {
			this.monthPlan = 0;
		}
	}

	public Integer getNext() {
		return next;
	}

	public void setNext(BigDecimal next) {
		if (null != next) {
			this.next = next.intValue();
		} else {
			this.next = 0;
		}
	}

	public Integer getOpenStockTotal() {
		return openStockTotal;
	}

	public void setOpenStockTotal(BigDecimal openStockTotal) {
		if (null != openStockTotal) {
			this.openStockTotal = openStockTotal.intValue();
		} else {
			this.openStockTotal = 0;
		}
	}

	public Float getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		if (null != percentage) {
			this.percentage = percentage.floatValue();
		} else {
			this.percentage = 0f;
		}
	}

	public Long getPriceId() {
		return priceId;
	}

	public void setPriceId(BigDecimal priceId) {
		if (null != priceId) {
			this.priceId = priceId.longValue();
		} else {
			this.priceId = 0l;
		}
	}

	public BigDecimal getPriceValue() {
		return priceValue;
	}

	public void setPriceValue(BigDecimal priceValue) {
		this.priceValue = priceValue;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(BigDecimal productId) {
		if (null != productId) {
			this.productId = productId.longValue();
		} else {
			this.productId = 0l;
		}
	}

	public String getProductInfoCode() {
		return productInfoCode;
	}

	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		if (null != quantity) {
			this.quantity = quantity.intValue();
		} else {
			this.quantity = 0;
		}
	}

	public Integer getRequimentStock() {
		return requimentStock;
	}

	public void setRequimentStock(BigDecimal requimentStock) {
		if (null != requimentStock) {
			this.requimentStock = requimentStock.intValue();
		} else {
			this.requimentStock = 0;
		}
	}

	public Integer getSafetyStockMin() {
		return safetyStockMin;
	}

	public void setSafetyStockMin(BigDecimal safetyStockMin) {
		if (null != safetyStockMin) {
			this.safetyStockMin = safetyStockMin.intValue();
		} else {
			this.safetyStockMin = 0;
		}
	}

	public Integer getStockPoDvkh() {
		return stockPoDvkh;
	}

	public void setStockPoDvkh(BigDecimal stockPoDvkh) {
		if (null != stockPoDvkh) {
			this.stockPoDvkh = stockPoDvkh.intValue();
		} else {
			this.stockPoDvkh = 0;
		}
	}

	public Integer getStockQty() {
		return stockQty;
	}

	public void setStockQty(BigDecimal stockQty) {
		if (null != stockQty) {
			this.stockQty = stockQty.intValue();
		} else {
			this.stockQty = 0;
		}
	}

	public String getWarning() {
		return warning;
	}

	public void setWarning(String warning) {
		this.warning = warning;
	}

	public Integer getPoImport() {
		return poImport;
	}

	public void setPoImport(BigDecimal poImport) {
		if (null != poImport) {
			this.poImport = poImport.intValue();
		} else {
			this.poImport = 0;
		}
	}

	public Integer getPoExport() {
		return poExport;
	}

	public void setPoExport(BigDecimal poExport) {
		if (null != poExport) {
			this.poExport = poExport.intValue();
		} else {
			this.poExport = 0;
		}
	}

	public Integer getPoMonthCumulate() {
		return poMonthCumulate;
	}

	public void setPoMonthCumulate(BigDecimal poMonthCumulate) {
		if (null != poMonthCumulate) {
			this.poMonthCumulate = poMonthCumulate.intValue();
		} else {
			this.poMonthCumulate = 0;
		}
	}

	public Integer getPoBoxQuantity() {
		return poBoxQuantity;
	}

	public void setPoBoxQuantity(BigDecimal poBoxQuantity) {
		if (null != poBoxQuantity) {
			this.poBoxQuantity = poBoxQuantity.intValue();
		} else {
			this.poBoxQuantity = 0;
		}
	}

	public Integer getPoStock() {
		return poStock;
	}

	public void setPoStock(BigDecimal poStock) {
		if (null != poStock) {
			this.poStock = poStock.intValue();
		} else {
			this.poStock = 0;
		}
	}
	
	public Float getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(BigDecimal grossWeight) {
		if(null != grossWeight){
			this.grossWeight = grossWeight.floatValue();
		}else{
			this.grossWeight = 0f;
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<PoAutoGroupByCatVO> groupByCat(List<PoAutoVO> listData)
			throws Exception {
		List<PoAutoGroupByCatVO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, "productInfoCode");

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<PoAutoGroupByCatVO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					PoAutoGroupByCatVO vo = new PoAutoGroupByCatVO();

					List<PoAutoVO> listObject = (List<PoAutoVO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setSubCat((String) mapEntry.getKey());

						vo.setData(listObject);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}
}
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Date;

import ths.dms.core.entities.enumtype.ApprovalStatus;

public class PoAuto01VO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private Long poAutoId;
	private String poAutoNumber;
	private Long shopId;
	private String shopCode;
	private String shopName;
	private int status;
	private Date poAutoDate;
	private String poAutoDateStr;
	private BigDecimal amount;

	// tong so luong, lay tu po_auto_detail
	private Long sumQuantity;

	public Long getPoAutoId() {
		return poAutoId;
	}

	public void setPoAutoId(Long poAutoId) {
		this.poAutoId = poAutoId;
	}

	public String getPoAutoNumber() {
		return poAutoNumber;
	}

	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public ApprovalStatus getStatus() {
		return ApprovalStatus.parseValue(status);
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getPoAutoDate() {
		return poAutoDate;
	}

	public void setPoAutoDate(Date poAutoDate) {
		this.poAutoDate = poAutoDate;
	}

	public Long getSumQuantity() {
		return sumQuantity;
	}

	public void setSumQuantity(Long sumQuantity) {
		this.sumQuantity = sumQuantity;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getPoAutoDateStr() {
		String dateString = "";
		if (this.poAutoDate == null)
			return dateString;
		Object[] params = new Object[] { this.poAutoDate };

		try {
			dateString = MessageFormat
					.format("{0,date," + "dd/MM/yyyy" + "}", params);
		} catch (Exception e) {
			
		}
		this.poAutoDateStr = dateString;
		return poAutoDateStr;
	}
	
}

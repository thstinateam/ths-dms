package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StDisplayPDGroupDTL implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long groupId;
	
	private List<StDisplayPDGroupDTLVO> displayPDGroupDTLVOs = new ArrayList<StDisplayPDGroupDTLVO>();

	public List<StDisplayPDGroupDTLVO> getDisplayPDGroupDTLVOs() {
		return displayPDGroupDTLVOs;
	}

	public void setDisplayPDGroupDTLVOs(List<StDisplayPDGroupDTLVO> displayPDGroupDTLVOs) {
		this.displayPDGroupDTLVOs = displayPDGroupDTLVOs;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class SumSalePlanVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer totalSalePlanQty;
	private Integer assignSalePlanQty;
	
	private BigDecimal totalSalePlanAmt;
	private BigDecimal assignSalePlanAmt;
	public Integer getTotalSalePlanQty() {
		if (totalSalePlanQty == null) {
			totalSalePlanQty = 0;
		}
		return totalSalePlanQty;
	}
	public void setTotalSalePlanQty(Integer totalSalePlanQty) {
		this.totalSalePlanQty = totalSalePlanQty;
	}
	public Integer getAssignSalePlanQty() {
		if (assignSalePlanQty == null) {
			assignSalePlanQty = 0;
		}
		return assignSalePlanQty;
	}
	public void setAssignSalePlanQty(Integer assignSalePlanQty) {
		this.assignSalePlanQty = assignSalePlanQty;
	}
	public BigDecimal getTotalSalePlanAmt() {
		return totalSalePlanAmt == null ? BigDecimal.valueOf(0) : totalSalePlanAmt;
	}
	public void setTotalSalePlanAmt(BigDecimal totalSalePlanAmt) {
		this.totalSalePlanAmt = totalSalePlanAmt;
	}
	public BigDecimal getAssignSalePlanAmt() {
		return assignSalePlanAmt == null ? BigDecimal.valueOf(0) : assignSalePlanAmt;
	}
	public void setAssignSalePlanAmt(BigDecimal assignSalePlanAmt) {
		this.assignSalePlanAmt = assignSalePlanAmt;
	}


}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Lop khai bao cua Chuong trinh va Don vi
 * @modify hunglm16
 * @since 22/11/2015
 */
public class PromotionShopMapVO implements Serializable{

	private static final long serialVersionUID = 7457477733939615641L;

	private Long id;
	private Long shopId;
	private Long promotionProgramId;
	
	private String shopCode;
	private String shopName;
	private String customerCode;
	private String shortCode;
	private String staffCode;
	private String promotionProgramCode;
	private String promotionProgramName;
	
	private BigDecimal quantityMaxShop;
	private BigDecimal quantityMaxCus;
	private BigDecimal numMax;
	private BigDecimal amountMax;
	private BigDecimal quantityMaxStaff;
	
	/**
	 * Khai bao caca phuong thuc GETTER/SETTER
	 * @return
	 */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getPromotionProgramId() {
		return promotionProgramId;
	}
	public void setPromotionProgramId(Long promotionProgramId) {
		this.promotionProgramId = promotionProgramId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}
	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}
	public String getPromotionProgramName() {
		return promotionProgramName;
	}
	public void setPromotionProgramName(String promotionProgramName) {
		this.promotionProgramName = promotionProgramName;
	}
	public BigDecimal getQuantityMaxShop() {
		return quantityMaxShop;
	}
	public void setQuantityMaxShop(BigDecimal quantityMaxShop) {
		this.quantityMaxShop = quantityMaxShop;
	}
	public BigDecimal getQuantityMaxCus() {
		return quantityMaxCus;
	}
	public void setQuantityMaxCus(BigDecimal quantityMaxCus) {
		this.quantityMaxCus = quantityMaxCus;
	}
	public BigDecimal getNumMax() {
		return numMax;
	}
	public void setNumMax(BigDecimal numMax) {
		this.numMax = numMax;
	}
	public BigDecimal getAmountMax() {
		return amountMax;
	}
	public void setAmountMax(BigDecimal amountMax) {
		this.amountMax = amountMax;
	}
	public BigDecimal getQuantityMaxStaff() {
		return quantityMaxStaff;
	}
	public void setQuantityMaxStaff(BigDecimal quantityMaxStaff) {
		this.quantityMaxStaff = quantityMaxStaff;
	}

}

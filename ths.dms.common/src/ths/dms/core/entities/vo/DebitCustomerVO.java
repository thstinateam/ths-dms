package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/*
 * Doi tuong cong no khach hang
 * ThuatTQ
 */
public class DebitCustomerVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String shortCode;
	private String customerName;
	
	private Date createDate;
	private Date payDate;
	private String orderNumber;
	private String address;
	private String staffCode;
	private String staffName;
	private String deliveryName;
	private String cashierName;
	private BigDecimal totalAmount;
	private BigDecimal totalAmountPay;
	private BigDecimal discountAmount;
	private BigDecimal totalAmountRemain;
	private String payType;
	private BigDecimal amountRemain;
	
	



	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BigDecimal getAmountRemain() {
		return amountRemain;
	}

	public void setAmountRemain(BigDecimal amountRemain) {
		this.amountRemain = amountRemain;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalAmountPay() {
		return totalAmountPay;
	}

	public void setTotalAmountPay(BigDecimal totalAmountPay) {
		this.totalAmountPay = totalAmountPay;
	}

	public BigDecimal getTotalAmountRemain() {
		return totalAmountRemain;
	}

	public void setTotalAmountRemain(BigDecimal totalAmountRemain) {
		this.totalAmountRemain = totalAmountRemain;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getCashierName() {
		return cashierName;
	}

	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}
}

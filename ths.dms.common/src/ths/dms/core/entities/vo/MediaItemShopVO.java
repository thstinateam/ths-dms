package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class MediaItemShopVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1956654986662515109L;
	private String shopCode;
	private HashMap<String, MediaItemCustomerVO> mapCustomer;
	private List<MediaItemCustomerVO> lstMediaItemCustomer;
	
	public HashMap<String, MediaItemCustomerVO> getMapCustomer() {
		return mapCustomer;
	}
	public void setMapCustomer(HashMap<String, MediaItemCustomerVO> mapCustomer) {
		this.mapCustomer = mapCustomer;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public List<MediaItemCustomerVO> getLstMediaItemCustomer() {
		return lstMediaItemCustomer;
	}
	public void setLstMediaItemCustomer(
			List<MediaItemCustomerVO> lstMediaItemCustomer) {
		this.lstMediaItemCustomer = lstMediaItemCustomer;
	}
	
}

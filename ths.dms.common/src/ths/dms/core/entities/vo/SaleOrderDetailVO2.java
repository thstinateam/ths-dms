/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VO sale_order_detail de xuat xml
 * 
 * @author lacnv1
 * @since Mar 14, 2015
 */
public class SaleOrderDetailVO2 implements Serializable {

	private static final long serialVersionUID = 1L;

	private BigDecimal discount;
	private BigDecimal discountPercent;
	private Integer isFreeItem;
	private String productCode;
	private BigDecimal amount;
	private String programCode;
	private Integer quantity;
	private String reasonCode;
	private BigDecimal price;
	private String uom1;
	private String uom2;
	
	public BigDecimal getDiscount() {
		return discount;
	}
	
	public BigDecimal getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(BigDecimal discountPercent) {
		this.discountPercent = discountPercent;
	}

	public Integer getIsFreeItem() {
		return isFreeItem;
	}

	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getUom1() {
		return uom1;
	}

	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	public String getUom2() {
		return uom2;
	}

	public void setUom2(String uom2) {
		this.uom2 = uom2;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
}
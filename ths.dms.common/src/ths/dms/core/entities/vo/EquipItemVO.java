package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.management.loading.PrivateClassLoader;

/**
 * Class EquipItemVO
 * 
 * @author phuongvm
 * @since 05/01/2015

 */
public class EquipItemVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id; 
	private String code; 
	private String name; 
	private Integer repairCount;  // so lan sua chua
	/**vuongmq; 06/04/2015*/
	private Integer type;
	private Integer warranty;
	private BigDecimal fromWorkerPrice;
	private BigDecimal toWorkerPrice;
	private BigDecimal fromMaterialPrice;
	private BigDecimal toMaterialPrice;
	private BigDecimal fromCapacity; // dung tich tu /** truong hop cap nhat pheiu khong can*/
	private BigDecimal toCapacity; // dung tich den /** truong hop cap nhat pheiu khong can*/
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getRepairCount() {
		return repairCount;
	}
	public void setRepairCount(Integer repairCount) {
		this.repairCount = repairCount;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getWarranty() {
		return warranty;
	}
	public void setWarranty(Integer warranty) {
		this.warranty = warranty;
	}
	public BigDecimal getFromWorkerPrice() {
		return fromWorkerPrice;
	}
	public void setFromWorkerPrice(BigDecimal fromWorkerPrice) {
		this.fromWorkerPrice = fromWorkerPrice;
	}
	public BigDecimal getToWorkerPrice() {
		return toWorkerPrice;
	}
	public void setToWorkerPrice(BigDecimal toWorkerPrice) {
		this.toWorkerPrice = toWorkerPrice;
	}
	public BigDecimal getFromMaterialPrice() {
		return fromMaterialPrice;
	}
	public void setFromMaterialPrice(BigDecimal fromMaterialPrice) {
		this.fromMaterialPrice = fromMaterialPrice;
	}
	public BigDecimal getToMaterialPrice() {
		return toMaterialPrice;
	}
	public void setToMaterialPrice(BigDecimal toMaterialPrice) {
		this.toMaterialPrice = toMaterialPrice;
	}
	public BigDecimal getFromCapacity() {
		return fromCapacity;
	}
	public void setFromCapacity(BigDecimal fromCapacity) {
		this.fromCapacity = fromCapacity;
	}
	public BigDecimal getToCapacity() {
		return toCapacity;
	}
	public void setToCapacity(BigDecimal toCapacity) {
		this.toCapacity = toCapacity;
	}
	
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;


/**
 * Mo ta class ShopParamVODayStart.java
 * @author vuongmq
 * @since Nov 30, 2015
 */
public class ShopParamVODayStart implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private String valueDateStr;
	private String valueTimeStr;
	
	public String getValueDateStr() {
		return valueDateStr;
	}
	public void setValueDateStr(String valueDateStr) {
		this.valueDateStr = valueDateStr;
	}
	public String getValueTimeStr() {
		return valueTimeStr;
	}
	public void setValueTimeStr(String valueTimeStr) {
		this.valueTimeStr = valueTimeStr;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class Rpt7_2_2_NganhHangVO implements Serializable {
	private String maNVBH;
	private String tenNVBH;
	private String maNganhHang;
	ArrayList<Rpt7_2_2_SanPhamVO> listRpt7_2_2_SanPhamVO = new ArrayList<Rpt7_2_2_SanPhamVO>();
	private BigDecimal tongSL;
	private BigDecimal tongTien;
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getMaNganhHang() {
		return maNganhHang;
	}
	public void setMaNganhHang(String maNganhHang) {
		this.maNganhHang = maNganhHang;
	}
	public ArrayList<Rpt7_2_2_SanPhamVO> getListRpt7_2_2_SanPhamVO() {
		return listRpt7_2_2_SanPhamVO;
	}
	public void setListRpt7_2_2_SanPhamVO(
			ArrayList<Rpt7_2_2_SanPhamVO> listRpt7_2_2_SanPhamVO) {
		this.listRpt7_2_2_SanPhamVO = listRpt7_2_2_SanPhamVO;
	}
	public BigDecimal getTongSL() {
		return tongSL;
	}
	public void setTongSL(BigDecimal tongSL) {
		this.tongSL = tongSL;
	}
	public BigDecimal getTongTien() {
		return tongTien;
	}
	public void setTongTien(BigDecimal tongTien) {
		this.tongTien = tongTien;
	}
	
	
}

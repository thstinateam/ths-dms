package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.enumtype.OrderType;


public class SaleOrderVO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long saleOrderId;//saleOrderId ma don hang
	private Long customerId;//customerId id khach hang
	private Long deliveryId;//deliveryId id nhan vien giao hang
	private Long staffId;//staffId id nhan vien ban hang
	private Long payReceivedId;
	private Long shopId;

	private BigDecimal total;// tong tien
	private BigDecimal amount;// tong tien chua chiet khau
	private BigDecimal discount;// tong tien chiet khau
	private BigDecimal remain;
	
	private Integer approved;
	private Integer isInvoice;// la hoa don tai chinh
	
	private Date orderDate;//orderDate ngay tao don hang
	private Date payDate;//ngay thanh toan bang pay_received
	
	private String shortCode;
	private String customerCode;//customerCode ma khach hang
	private String customerName;//customerName ten khach hang
	private String customerAddress;//dia chi khach hang
	private String customerPhone;//so dien thoai khach hang
	private String orderNumber;//orderNumber ma don hang
	private String staffCode;//staffCode ma nhan vien ban hang
	private String staffName;//staffCode ma nhan vien ban hang
	private String deliveryCode;//deliveryCode ma nhan vien giao hang
	private String deliveryName;//deliveryCode ma nhan vien giao hang
	private String orderType;
	private String fromSaleOrderCode;
	private String refOrderNumber;
	private String shopCode;
	private String address;
	private String payReceivedNumber;
	private String orderDateStr;//orderDate ngay tao don hang convert Chuoi
	private String isJoinOderNumber;
	private String deliveryDateStr;
	private String promoCode;
	private String invoiceNumber;
	private String priorityStr;
	
	//Danh sach so suat cua KM sp
	public List<SaleOrderPromotion> productQuantityReceivedList = new ArrayList<SaleOrderPromotion>();
	
	//Danh sach so suat cua KM don hang
	public List<SaleOrderPromotion> orderQuantityReceivedList = new ArrayList<SaleOrderPromotion>();
	
	public List<SaleOrderPromotion> getProductQuantityReceivedList() {
		return productQuantityReceivedList;
	}
	public void setProductQuantityReceivedList(
			List<SaleOrderPromotion> productQuantityReceivedList) {
		this.productQuantityReceivedList = productQuantityReceivedList;
	}
	public List<SaleOrderPromotion> getOrderQuantityReceivedList() {
		return orderQuantityReceivedList;
	}
	public void setOrderQuantityReceivedList(
			List<SaleOrderPromotion> orderQuantityReceivedList) {
		this.orderQuantityReceivedList = orderQuantityReceivedList;
	}
	public Date getPayDate() {
		return payDate;
	}
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	public String getIsJoinOderNumber() {
		return isJoinOderNumber;
	}
	public void setIsJoinOderNumber(String isJoinOderNumber) {
		this.isJoinOderNumber = isJoinOderNumber;
	}
	public String getOrderDateStr() {
		return orderDateStr;
	}
	public void setOrderDateStr(String orderDateStr) {
		this.orderDateStr = orderDateStr;
	}
	public Long getPayReceivedId() {
		return payReceivedId;
	}
	public void setPayReceivedId(Long payReceivedId) {
		this.payReceivedId = payReceivedId;
	}
	public String getRefOrderNumber() {
		return refOrderNumber;
	}
	public void setRefOrderNumber(String refOrderNumber) {
		this.refOrderNumber = refOrderNumber;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPayReceivedNumber() {
		return payReceivedNumber;
	}
	public void setPayReceivedNumber(String payReceivedNumber) {
		this.payReceivedNumber = payReceivedNumber;
	}
	/**
	 * @return the saleOrderId
	 */
	public Long getSaleOrderId() {
		return saleOrderId;
	}
	/**
	 * @param saleOrderId the saleOrderId to set
	 */
	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}
	/**
	 * @return the orderDate
	 */
	public Date getOrderDate() {
		return orderDate;
	}
	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return the customerCode
	 */
	public String getCustomerCode() {
		return customerCode;
	}
	/**
	 * @param customerCode the customerCode to set
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}
	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}
	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	/**
	 * @return the staffId
	 */
	public Long getStaffId() {
		return staffId;
	}
	/**
	 * @param staffId the staffId to set
	 */
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	/**
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}
	/**
	 * @param staffCode the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	/**
	 * @return the deliveryId
	 */
	public Long getDeliveryId() {
		return deliveryId;
	}
	/**
	 * @param deliveryId the deliveryId to set
	 */
	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}
	/**
	 * @return the deliveryCode
	 */
	public String getDeliveryCode() {
		return deliveryCode;
	}
	/**
	 * @param deliveryCode the deliveryCode to set
	 */
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public OrderType getOrderType() {
		return OrderType.parseValue(orderType);
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getFromSaleOrderCode() {
		return fromSaleOrderCode;
	}
	public void setFromSaleOrderCode(String fromSaleOrderCode) {
		this.fromSaleOrderCode = fromSaleOrderCode;
	}
	public Integer getApproved() {
		return approved;
	}
	public void setApproved(Integer approved) {
		this.approved = approved;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public Integer getIsInvoice() {
		return isInvoice;
	}
	public void setIsInvoice(Integer isInvoice) {
		this.isInvoice = isInvoice;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public BigDecimal getRemain() {
		return remain;
	}
	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}
	public String getDeliveryDateStr() {
		return deliveryDateStr;
	}
	public void setDeliveryDateStr(String deliveryDateStr) {
		this.deliveryDateStr = deliveryDateStr;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getPriorityStr() {
		return priorityStr;
	}
	public void setPriorityStr(String priorityStr) {
		this.priorityStr = priorityStr;
	}
}
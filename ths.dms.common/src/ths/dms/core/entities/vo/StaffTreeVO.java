package ths.dms.core.entities.vo;

import java.io.Serializable;

import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.OrganizationNodeType;

public class StaffTreeVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private ShopTreeVO shop;
	private String staffCode;
	private String staffName;
	private StaffType staffType;
	private ActiveType status;
	
	private OrganizationNodeType nodeType;// nodeType cay to chuc
	private Long typeId;
	private Integer nodeOrdinal; //sap sep
	private Long organizationId;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
	public ShopTreeVO getShop() {
		return shop;
	}
	public void setShop(ShopTreeVO shop) {
		this.shop = shop;
	}
	public StaffType getStaffType() {
		return staffType;
	}
	public void setStaffType(StaffType staffType) {
		this.staffType = staffType;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public OrganizationNodeType getNodeType() {
		return nodeType;
	}
	public void setNodeType(OrganizationNodeType nodeType) {
		this.nodeType = nodeType;
	}
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public Integer getNodeOrdinal() {
		return nodeOrdinal;
	}
	public void setNodeOrdinal(Integer nodeOrdinal) {
		this.nodeOrdinal = nodeOrdinal;
	}
	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
	
	
}

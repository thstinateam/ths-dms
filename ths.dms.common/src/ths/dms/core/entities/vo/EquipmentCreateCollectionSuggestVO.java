package ths.dms.core.entities.vo;

import java.io.Serializable;

public class EquipmentCreateCollectionSuggestVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mien;
	private String kenh;
	private String NPP;
	private String maKhachHang;
	private String tenKhachHang;
	private String maThietBi;
	private String soSeri;
	private String thoiGian;
	private String nguoiDungTen;
	private String dienThoai;
	private String soNha;
	private String tinh;
	private String huyen;
	private String xa;
	private String duong;
	private String soLuong;
	private String nhomThietBi;
	private String tinhTrang;
	private String giamSat;
	private String dtGiamSat;
	
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getKenh() {
		return kenh;
	}
	public void setKenh(String kenh) {
		this.kenh = kenh;
	}
	public String getNPP() {
		return NPP;
	}
	public void setNPP(String nPP) {
		NPP = nPP;
	}
	public String getMaKhachHang() {
		return maKhachHang;
	}
	public void setMaKhachHang(String maKhachHang) {
		this.maKhachHang = maKhachHang;
	}
	public String getTenKhachHang() {
		return tenKhachHang;
	}
	public void setTenKhachHang(String tenKhachHang) {
		this.tenKhachHang = tenKhachHang;
	}
	public String getMaThietBi() {
		return maThietBi;
	}
	public void setMaThietBi(String maThietBi) {
		this.maThietBi = maThietBi;
	}
	public String getSoSeri() {
		return soSeri;
	}
	public void setSoSeri(String soSeri) {
		this.soSeri = soSeri;
	}
	public String getThoiGian() {
		return thoiGian;
	}
	public void setThoiGian(String thoiGian) {
		this.thoiGian = thoiGian;
	}
	public String getNguoiDungTen() {
		return nguoiDungTen;
	}
	public void setNguoiDungTen(String nguoiDungTen) {
		this.nguoiDungTen = nguoiDungTen;
	}
	public String getDienThoai() {
		return dienThoai;
	}
	public void setDienThoai(String dienThoai) {
		this.dienThoai = dienThoai;
	}
	public String getSoNha() {
		return soNha;
	}
	public void setSoNha(String soNha) {
		this.soNha = soNha;
	}
	public String getTinh() {
		return tinh;
	}
	public void setTinh(String tinh) {
		this.tinh = tinh;
	}
	public String getHuyen() {
		return huyen;
	}
	public void setHuyen(String huyen) {
		this.huyen = huyen;
	}
	public String getXa() {
		return xa;
	}
	public void setXa(String xa) {
		this.xa = xa;
	}
	public String getDuong() {
		return duong;
	}
	public void setDuong(String duong) {
		this.duong = duong;
	}
	public String getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(String soLuong) {
		this.soLuong = soLuong;
	}
	public String getNhomThietBi() {
		return nhomThietBi;
	}
	public void setNhomThietBi(String nhomThietBi) {
		this.nhomThietBi = nhomThietBi;
	}
	public String getTinhTrang() {
		return tinhTrang;
	}
	public void setTinhTrang(String tinhTrang) {
		this.tinhTrang = tinhTrang;
	}
	public String getGiamSat() {
		return giamSat;
	}
	public void setGiamSat(String giamSat) {
		this.giamSat = giamSat;
	}
	public String getDtGiamSat() {
		return dtGiamSat;
	}
	public void setDtGiamSat(String dtGiamSat) {
		this.dtGiamSat = dtGiamSat;
	}
	
}

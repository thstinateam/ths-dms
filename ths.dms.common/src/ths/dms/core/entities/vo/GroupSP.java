package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class GroupSP implements Serializable {
	/** field serialVersionUID  field long */
	private static final long serialVersionUID = 1L;
	public long index;
	public List<Node> lstSP;
	public List<Node> lstSPRef;
	public int order;
	public GroupSP() {
		lstSP = new ArrayList<Node>();
		lstSPRef = new ArrayList<Node>();
	}
	public GroupSP(GroupSP g){
		this.index=g.index;
		this.order=g.order;
		lstSP = new ArrayList<Node>();
		if(g.lstSP!=null){
			for(Node n:g.lstSP){
				if(n!=null){
					if(n instanceof NodeSP){
						lstSP.add(new NodeSP(n));
					}else if(n instanceof NodeAmount){
						lstSP.add(new NodeAmount(n));
					}else{
						lstSP.add(new NodePercent(n));
					}
				}
			}
		}
		lstSPRef = new ArrayList<Node>();
		if(g.lstSPRef!=null){
			for(Node n:g.lstSPRef){
				if(n!=null){
					if(n instanceof NodeSP){
						lstSPRef.add(new NodeSP(n));
					}else if(n instanceof NodeAmount){
						lstSPRef.add(new NodeAmount(n));
					}else{
						lstSPRef.add(new NodePercent(n));
					}
				}
			}
		}
	}
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof GroupSP) || obj == null) {
			return false;
		}
		GroupSP __groupSP = (GroupSP) obj;
		if(__groupSP.lstSP.size() != this.lstSP.size()) {
			return false;
		}
		for(int i = 0; i < __groupSP.lstSP.size(); i++) {
			if(
				(__groupSP.lstSP.get(i).productCode != null && !__groupSP.lstSP.get(i).productCode.equals("") && this.lstSP.get(i).productCode != null && !this.lstSP.get(i).productCode.equals("") && !__groupSP.lstSP.get(i).productCode.equals(this.lstSP.get(i).productCode))
				||
				(__groupSP.lstSP.get(i).quantity != null && this.lstSP.get(i).quantity != null && __groupSP.lstSP.get(i).quantity != this.lstSP.get(i).quantity)
				||
				(__groupSP.lstSP.get(i).amount != null && this.lstSP.get(i).amount != null && !__groupSP.lstSP.get(i).amount.equals(this.lstSP.get(i).amount))
				||
				(__groupSP.lstSP.get(i).percent != null && this.lstSP.get(i).percent != null && __groupSP.lstSP.get(i).percent != this.lstSP.get(i).percent)
				||
				(__groupSP.lstSP.get(i).isRequired != null && this.lstSP.get(i).isRequired != null && __groupSP.lstSP.get(i).isRequired != this.lstSP.get(i).isRequired) 
			) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @author vuongmq
	 * @since 17/02/2016
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	public boolean add2Level(String productCode, Integer quantity, Boolean andOr) {
		if(lstSP.isEmpty()) {
			NodeSP node = new NodeSP(productCode, quantity, andOr);
			lstSP.add(node);
			return true;
		} else {
			if(lstSP.get(lstSP.size() - 1) instanceof NodeSP) {
				NodeSP node = new NodeSP(productCode, quantity, andOr);
				lstSP.add(node);
				return true;
			} else {
				return false;
			}
		}
	}
	public boolean add2Level(String productCode, Integer quantity) {
		if(lstSP.isEmpty()) {
			NodeSP node = new NodeSP(productCode, quantity);
			lstSP.add(node);
			return true;
		} else {
			if(lstSP.get(lstSP.size() - 1) instanceof NodeSP) {
				NodeSP node = new NodeSP(productCode, quantity);
				lstSP.add(node);
				return true;
			} else {
				return false;
			}
		}
	}
	public boolean add2Level(String productCode, BigDecimal amount, Boolean andOr) {
		if(lstSP.isEmpty()) {
			NodeSP node = new NodeSP(productCode, amount, andOr);
			lstSP.add(node);
			return true;
		} else {
			if(lstSP.get(lstSP.size() - 1) instanceof NodeSP) {
				NodeSP node = new NodeSP(productCode, amount, andOr);
				lstSP.add(node);
				return true;
			} else {
				return false;
			}
		}
	}
	public boolean add2Level(String productCode, BigDecimal amount) {
		if(lstSP.isEmpty()) {
			NodeSP node = new NodeSP(productCode, amount);
			lstSP.add(node);
			return true;
		} else {
			if(lstSP.get(lstSP.size() - 1) instanceof NodeSP) {
				NodeSP node = new NodeSP(productCode, amount);
				lstSP.add(node);
				return true;
			} else {
				return false;
			}
		}
	}
	public boolean add2Level(BigDecimal amount) {
		if(lstSP.isEmpty()) {
			NodeAmount node = new NodeAmount(amount);
			lstSP.add(node);
			return true;
		} else {
			if(lstSP.get(lstSP.size() - 1) instanceof NodeSP) {
				NodeAmount node = new NodeAmount(amount);
				lstSP.add(node);
				return true;
			} else {
				return false;
			}
		}
	}
	public boolean add2Level(Float percent) {
		if(lstSP.isEmpty()) {
			NodePercent node = new NodePercent(percent);
			lstSP.add(node);
			return true;
		} else {
			if(lstSP.get(lstSP.size() - 1) instanceof NodeSP) {
				NodePercent node = new NodePercent(percent);
				lstSP.add(node);
				return true;
			} else {
				return false;
			}
		}
	}
}
/*
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * 
 * VO cho bang EquipStatisticRecordDetail
 * @author trietptm
 * @since Mar 11, 2016
 */
public class EquipStatisticRecordDetailVO implements Serializable {
	/** field serialVersionUID  field long */
	private static final long serialVersionUID = -2665138724404164421L;
	
	private Integer statisticTime;
	private Integer status;
	private String statisticDate;
	
	public Integer getStatisticTime() {
		return statisticTime;
	}
	public void setStatisticTime(Integer statisticTime) {
		this.statisticTime = statisticTime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getStatisticDate() {
		return statisticDate;
	}
	public void setStatisticDate(String statisticDate) {
		this.statisticDate = statisticDate;
	}
	
}

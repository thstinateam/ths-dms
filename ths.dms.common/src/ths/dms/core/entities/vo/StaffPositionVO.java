package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.httpclient.util.DateUtil;

public class StaffPositionVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long staffId;
	private String staffCode;
	private String staffName;
	private Long staffOwnerId;
	private String staffOwnerCode;
	private String staffOwnerName;
	private String lstStaffOwnerIdStr;
	private Float lat;
	private Float lng;
	private Long shopId;
	private String shopCode;
	private String shopName;
	private Integer roleType;
	private Integer countVisit;
	private Float accuracy;
	private Date lastDatePosition;
	private String strLastDatePosition;
	private Date createTime;
	private Integer countStaff;
	private String hhmm;
	private Boolean isBold;
	private String text;//Text for node
	
	private Integer isHaveTraining;
	private String lstParentShopId;
	
	private String iconUrl;
	private Integer status;
	
	
	public String getLstStaffOwnerIdStr() {
		return lstStaffOwnerIdStr;
	}
	public void setLstStaffOwnerIdStr(String lstStaffOwnerIdStr) {
		this.lstStaffOwnerIdStr = lstStaffOwnerIdStr;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
	public Float getLat() {
		return lat;
	}
	public void setLat(Float lat) {
		this.lat = lat;
	}
	public Float getLng() {
		return lng;
	}
	public void setLng(Float lng) {
		this.lng = lng;
	}
	public Float getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(Float accuracy) {
		this.accuracy = accuracy;
	}
	/**
	 * @return the lastDatePosition
	 */
	public Date getLastDatePosition() {
		return lastDatePosition;
	}
	/**
	 * @param lastDatePosition the lastDatePosition to set
	 */
	public void setLastDatePosition(Date lastDatePosition) {
		this.lastDatePosition = lastDatePosition;
		if(lastDatePosition!=null){
			this.strLastDatePosition = DateUtil.formatDate(lastDatePosition, "dd/MM/yyyy hh:mm:ss a");
		}
	}
	public String getStrLastDatePosition() {
		return strLastDatePosition;
	}
	public void setStrLastDatePosition(String strLastDatePosition) {
		this.strLastDatePosition = strLastDatePosition;
	}
	public Long getStaffOwnerId() {
		return staffOwnerId;
	}
	public void setStaffOwnerId(Long staffOwnerId) {
		this.staffOwnerId = staffOwnerId;
	}
	public String getStaffOwnerCode() {
		return staffOwnerCode;
	}
	public void setStaffOwnerCode(String staffOwnerCode) {
		this.staffOwnerCode = staffOwnerCode;
	}
	public String getStaffOwnerName() {
		return staffOwnerName;
	}
	public void setStaffOwnerName(String staffOwnerName) {
		this.staffOwnerName = staffOwnerName;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public Integer getRoleType() {
		return roleType;
	}
	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}
	public Integer getCountVisit() {
		return countVisit;
	}
	public void setCountVisit(Integer countVisit) {
		this.countVisit = countVisit;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getCountStaff() {
		return countStaff;
	}
	public void setCountStaff(Integer countStaff) {
		this.countStaff = countStaff;
	}
	public String getHhmm() {
		return hhmm;
	}
	public void setHhmm(String hhmm) {
		this.hhmm = hhmm;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopName() {
		return shopName;
	}
	public void setIsBold(Boolean isBold) {
		this.isBold = isBold;
	}
	public Boolean getIsBold() {
		return isBold;
	}
	public Integer getIsHaveTraining() {
		return isHaveTraining;
	}
	public void setIsHaveTraining(Integer isHaveTraining) {
		this.isHaveTraining = isHaveTraining;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getLstParentShopId() {
		return lstParentShopId;
	}
	public void setLstParentShopId(String lstParentShopId) {
		this.lstParentShopId = lstParentShopId;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
}

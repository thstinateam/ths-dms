package ths.dms.core.entities.vo;

import java.io.Serializable;
/**
 * Lop VO cho cac truong lien quan den kho thiet bi
 * 
 * @author hunglm16
 * @since May 17, 2015
 * @description Anh huong truc tiep den combobox chon kho
 */
public class EquipStockVO implements Serializable{

	private static final long serialVersionUID = -5812714897269748239L;
	private Long shopId;
	private Long staffId;
	private Long equipStockId;
	private Long id;

	private String shopCode;
	private String shopName;
	private String customerCode;
	private String customerName;
	private String address;
	private String stockCode;
	private String stockName;
	private String code;
	private String name;
	private String equipStockCode;
	private String equipStockName;
	
	private Integer equipStockOrdinal;
	private Integer equipStockStatus;
	private Integer isUnder;

	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */

	public String getEquipStockCode() {
		return equipStockCode;
	}

	public Integer getIsUnder() {
		return isUnder;
	}

	public void setIsUnder(Integer isUnder) {
		this.isUnder = isUnder;
	}

	public Long getEquipStockId() {
		return equipStockId;
	}

	public void setEquipStockId(Long equipStockId) {
		this.equipStockId = equipStockId;
	}

	public void setEquipStockCode(String equipStockCode) {
		this.equipStockCode = equipStockCode;
	}

	public String getEquipStockName() {
		return equipStockName;
	}

	public void setEquipStockName(String equipStockName) {
		this.equipStockName = equipStockName;
	}

	public Integer getEquipStockOrdinal() {
		return equipStockOrdinal;
	}

	public void setEquipStockOrdinal(Integer equipStockOrdinal) {
		this.equipStockOrdinal = equipStockOrdinal;
	}

	public Integer getEquipStockStatus() {
		return equipStockStatus;
	}

	public void setEquipStockStatus(Integer equipStockStatus) {
		this.equipStockStatus = equipStockStatus;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
}

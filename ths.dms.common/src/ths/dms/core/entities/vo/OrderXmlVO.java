/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * VO map sale_order de xuat ra file xml
 * 
 * @author lacnv1
 * @since Mar 14, 2015
 */
public class OrderXmlVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private OrderXmlHeaderVO header;
	private OrderXmlDetailVO detail;
	private long orderId;
	
	public OrderXmlHeaderVO getHeader() {
		return header;
	}

	public void setHeader(OrderXmlHeaderVO hedeader) {
		this.header = hedeader;
	}
	
	public OrderXmlDetailVO getDetail() {
		return detail;
	}

	public void setDetail(OrderXmlDetailVO detail) {
		this.detail = detail;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public String getFileName() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String dateString = sdf.format(date);
		String milis = sdf.format(date);
		return "Exp_SalesOrder_" + this.getHeader().getDistCode() + "_" + dateString + "" + milis + ".xml";
	}

	public String getShopCode() {
		return this.getHeader().getDistCode();
	}
}
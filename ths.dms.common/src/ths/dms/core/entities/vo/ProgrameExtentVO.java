package ths.dms.core.entities.vo;

import java.io.Serializable;

public class ProgrameExtentVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long shopId;

	private Integer isNPP;

	private Integer isJoin;
	
	private Integer isChildJoin;

	private Long programShopMapId;
	
	private Long id;
	
	private String updateDate;
	
	
	

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getIsNPP() {
		return isNPP;
	}

	public void setIsNPP(Integer isNPP) {
		this.isNPP = isNPP;
	}

	public Integer getIsJoin() {
		return isJoin;
	}

	public void setIsJoin(Integer isJoin) {
		this.isJoin = isJoin;
	}

	public Long getProgramShopMapId() {
		return programShopMapId;
	}

	public void setProgramShopMapId(Long programShopMapId) {
		this.programShopMapId = programShopMapId;
	}

	public Integer getIsChildJoin() {
		return isChildJoin;
	}

	public void setIsChildJoin(Integer isChildJoin) {
		this.isChildJoin = isChildJoin;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class Node implements Serializable {
		public String productCode;
		public Integer quantity;
		public Boolean isRequired;
		public BigDecimal amount;
		public Float percent;
		public Node clone(){
			Node n=new Node();
			n.productCode=productCode;
			n.quantity=quantity;
			n.isRequired=isRequired;
			n.amount=amount;
			n.percent=percent;
			return n;
		}
	}
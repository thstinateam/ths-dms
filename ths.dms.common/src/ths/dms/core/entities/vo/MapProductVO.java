/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Dung cho cac du lieu lien quan den san pham
 * @author hunglm16
 * @since 11/11/2015
 */
public class MapProductVO implements Serializable {

	private static final long serialVersionUID = 4038640759201532433L;
	
	private Long id;
	private Long productId;
	private Long cycleCountId;
	private Long cycleCountMapProductId;
	
	private String uom1;
	private String code;
	private String name;
	private String productCode;
	private String productName;
	private String dateInventoryStr;
	private String countDateStr;
	
	private Integer statusCycleCount;
	private Integer checkLot;
	private Integer convfact;
	private Integer stockCardNumber;
	private Integer quantityCounted;
	private Integer quantityBeforeCount;
	private Integer flag;
	private Integer stt;
	
	private Date countDate;

	/***
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * @return
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getCycleCountId() {
		return cycleCountId;
	}

	public void setCycleCountId(Long cycleCountId) {
		this.cycleCountId = cycleCountId;
	}

	public Long getCycleCountMapProductId() {
		return cycleCountMapProductId;
	}

	public void setCycleCountMapProductId(Long cycleCountMapProductId) {
		this.cycleCountMapProductId = cycleCountMapProductId;
	}

	public String getUom1() {
		return uom1;
	}

	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDateInventoryStr() {
		return dateInventoryStr;
	}

	public void setDateInventoryStr(String dateInventoryStr) {
		this.dateInventoryStr = dateInventoryStr;
	}

	public Integer getStatusCycleCount() {
		return statusCycleCount;
	}

	public void setStatusCycleCount(Integer statusCycleCount) {
		this.statusCycleCount = statusCycleCount;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Integer getStockCardNumber() {
		return stockCardNumber;
	}

	public void setStockCardNumber(Integer stockCardNumber) {
		this.stockCardNumber = stockCardNumber;
	}

	public Integer getQuantityCounted() {
		return quantityCounted;
	}

	public void setQuantityCounted(Integer quantityCounted) {
		this.quantityCounted = quantityCounted;
	}

	public Integer getQuantityBeforeCount() {
		return quantityBeforeCount;
	}

	public void setQuantityBeforeCount(Integer quantityBeforeCount) {
		this.quantityBeforeCount = quantityBeforeCount;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Integer getStt() {
		return stt;
	}

	public void setStt(Integer stt) {
		this.stt = stt;
	}

	public Date getCountDate() {
		return countDate;
	}

	public void setCountDate(Date countDate) {
		this.countDate = countDate;
	}

	public String getCountDateStr() {
		return countDateStr;
	}

	public void setCountDateStr(String countDateStr) {
		this.countDateStr = countDateStr;
	}

}

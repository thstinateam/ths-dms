package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * VO cho in bien ban giao nhan
 * 
 * @author tamvnm
 * @sine 22/04/2015
 */
/**
 * @author tamvnm
 *
 */
/**
 * @author tamvnm
 *
 */
public class EquipmentDeliveryPrintVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**id bien ban giao nhan*/
	private Long id;
	/** ten mien*/
	private String mien;
	/** ten kenh */
	private String kenh;
	/** ma khach hang*/
	private String customerCode;
	/** ten khach hang*/
	private String customerName;
	/** nguoi dai dien*/
	private String representative;
	/** so dien thoai*/
	private String toPhone;
	/** so nha*/
	private String houseNumber;
	/** duong*/
	private String street;
	/** phuong*/
	private String ward;
	/** quan*/
	private String district;
	/** tp*/
	private String city;
	/** so luong thiet bi*/
	private Integer numberEquip;
	/**trinh trang thiet bi*/
	private String healthStatus;
	/** nhom thiet bi*/
	private String equipGroup;
	/** loai  thiet bi*/
	private String equipCategory;
	/** thoi gian mong muon nhan tu*/
	private String thoiGianNhan;
	/** so seri thiet bi*/
	private String seri;
	/** ma giam sat*/
	private String staffName;
	/** sdt giam sat */
	private String staffPhone;
	/** shop npp khach hang*/
	private Long shopId;
	/** ten shop npp khach hang*/
	private String shopName;
	/** ma shop npp khach hang*/
	private String shopCode;

	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getKenh() {
		return kenh;
	}

	public void setKenh(String kenh) {
		this.kenh = kenh;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getRepresentative() {
		return representative;
	}

	public void setRepresentative(String representative) {
		this.representative = representative;
	}

	public String getToPhone() {
		return toPhone;
	}

	public void setToPhone(String toPhone) {
		this.toPhone = toPhone;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getNumberEquip() {
		return numberEquip;
	}

	public void setNumberEquip(Integer numberEquip) {
		this.numberEquip = numberEquip;
	}

	public String getEquipGroup() {
		return equipGroup;
	}

	public void setEquipGroup(String equipGroup) {
		this.equipGroup = equipGroup;
	}

	public String getEquipCategory() {
		return equipCategory;
	}

	public void setEquipCategory(String equipCategory) {
		this.equipCategory = equipCategory;
	}

	public String getThoiGianNhan() {
		return thoiGianNhan;
	}

	public void setThoiGianNhan(String thoiGianNhan) {
		this.thoiGianNhan = thoiGianNhan;
	}

	public String getSeri() {
		return seri;
	}

	public void setSeri(String seri) {
		this.seri = seri;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffPhone() {
		return staffPhone;
	}

	public void setStaffPhone(String staffPhone) {
		this.staffPhone = staffPhone;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class SubLevelMapping implements Serializable {
	Long levelId;
	Long levelDetailId;
	Long productId;
	String productCode;
	String productName;
	Integer quantity;
	BigDecimal amount;
	Integer isRequired;
	public Long getLevelId() {
		return levelId;
	}
	public void setLevelId(Long levelId) {
		this.levelId = levelId;
	}
	public Long getLevelDetailId() {
		return levelDetailId;
	}
	public void setLevelDetailId(Long levelDetailId) {
		this.levelDetailId = levelDetailId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Integer getIsRequired() {
		return isRequired;
	}
	public void setIsRequired(Integer isRequired) {
		this.isRequired = isRequired;
	}
}

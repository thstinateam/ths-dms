package ths.dms.core.entities.vo;

import java.io.Serializable;

public class EquipmentCreateLendSuggestVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String loaiThietBi;
	private String dungTich;
	private String khachHang;
	private String tenKhachHang;
	private String ngDungTenMuonTu;
	private String quanHe;
	private String dienThoai;
	private String soNha;
	private String tinh;
	private String huyen;
	private String xa;
	private String duong;
	private String soCMND;
	private String noiCapCMND;
	private String ngayCapCMND;
	private String giayPhepDKKD;
	private String noiCapDKKD;
	private String ngayCapDKKD;
	private String thoiGianNhanTu;
	private String tinhTrang;
	private String khoXuat;
	private String doanhSoTB;
	public String getLoaiThietBi() {
		return loaiThietBi;
	}
	public void setLoaiThietBi(String loaiThietBi) {
		this.loaiThietBi = loaiThietBi;
	}
	public String getDungTich() {
		return dungTich;
	}
	public void setDungTich(String dungTich) {
		this.dungTich = dungTich;
	}
	public String getKhachHang() {
		return khachHang;
	}
	public void setKhachHang(String khachHang) {
		this.khachHang = khachHang;
	}
	public String getTenKhachHang() {
		return tenKhachHang;
	}
	public void setTenKhachHang(String tenKhachHang) {
		this.tenKhachHang = tenKhachHang;
	}
	public String getNgDungTenMuonTu() {
		return ngDungTenMuonTu;
	}
	public void setNgDungTenMuonTu(String ngDungTenMuonTu) {
		this.ngDungTenMuonTu = ngDungTenMuonTu;
	}
	public String getQuanHe() {
		return quanHe;
	}
	public void setQuanHe(String quanHe) {
		this.quanHe = quanHe;
	}
	public String getDienThoai() {
		return dienThoai;
	}
	public void setDienThoai(String dienThoai) {
		this.dienThoai = dienThoai;
	}
	public String getSoNha() {
		return soNha;
	}
	public void setSoNha(String soNha) {
		this.soNha = soNha;
	}
	public String getTinh() {
		return tinh;
	}
	public void setTinh(String tinh) {
		this.tinh = tinh;
	}
	public String getHuyen() {
		return huyen;
	}
	public void setHuyen(String huyen) {
		this.huyen = huyen;
	}
	public String getXa() {
		return xa;
	}
	public void setXa(String xa) {
		this.xa = xa;
	}
	public String getDuong() {
		return duong;
	}
	public void setDuong(String duong) {
		this.duong = duong;
	}
	public String getSoCMND() {
		return soCMND;
	}
	public void setSoCMND(String soCMND) {
		this.soCMND = soCMND;
	}
	public String getNoiCapCMND() {
		return noiCapCMND;
	}
	public void setNoiCapCMND(String noiCapCMND) {
		this.noiCapCMND = noiCapCMND;
	}
	public String getNgayCapCMND() {
		return ngayCapCMND;
	}
	public void setNgayCapCMND(String ngayCapCMND) {
		this.ngayCapCMND = ngayCapCMND;
	}
	public String getGiayPhepDKKD() {
		return giayPhepDKKD;
	}
	public void setGiayPhepDKKD(String giayPhepDKKD) {
		this.giayPhepDKKD = giayPhepDKKD;
	}
	public String getNoiCapDKKD() {
		return noiCapDKKD;
	}
	public void setNoiCapDKKD(String noiCapDKKD) {
		this.noiCapDKKD = noiCapDKKD;
	}
	public String getNgayCapDKKD() {
		return ngayCapDKKD;
	}
	public void setNgayCapDKKD(String ngayCapDKKD) {
		this.ngayCapDKKD = ngayCapDKKD;
	}
	public String getThoiGianNhanTu() {
		return thoiGianNhanTu;
	}
	public void setThoiGianNhanTu(String thoiGianNhanTu) {
		this.thoiGianNhanTu = thoiGianNhanTu;
	}
	public String getTinhTrang() {
		return tinhTrang;
	}
	public void setTinhTrang(String tinhTrang) {
		this.tinhTrang = tinhTrang;
	}
	public String getKhoXuat() {
		return khoXuat;
	}
	public void setKhoXuat(String khoXuat) {
		this.khoXuat = khoXuat;
	}
	public String getDoanhSoTB() {
		return doanhSoTB;
	}
	public void setDoanhSoTB(String doanhSoTB) {
		this.doanhSoTB = doanhSoTB;
	}
}

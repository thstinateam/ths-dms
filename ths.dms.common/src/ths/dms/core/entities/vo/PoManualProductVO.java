/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * Mo ta class PoManualProductVO.java
 * @author vuongmq
 * @since Dec 8, 2015
 */
public class PoManualProductVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private Long productId;
	private String productCode;
	private Integer quantity;
	private Integer poLineNumber;
	
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getPoLineNumber() {
		return poLineNumber;
	}
	public void setPoLineNumber(Integer poLineNumber) {
		this.poLineNumber = poLineNumber;
	}
}

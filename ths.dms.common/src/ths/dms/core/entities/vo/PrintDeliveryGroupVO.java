package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;

public class PrintDeliveryGroupVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String deliveryCode;
	private String deliveryName;
	private String customerShortCode;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String orderNumber;
	private ArrayList<PrintDeliveryGroupVO1> lstDeliveryGroup = new ArrayList<PrintDeliveryGroupVO1>() {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5079881679489746422L;

		@Override
		public boolean add(PrintDeliveryGroupVO1 e) {
			int index = this.indexOf(e);
			if(index >= 0) {
				PrintDeliveryGroupVO1 existObj = this.get(index);
				if(existObj.getQuantity() != null && existObj.getAmount() != null && e.getQuantity() != null && e.getAmount() != null) {
					existObj.setQuantity(existObj.getQuantity() + e.getQuantity());
					existObj.setAmount(existObj.getAmount().add(e.getAmount()));
				} else if(existObj.getDiscountAmount() != null && e.getDiscountAmount() != null) {
					existObj.setDiscountAmount(existObj.getDiscountAmount().add(e.getDiscountAmount()));
				} else if(existObj.getIsFreeItem() == 0 && existObj.getQuantity() != null) {
					existObj.setQuantity(existObj.getQuantity() + e.getQuantity());
				}
				return true;
			} else {
				return super.add(e);
			}
		}

		@Override
		public int indexOf(Object o) {
			int index = -1;
			PrintDeliveryGroupVO1 castObj = null;
			try {
				castObj = (PrintDeliveryGroupVO1)o;
			} catch (Exception e) {
				return -1;
			}
			if(castObj == null) {
				return -1;
			}
			for(int i = 0; i < this.size(); i++) {
				PrintDeliveryGroupVO1 indexObj = this.get(i);
				if(indexObj.getDeliveryCode() != null && indexObj.getCustomerShortCode() != null && indexObj.getProductCode() != null
						&& indexObj.getDeliveryCode().equals(castObj.getDeliveryCode()) 
						&& indexObj.getCustomerShortCode().equals(castObj.getCustomerShortCode())
						&& indexObj.getProductCode().equals(castObj.getProductCode())) {
					index = i;
					return index;
				}
			}
			return index;
		}
		
		
	};
	private ArrayList<PrintDeliveryGroupVO1> lstDeliveryGroupFree = new ArrayList<PrintDeliveryGroupVO1>() {

		/**
		 * 
		 */
		private static final long serialVersionUID = -7803345043491372550L;

		@Override
		public boolean add(PrintDeliveryGroupVO1 e) {
			int index = this.indexOf(e);
			if(index >= 0) {
				PrintDeliveryGroupVO1 existObj = this.get(index);
				if(existObj.getQuantity() != null && existObj.getAmount() != null && e.getQuantity() != null && e.getAmount() != null) {
					existObj.setQuantity(existObj.getQuantity() + e.getQuantity());
					existObj.setAmount(existObj.getAmount().add(e.getAmount()));
				} else if(existObj.getDiscountAmount() != null && e.getDiscountAmount() != null) {
					existObj.setDiscountAmount(existObj.getDiscountAmount().add(e.getDiscountAmount()));
				} else if(existObj.getIsFreeItem() == 0 && existObj.getQuantity() != null) {
					existObj.setQuantity(existObj.getQuantity() + e.getQuantity());
				}
				return true;
			} else {
				return super.add(e);
			}
		}

		@Override
		public int indexOf(Object o) {
			int index = -1;
			PrintDeliveryGroupVO1 castObj = null;
			try {
				castObj = (PrintDeliveryGroupVO1)o;
			} catch (Exception e) {
				return -1;
			}
			if(castObj == null) {
				return -1;
			}
			for(int i = 0; i < this.size(); i++) {
				PrintDeliveryGroupVO1 indexObj = this.get(i);
				if(indexObj.getDeliveryCode() != null && indexObj.getCustomerShortCode() != null && indexObj.getProductCode() != null
						&& indexObj.getDeliveryCode().equals(castObj.getDeliveryCode()) 
						&& indexObj.getCustomerShortCode().equals(castObj.getCustomerShortCode())
						&& indexObj.getProductCode().equals(castObj.getProductCode())) {
					index = i;
					return index;
				}
			}
			return index;
		}
		
	};
	private ArrayList<PrintDeliveryGroupVO1> lstDeliveryGroupFreeAmount = new ArrayList<PrintDeliveryGroupVO1>() {

		/**
		 * 
		 */
		private static final long serialVersionUID = -2493147194817965161L;

		@Override
		public boolean add(PrintDeliveryGroupVO1 e) {
			int index = this.indexOf(e);
			if(index >= 0) {
				PrintDeliveryGroupVO1 existObj = this.get(index);
				if(existObj.getQuantity() != null && existObj.getAmount() != null && e.getQuantity() != null && e.getAmount() != null) {
					existObj.setQuantity(existObj.getQuantity() + e.getQuantity());
					existObj.setAmount(existObj.getAmount().add(e.getAmount()));
				} else if(existObj.getDiscountAmount() != null && e.getDiscountAmount() != null) {
					existObj.setDiscountAmount(existObj.getDiscountAmount().add(e.getDiscountAmount()));
				} else if(existObj.getIsFreeItem() == 0 && existObj.getQuantity() != null) {
					existObj.setQuantity(existObj.getQuantity() + e.getQuantity());
				}
				return true;
			} else {
				return super.add(e);
			}
		}

		@Override
		public int indexOf(Object o) {
			int index = -1;
			PrintDeliveryGroupVO1 castObj = null;
			try {
				castObj = (PrintDeliveryGroupVO1)o;
			} catch (Exception e) {
				return -1;
			}
			if(castObj == null) {
				return -1;
			}
			for(int i = 0; i < this.size(); i++) {
				PrintDeliveryGroupVO1 indexObj = this.get(i);
				if(indexObj.getDeliveryCode() != null && indexObj.getCustomerShortCode() != null && indexObj.getProductCode() != null
						&& indexObj.getDeliveryCode().equals(castObj.getDeliveryCode()) 
						&& indexObj.getCustomerShortCode().equals(castObj.getCustomerShortCode())
						&& indexObj.getProductCode().equals(castObj.getProductCode())) {
					index = i;
					return index;
				}
			}
			return index;
		}
		
	};

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getCustomerShortCode() {
		return customerShortCode;
	}

	public void setCustomerShortCode(String customerShortCode) {
		this.customerShortCode = customerShortCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public ArrayList<PrintDeliveryGroupVO1> getLstDeliveryGroup() {
		return lstDeliveryGroup;
	}

	public void setLstDeliveryGroup(
			ArrayList<PrintDeliveryGroupVO1> lstDeliveryGroup) {
		this.lstDeliveryGroup = lstDeliveryGroup;
	}

	public ArrayList<PrintDeliveryGroupVO1> getLstDeliveryGroupFree() {
		return lstDeliveryGroupFree;
	}

	public void setLstDeliveryGroupFree(
			ArrayList<PrintDeliveryGroupVO1> lstDeliveryGroupFree) {
		this.lstDeliveryGroupFree = lstDeliveryGroupFree;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		if(this.orderNumber != null) {
			if(this.orderNumber.indexOf(orderNumber) != -1) {
				return;
			}
		}
		if(this.orderNumber == null || "".equals(this.orderNumber)) {
			this.orderNumber = orderNumber;
		} else {
			this.orderNumber = this.orderNumber + ", " + orderNumber;
		}
	}

	public void setLstDeliveryGroupFreeAmount(ArrayList<PrintDeliveryGroupVO1> lstDeliveryGroupFreeAmount) {
		this.lstDeliveryGroupFreeAmount = lstDeliveryGroupFreeAmount;
	}

	public ArrayList<PrintDeliveryGroupVO1> getLstDeliveryGroupFreeAmount() {
		return lstDeliveryGroupFreeAmount;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;

public class PrintDeliveryCustomerGroupVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String deliveryCode;
	private String deliveryName;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String invoiceNumber;
	private String carNumber;
	private ArrayList<PrintDeliveryCustomerGroupVO1> lstDeliveryGroup = new ArrayList<PrintDeliveryCustomerGroupVO1>();
	private ArrayList<PrintDeliveryCustomerGroupVO1> lstDeliveryGroupFree = new ArrayList<PrintDeliveryCustomerGroupVO1>();

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public ArrayList<PrintDeliveryCustomerGroupVO1> getLstDeliveryGroup() {
		return lstDeliveryGroup;
	}

	public void setLstDeliveryGroup(
			ArrayList<PrintDeliveryCustomerGroupVO1> lstDeliveryGroup) {
		this.lstDeliveryGroup = lstDeliveryGroup;
	}

	public ArrayList<PrintDeliveryCustomerGroupVO1> getLstDeliveryGroupFree() {
		return lstDeliveryGroupFree;
	}

	public void setLstDeliveryGroupFree(
			ArrayList<PrintDeliveryCustomerGroupVO1> lstDeliveryGroupFree) {
		this.lstDeliveryGroupFree = lstDeliveryGroupFree;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * @return the carNumber
	 */
	public String getCarNumber() {
		return carNumber;
	}

	/**
	 * @param carNumber the carNumber to set
	 */
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

}

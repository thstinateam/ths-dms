package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Date;

public class CustomerDebitVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long debitId;
	private String shortCode;
	private String customerName;
	private Long orderId;
	private String orderNumber;
	private Date orderDate;
	private BigDecimal total;
	private BigDecimal totalPay;
	private BigDecimal remain;
	private BigDecimal discountAmount;
	private BigDecimal payAmt;
	private BigDecimal discountAmt;
	
	private String nvttCodeName;
	private String nvghCodeName;
	private String nvbhCodeName;
	private String nvttName;
	private String nvghName;
	private String nvbhName;
	private String bankCode;
	
	private String orderDateStr;

	public Long getDebitId() {
		return debitId;
	}

	public void setDebitId(Long debitId) {
		this.debitId = debitId;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getTotalPay() {
		return totalPay;
	}

	public void setTotalPay(BigDecimal totalPay) {
		this.totalPay = totalPay;
	}

	public BigDecimal getRemain() {
		return remain;
	}

	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}

	public BigDecimal getPayAmt() {
		return payAmt;
	}
	public void setPayAmt(BigDecimal payAmt) {
		this.payAmt = payAmt;
	}

	public String getNvttCodeName() {
		return nvttCodeName;
	}

	public void setNvttCodeName(String nvttCodeName) {
		this.nvttCodeName = nvttCodeName;
	}

	public String getNvghCodeName() {
		return nvghCodeName;
	}

	public void setNvghCodeName(String nvghCodeName) {
		this.nvghCodeName = nvghCodeName;
	}

	public String getNvbhCodeName() {
		return nvbhCodeName;
	}

	public void setNvbhCodeName(String nvbhCodeName) {
		this.nvbhCodeName = nvbhCodeName;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getNvttName() {
		return nvttName;
	}

	public void setNvttName(String nvttName) {
		this.nvttName = nvttName;
	}

	public String getNvghName() {
		return nvghName;
	}

	public void setNvghName(String nvghName) {
		this.nvghName = nvghName;
	}

	public String getNvbhName() {
		return nvbhName;
	}

	public void setNvbhName(String nvbhName) {
		this.nvbhName = nvbhName;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public BigDecimal getDiscountAmt() {
		return discountAmt;
	}

	public void setDiscountAmt(BigDecimal discountAmt) {
		this.discountAmt = discountAmt;
	}

	public String getOrderDateStr() {
		if (orderDateStr == null || orderDateStr.trim().length() == 0) {
			orderDateStr = "";
			if (orderDate != null) {
				Object[] params = new Object[] { orderDate };
				try {
					orderDateStr = MessageFormat.format("{0,date,dd/MM/yyyy}", params);
				} catch (Exception e) {
					// pass through
				}
			}
		}
		return orderDateStr;
	}

	public void setOrderDateStr(String orderDateStr) {
		this.orderDateStr = orderDateStr;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	
}

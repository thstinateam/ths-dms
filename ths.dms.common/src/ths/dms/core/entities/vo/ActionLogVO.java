package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

public class ActionLogVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private Long shopId;
	private String shopCode;
	private String shopName;
	private Long staffId;
	private String staffCode;
	private String staffName;
	private Integer totalVisit;
	private Integer numVisit;
	private Integer numLess2Minute;
	private Integer numBetween2And30Minute;
	private Integer numOver30Minute;
	private Integer numOver60Minute;
	private Integer objectType;
	private Integer isOr;
	private Float visitTime;
	private Integer numCorrectRouting;
	private Integer numWrongRouting;
	private Integer numOrder;
	private String ereaCode;//code mien
	private String ereaName;//ten mien
	private String parentShopCode;//code vung
	private String parentShopName;//ten vung
	private Float totalVisitTime;
	private Float avgVisitTime;
	private Date startTime;
	private Date endTime;
	private String superCode; //ma giam sat nha phan phoi
	private String superName; //ma giam sat nha phan phoi
	
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Integer getTotalVisit() {
		return totalVisit;
	}
	public void setTotalVisit(Integer totalVisit) {
		this.totalVisit = totalVisit;
	}
	public Integer getNumVisit() {
		return numVisit;
	}
	public void setNumVisit(Integer numVisit) {
		this.numVisit = numVisit;
	}
	public Integer getNumLess2Minute() {
		return numLess2Minute;
	}
	public void setNumLess2Minute(Integer numLess2Minute) {
		this.numLess2Minute = numLess2Minute;
	}
	public Integer getNumBetween2And30Minute() {
		return numBetween2And30Minute;
	}
	public void setNumBetween2And30Minute(Integer numBetween2And30Minute) {
		this.numBetween2And30Minute = numBetween2And30Minute;
	}
	public Integer getNumOver30Minute() {
		return numOver30Minute;
	}
	public void setNumOver30Minute(Integer numOver30Minute) {
		this.numOver30Minute = numOver30Minute;
	}
	public Integer getIsOr() {
		return isOr;
	}
	public void setIsOr(Integer isOr) {
		this.isOr = isOr;
	}
	public Float getVisitTime() {
		return visitTime;
	}
	public void setVisitTime(Float visitTime) {
		this.visitTime = visitTime;
	}
	/**
	 * @return the objectType
	 */
	public Integer getObjectType() {
		return objectType;
	}
	/**
	 * @param objectType the objectType to set
	 */
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public Integer getNumCorrectRouting() {
		return numCorrectRouting;
	}
	public void setNumCorrectRouting(Integer numCorrectRouting) {
		this.numCorrectRouting = numCorrectRouting;
	}
	public Integer getNumWrongRouting() {
		return numWrongRouting;
	}
	public void setNumWrongRouting(Integer numWrongRouting) {
		this.numWrongRouting = numWrongRouting;
	}
	/**
	 * @return the numOrder
	 */
	public Integer getNumOrder() {
		return numOrder;
	}
	/**
	 * @param numOrder the numOrder to set
	 */
	public void setNumOrder(Integer numOrder) {
		this.numOrder = numOrder;
	}
	public String getEreaCode() {
		return ereaCode;
	}
	public void setEreaCode(String ereaCode) {
		this.ereaCode = ereaCode;
	}
	public String getEreaName() {
		return ereaName;
	}
	public void setEreaName(String ereaName) {
		this.ereaName = ereaName;
	}
	public String getParentShopCode() {
		return parentShopCode;
	}
	public void setParentShopCode(String parentShopCode) {
		this.parentShopCode = parentShopCode;
	}
	public String getParentShopName() {
		return parentShopName;
	}
	public void setParentShopName(String parentShopName) {
		this.parentShopName = parentShopName;
	}
	/**
	 * @return the shopId
	 */
	public Long getShopId() {
		return shopId;
	}
	/**
	 * @param shopId the shopId to set
	 */
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Float getTotalVisitTime() {
		return totalVisitTime;
	}
	public void setTotalVisitTime(Float totalVisitTime) {
		this.totalVisitTime = totalVisitTime;
	}
	public Float getAvgVisitTime() {
		return avgVisitTime;
	}
	public void setAvgVisitTime(Float avgVisitTime) {
		this.avgVisitTime = avgVisitTime;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getSuperCode() {
		return superCode;
	}
	public void setSuperCode(String superCode) {
		this.superCode = superCode;
	}
	public Integer getNumOver60Minute() {
		return numOver60Minute;
	}
	public void setNumOver60Minute(Integer numOver60Minute) {
		this.numOver60Minute = numOver60Minute;
	}
	public String getSuperName() {
		return superName;
	}
	public void setSuperName(String superName) {
		this.superName = superName;
	}
	
}

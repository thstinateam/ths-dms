package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * Lop doi tuong kho
 * 
 * @author hunglm16
 * @return Class VO Warehouse
 * @since August 15,2014
 * @description Khong them doi tuong la Object vao VO nay
 */
public class WarehouseVO implements Serializable {
	private static final long serialVersionUID = 4488396665144328036L;
	private Long warehouseId;
	private String warehouseCode;
	private String warehouseName;
	private String createDateStr;
	private String description;
	private String warehouseType;
	private String shopCode; 
	private String shopName;
	private String shopCodeV;
	private String shopCodeM;
	private Integer seq;
	private Integer status;
	private String warehouseTypeN;
	private Long shopId;
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getWarehouseTypeN() {
		return warehouseTypeN;
	}
	public void setWarehouseTypeN(String warehouseTypeN) {
		this.warehouseTypeN = warehouseTypeN;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public Long getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}
	public String getWarehouseCode() {
		return warehouseCode;
	}
	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}
	public String getWarehouseName() {
		return warehouseName;
	}
	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}
	public String getCreateDateStr() {
		return createDateStr;
	}
	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getWarehouseType() {
		return warehouseType;
	}
	public void setWarehouseType(String warehouseType) {
		this.warehouseType = warehouseType;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopCodeV() {
		return shopCodeV;
	}
	public void setShopCodeV(String shopCodeV) {
		this.shopCodeV = shopCodeV;
	}
	public String getShopCodeM() {
		return shopCodeM;
	}
	public void setShopCodeM(String shopCodeM) {
		this.shopCodeM = shopCodeM;
	}
	
	
}

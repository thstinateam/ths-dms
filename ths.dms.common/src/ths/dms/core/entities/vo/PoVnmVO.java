package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * 
 * @author lacnv1
 * @since Sep 09, 2014
 */
public class PoVnmVO implements Serializable {

	private static final long serialVersionUID = 1L;

	
	private Long poVnmId;
	private String invoiceNumber;
	private String reason;
	
	public Long getPoVnmId() {
		return poVnmId;
	}
	
	public void setPoVnmId(Long poVnmId) {
		this.poVnmId = poVnmId;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
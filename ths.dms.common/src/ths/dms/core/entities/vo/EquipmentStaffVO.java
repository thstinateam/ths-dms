package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.management.loading.PrivateClassLoader;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 * Class EquipmentStaff VO
 * @author tamvnm
 * @since March 24,2015
 * @description Lay thong tin nhan vien cho Phan quyen nguoi dung
 */
public class EquipmentStaffVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id; 
	private String staffCode;
	private String staffName;
	private String shopCode;
	private String shopName;
	private Integer status;
	private String roleCode; /** vuongmq; export*/
	private String statusStr;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getStatusStr() {
		if(ActiveType.RUNNING.getValue().equals(status)){
			statusStr = "Hoạt động";
		} else if(ActiveType.STOPPED.getValue().equals(status)){
			statusStr = "Tạm ngưng";
		}
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	
}

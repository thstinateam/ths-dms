/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;

import javax.persistence.Basic;

/**
 * Class dung chung cho chuong trinh khuyen mai
 * 
 * @author hunglm16
 * @since 19/09/2015
 */
public class PromotionProgramVO implements Serializable {

	private static final long serialVersionUID = -2381201460326711676L;

	private Long id;

	//ma ctkm
	private String promotionProgramCode;

	//ten ctkm
	private String promotionProgramName;

	//trang thai
	private Integer status;

	//loai km: ZV01 --> ZV21
	@Basic
	private String type;

	//kieu km
	private String proFormat;

	//tu ngay
	private String fromDate;

	//den ngay
	private String toDate;

	private String description;

	//kieu km
	private String nameText;

	//0: khong cho phep sua so luong va so suat
	//1: cho phep sua so luong , khong hien thi so suat
	//2: cho phep sua so suat , khong duoc sua so luong
	private Integer isEdited;

	private String userName;

	private Integer quantiMonthNewOpen;

	private String md5ValidCode;

	private Integer hasPortion; // cho biet CTKM co so suat hay khong (1 co, else: khong)

	/**
	 * Khai bao GETTER/SETTER
	 * */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}

	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}

	public String getPromotionProgramName() {
		return promotionProgramName;
	}

	public void setPromotionProgramName(String promotionProgramName) {
		this.promotionProgramName = promotionProgramName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProFormat() {
		return proFormat;
	}

	public void setProFormat(String proFormat) {
		this.proFormat = proFormat;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNameText() {
		return nameText;
	}

	public void setNameText(String nameText) {
		this.nameText = nameText;
	}

	public Integer getIsEdited() {
		return isEdited;
	}

	public void setIsEdited(Integer isEdited) {
		this.isEdited = isEdited;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getQuantiMonthNewOpen() {
		return quantiMonthNewOpen;
	}

	public void setQuantiMonthNewOpen(Integer quantiMonthNewOpen) {
		this.quantiMonthNewOpen = quantiMonthNewOpen;
	}

	public String getMd5ValidCode() {
		return md5ValidCode;
	}

	public void setMd5ValidCode(String md5ValidCode) {
		this.md5ValidCode = md5ValidCode;
	}

	public Integer getHasPortion() {
		return hasPortion;
	}

	public void setHasPortion(Integer hasPortion) {
		this.hasPortion = hasPortion;
	}
}

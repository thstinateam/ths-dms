/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.EquipStockAdjustFormDtl;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;

/**
 * thong tin phieu 
 * @author 
 * @version 
 */
public class EquipStockAdjustFormRecord implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String code;
	private StatusRecordsEquip status;
	private List<EquipStockAdjustFormDtl> equipStockAdjustFormDtl;
	private String description;
	private String note;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public StatusRecordsEquip getStatus() {
		return status;
	}
	public void setStatus(StatusRecordsEquip status) {
		this.status = status;
	}
	public List<EquipStockAdjustFormDtl> getEquipStockAdjustFormDtl() {
		return equipStockAdjustFormDtl;
	}
	public void setEquipStockAdjustFormDtl(
			List<EquipStockAdjustFormDtl> equipStockAdjustFormDtl) {
		this.equipStockAdjustFormDtl = equipStockAdjustFormDtl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

public class FormErrVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String formCode;
	private Integer deliveryStatusErr;
	private Integer formStatusErr;
	private Integer periodError;
	private Integer notGroupOrShop;
	private String groupInActive;
	private String shopInActive;
	private String groupNotEquip;
	private String recordNotComplete;
	private String equipTrading;
	
	public String getFormCode() {
		return formCode;
	}
	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}
	public Integer getDeliveryStatusErr() {
		return deliveryStatusErr;
	}
	public void setDeliveryStatusErr(Integer deliveryStatusErr) {
		this.deliveryStatusErr = deliveryStatusErr;
	}
	public Integer getFormStatusErr() {
		return formStatusErr;
	}
	public void setFormStatusErr(Integer formStatusErr) {
		this.formStatusErr = formStatusErr;
	}
	public Integer getPeriodError() {
		return periodError;
	}
	public void setPeriodError(Integer periodError) {
		this.periodError = periodError;
	}
	public Integer getNotGroupOrShop() {
		return notGroupOrShop;
	}
	public void setNotGroupOrShop(Integer notGroupOrShop) {
		this.notGroupOrShop = notGroupOrShop;
	}
	public String getGroupInActive() {
		return groupInActive;
	}
	public void setGroupInActive(String groupInActive) {
		this.groupInActive = groupInActive;
	}
	public String getShopInActive() {
		return shopInActive;
	}
	public void setShopInActive(String shopInActive) {
		this.shopInActive = shopInActive;
	}
	public String getGroupNotEquip() {
		return groupNotEquip;
	}
	public void setGroupNotEquip(String groupNotEquip) {
		this.groupNotEquip = groupNotEquip;
	}
	public String getRecordNotComplete() {
		return recordNotComplete;
	}
	public void setRecordNotComplete(String recordNotComplete) {
		this.recordNotComplete = recordNotComplete;
	}
	public String getEquipTrading() {
		return equipTrading;
	}
	public void setEquipTrading(String equipTrading) {
		this.equipTrading = equipTrading;
	}
	
}

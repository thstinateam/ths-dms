/**
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
/**
 * Danh sach Doanh so cho bang tong hop RPT_STAFF_SALE; voi Shop 
 * @author vuongmq
 * @since 21/09/2015
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class ShopSaleVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5057282796575247633L;
	private Long staffId;
	private String shopName;
	private String shopCode;
	private String staffCode;
	private String staffName;
	private String routingCode;
	private String routingName;
	
	private Float lat;
	private Float lng;
	private Integer dhKH;
	private Integer dhTH;
	private BigDecimal dayAmountPlan;
	private BigDecimal dayAmountApproved;
	private BigDecimal dayAmount;
	private BigDecimal dsKH;
	private BigDecimal dsTH;
	private BigDecimal dsDuyet;
	private BigDecimal dsConLai;
	private Integer score;
	private Long shopID;
	private Integer specificType;
	private BigDecimal monthAmountPlan;
	private BigDecimal monthAmountApproved;
	private BigDecimal monthAmount;
	private Integer dayQuantityPlan;
	private Integer dayQuantityApproved;
	private Integer dayQuantity;
	private Integer monthQuantityPlan;
	private Integer monthQuantityApproved;
	private Integer monthQuantity;
	
	public Long getShopId() {
		return shopID;
	}
	public void setShopId(Long shopId) {
		this.shopID = shopId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Float getLat() {
		return lat;
	}
	public void setLat(Float lat) {
		this.lat = lat;
	}
	public Float getLng() {
		return lng;
	}
	public void setLng(Float lng) {
		this.lng = lng;
	}
	public Integer getDhKH() {
		return dhKH;
	}
	public void setDhKH(Integer dhKH) {
		this.dhKH = dhKH;
	}
	public Integer getDhTH() {
		return dhTH;
	}
	public void setDhTH(Integer dhTH) {
		this.dhTH = dhTH;
	}
	public BigDecimal getDayAmountPlan() {
		return dayAmountPlan;
	}
	public void setDayAmountPlan(BigDecimal dayAmountPlan) {
		this.dayAmountPlan = dayAmountPlan;
	}
	public BigDecimal getDayAmountApproved() {
		return dayAmountApproved;	
	}
	public void setDayAmountApproved(BigDecimal dayAmountApproved) {
		this.dayAmountApproved = dayAmountApproved;
	}
	public BigDecimal getDayAmount() {
		return dayAmount;
	}
	public void setDayAmount(BigDecimal dayAmount) {
		this.dayAmount = dayAmount;
	}
	public BigDecimal getDsKH() {
		return dsKH;
	}
	public void setDsKH(BigDecimal dsKH) {
		this.dsKH = dsKH;
	}
	public BigDecimal getDsTH() {
		return dsTH;
	}
	public void setDsTH(BigDecimal dsTH) {
		this.dsTH = dsTH;
	}
	
	public BigDecimal getDsDuyet() {
		return dsDuyet;
	}
	public void setDsDuyet(BigDecimal dsDuyet) {
		this.dsDuyet = dsDuyet;
	}
	public BigDecimal getDsConLai() {
		return dsConLai;
	}
	public void setDsConLai(BigDecimal dsConLai) {
		this.dsConLai = dsConLai;
	}
	public BigDecimal getDayAmountLeft() {
		BigDecimal dayAmountLeft = null;
		dayAmountLeft = dayAmountPlan != null && dayAmount != null && dayAmountPlan.compareTo(dayAmount) > 0 ? dayAmountPlan.subtract(dayAmount) : BigDecimal.ZERO;
		BigDecimal __dayAmountLeft = dayAmountLeft.divide(BigDecimal.valueOf(1000)).setScale(0, RoundingMode.HALF_DOWN);
		return __dayAmountLeft;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Long getShopID() {
		return shopID;
	}
	public void setShopID(Long shopID) {
		this.shopID = shopID;
	}
	public Integer getSpecificType() {
		return specificType;
	}
	public void setSpecificType(Integer specificType) {
		this.specificType = specificType;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public BigDecimal getMonthAmountPlan() {
		return monthAmountPlan;
	}
	public void setMonthAmountPlan(BigDecimal monthAmountPlan) {
		this.monthAmountPlan = monthAmountPlan;
	}
	public BigDecimal getMonthAmountApproved() {
		return monthAmountApproved;
	}
	public void setMonthAmountApproved(BigDecimal monthAmountApproved) {
		this.monthAmountApproved = monthAmountApproved;
	}
	public BigDecimal getMonthAmount() {
		return monthAmount;
	}
	public void setMonthAmount(BigDecimal monthAmount) {
		this.monthAmount = monthAmount;
	}
	public Integer getDayQuantityPlan() {
		return dayQuantityPlan;
	}
	public void setDayQuantityPlan(Integer dayQuantityPlan) {
		this.dayQuantityPlan = dayQuantityPlan;
	}
	public Integer getDayQuantityApproved() {
		return dayQuantityApproved;
	}
	public void setDayQuantityApproved(Integer dayQuantityApproved) {
		this.dayQuantityApproved = dayQuantityApproved;
	}
	public Integer getDayQuantity() {
		return dayQuantity;
	}
	public void setDayQuantity(Integer dayQuantity) {
		this.dayQuantity = dayQuantity;
	}
	public Integer getMonthQuantityPlan() {
		return monthQuantityPlan;
	}
	public void setMonthQuantityPlan(Integer monthQuantityPlan) {
		this.monthQuantityPlan = monthQuantityPlan;
	}
	public Integer getMonthQuantityApproved() {
		return monthQuantityApproved;
	}
	public void setMonthQuantityApproved(Integer monthQuantityApproved) {
		this.monthQuantityApproved = monthQuantityApproved;
	}
	public Integer getMonthQuantity() {
		return monthQuantity;
	}
	public void setMonthQuantity(Integer monthQuantity) {
		this.monthQuantity = monthQuantity;
	}
	public String getRoutingCode() {
		return routingCode;
	}
	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}
	public String getRoutingName() {
		return routingName;
	}
	public void setRoutingName(String routingName) {
		this.routingName = routingName;
	}
	
}

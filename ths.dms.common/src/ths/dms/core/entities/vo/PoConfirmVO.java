package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ths.dms.core.common.utils.CDateUtil;
import ths.dms.core.entities.enumtype.PoProductType;

public class PoConfirmVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 396312171554634642L;
	// id sp
	private Long productId;
	// ma sp
	private String productCode;
	// ten sp
	private String productName;
	// so luong dat mua
	private Integer bookedQuantity;
	// gia sp
	private BigDecimal productPrice;
	// so luong chua nhap ve
	private Integer notImportQuantity;
	// ngay nhap ve NPP
	private Date importDate;
	//ngay nhap ve NPP Str
	private String importDateStr;
	// so luong da nhap ve npp
	private Integer importPoQuantity;
	// so luong chua nhap tung sp
	private Integer suspendPoDVKHQuantity;
	// vuongmq; 04/08/2015; them productType cho sp Po_vnm_detail
	private Integer productType;
	// tra ve string loai SP
	private String productTypeStr;
	
	public PoConfirmVO() {

	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getBookedQuantity() {
		if (bookedQuantity == null) {
			bookedQuantity = 0;
		}
		return bookedQuantity;
	}

	public void setBookedQuantity(Integer bookedQuantity) {
		this.bookedQuantity = bookedQuantity;
	}

	public BigDecimal getProductPrice() {
		return productPrice == null ? BigDecimal.valueOf(0) : productPrice;
	}

	public void setProductPrice(BigDecimal productPrice) {
		this.productPrice = productPrice;
	}

	public Integer getNotImportQuantity() {
		if (notImportQuantity == null) {
			notImportQuantity = 0;
		}
		return notImportQuantity;
	}

	public void setNotImportQuantity(Integer notImportQuantity) {
		this.notImportQuantity = notImportQuantity;
	}

	public Date getImportDate() {
		return importDate;
	}

	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}

	public Integer getImportPoQuantity() {
		if (importPoQuantity == null) {
			importPoQuantity = 0;
		}
		return importPoQuantity;
	}

	public void setImportPoQuantity(Integer importPoQuantity) {
		this.importPoQuantity = importPoQuantity;
	}

	public Integer getSuspendPoDVKHQuantity() {
		if (suspendPoDVKHQuantity == null) {
			suspendPoDVKHQuantity = 0;
		}
		return suspendPoDVKHQuantity;
	}

	public void setSuspendPoDVKHQuantity(Integer suspendPoDVKHQuantity) {
		this.suspendPoDVKHQuantity = suspendPoDVKHQuantity;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public String getProductTypeStr() {
		if (PoProductType.SALE.getValue().equals(productType)) {
			productTypeStr = "Hàng bán";
		} else if (PoProductType.PROMOTION.getValue().equals(productType)) {
			productTypeStr = "Hàng KM";
		} else if (PoProductType.POSM.getValue().equals(productType)) {
			productTypeStr = "POSM";
		}
		return productTypeStr;
	}

	public void setProductTypeStr(String productTypeStr) {
		this.productTypeStr = productTypeStr;
	}

	public String getImportDateStr() {
		if (importDate != null) {
			importDateStr = CDateUtil.toDateString(importDate, CDateUtil.DATE_FORMAT_STR);
		}
		return importDateStr;
	}

	public void setImportDateStr(String importDateStr) {
		this.importDateStr = importDateStr;
	}
}

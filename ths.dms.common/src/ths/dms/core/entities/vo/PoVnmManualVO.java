/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * Mo ta class PoVnmManualVO.java
 * @author vuongmq
 * @since Dec 14, 2015
 */
public class PoVnmManualVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long shopId;
	private Long fromId;
	private String orderNumber;
	private String asn;
	private String poDateStr;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getFromId() {
		return fromId;
	}
	public void setFromId(Long fromId) {
		this.fromId = fromId;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getAsn() {
		return asn;
	}
	public void setAsn(String asn) {
		this.asn = asn;
	}
	public String getPoDateStr() {
		return poDateStr;
	}
	public void setPoDateStr(String poDateStr) {
		this.poDateStr = poDateStr;
	}
	
}
package ths.dms.core.entities.vo;
/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.util.Date;

/**
 * Class Equip Lost Record VO
 * 
 * @author hunglm16
 * @since December 30,2014
 * @description Them thuoc tinh co trong bang lien quan den Equiment Record
 */
public class EquipProposalBorrowVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long equipId;
	private Long shopId;
	private Long stockId;
	private Long customerId;
	private Long idStock;
	
	private Integer status;
	private Integer stockType;
	private Integer recordStatus;
	private Integer deliveryStatus;
	private Integer isUnder;
	private Integer objectId;
	private Integer ch;
	
	private String code;
	private String name;
	private String stockCode;
	private String shortCode;
	private String customerName;
	private String address;
	private String shopCode;
	private String shopName;
	private String shopCodeName;
	private String serial;
	private String equipCode;
	private String conclusion;
	private String description;
	private String stock;
	private String stockName;

	private String customerAddress;
	private String equipCategoryCode;
	private String equipGroupName;
	private String equipCategoryName;
	private String customerPhone;
	private String representation;
	private String contractNumber;
	private String firstDateInUseStr;
	
	private Date lostDate;
	/**
	 * Khai bao phuong thuc GETTER/SETTER
	 * */
	
	public Long getId() {
		return id;
	}
	public String getFirstDateInUseStr() {
		return firstDateInUseStr;
	}
	public void setFirstDateInUseStr(String firstDateInUseStr) {
		this.firstDateInUseStr = firstDateInUseStr;
	}
	public Date getLostDate() {
		return lostDate;
	}
	public void setLostDate(Date lostDate) {
		this.lostDate = lostDate;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getRepresentation() {
		return representation;
	}
	public void setRepresentation(String representation) {
		this.representation = representation;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	
	public Integer getDeliveryStatus() {
		return deliveryStatus;
	}
	public void setDeliveryStatus(Integer deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	
	public Integer getStockType() {
		return stockType;
	}
	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEquipId() {
		return equipId;
	}
	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Integer getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopCodeName() {
		return shopCodeName;
	}
	public void setShopCodeName(String shopCodeName) {
		this.shopCodeName = shopCodeName;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getEquipCode() {
		return equipCode;
	}
	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}
	
	public String getConclusion() {
		return conclusion;
	}
	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}
	
	public String getEquipCategoryCode() {
		return equipCategoryCode;
	}
	public void setEquipCategoryCode(String equipCategoryCode) {
		this.equipCategoryCode = equipCategoryCode;
	}
	public String getEquipGroupName() {
		return equipGroupName;
	}
	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}
	public String getEquipCategoryName() {
		return equipCategoryName;
	}
	public void setEquipCategoryName(String equipCategoryName) {
		this.equipCategoryName = equipCategoryName;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public Integer getStatus() {
		return status;
	}
	public String getName() {
		return name;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getIsUnder() {
		return isUnder;
	}
	public void setIsUnder(Integer isUnder) {
		this.isUnder = isUnder;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public Integer getObjectId() {
		return objectId;
	}
	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}
	public Long getIdStock() {
		return idStock;
	}
	public void setIdStock(Long idStock) {
		this.idStock = idStock;
	}
	public Integer getCh() {
		return ch;
	}
	public void setCh(Integer ch) {
		this.ch = ch;
	}

}

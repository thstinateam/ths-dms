package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class RptDTBHTNTNVBHStaffInfo_7_2_2VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal staffId;
	private String staffCode;
	private String staffName;
	
	ArrayList<RptDTBHTNTNVBHDateInfo_7_2_2VO> listRptDTBHTNTNVBHDateInfoVO = new ArrayList<RptDTBHTNTNVBHDateInfo_7_2_2VO>();
	private BigDecimal sumRevenueStaff;
	private BigDecimal sumMoneyDiscountStaff; 
	private BigDecimal sumProductDiscountStaff;
	private BigDecimal sumMoneyStaff;
	private BigDecimal sumDiscountStaff;
	private BigDecimal sumSKUStaff;
	private BigDecimal sumDebitStaff;
	private BigDecimal sumCashStaff;
	private BigDecimal countSoDHStaff;
	
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
	
	public ArrayList<RptDTBHTNTNVBHDateInfo_7_2_2VO> getListRptDTBHTNTNVBHDateInfoVO() {
		return listRptDTBHTNTNVBHDateInfoVO;
	}
	public void setListRptDTBHTNTNVBHDateInfoVO(
			ArrayList<RptDTBHTNTNVBHDateInfo_7_2_2VO> listRptDTBHTNTNVBHDateInfoVO) {
		this.listRptDTBHTNTNVBHDateInfoVO = listRptDTBHTNTNVBHDateInfoVO;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public BigDecimal getSumRevenueStaff() {
		return sumRevenueStaff;
	}
	public void setSumRevenueStaff(BigDecimal sumRevenueStaff) {
		this.sumRevenueStaff = sumRevenueStaff;
	}
	public BigDecimal getSumMoneyDiscountStaff() {
		return sumMoneyDiscountStaff;
	}
	public void setSumMoneyDiscountStaff(BigDecimal sumMoneyDiscountStaff) {
		this.sumMoneyDiscountStaff = sumMoneyDiscountStaff;
	}
	public BigDecimal getSumProductDiscountStaff() {
		return sumProductDiscountStaff;
	}
	public void setSumProductDiscountStaff(BigDecimal sumProductDiscountStaff) {
		this.sumProductDiscountStaff = sumProductDiscountStaff;
	}
	public BigDecimal getSumMoneyStaff() {
		return sumMoneyStaff;
	}
	public void setSumMoneyStaff(BigDecimal sumMoneyStaff) {
		this.sumMoneyStaff = sumMoneyStaff;
	}
	public BigDecimal getSumDiscountStaff() {
		return sumDiscountStaff;
	}
	public void setSumDiscountStaff(BigDecimal sumDiscountStaff) {
		this.sumDiscountStaff = sumDiscountStaff;
	}
	public BigDecimal getSumSKUStaff() {
		return sumSKUStaff;
	}
	public void setSumSKUStaff(BigDecimal sumSKUStaff) {
		this.sumSKUStaff = sumSKUStaff;
	}
	public BigDecimal getSumDebitStaff() {
		return sumDebitStaff;
	}
	public void setSumDebitStaff(BigDecimal sumDebitStaff) {
		this.sumDebitStaff = sumDebitStaff;
	}
	public BigDecimal getSumCashStaff() {
		return sumCashStaff;
	}
	public void setSumCashStaff(BigDecimal sumCashStaff) {
		this.sumCashStaff = sumCashStaff;
	}
	public BigDecimal getStaffId() {
		return staffId;
	}
	public void setStaffId(BigDecimal staffId) {
		this.staffId = staffId;
	}
	public BigDecimal getCountSoDHStaff() {
		return countSoDHStaff;
	}
	public void setCountSoDHStaff(BigDecimal countSoDHStaff) {
		this.countSoDHStaff = countSoDHStaff;
	}
	
}

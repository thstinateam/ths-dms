package ths.dms.core.entities.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapMuaKM extends HashMap<Long, List<Long>>{

	public List<Long> put(Long indexMua, Long indexKM) {
		if(this.get(indexMua) == null) {
			List<Long> listIndexKM = new ArrayList<Long>();
			listIndexKM.add(indexKM);
			this.put(indexMua, listIndexKM);
			return listIndexKM;
		} else {
			List<Long> listIndexKM = this.get(indexMua);
			for(int i = 0; i < listIndexKM.size(); i++) {
				if(listIndexKM.get(i).equals(indexKM)) {
					return listIndexKM;
				}
			}
			listIndexKM.add(indexKM);
			return listIndexKM;
		}
	}
	
}

/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * @author tungmt
 * @since 7/8/2015
 */
public class SaleOrderPromoLotVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long productId;
	private Long warehouseId;
	
	private String programCode;
	private String productCode;
	
	private Boolean isKS;
	
	private BigDecimal discount;
	private BigDecimal maxDiscount;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getProgramCode() {
		return programCode;
	}
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Boolean getIsKS() {
		return isKS;
	}
	public void setIsKS(Boolean isKS) {
		this.isKS = isKS;
	}
	public Long getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}
	public BigDecimal getMaxDiscount() {
		return maxDiscount;
	}
	public void setMaxDiscount(BigDecimal maxDiscount) {
		this.maxDiscount = maxDiscount;
	}
	
	

}
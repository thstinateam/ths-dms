package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

public class MediaItemDetailVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1956654986662515109L;
	private Long id;
	private String shopCode;
	private String shopName;
	private String staffCode;
	private String staffName;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String displayProgramCode;
	private String url;
	private Date createDate;
	private String monthSeq;
	private String imageName;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getDisplayProgramCode() {
		return displayProgramCode;
	}
	public void setDisplayProgramCode(String displayProgramCode) {
		this.displayProgramCode = displayProgramCode;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getMonthSeq() {
		return monthSeq;
	}
	public void setMonthSeq(String monthSeq) {
		this.monthSeq = monthSeq;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
		
}

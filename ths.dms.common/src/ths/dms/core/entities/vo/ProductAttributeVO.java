package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;
/**
 * Quan ly thuoc tinh san pham
 * @author tientv11
 * @since 16/01/2015
 */
public class ProductAttributeVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String code;
	
	private String name;
	
	private String description;
	
	private Integer type;
	
	private Integer dataLength;
	
	private Integer minValue;
	
	private Integer maxValue;
	
	private Integer isEnumeration;
	
	private Integer isSearch;
	
	private Integer mandatory;
	
	private Integer displayOrder;
	
	private Integer status;
	
	private List<ProductAttributeDetailVO> attributeDetailVOs;
	
	
	private List<ProductAttributeEnumVO> attributeEnumVOs;
	
	
	

	public List<ProductAttributeEnumVO> getAttributeEnumVOs() {
		return attributeEnumVOs;
	}

	public void setAttributeEnumVOs(List<ProductAttributeEnumVO> attributeEnumVOs) {
		this.attributeEnumVOs = attributeEnumVOs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getDataLength() {
		if(dataLength == null){
			dataLength = 2000;
		}
		return dataLength;
	}

	public void setDataLength(Integer dataLength) {
		this.dataLength = dataLength;
	}

	public Integer getMinValue() {
		return minValue;
	}

	public void setMinValue(Integer minValue) {
		this.minValue = minValue;
	}

	public Integer getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Integer maxValue) {
		this.maxValue = maxValue;
	}

	public Integer getIsEnumeration() {
		return isEnumeration;
	}

	public void setIsEnumeration(Integer isEnumeration) {
		this.isEnumeration = isEnumeration;
	}

	public Integer getIsSearch() {
		return isSearch;
	}

	public void setIsSearch(Integer isSearch) {
		this.isSearch = isSearch;
	}

	public Integer getMandatory() {
		return mandatory;
	}

	public void setMandatory(Integer mandatory) {
		this.mandatory = mandatory;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<ProductAttributeDetailVO> getAttributeDetailVOs() {
		return attributeDetailVOs;
	}

	public void setAttributeDetailVOs(
			List<ProductAttributeDetailVO> attributeDetailVOs) {
		this.attributeDetailVOs = attributeDetailVOs;
	}
	
	

}

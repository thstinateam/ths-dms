/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * VO map dong du lieu don hang xuat ra file xml
 * 
 * @author lacnv1
 * @since Mar 14, 2015
 */
public class OrderXmlLineDataVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<String, String> data;

	public OrderXmlLineDataVO() {
		data = new LinkedHashMap<String, String>();
		
		setDistCode(null);
		setOrderNbr(null);
		setItemCode(null);
		setLineQty(null);
		setSalePrice(null);
		setUOM2(null);
		setDiscountPer(null);
		setDiscountAmt(null);
		setFreeItem(null);
		setLineProCode(null);
		setLineAmt(null);
		setReasonCode(null);
	}
	
	public void setValue(String name, String value){
		data.put(name, value);
	}
	public String getDistCode() {
		return data.get("DistCode");
	}

	public void setDistCode(String distCode) {
		data.put("DistCode", distCode);
	}

	public String getOrderNbr() {
		return data.get("OrderNbr");
	}

	public void setOrderNbr(String orderNbr) {
		data.put("OrderNbr", orderNbr);
	}

	public String getItemCode() {
		return data.get("ItemCode");
	}

	public void setItemCode(String itemCode) {
		data.put("ItemCode", itemCode);
	}

	public String getLineQty() {
		return data.get("LineQty");
	}

	public void setLineQty(String lineQty) {
		data.put("LineQty", lineQty);
	}

	public String getSalePrice() {
		return data.get("SalePrice");
	}

	public void setSalePrice(String salePrice) {
		data.put("SalePrice", salePrice);
	}

	public String getUOM2() {
		return data.get("UOM2");
	}

	public void setUOM2(String uOM2) {
		data.put("UOM2", uOM2);
	}

	public String getDiscountPer() {
		return data.get("DiscountPer");
	}

	public void setDiscountPer(String discountPer) {
		data.put("DiscountPer", discountPer);
	}

	public String getDiscountAmt() {
		return data.get("DiscountAmt");
	}

	public void setDiscountAmt(String discountAmt) {
		data.put("DiscountAmt", discountAmt);
	}

	public String getFreeItem() {
		return data.get("FreeItem");
	}

	public void setFreeItem(String freeItem) {
		data.put("FreeItem", freeItem);
	}

	public String getLineProCode() {
		return data.get("LineProCode");
	}

	public void setLineProCode(String lineProCode) {
		data.put("LineProCode", lineProCode);
	}

	public String getLineAmt() {
		return data.get("LineAmt");
	}

	public void setLineAmt(String lineAmt) {
		data.put("LineAmt", lineAmt);
	}

	public String getReasonCode() {
		return data.get("ReasonCode");
	}

	public void setReasonCode(String reasonCode) {
		data.put("ReasonCode", reasonCode);
	}

	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		return data.toString();
	}
}
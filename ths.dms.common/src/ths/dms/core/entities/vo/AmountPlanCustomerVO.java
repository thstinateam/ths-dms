/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
/***
 * @author vuongmq
 * @date 18/08/2015
 * @description marker cua Customer; ung voi doanh so cua NVBH (RPT_STAFF_SALE_DETAIL) 
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class AmountPlanCustomerVO implements Serializable  {

	private static final long serialVersionUID = 2602571432058482862L;
	private String shortCode;
	private String customerCode;
	private String customerName;
	private String address; //housenumber - street
	private String phone;
	private String mobiphone;
	private String lat;
	private String lng;
	
	private BigDecimal dayAmountPlan;
	private BigDecimal dayAmount;
	private BigDecimal dayAmountApproved;
	private BigDecimal monthAmountPlan;
	private BigDecimal monthAmount;
	private BigDecimal monthAmountApproved;
	// vuongmq; 18/08/2015; Lay them san luong
	private Integer dayQuantityPlan;
	private Integer dayQuantity;
	private Integer dayQuantityApproved;
	private Integer monthQuantityPlan;
	private Integer monthQuantity;
	private Integer monthQuantityApproved;
	
	private List<ActionLogCustomerVO> lstDetailActionLog;
	
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobiphone() {
		return mobiphone;
	}
	public void setMobiphone(String mobiphone) {
		this.mobiphone = mobiphone;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public BigDecimal getDayAmountPlan() {
		return dayAmountPlan;
	}
	public void setDayAmountPlan(BigDecimal dayAmountPlan) {
		this.dayAmountPlan = dayAmountPlan;
	}
	public BigDecimal getDayAmount() {
		return dayAmount;
	}
	public void setDayAmount(BigDecimal dayAmount) {
		this.dayAmount = dayAmount;
	}
	public BigDecimal getMonthAmountPlan() {
		return monthAmountPlan;
	}
	public void setMonthAmountPlan(BigDecimal monthAmountPlan) {
		this.monthAmountPlan = monthAmountPlan;
	}
	public BigDecimal getMonthAmount() {
		return monthAmount;
	}
	public void setMonthAmount(BigDecimal monthAmount) {
		this.monthAmount = monthAmount;
	}
	public BigDecimal getDayAmountApproved() {
		return dayAmountApproved;
	}
	public void setDayAmountApproved(BigDecimal dayAmountApproved) {
		this.dayAmountApproved = dayAmountApproved;
	}
	public BigDecimal getMonthAmountApproved() {
		return monthAmountApproved;
	}
	public void setMonthAmountApproved(BigDecimal monthAmountApproved) {
		this.monthAmountApproved = monthAmountApproved;
	}
	public Integer getDayQuantityPlan() {
		return dayQuantityPlan;
	}
	public void setDayQuantityPlan(Integer dayQuantityPlan) {
		this.dayQuantityPlan = dayQuantityPlan;
	}
	public Integer getDayQuantity() {
		return dayQuantity;
	}
	public void setDayQuantity(Integer dayQuantity) {
		this.dayQuantity = dayQuantity;
	}
	public Integer getDayQuantityApproved() {
		return dayQuantityApproved;
	}
	public void setDayQuantityApproved(Integer dayQuantityApproved) {
		this.dayQuantityApproved = dayQuantityApproved;
	}
	public Integer getMonthQuantityPlan() {
		return monthQuantityPlan;
	}
	public void setMonthQuantityPlan(Integer monthQuantityPlan) {
		this.monthQuantityPlan = monthQuantityPlan;
	}
	public Integer getMonthQuantity() {
		return monthQuantity;
	}
	public void setMonthQuantity(Integer monthQuantity) {
		this.monthQuantity = monthQuantity;
	}
	public Integer getMonthQuantityApproved() {
		return monthQuantityApproved;
	}
	public void setMonthQuantityApproved(Integer monthQuantityApproved) {
		this.monthQuantityApproved = monthQuantityApproved;
	}
	public List<ActionLogCustomerVO> getLstDetailActionLog() {
		return lstDetailActionLog;
	}
	public void setLstDetailActionLog(List<ActionLogCustomerVO> lstDetailActionLog) {
		this.lstDetailActionLog = lstDetailActionLog;
	}
	
}

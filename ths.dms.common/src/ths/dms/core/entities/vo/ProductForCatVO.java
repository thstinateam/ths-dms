/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

/**
 * Gom nhom nganh hang, nganh hang con, san pham
 * 
 * @author hunglm16
 * @since October 10, 2015
 */
public class ProductForCatVO implements Serializable {

	private static final long serialVersionUID = 4488396665144328036L;

	private Long id;
	private Long catId;
	private Long subCatId;
	private Long productId;

	private String code;
	private String catCode;
	private String subCatCode;
	private String productCode;
	private String name;
	private String catName;
	private String subCatName;
	private String productName;
	private String containers;

	private Integer gr;
	private Integer typeRow;
	
	public void safeSetNull() throws Exception {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Khai bao phuong thuc GETTER/SETTER
	 */
	public Long getId() {
		return id;
	}

	public Integer getGr() {
		return gr;
	}

	public void setGr(Integer gr) {
		this.gr = gr;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCatId() {
		return catId;
	}

	public void setCatId(Long catId) {
		this.catId = catId;
	}

	public Long getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(Long subCatId) {
		this.subCatId = subCatId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCatCode() {
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public String getSubCatCode() {
		return subCatCode;
	}

	public void setSubCatCode(String subCatCode) {
		this.subCatCode = subCatCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public String getSubCatName() {
		return subCatName;
	}

	public void setSubCatName(String subCatName) {
		this.subCatName = subCatName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getContainers() {
		return containers;
	}

	public void setContainers(String containers) {
		this.containers = containers;
	}

	public Integer getTypeRow() {
		return typeRow;
	}

	public void setTypeRow(Integer typeRow) {
		this.typeRow = typeRow;
	}

}

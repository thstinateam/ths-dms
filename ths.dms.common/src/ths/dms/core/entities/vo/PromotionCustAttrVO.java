package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.enumtype.AttributeColumnType;


public class PromotionCustAttrVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private Long objectId;
	
	private Integer objectType;
	
	private String name;
	//sontt them field de xu ly tren web:
	private List<?> listData;
	private AttributeColumnType valueType;
	//
	//field nay chi dung trong truong hop objectType=3
	List<ProductInfoVO> listProductInfoVO;
	//
	
	public Long getId() {
		return id;
	}

	public List<ProductInfoVO> getListProductInfoVO() {
		return listProductInfoVO;
	}

	public void setListProductInfoVO(List<ProductInfoVO> listProductInfoVO) {
		this.listProductInfoVO = listProductInfoVO;
	}

	public AttributeColumnType getValueType() {
		return valueType;
	}

	public void setValueType(AttributeColumnType valueType) {
		this.valueType = valueType;
	}


	public List<?> getListData() {
		return listData;
	}

	public void setListData(List<?> listData) {
		this.listData = listData;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public Integer getObjectType() {
		return objectType;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}

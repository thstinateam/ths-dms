package ths.dms.core.entities.vo;

import java.io.Serializable;
/**
 * Chi tiet thuoc tinh
 * @author vuongmq
 * @since 26/02/2015
 *
 */
public class StaffAttributeDetailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8837148003380968835L;
	
	private Long id;
	
	private Long staffAttId;
	
	private Long staffId;
	
	private String value;
	
	private Long staffAttEnumId;
	
	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStaffAttId() {
		return staffAttId;
	}

	public void setStaffAttId(Long staffAttId) {
		this.staffAttId = staffAttId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getStaffAttEnumId() {
		return staffAttEnumId;
	}

	public void setStaffAttEnumId(Long staffAttEnumId) {
		this.staffAttEnumId = staffAttEnumId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}


	
	

}

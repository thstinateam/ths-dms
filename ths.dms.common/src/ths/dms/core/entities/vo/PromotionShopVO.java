package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VO don vi thuoc CTKM
 * 
 * @author lacnv1
 * @since Aug 14, 2014
 */
public class PromotionShopVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long parentId; // id cua shop cha
	private String shopCode;
	private String shopName;
	private String promotionCode;
	private Integer isNPP;
	private Integer isEdit;
	private Integer isExists;
	private Integer isLevel;
	private BigDecimal quantity;
	private BigDecimal receivedQtt;
	private BigDecimal amountMax;
	private BigDecimal receivedAmt;
	private BigDecimal numMax;
	private BigDecimal receivedNum;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Integer getIsNPP() {
		return isNPP;
	}

	public void setIsNPP(Integer isNPP) {
		this.isNPP = isNPP;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getIsExists() {
		return isExists;
	}

	public void setIsExists(Integer isExists) {
		this.isExists = isExists;
	}

	public Integer getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Integer isEdit) {
		this.isEdit = isEdit;
	}

	public Integer getIsLevel() {
		return isLevel;
	}

	public void setIsLevel(Integer isLevel) {
		this.isLevel = isLevel;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public BigDecimal getAmountMax() {
		return amountMax;
	}

	public void setAmountMax(BigDecimal amountMax) {
		this.amountMax = amountMax;
	}

	public BigDecimal getReceivedAmt() {
		return receivedAmt;
	}

	public void setReceivedAmt(BigDecimal receivedAmt) {
		this.receivedAmt = receivedAmt;
	}

	public BigDecimal getNumMax() {
		return numMax;
	}

	public void setNumMax(BigDecimal numMax) {
		this.numMax = numMax;
	}

	public BigDecimal getReceivedNum() {
		return receivedNum;
	}

	public void setReceivedNum(BigDecimal receivedNum) {
		this.receivedNum = receivedNum;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getReceivedQtt() {
		return receivedQtt;
	}

	public void setReceivedQtt(BigDecimal receivedQtt) {
		this.receivedQtt = receivedQtt;
	}
	
}
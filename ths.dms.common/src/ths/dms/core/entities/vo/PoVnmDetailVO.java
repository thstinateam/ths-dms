package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class PoVnmDetailVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//productId
	private Long productId;
	
	//productCode
	private String productCode;
	
	//productName
	private String productName;
	
	//convfact
	private Integer convfact;
	
	//quantity
	private Integer quantity;
	
	//price
	private BigDecimal price;
	
	//amount
	private BigDecimal amount;
	
	//promotionProgrameCode
	private String promotionProgrameCode;
	
	//quantityStore
	private Integer quantityStore;
	
	//lot
	private String lot;
	

	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}


	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}


	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}


	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}


	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}


	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}


	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}


	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price == null ? BigDecimal.valueOf(0) : price;
	}


	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}


	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	/**
	 * @return the promotionProgrameCode
	 */
	public String getPromotionProgrameCode() {
		return promotionProgrameCode;
	}


	/**
	 * @param promotionProgrameCode the promotionProgrameCode to set
	 */
	public void setPromotionProgrameCode(String promotionProgrameCode) {
		this.promotionProgrameCode = promotionProgrameCode;
	}


	/**
	 * @return the quantityStore
	 */
	public Integer getQuantityStore() {
		if (quantityStore == null) {
			quantityStore = 0;
		}
		return quantityStore;
	}


	/**
	 * @param quantityStore the quantityStore to set
	 */
	public void setQuantityStore(Integer quantityStore) {
		this.quantityStore = quantityStore;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	/**
	 * @return the lot
	 */
	public String getLot() {
		return lot;
	}


	/**
	 * @param lot the lot to set
	 */
	public void setLot(String lot) {
		this.lot = lot;
	}


	/**
	 * @return the convfact
	 */
	public Integer getConvfact() {
		return convfact;
	}


	/**
	 * @param convfact the convfact to set
	 */
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
}

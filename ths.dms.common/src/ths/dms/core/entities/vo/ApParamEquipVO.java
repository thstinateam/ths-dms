package ths.dms.core.entities.vo;
/***
 * @vuongmq
 * ApParamEquipVO de lay danh sach VO
 * @date 22/05/2015
 */
import java.io.Serializable;

public class ApParamEquipVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String apParamCode;
	private String apParamName;
	private String description;
	private String type;
	private String value;
	private Integer status;
	
	/** lay email param repair to*/
	private Long shopId;
	private String shopName;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getApParamCode() {
		return apParamCode;
	}

	public void setApParamCode(String apParamCode) {
		this.apParamCode = apParamCode;
	}

	public String getApParamName() {
		return apParamName;
	}

	public void setApParamName(String apParamName) {
		this.apParamName = apParamName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
}
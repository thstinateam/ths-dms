package ths.dms.core.entities.vo;

import java.util.ArrayList;

public class ListGroupMua extends ArrayList<GroupMua> {
	@Override
	public int indexOf(Object obj) {
		if(obj instanceof Long) {
			long levelIndex = ((Long) obj).longValue();
			for(int i = 0; i < this.size(); i++) {
				for(GroupSP __obj : this.get(i).lstLevel) {
					if(__obj.index == levelIndex) {
						return i;
					}
				}
			}
			return -1;
		} else {
			return super.indexOf(obj);
		}
	}
	
	public GroupMua searchNode(Node node) {
		for(GroupMua groupMua : this) {
			for(GroupSP groupSP : groupMua.lstLevel) {
				for(int i = 0; i < groupSP.lstSP.size(); i++) {
					if(node.productCode != null && !node.productCode.equals("") && node.productCode.equals(groupSP.lstSP.get(i).productCode) && node.quantity != null && node.quantity.equals(groupSP.lstSP.get(i).quantity)) {
						return groupMua;
					} else if(node.productCode != null && !node.productCode.equals("") && node.productCode.equals(groupSP.lstSP.get(i).productCode) && node.amount != null && node.amount.equals(groupSP.lstSP.get(i).amount)) {
						return groupMua;
					} else if(node.amount != null && node.amount.equals(groupSP.lstSP.get(i).amount)) {
						return groupMua;
					}
				}
			}
		}
		return null;
	}
}

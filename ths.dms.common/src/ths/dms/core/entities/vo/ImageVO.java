package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImageVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1956654986662515109L;
	private Long id;
	private Long gsId;
	private Long nvbhId;
	private String nvbhCode;
	private String nvbhName;
	private String gsCode;
	private String gsName;
	private String urlImage;
	private String urlThum;
	private Date createDate;
	private String hhmmDate;
	private String dmyDate;
	private Float lat;
	private Float lng;
	private Integer countImg;
	private String displayProgrameCode;
	private String displayProgrameName;
	private Long displayProgrameId;
	private Long customerId;
	private String customerCode;
	private String customerName;
	private String street;
	private String housenumber;
	private String customerAddress;
	private String shopName;
	private String shopCode;
	private Long staffId;
	private String staffCode;
	private String staffName;
	private String createDateStr;
	private Integer role;
	private Integer result;
	private String levelCode;
	//private Integer monthSeq;
	private Float custLat;
	private Float custLng;
	private Integer objectType;
	private Integer toCycle;
	private Integer fromCycle;
	private String equipCode;
	private String seriNumber;
	private String equipCategoryName;
	private String equipCodeGroup;
	private String equipNameGroup;
	private Integer numberProduct;
	private String POSM;
	private String displayNote;
	private Integer isInSpected;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUrlImage() {
		return urlImage;
	}
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}
	public String getUrlThum() {
		return urlThum;
	}
	public void setUrlThum(String urlThum) {
		this.urlThum = urlThum;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Float getLat() {
		return lat;
	}
	public void setLat(Float lat) {
		this.lat = lat;
	}
	public Float getLng() {
		return lng;
	}
	public void setLng(Float lng) {
		this.lng = lng;
	}
	public Integer getCountImg() {
		return countImg;
	}
	public void setCountImg(Integer countImg) {
		this.countImg = countImg;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getDisplayProgrameCode() {
		return displayProgrameCode;
	}
	public void setDisplayProgrameCode(String displayProgrameCode) {
		this.displayProgrameCode = displayProgrameCode;
	}
	public String getDisplayProgrameName() {
		return displayProgrameName;
	}
	public void setDisplayProgrameName(String displayProgrameName) {
		this.displayProgrameName = displayProgrameName;
	}
	public Long getDisplayProgrameId() {
		return displayProgrameId;
	}
	public void setDisplayProgrameId(Long displayProgrameId) {
		this.displayProgrameId = displayProgrameId;
	}
	public String getHhmmDate() {
		return hhmmDate;
	}
	public void setHhmmDate(String hhmmDate) {
		this.hhmmDate = hhmmDate;
	}
	public String getDmyDate() {
		return dmyDate;
	}
	public void setDmyDate(String dmyDate) {
		this.dmyDate = dmyDate;
	}
	public String getCreateDateStr() {
		if(this.getCreateDate() !=  null) {
			String ddmmyyyy = new SimpleDateFormat("dd/MM/yyyy").format(this.getCreateDate());
			String hhmm = new SimpleDateFormat("HH:mm").format(this.getCreateDate());
			return ddmmyyyy +"-"+hhmm;
		}
		return "";
	}
	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}
	public void setRole(Integer role) {
		this.role = role;
	}
	public Integer getRole() {
		return role;
	}
	public String getCustomerAddress() {
		String customerTmp ="";
		if(this.housenumber != null) {
			customerTmp += this.housenumber;
		}
		if(this.street != null) {
			customerTmp += " " + this.street;
		}
		return customerTmp;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHousenumber() {
		return housenumber;
	}
	public void setHousenumber(String housenumber) {
		this.housenumber = housenumber;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Integer getResult() {
		return result;
	}
	public void setResult(Integer result) {
		this.result = result;
	}
	public String getLevelCode() {
		return levelCode;
	}
	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}
	/*public Integer getMonthSeq() {
		return monthSeq;
	}
	public void setMonthSeq(Integer monthSeq) {
		this.monthSeq = monthSeq;
	}*/
	public Float getCustLat() {
		return custLat;
	}
	public void setCustLat(Float custLat) {
		this.custLat = custLat;
	}
	public Float getCustLng() {
		return custLng;
	}
	public void setCustLng(Float custLng) {
		this.custLng = custLng;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public Integer getToCycle() {
		return toCycle;
	}
	public void setToCycle(Integer toCycle) {
		this.toCycle = toCycle;
	}
	public Integer getFromCycle() {
		return fromCycle;
	}
	public void setFromCycle(Integer fromCycle) {
		this.fromCycle = fromCycle;
	}
	public Long getGsId() {
		return gsId;
	}
	public void setGsId(Long gsId) {
		this.gsId = gsId;
	}
	public String getGsCode() {
		return gsCode;
	}
	public void setGsCode(String gsCode) {
		this.gsCode = gsCode;
	}
	public String getGsName() {
		return gsName;
	}
	public void setGsName(String gsName) {
		this.gsName = gsName;
	}
	public Long getNvbhId() {
		return nvbhId;
	}
	public void setNvbhId(Long nvbhId) {
		this.nvbhId = nvbhId;
	}
	public String getNvbhCode() {
		return nvbhCode;
	}
	public void setNvbhCode(String nvbhCode) {
		this.nvbhCode = nvbhCode;
	}
	public String getNvbhName() {
		return nvbhName;
	}
	public void setNvbhName(String nvbhName) {
		this.nvbhName = nvbhName;
	}
	public String getEquipCode() {
		return equipCode;
	}
	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}
	public String getSeriNumber() {
		return seriNumber;
	}
	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}
	public String getEquipCategoryName() {
		return equipCategoryName;
	}
	public void setEquipCategoryName(String equipCategoryName) {
		this.equipCategoryName = equipCategoryName;
	}
	public Integer getNumberProduct() {
		return numberProduct;
	}
	public void setNumberProduct(Integer numberProduct) {
		this.numberProduct = numberProduct;
	}
	public String getPOSM() {
		return POSM;
	}
	public void setPOSM(String pOSM) {
		POSM = pOSM;
	}
	public String getDisplayNote() {
		return displayNote;
	}
	public void setDisplayNote(String displayNote) {
		this.displayNote = displayNote;
	}
	public Integer getIsInSpected() {
		return isInSpected;
	}
	public void setIsInSpected(Integer isInSpected) {
		this.isInSpected = isInSpected;
	}
	public String getEquipCodeGroup() {
		return equipCodeGroup;
	}
	public void setEquipCodeGroup(String equipCodeGroup) {
		this.equipCodeGroup = equipCodeGroup;
	}
	public String getEquipNameGroup() {
		return equipNameGroup;
	}
	public void setEquipNameGroup(String equipNameGroup) {
		this.equipNameGroup = equipNameGroup;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;

public class EquipmentCodeVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Id thiet bi */
	private Long idEquipment;
	
	/** Ma thiet bi */
	private String equipmentCode;
	
	/** So seri */
	private String seriNumber;

	public Long getIdEquipment() {
		return idEquipment;
	}

	public void setIdEquipment(Long idEquipment) {
		this.idEquipment = idEquipment;
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}

}

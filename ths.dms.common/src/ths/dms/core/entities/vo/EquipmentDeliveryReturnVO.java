package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.EquipDeliveryRecord;
import ths.dms.core.entities.Equipment;

public class EquipmentDeliveryReturnVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EquipDeliveryRecord equipDeliveryRevord;
	private List<Equipment> lstEquipment;
	public EquipDeliveryRecord getEquipDeliveryRevord() {
		return equipDeliveryRevord;
	}
	public void setEquipDeliveryRevord(EquipDeliveryRecord equipDeliveryRevord) {
		this.equipDeliveryRevord = equipDeliveryRevord;
	}
	public List<Equipment> getLstEquipment() {
		return lstEquipment;
	}
	public void setLstEquipment(List<Equipment> lstEquipment) {
		this.lstEquipment = lstEquipment;
	}
}

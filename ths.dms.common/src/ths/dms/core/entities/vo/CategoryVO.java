package ths.dms.core.entities.vo;

import java.io.Serializable;

public class CategoryVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String catCode;
	private String catName;
	private String subCatCode;
	private String subCatName;

	public String getCatCode() {
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public String getSubCatCode() {
		return subCatCode;
	}

	public void setSubCatCode(String subCatCode) {
		this.subCatCode = subCatCode;
	}

	public String getSubCatName() {
		return subCatName;
	}

	public void setSubCatName(String subCatName) {
		this.subCatName = subCatName;
	}

}

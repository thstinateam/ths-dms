/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

/**
 * Lop thuc hien cho cac chuc nang lien quan den lich su Khach hang CTHTTM
 * @author hunglm16
 * @since 24/11/2015
 */
public class KeyShopCustomerHistoryVO implements Serializable {
	
	private static final long serialVersionUID = 4488396665144328036L;
	
	private String keyShopName;
	private String cycleName;
	private String levelName;
	private String userName;
	private String createDateStr;
	private BigDecimal multiplyer;
	
	/**
	 * Phuong thuc xu ly cac gia tri rong & null 
	 * @author hunglm16
	 * @throws Exception
	 * @since 24/11/2015
	 */
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	/**
	 * Khai bao cac phuogn thuc GETTER/SETTER
	 * @author hunglm16
	 * @throws Exception
	 * @since 24/11/2015
	 */
	public String getKeyShopName() {
		return keyShopName;
	}

	public void setKeyShopName(String keyShopName) {
		this.keyShopName = keyShopName;
	}

	public String getCycleName() {
		return cycleName;
	}

	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public BigDecimal getMultiplyer() {
		return multiplyer;
	}

	public void setMultiplyer(BigDecimal multiplyer) {
		this.multiplyer = multiplyer;
	}
	
}

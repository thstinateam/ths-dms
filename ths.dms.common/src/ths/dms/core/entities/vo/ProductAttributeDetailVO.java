package ths.dms.core.entities.vo;

import java.io.Serializable;
/**
 * Chi tiet thuoc tinh
 * @author tientv11
 * @since 16/01/2015
 *
 */
public class ProductAttributeDetailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8837148003380968835L;
	
	private Long id;
	
	private Long productAttId;
	
	private Long productId;
	
	private String value;
	
	private Long productAttEnumId;
	
	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductAttId() {
		return productAttId;
	}

	public void setProductAttId(Long productAttId) {
		this.productAttId = productAttId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getProductAttEnumId() {
		return productAttEnumId;
	}

	public void setProductAttEnumId(Long productAttEnumId) {
		this.productAttEnumId = productAttEnumId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	

}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TimeVisitCustomerVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private Date startTime;
	private Date endTime;
	private Date toDate;
	private String staffCode;
	private String staffName;
	private String shopCode;
	private String shopName;
	private String superName;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String superShopCode;
	private String parentSuperShopCode;
	
	private String CLOSE;
	private BigDecimal distance;
	
	private String tuyen;
	
	private BigDecimal phut;
	private BigDecimal giay;
	
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getSuperName() {
		return superName;
	}
	public void setSuperName(String superName) {
		this.superName = superName;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getSuperShopCode() {
		return superShopCode;
	}
	public void setSuperShopCode(String superShopCode) {
		this.superShopCode = superShopCode;
	}
	public String getParentSuperShopCode() {
		if ((parentSuperShopCode == null || parentSuperShopCode.length() < 1)
				&& (shopCode == null || shopCode.length() < 1)) {
			return "Tổng";
		}
		return parentSuperShopCode;
	}
	public void setParentSuperShopCode(String parentSuperShopCode) {
		this.parentSuperShopCode = parentSuperShopCode;
	}
	public String getCLOSE() {
		return CLOSE;
	}
	public void setCLOSE(String cLOSE) {
		CLOSE = cLOSE;
	}
	public BigDecimal getDistance() {
		return distance;
	}
	public void setDistance(BigDecimal distance) {
		this.distance = distance;
	}
	public String getTuyen() {
		return tuyen;
	}
	public void setTuyen(String tuyen) {
		this.tuyen = tuyen;
	}
	public BigDecimal getPhut() {
		return phut;
	}
	public void setPhut(BigDecimal phut) {
		this.phut = phut;
	}
	public BigDecimal getGiay() {
		return giay;
	}
	public void setGiay(BigDecimal giay) {
		this.giay = giay;
	}
	
}

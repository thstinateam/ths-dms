package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Lop doi tuong gia san pham
 * 
 * @author hunglm16
 * @return Class VO Price
 * @since August 15,2014
 * @description Khong them doi tuong la Object vao VO nay
 */
public class PriceVO implements Serializable {
	private static final long serialVersionUID = 4488396665144328036L;

	private Long id;
	private Long shopTypeId;
	private Long shopId;
	private Long customerTypeId;
	private Long customerId;
	private Long productId;
	private Float vat;
	private BigDecimal pricePackage;
	private BigDecimal pricePackageNotVAT;
	private BigDecimal price;
	private BigDecimal priceNotVAT;
	private String shopTypeName;
	private String shopName;
	private String customerTypeName;
	private String customerName;
	private String shopAppCode;
	private String shopCode;
	private String typeProductStr;
	private String productCode;
	private String productName;
	private String fromDateStr;
	private String toDateStr;
	private String typeCustomerStr;
	private Integer status;
	private Integer isShopApp;
	private String statusStr;
	private Integer shopChannel;
	
	public Integer getShopChannel() {
		return shopChannel;
	}
	public void setShopChannel(Integer shopChannel) {
		this.shopChannel = shopChannel;
	}
	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public Long getShopTypeId() {
		return shopTypeId;
	}
	public void setShopTypeId(Long shopTypeId) {
		this.shopTypeId = shopTypeId;
	}
	public Long getCustomerTypeId() {
		return customerTypeId;
	}
	public void setCustomerTypeId(Long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getShopTypeName() {
		return shopTypeName;
	}
	public void setShopTypeName(String shopTypeName) {
		this.shopTypeName = shopTypeName;
	}
	public String getCustomerTypeName() {
		return customerTypeName;
	}
	public void setCustomerTypeName(String customerTypeName) {
		this.customerTypeName = customerTypeName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Integer getIsShopApp() {
		return isShopApp;
	}
	public void setIsShopApp(Integer isShopApp) {
		this.isShopApp = isShopApp;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Float getVat() {
		return vat;
	}
	public void setVat(Float vat) {
		this.vat = vat;
	}
	public String getTypeCustomerStr() {
		return typeCustomerStr;
	}
	public void setTypeCustomerStr(String typeCustomerStr) {
		this.typeCustomerStr = typeCustomerStr;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getShopAppCode() {
		return shopAppCode;
	}
	public void setShopAppCode(String shopAppCode) {
		this.shopAppCode = shopAppCode;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getTypeProductStr() {
		return typeProductStr;
	}
	public void setTypeProductStr(String typeProductStr) {
		this.typeProductStr = typeProductStr;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getFromDateStr() {
		return fromDateStr;
	}
	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}
	public String getToDateStr() {
		return toDateStr;
	}
	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getPriceNotVAT() {
		return priceNotVAT;
	}
	public void setPriceNotVAT(BigDecimal priceNotVAT) {
		this.priceNotVAT = priceNotVAT;
	}
	public BigDecimal getPricePackage() {
		return pricePackage;
	}
	public void setPricePackage(BigDecimal pricePackage) {
		this.pricePackage = pricePackage;
	}
	public BigDecimal getPricePackageNotVAT() {
		return pricePackageNotVAT;
	}
	public void setPricePackageNotVAT(BigDecimal pricePackageNotVAT) {
		this.pricePackageNotVAT = pricePackageNotVAT;
	}
}

package ths.dms.core.entities.vo;
/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.EquipSalePlan;

/**
 * Class Equipment VO
 * 
 * @author hoanv25
 * @since December 10,2014
 * 
 * @author hunglm16
 * @since December 17,2014
 * @description Them thuoc tinh co trong bang Equiment
 */
public class EquipmentVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long shopId;
	private Long equipId; 
	private Long stockId; 
	private Long equipGroupId; 
	private Long equipProviderId; 
	private Long equiprepair;
	private Long equipitem;
	private Long equipDeliveryRecordId;
	private Long equipCategoryId;
	private Long customerTypeId;
	
	/** hoanv25 - May 13,2015; dung cho bao cao thiet bi */
	private Long repairId;
	private Long repairIdDtl;
	private Long periodId;
	/** end */

	private BigDecimal price; 
	private BigDecimal fromCapacity;
	private BigDecimal toCapacity;
	private BigDecimal amount;
	
	private BigDecimal priceActually; // vuongmq; 07/05/2014; lay cho ds phieu bao mat
	
	private Integer type;
	private Integer typeHMSC; //dung cho bảng equip_item truong type
	private Integer status;
	private Integer performStatus;
	private String statusStr;
	private Integer stockType; 
	private Integer tradeType; 
	private Integer tradeStatus; 
	private Integer usageStatus;
	private Integer countFile;
	private Integer liquidationStatus;

	private Integer quantity;
	private Integer fromMonth;
	private Integer toMonth;
	private Integer customerType;
	private String customerTypeStr;
	private Integer totalQuantity;
//	private Integer amount;
	private Integer manufacturingYear; 
	private Integer warranty; 
	private Integer repairCount;
	private Integer repairCountDtl;
	private Date createDate;
	private Date updateDate;
	private Date firstDateInUse; 
	private Date warrantyExpiredDate;

	private String code;
	private String note;
	private String name;
	private String codeName;
	private String shopCode;
	private String shopName;
	private String brandName;
	private String typeGroup;
	private String serial; 
	private String content;
	private String customerTypeCode;
	private String customerTypeName;
	private String capacityStr;
	private String stockCode; 
	private String createUser;
	private String updateUser;
	private String healthStatus; 
	private String usageStatusStr;
	private String equipBrandName;
	private String equipGroupName;
	private String equipGroupCode;
	private String equipCategoryName;
	private String equipProviderName;
	private String firstDateInUseStr; 
	private String manufacturingYearStr; 
	private String warrantyExpiredDateStr;
	private String groupEquip;
	private String fromStock;
	private String toStock;
	private String url;
	private String capacity;
	private String capacityFrom;
	private String capacityTo;
	private String equipCode;	
	private String equipCategoryCode;
	private String creatDate;
	private String contractNumber;
	private String period;
	private Integer indexRow;
	private String equipItem;
	private String equipImportRecordCode;
	private Long equipImportRecordId;
	private String createDateStr;
	private String stockName;
	private String customerName;
	private String shortCode;
	private String EquipLendCode;
	private String customerAddress;
	private String staffCode;
	private String createFormDate;
	private String fromDate;
	private String toDate;
	private Integer statusCus;
	
	private String codeMobile; // lay bao mat cua mobile
	private List<EquipSalePlan> lstEquipSalePlan;
	
	
	/**
	 * Khai bao GETTER/SETTER
	 * */
	public String getUsageStatusStr() {
		return usageStatusStr;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Long getEquipCategoryId() {
		return equipCategoryId;
	}

	public void setEquipCategoryId(Long equipCategoryId) {
		this.equipCategoryId = equipCategoryId;
	}

	public String getEquipCategoryCode() {
		return equipCategoryCode;
	}

	public void setEquipCategoryCode(String equipCategoryCode) {
		this.equipCategoryCode = equipCategoryCode;
	}

	public Long getEquipDeliveryRecordId() {
		return equipDeliveryRecordId;
	}

	public void setEquipDeliveryRecordId(Long equipDeliveryRecordId) {
		this.equipDeliveryRecordId = equipDeliveryRecordId;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getCapacityStr() {
		return capacityStr;
	}

	public void setCapacityStr(String capacityStr) {
		this.capacityStr = capacityStr;
	}

	public void setUsageStatusStr(String usageStatusStr) {
		this.usageStatusStr = usageStatusStr;
	}

	public String getEquipGroupCode() {
		return equipGroupCode;
	}

	public void setEquipGroupCode(String equipGroupCode) {
		this.equipGroupCode = equipGroupCode;
	}

	public String getEquipGroupName() {
		return equipGroupName;
	}

	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}

	public String getEquipCategoryName() {
		return equipCategoryName;
	}

	public void setEquipCategoryName(String equipCategoryName) {
		this.equipCategoryName = equipCategoryName;
	}

	public String getEquipBrandName() {
		return equipBrandName;
	}

	public void setEquipBrandName(String equipBrandName) {
		this.equipBrandName = equipBrandName;
	}

	public String getEquipProviderName() {
		return equipProviderName;
	}

	public void setEquipProviderName(String equipProviderName) {
		this.equipProviderName = equipProviderName;
	}

	public String getFirstDateInUseStr() {
		return firstDateInUseStr;
	}

	public void setFirstDateInUseStr(String firstDateInUseStr) {
		this.firstDateInUseStr = firstDateInUseStr;
	}

	public String getManufacturingYearStr() {
		return manufacturingYearStr;
	}

	public void setManufacturingYearStr(String manufacturingYearStr) {
		this.manufacturingYearStr = manufacturingYearStr;
	}

	public String getWarrantyExpiredDateStr() {
		return warrantyExpiredDateStr;
	}

	public void setWarrantyExpiredDateStr(String warrantyExpiredDateStr) {
		this.warrantyExpiredDateStr = warrantyExpiredDateStr;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Long getEquipGroupId() {
		return equipGroupId;
	}

	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}

	public Long getEquipProviderId() {
		return equipProviderId;
	}

	public void setEquipProviderId(Long equipProviderId) {
		this.equipProviderId = equipProviderId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getStockType() {
		return stockType;
	}

	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}

	public Integer getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(Integer tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public Integer getTradeType() {
		return tradeType;
	}

	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}

	public Integer getUsageStatus() {
		return usageStatus;
	}

	public void setUsageStatus(Integer usageStatus) {
		this.usageStatus = usageStatus;
	}

	public Date getFirstDateInUse() {
		return firstDateInUse;
	}

	public void setFirstDateInUse(Date firstDateInUse) {
		this.firstDateInUse = firstDateInUse;
	}

	public Integer getManufacturingYear() {
		return manufacturingYear;
	}

	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}

	public Date getWarrantyExpiredDate() {
		return warrantyExpiredDate;
	}

	public void setWarrantyExpiredDate(Date warrantyExpiredDate) {
		this.warrantyExpiredDate = warrantyExpiredDate;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getTypeGroup() {
		return typeGroup;
	}

	public void setTypeGroup(String typeGroup) {
		this.typeGroup = typeGroup;
	}

	public Integer getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(Integer fromMonth) {
		this.fromMonth = fromMonth;
	}

	public Integer getToMonth() {
		return toMonth;
	}

	public void setToMonth(Integer toMonth) {
		this.toMonth = toMonth;
	}

	public Integer getCustomerType() {
		return customerType;
	}

	public void setCustomerType(Integer customerType) {
		this.customerType = customerType;
	}

	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getCustomerTypeName() {
		return customerTypeName;
	}

	public void setCustomerTypeName(String customerTypeName) {
		this.customerTypeName = customerTypeName;
	}

	public String getGroupEquip() {
		return groupEquip;
	}

	public void setGroupEquip(String groupEquip) {
		this.groupEquip = groupEquip;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	public String getCreatDate() {
		return creatDate;
	}

	public void setCreatDate(String creatDate) {
		this.creatDate = creatDate;
	}

	public String getFromStock() {
		return fromStock;
	}

	public void setFromStock(String fromStock) {
		this.fromStock = fromStock;
	}

	public String getToStock() {
		return toStock;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setToStock(String toStock) {
		this.toStock = toStock;
	}

	public Long getEquiprepair() {
		return equiprepair;
	}

	public void setEquiprepair(Long equiprepair) {
		this.equiprepair = equiprepair;
	}

	public Long getEquipitem() {
		return equipitem;
	}

	public void setEquipitem(Long equipitem) {
		this.equipitem = equipitem;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public BigDecimal getFromCapacity() {
		return fromCapacity;
	}

	public BigDecimal getToCapacity() {
		return toCapacity;
	}

	public void setFromCapacity(BigDecimal fromCapacity) {
		this.fromCapacity = fromCapacity;
	}

	public void setToCapacity(BigDecimal toCapacity) {
		this.toCapacity = toCapacity;
	}

	public String getCapacityFrom() {
		return capacityFrom;
	}

	public void setCapacityFrom(String capacityFrom) {
		this.capacityFrom = capacityFrom;
	}

	public String getCapacityTo() {
		return capacityTo;
	}

	public void setCapacityTo(String capacityTo) {
		this.capacityTo = capacityTo;
	}

	public String getCustomerTypeStr() {
		return customerTypeStr;
	}

	public void setCustomerTypeStr(String customerTypeStr) {
		this.customerTypeStr = customerTypeStr;
	}

	public Integer getTypeHMSC() {
		return typeHMSC;
	}

	public void setTypeHMSC(Integer typeHMSC) {
		this.typeHMSC = typeHMSC;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public List<EquipSalePlan> getLstEquipSalePlan() {
		return lstEquipSalePlan;
	}

	public void setLstEquipSalePlan(List<EquipSalePlan> lstEquipSalePlan) {
		this.lstEquipSalePlan = lstEquipSalePlan;
	}

	public Integer getIndexRow() {
		return indexRow;
	}

	public void setIndexRow(Integer indexRow) {
		this.indexRow = indexRow;
	}

	public Integer getWarranty() {
		return warranty;
	}

	public void setWarranty(Integer warranty) {
		this.warranty = warranty;
	}

	public String getEquipImportRecordCode() {
		return equipImportRecordCode;
	}

	public void setEquipImportRecordCode(String equipImportRecordCode) {
		this.equipImportRecordCode = equipImportRecordCode;
	}

	public Long getEquipImportRecordId() {
		return equipImportRecordId;
	}

	public void setEquipImportRecordId(Long equipImportRecordId) {
		this.equipImportRecordId = equipImportRecordId;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Integer getPerformStatus() {
		return performStatus;
	}

	public void setPerformStatus(Integer performStatus) {
		this.performStatus = performStatus;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPriceActually() {
		return priceActually;
	}

	public void setPriceActually(BigDecimal priceActually) {
		this.priceActually = priceActually;
	}

	public String getEquipLendCode() {
		return EquipLendCode;
	}

	public void setEquipLendCode(String equipLendCode) {
		EquipLendCode = equipLendCode;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getRepairId() {
		return repairId;
	}

	public void setRepairId(Long repairId) {
		this.repairId = repairId;
	}

	public Long getRepairIdDtl() {
		return repairIdDtl;
	}

	public void setRepairIdDtl(Long repairIdDtl) {
		this.repairIdDtl = repairIdDtl;
	}

	public Long getPeriodId() {
		return periodId;
	}

	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	public Integer getRepairCount() {
		return repairCount;
	}

	public void setRepairCount(Integer repairCount) {
		this.repairCount = repairCount;
	}

	public Integer getRepairCountDtl() {
		return repairCountDtl;
	}

	public void setRepairCountDtl(Integer repairCountDtl) {
		this.repairCountDtl = repairCountDtl;
	}

	public String getEquipItem() {
		return equipItem;
	}

	public void setEquipItem(String equipItem) {
		this.equipItem = equipItem;
	}

	public String getCodeMobile() {
		return codeMobile;
	}

	public void setCodeMobile(String codeMobile) {
		this.codeMobile = codeMobile;
	}

	public Integer getCountFile() {
		return countFile;
	}

	public void setCountFile(Integer countFile) {
		this.countFile = countFile;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public Integer getLiquidationStatus() {
		return liquidationStatus;
	}

	public void setLiquidationStatus(Integer liquidationStatus) {
		this.liquidationStatus = liquidationStatus;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCreateFormDate() {
		return createFormDate;
	}

	public void setCreateFormDate(String createFormDate) {
		this.createFormDate = createFormDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getStatusCus() {
		return statusCus;
	}

	public void setStatusCus(Integer statusCus) {
		this.statusCus = statusCus;
	}

	public Long getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(Long customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public String getCustomerTypeCode() {
		return customerTypeCode;
	}

	public void setCustomerTypeCode(String customerTypeCode) {
		this.customerTypeCode = customerTypeCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class NewProductGroupVO implements Serializable {
	Long promotionId;
	Long groupMuaId;
	Long groupKMId;
	String groupMuaCode;
	String groupKMCode;
	String groupMuaName;
	String groupKMName;
	Integer minQuantityMua;
	BigDecimal minAmountMua;
	Integer maxQuantityKM;
	BigDecimal maxAmountKM;
	Integer stt;
	Integer multiple;
	Integer recursive;
	public Long getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}
	public Long getGroupMuaId() {
		return groupMuaId;
	}
	public void setGroupMuaId(Long groupMuaId) {
		this.groupMuaId = groupMuaId;
	}
	public Long getGroupKMId() {
		return groupKMId;
	}
	public void setGroupKMId(Long groupKMId) {
		this.groupKMId = groupKMId;
	}
	public String getGroupMuaCode() {
		return groupMuaCode;
	}
	public void setGroupMuaCode(String groupMuaCode) {
		this.groupMuaCode = groupMuaCode;
	}
	public String getGroupKMCode() {
		return groupKMCode;
	}
	public void setGroupKMCode(String groupKMCode) {
		this.groupKMCode = groupKMCode;
	}
	public String getGroupMuaName() {
		return groupMuaName;
	}
	public void setGroupMuaName(String groupMuaName) {
		this.groupMuaName = groupMuaName;
	}
	public String getGroupKMName() {
		return groupKMName;
	}
	public void setGroupKMName(String groupKMName) {
		this.groupKMName = groupKMName;
	}
	public Integer getMinQuantityMua() {
		return minQuantityMua;
	}
	public void setMinQuantityMua(Integer minQuantityMua) {
		this.minQuantityMua = minQuantityMua;
	}
	public BigDecimal getMinAmountMua() {
		return minAmountMua;
	}
	public void setMinAmountMua(BigDecimal minAmountMua) {
		this.minAmountMua = minAmountMua;
	}
	public Integer getMaxQuantityKM() {
		return maxQuantityKM;
	}
	public void setMaxQuantityKM(Integer maxQuantityKM) {
		this.maxQuantityKM = maxQuantityKM;
	}
	public BigDecimal getMaxAmountKM() {
		return maxAmountKM;
	}
	public void setMaxAmountKM(BigDecimal maxAmountKM) {
		this.maxAmountKM = maxAmountKM;
	}
	public Integer getStt() {
		return stt;
	}
	public void setStt(Integer stt) {
		this.stt = stt;
	}
	public Integer getMultiple() {
		return multiple;
	}
	public void setMultiple(Integer multiple) {
		this.multiple = multiple;
	}
	public Integer getRecursive() {
		return recursive;
	}
	public void setRecursive(Integer recursive) {
		this.recursive = recursive;
	}
}

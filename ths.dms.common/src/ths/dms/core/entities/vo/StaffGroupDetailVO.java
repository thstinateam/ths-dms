package ths.dms.core.entities.vo;

import java.io.Serializable;

public class StaffGroupDetailVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long shopId;
	private String shopCode;
	private Long groupId;
	private Long groupDetailId;
	private Long staffId;
	private String staffCode;
	private String staffName;
	private String mobile;
	private String staffType;
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public Long getGroupId() {
		return groupId;
	}
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	public Long getGroupDetailId() {
		return groupDetailId;
	}
	public void setGroupDetailId(Long groupDetailId) {
		this.groupDetailId = groupDetailId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getStaffType() {
		return staffType;
	}
	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}
}
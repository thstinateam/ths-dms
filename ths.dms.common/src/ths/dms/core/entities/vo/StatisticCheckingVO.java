package ths.dms.core.entities.vo;

import java.io.Serializable;

/**
 * @author phut
 */
public class StatisticCheckingVO implements Serializable {

	private static final long serialVersionUID = -7537768741081530734L;
	
	private Long detailId;
	private Long equipId;
	private Long shopId;
	private Long stockId;
	private Long stockTotalId;
	private Long equipGroupId;
	
	private String shopCode;
	private String shopName;
	private String shortCode;
	private String customerCode;
	private String customerName;
	private String address;
	private String stockCode;
	private String stockName;
	private String equipCode;
	private String equipName;
	private String staffCode;
	private String staffName;
	private String seri;
	private String equipNameGroup;
	private String equipCodeGroup;
	private String equipCategoryName;
	private String dateInWeek;
	private String checkDate;
	private String statisticDate;
	private String cusCodeAndName;
	
	private Integer status;
	private Integer stepCheck;
	private Integer isBelong;
	private Integer quantity;
	private Integer actualQuantity;
	private Integer numImage;
	private Integer flagApptachFile;

	
	/**
	 * Khai bao GETTER/SETTER
	 * */
	public Long getDetailId() {
		return detailId;
	}
	public void setDetailId(Long detailId) {
		this.detailId = detailId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEquipCode() {
		return equipCode;
	}
	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}
	public String getEquipName() {
		return equipName;
	}
	public void setEquipName(String equipName) {
		this.equipName = equipName;
	}
	public String getSeri() {
		return seri;
	}
	public void setSeri(String seri) {
		this.seri = seri;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getEquipNameGroup() {
		return equipNameGroup;
	}
	public void setEquipNameGroup(String equipNameGroup) {
		this.equipNameGroup = equipNameGroup;
	}
	public String getEquipCodeGroup() {
		return equipCodeGroup;
	}
	public void setEquipCodeGroup(String equipCodeGroup) {
		this.equipCodeGroup = equipCodeGroup;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public String getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	public Integer getStepCheck() {
		return stepCheck;
	}
	public void setStepCheck(Integer stepCheck) {
		this.stepCheck = stepCheck;
	}
	public Integer getIsBelong() {
		return isBelong;
	}
	public void setIsBelong(Integer isBelong) {
		this.isBelong = isBelong;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getActualQuantity() {
		return actualQuantity;
	}
	public void setActualQuantity(Integer actualQuantity) {
		this.actualQuantity = actualQuantity;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	public Long getStockTotalId() {
		return stockTotalId;
	}
	public void setStockTotalId(Long stockTotalId) {
		this.stockTotalId = stockTotalId;
	}
	public Long getEquipId() {
		return equipId;
	}
	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}
	public Long getEquipGroupId() {
		return equipGroupId;
	}
	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}
	public Integer getNumImage() {
		return numImage;
	}
	public void setNumImage(Integer numImage) {
		this.numImage = numImage;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getDateInWeek() {
		return dateInWeek;
	}
	public void setDateInWeek(String dateInWeek) {
		this.dateInWeek = dateInWeek;
	}
	public String getStatisticDate() {
		return statisticDate;
	}
	public void setStatisticDate(String statisticDate) {
		this.statisticDate = statisticDate;
	}
	public Integer getFlagApptachFile() {
		return flagApptachFile;
	}
	public void setFlagApptachFile(Integer flagApptachFile) {
		this.flagApptachFile = flagApptachFile;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getCusCodeAndName() {
		return cusCodeAndName;
	}
	public void setCusCodeAndName(String cusCodeAndName) {
		this.cusCodeAndName = cusCodeAndName;
	}
	public String getEquipCategoryName() {
		return equipCategoryName;
	}
	public void setEquipCategoryName(String equipCategoryName) {
		this.equipCategoryName = equipCategoryName;
	}
	
}

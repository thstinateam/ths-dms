package ths.dms.core.entities.vo;
/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Class Equipment VO
 * 
 * @author hoanv25
 * @since December 10,2014
 */
public class EquipmentRoleStockTempVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long idDetail;
	private Long idStock;
	private Long idEdit;
	private Long id;
	
	public Long getIdDetail() {
		return idDetail;
	}
	public Long getIdStock() {
		return idStock;
	}
	public void setIdDetail(Long idDetail) {
		this.idDetail = idDetail;
	}
	public void setIdStock(Long idStock) {
		this.idStock = idStock;
	}
	public Long getIdEdit() {
		return idEdit;
	}
	public void setIdEdit(Long idEdit) {
		this.idEdit = idEdit;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
}

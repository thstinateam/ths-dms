package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.entities.GroupLevelDetail;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.SaleOrderPromotion;

public class SaleOrderDetailVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 // KM cho mat hang ban
    public static final int PROMOTION_FOR_PRODUCT = 0;
	public static final int FREE_PRODUCT = 1;
	public static final int FREE_PRICE = 2;
	public static final int FREE_PERCENT = 3;
	public static final int PROMOTION_FOR_ORDER = 4;
	public static final int PROMOTION_FOR_ORDER_TYPE_PRODUCT = 5;
	public static final int PROMOTION_FOR_ZV21 = 6;
    public static final int ownerTypePromoProduct = 1;
    public static final int ownerTypePromoOrder = 0;
    public static final int PORTION_EDIT = 2;
    public static final int QUANTITY_EDIT = 1;
    public static final int PORTION_AND_QUANTITY_NOT_EDIT = 0;
	
	// thong tin chi tiet don hang
	private List<SaleOrderLot> saleOrderLot;
	
	private SaleOrderDetail saleOrderDetail;
	
	private Integer levelPromo;
	
	public List<SaleOrderPromoDetail> listPromoDetail = new ArrayList<SaleOrderPromoDetail>();
	
	private List<SaleOrderDetailVO> listPromotionForPromo21;
	
	private List<SaleOrderPromotion> listQuantityReceivedOrder;
	
	private Product product;
	// mat hang trong tam hay ko?
	private int isFocus;
	
	private Integer isEdited;

	private int type;// 1: tra thuong sp, 2: tra thuong tien, 3: tra thuong %
						// tien
	private String typeName;

	private Product promoProduct;

	private int promotionType;

	private int changeProduct = 0;// 1: la duoc lua chon
	
	private Long keyList;
	// private long quantityEdit;
	private Integer stock; // ton kho

	private Integer availableQuantity;
	
	private BigDecimal price;

	private List<SaleOrderDetailVO> changePPromotion;

	private int relation;
	
	//Ds Id san pham thuc te mua
	public List<Long> listIdProductOfGroup;
	// So luong san pham ban qui dinh trong group de dung cho truong isOwner
	private int numBuyProductInGroup;
	
	public ArrayList<GroupLevelDetail> listBuyProduct;
	
	public boolean isBundle;
	
	public List<SaleOrderPromotion> promotionQuantityReceivedList = new ArrayList<SaleOrderPromotion>();
	
	public boolean hasPromotion;
	
	//Danh sach KM don hang tinh KM khi doi CTKM
	private List<SaleOrderDetailVO> listSaleOrderPromotionForOrder;
	
	private List<SaleOrderDetailVO> listPromotion19;
	private List<SaleOrderDetailVO> listPromotion20;
	private List<SaleOrderDetailVO> listPromotion21;
	
	private Integer openPromo = 0;
	private Integer hasPortion;
	private Long productGroupId;
	
	public List<SaleOrderDetailVO> getListPromotion19() {
		return listPromotion19;
	}

	public void setListPromotion19(List<SaleOrderDetailVO> listPromotion19) {
		this.listPromotion19 = listPromotion19;
	}

	public List<SaleOrderDetailVO> getListPromotion20() {
		return listPromotion20;
	}

	public void setListPromotion20(List<SaleOrderDetailVO> listPromotion20) {
		this.listPromotion20 = listPromotion20;
	}

	public List<SaleOrderDetailVO> getListPromotion21() {
		return listPromotion21;
	}

	public void setListPromotion21(List<SaleOrderDetailVO> listPromotion21) {
		this.listPromotion21 = listPromotion21;
	}

	public List<SaleOrderDetailVO> getListSaleOrderPromotionForOrder() {
		return listSaleOrderPromotionForOrder;
	}

	public void setListSaleOrderPromotionForOrder(
			List<SaleOrderDetailVO> listSaleOrderPromotionForOrder) {
		this.listSaleOrderPromotionForOrder = listSaleOrderPromotionForOrder;
	}

	public boolean isHasPromotion() {
		return hasPromotion;
	}

	public void setHasPromotion(boolean hasPromotion) {
		this.hasPromotion = hasPromotion;
	}

	public List<SaleOrderPromotion> getPromotionQuantityReceivedList() {
		return promotionQuantityReceivedList;
	}

	public void setPromotionQuantityReceivedList(
			List<SaleOrderPromotion> promotionQuantityReceivedList) {
		this.promotionQuantityReceivedList = promotionQuantityReceivedList;
	}

	public void setBundle(boolean isBundle) {
		this.isBundle = isBundle;
	}

	public ArrayList<GroupLevelDetail> getListBuyProduct() {
		return listBuyProduct;
	}

	public void setListBuyProduct(ArrayList<GroupLevelDetail> listBuyProduct) {
		this.listBuyProduct = listBuyProduct;
	}

	public boolean getIsBundle() {
		return isBundle;
	}

	public void setIsBundle(boolean isBundle) {
		this.isBundle = isBundle;
	}

	public Integer getIsEdited() {
		return isEdited;
	}

	public void setIsEdited(Integer isEdited) {
		this.isEdited = isEdited;
	}

	public List<SaleOrderPromoDetail> getListPromoDetail() {
		return listPromoDetail;
	}

	public void setListPromoDetail(List<SaleOrderPromoDetail> listPromoDetail) {
		this.listPromoDetail = listPromoDetail;
	}

	public SaleOrderDetail getSaleOrderDetail() {
		return saleOrderDetail;
	}

	public void setSaleOrderDetail(SaleOrderDetail saleOrderDetail) {
		this.saleOrderDetail = saleOrderDetail;
	}

	public Integer getStock() {
		return stock;
	}

	public Integer getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public List<SaleOrderLot> getSaleOrderLot() {
		return saleOrderLot;
	}

	public void setSaleOrderLot(List<SaleOrderLot> saleOrderLot) {
		this.saleOrderLot = saleOrderLot;
	}

	public List<Long> getListIdProductOfGroup() {
		return listIdProductOfGroup;
	}

	public void setListIdProductOfGroup(List<Long> listIdProductOfGroup) {
		this.listIdProductOfGroup = listIdProductOfGroup;
	}

	public int getNumBuyProductInGroup() {
		return numBuyProductInGroup;
	}

	public void setNumBuyProductInGroup(int numBuyProductInGroup) {
		this.numBuyProductInGroup = numBuyProductInGroup;
	}

	public BigDecimal getPrice() {
		return price == null ? BigDecimal.valueOf(0) : price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	// public double getTotalAmount() {
	// return totalAmount;
	// }
	// public void setTotalAmount(double totalAmount) {
	// this.totalAmount = totalAmount;
	// }
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getIsFocus() {
		return isFocus;
	}

	public void setIsFocus(int isFocus) {
		this.isFocus = isFocus;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Product getPromoProduct() {
		return promoProduct;
	}

	public void setPromoProduct(Product promoProduct) {
		this.promoProduct = promoProduct;
	}

	public int getChangeProduct() {
		return changeProduct;
	}

	public void setChangeProduct(int changeProduct) {
		this.changeProduct = changeProduct;
	}

	public Long getKeyList() {
		return keyList;
	}

	public void setKeyList(Long keyList) {
		this.keyList = keyList;
	}

	public int getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(int promotionType) {
		this.promotionType = promotionType;
	}

	public List<SaleOrderDetailVO> getListPromotionForPromo21() {
		return listPromotionForPromo21;
	}

	public void setListPromotionForPromo21(
			List<SaleOrderDetailVO> listPromotionForPromo21) {
		this.listPromotionForPromo21 = listPromotionForPromo21;
	}

	public List<SaleOrderDetailVO> getChangePPromotion() {
		return changePPromotion;
	}

	public void setChangePPromotion(List<SaleOrderDetailVO> changePPromotion) {
		this.changePPromotion = changePPromotion;
	}

	public int getRelation() {
		return relation;
	}

	public void setRelation(int relation) {
		this.relation = relation;
	}

	public List<SaleOrderPromotion> getListQuantityReceivedOrder() {
		return listQuantityReceivedOrder;
	}

	public void setListQuantityReceivedOrder(
			List<SaleOrderPromotion> listQuantityReceivedOrder) {
		this.listQuantityReceivedOrder = listQuantityReceivedOrder;
	}

	public Integer getLevelPromo() {
		return levelPromo;
	}

	public void setLevelPromo(Integer levelPromo) {
		this.levelPromo = levelPromo;
	}

	public Integer getOpenPromo() {
		return openPromo;
	}

	public void setOpenPromo(Integer openPromo) {
		this.openPromo = openPromo;
	}

	public Long getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(Long productGroupId) {
		this.productGroupId = productGroupId;
	}

	public Integer getHasPortion() {
		return hasPortion;
	}

	public void setHasPortion(Integer hasPortion) {
		this.hasPortion = hasPortion;
	}

}
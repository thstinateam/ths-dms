package ths.dms.core.entities.vo;

import java.io.Serializable;

public class PPConvertDetailVO implements Serializable {
	Long id;
	Long convertId;
	String productCode;
	String productName;
	Integer isSourceProduct;
	Float factor;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getConvertId() {
		return convertId;
	}
	public void setConvertId(Long convertId) {
		this.convertId = convertId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getIsSourceProduct() {
		return isSourceProduct;
	}
	public void setIsSourceProduct(Integer isSourceProduct) {
		this.isSourceProduct = isSourceProduct;
	}
	public Float getFactor() {
		return factor;
	}
	public void setFactor(Float factor) {
		this.factor = factor;
	}
}

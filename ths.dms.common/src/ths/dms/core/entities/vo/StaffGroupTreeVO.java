package ths.dms.core.entities.vo;

import java.io.Serializable;

public class StaffGroupTreeVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String staffGroupName;
	private ShopTreeVO shop;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStaffGroupName() {
		return staffGroupName;
	}
	public void setStaffGroupName(String staffGroupName) {
		this.staffGroupName = staffGroupName;
	}
	public ShopTreeVO getShop() {
		return shop;
	}
	public void setShop(ShopTreeVO shop) {
		this.shop = shop;
	}
	
}

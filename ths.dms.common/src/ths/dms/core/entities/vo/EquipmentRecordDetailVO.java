package ths.dms.core.entities.vo;

import java.io.Serializable;

public class EquipmentRecordDetailVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String code;
	String name;
	String catName;
	Integer total;
	Integer numInstn;
	Integer numLost;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getNumInstn() {
		return numInstn;
	}
	public void setNumInstn(Integer numInstn) {
		this.numInstn = numInstn;
	}
	public Integer getNumLost() {
		return numLost;
	}
	public void setNumLost(Integer numLost) {
		this.numLost = numLost;
	}
}

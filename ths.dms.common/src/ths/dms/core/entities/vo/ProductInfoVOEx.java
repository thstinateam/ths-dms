package ths.dms.core.entities.vo;

import java.io.Serializable;

public class ProductInfoVOEx implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3356702502499942801L;
	
	private String description;

	private String productInfoCode;

	private Long productInfoId;

	private String productInfoName;

	private Integer status ;

	private Integer type;
	
	private String parentCode;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProductInfoCode() {
		return productInfoCode;
	}

	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}

	public Long getProductInfoId() {
		return productInfoId;
	}

	public void setProductInfoId(Long productInfoId) {
		this.productInfoId = productInfoId;
	}

	public String getProductInfoName() {
		return productInfoName;
	}

	public void setProductInfoName(String productInfoName) {
		this.productInfoName = productInfoName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}
	
}

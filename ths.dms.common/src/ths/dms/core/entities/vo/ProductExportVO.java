package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import ths.core.entities.vo.rpt.DynamicVO;

/**
 * class vo: san pham
 * 
 * @author lacnv1
 * @since Nov 22, 2013
 */
public class ProductExportVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long productId;
	private String productCode;
	private String productName;
	//private String shortName;
	private String parentProductCode;
	private String barcode;
	private String categoryName;
	private String categoryCode;
	private String subcatName;
	private String subcatCode;
	private String brandName;
	private String brandCode;
	private String flavourName;
	private String flavourCode;
	private String packingName;
	private String packingCode;
	private Integer orderIndex;
	private String uom1;
	private String uom2;
	private BigDecimal convfact;
	private BigDecimal netWeight;
	private BigDecimal grossWeight;
	private Double volume;
	private String expiryType;
	private Integer expiryDate;
	private BigDecimal priceIn;
	private Double vatIn;
	private BigDecimal priceNotVatIn;
	private BigDecimal priceOut;
	private Double vatOut;
	private BigDecimal priceNotVatOut;
	private String status;

	private Long catId;
	private Long subCatId;
	private Long brandId;
	private Integer checkLot;
	
	private List<DynamicVO> dynamicAttributes;
	
	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getParentProductCode() {
		return parentProductCode;
	}

	public void setParentProductCode(String parentProductCode) {
		this.parentProductCode = parentProductCode;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getFlavourName() {
		return flavourName;
	}

	public void setFlavourName(String flavourName) {
		this.flavourName = flavourName;
	}

	public String getUom1() {
		return uom1;
	}

	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	public String getUom2() {
		return uom2;
	}

	public void setUom2(String uom2) {
		this.uom2 = uom2;
	}

	public BigDecimal getConvfact() {
		return convfact;
	}

	public void setConvfact(BigDecimal convfact) {
		this.convfact = convfact;
	}

	public BigDecimal getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}

	public BigDecimal getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getExpiryType() {
		return expiryType;
	}

	public void setExpiryType(String expiryType) {
		this.expiryType = expiryType;
	}

	public Integer getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Integer expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getPriceIn() {
		return priceIn;
	}

	public void setPriceIn(BigDecimal priceIn) {
		this.priceIn = priceIn;
	}

	public Double getVatIn() {
		return vatIn;
	}

	public void setVatIn(Double vatIn) {
		this.vatIn = vatIn;
	}

	public BigDecimal getPriceNotVatIn() {
		return priceNotVatIn;
	}

	public void setPriceNotVatIn(BigDecimal priceNotVatIn) {
		this.priceNotVatIn = priceNotVatIn;
	}

	public BigDecimal getPriceOut() {
		return priceOut;
	}

	public void setPriceOut(BigDecimal priceOut) {
		this.priceOut = priceOut;
	}

	public Double getVatOut() {
		return vatOut;
	}

	public void setVatOut(Double vatOut) {
		this.vatOut = vatOut;
	}

	public BigDecimal getPriceNotVatOut() {
		return priceNotVatOut;
	}

	public void setPriceNotVatOut(BigDecimal priceNotVatOut) {
		this.priceNotVatOut = priceNotVatOut;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	/*public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}*/

	public Long getCatId() {
		return catId;
	}

	public void setCatId(Long catId) {
		this.catId = catId;
	}

	public Long getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(Long subCatId) {
		this.subCatId = subCatId;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public String getSubcatName() {
		return subcatName;
	}

	public void setSubcatName(String subcatName) {
		this.subcatName = subcatName;
	}

	public String getPackingName() {
		return packingName;
	}

	public void setPackingName(String packingName) {
		this.packingName = packingName;
	}

	public Integer getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(Integer orderIndex) {
		this.orderIndex = orderIndex;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getSubcatCode() {
		return subcatCode;
	}

	public void setSubcatCode(String subcatCode) {
		this.subcatCode = subcatCode;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getFlavourCode() {
		return flavourCode;
	}

	public void setFlavourCode(String flavourCode) {
		this.flavourCode = flavourCode;
	}

	public String getPackingCode() {
		return packingCode;
	}

	public void setPackingCode(String packingCode) {
		this.packingCode = packingCode;
	}

	public List<DynamicVO> getDynamicAttributes() {
		return dynamicAttributes;
	}

	public void setDynamicAttributes(List<DynamicVO> dynamicAttributes) {
		this.dynamicAttributes = dynamicAttributes;
	}

}
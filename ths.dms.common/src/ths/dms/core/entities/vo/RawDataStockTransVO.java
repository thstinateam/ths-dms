/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VO bao cao raw data
 * @author trietptm
 * @since Nov 16, 2015
 */
public class RawDataStockTransVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String regionCode;
	private String areaCode;
	private String fromShopCode;
	private String fromShopName;
	private String fromWarehouse;
	private String stockTransCode;
	private String transType;
	private String stockTransDate;
	private Integer approved;
	private Integer approvedStep;
	private String stockDate;
	private String toShopCode;
	private String toShopName;
	private String toWarehouse;
	private String productCode;
	private String productName;
	private BigDecimal price;
	private Integer convfact;
	private BigDecimal quantity;
	private String status;
	
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getFromShopCode() {
		return fromShopCode;
	}
	public void setFromShopCode(String fromShopCode) {
		this.fromShopCode = fromShopCode;
	}
	public String getFromShopName() {
		return fromShopName;
	}
	public void setFromShopName(String fromShopName) {
		this.fromShopName = fromShopName;
	}
	public String getFromWarehouse() {
		return fromWarehouse;
	}
	public void setFromWarehouse(String fromWarehouse) {
		this.fromWarehouse = fromWarehouse;
	}
	public String getStockTransCode() {
		return stockTransCode;
	}
	public void setStockTransCode(String stockTransCode) {
		this.stockTransCode = stockTransCode;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public String getStockTransDate() {
		return stockTransDate;
	}
	public void setStockTransDate(String stockTransDate) {
		this.stockTransDate = stockTransDate;
	}
	public Integer getApproved() {
		return approved;
	}
	public void setApproved(Integer approved) {
		this.approved = approved;
	}
	public Integer getApprovedStep() {
		return approvedStep;
	}
	public void setApprovedStep(Integer approvedStep) {
		this.approvedStep = approvedStep;
	}
	public String getStockDate() {
		return stockDate;
	}
	public void setStockDate(String stockDate) {
		this.stockDate = stockDate;
	}
	public String getToShopCode() {
		return toShopCode;
	}
	public void setToShopCode(String toShopCode) {
		this.toShopCode = toShopCode;
	}
	public String getToShopName() {
		return toShopName;
	}
	public void setToShopName(String toShopName) {
		this.toShopName = toShopName;
	}
	public String getToWarehouse() {
		return toWarehouse;
	}
	public void setToWarehouse(String toWarehouse) {
		this.toWarehouse = toWarehouse;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	
}

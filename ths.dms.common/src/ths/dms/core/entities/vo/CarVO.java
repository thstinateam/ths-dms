package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

public class CarVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long shopId;
	private Long carId;
	private String shopCode;
	private String shopName;
	private Integer shopType;
	private Integer countCar;
	private Integer countWarning;
	private Integer countRouting;
	private Boolean isBold;
	private Float lat;
	private Float lng;
	private Long customerId;
	private String shortCode;
	private String customerCode;
	private String customerName;
	private Float latCustomer;
	private Float lngCustomer;
	private String address;
	private String carNumber;
	private Integer isWarning;
	private String createDate;
	private String createTime;
	private Date time;
	private Integer seq;
	private Integer seqSet;//thu tu ghe tham du kien
	private String staffCode;
	private String staffName;
	private String imei;
	private Integer distance;
	private String mien;
	private String vung;
	private String isOr;//ngoai tuyen
	private Integer gpsSpeed;
	private Integer diemDaGiao;
	private Integer diemDaGiaoTrongTuyen;
	private Integer tongSoDiemGiao;
	private Integer fuelPercent;
	private String driverCode;
	private String lastCreateDate;
	private String lastCreateTime;
	private Integer lastDistance;
	private String lastAddress;
	private String lastCustomerCode;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getCarId() {
		return carId;
	}
	public void setCarId(Long carId) {
		this.carId = carId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Integer getShopType() {
		return shopType;
	}
	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}
	public Integer getCountCar() {
		return countCar;
	}
	public void setCountCar(Integer countCar) {
		this.countCar = countCar;
	}
	public Integer getCountWarning() {
		return countWarning;
	}
	public void setCountWarning(Integer countWarning) {
		this.countWarning = countWarning;
	}
	public Boolean getIsBold() {
		return isBold;
	}
	public void setIsBold(Boolean isBold) {
		this.isBold = isBold;
	}
	public Float getLat() {
		return lat;
	}
	public void setLat(Float lat) {
		this.lat = lat;
	}
	public Float getLng() {
		return lng;
	}
	public void setLng(Float lng) {
		this.lng = lng;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Float getLatCustomer() {
		return latCustomer;
	}
	public void setLatCustomer(Float latCustomer) {
		this.latCustomer = latCustomer;
	}
	public Float getLngCustomer() {
		return lngCustomer;
	}
	public void setLngCustomer(Float lngCustomer) {
		this.lngCustomer = lngCustomer;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getCarNumber() {
		return carNumber;
	}
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}
	public Integer getIsWarning() {
		return isWarning;
	}
	public void setIsWarning(Integer isWarning) {
		this.isWarning = isWarning;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public Integer getDistance() {
		return distance;
	}
	public void setDistance(Integer distance) {
		this.distance = distance;
	}
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public Integer getSeqSet() {
		return seqSet;
	}
	public void setSeqSet(Integer seqSet) {
		this.seqSet = seqSet;
	}
	public String getIsOr() {
		return isOr;
	}
	public void setIsOr(String isOr) {
		this.isOr = isOr;
	}
	public Integer getDiemDaGiao() {
		return diemDaGiao;
	}
	public void setDiemDaGiao(Integer diemDaGiao) {
		this.diemDaGiao = diemDaGiao;
	}
	public Integer getTongSoDiemGiao() {
		return tongSoDiemGiao;
	}
	public void setTongSoDiemGiao(Integer tongSoDiemGiao) {
		this.tongSoDiemGiao = tongSoDiemGiao;
	}
	public Integer getFuelPercent() {
		return fuelPercent;
	}
	public void setFuelPercent(Integer fuelPercent) {
		this.fuelPercent = fuelPercent;
	}
	public String getDriverCode() {
		return driverCode;
	}
	public void setDriverCode(String driverCode) {
		this.driverCode = driverCode;
	}
	public Integer getCountRouting() {
		return countRouting;
	}
	public void setCountRouting(Integer countRouting) {
		this.countRouting = countRouting;
	}
	public Integer getDiemDaGiaoTrongTuyen() {
		return diemDaGiaoTrongTuyen;
	}
	public void setDiemDaGiaoTrongTuyen(Integer diemDaGiaoTrongTuyen) {
		this.diemDaGiaoTrongTuyen = diemDaGiaoTrongTuyen;
	}
	public Integer getGpsSpeed() {
		return gpsSpeed;
	}
	public void setGpsSpeed(Integer gpsSpeed) {
		this.gpsSpeed = gpsSpeed;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getLastCreateDate() {
		return lastCreateDate;
	}
	public void setLastCreateDate(String lastCreateDate) {
		this.lastCreateDate = lastCreateDate;
	}
	public String getLastCreateTime() {
		return lastCreateTime;
	}
	public void setLastCreateTime(String lastCreateTime) {
		this.lastCreateTime = lastCreateTime;
	}
	public Integer getLastDistance() {
		return lastDistance;
	}
	public void setLastDistance(Integer lastDistance) {
		this.lastDistance = lastDistance;
	}
	public String getLastAddress() {
		return lastAddress;
	}
	public void setLastAddress(String lastAddress) {
		this.lastAddress = lastAddress;
	}
	public String getLastCustomerCode() {
		return lastCustomerCode;
	}
	public void setLastCustomerCode(String lastCustomerCode) {
		this.lastCustomerCode = lastCustomerCode;
	}

	
}

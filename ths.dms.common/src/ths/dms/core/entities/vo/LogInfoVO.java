package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.enumtype.FuncType;



public class LogInfoVO implements Serializable {
	
	private static final long serialVersionUID = -1878562251044796712L;
	
	public static final String FC_SALE_CREATE_ORDER = "FC_SALE_CREATE_ORDER"; //Tao don hang
	public static final String FC_SALE_ADJUST_ORDER = "FC_SALE_ADJUST_ORDER"; //Sua don hang
	public static final String FC_SALE_APPROVED_ORDER = "FC_SALE_APPROVED_ORDER"; //Duyet don hang
	public static final String FC_SALE_DENY_ORDER = "FC_SALE_DENY_ORDER"; //Tu choi don hang
	public static final String FC_SALE_DELETE_ORDER_DETAIL = "FC_SALE_DELETE_ORDER_DETAIL"; //Xoa chi tiet don hang
	public static final String FC_SALE_DELETE_ORDER = "FC_SALE_DELETE_ORDER"; // Xoa don hang
	public static final String FC_SALE_CANCEL_ORDER = "FC_SALE_CANCEL_ORDER"; // Huy
	public static final String FC_STOCK_ORDER_UPDATE = "FC_STOCK_ORDER_UPDATE"; // Cap nhat phai thu vao kho
	public static final String FC_STOCK_CONFIRM_ORDER = "FC_STOCK_CONFIRM_ORDER"; // Xac nhan DH & thanh toan
	
	public static final String ACTION_UPDATE = "UPDATE"; // Cap nhat
	public static final String ACTION_INSERT = "INSERT"; // Them moi
	public static final String ACTION_VIEW = "VIEW"; // Xem 
	public static final String ACTION_DELETE = "DELETE";	 // Xoa
	
	
	private String appCode; // Ma ung dung
	
	private String processCode; // Ma tien trinh
	
	private Date startDate; // Thoi gian bat dau
	
	private Date endDate; // Thoi gian ket thuc
	
	private String staffCode; // Ma tai khoan tac dong
		
	private String ip; // Dia chi IP tac dong
	
	private String functionCode; // Ma chuc nang
	
	private String actionType; //UPDATE,DELETE,INSERT,VIEW // Loai tac dong
	
	private String idObject; //Id doi tuong tac dong (Ma hoac Id)
	
	private String content = "";
	
	private String sessionId = "";
	
	private Long staffRootId;
	
	private Long shopRootId;
	
	private String ojectCode;
	
	private String originalCode;
	
	private String ipClientAddress;	/** Ip cua nguoi dung. */
	
	private String startTime;		/** Thoi gian bat dau. */
	
	private String endTime;		/** Thoi gian ket thuc. */
	
	private String startForEndTimeMiliSecon; /** Khoang thoi gian thuc thi*/
	
	private String loginUserCode;		/** Username dang nhap he thong. */
	
	
	/** The function type. */
	private FuncType functionType;		/** loai tac dong. */	
	
	private Long objectId;		/** Id cua doi tuong tac dong. */
	
	private String description;
	
	private String uri;
	
	private String actionResult;
	/**
	 * Khai bao phuong thuc GETTER/SETTER
	 * */
	
	
	
	/**
	 * Format noi dung tra ve
	 * @author TIENTV11
	 * @param productId : Id san pham
	 * @param whId : Id kho
	 * @param quantity : So luong tham gia, de sinh chenh lech kho
	 * @param beforeAQ : Ton kho dap ung truoc
	 * @param afterAQ : Ton kho dap ung sau khi thay doi
	 * @param bfQ : Ton kho thuc te truoc 
	 * @param afQ : Ton kho thuc te sau khi thay doi
	 * @return
	 */
	public void getContenFormat(Long productId,Long whId,Integer quantity,
			Integer beforeAQ,Integer afterAQ,Integer bfQ,Integer afQ){
		StringBuilder logContent = new StringBuilder("");
		try {			
			logContent.append("|");
			logContent.append("P:").append(productId).append(";");							
			logContent.append("W:").append(whId == null? "NULL":whId).append(";");	
			logContent.append("Q:").append(quantity).append(";");	
			logContent.append("WABF:").append(beforeAQ != null ? beforeAQ : "N/A").append(";");	
			logContent.append("WAAF:").append(afterAQ != null ? afterAQ : "N/A").append(";");	
			logContent.append("WQBF:").append(bfQ != null ? bfQ : "N/A").append(";");	
			logContent.append("WQAF:").append(afQ != null ? afQ : "N/A").append(";");
			this.setContent(this.content + logContent.toString());
		} catch (Exception e) {}
	}
	
	public String getIp() {
		return isNullOrEmpty(ip)? "N/A":ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getStaffCode() {
		return isNullOrEmpty(staffCode)? "N/A":staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAppCode() {		
		return isNullOrEmpty(appCode)? "N/A":appCode;
	}
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
	public String getProcessCode() {		
		return isNullOrEmpty(processCode)? "N/A":processCode;
	}
	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getFunctionCode() {		
		return isNullOrEmpty(functionCode)? "N/A":functionCode;
	}
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	public String getActionType() {		
		return isNullOrEmpty(actionType)? "N/A":actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public String getIdObject() {		
		return isNullOrEmpty(idObject)? "N/A":idObject;
	}
	public void setIdObject(String idObject) {
		this.idObject = idObject;
	}
	
	public String getContent() {
		return isNullOrEmpty(content)? "N/A":content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	private boolean isNullOrEmpty(final String s) {
		return (s == null || s.trim().length() == 0);
	}
	
	public LogInfoVO clone() {
		LogInfoVO cloneObject = new LogInfoVO();
		cloneObject.setAppCode(this.getAppCode());
		cloneObject.setProcessCode(this.getProcessCode());
		cloneObject.setStartDate(this.getStartDate());
		cloneObject.setEndDate(this.getEndDate());
		cloneObject.setStaffCode(this.getStaffCode());
		cloneObject.setIp(this.getIp());
		cloneObject.setFunctionCode(this.getFunctionCode());
		cloneObject.setActionType(this.getActionType());
		cloneObject.setIdObject(this.getIdObject());
		cloneObject.setContent (this.getContent ());		
		return cloneObject;
	}

	public Long getStaffRootId() {
		return staffRootId;
	}

	public void setStaffRootId(Long staffRootId) {
		this.staffRootId = staffRootId;
	}

	public Long getShopRootId() {
		return shopRootId;
	}

	public void setShopRootId(Long shopRootId) {
		this.shopRootId = shopRootId;
	}

	public String getOjectCode() {
		return ojectCode;
	}

	public void setOjectCode(String ojectCode) {
		this.ojectCode = ojectCode;
	}

	public String getOriginalCode() {
		return originalCode;
	}

	public void setOriginalCode(String originalCode) {
		this.originalCode = originalCode;
	}

	public String getIpClientAddress() {
		return ipClientAddress;
	}

	public void setIpClientAddress(String ipClientAddress) {
		this.ipClientAddress = ipClientAddress;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartForEndTimeMiliSecon() {
		return startForEndTimeMiliSecon;
	}

	public void setStartForEndTimeMiliSecon(String startForEndTimeMiliSecon) {
		this.startForEndTimeMiliSecon = startForEndTimeMiliSecon;
	}

	public String getLoginUserCode() {
		return loginUserCode;
	}

	public void setLoginUserCode(String loginUserCode) {
		this.loginUserCode = loginUserCode;
	}

	public FuncType getFunctionType() {
		return functionType;
	}

	public void setFunctionType(FuncType functionType) {
		this.functionType = functionType;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getActionResult() {
		return actionResult;
	}

	public void setActionResult(String actionResult) {
		this.actionResult = actionResult;
	}
}

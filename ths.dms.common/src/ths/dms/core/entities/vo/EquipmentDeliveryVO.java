package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class EquipmentDeliveryVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Id thiet bi */
	private Long equipmentId;
	
	/** Nam san xuat */
	private Integer year;
	
	/** Ma thiet bi */
	private String equipmentCode;
	
	private String equipmentName;
	
	/** So seri */
	private String seriNumber;
	
	/** Tinh trang thiet bi */
	private String healthStatus;
	
	/** Tinh trang thiet bi - ma */
	private String healthCode;
	
	/** Ma kho */
	private String stockCode;
	
	/** Ma + ten kho */
	private String stock;
	
	/** Ten kho */
	private String stockName;
	
	/** Loai thiet bi */
	private String typeEquipment;
	
	/** Ma nhom thiet bi */
	private String groupEquipmentCode;
	
	/** Ten nhom thiet bi */
	private String groupEquipmentName;
	
	private String groupEquipment;
	
	/** Dung tich (lit) */
	private String capacity;
	
	/** Nhan hieu */
	private String equipmentBrand;
	
	private String fromStock;
	
	private String toStock;
	
	/** Nha cung cap */
	private String equipmentProvider;
	
	/** Ma nha cung cap */
	private String equipmentProviderCode;
	
	/** Nam san xuat */
	private String yearManufacture;
	
	/** Ngay het han bao hanh */
	private String warrantyExpiredDate;

	/** Noi dung */
	private String contentDelivery;
	
	/** So lan sua chua */
	private Integer repairCount;

	/** Gia tri */
	private BigDecimal valueEquip;
	
	/** Gia tri thanh ly: ben thanh ly tai san*/
	private BigDecimal liquidationValue;
	
	/** Gia tri thu hoi: ben thanh ly tai san*/
	private BigDecimal evictionValue;
	
	/** Nguyen gia */
	private BigDecimal price;
	
	/** Nguyen gia (dong)*/
	private String priceMoney;
	
	/** Nam bat dau su dung */
	private String firstYearInUse;
	
	/** Id bien ban chuyen kho */
	private Long idStockRecord;
	private Long idStockRecordDtl;
	
	private Integer performStatus;
	
	// So thang khau hao
	private Integer depreciation;
	private String depreciationMonth;
	
	private List<EquipmentDeliveryVO> lstEquipmentVOs;
	
	private Integer liquidationStatus;
	
	/** begin thanh ly */
	private String shopCode;
	private String shopName;
	private String shortCode;
	private String customerName;
	private String address;
	private Integer stockType;
	private Long stockId;
	private Integer tradeType;
	
	private Integer tradeStatus;
	private Integer usageStatus;
	private Integer status;
	
	private Long shopId;
	/** end thanh ly */
	public List<EquipmentDeliveryVO> getLstEquipmentVOs() {
		return lstEquipmentVOs;
	}

	public void setLstEquipmentVOs(List<EquipmentDeliveryVO> lstEquipmentVOs) {
		this.lstEquipmentVOs = lstEquipmentVOs;
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getSeriNumber() {
		return seriNumber;
	}

	public void setSeriNumber(String seriNumber) {
		this.seriNumber = seriNumber;
	}
	
	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getTypeEquipment() {
		return typeEquipment;
	}

	public void setTypeEquipment(String typeEquipment) {
		this.typeEquipment = typeEquipment;
	}

	public String getGroupEquipmentCode() {
		return groupEquipmentCode;
	}

	public void setGroupEquipmentCode(String groupEquipmentCode) {
		this.groupEquipmentCode = groupEquipmentCode;
	}

	public String getGroupEquipmentName() {
		return groupEquipmentName;
	}

	public void setGroupEquipmentName(String groupEquipmentName) {
		this.groupEquipmentName = groupEquipmentName;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getEquipmentBrand() {
		return equipmentBrand;
	}

	public void setEquipmentBrand(String equipmentBrand) {
		this.equipmentBrand = equipmentBrand;
	}

	public String getEquipmentProvider() {
		return equipmentProvider;
	}

	public void setEquipmentProvider(String equipmentProvider) {
		this.equipmentProvider = equipmentProvider;
	}

	public Long getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(Long equipmentId) {
		this.equipmentId = equipmentId;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public String getYearManufacture() {
		return yearManufacture;
	}

	public void setYearManufacture(String yearManufacture) {
		this.yearManufacture = yearManufacture;
	}

	public String getWarrantyExpiredDate() {
		return warrantyExpiredDate;
	}

	public void setWarrantyExpiredDate(String warrantyExpiredDate) {
		this.warrantyExpiredDate = warrantyExpiredDate;
	}


	public String getContentDelivery() {
		return contentDelivery;
	}

	public void setContentDelivery(String contentDelivery) {
		this.contentDelivery = contentDelivery;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getGroupEquipment() {
		return groupEquipment;
	}

	public void setGroupEquipment(String groupEquipment) {
		this.groupEquipment = groupEquipment;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getRepairCount() {
		return repairCount;
	}

	public void setRepairCount(Integer repairCount) {
		this.repairCount = repairCount;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public BigDecimal getValueEquip() {
		return valueEquip;
	}

	public void setValueEquip(BigDecimal valueEquip) {
		this.valueEquip = valueEquip;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getIdStockRecord() {
		return idStockRecord;
	}

	public void setIdStockRecord(Long idStockRecord) {
		this.idStockRecord = idStockRecord;
	}

	public Long getIdStockRecordDtl() {
		return idStockRecordDtl;
	}

	public void setIdStockRecordDtl(Long idStockRecordDtl) {
		this.idStockRecordDtl = idStockRecordDtl;
	}

	public String getFromStock() {
		return fromStock;
	}

	public void setFromStock(String fromStock) {
		this.fromStock = fromStock;
	}

	public String getToStock() {
		return toStock;
	}

	public void setToStock(String toStock) {
		this.toStock = toStock;
	}

	public String getFirstYearInUse() {
		return firstYearInUse;
	}

	public void setFirstYearInUse(String firstYearInUse) {
		this.firstYearInUse = firstYearInUse;
	}

	public Integer getPerformStatus() {
		return performStatus;
	}

	public void setPerformStatus(Integer performStatus) {
		this.performStatus = performStatus;
	}

	public Integer getDepreciation() {
		return depreciation;
	}

	public void setDepreciation(Integer depreciation) {
		this.depreciation = depreciation;
	}

	public String getHealthCode() {
		return healthCode;
	}

	public void setHealthCode(String healthCode) {
		this.healthCode = healthCode;
	}

	public String getEquipmentProviderCode() {
		return equipmentProviderCode;
	}

	public void setEquipmentProviderCode(String equipmentProviderCode) {
		this.equipmentProviderCode = equipmentProviderCode;
	}

	public Integer getLiquidationStatus() {
		return liquidationStatus;
	}

	public void setLiquidationStatus(Integer liquidationStatus) {
		this.liquidationStatus = liquidationStatus;
	}

	public BigDecimal getLiquidationValue() {
		return liquidationValue;
	}

	public void setLiquidationValue(BigDecimal liquidationValue) {
		this.liquidationValue = liquidationValue;
	}

	public BigDecimal getEvictionValue() {
		return evictionValue;
	}

	public void setEvictionValue(BigDecimal evictionValue) {
		this.evictionValue = evictionValue;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getStockType() {
		return stockType;
	}

	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}

	public Integer getTradeType() {
		return tradeType;
	}

	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}

	public Integer getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(Integer tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public Integer getUsageStatus() {
		return usageStatus;
	}

	public void setUsageStatus(Integer usageStatus) {
		this.usageStatus = usageStatus;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPriceMoney() {
		return priceMoney;
	}

	public void setPriceMoney(String priceMoney) {
		this.priceMoney = priceMoney;
	}

	public String getDepreciationMonth() {
		return depreciationMonth;
	}

	public void setDepreciationMonth(String depreciationMonth) {
		this.depreciationMonth = depreciationMonth;
	}

	public Long getStockId() {
		return stockId;
	}

	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Quan ly bang EquipLostMobileRec khac du lieu tra ve
 * @author tientv11
 * @since 07/01/2015
 *
 */
public class EquipLostMobileRecVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private Long equipLostId;
	private Long customerId;
	private Long equipId;
	
	private Date lostDate;
	
	private Date lastArisingSalesDate;
	
	private String stockCode;
	
	private String stockName;
	
	private String stockAddress;
	
	private String stockCodeName;
	
	private String staffCodeName;
	
	private Integer recordStatus;
	
	private String shopCode;
	
	private String shopName;
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public Date getLostDate() {
		return lostDate;
	}

	public void setLostDate(Date lostDate) {
		this.lostDate = lostDate;
	}

	public Date getLastArisingSalesDate() {
		return lastArisingSalesDate;
	}

	public void setLastArisingSalesDate(Date lastArisingSalesDate) {
		this.lastArisingSalesDate = lastArisingSalesDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStockAddress() {
		return stockAddress;
	}

	public void setStockAddress(String stockAddress) {
		this.stockAddress = stockAddress;
	}
	
	public String getStockCodeName() {
		return stockCodeName;
	}

	public void setStockCodeName(String stockCodeName) {
		this.stockCodeName = stockCodeName;
	}

	public String getStaffCodeName() {
		return staffCodeName;
	}

	public void setStaffCodeName(String staffCodeName) {
		this.staffCodeName = staffCodeName;
	}

	public Integer getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}

	public Long getEquipLostId() {
		return equipLostId;
	}

	public void setEquipLostId(Long equipLostId) {
		this.equipLostId = equipLostId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	
	
}

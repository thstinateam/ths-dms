/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;


/**
 * 
 * @author lacnv1
 * @since Sep 22, 2014
 */
public class CarSOVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String carNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}
}
package ths.dms.core.entities.vo;

import java.io.Serializable;
/**
 * @author hunglm16
 * @since January 30,2015
 * @version 1.0
 * */
public class FileVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6367924615377435577L;
	
	private Long fileId;
	private String fileCode;
	private String fileName;
	private String acceptedFiles;
	private String thumbUrl;
	private Integer thumbnailWidth;
	private Integer thumbnailHeight;
	private String url;
	private String uri;
	
	public String getThumbUrl() {
		return thumbUrl;
	}
	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public String getFileCode() {
		return fileCode;
	}
	public void setFileCode(String fileCode) {
		this.fileCode = fileCode;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getAcceptedFiles() {
		return acceptedFiles;
	}
	public void setAcceptedFiles(String acceptedFiles) {
		this.acceptedFiles = acceptedFiles;
	}
	public Integer getThumbnailWidth() {
		return thumbnailWidth;
	}
	public void setThumbnailWidth(Integer thumbnailWidth) {
		this.thumbnailWidth = thumbnailWidth;
	}
	public Integer getThumbnailHeight() {
		return thumbnailHeight;
	}
	public void setThumbnailHeight(Integer thumbnailHeight) {
		this.thumbnailHeight = thumbnailHeight;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;

public class GeneralProductVO implements Serializable{
	private static final long serialVersionUID = 7760003387262454269L;
	private Long productId;
	private String productCode;
	private String productName;
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

}

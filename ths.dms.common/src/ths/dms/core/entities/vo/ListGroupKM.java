package ths.dms.core.entities.vo;

import java.util.ArrayList;

public class ListGroupKM extends ArrayList<GroupKM> {
	@Override
	public int indexOf(Object obj) {
		if(obj instanceof Long) {
			long levelIndex = ((Long) obj).longValue();
			for(int i = 0; i < this.size(); i++) {
				for(GroupSP __obj : this.get(i).lstLevel) {
					if(__obj.index == levelIndex) {
						return i;
					}
				}
			}
			return -1;
		} else {
			return super.indexOf(obj);
		}
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;

public class CustomerGTVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//mã nhóm giao hang, tên nhóm giao hàng, mã khách hàng, tên khách hàng, đơn vị
	private String groupTransferCode;
	
	private String groupTransferName;
	
	private String shortCode;
	
	private String customerName;
	
	private String shopCode;

	public String getGroupTransferCode() {
		return groupTransferCode;
	}

	public void setGroupTransferCode(String groupTransferCode) {
		this.groupTransferCode = groupTransferCode;
	}

	public String getGroupTransferName() {
		return groupTransferName;
	}

	public void setGroupTransferName(String groupTransferName) {
		this.groupTransferName = groupTransferName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
}

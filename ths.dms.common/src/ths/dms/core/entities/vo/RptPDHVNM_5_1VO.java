package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RptPDHVNM_5_1VO implements Serializable {
	/**
	 * 
	 */
	private Long poAutoId;
	private String ngayDatHang;
	private String donHangSo;
	private String trangThai;
	private Integer sku;
	private BigDecimal tongTien;
	private List<RptPDHVNM_5_1_CT_VO> lstDetail;
	public String getNgayDatHang() {
		return ngayDatHang;
	}
	public void setNgayDatHang(String ngayDatHang) {
		this.ngayDatHang = ngayDatHang;
	}
	public String getDonHangSo() {
		return donHangSo;
	}
	public void setDonHangSo(String donHangSo) {
		this.donHangSo = donHangSo;
	}
	public String getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}
	public Integer getSku() {
		return sku;
	}
	public void setSku(Integer sku) {
		this.sku = sku;
	}
	public BigDecimal getTongTien() {
		return tongTien;
	}
	public void setTongTien(BigDecimal tongTien) {
		this.tongTien = tongTien;
	}
	public List<RptPDHVNM_5_1_CT_VO> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<RptPDHVNM_5_1_CT_VO> lstDetail) {
		this.lstDetail = lstDetail;
	}
	public Long getPoAutoId() {
		return poAutoId;
	}
	public void setPoAutoId(Long poAutoId) {
		this.poAutoId = poAutoId;
	}
	
}
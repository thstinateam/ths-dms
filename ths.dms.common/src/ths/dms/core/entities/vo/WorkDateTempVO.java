package ths.dms.core.entities.vo;

import java.io.Serializable;



public class WorkDateTempVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer date;
	private Integer month;
	private String reason;
	public Integer getDate() {
		return date;
	}
	public void setDate(Integer date) {
		this.date = date;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
}
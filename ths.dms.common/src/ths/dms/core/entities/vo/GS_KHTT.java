package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class GS_KHTT implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	
	private String maVung;
	private String maMien;
	private String maNPP;
	private String tenNPP;
	private String maGSNPP;
	private String tenGSNPP;
	private String maNVBH;
	private String tenNVBH;
	private String maTuyen;
	private String tenTuyen;
	private String maSP;
	private String tenSP;
	private BigDecimal gia;
	private BigDecimal giachuaVAT;
	private BigDecimal quantityPlan;
	private BigDecimal quantityApproved;
	private BigDecimal quantityProgress;
	private BigDecimal amountPlan;
	private BigDecimal amountApproved;
	private BigDecimal amountProgress;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getMaVung() {
		return maVung;
	}

	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}

	public String getMaMien() {
		return maMien;
	}

	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getTenNPP() {
		return tenNPP;
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}

	public String getMaGSNPP() {
		return maGSNPP;
	}

	public void setMaGSNPP(String maGSNPP) {
		this.maGSNPP = maGSNPP;
	}

	public String getTenGSNPP() {
		return tenGSNPP;
	}

	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}

	public String getMaNVBH() {
		return maNVBH;
	}

	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}

	public String getTenNVBH() {
		return tenNVBH;
	}

	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}

	public String getMaTuyen() {
		return maTuyen;
	}

	public void setMaTuyen(String maTuyen) {
		this.maTuyen = maTuyen;
	}

	public String getTenTuyen() {
		return tenTuyen;
	}

	public void setTenTuyen(String tenTuyen) {
		this.tenTuyen = tenTuyen;
	}

	public String getMaSP() {
		return maSP;
	}

	public void setMaSP(String maSP) {
		this.maSP = maSP;
	}

	public String getTenSP() {
		return tenSP;
	}

	public void setTenSP(String tenSP) {
		this.tenSP = tenSP;
	}

	public BigDecimal getGia() {
		return gia;
	}

	public void setGia(BigDecimal gia) {
		this.gia = gia;
	}

	public BigDecimal getGiachuaVAT() {
		return giachuaVAT;
	}

	public void setGiachuaVAT(BigDecimal giachuaVAT) {
		this.giachuaVAT = giachuaVAT;
	}

	public BigDecimal getQuantityPlan() {
		return quantityPlan;
	}

	public void setQuantityPlan(BigDecimal quantityPlan) {
		this.quantityPlan = quantityPlan;
	}

	public BigDecimal getQuantityApproved() {
		return quantityApproved;
	}

	public void setQuantityApproved(BigDecimal quantityApproved) {
		this.quantityApproved = quantityApproved;
	}

	public BigDecimal getQuantityProgress() {
		return quantityProgress;
	}

	public void setQuantityProgress(BigDecimal quantityProgress) {
		this.quantityProgress = quantityProgress;
	}

	public BigDecimal getAmountPlan() {
		return amountPlan;
	}

	public void setAmountPlan(BigDecimal amountPlan) {
		this.amountPlan = amountPlan;
	}

	public BigDecimal getAmountApproved() {
		return amountApproved;
	}

	public void setAmountApproved(BigDecimal amountApproved) {
		this.amountApproved = amountApproved;
	}

	public BigDecimal getAmountProgress() {
		return amountProgress;
	}

	public void setAmountProgress(BigDecimal amountProgress) {
		this.amountProgress = amountProgress;
	}
}

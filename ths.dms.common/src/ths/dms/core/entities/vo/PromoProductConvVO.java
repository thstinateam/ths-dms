package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromoProductConvVO implements Serializable {
	Long id;
	Long promotionProductConvertId;
	Long productId1;
	Long productId0;
	Integer isSourceProduct;
	BigDecimal factor1;
	BigDecimal factor0;
	Integer status;
	Long promotionProgramId;
	BigDecimal quantity1;
	BigDecimal quantity0;
	BigDecimal amount1;
	BigDecimal amount0;
	
	
	
	public BigDecimal getQuantity1() {
		return quantity1;
	}
	public void setQuantity1(BigDecimal quantity1) {
		this.quantity1 = quantity1;
	}
	public BigDecimal getQuantity0() {
		return quantity0;
	}
	public void setQuantity0(BigDecimal quantity0) {
		this.quantity0 = quantity0;
	}
	public BigDecimal getAmount1() {
		return amount1;
	}
	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}
	public BigDecimal getAmount0() {
		return amount0;
	}
	public void setAmount0(BigDecimal amount0) {
		this.amount0 = amount0;
	}
	public Long getProductId1() {
		return productId1;
	}
	public void setProductId1(Long productId1) {
		this.productId1 = productId1;
	}
	public Long getProductId0() {
		return productId0;
	}
	public void setProductId0(Long productId0) {
		this.productId0 = productId0;
	}
	
	public BigDecimal getFactor1() {
		return factor1;
	}
	public void setFactor1(BigDecimal factor1) {
		this.factor1 = factor1;
	}
	public BigDecimal getFactor0() {
		return factor0;
	}
	public void setFactor0(BigDecimal factor0) {
		this.factor0 = factor0;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPromotionProductConvertId() {
		return promotionProductConvertId;
	}
	public void setPromotionProductConvertId(Long promotionProductConvertId) {
		this.promotionProductConvertId = promotionProductConvertId;
	}
	public Integer getIsSourceProduct() {
		return isSourceProduct;
	}
	public void setIsSourceProduct(Integer isSourceProduct) {
		this.isSourceProduct = isSourceProduct;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getPromotionProgramId() {
		return promotionProgramId;
	}
	public void setPromotionProgramId(Long promotionProgramId) {
		this.promotionProgramId = promotionProgramId;
	}
	
	
}

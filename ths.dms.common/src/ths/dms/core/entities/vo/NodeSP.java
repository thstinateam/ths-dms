package ths.dms.core.entities.vo;

import java.math.BigDecimal;

public class NodeSP extends Node {
		public NodeSP(String __productCode, Integer __quantity) {
			this.productCode = __productCode.toUpperCase().trim();
			this.quantity = __quantity;
			this.isRequired = false;
		}
		public NodeSP(String __productCode, BigDecimal __amount) {
			this.productCode = __productCode.toUpperCase().trim();
			this.amount = __amount;
			this.isRequired = false;
		}
		public NodeSP(String __productCode, Integer __quantity, Boolean __isRequired) {
			this.productCode = __productCode.toUpperCase().trim();
			this.quantity = __quantity;
			this.isRequired = __isRequired;
		}
		
		public NodeSP(String __productCode, BigDecimal __amount, Boolean __isRequired) {
			this.productCode = __productCode.toUpperCase().trim();
			this.amount = __amount;
			this.isRequired = __isRequired;
		}
		public NodeSP(Node node){
			this.productCode=node.productCode;
			this.amount=node.amount;
			this.isRequired=node.isRequired;
			this.quantity=node.quantity;
			this.percent=node.percent;
		}
	}
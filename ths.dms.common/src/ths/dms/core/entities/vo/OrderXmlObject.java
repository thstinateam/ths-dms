/*
 * Copyright YYYY Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;


/**
 * Doi tuong don hang map voi file xml
 * 
 * @author lacnv1
 * @since Mar 14, 2015
 */
public class OrderXmlObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Long> lstOrderId;
	private String fileName;
	private String xmlContent;

	public List<Long> getLstOrderId() {
		return lstOrderId;
	}

	public void setLstOrderId(List<Long> lstOrderId) {
		this.lstOrderId = lstOrderId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getXmlContent() {
		return xmlContent;
	}

	public void setXmlContent(String xmlContent) {
		this.xmlContent = xmlContent;
	}
}
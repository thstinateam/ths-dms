package ths.dms.core.entities.vo;

import java.io.Serializable;


public class SaleOrderNumberVO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String orderNumber;
	//staffId id nhan vien ban hang

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
}
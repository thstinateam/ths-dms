package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;


public class SaleProductVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private SaleOrderDetail salesOrderDetail;
	
	private List<SaleOrderLot> lstSaleOrderLot;
	
	private String lstProgramDat;// danh sach CTKM dat cua SP ban
	
	private String isUsingZVOrder;
	
	private String isUsingZVCode;
	
	public String getIsUsingZVCode() {
		return isUsingZVCode;
	}

	public void setIsUsingZVCode(String isUsingZVCode) {
		this.isUsingZVCode = isUsingZVCode;
	}

	public String getIsUsingZVOrder() {
		return isUsingZVOrder;
	}

	public void setIsUsingZVOrder(String isUsingZVOrder) {
		this.isUsingZVOrder = isUsingZVOrder;
	}

	public SaleOrderDetail getSaleOrderDetail() {
		return salesOrderDetail;
	}

	public void setSaleOrderDetail(SaleOrderDetail salesOrderDetail) {
		this.salesOrderDetail = salesOrderDetail;
	}

	public List<SaleOrderLot> getLstSaleOrderLot() {
		return lstSaleOrderLot;
	}

	public void setLstSaleOrderLot(List<SaleOrderLot> lstSaleOrderLot) {
		this.lstSaleOrderLot = lstSaleOrderLot;
	}

	public String getLstProgramDat() {
		return lstProgramDat;
	}

	public void setLstProgramDat(String lstProgramDat) {
		this.lstProgramDat = lstProgramDat;
	}

	
}
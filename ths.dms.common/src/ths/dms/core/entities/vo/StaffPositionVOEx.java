package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

public class StaffPositionVOEx implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long staffId;
	private String staffCode;
	private String staffName;
	private Long staffOwnerId;
	private String staffOwnerCode;
	private String staffOwnerName;
	private Float lat;
	private Float lng;
	private Long shopId;
	private String shopCode;
	private String shopName;
	private Integer roleType;
	private Integer countVisit;
	private Float accuracy;
	private Date createTime;
	private Integer countStaff;
	private String hhmm;
	private Boolean isBold;
	private Integer ordinalVisit;
	private Integer isOr;
	private Float cusLat;
	private Float cusLng;
	
	private String customerCode;
	private String customerName;
	private String address; //housenumber - street
	
	private String mobilephone;
	
	private String phone;	
	
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Float getLat() {
		return lat;
	}
	public void setLat(Float lat) {
		this.lat = lat;
	}
	public Float getLng() {
		return lng;
	}
	public void setLng(Float lng) {
		this.lng = lng;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Integer getRoleType() {
		return roleType;
	}
	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}
	public Integer getCountVisit() {
		return countVisit;
	}
	public void setCountVisit(Integer countVisit) {
		this.countVisit = countVisit;
	}
	public Float getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(Float accuracy) {
		this.accuracy = accuracy;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getCountStaff() {
		return countStaff;
	}
	public void setCountStaff(Integer countStaff) {
		this.countStaff = countStaff;
	}
	public Long getStaffOwnerId() {
		return staffOwnerId;
	}
	public void setStaffOwnerId(Long staffOwnerId) {
		this.staffOwnerId = staffOwnerId;
	}
	public String getStaffOwnerCode() {
		return staffOwnerCode;
	}
	public Boolean getIsBold() {
		return isBold;
	}
	public void setIsBold(Boolean isBold) {
		this.isBold = isBold;
	}
	public void setStaffOwnerCode(String staffOwnerCode) {
		this.staffOwnerCode = staffOwnerCode;
	}
	public String getStaffOwnerName() {
		return staffOwnerName;
	}
	public void setStaffOwnerName(String staffOwnerName) {
		this.staffOwnerName = staffOwnerName;
	}
	public String getHhmm() {
		return hhmm;
	}
	public void setHhmm(String hhmm) {
		this.hhmm = hhmm;
	}
	public Integer getOrdinalVisit() {
		return ordinalVisit;
	}
	public void setOrdinalVisit(Integer ordinalVisit) {
		this.ordinalVisit = ordinalVisit;
	}
	public Float getCusLat() {
		return cusLat;
	}
	public void setCusLat(Float cusLat) {
		this.cusLat = cusLat;
	}
	public Float getCusLng() {
		return cusLng;
	}
	public void setCusLng(Float cusLng) {
		this.cusLng = cusLng;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobilephone() {
		return mobilephone;
	}
	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Integer getIsOr() {
		return isOr;
	}
	public void setIsOr(Integer isOr) {
		this.isOr = isOr;
	}	
	
}

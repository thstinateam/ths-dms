package ths.dms.core.entities.vo;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.CommercialSupportType;

public class CommercialSupportCodeVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long shopId;
	
	private Long productId;
	
	private Long customerId;
	
	private String code;
	
	private CommercialSupportType type;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public CommercialSupportType getType() {
		return type;
	}

	public void setType(CommercialSupportType type) {
		this.type = type;
	}

	
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;

import ths.dms.core.entities.CycleCountResult;

public class CycleCountResultVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CycleCountResult cycleCountResult;
	
	private Boolean isUpdateQuantBfrCount;

	public CycleCountResult getCycleCountResult() {
		return cycleCountResult;
	}

	public void setCycleCountResult(CycleCountResult cycleCountResult) {
		this.cycleCountResult = cycleCountResult;
	}

	public Boolean getIsUpdateQuantBfrCount() {
		return isUpdateQuantBfrCount;
	}

	public void setIsUpdateQuantBfrCount(Boolean isUpdateQuantBfrCount) {
		this.isUpdateQuantBfrCount = isUpdateQuantBfrCount;
	}
	
	
}

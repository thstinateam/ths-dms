package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class GroupKM implements Serializable {
		public List<GroupSP> lstLevel = new ArrayList<GroupSP>();
		public String groupCode;
		public int order;
		
		public List<GroupSP> searchIndex(List<Long> lstIndex){
			List<GroupSP> lstGroupSP=new ArrayList<GroupSP>();
			if(lstIndex!=null){
				for(GroupSP groupSP : this.lstLevel) {
					if(lstIndex.contains(groupSP.index)){
						lstGroupSP.add(groupSP);
					}
				}
			}
			return lstGroupSP;
		}
		
		public GroupSP searchNode(Node node) {
			for(GroupSP groupSP : this.lstLevel) {
				for(int i = 0; i < groupSP.lstSP.size(); i++) {
					if(node.productCode != null && !node.productCode.equals("") && node.productCode.equals(groupSP.lstSP.get(i).productCode) && node.quantity != null && node.quantity.equals(groupSP.lstSP.get(i).quantity)) {
						return groupSP;
					} else if(node.productCode != null && !node.productCode.equals("") && node.productCode.equals(groupSP.lstSP.get(i).productCode) && node.amount != null && node.amount.equals(groupSP.lstSP.get(i).amount)) {
						return groupSP;
					} else if(node.amount != null && node.amount.equals(groupSP.lstSP.get(i).amount)) {
						return groupSP;
					}
				}
			}
			return null;
		}
		
		public GroupSP add2Level(String type, String productCode, Integer quantity, Boolean andOr, Long indexKM, Integer iRun, String[] arrFreeProduct, Integer[] arrFreeQuantity,List<GroupSP> lstLevelKM) {
			boolean isCreateNewLevel = false;
			if(lstLevelKM==null || lstLevelKM.size()==0){//chua co muc nao ket voi SPmua thi tao muc moi
				isCreateNewLevel=true;
			}
			if(lstLevel.isEmpty()) {
				NodeSP node = new NodeSP(productCode, quantity, andOr);
				GroupSP groupSP = new GroupSP();
				groupSP.order = lstLevel.size() + 1;
				groupSP.index = indexKM;
				groupSP.lstSP.add(node);
				lstLevel.add(groupSP);
				return groupSP;
			} else {
				if(!isCreateNewLevel){
					for(GroupSP groupSP : lstLevelKM) {
						for(Node node : groupSP.lstSP) {
							if(productCode.equals(node.productCode) && quantity.equals(node.quantity)) {
								return groupSP;
							}
						}
					}
				}
				if(lstLevel.size() == 1) {
					//moi chi co 1 level thoi => add sp nay vao danh sach sp cua level luon
					for(int i = 1; i <= lstLevel.get(0).lstSP.size(); i++) {
						if(productCode.equals(arrFreeProduct[iRun-i])) {
							isCreateNewLevel = true;
							break;
						}
					}
					if(!isCreateNewLevel) {
						for(int i = 0; i < lstLevel.get(lstLevel.size() - 1).lstSP.size(); i++) {
							if(productCode.equals(lstLevel.get(lstLevel.size() - 1).lstSP.get(i).productCode)) {
								isCreateNewLevel = true;
								break;
							}
						}
					}
					if(isCreateNewLevel) {
						NodeSP node = new NodeSP(productCode, quantity, andOr);
						GroupSP groupSP = new GroupSP();
						groupSP.order = lstLevel.size() + 1;
						groupSP.index = indexKM;
						groupSP.lstSP.add(node);
						lstLevel.add(groupSP);
						return groupSP;
					} else {
						NodeSP node = new NodeSP(productCode, quantity, andOr);
						lstLevel.get(lstLevel.size() - 1).lstSP.add(node);
						return lstLevel.get(lstLevel.size() - 1);
					}
				} else {
					GroupSP lastGroup = lstLevel.get(lstLevel.size() - 1);
					if(lstLevelKM!=null && lstLevelKM.size()>0){
						lastGroup=lstLevelKM.get(lstLevelKM.size() - 1);
					}
					for(int i = 1; i <= lastGroup.lstSP.size(); i++) {
						if(productCode.equals(arrFreeProduct[iRun - i])) {
							isCreateNewLevel = true;
							break;
						}
					}
					if(!isCreateNewLevel) {
						for(int i = 0; i < lstLevel.get(lstLevel.size() - 1).lstSP.size(); i++) {
							if(productCode.equals(lstLevel.get(lstLevel.size() - 1).lstSP.get(i).productCode)) {
								isCreateNewLevel = true;
								break;
							}
						}
					}
					if(isCreateNewLevel) {
						NodeSP node = new NodeSP(productCode, quantity, andOr);
						GroupSP groupSP = new GroupSP();
						groupSP.order = lstLevel.size() + 1;
						groupSP.index = indexKM;
						groupSP.lstSP.add(node);
						lstLevel.add(groupSP);
						return groupSP;
					} else {
						NodeSP node = new NodeSP(productCode, quantity, andOr);
						lastGroup.lstSP.add(node);
						return lastGroup;
					}
				}
			}
		}
		
		public GroupSP add2Level(BigDecimal amount, Long indexKM, Integer iRun, BigDecimal[] arrFreeAmount,List<GroupSP> lstLevelKM) {
			boolean isCreateNewLevel = false;
			if(lstLevelKM==null || lstLevelKM.size()==0){//chua co muc nao ket voi SPmua thi tao muc moi
				isCreateNewLevel=true;
			}
			if(lstLevel.isEmpty()) {
				NodeAmount node = new NodeAmount(amount);
				GroupSP groupSP = new GroupSP();
				groupSP.order = lstLevel.size() + 1;
				groupSP.index = indexKM;
				groupSP.lstSP.add(node);
				lstLevel.add(groupSP);
				return groupSP;
			} else {
				if(!isCreateNewLevel){// bat buoc tao level moi thi khong can duyet
					if(lstLevelKM.size()>0){
						for(GroupSP __groupSP : lstLevelKM) {//tim kiem trong cac muc da duoc map voi SP mua
							for(int i = 0; i < __groupSP.lstSP.size(); i++) {
								if(amount.equals(__groupSP.lstSP.get(i).amount)) {
									return __groupSP;
								}
							}
						}
					}else{
						for(GroupSP __groupSP : lstLevel) {//tim tat ca
							for(int i = 0; i < __groupSP.lstSP.size(); i++) {
								if(amount.equals(__groupSP.lstSP.get(i).amount)) {
									return __groupSP;
								}
							}
						}
					}
				}
				NodeAmount node = new NodeAmount(amount);
				GroupSP groupSP = new GroupSP();
				groupSP.order = lstLevel.size() + 1;
				groupSP.index = indexKM;
				groupSP.lstSP.add(node);
				lstLevel.add(groupSP);
				return groupSP;
			}
		}
		
		public GroupSP add2Level(Float percent, Long indexKM, Integer iRun, Float[] arrAmount,List<GroupSP> lstLevelKM) {
			boolean isCreateNewLevel = false;
			if(lstLevelKM==null || lstLevelKM.size()==0){//chua co muc nao ket voi SPmua thi tao muc moi
				isCreateNewLevel=true;
			}
			if(lstLevel.isEmpty()) {
				NodePercent node = new NodePercent(percent);
				GroupSP groupSP = new GroupSP();
				groupSP.order = lstLevel.size() + 1;
				groupSP.index = indexKM;
				groupSP.lstSP.add(node);
				lstLevel.add(groupSP);
				return groupSP;
			} else {
				if(!isCreateNewLevel){// bat buoc tao level moi thi khong can duyet
					if(lstLevelKM.size()>0){
						for(GroupSP __groupSP : lstLevelKM) {//tim kiem trong cac muc da duoc map voi SP mua
							for(int i = 0; i < __groupSP.lstSP.size(); i++) {
								if(percent.equals(__groupSP.lstSP.get(i).percent)) {
									return __groupSP;
								}
							}
						}
					}else{
						for(GroupSP __groupSP : lstLevel) {//tim tat ca
							for(int i = 0; i < __groupSP.lstSP.size(); i++) {
								if(percent.equals(__groupSP.lstSP.get(i).percent)) {
									return __groupSP;
								}
							}
						}
					}
				}
				NodePercent node = new NodePercent(percent);
				GroupSP groupSP = new GroupSP();
				groupSP.order = lstLevel.size() + 1;
				groupSP.index = indexKM;
				groupSP.lstSP.add(node);
				lstLevel.add(groupSP);
				return groupSP;
			}
		}
	}
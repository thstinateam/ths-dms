package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptPDHVNM_5_1_CT_VO implements Serializable {
	/**
	 * 
	 */
	private String maHang;
	private String tenHang;
	private String donViTinh;
	private Integer soLuong;
	private BigDecimal donGia;
	private BigDecimal thanhTien;
	private String ngayYeuCauGiaoHang;
	public String getMaHang() {
		return maHang;
	}
	public void setMaHang(String maHang) {
		this.maHang = maHang;
	}
	public String getTenHang() {
		return tenHang;
	}
	public void setTenHang(String tenHang) {
		this.tenHang = tenHang;
	}
	public String getDonViTinh() {
		return donViTinh;
	}
	public void setDonViTinh(String donViTinh) {
		this.donViTinh = donViTinh;
	}
	public Integer getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(Integer soLuong) {
		this.soLuong = soLuong;
	}
	public BigDecimal getDonGia() {
		return donGia;
	}
	public void setDonGia(BigDecimal donGia) {
		this.donGia = donGia;
	}
	public BigDecimal getThanhTien() {
		return thanhTien;
	}
	public void setThanhTien(BigDecimal thanhTien) {
		this.thanhTien = thanhTien;
	}
	public String getNgayYeuCauGiaoHang() {
		return ngayYeuCauGiaoHang;
	}
	public void setNgayYeuCauGiaoHang(String ngayYeuCauGiaoHang) {
		this.ngayYeuCauGiaoHang = ngayYeuCauGiaoHang;
	}
	
	

}
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class GroupLevelDetailVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	Long id;
	Long groupLevelId;
	Long productGroupId;
	String productCode;
	String productName;
	Integer convfact;
	Integer valueType;
	BigDecimal value;
	BigDecimal valueActually;
	Integer isRequired;
	Integer orderNumber;
	Integer minQuantity;
	Integer maxQuantity;
	private Integer maxQuantityFree;
	BigDecimal minAmount;
	BigDecimal maxAmount;
	Integer hasProduct;
	Integer groupLevelDetailId;
	Long productId;
	Long warehouseId;
	List<GroupLevelDetailVO> buyProducts;
	List<GroupLevelDetailVO> promoProducts;
	List<GroupLevelDetailVO> shareDiscount;
	String programCode;
	Integer payingOrder;
	Float percent;
	BigDecimal valueAmount;//quantity quy doi ra amount
	private List<SaleOrderLotVO> lstSaleOrderLot;
	Integer promotion;//so lan dat cua KM tien, %
	
	
	
	public List<GroupLevelDetailVO> getShareDiscount() {
		return shareDiscount;
	}
	public void setShareDiscount(List<GroupLevelDetailVO> shareDiscount) {
		this.shareDiscount = shareDiscount;
	}
	public Integer getPromotion() {
		return promotion;
	}
	public void setPromotion(Integer promotion) {
		this.promotion = promotion;
	}
	public BigDecimal getValueAmount() {
		return valueAmount;
	}
	public void setValueAmount(BigDecimal valueAmount) {
		this.valueAmount = valueAmount;
	}
	public Float getPercent() {
		return percent;
	}
	public void setPercent(Float percent) {
		this.percent = percent;
	}
	public Integer getPayingOrder() {
		return payingOrder;
	}
	public void setPayingOrder(Integer payingOrder) {
		this.payingOrder = payingOrder;
	}
	public Long getProductGroupId() {
		return productGroupId;
	}
	public void setProductGroupId(Long productGroupId) {
		this.productGroupId = productGroupId;
	}
	public List<GroupLevelDetailVO> getPromoProducts() {
		return promoProducts;
	}
	public void setPromoProducts(List<GroupLevelDetailVO> promoProducts) {
		this.promoProducts = promoProducts;
	}
	public BigDecimal getValueActually() {
		return valueActually;
	}
	public void setValueActually(BigDecimal valueActually) {
		this.valueActually = valueActually;
	}
	public Long getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}
	public String getProgramCode() {
		return programCode;
	}
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}
	public List<GroupLevelDetailVO> getBuyProducts() {
		return buyProducts;
	}
	public void setBuyProducts(List<GroupLevelDetailVO> buyProducts) {
		this.buyProducts = buyProducts;
	}
	
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public Integer getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Integer getMinQuantity() {
		return minQuantity;
	}
	public void setMinQuantity(Integer minQuantity) {
		this.minQuantity = minQuantity;
	}
	public Integer getMaxQuantity() {
		return maxQuantity;
	}
	public void setMaxQuantity(Integer maxQuantity) {
		this.maxQuantity = maxQuantity;
	}
	public BigDecimal getMinAmount() {
		return minAmount;
	}
	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}
	public BigDecimal getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}
	public Integer getHasProduct() {
		return hasProduct;
	}
	public void setHasProduct(Integer hasProduct) {
		this.hasProduct = hasProduct;
	}
	public Integer getGroupLevelDetailId() {
		return groupLevelDetailId;
	}
	public void setGroupLevelDetailId(Integer groupLevelDetailId) {
		this.groupLevelDetailId = groupLevelDetailId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getGroupLevelId() {
		return groupLevelId;
	}
	public void setGroupLevelId(Long groupLevelId) {
		this.groupLevelId = groupLevelId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getValueType() {
		return valueType;
	}
	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public Integer getIsRequired() {
		return isRequired;
	}
	public void setIsRequired(Integer isRequired) {
		this.isRequired = isRequired;
	}
	public List<SaleOrderLotVO> getLstSaleOrderLot() {
		return lstSaleOrderLot;
	}
	public void setLstSaleOrderLot(List<SaleOrderLotVO> lstSaleOrderLot) {
		this.lstSaleOrderLot = lstSaleOrderLot;
	}
	public Integer getMaxQuantityFree() {
		return maxQuantityFree;
	}
	public void setMaxQuantityFree(Integer maxQuantityFree) {
		this.maxQuantityFree = maxQuantityFree;
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

public class CustomerPositionVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private String customerCode;
	private String shortCode;
	private String customerName;
	private String address;
	private String mobiphone;
	private String phone;
	private Double customerLat;
	private Double customerLng;
	private Integer seq;
	private BigDecimal distanceOrder;
	private Integer objectType;
	private Date startTime;
	private Date endtime;
	private Double staffLat;
	private Double staffLng;
	private Double lastStaffLat;
	private Double lastStaffLng;
	private Integer isOr;
	private Integer type;//0: ngoai tuyen; //1: dang ghe tham; 2: ghe tham khong co don hang; 3; ghe tham co don hang; 4: chua ghe tham
	
	public void safeSetNull() throws IllegalArgumentException, IllegalAccessException{
		for(Field field:getClass().getDeclaredFields()) {
			if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
				field.set(this, BigDecimal.ZERO);
			}
			if(field.getType().equals(String.class) && field.get(this) == null) {
				field.set(this, "");
			}
		}
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobiphone() {
		return mobiphone;
	}
	public void setMobiphone(String mobiphone) {
		this.mobiphone = mobiphone;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Double getCustomerLat() {
		return customerLat;
	}
	public void setCustomerLat(Double customerLat) {
		this.customerLat = customerLat;
	}
	public Double getCustomerLng() {
		return customerLng;
	}
	public void setCustomerLng(Double customerLng) {
		this.customerLng = customerLng;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public BigDecimal getDistanceOrder() {
		return distanceOrder;
	}
	public void setDistanceOrder(BigDecimal distanceOrder) {
		this.distanceOrder = distanceOrder;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndtime() {
		return endtime;
	}
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	public Double getStaffLat() {
		return staffLat;
	}
	public void setStaffLat(Double staffLat) {
		this.staffLat = staffLat;
	}
	public Double getStaffLng() {
		return staffLng;
	}
	public void setStaffLng(Double staffLng) {
		this.staffLng = staffLng;
	}
	public Integer getIsOr() {
		return isOr;
	}
	public void setIsOr(Integer isOr) {
		this.isOr = isOr;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public Double getLastStaffLat() {
		return lastStaffLat;
	}
	public void setLastStaffLat(Double lastStaffLat) {
		this.lastStaffLat = lastStaffLat;
	}
	public Double getLastStaffLng() {
		return lastStaffLng;
	}
	public void setLastStaffLng(Double lastStaffLng) {
		this.lastStaffLng = lastStaffLng;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	
}

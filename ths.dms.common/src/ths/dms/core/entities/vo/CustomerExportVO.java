/**
 * Copyright (c) 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.core.entities.vo.rpt.DynamicVO;

/**
 * Doi tuong luu thong tin khach hang export
 * @author tuannd20
 * @since 04/09/2015
 */
public class CustomerExportVO implements Serializable {
	private static final long serialVersionUID = 6122738376973413486L;
	
	private String regionName;
	private String areaCode;
	private String areaName;
	private String shopCode;
	private String shopName;
	private String outletCode;
	private String outletName;
	private String houseNumber;
	private String street;
	private String wardCode;
	private String wardName;
	private String districtCode;
	private String districtName;
	private String provinceCode;
	private String provinceName;
	private String telephone;
	private String outletType;
	private String status;
	private String contactPerson;
	private String deliveryRoute;
	private String outletOwnerBirthday;
	private String bankNumber;
	private String bankName;
	private String bankBranch;
	private String bankAccountOwner;
	private String lastUser;
	private String customerEmail;
	private String customerMobiphone;
	private String customerFax;
	private String frequency;
	private String lat;
	private String lng;
	private String deliverier;
	private String cashier;
	private String maxDebitAmount;
	private String maxDebitDate;
	private String invoiceTax;
	private String invoiceNumberAccount;
	private String invoiceOutletName;
	private String invoiceCompanyName;
	private String invoiceNameBank;
	private String invoiceNameBranchBank;
	private String deliveryAddress;
	private String openDate;
	private String closeDate;
	private String isVat;
	private String salePosition;
	private List<DynamicVO> dynamicAttributes;
	
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getOutletCode() {
		return outletCode;
	}
	public void setOutletCode(String outletCode) {
		this.outletCode = outletCode;
	}
	public String getOutletName() {
		return outletName;
	}
	public void setOutletName(String outletName) {
		this.outletName = outletName;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getWardCode() {
		return wardCode;
	}
	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}
	public String getWardName() {
		return wardName;
	}
	public void setWardName(String wardName) {
		this.wardName = wardName;
	}
	public String getDistrictCode() {
		return districtCode;
	}
	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getProvinceCode() {
		return provinceCode;
	}
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getOutletType() {
		return outletType;
	}
	public void setOutletType(String outletType) {
		this.outletType = outletType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getDeliveryRoute() {
		return deliveryRoute;
	}
	public void setDeliveryRoute(String deliveryRoute) {
		this.deliveryRoute = deliveryRoute;
	}
	public String getOutletOwnerBirthday() {
		return outletOwnerBirthday;
	}
	public void setOutletOwnerBirthday(String outletOwnerBirthday) {
		this.outletOwnerBirthday = outletOwnerBirthday;
	}
	public String getBankNumber() {
		return bankNumber;
	}
	public void setBankNumber(String bankNumber) {
		this.bankNumber = bankNumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankBranch() {
		return bankBranch;
	}
	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}
	public String getBankAccountOwner() {
		return bankAccountOwner;
	}
	public void setBankAccountOwner(String bankAccountOwner) {
		this.bankAccountOwner = bankAccountOwner;
	}
	public String getLastUser() {
		return lastUser;
	}
	public void setLastUser(String lastUser) {
		this.lastUser = lastUser;
	}
	public List<DynamicVO> getDynamicAttributes() {
		return dynamicAttributes;
	}
	public void setDynamicAttributes(List<DynamicVO> dynamicAttributes) {
		this.dynamicAttributes = dynamicAttributes;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	public String getCustomerMobiphone() {
		return customerMobiphone;
	}
	public void setCustomerMobiphone(String customerMobiphone) {
		this.customerMobiphone = customerMobiphone;
	}
	public String getCustomerFax() {
		return customerFax;
	}
	public void setCustomerFax(String customerFax) {
		this.customerFax = customerFax;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getDeliverier() {
		return deliverier;
	}
	public void setDeliverier(String deliverier) {
		this.deliverier = deliverier;
	}
	public String getCashier() {
		return cashier;
	}
	public void setCashier(String cashier) {
		this.cashier = cashier;
	}
	public String getMaxDebitAmount() {
		return maxDebitAmount;
	}
	public void setMaxDebitAmount(String maxDebitAmount) {
		this.maxDebitAmount = maxDebitAmount;
	}
	public String getMaxDebitDate() {
		return maxDebitDate;
	}
	public void setMaxDebitDate(String maxDebitDate) {
		this.maxDebitDate = maxDebitDate;
	}
	public String getInvoiceTax() {
		return invoiceTax;
	}
	public void setInvoiceTax(String invoiceTax) {
		this.invoiceTax = invoiceTax;
	}
	public String getInvoiceNumberAccount() {
		return invoiceNumberAccount;
	}
	public void setInvoiceNumberAccount(String invoiceNumberAccount) {
		this.invoiceNumberAccount = invoiceNumberAccount;
	}
	public String getInvoiceOutletName() {
		return invoiceOutletName;
	}
	public void setInvoiceOutletName(String invoiceOutletName) {
		this.invoiceOutletName = invoiceOutletName;
	}
	public String getInvoiceCompanyName() {
		return invoiceCompanyName;
	}
	public void setInvoiceCompanyName(String invoiceCompanyName) {
		this.invoiceCompanyName = invoiceCompanyName;
	}
	public String getInvoiceNameBank() {
		return invoiceNameBank;
	}
	public void setInvoiceNameBank(String invoiceNameBank) {
		this.invoiceNameBank = invoiceNameBank;
	}
	public String getInvoiceNameBranchBank() {
		return invoiceNameBranchBank;
	}
	public void setInvoiceNameBranchBank(String invoiceNameBranchBank) {
		this.invoiceNameBranchBank = invoiceNameBranchBank;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getOpenDate() {
		return openDate;
	}
	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}
	public String getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}
	public String getIsVat() {
		return isVat;
	}
	public void setIsVat(String isVat) {
		this.isVat = isVat;
	}
	public String getSalePosition() {
		return salePosition;
	}
	public void setSalePosition(String salePosition) {
		this.salePosition = salePosition;
	}
}

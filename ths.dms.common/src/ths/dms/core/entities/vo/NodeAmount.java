package ths.dms.core.entities.vo;

import java.math.BigDecimal;

public class NodeAmount extends Node {
		public NodeAmount(BigDecimal __amount) {
			this.amount = __amount;
			this.isRequired=true;
		}
		public NodeAmount(Node n){
			this.productCode=n.productCode;
			this.quantity=n.quantity;
			this.isRequired=n.isRequired;
			this.amount=n.amount;
			this.percent=n.percent;
		}
	}
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Transient;

public class CustomerVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long id; //
	
	private Integer status;
	private Integer maxDebitDate;
	private Integer isRouting;
	private Integer numSkuInDate;
	private Integer numOrderInMonth;
	private Integer ordinalVisit; // Thu tu vieng tham
	private Integer isOr;
	private Integer count; 
	private Integer objectType;


	private BigDecimal saleAmount;
	private BigDecimal amountPlan; // Doanh so ke hoach
	private BigDecimal amount; // Doanh so thuc hien
	private BigDecimal dsDuyet;
	private BigDecimal totalInDate;
	private BigDecimal avgTotalInLastTwoMonth;
	private BigDecimal totalInMonth;
	private BigDecimal quantityInDate;
	private BigDecimal avgQuantityInLastTwoMonth;
	private BigDecimal quantityInMonth;


	private Date startTime;
	private Date endTime;

	private String shortCode;
	private String customerCode;
	private String customerName;
	private String customerCodeName; //code - name
	private String address; //housenumber - street
	private String phone;
	private String mobiphone;
	private String shopTypeName;
	private String loyalty;
	private String contactName;
	private String street;
	private String wardName;
	private String districtName;
	private String provinceName;
	private String areaName;
	private String channelTypeCode;
	private String createDate;
	private String updateDate;
	private String lastApproveOrder;
	private String invoiceTax;
	private String invoiceNumberAccount;
	private String staffName;
	private String shopName;
	private String housenumber;
	private String channelTypeName;
	private Long shopId;
	private Long cycleId;
	private Integer valueShowPrice;
	private Integer isShowPrice;
	private Integer rewardType;
	private String shopCode;
	private String wardCode;
	private String districtCode;
	private String provinceCode;
	private BigDecimal amountEquip;
	private Long areaId;
	
	private String createUserName;
	private String routes;
	
	private String shopVungName;
	private String shopMienName;
	private String shopKenhName;
	private String shopVungCode;
	private String shopMienCode;
	private String shopKenhCode;
	
	public Integer getValueShowPrice() {
		return valueShowPrice;
	}
	public void setValueShowPrice(Integer valueShowPrice) {
		this.valueShowPrice = valueShowPrice;
	}
	public Integer getIsShowPrice() {
		return isShowPrice;
	}
	public void setIsShowPrice(Integer isShowPrice) {
		this.isShowPrice = isShowPrice;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	private String lat;
	private String lng;
	private String startTimeHHMM;
	private String endTimeHHMM;
	
	public String getChannelTypeName() {
		return channelTypeName;
	}
	public void setChannelTypeName(String channelTypeName) {
		this.channelTypeName = channelTypeName;
	}
	
	public BigDecimal getDsDuyet() {
		return dsDuyet;
	}
	public void setDsDuyet(BigDecimal dsDuyet) {
		this.dsDuyet = dsDuyet;
	}
	public String getHousenumber() {
		return housenumber;
	}
	public void setHousenumber(String housenumber) {
		this.housenumber = housenumber;
	}
	public BigDecimal getTotalInDate() {
		return totalInDate;
	}
	public void setTotalInDate(BigDecimal totalInDate) {
		this.totalInDate = totalInDate;
	}
	public Integer getNumSkuInDate() {
		return numSkuInDate;
	}
	public void setNumSkuInDate(Integer numSkuInDate) {
		this.numSkuInDate = numSkuInDate;
	}
	public Integer getNumOrderInMonth() {
		return numOrderInMonth;
	}
	public void setNumOrderInMonth(Integer numOrderInMonth) {
		this.numOrderInMonth = numOrderInMonth;
	}
	public BigDecimal getAvgTotalInLastTwoMonth() {
		return avgTotalInLastTwoMonth;
	}
	public void setAvgTotalInLastTwoMonth(BigDecimal avgTotalInLastTwoMonth) {
		this.avgTotalInLastTwoMonth = avgTotalInLastTwoMonth;
	}
	public BigDecimal getTotalInMonth() {
		return totalInMonth;
	}
	public void setTotalInMonth(BigDecimal totalInMonth) {
		this.totalInMonth = totalInMonth;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobiphone() {
		return mobiphone;
	}
	public void setMobiphone(String mobiphone) {
		this.mobiphone = mobiphone;
	}
	public String getShopTypeName() {
		return shopTypeName;
	}
	public void setShopTypeName(String shopTypeName) {
		this.shopTypeName = shopTypeName;
	}
	public String getLoyalty() {
		return loyalty;
	}
	public void setLoyalty(String loyalty) {
		this.loyalty = loyalty;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getWardName() {
		return wardName;
	}
	public void setWardName(String wardName) {
		this.wardName = wardName;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getChannelTypeCode() {
		return channelTypeCode;
	}
	public void setChannelTypeCode(String channelTypeCode) {
		this.channelTypeCode = channelTypeCode;
	}
	public Integer getMaxDebitDate() {
		return maxDebitDate;
	}
	public void setMaxDebitDate(Integer maxDebitDate) {
		this.maxDebitDate = maxDebitDate;
	}
	public BigDecimal getSaleAmount() {
		return saleAmount;
	}
	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getLastApproveOrder() {
		return lastApproveOrder;
	}
	public void setLastApproveOrder(String lastApproveOrder) {
		this.lastApproveOrder = lastApproveOrder;
	}
	public String getInvoiceTax() {
		return invoiceTax;
	}
	public void setInvoiceTax(String invoiceTax) {
		this.invoiceTax = invoiceTax;
	}
	public String getInvoiceNumberAccount() {
		return invoiceNumberAccount;
	}
	public void setInvoiceNumberAccount(String invoiceNumberAccount) {
		this.invoiceNumberAccount = invoiceNumberAccount;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Integer getIsRouting() {
		return isRouting;
	}
	public void setIsRouting(Integer isRouting) {
		this.isRouting = isRouting;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomerCodeName() {
		return customerCodeName;
	}
	public void setCustomerCodeName(String customerCodeName) {
		this.customerCodeName = customerCodeName;
	}
	public BigDecimal getAmountPlan() {
		return amountPlan;
	}
	public void setAmountPlan(BigDecimal amountPlan) {
		this.amountPlan = amountPlan;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Integer getOrdinalVisit() {
		return ordinalVisit;
	}
	public void setOrdinalVisit(Integer ordinalVisit) {
		this.ordinalVisit = ordinalVisit;
	}
	public Integer getObjectType() {
		return objectType;
	}
	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public String getStartTimeHHMM() {
		if(startTime!=null) {
			System.out.println(new SimpleDateFormat("HH").format(startTime)+':'+new SimpleDateFormat("mm").format(startTime));
			return new SimpleDateFormat("HH").format(startTime)+':'+new SimpleDateFormat("mm").format(startTime);
		}
		else return "";
	}
	public void setStartTimeHHMM(String startTimeHHMM) {
		this.startTimeHHMM = startTimeHHMM;
	}
	public String getEndTimeHHMM() {
		if(endTime!=null) {
			System.out.println(new SimpleDateFormat("HH").format(endTime)+':'+new SimpleDateFormat("mm").format(endTime));
			return new SimpleDateFormat("HH").format(endTime)+':'+new SimpleDateFormat("mm").format(endTime);
		}
		else return "";
	}
	public void setEndTimeHHMM(String endTimeHHMM) {
		this.endTimeHHMM = endTimeHHMM;
	}
	public Integer getIsOr() {
		return isOr;
	}
	public void setIsOr(Integer isOr) {
		this.isOr = isOr;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public BigDecimal getQuantityInDate() {
		return quantityInDate;
	}
	public void setQuantityInDate(BigDecimal quantityInDate) {
		this.quantityInDate = quantityInDate;
	}
	public BigDecimal getAvgQuantityInLastTwoMonth() {
		return avgQuantityInLastTwoMonth;
	}
	public void setAvgQuantityInLastTwoMonth(BigDecimal avgQuantityInLastTwoMonth) {
		this.avgQuantityInLastTwoMonth = avgQuantityInLastTwoMonth;
	}
	public BigDecimal getQuantityInMonth() {
		return quantityInMonth;
	}
	public void setQuantityInMonth(BigDecimal quantityInMonth) {
		this.quantityInMonth = quantityInMonth;
	}
	public Integer getRewardType() {
		return rewardType;
	}

	public void setRewardType(Integer rewardType) {
		this.rewardType = rewardType;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public Long getCycleId() {
		return cycleId;
	}
	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getRoutes() {
		return routes;
	}
	public void setRoutes(String routes) {
		this.routes = routes;
	}
		
	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	
	public BigDecimal getAmountEquip() {
		return amountEquip;
	}

	public void setAmountEquip(BigDecimal amountEquip) {
		this.amountEquip = amountEquip;
	}
	
	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}
	public String getShopVungName() {
		return shopVungName;
	}
	public void setShopVungName(String shopVungName) {
		this.shopVungName = shopVungName;
	}
	public String getShopMienName() {
		return shopMienName;
	}
	public void setShopMienName(String shopMienName) {
		this.shopMienName = shopMienName;
	}
	public String getShopKenhName() {
		return shopKenhName;
	}
	public void setShopKenhName(String shopKenhName) {
		this.shopKenhName = shopKenhName;
	}
	public String getShopVungCode() {
		return shopVungCode;
	}
	public void setShopVungCode(String shopVungCode) {
		this.shopVungCode = shopVungCode;
	}
	public String getShopMienCode() {
		return shopMienCode;
	}
	public void setShopMienCode(String shopMienCode) {
		this.shopMienCode = shopMienCode;
	}
	public String getShopKenhCode() {
		return shopKenhCode;
	}
	public void setShopKenhCode(String shopKenhCode) {
		this.shopKenhCode = shopKenhCode;
	}
}

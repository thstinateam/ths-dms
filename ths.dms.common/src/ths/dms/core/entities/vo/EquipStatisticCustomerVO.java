package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

/**
 * @author phut
 * @since September 10, 2014
 */
public class EquipStatisticCustomerVO implements Serializable {
	private static final long serialVersionUID = 4488396665144328036L;
	Long id;
	String shopCode;
	String shortCode;
	String customerName;
	String houseNumber;
	String street;
	String address;
	String staffCode;
	String staffName;
	String equipCode;
	Integer statusCus;
	String cusUpdateDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getEquipCode() {
		return equipCode;
	}
	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}
	public Integer getStatusCus() {
		return statusCus;
	}
	public void setStatusCus(Integer statusCus) {
		this.statusCus = statusCus;
	}
	public String getCusUpdateDate() {
		return cusUpdateDate;
	}
	public void setCusUpdateDate(String cusUpdateDate) {
		this.cusUpdateDate = cusUpdateDate;
	}
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.entities.enumtype.DebtPaymentType;
import ths.dms.core.entities.enumtype.PayReceivedType;

public class DebitCustomerParams implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long shopId;
	private Long customerId;
	private Long bankId;
	private String payReceivedNumber;
	private DebtPaymentType debtPaymentType;
	private BigDecimal amount;
	private List<Long> lstDebitId;
	private List<BigDecimal> lstDebitAmt;
	private List<BigDecimal> lstDiscount;
	private String createUser;
	private PayReceivedType payReceivedType;
	private List<Long> lstCustId;
	private String payerName;
	private String payerAddress;
	private String paymentReason;
	private Integer receiptType;
	private Integer paymentStatus;
	
	///
	private List<Long> lstFromObjectId;
	private List<String> lstCashierCode;
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getPayReceivedNumber() {
		return payReceivedNumber;
	}
	public void setPayReceivedNumber(String payReceivedNumber) {
		this.payReceivedNumber = payReceivedNumber;
	}
	public DebtPaymentType getDebtPaymentType() {
		return debtPaymentType;
	}
	public void setDebtPaymentType(DebtPaymentType debtPaymentType) {
		this.debtPaymentType = debtPaymentType;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public List<Long> getLstDebitId() {
		return lstDebitId;
	}
	public void setLstDebitId(List<Long> lstDebitId) {
		this.lstDebitId = lstDebitId;
	}
	public List<BigDecimal> getLstDebitAmt() {
		return lstDebitAmt;
	}
	public void setLstDebitAmt(List<BigDecimal> lstDebitAmt) {
		this.lstDebitAmt = lstDebitAmt;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public PayReceivedType getPayReceivedType() {
		return payReceivedType;
	}
	public void setPayReceivedType(PayReceivedType payReceivedType) {
		this.payReceivedType = payReceivedType;
	}
	public List<BigDecimal> getLstDiscount() {
		return lstDiscount;
	}
	public void setLstDiscount(List<BigDecimal> lstDiscount) {
		this.lstDiscount = lstDiscount;
	}
	public Long getBankId() {
		return bankId;
	}
	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}
	public List<Long> getLstCustId() {
		return lstCustId;
	}
	public void setLstCustId(List<Long> lstCustId) {
		this.lstCustId = lstCustId;
	}
	public String getPayerName() {
		return payerName;
	}
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}
	public String getPayerAddress() {
		return payerAddress;
	}
	public void setPayerAddress(String payerAddress) {
		this.payerAddress = payerAddress;
	}
	public String getPaymentReason() {
		return paymentReason;
	}
	public void setPaymentReason(String paymentReason) {
		this.paymentReason = paymentReason;
	}
	public Integer getReceiptType() {
		return receiptType;
	}
	public void setReceiptType(Integer receiptType) {
		this.receiptType = receiptType;
	}

	/**
	 * @lacnv1
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DebitCustomerParams)) {
			return false;
		}
		DebitCustomerParams objT = (DebitCustomerParams)obj;
		if (payReceivedNumber == null || objT.getPayReceivedNumber() == null) {
			return false;
		}
		return payReceivedNumber.trim().equalsIgnoreCase(objT.getPayReceivedNumber().trim());
	}
	
	/**
	 * @author vuongmq
	 * @since 17/02/2016
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	public Integer getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(Integer paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public List<Long> getLstFromObjectId() {
		return lstFromObjectId;
	}
	public void setLstFromObjectId(List<Long> lstFromObjectId) {
		this.lstFromObjectId = lstFromObjectId;
	}
	public List<String> getLstCashierCode() {
		return lstCashierCode;
	}
	public void setLstCashierCode(List<String> lstCashierCode) {
		this.lstCashierCode = lstCashierCode;
	}
}

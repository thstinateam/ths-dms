package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Datpv4
 * @since July 02,2015
 * @description Class EquipmentRepairFormDTlVO
 */
public class EquipStockAdjustFormDtlVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Integer equipStockAdjustFormId;
	private String code;
	private String note;
	private Long equipId;
	private String equipCode;
	private String serial;
	private String healthStatus;
	private String healthStatusStr;
	private Long equipGroupId;
	private String  equipGroupName;
	private String  capacity;
	private String  equipBrandName;//Hieu
	private Long EquipProviderId;
	private String  equipProviderName;//Nha cung cap
	private Integer manufacturingYear; 
	private BigDecimal price;
	private Long stockId;
	private String stockType;
	private String stockName;
	private String stockCode;
	private String  equipCateGogyName;//Loai thiet bi
	private String warrantyExpiredDate;
	private String equipGroupCode;
	private String  equipProviderCode;//Nha cung cap
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getEquipStockAdjustFormId() {
		return equipStockAdjustFormId;
	}
	public void setEquipStockAdjustFormId(Integer equipStockAdjustFormId) {
		this.equipStockAdjustFormId = equipStockAdjustFormId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Long getEquipId() {
		return equipId;
	}
	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}
	public String getEquipCode() {
		return equipCode;
	}
	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getHealthStatus() {
		return healthStatus;
	}
	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}
	public String getHealthStatusStr() {
		return healthStatusStr;
	}
	public void setHealthStatusStr(String healthStatusStr) {
		this.healthStatusStr = healthStatusStr;
	}
	public Long getEquipGroupId() {
		return equipGroupId;
	}
	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}
	public String getEquipGroupName() {
		return equipGroupName;
	}
	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getEquipBrandName() {
		return equipBrandName;
	}
	public void setEquipBrandName(String equipBrandName) {
		this.equipBrandName = equipBrandName;
	}
	public Long getEquipProviderId() {
		return EquipProviderId;
	}
	public void setEquipProviderId(Long equipProviderId) {
		EquipProviderId = equipProviderId;
	}
	
	public Integer getManufacturingYear() {
		return manufacturingYear;
	}
	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	public String getStockType() {
		return stockType;
	}
	public void setStockType(String stockType) {
		this.stockType = stockType;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public String getEquipCateGogyName() {
		return equipCateGogyName;
	}
	public void setEquipCateGogyName(String equipCateGogyName) {
		this.equipCateGogyName = equipCateGogyName;
	}
	public String getWarrantyExpiredDate() {
		return warrantyExpiredDate;
	}
	public void setWarrantyExpiredDate(String warrantyExpiredDate) {
		this.warrantyExpiredDate = warrantyExpiredDate;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getEquipProviderName() {
		return equipProviderName;
	}
	public void setEquipProviderName(String equipProviderName) {
		this.equipProviderName = equipProviderName;
	}
	public String getEquipGroupCode() {
		return equipGroupCode;
	}
	public void setEquipGroupCode(String equipGroupCode) {
		this.equipGroupCode = equipGroupCode;
	}
	public String getEquipProviderCode() {
		return equipProviderCode;
	}
	public void setEquipProviderCode(String equipProviderCode) {
		this.equipProviderCode = equipProviderCode;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;

import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipProvider;

/**
 * Class EquipmentRepairFormVO
 * 
 * @author Datpv4
 * @since July 02,2015
 * @description 
 */
public class EquipStockAdjustFormVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String code;
	private String description;
	private String createFormDate;
	private Integer status;	
	private String note;
	
	//Detail
	Integer equipId;
	String serial;
	Integer healthStatus;
	EquipGroup equipGroup;
	Integer capacityFrom;
	Integer capacityTo;
	EquipProvider equipProvider;
	String stockName;
	Integer stockType;
	Integer stockId;
	Integer manufacturingYear;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreateFormDate() {
		return createFormDate;
	}
	public void setCreateFormDate(String createFormDate) {
		this.createFormDate = createFormDate;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getEquipId() {
		return equipId;
	}
	public void setEquipId(Integer equipId) {
		this.equipId = equipId;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public Integer getHealthStatus() {
		return healthStatus;
	}
	public void setHealthStatus(Integer healthStatus) {
		this.healthStatus = healthStatus;
	}
	public EquipGroup getEquipGroup() {
		return equipGroup;
	}
	public void setEquipGroup(EquipGroup equipGroup) {
		this.equipGroup = equipGroup;
	}
	public Integer getCapacityFrom() {
		return capacityFrom;
	}
	public void setCapacityFrom(Integer capacityFrom) {
		this.capacityFrom = capacityFrom;
	}
	public Integer getCapacityTo() {
		return capacityTo;
	}
	public void setCapacityTo(Integer capacityTo) {
		this.capacityTo = capacityTo;
	}
	public EquipProvider getEquipProvider() {
		return equipProvider;
	}
	public void setEquipProvider(EquipProvider equipProvider) {
		this.equipProvider = equipProvider;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public Integer getStockType() {
		return stockType;
	}
	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}
	public Integer getStockId() {
		return stockId;
	}
	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}
	public Integer getManufacturingYear() {
		return manufacturingYear;
	}
	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}

	
	
	
	
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * level
 * @author phut
 *
 */
public class NewLevelMapping implements Serializable {
	Long mapId;
	String levelCode;
	Integer stt;
	Long levelMuaId;
	Long levelKMId;
	Long groupMuaId;
	Long groupKMId;
	Integer minQuantity;
	BigDecimal minAmount;
	Float percent;
	Integer maxQuantity;
	BigDecimal maxAmount;
	List<ExMapping> listExLevelMua;//sub level
	List<ExMapping> listExLevelKM;//sub level
	public Long getMapId() {
		return mapId;
	}
	public void setMapId(Long mapId) {
		this.mapId = mapId;
	}
	public String getLevelCode() {
		return levelCode;
	}
	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}
	public Integer getStt() {
		return stt;
	}
	public void setStt(Integer stt) {
		this.stt = stt;
	}
	public Long getLevelMuaId() {
		return levelMuaId;
	}
	public void setLevelMuaId(Long levelMuaId) {
		this.levelMuaId = levelMuaId;
	}
	public Long getLevelKMId() {
		return levelKMId;
	}
	public void setLevelKMId(Long levelKMId) {
		this.levelKMId = levelKMId;
	}
	public Long getGroupMuaId() {
		return groupMuaId;
	}
	public void setGroupMuaId(Long groupMuaId) {
		this.groupMuaId = groupMuaId;
	}
	public Long getGroupKMId() {
		return groupKMId;
	}
	public void setGroupKMId(Long groupKMId) {
		this.groupKMId = groupKMId;
	}
	public Integer getMinQuantity() {
		return minQuantity;
	}
	public void setMinQuantity(Integer minQuantity) {
		this.minQuantity = minQuantity;
	}
	public BigDecimal getMinAmount() {
		return minAmount;
	}
	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}
	public Float getPercent() {
		return percent;
	}
	public void setPercent(Float percent) {
		this.percent = percent;
	}
	public Integer getMaxQuantity() {
		return maxQuantity;
	}
	public void setMaxQuantity(Integer maxQuantity) {
		this.maxQuantity = maxQuantity;
	}
	public BigDecimal getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}
	public List<ExMapping> getListExLevelMua() {
		return listExLevelMua;
	}
	public void setListExLevelMua(List<ExMapping> listExLevelMua) {
		this.listExLevelMua = listExLevelMua;
	}
	public List<ExMapping> getListExLevelKM() {
		return listExLevelKM;
	}
	public void setListExLevelKM(List<ExMapping> listExLevelKM) {
		this.listExLevelKM = listExLevelKM;
	}
}

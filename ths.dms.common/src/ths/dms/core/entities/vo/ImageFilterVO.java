package ths.dms.core.entities.vo;

import java.io.Serializable;

public class ImageFilterVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1032145310615444248L;
	
	private Long displayProgrameId;
	private Long shopId;
	public Long getDisplayProgrameId() {
		return displayProgrameId;
	}
	public void setDisplayProgrameId(Long displayProgrameId) {
		this.displayProgrameId = displayProgrameId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
}

/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
 /**
 * @author vuongmq
 * @date 20/08/2015
 * @description Doanh so cua NVBH voi customer cho bang tong hop RPT_STAFF_SALE_DETAIL
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class CustomerSaleVO implements Serializable {
	private Long customerId;
	private String shortCode;
	private String customerCode;
	private String customerName;
	private String address;
	private BigDecimal dayAmountPlan;
	private BigDecimal dayAmountApproved;
	private BigDecimal dayAmount;
	private BigDecimal monthAmountPlan;
	private BigDecimal monthAmountApproved;
	private BigDecimal monthAmount;
	private Integer dayQuantityPlan;
	private Integer dayQuantityApproved;
	private Integer dayQuantity;
	private Integer monthQuantityPlan;
	private Integer monthQuantityApproved;
	private Integer monthQuantity;
	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public BigDecimal getDayAmountPlan() {
		return dayAmountPlan;
	}
	public void setDayAmountPlan(BigDecimal dayAmountPlan) {
		this.dayAmountPlan = dayAmountPlan;
	}
	public BigDecimal getDayAmountApproved() {
		return dayAmountApproved;
	}
	public void setDayAmountApproved(BigDecimal dayAmountApproved) {
		this.dayAmountApproved = dayAmountApproved;
	}
	public BigDecimal getDayAmount() {
		return dayAmount;
	}
	public void setDayAmount(BigDecimal dayAmount) {
		this.dayAmount = dayAmount;
	}
	public BigDecimal getMonthAmountPlan() {
		return monthAmountPlan;
	}
	public void setMonthAmountPlan(BigDecimal monthAmountPlan) {
		this.monthAmountPlan = monthAmountPlan;
	}
	public BigDecimal getMonthAmountApproved() {
		return monthAmountApproved;
	}
	public void setMonthAmountApproved(BigDecimal monthAmountApproved) {
		this.monthAmountApproved = monthAmountApproved;
	}
	public BigDecimal getMonthAmount() {
		return monthAmount;
	}
	public void setMonthAmount(BigDecimal monthAmount) {
		this.monthAmount = monthAmount;
	}
	public Integer getDayQuantityPlan() {
		return dayQuantityPlan;
	}
	public void setDayQuantityPlan(Integer dayQuantityPlan) {
		this.dayQuantityPlan = dayQuantityPlan;
	}
	public Integer getDayQuantityApproved() {
		return dayQuantityApproved;
	}
	public void setDayQuantityApproved(Integer dayQuantityApproved) {
		this.dayQuantityApproved = dayQuantityApproved;
	}
	public Integer getDayQuantity() {
		return dayQuantity;
	}
	public void setDayQuantity(Integer dayQuantity) {
		this.dayQuantity = dayQuantity;
	}
	public Integer getMonthQuantityPlan() {
		return monthQuantityPlan;
	}
	public void setMonthQuantityPlan(Integer monthQuantityPlan) {
		this.monthQuantityPlan = monthQuantityPlan;
	}
	public Integer getMonthQuantityApproved() {
		return monthQuantityApproved;
	}
	public void setMonthQuantityApproved(Integer monthQuantityApproved) {
		this.monthQuantityApproved = monthQuantityApproved;
	}
	public Integer getMonthQuantity() {
		return monthQuantity;
	}
	public void setMonthQuantity(Integer monthQuantity) {
		this.monthQuantity = monthQuantity;
	}
	
}

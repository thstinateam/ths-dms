package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class AmountAndAmountPlanVO implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2602571432058482862L;
	
	private BigDecimal dayAmountPlan;
	private BigDecimal dayAmount;
	private BigDecimal dayAmountApproved;
	private BigDecimal monthAmountPlan;
	private BigDecimal monthAmount;
	private BigDecimal monthAmountApproved;
	private Integer dsBHKH;
	private Integer snBHDQ;
	private Integer tienDoChuan;
	// vuongmq; 18/08/2015; Lay them san luong
	private Integer dayQuantityPlan;
	private Integer dayQuantity;
	private Integer dayQuantityApproved;
	private Integer monthQuantityPlan;
	private Integer monthQuantity;
	private Integer monthQuantityApproved;
	
	public BigDecimal getDayAmountPlan() {
		return dayAmountPlan;
	}
	public void setDayAmountPlan(BigDecimal dayAmountPlan) {
		this.dayAmountPlan = dayAmountPlan;
	}
	public BigDecimal getDayAmount() {
		return dayAmount;
	}
	public void setDayAmount(BigDecimal dayAmount) {
		this.dayAmount = dayAmount;
	}
	public BigDecimal getMonthAmountPlan() {
		return monthAmountPlan;
	}
	public void setMonthAmountPlan(BigDecimal monthAmountPlan) {
		this.monthAmountPlan = monthAmountPlan;
	}
	public BigDecimal getMonthAmount() {
		return monthAmount;
	}
	public void setMonthAmount(BigDecimal monthAmount) {
		this.monthAmount = monthAmount;
	}
	public Integer getDsBHKH() {
		return dsBHKH;
	}
	public void setDsBHKH(Integer dsBHKH) {
		this.dsBHKH = dsBHKH;
	}
	public Integer getSnBHDQ() {
		return snBHDQ;
	}
	public void setSnBHDQ(Integer snBHDQ) {
		this.snBHDQ = snBHDQ;
	}
	public Integer getTienDoChuan() {
		return tienDoChuan;
	}
	public void setTienDoChuan(Integer tienDoChuan) {
		this.tienDoChuan = tienDoChuan;
	}
	public BigDecimal getDayAmountApproved() {
		return dayAmountApproved;
	}
	public void setDayAmountApproved(BigDecimal dayAmountApproved) {
		this.dayAmountApproved = dayAmountApproved;
	}
	public BigDecimal getMonthAmountApproved() {
		return monthAmountApproved;
	}
	public void setMonthAmountApproved(BigDecimal monthAmountApproved) {
		this.monthAmountApproved = monthAmountApproved;
	}
	public Integer getDayQuantityPlan() {
		return dayQuantityPlan;
	}
	public void setDayQuantityPlan(Integer dayQuantityPlan) {
		this.dayQuantityPlan = dayQuantityPlan;
	}
	public Integer getDayQuantity() {
		return dayQuantity;
	}
	public void setDayQuantity(Integer dayQuantity) {
		this.dayQuantity = dayQuantity;
	}
	public Integer getDayQuantityApproved() {
		return dayQuantityApproved;
	}
	public void setDayQuantityApproved(Integer dayQuantityApproved) {
		this.dayQuantityApproved = dayQuantityApproved;
	}
	public Integer getMonthQuantityPlan() {
		return monthQuantityPlan;
	}
	public void setMonthQuantityPlan(Integer monthQuantityPlan) {
		this.monthQuantityPlan = monthQuantityPlan;
	}
	public Integer getMonthQuantity() {
		return monthQuantity;
	}
	public void setMonthQuantity(Integer monthQuantity) {
		this.monthQuantity = monthQuantity;
	}
	public Integer getMonthQuantityApproved() {
		return monthQuantityApproved;
	}
	public void setMonthQuantityApproved(Integer monthQuantityApproved) {
		this.monthQuantityApproved = monthQuantityApproved;
	}
	
}

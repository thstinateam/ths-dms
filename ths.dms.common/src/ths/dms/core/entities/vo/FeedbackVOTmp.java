/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

/**
 * FeedbackVOTmp
 * @author vuongmq
 * @since 23/11/2015 
 */
public class FeedbackVOTmp implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private Long id; 
	private List<Long> lstCustomerId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<Long> getLstCustomerId() {
		return lstCustomerId;
	}
	public void setLstCustomerId(List<Long> lstCustomerId) {
		this.lstCustomerId = lstCustomerId;
	}

}

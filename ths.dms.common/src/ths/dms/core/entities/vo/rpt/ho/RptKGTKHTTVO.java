/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;

/**
 * Bao cao khong ghe tham KH trong tuyen
 * @author vuongmq
 * @since 06/11/2015 
 */
public class RptKGTKHTTVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	
	private String maMien;
	private String maVung;
	private String maNPP;
	private String tenNPP;
	private String maGSBH;
	private String tenGSBH;
	private String maNVBH;
	private String tenNVBH;
	private String ngay;
	private String maTuyen;
	private String tenTuyen;
	private String ngayTaoTuyen;
	private String maKH;
	private String tenKH;
	private String diaChi;
	
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaGSBH() {
		return maGSBH;
	}
	public void setMaGSBH(String maGSBH) {
		this.maGSBH = maGSBH;
	}
	public String getTenGSBH() {
		return tenGSBH;
	}
	public void setTenGSBH(String tenGSBH) {
		this.tenGSBH = tenGSBH;
	}
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getNgay() {
		return ngay;
	}
	public void setNgay(String ngay) {
		this.ngay = ngay;
	}
	public String getMaTuyen() {
		return maTuyen;
	}
	public void setMaTuyen(String maTuyen) {
		this.maTuyen = maTuyen;
	}
	public String getTenTuyen() {
		return tenTuyen;
	}
	public void setTenTuyen(String tenTuyen) {
		this.tenTuyen = tenTuyen;
	}
	public String getNgayTaoTuyen() {
		return ngayTaoTuyen;
	}
	public void setNgayTaoTuyen(String ngayTaoTuyen) {
		this.ngayTaoTuyen = ngayTaoTuyen;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	
}

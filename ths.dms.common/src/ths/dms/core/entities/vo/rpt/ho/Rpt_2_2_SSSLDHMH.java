/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.math.BigDecimal;

import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;

/**
 * So sanh so luong dat hang mua hang trong don hang
 * Mo ta class Rpt_2_3_CTMH.java
 * @author vuongmq
 * @since Dec 29, 2015
 */
public class Rpt_2_2_SSSLDHMH implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	
	private Long poVnmId; // nhieu san pham trung nhau; lay len het
	private String maMien;
	private String maVung;
	private String maNPP;
	private String tenNPP;
	private String orderNumber;
	private String asnNumber;
	private String invoiceNumber;
	private String productCode;
	private String productName;
	private String typeStr; // view
	private String statusStr; // view
	private Integer type;
	private Integer status;
	private Integer packageQuantity;
	private Integer packageQuantityReceived;
	private BigDecimal percentSuccess;
	
	public Long getPoVnmId() {
		return poVnmId;
	}
	public void setPoVnmId(Long poVnmId) {
		this.poVnmId = poVnmId;
	}
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getAsnNumber() {
		return asnNumber;
	}
	public void setAsnNumber(String asnNumber) {
		this.asnNumber = asnNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getTypeStr() {
		if (PoType.PO_CONFIRM.getValue().equals(type)) {
			typeStr = "Nhập hàng";
		} else if (PoType.RETURNED_SALES_ORDER.getValue().equals(type)) {
			typeStr = "Trả hàng";
		}
		return typeStr;
	}
	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}
	public String getStatusStr() {
		if (PoType.PO_CONFIRM.getValue().equals(type)) {
			if (PoVNMStatus.NOT_IMPORT.getValue().equals(status)) {
				statusStr = "Chưa nhập hàng";
			} else if (PoVNMStatus.IMPORTING.getValue().equals(status)) {
				statusStr = "Đã nhập một phần";
			} else if (PoVNMStatus.IMPORTED.getValue().equals(status)) {
				statusStr = "Đã nhập";
			} else if (PoVNMStatus.PENDING.getValue().equals(status)) {
				statusStr = "Có thể treo";
			} else if (PoVNMStatus.SUSPEND.getValue().equals(status)) {
				statusStr = "Đã treo";
			}
		} else if (PoType.RETURNED_SALES_ORDER.getValue().equals(type)) {
			if (PoVNMStatus.NOT_IMPORT.getValue().equals(status)) {
				statusStr = "Chưa xuất hàng";
			} else if (PoVNMStatus.IMPORTING.getValue().equals(status)) {
				statusStr = "Đã xuất một phần";
			} else if (PoVNMStatus.IMPORTED.getValue().equals(status)) {
				statusStr = "Đã xuất";
			} else if (PoVNMStatus.PENDING.getValue().equals(status)) {
				statusStr = "Có thể treo";
			} else if (PoVNMStatus.SUSPEND.getValue().equals(status)) {
				statusStr = "Đã treo";
			}
		}
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getPackageQuantity() {
		return packageQuantity;
	}
	public void setPackageQuantity(Integer packageQuantity) {
		this.packageQuantity = packageQuantity;
	}
	public Integer getPackageQuantityReceived() {
		return packageQuantityReceived;
	}
	public void setPackageQuantityReceived(Integer packageQuantityReceived) {
		this.packageQuantityReceived = packageQuantityReceived;
	}
	public BigDecimal getPercentSuccess() {
		return percentSuccess;
	}
	public void setPercentSuccess(BigDecimal percentSuccess) {
		this.percentSuccess = percentSuccess;
	}
}

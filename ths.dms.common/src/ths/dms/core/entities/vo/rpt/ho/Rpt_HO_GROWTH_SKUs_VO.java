/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.Cycle;

/**
 * Bao cao ASO
 * 
 * @author hunglm16
 * @since October 09,2015
 */
public class Rpt_HO_GROWTH_SKUs_VO implements Serializable {
	
	private static final long serialVersionUID = -6773523398369750352L;
	
	private List<Cycle> headerAutomatic;
	private List<Rpt_HO_GROWTH_SKUs_VO_Data> lstData;
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	public List<Cycle> getHeaderAutomatic() {
		return headerAutomatic;
	}
	public void setHeaderAutomatic(List<Cycle> headerAutomatic) {
		this.headerAutomatic = headerAutomatic;
	}
	public List<Rpt_HO_GROWTH_SKUs_VO_Data> getLstData() {
		return lstData;
	}
	public void setLstData(List<Rpt_HO_GROWTH_SKUs_VO_Data> lstData) {
		this.lstData = lstData;
	}
}
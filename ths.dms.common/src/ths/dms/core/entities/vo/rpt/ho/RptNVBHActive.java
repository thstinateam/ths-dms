/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;

/**
 * Danh cho xuat bao cao NVBH hoat dong
 * @author vuongmq
 * @since 06/10/2015
 */
public class RptNVBHActive implements Serializable {

	private static final long serialVersionUID = -9067802171857675980L;
	private Long staffId;
	private String staffCode;
	private String staffName;
	private String attendanceStatus;
	private String activeStatus;
	private String staffType;
	private Long shopId;
	private String shopCode;
	private String shopName;
	private String timeLoginEarly;
	private String timeLogin;
	private String timeLoginLate;
	private String timeLocationEarly;
	private String timeLocation;
	private String timeLocationLate;
	
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getAttendanceStatus() {
		return attendanceStatus;
	}
	public void setAttendanceStatus(String attendanceStatus) {
		this.attendanceStatus = attendanceStatus;
	}
	public String getActiveStatus() {
		return activeStatus;
	}
	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}
	public String getStaffType() {
		return staffType;
	}
	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getTimeLoginEarly() {
		return timeLoginEarly;
	}
	public void setTimeLoginEarly(String timeLoginEarly) {
		this.timeLoginEarly = timeLoginEarly;
	}
	public String getTimeLogin() {
		return timeLogin;
	}
	public void setTimeLogin(String timeLogin) {
		this.timeLogin = timeLogin;
	}
	public String getTimeLoginLate() {
		return timeLoginLate;
	}
	public void setTimeLoginLate(String timeLoginLate) {
		this.timeLoginLate = timeLoginLate;
	}
	public String getTimeLocationEarly() {
		return timeLocationEarly;
	}
	public void setTimeLocationEarly(String timeLocationEarly) {
		this.timeLocationEarly = timeLocationEarly;
	}
	public String getTimeLocation() {
		return timeLocation;
	}
	public void setTimeLocation(String timeLocation) {
		this.timeLocation = timeLocation;
	}
	public String getTimeLocationLate() {
		return timeLocationLate;
	}
	public void setTimeLocationLate(String timeLocationLate) {
		this.timeLocationLate = timeLocationLate;
	}
	
}
/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * ASO - SKU - Column dong
 * 
 * @author hunglm16
 * @since 23/10/2015
 */
public class Rpt_HO_ASO_VO_Header_Value implements Serializable {

	private static final long serialVersionUID = 5599434830759872081L;
	
	private List<Long> lstHeaderSKU;
	private Map<Long, String> mapHeaderSkuName;
	private Map<Long, String> mapHeaderSkuCode;
	private Map<Long, Integer> mapHeaderSkuType;
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	public List<Long> getLstHeaderSKU() {
		return lstHeaderSKU;
	}
	public void setLstHeaderSKU(List<Long> lstHeaderSKU) {
		this.lstHeaderSKU = lstHeaderSKU;
	}
	public Map<Long, String> getMapHeaderSkuName() {
		return mapHeaderSkuName;
	}
	public void setMapHeaderSkuName(Map<Long, String> mapHeaderSkuName) {
		this.mapHeaderSkuName = mapHeaderSkuName;
	}
	public Map<Long, String> getMapHeaderSkuCode() {
		return mapHeaderSkuCode;
	}
	public void setMapHeaderSkuCode(Map<Long, String> mapHeaderSkuCode) {
		this.mapHeaderSkuCode = mapHeaderSkuCode;
	}
	public Map<Long, Integer> getMapHeaderSkuType() {
		return mapHeaderSkuType;
	}
	public void setMapHeaderSkuType(Map<Long, Integer> mapHeaderSkuType) {
		this.mapHeaderSkuType = mapHeaderSkuType;
	}
}
/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Bao cao chi tiet khuyen mai
 * 
 * @author duongdt3
 * @since 06/11/2015
 */
public class RptCTKMVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String maMien;
	private String maVung;
	private String maNpp;
	private String tenNpp;
	private String staffCode;
	private String staffName;
	private String customerCode;
	private String customerName;
	private String address;
	private String promotionProgramCodes;
	private String promotionProgramNames;
	private String promotionProgramFromDates;
	private String promotionProgramToDates;
	private String orderNumber;
	private String orderDate;
	private String productCode;
	private String productName;
	private Long quantity;
	private String promotionProductCode;
	private String promotionProductName;
	private Long promotionQuantity;
	private BigDecimal discountAmount;
	private String manualPromotion;

	public String getMaMien() {
		return maMien;
	}

	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}

	public String getMaVung() {
		return maVung;
	}

	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}

	public String getMaNpp() {
		return maNpp;
	}

	public void setMaNpp(String maNpp) {
		this.maNpp = maNpp;
	}

	public String getTenNpp() {
		return tenNpp;
	}
	
	public void setTenNpp(String tenNpp) {
		this.tenNpp = tenNpp;
	}
	
	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPromotionProgramCodes() {
		return promotionProgramCodes;
	}

	public void setPromotionProgramCodes(String promotionProgramCodes) {
		this.promotionProgramCodes = promotionProgramCodes;
	}

	public String getPromotionProgramNames() {
		return promotionProgramNames;
	}

	public void setPromotionProgramNames(String promotionProgramNames) {
		this.promotionProgramNames = promotionProgramNames;
	}

	public String getPromotionProgramFromDates() {
		return promotionProgramFromDates;
	}

	public void setPromotionProgramFromDates(String promotionProgramFromDates) {
		this.promotionProgramFromDates = promotionProgramFromDates;
	}

	public String getPromotionProgramToDates() {
		return promotionProgramToDates;
	}

	public void setPromotionProgramToDates(String promotionProgramToDates) {
		this.promotionProgramToDates = promotionProgramToDates;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public String getPromotionProductCode() {
		return promotionProductCode;
	}

	public void setPromotionProductCode(String promotionProductCode) {
		this.promotionProductCode = promotionProductCode;
	}

	public String getPromotionProductName() {
		return promotionProductName;
	}

	public void setPromotionProductName(String promotionProductName) {
		this.promotionProductName = promotionProductName;
	}

	public Long getPromotionQuantity() {
		return promotionQuantity;
	}

	public void setPromotionQuantity(Long promotionQuantity) {
		this.promotionQuantity = promotionQuantity;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getManualPromotion() {
		return manualPromotion;
	}

	public void setManualPromotion(String manualPromotion) {
		this.manualPromotion = manualPromotion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}

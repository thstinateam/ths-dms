/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Bao cao thoi gian ghe tham KH
 * @author vuongmq
 * @since 05/11/2015 
 */
public class RptTGGTKHVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	
	private String maMien;
	private String maVung;
	private String maNPP;
	private String tenNPP;
	private String maGSBH;
	private String tenGSBH;
	private String maNVBH;
	private String tenNVBH;
	private String days;
	private String dayOfWeek;
	private Integer numCusIn;
	private Integer numCus;
	private Integer visitToal;
	private Integer inRouteNotVisit;
	private Integer vsinRoute;
	private Integer vsoutRoute;
	private Integer orderinRoute;
	private Integer orderoutRoute;
	private BigDecimal visitTime;
	private String visitTimeStr;
	private String startTime;
	private String endTime;
	private Integer numVisit5min;
	private Integer numVisit10min;
	private Integer numVisit15min;
	private Integer numVisit30min;
	private BigDecimal avgVisitTime;
	private String avgVisitTimeStr;
	
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaGSBH() {
		return maGSBH;
	}
	public void setMaGSBH(String maGSBH) {
		this.maGSBH = maGSBH;
	}
	public String getTenGSBH() {
		return tenGSBH;
	}
	public void setTenGSBH(String tenGSBH) {
		this.tenGSBH = tenGSBH;
	}
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public Integer getNumCus() {
		return numCus;
	}
	public void setNumCus(Integer numCus) {
		this.numCus = numCus;
	}
	public Integer getVisitToal() {
		return visitToal;
	}
	public void setVisitToal(Integer visitToal) {
		this.visitToal = visitToal;
	}
	public Integer getInRouteNotVisit() {
		return inRouteNotVisit;
	}
	public void setInRouteNotVisit(Integer inRouteNotVisit) {
		this.inRouteNotVisit = inRouteNotVisit;
	}
	public Integer getVsinRoute() {
		return vsinRoute;
	}
	public void setVsinRoute(Integer vsinRoute) {
		this.vsinRoute = vsinRoute;
	}
	public Integer getVsoutRoute() {
		return vsoutRoute;
	}
	public void setVsoutRoute(Integer vsoutRoute) {
		this.vsoutRoute = vsoutRoute;
	}
	public Integer getOrderinRoute() {
		return orderinRoute;
	}
	public void setOrderinRoute(Integer orderinRoute) {
		this.orderinRoute = orderinRoute;
	}
	public Integer getOrderoutRoute() {
		return orderoutRoute;
	}
	public void setOrderoutRoute(Integer orderoutRoute) {
		this.orderoutRoute = orderoutRoute;
	}
	public BigDecimal getVisitTime() {
		return visitTime;
	}
	public void setVisitTime(BigDecimal visitTime) {
		this.visitTime = visitTime;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Integer getNumVisit5min() {
		return numVisit5min;
	}
	public void setNumVisit5min(Integer numVisit5min) {
		this.numVisit5min = numVisit5min;
	}
	public Integer getNumVisit10min() {
		return numVisit10min;
	}
	public void setNumVisit10min(Integer numVisit10min) {
		this.numVisit10min = numVisit10min;
	}
	public Integer getNumVisit15min() {
		return numVisit15min;
	}
	public void setNumVisit15min(Integer numVisit15min) {
		this.numVisit15min = numVisit15min;
	}
	public Integer getNumVisit30min() {
		return numVisit30min;
	}
	public void setNumVisit30min(Integer numVisit30min) {
		this.numVisit30min = numVisit30min;
	}
	public BigDecimal getAvgVisitTime() {
		return avgVisitTime;
	}
	public void setAvgVisitTime(BigDecimal avgVisitTime) {
		this.avgVisitTime = avgVisitTime;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getDayOfWeek() {
		return dayOfWeek;
	}
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}
	public Integer getNumCusIn() {
		return numCusIn;
	}
	public void setNumCusIn(Integer numCusIn) {
		this.numCusIn = numCusIn;
	}
	public String getVisitTimeStr() {
		return visitTimeStr;
	}
	public void setVisitTimeStr(String visitTimeStr) {
		this.visitTimeStr = visitTimeStr;
	}
	public String getAvgVisitTimeStr() {
		return avgVisitTimeStr;
	}
	public void setAvgVisitTimeStr(String avgVisitTimeStr) {
		this.avgVisitTimeStr = avgVisitTimeStr;
	}
	
}

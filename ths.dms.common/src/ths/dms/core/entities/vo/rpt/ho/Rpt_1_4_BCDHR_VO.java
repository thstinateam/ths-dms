/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

/**
 * [1.4] Bao cao don hang Rot
 * 
 * @author hunglm16
 * @since 30/10/2015
 */
public class Rpt_1_4_BCDHR_VO implements Serializable {

	private static final long serialVersionUID = -1788091503076481349L;
	
	private String maVung;
	private String maMien;
	private String maNPP;
	private String tenNPP;
	private String maNVGH;
	private String tenNVGH;
	private String maNVBH;
	private String tenNVBH;
	private String maKH;
	private String tenKH;
	private String diaChiKH;
	private String soDH;
	private String trangThaiDH;
	private String ngayDH;
	private String ngayGH;
	private String lyDo;

	private BigDecimal giaTriDH;
	private BigDecimal chietKhau;
	private BigDecimal soTienPSTN;
	/**
	 * Xu ly cac gia tri null
	 * 
	 * @author hunglm16
	 * @return
	 * @since 22/10/2015
	 */
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
		}
	}
	
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * @author hunglm16
	 * @since 30/10/2015
	 */
	
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaNVGH() {
		return maNVGH;
	}
	public void setMaNVGH(String maNVGH) {
		this.maNVGH = maNVGH;
	}
	public String getTenNVGH() {
		return tenNVGH;
	}
	public void setTenNVGH(String tenNVGH) {
		this.tenNVGH = tenNVGH;
	}
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getDiaChiKH() {
		return diaChiKH;
	}
	public void setDiaChiKH(String diaChiKH) {
		this.diaChiKH = diaChiKH;
	}
	public String getSoDH() {
		return soDH;
	}
	public void setSoDH(String soDH) {
		this.soDH = soDH;
	}
	public String getNgayDH() {
		return ngayDH;
	}
	public void setNgayDH(String ngayDH) {
		this.ngayDH = ngayDH;
	}
	public String getNgayGH() {
		return ngayGH;
	}
	public void setNgayGH(String ngayGH) {
		this.ngayGH = ngayGH;
	}
	public BigDecimal getGiaTriDH() {
		return giaTriDH;
	}
	public void setGiaTriDH(BigDecimal giaTriDH) {
		this.giaTriDH = giaTriDH;
	}
	public BigDecimal getChietKhau() {
		return chietKhau;
	}
	public void setChietKhau(BigDecimal chietKhau) {
		this.chietKhau = chietKhau;
	}
	public BigDecimal getSoTienPSTN() {
		return soTienPSTN;
	}
	public void setSoTienPSTN(BigDecimal soTienPSTN) {
		this.soTienPSTN = soTienPSTN;
	}

	public String getTrangThaiDH() {
		return trangThaiDH;
	}

	public void setTrangThaiDH(String trangThaiDH) {
		this.trangThaiDH = trangThaiDH;
	}

	public String getLyDo() {
		return lyDo;
	}

	public void setLyDo(String lyDo) {
		this.lyDo = lyDo;
	}
	
}
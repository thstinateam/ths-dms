/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ths.core.entities.vo.rpt.DynamicVO;

/**
 * Bao cao tang truong SKUs - Data
 * 
 * @author hunglm16
 * @since 06/11/2015
 */
public class Rpt_HO_GROWTH_SKUs_VO_Data implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String maMien;
	private String tenMien;
	private String maVung;
	private String tenVung;
	private String maSanPham;
	private String tenSanPham;
	private Integer tinhTrangSP;
	private BigDecimal tongNamHt;
	private BigDecimal tongNamQk;
	private BigDecimal phanTramTong;
	private List<DynamicVO> lstValue = new ArrayList<DynamicVO>();
	
	/**
	 * Xy ly null cho cac gia tri mac dinh
	 * @author hunglm16
	 * @since October 12,2015
	 */
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */

	public String getMaMien() {
		return maMien;
	}

	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}

	public String getTenMien() {
		return tenMien;
	}

	public void setTenMien(String tenMien) {
		this.tenMien = tenMien;
	}

	public String getMaVung() {
		return maVung;
	}

	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}

	public String getTenVung() {
		return tenVung;
	}

	public void setTenVung(String tenVung) {
		this.tenVung = tenVung;
	}

	public String getMaSanPham() {
		return maSanPham;
	}

	public void setMaSanPham(String maSanPham) {
		this.maSanPham = maSanPham;
	}

	public String getTenSanPham() {
		return tenSanPham;
	}

	public void setTenSanPham(String tenSanPham) {
		this.tenSanPham = tenSanPham;
	}

	public List<DynamicVO> getLstValue() {
		return lstValue;
	}

	public void setLstValue(List<DynamicVO> lstValue) {
		this.lstValue = lstValue;
	}
	public BigDecimal getTongNamHt() {
		return tongNamHt;
	}
	public void setTongNamHt(BigDecimal tongNamHt) {
		this.tongNamHt = tongNamHt;
	}
	public BigDecimal getTongNamQk() {
		return tongNamQk;
	}
	public void setTongNamQk(BigDecimal tongNamQk) {
		this.tongNamQk = tongNamQk;
	}
	public Integer getTinhTrangSP() {
		return tinhTrangSP;
	}
	public void setTinhTrangSP(Integer tinhTrangSP) {
		this.tinhTrangSP = tinhTrangSP;
	}
	public BigDecimal getPhanTramTong() {
		return phanTramTong;
	}
	public void setPhanTramTong(BigDecimal phanTramTong) {
		this.phanTramTong = phanTramTong;
	}
	
}
/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * [1.4] Bao cao don hang Rot
 * 
 * @author hunglm16
 * @since 30/10/2015
 */
public class Rpt_1_4_G_BCDHCR_VO implements Serializable {

	private static final long serialVersionUID = -1788091503076481349L;
	
	private BigDecimal tongGiaTriDH;
	private BigDecimal tongChietKhau;
	private BigDecimal tongSoTienPSTN;
	
	private List<Rpt_1_4_BCDHR_VO> lstData = new ArrayList<Rpt_1_4_BCDHR_VO>();
	
	/**
	 * Xu ly cac gia tri null
	 * 
	 * @author hunglm16
	 * @return
	 * @since 22/10/2015
	 */
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
		}
	}
	
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * @author hunglm16
	 * @since 30/10/2015
	 */
	public BigDecimal getTongGiaTriDH() {
		return tongGiaTriDH;
	}

	public void setTongGiaTriDH(BigDecimal tongGiaTriDH) {
		this.tongGiaTriDH = tongGiaTriDH;
	}

	public BigDecimal getTongChietKhau() {
		return tongChietKhau;
	}

	public void setTongChietKhau(BigDecimal tongChietKhau) {
		this.tongChietKhau = tongChietKhau;
	}

	public BigDecimal getTongSoTienPSTN() {
		return tongSoTienPSTN;
	}

	public void setTongSoTienPSTN(BigDecimal tongSoTienPSTN) {
		this.tongSoTienPSTN = tongSoTienPSTN;
	}

	public List<Rpt_1_4_BCDHR_VO> getLstData() {
		return lstData;
	}

	public void setLstData(List<Rpt_1_4_BCDHR_VO> lstData) {
		this.lstData = lstData;
	}
	
}
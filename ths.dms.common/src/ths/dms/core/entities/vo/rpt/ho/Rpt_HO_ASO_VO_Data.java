/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ths.core.entities.vo.rpt.DynamicVO;

/**
 * VO ASO - Data
 * 
 * @author hunglm16
 * @since October 09,2015
 */
public class Rpt_HO_ASO_VO_Data implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String maVung;
	private String maMien;
	private String maNPP;
	private String tenNPP;
	private String maNV;
	private String tenNV;
	private String maTuyen;
	private String tenTuyen;
	private BigDecimal tongNV;
	private List<DynamicVO> lstValue = new ArrayList<DynamicVO>();
	
	/**
	 * Xy ly null cho cac gia tri mac dinh
	 * @author hunglm16
	 * @since October 12,2015
	 */
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaNV() {
		return maNV;
	}
	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}
	public String getTenNV() {
		return tenNV;
	}
	public void setTenNV(String tenNV) {
		this.tenNV = tenNV;
	}
	public String getMaTuyen() {
		return maTuyen;
	}
	public void setMaTuyen(String maTuyen) {
		this.maTuyen = maTuyen;
	}
	public String getTenTuyen() {
		return tenTuyen;
	}
	public void setTenTuyen(String tenTuyen) {
		this.tenTuyen = tenTuyen;
	}
	public BigDecimal getTongNV() {
		return tongNV;
	}
	public void setTongNV(BigDecimal tongNV) {
		this.tongNV = tongNV;
	}
	public List<DynamicVO> getLstValue() {
		return lstValue;
	}
	public void setLstValue(List<DynamicVO> lstValue) {
		this.lstValue = lstValue;
	}
	
}
/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * ASO - Column dong
 * 
 * @author hunglm16
 * @since October 09,2015
 */
public class Rpt_HO_ASO_VO_Header implements Serializable {

	private static final long serialVersionUID = -6471099973351944893L;
	
	private List<Long> lstHeaderAsoId;
	private Map<Long, String> mapHeaderAsoName;
	private Map<Long, String> mapHeaderAsoCode;
	private Map<Long, Rpt_HO_ASO_VO_Header_Value> mapHeader;
	
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	public List<Long> getLstHeaderAsoId() {
		return lstHeaderAsoId;
	}
	public void setLstHeaderAsoId(List<Long> lstHeaderAsoId) {
		this.lstHeaderAsoId = lstHeaderAsoId;
	}
	public Map<Long, String> getMapHeaderAsoName() {
		return mapHeaderAsoName;
	}
	public void setMapHeaderAsoName(Map<Long, String> mapHeaderAsoName) {
		this.mapHeaderAsoName = mapHeaderAsoName;
	}
	public Map<Long, String> getMapHeaderAsoCode() {
		return mapHeaderAsoCode;
	}
	public void setMapHeaderAsoCode(Map<Long, String> mapHeaderAsoCode) {
		this.mapHeaderAsoCode = mapHeaderAsoCode;
	}
	public Map<Long, Rpt_HO_ASO_VO_Header_Value> getMapHeader() {
		return mapHeader;
	}
	public void setMapHeader(Map<Long, Rpt_HO_ASO_VO_Header_Value> mapHeader) {
		this.mapHeader = mapHeader;
	}

}
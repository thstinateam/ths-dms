/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Danh cho xuat bao cao Don hang cho KH
 * @author vuongmq
 * @since 06/10/2015
 */
public class RptDHChoKHVO implements Serializable {

	private static final long serialVersionUID = -9067802171857675980L;
	
	private String maMien;
	private String maVung;
	private String maKhuVuc;
	private String maNPP;
	private String tenNPP;
	private String diaChi;
	private String phoneNumber;
	
	private String soDHTC;
	private BigDecimal tienDHTC;
	private String soDHDuyet;
	private BigDecimal tienDHDuyet;
	private String ngayLamViec;
	
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaKhuVuc() {
		return maKhuVuc;
	}
	public void setMaKhuVuc(String maKhuVuc) {
		this.maKhuVuc = maKhuVuc;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getSoDHTC() {
		return soDHTC;
	}
	public void setSoDHTC(String soDHTC) {
		this.soDHTC = soDHTC;
	}
	public BigDecimal getTienDHTC() {
		return tienDHTC;
	}
	public void setTienDHTC(BigDecimal tienDHTC) {
		this.tienDHTC = tienDHTC;
	}
	public String getSoDHDuyet() {
		return soDHDuyet;
	}
	public void setSoDHDuyet(String soDHDuyet) {
		this.soDHDuyet = soDHDuyet;
	}
	public BigDecimal getTienDHDuyet() {
		return tienDHDuyet;
	}
	public void setTienDHDuyet(BigDecimal tienDHDuyet) {
		this.tienDHDuyet = tienDHDuyet;
	}
	public String getNgayLamViec() {
		return ngayLamViec;
	}
	public void setNgayLamViec(String ngayLamViec) {
		this.ngayLamViec = ngayLamViec;
	}
}
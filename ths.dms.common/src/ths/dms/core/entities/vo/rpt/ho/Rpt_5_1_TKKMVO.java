/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo.rpt.ho;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

/**
 * Danh cho xuat bao cao Tong ket khuyen mai
 * 
 * @author hunglm16
 * @since 18/09/2015
 */
public class Rpt_5_1_TKKMVO implements Serializable {

	private static final long serialVersionUID = -9067802171857675980L;
	
	private String maMien;
	private String maVung;
	private String maNPP;
	private String tenNPP;
	private String maCTKM;
	private String tenCTK;
	private String loaiCTKM;
	private String thoiGian;
	private String maSP;
	private String tenSP;
	private String slThungLe;

	private BigDecimal soTienKM;
	private BigDecimal slLe;
	/**
	 * Danh cho xuat bao cao Tong ket khuyen mai
	 * 
	 * @author hunglm16
	 * @return
	 * @since 18/09/2015
	 */
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
		}
	}
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * @return
	 */
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaCTKM() {
		return maCTKM;
	}
	public void setMaCTKM(String maCTKM) {
		this.maCTKM = maCTKM;
	}
	public String getTenCTK() {
		return tenCTK;
	}
	public void setTenCTK(String tenCTK) {
		this.tenCTK = tenCTK;
	}
	public String getLoaiCTKM() {
		return loaiCTKM;
	}
	public void setLoaiCTKM(String loaiCTKM) {
		this.loaiCTKM = loaiCTKM;
	}
	public String getThoiGian() {
		return thoiGian;
	}
	public void setThoiGian(String thoiGian) {
		this.thoiGian = thoiGian;
	}
	public String getMaSP() {
		return maSP;
	}
	public void setMaSP(String maSP) {
		this.maSP = maSP;
	}
	public String getTenSP() {
		return tenSP;
	}
	public void setTenSP(String tenSP) {
		this.tenSP = tenSP;
	}
	public String getSlThungLe() {
		return slThungLe;
	}
	public void setSlThungLe(String slThungLe) {
		this.slThungLe = slThungLe;
	}
	public BigDecimal getSoTienKM() {
		return soTienKM;
	}
	public void setSoTienKM(BigDecimal soTienKM) {
		this.soTienKM = soTienKM;
	}
	public BigDecimal getSlLe() {
		return slLe;
	}
	public void setSlLe(BigDecimal slLe) {
		this.slLe = slLe;
	}
	
}
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Class EquipItemVO
 * 
 * @author liemtpt
 * @since 07/04/2015
 * 
 * @author hunglm16
 * @since May 05,2015
 * @description Ra soat va bo sung tham so
 */
public class EquipItemConfigVO implements Serializable {

	private static final long serialVersionUID = 5304990920987154955L;

	// Id thiet lap hang muc
	private Long id;
	private Long equipItemConfigId;

	// Ma thiet lap hang muc
	private String equipItemConfigCode;
	// Dung tich(lit)
	private String capacity;
	// Trang thai
	private Integer status;
	// Ma  hang muc
	private String equipItemCode;
	// Ten  hang muc
	private String equipItemName;
	private String eqItemConfStatusStr;

	// Duung tich tu
	private Integer fromCapacity;
	// Dung tich den
	private Integer toCapacity;
	//Don gia vat tu tu
	private BigDecimal fromMaterialPrice;
	//Don gia vat tu den
	private BigDecimal toMaterialPrice;
	// Don gia cong nhan tu
	private BigDecimal fromWorkerPrice;
	// Don gia cong nhan den
	private BigDecimal toWorkerPrice;

	private Integer flagCheckAll;
	private List<EquipItemConfigDtlVO> lstEquipItemConfigDtlVO;

	/**
	 * Khai bao phuong thuc GETTER/SETER
	 * */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEquipItemConfigCode() {
		return equipItemConfigCode;
	}

	public void setEquipItemConfigCode(String equipItemConfigCode) {
		this.equipItemConfigCode = equipItemConfigCode;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getEquipItemCode() {
		return equipItemCode;
	}

	public void setEquipItemCode(String equipItemCode) {
		this.equipItemCode = equipItemCode;
	}

	public String getEquipItemName() {
		return equipItemName;
	}

	public void setEquipItemName(String equipItemName) {
		this.equipItemName = equipItemName;
	}

	public BigDecimal getFromWorkerPrice() {
		return fromWorkerPrice;
	}

	public void setFromWorkerPrice(BigDecimal fromWorkerPrice) {
		this.fromWorkerPrice = fromWorkerPrice;
	}

	public BigDecimal getToWorkerPrice() {
		return toWorkerPrice;
	}

	public void setToWorkerPrice(BigDecimal toWorkerPrice) {
		this.toWorkerPrice = toWorkerPrice;
	}

	public BigDecimal getFromMaterialPrice() {
		return fromMaterialPrice;
	}

	public void setFromMaterialPrice(BigDecimal fromMaterialPrice) {
		this.fromMaterialPrice = fromMaterialPrice;
	}

	public BigDecimal getToMaterialPrice() {
		return toMaterialPrice;
	}

	public void setToMaterialPrice(BigDecimal toMaterialPrice) {
		this.toMaterialPrice = toMaterialPrice;
	}

	public List<EquipItemConfigDtlVO> getLstEquipItemConfigDtlVO() {
		return lstEquipItemConfigDtlVO;
	}

	public void setLstEquipItemConfigDtlVO(List<EquipItemConfigDtlVO> lstEquipItemConfigDtlVO) {
		this.lstEquipItemConfigDtlVO = lstEquipItemConfigDtlVO;
	}

	public Integer getFlagCheckAll() {
		return flagCheckAll;
	}

	public void setFlagCheckAll(Integer flagCheckAll) {
		this.flagCheckAll = flagCheckAll;
	}

	public Long getEquipItemConfigId() {
		return equipItemConfigId;
	}

	public void setEquipItemConfigId(Long equipItemConfigId) {
		this.equipItemConfigId = equipItemConfigId;
	}

	public Integer getFromCapacity() {
		return fromCapacity;
	}

	public void setFromCapacity(Integer fromCapacity) {
		this.fromCapacity = fromCapacity;
	}

	public Integer getToCapacity() {
		return toCapacity;
	}

	public void setToCapacity(Integer toCapacity) {
		this.toCapacity = toCapacity;
	}

	public String getEqItemConfStatusStr() {
		return eqItemConfStatusStr;
	}

	public void setEqItemConfStatusStr(String eqItemConfStatusStr) {
		this.eqItemConfStatusStr = eqItemConfStatusStr;
	}

}

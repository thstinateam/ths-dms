package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;


public class ProductLotVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private Integer productId;
	
	private String productCode;
	
	private String productName;
	
	private String lot;
	
	private Integer quantity;
	
	private BigDecimal price;
	
	private Float netWeight;
	
	private Float grossWeight;
	
	private Integer convfact;
	
	private Integer availableQuantity;
	
	private Integer checkLot;
	
	private BigDecimal totalAmount;
	
	private String bigUnit;
	
	private String smallUnit;

	public String getBigUnit() {
		return bigUnit;
	}

	public void setBigUnit(String bigUnit) {
		this.bigUnit = bigUnit;
	}

	public String getSmallUnit() {
		return smallUnit;
	}

	public void setSmallUnit(String smallUnit) {
		this.smallUnit = smallUnit;
	}

	public BigDecimal getTotalAmount() {
		Long a = quantity * price.longValue();
		totalAmount = BigDecimal.valueOf(a);
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price == null ? BigDecimal.valueOf(0) : price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Float getNetWeight() {
		return netWeight == null ? 0f : netWeight;
	}

	public void setNetWeight(Float netWeight) {
		this.netWeight = netWeight;
	}

	public Integer getConvfact() {
		if (convfact == null) {
			convfact = 0;
		}
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Float getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Float grossWeight) {
		this.grossWeight = grossWeight;
	}

	/**
	 * @return the availableQuantity
	 */
	public Integer getAvailableQuantity() {
		return availableQuantity;
	}

	/**
	 * @param availableQuantity the availableQuantity to set
	 */
	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}
	
	public ProductLotVO clone() {
		ProductLotVO lotVO = new ProductLotVO();
		lotVO.id = this.id;
		lotVO.productId = this.productId;
		lotVO.productCode = this.productCode;
		lotVO.productName = this.productName;
		lotVO.lot = this.lot;
		lotVO.quantity = this.quantity;
		lotVO.price = this.price;
		lotVO.netWeight = this.netWeight;
		lotVO.grossWeight = this.grossWeight;
		lotVO.convfact = this.convfact;
		lotVO.availableQuantity = this.availableQuantity;
		lotVO.checkLot = this.checkLot;
		return lotVO;
	}

	/**
	 * @return the checkLot
	 */
	public Integer getCheckLot() {
		return checkLot;
	}

	/**
	 * @param checkLot the checkLot to set
	 */
	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}
	
	public String getThung() {
		int bigUnit = 0;
		if(convfact <= 0){
			convfact = 1;
		}
		bigUnit = quantity/convfact;
		return String.valueOf(bigUnit);
	}
	
	public String getLe() {
		int smallUnit = 0;
		if(convfact <= 0){
			convfact = 1;
		}
		smallUnit = quantity%convfact;
		return String.valueOf(smallUnit);
	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import ths.dms.core.entities.enumtype.StatusRecordsEquip;

/**
 * Class EquipRepairPayFormVO
 * Danh sach quan ly thanh toan cua phieu sua chua
 *@author vuongmq
 *@since 22/06/2015
 *@description 
 */
public class EquipRepairPayFormVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String maPhieu;
	private String ngayTao; /** Neu xuat exel ngayTao nay la cua phieu sua chua*/
	private String ngayLap; 
	private Integer trangThai;
	private BigDecimal tongTien;
	
	/** xuat excel list phieu thanh toán*/
	private String trangThaiStr; /** trang thai String cua phieu thanh toan*/
	private String maPhieuSuaChua;
	private BigDecimal tongTienSuaChua;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMaPhieu() {
		return maPhieu;
	}
	public void setMaPhieu(String maPhieu) {
		this.maPhieu = maPhieu;
	}
	public String getNgayTao() {
		return ngayTao;
	}
	public void setNgayTao(String ngayTao) {
		this.ngayTao = ngayTao;
	}
	public String getNgayLap() {
		return ngayLap;
	}
	public void setNgayLap(String ngayLap) {
		this.ngayLap = ngayLap;
	}
	public Integer getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(Integer trangThai) {
		this.trangThai = trangThai;
	}
	public BigDecimal getTongTien() {
		return tongTien;
	}
	public void setTongTien(BigDecimal tongTien) {
		this.tongTien = tongTien;
	}
	public String getTrangThaiStr() {
		if (trangThai == StatusRecordsEquip.DRAFT.getValue()) {
			trangThaiStr = "Dự thảo";
    	} else if (trangThai == StatusRecordsEquip.WAITING_APPROVAL.getValue()) {
    		trangThaiStr = "Chờ duyệt";
    	} else if (trangThai == StatusRecordsEquip.APPROVED.getValue()) {
    		trangThaiStr = "Đã duyệt";
    	} else if (trangThai == StatusRecordsEquip.NO_APPROVAL.getValue()) {
    		trangThaiStr = "Không duyệt";
    	} else if (trangThai == StatusRecordsEquip.CANCELLATION.getValue()) {
    		trangThaiStr = "Hủy";
    	}
		return trangThaiStr;
	}
	public void setTrangThaiStr(String trangThaiStr) {
		this.trangThaiStr = trangThaiStr;
	}
	public String getMaPhieuSuaChua() {
		return maPhieuSuaChua;
	}
	public void setMaPhieuSuaChua(String maPhieuSuaChua) {
		this.maPhieuSuaChua = maPhieuSuaChua;
	}
	public BigDecimal getTongTienSuaChua() {
		return tongTienSuaChua;
	}
	public void setTongTienSuaChua(BigDecimal tongTienSuaChua) {
		this.tongTienSuaChua = tongTienSuaChua;
	}
}

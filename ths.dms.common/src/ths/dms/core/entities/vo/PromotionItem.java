package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.entities.GroupLevel;

public class PromotionItem implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String promotionProgramCode;
	private Integer ratiton;
	private Long parentGroupId;
	private Long subGroupId;
	private GroupLevel subGroupLevel;
	private List<List<GroupLevelDetailVO>> buyProducts;	// KM %
	private List<GroupLevelDetailVO> promotionProducts;	// KM SP
	private List<BigDecimal> lstAmountPercent;//list amount KM % theo product
	private BigDecimal debitAcc;
	private BigDecimal amount;
	private List<GroupLevelDetailVO> lstPromoProducts;	// KM SP (dung de luu ds doi sp khi chinh sua)
	
	public BigDecimal getDebitAcc() {
		return debitAcc;
	}
	public void setDebitAcc(BigDecimal debitAcc) {
		this.debitAcc = debitAcc;
	}
	public List<BigDecimal> getLstAmountPercent() {
		return lstAmountPercent;
	}
	public void setLstAmountPercent(List<BigDecimal> lstAmountPercent) {
		this.lstAmountPercent = lstAmountPercent;
	}
	
	public List<List<GroupLevelDetailVO>> getBuyProducts() {
		return buyProducts;
	}
	public void setBuyProducts(List<List<GroupLevelDetailVO>> buyProducts) {
		this.buyProducts = buyProducts;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getParentGroupId() {
		return parentGroupId;
	}
	public void setParentGroupId(Long parentGroupId) {
		this.parentGroupId = parentGroupId;
	}
	public Long getSubGroupId() {
		return subGroupId;
	}
	public void setSubGroupId(Long subGroupId) {
		this.subGroupId = subGroupId;
	}

	public Integer getRatiton() {
		return ratiton;
	}
	public void setRatiton(Integer ratiton) {
		this.ratiton = ratiton;
	}
	public GroupLevel getSubGroupLevel() {
		return subGroupLevel;
	}
	public void setSubGroupLevel(GroupLevel subGroupLevel) {
		this.subGroupLevel = subGroupLevel;
	}
	public List<GroupLevelDetailVO> getPromotionProducts() {
		return promotionProducts;
	}
	public void setPromotionProducts(List<GroupLevelDetailVO> promotionProducts) {
		this.promotionProducts = promotionProducts;
	}
	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}
	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public List<GroupLevelDetailVO> getLstPromoProducts() {
		return lstPromoProducts;
	}
	public void setLstPromoProducts(List<GroupLevelDetailVO> lstPromoProducts) {
		this.lstPromoProducts = lstPromoProducts;
	}
}

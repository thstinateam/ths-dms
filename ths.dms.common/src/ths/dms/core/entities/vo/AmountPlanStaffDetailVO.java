package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class AmountPlanStaffDetailVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long shopId;
	private String shopCode;
	private String shopName;
	private Long staffId;
	private String staffCode;
	private String staffName;
	private String lat;
	private String lng;
	private BigDecimal monthAmountPlan;
	private BigDecimal monthAmount;
	private BigDecimal monthApprovedAmount;//Doanh so duyet trong thang
	private Integer progress;
	private BigDecimal exist;
	
	
	
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public BigDecimal getMonthAmountPlan() {
		return monthAmountPlan;
	}				
	public void setMonthAmountPlan(BigDecimal monthAmountPlan) {
		this.monthAmountPlan = monthAmountPlan;
	}
	public BigDecimal getMonthAmount() {
		return monthAmount;
	}
	public void setMonthAmount(BigDecimal monthAmount) {
		this.monthAmount = monthAmount;
	}
	public Integer getProgress() {
		return progress;
	}
	public void setProgress(Integer progress) {
		this.progress = progress;
	}
	public BigDecimal getExist() {
		return exist;
	}
	public void setExist(BigDecimal exist) {
		this.exist = exist;
	}
	public BigDecimal getMonthApprovedAmount() {
		return monthApprovedAmount;
	}
	public void setMonthApprovedAmount(BigDecimal monthApprovedAmount) {
		this.monthApprovedAmount = monthApprovedAmount;
	}
}

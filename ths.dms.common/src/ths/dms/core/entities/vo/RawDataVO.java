package ths.dms.core.entities.vo;

import java.io.Serializable;

public class RawDataVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	
	private String chuKy;
	private String shop; //0:mien; 1:vung; 2:shopCode; 3:shopName; 4:phone; 5:address;
	private String routing; //0:routingCode; 1:routingName
	private String staff; //0:usmCode; 1:usmName; 2:staffCode; 3:staffName
	private String customer; //0:customerCode; 1:customerName; 2:phone; 3:type; 4:tansat; 5:houseNumber; 6:street
	private String area; //0:wardCode; 1:wardName; 2:districtCode; 3:districtName
	private String orderInfo; //0:orderDate; 1:InvoiceNumber; 2:TypeSKU; 3:orderType; 4:orderSource
	private String product; //0:productCode; 1:productName; 2:uom1; 3:convfact
	private String startEndTime; //0:startTime; 1:EndTime
	private String statusOrder; //0:trang thai don
	private Integer quantity;
	private Long price;
	private Long packagePrice;
	private Float discountPercent;
	private Long discountAmount;
	private Long amountDiscount;//thanh tien co giam gia
	private Long priceNotVat;//don gia truoc vat
	private Long amountNotVat;//thanh tien truoc vat
	private Long amount;//thanh tien sau vat
	private Integer printBatch;//so lan in don
	
	public String getChuKy() {
		return chuKy;
	}
	public void setChuKy(String chuKy) {
		this.chuKy = chuKy;
	}
	public String getShop() {
		return shop;
	}
	public void setShop(String shop) {
		this.shop = shop;
	}
	public String getRouting() {
		return routing;
	}
	public void setRouting(String routing) {
		this.routing = routing;
	}
	public String getStaff() {
		return staff;
	}
	public void setStaff(String staff) {
		this.staff = staff;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getOrderInfo() {
		return orderInfo;
	}
	public void setOrderInfo(String orderInfo) {
		this.orderInfo = orderInfo;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	public Long getPackagePrice() {
		return packagePrice;
	}
	public void setPackagePrice(Long packagePrice) {
		this.packagePrice = packagePrice;
	}
	public Long getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Long discountAmount) {
		this.discountAmount = discountAmount;
	}
	public String getStartEndTime() {
		return startEndTime;
	}
	public void setStartEndTime(String startEndTime) {
		this.startEndTime = startEndTime;
	}
	public Long getAmountDiscount() {
		return amountDiscount;
	}
	public void setAmountDiscount(Long amountDiscount) {
		this.amountDiscount = amountDiscount;
	}
	public Long getPriceNotVat() {
		return priceNotVat;
	}
	public void setPriceNotVat(Long priceNotVat) {
		this.priceNotVat = priceNotVat;
	}
	public Long getAmountNotVat() {
		return amountNotVat;
	}
	public void setAmountNotVat(Long amountNotVat) {
		this.amountNotVat = amountNotVat;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public Integer getPrintBatch() {
		return printBatch;
	}
	public void setPrintBatch(Integer printBatch) {
		this.printBatch = printBatch;
	}
	public String getStatusOrder() {
		return statusOrder;
	}
	public void setStatusOrder(String statusOrder) {
		this.statusOrder = statusOrder;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Float getDiscountPercent() {
		return discountPercent;
	}
	public void setDiscountPercent(Float discountPercent) {
		this.discountPercent = discountPercent;
	}
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.EquipStatisticRecord;

/**
 * VO du lieu xuat ra file excel
 * @author tamvnm
 * @since 16/01/2015
 */
public class EquipmentStatisticCheckingRecordExportVO implements Serializable{

	private static final long serialVersionUID = 3107951648345747544L;
	
	private EquipStatisticRecord equipStatisticRecord;
	private List<StatisticCheckingVO> lstStatictisCheckingDetails;
	
	
	public EquipStatisticRecord getEquipStatisticRecord() {
		return equipStatisticRecord;
	}
	public void setEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord) {
		this.equipStatisticRecord = equipStatisticRecord;
	}
	public List<StatisticCheckingVO> getLstStatictisCheckingDetails() {
		return lstStatictisCheckingDetails;
	}
	public void setLstStatictisCheckingDetails(
			List<StatisticCheckingVO> lstStatictisCheckingDetails) {
		this.lstStatictisCheckingDetails = lstStatictisCheckingDetails;
	}
	
	
}

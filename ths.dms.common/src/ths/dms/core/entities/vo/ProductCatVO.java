package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;

import ths.dms.core.entities.ProductInfo;

public class ProductCatVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ProductInfo productInfo;
	private ArrayList<ProductSubCatVO> lstProductSubCatVO = new ArrayList<ProductSubCatVO>();

	public ProductInfo getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}

	public ArrayList<ProductSubCatVO> getLstProductSubCatVO() {
		return lstProductSubCatVO;
	}

	public void setLstProductSubCatVO(
			ArrayList<ProductSubCatVO> lstProductSubCatVO) {
		this.lstProductSubCatVO = lstProductSubCatVO;
	}

}

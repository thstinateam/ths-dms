package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

public class KeyShopVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private Long id;
	private Long ksId;
	private Long ksLevelId;
	private Long fromCycleId;
	private Long toCycleId;
	private Long cycleId;
	private Long ksCustomerId;
	private Long ksCustomerLevelId;
	private Long productId;
	private Long warehouseId;
	
	private String ksCode;
	private String ksName;
	private String ksNameUpper;
	private String ksLevelCode;
	private String ksLevelName;
	private String fromCycle;
	private Integer fromCycleYear;
	private String toCycle;
	private Integer toCycleYear;
	private String description;
	private String productCode;
	private String productName;
	private String cat;
	private String subCat;
	private String brand;
	private String flavour;
	private String packing;
	
	private Integer convfact;
	
	private BigDecimal total;
	private BigDecimal totalDone;
	private BigDecimal quantityAmount;
	
	private Integer status;
	private String strStatus;
	private Integer ksType;
	private String strKSType;
	private Integer minPhoto;
	private Integer quantity;
	private Integer quantityRetail;
	private Integer quantityPackage;
	private Integer availableQuantity;
	private Integer rewardType;
	private String strRewardType;
	private String strRewardStatus;
	private String shopCode;
	private String shopName;
	private String customerCode;
	private Long customerId;
	private String customerName;
	private String address;//"provinceName", "districtName", "precinctName", "street", "housenumber",
	private String provinceName;
	private String districtName;
	private String precinctName;
	private String street;
	private String housenumber;
	private String levelCustomer;
	private String warehouseName;
	
	private BigDecimal amount;
	private Integer multiplier;
	private Integer quantityTarget;
	private BigDecimal amountTarget;
	private BigDecimal percentQuantity;
	private BigDecimal percentAmount;
	private BigDecimal totalReward;
	private BigDecimal totalRewardDone;
	private BigDecimal payReward;
	private String productReward;
	private String productRewardDone;
	private BigDecimal amountDiscount;
	private BigDecimal overAmountDiscount;
	private BigDecimal displayMoney;
	private BigDecimal supportMoney;
	private BigDecimal oddDiscount;
	private BigDecimal addMoney;
	private Integer result;
	private String strResult;
	
	private List<KeyShopVO> lstProduct;
	private List<KeyShopVO> lstLevel;
	private List<AllocateShopVO> lstShop;
	private List<Integer> lstYear;
	
	private String areaCode;
	private String zoneCode;
	
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	
	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Long getKsId() {
		return ksId;
	}

	public void setKsId(Long ksId) {
		this.ksId = ksId;
	}

	public String getKsCode() {
		return ksCode;
	}

	public void setKsCode(String ksCode) {
		this.ksCode = ksCode;
	}

	public String getKsName() {
		return ksName;
	}

	public void setKsName(String ksName) {
		this.ksName = ksName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFromCycle() {
		return fromCycle;
	}

	public void setFromCycle(String fromCycle) {
		this.fromCycle = fromCycle;
	}

	public String getToCycle() {
		return toCycle;
	}

	public void setToCycle(String toCycle) {
		this.toCycle = toCycle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getKsCustomerId() {
		return ksCustomerId;
	}

	public void setKsCustomerId(Long ksCustomerId) {
		this.ksCustomerId = ksCustomerId;
	}

	public Long getKsCustomerLevelId() {
		return ksCustomerLevelId;
	}

	public void setKsCustomerLevelId(Long ksCustomerLevelId) {
		this.ksCustomerLevelId = ksCustomerLevelId;
	}

	public Integer getKsType() {
		return ksType;
	}

	public void setKsType(Integer ksType) {
		this.ksType = ksType;
	}

	public Long getFromCycleId() {
		return fromCycleId;
	}

	public void setFromCycleId(Long fromCycleId) {
		this.fromCycleId = fromCycleId;
	}

	public Long getToCycleId() {
		return toCycleId;
	}

	public void setToCycleId(Long toCycleId) {
		this.toCycleId = toCycleId;
	}

	public Integer getMinPhoto() {
		return minPhoto;
	}

	public void setMinPhoto(Integer minPhoto) {
		this.minPhoto = minPhoto;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getSubCat() {
		return subCat;
	}

	public void setSubCat(String subCat) {
		this.subCat = subCat;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getFlavour() {
		return flavour;
	}

	public void setFlavour(String flavour) {
		this.flavour = flavour;
	}

	public String getPacking() {
		return packing;
	}

	public void setPacking(String packing) {
		this.packing = packing;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKsLevelCode() {
		return ksLevelCode;
	}

	public void setKsLevelCode(String ksLevelCode) {
		this.ksLevelCode = ksLevelCode;
	}

	public String getKsLevelName() {
		return ksLevelName;
	}

	public void setKsLevelName(String ksLevelName) {
		this.ksLevelName = ksLevelName;
	}

	public Long getKsLevelId() {
		return ksLevelId;
	}

	public void setKsLevelId(Long ksLevelId) {
		this.ksLevelId = ksLevelId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLevelCustomer() {
		return levelCustomer;
	}

	public void setLevelCustomer(String levelCustomer) {
		this.levelCustomer = levelCustomer;
	}

	public Integer getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(Integer multiplier) {
		this.multiplier = multiplier;
	}

	public Long getCycleId() {
		return cycleId;
	}

	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}

	public Integer getRewardType() {
		return rewardType;
	}

	public void setRewardType(Integer rewardType) {
		this.rewardType = rewardType;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Integer getQuantityTarget() {
		return quantityTarget;
	}

	public void setQuantityTarget(Integer quantityTarget) {
		this.quantityTarget = quantityTarget;
	}

	public BigDecimal getAmountTarget() {
		return amountTarget;
	}

	public void setAmountTarget(BigDecimal amountTarget) {
		this.amountTarget = amountTarget;
	}

	public BigDecimal getPercentQuantity() {
		return percentQuantity;
	}

	public void setPercentQuantity(BigDecimal percentQuantity) {
		this.percentQuantity = percentQuantity;
	}

	public BigDecimal getPercentAmount() {
		return percentAmount;
	}

	public void setPercentAmount(BigDecimal percentAmount) {
		this.percentAmount = percentAmount;
	}

	public BigDecimal getTotalReward() {
		return totalReward;
	}

	public void setTotalReward(BigDecimal totalReward) {
		this.totalReward = totalReward;
	}

	public BigDecimal getTotalRewardDone() {
		return totalRewardDone;
	}

	public void setTotalRewardDone(BigDecimal totalRewardDone) {
		this.totalRewardDone = totalRewardDone;
	}

	public BigDecimal getPayReward() {
		return payReward;
	}

	public void setPayReward(BigDecimal payReward) {
		this.payReward = payReward;
	}

	public String getProductReward() {
		return productReward;
	}

	public void setProductReward(String productReward) {
		this.productReward = productReward;
	}

	public String getProductRewardDone() {
		return productRewardDone;
	}

	public void setProductRewardDone(String productRewardDone) {
		this.productRewardDone = productRewardDone;
	}

	public BigDecimal getAmountDiscount() {
		return amountDiscount;
	}

	public void setAmountDiscount(BigDecimal amountDiscount) {
		this.amountDiscount = amountDiscount;
	}

	public BigDecimal getOverAmountDiscount() {
		return overAmountDiscount;
	}

	public void setOverAmountDiscount(BigDecimal overAmountDiscount) {
		this.overAmountDiscount = overAmountDiscount;
	}

	public BigDecimal getDisplayMoney() {
		return displayMoney;
	}

	public void setDisplayMoney(BigDecimal displayMoney) {
		this.displayMoney = displayMoney;
	}

	public BigDecimal getSupportMoney() {
		return supportMoney;
	}

	public void setSupportMoney(BigDecimal supportMoney) {
		this.supportMoney = supportMoney;
	}

	public BigDecimal getOddDiscount() {
		return oddDiscount;
	}

	public void setOddDiscount(BigDecimal oddDiscount) {
		this.oddDiscount = oddDiscount;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public BigDecimal getAddMoney() {
		return addMoney;
	}

	public void setAddMoney(BigDecimal addMoney) {
		this.addMoney = addMoney;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getTotalDone() {
		return totalDone;
	}

	public void setTotalDone(BigDecimal totalDone) {
		this.totalDone = totalDone;
	}

	public List<KeyShopVO> getLstProduct() {
		return lstProduct;
	}

	public void setLstProduct(List<KeyShopVO> lstProduct) {
		this.lstProduct = lstProduct;
	}

	public List<KeyShopVO> getLstLevel() {
		return lstLevel;
	}

	public void setLstLevel(List<KeyShopVO> lstLevel) {
		this.lstLevel = lstLevel;
	}

	public List<AllocateShopVO> getLstShop() {
		return lstShop;
	}

	public void setLstShop(List<AllocateShopVO> lstShop) {
		this.lstShop = lstShop;
	}

	public String getStrStatus() {
		return strStatus;
	}

	public void setStrStatus(String strStatus) {
		this.strStatus = strStatus;
	}

	public String getStrKSType() {
		return strKSType;
	}

	public void setStrKSType(String strKSType) {
		this.strKSType = strKSType;
	}

	public String getKsNameUpper() {
		return ksNameUpper;
	}

	public void setKsNameUpper(String ksNameUpper) {
		this.ksNameUpper = ksNameUpper;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getZoneCode() {
		return zoneCode;
	}

	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}

	public String getStrRewardType() {
		return strRewardType;
	}

	public void setStrRewardType(String strRewardType) {
		this.strRewardType = strRewardType;
	}

	public String getStrResult() {
		return strResult;
	}

	public void setStrResult(String strResult) {
		this.strResult = strResult;
	}

	public String getStrRewardStatus() {
		return strRewardStatus;
	}

	public void setStrRewardStatus(String strRewardStatus) {
		this.strRewardStatus = strRewardStatus;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public BigDecimal getQuantityAmount() {
		return quantityAmount;
	}

	public void setQuantityAmount(BigDecimal quantityAmount) {
		this.quantityAmount = quantityAmount;
	}

	public Integer getQuantityRetail() {
		return quantityRetail;
	}

	public void setQuantityRetail(Integer quantityRetail) {
		this.quantityRetail = quantityRetail;
	}

	public Integer getQuantityPackage() {
		return quantityPackage;
	}

	public void setQuantityPackage(Integer quantityPackage) {
		this.quantityPackage = quantityPackage;
	}

	public Integer getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public List<Integer> getLstYear() {
		return lstYear;
	}

	public void setLstYear(List<Integer> lstYear) {
		this.lstYear = lstYear;
	}

	public Integer getFromCycleYear() {
		return fromCycleYear;
	}

	public void setFromCycleYear(Integer fromCycleYear) {
		this.fromCycleYear = fromCycleYear;
	}

	public Integer getToCycleYear() {
		return toCycleYear;
	}

	public void setToCycleYear(Integer toCycleYear) {
		this.toCycleYear = toCycleYear;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getPrecinctName() {
		return precinctName;
	}

	public void setPrecinctName(String precinctName) {
		this.precinctName = precinctName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHousenumber() {
		return housenumber;
	}

	public void setHousenumber(String housenumber) {
		this.housenumber = housenumber;
	}
	
}

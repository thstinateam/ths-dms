package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;


public class SaleOrderDetailGroupVO  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long productId;
	
	private String productCode;
	
	private String productName;
	
	private BigDecimal price;
	
	private Integer stockQuantity;
	
	private Integer convfact;
	
	private Integer quantity;
	
	private BigDecimal amount;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPrice() {
		return price == null ? BigDecimal.valueOf(0) : price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getStockQuantity() {
		if (stockQuantity == null) {
			stockQuantity = 0;
		}
		return stockQuantity;
	}

	public void setStockQuantity(Integer stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public Integer getConvfact() {
		if (convfact == null) {
			convfact = 0;
		}
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}
	
	
}
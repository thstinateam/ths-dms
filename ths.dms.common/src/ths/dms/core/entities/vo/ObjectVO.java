package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;


public class ObjectVO<T>  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<T> lstObject;
	private KPaging<T> kPaging;
	
	public List<T> getLstObject() {
		return lstObject == null ? new ArrayList<T>() : lstObject;
	}
	public void setLstObject(List<T> lstObject) {
		this.lstObject = lstObject;
	}
	public KPaging<T> getkPaging() {
		return kPaging;
	}
	public void setkPaging(KPaging<T> kPaging) {
		this.kPaging = kPaging;
	}
	
	
}
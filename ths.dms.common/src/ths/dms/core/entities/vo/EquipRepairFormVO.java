package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Class EquipRepairFormVO
 * 
 *@author phuongvm
 *@since 30/12/2014
 *@description 
 */
public class EquipRepairFormVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String maPhieu;
	private String loaiThietBi;
	private String nhomThietBi;
	private String maThietBi;
	private String soSeri;
	private String kho;
	private String ngayTao;
	private String khachHang;
	private String donVi;
	private String diaChi;
	private Integer trangThai;
	private String tinhTrangHuHong;
	private String lyDoDeNghi;
	private Long tongTien;
	private String lyDoTuChoi;
	private Integer kyMo;
	private Integer daThanhToan;
	private Integer paymentPeriodOpen;
	private Date createFormDate;
	private String note;
	/** dung popup ql thanh toan sua chua*/
	private Integer namSanXuat;
	private String url;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMaPhieu() {
		return maPhieu;
	}
	public void setMaPhieu(String maPhieu) {
		this.maPhieu = maPhieu;
	}
	public String getLoaiThietBi() {
		return loaiThietBi;
	}
	public void setLoaiThietBi(String loaiThietBi) {
		this.loaiThietBi = loaiThietBi;
	}
	public String getNhomThietBi() {
		return nhomThietBi;
	}
	public void setNhomThietBi(String nhomThietBi) {
		this.nhomThietBi = nhomThietBi;
	}
	public String getMaThietBi() {
		return maThietBi;
	}
	public void setMaThietBi(String maThietBi) {
		this.maThietBi = maThietBi;
	}
	public String getSoSeri() {
		return soSeri;
	}
	public void setSoSeri(String soSeri) {
		this.soSeri = soSeri;
	}
	public String getKho() {
		return kho;
	}
	public void setKho(String kho) {
		this.kho = kho;
	}
	public String getNgayTao() {
		return ngayTao;
	}
	public void setNgayTao(String ngayTao) {
		this.ngayTao = ngayTao;
	}
	public Integer getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(Integer trangThai) {
		this.trangThai = trangThai;
	}
	public String getTinhTrangHuHong() {
		return tinhTrangHuHong;
	}
	public void setTinhTrangHuHong(String tinhTrangHuHong) {
		this.tinhTrangHuHong = tinhTrangHuHong;
	}
	public String getLyDoDeNghi() {
		return lyDoDeNghi;
	}
	public void setLyDoDeNghi(String lyDoDeNghi) {
		this.lyDoDeNghi = lyDoDeNghi;
	}
	public Long getTongTien() {
		return tongTien;
	}
	public void setTongTien(Long tongTien) {
		this.tongTien = tongTien;
	}
	public String getLyDoTuChoi() {
		return lyDoTuChoi;
	}
	public void setLyDoTuChoi(String lyDoTuChoi) {
		this.lyDoTuChoi = lyDoTuChoi;
	}
	public Integer getKyMo() {
		return kyMo;
	}
	public void setKyMo(Integer kyMo) {
		this.kyMo = kyMo;
	}
	public Integer getDaThanhToan() {
		return daThanhToan;
	}
	public void setDaThanhToan(Integer daThanhToan) {
		this.daThanhToan = daThanhToan;
	}
	public String getKhachHang() {
		return khachHang;
	}
	public void setKhachHang(String khachHang) {
		this.khachHang = khachHang;
	}
	public String getDonVi() {
		return donVi;
	}
	public void setDonVi(String donVi) {
		this.donVi = donVi;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public Integer getPaymentPeriodOpen() {
		return paymentPeriodOpen;
	}
	public void setPaymentPeriodOpen(Integer paymentPeriodOpen) {
		this.paymentPeriodOpen = paymentPeriodOpen;
	}
	public Integer getNamSanXuat() {
		return namSanXuat;
	}
	public void setNamSanXuat(Integer namSanXuat) {
		this.namSanXuat = namSanXuat;
	}
	public Date getCreateFormDate() {
		return createFormDate;
	}
	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}

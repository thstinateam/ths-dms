package ths.dms.core.entities.vo;
/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;

/**
 * Class EquipmentRoleVOTempImport
 * 
 * @author vuongmq
 * @date Mar 24,2015
 * Xu ly import VO temp de them quyen
 */
public class EquipmentRoleVOTempImport implements Serializable {

	private static final long serialVersionUID = 1L;
	private String code;
	private String name;	
	private String shopCode;
	private String stockCode;
	private ActiveType status;
	private Integer viewUnder;
	private EquipStock equipStock;
	private Shop shop;
	
	/**
	 * Khai bao GETTER/SETTER
	 * */
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public ActiveType getStatus() {
		return status;
	}
	public void setStatus(ActiveType status) {
		this.status = status;
	}
	public Integer getViewUnder() {
		return viewUnder;
	}
	public void setViewUnder(Integer viewUnder) {
		this.viewUnder = viewUnder;
	}
	public EquipStock getEquipStock() {
		return equipStock;
	}
	public void setEquipStock(EquipStock equipStock) {
		this.equipStock = equipStock;
	}
	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
}

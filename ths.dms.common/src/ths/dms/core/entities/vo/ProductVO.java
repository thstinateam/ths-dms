/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Class dung chung cho san pha,m
 * @modify hunglm16
 * @since 10/11/2015
 */
public class ProductVO implements Serializable {

	private static final long serialVersionUID = -347747521325693881L;
	
	private Long catId;
	private Long subCatId;
	private Long productId;

	private BigDecimal price;
	private Integer quantity;
	private Integer convfact;
	private Integer checkLot;

	private String catCode;
	private String catName;
	private String subCatCode;
	private String subCatName;
	private String productCode;
	private String productName;
	private String catelogyCode;
	private String productInfoCode;
	private String dateSysStr;
	private Integer productStatus;
	private String donvitinh;
	private String uom1;

	/**
	 * Cong chuoi thong tin id San Pham, id Nghanh hang con, id Nganh hang
	 * 
	 * @modify hunglm16
	 * @since 11/10/2015
	 * @description ra soat class 
	 * */
	public String toString() {
		String t = catId + " " + subCatId + " " + productId;
		return t;
	}
	
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getUom1() {
		return uom1;
	}

	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	public String getDonvitinh() {
		return donvitinh;
	}

	public void setDonvitinh(String donvitinh) {
		this.donvitinh = donvitinh;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getCatelogyCode() {
		return catelogyCode;
	}

	public void setCatelogyCode(String catelogyCode) {
		this.catelogyCode = catelogyCode;
	}

	public Long getCatId() {
		return catId;
	}

	public void setCatId(Long catId) {
		this.catId = catId;
	}

	public String getCatCode() {
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public Long getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(Long subCatId) {
		this.subCatId = subCatId;
	}

	public String getSubCatCode() {
		return subCatCode;
	}

	public void setSubCatCode(String subCatCode) {
		this.subCatCode = subCatCode;
	}

	public String getSubCatName() {
		return subCatName;
	}

	public void setSubCatName(String subCatName) {
		this.subCatName = subCatName;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public String getProductInfoCode() {
		return productInfoCode;
	}

	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}
	public Integer getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(Integer productStatus) {
		this.productStatus = productStatus;
	}
	public String getDateSysStr() {
		return dateSysStr;
	}

	public void setDateSysStr(String dateSysStr) {
		this.dateSysStr = dateSysStr;
	}
	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}
	
}

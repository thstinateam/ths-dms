package ths.dms.core.entities.vo;
/**
 * Import thu vien
 * */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Class Equip Lost Record VO
 * 
 * @author hunglm16
 * @since December 30,2014
 * @description Them thuoc tinh co trong bang lien quan den Equiment Record
 */
public class EquipRecordVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long equipId;
	private Long shopId;
	private Long stockId;
	private Long customerId;
	
	private Integer stockType;
	private Integer recordStatus;
	private Integer deliveryStatus;
	private Integer tracingResult;
	private Integer tracingPlace;
	private Integer manufacturingYear;
	
	private String code;
	private String stockCode;
	private String lostDateStr;
	private String lostDateStrVi;
	private String shortCode;
	private String customerName;
	private String shortCodeName;
	private String address;
	private String shopCode;
	private String shopName;
	private String shopCodeName;
	private String serial;
	private String equipCode;
	private String representor;
	private String lastArisingSalesDateStr;
	private String lastArisSalDateStrVi;
	private String conclusion;
	private String tracingResultStr;
	private String tracingPlaceStr;
	private String recommendedTreatment;
	private String deliveryStatusStr;
	private String actDateStr;
	private String customerAddress;
	private String equipCategoryCode;
	private String equipGroupName;
	private String equipCategoryName;
	private String customerPhone;
	private String representation;
	private String contractNumber;
	private String firstDateInUseStr;
	private String note;
	
	private Date lostDate;
	
	private BigDecimal priceActually; // vuongmq; 12/05/2015; export
	private String shopIsLevel5Code; // vuongmq; 12/05/2015; print, npp
	private String shopIsLevel3Code; // vuongmq; 12/05/2015; print, mien
	private String chucVuBenMuon; // vuongmq; 29/05/2015; print chuc vu ben ben; bao mat
	private String equipLostDate;//Datpv4 ; 26/06/2015 ngay mat 
	private String recordStatusStr;//Datpv4;26/06/2015 mo ta trang thai bien ban
	
	private String url;
	/**
	 * Khai bao phuong thuc GETTER/SETTER
	 * */
	
	public Long getId() {
		return id;
	}
	public String getFirstDateInUseStr() {
		return firstDateInUseStr;
	}
	public void setFirstDateInUseStr(String firstDateInUseStr) {
		this.firstDateInUseStr = firstDateInUseStr;
	}
	public Date getLostDate() {
		return lostDate;
	}
	public void setLostDate(Date lostDate) {
		this.lostDate = lostDate;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getRepresentation() {
		return representation;
	}
	public void setRepresentation(String representation) {
		this.representation = representation;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getLastArisSalDateStrVi() {
		return lastArisSalDateStrVi;
	}
	public void setLastArisSalDateStrVi(String lastArisSalDateStrVi) {
		this.lastArisSalDateStrVi = lastArisSalDateStrVi;
	}
	public String getLostDateStrVi() {
		return lostDateStrVi;
	}
	public void setLostDateStrVi(String lostDateStrVi) {
		this.lostDateStrVi = lostDateStrVi;
	}
	public Integer getManufacturingYear() {
		return manufacturingYear;
	}
	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}
	public Integer getDeliveryStatus() {
		return deliveryStatus;
	}
	public void setDeliveryStatus(Integer deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	public String getActDateStr() {
		return actDateStr;
	}
	public void setActDateStr(String actDateStr) {
		this.actDateStr = actDateStr;
	}
	public String getDeliveryStatusStr() {
		return deliveryStatusStr;
	}
	public void setDeliveryStatusStr(String deliveryStatusStr) {
		this.deliveryStatusStr = deliveryStatusStr;
	}
	public Integer getStockType() {
		return stockType;
	}
	public void setStockType(Integer stockType) {
		this.stockType = stockType;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEquipId() {
		return equipId;
	}
	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getStockId() {
		return stockId;
	}
	public void setStockId(Long stockId) {
		this.stockId = stockId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public Integer getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getLostDateStr() {
		return lostDateStr;
	}
	public void setLostDateStr(String lostDateStr) {
		this.lostDateStr = lostDateStr;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getShortCodeName() {
		return shortCodeName;
	}
	public void setShortCodeName(String shortCodeName) {
		this.shortCodeName = shortCodeName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopCodeName() {
		return shopCodeName;
	}
	public void setShopCodeName(String shopCodeName) {
		this.shopCodeName = shopCodeName;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getEquipCode() {
		return equipCode;
	}
	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}
	public String getRepresentor() {
		return representor;
	}
	public void setRepresentor(String representor) {
		this.representor = representor;
	}
	public String getLastArisingSalesDateStr() {
		return lastArisingSalesDateStr;
	}
	public void setLastArisingSalesDateStr(String lastArisingSalesDateStr) {
		this.lastArisingSalesDateStr = lastArisingSalesDateStr;
	}
	public String getConclusion() {
		return conclusion;
	}
	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}
	public String getTracingResultStr() {
		return tracingResultStr;
	}
	public void setTracingResultStr(String tracingResultStr) {
		this.tracingResultStr = tracingResultStr;
	}
	public String getTracingPlaceStr() {
		return tracingPlaceStr;
	}
	public void setTracingPlaceStr(String tracingPlaceStr) {
		this.tracingPlaceStr = tracingPlaceStr;
	}
	public String getRecommendedTreatment() {
		return recommendedTreatment;
	}
	public void setRecommendedTreatment(String recommendedTreatment) {
		this.recommendedTreatment = recommendedTreatment;
	}
	public Integer getTracingResult() {
		return tracingResult;
	}
	public void setTracingResult(Integer tracingResult) {
		this.tracingResult = tracingResult;
	}
	public Integer getTracingPlace() {
		return tracingPlace;
	}
	public void setTracingPlace(Integer tracingPlace) {
		this.tracingPlace = tracingPlace;
	}
	public String getEquipCategoryCode() {
		return equipCategoryCode;
	}
	public void setEquipCategoryCode(String equipCategoryCode) {
		this.equipCategoryCode = equipCategoryCode;
	}
	public String getEquipGroupName() {
		return equipGroupName;
	}
	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}
	public String getEquipCategoryName() {
		return equipCategoryName;
	}
	public void setEquipCategoryName(String equipCategoryName) {
		this.equipCategoryName = equipCategoryName;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public BigDecimal getPriceActually() {
		return priceActually;
	}
	public void setPriceActually(BigDecimal priceActually) {
		this.priceActually = priceActually;
	}
	public String getShopIsLevel5Code() {
		return shopIsLevel5Code;
	}
	public void setShopIsLevel5Code(String shopIsLevel5Code) {
		this.shopIsLevel5Code = shopIsLevel5Code;
	}
	public String getShopIsLevel3Code() {
		return shopIsLevel3Code;
	}
	public void setShopIsLevel3Code(String shopIsLevel3Code) {
		this.shopIsLevel3Code = shopIsLevel3Code;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getChucVuBenMuon() {
		return chucVuBenMuon;
	}
	public void setChucVuBenMuon(String chucVuBenMuon) {
		this.chucVuBenMuon = chucVuBenMuon;
	}
	public String getEquipLostDate() {
		return equipLostDate;
	}
	public void setEquipLostDate(String equipLostDate) {
		this.equipLostDate = equipLostDate;
	}
	public String getRecordStatusStr() {
		return recordStatusStr;
	}
	public void setRecordStatusStr(String recordStatusStr) {
		this.recordStatusStr = recordStatusStr;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
}

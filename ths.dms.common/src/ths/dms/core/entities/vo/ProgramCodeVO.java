package ths.dms.core.entities.vo;

import java.io.Serializable;

public class ProgramCodeVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long saleOrderId;
	private Long saleOrderDetailId;
	private Long productId;
	private String productCode;
	private String strProgramCode;
	
	public Long getSaleOrderId() {
		return saleOrderId;
	}
	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}
	public Long getSaleOrderDetailId() {
		return saleOrderDetailId;
	}
	public void setSaleOrderDetailId(Long saleOrderDetailId) {
		this.saleOrderDetailId = saleOrderDetailId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getStrProgramCode() {
		return strProgramCode;
	}
	public void setStrProgramCode(String strProgramCode) {
		this.strProgramCode = strProgramCode;
	}

}

/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * VO su dung tren view 
 * @author tuannd20
 */
public class EquipmentGroupVO implements Serializable {
	
	private static final long serialVersionUID = -7067549208319298000L;
	
	private Long id;
	private Long equipGroupId;
	
	private Integer status;
	
	/**Su dung import export excel cho equip group*/
	//STT row
	private Integer indexRow;
	// Ma nhom thiet bi
	private String code;
	// Ten nhom thiet bi
	private String name;
	// Trang thai nhom thiet bi
	private String statusStr;
	// Hieu
	private String brandName;
	// Dung tich tu
	private String capacityFrom;
	// Dung tich den
	private String capacityTo;
	// Loai 
	private String typeGroup;
	// Ngay tao
	private Date createDate;
	// Nguoi tao 
	private String createUser;
	//check nhom thiet bi co muc doanh so khong
	private Boolean flagCheckExistSP;
	// Danh sach muc doanh so
	private List<EquipSalePlanVO> lstEquipSalePlanVO;
	private List<String> row;
	
	//ma san pham
	private String productCode;
	
	//tu ngay
	private String fromDate;
	
	//den ngay
	private String toDate;
	
	/**
	 * Khai bao cac phuong thuc GETTER/SETER
	 * */
	
	public Long getId() {
		return id;
	}
	public Long getEquipGroupId() {
		return equipGroupId;
	}
	public void setEquipGroupId(Long equipGroupId) {
		this.equipGroupId = equipGroupId;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getIndexRow() {
		return indexRow;
	}
	public void setIndexRow(Integer indexRow) {
		this.indexRow = indexRow;
	}
	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getCapacityFrom() {
		return capacityFrom;
	}
	public void setCapacityFrom(String capacityFrom) {
		this.capacityFrom = capacityFrom;
	}
	public String getCapacityTo() {
		return capacityTo;
	}
	public void setCapacityTo(String capacityTo) {
		this.capacityTo = capacityTo;
	}
	public String getTypeGroup() {
		return typeGroup;
	}
	public void setTypeGroup(String typeGroup) {
		this.typeGroup = typeGroup;
	}
	public List<EquipSalePlanVO> getLstEquipSalePlanVO() {
		return lstEquipSalePlanVO;
	}
	public void setLstEquipSalePlanVO(List<EquipSalePlanVO> lstEquipSalePlanVO) {
		this.lstEquipSalePlanVO = lstEquipSalePlanVO;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public List<String> getRow() {
		return row;
	}
	public void setRow(List<String> row) {
		this.row = row;
	}
	public Boolean getFlagCheckExistSP() {
		return flagCheckExistSP;
	}
	public void setFlagCheckExistSP(Boolean flagCheckExistSP) {
		this.flagCheckExistSP = flagCheckExistSP;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
}

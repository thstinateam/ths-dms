package ths.dms.core.entities.vo;

import java.io.Serializable;

public class TreeFlagExtentVO<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private Long attrId;
	private Long isJoinId;
	private Long isJoin;
	private Long longG;
	private Integer isAttr;
	private Integer isChildJoin;
	private Integer channelType;
	private Integer channelObjectType;
	private Integer status;
	private Integer intG;
	private T object;
	public T getObject() {
		return object;
	}
	public void setObject(T object) {
		this.object = object;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getAttrId() {
		return attrId;
	}
	public void setAttrId(Long attrId) {
		this.attrId = attrId;
	}
	public Long getIsJoinId() {
		return isJoinId;
	}
	public void setIsJoinId(Long isJoinId) {
		this.isJoinId = isJoinId;
	}
	public Long getIsJoin() {
		return isJoin;
	}
	public void setIsJoin(Long isJoin) {
		this.isJoin = isJoin;
	}
	public Long getLongG() {
		return longG;
	}
	public void setLongG(Long longG) {
		this.longG = longG;
	}
	public Integer getIsAttr() {
		return isAttr;
	}
	public void setIsAttr(Integer isAttr) {
		this.isAttr = isAttr;
	}
	public Integer getIsChildJoin() {
		return isChildJoin;
	}
	public void setIsChildJoin(Integer isChildJoin) {
		this.isChildJoin = isChildJoin;
	}
	public Integer getChannelType() {
		return channelType;
	}
	public void setChannelType(Integer channelType) {
		this.channelType = channelType;
	}
	public Integer getChannelObjectType() {
		return channelObjectType;
	}
	public void setChannelObjectType(Integer channelObjectType) {
		this.channelObjectType = channelObjectType;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getIntG() {
		return intG;
	}
	public void setIntG(Integer intG) {
		this.intG = intG;
	}
	
}

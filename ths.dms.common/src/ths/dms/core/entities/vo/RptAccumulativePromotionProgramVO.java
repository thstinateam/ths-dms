package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.RptAccumulativePromotionProgram;
import ths.dms.core.entities.RptAccumulativePromotionProgramDetail;

public class RptAccumulativePromotionProgramVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private RptAccumulativePromotionProgram rptAccumulativePP;
	private List<RptAccumulativePromotionProgramDetail> rptAccumulativePPDetail;
	private List<RptAccumulativePromotionProgramDetail> rptAccumulativePPDetailFinal;
	
	public RptAccumulativePromotionProgram getRptAccumulativePP() {
		return rptAccumulativePP;
	}
	public void setRptAccumulativePP(RptAccumulativePromotionProgram rptAccumulativePromotionProgram) {
		this.rptAccumulativePP = rptAccumulativePromotionProgram;
	}
	public List<RptAccumulativePromotionProgramDetail> getRptAccumulativePPDetail() {
		return rptAccumulativePPDetail;
	}
	public void setRptAccumulativePPDetail(List<RptAccumulativePromotionProgramDetail> rptAccumulativePromotionProgramDetail) {
		this.rptAccumulativePPDetail = rptAccumulativePromotionProgramDetail;
	}
	public List<RptAccumulativePromotionProgramDetail> getRptAccumulativePPDetailFinal() {
		return rptAccumulativePPDetailFinal;
	}
	public void setRptAccumulativePPDetailFinal(List<RptAccumulativePromotionProgramDetail> rptAccumulativePPDetailFinal) {
		this.rptAccumulativePPDetailFinal = rptAccumulativePPDetailFinal;
	}
	
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Bao cao doanh so san luong
 * @author vuongmq
 * @since 26/10/2015 
 */
public class RptSLDSVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	
	private String maVung;
	private String maMien;
	private String maNPP;
	private String tenNPP;
	private String maGSNPP;
	private String tenGSNPP;
	private String maNVBH;
	private String tenNVBH;
	private String ngay;
	private String maTuyen;
	private String tenTuyen;
	private String maKH;
	private String tenKH;
	private String maSP;
	private String tenSP;
	private String nhanHang;
	private String nganhHang;
	private String nganhHangCon;
	private String dongGoi;
	private String huongVi;
	private String donViTinh;
	private String diaChi;
	private String phoneNumber;
	private String maNV;
	private String tenNV;
	
	private Integer quyCach;
	private Integer convfact;
	private Integer thung; 
	private Integer hop;
	private Integer thungDuyet; 
	private Integer hopDuyet;
	private Integer sanLuong;
	private Integer sanLuongDuyet;
	
	private BigDecimal giaChuaThue;
	private BigDecimal giaSauThue;
	private BigDecimal thanhTien;
	private BigDecimal thanhTienDuyet;
	
	private BigDecimal chiTieu;
	private BigDecimal doanhSo;
	private BigDecimal conLai;
	private BigDecimal tienDo;
	
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaGSNPP() {
		return maGSNPP;
	}
	public void setMaGSNPP(String maGSNPP) {
		this.maGSNPP = maGSNPP;
	}
	public String getTenGSNPP() {
		return tenGSNPP;
	}
	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getNgay() {
		return ngay;
	}
	public void setNgay(String ngay) {
		this.ngay = ngay;
	}
	public String getMaTuyen() {
		return maTuyen;
	}
	public void setMaTuyen(String maTuyen) {
		this.maTuyen = maTuyen;
	}
	public String getTenTuyen() {
		return tenTuyen;
	}
	public void setTenTuyen(String tenTuyen) {
		this.tenTuyen = tenTuyen;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getMaSP() {
		return maSP;
	}
	public void setMaSP(String maSP) {
		this.maSP = maSP;
	}
	public String getTenSP() {
		return tenSP;
	}
	public void setTenSP(String tenSP) {
		this.tenSP = tenSP;
	}
	public String getNhanHang() {
		return nhanHang;
	}
	public void setNhanHang(String nhanHang) {
		this.nhanHang = nhanHang;
	}
	public String getNganhHang() {
		return nganhHang;
	}
	public void setNganhHang(String nganhHang) {
		this.nganhHang = nganhHang;
	}
	public String getNganhHangCon() {
		return nganhHangCon;
	}
	public void setNganhHangCon(String nganhHangCon) {
		this.nganhHangCon = nganhHangCon;
	}
	public String getDongGoi() {
		return dongGoi;
	}
	public void setDongGoi(String dongGoi) {
		this.dongGoi = dongGoi;
	}
	public String getHuongVi() {
		return huongVi;
	}
	public void setHuongVi(String huongVi) {
		this.huongVi = huongVi;
	}
	public String getDonViTinh() {
		return donViTinh;
	}
	public void setDonViTinh(String donViTinh) {
		this.donViTinh = donViTinh;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Integer getQuyCach() {
		return quyCach;
	}
	public void setQuyCach(Integer quyCach) {
		this.quyCach = quyCach;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public Integer getThung() {
		return thung;
	}
	public void setThung(Integer thung) {
		this.thung = thung;
	}
	public Integer getHop() {
		return hop;
	}
	public void setHop(Integer hop) {
		this.hop = hop;
	}
	public Integer getThungDuyet() {
		return thungDuyet;
	}
	public void setThungDuyet(Integer thungDuyet) {
		this.thungDuyet = thungDuyet;
	}
	public Integer getHopDuyet() {
		return hopDuyet;
	}
	public void setHopDuyet(Integer hopDuyet) {
		this.hopDuyet = hopDuyet;
	}
	public Integer getSanLuong() {
		return sanLuong;
	}
	public void setSanLuong(Integer sanLuong) {
		this.sanLuong = sanLuong;
	}
	public Integer getSanLuongDuyet() {
		return sanLuongDuyet;
	}
	public void setSanLuongDuyet(Integer sanLuongDuyet) {
		this.sanLuongDuyet = sanLuongDuyet;
	}
	public BigDecimal getGiaChuaThue() {
		return giaChuaThue;
	}
	public void setGiaChuaThue(BigDecimal giaChuaThue) {
		this.giaChuaThue = giaChuaThue;
	}
	public BigDecimal getGiaSauThue() {
		return giaSauThue;
	}
	public void setGiaSauThue(BigDecimal giaSauThue) {
		this.giaSauThue = giaSauThue;
	}
	public BigDecimal getThanhTien() {
		return thanhTien;
	}
	public void setThanhTien(BigDecimal thanhTien) {
		this.thanhTien = thanhTien;
	}
	public BigDecimal getThanhTienDuyet() {
		return thanhTienDuyet;
	}
	public void setThanhTienDuyet(BigDecimal thanhTienDuyet) {
		this.thanhTienDuyet = thanhTienDuyet;
	}
	public BigDecimal getChiTieu() {
		return chiTieu;
	}
	public void setChiTieu(BigDecimal chiTieu) {
		this.chiTieu = chiTieu;
	}
	public BigDecimal getDoanhSo() {
		return doanhSo;
	}
	public void setDoanhSo(BigDecimal doanhSo) {
		this.doanhSo = doanhSo;
	}
	public BigDecimal getConLai() {
		return conLai;
	}
	public void setConLai(BigDecimal conLai) {
		this.conLai = conLai;
	}
	public BigDecimal getTienDo() {
		return tienDo;
	}
	public void setTienDo(BigDecimal tienDo) {
		this.tienDo = tienDo;
	}
	public String getMaNV() {
		return maNV;
	}
	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}
	public String getTenNV() {
		return tenNV;
	}
	public void setTenNV(String tenNV) {
		this.tenNV = tenNV;
	}
	
}

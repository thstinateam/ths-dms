package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.entities.Warehouse;

public class PoVnmDetailLotVO implements Serializable {
	/**
	 * 
	 */
	/*
	 * @author: hunglm16
	 * @since: February 7, 2014
	 * @description: co su dung cho them chuc nang getListFullPoVnmDetailVOByListPoVnmId lien quan 
	 * */
	private static final long serialVersionUID = 1L;
	
	//poVnmDetailLotId
	private Long poVnmDetailLotId;
	
	//productId
	private Long productId;
	
	//productCode
	private String productCode;
	
	//productName
	private String productName;
	
	private Integer checkLot = 0;
	
	//quantity
	private Integer quantity;
	
	//quantity_pay
	private Integer quantityPay;
	
	//quantity_received
	private Integer quantityReceived;
	
	//quantity_pay
	private Integer convfact;
	
	//price
	private BigDecimal price = new BigDecimal(0);
	
	//packagePrice
	private BigDecimal packagePrice = new BigDecimal(0);
	
	//amount
	private BigDecimal amount = new BigDecimal(0);
	
	//lot
	private String lot;
	
	//quantity (stock_total)
	private Integer quantityStock;
	
	private Integer quantityStockTotal;
	
	private String wareHouseName;
	
	private Long wareHouseId;
	
	private Integer returnValue;
	
	private Integer productType;
	
	private Long shopId;
	
	private Integer lineSaleOrder;

	private Integer linePo;
	
	List<Warehouse> lstWarehouse = new ArrayList<Warehouse>();
	
	public Integer getReturnValue() {
		return returnValue;
	}


	public void setReturnValue(Integer returnValue) {
		this.returnValue = returnValue;
	}


	public String getWareHouseName() {
		return wareHouseName;
	}


	public void setWareHouseName(String wareHouseName) {
		this.wareHouseName = wareHouseName;
	}


	public Long getPoVnmDetailLotId() {
		return poVnmDetailLotId;
	}


	public void setPoVnmDetailLotId(Long poVnmDetailLotId) {
		this.poVnmDetailLotId = poVnmDetailLotId;
	}


	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}


	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}


	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}


	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}


	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}


	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}


	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}


	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	public Integer getQuantityPay() {
		if (quantityPay == null) {
			quantityPay = 0;
		}
		return quantityPay;
	}


	public Integer getQuantityReceived() {
		if (quantityReceived == null) {
			quantityReceived = 0;
		}
		return quantityReceived;
	}


	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}


	public void setQuantityPay(Integer quantityPay) {
		this.quantityPay = quantityPay;
	}


	public Integer getConvfact() {
		if (convfact == null) {
			convfact = 0;
		}
		return convfact;
	}


	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}


	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price == null ? BigDecimal.valueOf(0) : price;
	}


	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getPackagePrice() {
		return packagePrice == null ? BigDecimal.ZERO : packagePrice;
	}


	public void setPackagePrice(BigDecimal packagePrice) {
		this.packagePrice = packagePrice;
	}


	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}


	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	/**
	 * @return the lot
	 */
	public String getLot() {
		return lot;
	}


	/**
	 * @param lot the lot to set
	 */
	public void setLot(String lot) {
		this.lot = lot;
	}


	public Integer getCheckLot() {
		return checkLot;
	}


	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}


	/**
	 * @return the quantityStock
	 */
	public Integer getQuantityStock() {
		if (quantityStock == null) {
			quantityStock = 0;
		}
		return quantityStock;
	}


	/**
	 * @param quantityStock the quantityStock to set
	 */
	public void setQuantityStock(Integer quantityStock) {
		this.quantityStock = quantityStock;
	}


	public Integer getQuantityStockTotal() {
		if (quantityStockTotal == null) {
			quantityStockTotal = 0;
		}
		return quantityStockTotal;
	}


	public void setQuantityStockTotal(Integer quantityStockTotal) {
		this.quantityStockTotal = quantityStockTotal;
	}


	public Long getWareHouseId() {
		return wareHouseId;
	}


	public void setWareHouseId(Long wareHouseId) {
		this.wareHouseId = wareHouseId;
	}


	public Integer getProductType() {
		return productType;
	}


	public void setProductType(Integer productType) {
		this.productType = productType;
	}


	public Long getShopId() {
		return shopId;
	}


	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}


	public List<Warehouse> getLstWarehouse() {
		return lstWarehouse;
	}


	public void setLstWarehouse(List<Warehouse> lstWarehouse) {
		this.lstWarehouse = lstWarehouse;
	}


	public Integer getLineSaleOrder() {
		return lineSaleOrder;
	}


	public void setLineSaleOrder(Integer lineSaleOrder) {
		this.lineSaleOrder = lineSaleOrder;
	}


	public Integer getLinePo() {
		return linePo;
	}


	public void setLinePo(Integer linePo) {
		this.linePo = linePo;
	}

}

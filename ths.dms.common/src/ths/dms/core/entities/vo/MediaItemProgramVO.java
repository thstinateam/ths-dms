package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

public class MediaItemProgramVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1956654986662515109L;
	private String displayProgramCode;
	private List<MediaItemDetailVO> lstMediaItem;
	public String getDisplayProgramCode() {
		return displayProgramCode;
	}
	public void setDisplayProgramCode(String displayProgramCode) {
		this.displayProgramCode = displayProgramCode;
	}
	public List<MediaItemDetailVO> getLstMediaItem() {
		return lstMediaItem;
	}
	public void setLstMediaItem(List<MediaItemDetailVO> lstMediaItem) {
		this.lstMediaItem = lstMediaItem;
	}
	
}

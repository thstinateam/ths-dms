package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.entities.SaleOrderDetail;

public class SaleOrderDetailForInvoidVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4970295328021146021L;
	
	private List<SaleOrderDetail> lstKhuyenMai;
	private List<SaleOrderDetail> lstSPBan;
	
	public SaleOrderDetailForInvoidVO(){
		lstKhuyenMai = new ArrayList<SaleOrderDetail>();
		lstSPBan = new ArrayList<SaleOrderDetail>();
	}
	
	public List<SaleOrderDetail> getLstKhuyenMai() {
		return lstKhuyenMai;
	}
	public void setLstKhuyenMai(List<SaleOrderDetail> lstKhuyenMai) {
		this.lstKhuyenMai = lstKhuyenMai;
	}
	public List<SaleOrderDetail> getLstSPBan() {
		return lstSPBan;
	}
	public void setLstSPBan(List<SaleOrderDetail> lstSPBan) {
		this.lstSPBan = lstSPBan;
	}
	
	
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class OrderVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal orderAmount;
	
	private List<SaleOrderDetailVO> listPromotionForOrderChange;

	public BigDecimal getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(BigDecimal orderAmount) {
		this.orderAmount = orderAmount;
	}

	public List<SaleOrderDetailVO> getListPromotionForOrderChange() {
		return listPromotionForOrderChange;
	}

	public void setListPromotionForOrderChange(
			List<SaleOrderDetailVO> listPromotionForOrderChange) {
		this.listPromotionForOrderChange = listPromotionForOrderChange;
	}
	
	
}

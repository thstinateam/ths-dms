package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class PrintDeliveryGroupExportVO1 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String orderNumber;
	private String deliveryCode;
	private String deliveryName;
	private String productCode;
	private String productName;
	private Integer convfact;
	private BigDecimal price;
	
	private ArrayList<PrintDeliveryGroupExportVO2> lstDeliveryGroup = new ArrayList<PrintDeliveryGroupExportVO2>();

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * @return the lstDeliveryGroup
	 */
	public ArrayList<PrintDeliveryGroupExportVO2> getLstDeliveryGroup() {
		return lstDeliveryGroup;
	}

	/**
	 * @param lstDeliveryGroup the lstDeliveryGroup to set
	 */
	public void setLstDeliveryGroup(ArrayList<PrintDeliveryGroupExportVO2> lstDeliveryGroup) {
		this.lstDeliveryGroup = lstDeliveryGroup;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

}

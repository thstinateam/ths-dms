/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.Shop;

/**
 * Mo ta class ShopParamVOSave.java
 * @author vuongmq
 * @since Nov 28, 2015
 */
public class ShopParamVOSave implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private Shop shop;
	
	private Long shopDistanceOrder; // thuoc table shop
	
	private String shopDTStart;
	private String shopDTMiddle;
	private String shopDTEnd;
	private Long shopDTStartId;
	private Long shopDTMiddleId;
	private Long shopDTEndId;
	
	private String shopCCDistance;
	private String shopCCStart;
	private String shopCCEnd;
	private Long shopCCDistanceId;
	private Long shopCCStartId;
	private Long shopCCEndId;
	
	private List<ShopParamVODayStart> lstDayStart;
	private List<Long> lstDayStartDelete;
	
	private List<ShopParamVODayEnd> lstDayEnd;
	private List<Long> lstDayEndDelete;
	
	private List<ShopParamVOProduct> lstProductShopMapVO;
	
	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	public Long getShopDistanceOrder() {
		return shopDistanceOrder;
	}
	public void setShopDistanceOrder(Long shopDistanceOrder) {
		this.shopDistanceOrder = shopDistanceOrder;
	}
	public String getShopDTStart() {
		return shopDTStart;
	}
	public void setShopDTStart(String shopDTStart) {
		this.shopDTStart = shopDTStart;
	}
	public String getShopDTMiddle() {
		return shopDTMiddle;
	}
	public void setShopDTMiddle(String shopDTMiddle) {
		this.shopDTMiddle = shopDTMiddle;
	}
	public String getShopDTEnd() {
		return shopDTEnd;
	}
	public void setShopDTEnd(String shopDTEnd) {
		this.shopDTEnd = shopDTEnd;
	}
	public String getShopCCDistance() {
		return shopCCDistance;
	}
	public void setShopCCDistance(String shopCCDistance) {
		this.shopCCDistance = shopCCDistance;
	}
	public String getShopCCStart() {
		return shopCCStart;
	}
	public void setShopCCStart(String shopCCStart) {
		this.shopCCStart = shopCCStart;
	}
	public String getShopCCEnd() {
		return shopCCEnd;
	}
	public void setShopCCEnd(String shopCCEnd) {
		this.shopCCEnd = shopCCEnd;
	}
	public Long getShopDTStartId() {
		return shopDTStartId;
	}
	public void setShopDTStartId(Long shopDTStartId) {
		this.shopDTStartId = shopDTStartId;
	}
	public Long getShopDTMiddleId() {
		return shopDTMiddleId;
	}
	public void setShopDTMiddleId(Long shopDTMiddleId) {
		this.shopDTMiddleId = shopDTMiddleId;
	}
	public Long getShopDTEndId() {
		return shopDTEndId;
	}
	public void setShopDTEndId(Long shopDTEndId) {
		this.shopDTEndId = shopDTEndId;
	}
	public Long getShopCCDistanceId() {
		return shopCCDistanceId;
	}
	public void setShopCCDistanceId(Long shopCCDistanceId) {
		this.shopCCDistanceId = shopCCDistanceId;
	}
	public Long getShopCCStartId() {
		return shopCCStartId;
	}
	public void setShopCCStartId(Long shopCCStartId) {
		this.shopCCStartId = shopCCStartId;
	}
	public Long getShopCCEndId() {
		return shopCCEndId;
	}
	public void setShopCCEndId(Long shopCCEndId) {
		this.shopCCEndId = shopCCEndId;
	}
	public List<ShopParamVODayStart> getLstDayStart() {
		return lstDayStart;
	}
	public void setLstDayStart(List<ShopParamVODayStart> lstDayStart) {
		this.lstDayStart = lstDayStart;
	}
	public List<Long> getLstDayStartDelete() {
		return lstDayStartDelete;
	}
	public void setLstDayStartDelete(List<Long> lstDayStartDelete) {
		this.lstDayStartDelete = lstDayStartDelete;
	}
	public List<ShopParamVODayEnd> getLstDayEnd() {
		return lstDayEnd;
	}
	public void setLstDayEnd(List<ShopParamVODayEnd> lstDayEnd) {
		this.lstDayEnd = lstDayEnd;
	}
	public List<Long> getLstDayEndDelete() {
		return lstDayEndDelete;
	}
	public void setLstDayEndDelete(List<Long> lstDayEndDelete) {
		this.lstDayEndDelete = lstDayEndDelete;
	}
	public List<ShopParamVOProduct> getLstProductShopMapVO() {
		return lstProductShopMapVO;
	}
	public void setLstProductShopMapVO(List<ShopParamVOProduct> lstProductShopMapVO) {
		this.lstProductShopMapVO = lstProductShopMapVO;
	}
}

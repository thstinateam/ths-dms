/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.MediaItem;

/**
 * FeedbackVOSave
 * @author vuongmq
 * @since 23/11/2015 
 */
public class FeedbackVOSave implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4488396665144328036L;
	private String typeParam;
	private Date remindDate;
	private String content;
	private List<FeedbackVOTmp> lstStaffId;
	private List<MediaItem> lstMediaItem;
	
	public final String getTypeParam() {
		return typeParam;
	}
	public final void setTypeParam(String typeParam) {
		this.typeParam = typeParam;
	}
	public final Date getRemindDate() {
		return remindDate;
	}
	public final void setRemindDate(Date remindDate) {
		this.remindDate = remindDate;
	}
	public final String getContent() {
		return content;
	}
	public final void setContent(String content) {
		this.content = content;
	}
	public final List<FeedbackVOTmp> getLstStaffId() {
		return lstStaffId;
	}
	public final void setLstStaffId(List<FeedbackVOTmp> lstStaffId) {
		this.lstStaffId = lstStaffId;
	}
	public final List<MediaItem> getLstMediaItem() {
		return lstMediaItem;
	}
	public final void setLstMediaItem(List<MediaItem> lstMediaItem) {
		this.lstMediaItem = lstMediaItem;
	}
}

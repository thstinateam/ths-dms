package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TreeVO<T> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1878562251044796712L;

	private T object;
	
	private List<TreeVO<T>> listChildren;

	public TreeVO() {
		listChildren = new ArrayList<TreeVO<T>>();
	}
	
	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

	public List<TreeVO<T>> getListChildren() {
		return listChildren;
	}

	public void setListChildren(List<TreeVO<T>> listChildren) {
		this.listChildren = listChildren;
	}

	
}

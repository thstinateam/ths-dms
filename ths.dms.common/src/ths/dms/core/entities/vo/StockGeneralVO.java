package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Lop VO chung cho cac chuc nang lien quan den Stock (Warehouse) de lam tham so
 * 
 * @author hunglm16
 * @return VO by Entities
 * @description Co the bo sung them cac tham so, nhung yeu cau Han che bo sung tham so dang Object
 */
public class StockGeneralVO implements Serializable {

	private static final long serialVersionUID = 1L;
	//TODO Tham so co hieu dung de so sanh
	public static final int STOCKTRANS = 1;
	public static final int POVNM = 2;

	private Long priceId;
	private Long productId;
	private Long warehouseId;
	private Long stockTotalId;
	private Long stockIssueId;
	private Long isIdDetail;
	private Long isIdAttr;

	private Integer vat;
	private Integer seq;
	private Integer checkLot;
	private Integer convfact;
	private Integer approved;
	private Integer quantity;
	private Integer availableQuantity;
	
	private BigDecimal price;
	private BigDecimal priceNotVat;
	private BigDecimal grossWeight;
	private BigDecimal totalAmount;
	
	private String productCode;
	private String productName;
	private String uom1;
	private String cat;
	private String subCat;
	private String warehouseCode;
	private String warehouseName;
	private String stockTransDateStr;
	private String stockTransCode;
	private String stockIssueNumber;
	private String issueType;
	private String about;
	private String staffText;
	private String contractNumber;
	private String carNumber;
	private String warehouseOutPutText;
	private String warehouseInputText;
	
	public Boolean isCheck;
	
	public Long getIsIdAttr() {
		return isIdAttr;
	}

	public void setIsIdAttr(Long isIdAttr) {
		this.isIdAttr = isIdAttr;
	}

	public Boolean getIsCheck() {
		if (isCheck == null) {
			return false;
		}
		return isCheck;
	}

	public void setIsCheck(Boolean isCheck) {
		this.isCheck = isCheck;
	}

	public Long getIsIdDetail() {
		return isIdDetail;
	}

	public void setIsIdDetail(Long isIdDetail) {
		this.isIdDetail = isIdDetail;
	}

	public Long getStockIssueId() {
		return stockIssueId;
	}

	public void setStockIssueId(Long stockIssueId) {
		this.stockIssueId = stockIssueId;
	}

	public String getStockTransDateStr() {
		return stockTransDateStr;
	}

	public void setStockTransDateStr(String stockTransDateStr) {
		this.stockTransDateStr = stockTransDateStr;
	}

	public String getStockTransCode() {
		return stockTransCode;
	}

	public void setStockTransCode(String stockTransCode) {
		this.stockTransCode = stockTransCode;
	}

	public String getStockIssueNumber() {
		return stockIssueNumber;
	}

	public void setStockIssueNumber(String stockIssueNumber) {
		this.stockIssueNumber = stockIssueNumber;
	}

	public String getIssueType() {
		return issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getStaffText() {
		return staffText;
	}

	public void setStaffText(String staffText) {
		this.staffText = staffText;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getWarehouseOutPutText() {
		return warehouseOutPutText;
	}

	public void setWarehouseOutPutText(String warehouseOutPutText) {
		this.warehouseOutPutText = warehouseOutPutText;
	}

	public String getWarehouseInputText() {
		return warehouseInputText;
	}

	public void setWarehouseInputText(String warehouseInputText) {
		this.warehouseInputText = warehouseInputText;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getApproved() {
		return approved;
	}

	public void setApproved(Integer approved) {
		this.approved = approved;
	}

	public BigDecimal getGrossWeight() {
		return grossWeight == null ? BigDecimal.valueOf(0) : grossWeight;
	}

	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Long getPriceId() {
		return priceId;
	}

	public void setPriceId(Long priceId) {
		this.priceId = priceId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getStockTotalId() {
		return stockTotalId;
	}

	public void setStockTotalId(Long stockTotalId) {
		this.stockTotalId = stockTotalId;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Integer getVat() {
		return vat;
	}

	public void setVat(Integer vat) {
		this.vat = vat;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Integer getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		if (price == null) {
			return BigDecimal.ZERO;
		}
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getPriceNotVat() {
		return priceNotVat;
	}

	public void setPriceNotVat(BigDecimal priceNotVat) {
		this.priceNotVat = priceNotVat;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUom1() {
		return uom1;
	}

	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getSubCat() {
		return subCat;
	}

	public void setSubCat(String subCat) {
		this.subCat = subCat;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	//	public StockGeneralVO clone() {
	//		StockGeneralVO temp = new StockGeneralVO();
	//		temp.priceId = priceId;
	//		temp.productId = productId;
	//		temp.stockTotalId=stockTotalId;
	//		temp.warehouseId=warehouseId;
	//		temp.seq=seq;
	//		temp.vat=vat;
	//		temp.checkLot=checkLot;
	//		temp.convfact=convfact;
	//		temp.availableQuantity=availableQuantity;
	//		temp.quantity=quantity;
	//		temp.price=price;
	//		temp.priceNotVat=priceNotVat;
	//		temp.productCode=productCode;
	//		temp.productName=productName;
	//		temp.uom1=uom1;
	//		temp.cat=cat;
	//		temp.subCat=subCat;
	//		temp.warehouseCode=warehouseCode;
	//		temp.warehouseName=warehouseName;
	//		return temp;
	//	}
}

package ths.dms.core.entities.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.StockTransLot;


// TODO: Auto-generated Javadoc
/**
 * The Class StockTransDetailVO.
 */
public class StockTransDetailVO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The stock trans detail. */
	private StockTransDetail stockTransDetail;
	
	/** The stock trans lots. */
	private List<StockTransLot> stockTransLots = new ArrayList<StockTransLot>();
	
	/** The lot. */
	private String lot;
	
	/** The list handle lot. */
	private List<String> listHandleLot;
	
	/** The list handle quantity. */
	private List<Integer> listHandleQuantity;
	
	
	
	
	/**
	 * Gets the stock trans lots.
	 *
	 * @return the stock trans lots
	 */
	public List<StockTransLot> getStockTransLots() {
		return stockTransLots;
	}

	/**
	 * Sets the stock trans lots.
	 *
	 * @param stockTransLots the new stock trans lots
	 */
	public void setStockTransLots(List<StockTransLot> stockTransLots) {
		this.stockTransLots = stockTransLots;
	}

	/**
	 * Gets the stock trans detail.
	 *
	 * @return the stock trans detail
	 */
	public StockTransDetail getStockTransDetail() {
		return stockTransDetail;
	}
	
	/**
	 * Sets the stock trans detail.
	 *
	 * @param stockTransDetail the new stock trans detail
	 */
	public void setStockTransDetail(StockTransDetail stockTransDetail) {
		this.stockTransDetail = stockTransDetail;
	}
	
	/**
	 * Gets the lot.
	 *
	 * @return the lot
	 */
	public String getLot() {
		return lot;
	}
	
	/**
	 * Sets the lot.
	 *
	 * @param lot the new lot
	 */
	public void setLot(String lot) {
		this.lot = lot;
	}
	
	/**
	 * Gets the list handle lot.
	 *
	 * @return the listHandleLot
	 */
	public List<String> getListHandleLot() {
		return listHandleLot;
	}
	
	/**
	 * Sets the list handle lot.
	 *
	 * @param listHandleLot the listHandleLot to set
	 */
	public void setListHandleLot(List<String> listHandleLot) {
		this.listHandleLot = listHandleLot;
	}
	
	/**
	 * Gets the list handle quantity.
	 *
	 * @return the listHandleQuantity
	 */
	public List<Integer> getListHandleQuantity() {
		return listHandleQuantity;
	}
	
	/**
	 * Sets the list handle quantity.
	 *
	 * @param listHandleQuantity the listHandleQuantity to set
	 */
	public void setListHandleQuantity(List<Integer> listHandleQuantity) {
		this.listHandleQuantity = listHandleQuantity;
	}
	
	
}
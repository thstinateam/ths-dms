/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.common.utils.CDateUtil;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;

/**
 * entity phieu thanh toan sua chua thiet bi
 * @author tuannd20
 */
/**
 * entity phieu thanh toan sua chua thiet bi; cap nhat 1 phieu thanh toan nhieu phieu sua chua
 * @author vuongmq
 * @date 22/06/2015
 */
@Entity
@Table(name = "EQUIP_REPAIR_PAY_FORM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_REPAIR_PAY_FORM_SEQ", allocationSize = 1)
public class EquipRepairPayForm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_REPAIR_PAY_FORM_ID")
	private Long id;
	
	/*@ManyToOne(targetEntity = EquipRepairForm.class)
	@JoinColumn(name = "EQUIP_REPAIR_FORM_ID", referencedColumnName = "EQUIP_REPAIR_FORM_ID")
	private EquipRepairForm equipRepairForm;*/
	
	@ManyToOne(targetEntity = EquipPeriod.class)
	@JoinColumn(name = "EQUIP_PERIOD_ID", referencedColumnName = "EQUIP_PERIOD_ID")
	private EquipPeriod equipPeriod;
	
	@Basic
	@Column(name = "CODE", length = 100)
	private String code;
	
	@Basic
	@Column(name = "PAY_DATE", columnDefinition = "timestamp(9)")
	@Temporal(TemporalType.TIMESTAMP)
	private Date paymentDate;
	
	/*//Tien vat tu
	@Basic
	@Column(name = "MATERIAL_AMOUNT", length = 22)
	private BigDecimal materialAmount;
	
	//Tien nhan cong
	@Basic
	@Column(name = "WORKER_AMOUNT", length = 22)
	private BigDecimal workerAmount;*/
	
	//Tong tien
	@Basic
	@Column(name = "TOTAL_AMOUNT", length = 22)
	private BigDecimal totalAmount;
	
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;
	
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	//Ngay duyet bien ban thanh toan
	@Basic
	@Column(name = "APPROVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approveDate;
	
	// Trang thai duyet cua thanh toan
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = { @Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.StatusRecordsEquip"), @Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private StatusRecordsEquip status = StatusRecordsEquip.DRAFT;
	
	//id NPP; dung de lay danh sach cua user phan quyen tu shop root tro xuong cua phieu thanh toan
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	// tam ngay lap la string
	@Transient
	private String paymentDateStr;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipPeriod getEquipPeriod() {
		return equipPeriod;
	}

	public void setEquipPeriod(EquipPeriod equipPeriod) {
		this.equipPeriod = equipPeriod;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public StatusRecordsEquip getStatus() {
		return status;
	}

	public void setStatus(StatusRecordsEquip status) {
		this.status = status;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public String getPaymentDateStr() {
		if (paymentDate != null) {
			paymentDateStr = CDateUtil.toDateString(paymentDate, CDateUtil.DATE_FORMAT_STR);
		}
		return paymentDateStr;
	}

	public void setPaymentDateStr(String paymentDateStr) {
		this.paymentDateStr = paymentDateStr;
	}
}

package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 * 
 * @author thachnn
 * @since 13/9/2013
 * 
 */

@Entity
@Table(name = "DISPLAY_PROGRAM_VNM")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "DISPLAY_PROGRAM_VNM_SEQ", allocationSize = 1)
public class StDisplayProgramVNM implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//code
	@Basic
	@Column(name = "DISPLAY_PROGRAM_CODE", length = 50)
	private String displayProgramCode;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "DISPLAY_PROGRAM_VNM_ID")
	private Long id;

	//name
	@Basic
	@Column(name = "DISPLAY_PROGRAM_NAME", length = 300)
	private String displayProgramName;
	
	@Basic
	@Column(name = "TYPE", length = 5)
	private Integer programeType;
	

	//tu ngay
	@Basic
	@Column(name = "FROM_DATE", length = 7)
	private Date fromDate;
	

	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.WAITING ;
	
	//den ngay
	@Basic
	@Column(name = "TO_DATE", length = 7)
	private Date toDate;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;


	public StDisplayProgramVNM clone() {
		StDisplayProgramVNM ret = new StDisplayProgramVNM();
		ret.setCreateDate(createDate);
		ret.setCreateUser(createUser);
		ret.setDisplayProgramCode(displayProgramCode);
		ret.setDisplayProgramName(displayProgramName);
		
		ret.setFromDate(fromDate);
		ret.setId(id);
		ret.setStatus(status);
		ret.setUpdateDate(updateDate);
		ret.setUpdateUser(updateUser);;
		return ret;
	}
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getDisplayProgramCode() {
		return displayProgramCode;
	}

	public void setDisplayProgramCode(String displayProgramCode) {
		this.displayProgramCode = displayProgramCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDisplayProgramName() {
		return displayProgramName;
	}

	public void setDisplayProgramName(String displayProgramName) {
		this.displayProgramName = displayProgramName;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public Integer getProgrameType() {
		return programeType;
	}
	public void setProgrameType(Integer programeType) {
		this.programeType = programeType;
	}
	
}
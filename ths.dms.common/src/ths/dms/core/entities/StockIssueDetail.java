package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "STOCK_ISSUE_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "STOCK_ISSUE_DETAIL_SEQ", allocationSize = 1)
public class StockIssueDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//Gi�
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal price;

	//S?n ph?m
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//S? l�?ng nh?p
	@Basic
	@Column(name = "QUANTITY_IN", length = 22)
	private BigDecimal quantityIn;

	//S? l�?ng xu?t
	@Basic
	@Column(name = "QUANTITY_OUT", length = 22)
	private BigDecimal quantityOut;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "STOCK_ISSUE_DETAIL_ID")
	private Long id;

	@ManyToOne(targetEntity = StockIssue.class)
	@JoinColumn(name = "STOCK_ISSUE_ID", referencedColumnName = "STOCK_ISSUE_ID")
	private StockIssue stockIssue;

	//Quy c�ch
	@Basic
	@Column(name = "UOM", length = 200)
	private String uom;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public BigDecimal getQuantityIn() {
		return quantityIn;
	}

	public void setQuantityIn(BigDecimal quantityIn) {
		this.quantityIn = quantityIn;
	}

	public BigDecimal getQuantityOut() {
		return quantityOut;
	}

	public void setQuantityOut(BigDecimal quantityOut) {
		this.quantityOut = quantityOut;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public StockIssue getStockIssue() {
		return stockIssue;
	}

	public void setStockIssue(StockIssue stockIssue) {
		this.stockIssue = stockIssue;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
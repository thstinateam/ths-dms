package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.StockObjectType;

/**
* Auto generate entities - STOCK_TOTAL
* @author nhanlt6
* @since 15/8/2014
*/

@Entity
@Table(name = "STOCK_TOTAL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "STOCK_TOTAL_SEQ", allocationSize = 1)
public class StockTotal implements Serializable {

	private static final long serialVersionUID = 1L;
	//Ton kho dap ung
	@Basic
	@Column(name = "AVAILABLE_QUANTITY", length = 22)
	private Integer availableQuantity;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	//Mo ta
	@Basic
	@Column(name = "DESCR", length = 600)
	private String descr;

	//Id doi tuong ton kho
	@Basic
	@Column(name = "OBJECT_ID", length = 600)
	private Long objectId;

	//Loai doi tuong: 1 doi tuong trong shop, 2 doi tuong trong staff, 3 doi tuong trong customer ,4: kho tam cho vansale
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.StockObjectType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private StockObjectType objectType;

	//id san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//So luong ton kho
	@Basic
	@Column(name = "QUANTITY", length = 20)
	private Integer quantity;

	//ID TON kho, Auto number
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "STOCK_TOTAL_ID")
	private Long id;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	//id kho
	@ManyToOne(targetEntity = Warehouse.class)
	@JoinColumn(name = "WAREHOUSE_ID", referencedColumnName = "WAREHOUSE_ID")
	private Warehouse warehouse;
	
	//Tồn kho nhân viên dưới góc nhìn kế toán, nếu object_type <> 2 -> lấy giá trị null
	@Basic
	@Column(name = "APPROVED_QUANTITY", length = 20)
	private Integer approvedQuantity;
		
	//trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING ;
	
	/*@Basic
	@Column(name = "IS_NOT_MIGRATE", length = 8)
	private Integer isNotMigrate;	

	@Basic
	@Column(name = "IS_NOT_TRIGGER", length = 8)
	private Integer isNotTrigger;
	
	@Basic
	@Column(name = "OLD_ID", length = 20)
	private Long oldId;
	
	@Basic
	@Column(name = "VERSION_DATA", length = 10)
	private Integer versionData;*/
	
	//id NPP; vuongmq Oct 10,2014; them vao quan ly don DP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;
	
	@Transient
	private Integer seqWarehouse;
	
	
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	public Integer getSeqWarehouse() {
		return seqWarehouse;
	}

	public void setSeqWarehouse(Integer seqWarehouse) {
		this.seqWarehouse = seqWarehouse;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public Integer getApprovedQuantity() {
		return approvedQuantity;
	}

	public void setApprovedQuantity(Integer approvedQuantity) {
		this.approvedQuantity = approvedQuantity;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}


	public Integer getAvailableQuantity() {
		if (availableQuantity == null) {
			availableQuantity = 0;
		}
		return availableQuantity;
	}

	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public StockObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(StockObjectType objectType) {
		this.objectType = objectType;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
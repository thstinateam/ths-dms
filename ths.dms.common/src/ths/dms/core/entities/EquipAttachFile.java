package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_ATTACH_FILE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_ATTACH_FILE_SEQ", allocationSize = 1)
public class EquipAttachFile implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_ATTACH_FILE_ID")
	private Long id;
	
	//1. Phi?u giao nh?n, 2. Phi?u chuy?n kho, 3. Phi?u s?a ch?a, 4. Phi?u b�o m?t, 5. Phi?u thu h?i v� thanh l?, 6. Bi�n b?n thanh l? t�i s?n, 7. Bi�n b?n ki?m k�
	@Basic
	@Column(name = "OBJECT_TYPE")
	private Integer objectType;
	
	//ID c?a phi?u theo OBJECT_TYPE
	@Basic
	@Column(name = "OBJECT_ID")
	private Long objectId;
	
	@Basic
	@Column(name = "URL", length = 1000)
	private String url;
	
	@Basic
	@Column(name = "FILE_NAME", length = 600)
	private String fileName;

	
	//Ng�y t?o
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	
	//Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;

	//Ng�y c?p nh?t
	@Basic
	@Column(name = "LAT")
	private Float lat;
	
	//Ng�y c?p nh?t
	@Basic
	@Column(name = "LNG")
	private Float lng;
	
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;
	
	@Basic
	@Column(name = "THUMB_URL", length = 1000)
	private String thumbUrl;
	
	@Basic
	@Column(name="RESULT")
	private Integer result;
	
	@Basic
	@Column(name="IS_INSPECTED")
	private Integer isInspected;
	
	@Basic
	@Column(name="NUM_PRODUCT")
	private Integer numberProduct;
	
	@Basic
	@Column(name="POSM")
	private String POSM;
	
	@Basic
	@Column(name="NOTE")
	private String note;
	
	@Basic
	@Column(name = "INSPECTING_USER", length = 200)
	private String inspectingUser;
	
	@Basic
	@Column(name = "INSPECTING_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date inspectingDate;
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public Integer getObjectType() {
		return objectType;
	}
	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public void setObjectType(Integer objectType) {
		this.objectType = objectType;
	}

	public String getThumbUrl() {
		return thumbUrl;
	}

	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLng() {
		return lng;
	}

	public void setLng(Float lng) {
		this.lng = lng;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public Integer getIsInspected() {
		return isInspected;
	}

	public void setIsInspected(Integer isInspected) {
		this.isInspected = isInspected;
	}

	public Integer getNumberProduct() {
		return numberProduct;
	}

	public void setNumberProduct(Integer numberProduct) {
		this.numberProduct = numberProduct;
	}

	public String getPOSM() {
		return POSM;
	}

	public void setPOSM(String pOSM) {
		POSM = pOSM;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getInspectingUser() {
		return inspectingUser;
	}

	public void setInspectingUser(String inspectingUser) {
		this.inspectingUser = inspectingUser;
	}

	public Date getInspectingDate() {
		return inspectingDate;
	}

	public void setInspectingDate(Date inspectingDate) {
		this.inspectingDate = inspectingDate;
	}
	
}
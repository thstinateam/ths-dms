package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 * Auto generate entities - HCM standard
 * 
 * @author hungnm16
 * @since December 14,2014
 * @description entities thuoc kenh thiet bi
 */
@Entity
@Table(name = "EQUIP_ROLE_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_ROLE_DETAIL_SEQ", allocationSize = 1)
public class EquipRoleDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	// Kh�a ch�nh
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_ROLE_DETAIL_ID")
	private Long id;
	
	//kho don vi cap duoi
	@Basic
	@Column(name = "IS_UNDER", length = 3)
	private Integer isUnder;

	// id don vi: shop
	@ManyToOne(targetEntity = EquipRole.class)
	@JoinColumn(name = "EQUIP_ROLE_ID", referencedColumnName = "EQUIP_ROLE_ID")
	private EquipRole equipRole;
	
	// id kho: equip stock
	@ManyToOne(targetEntity = EquipStock.class)
	@JoinColumn(name = "EQUIP_STOCK_ID", referencedColumnName = "EQUIP_STOCK_ID")
	private EquipStock equipStock;
	
	// Ng�y t?o
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	// Ng�?i t?o
	@Basic
	@Column(name = "CREATE_USER", length = 50)
	private String createUser;

	// Ng�y c?p nh?t
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp", insertable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	// Ng�?i c?p nh?t
	@Basic
	@Column(name = "UPDATE_USER", length = 50)
	private String updateUser;

	public Long getId() {
		return id;
	}

	public EquipRole getEquipRole() {
		return equipRole;
	}

	public EquipStock getEquipStock() {
		return equipStock;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEquipRole(EquipRole equipRole) {
		this.equipRole = equipRole;
	}

	public void setEquipStock(EquipStock equipStock) {
		this.equipStock = equipStock;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getIsUnder() {
		return isUnder;
	}

	public void setIsUnder(Integer isUnder) {
		this.isUnder = isUnder;
	}

	
}
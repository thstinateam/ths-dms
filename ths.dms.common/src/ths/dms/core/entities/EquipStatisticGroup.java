package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.EquipObjectType;
/**
* Auto generate entities - HCM standard
* 
* @author phut
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_STATISTIC_GROUP")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_STATISTIC_GROUP_SEQ", allocationSize = 1)
public class EquipStatisticGroup implements Serializable {

	private static final long serialVersionUID = 1L;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//ID nh�m thi?t b? tham gia ki?m k�
	@Column(name = "OBJECT_ID")
	private Long objectId;

	//ID nh�m thi?t b? ki?m k�
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_STATISTIC_GROUP_ID")
	private Long id;

	//ID bi�n b?n ki?m k�
	@ManyToOne(targetEntity = EquipStatisticRecord.class)
	@JoinColumn(name = "EQUIP_STATISTIC_RECORD_ID", referencedColumnName = "EQUIP_STATISTIC_RECORD_ID")
	private EquipStatisticRecord equipStatisticRecord;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;
	
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status = ActiveType.RUNNING;
	
	@Basic
	@Column(name = "OBJECT_TYPE", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.EquipObjectType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private EquipObjectType objectType;
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipStatisticRecord getEquipStatisticRecord() {
		return equipStatisticRecord;
	}

	public void setEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord) {
		this.equipStatisticRecord = equipStatisticRecord;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public ActiveType getStatus() {
		return status;
	}

	public void setStatus(ActiveType status) {
		this.status = status;
	}

	public EquipObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(EquipObjectType objectType) {
		this.objectType = objectType;
	}
}
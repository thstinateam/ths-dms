package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.PoProductType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "PO_VNM_DETAIL")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "PO_VNM_DETAIL_SEQ", allocationSize = 1)
public class PoVnmDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	//tong tien
	@Basic
	@Column(name = "AMOUNT", length = 22)
	private BigDecimal amount;

	//tong tien da tra
	@Basic
	@Column(name = "AMOUNT_PAY", length = 22)
	private BigDecimal amountPay = new BigDecimal(0);

	//ngay tao po
	@Basic
	@Column(name = "PO_VNM_DATE", length = 7)
	private Date poVnmDate;

	//id bang
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "PO_VNM_DETAIL_ID")
	private Long id;

	//id don hang
	@ManyToOne(targetEntity = PoVnm.class)
	@JoinColumn(name = "PO_VNM_ID", referencedColumnName = "PO_VNM_ID")
	private PoVnm poVnm;

	//gia
	@Basic
	@Column(name = "PRICE", length = 22)
	private BigDecimal priceValue;

	//id gia
	@ManyToOne(targetEntity = Price.class)
	@JoinColumn(name = "PRICE_ID", referencedColumnName = "PRICE_ID")
	private Price price;
	
	//id warehouse
	@ManyToOne(targetEntity = Warehouse.class)
	@JoinColumn(name = "WAREHOUSE_ID", referencedColumnName = "WAREHOUSE_ID")
	private Warehouse warehouse;

	//id san pham
	@ManyToOne(targetEntity = Product.class)
	@JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
	private Product product;

	//so luong
	@Basic
	@Column(name = "QUANTITY", length = 22)
	private Integer quantity;

	//so luong da tra
	@Basic
	@Column(name = "QUANTITY_PAY", length = 22)
	private Integer quantityPay = 0;
	
	@Basic
	@Column(name = "QUANTITY_RECEIVED", length = 22)
	private Integer quantityReceived;
	
	@Basic
	@Column(name = "AMOUNT_RECEIVED")
	private BigDecimal amountReceived;

	// 10/08/2015; dong bo ve co loai san pham
	//1: hang ban, 2: hang KM, 3: POSM
	@Basic
	@Column(name = "PRODUCT_TYPE", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.PoProductType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private PoProductType productType;
	
	// so luong thung
	@Basic
	@Column(name = "QUANTITY_PACKAGE", length = 20)
	private Integer quantityPackage;
	
	// so luong le
	@Basic
	@Column(name = "QUANTITY_RETAIL", length = 20)
	private Integer quantityRetail;
	
	// gia thung
	@Basic
	@Column(name = "PRICE_PACKAGE", length = 26)
	private BigDecimal pricePackage;
	
	//gia tri quy doi tu Thung -> le
	@Basic
	@Column(name = "CONVFACT", length = 20)
	private Integer convfact;
	
	@Basic
	@Column(name = "NOTE", length = 200)
	private String note;
	
	//po_line_number
	@Basic
	@Column(name = "PO_LINE_NUMBER", length = 20)
	private Integer poLineNumber;
	
	//so_line_number
	@Basic
	@Column(name = "SO_LINE_NUMBER", length = 20)
	private Integer soLineNumber;
	
	public Integer getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public BigDecimal getAmountReceived() {
		return amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.valueOf(0) : amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getAmountPay() {
		return amountPay == null ? BigDecimal.valueOf(0) : amountPay;
	}

	public void setAmountPay(BigDecimal amountPay) {
		this.amountPay = amountPay;
	}

	public Date getPoVnmDate() {
		return poVnmDate;
	}

	public void setPoVnmDate(Date poVnmDate) {
		this.poVnmDate = poVnmDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PoVnm getPoVnm() {
		return poVnm;
	}

	public void setPoVnm(PoVnm poVnm) {
		this.poVnm = poVnm;
	}

	public BigDecimal getPriceValue() {
		return priceValue == null ? BigDecimal.valueOf(0) : priceValue;
	}

	public void setPriceValue(BigDecimal priceValue) {
		this.priceValue = priceValue;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		if (quantity == null) {
			quantity = 0;
		}
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantityPay() {
		if (quantityPay == null) {
			quantityPay = 0;
		}
		return quantityPay;
	}

	public void setQuantityPay(Integer quantityPay) {
		this.quantityPay = quantityPay;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public PoProductType getProductType() {
		return productType;
	}

	public void setProductType(PoProductType productType) {
		this.productType = productType;
	}

	public Integer getQuantityPackage() {
		return quantityPackage;
	}

	public void setQuantityPackage(Integer quantityPackage) {
		this.quantityPackage = quantityPackage;
	}

	public Integer getQuantityRetail() {
		return quantityRetail;
	}

	public void setQuantityRetail(Integer quantityRetail) {
		this.quantityRetail = quantityRetail;
	}

	public BigDecimal getPricePackage() {
		return pricePackage;
	}

	public void setPricePackage(BigDecimal pricePackage) {
		this.pricePackage = pricePackage;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getPoLineNumber() {
		return poLineNumber;
	}

	public void setPoLineNumber(Integer poLineNumber) {
		this.poLineNumber = poLineNumber;
	}

	public Integer getSoLineNumber() {
		return soLineNumber;
	}

	public void setSoLineNumber(Integer soLineNumber) {
		this.soLineNumber = soLineNumber;
	}
	
	
}
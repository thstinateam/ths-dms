package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.ActiveType;

/**
 *
 * Auto generate entities - HaNoi standard
 * @author hungnm35
 * @since 16/7/2012
 * 
 */

@Entity
@Table(name = "ROUTING")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "ROUTING_SEQ", allocationSize = 1)
public class Routing implements Serializable {

	private static final long serialVersionUID = 1L;
	//ngay tao
	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	//nguoi tao
	@Basic
	@Column(name = "CREATE_USER", length = 40)
	private String createUser;

	//ma tuyen
	@Basic
	@Column(name = "ROUTING_CODE", length = 100)
	private String routingCode;

	//id bang tuyen
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "ROUTING_ID")
	private Long id;

	//ten tuyen
	@Basic
	@Column(name = "ROUTING_NAME", length = 750)
	private String routingName;

	//id NPP
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "SHOP_ID", referencedColumnName = "SHOP_ID")
	private Shop shop;

	//0: ngung hoat dong, 1:hoat dong
	// trang thai
	@Basic
	@Column(name = "STATUS", columnDefinition = "integer", nullable = false)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.ActiveType"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private ActiveType status;

	//ngay update
	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	//nguoi cap nhat
	@Basic
	@Column(name = "UPDATE_USER", length = 40)
	private String updateUser;

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the createUser
	 */
	public String getCreateUser() {
		return createUser;
	}

	/**
	 * @param createUser the createUser to set
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	/**
	 * @return the routingCode
	 */
	public String getRoutingCode() {
		return routingCode;
	}

	/**
	 * @param routingCode the routingCode to set
	 */
	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the routingName
	 */
	public String getRoutingName() {
		return routingName;
	}

	/**
	 * @param routingName the routingName to set
	 */
	public void setRoutingName(String routingName) {
		this.routingName = routingName;
	}

	/**
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * @param shop the shop to set
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * @return the status
	 */
	public ActiveType getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(ActiveType status) {
		this.status = status;
	}

	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the updateUser
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * @param updateUser the updateUser to set
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
}
package ths.dms.core.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
* Auto generate entities - HCM standard
* 
* @author hungnm16
* @since December 14,2014
* @description entities thuoc kenh thiet bi
*/
@Entity
@Table(name = "EQUIP_DELIVERY_RECORD")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name = "SEQ_STORE", sequenceName = "EQUIP_DELIVERY_RECORD_SEQ", allocationSize = 1)
public class EquipDeliveryRecord implements Serializable {

	private static final long serialVersionUID = 1L;

	//m? bi�n b?n
	@Basic
	@Column(name = "CODE", length = 400)
	private String code;

	//m? H?p �?ng
	@Basic
	@Column(name = "CONTRACT_NUMBER", length = 200)
	private String contractNumber;

	@Basic
	@Column(name = "CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
	@Column(name = "CREATE_FORM_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createFormDate;
	
	@Basic
	@Column(name = "CREATE_USER", length = 200)
	private String createUser;

	//m? KH
	@ManyToOne(targetEntity = Customer.class)
	@JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID")
	private Customer customer;

	//tr?ng th�i giao nh?n
	@Basic
	@Column(name = "DELIVERY_STATUS", length = 22)
	private Integer deliveryStatus;

	//ID bi�n b?n
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_STORE")
	@Column(name = "EQUIP_DELIVERY_RECORD_ID")
	private Long id;

	//K? qu?n l?
	@ManyToOne(targetEntity = EquipPeriod.class)
	@JoinColumn(name = "EQUIP_PERIOD_ID", referencedColumnName = "EQUIP_PERIOD_ID")
	private EquipPeriod equipPeriod;
	
//	@Basic
//	@Column(name = "FROM_STOCK_ID")
//	private Long fromStockId;
//
//	//Lo?i kho xu?t
//	@Basic
//	@Column(name = "FROM_STOCK_TYPE", length = 22)
//	private Integer fromStockType;

	//tr?ng th�i bi�n b?n
	@Basic
	@Column(name = "RECORD_STATUS", length = 22)
	private Integer recordStatus;

	//m? NV
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	@Basic
	@Column(name = "UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	@Basic
	@Column(name = "UPDATE_USER", length = 200)
	private String updateUser;
	
	@Basic
	@Column(name = "CONTRACT_CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date contractCreateDate;
	
	//ID biên bản đề nghị mượn thiết bị
	@ManyToOne(targetEntity = EquipLend.class)
	@JoinColumn(name = "EQUIP_LEND_ID", referencedColumnName = "EQUIP_LEND_ID")
	private EquipLend equipLend;
	
	//Đơn vị bên cho mượn
	@Basic
	@Column(name = "FROM_OBJECT_NAME", length = 50)
	private String fromObjectName;
	
	//Địa chỉ bên cho mượn
	@Basic
	@Column(name = "FROM_OBJECT_ADDRESS", length = 250)
	private String fromObjectAddress;
	
	//MST bên cho mượn
	@Basic
	@Column(name = "FROM_OBJECT_TAX", length = 20)
	private String fromObjectTax;
	
	//ĐT bên cho mượn
	@Basic
	@Column(name = "FROM_OBJECT_PHONE", length = 30)
	private String fromObjectPhone;
	
	//Đại diện bên cho mượn
	@Basic
	@Column(name = "FROM_REPRESENTATIVE", length = 250)
	private String fromRepresentative;
	
	//Chức vụ bên mượn
	@Basic
	@Column(name = "FROM_POSITION", length = 250)
	private String fromPosition;
	
	//Giấy ủy quyền bên cho mượn
	@Basic
	@Column(name = "FROM_PAGE", length = 250)
	private String fromPage;
	
	//Nơi cấp bên cho mượn
	@Basic
	@Column(name = "FROM_PAGE_PLACE", length = 250)
	private String fromPagePlace;
	
	//Ngày cấp bên cho mượn
	@Basic
	@Column(name = "FROM_PAGE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fromPageDate;
	
	//Ngày cấp bên cho mượn
	@Basic
	@Column(name = "APPROVE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approveDate;
	
	
	//Đơn vị mượn
	@ManyToOne(targetEntity = Shop.class)
	@JoinColumn(name = "TO_OBJECT_ID", referencedColumnName = "SHOP_ID")
	private Shop toShop;
//	@Basic
//	@Column(name = "TO_OBJECT_ID" , length = 20)
//	private Long toObjectId;
	
	//Vị trí đặt tủ
	@Basic
	@Column(name = "TO_OBJECT_ADDRES", length = 250)
	private String toObjectAddress;
	
	//Người đại diện bên mượn
	@Basic
	@Column(name = "TO_REPRESENTATIVE", length = 250)
	private String toRepresentative;
	
	//Chức vụ bên mươn
	@Basic
	@Column(name = "TO_POSITION", length = 250)
	private String toPosition;
	
	//Tủ mát
	@Basic
	@Column(name = "FREEZER", length = 22)
	private BigDecimal freezer;
	
	//Tủ đông
	@Basic
	@Column(name = "REFRIGERATOR", length = 22)
	private BigDecimal refrigerator;
	
	//Số CMND bên mượn
	@Basic
	@Column(name = "TO_IDNO", length = 20)
	private String toIdNO;
	
	//Nơi cấp CMND bên mượn
	@Basic
	@Column(name = "TO_IDNO_PLACE", length = 250)
	private String toIdNOPlace;
	
	//Ngày cấp CMND bên mượn
	@Basic
	@Column(name = "TO_IDNO_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date toIdNODate;
	
	//Người in
	@Basic
	@Column(name = "PRINT_USER", length = 50)
	private String printUser;

	//Ngày in
	@Basic
	@Column(name = "PRINT_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date printDate;
	
	//Tên đơn vị mượn
	@Basic
	@Column(name = "TO_OBJECT_NAME", length = 250)
	private String toObjectName;
	
	/**tamvnm: bo check shop*/
	//Đơn vị cho mượn
//	@ManyToOne(targetEntity = Shop.class)
//	@JoinColumn(name = "FROM_OBJECT_ID", referencedColumnName = "SHOP_ID")
//	private Shop fromShop;
		
	//So GP DKKD
	@Basic
	@Column(name = "TO_BUSINESS_LICENSE", length = 250)
	private String toBusinessLincense;
	
	//Ngay cap GP DKKD
	@Basic
	@Column(name = "TO_BUSINESS_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date toBusinessDate;
	
	//ĐT bên mượn
	@Basic
	@Column(name = "TO_PHONE", length = 100)
	private String toPhone;
	
	//Dia chi ben nhan
	@Basic
	@Column(name = "TO_ADDRESS", length = 250)
	private String toAddress;

	//So FAX ben cho muon
	@Basic
	@Column(name = "FROM_FAX", length = 30)
	private String fromFax;
	
	//Nơi cấp bên cho mượn
	@Basic
	@Column(name = "TO_BUSINESS_PLACE", length = 250)
	private String toBusinessPlace;
	
//	@Basic
//	@Column(name = "FROM_OBJECT_ID", length = 20)
//	private Long fromObjectId;

	@Basic
	@Column(name = "RELATION", length = 400)
	private String relation;
	
	@Basic
	@Column(name = "ADDRESS", length = 400)
	private String address;
	
	@Basic
	@Column(name = "STREET", length = 250)
	private String street;
	
	@Basic
	@Column(name = "WARD_NAME", length = 250)
	private String wardName;
	
	@Basic
	@Column(name = "DISTRICT_NAME", length = 250)
	private String districtName;
	
	@Basic
	@Column(name = "PROVINCE_NAME", length = 250)
	private String provinceName;
	
	@Basic
	@Column(name = "TO_PERMANENT_ADDRESS", length = 250)
	private String toPermanentAddress;
	
	@Basic
	@Column(name = "NOTE", length = 500)
	private String note;
	
	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Integer getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(Integer deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EquipPeriod getEquipPeriod() {
		return equipPeriod;
	}

	public void setEquipPeriod(EquipPeriod equipPeriod) {
		this.equipPeriod = equipPeriod;
	}

	public Integer getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getContractCreateDate() {
		return contractCreateDate;
	}

	public void setContractCreateDate(Date contractCreateDate) {
		this.contractCreateDate = contractCreateDate;
	}

	public EquipLend getEquipLend() {
		return equipLend;
	}

	public void setEquipLend(EquipLend equipLend) {
		this.equipLend = equipLend;
	}

	public String getFromObjectName() {
		return fromObjectName;
	}

	public void setFromObjectName(String fromObjectName) {
		this.fromObjectName = fromObjectName;
	}

	public String getFromObjectAddress() {
		return fromObjectAddress;
	}

	public void setFromObjectAddress(String fromObjectAddress) {
		this.fromObjectAddress = fromObjectAddress;
	}

	public String getFromObjectTax() {
		return fromObjectTax;
	}

	public void setFromObjectTax(String fromObjectTax) {
		this.fromObjectTax = fromObjectTax;
	}

	public String getFromObjectPhone() {
		return fromObjectPhone;
	}

	public void setFromObjectPhone(String fromObjectPhone) {
		this.fromObjectPhone = fromObjectPhone;
	}

	public String getFromRepresentative() {
		return fromRepresentative;
	}

	public void setFromRepresentative(String fromRepresentative) {
		this.fromRepresentative = fromRepresentative;
	}

	public String getFromPosition() {
		return fromPosition;
	}

	public void setFromPosition(String fromPosition) {
		this.fromPosition = fromPosition;
	}

	public String getFromPage() {
		return fromPage;
	}

	public void setFromPage(String fromPage) {
		this.fromPage = fromPage;
	}

	public String getFromPagePlace() {
		return fromPagePlace;
	}

	public void setFromPagePlace(String fromPagePlace) {
		this.fromPagePlace = fromPagePlace;
	}

	public Date getFromPageDate() {
		return fromPageDate;
	}

	public void setFromPageDate(Date fromPageDate) {
		this.fromPageDate = fromPageDate;
	}

	public String getToObjectAddress() {
		return toObjectAddress;
	}

	public void setToObjectAddress(String toObjectAddress) {
		this.toObjectAddress = toObjectAddress;
	}

	public String getToRepresentative() {
		return toRepresentative;
	}

	public void setToRepresentative(String toRepresentative) {
		this.toRepresentative = toRepresentative;
	}

	public String getToPosition() {
		return toPosition;
	}

	public void setToPosition(String toPosition) {
		this.toPosition = toPosition;
	}

	public BigDecimal getFreezer() {
		return freezer;
	}

	public void setFreezer(BigDecimal freezer) {
		this.freezer = freezer;
	}

	public BigDecimal getRefrigerator() {
		return refrigerator;
	}

	public void setRefrigerator(BigDecimal refrigerator) {
		this.refrigerator = refrigerator;
	}

	public String getToIdNO() {
		return toIdNO;
	}

	public void setToIdNO(String toIdNO) {
		this.toIdNO = toIdNO;
	}

	public String getToIdNOPlace() {
		return toIdNOPlace;
	}

	public void setToIdNOPlace(String toIdNOPlace) {
		this.toIdNOPlace = toIdNOPlace;
	}

	public Date getToIdNODate() {
		return toIdNODate;
	}

	public void setToIdNODate(Date toIdNODate) {
		this.toIdNODate = toIdNODate;
	}

	public String getPrintUser() {
		return printUser;
	}

	public void setPrintUser(String printUser) {
		this.printUser = printUser;
	}

	public Date getPrintDate() {
		return printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public String getToObjectName() {
		return toObjectName;
	}

	public void setToObjectName(String toObjectName) {
		this.toObjectName = toObjectName;
	}

	public Shop getToShop() {
		return toShop;
	}

	public void setToShop(Shop toShop) {
		this.toShop = toShop;
	}

//	public Shop getFromShop() {
//		return fromShop;
//	}
//
//	public void setFromShop(Shop fromShop) {
//		this.fromShop = fromShop;
//	}

	public String getToBusinessLincense() {
		return toBusinessLincense;
	}

	public void setToBusinessLincense(String toBusinessLincense) {
		this.toBusinessLincense = toBusinessLincense;
	}

	public Date getToBusinessDate() {
		return toBusinessDate;
	}

	public void setToBusinessDate(Date toBusinessDate) {
		this.toBusinessDate = toBusinessDate;
	}

	public String getToPhone() {
		return toPhone;
	}

	public void setToPhone(String toPhone) {
		this.toPhone = toPhone;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getFromFax() {
		return fromFax;
	}

	public void setFromFax(String fromFax) {
		this.fromFax = fromFax;
	}

	public String getToBusinessPlace() {
		return toBusinessPlace;
	}

	public void setToBusinessPlace(String toBusinessPlace) {
		this.toBusinessPlace = toBusinessPlace;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public Date getCreateFormDate() {
		return createFormDate;
	}

	public void setCreateFormDate(Date createFormDate) {
		this.createFormDate = createFormDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getWardName() {
		return wardName;
	}

	public void setWardName(String wardName) {
		this.wardName = wardName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getToPermanentAddress() {
		return toPermanentAddress;
	}

	public void setToPermanentAddress(String toPermanentAddress) {
		this.toPermanentAddress = toPermanentAddress;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
}
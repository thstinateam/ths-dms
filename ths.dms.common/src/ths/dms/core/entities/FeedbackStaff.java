/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import ths.dms.core.entities.enumtype.FeedbackStatus;
/**
 * Entities FEEDBACK_STAFF
 * @author vuongmq
 * @since 17/11/2015
 */
@Entity
@Table(name="FEEDBACK_STAFF")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SequenceGenerator(name="SEQ_STORE", sequenceName="FEEDBACK_STAFF_SEQ", allocationSize = 1)
public class FeedbackStaff implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQ_STORE")
	@Column(name="FEEDBACK_STAFF_ID")
	private Long id;
	
	//FEEDBACK_ID
	@ManyToOne(targetEntity = Feedback.class)
	@JoinColumn(name = "FEEDBACK_ID", referencedColumnName = "FEEDBACK_ID")
	private Feedback feedback;

	//staff_id
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "STAFF_ID", referencedColumnName = "STAFF_ID")
	private Staff staff;

	//RESULT
	@Basic
	@Column(name = "RESULT", columnDefinition = "integer", nullable = true)
	@Type(type = "ths.dms.core.entities.enumtype.GenericEnumUserType", parameters = {
			@Parameter(name = "enumClass", value = "ths.dms.core.entities.enumtype.FeedbackStatus"),
			@Parameter(name = "identifierMethod", value = "getValue"),
			@Parameter(name = "valueOfMethod", value = "parseValue") })
	private FeedbackStatus result;
	
	//DONE_DATE
	@Basic
	@Column(name="DONE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date doneDate;

	//NUM_RETURN
	@Basic
	@Column(name="NUM_RETURN", length = 2)
	private Integer numReturn;
	
	//TRAINING_PLAN_DETAIL_ID voi tung loai feedback
	@Basic
	@Column(name = "TRAINING_PLAN_DETAIL_ID", length = 20)
	private Long trainingPlanDetailId;
	
	//CREATE_USER_ID
	@ManyToOne(targetEntity = Staff.class)
	@JoinColumn(name = "CREATE_USER_ID", referencedColumnName = "STAFF_ID")
	private Staff createStaff;
	
	@Basic
	@Column(name="CREATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Basic
    @Column(name="UPDATE_USER", length = 100)
	private String updateUser;

	@Basic
	@Column(name="UPDATE_DATE", columnDefinition = "timestamp(9) default systimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Feedback getFeedback() {
		return feedback;
	}

	public void setFeedback(Feedback feedback) {
		this.feedback = feedback;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public FeedbackStatus getResult() {
		return result;
	}

	public void setResult(FeedbackStatus result) {
		this.result = result;
	}

	public Date getDoneDate() {
		return doneDate;
	}

	public void setDoneDate(Date doneDate) {
		this.doneDate = doneDate;
	}

	public final Integer getNumReturn() {
		return numReturn;
	}

	public final void setNumReturn(Integer numReturn) {
		this.numReturn = numReturn;
	}

	public Long getTrainingPlanDetailId() {
		return trainingPlanDetailId;
	}

	public void setTrainingPlanDetailId(Long trainingPlanDetailId) {
		this.trainingPlanDetailId = trainingPlanDetailId;
	}

	public Staff getCreateStaff() {
		return createStaff;
	}

	public void setCreateStaff(Staff createStaff) {
		this.createStaff = createStaff;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}

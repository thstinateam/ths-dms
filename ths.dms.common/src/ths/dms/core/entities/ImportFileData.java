package ths.dms.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="IMPORT_FILE_DATA")
public class ImportFileData implements Serializable {
	@Id
	@SequenceGenerator(name="IMPORT_FILE_DATA_GENERATOR", sequenceName="IMPORT_FILE_DATA_SEQ",allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IMPORT_FILE_DATA_GENERATOR")
	@Column(name="IMPORT_FILE_DATA_ID")
	private Long id;
		
	@Column(name="DATA_FILE_NAME")
	private String dataFileName;

	@Column(name="DATA_FILE_PATH")
	private String dataFilePath;
	
	@Column(name="MODULE_NAME")
	private String moduleName;
	
	@ManyToOne(targetEntity = Staff.class)	
	@JoinColumn(name="STAFF_ID", referencedColumnName="STAFF_ID")	
	private Staff staff;
	
	@Column(name="STATUS")
	private Integer status;
	
	@Column(name="ERROR_FILE_NAME")
	private String errorFileName;
	
	@Column(name="ERROR_FILE_PATH")
	private String errorFilePath;
	
	@Column(name="TOTAL_DATA_ROW")
	private Integer totalDataRow;
	
	@Column(name="TOTAL_DATA_ERROR")
	private Integer totalDataError;
	
	@Column(name="IMPORT_FLAG")
	private String importFlag;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="IMPORT_TIME")
	private Date importTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="COMPLETE_TIME")
	private Date completeTime;
	
	@Column(name="SERVER_IP")
	private String serverIP;
	
	@Column(name="FILE_TYPE")
	private String fileType;
	
	@Column(name="DATA_FILE_DOWNLOAD")
	private String dataFileDownload;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATE_DATE")
	private Date createDate;
	
	@Column(name="CREATE_USER")
	private String createUser;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DATE")
	private Date updateDate;

	@Column(name="UPDATE_USER")
	private String updateUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataFileName() {
		return dataFileName;
	}

	public void setDataFileName(String dataFileName) {
		this.dataFileName = dataFileName;
	}

	public String getDataFilePath() {
		return dataFilePath;
	}

	public void setDataFilePath(String dataFilePath) {
		this.dataFilePath = dataFilePath;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getErrorFileName() {
		return errorFileName;
	}

	public void setErrorFileName(String errorFileName) {
		this.errorFileName = errorFileName;
	}

	public String getErrorFilePath() {
		return errorFilePath;
	}

	public void setErrorFilePath(String errorFilePath) {
		this.errorFilePath = errorFilePath;
	}

	public Integer getTotalDataRow() {
		return totalDataRow;
	}

	public void setTotalDataRow(Integer totalDataRow) {
		this.totalDataRow = totalDataRow;
	}

	public Integer getTotalDataError() {
		return totalDataError;
	}

	public void setTotalDataError(Integer totalDataError) {
		this.totalDataError = totalDataError;
	}

	public String getImportFlag() {
		return importFlag;
	}

	public void setImportFlag(String importFlag) {
		this.importFlag = importFlag;
	}

	public Date getImportTime() {
		return importTime;
	}

	public void setImportTime(Date importTime) {
		this.importTime = importTime;
	}

	public Date getCompleteTime() {
		return completeTime;
	}

	public void setCompleteTime(Date completeTime) {
		this.completeTime = completeTime;
	}

	public String getDataFileDownload() {
		return dataFileDownload;
	}

	public void setDataFileDownload(String dataFileDownload) {
		this.dataFileDownload = dataFileDownload;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getServerIP() {
		return serverIP;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
}

package ths.dms.core.business;

import java.util.Date;
import java.util.List;
import java.util.Map;

import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.CycleCountMapProduct;
import ths.dms.core.entities.CycleCountResult;
import ths.dms.core.entities.enumtype.CycleCountFilter;
import ths.dms.core.entities.enumtype.CycleCountSearchFilter;
import ths.dms.core.entities.enumtype.CycleCountType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.MapProductFilter;
import ths.dms.core.entities.vo.CycleCountDiffVO;
import ths.dms.core.entities.vo.CycleCountMapProductVOEx;
import ths.dms.core.entities.vo.CycleCountResultVO;
import ths.dms.core.entities.vo.CycleCountSearchVO;
import ths.dms.core.entities.vo.CycleCountVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.MapProductVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;


/**
 * 
 * @author hungnm
 *
 */
public interface CycleCountMgr {

	CycleCount createCycleCount(CycleCount cycleCount) throws BusinessException;

	void updateCycleCount(CycleCount cycleCount) throws BusinessException;

	void deleteCycleCount(CycleCount cycleCount) throws BusinessException;

	CycleCount getCycleCountById(Long id) throws BusinessException;

	/*CycleCount getCycleCountByCode(String code) throws BusinessException;*/

	void deleteCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct)
			throws BusinessException;

	CycleCountMapProduct getCycleCountMapProductById(Long id)
			throws BusinessException;

	ObjectVO<CycleCountMapProduct> getListCycleCountMapProductByCycleCountId(
			KPaging<CycleCountMapProduct> kPaging, long cycleCountId)
			throws BusinessException;

	Boolean checkOngoingCycleCount(long shopId, long wareHouseId) throws BusinessException;

	CycleCountResult createCycleCountResult(CycleCountResult cycleCountResult)
			throws BusinessException;

	void deleteCycleCountResult(CycleCountResult cycleCountResult)
			throws BusinessException;

	CycleCountResult getCycleCountResultById(Long id)
			throws BusinessException;

	ObjectVO<CycleCountResult> getListCycleCountResult(
			KPaging<CycleCountResult> kPaging, Long cycleCountMapProductId)
			throws BusinessException;

	Boolean checkIfCycleCountMapProductExist(long cycleCountId,
			String productCode, Long id) throws BusinessException;

	Boolean checkIfCycleCountResultExist(long cycleCountMapId, String lotCode,
			Long id) throws BusinessException;

	ObjectVO<CycleCountMapProduct> getListCycleCountMapProduct(
			KPaging<CycleCountMapProduct> kPaging, Long cycleCountId,
			String productCode, String productName, Long catId, Long subCatId,
			Integer minQuantity, Integer maxQuantity,
			Boolean isOnlyGetDifferentQuantity) throws BusinessException;

	Integer getMaxStockCardNumber(Long cycleCountId) throws BusinessException;

	Boolean checkIfAnyProductCounted(Long cycleCountId)
			throws BusinessException;

	CycleCountMapProduct createCycleCountMapProduct(
			CycleCountMapProduct cycleCountMapProduct,
			Boolean isUpdateQuantityBfrCount, LogInfoVO log)
			throws BusinessException;

	void updateCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct,
			Boolean isUpdateQuantityBfrCount) throws BusinessException;

	void updateCycleCountResult(CycleCountResult cycleCountResult,
			Boolean isUpdateQuantityBfrCount) throws BusinessException;

	Integer getTotalStockCardNumber(Long cycleCountId) throws BusinessException;

	void updateListCycleCountResult(
			List<CycleCountResultVO> lstCycleCountResultVO)
			throws BusinessException;

	CycleCountMapProduct getCycleCountMapProduct(Long cycleCountId,
			Long productId) throws BusinessException;

	CycleCountResult getCycleCountResult(Long cycleCountId, Long productId,
			String lot) throws BusinessException;

	ObjectVO<CycleCountVO> getListCycleCountVO(KPaging<CycleCountVO> kPaging,
			String cycleCountVOCode, CycleCountType cycleCountVOStatus,
			String desc, Long shopId, Date fromDate, Date toDate,Date fromCreate,
			Date toCreate, Long wareHouseId, String strListShopId)
			throws BusinessException;


	ObjectVO<CycleCount> getListCycleCount(KPaging<CycleCount> kPaging,
			String cycleCountCode, CycleCountType cycleCountStatus,
			String desc, Long shopId, Date fromDate, Date toDate, Long wareHouseId, String strListShopId) throws BusinessException;
	
	ObjectVO<CycleCount> getListCycleCount(CycleCountSearchFilter filter) throws BusinessException;

	ObjectVO<CycleCount> getListCycleCountEx(KPaging<CycleCount> kPaging,
			String cycleCountCode, CycleCountType cycleCountStatus,
			String desc, Long shopId, Date fromDate, Date toDate,
			Boolean isCompleteOrOngoing, Long wareHouseId, String strListShopId) throws BusinessException;

	List<CycleCountResult> getListCycleCountResultEx(
			KPaging<CycleCountResult> kPaging, Long cycleCountMapProductId,
			Long ownerId, StockObjectType ownerType, Long productId)
			throws BusinessException;

	void updateListCycleCountMapProduct(List<CycleCountMapProduct> lstCycleCountMapProduct, LogInfoVO log)throws BusinessException;

	void createListCycleCountResult(List<CycleCountResult> lstCycleCountResult,LogInfoVO log) throws BusinessException;

	Boolean createListCycleCountMapProduct(CycleCount cycleCount,List<CycleCountMapProduct> lstCycleCountMapProduct,
			Boolean isCreatingAll, LogInfoVO log) throws BusinessException;

	CycleCount getCycleCountByCodeAndShop(String code, Long shopId)throws BusinessException;

	/**
	 * Lay ds chenh lech kho va thuc te cua kiem ke
	 * 
	 * @author lacnv1
	 * @since Nov 04, 2014
	 */
	List<CycleCountDiffVO> getListCycleCountDiff(long cycleCountId) throws BusinessException;

	/**
	 * Hoan thanh kiem kho
	 * @author trietptm
	 * @since 09/07/2015
	 * @param cycleCount
	 * @return
	 * @throws BusinessException
	 */
	Boolean completeCheckStock(CycleCount cycleCount, LogInfoVO logInfo) throws BusinessException;

	/**
	 * 
	 * @param filter
	 * @return danh sach duyet kiem ke
	 * @throws BusinessException
	 * @author trietptm
	 * @since 11/07/2015
	 */
	ObjectVO<CycleCountSearchVO> getListCycleCountSearchVO(CycleCountFilter filter) throws BusinessException;

	/**	
	 * 
	 * @author trietptm
	 * @since 15/07/2015
	 * @param cycleCount
	 * @param lstCCMapProductTest
	 * @param logInfo
	 * @return danh sach ten sp ton kho am
	 * @throws BusinessException
	 */
	List<String> approveStockCount(CycleCount cycleCount, List<CycleCountMapProduct> lstCCMapProductTest, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * Danh sach san pham bo sung nhap kiem ke
	 * @author hunglm16
	 * @param kPaging
	 * @param status
	 * @param cycleCountId
	 * @param lstProductId
	 * @param lstProductIdExcep
	 * @param productCode
	 * @param productName
	 * @return
	 * @throws BusinessException
	 * @since 10/11/2015
	 */
	ObjectVO<ProductVO> searchProductNotInCycleProductMap(KPaging<ProductVO> kPaging, Integer status, Long cycleCountId, List<Long> lstProductId, List<Long> lstProductIdExcep, String productCode, String productName) throws BusinessException;
	
	/**
	 * Thong tin the kiem kho
	 * @author hunglm16
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since 11/11/2015
	 */
	ObjectVO<MapProductVO> searchCycleCountMapProductByFilter(MapProductFilter<MapProductVO> filter) throws BusinessException;
	
	/**
	 * Lay the kiem kho va bo sung them san pham
	 * @author hunglm16
	 * @param filter
	 * @param lstProductId
	 * @return Danh sach the kiem kho tam
	 * @throws DataAccessException
	 * @since 12/11/2015
	 */
	ObjectVO<MapProductVO> getCycleCountMapProductByProductInsertTemp(MapProductFilter<MapProductVO> filter, List<Long> lstProductId) throws BusinessException;

	/**
	 * Cap nhat the kho
	 * @modify hunglm16
	 * @param cycleCount
	 * @param lstUpdatedCycleCountMapProduct
	 * @param lstCycleCountResult
	 * @param lstCycleCountMapProductVOEx
	 * @param lstDeletedCycleCountResult
	 * @param mapProductInsert
	 * @param log
	 * @throws BusinessException
	 * @since 14/11/2015
	 */
	void updateListCycleCountDetailAndCreateListCycleCountResult(CycleCount cycleCount, List<CycleCountMapProduct> lstUpdatedCycleCountMapProduct, List<CycleCountResult> lstCycleCountResult,
			List<CycleCountMapProductVOEx> lstCycleCountMapProductVOEx, List<CycleCountResult> lstDeletedCycleCountResult, Map<Long, Integer> mapProductInsert, LogInfoVO log) throws BusinessException;

	

}

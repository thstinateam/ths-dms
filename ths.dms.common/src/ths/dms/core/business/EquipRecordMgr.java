package ths.dms.core.business;

/**
 * Import thu vien
 * */
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipLiquidationForm;
import ths.dms.core.entities.EquipLiquidationFormDtl;
import ths.dms.core.entities.EquipLostMobileRec;
import ths.dms.core.entities.EquipLostRecord;
import ths.dms.core.entities.EquipStatisticCustomer;
import ths.dms.core.entities.EquipStatisticGroup;
import ths.dms.core.entities.EquipStatisticRecDtl;
import ths.dms.core.entities.EquipStatisticRecord;
import ths.dms.core.entities.EquipStatisticStaff;
import ths.dms.core.entities.EquipStatisticUnit;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.EquipObjectType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipStatisticFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.vo.EquipLostMobileRecVO;
import ths.dms.core.entities.vo.EquipRecordShopVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipStatisticCustomerVO;
import ths.dms.core.entities.vo.EquipStatisticRecordDetailVO;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentLiquidationFormVO;
import ths.dms.core.entities.vo.EquipmentRecordDetailVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Xu ly du lieu
 * 
 * @author hunglm16
 * @since December 31,2014
 * @description Dung chung cho Bien ban Thiet Bi
 * */
public interface EquipRecordMgr {
	/**
	 * Them moi thong tin Bien ban bao mat
	 * 
	 * @author hunglm16
	 * @since December 14,2014
	 * */
	EquipLostRecord createEquipLostRecord(EquipLostRecord equipLostRecord) throws BusinessException;

	/**
	 * Tim kiem Bien Ban Bao Mat
	 * 
	 * @author hunglm16
	 * @since December 31,2014
	 * */
	ObjectVO<EquipRecordVO> searchListEquipLostRecordVOByFilter(EquipRecordFilter<EquipRecordVO> filter) throws BusinessException;

	/**
	 * Tao moi bien ban thanh ly
	 * 
	 * @author nhutnn
	 * @since 31/12/2014
	 */
	EquipLiquidationForm createLiquidationForm(EquipLiquidationForm equipLiquidationForm, List<EquipLiquidationFormDtl> lstLiquidationFormDtls, EquipRecordFilter<EquipmentLiquidationFormVO> filter) throws BusinessException;

	/**
	 * Thay doi cap nhat danh sach Bien Ban Bao Mat theo filter
	 * 
	 * @author hunglm16
	 * @since January 01,2014
	 * */
	void changeByListEquipLostRecord(EquipRecordFilter<EquipLostRecord> filter) throws BusinessException;

	/**
	 * Tim kiem hinh anh bien ban
	 * 
	 * @author hunglm16
	 * @since January 01,2014
	 * */
	ObjectVO<EquipAttachFile> searchEquipAttachFileByFilter(BasicFilter<EquipAttachFile> filter) throws BusinessException;

	/**
	 * Thay doi cap nhat gia tri Bien Ban Bao Mat
	 * 
	 * @author hunglm16
	 * @since January 01,2014
	 * */
	void changeEquipLostRecord(EquipRecordFilter<EquipLostRecord> filter, EquipLostRecord equipLostRecOld) throws BusinessException;

	/**
	 * Lay Bien Ban Bao Mat Entites
	 * 
	 * @author hunglm16
	 * @since December 31,2014
	 * @param id
	 * */
	EquipLostRecord getEquipLostRecordById(Long id) throws BusinessException;
	
	/**
	 * Lay Bien Ban Bao Mat Entites thuoc shopId
	 * @author dungnt19
	 * @param id
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	EquipLostRecord getEquipLostRecordById(Long id, Long shopId) throws BusinessException;

	/**
	 * Them moi bien ban bao mat theo danh sach thiet bi
	 * 
	 * @author hunglm16
	 * @return 
	 * @since January 06,2014
	 * */
	List<Long> insertLoadRecordByListEquip(EquipRecordFilter<EquipLostRecord> filter) throws BusinessException;

	/**
	 * Chi tiet thiet bi cho bao mat
	 * 
	 * @author tientv11
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	List<EquipmentVO> getListEquipmentVOByEquiId(Long id) throws BusinessException;

	/**
	 * Them moi bien ban bao mat
	 * 
	 * @author hunglm16
	 * @return 
	 * @since January 07,2014
	 * */
	Long insertEquipLostRecord(EquipRecordFilter<EquipLostRecord> filter) throws BusinessException;

	EquipStatisticRecord getEquipStatisticRecordById(Long id) throws BusinessException;

	List<EquipStatisticGroup> getListEquipStatisticGroup(Long id) throws BusinessException;

	List<EquipGroup> getListEquipGroupByRecordId(Long recordId) throws BusinessException;

	EquipStatisticRecord createEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord, LogInfoVO logInfo) throws BusinessException;

	ObjectVO<EquipGroup> getListEquipGroupExceptInRecord(KPaging<EquipGroup> kPaging, Long recordId, String code, Long catId, Float fromCapacity, Float toCapacity, String brand) throws BusinessException;

	/**
	 * Lay danh sach shop kiem ke
	 * @author trietptm
	 * @param recordId
	 * @param lstShopSpecificType
	 * @param shopCode
	 * @param shopName
	 * @return
	 * @throws BusinessException
	 * @since Mar 7, 2016
	*/
	List<EquipRecordShopVO> getListShopOfRecord(Long recordId, List<ShopSpecificType> lstShopSpecificType, String shopCode, String shopName) throws BusinessException;

	List<EquipRecordShopVO> searchShopForRecord(Long recordId, Long pShopId, String shopCode, String shopName, Integer isLevel) throws BusinessException;

	List<EquipRecordShopVO> getListShopInRecord(long recordId, List<Long> lstShopId, boolean shopMapOnly) throws BusinessException;

	Boolean createListEquipStatisticUnit(List<EquipStatisticUnit> lstUnit, LogInfoVO logInfo) throws BusinessException;

	/**
	 * create list record when import
	 * @author phut
	 */
	void createListEquipStatisticGroup(List<EquipStatisticGroup> list, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Lay danh sach cac thiet bi thanh ly
	 * 
	 * @author nhutnn
	 * @since 06/01/2015
	 * @param kPaging
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentDeliveryVO> getListEquipmentLiquidationVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;

	/***
	 * @author vuongmq
	 * @date 08/07/2015
	 * Lay danh sach thiet bi pop up cua thanh ly tai san
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	ObjectVO<EquipmentDeliveryVO> getListEquipmentLiquidationPopup(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;
	
	/**
	 * Lay danh sach thiet bi bao mat tu may tinh bang
	 * 
	 * @author tientv11
	 * @sine 07/01/2015
	 * @param equipmentFilter
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<EquipLostMobileRecVO> getListEquipLostMobileRecByFilter(EquipmentFilter<EquipLostMobileRecVO> equipmentFilter) throws BusinessException;

	List<EquipmentRecordDetailVO> getListEquipmentRecordDetailVOByRecordId(Long recordId) throws BusinessException;

	ObjectVO<StatisticCheckingVO> getListStatisticCheckingVOByRecordId(KPaging<StatisticCheckingVO> kPaging, Long recordId, String shopCode, String shortCode, String address, String equipCode, String seri, Integer status) throws BusinessException;

	/**
	 * Lay bien ban thanh ly bang id
	 * 
	 * @author nhutnn
	 * @since 07/01/2014
	 * @param Id
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	EquipmentLiquidationFormVO getEquipmentLiquidationVOById(Long id, Long shopId) throws BusinessException;

	/**
	 * chinh sua bien ban thanh ly
	 * 
	 * @author nhutnn
	 * @since 07/01/2015
	 * @param eqDeliveryRecord
	 * @param lstDeliveryRecDtls
	 * @return
	 * @throws DataAccessException
	 */
	void updateLiquidationForm(EquipLiquidationForm equipLiquidationForm, List<EquipLiquidationFormDtl> lstLiquidationFormDtls, Integer status, EquipRecordFilter<EquipmentLiquidationFormVO> filter ) throws BusinessException;

	/**
	 * luu bien ban thanh ly
	 * 
	 * @author nhutnn
	 * @since 26/12/2014
	 * @param equipLiquidationForm
	 * @param status
	 * @throws BusinessException
	 */
	void saveEquipLiquidationForm(EquipLiquidationForm equipLiquidationForm, Integer status) throws BusinessException;
	
	/**
	 * Lay Max Code Bien Ban Bao Mat Theo Tien To
	 * 
	 * @author hunglm16
	 * @since January 08,2014
	 * */
	String getEquipLostRecordCodeByMaxCode(String maxCode);

	/**
	 * Tim kiem Danh sach bien ban bao mat Entites
	 * 
	 * @author hunglm16
	 * @since January 08,2014
	 * */
	ObjectVO<EquipLostRecord> searchEquipLostRecordByFilter(EquipRecordFilter<EquipLostRecord> filter) throws BusinessException;

	/**
	 * Lay danh sach hinh anh kiem ke thiet bi
	 * @author trietptm
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @since Mar 21, 2016
	 */
	ObjectVO<ImageVO> getListImageVO(EquipStatisticFilter filter) throws BusinessException;

	EquipAttachFile getEquipAttachFileById(Long id) throws BusinessException;

	EquipStatisticRecDtl geEquipStatisticRecDtlById(Long id) throws BusinessException;

	/**
	 * Tim kiem Danh sach bien ban bao mat tu mobile Entites
	 * 
	 * @author hunglm16
	 * @since January 12,2014
	 * */
	EquipLostMobileRec getEquipLostMobileRecById(Long id) throws BusinessException;
	EquipLostMobileRec getEquipLostMobileRecById(Long id, Long shopId) throws BusinessException;

	/**
	 * In Bien Ban Bao Mat
	 * 
	 * @author hunglm16
	 * @since January 15,2014
	 * */
	ObjectVO<EquipRecordVO> printfEquipLostRecordVOByFilter(EquipRecordFilter<EquipRecordVO> filter) throws BusinessException;

	/**
	 * Dem so so dong trong Bien ban bao mat tu Mobile theo Filter
	 * 
	 * @author hunglm16
	 * @since January 19,2014
	 * */
	Integer countEquipLostMobileRecByFilter(EquipRecordFilter<EquipRecordVO> filter) throws BusinessException;

	/**
	 * Cap nhat danh sach Bien ban bao mat tu Mobile theo Filter
	 * 
	 * @author hunglm16
	 * @since January 24,2015
	 * */
	void updateByListRecordMobile(EquipRecordFilter<EquipLostMobileRec> filter) throws BusinessException;

	/**
	 * Tim kiem Danh sach Bien ban bao mat tu Mobile theo Filter
	 * 
	 * @author hunglm16
	 * @throws BusinessException
	 * @since January 24,2015
	 * */
	ObjectVO<EquipLostMobileRec> searchEquipRecordFilter(EquipRecordFilter<EquipLostMobileRec> filter) throws BusinessException;

	/**
	 * lay bien ban kiem ke bang code
	 * 
	 * @author nhutnn
	 * @since28/01/2015
	 * 
	 * @param code
	 * @return
	 * @throws BusinessException
	 */
	EquipStatisticRecord getEquipStatisticRecordByCode(String code) throws BusinessException;

	/**
	 * kiem tra da ton tai bien ban kiem ke chua
	 * 
	 * @author nhutnn
	 * @since28/01/2015
	 * 
	 * @param code
	 * @return
	 * @throws BusinessException
	 */
	Boolean checkEquipStatisticRecordByCode(String code) throws BusinessException;

	/**
	 * Lay danh sach tap tin EquipAttachFile
	 * 
	 * @author hunglm16
	 * @since Feb 03,2014
	 * */
	ObjectVO<FileVO> searchListFileVOByFilter(EquipRecordFilter<FileVO> filter) throws BusinessException;
	/**
	 * Lay danh sach tap tin EquipAttachFile
	 * 
	 * @author hunglm16
	 * @since Feb 03,2014
	 * */
	void getLstEquipAttachFileId(EquipRecordFilter<EquipLostRecord> filter) throws BusinessException;

	/**
	 * delete customer
	 * @author phut
	 */
	void deleteCustomer(Long id) throws BusinessException;

	/**
	 * get list u, ke
	 * @author phut
	 */
	List<Product> getListShelfByRecordId(Long recordId)
			throws BusinessException;

	/**
	 * delete equip_statistic_group
	 * @author phut
	 */
	void deleteEquipStatisticGroup(Long recordId, Long groupId,
			EquipObjectType type) throws BusinessException;

	/**
	 * get list u, ke but not in equip_statistic_group
	 * @author phut
	 */
	ObjectVO<Product> getListShelfExceptInRecord(KPaging<Product> kPaging,
			Long recordId, String code, String name) throws BusinessException;

	/**
	 * get list shop in equip_statistic_unit
	 * @author phut
	 */
	List<Shop> getAllShopByLstId(Long recordId, List<Long> lstId)
			throws BusinessException;

	/**
	 * check shop in equip_statistic_unit
	 * @author phut
	 */
	Integer checkShopInRecordUnit(Long recordId, Long shopId)
			throws BusinessException;

	/**
	 * Tim kiem danh sach khach hang tab khach hang kiem ke
	 * 
	 * @author phuongvm
	 * @param kPaging
	 * @param recordId
	 * @param shortCode
	 * @param customerName
	 * @param statusCus
	 * @return danh sach khach hang tham gia kiem ke
	 * @throws BusinessException
	 * @since 17/12/2015
	 */
	ObjectVO<EquipStatisticCustomerVO> searchCustomer4Record(KPaging<EquipStatisticCustomerVO> kPaging, Long recordId, String shortCode, String customerName, Integer statusCus) throws BusinessException;

	/**
	 * insert list when import
	 * @author phut
	 */
	Boolean createListEquipStatisticCustomer(List<EquipStatisticCustomer> list,
			LogInfoVO logInfo) throws BusinessException;

	/**
	 * leave shop from unit
	 * @author phut
	 */
	void deleteEquipStatisticUnit(long recordId, long shopId,
			LogInfoVO logInfo, Boolean deleteChild) throws BusinessException;

	/**
	 * check shop has child
	 * @author phut
	 */
	Boolean checkHashChild(Long recordId, Long shopId) throws BusinessException;

	/**
	 * save statistic from group
	 * @author phut
	 */
	void saveGroupCheking(List<Long> listDetailId, List<Integer> listExists,
			LogInfoVO logInfo) throws BusinessException;

	/**
	 * save statistic from shelf
	 * @author phut
	 */
	void saveShelfCheking(List<Long> listDetailId, List<Integer> listActQty,
			LogInfoVO logInfo) throws BusinessException;

	/**
	 * get list detail for checking. if not exist in equip_statistic_rec_dtl => insert
	 * @author phut
	 */
	ObjectVO<StatisticCheckingVO> getListDetailGroupSoLan(
			KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId,
			Long equipGroupId, String equipCode, String seri, Long stockId,
			Integer stepCheck, LogInfoVO logInfo) throws BusinessException;

	/**
	 * get list detail for checking. if not exist in equip_statistic_rec_dtl => insert
	 * @author phut
	 */
	ObjectVO<StatisticCheckingVO> getListDetailGroupTuyen(
			KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId,
			Long equipGroupId, String equipCode, String seri, Long stockId,
			Date checkDate, LogInfoVO logInfo) throws BusinessException;

	/**
	 * get list detail for checking. if not exist in equip_statistic_rec_dtl => insert
	 * @author phut
	 */
	ObjectVO<StatisticCheckingVO> getListDetailShelfSoLan(
			KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId,
			Long productId, Long stockId, Integer stepCheck, LogInfoVO logInfo)
			throws BusinessException;

	/**
	 * get list detail for checking. if not exist in equip_statistic_rec_dtl => insert
	 * @author phut
	 */
	ObjectVO<StatisticCheckingVO> getListDetailShelfTuyen(
			KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId,
			Long productId, Long stockId, Date checkDate, LogInfoVO logInfo)
			throws BusinessException;
	/**
	 * Lay danh sach thong tin kiem ke u ke
	 * 
	 * @author phuongvm
	 * @since 22/04/2015
	 * */
	ObjectVO<StatisticCheckingVO> getListDetailShelf(EquipStatisticFilter filter) throws BusinessException;
	/**
	 * Lay danh sach thong tin kiem ke thiet bi
	 * 
	 * @author phuongvm
	 * @since 23/04/2015
	 * */
	ObjectVO<StatisticCheckingVO> getListDetailGroup(EquipStatisticFilter filter) throws BusinessException;
	/**
	 * Kiem tra xem thiet bi co thuoc nhom thiet bi va don vi cua kiem ke ko (true : thuoc, false: ko thuoc)
	 * 
	 * @author phuongvm
	 * @since 25/04/2015
	 * */
	Boolean checkEquipInUnitAndGroup(Long recordId, Long equipId) throws BusinessException;
	/**
	 * Lay don vi cua thiet bi o kho don vi
	 * 
	 * @author phuongvm
	 * @since 25/04/2015
	 * */
	Shop getShopInEquip(Long stockId) throws BusinessException;
	/**
	 * Tao danh sach kiem ke
	 * 
	 * @author phuongvm
	 * @since 25/04/2015
	 * */
	void createListEquipStatisticRecDetail(List<EquipStatisticRecDtl> lst) throws BusinessException;
	/**
	 * Lay chi tiet kiem ke
	 * 
	 * @author phuongvm
	 * @since 27/04/2015
	 * */
	EquipStatisticRecDtl geEquipStatisticRecDtlByFilter(EquipStatisticFilter filter) throws BusinessException;
	/**
	 * cap nhatchi tiet kiem ke
	 * 
	 * @author phuongvm
	 * @since 27/04/2015
	 * */
	void updateEquipStatisticRecDetail(EquipStatisticRecDtl equipStatisticRecDtl) throws BusinessException;
	/**
	 * check don vi co thuoc danh sach don vi kiem ke
	 * @author phuongvm
	 * @since 27/04/2015
	 */
	Boolean checkExistInEquipStatisticUnit(Long recordId, Long shopId) throws BusinessException;
	/**
	 * check u ke/nhom thiet bi co thuoc danh sach nhom kiem ke
	 * @author phuongvm
	 * @since 27/04/2015
	 */
	Boolean checkExistInEquipStatisticGroup(Long recordId, Long objectId) throws BusinessException;

	/**
	 * Tim kiem danh sach thiet bi bien ban sua chua, 
	 * lay theo phan quyen; popup thiet bi
	 * @author vuongmq
	 * @date 04/05/2015
	 */
	ObjectVO<EquipmentDeliveryVO> getListEquipmentLostPopUp( EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;

	/***
	 * @author vuongmq
	 * @date 04/05/2015
	 * cap nhat  lay kho popup cua Thiet bi, theo phan quyen
	 */
	ObjectVO<EquipStockVO> getListStockLostByFilter(EquipStockFilter filter) throws BusinessException;
	/**
	 * Lay danh sach khach hang dua vao bien ban kiem ke
	 * @author phuongvm
	 * @since 18/05/2015
	 * @param recordId
	 * @throws DataAccessException
	 */
	List<Customer> getListCustomerBuTCHTTM(Long recordId) throws BusinessException;
	/**
	 * check khach hang co thuoc kiem ke
	 * @author phuongvm
	 * @since 20/05/2015
	 */
	Boolean checkCustomerInEquipStatisCus(Long recordId, Long customerId) throws BusinessException;
	/**
	 * xoa tat ca khach hang co status = 0
	 * @author phuongvm
	 * @since 20/05/2015
	 */
	void deleteAllCustomer(Long id) throws BusinessException;
	
	/**
	 * Lay danh sach xuat excel don vi tham gia bien ban kiem ke
	 * @author hoanv25
	 * @since May 29/2015
	 * @param recordId, lstShopObjectType
	 * @throws DataAccessException
	 */
	List<EquipRecordShopVO> getListShopOfRecordExport(Long recordId, List<ShopObjectType> lstShopObjectType, String shopCode, String shopName) throws BusinessException;
	/**
	 * Lay danh sach don vi kiem ke con 
	 * 
	 * @author phuongvm
	 * @since 02/07/2015
	 * @param recordId
	 * @param shopId
	 * @throws DataAccessException
	 */
	List<EquipStatisticUnit> getListStatisticUnitChild(Long recordId,
			Long shopId) throws BusinessException;
	/**
	 * xoa danh sach don vị kiem ke 
	 * 
	 * @author phuongvm
	 * @since 02/07/2015
	 * @param List<EquipStatisticUnit> lst
	 * @throws DataAccessException
	 */
	void deleteListEquipStatisticUnit(List<EquipStatisticUnit> lst)
			throws BusinessException;
	/**
	 * Lay danh sách shop con cháu loại trừ 
	 * 
	 * @author phuongvm
	 * @since 02/07/2015
	 * @param shopId
	 * @param lstShopIdExcept
	 * @throws DataAccessException
	 */
	List<Shop> getListShopForEquipStatisticUnit(Long shopId,
			List<Long> lstShopIdExcept) throws BusinessException;
	/**
	 * Lay danh sách thiết bị khi tạo kiểm kê
	 * 
	 * @author phuongvm
	 * @since 07/07/2015
	 * @param filter
	 * @throws DataAccessException
	 */
	ObjectVO<EquipmentVO> getListEquipmentByStatisticFilter(
			EquipmentFilter<EquipmentVO> filter) throws BusinessException;
	/**
	 * Lay nhom kiem ke
	 * 
	 * @author phuongvm
	 * @since 08/07/2015
	 * @param recordId
	 * @param groupId
	 * @param type
	 * @throws DataAccessException
	 */
	EquipStatisticGroup getEquipStatisticGroupById(Long recordId, Long groupId,
			EquipObjectType type) throws BusinessException;
	/**
	 * lay danh sach thiet bi trong kiem ke
	 * @author phuongvm
	 * @param filter
	 * @return danh sach thiet bi trong nhom thiet bi
	 * @throws DataAccessException
	 */
	List<Equipment> getListEquipByRecordId(Long recordId)
			throws BusinessException;
	/**
	 * Kiem tra xem thiet bi co thuoc don vi cua kiem ke ko (true : thuoc, false: ko thuoc)
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @param equipId
	 * @since 15/07/2015
	 * */
	Boolean checkEquipInUnit(Long recordId, Long equipId)
			throws BusinessException;
	/**
	 * check nhan vien thiet bi co ton tai
	 * @author phuongvm
	 * @param recordId
	 * @param staffId
	 * @param equipId
	 * @since 20/07/2015
	 */
	Boolean checkStaffInEquipStatisStaff(Long recordId, Long staffId,
			Long equipId) throws BusinessException;
	/**
	 * tao nhan vien thiet bi co ton tai
	 * @author phuongvm
	 * @param entity 
	 * @since 20/07/2015
	 */
	EquipStatisticStaff createEquipStatisticStaff(EquipStatisticStaff entity)
			throws BusinessException;
	/**
	 * lay danh sach thiet bi nvbh de xuat excel tab khach hang
	 * @author phuongvm
	 * @param filter
	 * @return danh sach thiet bi trong nhom thiet bi
	 * @throws DataAccessException
	 */
	List<EquipmentVO> getListEquipmentStaffByStatisticFilter(
			EquipmentFilter<EquipmentVO> filter) throws BusinessException;
	/**
	 * Chay tien trinh khi chuyen trang thai kiem ke u ke
	 *
	 * @param recordId  
	 * @param programCode 
	 * @param lstShopId 
	 * @throws BusinessException the business exception
	 * @author phuongvm
	 */
	void runProcessForEquipShelfTotal(Long recordId, String programCode,
			String lstShopId) throws BusinessException;
	/**
	 * Xoa cac dong EquipStatisticStaff khong thoa dieu kien khi chuyen kiem ke thiet bi sang hoat dong
	 *
	 * @param recordId  
	 * @throws BusinessException the business exception
	 * @author phuongvm
	 */
	void deleteEquipStatisticStaffNotBelong(Long recordId)
			throws BusinessException;
	/**
	 * Cap nhat hinh anh kiem ke thiet bi
	 *
	 * @author phuongvm
	 * @param equipAttachFile  
	 * @throws BusinessException
	 * @since 16/09/2015
	 */
	void updateEquipAttachFile(EquipAttachFile equipAttachFile) throws BusinessException;
	
	/**
	 * Lay danh sach kiem ke giao voi tu ngay den ngay truyen vao
	 * @author phuongvm
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @return List<EquipmentVO> 
	 * @throws BusinessException
	 * @since 17/09/2015
	 */
	List<EquipmentVO> getListChecking(Date fromDate, Date toDate, Long shopId) throws BusinessException;
	
	/**
	 * Lay bien ban bao mat moi nhat dua vao thiet bi
	 * 
	 * @author phuongvm
	 * @param equipId
	 * @return EquipRecordVO
	 * @throws BusinessException
	 * @since 20/11/2015
	 */
	EquipRecordVO getLostRecordNewestByEquipId(Long equipId) throws BusinessException;
	
	/**
	 * Lay danh sach chi tiet bien ban thanh ly dua vao id bien ban
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @return danh sach chi tiet bien ban thanh ly
	 * @throws BusinessException
	 * @since 20/11/2015
	 */
	List<EquipLiquidationFormDtl> getListEquipLiquidationFormDtlByRecordId(Long recordId) throws BusinessException;
	
	/**
	 * Xoa du lieu bang EquipStatisticStaff dua vao bien ban kiem ke, nhan vien, thiet bi
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @param staffId
	 * @param equipId
	 * @throws BusinessException
	 * @since 03/12/2015
	 */
	void deleteEquipStatisticStaff(Long recordId, Long staffId, Long equipId) throws BusinessException;
	
	/**
	 * lay danh sach thiet bi kem nhan vien duoc chi dinh de xuat excel
	 * 
	 * @author phuongvm
	 * @param filter
	 * @return Danh sach thiet bi, nhan vien kiem ke
	 * @throws BusinessException
	 * @since 03/12/2015
	 */
	List<EquipmentVO> getListEquipStaffByStatisticFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException;
	
	/**
	 * Lay nhan vien chi dinh thiet bi trong bien ban kiem ke
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @param staffId
	 * @param equipId
	 * @return EquipStatisticStaff
	 * @throws BusinessException
	 * @since 04/12/2015
	 */
	EquipStatisticStaff getEquipStatisticStaff(Long recordId, Long staffId, Long equipId) throws BusinessException;
	
	/**
	 * Kiem tra nhan vien co quan ly shop 
	 * 
	 * @author phuongvm
	 * @param staffId
	 * @param shopId
	 * @return true: co quan ly, false: khong quan ly shop
	 * @throws BusinessException
	 * @since 09/12/2015
	 */
	Boolean isStaffBelongShop(long staffId, long shopId) throws BusinessException;
	
	/**
	 * Kiem tra ton tai so hop dong cua bien ban giao nhan
	 * 
	 * @author phuongvm
	 * @param contractNumber
	 * @return true: ton tai, false: ko ton tai
	 * @throws BusinessException
	 * @since 21/12/2015
	 */
	Boolean isExistContractNumInDeliveryRecord(String contractNumber) throws BusinessException;

	/**
	 * Xu ly lay ds EquipStatisticRecord
	 * @author trietptm
	 * @param filter
	 * @return ObjectVO<EquipStatisticRecord>
	 * @throws BusinessException
	 * @since Mar 4, 2016
	*/
	ObjectVO<EquipStatisticRecord> getListEquipStatisticRecord(EquipRecordFilter<EquipStatisticRecord> filter) throws BusinessException;

	/**
	 * them shop don vi tham gia kiem ke thiet bi
	 * @author trietptm
	 * @param recordId
	 * @param lstShopId
	 * @param lstUnit
	 * @param logInfo
	 * @throws BusinessException
	 * @since Mar 9, 2016
	*/
	void addShopForEquipStatistic(long recordId, List<Long> lstShopId, List<EquipStatisticUnit> lstUnit, LogInfoVO logInfo) throws BusinessException;

	/**
	 * lay thong tin so lan kiem ke
	 * @author trietptm
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @since Mar 11, 2016
	*/
	ObjectVO<EquipStatisticRecordDetailVO> getListStatisticTime(EquipStatisticFilter filter) throws BusinessException;

	/**
	 * Xoa EquipStatisticGroup
	 * @author trietptm
	 * @param filter
	 * @throws BusinessException
	 * @since Mar 22, 2016
	*/
	void deleteEquipStatisticGroup(EquipStatisticFilter filter) throws BusinessException;

}
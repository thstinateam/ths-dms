package ths.dms.core.business;

import java.util.List;
import java.util.Map;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.vo.ApParamVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

public interface EquipmentMgr {

	ApParam createApParam(ApParam apParam, LogInfoVO logInfo) throws BusinessException;

	void updateApParam(ApParam apParam, LogInfoVO logInfo) throws BusinessException;

	void deleteApParam(ApParam apParam, LogInfoVO logInfo) throws BusinessException;

	ApParam getApParamById(Long id) throws BusinessException;

	ApParam getApParamByCode(String code, ApParamType type) throws BusinessException;

	ApParam getApParamByCodeX(String code, ApParamType type, ActiveType status) throws BusinessException;

	List<ApParam> getListApParam(ApParamType type, ActiveType status) throws BusinessException;

	/**
	 * Lấy lý do nghỉ
	 * 
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<ApParam> getOffDateReason() throws BusinessException;

	ApParam getApParamByCodeEx(String code, ActiveType status) throws BusinessException;

	Map<String, String> getMapPromotionType() throws BusinessException;

	/**
	 * Lay danh sach ApParam theo dieu kien loc ApParamFilter
	 * 
	 * @author tulv2
	 * @since 09.17.2014
	 * */
	List<ApParam> getListApParamByFilter(ApParamFilter filter) throws BusinessException;

	/**
	 * Lay danh sach ApParam theo dieu kien loc ApParamFilter
	 * 
	 * @author phuongvm
	 * @since 06.10.2014
	 * */
	ObjectVO<ApParamVO> getListApParamByFilterEx(ApParamFilter filter) throws BusinessException;

	/**
	 * Tao moi ap_param (truong hop type khong co dinh nghia trong enum)
	 * 
	 * @author lacnv1
	 * @since Oct 13, 2014
	 */
	ApParam createApParamEx(ApParam apParam, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Cap nhat ap_param (truong hop type khong co dinh nghia trong enum)
	 * 
	 * @author lacnv1
	 * @since Oct 13, 2014
	 */
	ApParam updateApParamEx(ApParam apParam, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Tao moi ap_param_history
	 * 
	 * @author hoanv25
	 * @since Dec 04, 2014
	 */
//	ApParamHistory getApParamHistoryById(Long id) throws BusinessException;
//
//	ApParamHistory createApParamHistoryEx(ApParamHistory apParam, LogInfoVO logInfo) throws BusinessException;
//
//	void updateApParamAndApParamHistory(ApParam apParam, ApParamHistory apParamHistory, LogInfoVO logInfo) throws BusinessException;
}
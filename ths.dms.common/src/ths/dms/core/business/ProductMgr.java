package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.MediaThumbnail;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.PriceSaleIn;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.ProductIntroduction;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaType;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.ProductFilter;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.ProgramObjectType;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.TreeVOType;
import ths.dms.core.entities.filter.PriceFilter;
import ths.dms.core.entities.filter.ProductGeneralFilter;
import ths.dms.core.entities.vo.GeneralProductVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.entities.vo.PriceVO;
import ths.dms.core.entities.vo.ProductExportVO;
import ths.dms.core.entities.vo.ProductTreeVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.entities.vo.ProductVOEx;
import ths.dms.core.entities.vo.ProductsForPoAutoParams;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.exceptions.BusinessException;

public interface ProductMgr {

	Product createProduct(Product product, LogInfoVO logInfo) throws BusinessException;

	void updateProduct(Product product, LogInfoVO logInfo) throws BusinessException;

	void deleteProduct(Product product, LogInfoVO logInfo) throws BusinessException;

	Product getProductByCode(String code) throws BusinessException;

	Product getProductByCode(String code, Integer status) throws BusinessException;

	Price createPrice(Price price, LogInfoVO logInfo) throws BusinessException;

	void updatePrice(Price price, LogInfoVO logInfo) throws BusinessException;

	void deletePrice(Price price, LogInfoVO logInfo) throws BusinessException;

	Price getPriceById(Long id) throws BusinessException;

	Boolean isProductUsingByOthers(long productId) throws BusinessException;

	Boolean isPriceUsingByOthers(long priceId) throws BusinessException;

	Product getProductById(Long id) throws BusinessException;

	Boolean checkIfDateRangeInUse(Long productId, Date fromDate, Date toDate, Long priceId) throws BusinessException;

	ObjectVO<Product> getListProduct(KPaging<Product> kPaging, ActiveType status) throws BusinessException;

	Price getPriceByProductId(Long shopId, Long productId) throws BusinessException;

	Price getPriceByProductId(Long productId) throws BusinessException;

	Price getPriceByProductAndShopId(Long productId, Long shopId) throws BusinessException;

	/**
	 * lay danh sach sku them vao sale plan cho nha pp
	 * 
	 * @author thanhnguyen
	 * @param kPaging
	 * @param productCode
	 * @param productName
	 * @param month
	 * @param year
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<ProductVO> getListProductAddSkuForSalePlan(KPaging<ProductVO> kPaging, String productCode, String productName, Integer month, Integer year) throws BusinessException;

	ProductIntroduction createProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws BusinessException;

	void updateProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws BusinessException;

	void deleteProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws BusinessException;

	ProductIntroduction getProductIntroductionById(Long id) throws BusinessException;

	MediaItem createMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws BusinessException;

	//	void updateMediaItem(MediaItem mediaItem, LogInfoVO logInfo)
	//			throws BusinessException;

	void deleteMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws BusinessException;

	MediaItem getMediaItemById(Long id) throws BusinessException;

	List<MediaItem> getListMediaItemByMediaType(MediaType mediaType) throws BusinessException;

	ProductIntroduction getProductIntroductionByProduct(Long productId) throws BusinessException;

	public List<ProductLot> getProductLotByProductAndOwner(long productId, long ownerId, StockObjectType ownerType) throws BusinessException;

	List<MediaItem> getListMediaItemByProduct(KPaging<MediaItem> kPaging, Long productId) throws BusinessException;

	ObjectVO<Product> getListProductEx(ProductFilter filter) throws BusinessException;

	MediaThumbnail createMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws BusinessException;

	void updateMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws BusinessException;

	void deleteMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws BusinessException;

	MediaThumbnail getMediaThumbnailById(Long id) throws BusinessException;

	ObjectVO<Price> getListPrice(KPaging<Price> kPaging, String productCode, BigDecimal price, Date fromDate, Date toDate, ActiveType status, BigDecimal priceNotVat, BigDecimal vat, boolean isExport) throws BusinessException;

	List<ProductLot> getProductLotByProductAndOwnerEx(long productId, long ownerId, StockObjectType ownerType) throws BusinessException;

	ObjectVO<PoVnmDetailLotVO> getListProductForPoConfirm(KPaging<PoVnmDetailLotVO> kPaging, PoVnmFilter filter) throws BusinessException;

	ObjectVO<StockTotalVO> getListProductForPoAuto(KPaging<StockTotalVO> kPaging, ProductsForPoAutoParams filter) throws BusinessException;

	ObjectVO<Product> getListProductHasInSaleOrder(KPaging<Product> kPaging, String productCode, String productName, ActiveType status) throws BusinessException;

	Product getProductByCodeExceptZ(String code, Long staffId) throws BusinessException;

	ObjectVO<Product> getListProductExceptZ(KPaging<Product> paging, String productCode, String productName, Long staffId) throws BusinessException;

	Boolean isProductBelongEquipmentCat(Product product) throws BusinessException;

	Boolean checkProductInZ(String productCode) throws BusinessException;

	MediaItem checkIsExistsVideoOfProduct(Long productId) throws BusinessException;

	MediaItem updateMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws BusinessException;

	List<GeneralProductVO> getAllProducts(String productCode, KPaging<GeneralProductVO> kPaging, Boolean isExceptZCat) throws BusinessException;

	List<Product> getListProductEquipmentZ() throws BusinessException;

	List<ProductTreeVO> getListProductTreeVO(String productCode, String productName, Long programObjectId, ProgramObjectType programObjectType, Boolean exceptEquipmentCat, DisplayProductGroupType type) throws BusinessException;

	List<ProductTreeVO> getListProductTreeVOByParentNode(Long programObjectId, ProgramObjectType programObjectType, Long parentIdNode, TreeVOType parentTypeNode, Boolean exceptEquipmentCat, DisplayProductGroupType type) throws BusinessException;

	ObjectVO<Product> getListProductInCatZ(KPaging<Product> paging, String productCode, String productName) throws BusinessException;

	List<ProductLot> getProductLotByProduct(Product product) throws BusinessException;

	//SangTN
	ObjectVO<ProductVOEx> getListProductVOForVideo(ths.dms.core.entities.filter.ProductFilter filter, List<Product> lstProductExcept) throws BusinessException;

	List<ProductVOEx> getListCategory(String expectOne) throws BusinessException;

	//vuongmq
	ObjectVO<Product> getListProductByNameOrCode(KPaging<Product> kPaging, String name, String code, String categoryCode) throws BusinessException;

	ObjectVO<Product> getAllListProductEx(ProductFilter filter) throws BusinessException;

	/**
	 * lay danh sach tong ton kho cua sp theo nhieu gia
	 * 
	 * @author nhanlt6
	 * @return
	 * @throws BusinessException
	 */
	List<StockTotalVO> getPromotionProductInfo(Long productId, Long shopId, Integer shopChannel, Long customerId, Integer objectType, Date orderDate) throws BusinessException;

	/**
	 * Lay gia theo KH
	 * 
	 * @author lacnv1
	 * @since Sep 17, 2014
	 */
	Price getProductPriceForCustomer(long productId, Customer cust) throws BusinessException;

	Boolean checkPricetByFilter(PriceFilter<Price> filter) throws BusinessException;

	Price getPricetByFilter(PriceFilter<Price> filter) throws BusinessException;

	ObjectVO<PriceVO> searchPriceVOByFilter(PriceFilter<PriceVO> filter) throws BusinessException;

	ObjectVO<Price> getListPriceByFilter(PriceFilter<Price> filter) throws BusinessException;

	void deleteListPrice(List<Price> lstPrice, LogInfoVO logInfo) throws BusinessException;

	void updateWaitingPriceManager(List<Price> lstPrice, LogInfoVO logInfo, Integer flag) throws BusinessException;

	Boolean checkPricetByFilterEx(PriceFilter<Price> filter) throws BusinessException;

	Integer stopPrice(Price price, LogInfoVO logInfo) throws BusinessException;

	List<Price> getListPriceByShopId(Long shopId, Date lockDay, Long productId) throws BusinessException;

	List<ProductVO> getListProductForWarehouseExport(Integer status) throws BusinessException;

	/**
	 * Danh sach san pham ho tro nhap xuat vay muon
	 * 
	 * @author tientv11
	 * @param status
	 * @return
	 * @throws BusinessException
	 */
	List<ProductVO> getListProductForBorrowFilter(Integer status) throws BusinessException;

	/*************************** BEGIN HAM MOI TU HABECO *************************/
	/**
	 * Lay danh sach san pham theo ten (tim chinh xac) productName, tru san pham
	 * exceptProductId
	 * 
	 * @author lacnv1
	 * @since Dec 28, 2013
	 */
	List<Product> getListProductsByName(String productName, Long exceptProductId) throws BusinessException;

	/**
	 * Lay danh sach san pham theo ten shortName, tru san pham exceptProductId
	 * 
	 * @author haupv3
	 * @since Feb 10, 2014
	 */
	List<Product> getListProductsByShortName(String productName, Long exceptProductId) throws BusinessException;

	/**
	 * LacNV
	 */
	ObjectVO<ProductExportVO> getListProductExport(ProductFilter filter, KPaging<ProductExportVO> paging) throws BusinessException;

	/**
	 * @author lacnv1
	 * @since Dec 16, 2013
	 * @param product
	 * @param price1
	 *            - gia cong ty ban cho npp
	 * @param price2
	 *            - gia npp ban cho KH
	 */
	Product saveOrUpdateProduct(Product product, List<Long> lstAttributeId, List<String> lstAttributeValue, List<Integer> lstAttributeColumnValueType, LogInfoVO logInfo) throws BusinessException;

	/*************************** HAM MOI TU HABECO *************************/

	/**
	 * @author vuongmq
	 * @date Jan 22, 2015 save mediaItem (Image) cho product
	 */
	List<MediaItem> saveUploadImageInfo(List<MediaItem> mediaItems, List<MediaItem> mediaItemsVideo, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Lay gia theo tat ca dieu kien hien tai Ham hop le khi tra ra dung 1 dong
	 * gia. Neu co nhieu hon 1 dong gia thi sai du lieu
	 * 
	 * @author trungtm6
	 * @since 21/04/2015
	 * @param proId
	 * @param shopId
	 * @param date
	 *            : ngay lay gia
	 * @param shopTypeId
	 * @param cusId
	 * @param cusTypeId
	 * @return
	 * @throws BusinessException
	 */
	List<Price> getPriceByFullCondition(Long proId, Long shopId, Date date, Long shopTypeId, Long cusId, Long cusTypeId) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 21/7/2015
	 * @param filter
	 * @return Lay danh sach gia trong rpt theo filter
	 * @throws BusinessException
	 */
	List<Price> getPriceByRpt(SaleOrderFilter<Price> filter) throws BusinessException;

	Price getPriceByProductAndCustomer(Long productId, Long customerId, List<Integer> status) throws BusinessException;

	Price getPriceByProductChannelTypeShopAndCustomerNull(Long productId, Long channelTypeId, Long shopId, List<Integer> status) throws BusinessException;

	Price getPriceByProductChannelTypeShopType(Long customerTypeId, Long shopTypeId, Long productId, List<Integer> status) throws BusinessException;

	Price getPriceByProductShop(Long shopId, Long productId, List<Integer> status) throws BusinessException;

	Price getPriceByProductShopType(Long shopTypeId, Long productId, List<Integer> status) throws BusinessException;

	Boolean checkDuplicateOrderIndex(Long productId, int orderIndex) throws BusinessException;

	void increaseOrderIndexProduct(Product product) throws BusinessException;

	/**
	 * Lay ds san pham theo nganh hang
	 * 
	 * @author hoanv25
	 * @since July 10/2015
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	List<ImageVO> getListParentCatByCat(Long id) throws BusinessException;

	/**
	 * Lay ds san pham theo nganh con
	 * 
	 * @author hoanv25
	 * @since July 10/2015
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	List<ImageVO> getListParentCatBySubCat(Long id) throws BusinessException;

	/**
	 * Lay san pham khong quan tam status
	 * 
	 * @author tamvnm
	 * @version
	 * @param code - ma san pham
	 * @throws BusinessException
	 * @since Sep 15, 2015
	 */
	Product getProductByCodeNotByStatus(String code) throws BusinessException;
	
	/**
	 * Lay danh sach san pham
	 * 
	 * @author hunglm16
	 * @since May 20,2015
	 * */
	ObjectVO<ProductVO> getListProductVOByFilter(ProductGeneralFilter<ProductVO> filter) throws BusinessException;
	
	/**
	 * Lay nganh hang theo Id
	 * 
	 * @author hoanv25
	 * @since July 14/2015
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	ProductInfo getProductInfoById(Long id) throws BusinessException;

	/**
	 * Lay ds san pham theo nganh hang va nganh hang con
	 * 
	 * @author hoanv25
	 * @since July 20/2015
	 * @param idParentCat
	 *            , idParentSubCat
	 * @return
	 * @throws BusinessException
	 */
	List<ImageVO> getListParentCatByCatAndSubCat(Long idParentCat, Long idParentSubCat) throws BusinessException;

	/**
	 * lay danh sach nganh hang
	 * 
	 * @author hoanv25
	 * @param
	 * @return danh sach nganh hang
	 * @throws BusinessException
	 * @since 19/08/2015
	 */
	List<ProductInfo> getSubCatOnTree(Boolean b) throws BusinessException;

	/**
	 * kiem tra san pham dang chon productInfo
	 * @author trietptm
	 * @param pdType
	 * @param productInfoId
	 * @return danh sach san pham dang chon productInfo
	 * @throws BusinessException
	 * @since Nov 18, 2015
	 */
	List<Product> checkIfExistsProductByProductInfo(ProductType pdType, Long productInfoId) throws BusinessException;
	
	/**
	 * Tim kiem san pham voi thong tin thuoc tinh dong
	 * 
	 * @author tuannd20
	 * @param filter
	 *            tham so tim kiem
	 * @return danh sach san pham kem theo thong tin thuoc tinh dong
	 * @throws BusinessException
	 * @since 18/09/2015
	 */
	List<ProductExportVO> getProductWithDynamicAttributeInfo(ProductFilter filter) throws BusinessException;

	/**
	 * Xu ly tim kiem gia sale in
	 * @author trietptm
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @since Jan 20, 2016
	*/
	ObjectVO<PriceVO> searchPriceSaleInVOByFilter(PriceFilter<PriceVO> filter) throws BusinessException;

	/**
	 * Xu ly tam ngung gia sale in
	 * @author trietptm
	 * @param priceSaleIn
	 * @param logInfo
	 * @return
	 * @throws BusinessException
	 * @since Jan 21, 2016
	*/
	Boolean stopPriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws BusinessException;

	/**
	 * lay PriceSaleIn theo id
	 * @author trietptm
	 * @param id
	 * @return
	 * @throws BusinessException
	 * @since Jan 21, 2016
	*/
	PriceSaleIn getPriceSaleInById(Long id) throws BusinessException;

	/**
	 * tao moi PriceSaleIn
	 * @author trietptm
	 * @param priceSaleIn
	 * @param logInfo
	 * @return
	 * @throws BusinessException
	 * @since Jan 22, 2016
	*/
	PriceSaleIn createPriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws BusinessException;

	/**
	 * cap nhat PriceSaleIn
	 * @author trietptm
	 * @param priceSaleIn
	 * @param logInfo
	 * @throws BusinessException
	 * @since Jan 22, 2016
	*/
	void updatePriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws BusinessException;

	/**
	 * tao moi va cap nhat PriceSaleIn bi trung
	 * @author trietptm
	 * @param priceSaleIn
	 * @param logInfo
	 * @throws BusinessException
	 * @since Jan 22, 2016
	*/
	void createAndUpdateOldPriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Gets the product by filter.
	 * @author liemtpt
	 * @param filter the filter
	 * @return the product by filter
	 * @throws BusinessException the business exception
	 */
	Product getProductByFilter(ProductFilter filter) throws BusinessException;
}

package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.StaffAttribute;
import ths.dms.core.entities.StaffAttributeDetail;
import ths.dms.core.entities.StaffAttributeEnum;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.StaffAttributeFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffAttributeVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Quan ly Thuoc tinh nhan vien
 * @author liemtpt
 * @since 22/01/2015
 *
 */
public interface StaffAttributeMgr {
	/****
	    * create staff attribute
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	StaffAttribute createStaffAttribute(StaffAttribute attribute,
			LogInfoVO logInfoVO) throws BusinessException;
	/****
	    * delete staff attribute
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void deleteStaffAttribute(StaffAttribute attribute)
			throws BusinessException;
	/****
	    * get staff attribute theo id
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	StaffAttribute getStaffAttributeById(long id) throws BusinessException;
	/****
	    * update staff attribute
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void updateStaffAttribute(StaffAttribute attribute,LogInfoVO logInfo)
			throws BusinessException;
	/****
	    * get list staff attribute vo
	    * @author liemtpt
	    * @params AttributeDynamicFilterlter
	    * @param kPaging
	    * @return list staff attribute vo
	    * @throws DataAccessException
	    */
	ObjectVO<AttributeDynamicVO> getListStaffAttributeVO(AttributeDynamicFilter filter, KPaging<AttributeDynamicVO> kPaging)
			throws BusinessException;
	/****
	    * save or update staff attribute 
	    * @params AttributeDynamicVO
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void saveOrUpdateStaffAttribute(AttributeDynamicVO vo, LogInfoVO logInfo)
			throws BusinessException;
	/****
	    * save or update staff attribute enum
	    * @params AttributeEnumVO
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void saveOrUpdateStaffAttributeEnum(AttributeEnumVO vo, LogInfoVO logInfo)
			throws BusinessException;
	/****
	    * delete staff attribute enum by id
	    * @params AttributeEnumVO
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void deleteStaffAttributeEnum(StaffAttributeEnum attribute)
			throws BusinessException;
	/****
	    * get staff attribute enum by id
	    * @params AttributeEnumVO
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	StaffAttributeEnum getStaffAttributeEnumById(long id)
			throws BusinessException;
	/****
	    * update staff attribute enum
	    * @params AttributeEnumVO
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void updateStaffAttributeEnum(StaffAttributeEnum attribute,
			LogInfoVO logInfo) throws BusinessException;
	/****
	    * get list staff attribute enum by id
	    * @params AttributeEnumVO
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	List<StaffAttributeDetail> getListStaffAttributeByEnumId(long id)
			throws BusinessException;
	/****
	    * update staff attribute detail
	    * @params AttributeEnumVO
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void updateStaffAttributeDetail(StaffAttributeDetail attribute,
			LogInfoVO logInfo) throws BusinessException;

	/**
	 * Danh sach theo dieu kien tim kiem
	 * @author vuongmq
	 * @date 26/02/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<StaffAttributeVO> getListStaffAttributeVOFilter(StaffAttributeFilter filter)
		throws BusinessException;
}

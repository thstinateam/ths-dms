package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Car;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.StockGeneralFilter;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductOrderVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StockGeneralVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransVO;
import ths.dms.core.entities.vo.WarehouseVO;
import ths.dms.core.exceptions.BusinessException;

public interface StockManagerMgr {
	ObjectVO<WarehouseVO> getListWarehouseVOByFilter(BasicFilter<WarehouseVO> filter) throws BusinessException;
	Boolean checkIfRecordWarehouseExist(BasicFilter<WarehouseVO> filter) throws BusinessException;
	Warehouse createWarehouse(Warehouse warehouse, LogInfoVO logInfo) throws BusinessException;
	void updateWarehouse(Warehouse warehouse) throws BusinessException;
	Warehouse getWarehouseById(Long id) throws BusinessException;
	Integer copyWarehouseByFilter(BasicFilter<ShopVO> filter) throws BusinessException;
	Boolean checkIfRecordWarehouseInStockTotalExist(BasicFilter<WarehouseVO> filter) throws BusinessException;
	List<Warehouse> getListWarehouseByFilter(BasicFilter<Warehouse> filter) throws BusinessException;
	/**
	 * Load danh sach Kho theo SP
	 * @author nhanlt6
	 * @param filter
	 * @date 29/08/2014
	 * */
	ObjectVO<StockTotalVO> getListStockWarehouseByProductId(BasicFilter<StockTotalVO> filter) throws BusinessException;
	
	/**
	 * Load danh sach Kho theo list SP
	 * @author tungmt
	 * @param filter
	 * @date 16/12/2014
	 * */
	ObjectVO<StockTotalVO> getListStockWarehouseByListProduct(BasicFilter<StockTotalVO> filter) throws BusinessException;
	
	/**
	 * Lay ds sp + kho theo NVBH
	 * 
	 * @author lacnv1
	 * @since Sep 23, 2014
	 * 
	 * @param filter.longG idNVBH
	 * @param filter.shopId
	 * @param filter.code ma sp
	 * @param filter.name ten sp
	 */
	ObjectVO<StockTotalVO> getListStockTotalVOBySaler(BasicFilter<StockTotalVO> filter) throws BusinessException;
	
	/**
	 * Luu don hang vansale
	 * 
	 * @author lacnv1
	 * @since Sep 24, 2014
	 */
	StockTrans createStockTransDP(Staff st, Car car, List<ProductOrderVO> lstProducts, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * Lay ds sp theo don vansale DP cua NV
	 * 
	 * @author lacnv1
	 * @since Sep 25, 2014
	 */
	List<StockTotalVO> getListProductDPByStaff(long shopId, long staffId) throws BusinessException;
	
	/**
	 * Lay ds  don vansale DP cua NV
	 * 
	 * @author tungmt
	 * @since 19/01/2015
	 */
	List<StockTransVO> getListDPByStaff(StockStransFilter filter) throws BusinessException;
	
	/**
	 * Luu don hang vansale
	 * 
	 * @author lacnv1
	 * @since Sep 25, 2014
	 */
	StockTrans createStockTransGO(Staff st, List<ProductOrderVO> lstProducts, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * Kiem tra NV da co don GO trong ngay chua
	 * 
	 * @author lacnv1
	 * @since Nov 06, 2014
	 */
	boolean checkExistsGOStockTrans(long staffId, Date pDate) throws BusinessException;
	/**
	 * Kiem tra NV co don GO chua duyet
	 * 
	 * @author longnh15
	 * @since July 02, 2015
	 */
	boolean checkExistsGOStockTransNotApproves(long staffId, String type) throws BusinessException;
	/**
	 * search update information output warehouse
	 * 
	 * @author hunglm16
	 * @since November 8,2014
	 */
	ObjectVO<StockGeneralVO> searchUpdInformationOutputWarehouse(StockGeneralFilter<StockGeneralVO> filter) throws BusinessException;
	/**
	 * Tao ma Code cho phieu dieu chuyen kho
	 * 
	 * @author hunglm16
	 * @since November 3,2014
	 * */
	String generateStockTransCodeByTranType(Long shopId, String transType) throws BusinessException;
	/**
	 * Tim kiem san pham co trong kho ung voi Don vi Xac dinh
	 * 
	 * @author hunglm16
	 * @since October 31,2014
	 * */
	ObjectVO<StockGeneralVO> searchProductInWareHouse(StockGeneralFilter<StockGeneralVO> filter) throws BusinessException;
	/**
	 * Tao moi mot phieu dieu chuyen kho
	 * 
	 * @author hunglm16
	 * @since November 3,2014
	 * */
	void createStockTransferInternalWarehouse(Long wareHouseOutputId, Long wareHouseInputId, Long shopRootId, List<StockTransDetail> lstStockTransDetail, LogInfoVO logInfo) throws BusinessException;
	/**
	 * Tao du lieu
	 * 
	 * @author hunglm16
	 * @since December 15,2014
	 * @description Cap nhat thong tin phieu xuat kho kim van chuyen noi bo
	 * */
	void createStockIssueByFilter(StockGeneralFilter<StockGeneralVO> filter) throws BusinessException;
	/**
	 * Tim kiem kho ung voi Don vi Xac dinh
	 * 
	 * @author hunglm16
	 * @since December 16,2014
	 * */
	ObjectVO<Warehouse> searchWarehouseByShop(StockGeneralFilter<Warehouse> filter) throws BusinessException;
	
	/**
	 * Load danh sach sp theo don
	 * @author tungmt
	 * @param filter
	 * @date 30/01/2015
	 * */
	ObjectVO<StockTotalVO> getListStockTransDetail(BasicFilter<StockTotalVO> filter) throws BusinessException;

	ObjectVO<StockTotalVO> getListStockTransDetailForDC(BasicFilter<StockTotalVO> filter) throws BusinessException;

	
	/**
	 * Lay kho co do uu tien cao nhat
	 * 
	 * @author lacnv1
	 * @since Feb 14, 2014
	 */
	Warehouse getMostPriorityWareHouse(long shopId, Long productId) throws BusinessException;

}

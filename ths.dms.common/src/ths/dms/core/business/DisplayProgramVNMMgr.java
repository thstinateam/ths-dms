package ths.dms.core.business;

import java.util.Date;

import ths.dms.core.entities.DisplayProgramVNM;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

public interface DisplayProgramVNMMgr {
	DisplayProgramVNM getDisplayProgramVNMByID(Long id) throws BusinessException;
	DisplayProgramVNM getDisplayProgramVNMByCode(String code) throws BusinessException;
	ObjectVO<DisplayProgramVNM> getListDisplayProgramVNM(KPaging<DisplayProgramVNM> kPaging, Long shopId, String code, 
			String name, ActiveType status, Date fDate, Date tDate) throws BusinessException;
}

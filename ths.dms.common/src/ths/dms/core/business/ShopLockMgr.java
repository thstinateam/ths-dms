package ths.dms.core.business;


import java.util.Date;
import java.util.List;

import ths.dms.core.entities.ShopLock;
import ths.dms.core.entities.ShopLockReleasedLog;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.filter.StaffFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopLockLogVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.function.common.PkgFuncGetCloseDayErrorMsg;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;


// TODO: Auto-generated Javadoc
/**
 * The Interface ShopLockMgr.
 */
public interface ShopLockMgr {
	
	/**
	 * Lay ds NV co ban hang trong ngay chot
	 * @param shopId the shop id	 
	 * @throws BusinessException the business exception
	 * @author hoanv25
	 * @since 28/11/2014
	 */
	ObjectVO<StaffVO> getListStockVOFilter(StaffFilter filter) throws BusinessException;
	/**
	 * Creates the shop lock.
	 *
	 * @param shopLock the shop lock
	 * @return the shop lock
	 * @throws BusinessException the business exception
	 */
	ShopLock createShopLock(ShopLock shopLock) throws BusinessException;
	
	/**
	 * Update shop lock vansale.
	 *
	 * @param shopLock the shop lock
	 * @throws BusinessException the business exception
	 */
	void updateShopLockVansale(ShopLock shopLock) throws BusinessException;
	
	/**
	 * Update shop lock.
	 *
	 * @param shopLock the shop lock
	 * @param shopId the shop id
	 * @throws BusinessException the business exception
	 */
	void updateShopLock(ShopLock shopLock, Long shopId) throws BusinessException;
	
	/**
	 * Checks if is shop locked.
	 *
	 * @param shopId the shop id
	 * @return true, if is shop locked
	 * @throws BusinessException the business exception
	 */
	boolean isShopLocked(Long shopId) throws BusinessException;
	
	/**
	 * Checks if is shop locked by code.
	 *
	 * @param shopCode the shop code
	 * @return true, if is shop locked by code
	 * @throws BusinessException the business exception
	 */
	boolean isShopLockedByCode(String shopCode) throws BusinessException;
	
	/**
	 * Lay ngay đã chot theo npp = max lockdate.
	 *
	 * @param shopId the shop id
	 * @return the locked day
	 * @throws BusinessException the business exception
	 * @author lacnv1
	 * @since Mar 10, 2014
	 */
	Date getLockedDay(Long shopId) throws BusinessException;
	
	/**
	 * Lay ngay chot theo npp
	 * tra ve ngay hien tai neu lockDate null.
	 *
	 * @param shopId the shop id
	 * @return the application date
	 * @throws BusinessException the business exception
	 * @author lacnv1
	 * @since Apr 2, 2014
	 */
	Date getApplicationDate(Long shopId) throws BusinessException;
	
	/**
	 * Lay ngay chot theo npp = ngày đã chốt + 1.
	 *
	 * @param shopId the shop id
	 * @return the next locked day
	 * @throws BusinessException the business exception
	 * @author lacnv1
	 * @since Mar 10, 2014
	 */
	Date getNextLockedDay(Long shopId) throws BusinessException;
	
	/**
	 * Chay tien trinh cap nhat ton kho.
	 *
	 * @param lockDate the lock date
	 * @param shopId the shop id
	 * @throws BusinessException the business exception
	 * @author tungtt21
	 */
	void runProcess(Date lockDate, Long shopId) throws BusinessException;
	
	/**
	 * kiem tra co giao dich kho vansale.
	 *
	 * @param shopId the shop id
	 * @param lockDate the lock date
	 * @return the int
	 * @throws BusinessException the business exception
	 * @author tungtt21
	 */
	int checkStockTrans(Long shopId, Date lockDate) throws BusinessException;
	
	/**
	 * Kiem tra co ton tai kho nhan vien khi nhan nut chot ngay.
	 *
	 * @param shopId the shop id
	 * @return the int
	 * @throws BusinessException the business exception
	 * @author tungtt21
	 */
	int checkStockTransVansale(Long shopId) throws BusinessException;
	
	/**
	 * Kiem tra da chot kho nhan vien cua ngay chua.
	 *
	 * @param shopId the shop id
	 * @param lockDate the lock date
	 * @return true, if is stock trans locked
	 * @throws BusinessException the business exception
	 * @author tungtt21
	 */
	boolean isStockTransLocked(Long shopId, Date lockDate) throws BusinessException;
	
	/**
	 * Lay danh sach nhan vien vansale chua tra kho khi chot ngay.
	 *
	 * @param shopId the shop id
	 * @return the stock trans not yet return
	 * @throws BusinessException the business exception
	 * @author tungtt21
	 */
	String getStockTransNotYetReturn(Long shopId) throws BusinessException;
	
	/**
	 * Gets the shop lock van sale.
	 *
	 * @param shopId the shop id
	 * @param lockDate the lock date
	 * @return the shop lock van sale
	 * @throws BusinessException the business exception
	 * @author tungtt21
	 */
	ShopLock getShopLockVanSale(Long shopId, Date lockDate) throws BusinessException;
	
	/**
	 * Lay ds NV co giao dich xuat kho nhan vien trong ngay.
	 *
	 * @param shopId the shop id
	 * @param date the date
	 * @return the list stock trans staff id
	 * @throws BusinessException the business exception
	 * @author lacnv1
	 * @since Sep 12, 2014
	 */
	String getListStockTransStaffId(long shopId, Date date) throws BusinessException;
	
	/**
	 * Lay ds NV chua chot ngay.
	 *
	 * @param lstStaffId the lst staff id
	 * @param date the date
	 * @return the list staff lock stock yet
	 * @throws BusinessException the business exception
	 * @author lacnv1
	 * @since Sep 12, 2014
	 */
	String getListStaffLockStockYet(String lstStaffId, Date date) throws BusinessException;
	
	/**
	 * Lay ds NV co ton kho goc nhin ke toan khac 0.
	 *
	 * @param lstStaffId the lst staff id
	 * @return the list staff stock return yet
	 * @throws BusinessException the business exception
	 * @author lacnv1
	 * @since Sep 12, 2014
	 */
	String getListStaffStockReturnYet(String lstStaffId) throws BusinessException;
	
	/**
	 * Dem so luong don hang trong ngay chua duyet.
	 *
	 * @param shopId the shop id
	 * @param date the date
	 * @param orderTypeGroup the order type group
	 * @return the integer
	 * @throws BusinessException the business exception
	 * @author lacnv1
	 * @since Sep 12, 2014
	 * orderTypeGroup = 1 ->(order_type = IN, SO, CM, CO; approved = 1, approved_step = 3)
	 * orderTypeGroup = 2 ->(order_type = AI, AD; approved = 1, approved_step = 2)
	 * orderTypeGroup = 3 ->(trans_type = DCT, DCG; approved = 1, approved_step = 3)
	 */
	Integer countSaleOrderApprovedYet(long shopId, Date date, int orderTypeGroup) throws BusinessException;
	
	/**
	 * Lay ds shopLock tu fromdate den ngay hien tai.
	 *
	 * @param shopId the shop id
	 * @param fromDate the from date
	 * @return the list shop lock
	 * @throws BusinessException the business exception
	 * @author phuongvm
	 * @since 07/10/2014
	 */
	List<ShopLock> getListShopLock(Long shopId, Date fromDate) throws BusinessException;
	
	/**
	 * delete ds shopLock.
	 *
	 * @param lst the lst
	 * @throws BusinessException the business exception
	 * @author phuongvm
	 * @since 07/10/2014
	 */
	void deleteListShopLock(List<ShopLock> lst) throws BusinessException;
	
	/**
	 * Lay ds NV co ton kho goc nhin ke toan khac 0 (theo npp).
	 *
	 * @param shopId the shop id
	 * @return the list staff stock return yet2
	 * @throws BusinessException the business exception
	 * @author lacnv1
	 * @since Oct 15, 2014
	 */
	String getListStaffStockReturnYet2(long shopId) throws BusinessException;
	
	/**
	 * Gets the min lock date.
	 *
	 * @param shopId the shop id
	 * @return the min lock date
	 * @throws BusinessException the business exception
	 * @author phuongvm
	 * @since 17/10/2014
	 */
	Date getMinLockDate(Long shopId) throws BusinessException;
	
	/**
	 * @author tungmt
	 * @since 23/6/2015
	 * @param filter
	 * @return danh sach shop con va ngay chot cho man hinh chốt ngay 
	 * @throws DataAccessException
	 */
	ObjectVO<ShopVO> getListChildShopForCloseDay(ShopFilter filter) throws BusinessException;
	/**
	 * Lay bang lich su cap nhat ngay chot
	 * 
	 * @author hunglm16
	 * @param shopId
	 * @param lockDate
	 * @return Don vi va cac tinh trang ghi nhan
	 * @throws DataAccessException
	 * @since 01/09/2015 
	 */
	List<ShopLockLogVO> getShopLockLogVOByShopAndLockDate(Long shopId, Date lockDate) throws BusinessException;
	/** 
	 * Dem va Tong cac don loi buoc 1, 2, 3
	 * @author hunglm16
	 * @param shopId
	 * @param date
	 * @param orderTypeGroup
	 * @return
	 * @throws DataAccessException
	 * @since 03/09/2015
	 */
	List<BasicVO> countAndSumSaleOrderApprovedYet(long shopId, Date date, int orderTypeGroup) throws BusinessException;
	
	/**
	 * Lay danh sach don hang loi khi thuc hien chot ngay
	 * 
	 * @author hunglm16
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 * @since 21/10/2015
	 */
	List<PkgFuncGetCloseDayErrorMsg> getErrorForCloseDay(Long shopId) throws BusinessException;
	/**
	 * Cap nhat truong Khong quan tam trong lic su ghi log
	 * @author hunglm16
	 * @param shopId
	 * @param workingDay
	 * @param username
	 * @throws DataAccessException
	 * @since 15/11/2015
	 */
	void updatePassedByClosingDay(long shopId, Date workingDay, String username) throws DataAccessException;
	
	/**
	 * Xu ly getShopLockShopAndLockDate
	 * @author vuongmq
	 * @param shopId
	 * @param lockDate
	 * @return ShopLock
	 * @throws BusinessException
	 * @since Apr 14, 2016
	*/
	ShopLock getShopLockShopAndLockDate(Long shopId, Date lockDate) throws BusinessException;
	
	/**
	 * Xu ly deleteShopLockAndOpen
	 * @author vuongmq
	 * @param shopLock
	 * @param userName
	 * @throws BusinessException
	 * @since Apr 14, 2016
	 */
	void deleteShopLockAndOpen(ShopLock shopLock, String userName) throws BusinessException;
	
	/**
	 * Xu ly getShopLockReleaseLogShopAndReleaseDate
	 * @author vuongmq
	 * @param shopId
	 * @param releaseDate
	 * @return ShopLockReleasedLog
	 * @throws BusinessException
	 * @since Apr 14, 2016
	 */
	ShopLockReleasedLog getShopLockReleaseLogShopAndReleaseDate(Long shopId, Date releaseDate) throws BusinessException;
}

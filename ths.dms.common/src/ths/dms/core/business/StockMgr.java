package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockLock;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.StockTransLot;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTotalVOFilter;
import ths.dms.core.entities.enumtype.StockTransType;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductAmountVO;
import ths.dms.core.entities.vo.ProductLotVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransDetailVO;
import ths.dms.core.entities.vo.StockTransLotVO;
import ths.dms.core.entities.vo.StockTransVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_Stock;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface StockMgr {

	StockTrans createStockTrans(StockTrans stockTrans) throws BusinessException;

	void updateStockTrans(StockTrans stockTrans) throws BusinessException;

	void deleteStockTrans(StockTrans stockTrans) throws BusinessException;

	StockTrans getStockTransById(Long id) throws BusinessException;

	StockTrans getStockTransByCode(String code) throws BusinessException;

	StockTrans getStockTransByCodeAndShop(String code, Long shopId) throws BusinessException;

	/**
	 * Lay kho cua NPP theo ma
	 * 
	 * @author tientv11
	 * @param code
	 * @return
	 * @throws BusinessException
	 */
	Warehouse getWarehouseByCode(String code, Long shopId) throws BusinessException;

	void deleteStockTransDetail(StockTransDetail stockTransDetail) throws BusinessException;

	StockTransDetail getStockTransDetailById(Long id) throws BusinessException;

	StockTotal createStockTotal(StockTotal stockTotal) throws BusinessException;

	void updateStockTotal(StockTotal stockTotal) throws BusinessException;

	void deleteStockTotal(StockTotal stockTotal) throws BusinessException;

	StockTransDetail createStockTransDetail(StockTransDetailVO stockTransDetailVO, Integer fifo, Long shopId) throws BusinessException;

	/***
	 * @author vuongmq
	 * @date 11/08/2015
	 * @description Lay getStockTotalByEntities theo filter
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	StockTotal getStockTotalByEntities(StockTotalFilter filter) throws BusinessException;

	StockTotal getStockTotalByProductAndOwner(long productId, StockObjectType ownerType, long ownerId, Long warehouseId) throws BusinessException;

	ObjectVO<StockTrans> getListStockTrans(KPaging<StockTrans> kPaging, Date fromDate, Date toDate, Long shopId, StockTransType stockTransType) throws BusinessException;

	StockTransLot createStockTransLot(StockTransLot stockTransLot) throws BusinessException;

	void updateStockTransLot(StockTransLot stockTransLot) throws BusinessException;

	void deleteStockTransLot(StockTransLot stockTransLot) throws BusinessException;

	StockTransLot getStockTransLotById(Long id) throws BusinessException;

	ObjectVO<StockTransLot> getListStockTransLotByStockTransId(KPaging<StockTransLot> kPaging, long stockTransId) throws BusinessException;

	ProductLot createProductLot(ProductLot productLot) throws BusinessException;

	void updateProductLot(ProductLot productLot) throws BusinessException;

	void deleteProductLot(ProductLot productLot) throws BusinessException;

	ProductLot getProductLotById(Long id) throws BusinessException;

	List<Boolean> checkIfProductHasEnough(List<ProductAmountVO> lstProductAmountVO) throws BusinessException;

	//String generateStockTransCode() throws BusinessException;

	ProductLot getProductLotByCodeAndOwnerAndProduct(String lot, long ownerId, StockObjectType ownerType, long productId) throws BusinessException;

	ObjectVO<StockTransVO> getListStockTransVO(KPaging<StockTransVO> kPaging, Date fromDate, Date toDate, Long shopId, StockTransType stockTransType) throws BusinessException;

	ObjectVO<ProductLot> getListProductLot(KPaging<ProductLot> kPaging, Long ownerId, StockObjectType ownerType, Integer quantity, ActiveType status) throws BusinessException;

	ObjectVO<ProductLotVO> getListProductLotVO(KPaging<ProductLotVO> kPaging, Long ownerId, StockObjectType ownerType, Integer quantity, ActiveType status) throws BusinessException;

	ObjectVO<StockTotal> getListStockTotal(KPaging<StockTotal> kPaging, String productCode, String productName, String promotionProgramCode, Long ownerId, StockObjectType ownerType, Long catId, Long subCatId, Integer fromQuantity,
			Integer toQuantity, Long cycleCountId) throws BusinessException;

	void test() throws BusinessException;

	ObjectVO<StockTransLotVO> getListStockTransLotVO(KPaging<StockTransLotVO> kPaging, long stockTransId) throws BusinessException;

	ObjectVO<ProductLotVO> getListProductInStock(KPaging<ProductLotVO> kPaging, Long shopId, StockObjectType shopType, Long ownerId, StockObjectType ownerType, Integer quantity, ActiveType status, Integer fifo) throws BusinessException;

	/**
	 * 
	 * nhap kho
	 * 
	 * @author: hungnm -> thanhnn
	 * @param stockTrans
	 * @param lstStockTransDetailVO
	 * @param fifo
	 *            - 1 or null: FIFO, 2: LIFO
	 * @return
	 * @throws BusinessException
	 * @return: Boolean
	 * @throws:
	 */
	Long createListStockTransDetail(StockTrans stockTrans, List<StockTransDetailVO> lstStockTransDetailVO, Integer fifo, Long shopId) throws BusinessException;

	Long createStockTransForUpdated(StockTrans stockTrans, List<StockTransDetailVO> lstStockTransDetailVO, Integer fifo, Long shopId) throws BusinessException;

	StockTotal getStockTotalByProductCodeAndOwner(String productCode, StockObjectType ownerType, Long ownerId) throws BusinessException;

	StockTotalVO getStockTotalByProductCodeAndOwnerEx(String productCode, StockObjectType ownerType, Long ownerId) throws BusinessException;

	/**
	 * Lay du lieu lien quan den kho; Filter theo bo loc voi cac dieu kien can
	 * thiet
	 * 
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @author tientv11
	 */
	ObjectVO<StockTotalVO> getListStockTotalVO(StockTotalFilter filter) throws BusinessException;

	/**
	 * Danh sach san pham dung cho nhap lieu tren man hinh
	 * 
	 * @author tientv11
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	List<StockTotalVO> getListProductImportExport(StockTotalFilter filter) throws BusinessException;

	/**
	 * 
	 * lay danh sach product lot theo shop and product id
	 * 
	 * @author: thanhnn
	 * @param kPaging
	 * @param ownerId
	 * @param ownerType
	 * @param quantity
	 * @param status
	 * @param productId
	 * @return
	 * @throws BusinessException
	 * @return: ObjectVO<ProductLot>
	 * @throws:
	 */
	ObjectVO<ProductLot> getListProductLotWithProductID(KPaging<ProductLot> kPaging, Long ownerId, StockObjectType ownerType, Integer quantity, ActiveType status, Long productId) throws BusinessException;

	ObjectVO<StockTotalVO> getListStockTotalVOForImExport(KPaging<StockTotalVO> kPaging, String productCode, String productName, String promotionProgramCode, Long ownerId, StockObjectType ownerType, Long catId, Long subCatId, Integer fromQuantity,
			Integer toQuantity, List<Long> exceptProductId) throws BusinessException;

	List<ProductLot> getListProductLotWithHandleApparms(Long ownerId, StockObjectType ownerType, Integer quantity, ActiveType status, Long productId) throws BusinessException;

	ObjectVO<ProductLotVO> getListProductForVanSale(KPaging<ProductLotVO> kPaging, Long ownerId, StockObjectType ownerType) throws BusinessException;

	ObjectVO<ProductLotVO> getListProductVanSale(KPaging<ProductLotVO> kPaging, String productCode, String productName, Long ownerId, StockObjectType ownerType, Integer quantity, ActiveType status) throws BusinessException;

	List<StockTransLotVO> getListLot(Long fromOwnerId, StockObjectType fromOwnerType, Long toOwnerId, StockObjectType toOwnerType, Date stockTransDate) throws BusinessException;

	/**
	 * Kiem tra lot cua product het hang hay chua
	 * 
	 * @param productId
	 * @param productLot
	 * @param objectId
	 * @param objectType
	 * @return
	 * @throws BusinessException
	 */
	boolean isNotProductLotExpide(Long productId, String productLot, Long objectId, StockObjectType objectType) throws BusinessException;

	ObjectVO<StockTotalVO> getListProductForImExport(KPaging<StockTotalVO> kPaging, String productCode, String productName, Long objectId, StockObjectType objectType, Integer fromQuantity) throws BusinessException;

	/**
	 * 
	 * @author tientv
	 * @throws BusinessException
	 * @param shopId
	 * @description : Danh sach san pham ma co the ban cho npp, bao gom ca ton
	 *              kho
	 */
	List<StockTotalVO> getListProductsCanSalesForShop(Long shopId) throws BusinessException;

	/**
	 * @author cangnd
	 * @param staffCode
	 * @param shopID
	 * @return
	 * @throws BusinessException
	 */
	List<StockTrans> getListStockTrans(String staffCode, Long shopID) throws BusinessException;

	/**
	 * @author cangnd
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	public List<Staff> getListStaffTrans(Long shopId) throws BusinessException;

	ObjectVO<StockTotal> getListStockTotalNew(KPaging<StockTotal> kPaging, StockTotalVOFilter stockTotalVoFilter) throws BusinessException;

	ObjectVO<StockTotalVO> getListStockTotalVONew(KPaging<StockTotalVO> kPaging, StockTotalVOFilter stockTotalVoFilter) throws BusinessException;

	ObjectVO<StockTotal> getListStockTotalNewQuantity(KPaging<StockTotal> kPaging, StockTotalVOFilter stockTotalVoFilter) throws BusinessException;

	String generateStockTransCode(Long shopId) throws BusinessException;

	StockLock getStockLock(Long shopId, Long staffId) throws BusinessException;

	String generateStockTransCodeVansale(Long shopId, String transType) throws BusinessException;

	List<StockLock> getListStockLockByListStaffId(List<Long> lstStaffId, Long shopId) throws BusinessException;

	void updateStockLock(StockLock stockLock) throws BusinessException;

	void insertStockLock(StockLock stockLock) throws BusinessException;

	StockLock getStockLockByStaffCode(String staffCode, Long shopId) throws BusinessException;

	/**
	 * Lay danh sach kho theo dieu kien
	 * 
	 * @author tientv11
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	List<Warehouse> getListWarehouseByFilter(StockTotalFilter filter) throws BusinessException;

	/**
	 * In phieu xuat kho kiem van chuyen noi bo
	 * 
	 * @author vuonghn
	 */
	RptPXKKVCNB_Stock getPXKKVCNB(Long shopId, String staffCode, Long stockTransId, String transacName, String transacContent) throws BusinessException;

	/**
	 * @author nhanlt6
	 * @throws DataAccessException
	 * @param shopId
	 * @param productId
	 * @param saleOrderId
	 * @description : Danh sach so luong san pham ban cho npp load theo kho
	 */
	List<StockTotalVO> getListProductsCanSalesForShopByWarehouse(Long shopId, Long productId, Long saleOrderId) throws BusinessException;

	List<StockTrans> getListStockTransById(List<Long> listId) throws BusinessException;

	List<StockTrans> getListStockTransByFromId(Long fromId) throws BusinessException;

	/**
	 * Lay danh sach StockTotal theo filter co order by
	 * @author trietptm
	 * @param stockTotalFilter
	 * @return List<StockTotal> da order by 
	 * @throws BusinessException
	 * @since Dec 16, 2015
	*/
	List<StockTotal> getListStockTotalByProductAndOwner(StockTotalFilter stockTotalFilter) throws BusinessException;

	/**
	 * Lay ds sp trong kho
	 * 
	 * @author lacnv1
	 * @since Nov 03, 2014
	 */
	ObjectVO<StockTotalVO> getListProductForCycleCount(KPaging<StockTotalVO> paging, long cycleCountId, StockTotalVOFilter filter) throws BusinessException;

	/**
	 * Lay ds quan ly kho
	 * 
	 * @author hoanv25
	 * @since Dec 11, 2014
	 */
	ObjectVO<StockTransVO> getListStockTransVOFilter(StockStransFilter filter) throws BusinessException;

	/**
	 * Lay ds chon mat hang
	 * 
	 * @author hoanv25
	 * @since Dec 14, 2014
	 */
	ObjectVO<StockTotalVO> getListProductForCycleCountCategoryStock(KPaging<StockTotalVO> paging, long cycleCountId, StockTotalVOFilter filter) throws BusinessException;

	/**
	 * Tao moi kho nhan vien banhang vansale
	 * 
	 * @author hoanv25
	 */
	StockLock createStockLock(StockLock stl) throws BusinessException;

	/**
	 * Tim kiem danh sach kho nhan vien
	 * 
	 * @author hoanv25
	 * @param filter
	 */
	StockLock getStockLockByFilter(StockLock filter) throws BusinessException;

	/**
	 * Lay danh sach ton kho cua sp trong kho nha phan phoi, co cong lai so
	 * luong da dat trong don hang neu co sale_order_lot
	 * 
	 * @author lacnv1
	 * @param StockTotalFilter
	 * @return
	 * @throws BusinessException
	 */
	List<StockTotal> getListStockTotalByProductAndShop(StockTotalFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 13/5/2015
	 * @param kPaging
	 * @param stockTransId
	 * @return Danh sach san pham dieu chinh cho man hinh nhap xuat kho dieu
	 *         chinh
	 * @throws DataAccessException
	 */
	ObjectVO<StockTransLotVO> getListStockTransLotVOForStockUpdate(KPaging<StockTransLotVO> kPaging, long stockTransId) throws BusinessException;

	/**
	 * lay danh sach ton kho theo the kiem ke
	 * 
	 * @param cycleCount
	 * @return
	 * @throws DataAccessException
	 */
	List<StockTotal> getListStockTotalByCycleCount(CycleCount cycleCount) throws BusinessException;

	/**
	 * Kiem tra nhan vien con don SO, CO chua cap nhat phai thu va kho hay
	 * khong?
	 * 
	 * @param shopId
	 *            - id npp cua nhan vien
	 * @param staffId
	 *            - id nhan vien
	 * @param pDate
	 *            - ngay can kiem tra
	 * 
	 * @return integer = so don (SO, CO) chua cap nhat phai thu va kho (cach
	 *         nhau dau phay)
	 * 
	 * @author lacnv1
	 * @since Apr 21, 2015
	 */
	int countVansaleOrderNotUpdatedStock(long shopId, long staffId, Date pDate) throws BusinessException;

	/**
	 * Lay danh sach kho kiem ke thiet bi u ke theo dieu kien
	 * 
	 * @author phuongvm
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	List<Warehouse> getListWarehouseByFilterChecking(StockTotalFilter filter) throws BusinessException;

	/**
	 * @author trietptm
	 * @param kPaging
	 * @param stockTransId
	 * @return Danh sach san pham dieu chinh cho man hinh SearchSaleTransaction
	 * @throws BusinessException
	 */
	ObjectVO<StockTransLotVO> getListStockTransLotVOForSearchSale(KPaging<StockTransLotVO> kPaging, long stockTransId) throws BusinessException;

}

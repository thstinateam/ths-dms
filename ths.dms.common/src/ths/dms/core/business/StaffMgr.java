package ths.dms.core.business;

import java.util.Date;
import java.util.List;
import java.util.SortedMap;

import ths.dms.core.entities.ActionLog;
import ths.dms.core.entities.ParentStaffMap;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffGroup;
import ths.dms.core.entities.StaffGroupDetail;
import ths.dms.core.entities.StaffPositionLog;
import ths.dms.core.entities.StaffSaleCat;
import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffFilterNew;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.filter.StaffTypeFilter;
import ths.dms.core.entities.filter.SupFilter;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ParentStaffMapVO;
import ths.dms.core.entities.vo.ShopTreeVO;
import ths.dms.core.entities.vo.StaffComboxVO;
import ths.dms.core.entities.vo.StaffExportVO;
import ths.dms.core.entities.vo.StaffGroupDetailVO;
import ths.dms.core.entities.vo.StaffListPositionVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.StaffSimpleVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.UnitFilter;

public interface StaffMgr {

	Staff createStaff(Staff staff, LogInfoVO logInfo) throws BusinessException;

	void updateStaff(Staff staff, LogInfoVO logInfo) throws BusinessException;

	void deleteStaff(Staff staff, LogInfoVO logInfo) throws BusinessException;

	ParentStaffMap createParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws BusinessException;

	void createListParentStaffMap(Long staffId, List<Long> lstParentStaffId, LogInfoVO logInfo) throws BusinessException;

	void updateParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws BusinessException;

	void deleteParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws BusinessException;

	Staff getStaffByCode(String code) throws BusinessException;

	Staff getStaffLogin(String code, String password) throws BusinessException;

	Boolean isUsingByOthers(int staffId) throws BusinessException;

	Staff getStaffById(Long id) throws BusinessException;

	StaffSaleCat createStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws BusinessException;

	void updateStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws BusinessException;

	void deleteStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws BusinessException;

	StaffSaleCat getStaffSaleCatById(Long id) throws BusinessException;

	ObjectVO<StaffSaleCat> getListStaffSaleCat(KPaging<StaffSaleCat> kPaging, Long staffId) throws BusinessException;

	Boolean checkIfRecordExist(long staffId, Long catId, Long id) throws BusinessException;

	Staff getStaffByRoutingCustomer(Long customerId) throws BusinessException;

	Boolean checkIfStaffExists(String email, String mobiphone, String idno, Long id) throws BusinessException;

	ObjectVO<Staff> getListSupervisorByShop(KPaging<Staff> kPaging, String staffCode, String staffName, String childShopCode) throws BusinessException;

	ObjectVO<Staff> getListStaffEx(KPaging<Staff> kPaging, String staffCode, String staffTypeCode, String staffName, String mobilePhoneNum, ActiveType status, String shopCode, StaffObjectType staffType, ChannelTypeType channelTypeType,
			Integer channelObjectType) throws BusinessException;

	/**
	 * @author hungnm
	 * @param staffId
	 * @return
	 * @throws BusinessException
	 */
	Boolean checkIfStaffManageAnySaleMan(Long staffId) throws BusinessException;

	/**
	 * @author hungnm
	 * @param staffOwnerId
	 * @throws BusinessException
	 */
	void updateSaleManagerToNull(Long staffOwnerId) throws BusinessException;

	/**
	 * @author hungnm
	 * @param staffOwnerId
	 * @param lstStaffId
	 * @throws BusinessException
	 */
	void updateSaleManager(Long staffOwnerId, List<Long> lstStaffId) throws BusinessException;

	/**
	 * @author hungnm
	 * @param kPaging
	 * @param ownerId
	 * @param shopCode
	 * @param shopName
	 * @param staffCode
	 * @param staffName
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<Staff> getListStaffByOwnerId(KPaging<Staff> kPaging, Long ownerId, String shopCode, String shopName, String staffCode, String staffName) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param staffOwnerCode
	 * @param staffCode
	 * @param staffName
	 * @param staffObjectType
	 * @return
	 * @throws DataAccessException
	 * @throws BusinessException
	 */
	List<Staff> getListStaffByOwner(String staffOwnerCode, String staffCode, String staffName, StaffObjectType staffObjectType) throws BusinessException;

	/**
	 * truyen vao list channel object type
	 * 
	 * @author hungnm
	 * @param kPaging
	 * @param staffCode
	 * @param staffTypeCode
	 * @param staffName
	 * @param mobilePhoneNum
	 * @param status
	 * @param shopCode
	 * @param staffType
	 * @param channelTypeType
	 * @param lstChannelObjectType
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<Staff> getListStaffEx2(StaffFilter filter) throws BusinessException;

	Boolean checkStaffInShopOldFamily(String staffCode, String shopCode) throws BusinessException;

	StaffSaleCat getStaffSaleCat(Long staffId, Long productInfoId) throws BusinessException;

	ObjectVO<Staff> getListStaffNotManagedByOwnerId(KPaging<Staff> kPaging, Long ownerId, String shopCode, String shopName, String staffCode, String staffName, Long parentShopId) throws BusinessException;

	void updateOwnerIdToNull(Long staffOwnerId) throws BusinessException;

	String generateStaffCode() throws BusinessException;

	ObjectVO<Staff> getListStaffByShopCode(KPaging<Staff> kPaging, String shopCode) throws BusinessException;

	/**
	 * Lay ds nhan vien cung quan ly 1 tuyen tai cung mot thoi diem
	 * @author trietptm
	 * @param filter
	 * @return ds nhan vien cung quan ly 1 tuyen tai cung mot thoi diem
	 * @throws BusinessException
	 * @since Nov 26, 2015
	 */
	List<Staff> getSaleStaffByDate(RoutingCustomerFilter filter) throws BusinessException;

	Staff getSaleStaffByDateForCreateOrder(Long customerId, Long shopId, Date orderDate,Long staffId) throws BusinessException;
	
	ObjectVO<Staff> getListStaff(StaffFilter filter) throws BusinessException;

	ObjectVO<StaffVO> getListStaffVO(StaffFilter filter) throws BusinessException;

	ObjectVO<Staff> getListStaffEx1(KPaging<Staff> kPaging, String staffCode, String staffTypeCode, String staffName, String mobilePhoneNum, ActiveType status, String shopCode, StaffObjectType staffType, String saleTypeCode, String shopName)
			throws BusinessException;

	Boolean checkStaffPhoneExists(String phoneNum) throws BusinessException;

	ObjectVO<Staff> getListStaffWithFullCondition(StaffFilter filter) throws BusinessException;

	/**
	 * 
	 * lay danh sach nhan vien o nghiep vu giam sat nha phan phoi
	 * 
	 * @author: thanhnn
	 * @param kPaging
	 * @param staffCode
	 * @param staffName
	 * @param shopCode
	 * @param status
	 * @param type
	 * @param listObjectType
	 * @param isHasChild
	 *            - true neu lay tat ca cac cap shop con
	 * @return
	 * @throws BusinessException
	 * @return: ObjectVO<StaffVO>
	 * @throws:
	 */
	ObjectVO<StaffVO> getListStaffForSuperVisorWithCondition(KPaging<StaffVO> kPaging, String staffCode, String staffName, String shopCode, ActiveType status, ChannelTypeType type, List<StaffObjectType> listObjectType, Boolean isHasChild)
			throws BusinessException;

	/**
	 * Lay danh sach nhan vien cua shop
	 * 
	 * @author thuattq
	 */
	ObjectVO<Staff> getListStaffByShop(KPaging<Staff> kPaging, Long shopId, ActiveType activeType, List<StaffObjectType> objectType) throws BusinessException;

	/**
	 * 
	 * lay danh sach gsnpp theo shop dua vao staff_id
	 * 
	 * @author: thanhnn
	 * @param kPaging
	 * @param staffCode
	 * @param staffName
	 * @param shopCode
	 * @param isHasChild
	 * @return
	 * @throws BusinessException
	 * @return: List<Staff>
	 * @throws:
	 */
	ObjectVO<Staff> getListSupervisorAllowShop(KPaging<Staff> kPaging, String staffCode, String staffName, String shopCode, Boolean isHasChild) throws BusinessException;

	/**
	 * 
	 * lay danh sach staff theo gsnpp va shop
	 * 
	 * @author: thanhnn
	 * @param kPaging
	 * @param staffOwnerCode
	 * @param staffCode
	 * @param staffName
	 * @param shopCode
	 * @param isHasChild
	 * @return
	 * @throws BusinessException
	 * @return: ObjectVO<Staff>
	 * @throws:
	 */
	ObjectVO<Staff> getListStaffAllowOwnerAndShop(KPaging<Staff> kPaging, String staffOwnerCode, String staffCode, String staffName, String shopCode, Boolean isHasChild) throws BusinessException;

	ObjectVO<Staff> getListPreAndVanStaff(KPaging<Staff> kPaging, String staffCode, String staffName, Long shopId, ActiveType staffActiveType, List<StaffObjectType> channelType) throws BusinessException;

	ObjectVO<Staff> getListNVGS(KPaging<Staff> kPaging, String staffCode, String staffName, Long shopId, List<String> listTBHVCode) throws BusinessException;

	ObjectVO<Staff> getListNVGS2(KPaging<Staff> kPaging, Long shopId, String lstStaffCode) throws BusinessException;

	ObjectVO<Staff> getListNVGS3(KPaging<Staff> kPaging, String strListShopId, String staffCode, String staffName, String lstStaffCode) throws BusinessException;

	ObjectVO<Staff> getListStaffEx5(StaffFilter filter) throws BusinessException;

	ObjectVO<Staff> getListStaffExceptListStaff(KPaging<Staff> kPaging, String staffCode, String staffName, ActiveType status, String shopCode, Long staffTypeId, List<Long> exceptStaffIds) throws BusinessException;

	ObjectVO<Staff> getListNVBH(KPaging<Staff> kPaging, String staffCode, String staffName, List<String> lstStaffOwnerCode, Long shopId, Boolean isGetChildShop, List<String> lstTBHVCode) throws BusinessException;

	ObjectVO<StaffComboxVO> getListNVBHisShop(KPaging<StaffComboxVO> kPaging, String staffCode, String staffName, List<String> lstStaffOwnerCode, Long shopId, Boolean isGetChildShop, List<String> lstTBHVCode) throws BusinessException;

	ObjectVO<Staff> getListNVBH2(KPaging<Staff> kPaging, String staffCode, String staffName, List<String> lstStaffOwnerCode, String strListShopId, Boolean isGetChildShop, List<String> lstTBHVCode) throws BusinessException;

	ObjectVO<Staff> getListSupervisorByShopAndAncestor(KPaging<Staff> kPaging, String staffCode, String staffName, String childShopCode) throws BusinessException;

	ObjectVO<Staff> getListNVGH(KPaging<Staff> kPaging, Long shopId, String customerShortCode) throws BusinessException;

	ObjectVO<Staff> getListStaffForAbsentReport(KPaging<Staff> kPaging, String staffCode, String staffName, Long shopId) throws BusinessException;

	ObjectVO<Staff> getListPreAndVanStaffHasChild(KPaging<Staff> kPaging, String staffCode, String staffName, List<Long> shopIds, ActiveType staffActiveType, List<StaffObjectType> channelType, Boolean isHasChild, List<Long> listStaffOwnerIds)
			throws BusinessException;

	StaffGroup createStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws BusinessException;

	void deleteStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws BusinessException;

	void updateStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws BusinessException;

	StaffGroup getStaffGroupById(long id) throws BusinessException;

	ObjectVO<StaffGroup> getListStaffGroup(KPaging<StaffGroup> kPaging, Long shopId, ActiveType activeType) throws BusinessException;

	StaffGroupDetail checkIfStaffExistsInStaffGroup(Long staffId, Long staffGroupId, ActiveType activeType) throws BusinessException;

	StaffGroupDetail createStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws BusinessException;

	void deleteStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws BusinessException;

	void updateStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws BusinessException;

	StaffGroupDetail getStaffGroupDetailById(long id) throws BusinessException;

	ObjectVO<StaffGroupDetail> getListStaffGroupDetail(KPaging<StaffGroupDetail> kPaging, Long staffGroupId, ActiveType activeType) throws BusinessException;

	ObjectVO<Staff> getListStaffInStaffGroup(KPaging<Staff> paging, Long staffGroupId, ActiveType activeType) throws BusinessException;

	void removeStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws BusinessException;

	ObjectVO<ShopTreeVO> getListShopManagedByStaff(Long staffId, KPaging<ShopTreeVO> kPaging) throws BusinessException;

	void removeStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws BusinessException;

	void removeStaff(Staff staff, LogInfoVO logInfo) throws BusinessException;

	List<StaffGroupDetail> getListStaffGroupDetailByStaff(Long staffId) throws BusinessException;

	Boolean checkStaffExistsInShopWithStaffGroup(long shopId) throws BusinessException;

	ObjectVO<Staff> getListNVGSNew(KPaging<Staff> paging, String staffCode, String staffName, Long vungId, Long shopId, List<Long> lstExceptId, SortedMap<Long, List<Long>> deletedStaffByVung) throws BusinessException;

	ObjectVO<StaffGroupDetail> getListStaffByGroupDetail(StaffFilter filter) throws BusinessException;

	List<StaffPositionVO> getListStaffPositionWithTraining(Long shopId) throws BusinessException;

	List<StaffPositionVO> getListStaffPosition(Long shopId, Long staffId, Boolean isVNM, Boolean isMien, Boolean isVung, Boolean isNPP, Boolean oneNode) throws BusinessException;

	/**
	 * @modify hunglm16
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @since 24/11/2015
	 * @description ra soat phan quyen
	 */
	List<StaffPositionVO> getListStaffPosition(SupFilter filter) throws BusinessException;

	List<ParentStaffMapVO> getListParentStaffMapVO(SupFilter filter) throws BusinessException;

	Staff getGDMByShop(Long shopId) throws BusinessException;

	List<StaffPositionLog> getListStaffPosition2(Long userId, Boolean getTbhv, Boolean getGsnpp, Boolean getNvbh) throws BusinessException;

	Staff getStaffOnwerOfShop(Long shopId) throws BusinessException;

	Boolean checkStaffAvailableLatLng(Long staffId) throws BusinessException;

	TreeVO<StaffPositionVO> searchStaff(SupFilter filter) throws BusinessException;

	List<StaffPositionVO> getListStaffPosition1(Long shopId, Long staffOwnerId, StaffObjectType roleType, Boolean oneNode) throws BusinessException;

	ObjectVO<Staff> getListNVGSByTBHVId(KPaging<Staff> kPaging, Long tbhvId) throws BusinessException;

	/**
	 * Tim kiem nhan vien them moi trong NVPTKH (CTTB)
	 * 
	 * @author thongnm
	 */
	ObjectVO<Staff> getListNVBHForDisplayProgram(KPaging<Staff> kPaging, Long shopId, Long displayProgramId, String code, String name) throws BusinessException;

	/**
	 * Cap nhat cay don vi
	 * 
	 * @author thongnm
	 */

	void saveTree(List<String> lstStaffAddId, List<Long> lstStaffShopAddId, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Cap nhat cay don vi
	 * 
	 * @author thongnm
	 */

	Long saveTree(List<String> lstGroupDelete, List<String> lstStaffDeleteId, List<String> lstGroupIdChange, List<String> lstGroupIdAdd, List<String> lstGroupNameAdd, List<String> lstGroupShopAdd, List<String> lstStaffAddId,
			List<String> lstGroupNameChange, List<String> lstStaffGroupDeleteId, List<String> lstStaffGroupAddId, String idGroupAddNewStaff, LogInfoVO logInfo) throws BusinessException;

	StaffPositionVO getStaffPosition(Long staffId) throws BusinessException;

	ObjectVO<Staff> getListStaffForExportExcel(StaffFilter filter) throws BusinessException;

	Staff getStaffByCodeOfUserLogin(String code, Long parentId) throws BusinessException;

	List<Staff> getListStaffByShop(List<Long> lstShopId, ActiveType staffActiveType, List<StaffObjectType> objectTypes) throws BusinessException;

	/**
	 * 
	 * @param kPaging
	 * @param lstShopId
	 * @param staffName
	 * @param staffCode
	 * @param staffActiveType
	 * @param objectTypes
	 * @return
	 * @throws BusinessException
	 * @author vuonghn
	 */
	ObjectVO<Staff> getListStaffByShop(KPaging<Staff> kPaging, List<Long> lstShopId, String staffName, String staffCode, ActiveType staffActiveType, List<StaffObjectType> objectTypes) throws BusinessException;

	Staff getStaffByCodeAndShopId(String staffCode, Long shopId) throws BusinessException;
	
	Staff getStaffByCodeAndListShop(String staffCode, String lstLshopId) throws BusinessException;

	Staff getStaffByCodeAndShopId1(String staffCode, Long shopId) throws BusinessException;

	TreeVO<StaffPositionVO> searchStaffEx(SupFilter filter) throws BusinessException;

	List<StaffPositionVO> getListStaffPositionOptionsDate(SupFilter filter) throws BusinessException;

	StaffListPositionVO getStaffPositionEx(SupFilter filter) throws BusinessException;

	List<Staff> getListStaffByShopIdWithRunning(Long shopId) throws BusinessException;

	List<Staff> getStaffMultilChoice(Long staffId, String staffCode, Long shopId, Integer status) throws BusinessException;

	List<StaffSimpleVO> getListStaffByShopCombo(List<Long> lstShopId, ActiveType staffActiveType, List<StaffObjectType> objectTypes) throws BusinessException;

	Staff getStaffByInfoBasic(String code, Long shopId, ActiveType status) throws BusinessException;

	ObjectVO<Staff> getListNVGS4(KPaging<Staff> kPaging, String strListShopId, String staffCode, String staffName) throws BusinessException;

	/**
	 * Tam ngung tat ca cac visit_plan cua NVBH trong shop
	 * 
	 * @author lacnv1
	 * @since Jun 10, 2014
	 */
	void offVisitPlanOfStaff(long staffId, long shopId, String userCode) throws BusinessException;

	/**
	 * Lay danh sach NV popup cay don vi
	 * 
	 * @author lacnv1
	 * @since Jun 19, 2014
	 */
	ObjectVO<Staff> getListStaffPopup(StaffFilter filter) throws BusinessException;

	/**
	 * Lay danh sach NV (cay don vi)
	 * 
	 * @author lacnv1
	 * @since Jun 19, 2014
	 */
	ObjectVO<Staff> getListStaffOfShop(StaffFilter filter) throws BusinessException;

	/**
	 * Lay danh sach NV (cay don vi - export)
	 * 
	 * @author lacnv1
	 * @since Jun 19, 2014
	 */
	ObjectVO<StaffExportVO> getListExportStaffOfShop(StaffFilter filter) throws BusinessException;

	/**
	 * Lay danh sach NV (cay don vi)
	 * 
	 * @author lacnv1
	 * @since Jun 19, 2014
	 */
	ObjectVO<StaffGroupDetailVO> getListStaffGroupOfUnitTree(StaffFilter filter, KPaging<StaffGroupDetailVO> paging) throws BusinessException;

	/**
	 * Tam ngung nhan vien (khong xoa staff_owner_id)
	 * 
	 * @author lacnv1
	 * @since Jun 22, 2014
	 */
	void stopStaff(Staff staff, LogInfoVO logInfo) throws BusinessException;

	/*
	 * start Tich hop theo Phan quyen CMS
	 * 
	 * Bo sung tat ca cac ham moi vao phan vung nay *
	 */

	/**
	 * search Staff by Filter
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * @description Tim kiem dang Like '%%', in ()
	 * */
	ObjectVO<StaffVO> searchListStaffVOByFilter(StaffPrsmFilter<StaffVO> filter) throws BusinessException;
	
	/**
	 * search Staff by Filter
	 * 
	 * @author longnh15
	 * @since 25/05/2015
	 * @description Danh sach NVBH, NVGS cua NPP
	 * */
	ObjectVO<StaffVO> searchListStaffVOByShop(StaffPrsmFilter<StaffVO> filter) throws BusinessException;

	/**
	 * get Staff by Filter
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * @description Tim kiem dang Like, =
	 * */
	ObjectVO<StaffVO> getListStaffVOByFilter(StaffPrsmFilter<StaffVO> filter) throws BusinessException;

	/**
	 * get Staff by Filter
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * @description Tim kiem dang Like, =; ShopRoot
	 * */
	ObjectVO<StaffVO> getListStaffVOByShopId(BasicFilter<StaffVO> filter) throws BusinessException;

	///The end Tich hop theo Phan quyen CMS
	ObjectVO<StaffVO> getListNVGSAdapter(String shopId, Long staffRoot) throws BusinessException;

	List<Staff> getListStaffYMamagerStaffX(Long staffIdY, Long staffIdX) throws BusinessException;

	/**
	 * @author hunglm16
	 * @since September
	 * @description filter.getUsuerId is not null, filter.getShopId is not null
	 * */
	Staff getStaffByFilterWithCMS(BasicFilter<Staff> filter) throws BusinessException;

	List<Staff> getListStaffByShop(StaffFilter filter) throws BusinessException;

	/**
	 * Lay danh sach Nhan vien va con chau voi phan quyen Shop tuong ung
	 * 
	 * @author hunglm16
	 * @since October 9,2014
	 * */
	ObjectVO<StaffVO> getListStaffVOAndChilrentByShopId(BasicFilter<StaffVO> filter) throws BusinessException;

	/**
	 * @author hunglm16
	 * @since September
	 * @description Ham ho tro,regex la dau phau (,); filter.getParentStaffId()
	 *              is not null
	 * @param filter
	 *            .getInheritUserPriv(), filter.getLstShopId(),
	 *            filter.getIsLstChildStaffRoot(): Nhung tham so bat buoc
	 * */
	String returnStringGetArrayStaffCodeByArrCode(StaffPrsmFilter<StaffVO> filter, String arrStaffCodeStr) throws BusinessException, DataAccessException;

	/**
	 * @author hunglm16
	 * @since September
	 * @description filter.getInheritUserPriv() is not null,
	 *              filter.getArrLongS() is not null
	 * */
	List<String> returnLstStringGetArrayLstStaffCode(StaffPrsmFilter<StaffVO> filter, List<String> lstShortCode) throws BusinessException, DataAccessException;

	/**
	 * @author hunglm16
	 * @since September
	 * @description Ham ho tro,regex la dau phau (,); filter.getParentStaffId()
	 *              is not null
	 * */
	String returnStringGetArrayStaffCodeByArrId(StaffPrsmFilter<StaffVO> filter, String arrStaffIdStr) throws BusinessException, DataAccessException;

	ObjectVO<Staff> getListStaffNew(StaffFilterNew filter) throws BusinessException;
	/**
	 * Kiem tra cha con trong parent staff map
	 * 
	 * @author hunglm16
	 * @since October 23 ,2014
	 * */
	Boolean checkParentChildrentByDoubleStaffId(Long parentStaffId, Long childStaffId) throws BusinessException;
	/**
	 * Lay danh sach giam sat theo NVBH voi phan quyen CMS
	 * 
	 * @author hunglm16
	 * @since October 25 ,2014
	 * */
	ObjectVO<StaffVO> getListGSByStaffRootWithNVBH(StaffPrsmFilter<StaffVO> filter) throws BusinessException;

	ObjectVO<StaffVO> getListGSByStaffRootWithInParentStaffMap(StaffPrsmFilter<StaffVO> filter) throws BusinessException;

	/**
	 * Lay danh sach nhan vien da khoa kho trong ngay hien tai
	 * 
	 * @author lacnv1
	 * @since Nov 05, 2014
	 */
	ObjectVO<Staff> getListStaffLockedStock(StaffFilter filter) throws BusinessException;

	/**
	 * @author vuongmq
	 * @since Feb 27, 2015
	 * @param staff
	 * cap nhat va tao moi thong tin cua staff, thuoc tinh dong staff
	 */
	Staff saveOrUpdateStaff(Staff staff, List<Long> lstAttributeId, List<String> lstAttributeValue, List<Integer> lstAttributeColumnValueType, LogInfoVO logInfo) 	throws BusinessException;
	
	/**
	 * tim kiem nhan vien
	 * @author tuannd20
	 * @param kPaging Thong tin phan trang
	 * @param unitFilter Tieu chi tim kiem
	 * @return Ket qua tim kiem nhan vien
	 * @throws BusinessException
	 * @since 13/03/2015
	 */
	ObjectVO<StaffVO> findStaffVOBy(KPaging<StaffVO> kPaging, UnitFilter unitFilter) throws BusinessException;
	
	/**
	 * Lay danh sach giam sat
	 * 
	 * @author hoanv25
	 * @param shopId   Id cua shop can lay
	 * @return Ket qua tim kiem giam sat
	 * @throws BusinessException
	 * @since July 09/2015
	 */
	List<ImageVO> getListGSByNPP(Long shopId, ImageFilter filter) throws BusinessException;

	/**
	 * Lay danh sach NVBH
	 * 
	 * @author hoanv25
	 * @param shopId   Id cua shop can lay
	 * @return Ket qua tim kiem giam sat
	 * @throws BusinessException
	 * @since July 09/2015
	 */
	List<ImageVO> getListNVBHByNPP(Long shopId, ImageFilter filter) throws BusinessException;

	/**
	 * Lay danh sach giam sat theo id
	 * 
	 * @author hoanv25
	 * @param lstGsId  
	 * @return Ket qua tim kiem giam sat
	 * @throws BusinessException
	 * @since July 09/2015
	 */
	ObjectVO<StaffVO> getListGsById(List<Long> lstGsId) throws BusinessException;

	/**
	 * Lay danh sach NVBH
	 * 
	 * @author hoanv25
	 * @param shopId
	 *            Id cua shop can lay
	 * @return Ket qua tim kiem giam sat
	 * @throws BusinessException
	 * @since July 09/2015
	 */
	List<ImageVO> getListNVBHByNPPShop(List<Long> lstShopId, ImageFilter filter) throws BusinessException;

	/**
	 * Lay danh sach sub cua nvbh
	 * 
	 * @author hoanv25
	 * @param  filter
	 * @return 
	 * @throws BusinessException
	 * @since Auggust 17/2015
	 */
	ObjectVO<StaffVO> listEquipCategoryCode(StaffFilter filter) throws BusinessException;

	/**
	 * Kiem tra danh sach staff theo nganh hang
	 * 
	 * @author hoanv25
	 * @param filter
	 * @return 
	 * @throws BusinessException
	 * @since Auggust 19/2015
	 */
	ObjectVO<StaffVO> listCheckSubStaff(StaffSaleCat filter) throws BusinessException;

	/**
	 * lay danh sach staff theo nganh hang
	 * 
	 * @author hoanv25
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @since Auggust 19/2015
	 */
	ObjectVO<StaffVO> listCheckDelSubStaff(Long id) throws BusinessException;

	/**
	 * Lay danh sach san pham theo nganh hang 
	 * 
	 * @author hoanv25
	 * @param  idParentCat
	 * @return Ket qua tim kiem giam sat
	 * @throws BusinessException
	 * @since July 09/2015
	 */
/*	List<ImageVO> getListParentCatByNPP(Long idParentCat) throws BusinessException;*/
	
	/**
	 * lay danh sach thong tin nhan vien cung voi thong tin thuoc tinh dong
	 * @author tuannd20
	 * @param filter dieu kien loc nhan vien
	 * @return danh sach nhan vien cung voi thong tin thuoc tinh dong
	 * @throws BusinessException
	 * @since 13/09/2015
	 */
	List<StaffExportVO> getListStaffWithDynamicAttribute(StaffFilter filter) throws BusinessException;
	
	/**
	 * Lay vi tri cuoi cung
	 * @author trietptm
	 * @param staffId
	 * @param checkDate
	 * @param fromTime
	 * @param toTime
	 * @return
	 * @throws BusinessException
	 * @since Sep 19, 2015
	 */
	StaffPositionLog getLastStaffPositionLog(Long staffId, Date checkDate, String fromTime, String toTime) throws BusinessException;

	/**
	 * Lay ds khach hang ghe tham
	 * @author trietptm
	 * @param shopId
	 * @param staffId
	 * @param checkDate
	 * @param fromTime
	 * @param toTime
	 * @return
	 * @throws BusinessException
	 * @since Sep 19, 2015
	 */
	List<CustomerVO> getListCustomerHasVisitPlan(Long shopId, Long staffId, Date checkDate, String fromTime, String toTime) throws BusinessException;
	
	/**
	 * Lộ trình nhân viên bán hàng (DMS.LITE)
	 * @author trietptm
	 * @param staffId
	 * @param checkDate
	 * @param numPoint
	 * @param fromTime
	 * @param toTime
	 * @return
	 * @throws BusinessException
	 * @since Sep 19, 2015
	 */
	List<StaffPositionLog> showDirectionStaff(Long staffId, Date checkDate, Integer numPoint, String fromTime, String toTime) throws BusinessException;
	
	/**
	 * lay ds ActionLog theo dk
	 * @author trietptm
	 * @param staffId
	 * @param checkDate
	 * @param fromTime
	 * @param toTime
	 * @return
	 * @throws BusinessException
	 * @since Sep 19, 2015
	 */
	List<ActionLog> getListActionLog(Long staffId, Date checkDate, String fromTime, String toTime)	throws BusinessException;

	/**
	 * Lay danh sach staffVO phan quyen theo shopRoot
	 * @author vuongmq
	 * @param kPaging
	 * @param unitFilter
	 * @return List<StaffVO>
	 * @throws BusinessException
	 * @since 13/11/2015
	 */
	List<StaffVO> getListStaffVOInherit(KPaging<StaffVO> kPaging, UnitFilter unitFilter) throws BusinessException;

	/**
	 * Lay danh sach staffType phan quyen theo shopRoot, don vi chon
	 * @author vuongmq
	 * @param unitFilter
	 * @return List<StaffType>
	 * @throws BusinessException
	 * @since 18/11/2015
	 */
	List<StaffType> getListStaffType(UnitFilter unitFilter) throws BusinessException;

	/**
	 * Lay danh sach staff them feedback
	 * @author vuongmq
	 * @param filter
	 * @return ObjectVO<StaffVO>
	 * @throws BusinessException
	 * @since 18/11/2015
	 */
	ObjectVO<StaffVO> getListStaffVOFeedbackByFilter(StaffFilter filter) throws BusinessException;

	/**
	 * Lay danh sach StaffVO quan ly cua staffId
	 * @author vuongmq
	 * @param filter
	 * @return ObjectVO<StaffVO>
	 * @throws BusinessException
	 * @since 18/11/2015
	 */
	ObjectVO<StaffVO> getListStaffVOUserMapStaffByFilter(StaffFilter filter) throws BusinessException;

	/**
	 * lay danh sach staffType theo filter
	 * @author trietptm
	 * @param filter
	 * @return danh sach staffType
	 * @throws BusinessException
	 * @since Nov 27, 2015
	*/
	List<StaffType> getListAllStaffType(StaffTypeFilter filter) throws BusinessException;
	
	/**
	 * Lay danh sach giam sat Kenh KA - MT
	 * 
	 * @author tamvnm
	 * @since 07/07/2015
	 * */
	ObjectVO<StaffVO> getListGSByStaffRootKAMT(StaffPrsmFilter<StaffVO> filter) throws BusinessException;
	
	/**
	 * Xu ly getListGSByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<StaffVO>
	 * @throws BusinessException
	 * @since Feb 24, 2016
	 */
	List<StaffVO> getListGSByFilter(StaffPrsmFilter<StaffVO> filter) throws BusinessException;
}
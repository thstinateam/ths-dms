package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.RoutingCar;
import ths.dms.core.entities.RoutingCarCust;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.CarFilter;
import ths.dms.core.entities.vo.CarVO;
import ths.dms.core.entities.vo.LatLngVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;


/**
 * 
 * @author tungmt
 * @since 15/4/2014 
 * @description: quan ly xa giao hang cho vnm
 */
public interface SuperviserLogisticsMgr {
	/**
	 * 
	 * @author tungmt
	 * @since 15/4/2014 
	 * @description: 
	 */
	void createListCustomerArea(List<LatLngVO> lst, LogInfoVO logInfo) throws BusinessException;
	void deleteCustomerArea(Long shopId, LogInfoVO logInfo) throws BusinessException;
	
	List<CarVO> getListShopCar(Long parentShopId) throws BusinessException;
	List<CarVO> getListCar(Long parentShopId) throws BusinessException;
	List<CarVO> getListShopCarOneNode(Long parentShopId) throws BusinessException;
	List<Shop> getListAncestor(Long parentShopId ,String shopCode,String shopName) throws BusinessException;
	ObjectVO<CarVO> getListCarKP(CarFilter filter) throws BusinessException;
	ObjectVO<CarVO> getListCarVungKP(CarFilter filter) throws BusinessException;
	ObjectVO<CarVO> getListRoutingExistKP(CarFilter filter) throws BusinessException;
	List<CarVO> getListCustShop(Long parentShopId) throws BusinessException;
	List<CarVO> getListCarPosition(Long parentShopId,Long maxLogId) throws BusinessException;
	List<CarVO> getListRoutingCustPosition(CarFilter filter) throws BusinessException;
	List<CarVO> getListRoutingCustStatus(CarFilter filter) throws BusinessException;
	List<LatLngVO> getListCustomerArea(Long parentShopId) throws BusinessException;
	Boolean checkDriverHasCar(Long idRouting,Long staffId,Date deliveryDate) throws BusinessException;
	
	List<LatLngVO> getListCustomerByShop(CarFilter filter) throws BusinessException;
	
	
	RoutingCar getRoutingCarById(Long id) throws BusinessException;
	RoutingCarCust getRoutingCarCustById(Long id) throws BusinessException;
	RoutingCar createRoutingCar(RoutingCar rcar, LogInfoVO logInfo) throws BusinessException;
	RoutingCarCust createRoutingCarCust(RoutingCarCust rcar, LogInfoVO logInfo) throws BusinessException;
	void updateRoutingCar(RoutingCar rcar, LogInfoVO logInfo) throws BusinessException;
	void updateRoutingCarCust(RoutingCarCust rcar, LogInfoVO logInfo) throws BusinessException;
	
	ObjectVO<CarVO> getListCustomerByRouting(CarFilter filter) throws BusinessException;
	ObjectVO<CarVO> getListCustomerBySaleOrder(CarFilter filter) throws BusinessException;
	
	List<RoutingCar> getRoutingCarByFilter(CarFilter filter) throws BusinessException;
	List<CarVO> getBCVT11(CarFilter filter) throws BusinessException;
	List<CarVO> getBCVT12(CarFilter filter) throws BusinessException;
	List<CarVO> getBCVT13(CarFilter filter) throws BusinessException;
	List<CarVO> getBCVT14(CarFilter filter) throws BusinessException;
	List<CarVO> getBCVT15(CarFilter filter) throws BusinessException;
	List<CarVO> getBCVT16(CarFilter filter) throws BusinessException;
}

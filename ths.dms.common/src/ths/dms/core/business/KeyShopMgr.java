package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.KS;
import ths.dms.core.entities.KSCusProductReward;
import ths.dms.core.entities.KSCustomer;
import ths.dms.core.entities.KSCustomerLevel;
import ths.dms.core.entities.KSLevel;
import ths.dms.core.entities.KSProduct;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.KeyShopFilter;
import ths.dms.core.entities.vo.AllocateShopVO;
import ths.dms.core.entities.vo.KeyShopCustomerHistoryVO;
import ths.dms.core.entities.vo.KeyShopVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface KeyShopMgr {

	KS createKS(KS ks, LogInfoVO logInfo) throws BusinessException;

	void updateKS(KS ks, LogInfoVO logInfo) throws BusinessException;
	
	void deleteKS(KS ks, LogInfoVO logInfo) throws BusinessException;

	KS getKSById(Long id) throws BusinessException;
	
	KS getKSByCode(String code) throws BusinessException;

	KSLevel getKSLevelByCode(String levelCode, String ksCode) throws BusinessException;
	
	/**
	 * @author tungmt
	 * @since 8/7/2015
	 * @param filter
	 * @return Danh sach key shop vo theo filter
	 * @throws BusinessException
	 */
	ObjectVO<KeyShopVO> getListKeyShopVOByFilter(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 8/7/2015
	 * @param filter
	 * @return Kiem tra co khach hang nao nam ngoai khoang cap nhat keyshop hay khong
	 * @throws BusinessException
	 */
	Boolean checkExistsCustomerNotInKSCycle(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 9/7/2015
	 * @param filter
	 * @return Kiem tra khach hang co tham gia muc nao khong
	 * @throws BusinessException
	 */
	Boolean checkExistsCustomerNotInKSLevel(KeyShopFilter filter) throws BusinessException;
	
	/**
	 * @author tungmt
	 * @since 10/7/2015
	 * @param filter
	 * @return Kiem tra chuong trinh da tra thuong cho khach hang chua
	 * @throws BusinessException
	 */
	Boolean checkExistsRewardInKS(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 10/7/2015
	 * @param filter
	 * @return Lay danh sach ksProduct
	 * @throws BusinessException
	 */
	ObjectVO<KSProduct> getListKSProductByFilter(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 11/7/2015
	 * @param filter
	 * @return Lay danh sach ksCustomer
	 * @throws BusinessException
	 */
	ObjectVO<KSCustomer> getListKSCustomerByFilter(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 11/7/2015
	 * @param filter
	 * @return Lay danh sach kscustomerlevel 
	 * @throws BusinessException
	 */
	ObjectVO<KSCustomerLevel> getListKSCustomerLevelByFilter(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 11/7/2015
	 * @param filter
	 * @return Lay danh sach san pham tra thuong
	 * @throws BusinessException
	 */
	ObjectVO<KSCusProductReward> getListKSCusProductRewardByFilter(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 12/7/2015
	 * @param filter
	 * @return Lay danh sach shop_map cua ks
	 * @throws BusinessException
	 */
	List<Shop> getListShopForKSByFilter(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 12/7/2015
	 * @param filter
	 * @return Xoa cac shop cha, con cua shop truyen vao
	 * @throws BusinessException
	 */
	void removeShopMapByShop(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 12/7/2015
	 * @param filter
	 * @return Kiem tra khach hang ton tai trong don vi cung cap truoc khi xoa shop cha
	 * @throws BusinessException
	 */
	Boolean checkKSCustomerExistsShop(KeyShopFilter filter) throws BusinessException;

	Boolean checkKSCustomerExistsShopDel(KeyShopFilter filter) throws BusinessException;
	/**
	 * @author tungmt
	 * @since 13/7/2015
	 * @param filter
	 * @return Lay danh sach ksproduct theo vo
	 * @throws BusinessException
	 */
	ObjectVO<KeyShopVO> getListKSProductVOByFilter(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 13/7/2015
	 * @param filter
	 * @return Lay danh sach kslevel theo vo
	 * @throws BusinessException
	 */
	ObjectVO<KeyShopVO> getListKSLevelVOByFilter(KeyShopFilter filter) throws BusinessException;
	
	ObjectVO<KeyShopVO> getListKSCustomerLevelVOByFilter(KeyShopFilter filter) throws BusinessException;
	
	ObjectVO<KeyShopVO> getListKSCustomerVOByFilter(KeyShopFilter filter) throws BusinessException;
	
	ObjectVO<KeyShopVO> getListKSRewardVOByFilter(KeyShopFilter filter) throws BusinessException;

	KSLevel getKSLevelById(Long id) throws BusinessException;

	/**
	 * @author tungmt
	 * @version 1.0
	 * @param filter
	 * @return Danh sach keyshop cho don hang
	 * @exception BusinessException
	 * @see
	 * @since 6/8/2015
	 * @serial
	 */
	List<KeyShopVO> getListKSVOForRewardOrderByFilter(KeyShopFilter filter) throws BusinessException;
	
	KSCustomer getKSCustomerById(Long id) throws BusinessException;

	/**
	 * @author trietptm
	 * @param filter
	 * @return Danh sach đơn vị phân bổ keyshop
	 * @throws DataAccessException
	 * @since Aug 01, 2015
	 */
	List<AllocateShopVO> getListAllocateShop(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @version 1.0
	 * @param filter
	 * @return Thong tin KS cho don hang, bao gom danh sach san pham cua ks
	 * @exception BusinessException
	 * @see
	 * @since 6/8/2015
	 * @serial
	 */
	List<KeyShopVO> getInfoKSForOrder(KeyShopFilter filter) throws BusinessException;

	/**
	 * @author trietptm
	 * @param filter
	 * @return Thong tin KeyShop cho don hang
	 * @throws BusinessException
	 * @since Aug 17, 2015
	 */
	List<KeyShopVO> getListKeyShopVOForViewOrderByFilter(KeyShopFilter filter) throws BusinessException;
	
	/**
	 * Lich su cap nhat cua khach hang - CTHTTM
	 * @author hunglm16
	 * @param kPaging
	 * @param ksId
	 * @param cycleId
	 * @param ksLevelId
	 * @param customerId
	 * @param updateDate
	 * @return
	 * @throws DataAccessException
	 * @since 24/11/2015
	 */
	ObjectVO<KeyShopCustomerHistoryVO> getKSRegistedHistory(KPaging<KeyShopCustomerHistoryVO> kPaging, Long ksId, Long cycleId, Long ksLevelId, Long customerId, Date updateDate) throws BusinessException;
}

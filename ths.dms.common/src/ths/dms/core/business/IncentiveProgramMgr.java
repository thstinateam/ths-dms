package ths.dms.core.business;


public interface IncentiveProgramMgr {

	/*IncentiveProgram createIncentiveProgram(IncentiveProgram incentiveProgram, LogInfoVO logInfo) throws BusinessException;

	void deleteIncentiveProgram(IncentiveProgram incentiveProgram, LogInfoVO logInfo) throws BusinessException;
	*//**
	 * Kiem tra khi cap nhat CTKT sang hoat dong
	 * @author thongnm
	 *//*
	String checkUpdateIncentiveProgramForActive(Long incentiveProgramId) throws BusinessException;
	void updateIncentiveProgram(IncentiveProgram incentiveProgram, LogInfoVO logInfo) throws BusinessException;
	
	IncentiveProgram getIncentiveProgramById(long id) throws BusinessException;

	IncentiveProgramDetail createIncentiveProgramDetail(IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo) throws BusinessException;

	void deleteIncentiveProgramDetail(IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo) throws BusinessException;

	void updateIncentiveProgramDetail(IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo) throws BusinessException;
	
	IncentiveProgramDetail getIncentiveProgramDetailById(long id) throws BusinessException;

	IncentiveProgramLevel createIncentiveProgramLevel(IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo) throws BusinessException;

	void deleteIncentiveProgramLevel(IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo) throws BusinessException;

	void updateIncentiveProgramLevel(IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo) throws BusinessException;
	
	IncentiveProgramLevel getIncentiveProgramLevelById(long id) throws BusinessException;

	IncentiveShopMap createIncentiveShopMap(IncentiveShopMap incentiveShopMap, LogInfoVO logInfo) throws BusinessException;
	
	void updateIncentiveShopMap(IncentiveShopMap incentiveShopMap, LogInfoVO logInfo) throws BusinessException;
	
	IncentiveShopMap getIncentiveShopMapById(long id) throws BusinessException;
	
	ObjectVO<IncentiveProgramLevel> getListIncentiveProgramLevel(Long incentiveProgramId, ActiveType status,
			KPaging<IncentiveProgramLevel> kPaging)
			throws BusinessException;
	
	ObjectVO<IncentiveProgramDetail> getListIncentiveProgramDetail(Long incentiveProgramId, ActiveType status,
			KPaging<IncentiveProgramDetail> kPaging)
			throws BusinessException;

	ObjectVO<IncentiveShopMap> getListIncentiveShopMap(
			Long incentiveProgramId, ActiveType status,
			KPaging<IncentiveShopMap> kPaging)
			throws BusinessException;
	
	ObjectVO<IncentiveProgramVO> getListIncentiveProgramVO(IncentiveProgramFilter filter,
			KPaging<IncentiveProgramVO> kPaging)
			throws BusinessException;
	
	ObjectVO<IncentiveProgramLevelVO> getListIncentiveProgramLevelVO(
			IncentiveProgramLevelFilter filter,
			KPaging<IncentiveProgramLevelVO> kPaging)
			throws BusinessException;
	
	ObjectVO<IncentiveProgramDetailVO> getListIncentiveProgramDetailVO(
			IncentiveProgramDetailFilter filter,
			KPaging<IncentiveProgramDetailVO> kPaging)
			throws BusinessException;
	
	ObjectVO<IncentiveShopMapVO> getListIncentiveShopMapVO(
			IncentiveShopMapFilter filter, KPaging<IncentiveShopMapVO> kPaging)
			throws BusinessException;
	
	ObjectVO<Shop> getListShopNotJoinIncentiveProgram(
			Long incentiveProgramId, KPaging<Shop> kPaging)
			throws BusinessException;
	
	Boolean isExistProduct(Long incentiveProgramId, Long productId, Date fromDate, Date toDate,Long exceptedId)
			throws BusinessException;
	
	Boolean isExistShop(Long incentiveProgramId, Long shopId)
			throws BusinessException;
	
	IncentiveProgram getIncentiveProgramByCode(String code)
			throws BusinessException;
	
	List<IncentiveShopMap> getListAncestorIncentiveShopMap(
			Long incentiveProgramId, Long shopId) throws BusinessException;
	
	Boolean isExistCategory(Long incentiveProgramId, Long catId, Date fromDate, Date toDate)
			throws BusinessException;

	Boolean createListIncentiveShopMap(
			List<IncentiveShopMap> lstIncentiveShopMap, LogInfoVO logInfo)
			throws BusinessException;
	
	Boolean checkIfAncestorInIp(Long shopId, Long incentiveProgramId)
			throws BusinessException;
	
	IncentiveProgramLevel getIncentiveProgramLevelByIncentiveProCode(Long incentiveProgramId,
			String incentiveProCode, String levelCode, ActiveType activeType)
			throws BusinessException;
	
	IncentiveProgram copyIncentiveProgram(Long incentiveProgramId,
			LogInfoVO logInfo, String ipCode, String ipName)
			throws BusinessException;

	void deleteIncentiveShopMap(Long shopId, Long incentiveProgramId,
			LogInfoVO logInfo) throws BusinessException;

	Boolean isDuplicateProductByIncentiveProgram(
			IncentiveProgram incentiveProgram) throws BusinessException;

	Boolean isExistChildJoinProgram(String shopCode, Long incentiveProgramId)
			throws BusinessException;*/

	
}

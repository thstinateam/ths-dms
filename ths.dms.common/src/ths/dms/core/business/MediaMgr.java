package ths.dms.core.business;


import java.util.List;

import ths.dms.core.entities.Media;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.MediaMap;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.MediaFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

public interface MediaMgr {
	

	Media createMedia(Media media) throws BusinessException;

	void deleteMedia(Media media) throws BusinessException;

	Media getMediaById(long id) throws BusinessException;

	Media updateMedia(Media media) throws BusinessException;

	ObjectVO<Media>  getListMedia(MediaFilter filter) throws BusinessException;

	ObjectVO<Product> getListProductByMedia(KPaging<Product> kPaging,
			Long mediaId, Integer status) throws BusinessException;

	Media getMediaByCode(String mediaCode) throws BusinessException;

	MediaItem createMediaItem(MediaItem mediaItem) throws BusinessException;

	MediaItem updateMediaItem(MediaItem mediaItem) throws BusinessException;

	MediaItem getMediaItemById(Long mediaItemId) throws BusinessException;

	List<MediaItem> getListMediaItemByMedia(KPaging<MediaItem> kPaging,
			Long mediaId) throws BusinessException;
	
	MediaMap createMediaMap(MediaMap mediaMap) throws BusinessException;

	void deleteMediaMap(MediaMap mediaMap) throws BusinessException;

	MediaMap getMediaMapById(long id) throws BusinessException;

	MediaMap updateMediaMap(MediaMap mediaMap) throws BusinessException;

	MediaMap getMediaMapByMediaAndObject(Long mediaId, Long objectId,
			Long objectType) throws BusinessException;

}

package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ChannelTypeVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.dms.core.exceptions.BusinessException;

public interface ChannelTypeMgr {

	ChannelType createChannelType(ChannelType channelType, LogInfoVO logInfo)
			throws BusinessException;

	void updateChannelType(ChannelType channelType, LogInfoVO logInfo) throws BusinessException;

	void deleteChannelType(ChannelType channelType, LogInfoVO logInfo) throws BusinessException;

	ChannelType getChannelTypeByCode(String code, ChannelTypeType type)
			throws BusinessException;
	ChannelType getChannelTypeByName(String name, ChannelTypeType type)
			throws BusinessException;
	
	Boolean isUsingByOthers(long channelTypeId, ChannelTypeType type)
			throws BusinessException;

	ChannelType getChannelTypeById(Long id) throws BusinessException;

	ObjectVO<ChannelType> getListDescendantChannelType(
			KPaging<ChannelType> kPaging, Long parentId, ChannelTypeType type)
			throws BusinessException;

	/**
	 * Gets the list channel type order by id.
	 * @author phut
	 * @param kPaging the k paging
	 * @param channelTypeCode the channel type code
	 * @param channelTypeName the channel type name
	 * @param parentId the parent id
	 * @param type the type
	 * @param status the status
	 * @param isEdit the is edit
	 * @param statusSku the status sku
	 * @param statusAmount the status amount
	 * @param objectType the object type
	 * @return the list channel type order by id
	 * @throws BusinessException the business exception
	 */
	ObjectVO<ChannelType> getListChannelTypeOrderById(KPaging<ChannelType> kPaging,
			String channelTypeCode, String channelTypeName, Long parentId,
			ChannelTypeType type, ActiveType status, Integer isEdit,
			ActiveType statusSku, ActiveType statusAmount, Integer objectType)
			throws BusinessException;	
	
	Boolean isAllChildStoped(long channelTypeId, ChannelTypeType type)
			throws BusinessException;

	TreeVO<ChannelType> getChannelTypeTreeVOEx(ChannelType channelType,
			ChannelTypeType type, Long exceptionalChannelType)
			throws BusinessException;
	
	/**
	 * @author hungnm
	 * @param kPaging
	 * @param promotionShopMapId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<ChannelType> getListCustomerTypeNotInPromotionProgram(
			KPaging<ChannelType> kPaging, Long promotionShopMapId)
			throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param channelCode
	 * @param type
	 * @return
	 * @throws BusinessException
	 */
	Boolean checkExsitedChildChannelType(String channelCode,
			ChannelTypeType type) throws BusinessException;

	/**
	 * @author hungnm
	 * @param parentId
	 * @return
	 * @throws BusinessException
	 */
	Boolean checkChannelTypeHasAnyChild(Long parentId) throws BusinessException;

	ObjectVO<ChannelType> getListDescendantChannelTypeOrderById(
			KPaging<ChannelType> kPaging, Long parentId, ChannelTypeType type)
			throws BusinessException;

	Boolean isUsingByOtherRunningItem(long channelTypeId, ChannelTypeType type)
			throws BusinessException;

	Boolean checkAncestor(Long parentId, Long channelTypeId)
			throws BusinessException;

	TreeVO<ChannelType> getChannelTypeTreeVO(ChannelType channelType,
			ChannelTypeType type, Boolean isOrderByName)
			throws BusinessException;

	List<ChannelType> getListSubChannelType(Long parentId,
			ChannelTypeType type, Boolean isOrderByName)
			throws BusinessException;
	
	List<ChannelType> getListSubChannelTypeByType(Integer type)
			throws BusinessException;

	ObjectVO<ChannelType> getListChannelType(ChannelTypeFilter filter, KPaging<ChannelType> kPaging)
			throws BusinessException;
	
	/**
	 * Cap nhat loai KH dong thoi cap nhat lai KH co loai nay ve null (neu tam ngung loai)
	 * 
	 * @author lacnv1
	 * @since Jun 16, 2014
	 */
	void updateCustomerType(ChannelType type, LogInfoVO logInfo) throws BusinessException;

	List<ChannelTypeVO> getListChannelTypeVO();
}

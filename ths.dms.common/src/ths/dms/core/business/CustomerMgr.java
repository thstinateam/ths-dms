package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.CustomerCatLevel;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ParamsDAOGetListCustomer;
import ths.dms.core.entities.filter.CustomerFilter;
import ths.dms.core.entities.vo.Customer4SaleVO;
import ths.dms.core.entities.vo.CustomerCatLevelImportVO;
import ths.dms.core.entities.vo.CustomerExportVO;
import ths.dms.core.entities.vo.CustomerGTVO;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.DuplicatedCustomerVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.RptCustomerByProductCatLv01VO;
import ths.dms.core.exceptions.BusinessException;

public interface CustomerMgr {

	Customer createCustomer(Customer customer, String username, List<Long> lstAttributeId, List<String> lstAttributeValue, List<Integer> lstAttributeColumnValueType, LogInfoVO logInfo) throws BusinessException;

	void updateCustomer(Customer customer , String username, List<Long> lstAttributeId, List<String> lstAttributeValue, List<Integer> lstAttributeColumnValueType, LogInfoVO logInfo) throws BusinessException;

	void deleteCustomer(Customer customer, LogInfoVO logInfo) throws BusinessException;

	Customer getCustomerByCode(String code) throws BusinessException;
	Customer getCustomerByShortCode(String code) throws BusinessException;
	Customer getCustomerByCodeAndShop(String shortCode, Long shopId) throws BusinessException;

	Boolean isUsingByOthers(long customerId) throws BusinessException;

	Customer getCustomerById(Long id) throws BusinessException;

	CustomerCatLevel createCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo)
			throws BusinessException;

	void updateCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo)
			throws BusinessException;

	void deleteCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo)
			throws BusinessException;

	CustomerCatLevel getCustomerCatLevelById(Long id)
			throws BusinessException;

	Boolean checkIfCustomerCatLevelRecordExist(long customerId,
			long saleLevelCatId, Long id) throws BusinessException;

	Boolean checkIfCustomerExists(String email, String mobiphone,String phone,
			String numberIdentity, Long id, String string) throws BusinessException;

	/**
	 * Lay du lieu cho Bao cao Khach hang theo Nhom san pham
	 * 
	 * @param shopId
	 * @param cusIds
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<RptCustomerByProductCatLv01VO> getDataForCusByProReport(Long shopId,
			List<Long> cusIds, Date fromDate, Date toDate)
			throws BusinessException;

	/**
	 * @author hungnm
	 * @param customerId
	 * @return
	 * @throws BusinessException
	 */
	CustomerVO getCustomerVO(Long customerId) throws BusinessException;


	/**
	 * @author hungnm
	 * @param kPaging
	 * @param shortCode
	 * @param customerName
	 * @param parentShopCode
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<Customer> getListCustomerEx(KPaging<Customer> kPaging,
			String shortCode, String customerName, String parentShopCode,
			Long shopId, ActiveType status) throws BusinessException;

	ObjectVO<Customer> getListCustomer(ParamsDAOGetListCustomer params) throws BusinessException;
	
	/**
	 * Lay danh muc nghanh hang cua khach hang
	 * 
	 * @param kPaging
	 * @param shortCode
	 * @param customerName
	 * @param lstAreaCode
	 * @param phone
	 * @param mobiPhone
	 * @param customerTypeId
	 * @param status
	 * @param region
	 * @param loyalty
	 * @param groupTransferId
	 * @param cashierId
	 * @param address
	 * @param street
	 * @param shopCode
	 * @param shopCodeLike
	 * @param display
	 * @param location
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<CustomerCatLevelImportVO> getCustomerCatLevelImportVO(
			KPaging<CustomerCatLevelImportVO> kPaging, String shortCode,
			String customerName, List<String> lstAreaCode, String phone,
			String mobiPhone, Long customerTypeId, ActiveType status,
			String region, String loyalty, Long groupTransferId,
			Long cashierId, String address, String street, String shopCode,
			String shopCodeLike, String display, String location)
			throws BusinessException;

	/**
	 * @author hungnm
	 * @param customerId
	 * @param catId
	 * @return
	 * @throws BusinessException
	 */
	CustomerCatLevel getCustomerCatLevel(Long customerId, Long catId)
			throws BusinessException;

	ObjectVO<Customer> getListCustomerForGroupTransfer(
			KPaging<Customer> kPaging, String shortCode, String customerName,
			String parentShopCode, Long shopId, ActiveType status,
			Long notInDeliverId) throws BusinessException;

	ObjectVO<Customer> getListCustomerByShopCode(KPaging<Customer> kPaging,
			String shopCode) throws BusinessException;
	
	boolean  existsCode(Long cusID,Long shopId,String code) throws BusinessException;

	ObjectVO<CustomerCatLevel> getListCustomerCatLevel(
			KPaging<CustomerCatLevel> kPaging, Long customerId,
			Long saleLevelCatId, Boolean isOrderByName)
			throws BusinessException;

	/**
	 * Gets the list customer ex1.
	 * @author phut
	 * @param kPaging the k paging
	 * @param shortCode the short code
	 * @param customerName the customer name
	 * @param parentShopCode the parent shop code
	 * @param shopId the shop id
	 * @param address the address
	 * @param status the status
	 * @return the list customer ex1
	 * @throws BusinessException the business exception
	 */
	ObjectVO<Customer> getListCustomerEx1(KPaging<Customer> kPaging,
			String shortCode, String customerName, String parentShopCode,
			Long shopId, String address, ActiveType status)
			throws BusinessException;

	/**
	 * @author vuongmq
	 * @since: 25 - August, 2014
	 * @description: Search customer, open dialog F9 (chi dinh nhan vien giao hang)
	 */
	ObjectVO<Customer> getListCustomerEx1ByShopId(KPaging<Customer> kPaging,
			String shortCode, String customerName, String parentShopCode,
			Long shopId, String address, ActiveType status)
			throws BusinessException;
	
	ObjectVO<CustomerVO> getListCustomerFilterByShopId(KPaging<CustomerVO> kPaging,
			String shortCode, String customerName, String parentShopCode,
			Long shopId, String address, ActiveType status, String strListShopId, 
			Long cycleId, Long ksId)
			throws BusinessException;

	ObjectVO<Customer> getListCustomerForGroupTransferByShopId(
			KPaging<Customer> kPaging, String shortCode, String customerName,
			String parentShopCode, Long shopId, ActiveType status,
			Long notInDeliverId) throws BusinessException; 
	
	ObjectVO<Customer> getListCustomerByStaffRouting(KPaging<Customer> kPaging,
			Long shopId, String customerCode,
			String customerName, Long staffId, 
			ActiveType status) throws BusinessException;

	ObjectVO<CustomerGTVO> getListCustomerGTVOByGroupTransfer(
			KPaging<CustomerGTVO> kPaging, Long shopId,
			String groupTransferCode, String groupTransferName,
			String staffTransferCode, String shortCode, ActiveType status,
			String shopCode, Boolean check, String customerName)
			throws BusinessException;
	
	ObjectVO<Customer> searchCustomerForDSKH(KPaging<Customer> kPaging,
			String shopCode, String staffCode, String customerCode,
			String customerName) throws BusinessException;

	ObjectVO<Customer> getListCustomerNotInPromotionProgram(
			KPaging<Customer> kPaging, Long parentShopId,
			Long promotionShopMapId, String shortCode, String customerName,
			String address) throws BusinessException;

	Customer getCustomerMultiChoine(Long customerId, Long shopId, String shortCode, Integer status) throws BusinessException;

	List<Customer4SaleVO> getListCustomer4CreateOrder(Long shopId)
			throws BusinessException;
	
	List<Customer> getListCustomerByShortCode(Long shopId, String shortCode) throws BusinessException;

	/**
	 * @author sangtn
	 * @since 08-05-2014
	 * @description Get customer by full code and status 
	 * @note Extend from getCustomerByCode
	 */
	Customer getCustomerByCodeEx(String code, ActiveType status)
			throws BusinessException;
	
	/**
	 * Cap nhat channel_type cua KH ve null
	 * 
	 * @author lacnv1
	 * @since Jun 16, 2014
	 */
	void removeTypeOfCustomer(long typeId) throws BusinessException;
	/**
	 * Tim kiem khach hang theo phan quyen CMS 
	 * 
	 * @author hunglm16
	 * @description filter.getLstShopId() khong duoc de trong
	 * */
	ObjectVO<CustomerVO> searchListCustomerVOByFilter(CustomerFilter<CustomerVO> filter) throws BusinessException;
	/**
	 * Tim kiem khach hang theo phan quyen CMS ket hop Tuyen ban hang
	 * 
	 * @author hunglm16
	 * @description filter.getLstShopId() va filter.getStaffRootId khong duoc de trong
	 * */
	ObjectVO<CustomerVO> searchListCustomerVOForRoutingByFilter(CustomerFilter<CustomerVO> filter) throws BusinessException;

	/**
	 * Duyet khach hang moi
	 * @author tuannd20
	 * @param customerId id khach hang dang duyet
	 * @param logInfo thong tin ghi log
	 * @return danh sach khach hang trung (dia chi, trong khoang cach cho phep) voi khach hang dang duyet (neu co)
	 * @throws BusinessException
	 * @since 28/08/2015
	 */
	List<DuplicatedCustomerVO> approveNewCustomer(Long customerId, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * Tao khach hang moi
	 * @author tuannd20
	 * @param customerId id khach hang moi can tao ma
	 * @param logInfo thong tin ghi log
	 * @return khach hang tao moi
	 * @throws BusinessException
	 * @since 31/08/2015
	 */
	Customer createNewCustomer(Long customerId, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * Tim kiem khach hang voi thong tin thuoc tinh dong
	 * @author tuannd20
	 * @param param tham so tim kiem
	 * @return danh sach khach hang kem theo thong tin thuoc tinh dong
	 * @throws BusinessException
	 * @since 04/09/2015
	 */
	List<CustomerExportVO> getCustomerWithDynamicAttributeInfo(ParamsDAOGetListCustomer param) throws BusinessException;
	
	/**
	 * (M) add shop_id
	 * @author tuannd20
	 * @param shopId
	 * @param staffId
	 * @return
	 * @throws BusinessException
	 * @date 05/07/2014
	 */
	List<CustomerVO> getListCustomerBySaleStaffHasVisitPlan(Long shopId, Long staffId, Date checkDate) throws BusinessException;
	
	/**
	 * Xoa vi tri khach hang
	 * @author vuongmq
	 * @param customer
	 * @param logInfoVO
	 * @since 09/10/2015
	 */
	void deleteCustomerPosition(Customer customer, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Lay danh sach customer popup feedback
	 * @author vuongmq
	 * @param filter
	 * @return ObjectVO<CustomerVO>
	 * @throws BusinessException
	 * @since 18/11/2015
	 */
	ObjectVO<CustomerVO> getCustomerVOFeedback(CustomerFilter<CustomerVO> filter) throws BusinessException;

	/**
	 * Lay danh sach customer popup feedback theo routing cua staff
	 * @author vuongmq
	 * @param filter
	 * @return ObjectVO<CustomerVO>
	 * @throws BusinessException
	 * @since 18/11/2015
	 */
	ObjectVO<CustomerVO> getCustomerVORoutingFeedback(CustomerFilter<CustomerVO> filter) throws BusinessException;
	
	/**
	 * Tim kiem theo filter
	 * @author nhutnn
	 * @since 22/07/2015
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<CustomerVO> searchListCustomerFullVOByFilter(CustomerFilter<CustomerVO> filter) throws BusinessException;
}

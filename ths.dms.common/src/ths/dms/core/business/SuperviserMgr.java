package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.ActionLog;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.Routing;
import ths.dms.core.entities.RoutingCustomer;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffPositionLog;
import ths.dms.core.entities.TrainingPlan;
import ths.dms.core.entities.TrainingPlanDetail;
import ths.dms.core.entities.VisitPlan;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.filter.RoutingGeneralFilter;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.filter.StaffFilter;
import ths.dms.core.entities.filter.SupFilter;
import ths.dms.core.entities.filter.VisitPlanFilter;
import ths.dms.core.entities.vo.ActionLogVO;
import ths.dms.core.entities.vo.AmountAndAmountPlanVO;
import ths.dms.core.entities.vo.AmountPlanCustomerVO;
import ths.dms.core.entities.vo.AmountPlanStaffVO;
import ths.dms.core.entities.vo.CustomerPositionVO;
import ths.dms.core.entities.vo.CustomerSaleVO;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.NVBHSaleVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.RoutingCustomerVO;
import ths.dms.core.entities.vo.RoutingTreeVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.entities.vo.ShopSaleVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.StaffTrainingPlanVO;
import ths.dms.core.entities.vo.SuperVisorInfomationVO;
import ths.dms.core.entities.vo.SupervisorSaleVO;
import ths.dms.core.entities.vo.TrainingPlanVO;
import ths.dms.core.exceptions.BusinessException;

public interface SuperviserMgr {

	/**
	 * getListStaffByParentId
	 * @author hieunq1
	 * @param parentId
	 * @return
	 * @throws BusinessException
	 */
	List<Staff> getListStaffByOwnerId(Long ownerId) throws BusinessException;
	
	/**
	 * lay danh sach tuyen cua GSNPP
	 * @author thanhnguyen
	 * @param kPaging
	 * @param shopId
	 * @param status
	 * @param customerCode
	 * @param staffCode
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<RoutingVO> getListVisitPlanByCondition(
			KPaging<RoutingVO> kPaging, Long shopId, Long ownerId, ActiveType status,
			String customerCode, String staffCode, String routingCode, String routingName) throws BusinessException;
	
	/**
	 * tao tuyen moi
	 * @author thanhnguyen
	 * @param shopId
	 * @param routingCode
	 * @param routingName
	 * @param status
	 * @param listRoutingCustomer
	 * @throws BusinessException
	 */
	void createRouting(Long shopId, String routingCode, String routingName, ActiveType status, List<RoutingCustomer> listRoutingCustomer) throws BusinessException;
	
	/**
	 * 
	*  tao tuyen moi
	*  @author: thanhnn8
	*  @param routing
	*  @param listRoutingCustomer
	*  @throws BusinessException
	*  @return: void
	*  @throws:
	 */
	void createRoutingWithEntity(Routing routing, List<RoutingCustomer> listRoutingCustomer) throws BusinessException;
	
	/**
	 * lay danh sach khach hang trong tuyen, thu tu khach hang theo ngay ban hang
	 * @author thanhnguyen
	 * @param kPaging
	 * @param routingId
	 * @param status
	 * @param saleDate: truyen vao de lay thu tu tuyen
	 * @param shopId: add shopId
	 * @param hasChildShop: co lay cac shop con cua shopId truyen vao hay ko
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<RoutingCustomer> getListRoutingCustomer(
			KPaging<RoutingCustomer> kPaging, Long routingId,
			ActiveType status, Integer saleDate, Boolean hasOrderBySeq,
			Long shopId, Boolean hasChildShop, Long superId) throws BusinessException;


	/**
	 * 
	 * @author hieunq1
	 * @param id
	 * @param staffId
	 * @param logInfo
	 * @throws BusinessException
	 */
	void changeTrainingPlanDetailById(Long id, Long staffId, LogInfoVO logInfo)
			throws BusinessException;

	/**
	 * lay danh sach hinh anh cua khach hang / san pham
	 * @author hieunq1
	 * @param mediaType
	 * @return
	 * @throws BusinessException
	 */
	List<MediaItem> getListMediaItemByObjectIdAndObjectType(Long objectId, MediaObjectType objectType)
			throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	MediaItem getMediaItemById(Long id) throws BusinessException;
	
	Routing createRouting(Routing routing) throws BusinessException;

	void deleteRouting(Routing routing) throws BusinessException;

	void updateRouting(Routing routing) throws BusinessException;
	
	Routing getRoutingById(Long id) throws BusinessException;
	
	RoutingCustomer createRoutingCustomer(RoutingCustomer routingCustomer) throws BusinessException;

	void deleteRoutingCustomer(RoutingCustomer routingCustomer) throws BusinessException;

	void updateRoutingCustomer(RoutingCustomer routingCustomer) throws BusinessException;
	
	RoutingCustomer getRoutingCustomerById(Long id) throws BusinessException;
	
	void createRoutingCustomer(List<RoutingCustomer> listRoutingCustomer) throws BusinessException;
	
	/**
	 * update du lieu cho tuyen
	 * @author thanhnguyen
	 * @param routingId
	 * @param listDelete
	 * @param listUpdate
	 * @param listInsert
	 * @throws BusinessException
	 */
	void updateRouting(Routing routing, List<RoutingCustomer> listDelete,
			List<RoutingCustomer> listUpdate, List<RoutingCustomer> listInsert)
			throws BusinessException;
	
	/**
	 * lay danh sach lich su giao tuyen cua nhan vien ban hang
	 * @author thanhnguyen
	 * @param kPaging
	 * @param routingId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<VisitPlan> getListVisitPlanByRoutingId(SupFilter<VisitPlan> filter) throws BusinessException;
	
	VisitPlan createVisitPlan(VisitPlan visitPlan) throws BusinessException;
	
	/**
	 * kiem tra rang buoc da ton tai khach hang cho tuyen trong khoang thoi gian tu ngay den ngay
	 * @author thanhnguyen
	 * @param routingId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	VisitPlan getVisitPlan(Long routingId, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * lay danh sach tuyen
	 * @author thanhnguyen
	 * @param shopId
	 * @param routingCode
	 * @param routingName
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<Routing> getListRouting(KPaging<Routing> kPaging, Long parentShopId, Long shopId, Long superId, String routingCode, String routingName) throws BusinessException;
	
	void updateVisitPlan(VisitPlan visitPlan) throws BusinessException;
	
	VisitPlan getVisitPlanById(Long id) throws BusinessException;
	
	void updateListRoutingCustomer(List<RoutingCustomer> lstRoutingCustomer) throws BusinessException;
	
	/**
	 * kiem tra xem visit plan co ton tai hay khong - chuc nang cap nhat routing
	 * @author thanhnguyen
	 * @param routingId
	 * @param visitPlanId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	Boolean checkExitVisitPlan(Long routingId, Long visitPlanId, Long staffId, Date fromDate, Date toDate) throws BusinessException;
	
	
	Routing getRoutingByCode(Long shopId, String code) throws BusinessException;
	
	/**
	 * lay danh sach khach hang duoc quan ly boi shopCode tuong ung (khong lay de qui xuong duoi)
	 * @author thanhnguyen
	 * @param kPaging
	 * @param shortCode
	 * @param customerName
	 * @param shopCode
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<Customer> getListCustomerForOnlyShop(KPaging<Customer> kPaging, String shortCode, String customerName, String shopCode,List<Long> lstExceptionCus) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param superCode
	 * @param staffCode
	 * @param date
	 * @param note
	 * @param logInfo
	 * @throws BusinessException
	 */
	void cancelTrainingPlanDetailBySuperCodeStaffCode(String superCode, String staffCode,
			Date date, String note, LogInfoVO logInfo) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param ownerId
	 * @param staffId
	 * @param date
	 * @throws BusinessException
	 */
	void createTrainingPlanDetailByOwnerIdAndStaffId(Long ownerId,Shop chooseShop,
			Long staffId, Date date, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * 
	*  Lay danh sach staff voi ownerId va shopcode tuong ung
	*  @author: thanhnn
	*  @param ownerId
	*  @param shopCode
	*  @return
	*  @throws BusinessException
	*  @return: List<Staff>
	*  @throws:
	 */
	List<Staff> getListStaffByOwnerAndShop(Long ownerId, String shopCode) throws BusinessException;
	
	/**
	 * 
	*  lay danh sach visit plan - quan ly ke hoach ban hang (update tablet)
	*  @author: thanhnn
	*  @param shopId
	*  @param staffId
	*  @param month
	*  @param year
	*  @param type
	*  @return
	*  @throws BusinessException
	*  @return: List<VisitPlan>
	*  @throws:
	 */
	ObjectVO<VisitPlan> getListVisitPlanForManagerPlan(KPaging<VisitPlan> kPaging, Long shopId, Long staffId,
			Integer month, Integer year, ActiveType type)
			throws BusinessException;
	
	/**
	 * 
	*  lay danh sach giam sat nhan phan phoi cho truong hop lap ke hoach huan luyen
	*  @author: thanhnn
	*  @param kPaging
	*  @param parentShopId
	*  @param date
	*  @return
	*  @throws BusinessException
	*  @return: List<Staff>
	*  @throws:
	 */
	ObjectVO<Staff> getListSuperForTrainning(KPaging<Staff> kPaging,
			Long parentShopId, Date date) throws BusinessException;
	/**
	 * 
	*  Kiem tra ton tai training plan chua
	*  @author: thanhnn
	*  @param staffCode - ma GSNPP
	*  @param date
	*  @return
	*  @throws BusinessException
	*  @return: Boolean
	*  @throws:
	 */
	/**
	 * 
	*  kiem tra co ton tai nhan vien thuoc NPP co duoc quan ly boi GSNPP khong
	*  @author: thanhnn
	*  @param shopCode
	*  @param superCode
	*  @return
	*  @throws BusinessException
	*  @return: Boolean
	*  @throws:
	 */
	Boolean checkExitStaffManagerBy(String shopCode, String superCode) throws BusinessException;
	
	ObjectVO<Routing> getListRoutingBySuper(KPaging<Routing> kPaging ,Long superId, String routingCode, String routingName) throws BusinessException;

	/**
	 * 
	 * @param actionLog
	 * @param logVO
	 * @return
	 * @throws BusinessException
	 */
	ActionLog createActionLog(ActionLog actionLog, LogInfoVO logVO)
			throws BusinessException;
	
	ObjectVO<ActionLogVO> getListVisitPlanBySuperVisor(KPaging<ActionLogVO> kPaging, Long shopId,
			String staffCode, Date visitPlanDate, Boolean isHasChild) throws BusinessException;
	
	/**
	 * 
	*  lay danh sach vi tri khach hang theo nhan vien ban hang
	*  @author: thanhnn
	*  @param kPaging
	*  @param staffId
	*  @param visitPlanDate
	*  @return
	*  @throws BusinessException
	*  @return: ObjectVO<CustomerPositionVO>
	*  @throws:
	 */
	ObjectVO<CustomerPositionVO> getListCustomerPosition(
			KPaging<CustomerPositionVO> kPaging, Long staffId,
			Date visitPlanDate) throws BusinessException;
	/**
	 * 
	*  lay danh sach vi tri khach hang theo nhan vien ban hang
	*  @author: thanhnn
	*  @param kPaging
	*  @param staffId
	*  @param visitPlanDate
	*  @return
	*  @throws BusinessException
	*  @return: ObjectVO<CustomerPositionVO>
	*  @throws:
	 */
	List<CustomerPositionVO> getListCustomerPositionEx( Long staffId,
			Date visitPlanDate) throws BusinessException;
	/**
	 * 
	*  lay thong tin chi tiet cua TBHV (tra ve list cac GSNPP co di huan luyen cung)
	*  @author: thanhnn
	*  @param parentStaffId
	*  @return
	*  @throws BusinessException
	*  @return: List<SuperVisorInfomationVO>
	*  @throws:
	 */
	List<SuperVisorInfomationVO> getInfomationOfParentSuper(Long parentStaffId) throws BusinessException;
	
	/**
	 * 
	*  lay vi tri cua nhan vien cap duoi dua vao object type tuong ung - dung  chung TBHV & GSNPP
	*  @author: thanhnn
	*  @param ownerId
	*  @param objectType
	*  @return
	*  @throws BusinessException
	*  @return: List<StaffPositionVO>
	*  @throws:
	 */
	List<StaffPositionVO> getListStaffPosition(Long ownerId,
			StaffObjectType objectType) throws BusinessException;
	
	/**
	 * 
	*  lay cac khach hang trong tuyen cua ngay hien tai theo NVBH
	*  @author: thanhnn
	*  @param staffId
	*  @return
	*  @throws BusinessException
	*  @return: List<Customer>
	*  @throws:
	 */
	List<CustomerPositionVO> getListCustomerForStaff(Long staffId) throws BusinessException;
	
	/**
	 * 
	*  Lay thong tin chi tiet theo GSNPP (tra ve list cac TBHV di WW cung GS do)
	*  @author: thanhnn
	*  @param superStaffId
	*  @return
	*  @throws BusinessException
	*  @return: List<SuperVisorInfomationVO>
	*  @throws:
	 */
	List<SuperVisorInfomationVO> getInfomationOfSuper(Long superStaffId) throws BusinessException;
	
	/**
	 * 
	*  lay thong tin cua nhan vien (tra ve trong ban do phan giam sat)
	*  @author: thanhnn
	*  @param staffId
	*  @return
	*  @throws BusinessException
	*  @return: SuperVisorInfomationVO
	*  @throws:
	 */
	List<SuperVisorInfomationVO> getInfomationOfStaff(Long staffId) throws BusinessException;
	
	/**
	 * 
	*  Lay danh sach giam sat nha phan phoi cua truong ban hang vung
	*  @author: thanhnn
	*  @param staffId
	*  @return
	*  @throws BusinessException
	*  @return: List<StaffPositionVO>
	*  @throws:
	 */
	List<StaffPositionVO> getListSuperPosition(Long staffId) throws BusinessException;
	
	/**
	 * 
	*  Off tuyen
	*  @author: thanhnn
	*  @param routingId
	*  @throws BusinessException
	*  @return: void
	*  @throws:
	 */
	void offRouting(Long routingId, String updateUser) throws BusinessException;

	List<Staff> getListNVBHInTuyen(Long gsnppId, Long shopId) throws BusinessException;

	AmountAndAmountPlanVO getAmountAndAmountPlanByStaff(Staff staff) throws BusinessException;

	/**
	 * Lay staff position (co them wifi)
	 * @author vuongmq
	 * @param staffId
	 * @param shopId
	 * @return StaffPositionLog
	 * @throws BusinessException
	 * @since 17/08/2015
	 */
	StaffPositionLog getNewestStaffPositionLog(Long staffId, Long shopId) throws BusinessException;
	
	CustomerVO getCustomerVisitting(Long staffId) throws BusinessException;

	ObjectVO<SupervisorSaleVO> getListSupervisorSaleVO(
			KPaging<SupervisorSaleVO> kPaging, Long tbhvId, Long shopId)
			throws BusinessException;

	/**
	 * @author vuongmq
	 * @date 21/08/2015
	 * @descriptip Lay doanh so ngay, doanh so Luy ke, cua NVGS ung voi DS NVBH 
	 * @param kPaging
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<NVBHSaleVO> getListNVBHSaleVO(KPaging<NVBHSaleVO> kPaging, RptStaffSaleFilter filter) throws BusinessException;

	/**
	 * Lay doanh so ngay, doanh so Luy ke, cua Shop
	 * @author vuongmq
	 * @param kPaging
	 * @param filter
	 * @return ObjectVO<ShopSaleVO>
	 * @throws BusinessException
	 * @Since 21/09/2015
	 */
	ObjectVO<ShopSaleVO> getListShopSaleVO(KPaging<ShopSaleVO> kPaging, RptStaffSaleFilter filter) throws BusinessException;
	
	ObjectVO<CustomerSaleVO> getListCustomerSaleVO(KPaging<CustomerSaleVO> kPaging, Long nvbhId) throws BusinessException;

	AmountPlanStaffVO getAmountPlanOfStaff(StaffObjectType type, Long staffId,Long shopId) throws BusinessException;

	List<CustomerVO> getListCustomerBySaleStaffHasVisitPlan(Long staffId, Date checkDate) throws BusinessException;

	/**
	 * Lay danh sach GSNPP co the di huan luyen cung TBHV
	 * 
	 * @author thuattq
	 * @param tbhvId
	 * @param trainingDate
	 * @return
	 * @throws BusinessException
	 */
	List<StaffTrainingPlanVO> getListTrainingForAreaManager(Long tbhvId,
			Date trainingDate) throws BusinessException;

	/**
	 * Dem tat ca cac LHL cua TBHV tu ngay hien tai den ngay cuoi cung thang duoc chon
	 * 
	 * @author liemtpt
	 * @param tbhvId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	Integer countAllTrainingPlanNVGSByTBHVId(Long tbhvId) throws BusinessException;
	
	/**
	 * Duyet TrainingPlanDetail cho quan ly lich huan luyen
	 */

	ObjectVO<NVBHSaleVO> getListTBHMSaleVO(KPaging<NVBHSaleVO> kPaging, Long gdmId,Long shopId) throws BusinessException;
	/**
	 * Kiem tra shop co duoc quan ly boi NVGS
	 * @author thongnm
	 */
	Boolean checkShopManagedByNVGS(Long gsnppId, Long shopId)throws BusinessException;

	/**SangTN - Ve lo trinh*/
	List<StaffPositionLog> showDirectionStaff(Long staffId)
			throws BusinessException;
	
	/*********************  HUAN LUYEN ********************/
	/**
	 * 
	*  Kiem tra ton tai training plan chua
	*  @author: thanhnn
	*  @param staffCode - ma GSNPP
	*  @param date
	*  @return
	*  @throws BusinessException
	*  @return: Boolean
	*  @throws:
	 */
	TrainingPlan getTrainingPlan(String staffCode,Long shopId, Date date) throws BusinessException;
	//void createTrainingPlanDetailByOwnerIdAndStaffId(Long ownerId, Long staffId, Date date) throws BusinessException;
	void createTrainingPlanDetailByOwnerIdAndStaffId(Shop shopNPP, Long ownerId, Long staffId, Date date) throws BusinessException;
	TrainingPlanDetail getTrainingPlanDetail(String superCode, Date date) throws BusinessException;
	TrainingPlanDetail createTrainingPlanDetail(TrainingPlanDetail trainingPlanDetail, LogInfoVO logInfo) throws BusinessException;
	void deleteListTrainingPlanDetail(List<Long> lstTraningPlanDetailId,List<Long> lstTraningPlans)throws BusinessException;

	Routing getRoutingMultiChoice(Long routingId, Long shopId, String routingCode, Integer status) throws BusinessException;

	RoutingCustomer getRoutingCustomerByRoutingIdAndCustomerId(Long routingId, Long customerId) throws BusinessException;

	VisitPlan getVisitPlanMultiChoce(VisitPlanFilter filter) throws BusinessException;

	void updateVisitPlanSingle(VisitPlan visitPlan) throws BusinessException;

	VisitPlan createVisitPlanSingle(VisitPlan visitPlan) throws BusinessException;

	List<RoutingCustomerVO> getListRouCusWitFDateMinByListCustomerID(List<Long> lstCustomerId, Long routeId) throws BusinessException;

	void insertListCusInRouCus(List<Long> lstCustomerId, RoutingCustomerFilter filter) throws BusinessException;

	Date getMinStartDateByFilter(RoutingCustomerFilter filter) throws BusinessException;

	void updateRoutingCustomerByImport(RoutingCustomer rouCus, RoutingCustomerFilter filter) throws BusinessException;

	/**
	 * Lay danh sach Routing Customer hien tai va tuong lai
	 * 
	 * @author hunglm16
	 * @since October 6,2014
	 * @description Bo sung phan quyen CMS
	 * */
	List<RoutingCustomerVO> getListRusCusByEndateNextSysDate() throws BusinessException;

	List<Staff> getListNVBHInTuyenNew(Long gsnppId, Long shopId) throws BusinessException;

	List<VisitPlan> getListVisitPlanErrByStartDate(Long staffId, Long routingId, Date fDate, Long shopId) throws BusinessException;

	void createVisplanByStaffAndRouting(VisitPlan visitPlan) throws BusinessException;

	List<RoutingCustomer> getListRoutingCustomerListRouCusID(List<Long> lstID) throws BusinessException;

	void updateRoutingCustomer(List<RoutingCustomer> listDelete, List<RoutingCustomer> listUpdate, List<RoutingCustomer> listInsert) throws BusinessException;

	List<RoutingCustomer> getListRouCusErrByStartDate(Long customerId, Long routingId, Date fDate, Long shopId, Long rouCusId)
			throws BusinessException;

	Routing createRoutingImport(Routing routing) throws BusinessException;

	RoutingCustomer createRoutingCustomerImport(RoutingCustomer routingCustomer) throws BusinessException;

	Boolean checkRoutingErrDelete(Long routingId, Long shopId) throws BusinessException;

	List<RoutingCustomerVO> getListRoutingCustomerNew(KPaging<RoutingCustomer> kPaging, Long routingId, ActiveType status, Integer saleDate, Boolean hasOrderBySeq,
			Long shopId, Boolean hasChildShop, Long superId)
			throws BusinessException;

	int countSEQBySeqAndRoutingId(Long routingId, Integer seq) throws BusinessException;

	Boolean countCustomerInRouCusValidImport(String shortCode, String routingCode, Long shopId) throws BusinessException;

	void updateVisitplanImportFile(VisitPlan visitPlan) throws BusinessException;

	ObjectVO<RoutingCustomerVO> getListRoutingCustomerByCusOrRouting(Long routingId, Long shopId, Long customerId, String shortCode,
			String customerName, String address, Date startDate, Date endDate, Integer status) throws BusinessException;

	Routing createRoutingWithEntityNew(Routing routing) throws BusinessException;

	List<Routing> getListRoutingShopId(Long shopId) throws BusinessException;

	List<RoutingCustomer> getListRoutingCustomersBySeq(Long routingId, Integer seq) throws BusinessException;

	List<RoutingVO> exportTuyenBySelect(Long shopId, String routingCode, String routingName, String staffCode, Date sysdate, List<Long> lstRoutingId) throws BusinessException;

	List<VisitPlan> checkExitVisitPlanInShop(Long shopId, Long routingId, Long visitPlanId, Long staffId, Date fromDate, Date toDate) throws BusinessException;

	void updateListRoutingCustomerByListShortCode(List<String> lstShortCode,
			List<Integer> lstSeq, Shop shop, Integer saleDate, String userName,
			Long routingId) throws BusinessException;

	void updateRoutingCustomerImportFile(RoutingCustomer routingCus) throws BusinessException;

	void updateVisitplanByChangeTime(VisitPlan visitPlan, boolean flag, Boolean isAllowDelete) throws BusinessException;

	List<RoutingTreeVO> getListRoutingShopIdNew(Long shopId) throws BusinessException;

	/**
	 * Tim kiem tuyen
	 * @author hunglm16
	 * @description October 6,2014
	 * */
	ObjectVO<RoutingVO> searchListRoutingVOByFilter(RoutingGeneralFilter<RoutingVO> filter) throws BusinessException;

	ObjectVO<TrainingPlanVO> getListTrainingPlan(KPaging<TrainingPlanVO> kPaging, Long shopId, Long nvgsId, Date date, Long shopRootId, Long staffRootId, boolean flagChildCMS) throws BusinessException;

	/**
	 * Doanh so bang tong hop RPT_STAFF_SALE ung voi supervise  truyen vao
	 * @author vuongmq
	 * @param staff
	 * @param filter
	 * @return AmountAndAmountPlanVO
	 * @throws BusinessException
	 * @since 19/08/2015
	 */
	AmountAndAmountPlanVO getAmountAndAmountPlanByStaffSupervise(Staff staff, RptStaffSaleFilter filter) throws BusinessException;
	
	/**
	 * Lay khach hang cua NVBH cho bang tong hop RPT_STAFF_SALE_DETAIL
	 * @author vuongmq
	 * @param filter
	 * @return AmountPlanCustomerVO
	 * @throws BusinessException
	 * @since 19/08/2015
	 */
	AmountPlanCustomerVO getAmountPlanCustomerByStaff(RptStaffSaleFilter filter) throws BusinessException;
	
	
	Double getDistanceOfStaff(StaffFilter filter) throws BusinessException;
	
	Double calcNumKm(List<StaffPositionLog> list) throws BusinessException;

}

package ths.dms.core.business;

import java.util.Date;

import ths.dms.core.entities.ActionAudit;
import ths.dms.core.entities.ActionAuditDetail;
import ths.dms.core.entities.enumtype.ActionAuditType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.RptLogDiffVO;
import ths.dms.core.exceptions.BusinessException;

public interface LogMgr {


	ObjectVO<ActionAuditDetail> getListActionGeneralLogDetail(
			KPaging<ActionAuditDetail> kPaging, Long actionGeneralLogId)
			throws BusinessException;

	/**
	 * @author hungnm
	 * @param kPaging
	 * @param staffCode: nhan vien thuc hien
	 * @param customerId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<ActionAudit> getListActionGeneralLog(
			KPaging<ActionAudit> kPaging, String staffCode,
			Long customerId, Long staffId, Date fromDate, Date toDate)
			throws BusinessException;
	
	/**
	 * 
	*  lay danh sach action audit theo table name
	*  @author: thanhnn
	*  @param kPaging
	*  @param staffCode
	*  @param type
	*  @param fromDate
	*  @param toDate
	*  @return
	*  @throws BusinessException
	*  @return: List<ActionAudit>
	*  @throws:
	 */
	public ObjectVO<ActionAudit> getListActionAudit(KPaging<ActionAudit> kPaging, String staffCode, Long objectId,
			ActionAuditType type, Date fromDate, Date toDate)
			throws BusinessException;

	ObjectVO<ActionAuditDetail> getListActionAuditDetail(
			KPaging<ActionAuditDetail> kPaging, Long actionAuditId)
			throws BusinessException;

	public RptLogDiffVO getRptLogDiffVO(Long objectId, ActionAuditType type, Date fromDate, Date toDate) throws BusinessException;
}

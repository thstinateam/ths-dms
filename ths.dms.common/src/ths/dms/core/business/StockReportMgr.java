package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.core.entities.vo.rpt.RptImExStDataVO;
import ths.dms.core.exceptions.BusinessException;

public interface StockReportMgr {
	List<RptImExStDataVO> getImExStReportReport(Long shopId, Date fromDate,
			Date toDate) throws BusinessException;
	

	
	
}

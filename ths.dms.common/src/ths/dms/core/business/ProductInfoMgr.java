package ths.dms.core.business;


import java.util.List;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.ProductInfoMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductInfoMapType;
import ths.dms.core.entities.enumtype.ProductInfoObjectType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.CatalogFilter;
import ths.dms.core.entities.vo.CatalogVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductInfoVOEx;
import ths.dms.core.exceptions.BusinessException;

public interface ProductInfoMgr {

	ProductInfo createProductInfo(ProductInfo productInfo, LogInfoVO logInfo)
			throws BusinessException;

	void updateProductInfo(ProductInfo productInfo, LogInfoVO logInfo) throws BusinessException;

	void deleteProductInfo(ProductInfo productInfo, LogInfoVO logInfo) throws BusinessException;
	/**
	 * Lấy ProductInfo
	 * @author thongnm
	 */
	ProductInfo getProductInfoByCode(String code, ProductType type,
			String catCode, Boolean isActiveTypeRunning)
			throws BusinessException;
	
	List<ProductInfo> getListSubCat(ActiveType status,ProductType type, Long from_object) throws BusinessException;
	
	List<ProductInfo> getListSubCatByListCat(ActiveType status, ProductType type, List<Long> lstObjectId) throws BusinessException;
	
	List<ProductInfo> getListSubCat(ActiveType status, ProductType type, ProductInfoMapType mapType,
			Long from_object) throws BusinessException;
			
	ObjectVO<ProductInfo> getListProductInfo(KPaging<ProductInfo> kPaging,
			String productInfoCode, String productInfoName, String desc,
			ActiveType status, ProductType type, Boolean isExceptZCat,String sort,String order) throws BusinessException;

	Boolean isUsingByOthers(long productInfoId) throws BusinessException;

	ProductInfo getProductInfoById(Long id) throws BusinessException;

	Boolean removeProductInfo(Long productInfoId, LogInfoVO logInfo);

	ObjectVO<ProductInfo> getListSubCatNotInDisplayGroup(
			KPaging<ProductInfo> kPaging, Long displayGroupId,
			String subCatCode, String subCatName) throws BusinessException;

	ObjectVO<ProductInfo> getListProductInfoForStock(
			KPaging<ProductInfo> kPaging, String productInfoCode,
			String productInfoName, String desc, ActiveType status,
			ProductType type) throws BusinessException;

	ObjectVO<ProductInfo> getListProductInfoFromProduct(
			KPaging<ProductInfo> kPaging, String desc, ActiveType status,
			ProductType type) throws BusinessException;
	
	Boolean isEquimentCat(String catCode) throws BusinessException;

	/*****************************	Nganh hang con phu****************************************/
	/**
	 * Tim kiem danh sach nganh hang con phu
	 * @author thongnm
	 */
	ObjectVO<ProductInfoVOEx> getListProductInfoOfSecondSubCat(
			KPaging<ProductInfoVOEx> kPaging, String catCode,
			String subCatCode, String subCatName) throws BusinessException;
	/**
	 * Danh sach nganh hang trong nganh hang con phu
	 * @author thongnm
	 */
	List<ProductInfo> getListCatInSecondSubCat() throws BusinessException;
	/**
	 * Them moi nganh hang con phu
	 * @author thongnm
	 */
	void addToProductInfoOfSecondSubCat(String catCode, String subCatCode,
			String subCatName, String description, LogInfoVO logInfo) throws BusinessException;
	/**
	 * Cap nhat nganh hang con phu
	 * @author thongnm
	 */
	void updateToProductInfoOfSecondSubCat(String catCode, String subCatCode,
			String subCatName, String description, LogInfoVO logInfo)
			throws BusinessException;

	/**
	 * Lay danh sach Nganh hang theo Filter Basic
	 * 
	 * @author hunglm16
	 * @since September 23,2014
	 * */
	ObjectVO<ProductInfo> getListProductInfoByFilter(BasicFilter<ProductInfo> filter) throws BusinessException;

	/**
	 * @author vuongmq, copy habeco
	 * @date 15/01/2015
	 * @description lay danh sach categories
	 */
	ObjectVO<ProductInfo> getListProductInfoEx(KPaging<ProductInfo> kPaging,
			String productInfoCode, String productInfoName, ActiveType status,
			ProductType type, List<ProductInfoObjectType> objTypes, boolean orderByCode) throws BusinessException;
	
	/**
	 * Lay danh sach Nganh hang theo ma kiem ke
	 * 
	 * @author hoanv25
	 * @since  November, 20,2014
	 * */	
	ObjectVO<ProductInfo> getListProductInfoStock(KPaging<ProductInfo> kPaging,
			String productInfoCode, String productInfoName, String desc,
			ActiveType status, ProductType type) throws BusinessException;

	/**
	 * Lay danh sach type cua san pham
	 * 
	 * @param producttype
	 * @author hoanv25
	 * @since August, 04,2015
	 * */
	ObjectVO<ProductInfo> getListProductInfoTypeStock(KPaging<ProductInfo> kPaging, ActiveType producttype) throws BusinessException;

	/**
	 * Lay danh sach hinh thuc ban hang
	 * 
	 * @param producttype
	 * @author hoanv25
	 * @since August, 14,2015
	 * */
	ObjectVO<ApParam> getListShopProductInfoType(Object object, ActiveType producttype) throws BusinessException;
	
	/**
	 * Lay danh sach danh muc san pham
	 * @author trietptm
	 * @param filter
	 * @return danh sach danh muc san pham
	 * @throws BusinessException
	 * @since Nov 17, 2015
	 */
	ObjectVO<CatalogVO> getListCatalogByFilter(CatalogFilter filter) throws BusinessException;
	
	/**
	 * Check danh sach nganh hang
	 * 
	 * @param code , type, catCode, isActiveTypeRunning
	 * @author hoanv25
	 * @since August, 14,2015
	 * */
	ProductInfo getProductInfoByCodeCat(String code, ProductType type, Boolean isActiveTypeRunning) throws BusinessException;

	/**
	 * @author vuongmq
	 * @param code
	 * @param type
	 * @param isActiveTypeRunning
	 * @return ProductInfo
	 * @throws BusinessException
	 * @since 23/10/2015
	 */
	ProductInfo getProductInfoByWithNotSubCat(String code, ProductType type, Boolean isActiveTypeRunning) throws BusinessException;

	/**
	 * lay danh sach nganh hang con su dung nganh hang
	 * @author trietptm
	 * @param catId
	 * @param status
	 * @return danh sach nganh hang con su dung nganh hang nay 
	 * @throws BusinessException
	 * @since Nov 17, 2015
	 */
	List<ProductInfo> getListSubCatByCatId(Long catId, ActiveType status) throws BusinessException;
	
	/**
	 * chuyen status thanh delete cho Product Info
	 * @author trietptm
	 * @param productInfo
	 * @param logInfo
	 * @throws BusinessException 
	 * @since Nov 28, 2015
	*/
	void changeStatusDeleteProductInfo(ProductInfo productInfo, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * tao ProductInfoMap
	 * @author trietptm
	 * @param pdInfoMap
	 * @param logInfo
	 * @return ProductInfoMap moi tao
	 * @throws BusinessException
	 * @since Nov 17, 2015
	 */
	ProductInfoMap createProductInfoMap(ProductInfoMap pdInfoMap, LogInfoVO logInfo) throws BusinessException;

	/**
	 * cap nhat ProductInfoMap
	 * @author trietptm
	 * @param pdInfoMap
	 * @param logInfo
	 * @throws BusinessException
	 * @since Nov 17, 2015
	 */
	void updateProductInfoMap(ProductInfoMap pdInfoMap, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * lay ProductInfoMap theo nganh hang va nganh hang con
	 * @author trietptm
	 * @param id
	 * @return
	 * @throws BusinessException
	 * @since Nov 17, 2015
	 */
	ProductInfoMap getProductInfoMap(long catId, long subCatId, ProductInfoMapType productInfoMapType) throws BusinessException;

	/**
	 * lay ProductInfoMap theo to_object_id
	 * @author trietptm
	 * @param toId
	 * @param productInfoMapType
	 * @return
	 * @throws BusinessException
	 * @since Nov 17, 2015
	 */
	ProductInfoMap getProductInfoMapByToId(long toId, ProductInfoMapType productInfoMapType) throws BusinessException;

	/**
	 * cap nhat ProductInfo va ProductInfoMap
	 * @author trietptm
	 * @param productInfo
	 * @param parentProductInfo
	 * @param logInfo
	 * @throws BusinessException
	 * @since Nov 28, 2015
	*/
	void updateProductInfo(ProductInfo productInfo, ProductInfo parentProductInfo, LogInfoVO logInfo) throws BusinessException;

	/**
	 * tao moi ProductInfo va ProductInfoMap
	 * @author trietptm
	 * @param productInfo
	 * @param parentProductInfo
	 * @param logInfo
	 * @return new ProductInfo
	 * @throws BusinessException
	 * @since Nov 28, 2015
	*/
	ProductInfo createProductInfo(ProductInfo productInfo, ProductInfo parentProductInfo, LogInfoVO logInfo) throws BusinessException;

}

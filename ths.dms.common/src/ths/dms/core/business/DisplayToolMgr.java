package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.DisplayTool;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.DisplayToolCustomerFilter;
import ths.dms.core.entities.filter.DisplayToolFilter;
import ths.dms.core.entities.vo.DisplayToolCustomerVO;
import ths.dms.core.entities.vo.DisplayToolVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffSimpleVO;
import ths.dms.core.exceptions.BusinessException;

/**
 * The Interface DisplayToolMgr.
 * @author thongnm
 */
public interface DisplayToolMgr {

	void deleteListDisplayTools(List<Long> listId) throws BusinessException;
	
	ObjectVO<DisplayToolVO> getListDisplayTool(DisplayToolFilter filter) throws BusinessException;
	
	ObjectVO<Product> getListProductOfDisplayTool(KPaging<Product> kPaging,Long displayToolId) throws BusinessException;
	
	ObjectVO<Product> getListProductForProductTool(KPaging<Product> kPaging,String productCode, String productName, String toolCode)throws BusinessException;

	ObjectVO<DisplayToolVO> getListDisplayToolOfProduct(DisplayToolFilter filter) throws BusinessException;
	
	ObjectVO<DisplayToolCustomerVO> getListDisplayToolCustomerVo(KPaging<DisplayToolCustomerVO> kPaging,DisplayToolCustomerFilter filter) throws BusinessException;
	
	/*
	 * Check List tồn tại
	 */
	List<DisplayTool> checkListDisplayTool(List<DisplayTool> list) throws BusinessException;
	/*
	 * Them list display tool
	 */
	void createListDisplayTool(List<DisplayTool> list)throws BusinessException;
	/*
	 * Thêm sản phẩm vào tủ
	 */
	void createDisplayToolListProducts(Long id, List<Long> _list)throws BusinessException;
	/*
	 * Xoá sản phẩm trong tủ
	 */
	void deleteDisplayToolListProducts(Long id, List<Long> _list)throws BusinessException;
	/*
	 * Xoá tủ
	 */
	void deleteDisplayTool(Long id)throws BusinessException;	
	
	Boolean checkDateMonthToDay(Long shopId,Long staffId,String month) throws BusinessException;
	
	ObjectVO<DisplayTool> getListDisplayTool(KPaging<DisplayTool> kPaging,DisplayToolFilter filter) throws BusinessException;

	ObjectVO<Product> getListProductToMTDisplayTeamplate(KPaging<Product> kPaging, String productCode, String productName,
			Long idMTDisplayTeamplate) throws BusinessException;

	List<Product> getProductToIdDisplayTemplateAutocomlete(KPaging<Product> kPaging, String productCode, String productName,
			Long idMTDisplayTeamplate) throws BusinessException;

	Product checkProductToMTDisplayTeamplateChange(String productCode, Long idMTDisplayTeamplate) throws BusinessException;

	/* Sao chep nhieu khach hang muon tu cua nhieu nhan vien */
	List<StaffSimpleVO> getListDisplayToolStaffId(DisplayToolFilter filter) throws BusinessException;
}

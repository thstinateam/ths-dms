package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.OfficeDocShopMap;
import ths.dms.core.entities.OfficeDocument;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.filter.OfficeDocumentFilter;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.MediaItemShopVO;
import ths.dms.core.entities.vo.MediaItemVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OfficeDocmentVO;
import ths.dms.core.exceptions.BusinessException;

public interface ImageMgr {
	/** Quan ly tai lieu */
	/**
	 * @author tientv
	 * @des: Tao cong van
	 */
//	OfficeDocument createDocument(OfficeDocument document)throws BusinessException;	
	
	/**
	 * @author tientv
	 * @des : Danh sach cong van
	 */
	ObjectVO<OfficeDocument> getListOfficeDoc(KPaging<OfficeDocument> kPaging,OfficeDocumentFilter filter) throws BusinessException;
	
	/**
	 * @author tientv
	 * @des: Xoa cong van
	 */
//	void deleteDocument(Long Id)throws BusinessException;	
	
	
	
	void updateMediaItem(MediaItem image) throws BusinessException;
	
	
	
	/**
	 * Ham nay se lay all hinh anh dươi dang view binh thuong
	 * @return
	 */
	ObjectVO<ImageVO> getListImageVO(ImageFilter filter) throws BusinessException;
	/**
	 * 
	 * @param filter
	 * @return tra về một list album đã được group
	 * @throws BusinessException
	 */
	ObjectVO<ImageVO> getListImageVOGroup(ImageFilter filter) throws BusinessException;
	/**
	 * 
	 * @param filter nhớ truyền vào customer
	 * @return Tra ve 1 list hình ảnh thuộc 1 gruop
	 * @throws BusinessException
	 */
	ObjectVO<ImageVO> getListImageVODetail(ImageFilter filter) throws BusinessException;
	/**
	 * Ham tìm kiếm cttb
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<ImageVO> getListCTTB(ImageFilter filter) throws BusinessException;
	/**
	 * Lay ds cttb theo cay
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	List<ImageVO> getListCTTBByNPP(Long id)throws BusinessException;
	
	List<ImageVO> getListCTTBByListShop(List<Long> lstShopId)throws BusinessException;
	/**
	 * Xoá hình ảnh khỏi cttb
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	Boolean deleteImageVO(Long id) throws BusinessException;
	
	MediaItem getMediaItemById(Long id) throws BusinessException;
	
//	ObjectVO<ImageVO1> getListImagesKA(KPaging<ImageVO1> kPaging,
//			Long shopId, Long staffId, Date fromDate, Date toDate,
//			Integer type, Long customerId) throws BusinessException;

	Integer insertOfficeDocument(OfficeDocmentVO filter) throws BusinessException;

	Integer deleteOfficeDocument(OfficeDocument officeDocument)
			throws BusinessException;

	OfficeDocument getOfficeDocumentById(Long id) throws BusinessException;

	MediaItem getMediaItemByDocumentID (Long id) throws BusinessException;
	MediaItem getMediaItemByDocumentID (Long id, Integer object_type) throws BusinessException; 

	OfficeDocument getOfficeDocumentByCode(String documentCode)
			throws BusinessException;

	Integer deleteOffDocShopMapALL(List<OfficeDocShopMap> lstoffDoc,
			OfficeDocmentVO filterVO) throws BusinessException;

	Integer deleteOffDocShopMap(List<Shop> listShop, OfficeDocument offDoc,
			OfficeDocmentVO filterVO) throws BusinessException;

//	Integer updateOffDocShopMap(List<Shop> listShop, OfficeDocument offDoc,
//			OfficeDocmentVO filterVO) throws BusinessException;

	List<OfficeDocShopMap> getListOffShopMapToDocumentId(Long documentID)
			throws BusinessException;

	Integer insertOffDocShopMap(List<Shop> listShop, OfficeDocument offDoc,
			OfficeDocmentVO filterVO) throws BusinessException;
	
	//DuyNQ
	List<MediaItemShopVO> getListMediaItemShopVO(
			List<Long> lstCustomerId, Date fromDate, Date toDate,
			String displayProgramCode) throws BusinessException;

	ObjectVO<ImageVO> getListImageVOMT(ImageFilter filter)
			throws BusinessException;

	ObjectVO<ImageVO> getListImageVODetailMT(ImageFilter filter)
			throws BusinessException;

	ObjectVO<ImageVO> getListImageVOGroupMT(ImageFilter filter)
			throws BusinessException;

	MediaItemVO getMediaItemVOById(Long id) throws BusinessException;
}

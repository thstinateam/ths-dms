package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.Attribute;
import ths.dms.core.entities.AttributeDetail;
import ths.dms.core.entities.AttributeValue;
import ths.dms.core.entities.AttributeValueDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDetailFilter;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumFilter;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.AttributeFilter;
import ths.dms.core.entities.enumtype.AttributeVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.TableHasAttribute;
import ths.dms.core.entities.vo.DynamicAttributeVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

public interface AttributeMgr {

	Attribute createAttribute(Attribute attribute) throws BusinessException;

	void deleteAttribute(Attribute attribute) throws BusinessException;

	void updateAttribute(Attribute attribute) throws BusinessException;
	
	Attribute getAttributeById(long id) throws BusinessException;

	Attribute getAttributeByCode(String table, String code)
			throws BusinessException;
	
	ObjectVO<AttributeVO> getListAttributeVO(
			AttributeFilter filter, KPaging<AttributeVO> kPaging)
			throws BusinessException;
	
	AttributeDetail createAttributeDetail(AttributeDetail attributeDetail) throws BusinessException;

	void deleteAttributeDetail(AttributeDetail attributeDetail) throws BusinessException;
	
	void deleteListAttributeDetailByAttribute(Long attributeId) throws BusinessException;

	void updateAttributeDetail(AttributeDetail attributeDetail) throws BusinessException;
	
	AttributeDetail getAttributeDetailById(long id) throws BusinessException;

	AttributeDetail getAttributeDetailByCode(Long attributeId,
			String attributeDetailCode) throws BusinessException;

	ObjectVO<AttributeDetailVO> getListAttributeDetailVO(
			AttributeDetailFilter filter, KPaging<AttributeDetailVO> kPaging)
			throws BusinessException;
	
	Boolean isUsingByOther(String attributeCode)
			throws BusinessException;
	
	Boolean isExistAttributeDetailCode(Long attributeId,
			String attributeDetailCode) throws BusinessException;

	ObjectVO<Attribute> getListAttribute(KPaging<Attribute> kPaging,
			TableHasAttribute table, String attributeCode,
			String attributeName, AttributeColumnType columnType,
			ActiveType status) throws BusinessException;

	ObjectVO<AttributeDetailVO> getListAttributeDetailByAttributeId(
			KPaging<AttributeDetailVO> kPaging, Long id, ActiveType status)
			throws BusinessException;

	List<AttributeValueDetail> getListValue4Select(Long objId, Long attributeId, ActiveType status)
			throws BusinessException;

	AttributeValue getValueOfObject(Long objId, Long attributeId)
			throws BusinessException;
	/****
	    * Danh sach thuoc tinh dong cho nhan vien,san pham va khach hang
	    * @author liemtpt
	    * @param AttributeDynamicFilter
	    * @param kPaging
	    * @return list attribute dynamic vo
	    * @throws BusinessException
	    */
	ObjectVO<AttributeDynamicVO> getListAttributeDynamicVO(
			AttributeDynamicFilter filter, KPaging<AttributeDynamicVO> kPaging)
			throws BusinessException;
	/****
	    * Danh sach chi tiet gia tri thuoc tinh dong cho nhan vien,san pham va khach hang
	    * @author liemtpt
	    * @param AttributeDynamicFilter
	    * @param kPaging
	    * @return list attribute dynamic vo
	    * @throws BusinessException
	    */
	ObjectVO<AttributeEnumVO> getListAttributeDetailDynamicVO(
			AttributeEnumFilter filter, KPaging<AttributeEnumVO> kPaging)
			throws BusinessException;
	/****
	    * kiem tra ma thuoc tinh da ton tai trong DB chua
	    * @author liemtpt
	    * @param AttributeDynamicFilter
	    * @return TRUE: ton tai, FALSE: khong ton tai
	    * @throws BusinessException
	    */
	Boolean checkExistAttributeDynamic(AttributeDynamicFilter filter)
			throws BusinessException;
	/****
	    * @desciption: kiem tra thuoc tinh co ton tai trong bang Enum chua
	    * @author liemtpt
	    * @param AttributeDynamicFilter
	    * @return TRUE: ton tai, FALSE: khong ton tai
	    * @throws BusinessException
	    */
	Boolean checkExistAttributeEnumDynamic(AttributeEnumFilter filter)
			throws BusinessException;
	
	/**
	 * Lay cac thuoc tinh dong theo loai entity
	 * @author tuannd20
	 * @param clazz Loai doi tuong can lay thuoc tinh dong
	 * @param status Trang thai cua thuoc tinh dong
	 * @return Danh sach thuoc tinh dong dang phang (thuoc tinh, gia tri thuoc tinh)
	 * @throws BusinessException
	 * @since 03/09/2015
	 */
	<T> List<DynamicAttributeVO> getDynamicAttribute(Class<T> clazz, ActiveType status) throws BusinessException;
}

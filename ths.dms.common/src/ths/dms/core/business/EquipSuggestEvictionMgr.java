package ths.dms.core.business;

/**
 * Import thu vien
 * */
import java.util.List;

import ths.dms.core.entities.EquipLend;
import ths.dms.core.entities.EquipLendDetail;
import ths.dms.core.entities.EquipSuggestEviction;
import ths.dms.core.entities.EquipSuggestEvictionDTL;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.filter.EquipSuggestEvictionFilter;
import ths.dms.core.entities.vo.EquipBorrowVO;
import ths.dms.core.entities.vo.EquipSuggestEvictionDetailVO;
import ths.dms.core.entities.vo.EquipmentSuggestEvictionVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Xu ly du lieu
 * 
 * @author nhutnn
 * @since 14/07/2015
 * @description Dung cho Quan ly de nghi thu hoi thiet bi
 * */
public interface EquipSuggestEvictionMgr {
	/**
	 * Lay danh sach bien ban de nghi thu hoi tu
	 * 
	 * @author nhutnn
	 * @since 14/07/2015
	 * */
	ObjectVO<EquipmentSuggestEvictionVO> searchListEquipSuggestEvictionVOByFilter(EquipSuggestEvictionFilter<EquipmentSuggestEvictionVO> filter) throws BusinessException;

	/**
	 * lay bien ban bang id
	 * @author nhutnn	 
	 * @param id
	 * @return
	 * @throws BusinessException
	 * @since 15/07/2015
	 */
	EquipSuggestEviction getEquipSuggestEvictionById(Long id) throws BusinessException;
	
	/**
	 * lay bien ban bang ma bien ban
	 * @author nhutnn	 
	 * @param code
	 * @return
	 * @throws BusinessException
	 * @since 15/07/2015
	 */
	EquipSuggestEviction getEquipSuggestEvictionByCode(String code) throws BusinessException;
	
	/**
	 * Lay danh sach chi tiet bien ban
	 * @author nhutnn
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @since 05/07/2015
	 */
	List<EquipSuggestEvictionDTL> getListEquipSuggestEvictionDetailByFilter(EquipSuggestEvictionFilter<EquipSuggestEvictionDTL> filter) throws BusinessException;
	
	/**
	 * Cap nhat bien ban
	 * @author nhutnn
	 * @param filter
	 * @throws BusinessException
	 * @since 15/07/2015 
	 */
	void updateEquipSuggestEviction(EquipSuggestEvictionFilter<EquipSuggestEviction> filter) throws BusinessException;
	
	/**
	 * Lay dan sach xuat excel
	 * @author nhutnn
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since 20/07/2015
	 */
	List<EquipmentSuggestEvictionVO> getListEquipSuggestEvictionDetailVOForExportByFilter(EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO> filter) throws BusinessException;
	
	/**
	 * Tao bien ban thu hoi thiet bi
	 * @author nhutnn
	 * @param equipLend
	 * @param lstEquipLendDetails
	 * @return
	 * @throws BusinessException
	 * @since 21/07/2015
	 */
	EquipSuggestEviction createEquipSuggestEvictionWithListDetail(EquipSuggestEviction equipSuggestEviction, List<EquipSuggestEvictionDTL> lstEquipSuggestEvictionDTLs) throws BusinessException;
	
	/**
	 * Lay danh sach chi tiet bien ban 
	 * @author nhutnn
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @since 30/07/2015
	 */
	List<EquipSuggestEvictionDetailVO> getListEquipSuggestEvictionDetailVOByFilter(EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO> filter) throws BusinessException;
}
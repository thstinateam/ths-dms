package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.entities.Kpi;
import ths.dms.core.entities.KpiImplement;
import ths.dms.core.entities.SalePlan;
import ths.dms.core.entities.SalePlanVersion;
import ths.dms.core.entities.SalesPlan;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PlanType;
import ths.dms.core.entities.enumtype.SalePlanOwnerType;
import ths.dms.core.entities.vo.DistributeSalePlanVO;
import ths.dms.core.entities.vo.DynamicStaffKpiSaveVO;
import ths.dms.core.entities.vo.DynamicStaffKpiVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.SalePlanVO;
import ths.dms.core.entities.vo.SumSalePlanVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.CommonFilter;

/**
 * 
 * @author hieunq1
 *
 */
public interface SalePlanMgr {

	/**
	 * createSalePlan
	 * @author hieunq1
	 * @param salePlan
	 * @return
	 * @throws BusinessException
	 */
	SalePlan createSalePlan(SalePlan salePlan) throws BusinessException;

	/**
	 * updateSalePlan
	 * @author hieunq1
	 * @param salePlan
	 * @throws BusinessException
	 */
	void updateSalePlan(SalePlan salePlan) throws BusinessException;

	/**
	 * createSalePlanVersion
	 * @author hieunq1
	 * @param salePlanVersion
	 * @return
	 * @throws BusinessException
	 */
	SalePlanVersion createSalePlanVersion(SalePlanVersion salePlanVersion)
			throws BusinessException;

	/**
	 * getMaxVersionBySalePlanSersionId
	 * @author hieunq1
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	Integer getMaxVersionBySalePlanSersionId(long id) throws BusinessException;
	
	/**
	 * lay danh sach saleplan theo dieu kien search
	 * @author thanhnguyen
	 * @param parentId
	 * @param productId
	 * @param month
	 * @param year
	 * @param type
	 * @return
	 * @throws BusinessException
	 */
	List<DistributeSalePlanVO> getListSalePlanWithCondition(Long shopId, Long ownerId, String productCode,
			Integer month, Integer year, List<String> lstCateCode, PlanType type) throws BusinessException;


	/**
	 * getSumSalePlan
	 * @author hieunq1
	 * @param shopId
	 * @param productId
	 * @param shopType
	 * @param planType
	 * @param month
	 * @param year
	 * @return
	 * @throws BusinessException
	 */
	SumSalePlanVO getSumSalePlan(Long ownerId, Long productId,
			SalePlanOwnerType shopType, PlanType planType, int num, int year)
			throws BusinessException;
	
	/**
	 * update 1 dong SalePlan and SalePlanVersion cho module phan bo chi tieu thang
	 * @author thanhnguyen
	 * @param salePlan
	 * @throws BusinessException
	 */
	void updateSalePlanAndSalePlanVesion(SalePlan salePlan,String currentUser)
			throws BusinessException;
	/**
	 * insert 1 dong SalePlan and SalePlanVersion cho module phan bo chi tieu thang
	 * @author thanhnguyen
	 * @param salePlan
	 * @throws BusinessException
	 */
	void insertSalePlanAndSalePlanVersion(SalePlan salePlan)
			throws BusinessException;

	/**
	 * getListSalePlanVOForProduct
	 * @author hieunq1
	 * @param kPaging
	 * @param shopId
	 * @param staffCode
	 * @param catCode
	 * @param productCode
	 * @param month
	 * @param year
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<SalePlanVO> getListSalePlanVOForProduct(KPaging<SalePlanVO> kPaging,
			Long shopId, String staffCode, String catCode, String productCode,
			Integer month, Integer year) throws BusinessException;

	
	/**
	 * phan bo chi tieu theo thang cho NPP
	 * @author thanhnguyen
	 * @param shopId
	 * @param month
	 * @param year
	 * @param lstProductId
	 * @param lstQty
	 * @throws BusinessException
	 */
	void createSalePlanForShop(Long shopId, Integer num, Integer year,
			List<Long> lstProductId, List<Integer> lstQty,List<BigDecimal> lstAmount, String createUser)
					throws BusinessException;

	/**
	 * getListStaffByShopIdSR
	 * @author hieunq1 - > thuattq
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	/*List<Staff> getListStaffByShopIdSR(Long shopId) throws BusinessException;*/

	/**
	 * 
	 * @author hieunq1
	 * @param kPaging
	 * @param shopId
	 * @param staffCode
	 * @param catCode
	 * @param productCode
	 * @param month
	 * @param year
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<SalePlanVO> getListSalePlanVOProductOfShop(
			KPaging<SalePlanVO> kPaging, Long shopId, String staffCode,
			String catCode, String productCode,String productName, Integer month, Integer year, Integer fromQty, Integer toQty)
			throws BusinessException;
	
	
	SalePlan getSalePlanById(long id) throws BusinessException;
	
	/**
	 * 
	*  lay saleplan de kiem tra xem da ton tai hay chua - chi kiem tra cho shop
	*  @author: thanhnn8
	*  @param shopId
	*  @param catCode
	*  @param month
	*  @param year
	*  @param type
	*  @return
	*  @throws BusinessException
	*  @return: SalePlan
	*  @throws:
	 */
	SalePlan getSalePlanByCondition(Long ownerId, String catCode, Integer month,
			Integer year, PlanType type) throws BusinessException;
	
	/**
	 * 
	*  lay danh sach sale plan phan quan ly ke hoach tieu thu cua nvbh (phan ket hop tablet)
	*  @author: thanhnn
	*  @param kPaging
	*  @param shopId
	*  @param staffId
	*  @param productId
	*  @param month
	*  @param year
	*  @return
	*  @throws BusinessException
	*  @return: ObjectVO<SalesPlan>
	*  @throws:
	 */
	ObjectVO<SalesPlan> getListSalePlanForManagerPlan(
			KPaging<SalesPlan> kPaging, Long shopId, Long staffId,
			Long productId, Integer month, Integer year) throws BusinessException;
	
	
	/**
	 * 
	 *  lay danh sach sale plan phan quan ly ke hoach tieu thu cua nvbh (phan ket hop tablet)
	 *  @author: thanhnn
	 *  @param kPaging
	 *  @param shopId
	 *  @param staffId
	 *  @param productCode
	 *  @param month
	 *  @param year
	 *  @return
	 *  @throws BusinessException
	 *  @return: ObjectVO<SalesPlan>
	 *  @throws:
	 */
	ObjectVO<SalesPlan> getListSalePlanForManagerPlanSearchLike(
			KPaging<SalesPlan> kPaging, Long shopId, Long staffId,
			String productCode, Integer month, Integer year) throws BusinessException;
	
	/**
	 * 
	*  Kiem tra sale plan da ton tai trong database hay chua?
	*  @author: thanhnn
	*  @param ownerId
	*  @param productId
	*  @param ownerType
	*  @param planType
	*  @param month
	*  @param year
	*  @return
	*  @throws BusinessException
	*  @return: SalePlan
	*  @throws:
	 */
	SalePlan getSalePlanOfVNM(Long ownerId, Long productId, SalePlanOwnerType ownerType,
			PlanType planType, Integer month, Integer year) throws BusinessException;

	/**
	 * @author hungnm
	 * @param objectCode
	 * @param objectType
	 * @param month
	 * @param year
	 * @return
	 * @throws DataAccessException
	 * @throws BusinessException 
	 */
	Boolean checkSalePlan(String objectCode, SalePlanOwnerType objectType,
			String productCode, Integer month, Integer year)
			throws BusinessException;
	
	ObjectVO<DistributeSalePlanVO> getListSalePlanWithNoCondition(
			KPaging<DistributeSalePlanVO> kPaging,String productCode,String productName) throws BusinessException;

	List<SalePlanVO> getListSalePlanVO(Long shopId, String staffCode,
			List<String> lstCatCode, String productCode, Integer num,
			Integer year, Long gsnppId) throws BusinessException;

	ObjectVO<SalePlanVO> getListSalePlanVOProductOfParentShop(
			KPaging<SalePlanVO> kPaging, Long shopId, String productCode,
			String productName, Integer month, Integer year, Integer fromQty,
			Integer toQty) throws BusinessException;

	ObjectVO<SalePlan> getListSalePlan(KPaging<SalePlan> paging,
			String staffCode, List<String> lstCatCode, String productCode,
			Integer month, Integer year) throws BusinessException;

	SalePlan getSalePlan(String staffCode, String productCode, Integer num,
			Integer year) throws BusinessException;

	ObjectVO<SalePlanVO> getListSalePlanVOForLoadPage(
			KPaging<SalePlanVO> paging, Long shopId, Integer month, Integer year)
			throws BusinessException;

	List<SalePlanVO> getListSalePlanVO4KT(Long shopId, String staffCode,
			List<String> lstCatCode, String productCode, Integer month,
			Integer year) throws BusinessException;
	
	/**
	 * Lay version xuat file xml PO Auto
	 * @author tungtt21
	 * @param ownerId
	 * @param productId
	 * @param ownerType
	 * @param planType
	 * @param month
	 * @param year
	 * @return
	 * @throws BusinessException
	 */
	Integer getVersion(Long ownerId, Long productId, SalePlanOwnerType ownerType,
			PlanType planType, Integer month, Integer year) throws BusinessException;

	void createSalePlanForStaff(Long staffId, Integer month, Integer year, List<Long> lstProductId, List<Integer> lstQty, List<BigDecimal> lstAmount, String createUser, Long shopRootId) throws BusinessException;
	
	/**
	 * Lay danh sach chi tieu KPI 
	 * @author trungtm6
	 * @since 09/07/2015
	 * @param shopId
	 * @param year
	 * @param cycleNum
	 * @return
	 * @throws BusinessException
	 */
	List<DynamicStaffKpiVO> getListDynamicStaffKpi(Long shopId, Long cycleId) throws BusinessException;
	
	ObjectVO<Kpi> getListKpiByFilter(KPaging<Kpi> kPaging, CommonFilter filter) throws BusinessException;
	
	public void createOrUpdateListKi(List<DynamicStaffKpiSaveVO> lstKpiInfo, LogInfoVO log) throws BusinessException;
	
	public KpiImplement getKpiImplement(Long staffId, Long kpiId, Long cycleId) throws BusinessException;
	
}

package ths.dms.core.business;


import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;


public interface CycleMgr {
	
	Cycle createCycle(Cycle cycle, LogInfoVO logInfo) throws BusinessException;

	void updateCycle(Cycle cycle, LogInfoVO logInfo) throws BusinessException;

	void deleteCycle(Cycle cycle, LogInfoVO logInfo) throws BusinessException;

	Cycle getCycleById(Long id) throws BusinessException;

	Cycle getCycleByDate(Date curdate) throws BusinessException;

	Cycle getCycleByYearAndNum(int year, int num) throws BusinessException;

	List<Integer> getListYearOfAllCycle() throws BusinessException;

	List<CycleVO> getListCycleByFilter(CycleFilter filter)
			throws BusinessException;

	/**
	 * lay danh sach KPI theo chu ky
	 * 
	 * @param cycleId
	 * @author hoanv25
	 * @since July 24/2015
	 * 
	 */
	List<CycleVO> getListKPIByCycle(Long cycleId) throws BusinessException;

	/**
	 * lay danh sach KPI theo chu ky
	 * 
	 * @param cycleId
	 * @param lstKeyShopId
	 * @param fromDate
	 * @param toDate
	 * @author hoanv25
	 * @since July 24/2015
	 * 
	 */
	List<CycleVO> getListKeyShopByCycle(Long cycleId, List<Long> lstKeyShopId, String fromDate, String toDate) throws BusinessException;
	
	/**
	 * Lay chu ky hien tai
	 * @author hunglm16
	 * @return
	 * @throws DataAccessException
	 * @since 19/09/2015
	 */
	Cycle getCycleByCurrentSysdate() throws BusinessException;

	/**
	 * @author vuongmq
	 * @param filter
	 * @return Cycle
	 * @throws BusinessException
	 * @since 23/10/2015
	 */
	Cycle getCycleByFilter(CycleFilter filter) throws BusinessException;

	/**
	 * Lay cycle sau cycle Id truyen vao
	 * @author vuongmq
	 * @param cycleId
	 * @return Cycle
	 * @throws BusinessException
	 * @since 12/11/2015
	 */
	Cycle getCycleLast(Long cycleId) throws BusinessException;
}

package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.RptDebitMonth;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.RptDebitMonthObjectType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.RptCompareBookAndDeliveryProductVO;
import ths.core.entities.vo.rpt.RptInputProductVinamilkVO;
import ths.core.entities.vo.rpt.RptPoVnmDebitComparisonVO;
import ths.core.entities.vo.rpt.RptPoVnmVO;
import ths.core.entities.vo.rpt.RptReturnSaleOrderParentVO;
import ths.dms.core.exceptions.BusinessException;

public interface PoReportMgr {

	/**
	 * 3.1.4.5	Bảng đối chiếu công nợ mua hàng
	 * @author hieunq1
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	RptPoVnmDebitComparisonVO getRptPoVnmDebitComparisonVO(Long shopId, Date fromDate, Date toDate)
			throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param ownerId
	 * @param ownerType
	 * @return
	 * @throws BusinessException
	 */
	RptDebitMonth getRptDebitMonthByOwner(Long ownerId, RptDebitMonthObjectType ownerType, Date date)
			throws BusinessException;
	
	
	/**3.1.4.1	Bảng kê chứng từ hàng hóa mua vào
	 * @author hungnm
	 * @param kPaging
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<RptPoVnmVO> getListRptPoVnmVO(KPaging<RptPoVnmVO> kPaging,
			Date fromDate, Date toDate, Long shopId) throws BusinessException;

	/**
	 * 3.1.3.9 bao cao chuyen doi don hang
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param deliveryId
	 * @return
	 * @throws BusinessException
	 */
	List<RptReturnSaleOrderParentVO> getListChangeReturnSaleOrder(Long shopId,
			Date fromDate, Date toDate, Long deliveryId) throws BusinessException;
	
	/**
	 * 3.1.4.8 danh sach bao cao nhap hang tu vinamilk
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	List<RptInputProductVinamilkVO> getListRptImportProduct(
			Long shopId, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * 3.1.4.7 lay danh sach bao cao so sach so luong dat va giao hang
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	List<RptCompareBookAndDeliveryProductVO> getListCompareBookAndDeliveryProduct(
			Long shopId, Date fromDate, Date toDate) throws BusinessException;


}

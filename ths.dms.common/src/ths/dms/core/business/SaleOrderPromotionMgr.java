/**
 * mgr thao tac voi sale_order_promotion
 * @author tuannd20
 * @version 1.0
 * @since 11/10/2014
 */
package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.filter.SaleOrderPromotionFilter;
import ths.dms.core.exceptions.BusinessException;

public interface SaleOrderPromotionMgr {
	/**
	 * get list sale order promotion by filter
	 * @author tuannd20
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @date 11/10/2014
	 */
	List<SaleOrderPromotion> getSaleOrderPromotions(SaleOrderPromotionFilter filter) throws BusinessException;
}

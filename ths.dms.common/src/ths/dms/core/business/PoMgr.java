package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ths.dms.core.entities.PoAuto;
import ths.dms.core.entities.PoAutoDetail;
import ths.dms.core.entities.PoAutoGroup;
import ths.dms.core.entities.PoAutoGroupDetail;
import ths.dms.core.entities.PoAutoGroupShopMap;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.PoVnmDetail;
import ths.dms.core.entities.PoVnmLot;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PayReceivedType;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailVO;
import ths.dms.core.entities.enumtype.PoAutoGroupFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupShopMapFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupVO;
import ths.dms.core.entities.enumtype.PoObjectType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.PoVnmFilterBasic;
import ths.dms.core.entities.vo.CreatePoVnmDetailVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoAuto01VO;
import ths.dms.core.entities.vo.PoAutoGroupByCatVO;
import ths.dms.core.entities.vo.PoAutoGroupShopMapVO;
import ths.dms.core.entities.vo.PoAutoVO;
import ths.dms.core.entities.vo.PoCSVO;
import ths.dms.core.entities.vo.PoDVKHVO;
import ths.dms.core.entities.vo.PoSecVO;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.entities.vo.PoVnmDetailVO;
import ths.dms.core.entities.vo.PoVnmStockInVO;
import ths.dms.core.entities.vo.ProductInfoVOEx;
import ths.core.entities.vo.rpt.RptF1Lv01VO;
import ths.core.entities.vo.rpt.RptPoAutoLV01VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface PoMgr {
	/**
	 * Lay PoAuto by Id
	 * 
	 * @param id
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	PoAuto getPoAutoById(Long id) throws BusinessException;
	PoAutoGroupShopMap getPoAutoGroupShopMapById(Long id) throws BusinessException;
	/**
	 * Lay PoVnm bay id
	 * 
	 * @param id
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	PoVnm getPoVnmById(Long id) throws BusinessException;

	/**
	 * Lay PoVnm bay code
	 * 
	 * @param code
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	PoVnm getPoVnmByCode(String code) throws BusinessException;

	/**
	 * 
	 * @param kPaging
	 * @param shopId
	 * @param poAutoNumber
	 * @param fromDate
	 * @param toDate
	 * @param status
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<PoAuto> getListPoAuto(KPaging<PoAuto> kPaging, Long shopId,
			String poAutoNumber, Date fromDate, Date toDate,
			ApprovalStatus status) throws BusinessException;

	/**
	 * lay danh sach po auto phuc vu cho mang hinh quan ly po_auto
	 * 
	 * @param shopId
	 * @param poAutoNumber
	 * @param fromDate
	 * @param toDate
	 * @param status
	 * @param hasFindChildShop true: lay tat ca shop con, false: chi lay shop truyen vao
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<PoAuto01VO> getListPoAuto01(Long shopId, String poAutoNumber,
			Date fromDate, Date toDate, ApprovalStatus status,
			boolean hasFindChildShop) throws BusinessException;

	/**
	 * 
	 * @param kPaging
	 * @param poAutoId
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<PoAutoDetail> getListPoAutoDetail(KPaging<PoAutoDetail> kPaging,
			Long poAutoId) throws BusinessException;

	/**
	 * getListPoAuto
	 * @author hieunq1
	 * @param shopId
	 * @param checkAll
	 * @return
	 * @throws BusinessException
	 */
	List<PoAutoVO> getListPoAuto(Long shopId, Boolean checkAll) throws BusinessException;

	/**
	 * cap nhat nhap hang
	 * @param PONumber
	 * @param invoice_number
	 * @param quantity_check
	 * @param amout_check
	 * @param discount
	 * @throws BusinessException
	 * @author ThanhNN
	 */
	/*void updateInputPoVnm(Long shop_id, Long poVnmId, String PONumber, String invoice_number,
			Integer quantity_check, BigDecimal amout_check, BigDecimal discount, String createUser)
			throws BusinessException;*/

	/**
	 * @author vuongmq
	 * @date 11/08/2015
	 * @desciption Nhap /tra hang ung voi quantity input
	 * @param filter
	 * @throws BusinessException
	 */
	void updateInputPoVnm(PoVnmFilterBasic<PoVnm> filter) throws BusinessException;
	
	void updatePoVnm(PoVnm poVnm) throws BusinessException;

	/**
	 * 
	 * @param kPaging
	 * @param shopId
	 * @param invoiceNumber
	 * @param fromDate
	 * @param toDate
	 * @param PONumber
	 * @return
	 * @throws BusinessException
	 * @author ThanhNN
	 */
//	ObjectVO<PoVnm> getListObjectVOPoConfirm(KPaging<PoVnm> kPaging,
//			Long shopId, String invoiceNumber, Date fromDate, Date toDate,
//			String PONumber, PoVNMStatus poStatus, boolean hasFindChildShop)
//			throws BusinessException;

	/**
	 * Xu ly getListObjectVOPoConfirm
	 * @author vuongmq
	 * @param filter
	 * @return ObjectVO<PoVnmStockInVO>
	 * @throws BusinessException
	 * @since Jan 27, 2016
	 */
	ObjectVO<PoVnmStockInVO> getListObjectVOPoConfirm(PoVnmFilter filter) throws BusinessException;

	/**
	 * Lay danh sach PoConfirm theo cac dieu kien cho truoc
	 * 
	 * @param shopId
	 * @param poNumber
	 * @param invoiceNumb
	 * @param fromDate
	 * @param toDate
	 * @param poStatus
	 * @param hasFindChildShop true: tim trong tat ca shop con, false: tim trong shop tryen vao
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
//	List<PoVnm> getListPoConfirmByCondition(Long shopId, String poNumber,
//			String invoiceNumb, Date fromDate, Date toDate,
//			PoVNMStatus poStatus, boolean hasFindChildShop)
//			throws BusinessException;

	 List<PoVnm> getListPoConfirmByCondition(PoVnmFilter filter) throws BusinessException;
	 
	 /**
	 * @author vuongmq
	 * @date 07/08/2015
	 * @description lay danh sach po confirm theo filter
	 * Dung cho Lay danh sach Po confirm dieu chinh hoa don
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	 List<PoVnm> getListPoConfirmByFilter(PoVnmFilter filter) throws BusinessException;
	
	 /**
	 * Danh dau mot Po dich vu khach hang da xu ly treo
	 * 
	 * @param listPo
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 * TUNGTT chinh sua them shopId
	 */
	boolean setSuspendPoVNM(List<PoVnm> listPo, Long shopId) throws BusinessException;

	/**
	 * 
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<PoVnm> getListPoConfirm() throws BusinessException;

	/**
	 * 
	 * @param shopId
	 * @param poAutoNumber
	 * @param fromDate
	 * @param toDate
	 * @param poStatus
	 * @return
	 * @throws BusinessException
	 * @author HieuNQ
	 */
//	List<PoVnm> getListPoDVKH(Long shopId, String poAutoNumber, Date fromDate,
//			Date toDate, PoVNMStatus poStatus, boolean hasFindChildShop)
//			throws BusinessException;
	List<PoVnm> getListPoDVKH(PoVnmFilter filter)
			throws BusinessException;

	/**
	 * tao po auto
	 * 
	 * @author hieunq1
	 * @param poAuto
	 * @param listPoAutoDetail
	 * @throws BusinessException
	 */
	public void createShopPoAuto(Long shopId, Long staffId,
			Boolean checkAllProductType) throws BusinessException;

	/**
	 * lay danh sach po duoc chon
	 * 
	 * @author hieunq1
	 * @param poVnmId
	 * @return
	 * @throws BusinessException
	 */
	List<PoVnmLot> getListPoVnmLotByPoVnmId(Long poVnmId)
			throws BusinessException;

	/**
	 * Kiem tra PoConfirm co qua ngay quy dinh tra khong
	 * 
	 * @return
	 * @author ThuatTQ
	 * TUNGTT them shopId
	 */
	boolean checkValidateDatePoReturn(PoVnm poConfirm, Long shopId) throws BusinessException;

	/**
	 * Huy danh sach PoAuto
	 * 
	 * @param listPoAuto
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	void refusePoAuto(List<PoAuto> listPoAuto) throws BusinessException;

	/**
	 * Duyet danh sach PoAuto
	 * 
	 * @param listPoAuto
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	void approvedPoAuto(List<PoAuto> listPoAuto) throws BusinessException;

	/**
	 * kiem tra ton tai cua povnm voi don hang
	 * 
	 * @author hieunq1
	 * @param poCoNumber
	 * @return
	 * @throws BusinessException
	 */
	boolean checkExistedPoVnmByPoCoNumber(String poCoNumber)
			throws BusinessException;

	boolean checkQuantityInStockTotal(long shopId, long productId, int quantity)
			throws BusinessException;

	/**
	 * lay PO DVKH theo PO_AUTO_NUMBER
	 * 
	 * @author thanhnguyen
	 * @param poAutoNumber
	 * @return
	 * @throws BusinessException
	 */
	PoVnm getPoDVKH(String poAutoNumber) throws BusinessException;

	boolean checkQuantityInProductLot(long shopId, long productId, int quantity, String lot)
			throws BusinessException;

	/**
	 * tao PoVnm
	 * @author hieunq1
	 * @param poVnm
	 * @return
	 * @throws BusinessException
	 */
	PoVnm createPoVnm(PoVnm poVnm) throws BusinessException;

	void updatePoVnmDetail(PoVnmDetail poVnmDetail) throws BusinessException;

	void updateProductLot(ProductLot productLot) throws BusinessException;

	PoVnmDetail getPoVnmDetailById(long id) throws BusinessException;

	ProductLot getProductLotById(long id) throws BusinessException;
	
	List<PoVnmDetailVO> getListPoVnmVoDetail(Long poVnmId, Long shopId) throws BusinessException;
	
	List<PoVnmDetail> getListPoVnmVoDetail(Long poVnmId, Long shopId,Long productId) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	PoVnmLot getPoVnmLotById(long id) throws BusinessException;
	
	/**
	 * Lay du lieu cho bao cao so sanh xuat nhap ton F1. Khong dung ham nay nua,
	 * chuyen qua dung ham getDataForF1Report
	 * 
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	// List<RptCompareImExStF1VO> getDataForCompareImExStF1Report(Long shopId,
	// Date fromDate, Date toDate)
	// throws BusinessException;
	
	/**
	 * Lay du lieu cho BC so sanh phan QL PoAuto
	 * 
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	/*List<RptCompareImExStF1VO> getDataForComparePOAutoReport(Long shopId,
			Long poId) throws BusinessException;*/
	List<PoAutoGroupByCatVO> getDataForComparePOAutoReport(Long shopId,
			Long poId) throws BusinessException;

	/**
	 * getListPoVnmVoDetailByPoVnmId
	 * @author hieunq1
	 * @param poVnmId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<PoVnmDetail> getListPoVnmDetailByPoVnmId(KPaging<PoVnmDetail> kPaging, Long poVnmId)
			throws BusinessException;

	/**
	 * getListPoVnmDetailLotVOByPoVnmId
	 * @author hieunq1
	 * @param poVnmId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmId(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId)
			throws BusinessException;
	/**
	 * getListPoVnmDetailLotVOByPoVnmId
	 * @author vuongmq
	 * @param poVnmId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmIdStockIn(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId)
			throws BusinessException;

	/**
	 * getListPoVnmLotByPoVnmDetailId
	 * @author hieunq1
	 * @param poVnmDetailId
	 * @return
	 * @throws BusinessException
	 */
	List<PoVnmLot> getListPoVnmLotByPoVnmDetailId(Long poVnmDetailId)
			throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param poVnmDetail
	 * @return
	 * @throws BusinessException
	 */
	PoVnmDetail createPoVnmDetail(PoVnmDetail poVnmDetail)
			throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param poVnmLot
	 * @return
	 * @throws BusinessException
	 */
	PoVnmLot createPoVnmLot(PoVnmLot poVnmLot) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param poVnmLot
	 * @throws BusinessException
	 */
	void updatePoVnmLot(PoVnmLot poVnmLot) throws BusinessException;

	/**
	 * getProductLotByProductAndOwner
	 * @author hieunq1
	 * @param productId
	 * @param ownerId
	 * @param ownerType
	 * @return
	 * @throws BusinessException
	 */
	List<ProductLot> getProductLotByProductAndOwner(long productId,
			long ownerId, StockObjectType ownerType) throws BusinessException;

	/**
	 * updateListStockTotal
	 * @author hieunq1
	 * @param listStockTotal
	 * @throws BusinessException
	 */
	void updateListStockTotal(List<StockTotal> listStockTotal)
			throws BusinessException;

	/**
	 * createListPoVnmDetail
	 * @author hieunq1
	 * @param listPoVnmDetail
	 * @return
	 * @throws BusinessException
	 */
	void createListPoVnmDetail(List<PoVnmDetail> listPoVnmDetail)
			throws BusinessException;

	/**
	 * updateListPoVnmDetail
	 * @author hieunq1
	 * @param listPoVnmDetail
	 * @throws BusinessException
	 */
	void updateListPoVnmDetail(List<PoVnmDetail> listPoVnmDetail)
			throws BusinessException;
	/**
	 * createListProductLot
	 * @author hieunq1
	 * @param listProductLot
	 * @return
	 * @throws BusinessException
	 */
	void createListProductLot(List<ProductLot> listProductLot)
			throws BusinessException;
	/**
	 * updateListProductLot
	 * @author hieunq1
	 * @param listProductLot
	 * @throws BusinessException
	 */
	void updateListProductLot(List<ProductLot> listProductLot)
			throws BusinessException;
	/**
	 * createListPoVnmLot
	 * @author hieunq1
	 * @param listPoVnmLot
	 * @return
	 * @throws BusinessException
	 */
	void createListPoVnmLot(List<PoVnmLot> listPoVnmLot)
			throws BusinessException;
	/**
	 * updateListPoVnmLot
	 * @author hieunq1
	 * @param listPoVnmLot
	 * @throws BusinessException
	 */
	void updateListPoVnmLot(List<PoVnmLot> listPoVnmLot)
			throws BusinessException;
//	/**
//	 * createReturnPoVnm
//	 * @author hieunq1
//	 * @param shopId
//	 * @param oPoVnmId
//	 * @param oListPoVnmDetailId
//	 * @param oListPoVnmDetailValue
//	 * @param oListPoVnmLotId
//	 * @param oListPoVnmLotValue
//	 * @param nPoVnm
//	 * @param nMapPoVnmDetailLot
//	 * @return
//	 * @throws BusinessException
//	 */
//	void createReturnPoVnm(long shopId, long oPoVnmId,
//			List<Long> oListPoVnmDetailId, List<Integer> listPoVnmDetailQty,
//			List<Long> oListPoVnmLotId, List<Integer> listPoVnmLotQty,
//			PoVnm nPoVnm, MapVO<PoVnmDetail, List<PoVnmLot>> nMapPoVnmDetailLot)
//			throws BusinessException;

	/**
	 * createReturnPoVnm
	 * @author phuongvm
	 * @param shopId
	 * @param oPoVnmId
	 * @param oLstPoVnmDetailId
	 * @param oLstCheckLot
	 * @param lstPoVnmDetailQty
	 * @param invoiceCode
	 * @param invoiceDate
	 * @param checkQty
	 * @param checkValue
	 * @return 
	 * @throws BusinessException
	 */
	PoVnm createReturnPoVnm(Long shopId, Long oPoVnmId,
			List<Long> oLstPoVnmDetailId, List<Boolean> oLstCheckLot,
			List<Integer> lstPoVnmDetailQty, String invoiceNumber,
			Date invoiceDate, Integer quantityCheck, BigDecimal amountCheck, BigDecimal disCount, String user,
			List<Long> lstWarehouseId,List<Long> lstProductIdKho,List<Integer> lstValue,List<Long> lstPoVNMDetailId)
			throws BusinessException;

	/**
	 * Lay list Po DVKH theo dieu kien
	 * @param shopId
	 * @param poAutoNumber
	 * @param fromDate
	 * @param toDate
	 * @param poStatus
	 * @param hasFindChildShop true: lay tat ca shop con, false: chi lay shop truyen vao
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<PoCSVO> getListPoCSVO(Long shopId, String orderNumber, String poCoNumber,
			Date fromDate, Date toDate, PoVNMStatus poStatus, boolean hasFindChildShop)
			throws BusinessException;
	List<PoCSVO> getListPoCSVOEx(Long shopId, String orderNumber, String poCoNumber,
			Date fromDate, Date toDate, List<Integer> lstPoStatus, boolean hasFindChildShop)
			throws BusinessException;
	
	/**
	 * Lay du lieu cho report BC PoAuto cho NPP
	 * 
	 * @param listShopId
	 * @param toDate
	 * @param fromDate
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<RptPoAutoLV01VO> getDataForReportPoAutoShop(List<Long> listShopId,
			Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * Lay du lieu cho bao cao xuat nhap ton F1
	 * 
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<RptF1Lv01VO> getDataForF1Report(Long shopId, Date fromDate, Date toDate)
			throws BusinessException;

	List<PoDVKHVO> getPoDVKHforReport(Long shopId, Date fromDate, Date toDate,
			Long poVnmId) throws BusinessException; 
	
	/**
	 * 
	*  Duyet don hang po auto
	*  @author: thanhnn
	*  @param poAuto
	*  @param listProductId
	*  @param listQuantity
	*  @throws BusinessException
	*  @return: void
	*  @throws:
	 */
	void updateConfirmPoAuto(PoAuto poAuto, List<Long> listProductId,
			List<Integer> listQuantity) throws BusinessException;
	
	
	/**
	* 
	*  Tu choi po auto
	*  @author: thanhnn
	*  @param listPoAuto
	*  @throws BusinessException
	*  @return: void
	*  @throws:
	*/
	void rejectPoAuto(List<PoAuto> listPoAuto) throws BusinessException;
	
	ObjectVO<PoVnm> getListObjectVOPoVnm(PoVnmFilter filter, KPaging<PoVnm> kPaging)
			throws BusinessException;
	
	PoVnm createPoConfirm(PoVnm poConfirm,
			List<CreatePoVnmDetailVO> lstPoConfirmDetail)
			throws BusinessException;

	PoVnm transferPoDVKH(PoVnm poDVKH) throws BusinessException;

	/**
	 * @author hungnm
	 * @param poDvkhId
	 * @return
	 * @throws BusinessException
	 */
	Integer checkProductInPoDVKH(Long poDvkhId) throws BusinessException;

	PoVnm transferPoConfirm(PoVnm poConfirm) throws BusinessException;

	ObjectVO<PoVnm> getListPoConfirmByPoCS(KPaging<PoVnm> kPaging, PoVnm poCS,
			PoObjectType objectType) throws BusinessException;
	
	/**
	 * Xu ly getListPoConfirmByPoCSFromPoId
	 * @author vuongmq
	 * @param kPaging
	 * @param poCS
	 * @param objectType
	 * @return ObjectVO<PoVnm> 
	 * @throws BusinessException
	 * @since Jan 28, 2016
	 */
	ObjectVO<PoVnm> getListPoConfirmByPoCSFromPoId(KPaging<PoVnm> kPaging, PoVnm poCS, PoObjectType objectType) throws BusinessException;
	
	ObjectVO<PoVnmDetailLotVO> getPoConfirmDetail(
			KPaging<PoVnmDetailLotVO> kPaging, Long poDVKHId, Long poConfirmId)
			throws BusinessException;

	void updateListPoVnm(List<PoVnm> lstPoVnm) throws BusinessException;
	
	PoAutoGroup createPoAutoGroup(PoAutoGroup poAutoGroup) throws BusinessException;

	void deletePoAutoGroup(PoAutoGroup poAutoGroup) throws BusinessException;

	void updatePoAutoGroup(PoAutoGroup poAutoGroup) throws BusinessException;

	PoAutoGroup getPoAutoGroupById(Long id) throws BusinessException;
	
	ObjectVO<PoAutoGroupVO> getPoAutoGroupVO(
			PoAutoGroupFilter filter, KPaging<PoAutoGroupVO> kPaging)
			throws BusinessException;
	
	PoAutoGroupDetail createPoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail) throws BusinessException;

	void deletePoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail) throws BusinessException;

	void updatePoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail) throws BusinessException;

	PoAutoGroupDetail getPoAutoGroupDetailById(Long id) throws BusinessException;
	
	ObjectVO<PoAutoGroupDetail> getListPoAutoGroupDetail(KPaging<PoAutoGroupDetail> kPaging,
			Long poAutoGroupId) throws BusinessException;
	
	ObjectVO<PoAutoGroupDetailVO> getListPoAutoGroupDetailVO(PoAutoGroupDetailFilter filter, KPaging<PoAutoGroupDetailVO> kPaging) throws BusinessException;
	
	ObjectVO<ProductInfoVOEx> getListProductInfoCanBeAdded(
			int poAutoGroupId, String name, String code, boolean isSubCat, KPaging<ProductInfoVOEx> kPaging)
			throws BusinessException;

	ObjectVO<Product> getListProductCanBeAdded(
			int poAutoGroupId, String name, String code, KPaging<Product> kPaging)
			throws BusinessException;

	ObjectVO<PoSecVO> getListPoSecVO(KPaging<PoSecVO> paging,Long shopId,
			String poConfirmNumber, Date fromDate, Date toDate, Integer type)
			throws BusinessException;
	
	Boolean isExistsInfoOfPoAutoGroup(PoAutoGroup poAutoGroup)
			throws BusinessException;

	PoAutoGroup getPoAutoGroupByCode(String groupCode) throws BusinessException;

	void createSec(String payReceivedNumber, BigDecimal amount, Long poVnmId,
			Long shopId, String staffSignIn, Long debitDetailId, PayReceivedType payReceivedType , Integer bankId)
			throws BusinessException;
	
	/**
	 * Tao sec so tay giam no npp voi cong ty
	 * 
	 * @author lacnv1
	 * @since Apr 12, 2014
	 */
	void createSecGiamNo(String payReceivedNumber, BigDecimal amount, Long poVnmId,
			Long shopId, String staffSignIn, Long debitDetailId, Integer bankId)
			throws BusinessException;

	PoAutoGroupShopMap createPoAutoGroupShopMap(PoAutoGroupShopMap item,
			LogInfoVO logInfo) throws BusinessException;

	void updatePoAutoGroupShopMap(PoAutoGroupShopMap item, LogInfoVO logInfo)
			throws BusinessException;

	void deletePoAutoGroupShopMap(PoAutoGroupShopMap item, LogInfoVO logInfo)
			throws BusinessException;

	
	ObjectVO<PoAutoGroupShopMapVO> getListShopInPoGroup(PoAutoGroupShopMapFilter filter,
			ActiveType groupStatus) throws BusinessException;
	ObjectVO<PoAutoGroupVO> getPoAutoGroupVOEx(PoAutoGroupFilter filter,
			KPaging<PoAutoGroupVO> kPaging) throws BusinessException;
	
	ObjectVO<PoVnmDetailLotVO> getListFullPoVnmDetailVOByListPoVnmId(KPaging<PoVnmDetailLotVO> kPaging, List<Long> lstPoVnmId)
			throws BusinessException;

	void removePOConfirm(Long poVnmId) throws BusinessException;
	
	Map<String, List<PoAutoDetail>> getMapPOAuto(Long shopId, Long staffId, Boolean checkAllProductType) throws BusinessException;
	ObjectVO<ProductInfoVOEx> getListProductInfoCanBeAddedNPP(
			int poAutoGroupId, String name, String code, boolean isSubCat,
			KPaging<ProductInfoVOEx> kPaging) throws BusinessException;
	/**
	 * phuongvm
	 * @param kPaging
	 * @param poVnmId
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmIdEx(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId, Long shopId) throws BusinessException;
	
	/***
	 * @author vuongmq
	 * @date 03/08/2015
	 * @description lay danh sach POVNM don nhap, don tra
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<PoCSVO> getListPoCSVOFilter(PoVnmFilter filter) throws BusinessException;
	
	/**
	 * Xu ly getPoDVKHBySaleOrderNumber
	 * @author vuongmq
	 * @param filter
	 * @return PoVnm
	 * @throws BusinessException
	 * @since Dec 25, 2015
	 */
	PoVnm getPoDVKHBySaleOrderNumber(PoVnmFilter filter) throws BusinessException;
}

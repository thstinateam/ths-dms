package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.DataFormatException;

import ths.dms.core.entities.Car;
import ths.dms.core.entities.Invoice;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.InvoiceCustPayment;
import ths.dms.core.entities.enumtype.InvoiceFilter;
import ths.dms.core.entities.enumtype.InvoiceStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.PrintOrderFilter;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.SaleOrderDetailFilter;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.ConfirmFilter;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.vo.CommercialSupportCodeVO;
import ths.dms.core.entities.vo.CommercialSupportVO;
import ths.dms.core.entities.vo.GroupLevelDetailVO;
import ths.dms.core.entities.vo.KeyShopVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.OrderXmlObject;
import ths.dms.core.entities.vo.PrintDeliveryCustomerGroupVO;
import ths.dms.core.entities.vo.PrintDeliveryGroupExportVO;
import ths.dms.core.entities.vo.PrintDeliveryGroupVO;
import ths.dms.core.entities.vo.PrintOrderVO;
import ths.dms.core.entities.vo.ProductStockTotal;
import ths.dms.core.entities.vo.ProgramCodeVO;
import ths.dms.core.entities.vo.SaleOrderDetailGroupVO;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderLotVOEx;
import ths.dms.core.entities.vo.SaleOrderPromoLotVO;
import ths.dms.core.entities.vo.SaleOrderPromotionDetailVO;
import ths.dms.core.entities.vo.SaleOrderPromotionVO;
import ths.dms.core.entities.vo.SaleOrderStockDetailVO;
import ths.dms.core.entities.vo.SaleOrderStockVO;
import ths.dms.core.entities.vo.SaleOrderStockVOTemp;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.SaleOrderVOEx;
import ths.dms.core.entities.vo.SaleOrderVOEx2;
import ths.dms.core.entities.vo.SaleProductVO;
import ths.dms.core.entities.vo.SearchSaleTransactionErrorVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.TaxInvoiceVO;
import ths.core.entities.vo.rpt.RptAggegateVATInvoiceDataVO;
import ths.core.entities.vo.rpt.RptBHTSP_DSNhanVienBH;
import ths.core.entities.vo.rpt.RptExSaleOrder2VO;
import ths.core.entities.vo.rpt.RptOrderByPromotionDataVO;
import ths.core.entities.vo.rpt.RptPGHVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_DataConvert;
import ths.core.entities.vo.rpt.RptSaleOrderByProductLv01VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

/**
 * @author hungnm
 * 
 */
public interface SaleOrderMgr {
	static final Integer ORDER_COMFIRMED_SUCCESS = 0;	// xac nhan don hang thanh cong
	static final Integer ORDER_COMFIRMED_ALREADY = 1;	// don hang da duoc xac nhan
	static final Integer SALE_ORDER_OVER_REWARD_MONEY = 2;	// don hang vuot muc tra thuong tien
	static final Integer SALE_ORDER_OVER_REWARD_PRODUCT = 3;	// don hang vuot muc tra thuong san pham 
	static final Integer SALE_ORDER_OVER_QUANTITY_STOCK_TOTAL = 4;	// ton kho khong dap ung
	static final Integer ORDER_CONFIRM_ERROR = 128;	// loi he thong khi xac nhan don hang 

	void updateSaleOrder(SaleOrder saleOrder) throws BusinessException;

	SaleOrder getSaleOrderById(Long id) throws BusinessException;

	StockTotal getStockTotalByProductAndOwnerForUpdate(Long productId, StockObjectType ownerType, Long ownerId, Long warehouseId) throws BusinessException;
	
	/**
	 * Tao don hang
	 * 
	 * @author nhanlt6
	 * @param saleOrder
	 * @param listSaleProductVO
	 * @param listPromoProductVO
	 * @param List
	 *            SaleOrderPromotionVO
	 * @param List
	 *            SaleOrderPromotionDetailVO
	 * @return saleOrder
	 * @throws BusinessException
	 *             the business exception
	 * @since Aug 23, 2014
	 */
	SaleOrder createSaleOrder(SaleOrder saleOrder, List<SaleProductVO> lstSaleProductVO, List<SaleProductVO> lstPromoProductVO, List<SaleOrderPromotionVO> lstOrderPromotion, List<SaleOrderPromotionDetailVO> lstSOPromoDetail,
			List<GroupLevelDetailVO> lstCTTL, Integer isSaveNotCheckDebit, List<KeyShopVO> lstKeyShop, List<SaleOrderPromoLotVO> lstPromoLot,LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * In hoa don thue GTGT
	 * 
	 * @author hieunq1
	 * @param kPaging
	 * @param taxValue
	 * @param orderNumber
	 * @param fromDate
	 * @param toDate
	 * @param staffCode
	 * @param custCode
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<Invoice> getListInvoice(InvoiceFilter filter) throws BusinessException;
	
	void updateInvoice(Invoice invoice) throws BusinessException;

	/**
	 * lay danh sach don hang voi dieu kien tuong ung
	 * 
	 * @author thanhnguyen
	 * @param vat
	 * @param orderNumber
	 * @param fromDate
	 * @param toDate
	 * @param customerCode
	 * @param staffCode
	 * @param hasInvoice
	 *            - check cho truong hop chinh sua hay huy don hang
	 * @return
	 * @throws BusinessException
	 */
	List<SaleOrder> getListSaleOrderWithCondition(Float vat, String orderNumber, Date fromDate, Date toDate, String customerCode, String staffCode, Boolean hasInvoice) throws BusinessException;

	/**
	 * tao invoice khi cap nhat lai hoa don GTGT
	 * 
	 * @author thanhnguyen
	 * @param saleOrderId
	 * @param invoiceNumber
	 * @param listCustPayment
	 * @throws BusinessException
	 */
	void createInvoiceFromSaleOrder(Long shopId, String username, Map<Long, List<String>> listSaleOrder, Map<Long, List<InvoiceCustPayment>> listCustPayment) throws BusinessException;

	/**
	 * in phieu giao hang chi dinh nhan vien giao hang
	 * 
	 * @author hieunq1
	 * @param staffCode
	 * @param deliveryCode
	 * @param carCode
	 * @param approvalStatus
	 * @param fromDate
	 * @param toDate
	 * @param type
	 *            (type of sale_order not order_type)
	 * @return
	 * @throws BusinessException
	 */
	/*
	 * ObjectVO<SaleOrder> getListSaleOrderForDelivery(KPaging<SaleOrder>
	 * paging, Long shopId, String staffCode, String deliveryCode, String
	 * custCode, String orderNumber, Long carId, List<OrderType> lstOrderType,
	 * Date fromDate, Date toDate, SaleOrderType type, Boolean isPrint) throws
	 * BusinessException;
	 */

	/**
	 * @author vuongmq
	 * @param paging
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @description: Láº¥y danh sĂ¡ch Ä‘Æ¡n hĂ ng Ä‘á»ƒ chá»‰ Ä‘á»‹nh nhĂ¢n viĂªn giao hĂ ng
	 */
	ObjectVO<SaleOrder> getListSaleOrderForDelivery(KPaging<SaleOrder> paging, SoFilter filter) throws BusinessException;

	/**
	 * cap nhat nv giao hang va xe giao hang
	 * 
	 * @author hieunq1
	 * @param lstSaleOrderId
	 * @param deliveryId
	 * @param carId
	 * @throws BusinessException
	 */
	void updateSaleOrderForDelivery(List<Long> lstSaleOrderId, Long deliveryId, Long cashierId, Long carId) throws BusinessException;

	/**
	 * Lay thong tin don hang tu tablet day ve voi trang thai yeu cau xac nhan.
	 * 
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<SaleOrder> getListSaleOrderFromTablet() throws BusinessException;

	/**
	 * Tim kiem don hang tu tablet
	 * 
	 * @param orderNumber
	 * @param approved
	 *            Chi chap nhan gia tri: null, 2, 3 (danh rieng cho tablet)
	 * @param customerCode
	 * @param customerName
	 * @param staffId
	 * @param priority
	 * @param orderDate
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<SaleOrder> getListSaleOrderFromTabletByCondition(String orderNumber, SaleOrderStatus approved, String customerCode, String customerName, Long staffId, Integer priority, Date fromDate, Date toDate) throws BusinessException;

	/**
	 * Kiem tra mat hang trong tat ca cac don hang truyen vao co du so luong ton
	 * kho dap ung ko
	 * 
	 * @param shopId
	 * @param listOrderId
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<Product> checkEnoughProductQuantityInListOrder(Long shopId, List<Long> listOrderId) throws BusinessException;

	/**
	 * Chap nhan nhung dong hang tu tablet goi len
	 * 
	 * @param listSaleOrderId
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<SaleOrder> acceptSaleOrderFromTable(List<Long> listSaleOrderId, LogInfoVO logInfoVo) throws BusinessException;

	/**
	 * Tu choi don hang goi len tu tablet
	 * 
	 * @param listSaleOrderId
	 * @param importCode
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	void cancelSaleOrderFromTable(List<Long> listSaleOrderId, String importCode) throws BusinessException;

	/**
	 * In hoa don thue gia tri gia tang (tax invoice)
	 * 
	 * @param saleOrderId
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	TaxInvoiceVO getTaxInvoice(Long saleOrderId) throws BusinessException;

	ObjectVO<SaleOrderLot> getListSaleOrderLotBySaleOrderDetail(KPaging<SaleOrderLot> kPaging, long saleOrderDetailId) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param kPaging
	 * @param saleOrderId
	 * @param isFreeItem
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<SaleOrderDetailGroupVO> getListSaleOrderDetailGroupVO(KPaging<SaleOrderDetailGroupVO> kPaging, Long saleOrderId, Integer isFreeItem) throws BusinessException;

	/**
	 * lay san pham cho tao don hang
	 * 
	 * @author nhanlt6
	 * @param kPaging
	 * @param shopId
	 * @param staffId
	 * @param custId
	 * @param shopTypeFilter 
	 * @param customerTypeId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<OrderProductVO> getListOrderProductVO(KPaging<OrderProductVO> kPaging, Long shopId, Long staffId, Long custId, String pdCode, String pdName, String ppCode, Date lockDay, Long custTypeId, Long shopTypeFilter) throws BusinessException;

	/**
	 * approvedSaleOrder
	 * 
	 * @author hieunq1
	 * @param saleOrderId
	 * @param isApproved
	 * @param note
	 * @return
	 * @throws BusinessException
	 */
	boolean approvedSaleOrder(Long saleOrderId, Boolean isApproved, String note) throws BusinessException;

	void updateListSaleOrder(List<SaleOrder> lstSalesOrder) throws BusinessException;

	void updateListSaleOrderDetail(List<SaleOrderDetail> lstSalesOrder) throws BusinessException;

	/**
	 * @author hungnm
	 * @param lstSalesOrder
	 * @throws BusinessException
	 */
	void deleteListSaleOrder(List<SaleOrder> lstSalesOrder, String updatedStaffCode,LogInfoVO logInfoVO) throws BusinessException;

	public void deleteListSaleOrderByListId(List<Long> lstSalesOrderId, String updatedStaffCode,LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Gets the list sale order with invoice.
	 * 
	 * @author phut
	 * 
	 * @param saleStaffCode
	 *            the sale staff code
	 * @param deliveryStaffCode
	 *            the delivery staff code
	 * @param vat
	 *            the vat
	 * @param orderNumber
	 *            the order number
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param customerCode
	 *            the customer code
	 * @return the list sale order with invoice
	 * @throws BusinessException
	 *             the business exception
	 */
	/*
	 * List<SaleOrder> getListSaleOrderWithInvoice(Long shopId, String
	 * saleStaffCode, String deliveryStaffCode, Float vat, String orderNumber,
	 * Date fromDate, Date toDate, String customerCode, InvoiceCustPayment
	 * custPayment) throws BusinessException;
	 */

	/**
	 * Gets the list sale order with no invoice.
	 * 
	 * @author phut
	 * 
	 * @param saleStaffCode
	 *            the sale staff code
	 * @param deliveryStaffCode
	 *            the delivery staff code
	 * @param vat
	 *            the vat
	 * @param orderNumber
	 *            the order number
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param customerCode
	 *            the customer code
	 * @return the list sale order with no invoice
	 * @throws BusinessException
	 *             the business exception
	 */
	/*
	 * List<SaleOrder> getListSaleOrderWithNoInvoice(Long shopId, String
	 * saleStaffCode, String deliveryStaffCode, Float vat, String orderNumber,
	 * Date fromDate, Date toDate, String customerCode) throws
	 * BusinessException;
	 */

	/**
	 * huy hoa don
	 * 
	 * @author thanhnguyen
	 * @param lstinvoiceId
	 * @param lstsaleOrderId
	 * @throws BusinessException
	 */
	void removeInvoice(List<Long> lstInvoiceId, Long shopId, String userName) throws BusinessException;

	SaleOrderDetail createSaleOrderDetail(SaleOrderDetail saleOrderDetail) throws BusinessException;

	void updateSaleOrderDetail(SaleOrderDetail saleOrderDetail) throws BusinessException;

	void deleteSaleOrderDetail(SaleOrderDetail saleOrderDetail) throws BusinessException;

	/**
	 * Lay du lieu cho bao cao Ban hang theo san pham
	 * 
	 * @param shopId
	 * @param staffIds
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<RptSaleOrderByProductLv01VO> getDataForSaleByProductReport(Long shopId, List<Long> staffIds, Date fromDate, Date toDate) throws BusinessException;

	/**
	 * Lay du lieu cho Bao cao chi tiet khuyen mai theo chuong trinh
	 * 
	 * @param shopId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<RptOrderByPromotionDataVO> getDataForOrderByPromotionReport(Long shopId, List<Long> staffId, Date fromDate, Date toDate) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param saleOrderId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<SaleOrderDetail> getListSaleOrderDetailBySaleOrderId(KPaging<SaleOrderDetail> kPaging, Long saleOrderId) throws BusinessException;

	/**
	 * 
	 * @author nhanlt
	 * @param saleOrderId
	 * @return
	 * @throws BusinessException
	 */
	List<SaleOrderDetail> getListSaleOrderDetailBySaleOrderIdAndType(Long saleOrderId, Boolean isAutoPromotion) throws BusinessException;

	/**
	 * Lay du lieu cho Bao cao Ban ke hoa don GTGT
	 * 
	 * @param shopId
	 * @param customerId
	 * @param staffId
	 * @param VAT
	 * @param status
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<RptAggegateVATInvoiceDataVO> getDataForAggegateVATInvoiceReport(Long shopId, List<Long> customerId, Long staffId, Integer VAT, InvoiceStatus status, Date fromDate, Date toDate) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param listSaleOrderId
	 * @return
	 * @throws BusinessException
	 */
	ArrayList<PrintDeliveryGroupExportVO> getListPrintDeliveryGroupExportVO(List<Long> lstSaleOrderId, Date fromDate, Date toDate) throws BusinessException;

	SaleOrder updateReturnSaleOrder(long saleOrderId, String createUser, boolean flag) throws BusinessException;

	ObjectVO<SaleOrderDetailVOEx> getListSaleOrderDetailVOEx(KPaging<SaleOrderDetailVOEx> kPaging, Long saleOrderId, Integer isFreeItem, ProgramType programType, Boolean isAutoPromotion, Long customerTypeId, Long shopId, Date lockDay)
			throws BusinessException;

	public ObjectVO<SaleOrderDetailVOEx> getListSaleOrderDetailVOExForTablet(SaleOrder saleOrder, Integer isFreeItem, ProgramType programType, Boolean isAutoPromotion, Date lockDay, LogInfoVO logInfo) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param lstInvoiceNumber
	 * @return
	 * @throws BusinessException
	 */
	List<Invoice> getListInvoiceByLstInvoiceNumber(Long shopId, List<String> lstInvoiceNumber) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param listSaleOrderId
	 * @return
	 * @throws BusinessException
	 */
	List<PrintDeliveryGroupVO> getListPrintDeliveryGroupVO(List<Long> listSaleOrderId) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param listSaleOrderId
	 * @return
	 * @throws BusinessException
	 */
	List<PrintDeliveryCustomerGroupVO> getListPrintDeliveryCustomerGroupVO(List<Long> listSaleOrderId) throws BusinessException;

	ObjectVO<SaleOrder> getListSaleOrder(KPaging<SaleOrder> kPaging, String shopCode, Long customerId, Long staffId, Date fromDate, Date toDate, SaleOrderStatus approved, OrderType orderType, Integer state) throws BusinessException;

	/**
	 * tra ve string khong thoa dieu kien recieve < max trong promotion shop map
	 * 
	 * @param saleOrder
	 * @return
	 * @throws BusinessException
	 */
	String checkQuantityCheckAndMax(SaleOrder saleOrder) throws BusinessException;

	/**
	 * Gets the list sale order with no invoice ex1.
	 * 
	 * @author phut
	 * 
	 * @param shopId
	 *            the shop id
	 * @param saleStaffCode
	 *            the sale staff code
	 * @param deliveryStaffCode
	 *            the delivery staff code
	 * @param vat
	 *            the vat
	 * @param orderNumber
	 *            the order number
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param customerCode
	 *            the customer code
	 * @return the list sale order with no invoice ex1
	 * @throws BusinessException
	 *             the business exception
	 */
	List<SaleOrderVOEx> getListSaleOrderWithNoInvoiceEx(KPaging<SaleOrderVOEx> kPaging, Long shopId, String saleStaffCode, String deliveryStaffCode, String orderNumber, Date fromDate, Date toDate, String customerCode, Date lockDate,
			Integer isValueOrder, Integer numberValueOrder) throws BusinessException;

	Invoice getInvoiceByInvoiceNumber(String invoiceNumber, Long shopId) throws BusinessException;

	/**
	 * Check duplicate invoice number.
	 * 
	 * @author phut
	 * @param listSaleOrderId
	 *            the list sale order id
	 * @return the list
	 * @throws BusinessException
	 *             the business exception
	 * @author phut
	 * @since Jan 7, 2013
	 */
	List<SaleOrder> checkDuplicateInvoiceNumber(Long shopId, Map<Long, List<String>> listSaleOrderId) throws BusinessException;

	void checkDebitInSaleOrder(Long shopId, Long customerId, BigDecimal amount) throws BusinessException;

	List<SaleOrderVOEx> getListRptSaleOrderWithInvoice(List<Long> lstSaleOrderId) throws BusinessException;

	/**
	 * Gets the sale order lot by condition.
	 * 
	 * @author phut
	 * @param saleOrderId
	 *            the sale order id
	 * @param saleOrderDetailId
	 *            the sale order detail id
	 * @param lot
	 *            the lot
	 * @return the sale order lot by condition
	 * @throws BusinessException
	 *             the business exception
	 */
	Integer sumQuatitySaleOrderLotByCondition(Long saleOrderId, Long saleOrderDetailId, String lot) throws BusinessException;

	SaleOrderLot createSaleOrderLot(SaleOrderLot salesOrderLot) throws BusinessException;

	ObjectVO<CommercialSupportVO> getListCommercialSupport(KPaging<CommercialSupportVO> kPaging, Long shopId, Long customerId, Date orderDate, Long saleOrderId) throws BusinessException;

	ObjectVO<CommercialSupportVO> getCommercialSupport(KPaging<CommercialSupportVO> kPaging, Long shopId, Long customerId, Date orderDate, Long saleOrderId, String commercialSupportCode) throws BusinessException;

	List<Boolean> checkIfCommercialSupportExists(List<CommercialSupportCodeVO> lstCommercialSupportCodeVO, Date orderDate, Long saleOrderId) throws BusinessException;

	List<SaleOrderVOEx> getListSaleOrderVOEx(List<Long> lstInvoiceId) throws BusinessException;

	List<Invoice> getListInvoiceByCondition(KPaging<Invoice> kPaging, String orderNumber, String redInvoiceNumber, String staffCode, String deliveryCode, String shortCode, Date fromDate, Date toDate, Float vat, InvoiceCustPayment custPayment,
			Date lockDate, Long shopId, Integer isValueOrder, Boolean isVATGop, Integer numberValueOrder) throws BusinessException;

	ObjectVO<Invoice> getListInvoiceByConditionEx(KPaging<Invoice> kPaging, String orderNumber, String redInvoiceNumber, String staffCode, String deliveryCode, String shortCode, Date fromDate, Date toDate, Float vat, InvoiceCustPayment custPayment,
			Date lockDate, Long shopId, Integer isValueOrder) throws BusinessException;

	List<String> checkDuplicateInvoiceNumberEx1(Long shopId, List<String> lstInvoiceNumber) throws BusinessException;

	void updateInvoiceFromSaleOrder(Long shopId, List<Long> listInvoiceId, List<String> listInvoiceNumber, String username) throws BusinessException;

	ObjectVO<SaleOrder> getListSaleOrder(SaleOrderFilter filter, KPaging<SaleOrder> kPaging) throws BusinessException;

	Car getLastestCarFromListSaleOrderNumber(List<String> lstOrderNumber) throws BusinessException;

	List<RptExSaleOrder2VO> getListSaleOrderByDeliveryStaffGroupByProductCode(List<Long> listSaleOrderId, Long shopId) throws BusinessException;

	SaleOrder modifySaleOrder(SaleOrder salesOrder, List<SaleProductVO> lstSaleProductVO, List<SaleProductVO> lstPromoProductVO, Boolean isCheckQuantity, List<SaleOrderPromotionVO> lstOrderPromotion,
			List<SaleOrderPromotionDetailVO> lstOrderPromoDetail, Integer isSaveNotCheckDebit, List<KeyShopVO> lstKeyShop,LogInfoVO logInfoVO) throws BusinessException;

	ObjectVO<SaleOrder> getListSaleOrderEx(SoFilter filter) throws BusinessException;

	/**
	 * @author sangtn
	 * @since 10-06-2014
	 * @description clone from getListSaleOrderEx, change result from Entity to
	 *              VO
	 * @note for search sale order, sale order manager screen:
	 *       SearchSaleTransactionAction->searchOrder()
	 */
	ObjectVO<SaleOrderVOEx2> getListSaleOrderExEx(KPaging<SaleOrderVOEx2> kPaging, SoFilter filter) throws BusinessException;

	List<SaleOrder> getListSaleOrderConfirm(SoFilter filter) throws BusinessException;

	ObjectVO<SaleOrderVOEx2> getListSaleOrderVOConfirm(SoFilter filter) throws BusinessException;

	List<SaleOrderDetail> getListSaleOrderDetailConfirm(SoFilter filter) throws BusinessException;

	/**
	 * @author vuongmq
	 * @since 26 - August, 2014
	 * @return
	 * @description Láº¥y danh sĂ¡ch Ä‘Æ¡n hĂ ng Ä‘á»ƒ cáº­p nháº­t pháº£i thu vĂ  kho (máº·c Ä‘á»‹nh
	 *              láº¥y Ä‘Æ¡n hĂ ng IN)
	 */
	ObjectVO<SaleOrderStockVO> getListSaleOrderStock(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws BusinessException;

	/**
	 * @author trietptm
	 * @since Jul 24, 2015
	 * @return
	 * @description Lay ds don DC, DCT, DCG cho duyet dieu chinh kho
	 */
	ObjectVO<SaleOrderStockVO> getListSaleOrderStockUpdate(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws BusinessException;

	/**
	 * @author vuongmq
	 * @since 26 - August, 2014
	 * @return
	 * @description Láº¥y danh sĂ¡ch Ä‘Æ¡n hĂ ng Ä‘á»ƒ cáº­p nháº­t pháº£i thu vĂ  kho cĂ¡c Ä‘Æ¡n
	 *              hĂ ng van sale GO/DP/DCT/DGC/DC
	 */
	ObjectVO<SaleOrderStockVO> getListSaleOrderStockTrans(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws BusinessException;

	/**
	 * @author sangtn
	 * @since 10-06-2014
	 * @description clone from getListSaleOrderEx2, change result from Entity to
	 *              VO
	 * @note for search sale order, sale order manager screen:
	 *       SearchSaleTransactionAction->searchOrder()
	 */
	ObjectVO<SaleOrderVOEx2> getListSaleOrderEx2Ex(KPaging<SaleOrderVOEx2> kPaging, SoFilter filter) throws BusinessException;

	ObjectVO<SaleOrder> getListSaleOrderToReturn(SoFilter filter) throws BusinessException;

	Integer getOrderDebitMaxCustomer(SaleOrder saleOrder, Date lockDay) throws BusinessException;

	ObjectVO<SaleOrder> getListSaleOrderEx1(SoFilter filter) throws BusinessException;

	Boolean checkProductLotInSO(Long saleOrderId) throws BusinessException;
	
	Boolean checkOrderInDelay(Long staffId, int delayTime) throws BusinessException;

	List<SaleOrderLot> getListSaleOrderLotBySaleOrderId(Long saleOrderId) throws BusinessException;

	/**
	 * In phieu giao hang gop theo DH
	 * 
	 * @author thongnm
	 */
	List<RptPGHVO> getPGHGroupByDH(List<Long> listSaleOrderId, String strListShopId) throws BusinessException;

	/**
	 * In phieu giao hang gop theo KH
	 * 
	 * @author thongnm
	 */
	List<RptPGHVO> getPGHGroupByKH(List<Long> listSaleOrderId, String strListShopId) throws BusinessException;

	/**
	 * In phieu giao hang gop theo KH
	 * 
	 * @author thongnm
	 */
	List<RptPGHVO> getPGHGroupByNVGH(List<Long> listSaleOrderId, String strListShopId) throws BusinessException;

	/**
	 * lay bao cao doanh thu ban hang theo san pham
	 * 
	 * @author cangnd
	 */
	public List<RptBHTSP_DSNhanVienBH> getBHTSP_DSNhanVienVO(String shopCode, String dt_tu_ngay, String dt_den_ngay, String nhan_vien_ban_hang);

	SaleOrder rollbackSaleOrder(long saleOrderId, String createUser,LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Ä�áº¿m sá»‘ lÆ°á»£ng hĂ³a Ä‘Æ¡n chÆ°a Ä‘Æ°á»£c duyá»‡t trong ngĂ y cá»§a cá»­a hĂ ng
	 * 
	 * @author tungtt21
	 * @param shopId
	 * @param now
	 * @return
	 * @throws BusinessException
	 */
	int countSaleOrderNotApprovedOfShopInDay(Long shopId, Date lockDate) throws BusinessException;

	/**
	 * Lay danh sach don hang theo danh sach id
	 * 
	 * @author tungtt21
	 * @param listSaleOrderId
	 * @return
	 * @throws BusinessException
	 */
	List<SaleOrder> getListSaleOrderById(List<Long> listSaleOrderId) throws BusinessException;
	List<SaleOrder> getListSaleOrderById2(List<Long> listSaleOrderId) throws BusinessException;

	/**
	 * @author tungtt21
	 * @param listSaleOrderId
	 * @param orderType
	 * @return
	 * @throws BusinessException
	 */
	List<SaleOrder> getListSaleOrderById(List<Long> listSaleOrderId, Integer orderType) throws BusinessException;

	/**
	 * @author tungtt21
	 * @param orderNumber
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	SaleOrder getSaleOrderByOrderNumberAndShopId(String orderNumber, Long shopId) throws BusinessException;

	/**
	 * @author vuongmq
	 * @since Oct 15, 2014 su dung bĂªn
	 *        AssignTransferStaffAction.importExcelFile()
	 * @description Lay them dieu kien approved = 0 vĂ  approved_step =1
	 */
	SaleOrder getSaleOrderByOrderNumberAndShopIdApproved(String orderNumber, Long shopId) throws BusinessException;
	
	SaleOrder getSaleOrderByOrderNumberAndListShop(String orderNumber, String lstShopId) throws BusinessException;

	List<Invoice> getListInvoiceByLstInvoiceId(List<Long> lstInvoiceId) throws BusinessException;

	void updateListInvoice(List<Invoice> lstInvoice) throws BusinessException;

	/**
	 * @author nhanlt6
	 * @param saleOrderId
	 * @param listCustomerId
	 * @return
	 * @throws BusinessException
	 */
	List<SaleOrder> copySaleOrderForListCustomer(SaleOrder so, List<Long> lstCustomerId,LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Tu dong can tro cong no cua don tra hang
	 * 
	 * @author tungtt21
	 */
	void updateReturnOrderAutoDebit(long saleOrderId, String staffCode) throws BusinessException;

	/**
	 * Lay danh sach ma san phan ban trong don hang ma co gia khac voi gia hien
	 * tai (list null don ban dung)
	 * 
	 * @author tungtt21
	 * @param saleOrderId
	 * @return
	 * @throws BusinessException
	 */
	List<String> getListProductHasPriceError(SaleOrder so) throws BusinessException;

	/**
	 * @author vuonghn
	 */
	ObjectVO<OrderProductVO> getListOrderProductVansale(KPaging<OrderProductVO> kPaging, Long shopId, Long staffId, String pdCode, String pdName, Date lockDay) throws BusinessException;

	ObjectVO<OrderProductVO> getListOrderProductVOWithZ(KPaging<OrderProductVO> kPaging, Long shopId, Long staffId, Long custId, String pdCode, String pdName, String ppCode, Date lockDay) throws BusinessException;

	/**
	 * Lay danh sach cac hoa don do cua 1 don hang dua vao id
	 * 
	 * @author tungtt21
	 * @param saleOrderId
	 * @return
	 * @throws BusinessException
	 */
	List<Invoice> getListInvoiceBySaleOrderId(Long saleOrderId) throws BusinessException;
	List<Invoice> getListInvoiceBySaleOrderId2(long saleOrderId, int invoiceType) throws BusinessException;

	/**
	 * Lay phieu xuat kho kiem van chuyen noi bo tu don hang
	 * 
	 * @author lacnv1
	 * @since Jun 20, 2014
	 */
	List<RptPXKKVCNB_DataConvert> getPhieuXKVCNB(List<Long> lstsoId) throws BusinessException;

	/**
	 * Lay danh sach don hang cho don VAT gop
	 * 
	 * @author phuongvm
	 * @since 21/08/2014
	 * @param kPaging
	 * @param shopId
	 * @param saleStaffCode
	 * @param deliveryStaffCode
	 * @param orderNumber
	 * @param fromDate
	 * @param toDate
	 * @param customerCode
	 * @param lockDate
	 */
	List<SaleOrder> getListSaleOrderWithNoInvoiceGOP(KPaging<SaleOrderVOEx> kPaging, Long shopId, String saleStaffCode, String deliveryStaffCode, String orderNumber, Date fromDate, Date toDate, String customerCode, Date lockDate, Integer isValueOrder,Integer numberValueOrder)
			throws BusinessException;

	/**
	 * lay danh sach kho theo SP
	 * 
	 * @author nhanlt6
	 * @param paging
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<OrderProductVO> getListOrderProductWarehouseVO(KPaging<OrderProductVO> paging, Long productId, Long shopId) throws BusinessException;

	/**
	 * lay danh sach Don hang da bi sua so luong
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 * @throws BusinessException
	 */
	List<OrderProductVO> getListProductChangeQuantityWarning(SoFilter filter) throws BusinessException;

	/**
	 * lay danh sach Don hang da bi sua so suat
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 * @throws BusinessException
	 */
	List<OrderProductVO> getListProductChangeRationWarning(SoFilter filter) throws BusinessException;
	
	/**
	 * Lay danh sach SP vuot ton kho
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListProductQuantityWarning(SoFilter filter) throws BusinessException;

	/**
	 * Lay danh sach SP khac gia
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListProductPriceWarning(SoFilter filter) throws BusinessException;

	/**
	 * Lay danh sach SP co CTKM het han
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListProductPromotionWarning(SoFilter filter) throws BusinessException;
	
	/**
	 * Goi function kiem tra mat chi tiet
	 * 
	 * @author tungmt
	 * @since 26/01/2015
	 */
	String getListOrderNumberLostDetail(SoFilter filter) throws BusinessException;
	
	/**
	 * Lay danh sach SP co CTKM het han
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListWarningPromotionOfOrder(SoFilter filter) throws BusinessException;

	/**
	 * Lay danh sach don hang khac amount(rot chi tiet)
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListProductAmountWarning(SoFilter filter) throws BusinessException;

	/**
	 * Lay danh sach chi tiet don hang Confirm
	 * 
	 * @author tungmt
	 * @since 25/08/2014
	 */
	List<SaleOrderDetailVOEx> getListSaleOrderDetailForConfirm(SoFilter filter) throws BusinessException;

	/**
	 * tao 1 don gop va tra ve danh sach don tach theo vat
	 * 
	 * @author phuongvm
	 * @param donChinh
	 * @param lstDonGop
	 * @param lstSO
	 * @return
	 * @throws BusinessException
	 */
	List<SaleOrderVOEx> taoDonGopVaTraVeDanhSachDonTach(SaleOrder donChinh, List<SaleOrderDetail> lstDonGop, List<SaleOrder> lstSO) throws BusinessException;

	/**
	 * Lay chi tiet don hang
	 * 
	 * @since Aug 25, 2014
	 */
	List<SaleOrderDetailVOEx> getListSaleOrderDetailByOrderId(long saleOrderId, Integer isFreeItem, ProgramType programType, Boolean isAutoPromotion, Boolean isGetCTTL) throws BusinessException;

	/**
	 * cap nhat pocustomer.approve
	 * 
	 * @since Aug 25, 2014
	 */
	void updatePOCustomer(SoFilter filter) throws BusinessException;

	/**
	 * Tu choi don hang
	 * 
	 * @author tungmt
	 * @param lstSalesOrder
	 * @throws BusinessException
	 */
	void denyListSaleOrder(List<SaleOrder> lstSalesOrder, SoFilter filter, LogInfoVO loginfo) throws BusinessException;

	/**
	 * Huy don hang
	 * 
	 * @author tungmt
	 * @param lstSalesOrder
	 * @throws BusinessException
	 */
	void cancelListSaleOrder(List<SaleOrder> lstSalesOrder, SoFilter filter, LogInfoVO loginfo) throws BusinessException;

	/**
	 * Chap nhan don hang
	 * 
	 * @author tungmt
	 * @param lstSalesOrder
	 * @throws BusinessException
	 */
	List<SearchSaleTransactionErrorVO> confirmListSaleOrder(List<SaleOrder> lstSalesOrder, String description, LogInfoVO loginfo) throws BusinessException;

	/**
	 * Chap nhan thanh toĂ¡n
	 * 
	 * @author tungmt
	 * @param lstSalesOrder
	 * @throws BusinessException
	 */
	void confirmPayListSaleOrder(SoFilter filter, String description, LogInfoVO loginfo) throws BusinessException;

	/**
	 * luu don VAT gop
	 * 
	 * @author phuongvm
	 * @param shopId
	 * @param username
	 * @param listSaleOrder
	 * @param listCustPayment
	 * @param donChinh
	 * @param lstSO
	 * @return
	 * @throws BusinessException
	 */
	void createInvoiceFromSaleOrderEx(Long shopId, String username, Map<Long, List<String>> listSaleOrder, Map<Long, List<InvoiceCustPayment>> listCustPayment, SaleOrder donChinh, List<SaleOrder> lstSO, List<SaleOrderDetail> lstDetailGop)
			throws BusinessException;

	/**
	 * @author vuongmq
	 * @param saleOrderId
	 * @return
	 * @throws BusinessException
	 * @description laydon hang san pham so luong de tru ton kho
	 */
	List<SaleOrderStockDetailVO> getListSaleOrderStockDetail(long saleOrderId, String orderType) throws BusinessException;
	
	/**
	 * @author trietptm
	 * @param saleOrderId
	 * @return
	 * @throws BusinessException
	 * @description laydon hang san pham so luong de tru ton kho cho Duyet dieu chinh kho
	 */
	List<SaleOrderStockDetailVO> getListSaleOrderStockDetailUpdate(long saleOrderId, String orderType) throws BusinessException;

	/**
	 * update don hang, duyet dieu chinh kho
	 * @author vuongmq
	 * @param listDetail
	 * @param orderVO
	 * @param userName
	 * @param logInfoVO
	 * @return bolean
	 * @throws BusinessException
	 * @since 04/09/2015
	 */
	boolean updateSaleOrderStock(List<SaleOrderStockDetailVO> listDetail, SaleOrderStockVOTemp orderVO, String userName,LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Lay ds don hang pay
	 * 
	 * @author tungmt
	 * @since 03/09
	 */
	ObjectVO<SaleOrderVO> getListSaleOrderForPay(SoFilter filter) throws BusinessException;

	void cancelPay(SoFilter filter) throws BusinessException;

	/**
	 * Lay ds don hang in hoa don
	 * 
	 * @author tungmt
	 * @since 08/09
	 */
	ObjectVO<SaleOrder> getListSaleOrderForPrint(SoFilter filter) throws BusinessException;

	/**
	 * Lay danh sach don hang dieu chinh tang, giam
	 * 
	 * @author hunglm16
	 * @since September 9,2014
	 * */
	ObjectVO<SaleOrderVO> getListSaleOrderAIADVOByFilter(SaleOrderFilter<SaleOrderVO> filter) throws BusinessException;

	ObjectVO<PrintOrderVO> getListPrintOrderVO(PrintOrderFilter filter) throws BusinessException;

	ObjectVO<SaleOrderDetailVOEx> getListSaleOrderDetailVOByFilter(SaleOrderDetailFilter<SaleOrderDetailVOEx> filter) throws BusinessException;

	/**
	 * Duyet don hang
	 * @author trietptm
	 * @param param
	 * @return map loi
	 * @throws BusinessException
	 */
	Map<String, List<String>> updateSaleOrder4Print(ConfirmFilter param) throws BusinessException;

	/**
	 * Xac nhan don GO & DP
	 * @author trietptm
	 * @param param
	 * @throws BusinessException
	 * @since Oct 22, 2015
	 */
	void updateGODPOrder4Print(ConfirmFilter param) throws BusinessException;

	List<SaleOrderPromotionVO> getListSOPromotionBySOId(Long saleOrderId) throws BusinessException;

	List<PromotionProgram> checkListPromotionReceived(List<SaleOrderPromotionVO> listReceived, Long shopId, Long staffId, Long customerId) throws BusinessException;
	
	/**
	 * Kiem tra max so tien KM
	 * @author trietptm 
	 * @param saleOrderFilter
	 * @return chuoi promotionCode cua cac CTKM vuot muc
	 * @throws BusinessException
	 * @since Sep 09, 2015
	 */
	String checkDiscountAmountPromotionReceived(@SuppressWarnings("rawtypes") SaleOrderFilter saleOrderFilter) throws BusinessException;
	
	/**
	 * Kiem tra max so luong KM
	 * @author trietptm
	 * @param saleOrderFilter
	 * @return chuoi promotionCode cua cac CTKM vuot muc
	 * @throws BusinessException
	 * @since Sep 09, 2015
	 */
	String checkNumPromotionReceived(@SuppressWarnings("rawtypes") SaleOrderFilter saleOrderFilter) throws BusinessException;

	SaleOrderVO getSaleOrderVOSingle(SaleOrderFilter<SaleOrderVO> filter) throws BusinessException;

	void deleteSaleOrderAIAD(SaleOrder salesOrder) throws BusinessException;

	Long createSaleOrderAIAD(SaleOrder saleOrder, String arrStrInsertData) throws BusinessException, DataFormatException;

	ObjectVO<SaleOrder> getListSaleOrderExByFilter(SaleOrderFilter<SaleOrder> filter, String orderBy) throws BusinessException;

	ObjectVO<SaleOrderDetail> getListSaleOrderDetailByListSaleOrderId(SaleOrderDetailFilter<SaleOrderDetail> filter) throws BusinessException;

	void updateSaleOrderAIAD(SaleOrder saleOrder, String arrStrInsertData, List<Long> arrStrDeleteData, String arrStrUpdateData) throws BusinessException;

	void deleteSaleOrderAIADDetail(SaleOrder salesOrder) throws BusinessException;

	/**
	 * Huy don hang tren web
	 * 
	 * @author lacnv1
	 * @since Sep 29, 2014
	 */
	void cancelWebOrder(long orderId, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Lay ds don hang tra cua don hang
	 * 
	 * @author lacnv1
	 * @since Sep 30, 2014
	 * 
	 * @param soId
	 *            : id don hang goc
	 */
	List<SaleOrder> getListReturnOrderByOrderId(long soId, SaleOrderStatus approved, SaleOrderStep approvedStep) throws BusinessException;

	/**
	 * Huy don tra hang
	 * 
	 * @author lacnv1
	 * @since Sep 30, 2014
	 */
	void cancelReturnOrder(long orderId, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Get list sale order promo detail theo sale order va type
	 * 
	 * @author nhanlt6
	 * @since Oct 22, 2014
	 */
	List<SaleOrderDetailVOEx> getListSaleOrderPromoDetailForOrder(Long orderId, Integer promotionType) throws BusinessException;

	/**
	 * cancel order in print order step
	 * 
	 * @author tuannd20
	 * @param shopId
	 *            current shop id
	 * @param lstOrderNumber
	 *            list order's number to cancel
	 * @param logInfo
	 * @return error order
	 * @throws BusinessException
	 * @date 03/11/2014
	 */
	Map<String, List<String>> cancelOrdersInPrintOrderStep(Long shopId, List<String> lstOrderNumber, LogInfoVO logInfo) throws BusinessException;

	SaleOrder getSaleOrderByOrderNumber(String orderNumber, Long shopId) throws BusinessException;
	
	/**
	 * Search list sale order
	 * @author nhanlt6
	 * @param SoFilter
	 * @throws BusinessException
	 * @date 18/11/2014
	 */
	ObjectVO<SaleOrderVOEx2> searchSaleOrder(SoFilter filter) throws BusinessException;
	
	/**
	 * Lay danh sach CTHTTM co trong don hang
	 * 
	 * @author lacnv1
	 * @since Dec 10, 2014
	 */
	ObjectVO<CommercialSupportVO> getListCommercialSupportOfSaleOrder(long saleOrderId, KPaging<CommercialSupportVO> paging) throws BusinessException;
	
	/**
	 * Kiem tra xem co thieu hang cua khuyen mai tich luy hay khong
	 * Tra ve: true neu thieu hang o khuyen mai tich luy, nguoc lai: false
	 * 
	 * @author lacnv1
	 * @since Jan 07, 2015
	 */
	List<StockTotalVO> getListWaringAccumulativeProductQuantity(long shopId, long saleOrderId) throws BusinessException;
	
	boolean checkPromotionOpenOfSaleOrder(long saleOrderId, long customerId, Date lockDate, List<Long> lstPassOrderId) throws BusinessException;
	
	boolean checkAccumulativePromotionOfSaleOrder(long saleOrderId, Date orderDate, List<Long> lstPassOrderId) throws BusinessException;
	
	/**
	 * Chan nhan don hang tich luy trong truong hop thieu kho cho sp tich luy
	 * 
	 * @author lacnv1
	 * @since Jan 08, 2015
	 */
	List<SearchSaleTransactionErrorVO> confirmAccumulativeSaleOrder(List<SaleOrder> lstSalesOrder, String description, LogInfoVO loginfo) throws BusinessException;
	
	/**
	 * Kiem tra co thoa man dieu kien tich luy, mo moi khi duyet don hay khong
	 * 
	 * @author lacnv1
	 * @since Jan 08, 2015
	 */
	SearchSaleTransactionErrorVO prepareAprroveSaleOrder(SaleOrder so, List<Long> lstPassOrderId) throws BusinessException;
	
	/**
	 * Lay danh sach sale_order_promo_detail cua don hang
	 * 
	 * @author lacnv1
	 * @since Jan 20, 2015
	 */
	List<SaleOrderPromoDetail> getListSaleOrderPromoDetailOfOrder(long saleOrderId, String programCode) throws BusinessException;
	
	/**
	 * Lay danh sach don CO ma don SO cua no chua qua buoc xac nhan IN
	 * 
	 * @author lacnv1
	 * @since Jan 21, 2015
	 * 
	 * @param filter.lstSaleOrderId: danh sach id cua don hang can kiem tra
	 */
	List<OrderProductVO> getCONotHaveApprovedSO(SoFilter filter) throws BusinessException;

	ObjectVO<SaleOrderVOEx2> searchStockTrans(SoFilter filter) throws BusinessException;
	
	/**
	 * Lay nhung don hang co CTKM khong du so suat
	 * 
	 * @author lacnv1
	 * @date Feb, 09 2015
	 */
	List<OrderProductVO> getOrderNotEnoughPortion(SoFilter filter) throws BusinessException;
	
	/**
	 * Luu gia DH khi thay doi tim kiem
	 * 
	 * @author hoanv25
	 * @date 14/2/2015
	 */
	void updateShopParamByInvoice(ShopParam numberValueOrderParam)throws BusinessException;
	
	/**
	 *Tao moi gia DH khi tim kiem lan dau tien
	 * 
	 * @author hoanv25
	 * @date 14/2/2015
	 */
	void createShopParamByInvoice(ShopParam addNumberValueOrderParam)throws BusinessException;
	
	/**
	 * Huy don DC, DCG, DCT
	 * 
	 * @author lacnv1
	 * @since Mar 11, 2015
	 */
	List<SearchSaleTransactionErrorVO> cancelStockTrans(List<Long> lstId, long shopId, Date lockDate, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * Huy don DC, DCG, DCT cho Duyet dieu chinh kho
	 * 
	 * @author lacnv1
	 * @since Mar 11, 2015
	 */
	List<SearchSaleTransactionErrorVO> cancelStockTransUpdate(List<Long> lstId, Date lockDate, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * Lay thong tin don hang va dua vao file xml
	 * 
	 * @author lacnv1
	 * @since Mar 14, 2015
	 */
	List<OrderXmlObject> getSaleOrderXmlObject(SoFilter filter) throws BusinessException;
	
	/**
	 * Lay danh sach chi tiet don hang cho man hinh tra hang
	 * 
	 * @author lacnv1
	 * @since Mar 26, 2015
	 */
	List<SaleOrderLotVOEx> getListOrderDetailVOByOrderId(long orderId) throws BusinessException;
	
	/**
	 * Lay chi tiet khuyen mai tien, phan tram cua khuyen mai don hang, khuyen mai tich luy
	 * 
	 * @author lacnv1
	 * @since Mar 30, 2015
	 */
	List<SaleOrderLotVOEx> getListOrderPromoDetailByOrderId(long orderId) throws BusinessException;
	
	/**
	 * Lay so luot in trong ngay cua npp
	 * 
	 * @param shopId - id npp
	 * @param pDate - ngay can lay so luot in
	 * 
	 * @author lacnv1
	 * @since Apr 21, 2015
	 */
	int getMaxPrintBatch(long shopId, Date pDate) throws BusinessException;

	/**
	 * update don hang, duyet dieu chinh kho
	 * 
	 * @author trietptm
	 * @since Jul 24, 2015
	 */
	//boolean updateSaleOrderStockUpdate(List<SaleOrderStockDetailVO> listDetail, SaleOrderStockVOTemp orderVO, String userName, LogInfoVO logInfoVO)	throws BusinessException;

	/**
	 * @author trietptm
	 * @param listSaleOrderId
	 * @param productCode
	 * @return lay chuoi cac programCode cho tung san pham
	 * @throws BusinessException
	 * @since Aug 05, 2015
	 */
	List<ProgramCodeVO> getListProgramCodeVO(SaleOrderFilter<SaleOrder> filter) throws BusinessException;

	/**
	 * check truoc khi tra thuong keyShop va check ton kho
	 * @author trietptm
	 * @param lstSaleOrder
	 * @param lockDay
	 * @return Map error
	 * @throws BusinessException
	 * @since Aug 13, 2015
	 */
	public Map<String, Map<String, String>> prepareUpdateSaleOrder4Print(List<SaleOrder> lstSaleOrder, Date lockDay) throws BusinessException;

	/**
	 * @author tungmt
	 * @version 1.0
	 * @param filter
	 * @return Danh sach keyshop tra thuong vuot muc cho phep
	 * @exception BusinessException
	 * @see
	 * @since 17/8/2015
	 * @serial
	 */
	List<OrderProductVO> getListKeyShopWarning(SoFilter filter) throws BusinessException;
	/**
	 * Lst product stock total.
	 *
	 * @param lstShopId the lst shop id
	 * @param lstProductId the lst product id
	 * @return the list
	 * @throws BusinessException the business exception
	 */
	List<ProductStockTotal> lstProductStockTotal(SaleOrder tempOrder, StockTrans stockTrans, Integer type) throws BusinessException;
	
	/**
	 * Lst product sale plan.
	 *
	 * @param lstShopId the lst shop id
	 * @param lstProductId the lst product id
	 * @return the list
	 * @throws BusinessException the business exception
	 */
	List<ProductStockTotal> lstProductSalePlan(SaleOrder tempOrder, StockTrans stockTrans, Integer type) throws BusinessException;
	
	/**
	 * kiem tra dang thuc hien kiem kho
	 * @author trietptm
	 * @param saleOrderId
	 * @return
	 * @throws BusinessException
	 * @since Aug 26, 2015
	 */
	boolean checkStockCounting(long saleOrderId) throws BusinessException;

	/**
	 * Check kiem ke kho hien tai cua don hang dang thuc hien(0): thi khong cho duyet don hang 
	 * @author vuongmq
	 * @param filter
	 * @return boolean
	 * @throws BusinessException
	 * @since 28/09/2015
	 */
	boolean checkStockTransCycleCount(StockStransFilter filter) throws BusinessException;

	/**
	 * Duyet & cap nhat kho don GO & DP 
	 * @author trietptm
	 * @param param
	 * @return map loi
	 * @throws BusinessException
	 * @since Oct 22, 2015
	 */
	Map<String, List<String>> updateGODPOrderAndUpdateStock4Print(ConfirmFilter param) throws BusinessException;

	/**
	 * kiem tra dang thuc hien kiem kho cho don DP GO
	 * @author trietptm
	 * @param filter
	 * @return boolean
	 * @throws BusinessException
	 * @since Dec 2, 2015
	*/
	boolean checkStockCountingForStockTrans(StockStransFilter filter) throws BusinessException;

	/**
	 * check toan ven don hang
	 * @author trietptm
	 * @param filter
	 * @return chuoi sale_order_id bi loi 
	 * @throws BusinessException
	 * @since Dec 8, 2015
	*/
	String checkOrderIntegrity(SoFilter filter) throws BusinessException;
	
	void insertNewTable(List<SaleOrder> lstOrder, int action, LogInfoVO loginfo, Integer multipli) throws BusinessException;
	
	List<SaleOrder> getListCustomerNotApproved(Long customerID) throws BusinessException;
	
	List<OrderProductVO> getListSaleOrderID(List<Long> listSaleOrderID) throws BusinessException;
}

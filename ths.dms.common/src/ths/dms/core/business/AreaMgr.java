package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.Area;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AreaType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.AreaVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.dms.core.exceptions.BusinessException;

public interface AreaMgr {

	Area getAreaByCode(String code) throws BusinessException;

	void deleteArea(Area area, LogInfoVO logInfo) throws BusinessException;

	void updateArea(Area area, LogInfoVO logInfo) throws BusinessException;

	Area createArea(Area area, LogInfoVO logInfo) throws BusinessException;

	Boolean isUsingByOthers(long areaId) throws BusinessException;

	List<Area> getListSubArea(Long parentId) throws BusinessException;

	Area getAreaById(Long id) throws BusinessException;

	Boolean checkIfAreaHasAnyChildStopped(Long areaId) throws BusinessException;

	ObjectVO<Area> getListArea(KPaging<Area> kPaging, String areaCode,
			String areaName, Long parentId, ActiveType status,
			String provinceCode, String provinceName, String districtCode,
			String districtName, String precinctCode, String precinctName,
			AreaType type, Boolean isGetOneChildLevel,
			String sort, String order) throws BusinessException;

	TreeVO<Area> getAreaTreeVO(Area area, Long exceptionalArea)
			throws BusinessException;

	Boolean checkIfAreaHasAllChildStopped(Long areaId) throws BusinessException;

	TreeVO<Area> getAreaTreeVOEx(Area area, Long exceptionalArea,
			ActiveType status) throws BusinessException;

	/**
	 * @author hungnm
	 * @param AreaId
	 * @return
	 * @throws BusinessException
	 */
	TreeVO<Area> getAncestor(Long AreaId)
			throws BusinessException;

	List<Area> getListSubAreaEx(Long parentId, ActiveType activeType,
			Long exceptionalAreaId) throws BusinessException;

	Area getArea(String provinceCode, String districtCode, String precinctCode,
			AreaType type, ActiveType status) throws BusinessException;

	Area getAreaEqual(String areaCode, String areaName, Long parentId,
			ActiveType status, String provinceCode, String provinceName,
			String districtCode, String districtName, String precinctCode,
			String precinctName, AreaType type, Boolean isGetOneChildLevel)
			throws BusinessException;

	void updateListArea(List<Area> lstArea, LogInfoVO logInfo)
			throws BusinessException;

	TreeVO<Area> getAreaTreeVOEx1(Area area, Long exceptionalArea,
			ActiveType status) throws BusinessException;

	List<Area> getListSubArea(Long parentId, ActiveType activeType,
			Boolean isOrderByCode) throws BusinessException;
	
	List<Area> getListAreaFull() throws BusinessException;
	
	Area getAreaByPrecinct(String code) throws BusinessException;
	
	/**
	 * Lay danh sach dia ban theo loai
	 * 
	 * @author lacnv1
	 * @since Apr 21, 2014
	 */
	List<AreaVO> getListAreaByType(Long parentId, Integer type, Integer status) throws BusinessException;
	
	/**
	 * lay thong tin dia ban voi thong tin cac dia ban cha
	 * @author tuannd20
	 * @param status Trang thai dia ban can lay
	 * @return Danh sach dia ban & thong tin dia ban cha
	 * @throws BusinessException
	 * @since 28/03/2015
	 */
	List<AreaVO> retrieveAreaWithParentAreaInfo(ActiveType status) throws BusinessException;
}

/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.AsoPlan;
import ths.dms.core.entities.vo.AsoPlanVO;
import ths.dms.core.entities.vo.DynamicBigVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.filter.CommonFilter;

/**
 * AsoPlanMgr phan bo Aso
 * @author vuongmq
 * @since 19/10/2015 
 */
public interface AsoPlanMgr {

	/**
	 * @author vuongmq
	 * @param kPaging
	 * @param filter
	 * @return List<AsoPlan>
	 * @throws BusinessException
	 * @since 19/10/2015
	 */
	List<AsoPlan> getListAsoByFilter(CommonFilter filter) throws BusinessException;

	/**
	 * @author vuongmq
	 * @param filter
	 * @return AsoPlanVO
	 * @throws BusinessException
	 * @since 20/10/2015
	 */
	AsoPlanVO getAsoPlanVOByFilter(CommonFilter filter) throws BusinessException;

	/**
	 * @author vuongmq
	 * @param filter
	 * @return List<AsoPlanVO>
	 * @throws BusinessException
	 * @since 20/10/2015
	 */
	List<AsoPlanVO> getListAsoPlanVOByFilter(CommonFilter filter) throws BusinessException;
	
	/**
	 * @author vuongmq
	 * @param filter
	 * @return List<RoutingVO>
	 * @throws BusinessException
	 * @since 20/10/2015
	 */
	List<RoutingVO> getRoutingVOByFilter(CommonFilter filter) throws BusinessException;

	/**
	 * @author vuongmq
	 * @param asoPlanVO
	 * @param lstAsoPlanObject
	 * @param log
	 * @throws BusinessException
	 * @since 21/10/2015
	 */
	void createOrUpdateListAso(AsoPlanVO asoPlanVO, List<DynamicBigVO> lstAsoPlanObject, LogInfoVO log) throws BusinessException;
	
	/**
	 * @author vuongmq
	 * @param asoPlan
	 * @param lstAsoPlan
	 * @throws BusinessException
	 * @since 23/10/2015
	 */
	void createOrUpdateImportAso(AsoPlan asoPlan, List<AsoPlan> lstAsoPlan) throws BusinessException;

	/**
	 * @author vuongmq
	 * @param asoPlanVO
	 * @param logInfoVO
	 * @throws BusinessException
	 * @since 23/10/2015
	 */
	void resetAso(AsoPlanVO asoPlanVO, LogInfoVO logInfoVO) throws BusinessException;

}

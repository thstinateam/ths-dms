package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.ProductAttributeDetail;
import ths.dms.core.entities.ProductAttributeEnum;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.filter.ProductAttributeFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProductAttributeEnumVO;
import ths.dms.core.entities.vo.ProductAttributeVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Quan ly Thuoc tinh san pham
 * @author tientv11
 * @since 16/01/2015
 *
 */
public interface ProductAttributeMgr {

	/**
	 * Danh sach theo dieu kien tim kiem
	 * @author tientv11
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<ProductAttributeVO> getListProductAttributeVOFilter(ProductAttributeFilter filter)
		throws BusinessException;
	
	
	
	/**
	 * Danh sach theo dieu kien tim kiem
	 * @author tientv11
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<ProductAttributeEnumVO> getListProductAttributeEnumVOFilter(ProductAttributeFilter filter)
		throws BusinessException;


	/****
	    * save or update product attribute enum
	    * @params attirbute dynamic vo
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void saveOrUpdateProductAttribute(AttributeDynamicVO vo, LogInfoVO logInfo)
			throws BusinessException;


	/****
	    * save or update product attribute enum
	    * @params  attirbute enum vo
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void saveOrUpdateProductAttributeEnum(AttributeEnumVO vo, LogInfoVO logInfo)
			throws BusinessException;

	/****
	    * get product attribute enum by id
	    * @params id
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	ProductAttributeEnum getProductAttributeEnumById(long id)
			throws BusinessException;


	/****
	    * update product attribute detail
	    * @params product attirbute
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void updateProductAttributeEnum(ProductAttributeEnum attribute,
			LogInfoVO logInfo) throws BusinessException;


	/****
	    * get list product attribute by enum id
	    * @params id
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	List<ProductAttributeDetail> getListProductAttributeByEnumId(long id)
			throws BusinessException;


	/****
	    * update product attribute detail
	    * @params product attirbute
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void updateProductAttributeDetail(ProductAttributeDetail attribute,
			LogInfoVO logInfo) throws BusinessException;
}

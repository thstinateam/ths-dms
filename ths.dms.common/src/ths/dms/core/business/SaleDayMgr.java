package ths.dms.core.business;

import java.util.Date;

import ths.dms.core.entities.SaleDays;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

public interface SaleDayMgr {

	SaleDays createSaleDay(SaleDays saleDay, LogInfoVO logInfo) throws BusinessException;

	void updateSaleDay(SaleDays saleDay, LogInfoVO logInfo) throws BusinessException;

	void deleteSaleDay(SaleDays saleDay, LogInfoVO logInfo) throws BusinessException;

	ObjectVO<SaleDays> getListSaleDay(KPaging<SaleDays> kPaging, Integer fromYear,
			Integer toYear, ActiveType status) throws BusinessException;

	Boolean isUsingByOthers(long saleDayId) throws BusinessException;

	SaleDays getSaleDayById(Long id) throws BusinessException;

	/**
	 * lay so ngay lam viec trong nam cua VNM
	 * @author hieunq1
	 * @param year
	 * @return
	 * @throws BusinessException
	 */
	SaleDays getSaleDayByYear(Integer year) throws BusinessException;

	/**
	 * get so ngay lam viec tu ngay den ngay
	 * @author hieunq1
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	Integer getNumberWorkingDayByFromDateAndToDate(Date fromDate, Date toDate)
			throws BusinessException;

	Integer getNumOfHoliday(Date fromDate, Date toDate)
			throws BusinessException;

	Integer getSaleDayByDate(Date date) throws BusinessException;

	Integer getNumberWorkingDayByFromDateAndToDateEx(Date fromDate, Date toDate)
			throws BusinessException;

}

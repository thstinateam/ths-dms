package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.core.entities.vo.rpt.RptCustomerPayReceived1VO;
import ths.core.entities.vo.rpt.RptLimitDebitParentVO;
import ths.core.entities.vo.rpt.RptPayReceivedVO;
import ths.dms.core.exceptions.BusinessException;

public interface RptPayDebitMgr {

	/**
	 * 3.1.6.4	Phiếu thu
	 * @author hieunq1
	 * @param shopId
	 * @param custId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	RptPayReceivedVO getRptPayReceivedVO(Long shopId, String shortCode,
			Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * 3.1.6.1 bao cao cong no qua han
	 * @author thanhnguyen
	 * @param shopId
	 * @param date
	 * @param customerCode
	 * @return
	 * @throws BusinessException
	 */
	List<RptLimitDebitParentVO> getListLimitDebit(Long shopId, Date date,
			String customerCode) throws BusinessException;

	/**
	 * @author hungnm
	 * @param shopId
	 * @param fromPayDate
	 * @param toPayDate
	 * @param customerId
	 * @return
	 * @throws BusinessException
	 */
	List<RptCustomerPayReceived1VO> getListRptCustomerPayReceived1VO(
			Long shopId, Date fromPayDate, Date toPayDate, Long customerId)
			throws BusinessException;

}

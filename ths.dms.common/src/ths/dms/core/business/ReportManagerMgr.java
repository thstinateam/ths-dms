package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.Report;
import ths.dms.core.entities.ReportShopMap;
import ths.dms.core.entities.ReportUrl;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.ReportFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

public interface ReportManagerMgr {
	
	Report createReport(Report report) throws BusinessException;
	Report getReportById(Long id) throws BusinessException;
	void updateReport(Report report) throws BusinessException;
	void deleteReport(Report report) throws BusinessException;
	Report getReportByCode(String code) throws BusinessException;
	
	ReportUrl createReportUrl(ReportUrl reportUrl) throws BusinessException;
	ReportUrl getReportUrlById(Long id) throws BusinessException;
	void updateReportUrl(ReportUrl reportUrl) throws BusinessException;
	void deleteReportUrl(ReportUrl reportUrl) throws BusinessException;
	List<ReportUrl> getListReportUrlByReportId(Long reportId) throws BusinessException;
	List<ReportUrl> getListReportUrlByNPP(Long reportId) throws BusinessException;
	
	ReportShopMap createReportShopMap(ReportShopMap reportShopMap) throws BusinessException;
	ReportShopMap getReportShopMapById(Long id) throws BusinessException;
	ReportShopMap getReportShopMapByShopIdReportId(Long shopId, Long reportId) throws BusinessException;
	void updateReportShopMap(ReportShopMap reportShopMap) throws BusinessException;
	void deleteReportShopMap(ReportShopMap reportShopMap) throws BusinessException;
	List<ReportShopMap> getListReportShopMapByReportId(Long reportId) throws BusinessException;
	
	
	
	
	/** Quan ly mau bao cáo */
	/**
	 * @author vuongmq
	 * @des : Danh sach Report
	 */
	ObjectVO<Report> getListReport(KPaging<Report> kPaging, ReportFilter filter) throws BusinessException;
	
	ObjectVO<ReportUrl> getListReportUrl (KPaging<ReportUrl> kPaging, ReportFilter filter) throws BusinessException;
	
	ReportUrl getReportFileName(Long shopId, String maBC) throws BusinessException;
	
	/**
	 * Lay file bao cao mac dinh
	 * 
	 * @author lacnv1
	 * @since Oct 14, 2014
	 */
	ReportUrl getDefaultReportUrl(ReportFilter filter) throws BusinessException;
}

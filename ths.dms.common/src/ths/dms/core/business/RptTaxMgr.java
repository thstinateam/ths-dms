package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.InvoiceStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.RptInvVO;
import ths.core.entities.vo.rpt.RptInvoiceComparingVO;
import ths.dms.core.exceptions.BusinessException;

public interface RptTaxMgr {

	/**
	 * 3.1.7.1	Xem hóa đơn GTGT
	 * @author hieunq1 -> thanhnn
	 * @param kPaging
	 * @param shopId
	 * @param staffCode
	 * @param shortCode
	 * @param vat
	 * @param invSatus
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	List<RptInvVO> getListRptInvVO(KPaging<SaleOrder> kPaging, Long shopId,
			String staffCode, String shortCode, Float vat,
			InvoiceStatus invSatus, Date fromDate, Date toDate)
			throws BusinessException;

	/**
	 * @author hungnm
	 * @param kPaging
	 * @param vat
	 * @param customerCode
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @param status
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<RptInvoiceComparingVO> getListRptInvoiceComparingVO(
			KPaging<RptInvoiceComparingVO> kPaging, Float vat,
			String customerCode, Long staffId, Date fromDate, Date toDate,
			Long shopId, ActiveType status) throws BusinessException;

}

package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.EquipStockAdjustForm;
import ths.dms.core.entities.EquipStockAdjustFormDtl;
import ths.dms.core.entities.filter.EquipStockAdjustFormFilter;
import ths.dms.core.entities.vo.EquipStockAdjustFormDtlVO;
import ths.dms.core.entities.vo.EquipStockAdjustFormRecord;
import ths.dms.core.entities.vo.EquipStockAdjustFormVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

/**
 * @author Datpv4
 * @since July, 02 2015
 * @description EquipStockAdjustFormMrg dung cho Quan ly kho dieu chinh
 */

public interface EquipStockAdjustFormMgr {
	
	/**
	 * @author Datpv4
	 * @version
	 * @param EquipStockAdjustFormFilter filter
	 * @return 
	 * @since July, 02 2015
	 * @throws BusinessException
	 * @description get list EquipStockAdjustForm by Filter
	 */
	ObjectVO<EquipStockAdjustFormVO> getListEquipStockAdjustFormByFilter(EquipStockAdjustFormFilter filter) throws BusinessException;
	
	/**
	 * @author Datpv4
	 * @param Long id
	 * @return 
	 * @since July, 02 2015
	 * @throws BusinessException
	 * @description get EquipStockAdjustForm by Id
	 */
	EquipStockAdjustForm getEquipStockAdjustFormById(Long id) throws BusinessException;
	
	/**
	 * @author Datpv4
	 * @param EquipStockAdjustFormFilter filter
	 * @param LogInfoVO logInfoVO
	 * @return 
	 * @since July, 02 2015
	 * @throws BusinessException
	 * @description cap nhat trang thai
	 */
	void updateStatus(EquipStockAdjustFormFilter filter, LogInfoVO logInfoVO) throws BusinessException;
		
	/**
	 * @author Datpv4
	 * @param EquipStockAdjustFormFilter filter
	 * @return 
	 * @since July, 02 2015
	 * @throws BusinessException
	 * @description
	 */
	ObjectVO<EquipStockAdjustFormDtlVO> getListEquipStockAdjustFormDtlFilter(EquipStockAdjustFormFilter filter) throws BusinessException;
	
	/**
	 * @author Datpv4
	 * @param EquipStockAdjustFormRecord
	 * @param logInfo
	 * @return ObjectVO<EquipStockAdjustFormDtlVO>
	 * @since July, 02 2015
	 * @throws BusinessException
	 * @description create Or update EquipStockAdjustForm, them moi hoac cap nhat phieu
	 */
    String createOrUpdateEquipStockAdjustForm( EquipStockAdjustFormRecord record, LogInfoVO logInfo) throws BusinessException;

    /**
     * @author Datpv4
	 * @param List<Long> filter, danh sach id EquipStockAdjustForm
	 * @return 
	 * @since July, 02 2015
	 * @throws BusinessException
	 * @description get list EquipStockAdjustForm by list Id
	 */
    List<EquipStockAdjustForm> getListEquipStockAdjustFormByListId(List<Long> filter) throws BusinessException;

    EquipStockAdjustFormDtl getEquipStockAdjustFormDetailByID(Long id) throws BusinessException;
	
}
package ths.dms.core.business;

import java.util.Date;

import ths.dms.core.entities.PoCustomer;
import ths.dms.core.exceptions.BusinessException;

/**
 * @author tungtt21
 *
 */
public interface PoCustomerMgr {
	void updatePoCustomer(PoCustomer poCustomer) throws BusinessException;
	PoCustomer getPoCustomerByOrderNumberAndShopId(String orderNumber, Long shopId, Date orderDate)throws BusinessException;
}



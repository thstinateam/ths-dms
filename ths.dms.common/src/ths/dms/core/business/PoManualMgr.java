/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.Po;
import ths.dms.core.entities.PoDetail;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.filter.PoManualFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PoManualVO;
import ths.dms.core.entities.vo.PoManualVODetail;
import ths.dms.core.entities.vo.PoManualVOError;
import ths.dms.core.entities.vo.PoManualVOSave;
import ths.dms.core.entities.vo.PoVnmManualVO;
import ths.dms.core.exceptions.BusinessException;

/**
 * Mo ta class PoManualMgr.java
 * @author vuongmq
 * @since Dec 7, 2015
 */
public interface PoManualMgr {
	
	/**
	 * Xu ly createPo
	 * @author vuongmq
	 * @param po
	 * @return Po
	 * @throws BusinessException
	 * @since Dec 7, 2015
	*/
	Po createPo(Po po) throws BusinessException;

	/**
	 * Xu ly createListPo
	 * @author vuongmq
	 * @param listPo
	 * @throws BusinessException
	 * @since Dec 7, 2015
	*/
	void createListPo(List<Po> listPo) throws BusinessException;

	/**
	 * Xu ly deletePo
	 * @author vuongmq
	 * @param po
	 * @throws BusinessException
	 * @since Dec 7, 2015
	*/
	void deletePo(Po po) throws BusinessException;

	/**
	 * Xu ly getPoById
	 * @author vuongmq
	 * @param id
	 * @return Po
	 * @throws BusinessException
	 * @since Dec 7, 2015
	*/
	Po getPoById(long id) throws BusinessException;

	/**
	 * Xu ly updatePo
	 * @author vuongmq
	 * @param po
	 * @throws BusinessException
	 * @since Dec 7, 2015
	*/
	void updatePo(Po po) throws BusinessException;

	/**
	 * Xu ly updateListPo
	 * @author vuongmq
	 * @param listPo
	 * @throws BusinessException
	 * @since Dec 7, 2015
	*/
	void updateListPo(List<Po> listPo) throws BusinessException;

	/**
	 * Xu ly getPoByPoNumber
	 * @author vuongmq
	 * @param poNumber
	 * @return Po
	 * @throws BusinessException
	 * @since Feb 2, 2016
	*/
	Po getPoByPoNumber(String poNumber) throws BusinessException;
	
	/**
	 * Xu ly getListPoDetailByPoId
	 * @author vuongmq
	 * @param poId
	 * @return List<PoDetail>
	 * @throws BusinessException
	 * @since Dec 7, 2015
	*/
	List<PoDetail> getListPoDetailByPoId(Long poId) throws BusinessException;

	/**
	 * Xu ly getPoDetailByPoDetailIdAndProductId
	 * @author vuongmq
	 * @param poDetailId
	 * @return PoDetail
	 * @throws BusinessException
	 * @since Dec 7, 2015
	*/
	PoDetail getPoDetailByPoDetailIdAndProductId(Long poDetailId) throws BusinessException;

	/**
	 * Xu ly getListOrderProductVO
	 * @author vuongmq
	 * @param filter
	 * @return List<OrderProductVO>
	 * @throws BusinessException
	 * @since Dec 7, 2015
	*/
	List<OrderProductVO> getListOrderProductVO(PoManualFilter<OrderProductVO> filter) throws BusinessException;

	/**
	 * Xu ly saveOrderManual
	 * @author vuongmq
	 * @param poManualVOSave
	 * @param logInfoVO
	 * @return String
	 * @throws BusinessException
	 * @since Dec 8, 2015
	*/
	String saveOrderManual(PoManualVOSave poManualVOSave, LogInfoVO logInfoVO) throws BusinessException;
	
	/**
	 * Xu ly getListPoVNMByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<PoVnm>
	 * @throws BusinessException
	 * @since Dec 14, 2015
	*/
	List<PoVnm> getListPoVNMByFilter(PoVnmFilter filter) throws BusinessException;
	
	/**
	 * Xu ly getListPoVNMManualByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<PoVnmManualVO>
	 * @throws BusinessException
	 * @since Dec 15, 2015
	 */
	List<PoVnmManualVO> getListPoVNMManualByFilter(PoVnmFilter filter) throws BusinessException;
	
	/**
	 * Xu ly getListObjectVOPoVNMManualByFilter
	 * @author vuongmq
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @since Dec 15, 2015
	 */
	ObjectVO<PoVnmManualVO> getListObjectVOPoVNMManualByFilter(PoVnmFilter filter) throws BusinessException;
	
	/**
	 * Xu ly getListPoVNMDetailByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<OrderProductVO>
	 * @throws BusinessException
	 * @since Dec 14, 2015
	*/
	List<OrderProductVO> getListPoVNMDetailByFilter(PoVnmFilter filter) throws BusinessException;
	
	/**
	 * Xu ly getListPoManualVOByFilter
	 * @author vuongmq
	 * @param filter
	 * @return ObjectVO<PoManualVO>
	 * @throws BusinessException
	 * @since Dec 18, 2015
	*/
	ObjectVO<PoManualVO> getListPoManualVOByFilter(PoManualFilter<PoManualVO> filter) throws BusinessException;
	
	/**
	 * Xu ly getListPoManualDetailVOByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<PoManualVODetail>
	 * @throws BusinessException
	 * @since Dec 21, 2015
	*/
	List<PoManualVODetail> getListPoManualDetailVOByFilter(PoManualFilter<PoManualVODetail> filter) throws BusinessException;

	/**
	 * Xu ly updateStatusOrderManual
	 * @author vuongmq
	 * @param lstId
	 * @param approvedStep
	 * @param logInfoVO
	 * @return List<PoManualVOError>
	 * @throws BusinessException
	 * @since Dec 21, 2015
	*/
	List<PoManualVOError> updateStatusOrderManual(List<Long> lstId, Integer approvedStep, LogInfoVO logInfoVO) throws BusinessException;
	
	/**
	 * Xu ly getListPoDetailEditByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<OrderProductVO>
	 * @throws BusinessException
	 * @since Dec 28, 2015
	*/
	List<OrderProductVO> getListPoDetailEditByFilter(PoManualFilter<OrderProductVO> filter) throws BusinessException;
}



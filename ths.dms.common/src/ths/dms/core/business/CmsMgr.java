package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.enumtype.CmsFilter;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.CmsVO;
import ths.dms.core.entities.vo.RoleVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.TreeVOBasic;
import ths.dms.core.exceptions.BusinessException;

public interface CmsMgr {
	List<CmsVO> getListUrl(CmsFilter filter) throws BusinessException;

	TreeVOBasic<ShopVO> getListOrgAccess(CmsFilter filter) throws BusinessException;

	List<CmsVO> getListShopInRoleByUser(CmsFilter filter) throws BusinessException;

	List<CmsVO> getListRoleByUser(CmsFilter filter) throws BusinessException;

	CmsVO getAppTockenVOByCode(String code) throws BusinessException;

	List<CmsVO> getListFormControlByRole(CmsFilter filter) throws BusinessException;

	List<ShopVO> getListShopChildrenByRole(CmsFilter filter) throws BusinessException;

	List<ShopVO> getListShopByLstIdParent(CmsFilter filter) throws BusinessException;

	TreeVOBasic<ShopVO> getTreeShopByRole(CmsFilter filter) throws BusinessException;

	TreeVOBasic<ShopVO> getTreeFullShopVOByFilter(BasicFilter<ShopVO> filter) throws BusinessException;

	List<ShopVO> getFullShopInOrgAccessByFilter(CmsFilter filter) throws BusinessException;

	List<ShopVO> getListShopInOrgAccess(CmsFilter filter) throws BusinessException;

	/**
	 * Lay danh sach nhan thoa man quyen du lieu theo User ID <Inherit User
	 * Priv>
	 * 
	 * @author hunglm16
	 * @since August 21,2014
	 * @return List ShopVO
	 * @description Ham lay tat ca danh sach tinh ca con chau cua no voi nhieu
	 *              tham so
	 * */
	List<StaffVO> getListStaffByUserWithRole(CmsFilter filter) throws BusinessException;

	/**
	 * Lay danh sach nhan thoa man quyen du lieu theo User ID <Inherit User
	 * Priv>
	 * 
	 * @author hunglm16
	 * @since September 30,2014
	 * @return List Form
	 * @description Ham lay tat ca danh sach man hinh thuoc WEB
	 * */
	List<CmsVO> getListFormInRoleByFilter(CmsFilter filter) throws BusinessException;

	/**
	 * Tao cay Form
	 * 
	 * @author hunglm16
	 * @since September 30,2014
	 * @description Dac biet la tao cay bao cao, khong sua ham lien quan den ham
	 *              nay
	 * */
	TreeVOBasic<CmsVO> getTreeFormControlByRole(CmsFilter filter) throws BusinessException;

	/**
	 * Tao cay Form
	 * 
	 * @author hunglm16
	 * @since Mart 21,2015
	 * @description Dac biet la tao cay bao cao, khong sua ham lien quan den ham
	 *              nay
	 * */
	List<CmsVO> getListFormInRoleForTreeByFilter(CmsFilter filter) throws BusinessException;

	/**
	 * Kiem tra tinh hop le cua Shop
	 * 
	 * @param shop_id
	 *            - Đon vi tuong tac; Khong duoc trong
	 * @param staffRootId
	 *            - Lay tu currentUser trong session co the hieu la Inherit
	 *            User; truogn nay co the de trong
	 * @param shopRootId
	 *            - Lay tu currentUser trong session co the hieu la Don vi dang
	 *            chon de tuong tac voi he thong; truogn nay co the de trong
	 * @description staffRootId & shopRootId khonphai co it nhat mot truong co
	 *              gia tri
	 * @return 1 - Ton tai, 0 - Khong ton tai
	 * 
	 * @author hunglm16
	 * @since March 25,2015
	 * */
	int checkShopInCMSByFilter(CmsFilter filter) throws BusinessException;

	List<RoleVO> getListOwnerRoleShopVOByStaff(Long staffId)
			throws BusinessException;

	List<ShopVO> getListShopVOByStaffVO(CmsFilter filter)
			throws BusinessException;

	List<CmsVO> getListChildStaffInherit(CmsFilter filter)
			throws BusinessException;

	List<CmsVO> getListChildShopInherit(CmsFilter filter)
			throws BusinessException;

	List<ShopVO> getFullShopOwnerByFilter(CmsFilter filter)
			throws BusinessException;

	
	/**
	 * Lay man hinh theo URL
	 * 
	 * @author hunglm16
	 * @param url
	 * @param permissionType
	 * @param roleId
	 * @param userId
	 * @param inheritStaffId
	 * @return
	 * @throws DataAccessException
	 * @since August 26, 2015
	 */
	CmsVO getFormCmsVoByUrl(String url, Integer permissionType, Long roleId, Long userId, Long inheritStaffId) throws BusinessException;
}

package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.WorkingDateConfig;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.WorkingDateConfigFilter;
import ths.dms.core.entities.enumtype.WorkingDateProcedureType;
import ths.dms.core.entities.enumtype.WorkingDateType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.WorkDateTempVO;
import ths.dms.core.entities.vo.WorkingDateConfigVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

// TODO: Auto-generated Javadoc
/**
 * The Interface ExceptionDayMgr.
 */
public interface ExceptionDayMgr {
	
	/**
	 * Creates the exception day.
	 *
	 * @param exceptionDay the exception day
	 * @param logInfo the log info
	 * @return the exception day
	 * @throws BusinessException the business exception
	 */
	ExceptionDay createExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo)
			throws BusinessException;

	/**
	 * Update exception day.
	 *
	 * @param exceptionDay the exception day
	 * @param logInfo the log info
	 * @throws BusinessException the business exception
	 */
	void updateExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo)
			throws BusinessException;

	/**
	 * Delete exception day.
	 *
	 * @param exceptionDay the exception day
	 * @param logInfo the log info
	 * @throws BusinessException the business exception
	 */
	void deleteExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo)
			throws BusinessException;

	/**
	 * Gets the exception day by year.
	 *
	 * @param year the year
	 * @return the exception day by year
	 * @throws BusinessException the business exception
	 */
	List<ExceptionDay> getExceptionDayByYear(int year) throws BusinessException;
	
	/**
	 * Gets the exception day by id.
	 *
	 * @param id the id
	 * @return the exception day by id
	 * @throws BusinessException the business exception
	 */
	ExceptionDay getExceptionDayById(Long id) throws BusinessException;

	/**
	 * Gets the exception day by date.
	 *
	 * @param date the date
	 * @param shopId the shop id
	 * @return the exception day by date
	 * @throws BusinessException the business exception
	 */
	ExceptionDay getExceptionDayByDate(Date date,Long shopId) throws BusinessException;

	/**
	 * Gets the exception day by month.
	 *
	 * @param month the month
	 * @param year the year
	 * @param shopId the shop id
	 * @return the exception day by month
	 * @throws BusinessException the business exception
	 */
	List<ExceptionDay> getExceptionDayByMonth(int month, int year,Long shopId)
			throws BusinessException;

	/**
	 * Gets the exception day by year.
	 *
	 * @param year the year
	 * @param shopId the shop id
	 * @return the exception day by year
	 * @throws BusinessException the business exception
	 */
	List<ExceptionDay> getExceptionDayByYear(int year,Long shopId) throws BusinessException;
	
	/**
	 * Gets the num exception by date in month.
	 *
	 * @param toDate the to date
	 * @return the num exception by date in month
	 * @throws BusinessException the business exception
	 */
	Integer getNumExceptionByDateInMonth(Date toDate)
		throws BusinessException;
	
	/**
	 * Gets the num of holiday.
	 *
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the num of holiday
	 * @throws BusinessException the business exception
	 */
	Integer getNumOfHoliday(Date fromDate, Date toDate)
	throws BusinessException;

	/**
	 * Gets the exception day by month.
	 *
	 * @param month the month
	 * @param year the year
	 * @return the exception day by month
	 * @throws BusinessException the business exception
	 */
	List<ExceptionDay> getExceptionDayByMonth(int month, int year)
			throws BusinessException;

	/**
	 * Gets the list working date config by filter.
	 *
	 * @param filter the filter
	 * @return the list working date config by filter
	 * @throws BusinessException the business exception
	 * @author liemtpt
	 */
	List<WorkingDateConfig> getListWorkingDateConfigByFilter(
			WorkingDateConfigFilter filter) throws BusinessException;
	/**
	 * get shop parent working date config
	 * 
	 * @author liemtpt
	 * @since Feb 02,2014
	 * @description Lay shop cha theo dk shop chon 
	 * */
	Shop getShopParentWorkingDateConfig(Long shopId, WorkingDateType type)
			throws BusinessException;

	/**
	 * Call proceduce working date.
	 * @author liemtpt
	 * @param shopId the shop id
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @param type the type
	 * @throws BusinessException the business exception
	 */
	void callProceduceWorkingDate(Long shopId, Date fromDate, Date toDate,
			WorkingDateProcedureType type) throws BusinessException;
	/**
	 * inherit working date.
	 * @author liemtpt
	 * @param shopId the shop id
	 * @throws BusinessException the business exception
	 */
	int inheritWorkingDate(Shop shop, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * Creates the work date.
	 *
	 * @param shop the shop
	 * @param type the type
	 * @param fromMonth the from month
	 * @param toMonth the to month
	 * @param lstWorkDate the lst work date
	 * @param lstDefaultWork the lst default work
	 * @param logInfoVO the log info vo
	 * @return true, if successful
	 * @throws BusinessException the business exception
	 * @author vuongmq
	 * @date 30/01/2015
	 * @description thiet lap ngay lam viec
	 */
	int createWorkDate(Shop shop, Integer type, String fromMonth, String toMonth, List<WorkDateTempVO> lstWorkDate, List<Integer> lstDefaultWork, LogInfoVO logInfoVO) throws BusinessException;

	List<WorkingDateConfigVO> getWorkDateConfigDateCheckParent(Long shopId,	WorkingDateType type, ActiveType active) throws BusinessException;

	
	/**
	 * Gets the num of holiday by shop
	 *
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @param shopId
	 * @return the num of holiday of shop
	 * @throws DataAccessException the data access exception
	 */
	Integer getNumOfHoliday(Date fromDate, Date toDate, Long shopId) throws BusinessException;

	/**
	 * Creates the work date by shop parent.
	 *
	 * @param shop the shop
	 * @param type the type
	 * @param fromMonth the from month
	 * @param toMonth the to month
	 * @param lstWorkDate the lst work date
	 * @param lstDefaultWork the lst default work
	 * @param logInfoVO the log info vo
	 * @return true, if successful
	 * @throws BusinessException the business exception
	 * @author hoanv25
	 * @date August 14,2015
	 * @description thiet lap ngay lam viec cho shop con
	 */	
	Integer createWorkDateCopyShopParent(Shop newShop, WorkingDateType chiDinhDonVi, String fromMonth, Object object, Object object2, Object object3, LogInfoVO logInfoVO) throws BusinessException;
}

package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.CustomerDisplayPrograme;
import ths.dms.core.entities.DisplayShopMap;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StDisplayPdGroup;
import ths.dms.core.entities.StDisplayPdGroupDtl;
import ths.dms.core.entities.StDisplayPlAmtDtl;
import ths.dms.core.entities.StDisplayPlDpDtl;
import ths.dms.core.entities.StDisplayProgram;
import ths.dms.core.entities.StDisplayProgramExclusion;
import ths.dms.core.entities.StDisplayProgramLevel;
import ths.dms.core.entities.StDisplayProgramVNM;
import ths.dms.core.entities.StDisplayStaffMap;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.DisplayProgramExclusionFilter;
import ths.dms.core.entities.enumtype.DisplayProgramFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StDisplayPdGroupFilter;
import ths.dms.core.entities.enumtype.StDisplayProgramFilter;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.enumtype.StaffRoleType;
import ths.dms.core.entities.vo.CustomerDisplayProgrameVO;
import ths.dms.core.entities.vo.DisplayDetailVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.entities.vo.StDisplayPDGroupDTLVO;
import ths.dms.core.exceptions.BusinessException;

public interface DisplayProgrameMgr {
	
	
	/**
	 * Copy CTTB
	 * @author tientv
	 */
	StDisplayProgram copyDisplayProgram(Long displayProgramId, LogInfoVO logInfo,String displayProgramCode, String displayProgramName)throws BusinessException;
	
	/**
	 * Copy CTTB
	 * @author tientv
	 */
	Integer checkExistsProductDisplayGr(Long dpId,DisplayProductGroupType type) throws BusinessException;
	
	
	/** 
	 * @author tientv */
	StDisplayProgram getStDisplayProgramByCode(String code) throws BusinessException;
	/** 
	 * @author tientv */
	String checkUpdateDisplayProgramForActive(Long stDisplayProgramId) throws BusinessException;
	/** 
	 * @author tientv */
	void updateDisplayProgram(StDisplayProgram stDisplayProgram, LogInfoVO logInfo) throws BusinessException;
	
	/** 
	 * @author tientv */
	List<StDisplayPDGroupDTLVO> getDisplayPDGroupDTLVOs(Long levelId,Long st_display_pd_group_id) throws BusinessException;
	
	/** 
	 * @author tientv */
	StDisplayProgram createDisplayProgram(StDisplayProgram stDisplayProgram, LogInfoVO logInfo) throws BusinessException;
	
	/** 
	 * @author tientv */
	StDisplayPdGroupDtl getDisplayProductGroupDTLByGroupId(Long stDisplayPdGroupId,
			Long productId) throws BusinessException;
			
	/** 
	 * @author tientv */
	ObjectVO<StDisplayStaffMap> getListDisplayStaffMap(KPaging<StDisplayStaffMap> kPaging, 
			Long displayProgramId, List<Long> lstShopId, String month, boolean checkMap, long parentStaffId)throws BusinessException;
	
	/** 
	 * @author tientv */
	StDisplayStaffMap getDisplayStaffMap(Long dpId,Long staffId,Date month) throws BusinessException;
	
	/** 
	 * @des : lay chi tiet nhom phat sinh doanh so
	 * @author tientv */
	StDisplayPlAmtDtl getStDisplayPlAmtDtlByAmtGr(Long levelId,Long grId) throws BusinessException;
	
	/** 
	 * @des : lay chi tiet nhom san pham trung bay
	 * @author tientv */
	StDisplayPlDpDtl getStDisplayPlPrDtlByGr(Long grId,Long productId) throws BusinessException;	
	
	/** 
	 * @author tientv */
	void appendStDisplayStaffMap(List<StDisplayStaffMap> stDisplayStaffMaps,LogInfoVO logInfo)throws BusinessException;
	
	StDisplayProgram getStDisplayProgrameById(Long id) throws BusinessException;

	void createOrUpdateDisplayProgameCustomer(
			CustomerDisplayPrograme customerDisplayPrograme)
			throws BusinessException;

	ObjectVO<CustomerDisplayProgrameVO> getListCustomerDisplayPrograme(
			KPaging<CustomerDisplayProgrameVO> kPaging, Long shopId,
			Long id, String month) throws BusinessException;

	/*
	 * Xoa cttb cap nhat lai status=0
	 */
	void deleteListCustomerDisplayPrograme(List<Long> lstCustomerDisplayPrograme)
			throws BusinessException;

	int countShopConnectBy(Long shopId) throws BusinessException;

	ObjectVO<Customer> getListCustomerInLevel(KPaging<Customer> kPaging,
			String displayProgramCode, String levelCode)
			throws BusinessException;
	
	StDisplayProgramLevel checkLevelCodeExists(Long id,String levelCode)throws BusinessException;
	/**
	 * 
	 * @param lstCusDisplayPrograme
	 * @return
	 * @throws BusinessException
	 */
	List<CustomerDisplayPrograme> checkListDisplayProgrameCustomer(
			List<CustomerDisplayPrograme> lstCusDisplayPrograme)
			throws BusinessException;

	Boolean checkDisplayProgrameInShop(Long displayProgrameId, Long shopId, Date month)throws BusinessException;
	
	Boolean checkDisplayProgrameInShopForCustomer(Long displayProgrameId, Long shopIdOfCustomer, Date month)throws BusinessException;
	
	ObjectVO<DisplayDetailVO> getListCTTBbyId(KPaging<DisplayDetailVO> kPaging,Long shopId, Long idCTTB, Date date) throws BusinessException;

	/**
	 * Search CTTB 2013
	 * @author thachnn
	 */
	ObjectVO<StDisplayProgram> getListDisplayProgram(KPaging<StDisplayProgram> kPaging, StDisplayProgramFilter filter,
			StaffRoleType staffRole) throws BusinessException;

	ObjectVO<StDisplayPdGroup> getListDisplayGroup(
			KPaging<StDisplayPdGroup> kPaging, StDisplayPdGroupFilter filter,
			StaffRoleType staffRole) throws BusinessException;



	/**
	 * @author loctt
	 * @since 17Sep2013
	 */
	StDisplayProgram getStDisplayProgramById(Long id) throws BusinessException;


	ObjectVO<StDisplayProgramExclusion> getListDisplayProgramExclusion(
			KPaging<StDisplayProgramExclusion> kPaging,
			DisplayProgramExclusionFilter filter, StaffRole staffRole)
			throws BusinessException;
	/**
	 * Danh sach them moi CTTB Loai tru
	 * @author loctt
	 */
	ObjectVO<StDisplayProgram> getListDisplayProgramForExclusion(KPaging<StDisplayProgram> kPaging,
			DisplayProgramFilter filter) throws BusinessException;
	/**
	 * Them moi List CTTB loai tru
	 * @author loctt
	 */
	void createListDisplayProgramExclusion(StDisplayProgram displayProgram,
			List<Long> listDPExclusionId, LogInfoVO logInfo)
			throws BusinessException;
	/**
	 * @author loctt
	 */
	StDisplayProgramExclusion getDisplayProgramExclusionById(Long id) throws BusinessException;
	/**
	 * @author loctt
	 */
	void updateDisplayProgramExclusion( StDisplayProgramExclusion displayProgramExclusion, LogInfoVO logInfo) throws BusinessException;
	/**
	 * @author loctt, 18sep2013
	 */
	/**
	 * Danh sach muc cua CTTB
	 * @author loctt
	 */
	ObjectVO<StDisplayProgramLevel> getListDisplayProgramLevel(KPaging<StDisplayProgramLevel> kPaging, Long displayProgramId)
			throws BusinessException;

	/**
	 * @author thachnn
	 */
	ObjectVO<StDisplayPdGroupDtl> getListDisplayGroupDetail(
			KPaging<StDisplayPdGroupDtl> kPaging,
			Long stDisplayPdGroupId, StaffRoleType staffRole)
			throws BusinessException;
	/**
	 * Danh sach nhom san pham (type) 1 :DS 2:SKU
	 * @author loctt
	 */
	ObjectVO<StDisplayPdGroup> getListDisplayProductGroup(KPaging<StDisplayPdGroup> kPaging, 
			Long displayProgramId,DisplayProductGroupType type) throws BusinessException;
	//loctt 19sep2013
	StDisplayProgramLevel getDisplayProgramLevelById(Long id)
	throws BusinessException;
	/**
	 * Tao moi hoac cap nhat muc cua CTTB
	 * @author loctt
	 * @since 19sep2013
	 */
	void createOrUpdateDisplayProgramLevel(StDisplayProgramLevel displayProgramLevel,
			List<StDisplayPlAmtDtl> displayPlAmtDtls,
			List<StDisplayPlDpDtl> stDisplayPlDpDtls,
			LogInfoVO logInfo, Boolean isUpdate) throws BusinessException;
/**
 * @author loctt
 * @since 19sep2013
 * @param code
 * @param displayProgramId
 * @return
 * @throws BusinessException
 */
	StDisplayProgramLevel getDisplayProgramLevelByCode(String code, Long displayProgramId) throws BusinessException;
/**
 * @author loctt
 * @since 19sep2013
 */
	List<StDisplayPlAmtDtl> getListDisplayProgramLevelDetailByLevelId(Long levelId)
	throws BusinessException;
	/**
	 * @author loctt
	 * @since 19sep2013
	 */
	StDisplayPdGroup getDisplayProductGroupById(Long id) throws BusinessException;	
	
	/**
	 * @author thachnn
	 */
	void updateStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup, LogInfoVO logInfo) throws BusinessException;
	StDisplayPdGroup getStDisplayPdGroupByCode( Long stDisplayProgramId,DisplayProductGroupType type, String stDisplayPdGroupCode) throws BusinessException;
	StDisplayPdGroup createStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup, LogInfoVO logInfo)throws BusinessException;
	void deleteStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup) throws BusinessException;
	
	/**
	 * @author loctt
	 * @since 19sep2013
	 */
	void updateDisplayProgramLevel(StDisplayProgramLevel displayProgramLevel, LogInfoVO logInfo) throws BusinessException;
	
	void createListDisplayShopMap(List<DisplayShopMap> lstDisplayShopMap, LogInfoVO logInfo) throws BusinessException;
	void createDisplayShopMap(DisplayShopMap displayShopMap, LogInfoVO logInfo) throws BusinessException;
	/*
	 * author: lochp
	 * san pham doanh so CTTB
	 */
	StDisplayPdGroup getDisplayProductGroupByCode( Long displayProgramId,DisplayProductGroupType type, String displayProductGroupCode) throws BusinessException;
	void deleteStDisplayPdGroup(StDisplayPdGroup displayProductGroup, LogInfoVO logInfo) throws BusinessException;
	ObjectVO<StDisplayPdGroup> getListDisplayProgrameAmount(KPaging<StDisplayPdGroup> kPaging, StDisplayPdGroupFilter filter, StaffRole staffRole)
	throws BusinessException;
	ObjectVO<StDisplayPdGroupDtl> getListDisplayProgrameAmountProductDetail(KPaging<StDisplayPdGroupDtl> kPaging, StDisplayPdGroupFilter filter,
			StaffRole staffRole) throws BusinessException;
	
	void deleteDisplayShopMap(Long shopId, Long displayProgramId,LogInfoVO logInfo) throws BusinessException;

	ObjectVO<StDisplayPdGroupDtl> getListDisplayProductGroupDTL( KPaging<StDisplayPdGroupDtl> kPaging,
			Long displayProductGroupDTLId) throws BusinessException;
	void updateDisplayProductGroupDTL(StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo)throws BusinessException;
	StDisplayPdGroupDtl getDisplayProductGroupDTLById(Long id)throws BusinessException;
	StDisplayPdGroupDtl getStDisplayPdGroupDtlByProductId(Long productId) throws BusinessException;
	void createListDisplayProductGroupDTL(List<StDisplayPdGroupDtl> listDisplayProductGroupDTL, LogInfoVO logInfo) throws BusinessException;
	StDisplayPdGroupDtl getStDisplayPdGroupDtlByProductId(Long stDisplayPdGroupDtlId, Long productId) throws BusinessException;
	/**
	 * @author thachnn
	 * Them, xoa, sua SP vào CTTB - SPTB
	 */
	StDisplayPdGroupDtl getStDisplayPdGroupDtlById(Long id)throws BusinessException;
	void createListStDisplayPdGroupDtl(List<StDisplayPdGroupDtl> listStDisplayPdGroupDtl,LogInfoVO logInfo) throws BusinessException;
	void updateStDisplayPdGroupDTL(StDisplayPdGroupDtl stDisplayPdGroupDtl, LogInfoVO logInfo)throws BusinessException;
	StDisplayPdGroupDtl getStDisplayPdGroupDtlByGroupId(Long displayProductGroupId,Long productId) throws BusinessException;
	
	Boolean checkExistRecordDisplayShopMap(Long shopId,Long displayProgramId) throws BusinessException;
	List<Shop> getListShopActiveInDisplayProgram(Long displayProgramId, String shopCode, String shopName) throws BusinessException;

	/**
	 * hien danh sach san pham khi mo level dialog
	 * @author loctt
	 * @since 25sep2013
	 */
	ObjectVO<StDisplayPdGroup> getListLevelProductGroup(KPaging<StDisplayPdGroup> kPaging, 
			Long displayProgramId,DisplayProductGroupType type,Long levelId) throws BusinessException;

	ObjectVO<ProductVO> getListGroupProductVO(KPaging<ProductVO> kPaging,
			String productCode, String productName, Long displayProgramId,
			DisplayProductGroupType type)
			throws BusinessException;
	
	/**
	 * @author thachnn
	 */
	ObjectVO<Product> getListProduct(KPaging<Product> kPaging, 
			Long displayProgramId,String code, String name, DisplayProductGroupType type) throws BusinessException;
	/**
	 * @author loctt
	 * @param levelId
	 * @since 25sep2013
	 * @return
	 * @throws BusinessException
	 */
	List<StDisplayPlDpDtl> getListDisplayProductLevelDetailByLevelId(Long levelId)
	throws BusinessException;
	
	void updateDisplayStaffMap(StDisplayStaffMap displayStaffMap, LogInfoVO logInfo)
	throws BusinessException;
	
	StDisplayStaffMap createDisplayStaffMap(StDisplayStaffMap displayStaffMap, LogInfoVO logInfo)
	throws BusinessException;

	void deleteListDisplayStaffMap(List<Long> lstId, LogInfoVO logInfo)
			throws BusinessException;

	void deleteAllDisplayStaffMap(Long displayId, String month, boolean checkMap, long parentStaffId, LogInfoVO logInfo) throws BusinessException;
	
	CustomerDisplayPrograme createCustomerDisplayPrograme(
			CustomerDisplayPrograme customerDisplayPrograme)
			throws BusinessException;
	
	void updateCustomerDisplayPrograme(CustomerDisplayPrograme customerDisplayPrograme)
	throws BusinessException;
	
	CustomerDisplayPrograme checkExistDisplayCustomer(Long id,Long customerId,Long staffId,String levelCode,String month)throws BusinessException;
	
	CustomerDisplayPrograme getCustomerDisplayProgrameById(Long id)throws BusinessException;
	
	void deleteCustomerDisplayPrograme(CustomerDisplayPrograme customerDisplayPrograme)throws BusinessException;
	
	List<StDisplayProgram> getListDisplayProgramSysdateEX(List<Long> shopId,
			Date month) throws BusinessException;
	
	ObjectVO<CustomerDisplayProgrameVO> getListCustomerDisplayProgrameEX(
			KPaging<CustomerDisplayProgrameVO> kPaging, List<Long> shopId, Long id, String month,
			boolean checkMap, long parentStaffId) throws BusinessException;

	ObjectVO<StDisplayProgram> getListDisplayProgramByShop(
			KPaging<StDisplayProgram> kPaging, Long shopId,
			String displayProgramCode, String displayProgramName)
			throws BusinessException;
	
	Integer checkShopOfStaffJoinDisplayProgram(Long displayProgramId,Long staffId) throws BusinessException;

	//@author: Hung lm16; @since: January 17, 2014; Description: Lay danh sach CTTB theo nam 
	List<StDisplayProgram> getListDisplayProgramByListId(Long shopId, List<Long> lstId, Boolean flag, Integer year, String code,
			String name) throws BusinessException;

	ObjectVO<StDisplayProgram> getListDisplayProgramByYear(KPaging<StDisplayProgram> kPaging, Long shopId, Integer year,
			String code, String name) throws BusinessException;
	/**
	 * Lay danh sach CTTB theo shopID va theo nam
	 * @param shopID
	 * @param year
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<StDisplayProgram> getListDisplayProgramByShopAndYear(KPaging<StDisplayProgram> kPaging, Long shopID,Integer year, String code, String name) throws BusinessException;
	
	ObjectVO<StDisplayProgramVNM> getListDisplayProgramInTowDate(KPaging<StDisplayProgramVNM> kPaging, Long shopId,Date fDate, Date tDate, String code, String name) throws BusinessException;
	
	StDisplayProgramVNM getDisplayProgramByCode(String code) throws BusinessException;
}

package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ToolFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ToolVO;
import ths.dms.core.exceptions.BusinessException;

/**
 * 
 * @author tungmt
 * @since 13/3/2014 
 * @description: quan ly tu lanh cho vnm
 */
public interface SuperviserDeviceMgr {
	/**
	 * 
	 * @author tungmt
	 * @since 15/3/2014 
	 * @description: lay danh sach shop va count tu lanh
	 */
	List<ToolVO> getListShopTool(Long parentShopId, Integer typeSup) throws BusinessException;
	List<ToolVO> getListTool(Long parentShopId, Integer typeSup) throws BusinessException;
	List<ToolVO> getListShopToolOneNode(Long parentShopId, Integer typeSup) throws BusinessException;
	List<Shop> getListAncestor(Long parentShopId ,String shopCode,String shopName) throws BusinessException;
	ObjectVO<ToolVO> getListToolKP(ToolFilter filter) throws BusinessException;
	ObjectVO<ToolVO> getListChip(ToolFilter filter) throws BusinessException;
	ObjectVO<ToolVO> getListChipForCustomer(ToolFilter filter) throws BusinessException;
	void updateChip(Long chipId,Float minTemp,Float maxTemp) throws BusinessException;
	void updateConfig(int tempConfig,int positionConfig) throws BusinessException;
	void updateConfigDistance(int distanceConfig) throws BusinessException;
	List<ToolVO> getBCTemperature(ToolFilter filter) throws BusinessException;
	List<ToolVO> getBCPosition(ToolFilter filter) throws BusinessException;
}

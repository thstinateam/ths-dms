package ths.dms.core.business;
/**
 * @author vuongmq
 * @date 15/04/2015
 * cac ham lay equip_param cho thiet bi
 */

import java.util.List;

import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.vo.ApParamEquipVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.BusinessException;

public interface ApParamEquipMgr {

	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * tao moi ApParamEquip
	 */
	ApParamEquip createApParamEquip(ApParamEquip ApParamEquip, LogInfoVO logInfo) throws BusinessException;

	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * delete ApParamEquip
	 */
	void deleteApParamEquip(ApParamEquip ApParamEquip, LogInfoVO logInfo) throws BusinessException;

	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * update ApParamEquip
	 */
	void updateApParamEquip(ApParamEquip ApParamEquip, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay ApParamEquip theo id
	 */
	ApParamEquip getApParamEquipById(Long id) throws BusinessException;
	
	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay ApParamEquip theo code va type
	 */
	ApParamEquip getApParamEquipByCode(String code, ApParamEquipType type) throws BusinessException;
	
	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay ApParamEquip theo code, type va status
	 */
	ApParamEquip getApParamEquipByCodeX(String code, ApParamEquipType type, ActiveType status) throws BusinessException;
	
	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay ApParamEquip theo code va status
	 */
	ApParamEquip getApParamEquipByCodeActive(String code, ActiveType status) 	throws BusinessException;
	
	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay danh sach ApParamEquip theo type va status
	 */
	List<ApParamEquip> getListApParamEquipByType(ApParamEquipType type, ActiveType status) throws BusinessException;
	
	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay danh sach ApParamEquip theo filter
	 */
	List<ApParamEquip> getListApParamEquipByFilter(ApParamFilter filter)  throws BusinessException;

	/**
	 * @author vuongmq
	 * @date 22/05/2015
	 * lay danh sach email param repair to (o trang thai phieu sua chua cho duyet gui mail)
	 */
	List<ApParamEquipVO> getListEmailToRepairByFilter(ApParamFilter filter) throws BusinessException;
	/**
	 * Lay danh sach shop cap cha va Equip-Apparam neu co tuong ung
	 * 
	 * @author hunglm16
	 * @since 01/07/2015
	 */
	List<ApParamEquipVO> getListEmailToParentShopByFilter(ApParamFilter filter) throws BusinessException;
	
}
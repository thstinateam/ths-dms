package ths.dms.core.business;

/**
 * Import thu vien
 * */
import java.util.List;

import ths.dms.core.entities.EquipLend;
import ths.dms.core.entities.EquipLendDetail;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.vo.EquipBorrowVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Xu ly du lieu
 * 
 * @author hoanv25
 * @since March 09,2015
 * @description Dung cho Quan ly de nghi muon thiet bi
 * */
public interface EquipProposalBorrowMgr {	
	/**
	 * Lay danh sach Quan ly de nghi muon tu
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	ObjectVO<EquipBorrowVO> searchListProposalBorrowVOByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter) throws BusinessException;
	
	/**
	 * lay equip lend bang id
	 * @author nhutnn
	 * @since 03/07/2015
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	EquipLend getEquipLendById(Long id) throws BusinessException;
	
	/**
	 * lay equip lend bang ma bien ban
	 * @author nhutnn
	 * @since 03/07/2015
	 * @param code
	 * @return
	 * @throws BusinessException
	 */
	EquipLend getEquipLendByCode(String code) throws BusinessException;
	
	/**
	 * Cap nhat equiplend
	 * @author nhutnn
	 * @since 03/07/2015 
	 */
	void updateEquipLend(EquipLend equipLend) throws BusinessException;
	
	/**
	 * Lay dan sach xuat excel
	 * @author nhutnn
	 * @since 03/07/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipBorrowVO> getListEquipLendDetailVOForExportByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter) throws BusinessException;
	
	/**
	 * Tao bien ban muon thiet bi
	 * @author nhutnn
	 * @since 06/07/2015
	 * @param equipLend
	 * @param lstEquipLendDetails
	 * @return
	 * @throws BusinessException
	 */
	EquipLend createEquipLendWithListDetail(EquipLend equipLend, List<EquipLendDetail> lstEquipLendDetails) throws BusinessException;
	
	/**
	 * Chinh sua bien ban voi chi tiet
	 * @author nhutnn
	 * @since 08/07/2015
	 * @param equipLend
	 * @param lstEquipLendDetails
	 * @return
	 * @throws BusinessException
	 */
	void updateEquipLendWithListDetail(EquipLend equipLend, List<EquipLendDetail> lstEquipLendDetails, EquipProposalBorrowFilter<EquipBorrowVO> filter) throws BusinessException;
}
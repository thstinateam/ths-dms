package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.NewTreeVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.UnitFilter;



public interface ShopMgr {

	Shop getShopRoot(Long shopId) throws BusinessException;
	/**
	 * Tim kiem danh sach shop dung chung
	 * @modify hunglm16
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @since 24/11/2015
	 */
	ObjectVO<ShopVO> getListShopVOBasicByFilter(BasicFilter<ShopVO> filter) throws BusinessException;
	ObjectVO<ShopVO> getListShopVOBasicByFilter_GTMT(BasicFilter<ShopVO> filter) throws BusinessException;
	ObjectVO<Shop> getListShopByFilter(BasicFilter<Shop> filter) throws BusinessException;
	Shop getShopRunningByCode(String code) throws BusinessException;
	List<Shop> getListShopByCodeOrName(String searchStr, Long shopId) throws BusinessException;
	Shop getShopByCode(String code) throws BusinessException;
	
	Shop getShopByCodeNEW(String code) throws BusinessException;
	
	Shop getShopById(Long id) throws BusinessException;
	void deleteShop(Shop shop, LogInfoVO logInfo) throws BusinessException;
	void updateShop(Shop shop, LogInfoVO logInfo) throws BusinessException;
	Boolean checkAncestor(String parentCode, String shopCode)
			throws BusinessException;
	List<Shop> getListSubShop(Long parentId, ActiveType status)
			throws BusinessException;
	
	List<Shop> getListSubShopForPromotionProgram(Long parentId,
			ActiveType status, Long promotionProgramId)
			throws BusinessException;
	
	List<Shop> getListSubShopForFocusProgram(Long parentId, ActiveType status,
			Long focusProgramId) throws BusinessException;
	
	Shop createShop(Shop shop, LogInfoVO logInfo) throws BusinessException;
	List<Shop> getListSubShopExSaleMTIncludeStopped(Long parentId,Boolean oneLevel, Boolean includeStopped) throws BusinessException;
	List<Shop> getListSubShopExSaleMT(Long parentId,Boolean oneLevel) throws BusinessException;
	TreeVO<Shop> getShopTreeVOEx(Long shopId, List<Long> exceptionalShopId,
			String shopCode, String shopName, ActiveType status,Boolean isOrderByName)
			throws BusinessException;
	
	TreeVO<Shop> getShopTreeVO(Shop shop, Long exceptionalShop,
			String shopCode, String shopName,Boolean isOrderByName) throws BusinessException;
	
	List<Shop> getListShopExport(Long shopId) throws BusinessException;
	ObjectVO<Shop> getListShopOfUnitTreeSaleMT(KPaging<Shop> kPaging,String shopCode, String shopName, ActiveType status)throws BusinessException; //Loctt1 - Nov9, 2013
	
	List<Shop> getListSubShopEx(Long parentId, ActiveType status, Long exceptionalShopId) throws BusinessException;
	Shop getShopByIdAndIslevel(Long id, Integer isLevel) throws BusinessException;
	ObjectVO<Shop> getListShopNotInPoGroup(ShopFilter filter, long shopId)
			throws BusinessException;
	/**
	 * Lay danh muc cho bao cao loai HMDHT
	 * @author hunglm16
	 * @param shopId
	 * @throws BusinessException
	 * @since September 27,2014
	 * @description ke thua lai ham cu, khong chinh sua
	 * */
	Object getDMDHT(Long shopId) throws BusinessException;
	
	List<Shop> getAllShopChildrenByLstId(List<Long> lstId) throws BusinessException;
	
	ObjectVO<Shop> getAllShopChild(KPaging<Shop> kPaging, String shopCode,
			String shopName, ActiveType status, String taxNum,
			String invoiceNumberAccount, String phone,
			String mobilePhone,String email, Long shopTypeId, String shopParentCode)
			throws BusinessException;
	
	/**
	 * Lay danh sach shop theo filter
	 * @author tungmt
	 * @since 02/10/2014
	 * 
	 * @modify hunglm16
	 * @since 24/11/2015
	 * @desscription Bo sung phan quyen
	 * */
	ObjectVO<Shop> getListShop(ShopFilter filter) throws BusinessException;
	
	/**
	 * Kiem tra shop ton tai
	 * @author tungmt
	 * @since 02/10/2014
	 * @description 
	 * */
	Boolean checkIfShopExists(ShopFilter filter) throws BusinessException ;
	
	
	Integer getShopByTienTv() throws BusinessException;
	
	/**
	 * Lay shop theo code
	 * **/
	Shop getShopByCodeSaleMT(String code) throws BusinessException;
	/**
	 * Luu thong tin NPP + Huy bo ke hoach huan luyen cua NVBH thuoc NPP
	 * 
	 * @author lacnv1
	 * @since Jun 11, 2014
	 */
	void updateShopWithCancelSalerTrainingPlan(Shop shop, LogInfoVO logInfo) throws BusinessException;
	
	ObjectVO<Shop> getListShopOfUnitTree(KPaging<Shop> kPaging, Long curShopId, String shopCode, String shopName, ActiveType status)throws BusinessException;
	NewTreeVO<Shop, ProgrameExtentVO> getShopTreeVOForOfficeDocument(
			Long userId, Long roleId, Long shopId, Long offDocuID, String shopCode,
			String shopName) throws BusinessException;
	Shop getShopArea(Long shopId) throws BusinessException;
	int getCountChildByShop(ShopFilter filter) throws BusinessException;
	/**
	 * get list child shop vo by filter
	 * 
	 * @author liemtpt
	 * @since Feb 6, 2015
	 */
	List<ShopVO> getListChildShopVOByFilter(BasicFilter<ShopVO> filter)
			throws BusinessException;
	/**
	 * @author vuongmq
	 * @date 13/02/2015
	 * @description: lay list Shop voi id con Organization
	 */
	List<Shop> getListShopOrganization(ShopFilter filter) throws BusinessException;
	
	/**
	 * tim kiem don vi
	 * @author tuannd20
	 * @param kPaging thong tin phan trang
	 * @param unitFilter tieu chi tim kiem
	 * @return ket qua tim kiem don vi
	 * @throws BusinessException
	 * @since 26/02/2015
	 */
	ObjectVO<ShopVO> findShopVOBy(KPaging<ShopVO> kPaging, UnitFilter unitFilter) throws BusinessException;
	
	/**
	 * lay danh sach don vi dang co tren cay don vi
	 * @author tuannd20
	 * @param startFromShops Danh sach cac shop dung lam node goc cua cay. Neu truyen vao Null, mac dinh lay shop cao nhat (shop co parent = null) lam node goc
	 * @param includeStoppedShop true: lay ca nhung don vi tam ngung. false: ko lay don vi tam ngung
	 * @return danh sach don vi dang co tren cay don vi
	 * @throws BusinessException
	 * @since 27/02/2015
	 */
	List<Shop> getShopsOnTree(List<Long> startFromShops, Boolean includeStoppedShop) throws BusinessException;
	
	/**
	 * lay danh sach don vi/don vi loai tru tren cay phan quyen
	 * @author tuannd20
	 * @param shopFilter
	 * @param keyshopId
	 * @return
	 * @throws BusinessException
	 * @since 12/03/2015
	 */
	List<Shop> getShopsInAccessOnTree(ShopFilter shopFilter, Long keyshopId) throws BusinessException;
	
	/**
	 * checkShopHaveActiveSubShop: kiem tra shop co shop con hoat dong
	 * @author phuocdh2
	 * @description: Nhan vao shopId va kiem tra shop con co hoat dong
	 * @return boolean : true : co shop con con hoat dong false : cac shop con da tam ngung het
	 * @since 02/03/2015
	 */
	
	Boolean checkShopHaveActiveSubShop(ActiveType status, Long shopId) throws BusinessException;
	/**
	 * checkShopHaveActiveStaff lay so luong nhan vien truc thuoc shop nhap vao va 
	 * @author phuocdh2
	 * @return true: co nhan vien dang hoat dong false : shop khong co nhan vien dang hoat dong
	 * @throws BusinessException
	 * @since 02/03/2015
	 */
	Boolean checkShopHaveActiveStaff(ActiveType status,Long shopId) throws BusinessException ;
	
	/**
	 * checkShopHaveActiveCustomer lay so luong khach hang hoat dong truc thuoc shop nhap vao va 
	 * @author phuocdh2
	 * @return true : shop co khach hang hoat dong false : shop co khach hang het hoat dong
	 * @throws BusinessException
	 * @since 02/03/2015
	 */
	Boolean checkShopHaveActiveCustomer(ActiveType status,Long shopId) throws BusinessException ;
	/**
	 * tra ve danh sach don vi de export ra file excel
	 * @param kPaging thong tin phan trang
	 * @param unitFilter tieu chi tim kiem
	 * @author phuocdh2
	 * @return danh sach don vi de export
	 * @since 24 Mar 2015
	 */
	ObjectVO<ShopVO> findShopVOExport(KPaging<ShopVO> kPaging, UnitFilter unitFilter) throws BusinessException;

	/**
	 * lay danh sach shop_id theo level co cau hinh hien gia, an gia theo level tu 1-> 4... 
	 * shop con -> shop cha .. uu tien muc con, muc co level thap nhat
	 * @author phuocdh2
	 * @return List<ShopVO> danh sach shop_param
	 * @throws DataAccessException
	 * @since 08/04/2015
	 */
	List<ShopVO> getListShopVOConfigShowPrice(Long shopId, Integer status) throws BusinessException;

	ObjectVO<Shop> getListShopSpectific(ShopFilter shopFilter) throws BusinessException;
	
	/**
	 * lay cau hinh shop_param de qui len shop cha
	 * @author cuonglt3
	 * @param shopId
	 * @param sysModifyPrice
	 * @return
	 * @throws BusinessException
	 */
	List<ShopVO> getListShopConfigByParamCode(Long shopId,ApParamType sysModifyPrice) throws BusinessException;
	
	
	/**
	 * Lay cau hinh shop_param theo paramCode lay de quy va order by
	 * @author trungtm6
	 * @since 21/04/2015
	 * @param shopId
	 * @param paramCode
	 * @return
	 * @throws DataAccessException
	 */
	List<ShopParam> getConfigShopParam (Long shopId, String paramCode) throws BusinessException;
	
	/**
	 * @author tungmt
	 * @since 11/05/2015
	 * @param filter
	 * @return danh sach shop con
	 * @throws DataAccessException
	 * 
	 * @modify hunglm16
	 * @since 
	 */
	ObjectVO<Shop> getListChildShop(ShopFilter filter) throws BusinessException;
	
	/**
	 * @author quangnv11
	 * @since 28/05/2015
	 * @param filter
	 * @return kiem tra shop con
	 * @throws DataAccessException
	 */
	boolean isChild(Long parentID, Long childId) throws BusinessException;
	Shop getShopByParentIdNull() throws BusinessException;
	
	NewTreeVO<Shop, ProgrameExtentVO> getShopTreeInFocusProgram(
			Long focusProgramId, String shopCode, String shopName, Long parentShopId)
			throws BusinessException;
	/**
	 * Lay danh sach don vi chua tham gia chuong trinh
	 */
	NewTreeVO<Shop, ProgrameExtentVO> getShopTreeVOForFocusProgramEx(Long shopId,
			List<Long> exceptionalShopId, String shopCode, String shopName,
			ActiveType status, Long focusProgramId, String strListShopId)
			throws BusinessException;
	/**
	 * lay danh sach don vi dang co tren cay don vi
	 * @author tuannd20
	 * @param shopFilter dieu kien tim kiem
	 * @return danh sach don vi dang co tren cay don vi
	 * @throws BusinessException
	 * @since 27/02/2015
	 */
	List<Shop> getShopsOnTreeFilter(ShopFilter shopFilter, Long keyShopId) throws BusinessException;
	/**
	 * lay danh sach cac shop tham gia keyshop
	 * @author tuannd20
	 * @param permissionId id quyen du lieu can lay
	 * @return danh sach cac shop da duoc phan cho quyen du lieu
	 * @throws BusinessException
	 * @since 10/03/2015
	 */
	List<Shop> retriveShopsForKeyShop(Long keyshopId) throws BusinessException;
	
	/**
	 * Lay danh sach shop con ke can
	 * @author trungtm6
	 * @since 18/08/2015
	 * @param kPaging
	 * @param fitler
	 * @return
	 * @throws DataAccessException
	 */
	List<Shop> getListNextChildShop(ShopFilter filter) throws BusinessException;
	
	/**
	 * @author vuongmq
	 * @date 21/08/2015
	 * @descriptip Lay danh sach shop cua NVGS quan ly
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<Shop> getListShopOfSupervise(RptStaffSaleFilter filter) throws BusinessException;
	
	/**
	 * Lay cac shop cha & anh em cua shop <b>shopId<b/>
	 * @author tuannd20
	 * @param shopId id cua shop bat dau
	 * @return Danh sach shop cha & shop anh em cua shop shopId
	 * @throws DataAccessException
	 * @since 07/08/2015
	 */
	List<Shop> getAncestorAndSiblingShops(Long shopId) throws BusinessException;
	
	/**
	 * Lay danh sach don vi duoc check & con chau cua no
	 * @author hunglm16
	 * @param keyShopId
	 * @param status
	 * @return
	 * @throws DataAccessException
	 * @since 17/11/2015
	 */
	List<Shop> getShopForConnectChildrenByKeyShop(Long keyShopId, Integer status) throws BusinessException;
	/**
	 * Cap nhat thong tin Ks_Shop_Target
	 * @author hunglm16
	 * @param keyShopId
	 * @param username
	 * @throws DataAccessException
	 * @since 19/11/2015
	 */
	void updateKSShopTagertByKeyShop(long keyShopId, String username) throws DataAccessException;
	/**
	 * Lay danh sach don vi duoc check & con chau cua no
	 * @author hunglm16
	 * @param keyShopId
	 * @param cycleId
	 * @param status
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return
	 * @throws DataAccessException
	 * @since 19/11/2015
	 */
	List<ShopVO> getShopForConnectChildrenByKeyShopVO(Long keyShopId, Long cycleId, Integer status, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;
	/**
	 * Lay danh sach don vi con chau, bo qua chinh no
	 * @author hunglm16
	 * @param arrShopId
	 * @return
	 * @throws BusinessException
	 * @since 20/11/2015
	 */
	List<Shop> getListChildShopId(Integer status, Long... arrShopId) throws BusinessException;
	
	/**
	 * Lay danh sach don vi cha, bo qua chinh no
	 * @author hunglm16
	 * @param arrShopId
	 * @return
	 * @throws BusinessException
	 * @since 20/11/2015
	 */
	List<Shop> getListParentShopId(Integer status, Long... arrShopId) throws BusinessException;
	
	/**
	 * lay danh sach shop cha (long id)
	 * @author vuongmq
	 * @date March 24, 2015
	 */
	List<Long> getListLongShopParentByShopId(Long shopId) throws BusinessException;
	
	/**
	 * lay danh sach shop con (long id)
	 * @author vuongmq
	 * @date March 24, 2015
	 */
	List<Long> getListLongShopChildByShopId(Long shopId) throws BusinessException;
}
package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.ShopType;
import ths.dms.core.exceptions.BusinessException;

public interface ShopTypeMgr {
	ShopType getShopTypeByCode(String code) throws BusinessException;
	List<ShopType> getListShopTypeByType(List<Integer> status) throws BusinessException;
}
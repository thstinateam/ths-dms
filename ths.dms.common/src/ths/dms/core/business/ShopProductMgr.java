package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.ShopProduct;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopProductFilter;
import ths.dms.core.entities.enumtype.ShopProductType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;


public interface ShopProductMgr {

	ShopProduct createShopProduct(ShopProduct shopProduct, LogInfoVO logInfo)
			throws BusinessException;

	void updateShopProduct(ShopProduct shopProduct, LogInfoVO logInfo) throws BusinessException;

	void deleteShopProduct(ShopProduct shopProduct, LogInfoVO logInfo) throws BusinessException;

	ShopProduct getShopProductById(Long id) throws BusinessException;

	Boolean isUsingByOthers(long shopProductId) throws BusinessException;

	/**
	 * tungtt sua
	 * @param kPaging
	 * @param shopCode
	 * @param shopName
	 * @param catId
	 * @param productCode
	 * @param status
	 * @param type
	 * @param parentShopId
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<ShopProduct> getListShopProduct(KPaging<ShopProduct> kPaging,
			String shopCode, String shopName, Long catId, String productCode,
			ActiveType status, ShopProductType type, List<Long> listOrgAccess) throws BusinessException;

	
	ShopProduct getShopProductByCat(Long shopId, Long catId)
			throws BusinessException;//Phu sua lai ten ham nhe

	ShopProduct getShopProductByProduct(Long shopId, Long productId)
			throws BusinessException;

	ShopProduct getShopProductByImport(String shopCode, String codeFlag, String calendarD, int flag) throws BusinessException;

	ObjectVO<ShopProduct> getListShopProduct1(KPaging<ShopProduct> kPaging,
			String shopCode, String shopName, List<String> listProInfoCode,
			String productCode, ActiveType status, ShopProductType type,
			List<Long> lstParentShopId, Boolean flag) throws BusinessException;

	void deteteListShopProduct(List<ShopProduct> lstData, LogInfoVO logInfo,
			ShopProductFilter filter) throws BusinessException;
}

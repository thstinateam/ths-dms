/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.Feedback;
import ths.dms.core.entities.FeedbackStaff;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.filter.FeedbackFilter;
import ths.dms.core.entities.vo.FeedbackVO;
import ths.dms.core.entities.vo.FeedbackVOSave;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

/**
 * FeedbackMgr theo doi van de
 * @author vuongmq
 * @since 11/11/2015 
 */
public interface FeedbackMgr {

	/**
	 * Danh sach Feedback
	 * @author vuongmq
	 * @param filter
	 * @return List<Feedback>
	 * @throws BusinessException
	 * @since 11/11/2015
	 */
	List<Feedback> getListFeedbackByFilter(FeedbackFilter filter) throws BusinessException;

	/**
	 * get DS FeedbackStaff
	 * @author vuongmq
	 * @param filter
	 * @return List<FeedbackStaff>
	 * @throws BusinessException
	 * @since 17/11/2015
	 */
	List<FeedbackStaff> getListFeedbackStaffByFilter(FeedbackFilter filter) throws BusinessException;
	
	/**
	 * Lay FeedbackVO
	 * @author vuongmq
	 * @param filter
	 * @return FeedbackVO
	 * @throws BusinessException
	 * @since 11/11/2015
	 */
	FeedbackVO getFeedBackVOByFilter(FeedbackFilter filter) throws BusinessException;

	/**
	 * Lay danh sach ObjectVO<FeedbackVO>
	 * @author vuongmq
	 * @param filter
	 * @return ObjectVO<FeedbackVO>
	 * @throws BusinessException
	 * @since 11/11/2015
	 */
	ObjectVO<FeedbackVO> getListFeedbackVOByFilter(FeedbackFilter filter) throws BusinessException;

	/**
	 * get danh sach item feedback
	 * @author vuongmq
	 * @param filter
	 * @return List<MediaItem>
	 * @throws BusinessException
	 * @since 17/11/2015
	 */
	List<MediaItem> getListMediaItemFeedbackFilter(FeedbackFilter filter) throws BusinessException;

	/**
	 * DS updateFeedback
	 * @author vuongmq
	 * @param lstFeedbackStaff
	 * @param type
	 * @param logInfoVO
	 * @throws BusinessException
	 * @since 17/11/2015
	 */
	void updateFeedback(List<FeedbackStaff> lstFeedbackStaff, Integer type, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Xu ly saveFeedback
	 * @author vuongmq
	 * @param feedbackVOSave
	 * @param logInfoVO
	 * @since Nov 23, 2015
	*/
	void saveFeedback(FeedbackVOSave feedbackVOSave, LogInfoVO logInfoVO) throws BusinessException;
	
	/**
	 * Xu ly getListFeedbackIdCheckByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<FeedbackVO>
	 * @throws BusinessException
	 * @since Dec 17, 2015
	*/
	List<FeedbackVO> getListFeedbackIdCheckByFilter(FeedbackFilter filter) throws BusinessException;
}

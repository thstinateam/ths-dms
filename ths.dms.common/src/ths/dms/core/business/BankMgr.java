package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.Bank;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.BankFilter;
import ths.dms.core.entities.vo.BankVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

public interface BankMgr {

	Bank createBank(Bank bank, LogInfoVO logInfo) throws BusinessException;

	void updateBank(Bank bank, LogInfoVO logInfo) throws BusinessException;

	void deleteBank(Bank bank, LogInfoVO logInfo) throws BusinessException;

	Bank getBankById(Long id) throws BusinessException;
	
	Bank getBankByCode(String codeBank) throws BusinessException;
	
	List<Bank> getListBankByShop(Long shopId) throws BusinessException;
	
	List<Bank>  getListBankByShopAscBankName(Long shopId) throws BusinessException;
	
	ObjectVO<BankVO> getListBank(KPaging<BankVO> kPaging, BankFilter filter) throws BusinessException;
	
	
}

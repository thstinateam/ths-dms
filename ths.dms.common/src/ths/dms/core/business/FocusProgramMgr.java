package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.FocusChannelMap;
import ths.dms.core.entities.FocusChannelMapProduct;
import ths.dms.core.entities.FocusProgram;
import ths.dms.core.entities.FocusShopMap;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.FocusProgramFilter;
import ths.dms.core.entities.enumtype.HTBHFilter;
import ths.dms.core.entities.enumtype.HTBHVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductCatVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface FocusProgramMgr {

	FocusProgram createFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo)
			throws BusinessException;
	/**
	 * Kiem tra khi cap nhat CTTT sang hoat dong
	 * @author thongnm
	 */
	String checkUpdateFocusProgramForActive(Long focusProgramId) throws BusinessException;
	void updateFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo) throws BusinessException;

	void deleteFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo) throws BusinessException;

	FocusProgram getFocusProgramById(Long id) throws BusinessException;

	FocusProgram getFocusProgramByCode(String code) throws BusinessException;

//	FocusChannelMapProduct createFocusChannelMapProduct(
//			FocusChannelMapProduct focusChannelMapProduct, LogInfoVO logInfo) throws BusinessException;

	void updateFocusChannelMapProduct(FocusChannelMapProduct focusChannelMapProduct, LogInfoVO logInfo)
			throws BusinessException;

	void deleteFocusChannelMapProduct(FocusChannelMapProduct focusChannelMapProduct, LogInfoVO logInfo)
			throws BusinessException;

	FocusChannelMapProduct getFocusChannelMapProductById(Long id)
			throws BusinessException;

	void updateFocusShopMap(FocusShopMap focusShopMap, LogInfoVO logInfo) throws BusinessException;

	/*void deleteFocusShopMap(FocusShopMap focusShopMap, LogInfoVO logInfo) throws BusinessException;*/

	FocusShopMap getFocusShopMapById(Long id) throws BusinessException;

	FocusChannelMap createFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo)
			throws BusinessException;

	void updateFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo)
			throws BusinessException;

	void deleteFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo)
			throws BusinessException;

	FocusChannelMap getFocusChannelMapById(Long id) throws BusinessException;
	
	FocusChannelMap getFocusChannelMapByCode(String code) throws BusinessException;

	Boolean isUsingByOthers(Long focusProgramId) throws BusinessException;

	Boolean isDetailUsingByOthers(Long focusChannelMapProductId)
			throws BusinessException;

	Boolean isFocusChannelMapUsingByOthers(Long focusChannelMapId)
			throws BusinessException;

	Boolean isFocusShopMapUsingByOthers(Long focusShopMapId)
			throws BusinessException;

	Shop getShopMapArea(long focusShopMapId) throws BusinessException;

	ObjectVO<FocusProgram> getListFocusProgram(KPaging<FocusProgram> kPaging,
			FocusProgramFilter filter, StaffRole staffRole)
			throws BusinessException;

	Boolean checkIfFocusShopMapExist(long focusProgramId, String shopCode,
			Long id) throws DataAccessException, BusinessException;

	Boolean checkIfFocusChannelMapExist(long focusProgramId,
			String channelTypeCode, Date fromDate, Date toDate, Long id)
			throws DataAccessException, BusinessException;

	Boolean checkIfProductInFocusProgram(Long focusProgramId, String productCode)
			throws BusinessException;

	/*Boolean createListFocusShopMap(FocusShopMap focusShopMap, LogInfoVO logInfo)
			throws BusinessException;*/
	
	/**
	 * 
	 * @author hieunq1
	 * @param productCode
	 * @param productName
	 * @param exFpId
	 * @return
	 * @throws BusinessException
	 */
	List<ProductCatVO> getListProductCatVO(String productCode,
			String productName, Long exFpId, Integer limit) throws BusinessException;
	
	Boolean createListFocusShopMapEx(Long focusProgramId, List<Long> lstShopId, ActiveType status,
			LogInfoVO logInfo) throws BusinessException;

	Boolean checkShopFamilyInFocusProgram(Long shopId, Long focusProgramId)
			throws BusinessException;

	FocusShopMap createFocusShopMap(FocusShopMap focusShopMap, LogInfoVO logInfo)
			throws BusinessException;

	Boolean checkShopAncestorInFocusProgram(Long focusProgramId, Long shopId)
			throws BusinessException;

	ObjectVO<FocusShopMap> getListFocusShopMap(KPaging<FocusShopMap> kPaging,
			Long focusProgramId, String shopCode, String shopName,
			ActiveType status, Long parentShopId) throws BusinessException;

	List<ProductCatVO> getListProductCatVOEx(String productCode,
			String productName, Long exFpId) throws BusinessException;

	List<ProductInfo> getListSubCat(String productCode, String productName,
			Long exFpId, Long catId) throws BusinessException;

	List<Product> getListProductBySubCat(String productCode,
			String productName, Long exFpId, Long catId, Long subCatId)
			throws BusinessException;

	void createListFocusChannelMapProduct(List<Long> lstProductId,
			Long focusProgramId, String saleTypeCode, LogInfoVO logInfo,
			String type) throws BusinessException;

	Boolean createFocusChannelMapProduct(long focusProgramId,
			String saleTypeCode, String code, ProductType type,
			LogInfoVO logInfo) throws BusinessException;

	Boolean checkIfFocusChannelMapProductExist(long focusProgramId,
			String saleTypeCode, String productCode, Long id)
			throws DataAccessException, BusinessException;

	ObjectVO<FocusChannelMap> getListFocusChannelMap(
			KPaging<FocusChannelMap> kPaging, Long focusProgramId,
			String saleTypeCode, ActiveType status) throws BusinessException;

	ObjectVO<FocusChannelMapProduct> getListFocusChannelMapProduct(
			KPaging<FocusChannelMapProduct> kPaging, String productCode, String productName,
			Long focusProgramId, String type) throws BusinessException;

	Boolean checkIfSaleTypeExistProduct(long focusProgramId,
			String saleTypeCode) throws BusinessException;

	Boolean checkIfAncestorInFp(Long shopId, Long focusProgramId)
			throws BusinessException;

	ObjectVO<FocusShopMap> getListFocusShopMapV2(KPaging<FocusShopMap> kPaging,
			String focusProgramCode, String focusProgramName, String shopCode,
			String shopName, Date fromDate, Date toDate, ActiveType status)
			throws BusinessException;

	ObjectVO<FocusChannelMapProduct> getListFocusChannelMapProductEx(
			KPaging<FocusChannelMapProduct> kPaging, String productCode,
			String productName, Long focusProgramId, Long focusChannelMapId,
			String type) throws BusinessException;

	Boolean checkExitsFocusProgramWithParentShop(Long focusProgramid,
			String shopCode) throws BusinessException;
	
	ObjectVO<HTBHVO> getListHTBHVO(HTBHFilter filter, KPaging<HTBHVO> kPaging)
			throws BusinessException;
	
	FocusProgram copyFocusProgram(Long focusProgramId, LogInfoVO logInfo,
			String fpCode, String fpName) throws BusinessException;

	Boolean createListFocusShopMap(List<FocusShopMap> list, LogInfoVO logInfo)
			throws BusinessException;

	void deleteFocusShopMap(Long shopId, Long focusProgramId, long shopRootId, LogInfoVO logInfo) throws BusinessException;

	void createListFocusChannelMap(List<FocusChannelMap> listUpdate,
			List<FocusChannelMap> listAdd, LogInfoVO logInfo)
			throws BusinessException;

	Boolean isExistChildJoinProgram(String shopCode, Long focusProgramId)
			throws BusinessException;

	/** kiem tra shop co shop con tham gia 1 chuong trinh trong tam nao chua
	 * 
	 * @param focusProgramId
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	Boolean hasChildShopJoinFocusProgram(Long focusProgramId, Long shopId)
			throws BusinessException;
	
	/**
	 * Kiem tra trung san pham chuong trinh trong tam theo bo: CTTT - HTBH - SP - loaiMHTT
	 * 
	 * @author lacnv1
	 * @since Jun 17, 2014
	 */
	boolean checkExistsProductMap(long focusId, String saleTypeCode, String prodCode, String type) throws BusinessException;
	
	/**
	 * Dem chuong trinh trong tam theo don vi tham gia
	 * @author hunglm16
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since 15/11/2015
	 */
	int countFocusProgram(FocusProgramFilter filter) throws BusinessException;
}
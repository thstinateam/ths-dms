package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.Organization;
import ths.dms.core.entities.ShopType;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrganizationFilter;
import ths.dms.core.entities.enumtype.OrganizationUnitTypeFilter;
import ths.dms.core.entities.filter.OrganizationSystemFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrganizationUnitTypeVO;
import ths.dms.core.entities.vo.OrganizationVO;
import ths.dms.core.exceptions.BusinessException;
/**
 * The Interface OrganizationMgr.
 * @author vuongmq
 * @date 11/02/2015
 */
public interface OrganizationMgr {

	/**BEGIN VUONGMQ*/
	
	List<Organization> getListOrganization(OrganizationSystemFilter<Organization> filter) throws BusinessException;
	
	List<Organization> getListOrganizationParent(OrganizationSystemFilter<Organization> filter) throws BusinessException;
	
	OrganizationVO getOrganizationVOById(Long Id) throws BusinessException;
	
	List<OrganizationVO> getListOrganizationSystemVO(OrganizationSystemFilter<OrganizationVO> filter) throws BusinessException;
	
	OrganizationVO getOrganizationVOByParent() throws BusinessException;
	
	List<OrganizationVO> getListOrganizationSystemVOShop(OrganizationSystemFilter<OrganizationVO> filter) throws BusinessException;
	
	List<OrganizationVO> getListOrganizationSystemVOStaff(OrganizationSystemFilter<OrganizationVO> filter) throws BusinessException;
	
	List<StaffType> getListStaffType(OrganizationSystemFilter<StaffType> filter) throws BusinessException;
	/**END VUONGMQ*/
	
	/** BEGIN PHUOCDH2      **/
	
	ShopType createShopType (ShopType shopType) throws BusinessException;
	
	StaffType createStaffType (StaffType staffType) throws BusinessException;
	
	Organization createOrganization(Organization organization) throws BusinessException ;
	
	void updateOrganization(Organization organization) throws BusinessException ;
	
	void updateListOrganization(List<Organization> lstOrganization) throws BusinessException ;
	
	
	void deleteOrganization(Organization organization) throws BusinessException;
	
	Organization getOrganizationById(Long id) throws BusinessException;

	Organization getOrganizationByCode(String code) throws BusinessException;
	
	void updateStaffType(StaffType staffType) throws BusinessException;
	
	StaffType getStaffTypeById(long id) throws BusinessException;
	
	Integer getCountByPrefixShop(String prefix) throws BusinessException ;
	
    Integer getCountByPrefixStaff(String prefix) throws BusinessException;
    /**
	 * kiem tra loai don vi, chuc vu nay da co tren node cha hay chua 
	 * @author phuocdh2
	 * @since 03/05/2015
	 * @description : nhan vao loai don vi ,chuc vu va kiem tra loai don vi chuc vu nay da co tren node cha chua
	 */
	public List<Organization> getListOrgExistParentNodeByFilter(OrganizationFilter filter)throws BusinessException;
    /**
	 * kiem tra loai don vi, chuc vu nay da co duoi node con hay chua 
	 * @author phuocdh2
	 * @since 03/05/2015
	 * @description : nhan vao loai don vi ,chuc vu va kiem tra loai don vi chuc vu nay da co duoi node con chua
	 */
	List<Organization> getListOrgExistSubNodeByFilter(OrganizationFilter filter)throws BusinessException;
	/** PHUOCDH2 **/
	ObjectVO<OrganizationUnitTypeVO> getListOrganizationUnitType(OrganizationUnitTypeFilter filter,KPaging<OrganizationUnitTypeVO> kPaging) throws BusinessException;
	/**
	 * get shop type by id
	 * @param id
	 * @return shop type
	 * @createDate 12/02/2015
	 * @author liemtpt
	 * @description:  lay shop type by id
	 */
	ShopType getShopTypeById(long id) throws BusinessException;
	/**
	 * update shop type 
	 * @param shopType
	 * @createDate 12/02/2015
	 * @author liemtpt
	 * @description:  cap nhat shop type
	 */
	void updateShopType(ShopType shopType) throws BusinessException;
	/**
	 * get list organization by filter
	 * @param Organization filter
	 * @return the list organization
	 * @createDate 12/02/2015
	 * @author liemtpt
	 * @description:  lay danh sach organization
	 */
	List<Organization> getListOrganizationByFilter(OrganizationFilter filter)
			throws BusinessException;
	/**
	 * choose organization type
	 * @param organization vo
	 * @return the list organization
	 * @createDate 12/02/2015
	 * @author liemtpt
	 * @description:  chon loai (don vi con, don vi ngang cap hay chuc vu) luu xuong organization
	 */
	void chooseOrganizationType(OrganizationUnitTypeVO vo, LogInfoVO logInfo)
			throws BusinessException;

	/***
	 * @author vuongmq
	 * @date 24/02/2015
	 * xu ly save order nodeOrdinal theo thu tu tren cay keo tha
	 */
	void saveOrganization(List<Long> lstOrgIdSort, List<Integer> lstOrgSortNodeOrdinal, LogInfoVO logInfo) 	throws BusinessException;
	/**
	 * get list organization by id for context menu
	 * @param organizationId
	 * @createDate 26/02/2015
	 * @author liemtpt
	 * @description:  lay danh sach organization tu orgid cho context menu cua cay don vi
	 */
	List<OrganizationVO> getListOrganizationById4ContextMenu(Long orgId)
			throws BusinessException;
	/**
	 * get list staff exits staff type id in organization
	 * @param organizationId
	 * @createDate 05/03/2015
	 * @author liemtpt
	 * @description: lay danh sach nhan vien co ton tai loai chuc vu cua nhan vien dang duoc chon 
	 */
	List<Staff> getListStaffExitsStaffTypeIdInOrganization(Long orgId, Long staffId) throws BusinessException;
	
	/**
	 * Lay organization tuong ung voi cac dieu kien trong staff_type, shop_type
	 * @author phuocdh2
	 * @since 03/26/2015
	 * @description : nhan vao staff_code, staff_name,staff.status tra ra organization ung voi staff_name  do  
	 */
	Organization getOrgJoinTypeByFilter(OrganizationFilter filter) throws BusinessException ;
	/**
	 * Lay StaffType tuong ung voi cac dieu kien 
	 * @author phuocdh2
	 * @since 03/26/2015
	 * @description : nhan vao staff_code,staff_type , lay ra StaffType
	 */
	StaffType getStaffTypeByCondition(OrganizationFilter filter) throws BusinessException ;
	/**
	 * Lay ShopType tuong ung voi cac dieu kien 
	 * @author phuocdh2
	 * @since 03/26/2015
	 * @description : nhan vao shop_code,shop_type , lay ra ShopType
	 */
	ShopType getShopTypeByCondition(OrganizationFilter filter) throws BusinessException;

	/**
	 * @author tungmt
	 * @since 9/6/2015
	 * Check 2 org co phai cha con nhau ko
	 */
	Boolean checkParentOrg(OrganizationFilter filter) throws BusinessException;
}

package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.CustomerAttribute;
import ths.dms.core.entities.CustomerAttributeDetail;
import ths.dms.core.entities.CustomerAttributeEnum;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;


/**
 * Quan ly Thuoc tinh khach hang
 * @author liemtpt
 * @since 22/01/2015
 *
 */
public interface CustomerAttributeMgr {

	/****
	    * create customer attribute
	    * @params customerAttirbute
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	CustomerAttribute createCustomerAttribute(CustomerAttribute attribute,
			LogInfoVO logInfo) throws BusinessException;
	/****
	    * delete customer attribute
	    * @params customerAttirbute
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void deleteCustomerAttribute(CustomerAttribute attribute)
			throws BusinessException;
	/****
	    * get customer attribute by id
	    * @params id
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	CustomerAttribute getCustomerAttributeById(long id) throws BusinessException;
	/****
	    * update customer attribute
	    * @params customerAttirbute
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void updateCustomerAttribute(CustomerAttribute attribute,LogInfoVO logInfo)
			throws BusinessException;
	/****
	    * Danh sach thuoc tinh dong cho khach hang
	    * @author liemtpt
	    * @paraAttributeDynamicFilterlter
	    * @param kPaging
	    * @return list customer attribute vo
	    * @throws DataAccessException
	    */
	ObjectVO<AttributeDynamicVO> getListCustomerAttributeVO(AttributeDynamicFilter filter, KPaging<AttributeDynamicVO> kPaging)
			throws BusinessException;
	/****
	    * save or update customer attribute
	    * @params attribute dynamic vo
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void saveOrUpdateCustomerAttribute(AttributeDynamicVO vo, LogInfoVO logInfo)
			throws BusinessException;
	/****
	    * save or update customer attribute enum
	    * @params attribute enum vo
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void saveOrUpdateCustomerAttributeEnum(AttributeEnumVO vo, LogInfoVO logInfo)
			throws BusinessException;
	/****
	    * check exist promotion by object id
	    * @params customerAttirbute
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	Boolean checkExistPromotion(Long objectId) throws BusinessException;
	/****
	    * delete customer attribute enum
	    * @params customer attirbute enum
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void deleteCustomerAttributeEnum(CustomerAttributeEnum attribute)
			throws BusinessException;
	/****
	    * get customer attribute enum by id
	    * @params id
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	CustomerAttributeEnum getCustomerAttributeEnumById(long id)
			throws BusinessException;
	/****
	    * update customer attribute enum
	    * @params customerAttirbuteEnum
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void updateCustomerAttributeEnum(CustomerAttributeEnum attribute,
			LogInfoVO logInfo) throws BusinessException;
	/****
	    * get list customer attribute by enum id
	    * @params id
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	List<CustomerAttributeDetail> getListCustomerAttributeByEnumId(long id)
			throws BusinessException;
	/****
	    * update customer attribute detail
	    * @params customerAttirbuteDetail
	    * @author liemtpt
	    * @throws DataAccessException
	    */
	void updateCustomerAttributeDetail(CustomerAttributeDetail attribute,
			LogInfoVO logInfo) throws BusinessException;

}

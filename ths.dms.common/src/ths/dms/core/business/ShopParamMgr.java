/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.business;

import java.util.List;

import ths.dms.core.entities.ProductShopMap;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ShopParamType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopParamVO;
import ths.dms.core.entities.vo.ShopParamVOProduct;
import ths.dms.core.entities.vo.ShopParamVOSave;
import ths.dms.core.exceptions.BusinessException;

/**
 * Mo ta class ShopParamMgr.java
 * @author vuongmq
 * @since Nov 28, 2015
 */
public interface ShopParamMgr {

	/**
	 * Xu ly createShopParam
	 * @author vuongmq
	 * @param shopParam
	 * @param logInfo
	 * @return ShopParam
	 * @throws BusinessException
	 * @since Nov 28, 2015
	*/
	ShopParam createShopParam(ShopParam shopParam, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Xu ly deleteShopParam
	 * @author vuongmq
	 * @param shopParam
	 * @param logInfo
	 * @throws BusinessException
	 * @since Nov 28, 2015
	*/
	void deleteShopParam(ShopParam shopParam, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Xu ly updateShopParam
	 * @author vuongmq
	 * @param shopParam
	 * @param logInfo
	 * @throws BusinessException
	 * @since Nov 28, 2015
	*/
	void updateShopParam(ShopParam shopParam, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Xu ly getShopParamById
	 * @author vuongmq
	 * @param id
	 * @return ShopParam
	 * @throws BusinessException
	 * @since Nov 28, 2015
	*/
	ShopParam getShopParamById(long id) throws BusinessException;

	/**
	 * Xu ly getShopParamByShopIdCode
	 * @author vuongmq
	 * @param shopId
	 * @param code
	 * @param activeType
	 * @return ShopParam
	 * @throws BusinessException
	 * @since Nov 28, 2015
	*/
	ShopParam getShopParamByShopIdCode(Long shopId, String code, ActiveType activeType) throws BusinessException;

	/**
	 * Xu ly getShopParam
	 * @author vuongmq
	 * @param shopId
	 * @param code
	 * @param type
	 * @param activeType
	 * @return ShopParam
	 * @throws BusinessException
	 * @since Nov 28, 2015
	*/
	ShopParam getShopParam(Long shopId, String code, ShopParamType type, ActiveType activeType) throws BusinessException;

	/**
	 * Xu ly saveInfo
	 * @author vuongmq
	 * @param shopParamVOSave
	 * @param logInfoVO
	 * @since Nov 30, 2015
	*/
	void saveInfo(ShopParamVOSave shopParamVOSave, LogInfoVO logInfoVO) throws BusinessException;
	
	/**
	 * Xu ly getListShopParamVOByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<ShopParamVO>
	 * @throws BusinessException
	 * @since Nov 30, 2015
	*/
	List<ShopParamVO> getListShopParamVOByFilter(BasicFilter<ShopParamVO> filter) throws BusinessException;

	/**
	 * Xu ly getListShopParamVOViewTimeByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<ShopParamVO>
	 * @throws BusinessException
	 * @since Nov 30, 2015
	*/
	List<ShopParamVO> getListShopParamVOViewTimeByFilter(BasicFilter<ShopParamVO> filter) throws BusinessException;
	
	/**
	 * Xu ly getListObjectShopParamVOByFilter
	 * @author vuongmq
	 * @param filter
	 * @return ObjectVO<ShopParamVO>
	 * @throws BusinessException
	 * @since Nov 30, 2015
	*/
	ObjectVO<ShopParamVO> getListObjectShopParamVOByFilter(BasicFilter<ShopParamVO> filter) throws BusinessException;

	/**
	 * Xu ly getListObjectShopParamVOViewTimeByFilter
	 * @author vuongmq
	 * @param filter
	 * @return ObjectVO<ShopParamVO>
	 * @throws BusinessException
	 * @since Nov 30, 2015
	*/
	ObjectVO<ShopParamVO> getListObjectShopParamVOViewTimeByFilter(BasicFilter<ShopParamVO> filter) throws BusinessException;
	
	/**
	 * Xu ly getListProductShopMapByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<ShopParamVOProduct>
	 * @throws BusinessException
	 * @since Dec 2, 2015
	 */
	List<ShopParamVOProduct> getListProductShopMapByFilter (BasicFilter<ShopParamVOProduct> filter) throws BusinessException;
	
	/**
	 * Xu ly getListObjectProductShopMapByFilter
	 * @author vuongmq
	 * @param filter
	 * @return ObjectVO<ShopParamVOProduct>
	 * @throws BusinessException
	 * @since Dec 2, 2015
	 */
	ObjectVO<ShopParamVOProduct> getListObjectProductShopMapByFilter(BasicFilter<ShopParamVOProduct> filter) throws BusinessException;
	
	/**
	 * Xu ly getProductShopMapByShopProductId
	 * @author vuongmq
	 * @param shopId
	 * @param productId
	 * @return ProductShopMap
	 * @throws BusinessException
	 * @since Dec 2, 2015
	 */
	ProductShopMap getProductShopMapByShopProductId(Long shopId, Long productId) throws BusinessException;
}

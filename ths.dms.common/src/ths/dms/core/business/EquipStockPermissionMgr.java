package ths.dms.core.business;

/**
 * Import thu vien
 * */
import java.util.List;

import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.EquipRoleDetail;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.filter.EquipmentRoleDetailFilter;
import ths.dms.core.entities.vo.EquipProposalBorrowVO;
import ths.dms.core.entities.vo.EquipmentRoleStockTempVO;
import ths.dms.core.entities.vo.EquipmentRoleVO;
import ths.dms.core.entities.vo.EquipmentRoleVOTempImport;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StockTransLotVO;
import ths.dms.core.exceptions.BusinessException;

/**
 * Xu ly du lieu
 * 
 * @author hoanv25
 * @since March 09,2015
 * @description Dung cho Quan ly quyen kho thiet bi
 * */
public interface EquipStockPermissionMgr {	
	/**
	 * Lay danh sach Quan ly quyen kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */	
	ObjectVO<EquipProposalBorrowVO> searchListStockPermissionVOByFilter(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter)throws BusinessException;
	/**
	 * Lay danh sach kho thiet bi theo don vi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */	
	ObjectVO<EquipProposalBorrowVO> searchStockByFilter(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter)throws BusinessException;
	/**
	 * Kiem tra xem ma quyen kho thiet bi da ton tai hay chua
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */	
	EquipRole getEquipRoleByCodeEx(String code, ActiveType status)throws BusinessException;
	/**
	 * Tao moi quyen kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */	
	EquipRole createStockPermission(EquipRole eq, LogInfoVO logInfoVO)throws BusinessException;
	/**
	 * Lay ma quyen kho thiet bi theo Id
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	EquipRole getStockPermissionById(Long id) throws BusinessException;
	/**
	 * Cap nhat quyen kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */	
	void saveStockPermission(EquipRole eq, LogInfoVO logInfoVO)throws BusinessException;
	/**
	 * Xoa quyen kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */		
	void deleteStockPermission(EquipRole eq)throws BusinessException;
	/**
	 *Lay danh sach kho theo id ma quyen
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */			
	ObjectVO<EquipProposalBorrowVO> getListtEquipStockByEquipRoleId(KPaging<EquipProposalBorrowVO> kPaging, Long id)throws BusinessException;
	/**
	 * Tim kiem Danh sach kho
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since March 09,2015
	 */
	ObjectVO<EquipProposalBorrowVO> searchListStockPermissionVOAdd(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter)throws BusinessException;
	/**
	 * Tao moi Danh sach kho detail
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since March 09,2015
	 */	
	void createListStockRoleDetail(EquipmentRoleDetailFilter<EquipmentRoleVO> filter, EquipRole eq) throws BusinessException;
	/**
	 * Lay ma quyen detail kho thiet bi theo Id
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	EquipRoleDetail getListEquipRoleDetailById(Long id) throws BusinessException;
	/**
	 * Xoa ma quyen detail kho thiet bi theo Id
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	void deleteEquipRoleDetail(EquipRoleDetail edt)throws BusinessException;
	/**
	 * update detail kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	void updateStockPermissionVODetail(Long idRole, List<Long> lstStockNow, List<Long> listMapEditIdStock, List<EquipmentRoleStockTempVO> checkListStockVO, List<EquipmentRoleStockTempVO> noCheckListStockVO, LogInfoVO logInfoVO) throws BusinessException;
	/**
	 * Lay danh sach theo id kho va id role
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	ObjectVO<EquipProposalBorrowVO> getListEquipRoleDetailByStockIdAndRoleId(Long idcheck, Long id)throws BusinessException;
	/**
	 * update detail kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	void updateEquipRoleDetail(EquipRoleDetail eqrd, LogInfoVO logInfoVO)throws BusinessException;
	/**
	 * Lay danh sach kho theo id kho 
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	EquipStock getEquipStockById(Long idcheck)throws BusinessException;
	/**
	 * create detail kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	void createEquipRoleDetail(EquipRoleDetail eRDetailNew)throws BusinessException;
	
	/***
	 * @author vuongmq
	 * @date Mar 23,2015
	 * @description lay danh sach them cho equip_role left join equip_role_detail de xuat excel quan lý quyen kho thiet bi
	 */
	ObjectVO<EquipProposalBorrowVO> searchListStockPermissionVOByFilterExport(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws BusinessException;

	/**
	 * tim kiem kho con cua kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	ObjectVO<EquipProposalBorrowVO> searchListStockChildPermissionEdit(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter)throws BusinessException;

	
	/***
	 * @author vuongmq
	 * @date Mar 25,2015
	 * @description them moi equip_role va equip_role_detail quan lý quyen kho thiet bi
	 */
	EquipRole createEquipRoleAndDetail(List<EquipmentRoleVOTempImport> lstRoleExcel, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * tim kiem kho co chong nhau khong
	 * 
	 * @author hoanv25
	 * @since March 26,2015
	 * */
	ObjectVO<EquipProposalBorrowVO> searchLstAddStockId(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter)throws BusinessException;
	/**
	 * Kiem tra kho da chon va kho moi chon co trung chom hay khong
	 * 
	 * @author hoanv25
	 * @since March 26,2015	
	 */
	ObjectVO<EquipProposalBorrowVO> searchCheckAddStockChild(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter)throws BusinessException;
	/**
	 * Kiem tra danh sach kho con
	 * 
	 * @author hoanv25
	 * @since March 26,2015
	 */
	ObjectVO<EquipProposalBorrowVO> checkParenIdStock(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter)throws BusinessException;
	/**
	 * Lay danh sach kho de copy
	 * 
	 * @author hoanv25
	 * @since March 26,2015
	 */
	ObjectVO<EquipProposalBorrowVO> getListEquipRoleDetailByEquipRoleCode(Long id)throws BusinessException;
	/**
	 * Kiem tra xem kho phan quyen co bi trung chom khi them moi hay khong
	 * 
	 * @author hoanv25
	 * @param listmapIdStock 
	 * @param listNoCheckStock 
	 * @since March 26,2015
	 */
	ObjectVO<EquipProposalBorrowVO> checkStockPermissionAdd(List<Long> listCheckStock, List<Long> listNoCheckStock, List<Long> listmapIdStock)throws BusinessException;
	/**
	 * Kiem tra xem kho phan quyen co bi trung chom khi chinh sua hay khong
	 * 
	 * @author hoanv25	
	 * @since March 26,2015
	 */
	ObjectVO<EquipProposalBorrowVO> checkStockPermissionEdit(List<Long> listMapEditIdStock, List<Long> listEditIdRoleStock, List<EquipmentRoleStockTempVO> checkListStockVO, List<EquipmentRoleStockTempVO> noCheckListStockVO)throws BusinessException;

	/**
	 * Check id equip role da duoc gan quyen chua
	 * 
	 * @param: id
	 * @return: lst id
	 * @author hoanv25
	 * @since March 26,2015
	 * @throws BusinessException
	 */
	ObjectVO<EquipProposalBorrowVO> listEquipRoleById(Long id) throws BusinessException;

	/**
	 * Ham lay shop_id cua shop cha nhap vao
	 * 
	 * @param: id
	 * @return: lst id
	 * @author hoanv25
	 * @since April 14,2015
	 * @throws BusinessException
	 */
	List<EquipProposalBorrowVO> searchCheckShopId(Long id) throws BusinessException;
}
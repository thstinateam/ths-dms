package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.RptCustomerProductSaleOrderVO;
import ths.core.entities.vo.rpt.RptOrderDetailProductVO;
import ths.core.entities.vo.rpt.RptPayDetailDisplayProgr1ParentVO;
import ths.core.entities.vo.rpt.RptPayDetailDisplayProgrVO;
import ths.core.entities.vo.rpt.RptProductDistrResultVO;
import ths.core.entities.vo.rpt.RptProductExchangeVO;
import ths.core.entities.vo.rpt.RptPromotionDetailStaffVO;
import ths.core.entities.vo.rpt.RptRealTotalSalePlanParentVO;
import ths.core.entities.vo.rpt.RptRevenueByDisplayProgramLevel;
import ths.core.entities.vo.rpt.RptRevenueInDate2VO;
import ths.core.entities.vo.rpt.RptSaleOrderVO;
import ths.core.entities.vo.rpt.RptStaffProgramPromotionVO;
import ths.dms.core.exceptions.BusinessException;

public interface SaleOrderReportMgr {

	/**
	 * 3.1.5.2	Bảng kê chi tiết đơn hàng theo từng mặt hàng
	 * @author hieunq1
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @param staffId
	 * @return
	 * @throws BusinessException
	 */
	RptOrderDetailProductVO getRptOrderDetailProductVO(Long shopId, String staffCode,
			Date fromDate, Date toDate) throws BusinessException;

	/**
	 * 3.1.5.6	Danh sách khách hàng theo mặt hàng
	 * @author hieunq1
	 * @param shopId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	RptCustomerProductSaleOrderVO getListRptCustomerProductSaleOrderVO(
			Long shopId, Long staffId, Date fromDate, Date toDate)
			throws BusinessException;

	/**
	 * 3.1.5.10	Báo cáo chi tiết chi trả hàng trưng bày
	 * @author hieunq1
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	RptPayDetailDisplayProgrVO getRptPayDetailDisplayProgrVO(
			Long shopId, String ppCode, Date fromDate, Date toDate) throws BusinessException;


	/**
	 * 3.1.5.14	Báo cáo chi tiết khuyến mại theo chương trình – nhân viên 
	 * @author hieunq1
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	RptPromotionDetailStaffVO getRptPromotionDetailStaffVO(
			Long shopId, String staffCode, Date fromDate, Date toDate) throws BusinessException;

	/**
	 * 3.1.5.18	Báo cáo kết quả phân phối theo mặt hàng
	 * @author hieunq1
	 * @param shopId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	RptProductDistrResultVO getRptProductDistrResultVO(Long shopId,
			String staffCode, Date fromDate, Date toDate) throws BusinessException;

	/**3.1.3.1.2 phieu xuat hang theo nhan vien giao hang
	 * @author hungnm -> thanhnn
	 * @param kPaging
	 * @param shopId
	 * @param deliveryStaffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<RptSaleOrderVO> getListRptSaleOrderVO(
			KPaging<RptSaleOrderVO> kPaging, Long shopId, Long deliveryStaffId,
			Date fromDate, Date toDate, Boolean getDeliveryStaff) throws BusinessException;
	
	
	List<SaleOrder> getListRptReturnSaleOrder(Long shopId, OrderType orderType,
			Date fromDate, Date toDate, Long deliveryId) throws BusinessException;


	/**32.	Lấy doanh thu bán hàng trong ngày theo NVBH
	 * @author hungnm
	 * @param fromDate
	 * @param toDate
	 * @param staffId
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	List<RptRevenueInDate2VO> getListRptRptRevenueInDate2VO(Date fromDate,
			Date toDate, Long staffId, Long shopId) throws BusinessException;
	/**
	 * 3.1.5.7 bao cao chi tieu doanh so
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param staffCode
	 * @return
	 * @throws BusinessException
	 */
	List<RptRealTotalSalePlanParentVO> getListRealTotalSalePlan(
			Long shopId, Integer month, Integer year, String staffCode)
			throws BusinessException;

	/**
	 * @author hungnm
	 * @param kPaging
	 * @param displayProgramId
	 * @param customerId
	 * @param shopId
	 * @param year
	 * @return
	 * @throws BusinessException
	 */
	List<RptRevenueByDisplayProgramLevel> getListRptRevenueByDisplayProgramLevel(
			KPaging<RptRevenueByDisplayProgramLevel> kPaging,
			Long displayProgramId, Long customerId, Long shopId, Integer year)
			throws BusinessException;
	
	/**
	 * 3.1.5.15 bao cao chuong trinh trung bay theo nhan vien chuong trinh
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	List<RptStaffProgramPromotionVO> getListRptStaffProgramPromotion(
			Long shopId, String staffCode, Date fromDate, Date toDate) throws BusinessException;
	
	/**
	 * 3.1.5.11 bao cao tong hop chi tiet trung bay
	 * @author thanhnguyen
	 * @param shopId
	 * @param displayProgramCode
	 * @return
	 * @throws BusinessException
	 */
	RptPayDetailDisplayProgr1ParentVO getRptGenaralDetailDisplayProgram (Long shopId, String displayProgramCode) throws BusinessException;

	/**
	 * @author hungnm
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @param programCode
	 * @param catId
	 * @return
	 * @throws BusinessException
	 */
	List<RptProductExchangeVO> getListRptProductExchangeVO(Date fromDate,
			Date toDate, Long shopId, String programCode, Long catId)
			throws BusinessException;
}

package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Bank;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Debit;
import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.PayReceived;
import ths.dms.core.entities.PaymentDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.DebitPaymentType;
import ths.dms.core.entities.enumtype.DebitType;
import ths.dms.core.entities.enumtype.DebtPaymentType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PayReceivedType;
import ths.dms.core.entities.enumtype.PaymentStatus;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.ReceiptType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.filter.DebitFilter;
import ths.dms.core.entities.vo.CustomerDebitVO;
import ths.dms.core.entities.vo.DebitCustomerParams;
import ths.dms.core.entities.vo.DebitCustomerVO;
import ths.dms.core.entities.vo.DebitObjectVO;
import ths.dms.core.entities.vo.DebitShopVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PayReceivedVO;
import ths.core.entities.vo.rpt.RptDebtOfCustomerLv01VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface DebitMgr {

	DebitDetail createDebitDetail(DebitDetail debitDetail) throws BusinessException;

	void updateDebitDetail(DebitDetail debitDetail) throws BusinessException;

	void deleteDebitDetail(DebitDetail debitDetail) throws BusinessException;

	DebitDetail getDebitDetailById(Long id) throws BusinessException;
	
	/**
	 * lay danh sach DebitDetail theo list id
	 * @author trietptm
	 * @param filter
	 * @return List<DebitDetail>
	 * @throws BusinessException
	 * @since Dec 28, 2015
	*/
	List<DebitDetail> getListDebitDetailByFilter(SoFilter filter) throws BusinessException;

	PaymentDetail createPaymentDetail(PaymentDetail paymentDetail) throws BusinessException;

	void updatePaymentDetail(PaymentDetail paymentDetail) throws BusinessException;

	void deletePaymentDetail(PaymentDetail paymentDetail) throws BusinessException;

	PaymentDetail getPaymentDetailById(Long id) throws BusinessException;

	void updatePayReceived(PayReceived payReceived) throws BusinessException;

	void deletePayReceived(PayReceived payReceived) throws BusinessException;

	PayReceived getPayReceivedById(Long id) throws BusinessException;

	/**
	 * @Thanh_toan_theo_lo: PaymentDetail.DEBT_PAYEMENT_TYPE=MANUAL
	 * @Thanh_toan_tu_dong: PaymentDetail.DEBT_PAYEMENT_TYPE=AUTO
	 * @Xoa_no_nho: PaymentDetail.DEBT_PAYEMENT_TYPE=REMOVE_SMALL_DEBT; tao moi
	 *              payReceieved voi payReceivedNumber="PT" + randomNumber;
	 *              amount = total detail
	 * */
	PayReceived createPayReceived(PayReceived payReceived, List<PaymentDetail> lstPaymentDetail) throws BusinessException;

	/**
	 * lay danh sach cong no cua NPP
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param invoiceNumber
	 * @return
	 * @throws DataAccessException
	 */
	List<DebitShopVO> getListDebitShopVO(Long shopId, PoType poType) throws BusinessException;

	BigDecimal getDebitOfShop(Long shopId) throws BusinessException;

	/**
	 * updateDebitOfShop
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param payReceivedNumber
	 * @param amount
	 * @param lstDebitId
	 * @param lstDebitAmt
	 * @param logShopInfo
	 * @throws BusinessException
	 */
	void updateDebitOfShop(long shopId, String payReceivedNumber, BigDecimal amount, List<Long> lstDebitId, List<BigDecimal> lstDebitAmt, ReceiptType receiptType, String user) throws BusinessException;

	/**
	 * Lay danh sach no nho
	 * 
	 * @param kPaging
	 * @param shortCode
	 * @param fromObjectId
	 * @param type
	 * @param fromAmount
	 * @param toAmount
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 * @author HungNM
	 */
	ObjectVO<DebitDetail> getListPositiveDebitDetail(KPaging<DebitDetail> kPaging, String shortCode, Long fromObjectId, DebitOwnerType type, BigDecimal fromAmount, BigDecimal toAmount, Long shopId) throws BusinessException;

	/**
	 * thanh toan cong no khach hang
	 * 
	 * @author phuongvm
	 * @param shopId
	 * @param customerId
	 * @param payReceivedNumber
	 * @param amount
	 * @param lstDebitId
	 * @param lstDebitAmt
	 * @param createUser
	 * @throws BusinessException
	 */
	void updateDebitOfCustomer(Long shopId, Long customerId, String payReceivedNumber, DebtPaymentType debtPaymentType, BigDecimal amount, List<Long> lstDebitId, List<BigDecimal> lstDebitAmt, List<BigDecimal> lstDiscount, String createUser,
			PayReceivedType payReceivedType, Long bankId, PaymentStatus paymentStatus) throws BusinessException;

	/**
	 * Lay du lieu cho Bao cao Tong cong no khach hang
	 * 
	 * @param shopId
	 * @param customerIds
	 * @param reportDate
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<RptDebtOfCustomerLv01VO> getDataForReportDebtOfCustomer(Long shopId, List<Long> customerIds, Date reportDate) throws BusinessException;

	Debit getDebit(Long ownerId, DebitOwnerType ownerType) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @return
	 * @throws BusinessException
	 */
	String getPayReceivedStringSugguest(Date lockDate) throws BusinessException;

	/**
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param staffCode
	 * @param custName
	 * @param shortCode
	 * @param orderNumber
	 * @param fromAmount
	 * @param toAmount
	 * @param isReveived
	 * @return
	 * @throws BusinessException
	 */
	BigDecimal getCustomerTotalDebitDetail(Long shopId, String staffCode, String custName, String shortCode, String orderNumber, BigDecimal fromAmount, BigDecimal toAmount, Boolean isReveived, Long deliveryId, Long cashierId, Date fromDate,
			Date toDate) throws BusinessException;

	/**
	 * Lay phieu thanh toan pay_received theo so chung tu
	 * 
	 * @param payreceivedNumber - so chung tu
	 * @param shopId - id npp
	 * 
	 * @author lacnv1
	 * @since May 20, 2015
	 */
	PayReceived getPayReceivedByNumber(String payreceivedNumber, Long shopId) throws BusinessException;

	/**
	 * Xem cong no khach hang Module 3.1.6.4
	 * 
	 * @param customerId
	 * @param customerCode
	 * @param customerName
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<DebitCustomerVO> getDebitCustomer(Long shopId, Long customerId, String customerCode, String customerName, Date fromDate, Date toDate, DebitPaymentType type) throws BusinessException;

	/**
	 * Lay thong tin cong no cua mot khach hang
	 * 
	 * @param shopId
	 * @param customerId
	 * @return
	 * @throws BusinessException
	 */
	DebitDetail getDebitCustomerRemain(Long customerId) throws BusinessException;

	void updateListDebitOfCustomer(List<DebitCustomerParams> lstDebitCustomerParams, PaymentStatus paymentStatus) throws BusinessException;

	/**
	 * Lay danh sach cong no KH
	 * 
	 * @modify by lacnv1 - them id NVBH
	 * @modify date Mar 26, 2014
	 */
	List<CustomerDebitVO> getListCustomerDebitVO(Long shopId, String shortCode, String orderNumber, BigDecimal amount, BigDecimal fromAmount, BigDecimal toAmount, Boolean isReveived, Date fromDate, Date toDate, Long nvghId, Long nvttId, Long nvbhId)
			throws BusinessException;
	/**
	 * Lay danh sach cong no KH theo filter
	 * 
	 * @param filter - doi tuong chua dieu kien loc du lieu
	 * 
	 * @author lacnv1
	 * @since Apr 08, 2015
	 * 
	 * @see getListCustomerDebitVO
	 */
	List<CustomerDebitVO> getListCustomerDebitVOByFilter(DebitFilter<CustomerDebitVO> filter) throws BusinessException;

	/**
	 * Lay tong cong no
	 * 
	 * @author lacnv1
	 * @since Mar 26, 2014
	 */
	BigDecimal getTotalDebit(Long ownerId, DebitOwnerType ownerType) throws BusinessException;

	/**
	 * Xem cong no khach hang Module 3.1.6.4
	 * 
	 * @author lacnv1
	 * @since Mar 27, 2014
	 * @see getDebitCustomer
	 */
	ObjectVO<DebitCustomerVO> getListDebitCustomerVO(DebitFilter<DebitCustomerVO> filter) throws BusinessException;

	/**
	 * Lay no cua NPP voi VNM
	 * 
	 * @author lacnv1
	 * @since Apr 07, 2014
	 */
	List<DebitShopVO> getDebitVNMOfShop(Long shopId, Long typeInvoice) throws BusinessException;
	List<DebitShopVO> getDebitVNMOfShopAndId(Long shopId, List<Long> listDebitDetailId) throws BusinessException;

	/**
	 * Tao phieu dieu chinh tang no
	 * 
	 * @author lacnv1
	 * @since Apr 07, 2014
	 */
	void createDebitIncrement(Long shopId, String number, BigDecimal amount, Customer customer, String reason, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Lay cong no hien tai cua NPP
	 * 
	 * @author tungtt21
	 * @param shopId
	 * @param ownerType
	 * @param debitType
	 * @return
	 * @throws BusinessException
	 */
	BigDecimal getCurrentDebit(Long objectId, DebitOwnerType ownerType, DebitType debitType) throws BusinessException;

	void updateListCashierIdSaleOder(List<Long> lstFromObject, List<String> lstStaffCode, String user, Long shopId) throws BusinessException;

	List<CustomerDebitVO> getListCustomerDebitVOEx(Long shopId, String shortCode, String orderNumber, BigDecimal amount, BigDecimal fromAmount, BigDecimal toAmount, Boolean isReveived, Date fromDate, Date toDate, Long nvghId, Long nvttId,
			Long nvbhId, List<Long> lstDebitId) throws BusinessException;

	/**
	 * Tao phieu dieu chinh tang no
	 * 
	 * @author lacnv1
	 * @since Aug 28, 2014
	 */
	void createListDebitIncrement(List<DebitObjectVO> lstDebit, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Check so phieu dieu chinh cong no da ton tai chua
	 * 
	 * @author lacnv1
	 * @since Sep 04, 2014
	 */
	boolean existsDebitNumber(long shopId, String number) throws BusinessException;

	List<Bank> getListBankByShop(Long shopId, ActiveType status) throws BusinessException;

	Bank getBankByCodeAndShop(Long shopId, String Code) throws BusinessException;

	/**
	 * Luu phieu thanh toan o trang thai chua thanh toan, xuat du lieu ra phieu in
	 * 
	 * @author lacnv1
	 * @since Mar 23, 2015
	 */
	PayReceived exportPayment(DebitCustomerParams payment, LogInfoVO logInfo) throws BusinessException;
	
	/**
	 * Lay tong tien thanh toan cho debit_detail
	 * 
	 * @author lacnv1
	 * @since Mar 23, 2015
	 */
	BigDecimal getPaymentAmountOfDebitDetail(long debitDetailId, PaymentStatus paymentStatus) throws BusinessException;
	
	/**
	 * Lay thong tin pay_received theo id de xuat phieu
	 * 
	 * @author lacnv1
	 * @since Mar 20, 2015
	 */
	PayReceivedVO getPayReceivedVOForExport(long payId) throws BusinessException;
	
	/**
	 * Lay thong tin chi tiet phieu xuat
	 * 
	 * @author lacnv1
	 * @since Mar 24, 2015
	 */
	List<CustomerDebitVO> getCustomerDebitOfPayReceived(long shopId, long payId) throws BusinessException;
	
	/**
	 * Lay debit_detail dua theo from_object_number, shop_id
	 * 
	 * @author lacnv1
	 * @since Mar 25, 2015
	 */
	DebitDetail getDebitDetailByObjectNumber(long shopId, String fromObjectNumber, Date pDate) throws BusinessException;
	
	/**
	 * Cap nhat NVTT cho don hang
	 * 
	 * @author nhutnn
	 * @param filter
	 * @throws BusinessException
	 * @since 17/11/2015
	 */
	void updateListSaleOrderInDebitDetail(DebitFilter<CustomerDebitVO> filter) throws BusinessException;

	/**
	 * cap nhat NVTT cho list DebitDetail
	 * @author trietptm
	 * @param filter
	 * @throws BusinessException
	 * @since Dec 28, 2015
	*/
	void updateCashierForListDebitDetail(DebitFilter<CustomerDebitVO> filter) throws BusinessException;
}

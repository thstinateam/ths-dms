/*
 * 
 */
package ths.dms.core.business;

import vn.kunkun.vietbando.LatLng;
import ths.dms.core.entities.RequestLog;
import ths.dms.core.exceptions.BusinessException;

// TODO: Auto-generated Javadoc
/**
 * The Interface AuthenticationMgr.
 */
public interface VietBanDoMgr {

	LatLng fromStreetToLatLng(String sStreetName, RequestLog requestLog)
			throws BusinessException;

}

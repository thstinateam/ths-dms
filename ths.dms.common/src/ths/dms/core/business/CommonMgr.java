package ths.dms.core.business;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.core.entities.vo.rpt.RptKD_General_VO;
import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.EquipLender;
import ths.dms.core.entities.ImportFileData;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ArithmeticEnum;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PromotionProgramVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface CommonMgr {

	Date getSysDate() throws BusinessException;

	Date getYesterDay() throws BusinessException;

	RptKD_General_VO generalDateBySalesHO(Date fromDate, Date toDate) throws BusinessException;

	Date getDateBySysdateForNumberDay(Integer numberDay, boolean trunc) throws BusinessException;

	BigDecimal checkValidDateDate(String dateStr) throws BusinessException;

	ShopParam getShopParamByType(Long shopId, String type) throws BusinessException;

	void updateShopParam(ShopParam shopParam) throws BusinessException;

	ShopParam createShopParam(ShopParam shopParam) throws BusinessException;

	Integer getIslevel(Long shopId) throws BusinessException;
	
	

	/**
	 * Tao moi entity cho bat ky loai doi tuong nao
	 * 
	 * @author tientv11
	 * @param <T>
	 * @param object
	 * @return
	 * @throws BusinessException
	 */
	<T> T createEntity(T object) throws BusinessException;

	/**
	 * Tao moi List Entity cho bat ky loai doi tuong nao
	 * 
	 * @author tientv11
	 * @param <T>
	 * @param lstObjects
	 * @return
	 * @throws BusinessException
	 */
	<T> List<T> creatListEntity(List<T> lstObjects) throws BusinessException;

	/**
	 * Update Entity bat ky loai doi tuong nao
	 * 
	 * @author tientv11
	 * @param object
	 * @throws BusinessException
	 */
	void updateEntity(Object object) throws BusinessException;

	/**
	 * Update list entity bat ky loai doi tuong nao
	 * 
	 * @author tientv11
	 * @param <T>
	 * @param lstObjects
	 * @return
	 * @throws BusinessException
	 */
	<T> List<T> updateListEntity(List<T> lstObjects) throws BusinessException;

	/**
	 * Lay doi tuong theo id
	 * 
	 * @author tientv11
	 * @param <T>
	 * @param clazz
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	<T> T getEntityById(Class<T> clazz, Serializable id) throws BusinessException;

	/**
	 * Xoa doi tuong
	 * 
	 * @author tientv11
	 * @param object
	 * @throws BusinessException
	 */
	void deleteEntity(Object object) throws BusinessException;

	/**
	 * Kiem tra tinh dung dan du lieu voi Staff
	 * 
	 * @author hunglm16
	 * @since October 6 ,2014
	 * */
	boolean checkStaffInCMSByFilterWithCMS(BasicFilter<BasicVO> filter) throws BusinessException;

	/**
	 * Lay doi tuong la sanh sach nhan vien theo phan quyen CMS
	 * 
	 * @author hunglm16
	 * @since October 6 ,2014
	 * 
	 * @param getInheritUserPriv
	 *            - StaffRoot.getId, filter.getLstShopId(), Neu
	 *            currentUser.getListUser().size() >0 =>
	 *            filter.getIsLstChildStaffRoot() = true, Nguoc lai
	 *            filter.getIsLstChildStaffRoot() = false Do la nhung tham so
	 *            bat buoc
	 * */
	List<StaffVO> getListStaffVOFullChilrentByCMS(StaffPrsmFilter<StaffVO> filter) throws BusinessException;

	/**
	 * Lay danh sach ChannelType theo Type va Danh sach ObjectType
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * @description ObjectType = null thi lay tat ca cac ChannelType theo Type
	 * */
	List<ChannelType> getListChannelTypeByTypeAndArrObjType(Integer type, Integer[] objectType) throws BusinessException;
	
	/**
	 * Lay cau hinh shop_param theo phan cap, cap con khong co thi lay len cap cha
	 * 
	 * @author lacnv1
	 * @since Mar 16, 2015
	 */
	ShopParam getShopParamEx(long shopId, String type) throws BusinessException;

	ObjectVO<ImportFileData> getListImportFileData(
			KPaging<ImportFileData> kPaging, Long staffId,
			List<Integer> listStatus, String serverId) throws BusinessException;

	void updateImportFileData(ImportFileData importFileData, LogInfoVO logInfo)
			throws BusinessException;

	ImportFileData createImportFileData(ImportFileData importFileData,
			LogInfoVO logInfo) throws BusinessException;

	ImportFileData getImportFileData(String fileName, Long staffId)
			throws BusinessException;

	ObjectVO<Staff> getListStaffImportFile(KPaging<Staff> kPaging,
			List<Integer> listStatus, String serverId) throws BusinessException;
	
	/**
	 * Lay danh sach CTKM theo don vi phan quyen
	 * 
	 * @author hunglm16
	 * @param status
	 * @param shopId
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return
	 * @throws DataAccessException
	 * @since 19/09/2015
	 */
	List<PromotionProgramVO> getListVOPromotionProgramByShop(Integer status, Long shopId, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;

	/**
	 * So sanh 2 ngay
	 * @param date1
	 * @param date2
	 * @return 0 bang nhau
	 * @return -1 date1 nho hon date2
	 * @return 1 date1 lon hon date2
	 * @throws BusinessException
	 */
	Integer compareDateWithoutTime(Date date1, Date date2) throws BusinessException;
	
	/**
	 * Thuc hien phep tru hoac cong 2 ngay
	 * 
	 * @author hunglm16
	 * @param date1
	 * @param date2
	 * @param arithmetic cac phep tinh toan hoc
	 * @return gia tri theo phep toan
	 * @throws DataAccessException
	 * @since 29/10/2015 
	 */
	int arithmeticForDateByDateWithTrunc(Date date1, Date date2, ArithmeticEnum arithmetic) throws BusinessException;

	/**
	 * Lay danh sach nam
	 * @author hunglm16
	 * @return
	 * @throws BusinessException
	 * @since 09/11/2015
	 */
	List<BasicVO> getListYearSysConfig() throws BusinessException;
	
	/**
	 * Lay ky dong co ngay den lon nhat cua kenh thiet bi
	 * 
	 * @author hunglm16
	 * @since Jun 07,2015
	 * */
	Long getEquipPeriodIdByCloseForMaxToDate() throws BusinessException;
	
	/**
	 * Lay danh sach Equip_Lender
	 * 
	 * @author hunglm16
	 * 
	 * @param [shopId]: id Don vi - Khong duoc de rong
	 * @since August 18, 2015
	 * */
	EquipLender getFirtInformationEquipLender(Long shopId) throws BusinessException;
}

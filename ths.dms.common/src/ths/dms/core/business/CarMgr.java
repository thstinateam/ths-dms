package ths.dms.core.business;

import ths.dms.core.entities.Car;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.CarSOFilter;
import ths.dms.core.entities.vo.CarSOVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

public interface CarMgr {

	Car createCar(Car car, LogInfoVO logInfo) throws BusinessException;

	void updateCar(Car car, LogInfoVO logInfo) throws BusinessException;

	void deleteCar(Car car, LogInfoVO logInfo) throws BusinessException;
	
	Car getCarByNumberAndListShop(String carNumber, String lstShopId)	throws BusinessException;

	Boolean isUsingByOthers(long carId) throws BusinessException;

	Car getCarById(Long id) throws BusinessException;
	
	ObjectVO<Car> getListCar(KPaging<Car> kPaging, Long shopId, String type,
			String carNumber, Long labelId, Float gross, String category,
			String origin, ActiveType status, Boolean getCarInChildShop)
			throws BusinessException;
	
	Boolean checkIfRecordExists(String carNumber, Long id) throws BusinessException;

	Car getCarByNumberAndShopId(String carNumber, Long shopId) throws BusinessException;
	
	/**
	 * Lay ds xe
	 * 
	 * @author lacnv1
	 * @since Sep 22, 2014
	 */
	ObjectVO<CarSOVO> getListCar(CarSOFilter filter) throws BusinessException;
}

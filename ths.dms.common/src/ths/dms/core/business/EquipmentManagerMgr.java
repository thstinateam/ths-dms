package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipDeliveryRecDtl;
import ths.dms.core.entities.EquipDeliveryRecord;
import ths.dms.core.entities.EquipEvictionForm;
import ths.dms.core.entities.EquipEvictionFormDtl;
import ths.dms.core.entities.EquipFormHistory;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipGroupProduct;
import ths.dms.core.entities.EquipItem;
import ths.dms.core.entities.EquipItemConfig;
import ths.dms.core.entities.EquipItemConfigDtl;
import ths.dms.core.entities.EquipLend;
import ths.dms.core.entities.EquipLendDetail;
import ths.dms.core.entities.EquipLiquidationForm;
import ths.dms.core.entities.EquipLostRecord;
import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.EquipProvider;
import ths.dms.core.entities.EquipRepairForm;
import ths.dms.core.entities.EquipRepairFormDtl;
import ths.dms.core.entities.EquipRepairPayForm;
import ths.dms.core.entities.EquipRepairPayFormDtl;
import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.EquipRoleUser;
import ths.dms.core.entities.EquipSalePlan;
import ths.dms.core.entities.EquipStatisticRecord;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.EquipStockTotal;
import ths.dms.core.entities.EquipStockTransForm;
import ths.dms.core.entities.EquipStockTransFormDtl;
import ths.dms.core.entities.EquipSuggestEviction;
import ths.dms.core.entities.EquipSuggestEvictionDTL;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.EquipGFilter;
import ths.dms.core.entities.enumtype.EquipStockEquipFilter;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.EquipmentRepairFormFilter;
import ths.dms.core.entities.enumtype.EquipmentStatisticRecordStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.EquipItemConfigDtlFilter;
import ths.dms.core.entities.filter.EquipItemConfigFilter;
import ths.dms.core.entities.filter.EquipItemFilter;
import ths.dms.core.entities.filter.EquipPermissionFilter;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipRepairFilter;
import ths.dms.core.entities.filter.EquipRoleFilter;
import ths.dms.core.entities.filter.EquipStaffFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.filter.EquipSuggestEvictionFilter;
import ths.dms.core.entities.filter.EquipmentEvictionFilter;
import ths.dms.core.entities.filter.EquipmentGroupProductFilter;
import ths.dms.core.entities.filter.EquipmentSalePlaneFilter;
import ths.dms.core.entities.vo.EquipBorrowVO;
import ths.dms.core.entities.vo.EquipItemConfigDtlVO;
import ths.dms.core.entities.vo.EquipItemConfigVO;
import ths.dms.core.entities.vo.EquipItemVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipRepairFormVOList;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryPrintVO;
import ths.dms.core.entities.vo.EquipmentDeliveryReturnVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentEvictionVO;
import ths.dms.core.entities.vo.EquipmentEvictionVOs;
import ths.dms.core.entities.vo.EquipmentExVO;
import ths.dms.core.entities.vo.EquipmentGroupProductVO;
import ths.dms.core.entities.vo.EquipmentGroupVO;
import ths.dms.core.entities.vo.EquipmentLiquidationFormVO;
import ths.dms.core.entities.vo.EquipmentManagerVO;
import ths.dms.core.entities.vo.EquipmentPermissionVO;
import ths.dms.core.entities.vo.EquipmentRecordDeliveryVO;
import ths.dms.core.entities.vo.EquipmentRepairFormDtlVO;
import ths.dms.core.entities.vo.EquipmentRepairFormVO;
import ths.dms.core.entities.vo.EquipmentRepairPaymentRecord;
import ths.dms.core.entities.vo.EquipmentStaffVO;
import ths.dms.core.entities.vo.EquipmentStatisticCheckingRecordExportVO;
import ths.dms.core.entities.vo.EquipmentStockDeliveryVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductTreeVO;
import ths.dms.core.entities.vo.ShopViewParentVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Quan ly thiet bi Mgr
 * 
 * @author hunglm16
 * @since December 14,2014
 * */
public interface EquipmentManagerMgr {
	/**
	 * Them moi thong tin thiet bi
	 * 
	 * @author hunglm16
	 * @since December 14,2014
	 * */
	Equipment createEquipment(Equipment equipment) throws BusinessException;

	/**
	 * Cap nhat thong tin thiet bi
	 * 
	 * @author hunglm16
	 * @since December 14,2014
	 * */
	void updateEquipment(Equipment equipment) throws BusinessException;

	/**
	 * Tim kiem danh muc thiet bi
	 * 
	 * @author hoanv25
	 * @since December 15,2014
	 * */
	ObjectVO<EquipmentVO> getListEquipmentGeneral(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Creat EquipCategory
	 * 
	 * @author hoanv25
	 * @since December 15,2014
	 * */
	EquipCategory createEquipCategoryManagerCatalog(EquipCategory equipCategory, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Creat EquipProvider
	 * 
	 * @author hoanv25
	 * @since December 15,2014
	 * */
	EquipProvider createEquipProviderManagerCatalog(EquipProvider equipProvider, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Creat EquipItem
	 * 
	 * @author hoanv25
	 * @since December 15,2014
	 * */
	EquipItem createEquipItemManagerCatalog(EquipItem equipItem, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Update equipment manager catalog
	 * 
	 * @author hoanv25
	 * @since December 15,2014
	 * */
	EquipCategory updateEquipCategoryManagerCatalog(EquipCategory equipCategory, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Update EquipProvider
	 * 
	 * @author hoanv25
	 * @since December 15,2014
	 * */
	EquipProvider updateEquipProviderManagerCatalog(EquipProvider equipProvider, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Update EquipItem
	 * 
	 * @author hoanv25
	 * @since December 15,2014
	 * */
	EquipItem updateEquipItemManagerCatalog(EquipItem equipItem, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Lay EquipCategory theo Id
	 * 
	 * @author hoanv25
	 * @since December 16,2014
	 * */
	EquipCategory getEquipCategoryById(Long id) throws BusinessException;

	/**
	 * Lay EquipItem theo Id
	 * 
	 * @author hoanv25
	 * @since December 16,2014
	 * */
	EquipItem getEquipItemById(Long id) throws BusinessException;

	/**
	 * Lay EquipProvider theo Id
	 * 
	 * @author hoanv25
	 * @since December 16,2014
	 * */
	EquipProvider getEquipProviderById(Long id) throws BusinessException;
	
	/**
	 * Lay EquipProvider theo Code
	 * 
	 * @author tamvnm
	 * @since 26/06/2015
	 * */
	EquipProvider getEquipProviderByCode(String code) throws BusinessException;

	/**
	 * Lay danh sach thu hoi va thanh ly
	 * 
	 * @author phuongvm
	 * @since 16/12/2014
	 * */
	ObjectVO<EquipmentEvictionVO> getListEquipmentEviction(EquipmentEvictionFilter filter) throws BusinessException;

	/**
	 * Lay danh sach lich su cac phieu
	 * 
	 * @author phuongvm
	 * @since 16/12/2014
	 * */
	List<EquipmentEvictionVO> getHistory(Long idRecord, EquipTradeType recordType) throws BusinessException;

	/**
	 * Lay danh sach bien ban giao nhan
	 * 
	 * @author nhutnn
	 * @since 15/12/2014
	 * @param kPaging
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentRecordDeliveryVO> getListEquipmentRecordDeliveryVOByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter) throws BusinessException;

	/**
	 * Lay danh sach cac thiet bi ban giao
	 * 
	 * @author nhutnn
	 * @since 15/12/2014
	 * @param kPaging
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentDeliveryVO> getListEquipmentDeliveryVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;

	/**
	 * Lay equipment Entity
	 * 
	 * @author nhutnn
	 * @since 16/12/2014
	 * @param code
	 * @return
	 * @throws BusinessException
	 */
	Equipment getEquipmentEntityByCode(String code) throws BusinessException;

	/**
	 * Lay ky hien tai
	 * 
	 * @author nhutnn
	 * @since 16/12/2014
	 * @return
	 * @throws BusinessException
	 */
	EquipPeriod getEquipPeriodCurrent() throws BusinessException;
	
	/**
	 * Lay ky dang mo co ngay thuoc d
	 * 
	 * @author tamvnm
	 * @since 30/06/2015
	 * @return
	 * @throws BusinessException
	 */
	List<EquipPeriod> getListEquipPeriodByDate(Date d) throws BusinessException;

	/**
	 * Tao moi bien ban giao nhan
	 * 
	 * @author nhutnn
	 * @since 18/12/2014
	 * @param eqDeliveryRecord
	 * @param lstDeliveryRecDtls
	 * @return
	 * @throws DataAccessException
	 */
	EquipmentDeliveryReturnVO createRecordDeliveryByImportExcel(EquipDeliveryRecord eqDeliveryRecord, List<EquipDeliveryRecDtl> lstDeliveryRecDtls, EquipRecordFilter<EquipmentDeliveryVO> filter) throws BusinessException;

	/**
	 * Lay kho thiet bi
	 * 
	 * @author nhutnn
	 * @since 16/12/2014
	 * @param stockId
	 * @param stockType
	 * @return
	 * @throws BusinessException
	 */
	EquipStockTotal getEquipStockTotalById(Long stockId, EquipStockTotalType stockType, Long equipGroupId) throws BusinessException;

	/**
	 * Chinh sua kho thiet bi
	 * 
	 * @author nhutnn
	 * @since 16/12/2014
	 * @param eqDeliveryRecord
	 * @throws BusinessException
	 */
	void updateEquipmentStockTotal(EquipStockTotal eqDeliveryRecord) throws BusinessException;

	/**
	 * Lay getEquipGroupById theo Id
	 * 
	 * @author hoanv25
	 * @since December 17,2014
	 * */
	EquipGroup getEquipGroupById(Long id) throws BusinessException;

	/**
	 * Lay danh sach hieu thiet bi
	 * 
	 * @author hoanv25
	 * @since December 17,2014
	 * */
	/*
	 * List<EquipmentVO> getListEquipmentBrand(EquipmentFilter<EquipmentVO>
	 * filter) throws BusinessException;
	 */
	/**
	 * Lay danh sach ky quan ly thiet bj
	 * 
	 * @author tientv11
	 * @sine 17/12/2014
	 */
	ObjectVO<EquipPeriod> getListEquipPeriodByFilter(EquipmentFilter<EquipPeriod> filter) throws BusinessException;

	/**
	 * Lay phieu thu thoi thanh toan
	 * 
	 * @author phuongvm
	 * @since 17/12/2014
	 * */
	EquipEvictionForm getEquipEvictionFormById(long id) throws BusinessException;

	/**
	 * tao phieu thu thoi thanh toan
	 * 
	 * @author phuongvm
	 * @since 17/12/2014
	 * */
	EquipEvictionForm createEquipEvictionForm(EquipEvictionForm equipEvictionForm) throws BusinessException;

	/**
	 * cap nhat phieu thu hoi thanh toan
	 * 
	 * @author phuongvm
	 * @throws BusinessException
	 * @since 17/12/2014
	 * */
	void updateEquipEvictionFormStatus(EquipEvictionForm equipEvictionForm, String username, int status, List<EquipSuggestEvictionDTL> lstequipSug) throws BusinessException;

	/**
	 * cap nhat dong chi tiet bien ban de nghi thu hoi
	 * 
	 * @author tamvnm
	 * @throws BusinessException
	 * @since 27/07/2015
	 * */
	void updateStatusEquipSugEvicDTL(EquipSuggestEvictionDTL equipSug) throws BusinessException;

	/**
	 * Them lich su bien ban
	 * 
	 * @author nhutnn
	 * @since 17/12/2014
	 */
	EquipFormHistory createEquipmentHistoryRecord(EquipFormHistory equipFormHistory) throws BusinessException;

	/**
	 * Lay danh sach hieu thiet bi
	 * 
	 * @author hoanv25
	 * @since December 17,2014
	 * */
	List<EquipmentVO> getListEquipmentCategory(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay danh sach bien ban giao nhan cho xuat excel
	 * 
	 * @author nhutnn
	 * @since 17/12/2014
	 * @param equipmentFilter
	 * @return
	 * @throws BusinessException
	 */
	List<EquipmentRecordDeliveryVO> getListEquipmentRecordDeliveryForExport(EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter) throws BusinessException;

	/**
	 * Lay danh sach nhom thiet bi
	 * 
	 * @author hoanv25
	 * @since December 17,2014
	 * */
	ObjectVO<EquipmentVO> searchEquipmentGroup(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay danh sach thiet bi theo tham so Filter
	 * 
	 * @author hoanv25
	 * @since December 16,2014
	 * */
	ObjectVO<EquipmentVO> getListEquipmentByFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay danh sach loai thiet bi theo Filter
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	List<EquipCategory> getListEquipmentCategoryByFilter(EquipmentFilter<EquipCategory> filter) throws BusinessException;

	/**
	 * Lay danh sach NCC thiet bi theo Filter
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	List<EquipProvider> getListEquipProviderByFilter(EquipmentFilter<EquipProvider> filter) throws BusinessException;

	/**
	 * Tim kiem lich su thiet bi theo Filter
	 * 
	 * @author hunglm16
	 * @since December 19,2014
	 * */
	ObjectVO<EquipmentRepairFormVO> searchListEquipmentRepairFormVOByFilter(EquipmentRepairFormFilter<EquipmentRepairFormVO> filter) throws BusinessException;

	/**
	 * Lay danh sach kho thiet bi
	 * 
	 * @author phuongvm
	 * @since 18/12/2014
	 * */
	List<EquipStockTotal> getListEquipStockTotal(Long stockId, EquipStockTotalType stockType, Long equipGroupId) throws BusinessException;

	/**
	 * Tạo biên bản thu hồi
	 * 
	 * @author phuongvm
	 * @since 18/12/2014
	 * */
	EquipEvictionForm createEquipEvictionFormX(EquipEvictionForm equipEvictionForm, List<Equipment> lstEquipment, EquipRecordFilter<EquipRepairForm> filter) throws BusinessException;

	/**
	 * Lay danh sach nha cung cap
	 * 
	 * @author hoanv25
	 * @since December 17,2014
	 * */
	List<EquipmentVO> getListEquipmentProvider(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay danh sach theo Id khi update nhom thiet bi
	 * 
	 * @author hoanv25
	 * @since December 17,2014
	 * */
	ObjectVO<EquipmentVO> getEquipFlan(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Creat EquipGroup
	 * 
	 * @author hoanv25
	 * @since December 18,2014
	 * */
	EquipGroup createEquipGroup(EquipGroup equipGroup, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Creat EquipGroup
	 * 
	 * @author hoanv25
	 * @since December 18,2014
	 * */
	EquipSalePlan createEquipSalePlan(EquipSalePlan equipSalePlan, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Lấy Id cua Equip_Sale_Plan
	 * 
	 * @author hoanv25
	 * @since December 18,2014
	 * */
	EquipSalePlan getEquipSalePlanById(Long id) throws BusinessException;

	/**
	 * Update EquipGroup
	 * 
	 * @author hoanv25
	 * @since December 18,2014
	 * */
	EquipGroup updateEquipGroup(EquipGroup equipGroup, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Update EquipSalePlan
	 * 
	 * @author hoanv25
	 * @since December 18,2014
	 * */
	EquipSalePlan updateEquipSalePlan(EquipSalePlan equipSalePlan, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Lay bien ban giao nhan bang ID
	 * 
	 * @author nhutnn
	 * @since 18/12/2014
	 * @param Id
	 * @return
	 * @throws BusinessException
	 */
	EquipDeliveryRecord getEquipDeliveryRecordById(Long idRecord) throws BusinessException;

	/**
	 * luu bien ban
	 * 
	 * @author nhutnn
	 * @since 18/12/2014
	 * @param equipDeliveryRecord
	 * @param statusRecord
	 * @throws BusinessException
	 */
	void saveEquipRecordDelivery(EquipDeliveryRecord equipDeliveryRecord, Integer statusRecord) throws BusinessException;

	/**
	 * Lay Equipment theo Id
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	Equipment getEquipmentById(Long id) throws BusinessException;

	Equipment getEquipmentById(Long equipmentId, Long shopId) throws BusinessException;
	/**
	 * Sinh EquipImportRecordCode
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	String getEquipImportRecordCodeGenetated() throws BusinessException;

	/**
	 * Tim kiem loai thiet bi
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	ObjectVO<EquipmentVO> searchEquipmentGroupVObyFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay danh sach kho thiet bi
	 * 
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	ObjectVO<EquipmentStockDeliveryVO> getListEquipStockTotalVOByfilter(EquipmentFilter<EquipmentStockDeliveryVO> filter) throws BusinessException;

	/**
	 * Sinh EquipImportRecordCode theo MaxCode
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	String getEquipImportRecordCodeByMaxCode(String maxCode);

	/**
	 * Sinh getEquipmentCodeByMaxCodeAndPrefix theo MaxCode va Prefix
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	String getEquipmentCodeByMaxCodeAndPrefix(String maxCode, String prefix);
	
	/**
	 * Ham phat sinh code tu dong tang dan theo length
	 * @author tamvnm
	 * @param maxCode - code lon nhat hien tai
	 * @param prefix - chuoi so tang dan
	 * @param length - so luong so
	 * @return code lon nhat hien tai  + 1
	 * @since 31/08/2015
	 */
	String getEquipmentCodeByMaxCodeAndPrefixMaxLength(String maxCode, String prefix, int length);

	/**
	 * Tao moi Thiet bi va nhung du lieu lien quan
	 * 
	 * @author hunglm16
	 * @since December 21,2014
	 * */
	void createEquipmentInListEquip(Equipment equipment, EquipmentFilter<BasicVO> filter) throws BusinessException;

	/**
	 * Cap nhat thiet bi va nhung du lieu lien quan
	 * 
	 * @author hunglm16
	 * @since December 21,2014
	 * */
	void updateEquipmentInListEquip(Equipment equipment, EquipmentFilter<BasicVO> filter) throws BusinessException;

	/**
	 * Lay danh sach thiet bi da lap bien ban giao nhan theo khach hang
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 */
	List<EquipmentExVO> getListEquipmentDeliveryByCustomerId(Long customerId) throws BusinessException;

	/**
	 * Lay nhom thiet bi theo ma code
	 * 
	 * @author hunglm16
	 * @since december
	 */
	EquipGroup getEquipGroupByCode(String code) throws BusinessException;

	/**
	 * Lay bien ban bang id
	 * 
	 * @author nhutnn
	 * @since 22-12-2014
	 * @param Id
	 * @return
	 * @throws BusinessException
	 */
	EquipmentRecordDeliveryVO getEquipmentRecordDeliveryVOById(Long id) throws BusinessException;

	/**
	 * chinh sua bien ban giao nhan
	 * 
	 * @author nhutnn
	 * @since 22/12/2014
	 * @param eqDeliveryRecord
	 * @param lstDeliveryRecDtls
	 * @return
	 * @throws DataAccessException
	 */
	void updateRecordDelivery(EquipDeliveryRecord eqDeliveryRecord, List<EquipDeliveryRecDtl> lstDeliveryRecDtls, List<String> lstEquipDelete, Integer status, EquipRecordFilter<EquipmentDeliveryVO> filterFile) throws BusinessException;

	
	/**
	 * chinh sua bien ban giao nhan
	 * 
	 * @author tamvnm
	 * @since 2606/2015
	 * @param eqDeliveryRecord
	 * @param lstDeliveryRecDtls
	 * @return
	 * @throws DataAccessException
	 */
	void updateRecordDeliveryX(EquipDeliveryRecord eqDeliveryRecord, List<EquipDeliveryRecDtl> lstDeliveryRecDtls, List<EquipDeliveryRecDtl> lstEquipDelete, Integer status, EquipRecordFilter<EquipmentDeliveryVO> filterFile) throws BusinessException;

	/**
	 * Tim kiem tat ca thiet bi da duoc lap bien ban giao nhan theo khach hang
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 * */
	List<EquipmentExVO> getListAllEquipmentDeliveryByCustomerId(Long customerId, List<Long> lstEquipId) throws BusinessException;

	/**
	 * Lay danh sach chi tiet bien ban thu hoi thanh ly
	 * 
	 * @author phuongvm
	 * @since 22/12/2014
	 * */
	List<EquipEvictionFormDtl> getEquipEvictionFormDtlByEquipEvictionFormId(Long equipEvictionFormId) throws BusinessException;

	/**
	 * Cap nhat chi tiet bien ban thu hoi thanh ly
	 * 
	 * @author phuongvm
	 * @since 22/12/2014
	 * */
	EquipEvictionForm updateEquipEvictionForm(EquipEvictionForm equipEvictionForm, List<Equipment> lstEquipment, EquipRecordFilter<EquipRepairForm> filter) throws BusinessException;

	/**
	 * Lay danh sach kho nguon VNM va NPP
	 * 
	 * @throws Exception
	 * @author hoanv25
	 * @since Dec 24, 2014
	 */
	ObjectVO<EquipmentVO> getListEquipStock(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay danh sach kho dich VNM va NPP
	 * 
	 * @throws Exception
	 * @author hoanv25
	 * @since Dec 24, 2014
	 */
	ObjectVO<EquipmentVO> getListEquipToStock(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay danh sach nhom thiet bi
	 * 
	 * @author hoanv25
	 * @since December 17,2014
	 * */
	ObjectVO<EquipmentVO> searchEquipmentStockChange(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay bien ban giao nhan bang Id
	 * 
	 * @author hoanv25
	 * @since December 25,2014
	 * */
	EquipStockTransForm getEquipStockTransFormById(Long idRecord) throws BusinessException;
	
	/**
	 * Lay bien ban chuyen kho co phan quyen shop id
	 * @param idRecord
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	EquipStockTransForm getEquipStockTransFormById(Long idRecord, Long shopId) throws BusinessException;

	/**
	 * luu bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 24/12/2014
	 * @param equipDeliveryRecord
	 * @param statusRecord
	 * @throws BusinessException
	 */
	void saveStatusEquipStockRecord(EquipStockTransForm e, Integer statusRecord) throws BusinessException;
	
	/**
	 * Luu trang thai thuc hien
	 * @author dungnt19
	 * @param e
	 * @param performStatusRecord
	 * @throws BusinessException
	 */
	void saveDonePerformStatusRecord(EquipStockTransForm e, Integer performStatusRecord) throws BusinessException;

	EquipStockTransForm updateEquipStockTransForm(EquipStockTransForm e, LogInfoVO logInfoVO) throws BusinessException;

	/**
	 * Lay danh sach cac thiet bi
	 * 
	 * @author hoanv25
	 * @since 25/12/2014
	 * @param kPaging
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentDeliveryVO> getListEquipmentStockVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;

	/**
	 * Lay danh sach bien ban thanh ly
	 * 
	 * @author nhutnn
	 * @since 26/12/2014
	 * @param kPaging
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentLiquidationFormVO> getListEquipmentLiquidationFormVOByFilter(EquipmentFilter<EquipmentLiquidationFormVO> equipmentFilter) throws BusinessException;

	/**
	 * Lay danh sach bien ban thanh ly cho export
	 * 
	 * @author tamvnm
	 * @since 12/01/2015
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	List<EquipmentLiquidationFormVO> getListEquipmentLiquidationFormVOForExport(List<Long> equipmentFilter) throws BusinessException;

	/**
	 * Lay bien ban thanh ly bang ID
	 * 
	 * @author nhutnn
	 * @since 18/12/2014
	 * @param Id
	 * @return
	 * @throws BusinessException
	 */
	EquipLiquidationForm getEquipLiquidationFormById(Long idForm) throws BusinessException;

	/**
	 * Kiem tra ma Loai, NCC, Hieu, HangMucSuaChua trung
	 * 
	 * @author hoanv25
	 * @since 29/12/2014
	 * @param Code
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentVO> listEquipCategoryCode(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	ObjectVO<EquipmentVO> listEquipEquipProvider(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	ObjectVO<EquipmentVO> listEquipBrand(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	ObjectVO<EquipmentVO> listEquipItem(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay danh sach nhom thiet bi theo id loai
	 * 
	 * @author hoanv25
	 * @since 26/12/2014
	 * @param Id
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentVO> getListEquipGroupById(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	ObjectVO<EquipmentVO> getListEquipGroupByIdEquipProvider(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Kiem tra ma HangMucSuaChua khi thay doi trang thai
	 * 
	 * @author hoanv25
	 * @since 29/12/2014
	 * @param Code
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentVO> listEquipItemById(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay danh sach phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @param EquipRepairFilter
	 * @throws DataAccessException
	 */
	ObjectVO<EquipRepairFormVO> getListEquipRepair(EquipRepairFilter filter) throws BusinessException;

	/**
	 * Lay danh sach chi tiet phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @param EquipRepairFilter
	 *            .EquipRepairFormId
	 * @throws DataAccessException
	 */
	ObjectVO<EquipRepairFormDtl> getListEquipRepairDtlByEquipRepairId(EquipRepairFilter filter) throws BusinessException;

	/**
	 * Kiem tra ma nhom thiet bi khi tao moi
	 * 
	 * @author hoanv25
	 * @since 29/12/2014
	 * @param Code
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentVO> getlistEquipGroup(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Tao moi phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @param EquipRepairForm
	 * @throws DataAccessException
	 */
	EquipRepairForm createEquipRepairForm(EquipRepairForm equipRepairForm) throws BusinessException;

	/**
	 * cap nhat phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @param EquipRepairForm
	 * @throws DataAccessException
	 */
	EquipRepairForm updateEquipRepairForm(EquipRepairForm equipRepairForm) throws BusinessException;

	/**
	 * lay 1 phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @param EquipRepairForm
	 * @throws DataAccessException
	 */
	EquipRepairForm getEquipRepairFormById(long id) throws BusinessException;


	/**
	 * @author vuongmq
	 * @date 22/04/2015 
	 * in phieu phieu yeu cau sua chua*/
	List<EquipRepairFormVOList> getEquipRepairPrint(EquipRepairFilter filter) throws BusinessException;
	
	/**
	 * cap nhat trang thai phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 31/12/2014
	 * @param EquipRepairForm
	 * @throws DataAccessException
	 */
	void updateEquipRepairFormStatus(EquipRepairForm equipRepairForm, List<EquipRepairFormDtl> lstEquipRepairFormDtl, int status, Equipment equipOld, EquipRecordFilter<EquipRepairForm> filter) throws BusinessException;

	/**
	 * Them moi Lich su thiet bi voi tham so la thiet bi
	 * 
	 * @author hunglm16
	 * @since January 01,2014
	 */
	void createEquipHistoryByEquipment(Equipment equipment, String userName) throws BusinessException;

	/**
	 * Lay lich su Bien ban thiet bi
	 * 
	 * @author hunglm16
	 * @since January 05,2014
	 */
	ObjectVO<EquipRecordVO> getHistoryEquipRecordByFilter(EquipRecordFilter<EquipRecordVO> filter) throws BusinessException;

	/**
	 * Lay danh sach phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 01/01/2015
	 * @param EquipRepairFilter
	 * @throws DataAccessException
	 */
	ObjectVO<EquipRepairFormVO> getListEquipRepairEx(EquipRepairFilter filter) throws BusinessException;

	/**
	 * Lay id cua sale_plane theo id equip_group
	 * 
	 * @author hoanv25
	 * @since 3/1/2015
	 */
	ObjectVO<EquipmentVO> getListEquipSalePlan(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * tim kiem nhom thiet bi theo filter
	 * 
	 * @author tuannd20
	 * @param equipmentGroupFilter
	 *            dieu kien tim kiem thiet bi
	 * @return ket qua tim kiem nhom thiet bi
	 * @throws BusinessException
	 * @date 05/01/2015
	 */
	ObjectVO<EquipmentGroupVO> searchEquipmentGroupVO(EquipmentGroupProductFilter<EquipmentGroupVO> equipmentGroupFilter) throws BusinessException;
	
	/**
	 * Lay danh sach san pham nhom thiet bi cho export
	 * 
	 * @author tamvnm
	 * @param equipmentGroupFilter
	 *            dieu kien tim kiem thiet bi
	 * @return danh sach nhom thiet bi va san pham
	 * @throws BusinessException
	 * @since Sep 17 2015
	 */
	List<EquipmentGroupVO> getListEquipmentGroupVOForExport(EquipmentGroupProductFilter<EquipmentGroupVO> equipmentGroupFilter) throws BusinessException;

	/**
	 * lay danh sach san pham trong equipment_group
	 * 
	 * @author tuannd20
	 * @param equipmentGroupCode
	 *            ma nhom thiet bi
	 * @param equipmentGroupProductStatus
	 *            trang thai equipmentGroupProduct
	 * @param kPaging
	 *            phan trang ket qua
	 * @return danh sach san pham trong equipment_group
	 * @throws BusinessException
	 * @date 05/01/2015
	 */
	ObjectVO<EquipmentGroupProductVO> retrieveProductsInEquipmentGroup(String equipmentGroupCode, ActiveType equipmentGroupProductStatus, KPaging<EquipmentGroupProductVO> kPaging) throws BusinessException;

	/**
	 * xoa san pham ra khoi nhom thiet bi
	 * 
	 * @author tuannd20
	 * @param equipmentGroupCode
	 *            ma nhom thiet bi
	 * @param productCodes
	 *            ma san pham cua cac san pham can xoa
	 * @param logInfo
	 * @throws BusinessException
	 * 
	 * @author hunglm16
	 * @description ra soat va update
	 */
	void removeProductFromEquipmentGroup(String equipmentGroupCode, List<Long> equipGroupProductIds, LogInfoVO logInfo) throws BusinessException;

	/**
	 * them san pham vao nhom thiet bi
	 * 
	 * @author tuannd20
	 * @param equipmentGroupCode
	 *            ma nhom thiet bi
	 * @param productCodes
	 *            ma san pham cua cac san pham can them
	 * @param logInfo
	 * @throws BusinessException
	 */
	void addProductToEquipmentGroup(String equipmentGroupCode, List<String> productCodes, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Sua gird thong so nhom thiet bi
	 * 
	 * @author hoanv25
	 * @since 3/1/2015
	 * 
	 * @author hunglm16
	 * @since May 02,2015
	 * @description thay doi tham so
	 */
	void changeEquipSalePlanByFilter(EquipmentSalePlaneFilter<EquipGroup> filter) throws BusinessException;

	/**
	 * Them gird thong so nhom thiet bi
	 * 
	 * @author hoanv25
	 * @since 3/1/2015
	 */
	void createEquipSalePlanByFilter(EquipmentSalePlaneFilter<EquipmentVO> create) throws BusinessException;

	/**
	 * Lay danh sach hang muc
	 * 
	 * @author phuongvm
	 * @since 05/01/2015
	 * @param EquipItemFilter
	 * @throws DataAccessException
	 */
	ObjectVO<EquipItemVO> getListEquipItem(EquipItemFilter filter) throws BusinessException;


	/***
	 * @author vuongmq
	 * @date 06/04/2015
	 * lay danh sach hang muc popup lap phieu yeu cau sua chua
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	ObjectVO<EquipItemVO> getListEquipItemPopUpRepair(EquipItemFilter filter) throws BusinessException;

	
	/**
	 * Lay danh sach cac thiet bi ban giao
	 * 
	 * @author phuongvm
	 * @since 06/01/2015
	 * @param kPaging
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentDeliveryVO> getListEquipmentDeliveryVOByFilterEx(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;

	/**
	 * Tim kiem danh sach thiet bi bien ban sua chua, 
	 * lay theo phan quyen; popup thiet bi
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	ObjectVO<EquipmentDeliveryVO> getListEquipmentPopUp(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;
	/**
	 * Lay danh sach thiet bi co ban hop dong moi nhat
	 * 
	 * @author hunglm16
	 * @since January 06,2014
	 */
	ObjectVO<EquipmentVO> searchEquipByEquipDeliveryRecordNew(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay danh sach bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 05/01/2015
	 * 
	 * @throws DataAccessException
	 */
	EquipStockTransForm getEquipStockTransForm(Long id) throws BusinessException;

	/**
	 * Lay ma kho nguon
	 * 
	 * @author hoanv25
	 * @since 05/01/2015
	 * 
	 * @throws DataAccessException
	 */
	Shop getListEquipStockTransForm(Long fromStockId) throws BusinessException;

	/**
	 * Lay ma kho dich
	 * 
	 * @author hoanv25
	 * @since 05/01/2015
	 * 
	 * @throws DataAccessException
	 */
	Shop getListToStockTransForm(Long toStockId) throws BusinessException;

	/**
	 * Luu bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @param equipStockTransForm
	 * @param equipmentStockTransFormDtl
	 * @param logInfo
	 * @since 05/01/2015
	 * @throws DataAccessException
	 */
	EquipStockTransForm createStockTransFormByImportExcel(EquipStockTransForm equipStockTransForm, List<EquipStockTransFormDtl> equipmentStockTransFormDtl, EquipRecordFilter<EquipLostRecord>  filter, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Tao moi phieu sua chua
	 * 
	 * @author hoanv25
	 * @since 07/01/2015
	 * 
	 * @throws DataAccessException
	 */
	EquipRepairForm createEquipRepairForm(EquipRepairForm equipRepairForm, List<EquipRepairFormDtl> lstEquipRepairFormDtl, EquipRecordFilter<EquipRepairForm> filter) throws BusinessException;

	/**
	 * xay dung cay nganh hang - nganh hang con - san pham de them vao
	 * equipmentGroup
	 * 
	 * @author tuannd20
	 * @param productCode
	 *            ma san pham tim kiem
	 * @param productName
	 *            ten san pham tim kiem
	 * @param equipmentGroupId
	 *            id cua equipment_group
	 * @param exceptProductsInZCategory
	 *            loai bo san pham thuoc nganh hang Z hay ko
	 * @return danh sach nganh hang, san pham
	 * @throws BusinessException
	 */
	List<ProductTreeVO> buildProductTreeVOForEquipmentGroup(String productCode, String productName, Long equipmentGroupId, Boolean exceptProductsInZCategory) throws BusinessException;

	/***
	 * Lay EquipRepairPayFormBy Code
	 * @author vuongmq
	 * @date 19/04/2015
	 * @return EquipRepairPayForm
	 */
	EquipRepairPayForm getEquipRepairPayFormByCode(String code) throws BusinessException;
	
	/**
	 * cap nhat phieu thanh toan cho phieu sua chua
	 * 
	 * @author tuannd20
	 * @param equipmentRepairItemId
	 * @param equipmentRepairPaymentRecord
	 * @param logInfo
	 * @return null neu nhu cap nhat thanh cong, nguoc lai, key cua thong bao
	 *         loi
	 * @throws BusinessException
	 */
	String updateEquipmentRepairPaymentRecord(final Long equipmentRepairItemId, final EquipmentRepairPaymentRecord equipmentRepairPaymentRecord, final LogInfoVO logInfo) throws BusinessException;

	/**
	 * Xoa thiet bi khoi bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 05/01/2015
	 * 
	 * @throws DataAccessException
	 */
	void deleteStockTransFormDtl(List<Long> lstDelete) throws BusinessException;

	/**
	 * Lay danh sach thiet bi theo id bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 06/01/2015
	 * 
	 * @throws DataAccessException
	 */
	ObjectVO<EquipmentDeliveryVO> getListEquipmentequipmentStockTransFormVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;

	/**
	 * Luu bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 06/01/2015
	 * 
	 * @throws DataAccessException
	 */
	EquipStockTransForm updateEquipmentStockTransForm(EquipStockTransForm equipStockTransForm, List<Long> addEquipmentIds,EquipRecordFilter<EquipStockTransForm> filter, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Xu ly su kien enter ma thiet bi
	 * 
	 * @author hoanv25
	 * @since 08/01/2015
	 * 
	 * @throws DataAccessException
	 */
	ObjectVO<EquipmentDeliveryVO> getListEquipmentStockTransByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;

	/**
	 * lay danh sach hang muc da thanh toan cua phieu sua chua
	 * 
	 * @author tuannd20
	 * @param equipmentRepairFormId
	 *            id phieu sua chua
	 * @return chi tiet cac hang muc sua chua trong phieu thanh toan
	 * @throws BusinessException
	 */
	List<EquipRepairPayFormDtl> retrieveEquipmentRepairPaymentDetails(Long equipmentRepairFormId) throws BusinessException;

	/**
	 * lay phieu thanh toan cua phieu sua chua
	 * 
	 * @author tuannd20
	 * @param equipmentRepairFormId
	 *            id phieu sua chua
	 * @return chi tiet cac hang muc sua chua trong phieu thanh toan
	 * @throws BusinessException
	 */
	EquipRepairPayForm retrieveEquipmentRepairPayment(Long equipmentRepairFormId) throws BusinessException;

	/**
	 * cap nhat trang thai bien ban kiem ke
	 * 
	 * @author tuannd20
	 * @param equipmentStatisticIds
	 *            danh sach bien ban kiem ke cap nhat trang thai
	 * @param equipmentStatisticRecordStatus
	 *            trang thai bien ban kiem ke moi
	 * @param logInfo
	 *            thong tin ghi log
	 * @return danh sach cac bien ban cap nhat that bai
	 * @throws BusinessException
	 */
	List<FormErrVO> updateEquipmentStatisticStatus(List<Long> equipmentStatisticIds, EquipmentStatisticRecordStatus equipmentStatisticRecordStatus, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Kiem tra kho nguon cua thiet bi
	 * 
	 * @author hoanv25
	 * @param equipmentFilter
	 * @return danh sach thiet bi co trong kho nguon
	 * @since 13/01/2015
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentDeliveryVO> getCheckFromStockEquipment(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;

	/**
	 * Cap nhat EquipStatisticRecord
	 * @author trietptm
	 * @param equipStatisticRecord
	 * @param status
	 * @param type
	 * @param logInfo
	 * @throws BusinessException
	 * @since Mar 22, 2016
	 */
	void updateEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord, Integer status, Integer type, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Lay Thiet Bi Theo Ma Code
	 * 
	 * @author hunglm16
	 * @since January 13,2014
	 * */
	Equipment getEquipmentByCode(String code) throws BusinessException;

	/**
	 * Lay danh sach theo id chi tiet bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @param id
	 * @return danh sach chi tiet bien ban chuyen kho
	 * @since 13/01/2015
	 * @throws BusinessException
	 */
	EquipStockTransFormDtl getEquipStockTransFormDtlById(Long id) throws BusinessException;

	/**
	 * Lay equipment Entity by code theo filter
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	Equipment getEquipmentByCodeFilter(EquipmentFilter<Equipment> filter) throws BusinessException;
	
	/**
	 * Lay equipment Entity by code an status
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	Equipment getEquipmentByCodeAndStatus(String code, ActiveType status) throws BusinessException;
	
	/**
	 * Lay EquipItem theo code
	 * 
	 * @author phuongvm
	 * @since 13/01/2015
	 * */
	EquipItem getEquipItemByCode(String code) throws BusinessException;

	/**
	 * Lay danh sach in bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @param equipmentFilter
	 * @return danh sach in bien ban chuyen kho
	 * @since 14/01/2015
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentDeliveryVO> searchRecordStockChangeByEquipment(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;

	/**
	 * Tao moi phieu sua chua tu file excel
	 * 
	 * @author phuongvm
	 * @since 14/01/2015
	 * @param EquipRepairForm
	 * @throws DataAccessException
	 */
	EquipRepairForm createEquipRepairFormByExcel(EquipRepairForm equipRepairForm, List<EquipRepairFormDtl> lstEquipRepairFormDtl) throws BusinessException;

	/**
	 * Lay danh sach thu hoi va thanh ly de xuat file excel
	 * 
	 * @author tamvnm
	 * @since 14/01/2015
	 * @param lstIdRecord
	 * @throws DataAccessException
	 */
	List<EquipmentEvictionVO> getListEquipmentEvictionVOForExport(List<Long> lstIdRecord) throws BusinessException;

	/**
	 * Lay danh sach thiet bi theo id chi tiet bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @param equip
	 * @return danh sach thiet bi
	 * @since 14/01/2015
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentVO> getEquipStockTransFormDtlByFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException;

	/**
	 * Lay danh sach thiet bi theo id chi tiet bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @param equip
	 * @return danh sach thiet bi
	 * @since 14/01/2015
	 * @throws BusinessException
	 */
	Equipment getEquipmentByEquipId(Long equipId) throws BusinessException;

	/**
	 * Lay danh sach kiem ke de xuat file excel
	 * 
	 * @author tamvnm
	 * @since 16/01/2015
	 * @param lstIdRecord
	 * @throws DataAccessException
	 */
	List<EquipmentStatisticCheckingRecordExportVO> getListEquipmentStatisticCheckingVOForExport(List<Long> lstIdRecord);

	/**
	 * Chọn một thiết bị ở trạng thái ko giao dịch và khóa nó lại
	 * 
	 * @author tamvnm
	 * @since 08/05/2015
	 * @param EquipItemFilter
	 * @throws DataAccessException
	 */
	Equipment getEquipmentByIdAndBlock(EquipItemFilter filter) throws BusinessException;
	
	/**
	 * Lay danh sach bien ban thu hoi va thanh ly cho in
	 * 
	 * @author phuongvm
	 * @since 15/01/2015
	 * @param lstIdEviction
	 * @throws BusinessException
	 */
	List<EquipmentEvictionVOs> getListEquipmentEvictionVOForPrint(List<Long> lstIdRecord) throws BusinessException;

	/**
	 * Lay stock code - name ben EquipStock voi stock_id; cua kho don vi
	 * @author vuongmq
	 * @since 23/04/2015
	 * @return
	 */
	EquipStock getEquipStockByFilter(EquipStockEquipFilter<EquipStock> filter)  throws BusinessException ;
	
	/***
	 * @author vuongmq
	 * @date 20/04/2015
	 * lay equip_repair_form_dtl theo filter, 
	 * lay hang muc con thoi gian bao hanh
	 */
	EquipRepairFormDtl getEquipRepairFormDtlByFilter(EquipItemFilter filter) throws BusinessException ;
	/**
	 * lay chi tiet phieu sua chua
	 * 
	 * @author nhutnn
	 * @since 16/01/2015
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentRepairFormDtlVO> getListEquipRepairDtlVOByEquipRepairId(EquipRepairFilter filter) throws BusinessException;

	/**
	 * Lay danh sach kho man hinh phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 19/01/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	ObjectVO<EquipStockVO> getListStockByFilter(EquipStockFilter filter) throws BusinessException;

	/**
	 * Tim kiem so hop dong moi nhat
	 * 
	 * @author hunglm16
	 * @since January 26,2014
	 * */
	ObjectVO<EquipmentVO> searchContractNumberByFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException;
	
	/******************************* BEGIN VUONGMQ ******************************************/
	
	EquipStock getEquipStockbyCode(String code) throws BusinessException;
	
	Integer getEquipStockOrdinalMaxByShop(Long id) throws BusinessException;
	
	List<EquipStock> getEquipStockByShopAndOrdinal(Long id, Integer stt) throws BusinessException;
	/**
	 * Tim kiem kho thiet bi: equip_Stock
	 * 
	 * @author vuongmq
	 * @date Mar 13,2015
	 * */
	ObjectVO<EquipStock> getListEquipmentStockByFilter(EquipmentFilter<EquipStock> filter) throws BusinessException;
	
	/**
	 * cay don vị view
	 * 
	 * @author vuongmq
	 * @params filter 
	 * @date Mar 19,2015
	 * */
	List<ShopViewParentVO> searchShopTreeView(Long shopId, String code,	String name, EquipmentFilter<ShopViewParentVO> filter) throws BusinessException;
	/******************************* END VUONGMQ ******************************************/
	
	/**
	 * Tim kiem nhom thiet bi dang hoat dong trong bang equipment
	 * 
	 * @author hoanv25
	 * @since 28/1/2014
	 * */
	ObjectVO<EquipmentVO> getListCheckEquipGroupById(EquipmentFilter<EquipmentVO> filter) throws BusinessException;
	
	/**
	 * Tim kiem nhom thiet bi dang hoat dong trong bang equip_group_product
	 * 
	 * @author hoanv25
	 * @since 28/1/2014
	 * */
	ObjectVO<EquipmentVO> getListCheckEquipGroupProductByIdEquipGroup(EquipmentFilter<EquipmentVO> filter)throws BusinessException;
	/**
	 * Cập nhật bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @param addEquipmentIds
	 * @since 05/01/2015
	 * @throws DataAccessException
	 */
	EquipStockTransForm updateStockTransForm(EquipStockTransForm equipStockTransForm, List<EquipStockTransFormDtl> equipmentStockTransFormDtl, List<Long> addEquipmentIds, EquipRecordFilter<EquipStockTransForm> filter, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Tao moi nhom thiet bi tu file excel
	 * 
	 * @author liemtpt
	 * @since 18/03/2015
	 * @param EquipGroup
	 * @throws DataAccessException
	 */
//	void createEquipGroupByExcel(EquipmentVO equipGroup) throws BusinessException;
	void createEquipGroupByExcel(EquipmentGroupVO vo) throws BusinessException;

	/**
	 * get list equip sale plan by filter
	 * @author liemtpt
	 * @param EquipmentSalePlaneFilter
	 * @since 18/03/2015
	 * @param EquipGroup
	 * @throws DataAccessException
	 */
	List<EquipSalePlan> getListEquipSalePlanByFilter(EquipmentSalePlaneFilter<EquipSalePlan> filter) throws BusinessException;
	/**
	 * get list equip stock vo by filter
	 * @author liemtpt
	 * @param EquipStockFilter
	 * @since 23/03/2015
	 * @throws DataAccessException
	 * 
	 * @author hunglm16
	 * @since May 26, 2015
	 * @description lay danh sach kho theo phan quyen
	 */
	ObjectVO<EquipStockVO> getListEquipStockVOByFilter(EquipStockFilter filter) throws BusinessException;

	/**
	 * Tao moi Quyen nguoi dung
	 * 
	 * @author tamvnm
	 * @since 24/03/2015
	 * @param 
	 * @throws BusinessException
	 */
	EquipRoleUser createEquipRoleUser(EquipRoleUser roleUser) throws BusinessException;

	/**
	 * Lay danh sach staff cho phan quyen - Thiet bi
	 * 
	 * @author tamvnm
	 * @since 24/03/2015
	 * @param 
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentStaffVO> getStaffForPermission(EquipStaffFilter filter) throws BusinessException;

	

	/**
	 * Lay danh sach staff cho phan quyen - Thiet bi
	 * Note: lay danh sach staff giong nhu getStaffForPermission o tren; cong them roleCode quyen vao cho nhan vien nua
	 * @author vuongmq
	 * @date 26/03/2015
	 * @param 
	 * @throws DataAccessException
	 */
	ObjectVO<EquipmentStaffVO> getStaffForPermissionExportExcel( EquipStaffFilter filter) throws BusinessException;
	
	
	/**
	 * Lay danh sach quyen - Gan Quyen Cho Nguoi Dung
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param 
	 * @throws BusinessException
	 */
	ObjectVO<EquipmentPermissionVO> getPermissionByFilter(EquipPermissionFilter filter) throws BusinessException;

	/**
	 * Lay danh sach quyen  - popup search quyen
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param 
	 * @throws BusinessException
	 */
	ObjectVO<EquipRole> searchListEquipRoleByFilter(EquipRoleFilter filter) throws BusinessException;
	
	/**
	 * Lay EquipRole
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param  id
	 * @throws BusinessException
	 */
	EquipRole getEquipRoleById(Long id) throws BusinessException;

	
	/**
	 * Lay Entities EquipRoleUser ung voi filter
	 * @author vuongmq
	 * @since 27/03/2015
	 */
	EquipRoleUser getEquipEquipRoleUserByFilter(EquipPermissionFilter filter) throws BusinessException;
	/**
	 * Lay Entities EquipStock ung voi id
	 * @author phuongvm
	 * @since 30/03/2015
	 */
	EquipStock getEquipStockById(Long id) throws BusinessException;
	/**
	 * Update trạng thái hoặc kho đích trong biên bản chuyển kho đã duyệt
	 * @author phuongvm
	 * @since 03/04/2015
	 */
	EquipStockTransForm updateStockTransFormApproved(
			EquipStockTransForm equipStockTransForm,
			List<EquipStockTransFormDtl> equipmentStockTransFormDtl,
			List<Long> addEquipmentIds,
			EquipRecordFilter<EquipStockTransForm> filter, LogInfoVO logInfo)
			throws BusinessException;
	/**
	 * Ham lay danh sach StockTranFormDtl xuat excel
	 * @author phuongvm
	 * @since 06/04/2015
	 */
	List<EquipmentVO> searchEquipmentStockChangeExcel(EquipmentFilter<EquipmentVO> filter) throws BusinessException;
	/**
	 * get list equip item config by filter
	 * @author liemtpt
	 * @since 08/04/2015
	 * @param EquipItemConfigFilter
	 * @throws BusinessException
	 * @description  Lay danh sach dinh muc hang muc
	 */
	ObjectVO<EquipItemConfigVO> getListEquipItemConfigByFilter(EquipItemConfigFilter filter) throws BusinessException;
	/**
	 * get list equip item config detail by filter
	 * @author liemtpt
	 * @since 07/04/2015
	 * @param EquipItemConfigFilter
	 * @throws BusinessException
	 * @description  Lay danh sach dinh muc hang muc
	 */
	//ObjectVO<EquipItemConfigVO> getListEquipItemConfigDetailByFilter(EquipItemConfigFilter filter) throws BusinessException;
	List<EquipItemConfigDtlVO> getListEquipItemConfigDetailByFilter(EquipItemConfigDtlFilter filter) throws BusinessException;

	/**
	 * Lay danh sach cac thiet bi
	 * 
	 * @author tamvnm
	 * @since 22/05/2015
	 * @param kPaging
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	ObjectVO<EquipStockVO> getListEquipStockVOByRole(EquipStockFilter filter) throws BusinessException;

	
	/**
	 * Lay danh sach thiet bi cho muon
	 * 
	 * @author tamvnm
	 * @since 09/04/2015
	 * */
	List<EquipLendDetail> getListEquipmentLendDetailByLendId(Long equipLendId) throws BusinessException;
	
	/**
	 * Lay danh sach chi tiet thiet bi 
	 * 
	 * @author tamvnm
	 * @since 27/05/2015
	 * */
	List<EquipDeliveryRecDtl> getListDeliveryDetailByRecordId(Long equipDeliveryId) throws BusinessException;
	
	/**
	 * Lay danh sach chi tiet thiet bi 
	 * 
	 * @author tamvnm
	 * @since 18/05/2015
	 * @param equip suggest eviction id, status
	 * */
	List<EquipSuggestEvictionDTL> getListSuggestDetailByRecordId(Long equipSuggestId, Integer status) throws BusinessException;

	/**
	 * Lay danh sach EquipItemConfigDtl cua hang muc cau hinh
	 * @author vuongmq
	 * @since 14/04/2015
	 * */
	EquipItemConfigDtl getEquipItemConfigDtl(EquipItemFilter filter) throws BusinessException;

	/***
	 * @author vuongmq
	 * @date 15/04/2015
	 * Lấy doanh so theo nganh hang cua Ql sua chua
	 */
	Object getDoanhSoEquipRepair(EquipRepairFilter filter) throws BusinessException;
	
	/**
	 * Lay danh sach hop dong 
	 * 
	 * @author tamvnm
	 * @since 14/04/2015
	 * @params: equipmentFilter.numberContract
	 * */
	List<EquipDeliveryRecord> getListDeliveryRecordByFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException;
	
	/**
	 * Lay danh sach hop dong cho export excel
	 * 
	 * @author tamvnm
	 * @since 14/04/2015
	 * @params: EquipmentFilter
	 * */
	List<EquipmentRecordDeliveryVO> getListDeliveryRecordForExport(EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter) throws BusinessException;
	
	/**
	 * Lay danh sach thiet bi o kho phan quyen
	 * 
	 * @author tamvnm
	 * @since 14/04/2015
	 * @params: filter
	 * */
	ObjectVO<EquipmentDeliveryVO> getListEquipmentByRole(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException;

	/**
	 * Lay bien ban de nghi muon thiet bi, voi tat ca cac bien bang giao nhan va hop dong Da Duyet hoac Huy
	 * 
	 * @author tamvnm
	 * @since 15/04/2015
	 * @params: id bien ban
	 * */
	EquipLend getEquipLendById(Long id) throws BusinessException;
	
	/**
	 * Lay kho thiet bi theo phan quyen
	 * 
	 * @author tamvnm
	 * @since 05/05/2015
	 * @params: Ma kho
	 * */
	ObjectVO<EquipStockVO> getEquipStockCMSbyCode(EquipStockFilter filter) throws BusinessException;
	
	/**
	 * get code equip item config
	 * @author liemtpt
	 * @since 13/04/2015
	 * @param 
	 * @throws DataAccessException
	 * @description 
	 */
	String getCodeEquipItemConfig() throws BusinessException;
	
	/**
	 * get list equip item config dtl by filter
	 * @author liemtpt
	 * @since 15/04/2015
	 * @param EquipItemConfigDtlFilter
	 * @throws DataAccessException
	 * @description lay danh sach hang muc 
	 */
	List<EquipItemConfigDtl> getListEquipItemConfigDtlByFilter(EquipItemConfigDtlFilter filter) throws BusinessException;
	/**
	 * get equip item by code
	 * @author liemtpt
	 * @since 15/04/2015
	 * @param code
	 * @param status
	 * @throws DataAccessException
	 * @description lay dinh muc theo code 
	 */

	EquipItem getEquipItemByCodeEx(String code, ActiveType status) throws BusinessException;
	/**
	 * get list check exist crossing
	 * @author liemtpt
	 * @since 15/04/2015
	 * @param EquipItemConfigFilter
	 * @throws DataAccessException
	 * @description Check xem dung tich tu va den co trung chom du lieu trong DB 
	 */
	List<EquipItemConfig> getListCheckExistCrossing(EquipItemConfigFilter filter) throws BusinessException;
	/**
	 * 
	 * @author hoanv25
	 * @since 16/04/2015
	 * @param EquipItemConfigFilter
	 * @throws DataAccessException
	 * @description Lay list shop_id
	 */
	List<ShopViewParentVO> searchShopTreeCustomer(Long shopId, String code, String name)throws BusinessException;
	/**
	 * create or update equip item config by excel
	 * @author liemtpt
	 * @since 17/04/2015
	 * @param equipItemConfig
	 * @param lstEquipItemConfigDtl
	 * @throws DataAccessException
	 * @description import du lieu excel 
	 * 
	 * @author hunglm16
	 * @since May 04,2015
	 * @description Thay doi phuong thuc xu ly
	 */
	void createOrUpdateEquipItemConfigByExcel(EquipItemConfig equipItemConfig, List<EquipItemConfigDtl> lstEquipItemConfigDtl) throws BusinessException;
	
	/**
	 * Lay danh sach cac ky da dong ma co from_Date > filter.fromdate truyen vao 
	 * 
	 * @author phuongvm
	 * @since 20/04/2015
	 * @params: EquipmentFilter filter 
	 * */
	List<EquipPeriod> getListEquipPeriodClosedByFromDate(EquipmentFilter<EquipPeriod> filter) throws BusinessException;
	
	/**
	 * Lay danh sach bien ban de in
	 * 
	 * @author tamvnm
	 * @since 22/04/2015
	 * @params: id bien ban
	 * */
	List<EquipmentDeliveryPrintVO> getListEquipDeliveryRecordForPrint(List<Long> lstId)  throws BusinessException;

	/**
	 * Lay danh sach ky trong nam de in bao cao
	 * @author hoanv25
	 * @since May 05,2015
	 * @param filter
	 * @throws DataAccessException
	 * @description 
	 */
	List<EquipmentVO> getListEquipmentPeriod(EquipmentFilter<EquipmentVO> filter) throws BusinessException;
	
	/**
	 * Kiem tra tinh chung chom Dung tich
	 * 
	 * @param fCapacity: dung tich tu
	 * @param tCapacity: dung tich den
	 * @param status: tinh trang
	 * @param longsException[]: danh sach Id loai tru khong muon kiem tra
	 * 
	 * @author hunglm16
	 * @since May 04,2015
	 * */
	Integer countIntersectionCapacity(Integer fCapacity, Integer tCapacity, Integer status, Long[] longsException) throws BusinessException;
	
	/**
	 * Lay doi tuong dau tien EquipItemConfig theo Dung tich
	 * 
	 * @param fCapacity: dung tich tu
	 * @param tCapacity: dung tich den
	 * @param status: tinh trang
	 * 
	 * @author hunglm16
	 * @since May 04,2015
	 * */
	EquipItemConfig getEquipItemConfigByFromAndToCapacity(Integer fCapacity, Integer tCapacity, Integer status) throws BusinessException;
	
	/**
	 * Xuat excel Thiet lap dinh muc hang muc
	 * 
	 * @param equipItemConfigId: Ma ID
	 * 
	 * @author hunglm16
	 * @since May 04,2015
	 * */
	List<EquipItemConfigVO> exportListEquipItemConfigByFilter(EquipItemConfigFilter filter) throws BusinessException;
	
	/**
	 * Lay danh sach ky theo Id
	 * @author hoanv25
	 * @since May 05,2015
	 * @param filter
	 * @throws DataAccessException
	 * @description 
	 */
	EquipPeriod getListEquipPeriodById(Long id) throws BusinessException;
	/**
	 * Lay danh sach ky khi thay doi nam
	 * @author hoanv25
	 * @since May 05,2015
	 * @param filter
	 * @throws DataAccessException
	 * @description 
	 */
	ObjectVO<EquipmentVO> getListEquipmentPeriodChangeYear(EquipmentFilter<EquipmentVO> filter)throws BusinessException;

	/**
	 * Cap nhat Dinh muc hang muc theo trang thai
	 * 
	 * @author hunglm16
	 * @since May 07, 2015
	 * @description Cap nhat cho cac Detail
	 * */
	void updateEquipItemConfigByStatus(EquipItemConfig equipItemConfig, Integer status, String userName) throws BusinessException;

	/**
	 * Lay tat ca hang muc voi Dinh muc
	 * Nhung hang muc co dinh muc tu fill thong tin
	 * Nhung hang muc khac chua co de thong tin trong chi tiet hang muc rong
	 * 
	 * @param equipItemConfigId: Id Dinh Muc
	 * 
	 * @author hunglm16
	 * @since May 07, 2015
	 * */
	List<EquipItemConfigDtlVO> getEquipItemInDetailByEquipItemConfig(Long equipItemConfigId) throws BusinessException;

	/***
	 * @author vuongmq
	 * @date 11/05/2015
	 * lay danh sách kho cap tren cua Thiet bi ung voi tung loai 1: NPP; 2: KH
	 */
	List<EquipStock> getListEquipStockParentByFilter(EquipStockEquipFilter<EquipStock> filter) throws BusinessException;

	/***
	 * @author vuongmq
	 * @date 11/05/2015
	 * lay danh sách kho cap tren cua Thiet bi ung voi tung loai 1: NPP; 2: KH ; IF getListEquipStockParentByFilter() KHONG CO DU LIEU
	 */
	List<EquipStock> getListEquipStockParentSameLevelByFilter(EquipStockEquipFilter<EquipStock> filter) throws BusinessException;
	
	
	/***
	 * @author tamvnm
	 * @date 21/07/2015
	 * Lay danh sach nhom thiet bi status = 1
	 */
	List<EquipCategory> getListCategory() throws BusinessException;

	
	/**
	 * Lay danh sach loai thiet bi cho bao cao
	 * @author hoanv25
	 * @since May 13,2015
	 * @param statisticRecord
	 * @throws DataAccessException
	 * @description 
	 */
	List<EquipmentVO> getListCategoryCount(Long statisticRecord) throws BusinessException;
	/**
	 * Kiểm tra tồn tại thiết bị trong phiếu chuyển kho
	 * 
	 * @author phuongvm
	 * @since 13/05/2015
	 * @params: stockTranFormId
	 * @params: equipId
	 * */
	Boolean checkExistStockTranFormDtlByEquip(Long stockTranFormId, Long equipId) throws BusinessException;

	/**
	 * Lay danh sach nhom thiet bi theo Filter
	 * 
	 * @author hunglm16
	 * @since May 18, 2015
	 * */
	List<EquipmentGroupVO> getListEquipGroupForFilter(EquipGFilter<EquipmentGroupVO> filter) throws BusinessException;
	/**
	 * Lay danh sach san pham, nhom thiet bi
	 * 
	 * @author hunglm16
	 * @since May 20,2015
	 * */
	ObjectVO<EquipGroupProduct> getListEquipGroupProductByFilter(EquipmentGroupProductFilter<EquipGroupProduct> filter) throws BusinessException;

	/**
	 * Lay danh sach kiem ke theo ky
	 * 
	 * @author hoanv25
	 * @since May 20,2015
	 * @param filter
	 * @throws DataAccessException
	 * @description
	 */
	ObjectVO<EquipmentVO> getListEquipmentStatisticPeriod(EquipmentFilter<EquipmentVO> filter) throws BusinessException;
	
	/**
	 * Kiem tra chung chom tu ngay den ngay tren tung san pham trong nhom thiet bi
	 * 
	 * @author hunglm16
	 * @since May 22,2015
	 * 
	 * @param equipGroupId -> Id nhom thiet bi
	 * @param productCode -> Ma san pham
	 * @param fDateStr -> Ngay bat dau
	 * @param tDateStr -> Ngay ket thuc
	 * @param flag -> 1: Truong hop chung chom; 2: Truong hop toDate in DB is null
	 * */
	int checkFromDateToDateInEquipGroupProduct(Long equipGroupId, String productCode, String fDateStr, String tDateStr, int flag) throws BusinessException;

	/**
	 * Them moi san pham thiet bi
	 * 
	 * @author hunglm16
	 * @since May 22, 2015
	 * 
	 * @param equipGroupId -> id nhom thiet bi
	 * @param productCode -> ma san pham
	 * @param fromDate -> ngay bat dau
	 * @param toDate -> ngay ket thuc
	 * @param logInfo -> Cac truoc mac dinh
	 * */
	void addProductForEqupGroupProduct(Long equipGroupId, String productCode, Date fromDate, Date toDate, Boolean flagUpdate, LogInfoVO logInfo) throws BusinessException;
	/**
	 * dem so luong kho theo phan quyen kho
	 * 
	 * @author hunglm16
	 * @since May 26, 2015
	 * 
	 * @param staffRootId: id Nhan vien quyen thay the
	 * @param shopRootId: id don vi tuong tac he thong
	 * @param stockCode: ma kho
	 * */
	int countStockEquipForPermisionCSM(Long staffRootId, Long shopRootId, String stockCode) throws BusinessException;

	Equipment getEquipmentByCodeHasPermission(String code, Long staffRootId, Long shopRootId) throws BusinessException;
	/**
	 * get equip stock vo by filter for equipstocktrans
	 * 
	 * @author phuongvm
	 * @since 28/05/2015
	 * */
	ObjectVO<EquipStockVO> getListEquipStockVOByFilterForStockTrans(EquipStockFilter filter) throws BusinessException;
	/**
	 * Lay danh sach thiet bi
	 * 
	 * @author hunglm16
	 * @since May 27,2015
	 * @description ban chat gion getListEquipmentByFilter, toi uu hoa lai cau truy van
	 * */
	ObjectVO<EquipmentVO> getListEquipmentByFilterNew(EquipmentFilter<EquipmentVO> filter) throws BusinessException;
	/**
	 * Lay danh sach thiet bi co an theo phan quyen CMS
	 * 
	 * @author hunglm16
	 * @since May 27,2015
	 * @description filter.getShopRoot() is not null
	 * */
	ObjectVO<EquipmentManagerVO> getListEquipmentVOByShopInCMS(EquipmentFilter<EquipmentManagerVO> filter) throws BusinessException;

	/**
	 * Lay danh sach phieu sua chua; dung cho tao moi ds chi tiet cua phieu thanh toan
	 * @author vuongmq
	 * @since 24/06/2015
	 * @return
	 */
	List<EquipRepairForm> getListEquipRepairFormFilter(EquipRepairFilter filter) throws BusinessException;

	/**
	 * Lay danh sach chi tiet cua phieu thanh toan;
	 * @author vuongmq
	 * @since 24/06/2015
	 * @return
	 */
	List<EquipRepairPayFormDtl> getEquipmentRepairPaymentDtlFilter(EquipRepairFilter filter) throws BusinessException;
	/**
	 * Lay danh sach thiet bi cho kiem ke
	 * @author phuongvm
	 * @since 07/07/2015
	 * @return
	 */
	ObjectVO<EquipmentManagerVO> getListEquipmentVOByShopInCMSEx(
			EquipmentFilter<EquipmentManagerVO> filter)
			throws BusinessException;

	/**
	 * Lay loai thiet bi
	 * @author nhutnn
	 * @since 06/07/2015
	 * @param code
	 * @param activeType
	 * @return
	 * @throws BusinessException
	 */
	EquipCategory getEquipCategoryByCode(String code, ActiveType activeType) throws BusinessException;
	/**
	 * Lay danh sach nhom thiet bi theo Filter
	 * Danh cho bao cao Thiet bi
	 * 
	 * @author hunglm16
	 * @since July 10, 2015
	 * */
	List<EquipmentGroupVO> getListEquipGroupForFilterByRPT(EquipGFilter<EquipmentGroupVO> filter) throws BusinessException;
	
	/**
	 * Xuat bien ban hop dong giao nhan thiet bi tu Bien ben de nghi muon thiet bi
	 * @author nhutnn
	 * @since 09/07/2015
	 * @param filter
	 * @throws BusinessException
	 */
	List<EquipDeliveryRecord> exportListEquipDeliveryRecordExportByEquipLend(EquipProposalBorrowFilter<EquipBorrowVO> filter) throws BusinessException;

	/**
	 * Lay danh sach bien ban giao nhan co the xuat lai hop dong tu bien ban de nghi muon
	 * @author nhutnn
	 * @since 10/07/2015
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	List<EquipmentRecordDeliveryVO> getListDeliveryRecordVOForEquipLendByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> filter) throws BusinessException;
	
	/**
	 * Tao bien ban hop dong giao nhan thiet bi tu Bien ben de nghi muon thiet bi
	 * @author nhutnn
	 * @since 14/07/2015
	 * @param eqDeliveryRecord
	 * @return
	 * @throws BusinessException
	 */
	EquipDeliveryRecord createRecordDeliveryByEquipLend(EquipDeliveryRecord eqDeliveryRecord) throws BusinessException;
	
	/**
	 * Lay bien ban giao nhan
	 * @author nhutnn
	 * @since 23/07/2015
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	EquipmentRecordDeliveryVO getEquipmentRecordDeliveryVOByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> filter) throws BusinessException;
	
	/**
	 * Xuat bien ban thu hoi thiet bi tu Bien ben de nghi thu hoi thiet bi
	 * @author nhutnn
	 * @since 27/07/2015
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	List<EquipEvictionForm> exportListEquipEvictionExportByEquipSuggestEviction(EquipSuggestEvictionFilter<EquipSuggestEviction> filter) throws BusinessException;
	
	/**
	 * Lay danh sach bien ban thu hoi co the xuat lai hop dong tu bien ban de nghi thu hoi
	 * @author nhutnn
	 * @since 27/07/2015
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	List<EquipmentEvictionVO> getListFormEvictionVOForEquipSuggestEvictionByFilter(EquipmentFilter<EquipmentEvictionVO> filter)throws BusinessException;
	
	/**
	 * Tao bien ban thu hoi thiet bi tu Bien ben de nghi thu hoi thiet bi
	 * @author nhutnn
	 * @since 27/07/2015
	 * @param equipEvictionForm
	 * @param lstEquipments
	 * @return
	 * @throws BusinessException
	 */
	EquipEvictionForm createFormEvictionByEquipSuggestEviction(EquipEvictionForm equipEvictionForm, List<Equipment> lstEquipments) throws BusinessException;

	/**
	 * Lay EquipLiquidationForm theo id va shopId
	 * @author trietptm
	 * @param idForm
	 * @param shopId
	 * @return EquipLiquidationForm
	 * @throws BusinessException
	 * @since Apr 14, 2016
	*/
	EquipLiquidationForm getEquipLiquidationFormById(Long idForm, Long shopId) throws BusinessException;
}
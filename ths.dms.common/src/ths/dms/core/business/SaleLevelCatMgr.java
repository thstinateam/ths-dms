package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.entities.SaleLevelCat;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

public interface SaleLevelCatMgr {
	SaleLevelCat createSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws BusinessException;

	void deleteSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws BusinessException;

	void updateSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws BusinessException;
	
	SaleLevelCat getSaleLevelCatById(long id) throws BusinessException;

	ObjectVO<SaleLevelCat> getListSaleLevelCat(KPaging<SaleLevelCat> kPaging,
			Long catId, String catName, String saleLevel, ActiveType status)
			throws BusinessException;
	
	ObjectVO<SaleLevelCat> getListSaleLevelCatByProductInfo(
			KPaging<SaleLevelCat> kPaging, List<String> listProInfoCode,
			String catName, String saleLevel, BigDecimal fromAmount,
			BigDecimal toAmount, ActiveType status,String sort,String order) throws BusinessException;

	Boolean checkIfRecordExist(long catId, String saleLevel, Long id)
			throws BusinessException;

	SaleLevelCat getSaleLevelCatByCatAndSaleLevel(Long catId, String saleLevel)
			throws BusinessException;

	List<String> getListSaleLevel(Long catId, String catName, String saleLevel,
			ActiveType status) throws BusinessException;
}

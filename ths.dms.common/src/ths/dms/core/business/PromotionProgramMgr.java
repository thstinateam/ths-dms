package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ths.dms.core.entities.GroupLevel;
import ths.dms.core.entities.GroupMapping;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductGroup;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.PromotionCustAttr;
import ths.dms.core.entities.PromotionCustAttrDetail;
import ths.dms.core.entities.PromotionCustomerMap;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.PromotionStaffMap;
import ths.dms.core.entities.RptCTTLPay;
import ths.dms.core.entities.RptCTTLPayDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductGroupType;
import ths.dms.core.entities.enumtype.PromotionProgramFilter;
import ths.dms.core.entities.enumtype.PromotionShopMapFilter;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.filter.CalculatePromotionFilter;
import ths.dms.core.entities.filter.PromotionCustomerFilter;
import ths.dms.core.entities.filter.PromotionStaffFilter;
import ths.dms.core.entities.vo.ChannelTypeVO;
import ths.dms.core.entities.vo.ExMapping;
import ths.dms.core.entities.vo.ExcelPromotionHeader;
import ths.dms.core.entities.vo.GroupLevelVO;
import ths.dms.core.entities.vo.LevelMappingVO;
import ths.dms.core.entities.vo.ListGroupKM;
import ths.dms.core.entities.vo.ListGroupMua;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.MapMuaKM;
import ths.dms.core.entities.vo.NewLevelMapping;
import ths.dms.core.entities.vo.NewProductGroupVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PPConvertVO;
import ths.dms.core.entities.vo.ProductCatVO;
import ths.dms.core.entities.vo.ProductGroupVO;
import ths.dms.core.entities.vo.ProductInfoVO;
import ths.dms.core.entities.vo.PromotionCustAttUpdateVO;
import ths.dms.core.entities.vo.PromotionCustAttVO2;
import ths.dms.core.entities.vo.PromotionCustAttrVO;
import ths.dms.core.entities.vo.PromotionCustomerVO;
import ths.dms.core.entities.vo.PromotionItem;
import ths.dms.core.entities.vo.PromotionProductOpenVO;
import ths.dms.core.entities.vo.PromotionProductsVO;
import ths.dms.core.entities.vo.PromotionShopMapVO;
import ths.dms.core.entities.vo.PromotionShopVO;
import ths.dms.core.entities.vo.PromotionStaffVO;
import ths.dms.core.entities.vo.RptAccumulativePromotionProgramVO;
import ths.dms.core.entities.vo.SaleCatLevelVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO;
import ths.dms.core.entities.vo.ZV03View;
import ths.dms.core.entities.vo.ZV07View;
import ths.core.entities.vo.rpt.RptPromotionDetailLv01VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface PromotionProgramMgr {
	public static final String DUPLICATE_PRODUCT_IN_PRODUCT_GROUPS = "DUPLICATE_PRODUCT_IN_PRODUCT_GROUPS";
	public static final String DUPLICATE_LEVELS = "DUPLICATE_LEVELS";

	PromotionProgram createPromotionProgram(PromotionProgram promotionProgram, LogInfoVO logInfo) throws BusinessException;

	void updatePromotionProgram(PromotionProgram promotionProgram, LogInfoVO logInfo) throws BusinessException;

	void deletePromotionProgram(PromotionProgram promotionProgram, LogInfoVO logInfo) throws BusinessException;

	PromotionProgram getPromotionProgramByCode(String code) throws BusinessException;

	PromotionProgram getPromotionProgramByCodeByType(String code, ApParamType type) throws BusinessException;

	PromotionProgram getPromotionProgramById(Long id) throws BusinessException;

	PromotionShopMap createPromotionShopMap(PromotionShopMap promotionShopMap, LogInfoVO logInfo) throws BusinessException;

	void updatePromotionShopMap(PromotionShopMap promotionShopMap, LogInfoVO logInfo) throws BusinessException;

	PromotionShopMap getPromotionShopMapById(Long id) throws BusinessException;

	PromotionCustAttrDetail getPromotionCustAttrDetail(Long promotionCustAttrId, Long objectType, Long objectId) throws BusinessException;

	//lay danh sach cac thuoc tinh khach hang co the set cho CTKM
	List<PromotionCustAttrVO> getListPromotionCustAttrVOCanBeSet(KPaging<PromotionCustAttrVO> kPaging, long promotionProgramId) throws BusinessException;

	//lay danh sach cac thuoc tinh khach hang da set cho CTKM
	List<PromotionCustAttrVO> getListPromotionCustAttrVOAlreadySet(KPaging<PromotionCustAttrVO> kPaging, long promotionProgramId) throws BusinessException;

	// Muc doanh so theo nganh hang - khai bao muc
	List<ProductInfoVO> getListProductInfoVO() throws BusinessException;

	//Lay loai khac hang
	List<ChannelTypeVO> getListChannelTypeVO() throws BusinessException;

	// Muc doanh so theo nganh hang - gia tri moi nganh hang
	List<SaleCatLevelVO> getListSaleCatLevelVOByIdPro(Long id) throws BusinessException;

	// Combobox thuoc tinh dong cho type (4,5)
	List<AttributeDetailVO> getListPromotionCustAttVOCanBeSet(Long id) throws BusinessException;

	//Lay loai khach hang da dc set
	List<ChannelTypeVO> getListChannelTypeVOAlreadySet(KPaging<ChannelTypeVO> kPaging, long promotionProgramId) throws BusinessException;

	//Muc doanh so theo nganh hang - da duoc set mt
	List<SaleCatLevelVO> getListSaleCatLevelVOByIdProAlreadySet(KPaging<SaleCatLevelVO> kPaging, long promotionProgramId) throws BusinessException;

	//Muc doanh so theo nganh hang -da duoc set so
	List<SaleCatLevelVO> getListSaleCatLevelVOByIdProAlreadySetSO(KPaging<SaleCatLevelVO> kPaging, long promotionProgramId) throws BusinessException;

	//Value cua thuoc tinh dong type (1,2,3)
	List<PromotionCustAttVO2> getListPromotionCustAttVOValue(long promotionProgramId) throws BusinessException;

	//Value cua thuoc tinh dong type (4,5)
	List<PromotionCustAttVO2> getListPromotionCustAttVOValueDetail(long promotionProgramId) throws BusinessException;

	//Khi nhan nut cap nhat - tab khach hang 
	void createOrUpdatePromotionCustAttVO(Long promotionProgramId, List<PromotionCustAttUpdateVO> lst, LogInfoVO logInfo) throws BusinessException;

	Boolean checkIfPromotionCustomerMapExist(long promotionProgramId, String customerCode, Long id) throws BusinessException;

	Boolean checkIfPromotionShopMapExist(long promotionProgramId, String shopCode, Long id) throws BusinessException;

	Boolean createListPromotionShopMap(List<PromotionShopMap> lstPromotionShopMap, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Lay du lieu cho bao cao chi tiet khuyen mai
	 * 
	 * @param shopId
	 *            Id shop
	 * @param staffIds
	 *            List nhan vien BH
	 * @param fromDate
	 *            Ngay bat dau chuong trinh khuyen mai
	 * @param toDate
	 *            Ngay ket thuc chuong trinh khuyen mai
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	List<RptPromotionDetailLv01VO> getDataForPromotionDetailReport(Long shopId, List<Long> staffIds, Date fromDate, Date toDate) throws BusinessException;

	Boolean checkPromotionShopMapInCustomerMap(Long promotionShopMapId) throws BusinessException;

	Boolean checkProductInOtherPromotionProgram(String productCode, Long promotionProgramId, Long promotionProgramDetailId) throws BusinessException;

	PromotionShopMap getPromotionShopMap(Long shopId, Long promotionProgramId) throws BusinessException;

	Boolean checkProductInOtherPromotionProgramEx(String productCode, Date fromDate, Date toDate, Long promotionProgramId) throws BusinessException;

	Boolean checkIfPromotionCustomerMapExistEx(long promotionProgramId, String channelTypeCode, String shopCode, Long id) throws BusinessException;

	ObjectVO<PromotionShopMap> getListPromotionShopMap(KPaging<PromotionShopMap> kPaging, Long promotionProgramId, String shopCode, String shopName, Integer shopQuantity, ActiveType status, Long parentShopId) throws BusinessException;

	List<ProductCatVO> getListProductCatVOEx(String productCode, String productName, Long exPpId) throws BusinessException;

	List<ProductInfo> getListSubCat(String productCode, String productName, Long exPpId, Long catId) throws BusinessException;

	List<Product> getListProductBySubCat(String productCode, String productName, Long exPpId, Long catId, Long subCatId) throws BusinessException;

	PromotionShopMap checkPromotionShopMap(Long promotionProgramId, String shopCode, ActiveType status, Date lockDay) throws BusinessException;

	List<PromotionShopMap> getListPromotionShopMapWithListPromotionId(List<Long> listPromotionProgramId) throws BusinessException;

	/**
	 * 
	 * kiem tra xem chuong trinh khuyen mai o shop cha da ton tai hay chua
	 * 
	 * @author: thanhnn
	 * @param promotionProgramid
	 * @param shopCode
	 * @return
	 * @throws BusinessException
	 * @return: Boolean
	 * @throws:
	 */
	Boolean checkExitsPromotionWithParentShop(Long promotionProgramid, String shopCode) throws BusinessException;

	ObjectVO<ZV03View> getZV03ViewHeader(KPaging<ZV03View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV03View> getZV03ViewDetail(KPaging<ZV03View> paging, Long promotionProgramId, Long productId, Integer quant) throws BusinessException;

	ObjectVO<ZV03View> getZV06ViewHeader(KPaging<ZV03View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV03View> getZV06ViewDetail(KPaging<ZV03View> paging, Long promotionProgramId, Long productId, Long amount) throws BusinessException;

	ObjectVO<ZV07View> getZV07ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV07ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, Float discountPercent, Integer quantity) throws BusinessException;

	ObjectVO<ZV07View> getZV08ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV08ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, BigDecimal discountAmount, Integer quantity) throws BusinessException;

	PromotionCustomerMap createPromotionCustomerMap(PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo) throws BusinessException;

	void updatePromotionCustomerMap(PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo) throws BusinessException;

	void createListPromotionCustomerMap(PromotionShopMap sm, List<PromotionCustomerMap> lstPCM, LogInfoVO logInfo) throws BusinessException;

	void createListPromotionCustomerMapEx(List<PromotionShopMap> promotionShopMaps, List<PromotionCustomerMap> lstPCM, LogInfoVO logInfo) throws BusinessException;

	void deletePromotionCustomerMap(PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo) throws BusinessException;

	PromotionCustomerMap getPromotionCustomerMapById(Long id) throws BusinessException;

	PromotionCustomerMap getPromotionCustomerMap(Long promotionShopMapId, Long customerId) throws BusinessException;

	ObjectVO<PromotionCustomerMap> getListPromotionCustomerMap(KPaging<PromotionCustomerMap> kPaging, Long promotionProgramId, String shopCode, Long channelTypeId, String shortCode, Integer quantityMax, ActiveType status, String channelTypeName,
			Integer quantityReceived, String customerName, String channelTypeCode) throws BusinessException;

	PromotionCustomerMap checkPromotionCustomerMap(Long promotionProgramId, String shopCode, String shortCode, String channelTypeCode) throws BusinessException;

	ObjectVO<ZV07View> getZV09ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV09ViewFreeProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, Integer quantity) throws BusinessException;

	ObjectVO<ZV07View> getZV09ViewSaleProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, Integer quantity) throws BusinessException;

	ObjectVO<ZV07View> getZV10ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV10ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, BigDecimal amount) throws BusinessException;

	ObjectVO<ZV07View> getZV11ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV11ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, BigDecimal amount) throws BusinessException;

	ObjectVO<ZV07View> getZV12ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV12ViewFreeProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, BigDecimal amount) throws BusinessException;

	ObjectVO<ZV07View> getZV12ViewSaleProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, BigDecimal amount) throws BusinessException;

	ObjectVO<ZV07View> getZV13ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV13ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, Float discountPercent) throws BusinessException;

	ObjectVO<ZV07View> getZV14ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV14ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, BigDecimal discountAmount) throws BusinessException;

	void deletePromotionShopMap(long promotionId, long shopId, LogInfoVO logInfo) throws BusinessException;

	ObjectVO<ZV07View> getZV16ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV16ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, Float discountPercent) throws BusinessException;

	ObjectVO<ZV07View> getZV15ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV15ViewFreeProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, String productCodeStr) throws BusinessException;

	ObjectVO<ZV07View> getZV15ViewSaleProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, String freeProductCodeStr) throws BusinessException;

	ObjectVO<ZV07View> getZV18ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV18ViewFreeProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, String productCodeStr) throws BusinessException;

	ObjectVO<ZV07View> getZV18ViewSaleProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, String freeProductCodeStr) throws BusinessException;

	ObjectVO<ZV07View> getZV21ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, BigDecimal amount) throws BusinessException;

	ObjectVO<ZV07View> getZV17ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV17ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, BigDecimal discountAmount) throws BusinessException;

	ObjectVO<ZV07View> getZV21ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV01ViewHeader(Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV02ViewHeader(Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV04ViewHeader(Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV05ViewHeader(Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV19ViewHeader(Long promotionProgramId) throws BusinessException;

	ObjectVO<ZV07View> getZV20ViewHeader(Long promotionProgramId) throws BusinessException;

	ObjectVO<PromotionCustomerMap> getListPromotionCustomerMapByShopAndPP(KPaging<PromotionCustomerMap> paging, Long shopId, Long promotionProgramId, String customerCode, String customerName, String address) throws BusinessException;

	PromotionCustAttr getPromotionCustAttrByPromotion(long promotionId, Integer objectType, Long objectId) throws BusinessException;

	List<PromotionProgram> getListPromotionProgramByCustomerType(Long customerTypeId) throws BusinessException;

	ObjectVO<PromotionProgram> getListPromotionProgram(PromotionProgramFilter filter) throws BusinessException;
	
	ObjectVO<PromotionProgram> getListDefaultPromotionProgram(PromotionProgramFilter filter) throws BusinessException;

	Boolean isExistChildJoinProgram(String shopCode, Long promotionProgramId) throws BusinessException;

	List<PromotionShopMapVO> getListPromotionShopMapVO(PromotionShopMapFilter filter, KPaging<PromotionShopMapVO> kPaging) throws BusinessException;

	Boolean checkExistCustomerMap(Long id) throws BusinessException;

	/**
	 * Kiem tra CTKM co chua san pham thuoc CTKM khac dang hoat dong && khoang
	 * thoi gian hieu luc && don vi tham gia
	 * 
	 * @author lochp function: checkExistProductTimeShopInActive() throws
	 *         BusinessException
	 */
	Boolean checkExistProductTimeShopInActive(Long promotionProgramId, String startDate, String endDate) throws BusinessException;

	Boolean checkCTHMShopMap(String promotionProgramCode, Long shopId, ActiveType status, Date lockDay) throws BusinessException;

	/**
	 * Lay danh sach don vi cho CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 15, 2014
	 */
	List<PromotionShopVO> getShopTreeInPromotionProgram(long shopId, long promotionProgramId, String shopCode, String shopName, Integer quantity) throws BusinessException;

	/**
	 * 
	 * @param filter
	 * @return Lay danh sach khach hang cho CTKM
	 * @throws BusinessException
	 * @author trietptm
	 * @since Aug 03, 2015
	 */
	ObjectVO<PromotionCustomerVO> getCustomerInPromotionProgram(PromotionCustomerFilter filter) throws BusinessException;

	void importPromotionVNM(List<ExcelPromotionHeader> listHeader, Map<String, ListGroupMua> mapPromotionMua, Map<String, ListGroupKM> mapPromotionKM, MapMuaKM mapMuaKM, List<PromotionShopVO> lstPromotionShop, LogInfoVO logInfo)
			throws BusinessException;

	/**
	 * Lay danh sach so suat NVBH cho CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 16, 2014
	 */
	List<PromotionStaffVO> getSalerInPromotionProgram(long promotionId, String shopCode, String shopName, Integer quantity) throws BusinessException;

	/**
	 * 
	 * @param filter
	 * @return Lay danh sach khach hang popup them KH vao CTKM
	 * @throws BusinessException
	 * @author trietptm
	 * @since Aug 03, 2015
	 */
	ObjectVO<PromotionCustomerVO> searchCustomerForPromotionProgram(PromotionCustomerFilter filter) throws BusinessException;

	/**
	 * Kiem tra xem KH co ton tai trong CTKM chua
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 */
	List<PromotionCustomerVO> getListCustomerInPromotion(long promotionId, List<Long> lstCustId) throws BusinessException;

	/**
	 * Kiem tra xem don vi+don vi con co ton tai trong CTKM chua
	 * 
	 * @author lacnv1
	 * @since Aug 20, 2014
	 */
	List<PromotionShopVO> getListShopInPromotion(long promotionId, List<Long> lstShopId, boolean shopMapOnly) throws BusinessException;

	/**
	 * Lay promotion_staff_map theo id
	 * 
	 * @author lacnv1
	 * @since Aug 26, 2014
	 */
	PromotionStaffMap getPromotionStaffMapById(long id) throws BusinessException;

	/**
	 * cap nhat promotion_staff_map
	 * 
	 * @author lacnv1
	 * @since Aug 26, 2014
	 */
	void updatePromotionStaffMap(PromotionStaffMap promotionStaffMap, LogInfoVO logInfo) throws BusinessException;

	/**
	 * cap nhat promotion_staff_map
	 * 
	 * @author lacnv1
	 * @since Aug 26, 2014
	 */
	PromotionStaffMap createPromotionStaffMap(PromotionStaffMap promotionStaffMap, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Xoa promotion_staff_map theo CTKM va don vi
	 * 
	 * @author lacnv1
	 * @since Aug 26, 2014
	 */
	void removePromotionStaffMap(long promotionId, long shopId, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Tim kiem NVBH them vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	List<PromotionStaffVO> searchStaffForPromotion(PromotionStaffFilter filter) throws BusinessException;

	/**
	 * Kiem tra xem NVBH co ton tai trong CTKM chua
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	List<PromotionStaffVO> getListStaffInPromotion(long promotionId, List<Long> lstStaffId) throws BusinessException;

	/**
	 * Lay shop_map cha cua shop_id
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	PromotionShopMap getPromotionParentShopMap(long promotionId, long shopId, Integer status) throws BusinessException;

	/**
	 * Tao promotion_staff_map
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	void createListPromotionStaffMap(List<PromotionStaffMap> lst, LogInfoVO logInfo) throws BusinessException;

	List<ProductGroup> getListProductGroupByPromotionId(Long promotionId, ProductGroupType groupType) throws BusinessException;

	ProductGroup getProductGroupByCode(String productGroup, ProductGroupType groupType, Long promotionProgramId) throws BusinessException;

	ProductGroup createProductGroup(Long promotionId, String groupCode, String groupName, ProductGroupType groupType, Integer minQuantity, Integer maxQuantity, BigDecimal minAmount, BigDecimal maxAmount, Boolean multiple, Boolean recursive,
			Integer stt, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Lay danh sach so suat NVBH
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	List<PromotionShopMapVO> getListPromotionShopMapVO2(PromotionShopMapFilter filter) throws BusinessException;

	/**
	 * Lay promotion_staff_map theo promotion_shop_map_id
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	PromotionStaffMap getPromotionStaffMapByShopMapAndStaff(long shopMapId, long staffId) throws BusinessException;

	void deleteProductGroup(ProductGroup productGroup, LogInfoVO logInfo) throws BusinessException;

	ProductGroup getProductGroupById(Long groupId) throws BusinessException;

	List<GroupLevelVO> getListGroupLevelVO(Long groupId) throws BusinessException;

	/**
	 * Lay ds san pham khuyen mai tu san pham ban
	 * 
	 * @author: NhanLT6
	 * @param calculatePromotionFilter
	 * @return PromotionProductsVO
	 * @since 11/09/2014
	 */
	PromotionProductsVO calculatePromotionProducts2(CalculatePromotionFilter calculatePromotionFilter) throws BusinessException;

	void updateProductGroup(ProductGroup productGroup, LogInfoVO logInfo) throws BusinessException;

	void addLevel2SaleProductGroup(ProductGroup productGroup, List<Long> listLevelId, List<Integer> listMinQuantity, List<BigDecimal> listMinAmount, List<Integer> listOrder, List<String> listProductDetail, LogInfoVO logInfo) throws BusinessException;

	void addLevel2FreeProductGroup(ProductGroup productGroup, List<Long> listLevelId, List<Integer> listMinQuantity, List<Integer> listOrder, List<BigDecimal> listMaxAmount, List<Float> listPercent, List<String> listProductDetail, LogInfoVO logInfo)
			throws BusinessException;

	/**
	 * Lay CTKM theo sp va khach hang và NVBH
	 * 
	 * @author: NhanLT6
	 * @param productId
	 * @param shopId
	 * @param customerId
	 * @param orderDate
	 * @param saleOrderId
	 * @return: PromotionProgram
	 * @since 11/09/2014
	 */
	List<PromotionProgram> getPromotionProgramByProductAndShopAndCustomer(Long productId, long shopId, long customerId, Date orderDate, Long saleOrderId, Long staffId) throws BusinessException;

	List<LevelMappingVO> getListLevelMappingByGroupId(Long promotionId, Long groupMuaId, Long groupKMId) throws BusinessException;

	List<ProductGroupVO> getListProductGroupVO(Long promotionId, ProductGroupType groupType) throws BusinessException;

	void deleteGroupMapping(GroupMapping groupMapping) throws BusinessException;

	GroupMapping getGroupMappingByGroupCodeAndOrder(Long promotionId, String groupMuaCode, Integer orderLevelMua, String groupKMCode, Integer orderLevelKM) throws BusinessException;

	GroupMapping getGroupMappingById(Long id) throws BusinessException;

	GroupMapping createGroupMappingByGroupCodeAndOrder(Long promotionId, String groupMuaCode, Integer orderLevelMua, String groupKMCode, Integer orderLevelKM, LogInfoVO logInfo) throws BusinessException;

	List<GroupMapping> getGroupMappingByGroupCodeAndOrderMua(Long promotionId, String groupMuaCode, Integer orderLevelMua) throws BusinessException;

	List<GroupMapping> getListGroupMappingByLevelId(Long levelMuaId, Long levelKMId) throws BusinessException;

	void deleteGroupLevel(Long levelId, LogInfoVO logInfo) throws BusinessException;

	GroupLevel getGroupLevelByLevelId(Long levelId) throws BusinessException;

	void deleteLevelDetail(Long levelDetailId, LogInfoVO logInfo) throws BusinessException;

	/**
	 * get promotive product to change when editing sale order
	 * 
	 * @author tuannd20
	 * @param shopId
	 * @param saleOrderId
	 * @param productCode
	 * @param programCode
	 * @param gotPromoLevel
	 * @return
	 * @throws BusinessException
	 * @date 10/10/2014
	 */
	List<SaleOrderDetailVO> getChangePromotionProductForEditingSaleOrder(Long shopId, Long saleOrderId, String productCode, String programCode, Integer gotPromoLevel) throws BusinessException;

	List<SaleOrderDetailVO> getChangePromotionProductForEditingSaleOrder2(Long shopId, Long saleOrderId, String productCode, String programCode, Long promoLevelId, Integer gotPromoLevel) throws BusinessException;

	List<GroupLevel> getListLevelNotInMapping(Long promotionId) throws BusinessException;

	PromotionProgram checkProductExistInOrPromotion(Long promotionId) throws BusinessException;

	List<NewProductGroupVO> getListNewProductGroupByPromotionId(Long promotionId) throws BusinessException;

	ObjectVO<NewLevelMapping> getListMappingLevel(Long groupMuaId, Long groupKMId, Integer fromLevel, Integer toLevel) throws BusinessException;

	NewLevelMapping newAddLevel(Long groupMuaId, Long groupKMId, String levelCode, Integer stt, LogInfoVO logInfo) throws BusinessException;

	NewLevelMapping newUpdateLevel(Long groupMuaId, Long groupKMId, Long levelMuaId, Long levelKMId, String levelCode, Integer stt, LogInfoVO logInfo) throws BusinessException;

	GroupLevel getGroupLevelByLevelCode(Long groupId, String levelCode, Integer orderNumber) throws BusinessException;

	void newDeleteLevel(Long mapId, Long levelMuaId, Long levelKMId, LogInfoVO logInfo) throws BusinessException;

	void newDeleteSubLevel(Long levelId, LogInfoVO logInfo) throws BusinessException;

	Map newSaveSublevel(Long mappingId, Integer stt, List<ExMapping> listSubLevelMua, List<ExMapping> listSubLevelKM, LogInfoVO logInfo) throws BusinessException;

	List<NewLevelMapping> newCopyLevel(Long mappingId, Integer copyNum, LogInfoVO logInfo) throws BusinessException;

	/**
	 * Lay danh sach san pham tich luy cua khach hang theo CTKM
	 * 
	 * @author tungmt
	 * @return ds CTTL
	 * @throws BusinessException
	 * @date 24/12/2014
	 */
	RptAccumulativePromotionProgramVO getRptAccumulativePPForCustomer(Long shopId, Long customerId, Long promotionId, Date orderDate) throws BusinessException;

	/**
	 * tinh KM tich luy khi KH tao don hang
	 * 
	 * @author tuannd20
	 * @param shopId
	 * @param customerId
	 * @param staffId
	 * @param orderDate
	 * @return ds KM ma KH nhan duoc
	 * @throws BusinessException
	 * @date 10/12/2014
	 */
	List<PromotionItem> calculatePromotionZV23(Long shopId, Long customerId, Long staffId, Date orderDate, Long exceptOrderId) throws BusinessException;

	List<PPConvertVO> listPromotionProductConvertVO(Long promotionId, String name) throws BusinessException;

	void savePPConvert(Long promotionId, List<PPConvertVO> listVO, LogInfoVO logInfo) throws BusinessException;

	void deletePPConvert(Long id, LogInfoVO logInfo) throws BusinessException;

	void deletePPConvertDetail(Long id, LogInfoVO logInfo) throws BusinessException;

	List<PromotionProductOpenVO> listPromotionProductOpenVO(Long promotionId) throws BusinessException;

	void deletePromotionProductOpen(Long id) throws BusinessException;

	void saveListPromotionProductOpen(Long promotionId, List<PromotionProductOpenVO> listProductOpen, LogInfoVO logInfo) throws BusinessException;

	Integer checkLevel(Long mappingId, List<ExMapping> listSubLevelMua, List<ExMapping> listSubLevelKM) throws BusinessException;

	RptCTTLPay createRptCTTLPay(RptCTTLPay rptCTTLPay, LogInfoVO logInfo) throws BusinessException;

	RptCTTLPay getRptCTTLPayById(Long id) throws BusinessException;

	RptCTTLPayDetail createRptCTTLPayDetail(RptCTTLPayDetail rptCTTLPayDetail, LogInfoVO logInfo) throws BusinessException;

	RptCTTLPayDetail getRptCTTLPayDetailById(Long id) throws BusinessException;

	/**
	 * kiem tra cau truc cua cac CTKM ma DH dang huong co thay doi cau truc hay
	 * ko
	 * 
	 * @author tuannd20
	 * @param saleOrderId
	 * @return true: co it nhat 1 CTKM thay doi cau truc. Nguoc lai, false
	 * @throws BusinessException
	 * @date 20/12/2014
	 */
	Boolean isSaleOrderPromotionProgramStructureHasChanged(Long saleOrderId) throws BusinessException;

	/**
	 * lay danh sach CTKM ma DH duoc huong, da thay doi cau truc
	 * 
	 * @author tuannd20
	 * @param saleOrderId
	 * @return
	 * @throws BusinessException
	 * @date 22/12/2014
	 */
	List<String> getChangedStructurePromotionProgramInSaleOrder(Long saleOrderId) throws BusinessException;

	/**
	 * lay danh sach cac DH co CTKM bi thay doi co cau.
	 * OrderProductVO.promotionProgramCode: cac CTKM thay doi co cau cua DH,
	 * cach nhau bang dau ','
	 * 
	 * @author tuannd20
	 * @param saleOrderIds
	 * @return danh sach DH co cau thay doi
	 * @throws BusinessException
	 * @date 23/12/2014
	 */
	List<OrderProductVO> getSaleOrdersHasChangedStructurePromotionProgram(List<Long> saleOrderIds) throws BusinessException;

	/**
	 * lay danh sach cac DH co CTKM bi thay doi co cau.
	 * OrderProductVO.promotionProgramCode: cac CTKM thay doi co cau cua DH,
	 * cach nhau bang dau ','
	 * 
	 * @author tuannd20
	 * @param soFilter
	 * @return danh sach DH co cau thay doi
	 * @throws BusinessException
	 * @date 23/12/2014
	 */
	List<OrderProductVO> getSaleOrdersHasChangedStructurePromotionProgramWithFilter(SoFilter soFilter) throws BusinessException;

	List<Product> getListProductInSaleLevel(Long promotionId) throws BusinessException;

	Boolean checkCoCau(Long promotionId) throws BusinessException;

	/**
	 * Lay thong tin khuyen mai tich luy cua don hang
	 * 
	 * @author lacnv1
	 * @since Dec 31, 2014
	 */
	List<PromotionItem> getAccumulationInfoOfOrder(long saleOrderId) throws BusinessException;

	/**
	 * Kiem tra xem CTKM co thuoc loai co so suat hay khong Tra ve: true: co so
	 * suat, nguoc lai: false
	 * 
	 * @author lacnv1
	 * @since Jan 28, 2015
	 */
	boolean checkPromotionProgramHasPortion(String programCode, long shopId, long customerId, long staffId, Date lockDate) throws BusinessException;

	List<PromotionShopMapVO> getListPromotionShopMapVO3(PromotionShopMapFilter filter) throws BusinessException;

	/**
	 * Lay so suat con lai cua CTKM
	 * 
	 * @author lacnv1
	 * @return so suat con lai
	 * @throws DataAccessException
	 * @since Feb 10, 2015
	 */
	Integer getQuantityRemainOfPromotionProgram(long promotionId, long shopId, long staffId, long customerId, Date lockDate) throws BusinessException;

	/**
	 * Lay so tien tra con lai cua CTKM
	 * @author trietptm
	 * @param promotionCode
	 * @param shopId
	 * @param staffId
	 * @param customerId
	 * @param lockDate
	 * @return so suat con lai
	 * @throws BusinessException
	 * @since Feb 04, 2015
	 */
	BigDecimal getAmountRemainOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId, Date lockDate) throws BusinessException;

	/**
	 * Lay so luong tra con lai cua CTKM
	 * @author trietptm
	 * @param promotionCode
	 * @param shopId
	 * @param staffId
	 * @param customerId
	 * @param lockDate
	 * @return so suat con lai
	 * @throws BusinessException
	 * @since Sep 04, 2015
	 */
	Integer getNumRemainOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId, Date lockDate) throws BusinessException;

	/**
	 * update cot md5validCode
	 * 
	 * @author tungmt
	 * @throws DataAccessException
	 * @since 24/02/15
	 */
	void updateMD5ValidCode(PromotionProgram pp, LogInfoVO logInfo) throws BusinessException;

	Integer getMaxOrderNumberOfGroupLevel(Long groupProductId) throws BusinessException;

	/**
	 * Tim kiem don vi trong CTKM
	 * 
	 * @param promotionId
	 *            , shopCode, shopName, quantity, isLevel
	 * @author hunglm16
	 * @since March 24,2015
	 * */
	List<PromotionShopVO> searchShopTreeInPromotionProgramImpr(PromotionShopMapFilter filter) throws BusinessException;

	/**
	 * Lay danh sach promotionShopMap
	 * 
	 * @author nhutnn
	 * @since 23/04/2015
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	List<PromotionShopMap> getPromotionChildrenShopMap(PromotionShopMapFilter filter) throws BusinessException;

	/**
	 * kiem tra ton tai PromotionShopMap theo phan quyen
	 * 
	 * @param strListShopId
	 * @param promotionProgramId
	 * @return
	 * @throws BusinessException
	 */
	boolean checkExistPromotionShopMapByListShop(String strListShopId, Long promotionProgramId) throws BusinessException;

	/**
	 * 
	 * @param lstId
	 * @param promotionId
	 * @return danh sach PromotionShopMap theo id khach hang
	 * @throws BusinessException
	 * @author trietptm
	 * @since Aug 03, 2015
	 */
	List<PromotionShopMap> getPromotionShopMapByCustomer(List<Long> lstId, Long promotionId) throws BusinessException;
	
	/**
	 * Cap nhat thu tu muc cho nhom san pham ban
	 * 
	 * @author hunglm16
	 * @version
	 * @param promotionId: id CTKM can cap nhat
	 * @param productGroupId: nhom san pham ban can cap nhat
	 * @param logInfo : thong tin dang nhap
	 * @throws DataAccessException
	 * @since September 08, 2015
	 */
	void updateGroupLevelOrderNumber(long promotionId, long productGroupId, LogInfoVO logInfo) throws BusinessException;
	/**
	 * Lay danh sach don vi tham gia CTKM
	 * @author hunglm16
	 * @param filter
	 * @param kPaging
	 * @return
	 * @throws DataAccessException
	 * @since 25/09/2015
	 */
	List<PromotionShopMapVO> getPromotionShopMapVOByFilter(PromotionShopMapFilter filter, KPaging<PromotionShopMapVO> kPaging) throws BusinessException;

	/**
	 * Lay danh sach don vi popup them don vi vao CTKM
	 * @author hunglm16
	 * @param promotionId
	 * @param pShopId
	 * @param shopCode
	 * @param shopName
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return
	 * @throws BusinessException
	 * @sice 26/11/2015
	 */
	List<PromotionShopVO> searchShopForPromotionProgram(long promotionId, long pShopId, String shopCode, String shopName, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException;
	
	List<PromotionCustomerMap> getListPromotionCustomerMapEntity(Long customerID, PromotionShopMap promotionShopMap) throws BusinessException;
	
	List<PromotionStaffMap> getListPromotionStaffMapAddPromotionStaff(Long promotionShopMapId, Long staffId, Long shopId) throws BusinessException;

	/**
	 * 
	 * Xu ly getAmountRemainApprovedOfPromotionProgram
	 * @author vuongmq
	 * @param promotionCode
	 * @param shopId
	 * @param staffId
	 * @param customerId
	 * @param lockDate
	 * @return BigDecimal
	 * @throws BusinessException
	 * @since Apr 1, 2016
	 */
	BigDecimal getAmountRemainApprovedOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId, Date lockDate) throws BusinessException;

	/**
	 * 
	 * Xu ly getNumRemainApprovedOfPromotionProgram
	 * @author vuongmq
	 * @param promotionCode
	 * @param shopId
	 * @param staffId
	 * @param customerId
	 * @param lockDate
	 * @return Integer
	 * @throws BusinessException
	 * @since Apr 1, 2016
	 */
	Integer getNumRemainApprovedOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId, Date lockDate) throws BusinessException;
}
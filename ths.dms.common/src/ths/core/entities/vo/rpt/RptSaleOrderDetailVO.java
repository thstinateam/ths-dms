package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

public class RptSaleOrderDetailVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal soId;
	private BigDecimal sodId;
	private String productCode;
	private String productName;
	private BigDecimal convfact;
	private BigDecimal price;
	private BigDecimal quantity;
	private BigDecimal amount;
	private String promotionName;
	private String promotionCode;
	private String carNumber;
	private String customerName;
	private String customerCode;
	private String customerShortCode;
	private String customerAddress;
	private String customerPhone;
	private String customerMobiphone;
	private String shopName;
	private String shopPhone;
	private String shopTaxNum;
	private String shopAddress;
	private String staffCode;
	private String staffName;
	private String deliveryCode;
	private String deliveryName;
	private Date orderDate;
	private String invoiceNumber;
	private BigDecimal invoiceId;
	private BigDecimal numThung;
	private BigDecimal numLe;
	
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public BigDecimal getSoId() {
		return soId;
	}

	public void setSoId(BigDecimal soId) {
		this.soId = soId;
	}
	public BigDecimal getSodId() {
		return sodId;
	}

	public void setSodId(BigDecimal sodId) {
		this.sodId = sodId;
	}

	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public BigDecimal getConvfact() {
		return convfact;
	}

	public void setConvfact(BigDecimal convfact) {
		this.convfact = convfact;
	}

	public String getPromotionCode() {
		return promotionCode;
	}
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	public String getCarNumber() {
		return carNumber;
	}
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getPromotionName() {
		return promotionName;
	}
	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerShortCode() {
		return customerShortCode;
	}
	public void setCustomerShortCode(String customerShortCode) {
		this.customerShortCode = customerShortCode;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public BigDecimal getNumThung() {
		return numThung;
	}

	public void setNumThung(BigDecimal numThung) {
		this.numThung = numThung;
	}

	public BigDecimal getNumLe() {
		return numLe;
	}

	public void setNumLe(BigDecimal numLe) {
		this.numLe = numLe;
	}

	public String getShopPhone() {
		return shopPhone;
	}
	public void setShopPhone(String shopPhone) {
		this.shopPhone = shopPhone;
	}
	public String getShopTaxNum() {
		return shopTaxNum;
	}
	public void setShopTaxNum(String shopTaxNum) {
		this.shopTaxNum = shopTaxNum;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public BigDecimal getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(BigDecimal invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getCustomerMobiphone() {
		return customerMobiphone;
	}

	public void setCustomerMobiphone(String customerMobiphone) {
		this.customerMobiphone = customerMobiphone;
	}
		
}

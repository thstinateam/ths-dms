package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptPromotionDetailLv01VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String promotionCode;
	private String promotionName;
	private List<RptPromotionDetailDataVO> listData;

	public RptPromotionDetailLv01VO() {
		this.listData = new ArrayList<RptPromotionDetailDataVO>();
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public List<RptPromotionDetailDataVO> getListData() {
		return listData;
	}

	public void setListData(List<RptPromotionDetailDataVO> listData) {
		this.listData = listData;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 1.lop VO dung de mapping thong tin tu du lieu truy van
 * 2. Dung cho bao cao phieu xuat kho kiem van chuyen noi bo
 * @author cangnd
 *
 */

public class RptPXKKVCNB_DataConvert implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date ngay_hien_tai;

	private String ten_nguoi_van_chuyen;
	private String don_vi_ban_hang;
	private String ma_so_thue;

	private String dia_chi_kho;
	private String dia_chi_nhan_vien;
	private String so_dien_thoai;
	private String so_tai_khoan;
	private String so_xe;
	
	private String ma_san_pham;
	private String ten_san_pham;
	
	private BigDecimal so_thung;
	private BigDecimal so_le;
	private BigDecimal don_gia;
	private BigDecimal thanh_tien;
	public Date getNgay_hien_tai() {
		return ngay_hien_tai;
	}
	public void setNgay_hien_tai(Date ngay_hien_tai) {
		this.ngay_hien_tai = ngay_hien_tai;
	}
	public String getTen_nguoi_van_chuyen() {
		return ten_nguoi_van_chuyen;
	}
	public void setTen_nguoi_van_chuyen(String ten_nguoi_van_chuyen) {
		this.ten_nguoi_van_chuyen = ten_nguoi_van_chuyen;
	}
	public String getDon_vi_ban_hang() {
		return don_vi_ban_hang;
	}
	public void setDon_vi_ban_hang(String don_vi_ban_hang) {
		this.don_vi_ban_hang = don_vi_ban_hang;
	}
	public String getMa_so_thue() {
		return ma_so_thue;
	}
	public void setMa_so_thue(String ma_so_thue) {
		this.ma_so_thue = ma_so_thue;
	}
	public String getDia_chi_kho() {
		return dia_chi_kho;
	}
	public void setDia_chi_kho(String dia_chi_kho) {
		this.dia_chi_kho = dia_chi_kho;
	}
	public String getDia_chi_nhan_vien() {
		return dia_chi_nhan_vien;
	}
	public void setDia_chi_nhan_vien(String dia_chi_nhan_vien) {
		this.dia_chi_nhan_vien = dia_chi_nhan_vien;
	}
	public String getSo_dien_thoai() {
		return so_dien_thoai;
	}
	public void setSo_dien_thoai(String so_dien_thoai) {
		this.so_dien_thoai = so_dien_thoai;
	}
	public String getSo_tai_khoan() {
		return so_tai_khoan;
	}
	public void setSo_tai_khoan(String so_tai_khoan) {
		this.so_tai_khoan = so_tai_khoan;
	}
	public String getSo_xe() {
		return so_xe;
	}
	public void setSo_xe(String so_xe) {
		this.so_xe = so_xe;
	}
	public String getMa_san_pham() {
		return ma_san_pham;
	}
	public void setMa_san_pham(String ma_san_pham) {
		this.ma_san_pham = ma_san_pham;
	}
	public String getTen_san_pham() {
		return ten_san_pham;
	}
	public void setTen_san_pham(String ten_san_pham) {
		this.ten_san_pham = ten_san_pham;
	}
	public BigDecimal getSo_thung() {
		return so_thung;
	}
	public void setSo_thung(BigDecimal so_thung) {
		this.so_thung = so_thung;
	}
	public BigDecimal getSo_le() {
		return so_le;
	}
	public void setSo_le(BigDecimal so_le) {
		this.so_le = so_le;
	}
	public BigDecimal getDon_gia() {
		return don_gia;
	}
	public void setDon_gia(BigDecimal don_gia) {
		this.don_gia = don_gia;
	}
	public BigDecimal getThanh_tien() {
		return thanh_tien;
	}
	public void setThanh_tien(BigDecimal thanh_tien) {
		this.thanh_tien = thanh_tien;
	}
	
	

	

	
}

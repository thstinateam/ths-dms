package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptPXKKVCNB_ProductVansale implements Serializable 
{
	private String ten_san_pham;
	private String ma_san_pham;

	private String don_vi_tinh;
	
	private String so_luong_thuc_xuat;

	private BigDecimal so_thung;

	private BigDecimal so_le;
	
	private BigDecimal don_gia;
	private BigDecimal thanh_tien;
	public String getTen_san_pham() {
		return ten_san_pham;
	}
	public void setTen_san_pham(String ten_san_pham) {
		this.ten_san_pham = ten_san_pham;
	}
	public String getMa_san_pham() {
		return ma_san_pham;
	}
	public void setMa_san_pham(String ma_san_pham) {
		this.ma_san_pham = ma_san_pham;
	}
	public String getDon_vi_tinh() {
		return don_vi_tinh;
	}
	public void setDon_vi_tinh(String don_vi_tinh) {
		this.don_vi_tinh = don_vi_tinh;
	}

	public String getSo_luong_thuc_xuat() {
		return so_thung + "/" + so_le;
	}
	public void setSo_luong_thuc_xuat(String so_luong_thuc_xuat) {
		this.so_luong_thuc_xuat = so_luong_thuc_xuat;
	}
	
	public BigDecimal getSo_thung() {
		return so_thung;
	}
	public void setSo_thung(BigDecimal so_thung) {
		this.so_thung = so_thung;
	}
	public BigDecimal getSo_le() {
		return so_le;
	}
	public void setSo_le(BigDecimal so_le) {
		this.so_le = so_le;
	}
	public BigDecimal getDon_gia() {
		return don_gia;
	}
	public void setDon_gia(BigDecimal don_gia) {
		this.don_gia = don_gia;
	}
	public BigDecimal getThanh_tien() {
		return thanh_tien;
	}
	public void setThanh_tien(BigDecimal thanh_tien) {
		this.thanh_tien = thanh_tien;
	}
	

	
	
}

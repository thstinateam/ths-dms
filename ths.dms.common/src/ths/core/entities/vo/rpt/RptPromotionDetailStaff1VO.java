package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

public class RptPromotionDetailStaff1VO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String promotionProgramCode;
	private String promotionProgramName;
	private ArrayList<RptPromotionDetailStaff2VO> lstRptPromotionDetailStaff2VO = new ArrayList<RptPromotionDetailStaff2VO>();


	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}

	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}

	public String getPromotionProgramName() {
		return promotionProgramName;
	}

	public void setPromotionProgramName(String promotionProgramName) {
		this.promotionProgramName = promotionProgramName;
	}

	/**
	 * @return the lstRptPromotionDetailStaff2VO
	 */
	public ArrayList<RptPromotionDetailStaff2VO> getLstRptPromotionDetailStaff2VO() {
		return lstRptPromotionDetailStaff2VO;
	}

	/**
	 * @param lstRptPromotionDetailStaff2VO the lstRptPromotionDetailStaff2VO to set
	 */
	public void setLstRptPromotionDetailStaff2VO(
			ArrayList<RptPromotionDetailStaff2VO> lstRptPromotionDetailStaff2VO) {
		this.lstRptPromotionDetailStaff2VO = lstRptPromotionDetailStaff2VO;
	}
}

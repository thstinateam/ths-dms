package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptImExStDataVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String catName;
	private String catCode;
	private Long catId;
	private BigDecimal quantityCloseSTBeginTotal = BigDecimal.ZERO;
	private BigDecimal quantityCloseSTEndTotal= BigDecimal.ZERO;
	private List<RptImExStDataDetailVO> listData;

	public RptImExStDataVO() {
		listData = new ArrayList<RptImExStDataDetailVO>();
	}

	public List<RptImExStDataDetailVO> getListData() {
		return listData;
	}

	public void setListData(List<RptImExStDataDetailVO> listData) {
		this.listData = listData;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public String getCatCode() {
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public Long getCatId() {
		return catId;
	}

	public void setCatId(Long catId) {
		this.catId = catId;
	}
	
	public BigDecimal getQuantityCloseSTBeginTotal() {
		return quantityCloseSTBeginTotal;
	}

	public void setQuantityCloseSTBeginTotal(BigDecimal quantityCloseSTBeginTotal) {
		this.quantityCloseSTBeginTotal = quantityCloseSTBeginTotal;
	}

	public BigDecimal getQuantityCloseSTEndTotal() {
		return quantityCloseSTEndTotal;
	}

	public void setQuantityCloseSTEndTotal(BigDecimal quantityCloseSTEndTotal) {
		this.quantityCloseSTEndTotal = quantityCloseSTEndTotal;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		if(catId != null){
			sb.append("catId :" + catId);
		}
		if(catName != null){
			sb.append("catName :" + catId);
		}
		if(catCode != null){
			sb.append("catCode :" + catCode);
		}
		sb.append("\n");
		if(listData != null){
			for (RptImExStDataDetailVO child : listData) {
				sb.append(child.toString());
				sb.append("\n");
			}
		}
		return sb.toString();
	}
}

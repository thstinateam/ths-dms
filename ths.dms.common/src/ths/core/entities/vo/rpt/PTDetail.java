package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class PTDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String shortCode;
	public String customerName;
	public String payReceivedNumber;
	public String createDate;
	public BigDecimal amount;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPayReceivedNumber() {
		return payReceivedNumber;
	}

	public void setPayReceivedNumber(String payReceivedNumber) {
		this.payReceivedNumber = payReceivedNumber;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}

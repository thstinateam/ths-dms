package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCDS1VO implements Serializable{

	private String mien;
	private String khuVuc;
	private String maNPP;
	private String tenNPP;
	private String GSNPP;
	private String tenGSNPP;
	private String NVBH;
	private String tenNVBH;
	private String nganhHang;
	private String sku;
	private BigDecimal soLuong;
	private BigDecimal doanhSo;
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getKhuVuc() {
		return khuVuc;
	}
	public void setKhuVuc(String khuVuc) {
		this.khuVuc = khuVuc;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getGSNPP() {
		return GSNPP;
	}
	public void setGSNPP(String gSNPP) {
		GSNPP = gSNPP;
	}
	public String getTenGSNPP() {
		return tenGSNPP;
	}
	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}
	public String getNVBH() {
		return NVBH;
	}
	public void setNVBH(String nVBH) {
		NVBH = nVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getNganhHang() {
		return nganhHang;
	}
	public void setNganhHang(String nganhHang) {
		this.nganhHang = nganhHang;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public BigDecimal getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(BigDecimal soLuong) {
		this.soLuong = soLuong;
	}
	public BigDecimal getDoanhSo() {
		return doanhSo;
	}
	public void setDoanhSo(BigDecimal doanhSo) {
		this.doanhSo = doanhSo;
	}
	
	
}

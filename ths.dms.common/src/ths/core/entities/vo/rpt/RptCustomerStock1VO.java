package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class RptCustomerStock1VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String productInfoCode;

	private ArrayList<RptCustomerStock2VO> lstRptCustomerStock2VO = new ArrayList<RptCustomerStock2VO>();

	private BigDecimal amount = new BigDecimal(0);
	
	public String getProductInfoCode() {
		return productInfoCode;
	}

	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}

	public ArrayList<RptCustomerStock2VO> getLstRptCustomerStock2VO() {
		return lstRptCustomerStock2VO;
	}

	public void setLstRptCustomerStock2VO(
			ArrayList<RptCustomerStock2VO> lstRptCustomerStock2VO) {
		this.lstRptCustomerStock2VO = lstRptCustomerStock2VO;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
/**
 * @author hunglm16
 * @since January 21, 2014
 * @description report Kd1: Doanh so ban hang cua nhan vien ban hang theo SKUs
 * */
public class RptKD_General_VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Double soNgayBanHang;
	private Double soNgayBanHangTinhNgayLe;
	private Double soNgayThucHien;
	private Double tienDoChuan;//*100%
	private Double tienThucHien; //*100%
	private Double soNgayNghiLe; //*100%
	
	public Double getSoNgayBanHangTinhNgayLe() {
		return soNgayBanHangTinhNgayLe;
	}
	public void setSoNgayBanHangTinhNgayLe(Double soNgayBanHangTinhNgayLe) {
		this.soNgayBanHangTinhNgayLe = soNgayBanHangTinhNgayLe;
	}
	public Double getSoNgayNghiLe() {
		return soNgayNghiLe;
	}
	public void setSoNgayNghiLe(Double soNgayNghiLe) {
		this.soNgayNghiLe = soNgayNghiLe;
	}
	public Double getSoNgayBanHang() {
		return soNgayBanHang;
	}
	public void setSoNgayBanHang(Double soNgayBanHang) {
		this.soNgayBanHang = soNgayBanHang;
	}
	public Double getSoNgayThucHien() {
		return soNgayThucHien;
	}
	public void setSoNgayThucHien(Double soNgayThucHien) {
		this.soNgayThucHien = soNgayThucHien;
	}
	public Double getTienDoChuan() {
		return tienDoChuan;
	}
	public void setTienDoChuan(Double tienDoChuan) {
		this.tienDoChuan = tienDoChuan;
	}
	public Double getTienThucHien() {
		return tienThucHien;
	}
	public void setTienThucHien(Double tienThucHien) {
		this.tienThucHien = tienThucHien;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
public class RptPayDisplayProgrameVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String displayProgrameCode;
	private String displayProgrameName;
	private String fDate;
	private String tDate;
	ArrayList<RptPayDisplayProgrameRecordProductVO> listRecord = new ArrayList<RptPayDisplayProgrameRecordProductVO>();
	
	public String getDisplayProgrameCode() {
		return displayProgrameCode;
	}
	public void setDisplayProgrameCode(String displayProgrameCode) {
		this.displayProgrameCode = displayProgrameCode;
	}
	public String getDisplayProgrameName() {
		return displayProgrameName;
	}
	public void setDisplayProgrameName(String displayProgrameName) {
		this.displayProgrameName = displayProgrameName;
	}
	
	public String getfDate() {
		return fDate;
	}
	public void setfDate(String fDate) {
		this.fDate = fDate;
	}
	public String gettDate() {
		return tDate;
	}
	public void settDate(String tDate) {
		this.tDate = tDate;
	}
	public ArrayList<RptPayDisplayProgrameRecordProductVO> getListRecord() {
		return listRecord;
	}
	public void setListRecord(
			ArrayList<RptPayDisplayProgrameRecordProductVO> listRecord) {
		this.listRecord = listRecord;
	}
	
	
}
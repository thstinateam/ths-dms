package ths.core.entities.vo.rpt;

import java.io.Serializable;

import ths.dms.core.entities.enumtype.EquipDeliveryContentType;

public class Rpt_3116_WVKD_03F5SC1_1VO implements Serializable {

	/**
	 * 3.1.1.6 [WV-KD-03-F5] Danh sách mượn tủ đông, tủ mát theo kỳ của NPP, ASO
	 * 
	 * @author Datpv4
	 * @since May 13,2015
	 */
	private static final long serialVersionUID = 1L;

	String sohd;
	String ngayhd;
	String sobbgn;
	String ngaybbgn;
	String mien;
	String vung;
	String manpp;
	String tennpp;
	String makh;
	String tenkh;
	String ngaymuon;
	Integer soluong;
	String loaitu;
	String nhomtu;
	String hieutu;
	String dungtich;
	Integer namsx;
	String mataisan;
	String soserial;
	String ngaysudung;
	String noidungmuontu;
	String nd_capmoi;//Cap moi
	String nd_dctukh;//Dieu chuyen tu khach hang khac
	String nd_khac;//kac
	public String getSohd() {
		return sohd;
	}
	public void setSohd(String sohd) {
		this.sohd = sohd;
	}
	public String getNgayhd() {
		return ngayhd;
	}
	public void setNgayhd(String ngayhd) {
		this.ngayhd = ngayhd;
	}
	public String getSobbgn() {
		return sobbgn;
	}
	public void setSobbgn(String sobbgn) {
		this.sobbgn = sobbgn;
	}
	public String getNgaybbgn() {
		return ngaybbgn;
	}
	public void setNgaybbgn(String ngaybbgn) {
		this.ngaybbgn = ngaybbgn;
	}
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getManpp() {
		return manpp;
	}
	public void setManpp(String manpp) {
		this.manpp = manpp;
	}
	
	public String getTennpp() {
		return tennpp;
	}
	public void setTennpp(String tennpp) {
		this.tennpp = tennpp;
	}
	public String getMakh() {
		return makh;
	}
	public void setMakh(String makh) {
		this.makh = makh;
	}
	public String getTenkh() {
		return tenkh;
	}
	public void setTenkh(String tenkh) {
		this.tenkh = tenkh;
	}
	public String getNgaymuon() {
		return ngaymuon;
	}
	public void setNgaymuon(String ngaymuon) {
		this.ngaymuon = ngaymuon;
	}
	public Integer getSoluong() {
		return soluong;
	}
	public void setSoluong(Integer soluong) {
		this.soluong = soluong;
	}
	public String getLoaitu() {
		return loaitu;
	}
	public void setLoaitu(String loaitu) {
		this.loaitu = loaitu;
	}
	public String getNhomtu() {
		return nhomtu;
	}
	public void setNhomtu(String nhomtu) {
		this.nhomtu = nhomtu;
	}
	public String getHieutu() {
		return hieutu;
	}
	public void setHieutu(String hieutu) {
		this.hieutu = hieutu;
	}
	public String getDungtich() {
		return dungtich;
	}
	public void setDungtich(String dungtich) {
		this.dungtich = dungtich;
	}
	
	public Integer getNamsx() {
		return namsx;
	}
	public void setNamsx(Integer namsx) {
		this.namsx = namsx;
	}
	public String getMataisan() {
		return mataisan;
	}
	public void setMataisan(String mataisan) {
		this.mataisan = mataisan;
	}
	public String getSoserial() {
		return soserial;
	}
	public void setSoserial(String soserial) {
		this.soserial = soserial;
	}
	public String getNgaysudung() {
		return ngaysudung;
	}
	public void setNgaysudung(String ngaysudung) {
		this.ngaysudung = ngaysudung;
	}
	public String getNoidungmuontu() {
		return noidungmuontu;
	}
	public void setNoidungmuontu(String noidungmuontu) {
		this.noidungmuontu = noidungmuontu;
	}
	
	
}
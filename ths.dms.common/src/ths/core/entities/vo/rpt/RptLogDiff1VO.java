package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.entities.ActionAuditDetail;

public class RptLogDiff1VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String actionUser;// 1
	private String actionType;// 1
	private String actionDate;// 1
	private String actionIp;// 1
	
	private List<ActionAuditDetail> lstActionAuditDetail = new ArrayList<ActionAuditDetail>();

	public String getActionUser() {
		return actionUser;
	}

	public void setActionUser(String actionUser) {
		this.actionUser = actionUser;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getActionDate() {
		return actionDate;
	}

	public void setActionDate(String actionDate) {
		this.actionDate = actionDate;
	}

	public String getActionIp() {
		return actionIp;
	}

	public void setActionIp(String actionIp) {
		this.actionIp = actionIp;
	}

	public List<ActionAuditDetail> getLstActionAuditDetail() {
		return lstActionAuditDetail;
	}

	public void setLstActionAuditDetail(List<ActionAuditDetail> lstActionAuditDetail) {
		this.lstActionAuditDetail = lstActionAuditDetail;
	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class Rpt7_2_14VO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mien;
	private String tenMien;
	private String vung;
	private String tenVung;
	private String maNPP;
	private String tenNPP;
	private String gsnpp;
	private String tenGSNPP;
	private String nvbh;
	private String tenNVBH;
	private String maKH;
	private String tenKH;
	private String ngayHoaDon;
	private BigDecimal datHang;
	private BigDecimal duyet;
	private BigDecimal chenhLech;
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getTenMien() {
		return tenMien;
	}
	public void setTenMien(String tenMien) {
		this.tenMien = tenMien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getTenVung() {
		return tenVung;
	}
	public void setTenVung(String tenVung) {
		this.tenVung = tenVung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getGsnpp() {
		return gsnpp;
	}
	public void setGsnpp(String gsnpp) {
		this.gsnpp = gsnpp;
	}
	public String getTenGSNPP() {
		return tenGSNPP;
	}
	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}
	public String getNvbh() {
		return nvbh;
	}
	public void setNvbh(String nvbh) {
		this.nvbh = nvbh;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getNgayHoaDon() {
		return ngayHoaDon;
	}
	public void setNgayHoaDon(String ngayHoaDon) {
		this.ngayHoaDon = ngayHoaDon;
	}
	public BigDecimal getDatHang() {
		return datHang;
	}
	public void setDatHang(BigDecimal datHang) {
		this.datHang = datHang;
	}
	public BigDecimal getDuyet() {
		return duyet;
	}
	public void setDuyet(BigDecimal duyet) {
		this.duyet = duyet;
	}
	public BigDecimal getChenhLech() {
		return chenhLech;
	}
	public void setChenhLech(BigDecimal chenhLech) {
		this.chenhLech = chenhLech;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptRevenueByDisplayProgramLevel implements Serializable{
	private static final long serialVersionUID = 1L;
	private String customerShortCode;
	private String customerName;
	private String displayProgramCode;
	private Date fromDate;
	private Date toDate;
	private String levelCode;
	private BigDecimal levelAmount;
	private BigDecimal realAmount;
	public String getCustomerShortCode() {
		return customerShortCode;
	}
	public void setCustomerShortCode(String customerShortCode) {
		this.customerShortCode = customerShortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getDisplayProgramCode() {
		return displayProgramCode;
	}
	public void setDisplayProgramCode(String displayProgramCode) {
		this.displayProgramCode = displayProgramCode;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getLevelCode() {
		return levelCode;
	}
	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}
	public BigDecimal getLevelAmount() {
		return levelAmount;
	}
	public void setLevelAmount(BigDecimal levelAmount) {
		this.levelAmount = levelAmount;
	}
	public BigDecimal getRealAmount() {
		return realAmount;
	}
	public void setRealAmount(BigDecimal realAmount) {
		this.realAmount = realAmount;
	}
}

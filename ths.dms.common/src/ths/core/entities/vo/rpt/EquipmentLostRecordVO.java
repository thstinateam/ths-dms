/**
 * Copyright (c) 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.core.entities.vo.rpt;

import java.io.Serializable;

/**
 * VO su dung trong table cua BB Bao mat
 * @author tuannd20
 * @since 26/02/2016
 */
public class EquipmentLostRecordVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String contractNumber;
	private String provideYear;
	private String equipmentType;
	private String equipmentModel;
	private String equipmentQuantity;
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getProvideYear() {
		return provideYear;
	}
	public void setProvideYear(String provideYear) {
		this.provideYear = provideYear;
	}
	public String getEquipmentType() {
		return equipmentType;
	}
	public void setEquipmentType(String equipmentType) {
		this.equipmentType = equipmentType;
	}
	public String getEquipmentModel() {
		return equipmentModel;
	}
	public void setEquipmentModel(String equipmentModel) {
		this.equipmentModel = equipmentModel;
	}
	public String getEquipmentQuantity() {
		return equipmentQuantity;
	}
	public void setEquipmentQuantity(String equipmentQuantity) {
		this.equipmentQuantity = equipmentQuantity;
	}
}

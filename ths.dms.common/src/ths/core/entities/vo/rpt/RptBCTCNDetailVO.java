package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

public class RptBCTCNDetailVO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Date orderDate;
	
	private String orderNumber;
	
	private String orderType;
	
	//no qua han
	private BigDecimal overdueDebt;
	
	//no den han
	private BigDecimal nextDebt;
	
	//no chua den han
	private BigDecimal availableDebt;
	
	private BigDecimal total;
	
	private String customerInfo;
	
	private String staffInfo;
	
	private String deliveryStaffInfo;
	
	private Date paidDate;
	
	private String groupInfo;
	
	private BigDecimal groupOverdueDebt;
	
	private BigDecimal groupNextDebt;
	
	private BigDecimal groupAvailableDebt;
	
	private BigDecimal groupTotal;
	
	private Integer soThuTu;
	
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public BigDecimal getOverdueDebt() {
		return overdueDebt;
	}

	public void setOverdueDebt(BigDecimal overdueDebt) {
		this.overdueDebt = overdueDebt;
	}

	public BigDecimal getNextDebt() {
		return nextDebt;
	}

	public void setNextDebt(BigDecimal nextDebt) {
		this.nextDebt = nextDebt;
	}

	public BigDecimal getAvailableDebt() {
		return availableDebt;
	}

	public void setAvailableDebt(BigDecimal availableDebt) {
		this.availableDebt = availableDebt;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(String customerInfo) {
		this.customerInfo = customerInfo;
	}

	public String getStaffInfo() {
		return staffInfo;
	}

	public void setStaffInfo(String staffInfo) {
		this.staffInfo = staffInfo;
	}

	public String getDeliveryStaffInfo() {
		return deliveryStaffInfo;
	}

	public void setDeliveryStaffInfo(String deliveryStaffInfo) {
		this.deliveryStaffInfo = deliveryStaffInfo;
	}

	public String getGroupInfo() {
		return groupInfo;
	}

	public void setGroupInfo(String groupInfo) {
		this.groupInfo = groupInfo;
	}

	public BigDecimal getGroupOverdueDebt() {
		return groupOverdueDebt;
	}

	public void setGroupOverdueDebt(BigDecimal groupOverdueDebt) {
		this.groupOverdueDebt = groupOverdueDebt;
	}

	public BigDecimal getGroupNextDebt() {
		return groupNextDebt;
	}

	public void setGroupNextDebt(BigDecimal groupNextDebt) {
		this.groupNextDebt = groupNextDebt;
	}

	public BigDecimal getGroupAvailableDebt() {
		return groupAvailableDebt;
	}

	public void setGroupAvailableDebt(BigDecimal groupAvailableDebt) {
		this.groupAvailableDebt = groupAvailableDebt;
	}

	public BigDecimal getGroupTotal() {
		return groupTotal;
	}

	public void setGroupTotal(BigDecimal groupTotal) {
		this.groupTotal = groupTotal;
	}

	public Integer getSoThuTu() {
		return soThuTu;
	}

	public void setSoThuTu(Integer soThuTu) {
		this.soThuTu = soThuTu;
	}
	
}

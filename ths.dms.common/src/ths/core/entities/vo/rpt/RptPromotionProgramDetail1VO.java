package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

public class RptPromotionProgramDetail1VO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String staffCode;
	
	private List<RptPromotionProgramDetailVO> lstRptPromotionProgramDetailVO;

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public List<RptPromotionProgramDetailVO> getLstRptPromotionProgramDetailVO() {
		return lstRptPromotionProgramDetailVO;
	}

	public void setLstRptPromotionProgramDetailVO(
			List<RptPromotionProgramDetailVO> lstRptPromotionProgramDetailVO) {
		this.lstRptPromotionProgramDetailVO = lstRptPromotionProgramDetailVO;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;

/**
 * 
 * @author cangnd
 *
 */
public class RptBHTSP_ThongTinTVVO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String shop_id;
	private Integer staff_id;
	private String staff_name;
	private String product_info_code;
	private String cat_id;
	private String product_code;
	private String product_name;
	private Integer quantity;
	private Float price;

	
	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}
	
	public String getShop_id() {
		return shop_id;
	}
	
	public void setStaff_name(String staff_name) {
		this.staff_name = staff_name;
	}
	public String getStaff_name() {
		return staff_name;
	}
	public void setProduct_info_code(String product_info_code) {
		this.product_info_code = product_info_code;
	}
	public String getProduct_info_code() {
		return product_info_code;
	}
	public void setCat_id(String cat_id) {
		this.cat_id = cat_id;
	}
	public String getCat_id() {
		return cat_id;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getProduct_name() {
		return product_name;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Float getPrice() {
		return price;
	}

	public void setStaff_id(Integer staff_id) {
		this.staff_id = staff_id;
	}

	public Integer getStaff_id() {
		return staff_id;
	}









	
}

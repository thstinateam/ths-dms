package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ths.dms.core.common.utils.GroupUtility;

public class RptSaleOrderByProductDataVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long staffId;
	private String staffCode;
	private String staffName;
	private Long quantity;
	private BigDecimal amount;
	private Float price;
	private Long productId;
	private String productCode;
	private String productName;
	private String productInfo;

	public RptSaleOrderByProductDataVO() {

	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(String productInfo) {
		this.productInfo = productInfo;
	}

	@SuppressWarnings("unchecked")
	public static List<RptSaleOrderByProductLv01VO> groupByStaffAndCat(
			List<RptSaleOrderByProductDataVO> listData) throws Exception {
		List<RptSaleOrderByProductLv01VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, "staffCode");

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptSaleOrderByProductLv01VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptSaleOrderByProductLv01VO vo = new RptSaleOrderByProductLv01VO();

					List<RptSaleOrderByProductDataVO> listObject = (List<RptSaleOrderByProductDataVO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setStaffCode(listObject.get(0).getStaffCode());
						vo.setStaffName(listObject.get(0).getStaffName());

						List<RptSaleOrderByProductLv02VO> listLV02 = RptSaleOrderByProductDataVO
								.groupByCat(listObject);

						vo.setListData(listLV02);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}

	@SuppressWarnings("unchecked")
	public static List<RptSaleOrderByProductLv02VO> groupByCat(
			List<RptSaleOrderByProductDataVO> listData) throws Exception {
		List<RptSaleOrderByProductLv02VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, "productInfo");

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptSaleOrderByProductLv02VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptSaleOrderByProductLv02VO vo = new RptSaleOrderByProductLv02VO();

					List<RptSaleOrderByProductDataVO> listObject = (List<RptSaleOrderByProductDataVO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setCat((String) mapEntry.getKey());
						vo.setListData(listObject);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCCTKM_RecordDetailVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal shopId;
	private String shopName;
	private String shopAddress;
	
	private String promotionProgramCode;
	private String promotionProgramDecription;
	
	private BigDecimal orderId;
	private String orderDate;
	private String staffName;
	private String staffCode;
	private String customerCode;
	private String customerType;
	private String productCode;
	private BigDecimal quantity;
	private BigDecimal freeItem;
	private String productCodeFree;
	private BigDecimal quantityFree;
	
	public BigDecimal getShopId() {
		return shopId;
	}
	public void setShopId(BigDecimal shopId) {
		this.shopId = shopId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}
	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}
	public String getPromotionProgramDecription() {
		return promotionProgramDecription;
	}
	public void setPromotionProgramDecription(String promotionProgramDecription) {
		this.promotionProgramDecription = promotionProgramDecription;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	
	public BigDecimal getOrderId() {
		return orderId;
	}
	public void setOrderId(BigDecimal orderId) {
		this.orderId = orderId;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getFreeItem() {
		return freeItem;
	}
	public void setFreeItem(BigDecimal freeItem) {
		this.freeItem = freeItem;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getProductCodeFree() {
		return productCodeFree;
	}
	public void setProductCodeFree(String productCodeFree) {
		this.productCodeFree = productCodeFree;
	}
	public BigDecimal getQuantityFree() {
		return quantityFree;
	}
	public void setQuantityFree(BigDecimal quantityFree) {
		this.quantityFree = quantityFree;
	}
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

//doanh so phan phoi theo nhom hang
public class RptBCKD1_2 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer soNgayBanHang;
	private Integer soNgayThucHien;
	private Integer tienDoChuan;
	private Date tuNgay;
	private Date denNgay;
	private List<RptBCKD1_2_Mien> lstDetail;
	
	public Integer getSoNgayBanHang() {
		return soNgayBanHang;
	}
	public void setSoNgayBanHang(Integer soNgayBanHang) {
		this.soNgayBanHang = soNgayBanHang;
	}
	public Integer getSoNgayThucHien() {
		return soNgayThucHien;
	}
	public void setSoNgayThucHien(Integer soNgayThucHien) {
		this.soNgayThucHien = soNgayThucHien;
	}
	public Integer getTienDoChuan() {
		return tienDoChuan;
	}
	public void setTienDoChuan(Integer tienDoChuan) {
		this.tienDoChuan = tienDoChuan;
	}
	public Date getTuNgay() {
		return tuNgay;
	}
	public void setTuNgay(Date tuNgay) {
		this.tuNgay = tuNgay;
	}
	public Date getDenNgay() {
		return denNgay;
	}
	public void setDenNgay(Date denNgay) {
		this.denNgay = denNgay;
	}
	public List<RptBCKD1_2_Mien> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<RptBCKD1_2_Mien> lstDetail) {
		this.lstDetail = lstDetail;
	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD18CTVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mienCode;//ma mien
	private String catName;//ten nganh hang
	private String subCatName;//ten nganh hang con
	private Integer numStart;//so luong tai thang bat dau
	private Integer numEnd;//so luong tai thang ket thuc
	private Integer numDiff;//numEnd - numStart
	public String getMienCode() {
		return mienCode;
	}
	public void setMienCode(String mienCode) {
		this.mienCode = mienCode;
	}
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	public String getSubCatName() {
		return subCatName;
	}
	public void setSubCatName(String subCatName) {
		this.subCatName = subCatName;
	}
	public Integer getNumStart() {
		return numStart;
	}
	public void setNumStart(BigDecimal numStart) {
		this.numStart = numStart.intValue();
	}
	public Integer getNumEnd() {
		return numEnd;
	}
	public void setNumEnd(BigDecimal numEnd) {
		this.numEnd = numEnd.intValue();
	}
	public Integer getNumDiff() {
		return numDiff;
	}
	public void setNumDiff(BigDecimal numDiff) {
		this.numDiff = numDiff.intValue();
	}
	
}

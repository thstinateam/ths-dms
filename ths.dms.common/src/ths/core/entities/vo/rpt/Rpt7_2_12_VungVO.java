package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class Rpt7_2_12_VungVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3877270696091279101L;
	private String mien;
	private String tenMien;
	private String vung;
	private BigDecimal dsKeHoach;
	private BigDecimal dsMTB;
	private BigDecimal dsThucTe;
	private List<Rpt7_2_12_NPPVO> listNPP;
	
	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getTenMien() {
		return tenMien;
	}

	public void setTenMien(String tenMien) {
		this.tenMien = tenMien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public BigDecimal getDsKeHoach() {
		return dsKeHoach;
	}

	public void setDsKeHoach(BigDecimal dsKeHoach) {
		this.dsKeHoach = dsKeHoach;
	}

	public BigDecimal getDsMTB() {
		return dsMTB;
	}

	public void setDsMTB(BigDecimal dsMTB) {
		this.dsMTB = dsMTB;
	}

	public BigDecimal getDsThucTe() {
		return dsThucTe;
	}

	public void setDsThucTe(BigDecimal dsThucTe) {
		this.dsThucTe = dsThucTe;
	}

	public List<Rpt7_2_12_NPPVO> getListNPP() {
		return listNPP;
	}

	public void setListNPP(List<Rpt7_2_12_NPPVO> listNPP) {
		this.listNPP = listNPP;
	}

}

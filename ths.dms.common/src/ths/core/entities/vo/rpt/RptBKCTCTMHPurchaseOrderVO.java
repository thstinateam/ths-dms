package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

public class RptBKCTCTMHPurchaseOrderVO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String saleOrderNumber;
	private String purchaseOrder;
	private String orderDate;
	private String poConfirm;
	private String invoiceNumber;
	private String importDate;
	private String status;
	private List<RptBKCTCTMHProductRecordVO> listRptBKCTCTMHProductRecordVO;
	
	public String getSaleOrderNumber() {
		return saleOrderNumber;
	}
	public void setSaleOrderNumber(String saleOrderNumber) {
		this.saleOrderNumber = saleOrderNumber;
	}
	public String getPurchaseOrder() {
		return purchaseOrder;
	}
	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getPoConfirm() {
		return poConfirm;
	}
	public void setPoConfirm(String poConfirm) {
		this.poConfirm = poConfirm;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getImportDate() {
		return importDate;
	}
	public void setImportDate(String importDate) {
		this.importDate = importDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<RptBKCTCTMHProductRecordVO> getListRptBKCTCTMHProductRecordVO() {
		return listRptBKCTCTMHProductRecordVO;
	}
	public void setListRptBKCTCTMHProductRecordVO(
			List<RptBKCTCTMHProductRecordVO> listRptBKCTCTMHProductRecordVO) {
		this.listRptBKCTCTMHProductRecordVO = listRptBKCTCTMHProductRecordVO;
	}
}

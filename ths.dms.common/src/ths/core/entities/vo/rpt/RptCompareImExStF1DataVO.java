package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ths.dms.core.common.utils.GroupUtility;

public class RptCompareImExStF1DataVO implements Serializable {
	private static final long serialVersionUID = -573948373613262483L;

	private String productInfoCode;
	private String productCode;
	private String productName;
	// ton dau ky
	private Long openStockTotal;
	// nhap
	private Long importQty;
	// xuat
	private Long exportQty;
	// Ton kho tai thoi diem hien tai
	private Long stockQty;
	// luy ke tieu thu thang
	private Long monthCumulate;
	// ke hoach tieu thu thang
	private Long monthPlan;
	// dinh muc ke hoach thang: ? tong so ngay ban hang trong thang
	private Long dayPlan;
	// du tru ke hoach : ? tong so ngay ban hang trong thang
	private Double dayReservePlan;
	// du tru thuc te : ? so ngay ban hang thuc te
	private Double dayReserveReal;
	// min
	private Long min;
	// lead
	private Long lead;
	// next
	private Long next;
	// yeu cau ton so luong : ? tong so ngay ban hang trong thang
	private Long requimentStock;
	// so luong po comfirm
	private Long poConfirmQty;
	// so luong po dvkh
	private Long stockPoDvkh;
	// quy cach
	private Long convfact;
	private Double percentage;
	private BigDecimal price;
	// nhap po
	private Long poImport;
	// xuat po
	private Long poExport;
	// ton po
	private Long poStock;
	// luy ke tieu thu thang po
	private Long poMonthCumulate;
	// sl thung po
	// private String poBoxQuantity;
	private Long poBoxQuantity;

	private Long poQuantity;
	private Long poConvfact;

	// thanh tien
	private BigDecimal amount;
	// canh bao
	private String warning;
	// so luong thung
	private Long boxQuantity;

	private Integer realSaleDateInMonth;

	public RptCompareImExStF1DataVO() {
	}

	public String getProductInfoCode() {
		return productInfoCode;
	}

	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getOpenStockTotal() {
		return openStockTotal;
	}

	public void setOpenStockTotal(Long openStockTotal) {
		this.openStockTotal = openStockTotal;
	}

	public Long getImportQty() {
		return importQty;
	}

	public void setImportQty(Long importQty) {
		this.importQty = importQty;
	}

	public Long getExportQty() {
		if (exportQty == null) {
			exportQty = 0l;
		}
		return exportQty;
	}

	public void setExportQty(Long exportQty) {
		this.exportQty = exportQty;
	}

	public Long getStockQty() {
		if (stockQty == null) {
			stockQty = 0l;
		}
		return stockQty;
	}

	public void setStockQty(Long stockQty) {
		this.stockQty = stockQty;
	}

	public Long getMonthCumulate() {
		if (monthCumulate == null) {
			monthCumulate = 0l;
		}
		return monthCumulate;
	}

	public void setMonthCumulate(Long monthCumulate) {
		this.monthCumulate = monthCumulate;
	}

	public Long getMonthPlan() {
		if (monthPlan == null) {
			monthPlan = 0l;
		}
		return monthPlan;
	}

	public void setMonthPlan(Long monthPlan) {
		this.monthPlan = monthPlan;
	}

	public Long getDayPlan() {
		if (dayPlan == null) {
			dayPlan = 0l;
		}
		return dayPlan;
	}

	public void setDayPlan(Long dayPlan) {
		this.dayPlan = dayPlan;
	}

	public Double getDayReservePlan() {
		if (dayReservePlan == null) {
			dayReservePlan = 0d;
		}
		return dayReservePlan;
	}

	public void setDayReservePlan(Double dayReservePlan) {
		this.dayReservePlan = dayReservePlan;
	}

	public Double getDayReserveReal() {
		if (dayReserveReal == null) {
			dayReserveReal = 0d;
		}
		return dayReserveReal;
	}

	public void setDayReserveReal(Double dayReserveReal) {
		this.dayReserveReal = dayReserveReal;
	}

	public Long getMin() {
		return min;
	}

	public void setMin(Long min) {
		this.min = min;
	}

	public Long getLead() {
		if (lead == null) {
			lead = 0l;
		}
		return lead;
	}

	public void setLead(Long lead) {
		this.lead = lead;
	}

	public Long getNext() {
		if (next == null) {
			next = 0l;
		}
		return next;
	}

	public void setNext(Long next) {
		this.next = next;
	}

	public Long getRequimentStock() {
		if (requimentStock == null) {
			requimentStock = 0l;
		}
		return requimentStock;
	}

	public void setRequimentStock(Long requimentStock) {
		this.requimentStock = requimentStock;
	}

	public Long getPoConfirmQty() {
		if (poConfirmQty == null) {
			poConfirmQty = 0l;
		}
		return poConfirmQty;
	}

	public void setPoConfirmQty(Long poConfirmQty) {
		this.poConfirmQty = poConfirmQty;
	}

	public Long getStockPoDvkh() {
		if (stockPoDvkh == null) {
			stockPoDvkh = 0l;
		}
		return stockPoDvkh;
	}

	public void setStockPoDvkh(Long stockPoDvkh) {
		this.stockPoDvkh = stockPoDvkh;
	}

	public Long getConvfact() {
		if (convfact == null) {
			convfact = 0l;
		}
		return convfact;
	}

	public void setConvfact(Long convfact) {
		this.convfact = convfact;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public BigDecimal getPrice() {
		return price == null ? BigDecimal.valueOf(0) : price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getBoxQuantity() {
		double quatity = (((double) this.getMin() + (double) getLead() + (double) getNext())
				* (double) getDayPlan() - (double) getStockQty() - (double) getStockPoDvkh())
				/ (double) getConvfact();
		if (quatity > 20) {
			if (quatity % 5 != 0) {
				quatity = (double) (Math.ceil(quatity / 5) * 5);
			}
		}
		if (getMonthCumulate() + quatity * getConvfact() > getPercentage()
				* getMonthPlan()) {
			quatity = (double) ((double) getPercentage() * getMonthPlan() - (double) getMonthCumulate())
					/ getConvfact();
		}

		this.boxQuantity = Math.round(quatity);

		return this.boxQuantity;
	}

	public void setBoxQuantity(Long boxQuantity) {
		this.boxQuantity = boxQuantity;
	}

	public BigDecimal getAmount() {
		if (null == this.amount) {
			this.amount = this.getPrice()
					.multiply(new BigDecimal(this.getBoxQuantity()))
					.multiply(new BigDecimal(this.getConvfact()));
		}

		return amount;
	}

	public void setAmout(BigDecimal amount) {
		this.amount = amount;
	}

	public String getWarning() {
		if (null == this.getRealSaleDateInMonth()
				|| this.getRealSaleDateInMonth() == 0) {
			return " ";
		}

		if (null == this.warning || "".equals(this.warning)) {
			if (getMonthCumulate() / this.getRealSaleDateInMonth() < getDayPlan() * 70 / 100) {
				this.warning = "X";
			} else if (getDayReserveReal() > 4 * this.getMin()) {
				this.warning = "QX";
			} else if (this.getMonthCumulate() > (getMonthPlan() - this
					.getStockQty() * (getPercentage() / 100.0))) {
				this.warning = "O";
			} else {
				this.warning = " ";
			}
		}

		return warning;
	}

	public void setWarning(String warning) {
		this.warning = warning;
	}

	public Long getPoImport() {
		if (poImport == null) {
			poImport = 0l;
		}
		return poImport;
	}

	public void setPoImport(Long poImport) {
		this.poImport = poImport;
	}

	public Long getPoExport() {
		if (poExport == null) {
			poExport = 0l;
		}
		return poExport;
	}

	public void setPoExport(Long poExport) {
		this.poExport = poExport;
	}

	public Long getPoStock() {
		if (poStock == null) {
			poStock = 0l;
		}
		return poStock;
	}

	public void setPoStock(Long poStock) {
		this.poStock = poStock;
	}

	public Long getPoMonthCumulate() {
		if (poMonthCumulate == null) {
			poMonthCumulate = 0l;
		}
		return poMonthCumulate;
	}

	public void setPoMonthCumulate(Long poMonthCumulate) {
		this.poMonthCumulate = poMonthCumulate;
	}

	public Long getPoBoxQuantity() {
		if (null == poBoxQuantity) {
			this.poBoxQuantity = (this.getPoQuantity() / this.getPoConvfact());
		}

		return poBoxQuantity;
	}

	public void setPoBoxQuantity(Long poBoxQuantity) {
		this.poBoxQuantity = poBoxQuantity;
	}

	public Long getPoQuantity() {
		return poQuantity;
	}

	public void setPoQuantity(Long poQuantity) {
		this.poQuantity = poQuantity;
	}

	public Long getPoConvfact() {
		return poConvfact;
	}

	public void setPoConvfact(Long poConvfact) {
		this.poConvfact = poConvfact;
	}

	public Integer getRealSaleDateInMonth() {
		return realSaleDateInMonth;
	}

	public void setRealSaleDateInMonth(Integer realSaleDateInMonth) {
		this.realSaleDateInMonth = realSaleDateInMonth;
	}

	@SuppressWarnings("unchecked")
	public static List<RptCompareImExStF1VO> groupByCat(
			List<RptCompareImExStF1DataVO> listData) throws Exception {
		List<RptCompareImExStF1VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, "productInfoCode");

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptCompareImExStF1VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptCompareImExStF1VO vo = new RptCompareImExStF1VO();

					List<RptCompareImExStF1DataVO> listObject = (List<RptCompareImExStF1DataVO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setSubCat((String) mapEntry.getKey());

						vo.setData(listObject);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}
}

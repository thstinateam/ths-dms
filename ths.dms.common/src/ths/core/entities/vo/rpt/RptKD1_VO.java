package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * @author hunglm16
 * @since January 21, 2014
 * @description report Kd1: Doanh so ban hang cua nhan vien ban hang theo SKUs
 * */
public class RptKD1_VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String tenNVBH;
	private String mien;
	private String vung;
	private String tbhv;
	private String maNPP;
	private String gsnpp;
	private String maNVBH;
	private String doiBanHang;
	private String skus;
	public String getSkus() {
		return skus;
	}
	public void setSkus(String skus) {
		this.skus = skus;
	}
	public String getToMonth() {
		return toMonth;
	}
	public void setToMonth(String toMonth) {
		this.toMonth = toMonth;
	}
	public String getPrevMonth() {
		return prevMonth;
	}
	public void setPrevMonth(String prevMonth) {
		this.prevMonth = prevMonth;
	}
	private BigDecimal khtt;
	private String toMonth;
	private String prevMonth;
	
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getTbhv() {
		return tbhv;
	}
	public void setTbhv(String tbhv) {
		this.tbhv = tbhv;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getGsnpp() {
		return gsnpp;
	}
	public void setGsnpp(String gsnpp) {
		this.gsnpp = gsnpp;
	}
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getDoiBanHang() {
		return doiBanHang;
	}
	public void setDoiBanHang(String doiBanHang) {
		this.doiBanHang = doiBanHang;
	}
	public BigDecimal getKhtt() {
		return khtt;
	}
	public void setKhtt(BigDecimal khtt) {
		this.khtt = khtt;
	}
}

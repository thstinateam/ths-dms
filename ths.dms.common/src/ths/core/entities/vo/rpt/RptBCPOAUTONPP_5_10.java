package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCPOAUTONPP_5_10 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String shopCode;
	String shopName;
	String poAutoDate;
	String poAutoNumber;
	Integer numSKU;
	String productCode;
	String productName;
	BigDecimal price;
	String thungLe;
	
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getPoAutoDate() {
		return poAutoDate;
	}
	public void setPoAutoDate(String poAutoDate) {
		this.poAutoDate = poAutoDate;
	}
	public String getPoAutoNumber() {
		return poAutoNumber;
	}
	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}
	public Integer getNumSKU() {
		return numSKU;
	}
	public void setNumSKU(Integer numSKU) {
		this.numSKU = numSKU;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getThungLe() {
		return thungLe;
	}
	public void setThungLe(String thungLe) {
		this.thungLe = thungLe;
	}
}

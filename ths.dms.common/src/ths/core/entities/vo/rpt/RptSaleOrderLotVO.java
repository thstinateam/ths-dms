package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptSaleOrderLotVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// p.product_code, p.product_name, sol.lot, sod.price, sol.quantity,
	// sod.quantity,
	// p.convfact, sod.is_free_item, sod.discount_amount
	private String productCode;

	private String productName;

	private String lot;

	private BigDecimal price;
	
	private BigDecimal pkPrice;

	private Integer lotQuantity;
	
	private Integer promotionLotQuantity;
	
	private Integer quantity;

	private Integer convfact;

	private Integer isFreeItem;

	private BigDecimal discountAmount;

	private String promotionCode;

	private String promotionName;
	
	private BigDecimal totalWeight = new BigDecimal(0);
	
	private BigDecimal discount;

	public BigDecimal getDiscount() {
		if (discount == null) {
			return new BigDecimal(0);
		}
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Integer getIsFreeItem() {
		return isFreeItem;
	}

	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Integer getLotQuantity() {
		return lotQuantity;
	}

	public void setLotQuantity(Integer lotQuantity) {
		this.lotQuantity = lotQuantity;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public Integer getPromotionLotQuantity() {
		return promotionLotQuantity;
	}

	public void setPromotionLotQuantity(Integer promotionLotQuantity) {
		this.promotionLotQuantity = promotionLotQuantity;
	}
	
//	public Integer getThung() {
//		return this.getQuantity() / this.getConvfact();
//	}
	
	public Integer getThung(){
		if (this.getQuantity() == null || this.getConvfact() == null) return null;
		return this.getQuantity() / this.getConvfact();
	}
	
	public Integer getLe() {
		if (this.getQuantity() == null || this.getConvfact() == null) return null;
		return this.getQuantity() % this.getConvfact();
	}

	public BigDecimal getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}

	public BigDecimal getPkPrice() {
		return pkPrice;
	}

	public void setPkPrice(BigDecimal pkPrice) {
		this.pkPrice = pkPrice;
	}
	
	
}
package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptCustomerByRoutingLv01VO implements Serializable {

	private static final long serialVersionUID = 1L;
	private BigDecimal staffId;
	private String staffCode;
	private String staffName;
	private String status;
	private List<RptCustomerByRoutingDataVO> listData;

	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public RptCustomerByRoutingLv01VO() {
		this.listData = new ArrayList<RptCustomerByRoutingDataVO>();
	}

	public BigDecimal getStaffId() {
		return staffId;
	}

	public void setStaffId(BigDecimal staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		if (null != staffCode) {
			this.staffCode = staffCode;
		} else {
			this.staffCode = "";
		}
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		if (null != staffName) {
			this.staffName = staffName;
		} else {
			this.staffName = "";
		}
	}

	public List<RptCustomerByRoutingDataVO> getListData() {
		return listData;
	}

	public void setListData(List<RptCustomerByRoutingDataVO> listData) {
		this.listData = listData;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		if (null != status) {
			this.status = status;
		} else {
			this.status = "";
		}
	}
}

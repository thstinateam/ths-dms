/*
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.core.entities.vo.rpt;

import java.util.Date;

/**
 * 
 * Auto generate entities - HoChiMinh standard
 * 
 * @author hunglm16
 * @since 26/08/2015
 * */
public class AppLogVO  {

	//ID bang
	private Long id;

	//Ma Don Vi
	private Long shopId;

	//ID Nhan vien
	private Long staffId;

	//ID Nhan vien thay the
	private Long ingeritStaffId;

	//ID Man hinh
	private Long formId;
	
	//ID Vai tro
	private Long roleId;

	//Thoi gian ghi Log
	private Date logDate;

	//Loai log
	private Integer logType;

	//Dia chi IP
	private String ipAddress;

	//Ma ung dung
	private String appCode;

	//URL Gui den server
	private String urlRequest;

	//Ngay tao
	private Date createDate;

	//Nguoi tao
	private String createUser;

	/**
	 * Khai bao cac phuong thuc GETTER/SETTER
	 * */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getIngeritStaffId() {
		return ingeritStaffId;
	}

	public void setIngeritStaffId(Long ingeritStaffId) {
		this.ingeritStaffId = ingeritStaffId;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public Integer getLogType() {
		return logType;
	}

	public void setLogType(Integer logType) {
		this.logType = logType;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getUrlRequest() {
		return urlRequest;
	}

	public void setUrlRequest(String urlRequest) {
		this.urlRequest = urlRequest;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

}
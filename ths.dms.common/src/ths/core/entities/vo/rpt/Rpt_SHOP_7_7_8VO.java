package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class Rpt_SHOP_7_7_8VO implements Serializable {

	/**
	 * @author hunglm16
	 * @since JUNE 11, 2014
	 * @description SHOP_REPPORT 7.7.8
	 */
	private static final long serialVersionUID = 1L;
	private String soDHHT;
	private BigDecimal triGiaDHSauThue;
	private String trangThai;
	private String maKH;
	private String tenKH;
	private String soHDGTGT;
	private String ngayHoaDon;
	private String ngayXuatHoaDon;
	private String maNV;
	private String tenNV;
	private Integer status;
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getSoDHHT() {
		return soDHHT;
	}
	public void setSoDHHT(String soDHHT) {
		this.soDHHT = soDHHT;
	}
	public BigDecimal getTriGiaDHSauThue() {
		return triGiaDHSauThue;
	}
	public void setTriGiaDHSauThue(BigDecimal triGiaDHSauThue) {
		this.triGiaDHSauThue = triGiaDHSauThue;
	}
	public String getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getSoHDGTGT() {
		return soHDGTGT;
	}
	public void setSoHDGTGT(String soHDGTGT) {
		this.soHDGTGT = soHDGTGT;
	}
	public String getNgayHoaDon() {
		return ngayHoaDon;
	}
	public void setNgayHoaDon(String ngayHoaDon) {
		this.ngayHoaDon = ngayHoaDon;
	}
	public String getNgayXuatHoaDon() {
		return ngayXuatHoaDon;
	}
	public void setNgayXuatHoaDon(String ngayXuatHoaDon) {
		this.ngayXuatHoaDon = ngayXuatHoaDon;
	}
	public String getMaNV() {
		return maNV;
	}
	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}
	public String getTenNV() {
		return tenNV;
	}
	public void setTenNV(String tenNV) {
		this.tenNV = tenNV;
	}
}

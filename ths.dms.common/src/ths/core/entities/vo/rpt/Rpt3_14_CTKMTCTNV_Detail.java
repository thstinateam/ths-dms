package ths.core.entities.vo.rpt;

import java.math.BigDecimal;


public class Rpt3_14_CTKMTCTNV_Detail implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 710560671268651768L;
	private String programName; //ten CTKM
	private String staff;
	private BigDecimal quantityPromotion; //so luoong km hang
	private BigDecimal amountPromotion;   //so tien km hang
	private BigDecimal moneyPromotion;    //so tien km bang tien
	private BigDecimal total;			  //tong cong

	public String getStaff() {
		return staff;
	}
	public void setStaff(String staff) {
		this.staff = staff;
	}
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	public BigDecimal getQuantityPromotion() {
		return quantityPromotion;
	}
	public void setQuantityPromotion(BigDecimal quantityPromotion) {
		this.quantityPromotion = quantityPromotion;
	}
	public BigDecimal getAmountPromotion() {
		return amountPromotion;
	}
	public void setAmountPromotion(BigDecimal amountPromotion) {
		this.amountPromotion = amountPromotion;
	}
	public BigDecimal getMoneyPromotion() {
		return moneyPromotion;
	}
	public void setMoneyPromotion(BigDecimal moneyPromotion) {
		this.moneyPromotion = moneyPromotion;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD17VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String maMien;
	private String maTinhThanh;
	private String tenTinhThanh;
	private Integer tongSoXa;
	private Integer tongSoXaPSDS;
	private Integer tongSoXaChuaPSDS;
	private Integer dsto500k;
	private Integer dsto500kto1m;
	private Integer ds1mto2m;
	private Integer ds2mto5m;
	private Integer ds5mto10m;
	private Integer ds10mto50m;
	private Integer ds50mto100m;
	private Integer ds100mto250m;
	private Integer ds250mto;
	private BigDecimal tongPSDS;
	
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaTinhThanh() {
		return maTinhThanh;
	}
	public void setMaTinhThanh(String maTinhThanh) {
		this.maTinhThanh = maTinhThanh;
	}
	public String getTenTinhThanh() {
		return tenTinhThanh;
	}
	public void setTenTinhThanh(String tenTinhThanh) {
		this.tenTinhThanh = tenTinhThanh;
	}
	public Integer getDsto500k() {
		return dsto500k;
	}
	public void setDsto500k(BigDecimal dsto500k) {
		if (dsto500k != null) {
			this.dsto500k = dsto500k.intValue();
		}
	}
	public Integer getDs1mto2m() {
		return ds1mto2m;
	}
	public void setDs1mto2m(BigDecimal ds1mto2m) {
		if (ds1mto2m != null) {
			this.ds1mto2m = ds1mto2m.intValue();
		}
	}
	public Integer getDs5mto10m() {
		return ds5mto10m;
	}
	public void setDs5mto10m(BigDecimal ds5mto10m) {
		if (ds5mto10m != null) {
			this.ds5mto10m = ds5mto10m.intValue();
		}
	}
	public Integer getDs10mto50m() {
		return ds10mto50m;
	}
	public void setDs10mto50m(BigDecimal ds10mto50m) {
		if (ds10mto50m != null) {
			this.ds10mto50m = ds10mto50m.intValue();
		}
	}
	public Integer getDs50mto100m() {
		return ds50mto100m;
	}
	public void setDs50mto100m(BigDecimal ds50mto100m) {
		if (ds50mto100m != null) {
			this.ds50mto100m = ds50mto100m.intValue();
		}
	}
	public Integer getDs100mto250m() {
		return ds100mto250m;
	}
	public void setDs100mto250m(BigDecimal ds100mto250m) {
		if (ds100mto250m != null) {
			this.ds100mto250m = ds100mto250m.intValue();
		}
	}
	public Integer getDs250mto() {
		return ds250mto;
	}
	public void setDs250mto(BigDecimal ds250mto) {
		if (ds250mto != null) {
			this.ds250mto = ds250mto.intValue();
		}
	}
	public BigDecimal getTongPSDS() {
		return tongPSDS;
	}
	public void setTongPSDS(BigDecimal tongPSDS) {
		this.tongPSDS = tongPSDS;
	}
	public Integer getTongSoXa() {
		return tongSoXa;
	}
	public void setTongSoXa(BigDecimal tongSoXa) {
		if (tongSoXa != null) {
			this.tongSoXa = tongSoXa.intValue();
		}
	}
	public Integer getTongSoXaPSDS() {
		return tongSoXaPSDS;
	}
	public void setTongSoXaPSDS(BigDecimal tongSoXaPSDS) {
		if (tongSoXaPSDS != null) {
			this.tongSoXaPSDS = tongSoXaPSDS.intValue();
		}
	}
	public Integer getTongSoXaChuaPSDS() {
		return tongSoXaChuaPSDS;
	}
	public void setTongSoXaChuaPSDS(BigDecimal tongSoXaChuaPSDS) {
		if (tongSoXaChuaPSDS != null) {
			this.tongSoXaChuaPSDS = tongSoXaChuaPSDS.intValue();
		}
	}
	public Integer getDsto500kto1m() {
		return dsto500kto1m;
	}
	public void setDsto500kto1m(BigDecimal dsto500kto1m) {
		if (dsto500kto1m != null) {
			this.dsto500kto1m = dsto500kto1m.intValue();
		}
	}
	public Integer getDs2mto5m() {
		return ds2mto5m;
	}
	public void setDs2mto5m(BigDecimal ds2mto5m) {
		if (ds2mto5m != null) {
			this.ds2mto5m = ds2mto5m.intValue();
		}
	}
	
}

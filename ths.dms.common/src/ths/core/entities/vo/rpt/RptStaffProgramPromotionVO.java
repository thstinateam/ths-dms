package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptStaffProgramPromotionVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<RptPromotionDetailStaff2VO> lstRptPromotionDetailStaff2VO = new ArrayList<RptPromotionDetailStaff2VO>();
	private String staffCode;
	private String staffName;
	

	/**
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}

	/**
	 * @param staffCode the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	/**
	 * @return the staffName
	 */
	public String getStaffName() {
		return staffName;
	}

	/**
	 * @param staffName the staffName to set
	 */
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	/**
	 * @return the lstRptPromotionDetailStaff1VO
	 */
	public List<RptPromotionDetailStaff2VO> getLstRptPromotionDetailStaff2VO() {
		return lstRptPromotionDetailStaff2VO;
	}

	/**
	 * @param lstRptPromotionDetailStaff1VO
	 *            the lstRptPromotionDetailStaff1VO to set
	 */
	public void setLstRptPromotionDetailStaff1VO(
			List<RptPromotionDetailStaff2VO> lstRptPromotionDetailStaff2VO) {
		this.lstRptPromotionDetailStaff2VO = lstRptPromotionDetailStaff2VO;
	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD11VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String maMien;
    private String maVung;
    private String maNpp;
    private String maNvbh;
    private String maDiemLe;
    private String muc;
    private BigDecimal keHoach;
    private BigDecimal thucHien;
    private BigDecimal thuaThieu;
    private Integer flag;
    private String charFlag;
    
	public String getCharFlag() {
		return charFlag;
	}
	public void setCharFlag(String charFlag) {
		this.charFlag = charFlag;
	}
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNpp() {
		return maNpp;
	}
	public void setMaNpp(String maNpp) {
		this.maNpp = maNpp;
	}
	public String getMaNvbh() {
		return maNvbh;
	}
	public void setMaNvbh(String maNvbh) {
		this.maNvbh = maNvbh;
	}
	public String getMaDiemLe() {
		return maDiemLe;
	}
	public void setMaDiemLe(String maDiemLe) {
		this.maDiemLe = maDiemLe;
	}
	public String getMuc() {
		return muc;
	}
	public void setMuc(String muc) {
		this.muc = muc;
	}
	public BigDecimal getKeHoach() {
		return keHoach;
	}
	public void setKeHoach(BigDecimal keHoach) {
		this.keHoach = keHoach;
	}
	public BigDecimal getThucHien() {
		return thucHien;
	}
	public void setThucHien(BigDecimal thucHien) {
		this.thucHien = thucHien;
	}
	public BigDecimal getThuaThieu() {
		return thuaThieu;
	}
	public void setThuaThieu(BigDecimal thuaThieu) {
		this.thuaThieu = thuaThieu;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(BigDecimal flag) {
		this.flag = flag.intValue();
	}
}
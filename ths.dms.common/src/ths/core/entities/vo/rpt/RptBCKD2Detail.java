package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

//doanh so phan phoi theo nhom hang
public class RptBCKD2Detail implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String tenNvbh;
	private String mien;
	private String vung;
	private String truongBanHangVung;
	private String maNpp;
	private String gsNpp;
	private String maNvbh;
	private String doiBanHang;
	private String nhan;
	private BigDecimal khtt;
	private Integer khPsdsThangTruoc;
	private Integer khPsdsThangNay;
	private BigDecimal dsThangNay;
	private BigDecimal dsThangTruoc;
	private BigDecimal doanhSoBinhQuanNgay;
	private Float phanTramThucHienKeHoach;
	private BigDecimal luyKeKH;
	private BigDecimal thieu;
	public String getTenNvbh() {
		return tenNvbh;
	}
	public void setTenNvbh(String tenNvbh) {
		this.tenNvbh = tenNvbh;
	}
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getTruongBanHangVung() {
		return truongBanHangVung;
	}
	public void setTruongBanHangVung(String truongBanHangVung) {
		this.truongBanHangVung = truongBanHangVung;
	}
	public String getMaNpp() {
		return maNpp;
	}
	public void setMaNpp(String maNpp) {
		this.maNpp = maNpp;
	}
	public String getGsNpp() {
		return gsNpp;
	}
	public void setGsNpp(String gsNpp) {
		this.gsNpp = gsNpp;
	}
	public String getMaNvbh() {
		return maNvbh;
	}
	public void setMaNvbh(String maNvbh) {
		this.maNvbh = maNvbh;
	}
	public String getDoiBanHang() {
		return doiBanHang;
	}
	public void setDoiBanHang(String doiBanHang) {
		this.doiBanHang = doiBanHang;
	}
	public String getNhan() {
		return nhan;
	}
	public void setNhan(String nhan) {
		this.nhan = nhan;
	}
	public BigDecimal getKhtt() {
		return khtt;
	}
	public void setKhtt(BigDecimal khtt) {
		this.khtt = khtt;
	}
	public Integer getKhPsdsThangTruoc() {
		return khPsdsThangTruoc;
	}
	public void setKhPsdsThangTruoc(BigDecimal khPsdsThangTruoc) {
		this.khPsdsThangTruoc = Integer.parseInt(khPsdsThangTruoc.toString());
	}
	public Integer getKhPsdsThangNay() {
		return khPsdsThangNay;
	}
	public void setKhPsdsThangNay(BigDecimal khPsdsThangNay) {
		this.khPsdsThangNay = Integer.parseInt(khPsdsThangNay.toString());
	}
	public BigDecimal getDsThangNay() {
		return dsThangNay;
	}
	public void setDsThangNay(BigDecimal dsThangNay) {
		this.dsThangNay = dsThangNay;
	}
	public BigDecimal getDsThangTruoc() {
		return dsThangTruoc;
	}
	public void setDsThangTruoc(BigDecimal dsThangTruoc) {
		this.dsThangTruoc = dsThangTruoc;
	}
	public BigDecimal getDoanhSoBinhQuanNgay() {
		return doanhSoBinhQuanNgay;
	}
	public void setDoanhSoBinhQuanNgay(BigDecimal doanhSoBinhQuanNgay) {
		this.doanhSoBinhQuanNgay = doanhSoBinhQuanNgay;
	}
	public Float getPhanTramThucHienKeHoach() {
		return phanTramThucHienKeHoach;
	}
	public void setPhanTramThucHienKeHoach(BigDecimal phanTramThucHienKeHoach) {
		this.phanTramThucHienKeHoach = Float.parseFloat(phanTramThucHienKeHoach.toString());
	}
	public BigDecimal getLuyKeKH() {
		return luyKeKH;
	}
	public void setLuyKeKH(BigDecimal luyKeKH) {
		this.luyKeKH = luyKeKH;
	}
	public BigDecimal getThieu() {
		return thieu;
	}
	public void setThieu(BigDecimal thieu) {
		this.thieu = thieu;
	}
}

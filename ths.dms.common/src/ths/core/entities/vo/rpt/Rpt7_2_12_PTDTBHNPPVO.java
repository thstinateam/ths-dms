package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class Rpt7_2_12_PTDTBHNPPVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3877270696091279101L;
	private String mien;
	private String tenMien;
	private BigDecimal dsKeHoach;
	private BigDecimal dsMTB;
	private BigDecimal dsThucTe;
	private List<Rpt7_2_12_VungVO> listVung;
	
	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getTenMien() {
		return tenMien;
	}

	public void setTenMien(String tenMien) {
		this.tenMien = tenMien;
	}

	public BigDecimal getDsKeHoach() {
		return dsKeHoach;
	}

	public void setDsKeHoach(BigDecimal dsKeHoach) {
		this.dsKeHoach = dsKeHoach;
	}

	public BigDecimal getDsMTB() {
		return dsMTB;
	}

	public void setDsMTB(BigDecimal dsMTB) {
		this.dsMTB = dsMTB;
	}

	public BigDecimal getDsThucTe() {
		return dsThucTe;
	}

	public void setDsThucTe(BigDecimal dsThucTe) {
		this.dsThucTe = dsThucTe;
	}

	public List<Rpt7_2_12_VungVO> getListVung() {
		return listVung;
	}

	public void setListVung(List<Rpt7_2_12_VungVO> listVung) {
		this.listVung = listVung;
	}

}

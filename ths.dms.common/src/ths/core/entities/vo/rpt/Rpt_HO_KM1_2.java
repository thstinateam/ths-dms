package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

public class Rpt_HO_KM1_2 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String maMien;
	private String maVung;
	private String loaiCTKM;
	private String shopCode;//Ma NPP
	private String shopName;//Ma NPP
	private String promotionCode;//maCTKM 
	private String promotionName;//tenCTKM
	private String promotionContent;//noi dung CTKM
	private Date promoFromDate;//ngay CTBD
	private Date promoToDate;//ngay CTKT
	private Date orderDate;//ngay
	private String productCode;//Ma SP
	private String productName;//Ten Sp
	private BigDecimal discountPromo;//Chi phi KM tien thuc te
	private BigDecimal quantityPromo;//So luong hang KM thuc te  
	private BigDecimal amountPromo;//Chi phi hang KM thuc te
	private BigDecimal quantityReal;//So luong thuc te
	private BigDecimal amountReal;//Doanh thu thuc te 
	
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public String getPromotionContent() {
		return promotionContent;
	}

	public void setPromotionContent(String promotionContent) {
		this.promotionContent = promotionContent;
	}

	public Date getPromoFromDate() {
		return promoFromDate;
	}

	public void setPromoFromDate(Date promoFromDate) {
		this.promoFromDate = promoFromDate;
	}

	public Date getPromoToDate() {
		return promoToDate;
	}

	public void setPromoToDate(Date promoToDate) {
		this.promoToDate = promoToDate;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getDiscountPromo() {
		return discountPromo;
	}

	public void setDiscountPromo(BigDecimal discountPromo) {
		this.discountPromo = discountPromo;
	}

	public BigDecimal getQuantityPromo() {
		return quantityPromo;
	}

	public void setQuantityPromo(BigDecimal quantityPromo) {
		this.quantityPromo = quantityPromo;
	}

	public BigDecimal getAmountPromo() {
		return amountPromo;
	}

	public void setAmountPromo(BigDecimal amountPromo) {
		this.amountPromo = amountPromo;
	}

	public BigDecimal getQuantityReal() {
		return quantityReal;
	}

	public void setQuantityReal(BigDecimal quantityReal) {
		this.quantityReal = quantityReal;
	}

	public BigDecimal getAmountReal() {
		return amountReal;
	}

	public void setAmountReal(BigDecimal amountReal) {
		this.amountReal = amountReal;
	}

	public String getMaMien() {
		return maMien;
	}

	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}

	public String getMaVung() {
		return maVung;
	}

	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}

	public String getLoaiCTKM() {
		return loaiCTKM;
	}

	public void setLoaiCTKM(String loaiCTKM) {
		this.loaiCTKM = loaiCTKM;
	}
	
}

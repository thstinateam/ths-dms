package ths.core.entities.vo.rpt;

import java.math.BigDecimal;
import java.util.List;


public class Rpt7_2_5_CTTT implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 710560671268651768L;
	private String code;
	private BigDecimal sumAmount;
	private BigDecimal sumPayAmount;
	private BigDecimal sumDebitAmount;
	
	private List<Rpt7_2_5_CTTT_Detail> listDetail;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public BigDecimal getSumAmount() {
		return sumAmount;
	}
	public void setSumAmount(BigDecimal sumAmount) {
		this.sumAmount = sumAmount;
	}
	public BigDecimal getSumPayAmount() {
		return sumPayAmount;
	}
	public void setSumPayAmount(BigDecimal sumPayAmount) {
		this.sumPayAmount = sumPayAmount;
	}
	public BigDecimal getSumDebitAmount() {
		return sumDebitAmount;
	}
	public void setSumDebitAmount(BigDecimal sumDebitAmount) {
		this.sumDebitAmount = sumDebitAmount;
	}
	public List<Rpt7_2_5_CTTT_Detail> getListDetail() {
		return listDetail;
	}
	public void setListDetail(List<Rpt7_2_5_CTTT_Detail> listDetail) {
		this.listDetail = listDetail;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

public class RptParentSaleOrderOfDeliveryVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String deliveryCode;
	private String deliveryName;
	ArrayList<RptSaleOrderOfDeliveryVO> listVO = new ArrayList<RptSaleOrderOfDeliveryVO>();
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public ArrayList<RptSaleOrderOfDeliveryVO> getListVO() {
		return listVO;
	}
	public void setListVO(ArrayList<RptSaleOrderOfDeliveryVO> listVO) {
		this.listVO = listVO;
	}
	
}

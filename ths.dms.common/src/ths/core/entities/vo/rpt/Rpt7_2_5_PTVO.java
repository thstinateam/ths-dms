package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class Rpt7_2_5_PTVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String shopName;
	private String shopAddress;
	private String cashierCode;
	private String cashierName;
	private BigDecimal totalAmount;
	private String strTotalAmount;
	private List<Rpt7_2_5_PT_Detail> lstDetail;
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String getCashierCode() {
		return cashierCode;
	}
	public void setCashierCode(String cashierCode) {
		this.cashierCode = cashierCode;
	}
	public String getCashierName() {
		return cashierName;
	}
	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getStrTotalAmount() {
		return strTotalAmount;
	}
	public void setStrTotalAmount(String strTotalAmount) {
		this.strTotalAmount = strTotalAmount;
	}
	public List<Rpt7_2_5_PT_Detail> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<Rpt7_2_5_PT_Detail> lstDetail) {
		this.lstDetail = lstDetail;
	}
	
	
}

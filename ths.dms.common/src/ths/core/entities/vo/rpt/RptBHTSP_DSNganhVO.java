package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


//danh sach san pham theo nhan vien ban hang
public class RptBHTSP_DSNganhVO implements Serializable{
	private String ten_nganh;
	private Integer so_san_pham;
	private Float thanh_tien;

	private List<RptBHTSP_SanPhamVO> danh_sach_san_pham;
	
	public RptBHTSP_DSNganhVO(List<RptBHTSP_ThongTinTVVO> information)
	{		
		danh_sach_san_pham = new ArrayList<RptBHTSP_SanPhamVO>();
		
		so_san_pham = 0;
		thanh_tien = (float)0;
		
		
		int iCount = information.size();
		if(iCount> 0)
		{
			ten_nganh = information.get(0).getProduct_info_code();
		}
		
			
		for(int i = 0; i < iCount; i++)
		{					
			RptBHTSP_ThongTinTVVO inforItem = information.get(i);
			RptBHTSP_SanPhamVO spExist = GetExistProductCode(inforItem.getProduct_code());
			if(spExist != null)
			{
				spExist.setSo_luong(inforItem.getQuantity() +  spExist.getSo_luong());
				//set don gia?
				continue;
			}
			RptBHTSP_SanPhamVO sanpham_item = new RptBHTSP_SanPhamVO(inforItem);
			danh_sach_san_pham.add(sanpham_item);
								
		}
		
		int iCountSP = danh_sach_san_pham.size();
		for(int i = 0; i < iCountSP; i++)
		{
			this.so_san_pham += danh_sach_san_pham.get(i).getSo_luong();
			this.thanh_tien += danh_sach_san_pham.get(i).getThanh_tien();
		}
		
		
		
		
	}
	
	public void setTen_nganh(String ten_nganh) {
		this.ten_nganh = ten_nganh;
	}
	public String getTen_nganh() {
		return ten_nganh;
	}
	public void setSo_san_pham(Integer so_san_pham) {
		this.so_san_pham = so_san_pham;
	}
	public Integer getSo_san_pham() {
		return so_san_pham;
	}
	public void setThanh_tien(Float thanh_tien) {
		this.thanh_tien = thanh_tien;
	}
	public Float getThanh_tien() {
		return thanh_tien;
	}
	
	public void setDanh_sach_san_pham(List<RptBHTSP_SanPhamVO> danh_sach_san_pham) {
		this.danh_sach_san_pham = danh_sach_san_pham;
		
		Float total = (float)0;
		int quantityTotal = 0;
		
		for (RptBHTSP_SanPhamVO sanpham_item : danh_sach_san_pham) 
		{
			total += sanpham_item.getThanh_tien();
			quantityTotal += sanpham_item.getSo_luong();
		}
		
		this.so_san_pham = quantityTotal;
	}

	public List<RptBHTSP_SanPhamVO> getDanh_sach_san_pham() {
		return danh_sach_san_pham;
	}
	
	private RptBHTSP_SanPhamVO GetExistProductCode(String productCode)
	{
		RptBHTSP_SanPhamVO dsSanPhamItem = null;
		if(danh_sach_san_pham != null)
		{
			for (RptBHTSP_SanPhamVO itemSP : danh_sach_san_pham)	 
			{
				if(itemSP.getMa_hang().equals(productCode))
				{
					dsSanPhamItem = itemSP;
					break;
				}
			}
		}
		return dsSanPhamItem;
	}

	
	
	
}

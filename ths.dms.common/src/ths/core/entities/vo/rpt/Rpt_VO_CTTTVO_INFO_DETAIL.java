package ths.core.entities.vo.rpt;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author cangnd
 *
 */
public class Rpt_VO_CTTTVO_INFO_DETAIL implements java.io.Serializable 
{
	
	private Date ngay_hop_dong;
	private String so_hop_dong = "";
	private String loai_no = "";
	private Date ngay_thanh_toan;
	private String loai_tt = "";
	private String so_cttt = "";
	private BigDecimal tien_tt;
	private BigDecimal tien_hop_dong;
	private String s_ngay_hop_dong;
	private String s_ngay_thanh_toan;
	private String ma_kh = "";
	private String nvtt = "";
	
	public Date getNgay_hop_dong() {
		return ngay_hop_dong;
	}
	public void setNgay_hop_dong(Date ngay_hop_dong) {
		this.ngay_hop_dong = ngay_hop_dong;
	}	

	public String getSo_hop_dong() {
		return so_hop_dong;
	}
	public void setSo_hop_dong(String so_hop_dong) {
		this.so_hop_dong = so_hop_dong;
	}
	public String getLoai_no() {
		return loai_no;
	}
	public void setLoai_no(String loai_no) {
		this.loai_no = loai_no;
	}

	public Date getNgay_thanh_toan() {
		return ngay_thanh_toan;
	}
	public void setNgay_thanh_toan(Date ngay_thanh_toan) {
		this.ngay_thanh_toan = ngay_thanh_toan;
	}
	public String getLoai_tt() {
		return loai_tt;
	}
	public void setLoai_tt(String loai_tt) {
		this.loai_tt = loai_tt;
	}
	public String getSo_cttt() {
		return so_cttt;
	}
	public void setSo_cttt(String so_cttt) {
		this.so_cttt = so_cttt;
	}

	public BigDecimal getTien_tt() {
		return tien_tt;
	}
	public void setTien_tt(BigDecimal tien_tt) {
		this.tien_tt = tien_tt;
	}

	public BigDecimal getTien_hop_dong() {
		return tien_hop_dong;
	}
	public void setTien_hop_dong(BigDecimal tien_hop_dong) {
		this.tien_hop_dong = tien_hop_dong;
	}
	
	public String getMa_kh() {
		return ma_kh;
	}
	public void setMa_kh(String ma_kh) {
		this.ma_kh = ma_kh;
	}
	public String getNvtt() {
		return nvtt;
	}
	public void setNvtt(String nvtt) {
		this.nvtt = nvtt;
	}
	public void setS_ngay_hop_dong(String s_ngay_hop_dong) {
		this.s_ngay_hop_dong = s_ngay_hop_dong;
	}
	public String getS_ngay_hop_dong() {
		if(s_ngay_hop_dong==null&&ngay_hop_dong!=null){
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			s_ngay_hop_dong = formatter.format(ngay_hop_dong);
		}
		return s_ngay_hop_dong;
	}
	public void setS_ngay_thanh_toan(String s_ngay_thanh_toan) {
		this.s_ngay_thanh_toan = s_ngay_thanh_toan;
	}
	public String getS_ngay_thanh_toan() {
		if(s_ngay_thanh_toan==null&&ngay_thanh_toan!=null){
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			s_ngay_thanh_toan = formatter.format(ngay_thanh_toan);
		}
		return s_ngay_thanh_toan;
	}
}

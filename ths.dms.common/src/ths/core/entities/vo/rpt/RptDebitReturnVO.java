package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptDebitReturnVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String shopRegionCode;
	private String shopAreaCode;
	private String shopCode;
	private String shopName;
	private String billCode;
	private String billDate;
	private String billType;
	private String deadline;
	private BigDecimal debitAmount;
	private BigDecimal discountAmount;
	private BigDecimal payedAmount;
	private BigDecimal remainAmount;
	private BigDecimal notexpiredAmount;
	private BigDecimal expiredAmount;
	private String invoiceNumber;
	private String invoiceDate;
	private String requestDate;
	private String receivedDate;

	public String getShopRegionCode() {
		return shopRegionCode;
	}

	public void setShopRegionCode(String shopRegionCode) {
		this.shopRegionCode = shopRegionCode;
	}

	public String getShopAreaCode() {
		return shopAreaCode;
	}

	public void setShopAreaCode(String shopAreaCode) {
		this.shopAreaCode = shopAreaCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getBillCode() {
		return billCode;
	}

	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public String getDeadline() {
		return deadline;
	}

	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getPayedAmount() {
		return payedAmount;
	}

	public void setPayedAmount(BigDecimal payedAmount) {
		this.payedAmount = payedAmount;
	}

	public BigDecimal getRemainAmount() {
		return remainAmount;
	}

	public void setRemainAmount(BigDecimal remainAmount) {
		this.remainAmount = remainAmount;
	}

	public BigDecimal getNotexpiredAmount() {
		return notexpiredAmount;
	}

	public void setNotexpiredAmount(BigDecimal notexpiredAmount) {
		this.notexpiredAmount = notexpiredAmount;
	}

	public BigDecimal getExpiredAmount() {
		return expiredAmount;
	}

	public void setExpiredAmount(BigDecimal expiredAmount) {
		this.expiredAmount = expiredAmount;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}

	public String getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

import ths.dms.core.entities.Shop;

public class RptPayDetailDisplayProgrVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<RptPayDetailDisplayProgr1VO> lstRptPayDetailDisplayProgr1VO = new ArrayList<RptPayDetailDisplayProgr1VO>();
	private Shop shop;
//	private DisplayProgram dpProgram;

	/**
	 * @return the lstRptPayDetailDisplayProgr1VO
	 */
	public ArrayList<RptPayDetailDisplayProgr1VO> getLstRptPayDetailDisplayProgr1VO() {
		return lstRptPayDetailDisplayProgr1VO;
	}

	/**
	 * @param lstRptPayDetailDisplayProgr1VO
	 *            the lstRptPayDetailDisplayProgr1VO to set
	 */
	public void setLstRptPayDetailDisplayProgr1VO(
			ArrayList<RptPayDetailDisplayProgr1VO> lstRptPayDetailDisplayProgr1VO) {
		this.lstRptPayDetailDisplayProgr1VO = lstRptPayDetailDisplayProgr1VO;
	}

	/**
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * @param shop
	 *            the shop to set
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * @return the dpProgram
	 */
//	public DisplayProgram getDpProgram() {
//		return dpProgram;
//	}
//
//	/**
//	 * @param dpProgram the dpProgram to set
//	 */
//	public void setDpProgram(DisplayProgram dpProgram) {
//		this.dpProgram = dpProgram;
//	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptCustomerByProductCatLv01VO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String customerCode;
	private String customerName;
	private List<RptCustomerByProductCatLv02VO> listData;

	public RptCustomerByProductCatLv01VO() {
		this.listData = new ArrayList<RptCustomerByProductCatLv02VO>();
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public List<RptCustomerByProductCatLv02VO> getListData() {
		return listData;
	}

	public void setListData(List<RptCustomerByProductCatLv02VO> listData) {
		this.listData = listData;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class Rpt7_7_6VO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String shopCode;
	private String shopName;
	private String shopPhone;
	private String shopAddress;
	private Integer tongSKU;
	private BigDecimal tongTTT;
	private BigDecimal tongTST;
	private List<Rpt7_7_6VODetail> lstDetail;
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopPhone() {
		return shopPhone;
	}
	public void setShopPhone(String shopPhone) {
		this.shopPhone = shopPhone;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public Integer getTongSKU() {
		return tongSKU;
	}
	public void setTongSKU(Integer tongSKU) {
		this.tongSKU = tongSKU;
	}
	public BigDecimal getTongTTT() {
		return tongTTT;
	}
	public void setTongTTT(BigDecimal tongTTT) {
		this.tongTTT = tongTTT;
	}
	public BigDecimal getTongTST() {
		return tongTST;
	}
	public void setTongTST(BigDecimal tongTST) {
		this.tongTST = tongTST;
	}
	public List<Rpt7_7_6VODetail> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<Rpt7_7_6VODetail> lstDetail) {
		this.lstDetail = lstDetail;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RptBCKD15VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer soThang;
	private Date thangBC;
	private List<RptBCKD15DataVO> lstDetail;

	public Integer getSoThang() {
		return soThang;
	}

	public void setSoThang(Integer soThang) {
		this.soThang = soThang;
	}

	public Date getThangBC() {
		return thangBC;
	}

	public void setThangBC(Date thangBC) {
		this.thangBC = thangBC;
	}

	public List<RptBCKD15DataVO> getLstDetail() {
		return lstDetail;
	}

	public void setLstDetail(List<RptBCKD15DataVO> lstDetail) {
		this.lstDetail = lstDetail;
	}
}

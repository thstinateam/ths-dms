package ths.core.entities.vo.rpt;

import java.util.List;

public class RptDSTMCTTBListVO {
	private List<RptDSTMCTTBVO> lstData;
	public RptDSTMCTTBListVO(List<RptDSTMCTTBVO> lst){
		lstData = lst;
	}
	public List<RptDSTMCTTBVO> getLstData() {
		return lstData;
	}

	public void setLstData(List<RptDSTMCTTBVO> lstData) {
		this.lstData = lstData;
	}
}

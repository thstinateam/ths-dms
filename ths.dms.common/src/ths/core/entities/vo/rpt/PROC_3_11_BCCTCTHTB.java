package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * @author hunglm16
 * @since January 21, 2014
 * @description report Ds3: Bao cao chi tiet chi tra hang TB
 * */
public class PROC_3_11_BCCTCTHTB implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer stt;
	public Integer getStt() {
		return stt;
	}
	public void setStt(Integer stt) {
		this.stt = stt;
	}
	private String soHoaDon;
	private String maKhachHang;
	private String tenHang;
	private Integer soLuongKMHang;
	
	private BigDecimal donGia;
	private BigDecimal thanhTien;
	private String maCTTB;
	private String strCTTB;
	
	public String getStrCTTB() {
		return strCTTB;
	}
	public void setStrCTTB(String strCTTB) {
		this.strCTTB = strCTTB;
	}
	public String getSoHoaDon() {
		return soHoaDon;
	}
	public void setSoHoaDon(String soHoaDon) {
		this.soHoaDon = soHoaDon;
	}
	public String getMaKhachHang() {
		return maKhachHang;
	}
	public void setMaKhachHang(String maKhachHang) {
		this.maKhachHang = maKhachHang;
	}
	public String getTenHang() {
		return tenHang;
	}
	public void setTenHang(String tenHang) {
		this.tenHang = tenHang;
	}
	public Integer getSoLuongKMHang() {
		return soLuongKMHang;
	}
	public void setSoLuongKMHang(Integer soLuongKMHang) {
		this.soLuongKMHang = soLuongKMHang;
	}
	public BigDecimal getDonGia() {
		return donGia;
	}
	public void setDonGia(BigDecimal donGia) {
		this.donGia = donGia;
	}
	public BigDecimal getThanhTien() {
		return thanhTien;
	}
	public void setThanhTien(BigDecimal thanhTien) {
		this.thanhTien = thanhTien;
	}
	public String getMaCTTB() {
		return maCTTB;
	}
	public void setMaCTTB(String maCTTB) {
		this.maCTTB = maCTTB;
	}

}

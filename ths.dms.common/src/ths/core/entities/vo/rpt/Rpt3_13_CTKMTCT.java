package ths.core.entities.vo.rpt;

import java.math.BigDecimal;
import java.util.List;


public class Rpt3_13_CTKMTCT implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 710560671268651768L;
	private String programName; //ten CTKM
	private BigDecimal quantityPromotion; //so luong km hang
	private BigDecimal amountPromotion;   //so tien km hang
	private BigDecimal moneyPromotion;    //so tien km bang tien
	private BigDecimal total;			  //tong cong
	private BigDecimal quantityMPromotion; //so luong km hang
	private List<Rpt3_13_CTKMTCT_Date> listDate;

	
	public BigDecimal getQuantityMPromotion() {
		if (quantityMPromotion != null) {
			return quantityMPromotion;
		}
		return BigDecimal.ZERO;
	}
	public void setQuantityMPromotion(BigDecimal quantityMPromotion) {
		this.quantityMPromotion = quantityMPromotion;
	}
	
	public List<Rpt3_13_CTKMTCT_Date> getListDate() {
		return listDate;
	}
	public void setListDate(List<Rpt3_13_CTKMTCT_Date> listDate) {
		this.listDate = listDate;
	}
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	public BigDecimal getQuantityPromotion() {
		if (quantityPromotion != null) {
			return quantityPromotion;
		}
		return BigDecimal.ZERO;
	}
	public void setQuantityPromotion(BigDecimal quantityPromotion) {
		this.quantityPromotion = quantityPromotion;
	}
	public BigDecimal getAmountPromotion() {
		if (amountPromotion != null) {
			return amountPromotion;
		}
		return BigDecimal.ZERO;
	}
	public void setAmountPromotion(BigDecimal amountPromotion) {
		this.amountPromotion = amountPromotion;
	}
	public BigDecimal getMoneyPromotion() {
		if (moneyPromotion != null) {
			return moneyPromotion;
		}
		return BigDecimal.ZERO;
	}
	public void setMoneyPromotion(BigDecimal moneyPromotion) {
		this.moneyPromotion = moneyPromotion;
	}
	public BigDecimal getTotal() {
		if (total != null) {
			return total;
		}
		return BigDecimal.ZERO;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
}

package ths.core.entities.vo.rpt;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

public class RptPoStatusTracking3VO {
	private Date poAutoDate;
	
	private String date;
	
	private String poAutoNumber;
	
	private List<RptPoStatusTrackingVO> lstRptPoStatusTrackingVO;

	public Date getPoAutoDate() {
		return poAutoDate;
	}

	public void setPoAutoDate(Date poAutoDate) {
		this.poAutoDate = poAutoDate;
		date = "";
		if (poAutoDate == null)
			return;
		Object[] params = new Object[] { poAutoDate };

		try {
			date = MessageFormat
					.format("{0,date," + "dd/MM/yyyy" + "}", params);
		} catch (Exception e) {

		}
	}

	public String getPoAutoNumber() {
		return poAutoNumber;
	}

	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}

	public List<RptPoStatusTrackingVO> getLstRptPoStatusTrackingVO() {
		return lstRptPoStatusTrackingVO;
	}

	public void setLstRptPoStatusTrackingVO(
			List<RptPoStatusTrackingVO> lstRptPoStatusTrackingVO) {
		this.lstRptPoStatusTrackingVO = lstRptPoStatusTrackingVO;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}
}

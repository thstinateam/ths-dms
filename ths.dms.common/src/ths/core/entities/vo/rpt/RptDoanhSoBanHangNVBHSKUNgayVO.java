package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RptDoanhSoBanHangNVBHSKUNgayVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer soNgayBanHang;
	private Integer soNgayThucHien;
	private Float tienDoChuan;
	private Float tienDoThucHien;
	private Date ngayBatDau;
	private Date ngayKetThuc;
	private List<RptDoanhSoBanHangNVBHSKUNgayDataVO> data;

	public RptDoanhSoBanHangNVBHSKUNgayVO() {
	}

	public Integer getSoNgayBanHang() {
		return soNgayBanHang;
	}

	public void setSoNgayBanHang(Integer soNgayBanHang) {
		this.soNgayBanHang = soNgayBanHang;
	}

	public Integer getSoNgayThucHien() {
		return soNgayThucHien;
	}

	public void setSoNgayThucHien(Integer soNgayThucHien) {
		this.soNgayThucHien = soNgayThucHien;
	}

	public Float getTienDoChuan() {
		return tienDoChuan;
	}

	public void setTienDoChuan(Float tienDoChuan) {
		this.tienDoChuan = tienDoChuan;
	}

	public Float getTienDoThucHien() {
		return tienDoThucHien;
	}

	public void setTienDoThucHien(Float tienDoThucHien) {
		this.tienDoThucHien = tienDoThucHien;
	}

	public List<RptDoanhSoBanHangNVBHSKUNgayDataVO> getData() {
		return data;
	}

	public void setData(List<RptDoanhSoBanHangNVBHSKUNgayDataVO> data) {
		this.data = data;
	}

	public Date getNgayBatDau() {
		return ngayBatDau;
	}

	public void setNgayBatDau(Date ngayBatDau) {
		this.ngayBatDau = ngayBatDau;
	}

	public Date getNgayKetThuc() {
		return ngayKetThuc;
	}

	public void setNgayKetThuc(Date ngayKetThuc) {
		this.ngayKetThuc = ngayKetThuc;
	}
}
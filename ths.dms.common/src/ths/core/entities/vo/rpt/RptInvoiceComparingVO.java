package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;

import ths.dms.core.entities.enumtype.ActiveType;

public class RptInvoiceComparingVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String orderNumber;
	private String saleOverValue;
	private Integer status;
	private String customerCode;
	private String customerName;
	private String invoiceNumber;
	private Date orderDate;
	private Date invoiceDate;
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getSaleOverValue() {
		return saleOverValue;
	}
	public void setSaleOverValue(String saleOverValue) {
		this.saleOverValue = saleOverValue;
	}
	public ActiveType getStatus() {
		return ActiveType.parseValue(status);
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class Rpt_SHOP_7_7_8VO_PFD implements Serializable {

	/**
	 * @author hunglm16
	 * @since JUNE 11, 2014
	 * @description SHOP_REPPORT 7.7.8
	 */
	private static final long serialVersionUID = 1L;
	private String shopCode;
	private String shopName;
	private String shopAddress;
	private String shopPhone;
	private Integer tongSoDHDaIn;
	private Integer tongCongSoDHHT;
	private BigDecimal tongGiaTriSauThue;
	
	public Integer getTongSoDHDaIn() {
		return tongSoDHDaIn;
	}
	public void setTongSoDHDaIn(Integer tongSoDHDaIn) {
		this.tongSoDHDaIn = tongSoDHDaIn;
	}
	public Integer getTongCongSoDHHT() {
		return tongCongSoDHHT;
	}
	public void setTongCongSoDHHT(Integer tongCongSoDHHT) {
		this.tongCongSoDHHT = tongCongSoDHHT;
	}
	public BigDecimal getTongGiaTriSauThue() {
		return tongGiaTriSauThue;
	}
	public void setTongGiaTriSauThue(BigDecimal tongGiaTriSauThue) {
		this.tongGiaTriSauThue = tongGiaTriSauThue;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String getShopPhone() {
		return shopPhone;
	}
	public void setShopPhone(String shopPhone) {
		this.shopPhone = shopPhone;
	}
	private List<Rpt_SHOP_7_7_8VO> lstDetail;
	public List<Rpt_SHOP_7_7_8VO> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<Rpt_SHOP_7_7_8VO> lstDetail) {
		this.lstDetail = lstDetail;
	}
}

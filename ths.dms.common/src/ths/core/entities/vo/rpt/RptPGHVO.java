package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.entities.Customer;

public class RptPGHVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// danh sach mat hang ban
	private List<RptPGHDetailVO> lstSaleProduct =  new ArrayList<RptPGHDetailVO>();
	
	// danh sach mat hang khuyen mai
	private List<RptPGHDetailVO> lstPromoProduct = new ArrayList<RptPGHDetailVO>();
	
	// ma - ten - so dien thoai khach hang
	private String customerInfo;
	
	private Customer customer;
	
	// ten va xe nvgh
	private String deliveryStaffInfo;
	
	// dia chi giao hang
	private String deliveryAddress;
	
	/** Thong tin khac cua don hang */
	private String otherInformation;

//	private String staffInfo;
	
	// danh sach hoa don gop
	private String lstOrderNumber;
	
	//danh sach ngay hoa don
	private String lstOrderDate;
	
	//danh sach nhan vien giao hang
	private String lstDelivery;
	
	//tong tien phai tra
	private BigDecimal total;
		
	//chiet khau hoa don
	private BigDecimal billDiscount = new BigDecimal(0);
	
	//chiet khau tien
	private BigDecimal moneyDiscount;
	
	//dieu chinh
	private BigDecimal modifiedValue = new BigDecimal(0);
	
	//gia tri hang khuyen mai
	private BigDecimal promotionValue = new BigDecimal(0);
	
	//tien chua chiet khau
	private BigDecimal amount;
	
	private Long deliveryId;
	
	private Long customerId;
	
	private String totalString;	

	private String phone;
	
	private String address;
	
	private String orderDate;
	
	private Integer sumThung = 0;
	
	private Integer sumLe = 0;

	public String getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(String customerInfo) {
		this.customerInfo = customerInfo;
	}

	public String getDeliveryStaffInfo() {
		return deliveryStaffInfo;
	}

	public void setDeliveryStaffInfo(String deliveryStaffInfo) {
		this.deliveryStaffInfo = deliveryStaffInfo;
	}

	public String getLstOrderNumber() {
		return lstOrderNumber;
	}

	public void setLstOrderNumber(String lstOrderNumber) {
		this.lstOrderNumber = lstOrderNumber;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getBillDiscount() {
		return billDiscount;
	}

	public void setBillDiscount(BigDecimal billDiscount) {
		this.billDiscount = billDiscount;
	}

	public BigDecimal getMoneyDiscount() {
		return moneyDiscount;
	}

	public void setMoneyDiscount(BigDecimal moneyDiscount) {
		this.moneyDiscount = moneyDiscount;
	}

	public BigDecimal getModifiedValue() {
		return modifiedValue;
	}

	public void setModifiedValue(BigDecimal modifiedValue) {
		this.modifiedValue = modifiedValue;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public String getDeliveryAddress() {
		return deliveryAddress== null?"":deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public BigDecimal getPromotionValue() {
		return promotionValue;
	}

	public void setPromotionValue(BigDecimal promotionValue) {
		this.promotionValue = promotionValue;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getTotalString() {
		return totalString== null?"":totalString;
	}

	public void setTotalString(String totalString) {
		this.totalString = totalString;
	}

	public List<RptPGHDetailVO> getLstSaleProduct() {
		return lstSaleProduct;
	}

	public void setLstSaleProduct(List<RptPGHDetailVO> lstSaleProduct) {
		this.lstSaleProduct = lstSaleProduct;
	}

	public List<RptPGHDetailVO> getLstPromoProduct() {
		return lstPromoProduct;
	}

	public void setLstPromoProduct(List<RptPGHDetailVO> lstPromoProduct) {
		this.lstPromoProduct = lstPromoProduct;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getOtherInformation() {
		return otherInformation;
	}

	public void setOtherInformation(String otherInformation) {
		this.otherInformation = otherInformation;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getLstOrderDate() {
		return lstOrderDate;
	}

	public void setLstOrderDate(String lstOrderDate) {
		this.lstOrderDate = lstOrderDate;
	}

	public String getLstDelivery() {
		return lstDelivery;
	}

	public void setLstDelivery(String lstDelivery) {
		this.lstDelivery = lstDelivery;
	}

	public Integer getSumThung() {
		return sumThung;
	}

	public void setSumThung(Integer sumThung) {
		this.sumThung = sumThung;
	}

	public Integer getSumLe() {
		return sumLe;
	}

	public void setSumLe(Integer sumLe) {
		this.sumLe = sumLe;
	}
	
}

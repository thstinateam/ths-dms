package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD15DataVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String mien;
	private String khuVuc;
	private String maNPP;
	private String maKH;
	private String tenKH;
	private String diaChiKH;
	private String kenh;
	private String thang;
	private BigDecimal phatSinh;

	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getKhuVuc() {
		return khuVuc;
	}

	public void setKhuVuc(String khuVuc) {
		this.khuVuc = khuVuc;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getMaKH() {
		return maKH;
	}

	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}

	public String getTenKH() {
		return tenKH;
	}

	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}

	public String getDiaChiKH() {
		return diaChiKH;
	}

	public void setDiaChiKH(String diaChiKH) {
		this.diaChiKH = diaChiKH;
	}

	public String getKenh() {
		return kenh;
	}

	public void setKenh(String kenh) {
		this.kenh = kenh;
	}

	public String getThang() {
		return thang;
	}

	public void setThang(String thang) {
		this.thang = thang;
	}

	public BigDecimal getPhatSinh() {
		return phatSinh;
	}

	public void setPhatSinh(BigDecimal phatSinh) {
		this.phatSinh = phatSinh;
	}
}

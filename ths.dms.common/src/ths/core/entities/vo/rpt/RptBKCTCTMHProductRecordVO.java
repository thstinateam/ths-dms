package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author hunglm16
 * @since MAY 30,2014
 * @description Bao cao 7.3.7 Bang ke chi tiet CT mua hang 
 */
public class RptBKCTCTMHProductRecordVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String saleOrderNumber;
	private String purchaseOrder;
	private String orderDate;
	private String poConfirm;
	private String invoiceNumber;
	private String importDate;
	private BigDecimal status;
	private String statusStr;
	private String productCode;
	private String productName;
	private BigDecimal quantity;
	private BigDecimal price;
	private BigDecimal total;
	private String povnmDate;
	
	
	public String getPovnmDate() {
		return povnmDate;
	}
	public void setPovnmDate(String povnmDate) {
		this.povnmDate = povnmDate;
	}
	public String getStatusStr() {
		//if (statusStr.toUpperCase().equals("isDaNhap".toUpperCase()))
		if(statusStr.toUpperCase().equals("isDaNhap".toUpperCase())) {
			return "Đã nhập";
		} else {
			return "Chưa nhập";
		}
		
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public String getSaleOrderNumber() {
		return saleOrderNumber;
	}
	public void setSaleOrderNumber(String saleOrderNumber) {
		this.saleOrderNumber = saleOrderNumber;
	}
	public String getPurchaseOrder() {
		return purchaseOrder;
	}
	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getPoConfirm() {
		return poConfirm;
	}
	public void setPoConfirm(String poConfirm) {
		this.poConfirm = poConfirm;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getImportDate() {
		return importDate;
	}
	public void setImportDate(String importDate) {
		this.importDate = importDate;
	}
	public BigDecimal getStatus() {
		return status;
	}
	public void setStatus(BigDecimal status) {
		this.status = status;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getTotal() {
		if(total==null){
			total = price.multiply(quantity);
		}
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	
}

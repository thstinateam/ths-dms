package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

public class RptCustomerPayReceived1VO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String customerCode;
	
	private List<RptCustomerPayReceivedVO> lstRptCustomerPayReceivedVO;

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public List<RptCustomerPayReceivedVO> getLstRptCustomerPayReceivedVO() {
		return lstRptCustomerPayReceivedVO;
	}

	public void setLstRptCustomerPayReceivedVO(
			List<RptCustomerPayReceivedVO> lstRptCustomerPayReceivedVO) {
		this.lstRptCustomerPayReceivedVO = lstRptCustomerPayReceivedVO;
	}
	
	
}

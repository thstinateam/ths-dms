package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptKD_8_1_CTVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String mien;
	private String vung;
	private String maNPP;
	private String tenNPP;
	private String maNVBH;
	private String tenNVBH;
	private String doi;
	private Float diemTheoTuyen;
	private Float diemDoanhSo;
	private BigDecimal mucTieu;// ke hoach tieu thu
	private BigDecimal thucHien;// ke hoach luy ke
	private Float tyLe;// ty le
	private Float lkDiemTheoTuyen;
	private Integer lkXepHangTheoTuyen;
	private BigDecimal lkMucTieuDoanhSo;
	private BigDecimal lkThucHienDoanhSo;//
	private Float lkTyLe;// ty le
	private Float lkDiemDoanhSo;
	private Integer lkXepHangDoanhSo;
	private Integer lkSoLanNhoHonBa;

	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getTenNPP() {
		return tenNPP;
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}

	public String getMaNVBH() {
		return maNVBH;
	}

	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}

	public String getTenNVBH() {
		return tenNVBH;
	}

	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}

	public String getDoi() {
		return doi;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public Float getDiemTheoTuyen() {
		return diemTheoTuyen;
	}

	public void setDiemTheoTuyen(BigDecimal diemTheoTuyen) {
		if (null != diemTheoTuyen) {
			this.diemTheoTuyen = diemTheoTuyen.floatValue();
		}
	}

	public Float getDiemDoanhSo() {
		return diemDoanhSo;
	}

	public void setDiemDoanhSo(BigDecimal diemDoanhSo) {
		if (null != diemDoanhSo) {
			this.diemDoanhSo = diemDoanhSo.floatValue();
		}
	}

	public BigDecimal getMucTieu() {
		return mucTieu;
	}

	public void setMucTieu(BigDecimal mucTieu) {
		this.mucTieu = mucTieu;
	}

	public BigDecimal getThucHien() {
		return thucHien;
	}

	public void setThucHien(BigDecimal thucHien) {
		this.thucHien = thucHien;
	}

	public Float getTyLe() {
		return tyLe;
	}

	public void setTyLe(BigDecimal tyLe) {
		if (null != tyLe) {
			this.tyLe = tyLe.floatValue();
		}
	}

	public Float getLkDiemTheoTuyen() {
		return lkDiemTheoTuyen;
	}

	public void setLkDiemTheoTuyen(BigDecimal lkDiemTheoTuyen) {
		if (null != lkDiemTheoTuyen) {
			this.lkDiemTheoTuyen = lkDiemTheoTuyen.floatValue();
		}
	}

	public Integer getLkXepHangTheoTuyen() {
		return lkXepHangTheoTuyen;
	}

	public void setLkXepHangTheoTuyen(BigDecimal lkXepHangTheoTuyen) {
		if (null != lkXepHangTheoTuyen) {
			this.lkXepHangTheoTuyen = lkXepHangTheoTuyen.intValue();
		}
	}

	public BigDecimal getLkMucTieuDoanhSo() {
		return lkMucTieuDoanhSo;
	}

	public void setLkMucTieuDoanhSo(BigDecimal lkMucTieuDoanhSo) {
		if (null != lkMucTieuDoanhSo) {
			this.lkMucTieuDoanhSo = lkMucTieuDoanhSo;
		}
	}

	public BigDecimal getLkThucHienDoanhSo() {
		return lkThucHienDoanhSo;
	}

	public void setLkThucHienDoanhSo(BigDecimal lkThucHienDoanhSo) {
		if (null != lkThucHienDoanhSo) {
			this.lkThucHienDoanhSo = lkThucHienDoanhSo;
		}
	}

	public Float getLkTyLe() {
		return lkTyLe;
	}

	public void setLkTyLe(BigDecimal lkTyLe) {
		if (null != lkTyLe) {
			this.lkTyLe = lkTyLe.floatValue();
		}
	}

	public Float getLkDiemDoanhSo() {
		return lkDiemDoanhSo;
	}

	public void setLkDiemDoanhSo(BigDecimal lkDiemDoanhSo) {
		if (null != lkDiemDoanhSo) {
			this.lkDiemDoanhSo = lkDiemDoanhSo.floatValue();
		}
	}

	public Integer getLkXepHangDoanhSo() {
		return lkXepHangDoanhSo;
	}

	public void setLkXepHangDoanhSo(BigDecimal lkXepHangDoanhSo) {
		if (null != lkXepHangDoanhSo) {
			this.lkXepHangDoanhSo = lkXepHangDoanhSo.intValue();
		}
	}

	public Integer getLkSoLanNhoHonBa() {
		return lkSoLanNhoHonBa;
	}

	public void setLkSoLanNhoHonBa(BigDecimal lkSoLanNhoHonBa) {
		if (null != lkSoLanNhoHonBa) {
			this.lkSoLanNhoHonBa = lkSoLanNhoHonBa.intValue();
		}
	}
}
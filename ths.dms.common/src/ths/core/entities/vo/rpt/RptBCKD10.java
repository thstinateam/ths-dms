package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

//doanh so phan phoi theo nhom hang
public class RptBCKD10 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer soNgayBanHang;
	private Integer soNgayThucHien;
	private Float tienDoChuan;
	private Date tuNgay;
	private Date denNgay;
	private List<RptBCKD10VO> lstDetail;
	
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public Integer getSoNgayBanHang() {
		return soNgayBanHang;
	}
	public void setSoNgayBanHang(Integer soNgayBanHang) {
		this.soNgayBanHang = soNgayBanHang;
	}
	public Integer getSoNgayThucHien() {
		return soNgayThucHien;
	}
	public void setSoNgayThucHien(Integer soNgayThucHien) {
		this.soNgayThucHien = soNgayThucHien;
	}
	public Float getTienDoChuan() {
		return tienDoChuan;
	}
	public void setTienDoChuan(Float tienDoChuan) {
		this.tienDoChuan = tienDoChuan;
	}
	public Date getTuNgay() {
		return tuNgay;
	}
	public void setTuNgay(Date tuNgay) {
		this.tuNgay = tuNgay;
	}
	public Date getDenNgay() {
		return denNgay;
	}
	public void setDenNgay(Date denNgay) {
		this.denNgay = denNgay;
	}
	public List<RptBCKD10VO> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<RptBCKD10VO> lstDetail) {
		this.lstDetail = lstDetail;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;


public class RptBCDKTT_9_1VO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String soTheKiemKho;
	private String maHang;
	private String tenHang;
	private String soLo;
	private String ngayTaoThe;
	private String tonKhoThucTe;
	public String getSoTheKiemKho() {
		return soTheKiemKho;
	}
	public void setSoTheKiemKho(String soTheKiemKho) {
		this.soTheKiemKho = soTheKiemKho;
	}
	public String getMaHang() {
		return maHang;
	}
	public void setMaHang(String maHang) {
		this.maHang = maHang;
	}
	public String getTenHang() {
		return tenHang;
	}
	public void setTenHang(String tenHang) {
		this.tenHang = tenHang;
	}
	public String getSoLo() {
		return soLo;
	}
	public void setSoLo(String soLo) {
		this.soLo = soLo;
	}
	public String getNgayTaoThe() {
		return ngayTaoThe;
	}
	public void setNgayTaoThe(String ngayTaoThe) {
		this.ngayTaoThe = ngayTaoThe;
	}
	public String getTonKhoThucTe() {
		return tonKhoThucTe;
	}
	public void setTonKhoThucTe(String tonKhoThucTe) {
		this.tonKhoThucTe = tonKhoThucTe;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}

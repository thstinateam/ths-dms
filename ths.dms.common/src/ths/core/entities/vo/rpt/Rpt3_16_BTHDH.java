package ths.core.entities.vo.rpt;

import java.math.BigDecimal;


public class Rpt3_16_BTHDH implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8296834030673089312L;
	
	private String orderDate; //Ngay hoa don
	private String customerCode; //Ten khach hang
	private String oddName; //Ten diem le
	private String houseNumber; //Số
	private String streetName; //Ten duong
	private String district;	//Quan
	private String productCode; //Ma hang
	private BigDecimal amount; //Thanh tien
	private String orderNumber; //So HĐ
	private BigDecimal total; //Tong cong
	private BigDecimal groupAmount; //Doanh so nhom
	private BigDecimal allowAmount; //Dinh muc cho phep
	private BigDecimal suggestAmount; //NPP de nghi VNM thanh toan
	
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getOddName() {
		return oddName;
	}
	public void setOddName(String oddName) {
		this.oddName = oddName;
	}

	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getGroupAmount() {
		return groupAmount;
	}
	public void setGroupAmount(BigDecimal groupAmount) {
		this.groupAmount = groupAmount;
	}
	public BigDecimal getAllowAmount() {
		return allowAmount;
	}
	public void setAllowAmount(BigDecimal allowAmount) {
		this.allowAmount = allowAmount;
	}
	public BigDecimal getSuggestAmount() {
		return suggestAmount;
	}
	public void setSuggestAmount(BigDecimal suggestAmount) {
		this.suggestAmount = suggestAmount;
	}
}

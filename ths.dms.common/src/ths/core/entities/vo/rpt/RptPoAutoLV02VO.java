package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ths.dms.core.common.utils.GroupUtility;

public class RptPoAutoLV02VO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Date poDate;
	private String strPoDate;
	private String shopCode;
	private List<RptPoAutoLV03VO> listData;

	public RptPoAutoLV02VO() {
		listData = new ArrayList<RptPoAutoLV03VO>();
	}

	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		this.strPoDate = format.format(poDate);
	}

	public List<RptPoAutoLV03VO> getListData() {
		return listData;
	}

	public void setListData(List<RptPoAutoLV03VO> listData) {
		this.listData = listData;
	}

	public void setStrPoDate(String strPoDate) {
		this.strPoDate = strPoDate;
	}

	public String getStrPoDate() {
		return strPoDate;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	@SuppressWarnings("unchecked")
	public static List<RptPoAutoLV02VO> groupBy(
			List<RptPoAutoLV03VO> listData, String fieldName)
			throws Exception {
		List<RptPoAutoLV02VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, fieldName);

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptPoAutoLV02VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptPoAutoLV02VO vo = new RptPoAutoLV02VO();
					List<RptPoAutoLV03VO> listObject = (List<RptPoAutoLV03VO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setStrPoDate((String) mapEntry.getKey());
						vo.setListData(listObject);
						vo.setShopCode(listObject.get(0).getShopCode());

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}
}

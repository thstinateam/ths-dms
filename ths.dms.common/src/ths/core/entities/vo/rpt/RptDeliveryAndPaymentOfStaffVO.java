package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class RptDeliveryAndPaymentOfStaffVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<RptSaleOrderLotVO> listProduct;
	private List<RptSaleOrderLotVO> listPromotionProduct;

	private boolean isInvoice;
	
	private String customerCode;
	private String shortCode;
	private String customerName;
	private String customerPhoneNumber;
	private String customerAddress;
	private String invoiceNumber;

	private String deliveryStaff;
	private String deliveryName;
	private String staffName;
	private String saleStaff;
	private String checkHD;
	
	private Date invoiceDate;

	// chua chiet khau
	private BigDecimal amount;
	// chiet khau hoa don
	private BigDecimal discount;
	
	// chiet khau tien
	private BigDecimal discountAmount;
	
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	// toan bo
	private BigDecimal total;
	
	private String totalString;
	private BigDecimal totalWeight = new BigDecimal(0);

	//saleOrderId ma don hang SangTN
	private Long saleOrderId;
	
	public Long getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public List<RptSaleOrderLotVO> getListProduct() {
		return listProduct;
	}

	public void setListProduct(List<RptSaleOrderLotVO> listProduct) {
		this.listProduct = listProduct;
	}

	public List<RptSaleOrderLotVO> getListPromotionProduct() {
		return listPromotionProduct;
	}

	public void setListPromotionProduct(
			List<RptSaleOrderLotVO> listPromotionProduct) {
		this.listPromotionProduct = listPromotionProduct;
	}

	public boolean isInvoice() {
		return isInvoice;
	}

	public void setInvoice(boolean isInvoice) {
		this.isInvoice = isInvoice;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getDeliveryStaff() {
		return deliveryStaff;
	}

	public void setDeliveryStaff(String deliveryStaff) {
		this.deliveryStaff = deliveryStaff;
	}

	public String getSaleStaff() {
		return saleStaff;
	}

	public void setSaleStaff(String saleStaff) {
		this.saleStaff = saleStaff;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getTotalString() {
		return totalString;
	}

	public void setTotalString(String totalString) {
		this.totalString = totalString;
	}

	public void setCheckHD(String checkHD) {
		this.checkHD = checkHD;
	}

	public String getCheckHD() {
		return checkHD;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public BigDecimal getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ths.dms.core.common.utils.GroupUtility;

public class RptExImStockOfStaffDataVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long staffId;
	private String staffCode;
	private String staffName;

	// ma hang hoa
	private String productCode;
	// ma hang hoa
	private String productName;
	// gia hang hoa
	private BigDecimal price;
	// so luong xuat
	private Long exportQuantity;
	// tong tien xuat
	private BigDecimal exportAmount;
	// so luong ban
	private Long sellQuantity;
	// tong tien ban
	private BigDecimal sellAmount;
	// so luong ban
	private Long promoteQuantity;
	// tong tien ban
	private BigDecimal promoteAmount;
	// so luong ton
	private Long stockQuantity;
	// tong tien ton
	private BigDecimal stockAmount;
	
	private String groupString;
	
	private BigDecimal totalExpAmount;
	
	private BigDecimal totalSellAmount;
	
	private BigDecimal totalPromoteAmount;
	
	private BigDecimal totalStockAmount;
	
	public RptExImStockOfStaffDataVO() {
	}

	@SuppressWarnings("unchecked")
	public static List<RptExImStockOfStaffLv01VO> groupByCusAndCat(
			List<RptExImStockOfStaffDataVO> listData) throws Exception {
		List<RptExImStockOfStaffLv01VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, "staffId");

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptExImStockOfStaffLv01VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptExImStockOfStaffLv01VO vo = new RptExImStockOfStaffLv01VO();

					List<RptExImStockOfStaffDataVO> listObject = (List<RptExImStockOfStaffDataVO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setStaffCode(listObject.get(0).getStaffCode());
						vo.setStaffId(listObject.get(0).getStaffId());
						vo.setStaffName(listObject.get(0).getStaffName());
						vo.setListData(listObject);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getExportQuantity() {
		return exportQuantity;
	}

	public void setExportQuantity(Long exportQuantity) {
		this.exportQuantity = exportQuantity;
	}

	public BigDecimal getExportAmount() {
		return exportAmount;
	}

	public void setExportAmount(BigDecimal exportAmount) {
		this.exportAmount = exportAmount;
	}

	public Long getSellQuantity() {
		return sellQuantity;
	}

	public void setSellQuantity(Long sellQuantity) {
		this.sellQuantity = sellQuantity;
	}

	public BigDecimal getSellAmount() {
		return sellAmount;
	}

	public void setsellAmount(BigDecimal sellAmount) {
		this.sellAmount = sellAmount;
	}

	public Long getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(Long stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public BigDecimal getStockAmount() {
		return stockAmount;
	}

	public void setStockAmount(BigDecimal stockAmount) {
		this.stockAmount = stockAmount;
	}

	public Long getPromoteQuantity() {
		return promoteQuantity;
	}

	public void setPromoteQuantity(Long promoteQuantity) {
		this.promoteQuantity = promoteQuantity;
	}

	public BigDecimal getPromoteAmount() {
		return promoteAmount;
	}

	public void setPromoteAmount(BigDecimal promoteAmount) {
		this.promoteAmount = promoteAmount;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getTotalExpAmount() {
		return totalExpAmount;
	}

	public void setTotalExpAmount(BigDecimal totalExpAmount) {
		this.totalExpAmount = totalExpAmount;
	}

	public BigDecimal getTotalSellAmount() {
		return totalSellAmount;
	}

	public void setTotalSellAmount(BigDecimal totalSellAmount) {
		this.totalSellAmount = totalSellAmount;
	}

	public BigDecimal getTotalPromoteAmount() {
		return totalPromoteAmount;
	}

	public void setTotalPromoteAmount(BigDecimal totalPromoteAmount) {
		this.totalPromoteAmount = totalPromoteAmount;
	}

	public BigDecimal getTotalStockAmount() {
		return totalStockAmount;
	}

	public void setTotalStockAmount(BigDecimal totalStockAmount) {
		this.totalStockAmount = totalStockAmount;
	}

	public String getGroupString() {
		return groupString;
	}

	public void setGroupString(String groupString) {
		this.groupString = groupString;
	}
	
}
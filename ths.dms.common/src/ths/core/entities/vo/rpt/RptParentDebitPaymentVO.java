package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

public class RptParentDebitPaymentVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String groupCode;
	private String groupName;
	private ArrayList<RptDebitPaymentVO> list = new ArrayList<RptDebitPaymentVO>();
	
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public ArrayList<RptDebitPaymentVO> getList() {
		return list;
	}
	public void setList(ArrayList<RptDebitPaymentVO> list) {
		this.list = list;
	}
	
}

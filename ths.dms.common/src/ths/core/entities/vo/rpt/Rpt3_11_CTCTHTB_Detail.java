package ths.core.entities.vo.rpt;

import java.math.BigDecimal;


public class Rpt3_11_CTCTHTB_Detail implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 710560671268651768L;
	private String programCode; //ma CTTB
	private String programName; //ten CTTB
	private String fromDate;	//tu ngay
	private String toDate;		//den ngay
	private String invoiceNumber;	//so hoa don
	private String customerCode;	//ma khach hang
	private String productName;		//ten hang
	private BigDecimal quantity;	//tong so luong
	private BigDecimal price;		//don gia
	private BigDecimal amount;	//tong thanh tien
	public String getProgramCode() {
		return programCode;
	}
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
}

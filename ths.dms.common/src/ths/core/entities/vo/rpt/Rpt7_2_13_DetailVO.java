package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class Rpt7_2_13_DetailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3877270696091279101L;
	private String mien;
	private String tenMien;
	private String vung;
	private String npp;
	private String tenNPP;
	private String gsnpp;
	private String tenGSNPP;
	private String maNVBH;
	private String tenNVBH;
	private String cat;
	private BigDecimal dsKeHoach;
	private BigDecimal dsMTB;
	private BigDecimal dsThucTe;
	private BigDecimal tyLeMTB;
	private BigDecimal tyLe;
	private Integer groupNo;
	
	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getTenMien() {
		return tenMien;
	}

	public void setTenMien(String tenMien) {
		this.tenMien = tenMien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getTenNPP() {
		return tenNPP;
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public BigDecimal getDsKeHoach() {
		return dsKeHoach;
	}

	public void setDsKeHoach(BigDecimal dsKeHoach) {
		this.dsKeHoach = dsKeHoach;
	}

	public BigDecimal getDsMTB() {
		return dsMTB;
	}

	public void setDsMTB(BigDecimal dsMTB) {
		this.dsMTB = dsMTB;
	}

	public BigDecimal getDsThucTe() {
		return dsThucTe;
	}

	public void setDsThucTe(BigDecimal dsThucTe) {
		this.dsThucTe = dsThucTe;
	}

	public BigDecimal getTyLeMTB() {
		return tyLeMTB;
	}

	public void setTyLeMTB(BigDecimal tyLeMTB) {
		this.tyLeMTB = tyLeMTB;
	}

	public BigDecimal getTyLe() {
		return tyLe;
	}

	public void setTyLe(BigDecimal tyLe) {
		this.tyLe = tyLe;
	}

	public String getGsnpp() {
		return gsnpp;
	}

	public void setGsnpp(String gsnpp) {
		this.gsnpp = gsnpp;
	}

	public String getMaNVBH() {
		return maNVBH;
	}

	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}

	public String getTenNVBH() {
		return tenNVBH;
	}

	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}

	public String getNpp() {
		return npp;
	}

	public void setNpp(String npp) {
		this.npp = npp;
	}

	public String getTenGSNPP() {
		return tenGSNPP;
	}

	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}

	public Integer getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(Integer groupNo) {
		this.groupNo = groupNo;
	}
}

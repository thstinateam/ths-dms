package ths.core.entities.vo.rpt;

import java.io.Serializable;
/**
 * @author tungmt
 * @since March 08, 2014
 * @description Class Map Report Bao cao don dat hang f2
 */
public class RptDDHF2VO implements Serializable{
	//BAO CAO DAONH SO TIEU THU CUA TUNG DIEM LE
	private static final long serialVersionUID = 1L;
	
	private Integer orderRow;
	private Long productId;
	private Integer orderCol;
	private String productCode;
	private Integer convfact;
	private Double price;
	private Long catId;
	private Double orderAmount;
	private String orderQuantity;
	private Double shopAmount;
	private String shopQuantity;
	
	public Integer getOrderRow() {
		return orderRow;
	}
	public void setOrderRow(Integer orderRow) {
		this.orderRow = orderRow;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Integer getOrderCol() {
		return orderCol;
	}
	public void setOrderCol(Integer orderCol) {
		this.orderCol = orderCol;
	}
	public String getProductCode() {
		if(orderRow == 0 && productId == 0 && orderCol == 1){
			return "Tổng cộng nhóm " + productCode;
		} else if (this.orderRow == 2 && this.productId == 0 && this.orderCol == 2) {
			return "Tổng cộng kho ";
		} else {
			return productCode;
		}
		//return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Long getCatId() {
		return catId;
	}
	public void setCatId(Long catId) {
		this.catId = catId;
	}
	public Double getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(Double orderAmount) {
		this.orderAmount = orderAmount;
	}
	public Double getShopAmount() {
		return shopAmount;
	}
	public void setShopAmount(Double shopAmount) {
		this.shopAmount = shopAmount;
	}
	public String getShopQuantity() {
		return shopQuantity;
	}
	public void setShopQuantity(String shopQuantity) {
		this.shopQuantity = shopQuantity;
	}
	public String getOrderQuantity() {
		return orderQuantity;
	}
	public void setOrderQuantity(String orderQuantity) {
		this.orderQuantity = orderQuantity;
	}
}

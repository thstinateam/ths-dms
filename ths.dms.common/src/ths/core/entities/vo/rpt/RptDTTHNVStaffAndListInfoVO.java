package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptDTTHNVStaffAndListInfoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String staffCode;
	private String staffName;
	private BigDecimal moneyForStaff; // tien mat
	private BigDecimal remainForStaff; // tien no
	private BigDecimal discountForStaff; // chiet khau
	private BigDecimal promotionMoneyForStaff; // khuyen mai tien
	private BigDecimal promotionStockForStaff; // khuyen mai hang
	private BigDecimal revenueForStaff; // doanh thu
	private BigDecimal salesForStaff; // doanh so
	ArrayList<RptDTTHNVRecordVO> listDetail = new ArrayList<RptDTTHNVRecordVO>();

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
	public BigDecimal getMoneyForStaff() {
		return moneyForStaff;
	}

	public void setMoneyForStaff(BigDecimal moneyForStaff) {
		this.moneyForStaff = moneyForStaff;
	}

	public List<RptDTTHNVRecordVO> getListDetail() {
		return listDetail;
	}

	public void setListDetail(ArrayList<RptDTTHNVRecordVO> listDetail) {
		this.listDetail = listDetail;
	}

	public BigDecimal getRemainForStaff() {
		return remainForStaff;
	}

	public void setRemainForStaff(BigDecimal remainForStaff) {
		this.remainForStaff = remainForStaff;
	}

	public BigDecimal getDiscountForStaff() {
		return discountForStaff;
	}

	public void setDiscountForStaff(BigDecimal discountForStaff) {
		this.discountForStaff = discountForStaff;
	}

	public BigDecimal getPromotionMoneyForStaff() {
		return promotionMoneyForStaff;
	}

	public void setPromotionMoneyForStaff(BigDecimal promotionMoneyForStaff) {
		this.promotionMoneyForStaff = promotionMoneyForStaff;
	}

	public BigDecimal getPromotionStockForStaff() {
		return promotionStockForStaff;
	}

	public void setPromotionStockForStaff(BigDecimal promotionStockForStaff) {
		this.promotionStockForStaff = promotionStockForStaff;
	}

	public BigDecimal getRevenueForStaff() {
		return revenueForStaff;
	}

	public void setRevenueForStaff(BigDecimal revenueForStaff) {
		this.revenueForStaff = revenueForStaff;
	}

	public BigDecimal getSalesForStaff() {
		return salesForStaff;
	}

	public void setSalesForStaff(BigDecimal salesForStaff) {
		this.salesForStaff = salesForStaff;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class Rpt10_1_2Detail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8921340805150425581L;
	private String nhomDuocGiao;
	private String nhomThucHien;
	private BigDecimal skuDuocGiao;
	private BigDecimal chiTieuDoanhSo; 
	private BigDecimal soKHPSDS; //so kh khong trung lap phat sinh doanh so
	private BigDecimal doanhSoThucDat;
	private BigDecimal doanhSoThucDatLuyKe; 
	private BigDecimal soSKUThucHien;
	private String tyLeDatNhom;
	public String getNhomDuocGiao() {
		return nhomDuocGiao;
	}
	public void setNhomDuocGiao(String nhomDuocGiao) {
		this.nhomDuocGiao = nhomDuocGiao;
	}
	public String getNhomThucHien() {
		return nhomThucHien;
	}
	public void setNhomThucHien(String nhomThucHien) {
		this.nhomThucHien = nhomThucHien;
	}
	public BigDecimal getSkuDuocGiao() {
		return skuDuocGiao;
	}
	public void setSkuDuocGiao(BigDecimal skuDuocGiao) {
		this.skuDuocGiao = skuDuocGiao;
	}
	public BigDecimal getChiTieuDoanhSo() {
		return chiTieuDoanhSo;
	}
	public void setChiTieuDoanhSo(BigDecimal chiTieuDoanhSo) {
		this.chiTieuDoanhSo = chiTieuDoanhSo;
	}
	public BigDecimal getSoKHPSDS() {
		return soKHPSDS;
	}
	public void setSoKHPSDS(BigDecimal soKHPSDS) {
		this.soKHPSDS = soKHPSDS;
	}
	public BigDecimal getDoanhSoThucDat() {
		return doanhSoThucDat;
	}
	public void setDoanhSoThucDat(BigDecimal doanhSoThucDat) {
		this.doanhSoThucDat = doanhSoThucDat;
	}
	public BigDecimal getDoanhSoThucDatLuyKe() {
		return doanhSoThucDatLuyKe;
	}
	public void setDoanhSoThucDatLuyKe(BigDecimal doanhSoThucDatLuyKe) {
		this.doanhSoThucDatLuyKe = doanhSoThucDatLuyKe;
	}
	public BigDecimal getSoSKUThucHien() {
		return soSKUThucHien;
	}
	public void setSoSKUThucHien(BigDecimal soSKUThucHien) {
		this.soSKUThucHien = soSKUThucHien;
	}
	public String getTyLeDatNhom() {
		return tyLeDatNhom;
	}
	public void setTyLeDatNhom(String tyLeDatNhom) {
		this.tyLeDatNhom = tyLeDatNhom;
	}
	
	
}

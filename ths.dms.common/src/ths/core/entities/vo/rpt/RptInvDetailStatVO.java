package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;


public class RptInvDetailStatVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<RptInvDetailStatDetailVO> listDetail = new ArrayList<RptInvDetailStatDetailVO>();
	private String invoiceNumber;
	private BigDecimal totalSku;
	private BigDecimal totalAmount;
	private BigDecimal totalFinalAmount;
	private BigDecimal totalDiscount;
	private BigDecimal totalTaxAmount;

	public BigDecimal getTotalSku() {
		return totalSku;
	}

	public void setTotalSku(BigDecimal totalSku) {
		this.totalSku = totalSku;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalFinalAmount() {
		return totalFinalAmount;
	}

	public void setTotalFinalAmount(BigDecimal totalFinalAmount) {
		this.totalFinalAmount = totalFinalAmount;
	}

	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public ArrayList<RptInvDetailStatDetailVO> getListDetail() {
		return listDetail;
	}

	public void setListDetail(ArrayList<RptInvDetailStatDetailVO> listDetail) {
		this.listDetail = listDetail;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public BigDecimal getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(BigDecimal totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

}

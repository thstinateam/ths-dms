package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCBAOMAT1_5VO implements Serializable {
	/**
	 * VO bao cao thiet bi; bao mat
	 * @author vuongmq
	 * @since May 18, 2014
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mien;
	private String vung;
	private String code; // ma bien ban bao mat;
	private String shopCode;
	private String shopName;
	private String numberCode;
	private String createFormDate;
	private String customerCode;
	private String customerName;
	private String period; // ten ky
	private String categoryName; // loai tu
	private String groupName; // nhom tu
	private String brandName; // hieu
	private String capacity; //dung tich
	private String manufacturingYear; //nam san xuat
	private String equipCode; // ma tai san
	private String serialNumber; // ma tai san
	
	private BigDecimal price; // nguyen gia
	private BigDecimal priceActually; // gia tri con lai
	
	private String lostDateStr; // ngay mat
	private String lastSaleDateStr; // ngay het phat sinh doanh so cuoi
	
	private Integer gsAndNPPsearch; // GS và NPP truy tim ; TRACING_PLACE
	private Integer resultSearch; // ket qua truy tim; TRACING_RESULT
	
	private String gsAndNPPsearchStr; // GS và NPP truy tim ; TRACING_PLACE
	private String resultSearchStr; // ket qua truy tim; TRACING_RESULT
	
	private String conclusion; // ket luan;
	private String recommendedTreatment; // RECOMMENDED_TREATMENT
	
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getNumberCode() {
		return numberCode;
	}
	public void setNumberCode(String numberCode) {
		this.numberCode = numberCode;
	}
	public String getCreateFormDate() {
		return createFormDate;
	}
	public void setCreateFormDate(String createFormDate) {
		this.createFormDate = createFormDate;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getManufacturingYear() {
		return manufacturingYear;
	}
	public void setManufacturingYear(String manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}
	public String getEquipCode() {
		return equipCode;
	}
	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getPriceActually() {
		return priceActually;
	}
	public void setPriceActually(BigDecimal priceActually) {
		this.priceActually = priceActually;
	}
	public String getLostDateStr() {
		return lostDateStr;
	}
	public void setLostDateStr(String lostDateStr) {
		this.lostDateStr = lostDateStr;
	}
	public String getLastSaleDateStr() {
		return lastSaleDateStr;
	}
	public void setLastSaleDateStr(String lastSaleDateStr) {
		this.lastSaleDateStr = lastSaleDateStr;
	}
	public Integer getGsAndNPPsearch() {
		return gsAndNPPsearch;
	}
	public void setGsAndNPPsearch(Integer gsAndNPPsearch) {
		this.gsAndNPPsearch = gsAndNPPsearch;
	}
	public Integer getResultSearch() {
		return resultSearch;
	}
	public void setResultSearch(Integer resultSearch) {
		this.resultSearch = resultSearch;
	}
	public String getGsAndNPPsearchStr() {
		if (gsAndNPPsearch != null && gsAndNPPsearch == 0) {
			gsAndNPPsearchStr = "Tại địa chỉ kinh doanh";
		} else if (gsAndNPPsearch != null && gsAndNPPsearch == 1) {
			gsAndNPPsearchStr = "Tại địa chỉ thường trú của Khách hàng";
		}
		return gsAndNPPsearchStr;
	}
	public void setGsAndNPPsearchStr(String gsAndNPPsearchStr) {
		this.gsAndNPPsearchStr = gsAndNPPsearchStr;
	}
	public String getResultSearchStr() {
		if (resultSearch != null && resultSearch == 0) {
			resultSearchStr = "Tìm được khách hàng nhưng không thấy tủ";
		} else if (resultSearch != null && resultSearch == 1) {
			resultSearchStr = "Không tìm thấy khách hàng";
		}
		return resultSearchStr;
	}
	public void setResultSearchStr(String resultSearchStr) {
		this.resultSearchStr = resultSearchStr;
	}
	public String getConclusion() {
		return conclusion;
	}
	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}
	public String getRecommendedTreatment() {
		return recommendedTreatment;
	}
	public void setRecommendedTreatment(String recommendedTreatment) {
		this.recommendedTreatment = recommendedTreatment;
	}


	
}

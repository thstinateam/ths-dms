package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RptBKCTHHMVVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String shopName;
	private String shopAddress;
	private BigDecimal totalPO = BigDecimal.ZERO;
	private BigDecimal totalDiscount = BigDecimal.ZERO;
	private BigDecimal totalVAT = BigDecimal.ZERO;
	private BigDecimal totalAmount = BigDecimal.ZERO;
	private List<RptBKCTHHMV_RecordVO> lstRecord;
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public BigDecimal getTotalPO() {
		return totalPO;
	}
	public void setTotalPO(BigDecimal totalPO) {
		this.totalPO = totalPO;
	}
	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public BigDecimal getTotalVAT() {
		return totalVAT;
	}
	public void setTotalVAT(BigDecimal totalVAT) {
		this.totalVAT = totalVAT;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public List<RptBKCTHHMV_RecordVO> getLstRecord() {
		return lstRecord;
	}
	public void setLstRecord(List<RptBKCTHHMV_RecordVO> lstRecord) {
		this.lstRecord = lstRecord;
	}
	
}

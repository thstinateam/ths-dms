package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptReturnSaleOrderAllowDeliveryProductVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String productCode;
	private String productName;
	private BigDecimal amount;
	private BigDecimal promotionAmount;
	private BigDecimal price;
	private List<RptReturnSaleOrderAllowDeliveryVO> listVO = new ArrayList<RptReturnSaleOrderAllowDeliveryVO>();
	
	public void safeSetNull() throws IllegalArgumentException, IllegalAccessException{
		for(Field field:getClass().getDeclaredFields()) {
			if(field.getType().equals(Integer.class) && field.get(this) == null) {
				field.set(this, 0);
			}
			if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
				field.set(this, BigDecimal.ZERO);
			}
			if(field.getType().equals(String.class) && field.get(this) == null) {
				field.set(this, "");
			}
		}
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getPromotionAmount() {
		return promotionAmount;
	}
	public void setPromotionAmount(BigDecimal promotionAmount) {
		this.promotionAmount = promotionAmount;
	}
	public List<RptReturnSaleOrderAllowDeliveryVO> getListVO() {
		return listVO;
	}
	public void setListVO(List<RptReturnSaleOrderAllowDeliveryVO> listVO) {
		this.listVO = listVO;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

public class RptPoCfrmFromDvkh1VO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String saleOrderNumber;
	private ArrayList<RptPoCfrmFromDvkh2VO> lstRptPoCfrmFromDvkh2VO = new ArrayList<RptPoCfrmFromDvkh2VO>();

	public String getSaleOrderNumber() {
		return saleOrderNumber;
	}

	public void setSaleOrderNumber(String saleOrderNumber) {
		this.saleOrderNumber = saleOrderNumber;
	}

	public ArrayList<RptPoCfrmFromDvkh2VO> getLstRptPoCfrmFromDvkh2VO() {
		return lstRptPoCfrmFromDvkh2VO;
	}

	public void setLstRptPoCfrmFromDvkh2VO(
			ArrayList<RptPoCfrmFromDvkh2VO> lstRptPoCfrmFromDvkh2VO) {
		this.lstRptPoCfrmFromDvkh2VO = lstRptPoCfrmFromDvkh2VO;
	}
}

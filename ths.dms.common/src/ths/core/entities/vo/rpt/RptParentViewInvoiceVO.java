package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class RptParentViewInvoiceVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Date orderDate;
	private String cpnyName;
	private String cpnyAddr;
	private String cpnyBankAccount;
	private String deliveryName;
	private String selName;
	private String orderNumber;
	private String custName;
	private String custTaxNumber;
	private String customerCode;
	private String customerAddr;
	private Integer custPayment;
	private String custDelAddr;
	private String custBankAccount;
	private String custBankName;
	private Float vat;
	private BigDecimal amount;
	private BigDecimal taxAmount;
	private BigDecimal discount;
	private String staffCode;
	private String staffName;
	private String invoiceNumber;
	
	// so tien viet bang chu
	private String totalAmountString;
	// Tong cong tien thanh toan
	private BigDecimal totalMoney;
	private String taxAmountString;
	private String amountString;
	private String totalMoneyString;	
	private String paymentType;

	private ArrayList<RptViewInvoiceVO> listProducts = new ArrayList<RptViewInvoiceVO>();
	
	public String getCpnyName() {
		return cpnyName==null?"":cpnyName;
	}
	public void setCpnyName(String cpnyName) {
		this.cpnyName = cpnyName;
	}
	public String getCpnyAddr() {
		return cpnyAddr==null?"":cpnyAddr;
	}
	public void setCpnyAddr(String cpnyAddr) {
		this.cpnyAddr = cpnyAddr;
	}
	public String getCpnyBankAccount() {
		return cpnyBankAccount==null?"":cpnyBankAccount;
	}
	public void setCpnyBankAccount(String cpnyBankAccount) {
		this.cpnyBankAccount = cpnyBankAccount;
	}
	public String getDeliveryName() {
		return deliveryName==null?"":deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public String getSelName() {
		return selName==null?"":selName;
	}
	public void setSelName(String selName) {
		this.selName = selName;
	}
	public String getOrderNumber() {
		return orderNumber==null?"":orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustTaxNumber() {
		return custTaxNumber==null?"":custTaxNumber;
	}
	public void setCustTaxNumber(String custTaxNumber) {
		this.custTaxNumber = custTaxNumber;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerAddr() {
		return customerAddr==null?"":customerAddr;
	}
	public void setCustomerAddr(String customerAddr) {
		this.customerAddr = customerAddr;
	}
	public Integer getCustPayment() {
		return custPayment;
	}
	public void setCustPayment(Integer custPayment) {
		this.custPayment = custPayment;
	}
	public String getCustDelAddr() {
		return custDelAddr==null?"":custDelAddr;
	}
	public void setCustDelAddr(String custDelAddr) {
		this.custDelAddr = custDelAddr;
	}
	public String getCustBankAccount() {
		return custBankAccount==null?"":custBankAccount;
	}
	public void setCustBankAccount(String custBankAccount) {
		this.custBankAccount = custBankAccount;
	}
	public String getCustBankName() {
		return custBankName==null?"":custBankName;
	}
	public void setCustBankName(String custBankName) {
		this.custBankName = custBankName;
	}
	public Float getVat() {
		return vat==null?0.0f:vat;
	}
	public void setVat(Float vat) {
		this.vat = vat;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getTaxAmount() {
		return taxAmount==null?BigDecimal.ZERO:taxAmount;
	}
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	/**
	 * @return the orderDate
	 */
	public Date getOrderDate() {
		return orderDate;
	}
	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}	
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	/**
	 * @param invoiceNumber the invoiceNumber to set
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	public String getOrderDay(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(orderDate);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		if(day <10){
			return "0" + day;
		}
		return String.valueOf(day);
	}
	public String getOrderMonth(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(orderDate);
		int month = cal.get(Calendar.MONTH) + 1;		
		if(month <10){
			return "0" + month;
		}
		return String.valueOf(month);
	}
	public String getOrderYear(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(orderDate);
		int year = cal.get(Calendar.YEAR);
		return String.valueOf(year);
	}
	
	public String getTotalAmountString() {
		return totalAmountString==null?"":totalAmountString;
	}
	public void setTotalAmountString(String totalAmountString) {
		this.totalAmountString = totalAmountString;
	}	
	public BigDecimal getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}
	public ArrayList<RptViewInvoiceVO> getListProducts() {
		return listProducts;
	}
	public void setListProducts(ArrayList<RptViewInvoiceVO> listProducts) {
		this.listProducts = listProducts;
	}
	public String getTaxAmountString() {
		return taxAmountString==null?"":taxAmountString;
	}
	public void setTaxAmountString(String taxAmountString) {
		this.taxAmountString = taxAmountString;
	}
	public String getAmountString() {
		return amountString==null?"":amountString;
	}
	public void setAmountString(String amountString) {
		this.amountString = amountString;
	}
	public String getTotalMoneyString() {
		return totalMoneyString==null?"":totalMoneyString;
	}
	public void setTotalMoneyString(String totalMoneyString) {
		this.totalMoneyString = totalMoneyString;
	}
	public String getPaymentType() {
		return paymentType==null?"":paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @return the discount
	 */
	public BigDecimal getDiscount() {
		return discount==null?BigDecimal.ZERO:discount;
	}
	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
}

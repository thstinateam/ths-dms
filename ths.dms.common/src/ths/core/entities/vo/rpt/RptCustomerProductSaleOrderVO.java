package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

import ths.dms.core.entities.Shop;

public class RptCustomerProductSaleOrderVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Shop shop;
	private ArrayList<RptCustomerProductSaleOrder1VO> lstRptCustomerProductSaleOrder1VO = new ArrayList<RptCustomerProductSaleOrder1VO>();
	/**
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}
	/**
	 * @param shop the shop to set
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	/**
	 * @return the lstRptCustomerStock1VO
	 */
	public ArrayList<RptCustomerProductSaleOrder1VO> getRptCustomerProductSaleOrder1VO() {
		return lstRptCustomerProductSaleOrder1VO;
	}
	/**
	 * @param lstRptCustomerProductSaleOrder1VO the lstRptCustomerStock1VO to set
	 */
	public void setRptCustomerProductSaleOrder1VO(ArrayList<RptCustomerProductSaleOrder1VO> lstRptCustomerProductSaleOrder1VO) {
		this.lstRptCustomerProductSaleOrder1VO = lstRptCustomerProductSaleOrder1VO;
	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

import ths.dms.core.entities.Shop;

public class RptCycleCountDiffVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Shop shop;

	private ArrayList<RptCycleCountDiff1VO> lstRptCycleCountDiff1VO = new ArrayList<RptCycleCountDiff1VO>();

	public ArrayList<RptCycleCountDiff1VO> getLstRptCycleCountDiff1VO() {
		return lstRptCycleCountDiff1VO;
	}

	public void setLstRptCycleCountDiff1VO(
			ArrayList<RptCycleCountDiff1VO> lstRptCycleCountDiff1VO) {
		this.lstRptCycleCountDiff1VO = lstRptCycleCountDiff1VO;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
/**
 * @author tungmt
 * @since March 08, 2014
 * @description Class Map Report Bao cao don dat hang f2
 */
public class RptCTKMTCK739VO implements Serializable{
	//BAO CAO DAONH SO TIEU THU CUA TUNG DIEM LE
	private static final long serialVersionUID = 1L;
	
	private Integer orderRow;
	private String orderDate;
	private String orderNumber;
	private String fromOrderNumber;
	private String staffCode;
	private String staffName;
	private String customerCode;
	private String customerName;
	private String saleProductCode;
	private Double saleQuantity;
	private String promoProductCode;
	private Double promoQuantity;
	private Double discountAmount;
	private String customerType;
	private String promotionCode;
	private String promotionName;
	public Integer getOrderRow() {
		return orderRow;
	}
	public void setOrderRow(Integer orderRow) {
		this.orderRow = orderRow;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getFromOrderNumber() {
		return fromOrderNumber;
	}
	public void setFromOrderNumber(String fromOrderNumber) {
		this.fromOrderNumber = fromOrderNumber;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getSaleProductCode() {
		return saleProductCode;
	}
	public void setSaleProductCode(String saleProductCode) {
		this.saleProductCode = saleProductCode;
	}
	public Double getSaleQuantity() {
		return saleQuantity;
	}
	public void setSaleQuantity(Double saleQuantity) {
		this.saleQuantity = saleQuantity;
	}
	public String getPromoProductCode() {
		return promoProductCode;
	}
	public void setPromoProductCode(String promoProductCode) {
		this.promoProductCode = promoProductCode;
	}
	public Double getPromoQuantity() {
		return promoQuantity;
	}
	public void setPromoQuantity(Double promoQuantity) {
		this.promoQuantity = promoQuantity;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getPromotionCode() {
		return promotionCode;
	}
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	public String getPromotionName() {
		return promotionName;
	}
	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RptXBTNV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String staffCode;
	public String staffName;
	public BigDecimal sumMoneyExport;
	public BigDecimal sumMoneySale;
	public BigDecimal sumMoneyPromotion;
	public BigDecimal sumMoneyStock;

	public List<RptXBTNVRecord> listDetail;

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public BigDecimal getSumMoneyExport() {
		return sumMoneyExport;
	}

	public void setSumMoneyExport(BigDecimal sumMoneyExport) {
		this.sumMoneyExport = sumMoneyExport;
	}

	public BigDecimal getSumMoneySale() {
		return sumMoneySale;
	}

	public void setSumMoneySale(BigDecimal sumMoneySale) {
		this.sumMoneySale = sumMoneySale;
	}

	public BigDecimal getSumMoneyPromotion() {
		return sumMoneyPromotion;
	}

	public void setSumMoneyPromotion(BigDecimal sumMoneyPromotion) {
		this.sumMoneyPromotion = sumMoneyPromotion;
	}

	public BigDecimal getSumMoneyStock() {
		return sumMoneyStock;
	}

	public void setSumMoneyStock(BigDecimal sumMoneyStock) {
		this.sumMoneyStock = sumMoneyStock;
	}

	public List<RptXBTNVRecord> getListDetail() {
		return listDetail;
	}

	public void setListDetail(List<RptXBTNVRecord> listDetail) {
		this.listDetail = listDetail;
	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * @author hunglm16
 * @since January 25, 2014
 * @description report Dt3: Bao cao danh sach don hang trong ngay theo NVBH
 * */
public class RptBCDSDHTNTNVBH_DS3_1_VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long maNVBH;
	private Integer chietKhau;
	private Integer sku;
	private BigDecimal tongDT;
	private BigDecimal tongDS;
	private BigDecimal kmTien;
	private BigDecimal kmHang;
	private String donHang;
    private String nvbh;
    private String khachHang;
    private String hinhThuc;
    private String soHoaDon;
//    private Date ngayBan;
    
    private String ngayBan;
    
	public String getNgayBan() {
		return ngayBan;
	}
	public void setNgayBan(String ngayBan) {
		this.ngayBan = ngayBan;
	}
	public Long getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(Long maNVBH) {
		this.maNVBH = maNVBH;
	}
	public Integer getChietKhau() {
		return chietKhau;
	}
	public void setChietKhau(Integer chietKhau) {
		this.chietKhau = chietKhau;
	}
	public Integer getSku() {
		return sku;
	}
	public void setSku(Integer sku) {
		this.sku = sku;
	}
	public BigDecimal getTongDT() {
		return tongDT;
	}
	public void setTongDT(BigDecimal tongDT) {
		this.tongDT = tongDT;
	}
	public BigDecimal getTongDS() {
		return tongDS;
	}
	public void setTongDS(BigDecimal tongDS) {
		this.tongDS = tongDS;
	}
	public BigDecimal getKmTien() {
		return kmTien;
	}
	public void setKmTien(BigDecimal kmTien) {
		this.kmTien = kmTien;
	}
	public BigDecimal getKmHang() {
		return kmHang;
	}
	public void setKmHang(BigDecimal kmHang) {
		this.kmHang = kmHang;
	}
	public String getDonHang() {
		return donHang;
	}
	public void setDonHang(String donHang) {
		this.donHang = donHang;
	}
	public String getNvbh() {
		return nvbh;
	}
	public void setNvbh(String nvbh) {
		this.nvbh = nvbh;
	}
	
	public String getKhachHang() {
		return khachHang;
	}
	public void setKhachHang(String khachHang) {
		this.khachHang = khachHang;
	}
	public String getHinhThuc() {
		return hinhThuc;
	}
	public void setHinhThuc(String hinhThuc) {
		this.hinhThuc = hinhThuc;
	}
	public String getSoHoaDon() {
		return soHoaDon;
	}
	public void setSoHoaDon(String soHoaDon) {
		this.soHoaDon = soHoaDon;
	}

}

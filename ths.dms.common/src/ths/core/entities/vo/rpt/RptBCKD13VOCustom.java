package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RptBCKD13VOCustom implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer stt;
	private String maMien;
    private String maVung;
    private String maNPP;
    private String tenNPP;
    private String maKH;
    private String tenKH;
    private String soNha;
    private String duong;
    private String phuongXa;
    private String quanHuyen;
    private String tinhThanh;
    private Integer slTu;
    private Integer slDiemBan;
    
//    private BigDecimal kpsds3ThangTruoc;
//    private Date rptMonth;
//    private BigDecimal amount;
    
    private List<ColumnSaleMoneyAndBooleanVO> lstColumnSaleMoneyAndBoolean;
    private String columnName;
	private BigDecimal columnValue;
    
	public Integer getStt() {
		return stt;
	}
	public void setStt(Integer stt) {
		this.stt = stt;
	}
	public List<ColumnSaleMoneyAndBooleanVO> getLstColumnSaleMoneyAndBoolean() {
		return lstColumnSaleMoneyAndBoolean;
	}
	public void setLstColumnSaleMoneyAndBoolean(
			List<ColumnSaleMoneyAndBooleanVO> lstColumnSaleMoneyAndBoolean) {
		this.lstColumnSaleMoneyAndBoolean = lstColumnSaleMoneyAndBoolean;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
	public BigDecimal getColumnValue() {
		return columnValue;
	}
	public void setColumnValue(BigDecimal columnValue) {
		this.columnValue = columnValue;
	}
	public void setSlTu(Integer slTu) {
		this.slTu = slTu;
	}
	public void setSlDiemBan(Integer slDiemBan) {
		this.slDiemBan = slDiemBan;
	}
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getSoNha() {
		return soNha;
	}
	public void setSoNha(String soNha) {
		this.soNha = soNha;
	}
	public String getDuong() {
		return duong;
	}
	public void setDuong(String duong) {
		this.duong = duong;
	}
	public String getPhuongXa() {
		return phuongXa;
	}
	public void setPhuongXa(String phuongXa) {
		this.phuongXa = phuongXa;
	}
	public String getQuanHuyen() {
		return quanHuyen;
	}
	public void setQuanHuyen(String quanHuyen) {
		this.quanHuyen = quanHuyen;
	}
	public String getTinhThanh() {
		return tinhThanh;
	}
	public void setTinhThanh(String tinhThanh) {
		this.tinhThanh = tinhThanh;
	}
	public Integer getSlTu() {
		return slTu;
	}
	public void setSlTu(BigDecimal slTu) {
		if (slTu != null)
			this.slTu = slTu.intValue();
	}
	public Integer getSlDiemBan() {
		return slDiemBan;
	}
	public void setSlDiemBan(BigDecimal slDiemBan) {
		if (slDiemBan != null)
		this.slDiemBan = slDiemBan.intValue();
	}
    
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptSaleOrderInDateVO implements Serializable{
	private static final long serialVersionUID = 1L;

	private String stt;
	
	private Integer flag;
	
	private String customerInfo;
	
	private String orderNumber;
	
	private BigDecimal tongDTEnd;
	
	private BigDecimal kmTienEnd;
	
	public BigDecimal getTongDTEnd() {
		return tongDTEnd;
	}

	public void setTongDTEnd(BigDecimal tongDTEnd) {
		this.tongDTEnd = tongDTEnd;
	}

	public BigDecimal getKmTienEnd() {
		return kmTienEnd;
	}

	public void setKmTienEnd(BigDecimal kmTienEnd) {
		this.kmTienEnd = kmTienEnd;
	}

	public BigDecimal getKmHangEnd() {
		return kmHangEnd;
	}

	public void setKmHangEnd(BigDecimal kmHangEnd) {
		this.kmHangEnd = kmHangEnd;
	}

	public Integer getSkuEnd() {
		return skuEnd;
	}

	public void setSkuEnd(Integer skuEnd) {
		this.skuEnd = skuEnd;
	}

	public BigDecimal getTongDSEnd() {
		return tongDSEnd;
	}

	public void setTongDSEnd(BigDecimal tongDSEnd) {
		this.tongDSEnd = tongDSEnd;
	}

	private BigDecimal kmHangEnd;
	
	private Integer skuEnd;
	
	private BigDecimal tongDSEnd;
	
	private String invoiceNumber;

	private Integer orderSource;
	
	private String orderSourceTemp;
	
	public String getOrderSourceTemp() {
		return orderSourceTemp;
	}

	public void setOrderSourceTemp(String orderSourceTemp) {
		this.orderSourceTemp = orderSourceTemp;
	}

	private BigDecimal total;
	
	private BigDecimal discount;
	
	private BigDecimal amount;
	
	private BigDecimal totalPromotion;
	
	private Integer numSku;
	
	private Date orderDate;
	
	private String staffInfo;
	
	private BigDecimal kmHang;
	
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getStaffInfo() {
		return staffInfo;
	}

	public void setStaffInfo(String staffInfo) {
		this.staffInfo = staffInfo;
	}

	public String getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(String customerInfo) {
		this.customerInfo = customerInfo;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Integer getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(Integer orderSource) {
		this.orderSource = orderSource;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getTotalPromotion() {
		return totalPromotion;
	}

	public void setTotalPromotion(BigDecimal totalPromotion) {
		this.totalPromotion = totalPromotion;
	}

	public Integer getNumSku() {
		return numSku;
	}

	public void setNumSku(Integer numSku) {
		this.numSku = numSku;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public void setStt(String stt) {
		this.stt = stt;
	}

	public String getStt() {
		return stt;
	}

	public BigDecimal getKmHang() {
		return kmHang;
	}

	public void setKmHang(BigDecimal kmHang) {
		this.kmHang = kmHang;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Integer getFlag() {
		return flag;
	}
}

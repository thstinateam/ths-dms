package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RptPGNVTTGVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// danh sach mat hang ban
	private List<RptPGNVTTGDetailVO> lstSaleProduct;
	
	// danh sach mat hang khuyen mai
	private List<RptPGNVTTGDetailVO> lstPromoProduct;
	
	// ma - ten - so dien thoai khach hang
	private String customerInfo;
	
	// ten va xe nvgh
	private String deliveryStaffInfo;
	
	// dia chi giao hang
	private String deliveryAddress;

//	private String staffInfo;
	
	// danh sach hoa don gop
	private String lstOrderNumber;
	
	//tong tien phai tra
	private BigDecimal total;
		
	//chiet khau hoa don
	private BigDecimal billDiscount = new BigDecimal(0);
	
	//chiet khau tien
	private BigDecimal moneyDiscount;
	
	//dieu chinh
	private BigDecimal modifiedValue = new BigDecimal(0);
	
	//gia tri hang khuyen mai
	private BigDecimal promotionValue = new BigDecimal(0);
	
	//tien chua chiet khau
	private BigDecimal amount;
	
	private Long deliveryId;
	
	private Long customerId;
	
	private String totalString;	

	public List<RptPGNVTTGDetailVO> getLstSaleProduct() {
		return lstSaleProduct;
	}

	public void setLstSaleProduct(List<RptPGNVTTGDetailVO> lstSaleProduct) {
		this.lstSaleProduct = lstSaleProduct;
	}

	public List<RptPGNVTTGDetailVO> getLstPromoProduct() {
		return lstPromoProduct;
	}

	public void setLstPromoProduct(List<RptPGNVTTGDetailVO> lstPromoProduct) {
		this.lstPromoProduct = lstPromoProduct;
	}

	public String getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(String customerInfo) {
		this.customerInfo = customerInfo;
	}

	public String getDeliveryStaffInfo() {
		return deliveryStaffInfo;
	}

	public void setDeliveryStaffInfo(String deliveryStaffInfo) {
		this.deliveryStaffInfo = deliveryStaffInfo;
	}

	public String getLstOrderNumber() {
		return lstOrderNumber;
	}

	public void setLstOrderNumber(String lstOrderNumber) {
		this.lstOrderNumber = lstOrderNumber;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getBillDiscount() {
		return billDiscount;
	}

	public void setBillDiscount(BigDecimal billDiscount) {
		this.billDiscount = billDiscount;
	}

	public BigDecimal getMoneyDiscount() {
		return moneyDiscount;
	}

	public void setMoneyDiscount(BigDecimal moneyDiscount) {
		this.moneyDiscount = moneyDiscount;
	}

	public BigDecimal getModifiedValue() {
		return modifiedValue;
	}

	public void setModifiedValue(BigDecimal modifiedValue) {
		this.modifiedValue = modifiedValue;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public String getDeliveryAddress() {
		return deliveryAddress== null?"":deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public BigDecimal getPromotionValue() {
		return promotionValue;
	}

	public void setPromotionValue(BigDecimal promotionValue) {
		this.promotionValue = promotionValue;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getTotalString() {
		return totalString== null?"":totalString;
	}

	public void setTotalString(String totalString) {
		this.totalString = totalString;
	}
}

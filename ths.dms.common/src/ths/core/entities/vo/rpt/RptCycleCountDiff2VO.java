package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptCycleCountDiff2VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cycleCountCode;// 1
	private String productCode;// 2
	private List<RptCycleCountDiff3VO> lstRptCycleCountDiff3VO = new ArrayList<RptCycleCountDiff3VO>();
	private Integer quantityCounted = 0;// 5
	private Integer quantityBeforeCount = 0;// 6
	private BigDecimal price;// 7
	private Integer quantityDiff = 0;// 8
	private BigDecimal amountDiff = new BigDecimal(0);// 9
	private String stockCardNumber;
	private Integer checkLot;


	public String getCycleCountCode() {
		return cycleCountCode;
	}

	public void setCycleCountCode(String cycleCountCode) {
		this.cycleCountCode = cycleCountCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public List<RptCycleCountDiff3VO> getLstRptCycleCountDiff3VO() {
		return lstRptCycleCountDiff3VO;
	}

	public void setLstRptCycleCountDiff3VO(
			List<RptCycleCountDiff3VO> lstRptCycleCountDiff3VO) {
		this.lstRptCycleCountDiff3VO = lstRptCycleCountDiff3VO;
	}

	public Integer getQuantityCounted() {
		return quantityCounted;
	}

	public void setQuantityCounted(Integer quantityCounted) {
		this.quantityCounted = quantityCounted;
	}

	public Integer getQuantityBeforeCount() {
		return quantityBeforeCount;
	}

	public void setQuantityBeforeCount(Integer quantityBeforeCount) {
		this.quantityBeforeCount = quantityBeforeCount;
	}

	public Integer getQuantityDiff() {
		return quantityDiff;
	}

	public void setQuantityDiff(Integer quantityDiff) {
		this.quantityDiff = quantityDiff;
	}

	public BigDecimal getAmountDiff() {
		return amountDiff;
	}

	public void setAmountDiff(BigDecimal amountDiff) {
		this.amountDiff = amountDiff;
	}

	public String getStockCardNumber() {
		return stockCardNumber;
	}

	public void setStockCardNumber(String stockCardNumber) {
		this.stockCardNumber = stockCardNumber;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
}

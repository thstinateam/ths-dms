package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class RptPDHCGVO implements Serializable{
	
	private Long soID; //id hoa don
	private String customerName; //ho ten khach hang
	private String customerAddress; //dia chi giao hang
	private String deliveryCode; //ma nv giao hang
	private String deliveryName; //ho ten nv giao hang
	private String orderNumber; //hoa don so
	private Date orderDate; //ngay hoa don
	private String staffCode; //ma nv ban hang
	private String staffName; //ho ten nv ban hang
	private BigDecimal discount; //chiet khau don hang
	private BigDecimal total; //tong tien
	
	private List<RptSaleOrderDetail2VO> lst;
	
	
	public Long getSoID() {
		return soID;
	}
	public void setSoID(Long soID) {
		this.soID = soID;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public List<RptSaleOrderDetail2VO> getLst() {
		return lst;
	}
	public void setLst(List<RptSaleOrderDetail2VO> lst) {
		this.lst = lst;
	}
	
	

}

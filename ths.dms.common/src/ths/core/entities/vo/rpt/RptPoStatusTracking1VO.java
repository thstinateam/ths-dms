package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

public class RptPoStatusTracking1VO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String poAutoNumber;

	private Integer skuQuantity;
	
	private List<RptPoStatusTrackingVO> lstRptPoStatusTrackingVO;

	public String getPoAutoNumber() {
		return poAutoNumber;
	}

	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}

	public List<RptPoStatusTrackingVO> getLstRptPoStatusTrackingVO() {
		return lstRptPoStatusTrackingVO;
	}

	public void setLstRptPoStatusTrackingVO(
			List<RptPoStatusTrackingVO> lstRptPoStatusTrackingVO) {
		this.lstRptPoStatusTrackingVO = lstRptPoStatusTrackingVO;
	}
	
	public Integer getSkuQuantity() {
		return skuQuantity;
	}

	public void setSkuQuantity(Integer skuQuantity) {
		this.skuQuantity = skuQuantity;
	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

public class RptProductPoVnmDetail1VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String productCode;
	private String productName;
	private ArrayList<RptProductPoVnmDetail2VO> lstRptProductPoVnmDetail2VO = new ArrayList<RptProductPoVnmDetail2VO>();

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public ArrayList<RptProductPoVnmDetail2VO> getLstRptProductPoVnmDetail2VO() {
		return lstRptProductPoVnmDetail2VO;
	}

	public void setLstRptProductPoVnmDetail2VO(
			ArrayList<RptProductPoVnmDetail2VO> lstRptProductPoVnmDetail2VO) {
		this.lstRptProductPoVnmDetail2VO = lstRptProductPoVnmDetail2VO;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.util.DateUtil;

import ths.dms.core.common.utils.GroupUtility;

public class RptReturnSaleOrderByDeliveryDataVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Date orderDate;
	private String strOrderDate;
	private String orderNumber;
	private BigDecimal total;
	private Long deliveryId;
	private String deliveryCode;
	private String deliveryName;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String shortCode;
	private String phone;
	private String mobiphone;

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
		if(orderDate!=null){
			this.strOrderDate = DateUtil.formatDate(this.orderDate, "dd/MM/yyyy");
		}
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
		if(total==null){
			this.total = BigDecimal.ZERO;
		}
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobiphone() {
		return mobiphone;
	}

	public void setMobiphone(String mobiphone) {
		this.mobiphone = mobiphone;
	}

	public String getCustomerAddress() {
		if(customerAddress!=null && customerAddress!=""){
			return customerAddress;
		}else{
			return "";
		}
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}
	
	public String getStrOrderDate() {
		return strOrderDate;
	}

	public void setStrOrderDate(String strOrderDate) {
		this.strOrderDate = strOrderDate;
	}

	@SuppressWarnings("unchecked")
	public static List<RptReturnSaleOrderByDeliveryLv01VO> groupByCusAndCat(
			List<RptReturnSaleOrderByDeliveryDataVO> listData) throws Exception {
		List<RptReturnSaleOrderByDeliveryLv01VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, "deliveryId");

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptReturnSaleOrderByDeliveryLv01VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptReturnSaleOrderByDeliveryLv01VO vo = new RptReturnSaleOrderByDeliveryLv01VO();

					List<RptReturnSaleOrderByDeliveryDataVO> listObject = (List<RptReturnSaleOrderByDeliveryDataVO>) mapEntry.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setDeliveryId(listObject.get(0).getDeliveryId());
						vo.setDeliveryCode(listObject.get(0).getDeliveryCode());
						vo.setDeliveryName(listObject.get(0).getDeliveryName());

						vo.setListData(listObject);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}
}
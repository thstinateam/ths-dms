package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

//doanh so phan phoi theo nhom hang
public class RptBCKD1_2_Mien implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String parentSuperShopCode;
	private BigDecimal sumAmountMien;//tong ds
	//private Float sumAmountMien;
	private Integer sumCustomerMien;//tong khach hang
	private List<RptBCKD1_2_Vung> listVung;
	
	public List<RptBCKD1_2_Vung> getListVung() {
		return listVung;
	}

	public void setListVung(List<RptBCKD1_2_Vung> listVung) {
		this.listVung = listVung;
	}

	public BigDecimal getSumAmountMien() {
		return sumAmountMien;
	}

	public void setSumAmountMien(BigDecimal sumAmountMien) {
		this.sumAmountMien = sumAmountMien;
	}

	public Integer getSumCustomerMien() {
		return sumCustomerMien;
	}

	public void setSumCustomerMien(Integer sumCustomerMien) {
		this.sumCustomerMien = sumCustomerMien;
	}

	public String getParentSuperShopCode() {
		return parentSuperShopCode;
	}

	public void setParentSuperShopCode(String parentSuperShopCode) {
		this.parentSuperShopCode = parentSuperShopCode;
	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

public class RptAbsentVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer colNum; //STT : 
	private String mienCode;
	private String vungCode;
	private String tbhv;
	private String shopCode;
	private String shopName;
	private String nvgs;
	private String staffCode;
	private String staffName;
	private List<RptAbsentDetailVO> lstDay;
	private String dayStr;
	private String dayVal;//X:ko bat may
	private String summaryValue;
	private List<String> lstDayVal;
	
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<String> getLstDayVal() {
		return lstDayVal;
	}
	public void setLstDayVal(List<String> lstDayVal) {
		this.lstDayVal = lstDayVal;
	}
	public Integer getColNum() {
		return colNum;
	}
	public void setColNum(Integer colNum) {
		this.colNum = colNum;
	}
	public String getMienCode() {
		return mienCode;
	}
	public void setMienCode(String mienCode) {
		this.mienCode = mienCode;
	}
	public String getVungCode() {
		return vungCode;
	}
	public void setVungCode(String vungCode) {
		this.vungCode = vungCode;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public List<RptAbsentDetailVO> getLstDay() {
		return lstDay;
	}
	public void setLstDay(List<RptAbsentDetailVO> lstDay) {
		this.lstDay = lstDay;
	}
	public String getDayStr() {
		return dayStr;
	}
	public void setDayStr(String dayStr) {
		this.dayStr = dayStr;
	}
	public String getDayVal() {
		return dayVal;
	}
	public void setDayVal(String dayVal) {
		this.dayVal = dayVal;
	}
	public String getSummaryValue() {
		return summaryValue;
	}
	public void setSummaryValue(String summaryValue) {
		this.summaryValue = summaryValue;
	}

	public String getTbhv() {
		return tbhv;
	}

	public void setTbhv(String tbhv) {
		this.tbhv = tbhv;
	}

	public String getNvgs() {
		return nvgs;
	}

	public void setNvgs(String nvgs) {
		this.nvgs = nvgs;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCVT1 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String MAMIEN; //mien
	private String MAVUNG; //vung
	private String MANPP; //npp
	private String TENNPP; //ten npp
	private String TENGSNPP; //ten gsnpp
	private String MANVBH; //nvbh
	private String TENNVBH; //ten nvbh
	private Integer TONGSOKHACHHANG; //Tong so KH
	private Integer SOKHACHHANGTHUCGHE; //SOKHACHHANGTHUCGHE
	private Integer KHACHHANGTRONGTUYENKOGHETHAM; //KHACHHANGTRONGTUYENKOGHETHAM
	private Integer TONGTHOIGIANLAMVIEC; //TONGTHOIGIANLAMVIEC   ----10
	private String THOIGIANBATDAU; //THOIGIANBATDAU
	private String THOIGIANKETTHUC; // THOIGIANKETTHUC
	private Integer SOLANGHETHAMDUOI5PHUT; //SOLANGHETHAMDUOI5PHUT
	private Integer SOLANGHETHAMDUOI30PHUT; //SOLANGHETHAMDUOI30PHUT
	private Integer SOLANGHETHAMDUOI60PHUT; //SOLANGHETHAMDUOI60PHUT
	private Integer SOLANGHETHAMTREN60PHUT; //SOLANGHETHAMTREN60PHUT
	private Float TBTHOIGIANGHETHAMKHACHHANG ; //TBTHOIGIANGHETHAMKHACHHANG'
	
	public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	public String getMAMIEN() {
		return MAMIEN;
	}

	public void setMAMIEN(String mAMIEN) {
		MAMIEN = mAMIEN;
	}

	public String getMAVUNG() {
		return MAVUNG;
	}

	public void setMAVUNG(String mAVUNG) {
		MAVUNG = mAVUNG;
	}

	public String getMANPP() {
		return MANPP;
	}

	public void setMANPP(String mANPP) {
		MANPP = mANPP;
	}

	public String getTENNPP() {
		return TENNPP;
	}

	public void setTENNPP(String tENNPP) {
		TENNPP = tENNPP;
	}

	public String getTENGSNPP() {
		return TENGSNPP;
	}

	public void setTENGSNPP(String tENGSNPP) {
		TENGSNPP = tENGSNPP;
	}

	public String getMANVBH() {
		return MANVBH;
	}

	public void setMANVBH(String mANVBH) {
		MANVBH = mANVBH;
	}

	public String getTENNVBH() {
		return TENNVBH;
	}

	public void setTENNVBH(String tENNVBH) {
		TENNVBH = tENNVBH;
	}

	public Integer getTONGSOKHACHHANG() {
		if(TONGSOKHACHHANG!=null){
			return TONGSOKHACHHANG;
		}
		return 0;
	}

	public void setTONGSOKHACHHANG(BigDecimal tONGSOKHACHHANG) {
		if (tONGSOKHACHHANG != null) {
			TONGSOKHACHHANG = tONGSOKHACHHANG.intValue();
		}
	}

	public Integer getSOKHACHHANGTHUCGHE() {
		return SOKHACHHANGTHUCGHE;
	}

	public void setSOKHACHHANGTHUCGHE(BigDecimal sOKHACHHANGTHUCGHE) {
		if (sOKHACHHANGTHUCGHE != null) {
			this.SOKHACHHANGTHUCGHE = sOKHACHHANGTHUCGHE.intValue();
		}
	}

	public Integer getKHACHHANGTRONGTUYENKOGHETHAM() {
		return KHACHHANGTRONGTUYENKOGHETHAM;
	}

	public void setKHACHHANGTRONGTUYENKOGHETHAM(BigDecimal kHACHHANGTRONGTUYENKOGHETHAM) {
		if (kHACHHANGTRONGTUYENKOGHETHAM != null) {
			this.KHACHHANGTRONGTUYENKOGHETHAM = kHACHHANGTRONGTUYENKOGHETHAM.intValue();
		}
	}

	public Integer getTONGTHOIGIANLAMVIEC() {
		return TONGTHOIGIANLAMVIEC;
	}

	public void setTONGTHOIGIANLAMVIEC(BigDecimal tONGTHOIGIANLAMVIEC) {
		if (tONGTHOIGIANLAMVIEC != null) {
			this.TONGTHOIGIANLAMVIEC = tONGTHOIGIANLAMVIEC.intValue();
		}
	}

	public String getTHOIGIANBATDAU() {
		return THOIGIANBATDAU;
	}

	public void setTHOIGIANBATDAU(String tHOIGIANBATDAU) {
		THOIGIANBATDAU = tHOIGIANBATDAU;
	}

	public String getTHOIGIANKETTHUC() {
		return THOIGIANKETTHUC;
	}

	public void setTHOIGIANKETTHUC(String tHOIGIANKETTHUC) {
		THOIGIANKETTHUC = tHOIGIANKETTHUC;
	}

	public Integer getSOLANGHETHAMDUOI5PHUT() {
		return SOLANGHETHAMDUOI5PHUT;
	}
	
	public void setSOLANGHETHAMDUOI5PHUT(BigDecimal sOLANGHETHAMDUOI5PHUT) {
		if (sOLANGHETHAMDUOI5PHUT != null) {
			this.SOLANGHETHAMDUOI5PHUT = sOLANGHETHAMDUOI5PHUT.intValue();
		}
	}

	public Integer getSOLANGHETHAMDUOI30PHUT() {
		return SOLANGHETHAMDUOI30PHUT;
	}

	public void setSOLANGHETHAMDUOI30PHUT(BigDecimal sOLANGHETHAMDUOI30PHUT) {
		if (sOLANGHETHAMDUOI30PHUT != null) {
			this.SOLANGHETHAMDUOI30PHUT = sOLANGHETHAMDUOI30PHUT.intValue();
		}
	}

	public Integer getSOLANGHETHAMDUOI60PHUT() {
		return SOLANGHETHAMDUOI60PHUT;
	}

	public void setSOLANGHETHAMDUOI60PHUT(BigDecimal sOLANGHETHAMDUOI60PHUT) {
		if (sOLANGHETHAMDUOI60PHUT != null) {
			this.SOLANGHETHAMDUOI60PHUT = sOLANGHETHAMDUOI60PHUT.intValue();
		}
	}

	public Integer getSOLANGHETHAMTREN60PHUT() {
		return SOLANGHETHAMTREN60PHUT;
	}

	public void setSOLANGHETHAMTREN60PHUT(BigDecimal sOLANGHETHAMTREN60PHUT) {
		if (sOLANGHETHAMTREN60PHUT != null) {
			this.SOLANGHETHAMTREN60PHUT = sOLANGHETHAMTREN60PHUT.intValue();
		}
	}

	public Float getTBTHOIGIANGHETHAMKHACHHANG() {
		return TBTHOIGIANGHETHAMKHACHHANG;
	}

	public void setTBTHOIGIANGHETHAMKHACHHANG(BigDecimal tBTHOIGIANGHETHAMKHACHHANG) {
		if (tBTHOIGIANGHETHAMKHACHHANG != null) {
			this.TBTHOIGIANGHETHAMKHACHHANG = tBTHOIGIANGHETHAMKHACHHANG.floatValue();
		}
	}
}

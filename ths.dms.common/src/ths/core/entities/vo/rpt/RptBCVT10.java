package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCVT10 implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String mien;
	private String maGSMT;	
	private String tenGSMT;
	private String maKH;
	private String tenKH;
	private String ngay;
	private String maMau;
	private String maCT;
	private String tenCT;
	private String loai;
	private BigDecimal quydinh;
	private BigDecimal thucte;
	
	
	public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}


	public String getMien() {
		return mien;
	}


	public void setMien(String mien) {
		this.mien = mien;
	}


	public String getMaGSMT() {
		return maGSMT;
	}


	public void setMaGSMT(String maGSMT) {
		this.maGSMT = maGSMT;
	}


	public String getTenGSMT() {
		return tenGSMT;
	}


	public void setTenGSMT(String tenGSMT) {
		this.tenGSMT = tenGSMT;
	}


	public String getMaKH() {
		return maKH;
	}


	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}


	public String getTenKH() {
		return tenKH;
	}


	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}


	public String getNgay() {
		return ngay;
	}


	public void setNgay(String ngay) {
		this.ngay = ngay;
	}


	public String getMaMau() {
		return maMau;
	}


	public void setMaMau(String maMau) {
		this.maMau = maMau;
	}


	public String getMaCT() {
		return maCT;
	}


	public void setMaCT(String maCT) {
		this.maCT = maCT;
	}


	public String getTenCT() {
		return tenCT;
	}


	public void setTenCT(String tenCT) {
		this.tenCT = tenCT;
	}


	public String getLoai() {
		return loai;
	}


	public void setLoai(String loai) {
		this.loai = loai;
	}


	public BigDecimal getQuydinh() {
		return quydinh;
	}


	public void setQuydinh(BigDecimal quydinh) {
		this.quydinh = quydinh;
	}


	public BigDecimal getThucte() {
		return thucte;
	}


	public void setThucte(BigDecimal thucte) {
		this.thucte = thucte;
	}	
}

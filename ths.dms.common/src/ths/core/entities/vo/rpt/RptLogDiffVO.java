package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptLogDiffVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<RptLogDiff1VO> lstRptLogDiff1VO = new ArrayList<RptLogDiff1VO>();

	public List<RptLogDiff1VO> getLstRptLogDiff1VO() {
		return lstRptLogDiff1VO;
	}

	public void setLstRptLogDiff1VO(List<RptLogDiff1VO> lstRptLogDiff1VO) {
		this.lstRptLogDiff1VO = lstRptLogDiff1VO;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptImExStDataDetailVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long catId;
	private String catName;
	private String catCode;
	
	private String productCode;
	private String productName;
	private Integer productConvfact;
	private BigDecimal productPrice;
	// ton dau ky
	private Long quantityCloseSTBegin;
	// ton cuoi ky
	private Long quantityCloseSTEnd;
	// xuat ban
	private Long quantitySaleToCus;
	// xuat ban khuyen mai
	private Long quantitySaleOffCus;
	// khach hang tra hang ban
	private Long quantitySaleReturn;
	// khach hang tra hang khuyen mai
	private Long quantityPromotionReturn;
	// mua hang VNM
	private Long quantityBuyFromVNM;
	// tra hang VNM
	private Long quantityReturnToVNM;
	// nhap kho
	private Long quantityImport;
	// xuat kho
	private Long quantityExport;
	/**phuc vu bao cao*/
	private Integer index;
	//Ton dau ky :Thung/Le
	private String quantityCloseSTBeginConvfact;
	//Ton dau ky : Tong tien
	private BigDecimal quantityCloseSTBeginTotal;
	//Xuat ban :Thung/Le
	private String quantitySaleToConvfact;
	//Xuat ban :Thung/Le KM
	private String quantitySaleOffConvfact;
	// KH tra hang: Thung/Le
	private String quantitySaleReturnConvfact;
	// KH tra hang: Thung.Le KM
	private String quantityPromotionReturnConvfact;
	//mua hang VNM :Thung/Le 
	private String quantityBuyFromVNMConvfact;
	//tra hang VNM :Thung/Le 
	private String quantityReturnToVNMConvfact;
	//nhap kho vnm :Thung/Le 
	private String quantityImportConvfact;
	//Xuat kho vnm :Thung/Le 
	private String quantityExportConvfact;
	//Ton cuoi ky :Thung/Le
	private String quantityCloseSTEndConvfact;
	//Ton cuoi ky : Tong tien
	private BigDecimal quantityCloseSTEndTotal;
		
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(BigDecimal productPrice) {
		this.productPrice = productPrice;
	}

	public Long getQuantityCloseSTBegin() {
		return quantityCloseSTBegin;
	}

	public void setQuantityCloseSTBegin(BigDecimal quantityCloseSTBegin) {
		this.quantityCloseSTBegin = quantityCloseSTBegin.longValue();
	}

	public Long getQuantityCloseSTEnd() {
		return quantityCloseSTEnd;
	}

	public void setQuantityCloseSTEnd(BigDecimal quantityCloseSTEnd) {
		this.quantityCloseSTEnd = quantityCloseSTEnd.longValue();
	}

	public Long getQuantitySaleToCus() {
		return quantitySaleToCus;
	}

	public String getCatCode() {
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public void setQuantitySaleToCus(BigDecimal quantitySaleToCus) {
		this.quantitySaleToCus = quantitySaleToCus.longValue();
	}

	public Long getQuantitySaleOffCus() {
		return quantitySaleOffCus;
	}

	public void setQuantitySaleOffCus(BigDecimal quantitySaleOffCus) {
		this.quantitySaleOffCus = quantitySaleOffCus.longValue();
	}

	public Integer getProductConvfact() {
		return productConvfact;
	}

	public void setProductConvfact(BigDecimal productConvfact) {
		this.productConvfact = productConvfact.intValue();
	}

	public Long getCatId() {
		return catId;
	}

	public void setCatId(BigDecimal catId) {
		this.catId = catId.longValue();
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public Long getQuantityBuyFromVNM() {
		return quantityBuyFromVNM;
	}

	public void setQuantityBuyFromVNM(BigDecimal quantityBuyFromVNM) {
		this.quantityBuyFromVNM = quantityBuyFromVNM.longValue();
	}

	public Long getQuantityReturnToVNM() {
		return quantityReturnToVNM;
	}

	public void setQuantityReturnToVNM(BigDecimal quantityReturnToVNM) {
		this.quantityReturnToVNM = quantityReturnToVNM.longValue();
	}

	public Long getQuantityImport() {
		return quantityImport;
	}

	public void setQuantityImport(BigDecimal quantityImport) {
		this.quantityImport = quantityImport.longValue();
	}

	public Long getQuantityExport() {
		return quantityExport;
	}

	public void setQuantityExport(BigDecimal quantityExport) {
		this.quantityExport = quantityExport.longValue();
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getQuantityCloseSTBeginConvfact() {
		return quantityCloseSTBeginConvfact;
	}

	public void setQuantityCloseSTBeginConvfact(String quantityCloseSTBeginConvfact) {
		this.quantityCloseSTBeginConvfact = quantityCloseSTBeginConvfact;
	}

	public BigDecimal getQuantityCloseSTBeginTotal() {
		return quantityCloseSTBeginTotal;
	}

	public void setQuantityCloseSTBeginTotal(BigDecimal quantityCloseSTBeginTotal) {
		this.quantityCloseSTBeginTotal = quantityCloseSTBeginTotal;
	}

	public String getQuantitySaleToConvfact() {
		return quantitySaleToConvfact;
	}

	public void setQuantitySaleToConvfact(String quantitySaleToConvfact) {
		this.quantitySaleToConvfact = quantitySaleToConvfact;
	}

	public String getQuantitySaleOffConvfact() {
		return quantitySaleOffConvfact;
	}

	public void setQuantitySaleOffConvfact(String quantitySaleOffConvfact) {
		this.quantitySaleOffConvfact = quantitySaleOffConvfact;
	}

	public String getQuantityBuyFromVNMConvfact() {
		return quantityBuyFromVNMConvfact;
	}

	public void setQuantityBuyFromVNMConvfact(String quantityBuyFromVNMConvfact) {
		this.quantityBuyFromVNMConvfact = quantityBuyFromVNMConvfact;
	}

	public String getQuantityReturnToVNMConvfact() {
		return quantityReturnToVNMConvfact;
	}

	public void setQuantityReturnToVNMConvfact(String quantityReturnToVNMConvfact) {
		this.quantityReturnToVNMConvfact = quantityReturnToVNMConvfact;
	}

	public String getQuantityImportConvfact() {
		return quantityImportConvfact;
	}

	public void setQuantityImportConvfact(String quantityImportConvfact) {
		this.quantityImportConvfact = quantityImportConvfact;
	}

	public String getQuantityExportConvfact() {
		return quantityExportConvfact;
	}

	public void setQuantityExportConvfact(String quantityExportConvfact) {
		this.quantityExportConvfact = quantityExportConvfact;
	}

	public String getQuantityCloseSTEndConvfact() {
		return quantityCloseSTEndConvfact;
	}

	public void setQuantityCloseSTEndConvfact(String quantityCloseSTEndConvfact) {
		this.quantityCloseSTEndConvfact = quantityCloseSTEndConvfact;
	}

	public BigDecimal getQuantityCloseSTEndTotal() {
		return quantityCloseSTEndTotal;
	}

	public void setQuantityCloseSTEndTotal(BigDecimal quantityCloseSTEndTotal) {
		this.quantityCloseSTEndTotal = quantityCloseSTEndTotal;
	}
	
	public Long getQuantitySaleReturn() {
		return quantitySaleReturn;
	}

	public void setQuantitySaleReturn(BigDecimal quantitySaleReturn) {
		this.quantitySaleReturn = quantitySaleReturn.longValue();
	}

	public Long getQuantityPromotionReturn() {
		return quantityPromotionReturn;
	}

	public void setQuantityPromotionReturn(BigDecimal quantityPromotionReturn) {
		this.quantityPromotionReturn = quantityPromotionReturn.longValue();
	}

	public String getQuantitySaleReturnConvfact() {
		return quantitySaleReturnConvfact;
	}

	public void setQuantitySaleReturnConvfact(String quantitySaleReturnConvfact) {
		this.quantitySaleReturnConvfact = quantitySaleReturnConvfact;
	}

	public String getQuantityPromotionReturnConvfact() {
		return quantityPromotionReturnConvfact;
	}

	public void setQuantityPromotionReturnConvfact(
			String quantityPromotionReturnConvfact) {
		this.quantityPromotionReturnConvfact = quantityPromotionReturnConvfact;
	}

	//
	public void initReportField(){
		if(this.productConvfact != null && this.productConvfact > 0){
			if(this.quantityCloseSTBegin != null){
				if(this.quantityCloseSTBegin == 0){
					this.quantityCloseSTBeginConvfact = "0";
				}else{
					long nguyen = this.quantityCloseSTBegin/this.productConvfact;
					long du = this.quantityCloseSTBegin%this.productConvfact;
					if(du<0 && nguyen==0){
						du = du*(-1);
						this.quantityCloseSTBeginConvfact = "-"+nguyen + "/" + du;
					}else{
						if(du<0){
							du = du*(-1);
						}
						this.quantityCloseSTBeginConvfact = nguyen + "/" + du;
					}
				}
			}
			if(this.quantitySaleToCus != null){
				if(this.quantitySaleToCus == 0){
					this.quantitySaleToConvfact = "0";
				}else{
					long nguyen = this.quantitySaleToCus/this.productConvfact;
					long du = this.quantitySaleToCus%this.productConvfact;
					if(nguyen==0){
						if(du<0){
							du = du*(-1);
							this.quantitySaleToConvfact = nguyen + "/" + du;
						}else{
							this.quantitySaleToConvfact = "-" + nguyen + "/" + du;
						}
					}else{
						if(du<0){
							du = du*(-1);
						}
						nguyen=nguyen*(-1);
						this.quantitySaleToConvfact = nguyen + "/" + du;
					}
				}
			}
			if(this.quantitySaleOffCus != null){
				if(this.quantitySaleOffCus==0){
					this.quantitySaleOffConvfact = "0";
				}else{
					long nguyen = this.quantitySaleOffCus/this.productConvfact;
					long du = this.quantitySaleOffCus%this.productConvfact;
					if(nguyen==0){
						if(du<0){
							du = du*(-1);
							this.quantitySaleOffConvfact = nguyen + "/" + du;
						}else{
							this.quantitySaleOffConvfact = "-" + nguyen + "/" + du;
						}
					}else{
						if(du<0){
							du = du*(-1);
						}
						nguyen=nguyen*(-1);
						this.quantitySaleOffConvfact = nguyen + "/" + du;
					}
				}
			}
			if(this.quantitySaleReturn != null){
				if(this.quantitySaleReturn == 0){
					this.quantitySaleReturnConvfact = "0";
				}else{
					long nguyen = this.quantitySaleReturn/this.productConvfact;
					long du = this.quantitySaleReturn%this.productConvfact;
					if(du<0 && nguyen==0){
						du = du*(-1);
						this.quantitySaleReturnConvfact = "-" + nguyen + "/" + du;
					}else{
						if(du<0){
							du = du*(-1);
						}
						this.quantitySaleReturnConvfact = nguyen + "/" + du;
					}
				}
			}
			if(this.quantityPromotionReturn != null){
				if(this.quantityPromotionReturn == 0){
					this.quantityPromotionReturnConvfact = "0";
				}else{
					long nguyen = this.quantityPromotionReturn/this.productConvfact;
					long du = this.quantityPromotionReturn%this.productConvfact;
					if(du<0 && nguyen==0){
						du = du*(-1);
						this.quantityPromotionReturnConvfact = "-" + nguyen + "/" + du;
					}else{
						if(du<0){
							du = du*(-1);
						}
						this.quantityPromotionReturnConvfact = nguyen + "/" + du;
					}
				}
			}
			if(this.quantityBuyFromVNM != null){
				if(this.quantityBuyFromVNM==0){
					this.quantityBuyFromVNMConvfact = "0";
				}else{
					long nguyen = this.quantityBuyFromVNM/this.productConvfact;
					long du = this.quantityBuyFromVNM%this.productConvfact;
					if(du<0 && nguyen==0){
						du = du*(-1);
						this.quantityBuyFromVNMConvfact = "-" + nguyen + "/" + du;
					}else{
						if(du<0){
							du = du*(-1);
						}
						this.quantityBuyFromVNMConvfact = nguyen + "/" + du;
					}
				}
			}
			if(this.quantityReturnToVNM != null){
				if(this.quantityReturnToVNM==0){
					this.quantityReturnToVNMConvfact = "0";
				}else{
					long nguyen = this.quantityReturnToVNM/this.productConvfact;
					long du = this.quantityReturnToVNM%this.productConvfact;
					if(nguyen==0){
						if(du<0){
							du = du*(-1);
							this.quantityReturnToVNMConvfact = nguyen + "/" + du;
						}else{
							this.quantityReturnToVNMConvfact = "-" + nguyen + "/" + du;
						}
					}else{
						if(du<0){
							du = du*(-1);
						}
						nguyen=nguyen*(-1);
						this.quantityReturnToVNMConvfact = nguyen + "/" + du;
					}
				}
			}
			if(this.quantityImport != null){
				if(this.quantityImport==0){
					this.quantityImportConvfact = "0";
				}else{
					long nguyen = this.quantityImport/this.productConvfact;
					long du = this.quantityImport%this.productConvfact;
					if(du<0 && nguyen==0){
						du = du*(-1);
						this.quantityImportConvfact = "-" + nguyen + "/" + du;
					}else{
						if(du<0){
							du = du*(-1);
						}
						this.quantityImportConvfact = nguyen + "/" + du;
					}
				}
			}
			if(this.quantityExport != null){
				if(this.quantityExport==0){
					this.quantityExportConvfact = "0";
				}else{
					long nguyen = this.quantityExport/this.productConvfact;
					long du = this.quantityExport%this.productConvfact;
					if(nguyen==0){
						if(du<0){
							du = du*(-1);
							this.quantityExportConvfact = nguyen + "/" + du;
						}else{
							this.quantityExportConvfact = "-" + nguyen + "/" + du;
						}
					}else{
						if(du<0){
							du = du*(-1);
						}
						nguyen=nguyen*(-1);
						this.quantityExportConvfact = nguyen + "/" + du;
					}
				}
			}
			if(this.quantityCloseSTEnd != null){
				if(this.quantityCloseSTEnd==0){
					this.quantityCloseSTEndConvfact = "0";
				}else{
					long nguyen = this.quantityCloseSTEnd/this.productConvfact;
					long du = this.quantityCloseSTEnd%this.productConvfact;
					if(du<0 && nguyen==0){
						du = du*(-1);
						this.quantityCloseSTEndConvfact = "-" + nguyen + "/" + du;
					}else{
						if(du<0){
							du = du*(-1);
						}
						this.quantityCloseSTEndConvfact = nguyen + "/" + du;
					}
				}
			}
		}
		
		if(this.productPrice != null){
			if(this.quantityCloseSTBegin != null){
				this.quantityCloseSTBeginTotal = this.productPrice.multiply(new BigDecimal(this.quantityCloseSTBegin));
			}
			if(this.quantityCloseSTEnd != null){
				this.quantityCloseSTEndTotal = this.productPrice.multiply(new BigDecimal(this.quantityCloseSTEnd));
			}
		}
	}
	public String toString(){
		StringBuilder sb = new StringBuilder();
		if(catId != null){
			sb.append(" catId :" + catId);
		}
		if(catName != null){
			sb.append(" catName :" + catId);
		}
		if(catCode != null){
			sb.append(" catCode :" + catCode);
		}
		if(productConvfact != null){
			sb.append(" productConvfact :" + productConvfact);
		}
		if(productCode != null){
			sb.append(" productCode :" + productCode);
		}
		if(productName != null){
			sb.append(" productName :" + productName);
		}
		if(productPrice != null){
			sb.append(" productPrice :" + productPrice);
		}
		if(quantityCloseSTBegin != null){
			sb.append(" quantityCloseSTBegin :" + quantityCloseSTBegin);
		}
		if(quantityCloseSTBegin != null){
			sb.append(" quantityCloseSTBegin :" + quantityCloseSTBegin);
		}
		if(quantityCloseSTEnd != null){
			sb.append(" quantityCloseSTEnd :" + quantityCloseSTEnd);
		}
		if(quantitySaleOffCus != null){
			sb.append(" quantitySaleOffCus :" + quantitySaleOffCus);
		}
		if(quantityBuyFromVNM != null){
			sb.append(" quantityBuyFromVNM :" + quantityBuyFromVNM);
		}
		if(quantityReturnToVNM != null){
			sb.append(" quantityReturnToVNM :" + quantityReturnToVNM);
		}
		if(quantityImport != null){
			sb.append(" quantityImport :" + quantityImport);
		}
		if(quantityExport != null){
			sb.append(" quantityExport :" + quantityExport);
		}
		
		if(index != null){
			sb.append(" index :" + index);
		}
		if(quantityCloseSTBeginConvfact != null){
			sb.append(" quantityCloseSTBeginConvfact :" + quantityCloseSTBeginConvfact);
		}
		if(quantityCloseSTBeginTotal != null){
			sb.append(" quantityCloseSTBeginTotal :" + quantityCloseSTBeginTotal);
		}
		if(quantitySaleToConvfact != null){
			sb.append(" quantitySaleToConvfact :" + quantitySaleToConvfact);
		}
		if(quantitySaleOffConvfact != null){
			sb.append(" quantitySaleOffConvfact :" + quantitySaleOffConvfact);
		}
		if(quantityBuyFromVNMConvfact != null){
			sb.append(" quantityBuyFromVNMConvfact :" + quantityBuyFromVNMConvfact);
		}
		if(quantityReturnToVNMConvfact != null){
			sb.append(" quantityReturnToVNMConvfact :" + quantityReturnToVNMConvfact);
		}
		if(quantityImportConvfact != null){
			sb.append(" quantityImportConvfact :" + quantityImportConvfact);
		}
		if(quantityExportConvfact != null){
			sb.append(" quantityExportConvfact :" + quantityExportConvfact);
		}
		if(quantityCloseSTEndConvfact != null){
			sb.append(" quantityCloseSTEndConvfact :" + quantityCloseSTEndConvfact);
		}
		if(quantityCloseSTEndTotal != null){
			sb.append(" quantityCloseSTEndTotal :" + quantityCloseSTEndTotal);
		}
		
		return sb.toString();
	}
}

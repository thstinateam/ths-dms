package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VO: bao cao ho - kM1.3
 * 
 * @author lacnv1
 * @since Mar 21, 2014
 */
public class Rpt_HO_KM1_3 implements Serializable {

	private static final long serialVersionUID = 1L;

	private String mien;
	private String vung;
	private String maNPP;
	private String tenNPP;
	private String maCTKM;
	private String loaiCTKM;
	private String tenCTKM;
	private String maNVBH;
	private String tenNVBH;
	private String maKH;
	private String tenKH;
	private String diaChi;
	private String ngayBD;
	private String ngayKT;
	private String ngayDH;
	private String maSP;
	private String tenSP;
	private BigDecimal kmTienThucTe; // khuyen mai tien
	private Integer slgKmThucTe;
	private BigDecimal tienKmThucTe; // tien hang KM
	private Integer slgThucTe;
	private BigDecimal doanhThuThucTe;

	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getTenNPP() {
		return tenNPP;
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}

	public String getMaCTKM() {
		return maCTKM;
	}

	public void setMaCTKM(String maCTKM) {
		this.maCTKM = maCTKM;
	}

	public String getLoaiCTKM() {
		return loaiCTKM;
	}

	public void setLoaiCTKM(String loaiCTKM) {
		this.loaiCTKM = loaiCTKM;
	}

	public String getTenCTKM() {
		return tenCTKM;
	}

	public void setTenCTKM(String tenCTKM) {
		this.tenCTKM = tenCTKM;
	}

	public String getMaNVBH() {
		return maNVBH;
	}

	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}

	public String getTenNVBH() {
		return tenNVBH;
	}

	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}

	public String getMaKH() {
		return maKH;
	}

	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}

	public String getNgayBD() {
		return ngayBD;
	}

	public void setNgayBD(String ngayBD) {
		this.ngayBD = ngayBD;
	}

	public String getNgayKT() {
		return ngayKT;
	}

	public void setNgayKT(String ngayKT) {
		this.ngayKT = ngayKT;
	}

	public String getNgayDH() {
		return ngayDH;
	}

	public void setNgayDH(String ngayDH) {
		this.ngayDH = ngayDH;
	}

	public String getMaSP() {
		return maSP;
	}

	public void setMaSP(String maSP) {
		this.maSP = maSP;
	}

	public String getTenSP() {
		return tenSP;
	}

	public void setTenSP(String tenSP) {
		this.tenSP = tenSP;
	}

	public BigDecimal getKmTienThucTe() {
		return kmTienThucTe;
	}

	public void setKmTienThucTe(BigDecimal kmTienThucTe) {
		this.kmTienThucTe = kmTienThucTe;
	}

	public Integer getSlgKmThucTe() {
		return slgKmThucTe;
	}

	public void setSlgKmThucTe(Integer slgKmThucTe) {
		this.slgKmThucTe = slgKmThucTe;
	}

	public BigDecimal getTienKmThucTe() {
		return tienKmThucTe;
	}

	public void setTienKmThucTe(BigDecimal tienKmThucTe) {
		this.tienKmThucTe = tienKmThucTe;
	}

	public Integer getSlgThucTe() {
		return slgThucTe;
	}

	public void setSlgThucTe(Integer slgThucTe) {
		this.slgThucTe = slgThucTe;
	}

	public BigDecimal getDoanhThuThucTe() {
		return doanhThuThucTe;
	}

	public void setDoanhThuThucTe(BigDecimal doanhThuThucTe) {
		this.doanhThuThucTe = doanhThuThucTe;
	}

	public String getTenKH() {
		return tenKH;
	}

	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
}
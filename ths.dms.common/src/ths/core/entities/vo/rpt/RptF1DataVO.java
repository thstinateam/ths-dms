package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ths.dms.core.common.utils.GroupUtility;

public class RptF1DataVO implements Serializable {
	private static final long serialVersionUID = 1L;

	// so ngay ban hang trong thang
	private Integer saleDateInMonth;
	// so ngay ban hang thu te trong thang
	private Integer realSaleDateInMonth;
	// toDate
	private Date toDate;

	// product status
	private Integer status;
	// ma nganh hang
	private String productInfoCode;
	// ten nganh hang
	private String productInfoName;
	// ma san pham
	private String productCode;
	// ti le tang truong
	private Integer percentage;

	private String productName;
	private BigDecimal price;
	// ton dau ky
	private Long openStockTotal;
	private Long importQty;
	private Long exportQty;
	// ton cuoi ky
	private Long closeStockTotal;
	// luy ke tieu thu thang
	private Long monthCumulate;
	//luy ke tieu thu 10 ngay gan nhat
	private Long monthCumulateTen;
	// ke hoach tieu thu thang
	private Long monthPlan;
	// dinh muc ke hoach
	private Long dayPlan;
	// ngay du tru: ke hoach (*)
	private Float dayReservePlan;
	// ngay du tru: thuc te (*)
	private Long dayReserveReal;
	// ton kho an toan
	private Long min;
	private Long max;
	private Long lead;
	private Long next;
	// yeu cau ton so luong
	private Long requimentStock;
	// so luong po comfirm
	private Long poCfQty;
	// so luong po dvkh
	private Long poCsQty;
	// quy cach
	private Long convfact;
	// so luong thung
	private Long boxQuantity;
	// thanh tien
	private BigDecimal amount;
	// canh bao
	private String warning;
	
	private int type;
	//kiem tra dat hang tat ca ngay trong tuan
	private Long isOrderFull;
	//kiem tra khong dat hang tat ca ngay trong tuan
	private Long isOrderNotFull;
	//kiem tra mat hang co ton tai trong nhom co dinh
	private Long isGroup;
	//tong gia tri ton
	private BigDecimal amountTotal;
	
	public RptF1DataVO() {
	}

	public Integer getSaleDateInMonth() {
		return saleDateInMonth;
	}

	public void setSaleDateInMonth(BigDecimal saleDateInMonth) {
		this.saleDateInMonth = saleDateInMonth.intValue();
	}

	public Integer getRealSaleDateInMonth() {
		return realSaleDateInMonth;
	}

	public void setRealSaleDateInMonth(BigDecimal realSaleDateInMonth) {
		this.realSaleDateInMonth = realSaleDateInMonth.intValue();
	}
	
	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getProductInfoCode() {
		return productInfoCode;
	}

	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Integer getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage.intValue();
	}

	public String getProductName() {
//		if(this.getType()==0){
//			return this.getProductCode()+"-"+productName;
//		}else{
			return productName;
		//}
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getOpenStockTotal() {
		return openStockTotal;
	}

	public void setOpenStockTotal(BigDecimal openStockTotal) {
		this.openStockTotal = openStockTotal.longValue();
	}

	public Long getImportQty() {
		return importQty;
	}

	public void setImportQty(BigDecimal importQty) {
		this.importQty = importQty.longValue();
	}

	public Long getExportQty() {
		return exportQty;
	}

	public void setExportQty(BigDecimal exportQty) {
		this.exportQty = exportQty.longValue();
	}

	public Long getCloseStockTotal() {
		return closeStockTotal;
	}

	public void setCloseStockTotal(BigDecimal closeStockTotal) {
		this.closeStockTotal = closeStockTotal.longValue();
	}

	public Long getMonthCumulate() {
		return monthCumulate;
	}

	public void setMonthCumulate(BigDecimal monthCumulate) {
		this.monthCumulate = monthCumulate.longValue();
	}

	public Long getMonthPlan() {
		return monthPlan;
	}

	public void setMonthPlan(BigDecimal monthPlan) {
		this.monthPlan = monthPlan.longValue();
	}

	/**
	 * Dinh muc KH = KH tieu thu / so ngay ban hang trong thang
	 * 
	 * @return
	 * @author ThuatTQ
	 */
	public Long getDayPlan() {
		if (null == this.dayPlan) {
			if (null == this.getSaleDateInMonth() || this.getSaleDateInMonth() == 0) {
				this.dayPlan =  0l;
			}else{
				this.dayPlan = (long) Math.round((float) this.getMonthPlan()
						/ this.getSaleDateInMonth());
			}
		}

		return this.dayPlan;
	}

	public void setDayPlan(BigDecimal dayPlan) {
		this.dayPlan = dayPlan.longValue();
	}

	/**
	 * Ngay du tru KH = ton / dinh muc ke hoach. Lam tron 2 chu so
	 * 
	 * @return
	 * @author ThuatTQ
	 */
	public Float getDayReservePlan() {
		if (null == this.dayReservePlan&&this.getType()==0) {
			if (null == this.getDayPlan() || this.getDayPlan() == 0) {
				return 0f;
			}else{
				this.dayReservePlan = (float) this.getCloseStockTotal()
						/ this.getDayPlan();
	
				// BigDecimal a = new BigDecimal(this.dayReservePlan);
				// a.setScale(2);
				// this.dayReservePlan = a.floatValue();
	
				this.dayReservePlan = (float) (Math
						.round(this.dayReservePlan * 100.00) / 100.00);
			}
		}

		return this.dayReservePlan;
	}

	public void setDayReservePlan(BigDecimal dayReservePlan) {
		this.dayReservePlan = dayReservePlan.floatValue();
	}

	/**
	 * Ngay du tru thuc te = ton /( luy ke tieu thu 10 ngay gan nhat / 10)
	 * 
	 * @return
	 * @author ThuatTQ
	 */
	public Long getDayReserveReal() {
		/*if(this.getProductCode().equals("04FD41")){
			System.out.print("aa");
		}*/
		/*if (null == this.dayReserveReal&&this.getType()==0) {
			if (null == this.getMonthCumulateTen() || this.getMonthCumulateTen() == 0 || this.getMonthCumulateTen() / 10 == 0) {
				return this.getCloseStockTotal();
			}
			this.dayReserveReal = (long) Math.round((float)(this.getCloseStockTotal())
					/ Math.round((float)this.getMonthCumulateTen() / 10.0));
		}*/
		/*if(this.getProductCode().equals("02BA22")) {
			System.out.println("b45345435b");
		}*/
		if(this.dayReserveReal == null&&this.getType()==0){
			if (Math.round((float)this.getMonthCumulateTen() / 10.0) > 1) {
				float tb = Math.round((float)this.getMonthCumulateTen() / 10.0);
				this.dayReserveReal = (long) Math.round((float)(this.getCloseStockTotal())
						/ tb);
			} else {
				this.dayReserveReal = this.getCloseStockTotal();
			}
		}
		return dayReserveReal;
	}

	public void setDayReserveReal(BigDecimal dayReserveReal) {
		/*if(this.getProductCode().equals("02BA22")) {
			System.out.println("b==============b");
		}*/
		this.dayReserveReal = dayReserveReal.longValue();
	}

	public Long getMin() {
		return min;
	}

	public void setMin(BigDecimal min) {
		this.min = min.longValue();
	}

	public Long getLead() {
		return lead;
	}

	public void setLead(BigDecimal lead) {
		this.lead = lead.longValue();
	}

	public Long getNext() {
		return next;
	}

	public void setNext(BigDecimal next) {
		this.next = next.longValue();
	}

	/**
	 * yeu cau ton = (min + next + lead) * dinh muc KH
	 * 
	 * @return
	 * @author ThuatTQ
	 */
	public Long getRequimentStock() {
		if (this.requimentStock == null) {
			this.requimentStock = (this.getMin() + this.getNext() + this
					.getLead()) * this.getDayPlan();
		}

		return requimentStock;
	}

	public void setRequimentStock(BigDecimal requimentStock) {
		this.requimentStock = requimentStock.longValue();
	}

	public Long getPoCfQty() {
		return poCfQty;
	}

	public void setPoCfQty(BigDecimal poCfQty) {
		this.poCfQty = poCfQty.longValue();
	}

	public Long getPoCsQty() {
		return poCsQty;
	}

	public void setPoCsQty(BigDecimal poCsQty) {
		this.poCsQty = poCsQty.longValue();
	}

	public Long getConvfact() {
		return convfact;
	}

	public void setConvfact(BigDecimal convfact) {
		this.convfact = convfact.longValue();
	}

	/**
	 * so luong thung = ((min + next + lead) * dinh muc KH ton PO DVKH (I)
	 * )/QC
	 * 
	 * @return
	 * @author ThuatTQ
	 */
	public Long getBoxQuantity() {
		if (this.boxQuantity == null) {
			/*if(this.getProductCode().equals("02BA22")) {
				System.out.println("bb");
			}*/
			if (null == this.getConvfact() || this.getConvfact() == 0) {
				return 0l;
			}
			float reqOrder = ((this.getMin()+this.getNext()+ this.getLead())
								*this.getDayPlan()//(float)(Math.round(dayPlan*100.00)/100.00)//this.getDayPlan()
								-this.getCloseStockTotal() 
								- (this.getPoCfQty()>this.getPoCsQty()?this.getPoCfQty():this.getPoCsQty()));
			float quantity = 0f;
			if((this.getIsGroup()==1&&this.getDayReserveReal()>7) //LA MAT HANG CO DINH VA CO DTTT>7
					)
			{
				this.boxQuantity = 0l;
				return boxQuantity;
			} else if ((this.getIsGroup()!=1) && (("D".equals(this.getProductInfoCode())&&this.getDayReserveReal()>5&&this.getIsOrderFull()==1) //la loai D va dttt>5 va dat hang tat ca ngay trong tuan
					|| ("D".equals(this.getProductInfoCode())&&this.getDayReserveReal()>6&&this.getIsOrderFull()!=1) // la loai D va dttt>6 va khong duoc dat hang tat ca ngay trong tuan
					|| (("A".equals(this.getProductInfoCode())||"B".equals(this.getProductInfoCode()))&& this.getDayReserveReal()>15) // la loai A,B va dttt>15
					|| (!("D".equals(this.getProductInfoCode())||"A".equals(this.getProductInfoCode())||"B".equals(this.getProductInfoCode())) && this.getDayReserveReal()>12) // loai khac dttt>12
					)){
				this.boxQuantity = 0l;
				return boxQuantity;
			}
			if ((this.getMonthCumulate()>(this.getMonthPlan()*this.getPercentage()/100))) {
				this.boxQuantity = 0l;
				return boxQuantity;
			}
			if(this.getMonthCumulate() + reqOrder <= this.getMonthPlan() * this.getPercentage()/100){
				quantity = (float)reqOrder/this.getConvfact();
			}else{
				quantity = (float)(this.getMonthPlan()*this.getPercentage()/100-this.getMonthCumulate())/this.getConvfact();
			}
			if(this.isGroup==1 && reqOrder>this.getMonthPlan()*this.getMax()){
				quantity = (float)(this.getMonthPlan()*this.getMax())/this.getConvfact();
			}
			quantity = Math.round(quantity);
			if (quantity > 20) {
				if (quantity % 5 != 0) {
					quantity = (float) (Math.ceil(quantity / 5) * 5);
				}
			}
			this.boxQuantity = (long) Math.round(quantity);
			if(this.boxQuantity < 0){
				this.boxQuantity = 0l;
			}
		}
		
		return boxQuantity;
	}

	public void setBoxQuantity(BigDecimal boxQuantity) {
		this.boxQuantity = boxQuantity.longValue();
	}

	public BigDecimal getAmount() {
		if (null == this.amount) {
			this.amount = this.getPrice()
					.multiply(new BigDecimal(this.getBoxQuantity()))
					.multiply(new BigDecimal(this.getConvfact()));
		}

		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getWarning() {
		/*if(this.getProductCode().equals("02AC13")){
			System.out.print("");
		}*/
		if ((null == this.warning || "".equals(this.warning))&&this.type==0) {
			if (null == this.getRealSaleDateInMonth()
					|| this.getRealSaleDateInMonth() == 0) {
				return " ";
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(toDate);
			Calendar calTen = Calendar.getInstance();
			calTen.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), 10);
			int isDate = cal.compareTo(calTen);
			float reqOrder = ((this.getMin()+this.getNext()+ this.getLead())
					*this.getDayPlan()//(float)(Math.round(dayPlan*100.00)/100.00)
					-this.getCloseStockTotal() 
					- (this.getPoCfQty()>this.getPoCsQty()?this.getPoCfQty():this.getPoCsQty()));///this.getConvfact();
			if((this.getIsGroup()==1&&this.getDayReserveReal()>14)
				||(("A".equals(this.getProductInfoCode())||"B".equals(this.getProductInfoCode()))&&this.getDayReserveReal()>30)	
				||("D".equals(this.getProductInfoCode())&&this.getDayReserveReal()>10)
				||(!("A".equals(this.getProductInfoCode())||"B".equals(this.getProductInfoCode())||"D".equals(this.getProductInfoCode()))&&this.getDayReserveReal()>24)
				||(this.getMonthCumulate()/(this.getRealSaleDateInMonth()*1.00)<this.getDayPlan()*70/100.00
						&& isDate > 0)
					){
				this.warning = "X";
			}else{
				if(getDayReserveReal() > 4 * this.getMin()&&isDate>0){
					this.warning = "QX";
				}else{
					if(Math.round(reqOrder)<=0&&this.getMonthCumulate()>this.getMonthPlan()){
						this.warning = "O";
					}else{
						this.warning = " ";
					}
				}
			}
		}

		return warning;
	}

	public void setWarning(String warning) {
		this.warning = warning;
	}

	@SuppressWarnings("unchecked")
	public static List<RptF1Lv01VO> groupByCat(List<RptF1DataVO> listData)
			throws Exception {
		List<RptF1Lv01VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, "productInfoCode");

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptF1Lv01VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptF1Lv01VO vo = new RptF1Lv01VO();

					List<RptF1DataVO> listObject = (List<RptF1DataVO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setProductInfoName((String) mapEntry.getKey());

						vo.setListData(listObject);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(BigDecimal status) {
		if(null != status){
			this.status = status.intValue();			
		}
		else{
			this.status = null;
		}
	}

	public Long getMonthCumulateTen() {
		return monthCumulateTen;
	}

	public void setMonthCumulateTen(BigDecimal monthCumulateTen) {
		this.monthCumulateTen = monthCumulateTen.longValue();
	}

	public Long getIsOrderFull() {
		return isOrderFull;
	}

	public void setIsOrderFull(BigDecimal isOrderFull) {
		this.isOrderFull = isOrderFull.longValue();
	}

	public Long getIsOrderNotFull() {
		return isOrderNotFull;
	}

	public void setIsOrderNotFull(BigDecimal isOrderNotFull) {
		this.isOrderNotFull = isOrderNotFull.longValue();
	}

	public Long getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(BigDecimal isGroup) {
		this.isGroup = isGroup.longValue();
	}

	public String getProductInfoName() {
		return productInfoName;
	}

	public void setProductInfoName(String productInfoName) {
		this.productInfoName = productInfoName;
	}

	public Long getMax() {
		return max;
	}

	public void setMax(BigDecimal max) {
		this.max = max.longValue();
	}

	public BigDecimal getAmountTotal() {
		return amountTotal;
	}

	public void setAmountTotal(BigDecimal amountTotal) {
		this.amountTotal = amountTotal;
	}
	
	
	
}

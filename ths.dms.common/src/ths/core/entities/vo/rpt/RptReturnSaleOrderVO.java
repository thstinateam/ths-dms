package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

public class RptReturnSaleOrderVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long soId;
	private Long sodId;
	private Date orderDate;
	private String orderNumber;
	private String rootOrderNumber;
	private BigDecimal amount;
	private BigDecimal total;
	private String staffCode;
	private String deliveryCode;
	private String staffName;
	private String deliveryName;
	private Integer isFreeItem;
	private Integer quantity;//so luong
	private BigDecimal detailAmount;//thanh tien
	private BigDecimal discountAmount;//chiet khau tien
	private Float discountPercent;
	private BigDecimal priceValue;//don gia
	private String productCode;//ma san pham
	private String productName;//ten san pham
	private Integer convfact;//cach qui doi
	private String customerCode;
	private String shortCode;
	private String customerName;
	private String customerAddress;
	private String phone;
	private String mobiphone;
	private String promotionProgramCode;
	private String promotionProgramname;
	private String invoiceNumber;
	private Integer numThung=0;//thung
	private Integer numLe=0;//le
	
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public Integer getIsFreeItem() {
		return isFreeItem;
	}
	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getRootOrderNumber() {
		return rootOrderNumber;
	}
	public void setRootOrderNumber(String rootOrderNumber) {
		this.rootOrderNumber = rootOrderNumber;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public BigDecimal getDetailAmount() {
		return detailAmount;
	}
	public void setDetailAmount(BigDecimal detailAmount) {
		this.detailAmount = detailAmount;
	}
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Float getDiscountPercent() {
		return discountPercent;
	}
	public void setDiscountPercent(Float discountPercent) {
		this.discountPercent = discountPercent;
	}
	public BigDecimal getPriceValue() {
		return priceValue;
	}
	public void setPriceValue(BigDecimal priceValue) {
		this.priceValue = priceValue;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public Long getSoId() {
		return soId;
	}
	public void setSoId(Long soId) {
		this.soId = soId;
	}
	public Long getSodId() {
		return sodId;
	}
	public void setSodId(Long sodId) {
		this.sodId = sodId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobiphone() {
		return mobiphone;
	}
	public void setMobiphone(String mobiphone) {
		this.mobiphone = mobiphone;
	}
	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}
	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}
	public String getPromotionProgramname() {
		return promotionProgramname;
	}
	public void setPromotionProgramname(String promotionProgramname) {
		this.promotionProgramname = promotionProgramname;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Integer getNumThung() {
		return numThung;
	}
	public void setNumThung(Integer numThung) {
		this.numThung = numThung;
	}
	public Integer getNumLe() {
		return numLe;
	}
	public void setNumLe(Integer numLe) {
		this.numLe = numLe;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	
}

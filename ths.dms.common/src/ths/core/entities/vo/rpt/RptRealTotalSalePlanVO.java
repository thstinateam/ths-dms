package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptRealTotalSalePlanVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long productId;
	private Integer planQuantity;
	private BigDecimal planAmount;
	private String productCode;
	private String productName;
	private Integer isFreeItem;
	private Integer realQuantity;
	private BigDecimal realAmount;
	private Float priceValue;
	private String staffCode;
	private String staffName;
	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	/**
	 * @return the planQuantity
	 */
	public Integer getPlanQuantity() {
		return planQuantity;
	}
	/**
	 * @param planQuantity the planQuantity to set
	 */
	public void setPlanQuantity(Integer planQuantity) {
		this.planQuantity = planQuantity;
	}
	/**
	 * @return the planAmount
	 */
	public BigDecimal getPlanAmount() {
		return planAmount;
	}
	/**
	 * @param planAmount the planAmount to set
	 */
	public void setPlanAmount(BigDecimal planAmount) {
		this.planAmount = planAmount;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the isFreeItem
	 */
	public Integer getIsFreeItem() {
		return isFreeItem;
	}
	/**
	 * @param isFreeItem the isFreeItem to set
	 */
	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}
	/**
	 * @return the realQuantity
	 */
	public Integer getRealQuantity() {
		return realQuantity;
	}
	/**
	 * @param realQuantity the realQuantity to set
	 */
	public void setRealQuantity(Integer realQuantity) {
		this.realQuantity = realQuantity;
	}
	/**
	 * @return the realAmount
	 */
	public BigDecimal getRealAmount() {
		return realAmount;
	}
	/**
	 * @param realAmount the realAmount to set
	 */
	public void setRealAmount(BigDecimal realAmount) {
		this.realAmount = realAmount;
	}
	/**
	 * @return the priceValue
	 */
	public Float getPriceValue() {
		return priceValue;
	}
	/**
	 * @param priceValue the priceValue to set
	 */
	public void setPriceValue(Float priceValue) {
		this.priceValue = priceValue;
	}
	/**
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}
	/**
	 * @param staffCode the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	/**
	 * @return the staffName
	 */
	public String getStaffName() {
		return staffName;
	}
	/**
	 * @param staffName the staffName to set
	 */
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
}

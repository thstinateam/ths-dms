package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.httpclient.util.DateUtil;

public class RptPoCfrmFromDvkh2VO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String saleOrderNumber="";
	private String dvkhProductCode="";
	private Integer dvkhQty=0;
	private BigDecimal price=BigDecimal.ZERO;
	private Integer deliveryQty=0;
	private Date importDate;
	private String strImportDate="";
	private String importProductCode="";
	private Integer importQty=0;
	private Integer waitQty=0;

	public String getSaleOrderNumber() {
		return saleOrderNumber;
	}

	public void setSaleOrderNumber(String saleOrderNumber) {
		this.saleOrderNumber = saleOrderNumber;
	}

	public String getDvkhProductCode() {
		return dvkhProductCode;
	}

	public void setDvkhProductCode(String dvkhProductCode) {
		this.dvkhProductCode = dvkhProductCode;
	}

	public Integer getDvkhQty() {
		return dvkhQty;
	}

	public void setDvkhQty(Integer dvkhQty) {
		this.dvkhQty = dvkhQty;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getDeliveryQty() {
		return deliveryQty;
	}

	public void setDeliveryQty(Integer deliveryQty) {
		this.deliveryQty = deliveryQty;
	}

	public Date getImportDate() {
		return importDate;
	}

	public void setImportDate(Date importDate) {
		this.importDate = importDate;
		if(importDate!=null){
			this.strImportDate = DateUtil.formatDate(this.importDate, "dd/MM/yyyy");
		}
	}

	public String getImportProductCode() {
		return importProductCode;
	}

	public void setImportProductCode(String importProductCode) {
		if(importProductCode!=null && importProductCode!=""){
			this.importProductCode = importProductCode;
		}else{
			this.importProductCode = "";
		}
	}

	public Integer getImportQty() {
		return importQty;
	}

	public void setImportQty(Integer importQty) {
		this.importQty = importQty;
	}

	public Integer getWaitQty() {
		return waitQty;
	}

	public void setWaitQty(Integer waitQty) {
		this.waitQty = waitQty;
	}

	public String getStrImportDate() {
		return strImportDate;
	}

	public void setStrImportDate(String strImportDate) {
		this.strImportDate = strImportDate;
	}
}

/*
 * Copyright 2016 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


/**
 * Bao cao 3.1.1.10 [WV-KD-03-F12] BÁO CÁO TỔNG HỢP TỦ ĐÔNG - TỦ MÁT KỲ
 * 
 * @author tamvnm
 * @since May 55,2015
 */
public class Rpt_3_1_1_8_BCTDTMSC_F10_VO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long equipRepairId;
	private String mien;
	private String vung;
	private String repairCode;
	private String createFormDate;
	private String shopCode;
	private String shopName;
	private String staffCode;
	private String staffName;
	private String shortCode;
	private String customerCode;
	private String customerName;
	private String groupName;
	private String categoryName;
	private String brandName;
	private String capacity;
	private Integer manufacturingYear;
	private String firstDayInUse;
	private String equipCode;
	private String serialNumber;
	private String itemName;
	private String note;
	private BigDecimal repairTotalAmount;
	private Integer repairCount;
	private String repairedInMonth;
	private String repairPayCode;
	private String repairPayDate;
	private BigDecimal repairPayTotalAmuont;
	private Long equipId;
	private Long shopId;
	private String repairPayCreateDate;
	private Integer stt;
	private BigDecimal materialTotalAmount;
	private List<DynamicVO> lstDynamic;
	
	public Long getEquipRepairId() {
		return equipRepairId;
	}

	public void setEquipRepairId(Long equipRepairId) {
		this.equipRepairId = equipRepairId;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getRepairCode() {
		return repairCode;
	}

	public void setRepairCode(String repairCode) {
		this.repairCode = repairCode;
	}

	public String getCreateFormDate() {
		return createFormDate;
	}

	public void setCreateFormDate(String createFormDate) {
		this.createFormDate = createFormDate;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public Integer getManufacturingYear() {
		return manufacturingYear;
	}

	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}

	public String getFirstDayInUse() {
		return firstDayInUse;
	}

	public void setFirstDayInUse(String firstDayInUse) {
		this.firstDayInUse = firstDayInUse;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public BigDecimal getRepairTotalAmount() {
		return repairTotalAmount;
	}

	public void setRepairTotalAmount(BigDecimal repairTotalAmount) {
		this.repairTotalAmount = repairTotalAmount;
	}

	public Integer getRepairCount() {
		return repairCount;
	}

	public void setRepairCount(Integer repairCount) {
		this.repairCount = repairCount;
	}

	public String getRepairedInMonth() {
		return repairedInMonth;
	}

	public void setRepairedInMonth(String repairedInMonth) {
		this.repairedInMonth = repairedInMonth;
	}

	public String getRepairPayCode() {
		return repairPayCode;
	}

	public void setRepairPayCode(String repairPayCode) {
		this.repairPayCode = repairPayCode;
	}

	public String getRepairPayDate() {
		return repairPayDate;
	}

	public void setRepairPayDate(String repairPayDate) {
		this.repairPayDate = repairPayDate;
	}

	public BigDecimal getRepairPayTotalAmuont() {
		return repairPayTotalAmuont;
	}

	public void setRepairPayTotalAmuont(BigDecimal repairPayTotalAmuont) {
		this.repairPayTotalAmuont = repairPayTotalAmuont;
	}

	public Long getEquipId() {
		return equipId;
	}

	public void setEquipId(Long equipId) {
		this.equipId = equipId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public List<DynamicVO> getLstDynamic() {
		return lstDynamic;
	}

	public void setLstDynamic(List<DynamicVO> lstDynamic) {
		this.lstDynamic = lstDynamic;
	}

	public String getRepairPayCreateDate() {
		return repairPayCreateDate;
	}

	public void setRepairPayCreateDate(String repairPayCreateDate) {
		this.repairPayCreateDate = repairPayCreateDate;
	}

	public Integer getStt() {
		return stt;
	}

	public void setStt(Integer stt) {
		this.stt = stt;
	}

	public BigDecimal getMaterialTotalAmount() {
		return materialTotalAmount;
	}

	public void setMaterialTotalAmount(BigDecimal materialTotalAmount) {
		this.materialTotalAmount = materialTotalAmount;
	}

	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCTGBHHQCNVBH_DS3_VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String maMien;
	private String maVung;
	private String maNPP;
	private String tenNPP;

	private String maGSNPP;
	private String tenGSNPP;
	private String maNVBH;
	private String tenNVBH;

	private Long soKHKH;
	private Long soKHTH;
	private Integer soHD;
	private Long dsTH;
	private String tg;

	public String getMaMien() {
		if(maMien!=null){
			return maMien;
		}
		return "";
	}

	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}

	public String getMaVung() {
		if(maVung!=null){
			return maVung;
		}
		return "";
	}

	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}

	public String getMaNPP() {
		if(maNPP!=null){
			return maNPP;
		}
		return "";
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getTenNPP() {
		if(tenNPP!=null){
			return tenNPP;
		}
		return "";
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}

	public String getMaGSNPP() {
		if(maGSNPP!=null){
			return maGSNPP;
		}
		return "";
	}

	public void setMaGSNPP(String maGSNPP) {
		this.maGSNPP = maGSNPP;
	}

	public String getTenGSNPP() {
		if(tenGSNPP!=null){
			return tenGSNPP;
		}
		return "";
	}

	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}

	public String getMaNVBH() {
		if(maNVBH!=null){
			return maNVBH;
		}
		return "";
	}

	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}

	public String getTenNVBH() {
		if(tenNVBH!=null){
			return tenNVBH;
		}
		return "";
	}

	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}

	public Long getSoKHKH() {
		if(soKHKH!=null){
			return soKHKH;
		}
		return 0l;
	}

	public void setSoKHKH(Long soKHKH) {
		this.soKHKH = soKHKH;
	}

	public Long getSoKHTH() {
		if(soKHTH!=null){
			return soKHTH;
		}
		return 0l;
	}

	public void setSoKHTH(Long soKHTH) {
		this.soKHTH = soKHTH;
	}

	public Integer getSoHD() {
		if(soHD!=null){
			return soHD;
		}
		return 0;
	}

	public void setSoHD(Integer soHD) {
		this.soHD = soHD;
	}

	public Long getDsTH() {
		if(dsTH!=null){
			return dsTH;
		}
		return 0l;
	}

	public void setDsTH(Long dsTH) {
		this.dsTH = dsTH;
	}

	public String getTg() {
		return tg;
	}

	public void setTg(String tg) {
		this.tg = tg;
	}

	public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
		} 
	}

}

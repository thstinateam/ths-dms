package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptReturnSaleOrderAllowDeliveryVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
//	private Date orderDate;
	private BigDecimal sumDiscountAmount;
	private BigDecimal amount;
	private BigDecimal discountAmount;
//	private BigDecimal discountPercent;
	private BigDecimal promotionAmount;
	private BigDecimal price;
	private String deliveryCode;
	private String deliveryName;
	private String productCode;
	private String productName;
	private BigDecimal quantity;
	private BigDecimal promotionQuantity;
	private BigDecimal convfact;
	private String lot;
	
	public void safeSetNull() throws IllegalArgumentException, IllegalAccessException{
		for(Field field:getClass().getDeclaredFields()) {
			if(field.getType().equals(Integer.class) && field.get(this) == null) {
				field.set(this, 0);
			}
			if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
				field.set(this, BigDecimal.ZERO);
			}
			if(field.getType().equals(String.class) && field.get(this) == null) {
				field.set(this, "");
			}
		}
	}
	/**
	 * @return the orderDate
	 */
//	public Date getOrderDate() {
//		return orderDate;
//	}
	/**
	 * @param orderDate the orderDate to set
	 */
//	public void setOrderDate(Date orderDate) {
//		this.orderDate = orderDate;
//	}
	/**
	 * @return the discountAmount
	 */
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}
	/**
	 * @param discountAmount the discountAmount to set
	 */
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}
	/**
	 * @return the discountPercent
	 */
//	public BigDecimal getDiscountPercent() {
//		return discountPercent;
//	}
	/**
	 * @param discountPercent the discountPercent to set
	 */
//	public void setDiscountPercent(BigDecimal discountPercent) {
//		this.discountPercent = discountPercent;
//	}
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the lot
	 */
	public String getLot() {
		return lot;
	}
	/**
	 * @param lot the lot to set
	 */
	public void setLot(String lot) {
		this.lot = lot;
	}
	/**
	 * @return the totalQuantity
	 */
	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getPromotionQuantity() {
		return promotionQuantity;
	}
	public void setPromotionQuantity(BigDecimal promotionQuantity) {
		this.promotionQuantity = promotionQuantity;
	}
	public BigDecimal getConvfact() {
		return convfact;
	}
	public void setConvfact(BigDecimal convfact) {
		this.convfact = convfact;
	}
	/**
	 * @return the sumDiscountAmount
	 */
	public BigDecimal getSumDiscountAmount() {
		return sumDiscountAmount;
	}
	/**
	 * @param sumDiscountAmount the sumDiscountAmount to set
	 */
	public void setSumDiscountAmount(BigDecimal sumDiscountAmount) {
		this.sumDiscountAmount = sumDiscountAmount;
	}
	public BigDecimal getPromotionAmount() {
		return promotionAmount;
	}
	public void setPromotionAmount(BigDecimal promotionAmount) {
		this.promotionAmount = promotionAmount;
	}
	public Integer getThung() {
		return quantity.intValue() / convfact.intValue();
	}
	public Integer getLe() {
		return quantity.intValue() % convfact.intValue();
	}
	public String getThungLe() {
		Integer thung = quantity.intValue() / convfact.intValue();
		Integer le = quantity.intValue() % convfact.intValue();
		return thung.toString() + "/" + le.toString();
	}
	public Integer getThungPromo() {
		return promotionQuantity.intValue() / convfact.intValue();
	}
	public Integer getLePromo() {
		return promotionQuantity.intValue() % convfact.intValue();
	}
	public String getThungLePromo() {
		Integer thung = promotionQuantity.intValue() / convfact.intValue();
		Integer le = promotionQuantity.intValue() % convfact.intValue();
		return thung.toString() + "/" + le.toString();
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD16DataVO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String mien;
	private String tinh;
	private String huyen;
	private String phuongXa;
	private String nhaPP;
	private Integer psThangTruoc;
	private Integer psThangChon;
	private Integer xaMatDi;
	private Integer xaPSDS;

	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getTinh() {
		return tinh;
	}

	public void setTinh(String tinh) {
		this.tinh = tinh;
	}

	public String getHuyen() {
		return huyen;
	}

	public void setHuyen(String huyen) {
		this.huyen = huyen;
	}

	public String getPhuongXa() {
		return phuongXa;
	}

	public void setPhuongXa(String phuongXa) {
		this.phuongXa = phuongXa;
	}

	public String getNhaPP() {
		return nhaPP;
	}

	public void setNhaPP(String nhaPP) {
		this.nhaPP = nhaPP;
	}

	public Integer getPsThangTruoc() {
		return psThangTruoc;
	}

	public void setPsThangTruoc(BigDecimal psThangTruoc) {
		if (null != psThangTruoc) {
			this.psThangTruoc = psThangTruoc.intValue();
		} else {
			this.psThangTruoc = 0;
		}
	}

	public Integer getPsThangChon() {
		return psThangChon;
	}

	public void setPsThangChon(BigDecimal psThangChon) {
		if (null != psThangChon) {
			this.psThangChon = psThangChon.intValue();
		} else {
			this.psThangChon = 0;
		}
	}

	public Integer getXaMatDi() {
		return xaMatDi;
	}

	public void setXaMatDi(BigDecimal xaMatDi) {
		if (null != xaMatDi) {
			this.xaMatDi = xaMatDi.intValue();
		} else {
			this.xaMatDi = 0;
		}
	}

	public Integer getXaPSDS() {
		return xaPSDS;
	}

	public void setXaPSDS(BigDecimal xaPSDS) {
		if (null != xaPSDS) {
			this.xaPSDS = xaPSDS.intValue();
		} else {
			this.xaPSDS = 0;
		}
	}
}

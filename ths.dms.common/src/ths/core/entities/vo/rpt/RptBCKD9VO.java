package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCKD9VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mien;
	private String vung;
	private String maNpp;
	private String itemNo;
	private BigDecimal tonDauKy1;
	private BigDecimal totalNhap; 
	private BigDecimal totalXuat;
	private BigDecimal cuoiKy1;
	private BigDecimal giaTriCuoiKy1; 
	private BigDecimal soLuongBan;
	private BigDecimal giaTriBan; 
	private BigDecimal soLuongConLai;
	
	 public void safeSetNull() throws Exception{
			try {
				for(Field field:getClass().getDeclaredFields()) {
					if(field.getType().equals(Integer.class) && field.get(this) == null) {
						field.set(this, 0);
					}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
						field.set(this, BigDecimal.ZERO);
					}else if(field.getType().equals(Float.class) && field.get(this) == null) {
						field.set(this, 0f);
					}else if(field.getType().equals(Double.class) && field.get(this) == null) {
						field.set(this, 0d);
					}else if(field.getType().equals(Long.class) && field.get(this) == null) {
						field.set(this, 0l);
					}else if(field.getType().equals(String.class) && field.get(this) == null) {
						field.set(this, "");
					}
				}
			} catch (Exception e) {
				throw e;
			} 
		}
	
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getMaNpp() {
		return maNpp;
	}
	public void setMaNpp(String maNpp) {
		this.maNpp = maNpp;
	}
	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}
	public BigDecimal getTonDauKy1() {
		return tonDauKy1;
	}
	public void setTonDauKy1(BigDecimal tonDauKy1) {
		this.tonDauKy1 = tonDauKy1;
	}
	public BigDecimal getTotalNhap() {
		return totalNhap;
	}
	public void setTotalNhap(BigDecimal totalNhap) {
		this.totalNhap = totalNhap;
	}
	public BigDecimal getTotalXuat() {
		return totalXuat;
	}
	public void setTotalXuat(BigDecimal totalXuat) {
		this.totalXuat = totalXuat;
	}
	public BigDecimal getCuoiKy1() {
		return cuoiKy1;
	}
	public void setCuoiKy1(BigDecimal cuoiKy1) {
		this.cuoiKy1 = cuoiKy1;
	}
	public BigDecimal getGiaTriCuoiKy1() {
		return giaTriCuoiKy1;
	}
	public void setGiaTriCuoiKy1(BigDecimal giaTriCuoiKy1) {
		this.giaTriCuoiKy1 = giaTriCuoiKy1;
	}
	public BigDecimal getSoLuongBan() {
		return soLuongBan;
	}
	public void setSoLuongBan(BigDecimal soLuongBan) {
		this.soLuongBan = soLuongBan;
	}
	public BigDecimal getGiaTriBan() {
		return giaTriBan;
	}
	public void setGiaTriBan(BigDecimal giaTriBan) {
		this.giaTriBan = giaTriBan;
	}
	public BigDecimal getSoLuongConLai() {
		return soLuongConLai;
	}
	public void setSoLuongConLai(BigDecimal soLuongConLai) {
		this.soLuongConLai = soLuongConLai;
	}
}

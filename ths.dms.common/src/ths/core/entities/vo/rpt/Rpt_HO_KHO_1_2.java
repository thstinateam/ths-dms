package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Rpt_HO_KHO_1_2 implements Serializable{
	private static final long serialVersionUID = 1L;

	private String area; 							// vung
	private String location; 						// mien
	private String shopCode; 						// ma npp
	private String shopName; 						// ten npp
	private String productCode; 					// ma san pham
	private String productName; 					// ten san pham
	private String catName; 						// nhom san pham

	private Long sumOpenStockTotal; 				// ton dau
	private BigDecimal sumOpenStockTotalAmount; 	// gia tri ton dau
	private Long sumExportSale; 					// hang xuat ban
	private Long sumExportPromotion;				// hang xuat khuyen mai
	private Long sumImportSale;						// tra hang ban
	private Long sumImportPromotion;				// tra hang khuyen mai
	private Long sumImportVNM; 						// nhap tu VNM
	private Long sumExportVNM; 						// xuat tra VNM
	private Long sumImportTotal;					// tong nham
	private Long sumExportTotal;					// tong xuat
	private Long sumCloseStockTotal; 				// ton cuoi
	private BigDecimal sumCloseStockTotalAmount; 	// gia tri ton cuoi
	
	private List<DynamicVO> lstValue = new ArrayList<DynamicVO>();
	
	/*public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}*/

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public Long getSumOpenStockTotal() {
		return sumOpenStockTotal;
	}

	public void setSumOpenStockTotal(Long sumOpenStockTotal) {
		this.sumOpenStockTotal = sumOpenStockTotal;
	}

	public BigDecimal getSumOpenStockTotalAmount() {
		return sumOpenStockTotalAmount;
	}

	public void setSumOpenStockTotalAmount(BigDecimal sumOpenStockTotalAmount) {
		this.sumOpenStockTotalAmount = sumOpenStockTotalAmount;
	}

	public Long getSumExportSale() {
		return sumExportSale;
	}

	public void setSumExportSale(Long sumExportSale) {
		this.sumExportSale = sumExportSale;
	}

	public Long getSumExportPromotion() {
		return sumExportPromotion;
	}

	public void setSumExportPromotion(Long sumExportPromotion) {
		this.sumExportPromotion = sumExportPromotion;
	}

	public Long getSumImportSale() {
		return sumImportSale;
	}

	public void setSumImportSale(Long sumImportSale) {
		this.sumImportSale = sumImportSale;
	}

	public Long getSumImportPromotion() {
		return sumImportPromotion;
	}

	public void setSumImportPromotion(Long sumImportPromotion) {
		this.sumImportPromotion = sumImportPromotion;
	}

	public Long getSumImportVNM() {
		return sumImportVNM;
	}

	public void setSumImportVNM(Long sumImportVNM) {
		this.sumImportVNM = sumImportVNM;
	}

	public Long getSumExportVNM() {
		return sumExportVNM;
	}

	public void setSumExportVNM(Long sumExportVNM) {
		this.sumExportVNM = sumExportVNM;
	}

	public Long getSumImportTotal() {
		return sumImportTotal;
	}

	public void setSumImportTotal(Long sumImportTotal) {
		this.sumImportTotal = sumImportTotal;
	}

	public Long getSumExportTotal() {
		return sumExportTotal;
	}

	public void setSumExportTotal(Long sumExportTotal) {
		this.sumExportTotal = sumExportTotal;
	}

	public Long getSumCloseStockTotal() {
		return sumCloseStockTotal;
	}

	public void setSumCloseStockTotal(Long sumCloseStockTotal) {
		this.sumCloseStockTotal = sumCloseStockTotal;
	}

	public BigDecimal getSumCloseStockTotalAmount() {
		return sumCloseStockTotalAmount;
	}

	public void setSumCloseStockTotalAmount(BigDecimal sumCloseStockTotalAmount) {
		this.sumCloseStockTotalAmount = sumCloseStockTotalAmount;
	}

	public List<DynamicVO> getLstValue() {
		return lstValue;
	}

	public void setLstValue(List<DynamicVO> lstValue) {
		this.lstValue = lstValue;
	}
}

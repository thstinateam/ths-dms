package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptDoanhSoBanHangNVBHSKUNgayDataVO implements Serializable {
	private static final long serialVersionUID = 1L;

	String maNVBH;
	String tenNVBH;
	String mien;
	String vung;
	String tbhv;
	String maNPP;
	String maGSNPP;
	String doiBanHang;
	String skus;
	BigDecimal kehoachTT;
	Integer khachHangThangTruoc;
	Integer khachHangThangNay;
	BigDecimal doanhSoThangTruoc;
	BigDecimal doanhSoThangNay;
	BigDecimal luyKeKeHoach;
	BigDecimal phanTramThucHienKH;
	BigDecimal thieu;
	BigDecimal doanhSoBQPTHNgay;

	public RptDoanhSoBanHangNVBHSKUNgayDataVO() {
	}

	public String getMaNVBH() {
		return maNVBH;
	}

	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}

	public String getTenNVBH() {
		return tenNVBH;
	}

	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}

	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getMaGSNPP() {
		return maGSNPP;
	}

	public void setMaGSNPP(String maGSNPP) {
		this.maGSNPP = maGSNPP;
	}

	public String getDoiBanHang() {
		return doiBanHang;
	}

	public void setDoiBanHang(String doiBanHang) {
		this.doiBanHang = doiBanHang;
	}

	public String getSkus() {
		return skus;
	}

	public void setSkus(String skus) {
		this.skus = skus;
	}

	public BigDecimal getKehoachTT() {
		return kehoachTT;
	}

	public void setKehoachTT(BigDecimal kehoachTT) {
		this.kehoachTT = kehoachTT;
	}

	public Integer getKhachHangThangTruoc() {
		return khachHangThangTruoc;
	}

	public void setKhachHangThangTruoc(BigDecimal khachHangThangTruoc) {
		if (null != khachHangThangTruoc) {
			this.khachHangThangTruoc = khachHangThangTruoc.intValue();
		} else {
			this.khachHangThangTruoc = 0;
		}
	}

	public Integer getKhachHangThangNay() {
		return khachHangThangNay;
	}

	public void setKhachHangThangNay(BigDecimal khachHangThangNay) {
		if (null != khachHangThangNay) {
			this.khachHangThangNay = khachHangThangNay.intValue();
		} else {
			this.khachHangThangNay = 0;
		}
	}

	public BigDecimal getDoanhSoThangTruoc() {
		return doanhSoThangTruoc;
	}

	public void setDoanhSoThangTruoc(BigDecimal doanhSoThangTruoc) {
		this.doanhSoThangTruoc = doanhSoThangTruoc;
	}

	public BigDecimal getDoanhSoThangNay() {
		return doanhSoThangNay;
	}

	public void setDoanhSoThangNay(BigDecimal doanhSoThangNay) {
		this.doanhSoThangNay = doanhSoThangNay;
	}

	public BigDecimal getLuyKeKeHoach() {
		return luyKeKeHoach;
	}

	public void setLuyKeKeHoach(BigDecimal luyKeKeHoach) {
		this.luyKeKeHoach = luyKeKeHoach;
	}

	public BigDecimal getPhanTramThucHienKH() {
		return this.phanTramThucHienKH;
	}

	public void setPhanTramThucHienKH(BigDecimal phanTramThucHienKH) {
		this.phanTramThucHienKH = phanTramThucHienKH;
	}

	public BigDecimal getThieu() {
		return thieu;
	}

	public void setThieu(BigDecimal thieu) {
		this.thieu = thieu;
	}

	public BigDecimal getDoanhSoBQPTHNgay() {
		return doanhSoBQPTHNgay;
	}

	public void setDoanhSoBQPTHNgay(BigDecimal doanhSoBQPTHNgay) {
		this.doanhSoBQPTHNgay = doanhSoBQPTHNgay;
	}

	public String getTbhv() {
		return tbhv;
	}

	public void setTbhv(String tbhv) {
		this.tbhv = tbhv;
	}
}
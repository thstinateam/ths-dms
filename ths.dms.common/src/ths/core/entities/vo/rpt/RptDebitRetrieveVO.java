package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptDebitRetrieveVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String shopRegionCode;
	private String shopAreaCode;
	private String shopCode;
	private String shopName;
	private String customerCode;
	private String customerName;
	private String billCode;
	private String billDate;
	private String billType;
	private String reason;
	private BigDecimal debitAmount;
	private BigDecimal discountAmount;
	private BigDecimal payedAmount;
	private BigDecimal remainAmount;

	public String getShopRegionCode() {
		return shopRegionCode;
	}

	public void setShopRegionCode(String shopRegionCode) {
		this.shopRegionCode = shopRegionCode;
	}

	public String getShopAreaCode() {
		return shopAreaCode;
	}

	public void setShopAreaCode(String shopAreaCode) {
		this.shopAreaCode = shopAreaCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getBillCode() {
		return billCode;
	}

	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getPayedAmount() {
		return payedAmount;
	}

	public void setPayedAmount(BigDecimal payedAmount) {
		this.payedAmount = payedAmount;
	}

	public BigDecimal getRemainAmount() {
		return remainAmount;
	}

	public void setRemainAmount(BigDecimal remainAmount) {
		this.remainAmount = remainAmount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}

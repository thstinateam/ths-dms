package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptBCCTKMVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long shopId;
	private String shopName;
	private String shopAddress;
	private List<RptBCCTKMDetailVO> lstBCCTKMDetailVO = new ArrayList<RptBCCTKMDetailVO>();
	
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public List<RptBCCTKMDetailVO> getLstBCCTKMDetailVO() {
		return lstBCCTKMDetailVO;
	}
	public void setLstBCCTKMDetailVO(List<RptBCCTKMDetailVO> lstBCCTKMDetailVO) {
		this.lstBCCTKMDetailVO = lstBCCTKMDetailVO;
	}
	
	
}

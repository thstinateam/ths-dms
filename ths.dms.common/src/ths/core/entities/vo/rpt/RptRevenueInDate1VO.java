package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RptRevenueInDate1VO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Date orderDate;
	
	private List<RptRevenueInDateVO> lstRptRevenueInDateVO;

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public List<RptRevenueInDateVO> getLstRptRevenueInDateVO() {
		return lstRptRevenueInDateVO;
	}

	public void setLstRptRevenueInDateVO(
			List<RptRevenueInDateVO> lstRptRevenueInDateVO) {
		this.lstRptRevenueInDateVO = lstRptRevenueInDateVO;
	}
	
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ths.dms.core.entities.vo.SaleOrderNumberVO;

public class RptParentReturnSaleOrderAllowDeliveryVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String deliveryCode;
	private String deliveryName;
	private List<SaleOrderNumberVO> listSaleOrderNumber;
	private String lstSaleOrderNumberString;
	private BigDecimal sumAmount;
	private BigDecimal sumDiscountAmount;
	private BigDecimal sumPromotionAmount;
	private String sumQuantity;
	private String sumPromotionQuantity;
//	private List<RptReturnSaleOrderAllowDeliveryVO> listVO = new ArrayList<RptReturnSaleOrderAllowDeliveryVO>();
	private List<RptReturnSaleOrderAllowDeliveryProductVO> listProductVO = new ArrayList<RptReturnSaleOrderAllowDeliveryProductVO>();
	
	public void safeSetNull() throws IllegalArgumentException, IllegalAccessException{
		for(Field field:getClass().getDeclaredFields()) {
			if(field.getType().equals(Integer.class) && field.get(this) == null) {
				field.set(this, 0);
			}
			if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
				field.set(this, BigDecimal.ZERO);
			}
			if(field.getType().equals(String.class) && field.get(this) == null) {
				field.set(this, "");
			}
		}
	}
	/**
	 * @return the isFreeItem
	 */
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
//	public List<RptReturnSaleOrderAllowDeliveryVO> getListVO() {
//		return listVO;
//	}
//	public void setListVO(List<RptReturnSaleOrderAllowDeliveryVO> listVO) {
//		this.listVO = listVO;
//	}
	public List<RptReturnSaleOrderAllowDeliveryProductVO> getListProductVO() {
		return listProductVO;
	}
	public void setListProductVO(List<RptReturnSaleOrderAllowDeliveryProductVO> listProductVO) {
		this.listProductVO = listProductVO;
	}
	public BigDecimal getSumAmount() {
		return sumAmount;
	}
	public void setSumAmount(BigDecimal sumAmount) {
		this.sumAmount = sumAmount;
	}
	public BigDecimal getSumDiscountAmount() {
		return sumDiscountAmount;
	}
	public void setSumDiscountAmount(BigDecimal sumDiscountAmount) {
		this.sumDiscountAmount = sumDiscountAmount;
	}
	public List<SaleOrderNumberVO> getListSaleOrderNumber() {
		return listSaleOrderNumber;
	}
	public void setListSaleOrderNumber(List<SaleOrderNumberVO> listSaleOrderNumber) {
		this.listSaleOrderNumber = listSaleOrderNumber;
	}
	public String getLstSaleOrderNumberString() {
		return lstSaleOrderNumberString;
	}
	public void setLstSaleOrderNumberString(String lstSaleOrderNumberString) {
		this.lstSaleOrderNumberString = lstSaleOrderNumberString;
	}
	public BigDecimal getSumPromotionAmount() {
		return sumPromotionAmount;
	}
	public void setSumPromotionAmount(BigDecimal sumPromotionAmount) {
		this.sumPromotionAmount = sumPromotionAmount;
	}
	public String getSumQuantity() {
		return sumQuantity;
	}
	public void setSumQuantity(String sumQuantity) {
		this.sumQuantity = sumQuantity;
	}
	public String getSumPromotionQuantity() {
		return sumPromotionQuantity;
	}
	public void setSumPromotionQuantity(String sumPromotionQuantity) {
		this.sumPromotionQuantity = sumPromotionQuantity;
	}
}

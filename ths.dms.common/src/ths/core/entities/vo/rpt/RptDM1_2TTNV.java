package ths.core.entities.vo.rpt;

import java.io.Serializable;

/**
 * @author sangtn
 * @since Mar 21, 2014
 * @description VO - Xuat bao cao danh muc KM1.2
 */
public class RptDM1_2TTNV implements Serializable {

	private static final long serialVersionUID = 1L;

	private String mien;
	private String vung;
	private String maNPP;
	private String tenNPP;
	private String maNVBH;
	private String tenNVBH;
	private Integer status;
	private String workState;
	
	public String getMien() {
		return mien;
	}
	
	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getMaNVBH() {
		return maNVBH;
	}

	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}

	public String getTenNVBH() {
		return tenNVBH;
	}

	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}

	public String getWorkState() {
		return workState;
	}

	public void setWorkState(String workState) {
		this.workState = workState;
	}

	public String getTenNPP() {
		return tenNPP;
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
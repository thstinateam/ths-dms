package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;

public class RptCountStoreVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String productCode;
	private String productName;
	private String shopName;
	private String address;
	private Date createDate;
	private String lot;
	private Integer quantityCounted;
	private Integer stockCardNumber;
	private Integer convfact;
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}
	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return the lot
	 */
	public String getLot() {
		return lot;
	}
	/**
	 * @param lot the lot to set
	 */
	public void setLot(String lot) {
		this.lot = lot;
	}
	/**
	 * @return the quantityCounted
	 */
	public Integer getQuantityCounted() {
		return quantityCounted;
	}
	/**
	 * @param quantityCounted the quantityCounted to set
	 */
	public void setQuantityCounted(Integer quantityCounted) {
		this.quantityCounted = quantityCounted;
	}
	/**
	 * @return the stockCardNumber
	 */
	public Integer getStockCardNumber() {
		return stockCardNumber;
	}
	/**
	 * @param stockCardNumber the stockCardNumber to set
	 */
	public void setStockCardNumber(Integer stockCardNumber) {
		this.stockCardNumber = stockCardNumber;
	}
	/**
	 * @return the convfact
	 */
	public Integer getConvfact() {
		return convfact;
	}
	/**
	 * @param convfact the convfact to set
	 */
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
}

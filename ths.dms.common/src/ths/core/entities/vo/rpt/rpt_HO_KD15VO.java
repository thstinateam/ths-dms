package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import ths.core.entities.vo.rpt.DynamicVO;

public class rpt_HO_KD15VO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String mien;
	private String vung;
	private String maNPP;
	private String maDiemLe;
	private String tenDiemLe;
	private String diaChi;
	private List<DynamicVO> lstDynamicVOs = new ArrayList<DynamicVO>();
	
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getMaDiemLe() {
		return maDiemLe;
	}
	public void setMaDiemLe(String maDiemLe) {
		this.maDiemLe = maDiemLe;
	}
	public String getTenDiemLe() {
		return tenDiemLe;
	}
	public void setTenDiemLe(String tenDiemLe) {
		this.tenDiemLe = tenDiemLe;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public List<DynamicVO> getLstDynamicVOs() {
		return lstDynamicVOs;
	}
	public void setLstDynamicVOs(List<DynamicVO> lstDynamicVOs) {
		this.lstDynamicVOs = lstDynamicVOs;
	}
	
}

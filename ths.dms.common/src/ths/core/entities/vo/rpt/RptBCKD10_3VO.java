package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptBCKD10_3VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String maMien;
    private String maVung;
    private String maNpp;
    private String tenNpp;
    private BigDecimal dsKeHoach;
    private BigDecimal dsThucHien;
    private BigDecimal dsKhac;
    /*private String levelCode;
    private BigDecimal kh;
    private BigDecimal pp;*/
    private BigDecimal tongKh;
    private BigDecimal tongPp; 
    private String productName;
    private String displayProgram;
    private String fromDate;
    private String toDate;
    
    private List<DynamicVO> lstValue = new ArrayList<DynamicVO>();
    
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getDisplayProgram() {
		return displayProgram;
	}
	public void setDisplayProgram(String displayProgram) {
		this.displayProgram = displayProgram;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNpp() {
		return maNpp;
	}
	public void setMaNpp(String maNpp) {
		this.maNpp = maNpp;
	}
	public String getTenNpp() {
		return tenNpp;
	}
	public void setTenNpp(String tenNpp) {
		this.tenNpp = tenNpp;
	}
	public BigDecimal getDsKeHoach() {
		return dsKeHoach;
	}
	public void setDsKeHoach(BigDecimal dsKeHoach) {
		this.dsKeHoach = dsKeHoach;
	}
	public BigDecimal getDsThucHien() {
		return dsThucHien;
	}
	public void setDsThucHien(BigDecimal dsThucHien) {
		this.dsThucHien = dsThucHien;
	}
	public BigDecimal getDsKhac() {
		return dsKhac;
	}
	public void setDsKhac(BigDecimal dsKhac) {
		this.dsKhac = dsKhac;
	}
	
	public Float getPhanTramThucHien() {
		return this.getDsThucHien().floatValue() / this.getDsKeHoach().floatValue();
	}
	public BigDecimal getTongKh() {
		return tongKh;
	}
	public void setTongKh(BigDecimal tongKh) {
		this.tongKh = tongKh;
	}
	public BigDecimal getTongPp() {
		return tongPp;
	}
	public void setTongPp(BigDecimal tongPp) {
		this.tongPp = tongPp;
	}
	public List<DynamicVO> getLstValue() {
		return lstValue;
	}
	public void setLstValue(List<DynamicVO> lstValue) {
		this.lstValue = lstValue;
	}
}

package ths.core.entities.vo.rpt;

import java.math.BigDecimal;
import java.util.List;


public class Rpt3_14_CTKMTCTNV implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 710560671268651768L;
	private String programName; //ten CTKM
	private BigDecimal quantityPromotion; //so luoong km hang
	private BigDecimal amountPromotion;   //so tien km hang
	private BigDecimal moneyPromotion;    //so tien km bang tien
	private BigDecimal total;			  //tong cong
	private List<Rpt3_14_CTKMTCTNV_Detail> listDetail;

	public List<Rpt3_14_CTKMTCTNV_Detail> getListDetail() {
		return listDetail;
	}
	public void setListDetail(List<Rpt3_14_CTKMTCTNV_Detail> listDetail) {
		this.listDetail = listDetail;
	}
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	public BigDecimal getQuantityPromotion() {
		return quantityPromotion;
	}
	public void setQuantityPromotion(BigDecimal quantityPromotion) {
		this.quantityPromotion = quantityPromotion;
	}
	public BigDecimal getAmountPromotion() {
		return amountPromotion;
	}
	public void setAmountPromotion(BigDecimal amountPromotion) {
		this.amountPromotion = amountPromotion;
	}
	public BigDecimal getMoneyPromotion() {
		return moneyPromotion;
	}
	public void setMoneyPromotion(BigDecimal moneyPromotion) {
		this.moneyPromotion = moneyPromotion;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
}

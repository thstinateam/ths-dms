package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptDSPPNHCTVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mien;
	private String khuVuc;
	private Long idNPP;
	private String maNPP;
	private Long idNhomHang;
	private String nhomHang;
	private Integer soDiemLe;//so diem le phat sinh doanh so
	private BigDecimal doanhSo;//doanh so
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getKhuVuc() {
		return khuVuc;
	}
	public void setKhuVuc(String khuVuc) {
		this.khuVuc = khuVuc;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getNhomHang() {
		return nhomHang;
	}
	public void setNhomHang(String nhomHang) {
		this.nhomHang = nhomHang;
	}
	public Integer getSoDiemLe() {
		return soDiemLe;
	}
	public void setSoDiemLe(BigDecimal soDiemLe) {
		this.soDiemLe = null;
		if(soDiemLe != null){
			this.soDiemLe = soDiemLe.intValue();
		}
	}
	public BigDecimal getDoanhSo() {
		return doanhSo;
	}
	public void setDoanhSo(BigDecimal doanhSo) {
		this.doanhSo = doanhSo;
	}
	public Long getIdNPP() {
		return idNPP;
	}
	public void setIdNPP(BigDecimal idNPP) {
		this.idNPP = null;
		if(idNPP != null){
			this.idNPP = idNPP.longValue();
		}
	}
	public Long getIdNhomHang() {
		return idNhomHang;
	}
	public void setIdNhomHang(BigDecimal idNhomHang) {
		this.idNhomHang = null;
		if(idNhomHang != null){
			this.idNhomHang = idNhomHang.longValue();
		}
	}

}

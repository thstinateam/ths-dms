package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptSaleOrderByProductLv01VO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String staffCode;
	private String staffName;
	private List<RptSaleOrderByProductLv02VO> listData;

	public RptSaleOrderByProductLv01VO() {
		this.listData = new ArrayList<RptSaleOrderByProductLv02VO>();
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public List<RptSaleOrderByProductLv02VO> getListData() {
		return listData;
	}

	public void setListData(List<RptSaleOrderByProductLv02VO> listData) {
		this.listData = listData;
	}
}

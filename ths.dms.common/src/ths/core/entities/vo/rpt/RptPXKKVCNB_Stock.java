package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * lop VO phuc vu cho bao cao phieu xuat kho kiem van chuyen noi bo
 * 
 * @author cangnd
 * 
 */
public class RptPXKKVCNB_Stock implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String don_vi_ban_hang = "";
	private String ma_so_thue = "";
	private String dia_chi_nhan_vien = "";
	private String so_dien_thoai = "";
	private String so_tai_khoan = "";
	private String lenh_dieu_dong = "";
	private Date ngay_hien_tai = new Date();
	private String noi_dung = ""; // ve viiec
	private String ten_nguoi_van_chuyen = "";
	private String so_xe = "";
	private String dia_chi_kho = "";

	private Integer don_vi_ngay;
	private Integer don_vi_nam;
	private Integer don_vi_thang;

	private List<RptPXKKVCNB_ProductVansale> danh_sach_san_pham;
	
	public RptPXKKVCNB_Stock() {
		return;
	}

	public RptPXKKVCNB_Stock(List<RptPXKKVCNB_DataConvert> datas, String nameManeuver, String content)
	{
		if(datas != null && datas.size() > 0)	{
			RptPXKKVCNB_DataConvert dataItemF = datas.get(0);
			if(dataItemF.getDon_vi_ban_hang() != null)
				this.don_vi_ban_hang = dataItemF.getDon_vi_ban_hang();
			if(dataItemF.getMa_so_thue() != null)
				this.ma_so_thue = dataItemF.getMa_so_thue();
			if(dataItemF.getDia_chi_nhan_vien() != null)
				this.dia_chi_nhan_vien = dataItemF.getDia_chi_nhan_vien();
			if(dataItemF.getSo_dien_thoai() != null)
				this.so_dien_thoai = dataItemF.getSo_dien_thoai();	
			if(dataItemF.getTen_nguoi_van_chuyen() != null)
				this.ten_nguoi_van_chuyen = dataItemF.getTen_nguoi_van_chuyen();
			if(dataItemF.getSo_xe() != null)
				this.so_xe = dataItemF.getSo_xe();
			if(dataItemF.getDia_chi_kho() != null)
				this.dia_chi_kho = dataItemF.getDia_chi_kho();
			if(dataItemF.getNgay_hien_tai() != null)
				setNgay_hien_tai( dataItemF.getNgay_hien_tai() );
			if(dataItemF.getSo_tai_khoan() != null)
				this.so_tai_khoan = dataItemF.getSo_tai_khoan();
			if(dataItemF.getSo_dien_thoai() != null)
				this.so_dien_thoai = dataItemF.getSo_dien_thoai();
			//this.don_vi_ban_hang = datas
		}
		this.lenh_dieu_dong = nameManeuver;
		this.noi_dung = content;
		
		danh_sach_san_pham = new ArrayList<RptPXKKVCNB_ProductVansale>();
		for (RptPXKKVCNB_DataConvert itemDataConvert : datas) 
		{	
			
			RptPXKKVCNB_ProductVansale productItem = new RptPXKKVCNB_ProductVansale();
			productItem.setDon_gia(itemDataConvert.getDon_gia());
			productItem.setDon_vi_tinh("Thùng/Lẻ");
			productItem.setMa_san_pham(itemDataConvert.getMa_san_pham());
			productItem.setSo_le(itemDataConvert.getSo_le());
			productItem.setSo_luong_thuc_xuat(itemDataConvert.getSo_thung().toString() + "/" + itemDataConvert.getSo_le().toString());
			productItem.setThanh_tien(itemDataConvert.getThanh_tien());
			
			productItem.setSo_thung(itemDataConvert.getSo_thung());
			productItem.setTen_san_pham(itemDataConvert.getTen_san_pham());
			
			danh_sach_san_pham.add(productItem);
		}
		
	}

	public String getDon_vi_ban_hang() {
		return don_vi_ban_hang;
	}

	public void setDon_vi_ban_hang(String don_vi_ban_hang) {
		this.don_vi_ban_hang = don_vi_ban_hang;
	}

	public String getMa_so_thue() {
		return ma_so_thue;
	}

	public void setMa_so_thue(String ma_so_thue) {
		this.ma_so_thue = ma_so_thue;
	}

	public String getDia_chi_nhan_vien() {
		return dia_chi_nhan_vien;
	}

	public void setDia_chi_nhan_vien(String dia_chi_nhan_vien) {
		this.dia_chi_nhan_vien = dia_chi_nhan_vien;
	}

	public String getSo_dien_thoai() {
		return so_dien_thoai;
	}

	public void setSo_dien_thoai(String so_dien_thoai) {
		this.so_dien_thoai = so_dien_thoai;
	}

	public String getSo_tai_khoan() {
		return so_tai_khoan;
	}

	public void setSo_tai_khoan(String so_tai_khoan) {
		this.so_tai_khoan = so_tai_khoan;
	}

	public String getLenh_dieu_dong() {
		return lenh_dieu_dong;
	}

	public void setLenh_dieu_dong(String lenh_dieu_dong) {
		this.lenh_dieu_dong = lenh_dieu_dong;
	}

	public Date getNgay_hien_tai() {
		return ngay_hien_tai;
	}

	public void setNgay_hien_tai(Date ngay_hien_tai) {
		this.ngay_hien_tai = ngay_hien_tai;
	}

	public String getNoi_dung() {
		return noi_dung;
	}

	public void setNoi_dung(String noi_dung) {
		this.noi_dung = noi_dung;
	}

	public String getTen_nguoi_van_chuyen() {
		return ten_nguoi_van_chuyen;
	}

	public void setTen_nguoi_van_chuyen(String ten_nguoi_van_chuyen) {
		this.ten_nguoi_van_chuyen = ten_nguoi_van_chuyen;
	}

	public String getSo_xe() {
		return so_xe;
	}

	public void setSo_xe(String so_xe) {
		this.so_xe = so_xe;
	}

	public String getDia_chi_kho() {
		return dia_chi_kho;
	}

	public void setDia_chi_kho(String dia_chi_kho) {
		this.dia_chi_kho = dia_chi_kho;
	}

	public List<RptPXKKVCNB_ProductVansale> getDanh_sach_san_pham() {
		return danh_sach_san_pham;
	}

	public void setDanh_sach_san_pham(
			List<RptPXKKVCNB_ProductVansale> danh_sach_san_pham) {
		this.danh_sach_san_pham = danh_sach_san_pham;
	}

	public void setDon_vi_ngay(Integer don_vi_ngay) {
		this.don_vi_ngay = don_vi_ngay;
	}

	public Integer getDon_vi_ngay() {
		return don_vi_ngay;
	}

	public void setDon_vi_nam(Integer don_vi_nam) {
		this.don_vi_nam = don_vi_nam;
	}

	public Integer getDon_vi_nam() {
		return don_vi_nam;
	}

	public void setDon_vi_thang(Integer don_vi_thang) {
		this.don_vi_thang = don_vi_thang;
	}

	public Integer getDon_vi_thang() {
		return don_vi_thang;
	}

}

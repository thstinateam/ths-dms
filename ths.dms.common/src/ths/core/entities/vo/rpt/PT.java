package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class PT implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String shortCode;
	public String customerName;
	public BigDecimal sumAmount;
	public String sumAmountByChar;
	public List<PTDetail> listDetail;

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public BigDecimal getSumAmount() {
		return sumAmount;
	}

	public void setSumAmount(BigDecimal sumAmount) {
		this.sumAmount = sumAmount;
	}

	public String getSumAmountByChar() {
		return sumAmountByChar;
	}

	public void setSumAmountByChar(String sumAmountByChar) {
		this.sumAmountByChar = sumAmountByChar;
	}

	public List<PTDetail> getListDetail() {
		return listDetail;
	}

	public void setListDetail(List<PTDetail> listDetail) {
		this.listDetail = listDetail;
	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

import ths.dms.core.entities.Shop;

public class RptProductDistrResultVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<RptProductDistrResult1VO> lstRptProductDistrResult2VO = new ArrayList<RptProductDistrResult1VO>();

	private Shop shop;

	/**
	 * @return the lstRptProductDistrResult2VO
	 */
	public ArrayList<RptProductDistrResult1VO> getLstRptProductDistrResult2VO() {
		return lstRptProductDistrResult2VO;
	}

	/**
	 * @param lstRptProductDistrResult2VO
	 *            the lstRptProductDistrResult2VO to set
	 */
	public void setLstRptProductDistrResult2VO(
			ArrayList<RptProductDistrResult1VO> lstRptProductDistrResult2VO) {
		this.lstRptProductDistrResult2VO = lstRptProductDistrResult2VO;
	}

	/**
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * @param shop the shop to set
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD7VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String MIEN;
	private String VUNG;
	private String TBHV;
	private BigDecimal KHTTTHANG;
	private BigDecimal LKTHTHANG;
	private Integer HTKHTHANG;
	private Integer XEPHANGTHANG;
	private BigDecimal KHTTNAM;
	private BigDecimal LKTHNAM;
	private Integer HTKHNAM;
	private Integer XEPHANGNAM;
	private BigDecimal LKTHNAMTRUOC;
	private BigDecimal LKTHTHANGNAMTRUOC;
	private Integer TTDSSOVOINAMTRUOC;
	public String getMIEN() {
		return MIEN;
	}
	public void setMIEN(String mIEN) {
		MIEN = mIEN;
	}
	public String getVUNG() {
		return VUNG;
	}
	public void setVUNG(String vUNG) {
		VUNG = vUNG;
	}
	public String getTBHV() {
		return TBHV;
	}
	public void setTBHV(String tBHV) {
		TBHV = tBHV;
	}
	public BigDecimal getKHTTTHANG() {
		return KHTTTHANG;
	}
	public void setKHTTTHANG(BigDecimal kHTTTHANG) {
		KHTTTHANG = kHTTTHANG;
	}
	public BigDecimal getLKTHTHANG() {
		return LKTHTHANG;
	}
	public void setLKTHTHANG(BigDecimal lKTHTHANG) {
		LKTHTHANG = lKTHTHANG;
	}
	public Integer getHTKHTHANG() {
		return HTKHTHANG;
	}
	public void setHTKHTHANG(BigDecimal hTKHTHANG) {
		if (hTKHTHANG != null) {
			HTKHTHANG = hTKHTHANG.intValue();
		}
	}
	public Integer getXEPHANGTHANG() {
		return XEPHANGTHANG;
	}
	public void setXEPHANGTHANG(BigDecimal xEPHANGTHANG) {
		if (xEPHANGTHANG != null) {
			XEPHANGTHANG = xEPHANGTHANG.intValue();
		}
	}
	public BigDecimal getKHTTNAM() {
		return KHTTNAM;
	}
	public void setKHTTNAM(BigDecimal kHTTNAM) {
		KHTTNAM = kHTTNAM;
	}
	public BigDecimal getLKTHNAM() {
		return LKTHNAM;
	}
	public void setLKTHNAM(BigDecimal lKTHNAM) {
		LKTHNAM = lKTHNAM;
	}
	public Integer getHTKHNAM() {
		return HTKHNAM;
	}
	public void setHTKHNAM(BigDecimal hTKHNAM) {
		if (hTKHNAM != null) {
			HTKHNAM = hTKHNAM.intValue();
		}
	}
	public Integer getXEPHANGNAM() {
		return XEPHANGNAM;
	}
	public void setXEPHANGNAM(BigDecimal xEPHANGNAM) {
		if (xEPHANGNAM != null) {
			XEPHANGNAM = xEPHANGNAM.intValue();
		}
	}
	public BigDecimal getLKTHNAMTRUOC() {
		return LKTHNAMTRUOC;
	}
	public void setLKTHNAMTRUOC(BigDecimal lKTHNAMTRUOC) {
		LKTHNAMTRUOC = lKTHNAMTRUOC;
	}
	public BigDecimal getLKTHTHANGNAMTRUOC() {
		return LKTHTHANGNAMTRUOC;
	}
	public void setLKTHTHANGNAMTRUOC(BigDecimal lKTHTHANGNAMTRUOC) {
		LKTHTHANGNAMTRUOC = lKTHTHANGNAMTRUOC;
	}
	public Integer getTTDSSOVOINAMTRUOC() {
		return TTDSSOVOINAMTRUOC;
	}
	public void setTTDSSOVOINAMTRUOC(BigDecimal tTDSSOVOINAMTRUOC) {
		if (tTDSSOVOINAMTRUOC != null) {
			TTDSSOVOINAMTRUOC = tTDSSOVOINAMTRUOC.intValue();
		}
	}
	
}

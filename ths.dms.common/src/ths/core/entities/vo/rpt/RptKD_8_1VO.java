package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RptKD_8_1VO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer soNgayBanHang;//so ngay ban hang
	private Integer soNgayThucHien;//so ngay thuc hien
	private Float tienDoChuan;//tien do chuan
	private Date ngayBatDau;//ngay bat dau
	private Date ngayKetThuc;//ngay ket thuc
	private List<RptKD_8_1_CTVO> lstDetail = new ArrayList<RptKD_8_1_CTVO>();
	public Integer getSoNgayBanHang() {
		return soNgayBanHang;
	}
	public void setSoNgayBanHang(Integer soNgayBanHang) {
		this.soNgayBanHang = soNgayBanHang;
	}
	public Integer getSoNgayThucHien() {
		return soNgayThucHien;
	}
	public void setSoNgayThucHien(Integer soNgayThucHien) {
		this.soNgayThucHien = soNgayThucHien;
	}
	public Float getTienDoChuan() {
		return tienDoChuan;
	}
	public void setTienDoChuan(Float tienDoChuan) {
		this.tienDoChuan = tienDoChuan;
	}
	public Date getNgayBatDau() {
		return ngayBatDau;
	}
	public void setNgayBatDau(Date ngayBatDau) {
		this.ngayBatDau = ngayBatDau;
	}
	public Date getNgayKetThuc() {
		return ngayKetThuc;
	}
	public void setNgayKetThuc(Date ngayKetThuc) {
		this.ngayKetThuc = ngayKetThuc;
	}
	public List<RptKD_8_1_CTVO> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<RptKD_8_1_CTVO> lstDetail) {
		this.lstDetail = lstDetail;
	}
}

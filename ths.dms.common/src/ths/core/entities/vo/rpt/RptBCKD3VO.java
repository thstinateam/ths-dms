package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCKD3VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long staffId;
    private String staffCode; //ma nhan vien
    private String staffName; //ten nhan vien
    private String saleGroup; //nhom
    private String productInfoCode; //ma nganh hang
    private Integer currentCountCustomer; //so khach hang thang nay
    private Integer previorCountCustomer; //so khach hang thang truoc
    private BigDecimal currentAmount; //doanh so thang nay
    private BigDecimal previorAmount; //doanh so thang truoc
    private BigDecimal planAmount; //doanh so ke hoach
    private String shopCode; //ma nha phan phoi
    private String superCode; //ma gsnpp
    private String superName; //ten gsnpp
    private String superShopCode; //ma vung
    private String parentSuperShopCode; // ma mien
    private String tbhvCode; //ma tbhv
    private String tbhvName; //ten tbhv
    private BigDecimal lkkh; //luy ke ke hoach
    private Integer htkh;//% hoan thanh ke hoach
    private BigDecimal thieu;//thieu
    private BigDecimal dsbqngay;//doanh so binh quan ngay
    
    public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
    
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(BigDecimal staffId) {
		this.staffId = staffId.longValue();
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getSaleGroup() {
		return saleGroup;
	}
	public void setSaleGroup(String saleGroup) {
		this.saleGroup = saleGroup;
	}
	public String getProductInfoCode() {
		return productInfoCode;
	}
	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}
	public Integer getCurrentCountCustomer() {
		return currentCountCustomer;
	}
	public void setCurrentCountCustomer(BigDecimal currentCountCustomer) {
		this.currentCountCustomer = currentCountCustomer.intValue();
	}
	public Integer getPreviorCountCustomer() {
		return previorCountCustomer;
	}
	public void setPreviorCountCustomer(BigDecimal previorCountCustomer) {
		this.previorCountCustomer = previorCountCustomer.intValue();
	}
	public BigDecimal getCurrentAmount() {
		return currentAmount;
	}
	public void setCurrentAmount(BigDecimal currentAmount) {
		this.currentAmount = currentAmount;
	}
	public BigDecimal getPreviorAmount() {
		return previorAmount;
	}
	public void setPreviorAmount(BigDecimal previorAmount) {
		this.previorAmount = previorAmount;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getSuperCode() {
		return superCode;
	}
	public void setSuperCode(String superCode) {
		this.superCode = superCode;
	}
	public String getSuperName() {
		return superName;
	}
	public void setSuperName(String superName) {
		this.superName = superName;
	}
	public String getSuperShopCode() {
		return superShopCode;
	}
	public void setSuperShopCode(String superShopCode) {
		this.superShopCode = superShopCode;
	}
	public String getParentSuperShopCode() {
		return parentSuperShopCode;
	}
	public void setParentSuperShopCode(String parentSuperShopCode) {
		this.parentSuperShopCode = parentSuperShopCode;
	}
	public String getTbhvCode() {
		return tbhvCode;
	}
	public void setTbhvCode(String tbhvCode) {
		this.tbhvCode = tbhvCode;
	}
	public String getTbhvName() {
		return tbhvName;
	}
	public void setTbhvName(String tbhvName) {
		this.tbhvName = tbhvName;
	}
	public BigDecimal getPlanAmount() {
		return planAmount;
	}
	public void setPlanAmount(BigDecimal planAmount) {
		this.planAmount = planAmount;
	}
	public BigDecimal getLkkh() {
		return lkkh;
	}
	public void setLkkh(BigDecimal lkkh) {
		this.lkkh = lkkh;
	}
	public Integer getHtkh() {
		return htkh;
	}
	public void setHtkh(BigDecimal htkh) {
		this.htkh = htkh.intValue();
	}
	public BigDecimal getThieu() {
		return thieu;
	}
	public void setThieu(BigDecimal thieu) {
		this.thieu = thieu;
	}
	public BigDecimal getDsbqngay() {
		return dsbqngay;
	}
	public void setDsbqngay(BigDecimal dsbqngay) {
		this.dsbqngay = dsbqngay;
	}
    
}

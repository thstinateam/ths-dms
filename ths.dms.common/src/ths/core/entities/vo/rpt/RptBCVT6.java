package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCVT6 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mien; //mien
	private String vung; //vung
	private String shopcode; //npp
	private String shopNAME; //ten npp
	private String STAFFCODE; //nvgs
	private String NAME; //ten nvgs
	private BigDecimal CHAMCONGMUON;
	private String THOIGIANCHAMCONGTAINPP;
	private BigDecimal KHONGCHAMCONG;
	private BigDecimal GHETHAMDIEMBANTRE;
	private String THOIGIANGHETHAMDIEMBANTRE;
	private String MAKH;
	private String TENKH;
	private String DIACHI;
	
	public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getShopcode() {
		return shopcode;
	}

	public void setShopcode(String shop_code) {
		this.shopcode = shop_code;
	}

	public String getShopNAME() {
		return shopNAME;
	}

	public void setShopNAME(String shop_NAME) {
		this.shopNAME = shop_NAME;
	}

	public String getSTAFFCODE() {
		return STAFFCODE;
	}

	public void setSTAFFCODE(String sTAFF_CODE) {
		STAFFCODE = sTAFF_CODE;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public BigDecimal getCHAMCONGMUON() {
		return CHAMCONGMUON;
	}

	public void setCHAMCONGMUON(BigDecimal cHAMCONGMUON) {
		CHAMCONGMUON = cHAMCONGMUON;
	}

	public String getTHOIGIANCHAMCONGTAINPP() {
		return THOIGIANCHAMCONGTAINPP;
	}

	public void setTHOIGIANCHAMCONGTAINPP(String tHOIGIANCHAMCONGTAINPP) {
		THOIGIANCHAMCONGTAINPP = tHOIGIANCHAMCONGTAINPP;
	}

	public BigDecimal getKHONGCHAMCONG() {
		return KHONGCHAMCONG;
	}

	public void setKHONGCHAMCONG(BigDecimal kHONGCHAMCONG) {
		KHONGCHAMCONG = kHONGCHAMCONG;
	}

	public BigDecimal getGHETHAMDIEMBANTRE() {
		return GHETHAMDIEMBANTRE;
	}

	public void setGHETHAMDIEMBANTRE(BigDecimal gHETHAMDIEMBANTRE) {
		GHETHAMDIEMBANTRE = gHETHAMDIEMBANTRE;
	}

	public String getTHOIGIANGHETHAMDIEMBANTRE() {
		return THOIGIANGHETHAMDIEMBANTRE;
	}

	public void setTHOIGIANGHETHAMDIEMBANTRE(String tHOIGIANGHETHAMDIEMBANTRE) {
		THOIGIANGHETHAMDIEMBANTRE = tHOIGIANGHETHAMDIEMBANTRE;
	}

	public String getMAKH() {
		return MAKH;
	}

	public void setMAKH(String mAKH) {
		MAKH = mAKH;
	}

	public String getTENKH() {
		return TENKH;
	}

	public void setTENKH(String tENKH) {
		TENKH = tENKH;
	}

	public String getDIACHI() {
		return DIACHI;
	}

	public void setDIACHI(String dIACHI) {
		DIACHI = dIACHI;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author hungnm
 * 
 */
public class RptBCNgayVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String tenNV;
	private String maNV;
	private String loaiNV;
	private BigDecimal doanhSoNgay;
	private BigDecimal doanhSoLuyKe;
	private BigDecimal keHoachLuyKe;
	private Float phanTramTheoTienDo;
	private BigDecimal chenhLenhTienDo;
	private BigDecimal doanhSoKeHoach;
	private BigDecimal doanhSoPhaiBanNS;
	private Integer keHoachMucTieu;
	private Integer f4TrongNgay;
	private Integer f4LuyKe;
	private Integer f4ThangTruoc;
	private Integer soDiemBan;
	private Integer soDiemChuaPP;
	private Integer palmTrongTuyen;
	private Integer palmNgoaiTuyen;
	private Integer traVeHomTruoc;
	private Integer donTay;
	private String doanhSo;
	private String phanPhoi;

	public RptBCNgayVO() {
	}

	public String getTenNV() {
		return tenNV;
	}

	public void setTenNV(String tenNV) {
		this.tenNV = tenNV;
	}

	public String getLoaiNV() {
		return loaiNV;
	}

	public void setLoaiNV(String loaiNV) {
		this.loaiNV = loaiNV;
	}

	public BigDecimal getDoanhSoNgay() {
		return doanhSoNgay;
	}

	public void setDoanhSoNgay(BigDecimal doanhSoNgay) {
		this.doanhSoNgay = doanhSoNgay;
	}

	public BigDecimal getDoanhSoLuyKe() {
		return doanhSoLuyKe;
	}

	public void setDoanhSoLuyKe(BigDecimal doanhSoLuyKe) {
		this.doanhSoLuyKe = doanhSoLuyKe;
	}

	public BigDecimal getKeHoachLuyKe() {
		return keHoachLuyKe;
	}

	public void setKeHoachLuyKe(BigDecimal keHoachLuyKe) {
		this.keHoachLuyKe = keHoachLuyKe;
	}

	public Float getPhanTramTheoTienDo() {
		return phanTramTheoTienDo;
	}

	public void setPhanTramTheoTienDo(BigDecimal phanTramTheoTienDo) {
		if (null != phanTramTheoTienDo) {
			this.phanTramTheoTienDo = phanTramTheoTienDo.floatValue();
		} else {
			this.phanTramTheoTienDo = 0f;
		}
	}

	public BigDecimal getChenhLenhTienDo() {
		return chenhLenhTienDo;
	}

	public void setChenhLenhTienDo(BigDecimal chenhLenhTienDo) {
		this.chenhLenhTienDo = chenhLenhTienDo;
	}

	public BigDecimal getDoanhSoKeHoach() {
		return doanhSoKeHoach;
	}

	public void setDoanhSoKeHoach(BigDecimal doanhSoKeHoach) {
		this.doanhSoKeHoach = doanhSoKeHoach;
	}

	public BigDecimal getDoanhSoPhaiBanNS() {
		return doanhSoPhaiBanNS;
	}

	public void setDoanhSoPhaiBanNS(BigDecimal doanhSoPhaiBanNS) {
		this.doanhSoPhaiBanNS = doanhSoPhaiBanNS;
	}

	public Integer getKeHoachMucTieu() {
		return keHoachMucTieu;
	}

	public void setKeHoachMucTieu(BigDecimal keHoachMucTieu) {
		if (null != keHoachMucTieu) {
			this.keHoachMucTieu = keHoachMucTieu.intValue();
		} else {
			this.keHoachMucTieu = 0;
		}
	}

	public Integer getF4TrongNgay() {
		return f4TrongNgay;
	}

	public void setF4TrongNgay(BigDecimal f4TrongNgay) {
		if (null != f4TrongNgay) {
			this.f4TrongNgay = f4TrongNgay.intValue();
		} else {
			this.f4TrongNgay = 0;
		}
	}

	public Integer getF4LuyKe() {
		return f4LuyKe;
	}

	public void setF4LuyKe(BigDecimal f4LuyKe) {
		if (null != f4LuyKe) {
			this.f4LuyKe = f4LuyKe.intValue();
		} else {
			this.f4LuyKe = 0;
		}
	}

	public Integer getF4ThangTruoc() {
		return f4ThangTruoc;
	}

	public void setF4ThangTruoc(BigDecimal f4ThangTruoc) {
		if (null != f4ThangTruoc) {
			this.f4ThangTruoc = f4ThangTruoc.intValue();
		} else {
			this.f4ThangTruoc = 0;
		}
	}

	public Integer getSoDiemBan() {
		return soDiemBan;
	}

	public void setSoDiemBan(BigDecimal soDiemBan) {
		if (null != soDiemBan) {
			this.soDiemBan = soDiemBan.intValue();
		} else {
			this.soDiemBan = 0;
		}
	}

	public Integer getSoDiemChuaPP() {
		return soDiemChuaPP;
	}

	public void setSoDiemChuaPP(BigDecimal soDiemChuaPP) {
		if (null != soDiemChuaPP) {
			this.soDiemChuaPP = soDiemChuaPP.intValue();
		} else {
			this.soDiemChuaPP = 0;
		}
	}

	public Integer getPalmTrongTuyen() {
		return palmTrongTuyen;
	}

	public void setPalmTrongTuyen(BigDecimal palmTrongTuyen) {
		if (null != palmTrongTuyen) {
			this.palmTrongTuyen = palmTrongTuyen.intValue();
		} else {
			this.palmTrongTuyen = 0;
		}
	}

	public Integer getPalmNgoaiTuyen() {
		return palmNgoaiTuyen;
	}

	public void setPalmNgoaiTuyen(BigDecimal palmNgoaiTuyen) {
		if (null != palmNgoaiTuyen) {
			this.palmNgoaiTuyen = palmNgoaiTuyen.intValue();
		} else {
			this.palmNgoaiTuyen = 0;
		}
	}

	public Integer getTraVeHomTruoc() {
		return traVeHomTruoc;
	}

	public void setTraVeHomTruoc(BigDecimal traVeHomTruoc) {
		if (null != traVeHomTruoc) {
			this.traVeHomTruoc = traVeHomTruoc.intValue();
		} else {
			this.traVeHomTruoc = 0;
		}
	}

	public Integer getDonTay() {
		return donTay;
	}

	public void setDonTay(BigDecimal donTay) {
		if (null != donTay) {
			this.donTay = donTay.intValue();
		} else {
			this.donTay = 0;
		}
	}

	public String getDoanhSo() {
		if (doanhSo == null || "".equals(doanhSo.trim())) {
			if (null != phanTramTheoTienDo) {
				if (phanTramTheoTienDo > 100) {
					doanhSo = "Vượt tiến độ";
				}
				if (phanTramTheoTienDo == 100) {
					doanhSo = "Đúng tiến độ";
				}
				if (phanTramTheoTienDo >= 80 && phanTramTheoTienDo < 100) {
					doanhSo = "Cần cải thiện";
				} else {
					doanhSo = "Yếu, khắc phục gắp";
				}
			}
		}
		
		return doanhSo;
	}

	public void setDoanhSo(String doanhSo) {
		this.doanhSo = doanhSo;
	}

	public String getPhanPhoi() {
		return phanPhoi;
	}

	public void setPhanPhoi(String phanPhoi) {
		this.phanPhoi = phanPhoi;
	}

	public String getMaNV() {
		return maNV;
	}

	public void setMaNV(String maNV) {
		this.maNV = maNV;
	};
}
package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptKD9_Detail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5667822262029978787L;
	
	private String mien;
	private String vung;
	private String npp;
	private String itemNo;
	private BigDecimal tonDauKy1;
	private BigDecimal totalNhap;
	private BigDecimal totalXuat;
	private BigDecimal cuoiKy1;
	private BigDecimal giaTriCKy1;
	private BigDecimal soLuongBan;
	private BigDecimal giaTriBan;
	private BigDecimal soLuongConLai;
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getNpp() {
		return npp;
	}
	public void setNpp(String npp) {
		this.npp = npp;
	}
	public String getItemNo() {
		return itemNo;
	}
	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}
	public BigDecimal getTonDauKy1() {
		return tonDauKy1;
	}
	public void setTonDauKy1(BigDecimal tonDauKy1) {
		this.tonDauKy1 = tonDauKy1;
	}
	public BigDecimal getTotalNhap() {
		return totalNhap;
	}
	public void setTotalNhap(BigDecimal totalNhap) {
		this.totalNhap = totalNhap;
	}
	public BigDecimal getTotalXuat() {
		return totalXuat;
	}
	public void setTotalXuat(BigDecimal totalXuat) {
		this.totalXuat = totalXuat;
	}
	public BigDecimal getCuoiKy1() {
		return cuoiKy1;
	}
	public void setCuoiKy1(BigDecimal cuoiKy1) {
		this.cuoiKy1 = cuoiKy1;
	}
	public BigDecimal getGiaTriCKy1() {
		return giaTriCKy1;
	}
	public void setGiaTriCKy1(BigDecimal giaTriCKy1) {
		this.giaTriCKy1 = giaTriCKy1;
	}
	public BigDecimal getSoLuongBan() {
		return soLuongBan;
	}
	public void setSoLuongBan(BigDecimal soLuongBan) {
		this.soLuongBan = soLuongBan;
	}
	public BigDecimal getGiaTriBan() {
		return giaTriBan;
	}
	public void setGiaTriBan(BigDecimal giaTriBan) {
		this.giaTriBan = giaTriBan;
	}
	public BigDecimal getSoLuongConLai() {
		return soLuongConLai;
	}
	public void setSoLuongConLai(BigDecimal soLuongConLai) {
		this.soLuongConLai = soLuongConLai;
	}
	
	
}

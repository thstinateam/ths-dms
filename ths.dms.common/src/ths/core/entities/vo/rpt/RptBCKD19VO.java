package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RptBCKD19VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Date tuNgay;
	private Date denNgay;
	private List<RptBCKD19DataVO> lstDetail;
	private List<RptBCKD19DataVO_1> lstDetail_1;

	public Date getTuNgay() {
		return tuNgay;
	}

	public void setTuNgay(Date tuNgay) {
		this.tuNgay = tuNgay;
	}

	public Date getDenNgay() {
		return denNgay;
	}

	public void setDenNgay(Date denNgay) {
		this.denNgay = denNgay;
	}

	public List<RptBCKD19DataVO> getLstDetail() {
		return lstDetail;
	}

	public void setLstDetail(List<RptBCKD19DataVO> lstDetail) {
		this.lstDetail = lstDetail;
	}

	public void setLstDetail_1(List<RptBCKD19DataVO_1> lstDetail_1) {
		this.lstDetail_1 = lstDetail_1;
	}

	public List<RptBCKD19DataVO_1> getLstDetail_1() {
		return lstDetail_1;
	}
}

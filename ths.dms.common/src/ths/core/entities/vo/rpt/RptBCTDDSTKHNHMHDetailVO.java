package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCTDDSTKHNHMHDetailVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal customerId;
	private String customerCode;
	private String customerName;
	private String shortCode;

	private BigDecimal catId;
	private String catCode;
	private String catName;
	
	private BigDecimal subCatId;
	private String subCatCode;
	private String subCatName;


	private BigDecimal brandId;
	private String brandCode;
	private String brandName;


	private BigDecimal flavourId;
	private String flavourCode;
	private String flavourName;

	private BigDecimal productId;
	private String productCode;
	private String productName;

	private BigDecimal price;
	private BigDecimal quantity;
	private BigDecimal amountMoney;
	
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	public BigDecimal getCustomerId() {
		return customerId;
	}
	public void setCustomerId(BigDecimal customerId) {
		this.customerId = customerId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public BigDecimal getCatId() {
		return catId;
	}
	public void setCatId(BigDecimal catId) {
		this.catId = catId;
	}
	public String getCatCode() {
		return catCode;
	}
	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	public BigDecimal getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(BigDecimal subCatId) {
		this.subCatId = subCatId;
	}
	public String getSubCatCode() {
		return subCatCode;
	}
	public void setSubCatCode(String subCatCode) {
		this.subCatCode = subCatCode;
	}
	public String getSubCatName() {
		return subCatName;
	}
	public void setSubCatName(String subCatName) {
		this.subCatName = subCatName;
	}
	public BigDecimal getBrandId() {
		return brandId;
	}
	public void setBrandId(BigDecimal brandId) {
		this.brandId = brandId;
	}
	public String getBrandCode() {
		return brandCode;
	}
	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	
	public BigDecimal getFlavourId() {
		return flavourId;
	}
	public void setFlavourId(BigDecimal flavourId) {
		this.flavourId = flavourId;
	}
	public String getFlavourCode() {
		return flavourCode;
	}
	public void setFlavourCode(String flavourCode) {
		this.flavourCode = flavourCode;
	}
	public String getFlavourName() {
		return flavourName;
	}
	public void setFlavourName(String flavourName) {
		this.flavourName = flavourName;
	}
	public BigDecimal getProductId() {
		return productId;
	}
	public void setProductId(BigDecimal productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getAmountMoney() {
		return amountMoney;
	}
	public void setAmountMoney(BigDecimal amountMoney) {
		this.amountMoney = amountMoney;
	}
	public RptBCTDDSTKHNHMHDetailVO() {
		
	}	
	
	
	
}

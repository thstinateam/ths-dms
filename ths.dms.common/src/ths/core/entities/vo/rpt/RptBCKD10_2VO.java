package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RptBCKD10_2VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer soNgayBanHang;
	private Integer soNgayThucHien;
	private Float tienDoChuan;
	private Date tuNgay;
	private Date denNgay;
	private String productInfo;
	private List<RptBCKD10_2DataVO> lstDetail;

	public Integer getSoNgayBanHang() {
		return soNgayBanHang;
	}

	public void setSoNgayBanHang(Integer soNgayBanHang) {
		this.soNgayBanHang = soNgayBanHang;
	}

	public Integer getSoNgayThucHien() {
		return soNgayThucHien;
	}

	public void setSoNgayThucHien(Integer soNgayThucHien) {
		this.soNgayThucHien = soNgayThucHien;
	}

	public Float getTienDoChuan() {
		return tienDoChuan;
	}

	public void setTienDoChuan(Float tienDoChuan) {
		this.tienDoChuan = tienDoChuan;
	}

	public Date getTuNgay() {
		return tuNgay;
	}

	public void setTuNgay(Date tuNgay) {
		this.tuNgay = tuNgay;
	}

	public Date getDenNgay() {
		return denNgay;
	}

	public void setDenNgay(Date denNgay) {
		this.denNgay = denNgay;
	}

	public List<RptBCKD10_2DataVO> getLstDetail() {
		return lstDetail;
	}

	public void setLstDetail(List<RptBCKD10_2DataVO> lstDetail) {
		this.lstDetail = lstDetail;
	}

	public String getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(String productInfo) {
		this.productInfo = productInfo;
	}
}

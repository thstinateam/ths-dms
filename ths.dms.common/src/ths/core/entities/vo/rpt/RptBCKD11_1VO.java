package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD11_1VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String MAMIEN;
	private String MAVUNG;
	private String MANPP;
	private String MANVBH;
	private String MADIEMLE;
	private String MUC;
	private BigDecimal KEHOACH;
	private BigDecimal THUCHIEN;
	private String PHANTRAM;
	public String getMAMIEN() {
		return MAMIEN;
	}
	public void setMAMIEN(String mAMIEN) {
		MAMIEN = mAMIEN;
	}
	public String getMAVUNG() {
		return MAVUNG;
	}
	public void setMAVUNG(String mAVUNG) {
		MAVUNG = mAVUNG;
	}
	public String getMANPP() {
		return MANPP;
	}
	public void setMANPP(String mANPP) {
		MANPP = mANPP;
	}
	public String getMANVBH() {
		return MANVBH;
	}
	public void setMANVBH(String mANVBH) {
		MANVBH = mANVBH;
	}
	public String getMADIEMLE() {
		return MADIEMLE;
	}
	public void setMADIEMLE(String mADIEMLE) {
		MADIEMLE = mADIEMLE;
	}
	public String getMUC() {
		return MUC;
	}
	public void setMUC(String mUC) {
		MUC = mUC;
	}
	public BigDecimal getKEHOACH() {
		return KEHOACH;
	}
	public void setKEHOACH(BigDecimal kEHOACH) {
		KEHOACH = kEHOACH;
	}
	public BigDecimal getTHUCHIEN() {
		return THUCHIEN;
	}
	public void setTHUCHIEN(BigDecimal tHUCHIEN) {
		THUCHIEN = tHUCHIEN;
	}
	public String getPHANTRAM() {
		return PHANTRAM;
	}
	public void setPHANTRAM(String pHANTRAM) {
		PHANTRAM = pHANTRAM;
	}
	
}

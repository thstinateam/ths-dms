package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class Rpt_7_2_16 implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String maNPP;
	private String tenNPP;
	private String ngay;
	private BigDecimal soDonHang;
	private BigDecimal slHangBan;
	private BigDecimal slHangKM;
	private BigDecimal doanhThu;
	
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
		}
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getTenNPP() {
		return tenNPP;
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}

	public String getNgay() {
		return ngay;
	}

	public void setNgay(String ngay) {
		this.ngay = ngay;
	}

	public BigDecimal getSoDonHang() {
		return soDonHang;
	}

	public void setSoDonHang(BigDecimal soDonHang) {
		this.soDonHang = soDonHang;
	}

	public BigDecimal getSlHangBan() {
		return slHangBan;
	}

	public void setSlHangBan(BigDecimal slHangBan) {
		this.slHangBan = slHangBan;
	}

	public BigDecimal getSlHangKM() {
		return slHangKM;
	}

	public void setSlHangKM(BigDecimal slHangKM) {
		this.slHangKM = slHangKM;
	}

	public BigDecimal getDoanhThu() {
		return doanhThu;
	}

	public void setDoanhThu(BigDecimal doanhThu) {
		this.doanhThu = doanhThu;
	}
	
	
}
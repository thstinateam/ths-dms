package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptPGHDetailVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String productCode;
	
	private String productName;
	
	private Integer convfact;
	
	private Integer quantity = 0;
	
	private BigDecimal price;
	
	private BigDecimal packagePrice;
	
	private BigDecimal amount = new BigDecimal(0);
	
	private List<RptPGHLotVO> lstProductLot = new ArrayList<RptPGHLotVO>();
	
	private String promotionName;
	
	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getThung() {
		return this.getQuantity() / this.getConvfact();
	}

	public Integer getLe() {
		return this.getQuantity() % this.getConvfact();
	}

	public List<RptPGHLotVO> getLstProductLot() {
		return lstProductLot;
	}

	public void setLstProductLot(List<RptPGHLotVO> lstProductLot) {
		this.lstProductLot = lstProductLot;
	}

	public BigDecimal getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(BigDecimal packagePrice) {
		this.packagePrice = packagePrice;
	}
}
/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

/**
 * Thoi gian lam viec cua NVBH
 * 
 * @author hunglm16
 * @since 23/10/2015
 */
public class Rpt_TGLVCNVBH_VO implements Serializable {

	private static final long serialVersionUID = 3807222792931957185L;

	private String maMien;
	private String maVung;
	private String maNPP;
	private String tenNPP;
	private String maNV;
	private String tenNV;
	private String maGS;
	private String tenGS;
	private String ngayLV;
	private String dungGio;
	private String denTre;
	private String khongDen;
	private String kcGanNhat;
	private String tgQDNPP;
	private String tngayQDNPP;
	private String dngayQDNPP;
	private String gtDTBS;
	private String gtCCBS;
	private String tongGTBS;
	private String gtDTBC;
	private String gtCCBC;
	private String tongGTBC;

	private BigDecimal kcQD;

	/**
	 * Xu ly du lieu null
	 * 
	 * @author hunglm16
	 * @throws Exception
	 * @since 23/10/2015
	 */
	public void safeSetNull() throws Exception {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Xu ly du lieu null
	 * 
	 * @author hunglm16
	 * @throws Exception
	 * @since 23/10/2015
	 */
	private void xuLyDuLieuViPham() {
		if (dungGio != null && dungGio.trim().length() > 0) {
			denTre = null;
			khongDen = null;
			kcGanNhat = null;
		} else {
			if (denTre != null && denTre.trim().length() > 0) {
				khongDen = null;
				kcGanNhat = null;
			}
		}
	}

	/**
	 * Khai bao phuong thuc GETTER/SETTER
	 */

	public String getMaVung() {
		return maVung;
	}

	public String getKhongDen() {
		xuLyDuLieuViPham();
		return khongDen;
	}

	public void setKhongDen(String khongDen) {
		this.khongDen = khongDen;
	}

	public String getNgayLV() {
		return ngayLV;
	}

	public void setNgayLV(String ngayLV) {
		this.ngayLV = ngayLV;
	}

	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}

	public String getMaMien() {
		return maMien;
	}

	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getTenNPP() {
		return tenNPP;
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}

	public String getMaNV() {
		return maNV;
	}

	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}

	public String getTenNV() {
		return tenNV;
	}

	public void setTenNV(String tenNV) {
		this.tenNV = tenNV;
	}

	public String getTgQDNPP() {
		return tgQDNPP;
	}

	public void setTgQDNPP(String tgQDNPP) {
		this.tgQDNPP = tgQDNPP;
	}

	public String getTngayQDNPP() {
		return tngayQDNPP;
	}

	public void setTngayQDNPP(String tngayQDNPP) {
		this.tngayQDNPP = tngayQDNPP;
	}

	public String getDngayQDNPP() {
		return dngayQDNPP;
	}

	public void setDngayQDNPP(String dngayQDNPP) {
		this.dngayQDNPP = dngayQDNPP;
	}

	public String getDungGio() {
		xuLyDuLieuViPham();
		return dungGio;
	}

	public void setDungGio(String dungGio) {
		this.dungGio = dungGio;
	}

	public String getDenTre() {
		xuLyDuLieuViPham();
		return denTre;
	}

	public void setDenTre(String denTre) {
		this.denTre = denTre;
	}

	public String getGtDTBS() {
		return gtDTBS;
	}

	public void setGtDTBS(String gtDTBS) {
		this.gtDTBS = gtDTBS;
	}

	public String getGtCCBS() {
		return gtCCBS;
	}

	public void setGtCCBS(String gtCCBS) {
		this.gtCCBS = gtCCBS;
	}

	public String getTongGTBS() {
		return tongGTBS;
	}

	public void setTongGTBS(String tongGTBS) {
		this.tongGTBS = tongGTBS;
	}

	public String getGtDTBC() {
		return gtDTBC;
	}

	public void setGtDTBC(String gtDTBC) {
		this.gtDTBC = gtDTBC;
	}

	public String getGtCCBC() {
		return gtCCBC;
	}

	public void setGtCCBC(String gtCCBC) {
		this.gtCCBC = gtCCBC;
	}

	public String getTongGTBC() {
		return tongGTBC;
	}

	public void setTongGTBC(String tongGTBC) {
		this.tongGTBC = tongGTBC;
	}

	public String getKcGanNhat() {
		xuLyDuLieuViPham();
		return kcGanNhat;
	}

	public void setKcGanNhat(String kcGanNhat) {
		this.kcGanNhat = kcGanNhat;
	}

	public BigDecimal getKcQD() {
		return kcQD;
	}

	public void setKcQD(BigDecimal kcQD) {
		this.kcQD = kcQD;
	}

	public String getMaGS() {
		return maGS;
	}

	public void setMaGS(String maGS) {
		this.maGS = maGS;
	}

	public String getTenGS() {
		return tenGS;
	}

	public void setTenGS(String tenGS) {
		this.tenGS = tenGS;
	}

}

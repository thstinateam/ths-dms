package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

public class RptBCTDDSTKHNHMHLv1VO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal customerId;
	private String customerCode;
	private String customerName;
	private String shortCode;
	private List<RptBCTDDSTKHNHMHCatLv2VO> lstDataLv2;
	private BigDecimal amountMoney;
	
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}	
	
	public BigDecimal getCustomerId() {
		return customerId;
	}
	public void setCustomerId(BigDecimal customerId) {
		this.customerId = customerId;
	}
	public String getCustomerCode() {
		if(null != customerName && !customerName.equals("")){
			return customerCode + " - " + customerName;
		}
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getShortCode() {
		if(null != customerName && !customerName.equals("")){
			return shortCode + " - " + customerName;
		}
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	
	public void setAmountMoney(BigDecimal amountMoney) {
		this.amountMoney = amountMoney;
	}
	
	public BigDecimal getAmountMoney() {		
		return amountMoney;		
	}
	
	public List<RptBCTDDSTKHNHMHCatLv2VO> getLstDataLv2() {
		return lstDataLv2;
	}
	public void setLstDataLv2(List<RptBCTDDSTKHNHMHCatLv2VO> lstDataLv2) {
		this.lstDataLv2 = lstDataLv2;
		if(null != lstDataLv2 && lstDataLv2.size() > 0){
			amountMoney = BigDecimal.ZERO;
			for(int i = 0; i<lstDataLv2.size(); i++){
				if(null != lstDataLv2.get(i).getAmountMoney()){
					amountMoney = amountMoney.add(lstDataLv2.get(i).getAmountMoney());
				}				
			}
		}
	}
	public RptBCTDDSTKHNHMHLv1VO() {		
	}
}

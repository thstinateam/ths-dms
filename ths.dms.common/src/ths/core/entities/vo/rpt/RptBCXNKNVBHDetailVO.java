package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCXNKNVBHDetailVO implements Serializable{
	
	private BigDecimal shopId;
	
	private String stockTransDate;
	
	private BigDecimal staffId;
	private String staffCode;
	private String staffName;	
	
	private String stockTransCode;
	
	
	private BigDecimal productId;
	private String productCode;	
	private String productName;
	
	private BigDecimal price;
	
	private String lot;
	
	private BigDecimal quantityCheckLot;
	private BigDecimal quantityNotCheckLot;
	private BigDecimal convfact;
	private BigDecimal checkLot;
	
	private BigDecimal soThung;
	private BigDecimal soLe;
	private BigDecimal thanhTien;
	
	//private BigDecimal sumAmount;

	public BigDecimal getShopId() {
		return shopId;
	}

	public void setShopId(BigDecimal shopId) {
		this.shopId = shopId;
	}

	public String getStockTransDate() {
		return stockTransDate;
	}

	public void setStockTransDate(String stockTransDate) {
		this.stockTransDate = stockTransDate;
	}

	public BigDecimal getStaffId() {
		return staffId;
	}

	public void setStaffId(BigDecimal staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStockTransCode() {
		return stockTransCode;
	}

	public void setStockTransCode(String stockTransCode) {
		this.stockTransCode = stockTransCode;
	}

	public BigDecimal getProductId() {
		return productId;
	}

	public void setProductId(BigDecimal productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public BigDecimal getQuantityCheckLot() {
		return quantityCheckLot;
	}

	public void setQuantityCheckLot(BigDecimal quantityCheckLot) {
		this.quantityCheckLot = quantityCheckLot;
	}

	public BigDecimal getQuantityNotCheckLot() {
		return quantityNotCheckLot;
	}

	public void setQuantityNotCheckLot(BigDecimal quantityNotCheckLot) {
		this.quantityNotCheckLot = quantityNotCheckLot;
	}
	
	public BigDecimal getConvfact() {
		return convfact;
	}

	public void setConvfact(BigDecimal convfact) {
		this.convfact = convfact;
	}
	
	public BigDecimal getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(BigDecimal checkLot) {
		this.checkLot = checkLot;
	}

	public BigDecimal getSoThung() {
		return soThung;
	}

	public void setSoThung(BigDecimal soThung) {
		this.soThung = soThung;
	}

	public BigDecimal getSoLe() {
		return soLe;
	}

	public void setSoLe(BigDecimal soLe) {
		this.soLe = soLe;
	}

	public BigDecimal getThanhTien() {
		return thanhTien;
	}

	public void setThanhTien(BigDecimal thanhTien) {
		this.thanhTien = thanhTien;
	}



//	public BigDecimal getSumAmount() {
//		return sumAmount;
//	}
//
//	public void setSumAmount(BigDecimal sumAmount) {
//		this.sumAmount = sumAmount;
//	}	
	
//	private static final long serialVersionUID = 1L;
//	
//	private String productCode;
//	
//	private String productName;
//	
//	private BigDecimal price;
//	
//	private String lot;
//	
//	private Integer convfact;
//	
//	private Integer quantity;
//	
//	private BigDecimal amount;
//	
//	private String thungLe;
//	
//	private String thung;
//	
//	private String le;
//
//	public String getProductCode() {
//		return productCode;
//	}
//
//	public void setProductCode(String productCode) {
//		this.productCode = productCode;
//	}
//
//	public String getProductName() {
//		return productName;
//	}
//
//	public void setProductName(String productName) {
//		this.productName = productName;
//	}
//
//	public BigDecimal getPrice() {
//		return price;
//	}
//
//	public void setPrice(BigDecimal price) {
//		this.price = price;
//	}
//
//	public String getLot() {
//		return lot;
//	}
//
//	public void setLot(String lot) {
//		this.lot = lot;
//	}
//
//	public Integer getConvfact() {
//		return convfact;
//	}
//
//	public void setConvfact(Integer convfact) {
//		this.convfact = convfact;
//	}
//
//	public Integer getQuantity() {
//		return quantity;
//	}
//
//	public void setQuantity(Integer quantity) {
//		this.quantity = quantity;
//	}
//
//	public BigDecimal getAmount() {
//		if (price != null && quantity != null)
//			return price.multiply(new BigDecimal(quantity));
//		else
//			return null;
//	}
//
//	public void setAmount(BigDecimal amount) {
//		this.amount = amount;
//	}
//
//	public String getThungLe() {
//		Integer thung = this.quantity / this.convfact;
//		Integer le = this.quantity % this.convfact;
//		return thung.toString() + "/" + le.toString();
//	}
//	
//	public String getThung() {
//		Integer thung = this.quantity / this.convfact;
//		return thung.toString();
//	}
//	
//	public String getLe() {
//		Integer le = this.quantity % this.convfact;
//		return le.toString();
//	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RptSaleOrderInDate1VO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Date orderDate;
	
	private List<RptSaleOrderInDateVO> lstRptSaleOrderInDateVO;

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public List<RptSaleOrderInDateVO> getLstRptSaleOrderInDateVO() {
		return lstRptSaleOrderInDateVO;
	}

	public void setLstRptSaleOrderInDateVO(
			List<RptSaleOrderInDateVO> lstRptSaleOrderInDateVO) {
		this.lstRptSaleOrderInDateVO = lstRptSaleOrderInDateVO;
	}
	
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCTGGTCNVBH_1VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String areaCode;
	private String locationCode;
	private String shopCode;
	private String shopName;

	private String staffOwnerCode;
	private String staffOwnerName;
	private String staffCode;
	private String staffName;

	private Long afterStartTime;
	private Long beforeEndTime;
	private Long lowerThan5Min;
	private Long lowerThan30Min;
	private Long lowerThan60Min;
	private Long largeThan60Min;
	private BigDecimal avgTimePerCustomer;
	
	public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getStaffOwnerCode() {
		return staffOwnerCode;
	}
	public void setStaffOwnerCode(String staffOwnerCode) {
		this.staffOwnerCode = staffOwnerCode;
	}
	public String getStaffOwnerName() {
		return staffOwnerName;
	}
	public void setStaffOwnerName(String staffOwnerName) {
		this.staffOwnerName = staffOwnerName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Long getAfterStartTime() {
		return afterStartTime;
	}
	public void setAfterStartTime(Long afterStartTime) {
		this.afterStartTime = afterStartTime;
	}
	public Long getBeforeEndTime() {
		return beforeEndTime;
	}
	public void setBeforeEndTime(Long beforeEndTime) {
		this.beforeEndTime = beforeEndTime;
	}
	public Long getLowerThan5Min() {
		return lowerThan5Min;
	}
	public void setLowerThan5Min(Long lowerThan5Min) {
		this.lowerThan5Min = lowerThan5Min;
	}
	public Long getLowerThan30Min() {
		return lowerThan30Min;
	}
	public void setLowerThan30Min(Long lowerThan30Min) {
		this.lowerThan30Min = lowerThan30Min;
	}
	public Long getLowerThan60Min() {
		return lowerThan60Min;
	}
	public void setLowerThan60Min(Long lowerThan60Min) {
		this.lowerThan60Min = lowerThan60Min;
	}
	public Long getLargeThan60Min() {
		return largeThan60Min;
	}
	public void setLargeThan60Min(Long largeThan60Min) {
		this.largeThan60Min = largeThan60Min;
	}
	public BigDecimal getAvgTimePerCustomer() {
		return avgTimePerCustomer;
	}
	public void setAvgTimePerCustomer(BigDecimal avgTimePerCustomer) {
		this.avgTimePerCustomer = avgTimePerCustomer;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
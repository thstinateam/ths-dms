package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ths.dms.core.common.utils.GroupUtility;

public class RptDebtOfCustomerDataVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long customerId;
	private String customerCode;
	private String customerName;
	private Date customerDate;
	private String orderNumber;
	private String orderType;
	// ngay den han phai tra
	private Date duaDate;
	// so ngay no toi da cua khach hang
	private Integer maxDebitDate;
	// gia tri no (co the am, hoac duong)
	private BigDecimal remain;
	// no qua han
	private BigDecimal overdueDebt;
	// no den han
	private BigDecimal dueDebt;
	// no chua den han
	private BigDecimal undueDebt;
	// nhan vien ban hang
	private String staffCode;
	private String staffName;
	// nhan vien giao hang
	private String deliveryCode;
	private String deliveryName;

	public RptDebtOfCustomerDataVO() {
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getCustomerDate() {
		return customerDate;
	}

	public void setCustomerDate(Date customerDate) {
		this.customerDate = customerDate;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Date getDuaDate() {
		return duaDate;
	}

	public void setDuaDate(Date duaDate) {
		this.duaDate = duaDate;
	}

	public Integer getMaxDebitDate() {
		return maxDebitDate;
	}

	public void setMaxDebitDate(Integer maxDebitDate) {
		this.maxDebitDate = maxDebitDate;
	}

	public BigDecimal getRemain() {
		return remain;
	}

	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}

	public BigDecimal getOverdueDebt() {
		return overdueDebt;
	}

	public void setOverdueDebt(BigDecimal overdueDebt) {
		this.overdueDebt = overdueDebt;
	}

	public BigDecimal getDueDebt() {
		return dueDebt;
	}

	public void setDueDebt(BigDecimal dueDebt) {
		this.dueDebt = dueDebt;
	}

	public BigDecimal getUndueDebt() {
		return undueDebt;
	}

	public void setUndueDebt(BigDecimal undueDebt) {
		this.undueDebt = undueDebt;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	@SuppressWarnings("unchecked")
	public static List<RptDebtOfCustomerLv01VO> groupByCustomerCode(
			List<RptDebtOfCustomerDataVO> listData) throws Exception {
		List<RptDebtOfCustomerLv01VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, "customerCode");

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptDebtOfCustomerLv01VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptDebtOfCustomerLv01VO vo = new RptDebtOfCustomerLv01VO();

					List<RptDebtOfCustomerDataVO> listObject = (List<RptDebtOfCustomerDataVO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setCustomerCode((String) mapEntry.getKey());
						vo.setCustomerName(listObject.get(0).getCustomerName());

						vo.setListData(listObject);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}

}

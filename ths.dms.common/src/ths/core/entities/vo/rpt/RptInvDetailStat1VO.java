package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class RptInvDetailStat1VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String invoiceNumber;
	private Long totalSku = 0l;
	private BigDecimal totalAmount = BigDecimal.ZERO;
	private BigDecimal totalFinalAmount = BigDecimal.ZERO;

	private BigDecimal totalDiscount = BigDecimal.ZERO;
	private BigDecimal totalTaxAmount = BigDecimal.ZERO;
	private Float vat;
	
	private ArrayList<RptInvDetailStatDetailVO> lstRptInvDetailStat2VO = new ArrayList<RptInvDetailStatDetailVO>();
	
	private String totalAmountChar;
	private String totalFinalAmountChar;

	private String totalDiscountChar;
	private String totalTaxAmountChar;

	public String getTotalAmountChar() {
		return totalAmountChar;
	}

	public void setTotalAmountChar(String totalAmountChar) {
		this.totalAmountChar = totalAmountChar;
	}

	public String getTotalFinalAmountChar() {
		return totalFinalAmountChar;
	}

	public void setTotalFinalAmountChar(String totalFinalAmountChar) {
		this.totalFinalAmountChar = totalFinalAmountChar;
	}

	public String getTotalDiscountChar() {
		return totalDiscountChar;
	}

	public void setTotalDiscountChar(String totalDiscountChar) {
		this.totalDiscountChar = totalDiscountChar;
	}

	public String getTotalTaxAmountChar() {
		return totalTaxAmountChar;
	}

	public void setTotalTaxAmountChar(String totalTaxAmountChar) {
		this.totalTaxAmountChar = totalTaxAmountChar;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public ArrayList<RptInvDetailStatDetailVO> getLstRptInvDetailStat2VO() {
		return lstRptInvDetailStat2VO;
	}

	public void setLstRptInvDetailStat2VO(
			ArrayList<RptInvDetailStatDetailVO> lstRptInvDetailStat2VO) {
		this.lstRptInvDetailStat2VO = lstRptInvDetailStat2VO;
	}

	public Long getTotalSku() {
		return totalSku;
	}

	public void setTotalSku(Long totalSku) {
		this.totalSku = totalSku;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalFinalAmount() {
		return totalFinalAmount;
	}

	public void setTotalFinalAmount(BigDecimal totalFinalAmount) {
		this.totalFinalAmount = totalFinalAmount;
	}

	public BigDecimal getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public Float getVat() {
		return vat;
	}

	public void setVat(Float vat) {
		this.vat = vat;
	}

	public BigDecimal getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(BigDecimal totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

}

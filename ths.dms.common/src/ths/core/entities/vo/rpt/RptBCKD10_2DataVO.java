package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD10_2DataVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String areaCode; // ma mien
	private String locationCode; // ma vung
	private String shopCode; // ma nha phan phoi
	private String shopName; // ten nha phan phoi
	private String cusCode; // ma khach hang
	private String cusName; // ten khach hang
	private String cusAddress; // dia chi khach hang
	private Integer amountPlan; // luy ke muc tieu
	private Integer amount; // luy ke thuc hien
	private String percentAmount; // phan tram thuc hien
	private Integer g;//GROUPING_ID
	public Integer getG() {
		return g;
	}

	public void setG(Integer g) {
		this.g = g;
	}

	public void setAmountPlan(Integer amountPlan) {
		this.amountPlan = amountPlan;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getCusCode() {
		return cusCode;
	}

	public void setCusCode(String cusCode) {
		this.cusCode = cusCode;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCusAddress() {
		return cusAddress;
	}

	public void setCusAddress(String cusAddress) {
		this.cusAddress = cusAddress;
	}

	public Integer getAmountPlan() {
		return amountPlan;
	}

	public void setAmountPlan(BigDecimal amountPlan) {
		if (amountPlan != null) {
			this.amountPlan = amountPlan.intValue();
		}
	}

	public String getPercentAmount() {
		return percentAmount;
	}

	public void setPercentAmount(String percentAmount) {
		this.percentAmount = percentAmount;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		if (amount != null) {
			this.amount = amount.intValue();
		}
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD19_1VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String mien;
    private String vung;
    private String gsnpp;
    private String doi;
    private Integer count;
    private BigDecimal amountPlan;
    private BigDecimal amount;
    private Integer percent;
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getGsnpp() {
		return gsnpp;
	}
	public void setGsnpp(String gsnpp) {
		this.gsnpp = gsnpp;
	}
	public String getDoi() {
		return doi;
	}
	public void setDoi(String doi) {
		this.doi = doi;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public BigDecimal getAmountPlan() {
		return amountPlan;
	}
	public void setAmountPlan(BigDecimal amountPlan) {
		this.amountPlan = amountPlan;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Integer getPercent() {
		return percent;
	}
	public void setPercent(Integer percent) {
		this.percent = percent;
	}
	
	
    
}

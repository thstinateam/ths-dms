package ths.core.entities.vo.rpt;



public class RptGS_1_1_BCTTT implements java.io.Serializable {

	/**
	 * @author hunglm16
	 * @since JUNE 9,2014
	 * @description Update
	 */
	private static final long serialVersionUID = 710560671268651768L;
	private String maMien;
	private String maVung;
	private String maNPP;
	private String tenNPP;
	private String maTuyen;
	private String tenTuyen;
	private String maNVBH;
	private String tenNVBH;
	private String maKH;
	private String tenKH;
	private String diaChi;
	private String ngayBatDau;
	private String ngayKetThuc;
	private Integer t2;
	private Integer t3;
	private Integer t4;
	private Integer t5;
	private Integer t6;
	private Integer t7;
	private Integer t8;
	private Integer w1;
	private Integer w2;
	private Integer w3;
	private Integer w4;
	private Integer seq2;
	private Integer seq3;
	private Integer seq4;
	private Integer seq5;
	private Integer seq6;
	private Integer seq7;
	private Integer seq8;
	private String trangThai;
	
	public String getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaTuyen() {
		return maTuyen;
	}
	public void setMaTuyen(String maTuyen) {
		this.maTuyen = maTuyen;
	}
	public String getTenTuyen() {
		return tenTuyen;
	}
	public void setTenTuyen(String tenTuyen) {
		this.tenTuyen = tenTuyen;
	}
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getNgayBatDau() {
		return ngayBatDau;
	}
	public void setNgayBatDau(String ngayBatDau) {
		this.ngayBatDau = ngayBatDau;
	}
	public String getNgayKetThuc() {
		return ngayKetThuc;
	}
	public void setNgayKetThuc(String ngayKetThuc) {
		this.ngayKetThuc = ngayKetThuc;
	}
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public Integer getT2() {
		if(t2!=null){
			return t2;
		}
		return 0;
	}
	public void setT2(Integer t2) {
		this.t2 = t2;
	}
	public Integer getT3() {
		if(t3!=null){
			return t3;
		}
		return 0;
	}
	public void setT3(Integer t3) {
		this.t3 = t3;
	}
	public Integer getT4() {
		if(t4!=null){
			return t4;
		}
		return 0;
	}
	public void setT4(Integer t4) {
		this.t4 = t4;
	}
	public Integer getT5() {
		if(t5!=null){
			return t5;
		}
		return 0;
	}
	public void setT5(Integer t5) {
		this.t5 = t5;
	}
	public Integer getT6() {
		if(t6!=null){
			return t6;
		}
		return 0;
	}
	public void setT6(Integer t6) {
		this.t6 = t6;
	}
	public Integer getT7() {
		if(t7!=null){
			return t7;
		}
		return 0;
	}
	public void setT7(Integer t7) {
		this.t7 = t7;
	}
	public Integer getT8() {
		if(t8!=null){
			return t8;
		}
		return 0;
	}
	public void setT8(Integer t8) {
		this.t8 = t8;
	}
	public Integer getW1() {
		if(w1!=null){
			return w1;
		}
		return 0;
	}
	public void setW1(Integer w1) {
		this.w1 = w1;
	}
	public Integer getW2() {
		if(w2!=null){
			return w2;
		}
		return 0;
	}
	public void setW2(Integer w2) {
		this.w2 = w2;
	}
	public Integer getW3() {
		if(w3!=null){
			return w3;
		}
		return 0;
	}
	public void setW3(Integer w3) {
		this.w3 = w3;
	}
	public Integer getW4() {
		if(w4!=null){
			return w4;
		}
		return 0;
	}
	public void setW4(Integer w4) {
		this.w4 = w4;
	}
	public Integer getSeq2() {
		if(seq2!=null){
			return seq2;
		}
		return 0;
	}
	public void setSeq2(Integer seq2) {
		this.seq2 = seq2;
	}
	public Integer getSeq3() {
		if(seq3!=null){
			return seq3;
		}
		return 0;
	}
	public void setSeq3(Integer seq3) {
		this.seq3 = seq3;
	}
	public Integer getSeq4() {
		if(seq4!=null){
			return seq4;
		}
		return 0;
	}
	public void setSeq4(Integer seq4) {
		this.seq4 = seq4;
	}
	public Integer getSeq5() {
		if(seq5!=null){
			return seq5;
		}
		return 0;
	}
	public void setSeq5(Integer seq5) {
		this.seq5 = seq5;
	}
	public Integer getSeq6() {
		if(seq6!=null){
			return seq6;
		}
		return 0;
	}
	public void setSeq6(Integer seq6) {
		this.seq6 = seq6;
	}
	public Integer getSeq7() {
		if(seq7!=null){
			return seq7;
		}
		return 0;
	}
	public void setSeq7(Integer seq7) {
		this.seq7 = seq7;
	}
	public Integer getSeq8() {
		if(seq8!=null){
			return seq8;
		}
		return 0;
	}
	public void setSeq8(Integer seq8) {
		this.seq8 = seq8;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	
}

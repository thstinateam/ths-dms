/*
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.core.entities.vo.rpt;

import java.io.Serializable;


/**
 * Bao cao 3.1.1.9	[WV-KD-03-F11] Danh sách khách hàng mượn tủ
 * 
 * @author tamvnm
 * @since May 55,2015
 */
public class Rpt_3_1_1_9_DSKHMT_F11_VO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long shopId;
	private Long customerId;
	private String dateContract;  
	private String contractNumber;
	private String mien;
	private String vung;
	private String shopCode;
	private String shopName;
	private String maGSNPP;
	private String tenGSNPP;
	private String shortCode;
	private String equipCode;
	private String equipName;
	private String serialNumber;
	private String customerName;
	private String address;
	private String equipGroupName;
	private String categoryCode;
	private String categoryName;
	private String brandName;
	private String capacity;
	private String dateFirstUse;
	private String endDateContract;
	private String reasonEndContract;
	private String healthStatusStr;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getDateContract() {
		return dateContract;
	}
	public void setDateContract(String dateContract) {
		this.dateContract = dateContract;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getMaGSNPP() {
		return maGSNPP;
	}
	public void setMaGSNPP(String maGSNPP) {
		this.maGSNPP = maGSNPP;
	}
	public String getTenGSNPP() {
		return tenGSNPP;
	}
	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getEquipCode() {
		return equipCode;
	}
	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}
	public String getEquipName() {
		return equipName;
	}
	public void setEquipName(String equipName) {
		this.equipName = equipName;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEquipGroupName() {
		return equipGroupName;
	}
	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getDateFirstUse() {
		return dateFirstUse;
	}
	public void setDateFirstUse(String dateFirstUse) {
		this.dateFirstUse = dateFirstUse;
	}
	public String getEndDateContract() {
		return endDateContract;
	}
	public void setEndDateContract(String endDateContract) {
		this.endDateContract = endDateContract;
	}
	public String getReasonEndContract() {
		return reasonEndContract;
	}
	public void setReasonEndContract(String reasonEndContract) {
		this.reasonEndContract = reasonEndContract;
	}
	public String getHealthStatusStr() {
		return healthStatusStr;
	}
	public void setHealthStatusStr(String healthStatusStr) {
		this.healthStatusStr = healthStatusStr;
	}
	
}
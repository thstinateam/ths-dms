package ths.core.entities.vo.rpt;

import java.math.BigDecimal;

public class Rpt_TDXNTCTVO implements java.io.Serializable
{
	private String ma_nganh;

	private String ten_hang;
	private String ma_hang;
	private BigDecimal don_gia;
	
	private BigDecimal so_luong_ton_dau;
	private String thung_le_ton_dau;
	private BigDecimal tong_tien_ton_dau;
	
	private BigDecimal so_luong_ban;
	private String thung_le_ban;
	private BigDecimal tong_tien_ban;
	
	private BigDecimal so_luong_khuyen_mai;
	private String thung_le_khuyen_mai;
	private BigDecimal tong_tien_khuyen_mai;
	
	private BigDecimal so_luong_mua_hang;
	private String thung_le_mua_hang;
	private BigDecimal tong_tien_mua_hang;
	
	private BigDecimal so_luong_tra_hang;
	private String thung_le_tra_hang;
	private BigDecimal tong_tien_tra_hang;
	
	private BigDecimal so_luong_nhap_kho;
	private String thung_le_nhap_kho;
	
	private BigDecimal so_luong_xuat_kho;
	private String thung_le_xuat_kho;
	
	private BigDecimal so_luong_ton_cuoi;
	private String thung_le_ton_cuoi;
	private BigDecimal tong_tien_cuoi;
	public String getMa_nganh() {
		return ma_nganh;
	}
	public void setMa_nganh(String ma_nganh) {
		this.ma_nganh = ma_nganh;
	}
	public String getTen_hang() {
		return ten_hang;
	}
	public void setTen_hang(String ten_hang) {
		this.ten_hang = ten_hang;
	}
	public String getMa_hang() {
		return ma_hang;
	}
	public void setMa_hang(String ma_hang) {
		this.ma_hang = ma_hang;
	}
	public BigDecimal getDon_gia() {
		return don_gia;
	}
	public void setDon_gia(BigDecimal don_gia) {
		this.don_gia = don_gia;
	}
	public BigDecimal getSo_luong_ton_dau() {
		return so_luong_ton_dau;
	}
	public void setSo_luong_ton_dau(BigDecimal so_luong_ton_dau) {
		this.so_luong_ton_dau = so_luong_ton_dau;
	}
	public String getThung_le_ton_dau() {
		return thung_le_ton_dau;
	}
	public void setThung_le_ton_dau(String thung_le_ton_dau) {
		this.thung_le_ton_dau = thung_le_ton_dau;
	}
	public BigDecimal getTong_tien_ton_dau() {
		return tong_tien_ton_dau;
	}
	public void setTong_tien_ton_dau(BigDecimal tong_tien_ton_dau) {
		this.tong_tien_ton_dau = tong_tien_ton_dau;
	}
	public BigDecimal getSo_luong_ban() {
		return so_luong_ban;
	}
	public void setSo_luong_ban(BigDecimal so_luong_ban) {
		this.so_luong_ban = so_luong_ban;
	}
	public String getThung_le_ban() {
		return thung_le_ban;
	}
	public void setThung_le_ban(String thung_le_ban) {
		this.thung_le_ban = thung_le_ban;
	}
	public BigDecimal getTong_tien_ban() {
		return tong_tien_ban;
	}
	public void setTong_tien_ban(BigDecimal tong_tien_ban) {
		this.tong_tien_ban = tong_tien_ban;
	}
	public BigDecimal getSo_luong_khuyen_mai() {
		return so_luong_khuyen_mai;
	}
	public void setSo_luong_khuyen_mai(BigDecimal so_luong_khuyen_mai) {
		this.so_luong_khuyen_mai = so_luong_khuyen_mai;
	}
	public String getThung_le_khuyen_mai() {
		return thung_le_khuyen_mai;
	}
	public void setThung_le_khuyen_mai(String thung_le_khuyen_mai) {
		this.thung_le_khuyen_mai = thung_le_khuyen_mai;
	}
	public BigDecimal getTong_tien_khuyen_mai() {
		return tong_tien_khuyen_mai;
	}
	public void setTong_tien_khuyen_mai(BigDecimal tong_tien_khuyen_mai) {
		this.tong_tien_khuyen_mai = tong_tien_khuyen_mai;
	}
	public BigDecimal getSo_luong_mua_hang() {
		return so_luong_mua_hang;
	}
	public void setSo_luong_mua_hang(BigDecimal so_luong_mua_hang) {
		this.so_luong_mua_hang = so_luong_mua_hang;
	}
	public String getThung_le_mua_hang() {
		return thung_le_mua_hang;
	}
	public void setThung_le_mua_hang(String thung_le_mua_hang) {
		this.thung_le_mua_hang = thung_le_mua_hang;
	}
	public BigDecimal getTong_tien_mua_hang() {
		return tong_tien_mua_hang;
	}
	public void setTong_tien_mua_hang(BigDecimal tong_tien_mua_hang) {
		this.tong_tien_mua_hang = tong_tien_mua_hang;
	}
	public BigDecimal getSo_luong_tra_hang() {
		return so_luong_tra_hang;
	}
	public void setSo_luong_tra_hang(BigDecimal so_luong_tra_hang) {
		this.so_luong_tra_hang = so_luong_tra_hang;
	}
	public String getThung_le_tra_hang() {
		return thung_le_tra_hang;
	}
	public void setThung_le_tra_hang(String thung_le_tra_hang) {
		this.thung_le_tra_hang = thung_le_tra_hang;
	}
	public BigDecimal getTong_tien_tra_hang() {
		return tong_tien_tra_hang;
	}
	public void setTong_tien_tra_hang(BigDecimal tong_tien_tra_hang) {
		this.tong_tien_tra_hang = tong_tien_tra_hang;
	}
	public BigDecimal getSo_luong_nhap_kho() {
		return so_luong_nhap_kho;
	}
	public void setSo_luong_nhap_kho(BigDecimal so_luong_nhap_kho) {
		this.so_luong_nhap_kho = so_luong_nhap_kho;
	}
	public String getThung_le_nhap_kho() {
		return thung_le_nhap_kho;
	}
	public void setThung_le_nhap_kho(String thung_le_nhap_kho) {
		this.thung_le_nhap_kho = thung_le_nhap_kho;
	}
	public BigDecimal getSo_luong_xuat_kho() {
		return so_luong_xuat_kho;
	}
	public void setSo_luong_xuat_kho(BigDecimal so_luong_xuat_kho) {
		this.so_luong_xuat_kho = so_luong_xuat_kho;
	}
	public String getThung_le_xuat_kho() {
		return thung_le_xuat_kho;
	}
	public void setThung_le_xuat_kho(String thung_le_xuat_kho) {
		this.thung_le_xuat_kho = thung_le_xuat_kho;
	}
	public BigDecimal getSo_luong_ton_cuoi() {
		return so_luong_ton_cuoi;
	}
	public void setSo_luong_ton_cuoi(BigDecimal so_luong_ton_cuoi) {
		this.so_luong_ton_cuoi = so_luong_ton_cuoi;
	}
	public String getThung_le_ton_cuoi() {
		return thung_le_ton_cuoi;
	}
	public void setThung_le_ton_cuoi(String thung_le_ton_cuoi) {
		this.thung_le_ton_cuoi = thung_le_ton_cuoi;
	}
	public BigDecimal getTong_tien_cuoi() {
		return tong_tien_cuoi;
	}
	public void setTong_tien_cuoi(BigDecimal tong_tien_cuoi) {
		this.tong_tien_cuoi = tong_tien_cuoi;
	}
	


	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
public class RptPayDisplayProgrameRecordProductVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String displayProgrameCode;
	private String displayProgrameName;
	private String fDate;
	private String tDate;
	
	private String productCode;
	private String productName;
	private BigDecimal promotionQuantity;
	private BigDecimal price;
	//private BigDecimal money;//Tien se tu lay soluong*gia tren jasper.
	//private Date orderDate;
	
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	public String getProductCode() {
		return productCode;
	}
	public String getDisplayProgrameCode() {
		return displayProgrameCode;
	}
	public void setDisplayProgrameCode(String displayProgrameCode) {
		this.displayProgrameCode = displayProgrameCode;
	}
	public String getDisplayProgrameName() {
		return displayProgrameName;
	}
	public void setDisplayProgrameName(String displayProgrameName) {
		this.displayProgrameName = displayProgrameName;
	}
	
	public String getfDate() {
		return fDate;
	}
	public void setfDate(String fDate) {
		this.fDate = fDate;
	}
	public String gettDate() {
		return tDate;
	}
	public void settDate(String tDate) {
		this.tDate = tDate;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public BigDecimal getPromotionQuantity() {
		return promotionQuantity;
	}
	public void setPromotionQuantity(BigDecimal promotionQuantity) {
		this.promotionQuantity = promotionQuantity;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	/*public BigDecimal getMoney() {
		return money;
	}
	public void setMoney(BigDecimal money) {
		this.money = money;
	}*/
	
}

package ths.core.entities.vo.rpt;

import java.math.BigDecimal;
import java.util.Date;

public class Rpt3_13_CTKMTCT_Detail implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 710560671268651768L;
	private String programName; //ten CTKM
	private Date orderDate;
	private String staff;
	private String customer;
	private String product;
	private BigDecimal quantityPromotion; //so luoong km hang
	private BigDecimal amountPromotion; //so tien km hang
	private BigDecimal moneyPromotion; //so tien km bang tien
	private BigDecimal quantityMPromotion;
	private BigDecimal total; //tong cong

	public BigDecimal getQuantityMPromotion() {
		if (quantityMPromotion != null) {
			return quantityMPromotion;
		}
		return BigDecimal.ZERO;
	}

	public void setQuantityMPromotion(BigDecimal quantityMPromotion) {
		this.quantityMPromotion = quantityMPromotion;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getStaff() {
		return staff;
	}

	public void setStaff(String staff) {
		this.staff = staff;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public BigDecimal getQuantityPromotion() {
		if (quantityPromotion != null) {
			return quantityPromotion;
		}
		return BigDecimal.ZERO;
	}

	public void setQuantityPromotion(BigDecimal quantityPromotion) {
		this.quantityPromotion = quantityPromotion;
	}

	public BigDecimal getAmountPromotion() {
		if (amountPromotion != null) {
			return amountPromotion;
		}
		return BigDecimal.ZERO;
	}

	public void setAmountPromotion(BigDecimal amountPromotion) {
		this.amountPromotion = amountPromotion;
	}

	public BigDecimal getMoneyPromotion() {
		if (moneyPromotion != null) {
			return moneyPromotion;
		}
		return BigDecimal.ZERO;
	}

	public void setMoneyPromotion(BigDecimal moneyPromotion) {
		this.moneyPromotion = moneyPromotion;
	}

	public BigDecimal getTotal() {
		if (total != null) {
			return total;
		}
		return BigDecimal.ZERO;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
}

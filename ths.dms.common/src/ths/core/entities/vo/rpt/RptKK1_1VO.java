/*
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * Mo ta class RptKK1_1VO.java
 * @author vuongmq
 * @since Mar 29, 2016
 */
public class RptKK1_1VO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long id;

	private String shopCode;
	private String shopName;
	private String shortCode;
	private String customerName;
	private String userStatisticCode;
	private String userStatisticName;
	private String equipCode; // ma tai san
	private String serial;
	private String equipGroupCode;
	private String equipGroupName;
	private String categoryCode; // ma loai tb
	private String categoryName; // ten loai tb
	private String brandName; // hieu
	private String capacity; //dung tich
	private String manufacturingYear; //nam san xuat
	private String statisticDate; //ngay kiem ke
	private String reason;
	private String statisticDateEnd; //time kiem ke cuoi
	private String statusCurrent; // tinh trang hien tai
	private Integer numberStatistic;
	private Integer numberTotalImg; //time kiem ke cuoi
	
	/** Khai bao method */
	public void safeSetNull() {
		//Set gia tri mac dinh cho cac truong null
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			//ghi log
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getUserStatisticCode() {
		return userStatisticCode;
	}

	public void setUserStatisticCode(String userStatisticCode) {
		this.userStatisticCode = userStatisticCode;
	}

	public String getUserStatisticName() {
		return userStatisticName;
	}

	public void setUserStatisticName(String userStatisticName) {
		this.userStatisticName = userStatisticName;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getEquipGroupCode() {
		return equipGroupCode;
	}

	public void setEquipGroupCode(String equipGroupCode) {
		this.equipGroupCode = equipGroupCode;
	}

	public String getEquipGroupName() {
		return equipGroupName;
	}

	public void setEquipGroupName(String equipGroupName) {
		this.equipGroupName = equipGroupName;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getManufacturingYear() {
		return manufacturingYear;
	}

	public void setManufacturingYear(String manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}

	public String getStatisticDate() {
		return statisticDate;
	}

	public void setStatisticDate(String statisticDate) {
		this.statisticDate = statisticDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatisticDateEnd() {
		return statisticDateEnd;
	}

	public void setStatisticDateEnd(String statisticDateEnd) {
		this.statisticDateEnd = statisticDateEnd;
	}

	public String getStatusCurrent() {
		return statusCurrent;
	}

	public void setStatusCurrent(String statusCurrent) {
		this.statusCurrent = statusCurrent;
	}

	public Integer getNumberStatistic() {
		return numberStatistic;
	}

	public void setNumberStatistic(Integer numberStatistic) {
		this.numberStatistic = numberStatistic;
	}

	public Integer getNumberTotalImg() {
		return numberTotalImg;
	}

	public void setNumberTotalImg(Integer numberTotalImg) {
		this.numberTotalImg = numberTotalImg;
	}
	
}

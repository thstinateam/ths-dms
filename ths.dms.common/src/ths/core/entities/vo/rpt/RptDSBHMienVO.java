package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class RptDSBHMienVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer soNgayBanHang;
	private Integer soNgayNghiLe;
	private Integer soNgayThucHien;
	private Float tienDoChuan;
	private Date tuNgay;
	private Date denNgay;
	private List<RptDSBHMienCTVO> lstMien;
	
	public void safeSetNull() throws IllegalArgumentException, IllegalAccessException{
		for(Field field:getClass().getDeclaredFields()) {
			if(field.getType().equals(Integer.class) && field.get(this) == null) {
				field.set(this, 0);
			}
			if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
				field.set(this, BigDecimal.ZERO);
			}
			if(field.getType().equals(String.class) && field.get(this) == null) {
				field.set(this, "");
			}
		}
	}
	
	public Integer getSoNgayBanHang() {
		return soNgayBanHang;
	}
	public void setSoNgayBanHang(Integer soNgayBanHang) {
		this.soNgayBanHang = soNgayBanHang;
	}
	public Integer getSoNgayNghiLe() {
		return soNgayNghiLe;
	}
	public void setSoNgayNghiLe(Integer soNgayNghiLe) {
		this.soNgayNghiLe = soNgayNghiLe;
	}
	public Integer getSoNgayThucHien() {
		return soNgayThucHien;
	}
	public void setSoNgayThucHien(Integer soNgayThucHien) {
		this.soNgayThucHien = soNgayThucHien;
	}
	public Float getTienDoChuan() {
		return tienDoChuan;
	}
	public void setTienDoChuan(Float tienDoChuan) {
		this.tienDoChuan = tienDoChuan;
	}
	public Date getTuNgay() {
		return tuNgay;
	}
	public void setTuNgay(Date tuNgay) {
		this.tuNgay = tuNgay;
	}
	public Date getDenNgay() {
		return denNgay;
	}
	public void setDenNgay(Date denNgay) {
		this.denNgay = denNgay;
	}
	public List<RptDSBHMienCTVO> getLstMien() {
		return lstMien;
	}
	public void setLstMien(List<RptDSBHMienCTVO> lstMien) {
		this.lstMien = lstMien;
	}
}

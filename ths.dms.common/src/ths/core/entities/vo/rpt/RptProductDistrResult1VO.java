package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

public class RptProductDistrResult1VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String staffCode;
	private String staffName;

	private ArrayList<RptProductDistrResult2VO> lstRptProductDistrResult2VO = new ArrayList<RptProductDistrResult2VO>();

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	/**
	 * @return the lstRptProductDistrResult2VO
	 */
	public ArrayList<RptProductDistrResult2VO> getLstRptProductDistrResult2VO() {
		return lstRptProductDistrResult2VO;
	}

	/**
	 * @param lstRptProductDistrResult2VO
	 *            the lstRptProductDistrResult2VO to set
	 */
	public void setLstRptProductDistrResult2VO(
			ArrayList<RptProductDistrResult2VO> lstRptProductDistrResult2VO) {
		this.lstRptProductDistrResult2VO = lstRptProductDistrResult2VO;
	}
}

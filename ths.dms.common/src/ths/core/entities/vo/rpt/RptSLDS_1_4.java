package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

public class RptSLDS_1_4 implements Serializable {
	private String mien;
	private String vung;
	private String npp;
	private String tennpp;
	private String staffCode;
	private String staffName;
	private List<DynamicVO> lstValue;
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getTennpp() {
		return tennpp;
	}
	public void setTennpp(String tennpp) {
		this.tennpp = tennpp;
	}
	public String getNpp() {
		return npp;
	}
	public void setNpp(String npp) {
		this.npp = npp;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public List<DynamicVO> getLstValue() {
		return lstValue;
	}
	public void setLstValue(List<DynamicVO> lstValue) {
		this.lstValue = lstValue;
	}
}

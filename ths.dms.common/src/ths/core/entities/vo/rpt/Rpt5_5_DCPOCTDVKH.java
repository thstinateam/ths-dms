package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class Rpt5_5_DCPOCTDVKH implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7242487419194102444L;
	private String saleOrderNumber; //so hoa don cua po dvkh
	private String productCode; //ma san pham
	private Integer quantityTotal; //so luong dat hang
	private Integer price; //gia san pham
	private String createDate; //ngay nhap he thong NPP
	private Integer quantityWaitting; //so luong cho giao
	private Integer quantityImport; //so luong PO nhap
	private Integer quantityRemain; //so luong PO DVKH con treo
	
	public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	public String getSaleOrderNumber() {
		return saleOrderNumber;
	}

	public void setSaleOrderNumber(String saleOrderNumber) {
		this.saleOrderNumber = saleOrderNumber;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public Integer getQuantityTotal() {
		return quantityTotal;
	}

	public void setQuantityTotal(Integer quantityTotal) {
		this.quantityTotal = quantityTotal;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getQuantityWaitting() {
		return quantityWaitting;
	}

	public void setQuantityWaitting(Integer quantityWaitting) {
		this.quantityWaitting = quantityWaitting;
	}

	public Integer getQuantityImport() {
		return quantityImport;
	}

	public void setQuantityImport(Integer quantityImport) {
		this.quantityImport = quantityImport;
	}

	public Integer getQuantityRemain() {
		return quantityRemain;
	}

	public void setQuantityRemain(Integer quantityRemain) {
		this.quantityRemain = quantityRemain;
	}	
	
}

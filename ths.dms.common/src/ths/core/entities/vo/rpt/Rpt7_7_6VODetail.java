package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class Rpt7_7_6VODetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String shopCode;
	private String shopName;
	private String shopAddress;
	private String shopPhone;
	
	private String soHDGTGT;
	private String ngayXuatHD;
	private String maKH;
	private String tenKH;
	private String diaChiKH;
	private String maSoThue;
	private Integer SKU;
	private BigDecimal thueSuat;
	private BigDecimal thanhTienTT;
	private BigDecimal thanhTienST;
	private Integer trangThai;
	private String soDH;
	private String maNV;
	
	private String sttStr;
	
	public String getSttStr() {
		return sttStr;
	}
	public void setSttStr(String sttStr) {
		this.sttStr = sttStr;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String getShopPhone() {
		return shopPhone;
	}
	public void setShopPhone(String shopPhone) {
		this.shopPhone = shopPhone;
	}
	public String getSoHDGTGT() {
		return soHDGTGT;
	}
	public void setSoHDGTGT(String soHDGTGT) {
		this.soHDGTGT = soHDGTGT;
	}
	public String getNgayXuatHD() {
		return ngayXuatHD;
	}
	public void setNgayXuatHD(String ngayXuatHD) {
		this.ngayXuatHD = ngayXuatHD;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getDiaChiKH() {
		return diaChiKH;
	}
	public void setDiaChiKH(String diaChiKH) {
		this.diaChiKH = diaChiKH;
	}
	public String getMaSoThue() {
		return maSoThue;
	}
	public void setMaSoThue(String maSoThue) {
		this.maSoThue = maSoThue;
	}
	public Integer getSKU() {
		return SKU;
	}
	public void setSKU(Integer sKU) {
		SKU = sKU;
	}
	public BigDecimal getThueSuat() {
		return thueSuat;
	}
	public void setThueSuat(BigDecimal thueSuat) {
		this.thueSuat = thueSuat;
	}
	public BigDecimal getThanhTienTT() {
		return thanhTienTT;
	}
	public void setThanhTienTT(BigDecimal thanhTienTT) {
		this.thanhTienTT = thanhTienTT;
	}
	public BigDecimal getThanhTienST() {
		return thanhTienST;
	}
	public void setThanhTienST(BigDecimal thanhTienST) {
		this.thanhTienST = thanhTienST;
	}
	public Integer getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(Integer trangThai) {
		this.trangThai = trangThai;
	}
	public String getSoDH() {
		return soDH;
	}
	public void setSoDH(String soDH) {
		this.soDH = soDH;
	}
	public String getMaNV() {
		return maNV;
	}
	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}
	
	
}

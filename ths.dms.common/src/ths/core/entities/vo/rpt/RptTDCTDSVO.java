package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RptTDCTDSVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8204227312362971897L;

	private String staffCode;
	private String staffName;
	private BigDecimal sumQuantityPlan;
	private BigDecimal sumAmountPlan;
	private BigDecimal sumQuantityReal;
	private BigDecimal sumAmountReal;
	private List<RptTDCTDSVODetail> listDetail;

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public BigDecimal getSumQuantityPlan() {
		return sumQuantityPlan;
	}

	public void setSumQuantityPlan(BigDecimal sumQuantityPlan) {
		this.sumQuantityPlan = sumQuantityPlan;
	}

	public BigDecimal getSumAmountPlan() {
		return sumAmountPlan;
	}

	public void setSumAmountPlan(BigDecimal sumAmountPlan) {
		this.sumAmountPlan = sumAmountPlan;
	}

	public BigDecimal getSumQuantityReal() {
		return sumQuantityReal;
	}

	public void setSumQuantityReal(BigDecimal sumQuantityReal) {
		this.sumQuantityReal = sumQuantityReal;
	}

	public BigDecimal getSumAmountReal() {
		return sumAmountReal;
	}

	public void setSumAmountReal(BigDecimal sumAmountReal) {
		this.sumAmountReal = sumAmountReal;
	}

	public List<RptTDCTDSVODetail> getListDetail() {
		return listDetail;
	}

	public void setListDetail(List<RptTDCTDSVODetail> listDetail) {
		this.listDetail = listDetail;
	}

}

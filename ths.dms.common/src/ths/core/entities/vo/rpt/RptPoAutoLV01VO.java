package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ths.dms.core.common.utils.GroupUtility;

public class RptPoAutoLV01VO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String shopId;
	private String shopCode;
	private String shopName;
	private List<RptPoAutoLV02VO> listData;

	public RptPoAutoLV01VO() {
		listData = new ArrayList<RptPoAutoLV02VO>();
	}

	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public List<RptPoAutoLV02VO> getListData() {
		return listData;
	}

	public void setListData(List<RptPoAutoLV02VO> listData) {
		this.listData = listData;
	}

	@SuppressWarnings("unchecked")
	public static List<RptPoAutoLV01VO> groupBy(
			List<RptPoAutoLV02VO> listData, String fieldName)
			throws Exception {
		List<RptPoAutoLV01VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, fieldName);

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptPoAutoLV01VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptPoAutoLV01VO vo = new RptPoAutoLV01VO();
					List<RptPoAutoLV02VO> listObject = (List<RptPoAutoLV02VO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setListData(listObject);
						vo.setShopCode(listObject.get(0).getShopCode());

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class RptOrderDetailProduct1VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String productCode;
	private String productName;
	private BigDecimal price;
	private ArrayList<RptOrderDetailProduct2VO> lstRptOrderDetailProduct1VO = new ArrayList<RptOrderDetailProduct2VO>();

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public ArrayList<RptOrderDetailProduct2VO> getLstRptOrderDetailProduct1VO() {
		return lstRptOrderDetailProduct1VO;
	}

	public void setLstRptOrderDetailProduct1VO(
			ArrayList<RptOrderDetailProduct2VO> lstRptOrderDetailProduct1VO) {
		this.lstRptOrderDetailProduct1VO = lstRptOrderDetailProduct1VO;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}

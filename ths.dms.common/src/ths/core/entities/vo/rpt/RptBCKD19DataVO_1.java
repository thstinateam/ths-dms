package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD19DataVO_1 implements Serializable {
	private static final long serialVersionUID = 1L;

	private Float diemPA;
	private Integer nho3diem;

	public Float getDiemPA() {
		return diemPA;
	}
	public void setDiemPA(BigDecimal diemPA) {
		if (null != diemPA) {
			this.diemPA = diemPA.floatValue();
		}
	}
	public Integer getNho3diem() {
		return nho3diem;
	}
	public void setNho3diem(BigDecimal nho3diem) {
		if (null != nho3diem) {
			this.nho3diem = nho3diem.intValue();
		}
	}
}

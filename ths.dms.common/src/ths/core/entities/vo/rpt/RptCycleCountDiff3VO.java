package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptCycleCountDiff3VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cycleCountCode;// 1
	private String stockCardNumber;
	private String productCode;// 2
	private String productName;// 2.1
	private String lot;// 3
	private Date startDate;// 4
	private Integer quantityCounted;// 5
	private Integer quantityBeforeCount;// 6
	private BigDecimal price;// 7
	private Integer quantityDiff;// 8
	private BigDecimal amountDiff;// 9
	private String shopCode;
	private Integer checkLot;
	private String startDateString;
	private Integer sumQuantityCounted;
	private Integer sumQuantityBeforeCount;
	private Integer sumQuantityDiff;
	private Integer sumPrice;
	private Long sumAmount;
	private Integer sumAllQuantityCounted;
	private Integer sumAllQuantityBeforeCount;
	private Integer sumAllQuantityDiff;
	private Long sumaAllAmount;
	private Double percent;
	private Integer convfact;
	private String quantityCountedConvfact;
	private String quantityBeforeCountConvfact;
	private String quantityDiffConvfact;
	

	public Double getPercent() {
		return percent;
	}

	public void setPercent(Double percent) {
		this.percent = percent;
	}

	public Integer getSumAllQuantityCounted() {
		return sumAllQuantityCounted;
	}

	public void setSumAllQuantityCounted(Integer sumAllQuantityCounted) {
		this.sumAllQuantityCounted = sumAllQuantityCounted;
	}

	public Integer getSumAllQuantityBeforeCount() {
		return sumAllQuantityBeforeCount;
	}

	public void setSumAllQuantityBeforeCount(Integer sumAllQuantityBeforeCount) {
		this.sumAllQuantityBeforeCount = sumAllQuantityBeforeCount;
	}

	public Integer getSumAllQuantityDiff() {
		return sumAllQuantityDiff;
	}

	public void setSumAllQuantityDiff(Integer sumAllQuantityDiff) {
		this.sumAllQuantityDiff = sumAllQuantityDiff;
	}

	public Long getSumaAllAmount() {
		return sumaAllAmount;
	}

	public void setSumaAllAmount(Long sumaAllAmount) {
		this.sumaAllAmount = sumaAllAmount;
	}

	public Integer getSumQuantityBeforeCount() {
		return sumQuantityBeforeCount;
	}

	public void setSumQuantityBeforeCount(Integer sumQuantityBeforeCount) {
		this.sumQuantityBeforeCount = sumQuantityBeforeCount;
	}

	public Integer getSumQuantityDiff() {
		return sumQuantityDiff;
	}

	public void setSumQuantityDiff(Integer sumQuantityDiff) {
		this.sumQuantityDiff = sumQuantityDiff;
	}

	public Integer getSumPrice() {
		return sumPrice;
	}

	public void setSumPrice(Integer sumPrice) {
		this.sumPrice = sumPrice;
	}

	public Long getSumAmount() {
		return sumAmount;
	}

	public void setSumAmount(Long sumAmount) {
		this.sumAmount = sumAmount;
	}

	public Integer getSumQuantityCounted() {
		return sumQuantityCounted;
	}

	public void setSumQuantityCounted(Integer sumQuantityCounted) {
		this.sumQuantityCounted = sumQuantityCounted;
	}

	public String getStartDateString() {
		return startDateString;
	}

	public void setStartDateString(String startDateString) {
		this.startDateString = startDateString;
	}

	public String getCycleCountCode() {
		return cycleCountCode;
	}

	public void setCycleCountCode(String cycleCountCode) {
		this.cycleCountCode = cycleCountCode;
	}
	public String getStockCardNumber() {
		return stockCardNumber;
	}

	public void setStockCardNumber(String stockCardNumber) {
		this.stockCardNumber = stockCardNumber;
	}
	
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Integer getQuantityCounted() {
		return quantityCounted;
	}

	public void setQuantityCounted(Integer quantityCounted) {
		this.quantityCounted = quantityCounted;
	}

	public Integer getQuantityBeforeCount() {
		return quantityBeforeCount;
	}

	public void setQuantityBeforeCount(Integer quantityBeforeCount) {
		this.quantityBeforeCount = quantityBeforeCount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getQuantityDiff() {
		return quantityDiff;
	}

	public void setQuantityDiff(Integer quantityDiff) {
		this.quantityDiff = quantityDiff;
	}

	public BigDecimal getAmountDiff() {
		return amountDiff;
	}

	public void setAmountDiff(BigDecimal amountDiff) {
		this.amountDiff = amountDiff;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public String getQuantityCountedConvfact() {
		return quantityCountedConvfact;
	}

	public void setQuantityCountedConvfact(String quantityCountedConvfact) {
		this.quantityCountedConvfact = quantityCountedConvfact;
	}

	public String getQuantityBeforeCountConvfact() {
		return quantityBeforeCountConvfact;
	}

	public void setQuantityBeforeCountConvfact(String quantityBeforeCountConvfact) {
		this.quantityBeforeCountConvfact = quantityBeforeCountConvfact;
	}

	public String getQuantityDiffConvfact() {
		return quantityDiffConvfact;
	}

	public void setQuantityDiffConvfact(String quantityDiffConvfact) {
		this.quantityDiffConvfact = quantityDiffConvfact;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

}

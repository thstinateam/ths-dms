package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class Rpt_HO_Kho_F1_Tho implements Serializable{
	private static final long serialVersionUID = 1L;

	private String maNPP; 							// npp
	private String maSanPham; 						// san pham
	private Long tonDauKy; 				// ton dau ky
	private Long nhapT1; 
	private Long nhapT2; 
	private Long nhapT3; 
	private Long nhapT4; 
	private Long xuatT1;
	private Long xuatT2;
	private Long xuatT3;
	private Long xuatT4;
	private Long tonCuoiKy;
	private BigDecimal giaTriCuoiKy; 	// gia tri ton cuoi
	
	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getMaSanPham() {
		return maSanPham;
	}

	public void setMaSanPham(String maSanPham) {
		this.maSanPham = maSanPham;
	}

	public Long getTonDauKy() {
		return tonDauKy;
	}

	public void setTonDauKy(Long tonDauKy) {
		this.tonDauKy = tonDauKy;
	}

	public Long getNhapT1() {
		return nhapT1;
	}

	public void setNhapT1(Long nhapT1) {
		this.nhapT1 = nhapT1;
	}

	public Long getNhapT2() {
		return nhapT2;
	}

	public void setNhapT2(Long nhapT2) {
		this.nhapT2 = nhapT2;
	}

	public Long getNhapT3() {
		return nhapT3;
	}

	public void setNhapT3(Long nhapT3) {
		this.nhapT3 = nhapT3;
	}

	public Long getNhapT4() {
		return nhapT4;
	}

	public void setNhapT4(Long nhapT4) {
		this.nhapT4 = nhapT4;
	}

	public Long getXuatT1() {
		return xuatT1;
	}

	public void setXuatT1(Long xuatT1) {
		this.xuatT1 = xuatT1;
	}

	public Long getXuatT2() {
		return xuatT2;
	}

	public void setXuatT2(Long xuatT2) {
		this.xuatT2 = xuatT2;
	}

	public Long getXuatT3() {
		return xuatT3;
	}

	public void setXuatT3(Long xuatT3) {
		this.xuatT3 = xuatT3;
	}

	public Long getXuatT4() {
		return xuatT4;
	}

	public void setXuatT4(Long xuatT4) {
		this.xuatT4 = xuatT4;
	}

	public Long getTonCuoiKy() {
		return tonCuoiKy;
	}

	public void setTonCuoiKy(Long tonCuoiKy) {
		this.tonCuoiKy = tonCuoiKy;
	}

	public BigDecimal getGiaTriCuoiKy() {
		return giaTriCuoiKy;
	}

	public void setGiaTriCuoiKy(BigDecimal giaTriCuoiKy) {
		this.giaTriCuoiKy = giaTriCuoiKy;
	}

	public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}

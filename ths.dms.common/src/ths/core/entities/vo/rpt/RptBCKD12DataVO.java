package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD12DataVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String maMien;
	private String maVung;
	private String maNPP;
	private String maNVBH;
	private String maKH;
	private String tenKH;
	private String soNha;
	private String duong;
	private String phuongXa;
	private String quanHuyen;
	private String tinhThanh;
	private BigDecimal dsLuyKe;
	// private BigDecimal dsThangTruoc;
	private BigDecimal dsNamTruoc;
	private BigDecimal dsKeHoach;
	private Integer dsPhanTram;
	private Integer diem;

	public String getMaMien() {
		return maMien;
	}

	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}

	public String getMaVung() {
		return maVung;
	}

	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getMaNVBH() {
		return maNVBH;
	}

	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}

	public String getMaKH() {
		return maKH;
	}

	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}

	public String getTenKH() {
		return tenKH;
	}

	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}

	public String getSoNha() {
		return soNha;
	}

	public void setSoNha(String soNha) {
		this.soNha = soNha;
	}

	public String getDuong() {
		return duong;
	}

	public void setDuong(String duong) {
		this.duong = duong;
	}

	public String getPhuongXa() {
		return phuongXa;
	}

	public void setPhuongXa(String phuongXa) {
		this.phuongXa = phuongXa;
	}

	public String getQuanHuyen() {
		return quanHuyen;
	}

	public void setQuanHuyen(String quanHuyen) {
		this.quanHuyen = quanHuyen;
	}

	public String getTinhThanh() {
		return tinhThanh;
	}

	public void setTinhThanh(String tinhThanh) {
		this.tinhThanh = tinhThanh;
	}

	public BigDecimal getDsLuyKe() {
		return dsLuyKe;
	}

	public void setDsLuyKe(BigDecimal dsLuyKe) {
		this.dsLuyKe = dsLuyKe;
	}

//	public BigDecimal getDsThangTruoc() {
//		return dsThangTruoc;
//	}

//	public void setDsThangTruoc(BigDecimal dsThangTruoc) {
//		this.dsThangTruoc = dsThangTruoc;
//	}

	public BigDecimal getDsNamTruoc() {
		return dsNamTruoc;
	}

	public void setDsNamTruoc(BigDecimal dsNamTruoc) {
		this.dsNamTruoc = dsNamTruoc;
	}

	public BigDecimal getDsKeHoach() {
		return dsKeHoach;
	}

	public void setDsKeHoach(BigDecimal dsKeHoach) {
		this.dsKeHoach = dsKeHoach;
	}

	public Integer getDsPhanTram() {
		return dsPhanTram;
	}

	public void setDsPhanTram(BigDecimal dsPhanTram) {
		if (null != dsPhanTram) {
			this.dsPhanTram = dsPhanTram.intValue();
		} else {
			this.dsPhanTram = 0;
		}
	}

	public Integer getDiem() {
		return diem;
	}

	public void setDiem(BigDecimal diem) {
		if (null != diem) {
			this.diem = diem.intValue();
		} else {
			this.diem = 0;
		}
	}
}

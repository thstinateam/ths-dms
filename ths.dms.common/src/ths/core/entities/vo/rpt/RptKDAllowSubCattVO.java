package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptKDAllowSubCattVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String parentSuperShopCode;//ma mien
	private String superShopCode;//ma vung
	private String shopCode;//ma npp
	private String productInfoCode;//ma nganh hang
	private BigDecimal sumAmount;//tong ds
	private Integer sumCustomer;//tong khach hang
	
	public BigDecimal getSumAmount() {
		return sumAmount;
	}
	public void setSumAmount(BigDecimal sumAmount) {
		this.sumAmount = sumAmount;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getProductInfoCode() {
		return productInfoCode;
	}
	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}
//	public BigDecimal getSumAmount() {
//		return sumAmount;
//	}
//	public void setSumAmount(BigDecimal sumAmount) {
//		this.sumAmount = sumAmount;
//	}
	public String getSuperShopCode() {
		return superShopCode;
	}
	public void setSuperShopCode(String superShopCode) {
		this.superShopCode = superShopCode;
	}
	public String getParentSuperShopCode() {
		return parentSuperShopCode;
	}
	public void setParentSuperShopCode(String parentSuperShopCode) {
		this.parentSuperShopCode = parentSuperShopCode;
	}
	public Integer getSumCustomer() {
		return sumCustomer;
	}
	public void setSumCustomer(BigDecimal sumCustomer) {
		this.sumCustomer = sumCustomer.intValue();
	}
	
}

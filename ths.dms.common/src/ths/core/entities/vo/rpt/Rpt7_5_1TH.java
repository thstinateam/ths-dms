package ths.core.entities.vo.rpt;

import java.io.Serializable;

/**
 * VO bao cao tuoi no phai thu - tong hop
 * 
 * @author lacnv1
 * @since Mar 18, 2014
 */
public class Rpt7_5_1TH implements Serializable {

	private static final long serialVersionUID = 1L;

	private String maKH;
	private String tenKH;
	private Double chuaToiHan;
	private Double quaHan1Ngay;
	private Double quaHan30Ngay;
	private Double quaHan60Ngay;
	private Double quaHan90Ngay;
	private Double congNo;

	public String getTenKH() {
		return tenKH;
	}

	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}

	public Double getChuaToiHan() {
		return chuaToiHan;
	}

	public void setChuaToiHan(Double chuaToiHan) {
		this.chuaToiHan = chuaToiHan;
	}
	
	public String getMaKH() {
		return maKH;
	}

	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}

	public Double getCongNo() {
		return congNo;
	}

	public void setCongNo(Double congNo) {
		this.congNo = congNo;
	}

	public Double getQuaHan1Ngay() {
		return quaHan1Ngay;
	}

	public void setQuaHan1Ngay(Double quaHan1Ngay) {
		this.quaHan1Ngay = quaHan1Ngay;
	}

	public Double getQuaHan30Ngay() {
		return quaHan30Ngay;
	}

	public void setQuaHan30Ngay(Double quaHan30Ngay) {
		this.quaHan30Ngay = quaHan30Ngay;
	}

	public Double getQuaHan60Ngay() {
		return quaHan60Ngay;
	}

	public void setQuaHan60Ngay(Double quaHan60Ngay) {
		this.quaHan60Ngay = quaHan60Ngay;
	}

	public Double getQuaHan90Ngay() {
		return quaHan90Ngay;
	}

	public void setQuaHan90Ngay(Double quaHan90Ngay) {
		this.quaHan90Ngay = quaHan90Ngay;
	}
}
package ths.core.entities.vo.rpt;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author cangnd
 *
 */
public class Rpt_PO_CTTTVO implements java.io.Serializable
{
	private String ten_nhan_vien;
	private ArrayList<Rpt_VO_CTTTVO_INFO_DETAIL> danh_sach_chi_tiet;
	
	public Rpt_PO_CTTTVO()
	{
		danh_sach_chi_tiet = new ArrayList<Rpt_VO_CTTTVO_INFO_DETAIL>();
	}
	
	public Rpt_PO_CTTTVO(List<Rpt_PO_CTTT_MAPPINGVO> datas)
	{
		if(datas != null)
		{
			danh_sach_chi_tiet = new ArrayList<Rpt_VO_CTTTVO_INFO_DETAIL>();
			
			int iSize = datas.size();
			if(iSize > 0)
			{
				this.ten_nhan_vien = datas.get(0).getNvtt();
				for (Rpt_PO_CTTT_MAPPINGVO item : datas) 
				{
					Rpt_VO_CTTTVO_INFO_DETAIL dataConverting = new Rpt_VO_CTTTVO_INFO_DETAIL();
					dataConverting.setLoai_no(item.getLoai_no());
					dataConverting.setLoai_tt(item.getLoai_tt());
					dataConverting.setMa_kh(item.getMa_kh());
					dataConverting.setNgay_hop_dong(item.getNgay_hop_dong());
					dataConverting.setNgay_thanh_toan(item.getNgay_thanh_toan());
					dataConverting.setNvtt(item.getNvtt());
					dataConverting.setSo_cttt(item.getSo_cttt());
					dataConverting.setSo_hop_dong(item.getSo_hop_dong());
					dataConverting.setTien_hop_dong(item.getTien_hop_dong());
					dataConverting.setTien_tt(item.getTien_tt());
					
					danh_sach_chi_tiet.add(dataConverting);
				}
			}
		}
	}
	
	public Rpt_PO_CTTTVO(List<Rpt_PO_CTTT_MAPPINGVO> datas, Integer typePerson)
	{
		if(datas != null)
		{
			danh_sach_chi_tiet = new ArrayList<Rpt_VO_CTTTVO_INFO_DETAIL>();
			
			int iSize = datas.size();
			if(iSize > 0)
			{
				if(typePerson == 1) //NVBH
					this.ten_nhan_vien = datas.get(0).getNvtt();
				else
					this.ten_nhan_vien = datas.get(0).getMa_kh();
				
				for (Rpt_PO_CTTT_MAPPINGVO item : datas) 
				{
					Rpt_VO_CTTTVO_INFO_DETAIL dataConverting = new Rpt_VO_CTTTVO_INFO_DETAIL();
					dataConverting.setLoai_no(item.getLoai_no());
					dataConverting.setLoai_tt(item.getLoai_tt());
					dataConverting.setMa_kh(item.getMa_kh());
					dataConverting.setNgay_hop_dong(item.getNgay_hop_dong());
					dataConverting.setNgay_thanh_toan(item.getNgay_thanh_toan());
					dataConverting.setNvtt(item.getNvtt());
					dataConverting.setSo_cttt(item.getSo_cttt());
					dataConverting.setSo_hop_dong(item.getSo_hop_dong());
					dataConverting.setTien_hop_dong(item.getTien_hop_dong());
					dataConverting.setTien_tt(item.getTien_tt());
					
					danh_sach_chi_tiet.add(dataConverting);
				}
			}
		}
	}
	
	public String getTen_nhan_vien() {
		return ten_nhan_vien;
	}
	public void setTen_nhan_vien(String ten_nhan_vien) {
		this.ten_nhan_vien = ten_nhan_vien;
	}
	public List<Rpt_VO_CTTTVO_INFO_DETAIL> getDanh_sach_chi_tiet() {
		return danh_sach_chi_tiet;
	}
	public void setDanh_sach_chi_tiet(
			ArrayList<Rpt_VO_CTTTVO_INFO_DETAIL> danh_sach_chi_tiet) {
		this.danh_sach_chi_tiet = danh_sach_chi_tiet;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptCompareImExStF1VO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2479213854656879307L;
	private String subCat;
	private List<RptCompareImExStF1DataVO> data;

	public RptCompareImExStF1VO() {
		data = new ArrayList<RptCompareImExStF1DataVO>();
	}

	public String getSubCat() {
		return subCat;
	}

	public void setSubCat(String subCat) {
		this.subCat = subCat;
	}

	public List<RptCompareImExStF1DataVO> getData() {
		return data;
	}

	public void setData(List<RptCompareImExStF1DataVO> data) {
		this.data = data;
	}
}

package ths.core.entities.vo.rpt;

import java.util.ArrayList;
import java.util.List;

public class Rpt_TDXNTCT_NganhVO implements java.io.Serializable 
{
	private String ten_nganh;
	private ArrayList<Rpt_TDXNTCT_SPVO> DSSanPham;
	
	public Rpt_TDXNTCT_NganhVO()
	{
		DSSanPham = new ArrayList<Rpt_TDXNTCT_SPVO>();
	}
	
	public Rpt_TDXNTCT_NganhVO(List<Rpt_TDXNTCTVO> data)
	{
		DSSanPham = new ArrayList<Rpt_TDXNTCT_SPVO>();
		if(data != null && data.size() > 0)
		{
			this.ten_nganh = data.get(0).getMa_nganh();
			
		}
		
		for (Rpt_TDXNTCTVO item : data) 
		{
			Rpt_TDXNTCT_SPVO itemTDXNTCT = new Rpt_TDXNTCT_SPVO();
			
			itemTDXNTCT.setDon_gia(item.getDon_gia());
			itemTDXNTCT.setMa_hang(item.getMa_hang());
			itemTDXNTCT.setSo_luong_ban(item.getSo_luong_ban());
			itemTDXNTCT.setSo_luong_khuyen_mai(item.getSo_luong_khuyen_mai());
			itemTDXNTCT.setSo_luong_mua_hang(item.getSo_luong_mua_hang());
			itemTDXNTCT.setSo_luong_nhap_kho(item.getSo_luong_nhap_kho());
			itemTDXNTCT.setSo_luong_ton_cuoi(item.getSo_luong_ton_cuoi());
			itemTDXNTCT.setSo_luong_ton_dau(item.getSo_luong_ton_dau());
			itemTDXNTCT.setSo_luong_tra_hang(item.getSo_luong_ton_dau());
			itemTDXNTCT.setSo_luong_tra_hang(item.getSo_luong_tra_hang());
			itemTDXNTCT.setSo_luong_xuat_kho(item.getSo_luong_xuat_kho());
			itemTDXNTCT.setTen_hang(item.getTen_hang());
			itemTDXNTCT.setThung_le_ban(item.getThung_le_ban());
			itemTDXNTCT.setThung_le_khuyen_mai(item.getThung_le_khuyen_mai());
			itemTDXNTCT.setThung_le_mua_hang(item.getThung_le_mua_hang());
			itemTDXNTCT.setThung_le_nhap_kho(item.getThung_le_nhap_kho());
			itemTDXNTCT.setThung_le_ton_cuoi(item.getThung_le_ton_cuoi());
			itemTDXNTCT.setThung_le_ton_dau(item.getThung_le_ton_cuoi());
			itemTDXNTCT.setThung_le_tra_hang(item.getThung_le_tra_hang());
			itemTDXNTCT.setThung_le_xuat_kho(item.getThung_le_xuat_kho());
			itemTDXNTCT.setTong_tien_ban(item.getTong_tien_ban());
			itemTDXNTCT.setTong_tien_khuyen_mai(item.getTong_tien_khuyen_mai());
			itemTDXNTCT.setTong_tien_mua_hang(item.getTong_tien_mua_hang());
			itemTDXNTCT.setTong_tien_ton_cuoi(item.getTong_tien_cuoi());
			itemTDXNTCT.setTong_tien_ton_dau(item.getTong_tien_ton_dau());
			itemTDXNTCT.setTong_tien_tra_hang(item.getTong_tien_tra_hang());
			
			DSSanPham.add(itemTDXNTCT);
			
		}
	}
	
	public String getTen_nganh() 
	{
		return ten_nganh;
	}
	public void setTen_nganh(String ten_nganh) {
		this.ten_nganh = ten_nganh;
	}
	public ArrayList<Rpt_TDXNTCT_SPVO> getDSSanPham() {
		return DSSanPham;
	}
	public void setDSSanPham(ArrayList<Rpt_TDXNTCT_SPVO> dSSanPham) {
		DSSanPham = dSSanPham;
	}
	
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptBCTDTMTHVO implements Serializable {
	/**
	 * VO bao cao thiet bi
	 * 
	 * @author hoanv25
	 * @since May 05, 2014
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long equipRepairId;
	private Long id;

	private String parentShop; //kenh
	private String area; //khu vuc
	private String mien;
	private String vung;
	private String shopCode;
	private String shopName;
	private String numberCode;
	private String customerCode;
	private String customerName;
	private String dateEviction; // ngay thu hoi
	private String categoryName; // loai tu
	private String groupName; // loai tu
	private String brandName; // hieu
	private String capacity; //dung tich
	private String manufacturingYear; //nam san xuat
	private String equipCode; // ma tai san
	private String serialNumber; // ma tai san
	private String reason;
	private String statisticDate; //ngay kiem ke
	private String equipRepairCode;
	private String dateRepair;
	private String dateFirst;
	private String equipItem;
	private String equipRepairMonth;
	private String dateAmount;
	private String GSNPP;
	private String firstDateUse;
	
	/**
	 * KK1.1 moi
	 */
	private String kenh;
	private String supervise;
	private int isLevel;
	
	private BigDecimal amount;
	private BigDecimal totalAmount;
	private BigDecimal total;
	
	private Integer repairCount;	
	private Integer openStock; //ton kho dau ky
	private Integer closeStock; // ton kho cuoi ky
	private Integer closeLend; // sl muon cuoi ky
	private Integer openLend;// sl dau ky
	private Integer inLend; //sl trong ky
	private Integer totalEvic; //sl thu hoi thanh ly
	private Integer totalRepair; // sl yeu cau sua
	private Integer finishTotalRepair; //sl da sua
	private Integer lostPeriod; // sl mat trong ky
	private Integer statisticTime; //lan kiem ke
	private Integer quantityOversight; //so luong giam sat
	private Integer shelvesCompany; // u ke cong ty
	private Integer shelvesOversight;//u ke giam sat
	
//	private Integer totalQuantityCompany;
	private Integer quantityCompany; //so luong cong ty theo doi
//	private Integer totalQuantityChecking;
	private Integer quantityChecking;
//	private Integer totalDifficult;
	private Integer difficult;
	
	private List<DynamicVO> lstDynamic = new ArrayList<DynamicVO>();
	
	/** Khai bao method */
	public void safeSetNull() {
		//Set gia tri mac dinh cho cac truong null
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			//ghi log
		}
	}
	
	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getNumberCode() {
		return numberCode;
	}

	public void setNumberCode(String numberCode) {
		this.numberCode = numberCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDateEviction() {
		return dateEviction;
	}

	public void setDateEviction(String dateEviction) {
		this.dateEviction = dateEviction;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getManufacturingYear() {
		return manufacturingYear;
	}

	public void setManufacturingYear(String manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}

	public String getEquipCode() {
		return equipCode;
	}

	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getEquipRepairCode() {
		return equipRepairCode;
	}

	public void setEquipRepairCode(String equipRepairCode) {
		this.equipRepairCode = equipRepairCode;
	}

	public String getDateRepair() {
		return dateRepair;
	}

	public void setDateRepair(String dateRepair) {
		this.dateRepair = dateRepair;
	}

	public String getDateFirst() {
		return dateFirst;
	}

	public void setDateFirst(String dateFirst) {
		this.dateFirst = dateFirst;
	}

	public String getEquipItem() {
		return equipItem;
	}

	public void setEquipItem(String equipItem) {
		this.equipItem = equipItem;
	}

	public String getEquipRepairMonth() {
		return equipRepairMonth;
	}

	public void setEquipRepairMonth(String equipRepairMonth) {
		this.equipRepairMonth = equipRepairMonth;
	}

	public String getDateAmount() {
		return dateAmount;
	}

	public void setDateAmount(String dateAmount) {
		this.dateAmount = dateAmount;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getRepairCount() {
		return repairCount;
	}

	public void setRepairCount(Integer repairCount) {
		this.repairCount = repairCount;
	}

	public String getGSNPP() {
		return GSNPP;
	}

	public void setGSNPP(String gSNPP) {
		GSNPP = gSNPP;
	}

	public Long getEquipRepairId() {
		return equipRepairId;
	}

	public void setEquipRepairId(Long equipRepairId) {
		this.equipRepairId = equipRepairId;
	}

	public Integer getOpenStock() {
		return openStock;
	}

	public void setOpenStock(Integer openStock) {
		this.openStock = openStock;
	}

	public Integer getCloseStock() {
		return closeStock;
	}

	public void setCloseStock(Integer closeStock) {
		this.closeStock = closeStock;
	}

	public Integer getCloseLend() {
		return closeLend;
	}

	public void setCloseLend(Integer closeLend) {
		this.closeLend = closeLend;
	}

	public Integer getOpenLend() {
		return openLend;
	}

	public void setOpenLend(Integer openLend) {
		this.openLend = openLend;
	}

	public Integer getInLend() {
		return inLend;
	}

	public void setInLend(Integer inLend) {
		this.inLend = inLend;
	}

	public Integer getTotalEvic() {
		return totalEvic;
	}

	public void setTotalEvic(Integer totalEvic) {
		this.totalEvic = totalEvic;
	}

	public Integer getTotalRepair() {
		return totalRepair;
	}

	public void setTotalRepair(Integer totalRepair) {
		this.totalRepair = totalRepair;
	}

	public Integer getFinishTotalRepair() {
		return finishTotalRepair;
	}

	public void setFinishTotalRepair(Integer finishTotalRepair) {
		this.finishTotalRepair = finishTotalRepair;
	}

	public Integer getLostPeriod() {
		return lostPeriod;
	}

	public void setLostPeriod(Integer lostPeriod) {
		this.lostPeriod = lostPeriod;
	}

	public String getParentShop() {
		return parentShop;
	}

	public void setParentShop(String parentShop) {
		this.parentShop = parentShop;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Integer getStatisticTime() {
		return statisticTime;
	}

	public void setStatisticTime(Integer statisticTime) {
		this.statisticTime = statisticTime;
	}

	public String getStatisticDate() {
		return statisticDate;
	}

	public void setStatisticDate(String statisticDate) {
		this.statisticDate = statisticDate;
	}

	public Integer getQuantityCompany() {
		return quantityCompany;
	}

	public void setQuantityCompany(Integer quantityCompany) {
		this.quantityCompany = quantityCompany;
	}

	public Integer getShelvesCompany() {
		return shelvesCompany;
	}

	public void setShelvesCompany(Integer shelvesCompany) {
		this.shelvesCompany = shelvesCompany;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getShelvesOversight() {
		return shelvesOversight;
	}

	public void setShelvesOversight(Integer shelvesOversight) {
		this.shelvesOversight = shelvesOversight;
	}

	public Integer getQuantityOversight() {
		return quantityOversight;
	}

	public void setQuantityOversight(Integer quantityOversight) {
		this.quantityOversight = quantityOversight;
	}

	public String getFirstDateUse() {
		return firstDateUse;
	}

	public void setFirstDateUse(String firstDateUse) {
		this.firstDateUse = firstDateUse;
	}

	public List<DynamicVO> getLstDynamic() {
		return lstDynamic;
	}

	public void setLstDynamic(List<DynamicVO> lstDynamic) {
		this.lstDynamic = lstDynamic;
	}

	/** @return the kenh */
	public String getKenh() {
		return kenh;
	}

	/** @param kenh the kenh to set */
	public void setKenh(String kenh) {
		this.kenh = kenh;
	}

	/** @return giam sat */
	public String getSupervise() {
		return supervise;
	}

	/** @param supervise the supervise to set */
	public void setSupervise(String supervise) {
		this.supervise = supervise;
	}

	/** @return so luong kiem ke thuc te */
	public Integer getQuantityChecking() {
		return quantityChecking;
	}

	/** @param set so luong kiem ke thuc te*/
	public void setQuantityChecking(Integer quantityChecking) {
		this.quantityChecking = quantityChecking;
	}

	/** @return chenh lech */
	public Integer getDifficult() {
		return difficult;
	}

	/** @param set chenh lech*/
	public void setDifficult(Integer difficult) {
		this.difficult = difficult;
	}

	/** @return the isLevel */
	public int getIsLevel() {
		return isLevel;
	}

	/** @param isLevel the isLevel to set */
	public void setIsLevel(int isLevel) {
		this.isLevel = isLevel;
	}
}

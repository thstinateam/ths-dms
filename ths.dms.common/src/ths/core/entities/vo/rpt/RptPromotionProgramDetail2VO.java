package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

public class RptPromotionProgramDetail2VO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String promotionCode;
	
	private List<RptPromotionProgramDetail1VO> lstRptPromotionProgramDetail1VO;

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public List<RptPromotionProgramDetail1VO> getLstRptPromotionProgramDetail1VO() {
		return lstRptPromotionProgramDetail1VO;
	}

	public void setLstRptPromotionProgramDetail1VO(
			List<RptPromotionProgramDetail1VO> lstRptPromotionProgramDetail1VO) {
		this.lstRptPromotionProgramDetail1VO = lstRptPromotionProgramDetail1VO;
	}
	
	
	
}

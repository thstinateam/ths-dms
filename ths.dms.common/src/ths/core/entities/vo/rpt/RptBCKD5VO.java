package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD5VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String shopName;// mien
	private String cat;// nganh hang // Nhom
	private String subCat;// nganh hang con // Nhan
	private String productCode; // ma san pham
	private String productName; // ten san pham
	private BigDecimal planAmount;// ke hoach thang // khtt
	private BigDecimal khLuyKe; // kh luy ke
	private BigDecimal khBQNgay; // kh BQ Ngay
	private BigDecimal orderAmount;// luy ke cua thang nay // TH luy ke
	// TH trong ngay
	private BigDecimal orderToDateAmount;// doanh so cua ngay hom nay
	private BigDecimal previorAmount;// luy ke cua thang truoc // LK thang truoc
	// Tong thang truoc
	private BigDecimal previorMonthAmount;// doanh so cua thang truoc
	private BigDecimal duThieuLuyKe;// Du / Thieu luy ke
	private BigDecimal phanTramThucHien;// % thuc hien

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getSubCat() {
		return subCat;
	}

	public void setSubCat(String subCat) {
		this.subCat = subCat;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPlanAmount() {
		return planAmount;
	}

	public void setPlanAmount(BigDecimal planAmount) {
		this.planAmount = planAmount;
	}

	public BigDecimal getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(BigDecimal orderAmount) {
		this.orderAmount = orderAmount;
	}

	public BigDecimal getOrderToDateAmount() {
		return orderToDateAmount;
	}

	public void setOrderToDateAmount(BigDecimal orderToDateAmount) {
		this.orderToDateAmount = orderToDateAmount;
	}

	public BigDecimal getPreviorAmount() {
		return previorAmount;
	}

	public void setPreviorAmount(BigDecimal previorAmount) {
		this.previorAmount = previorAmount;
	}

	public BigDecimal getPreviorMonthAmount() {
		return previorMonthAmount;
	}

	public void setPreviorMonthAmount(BigDecimal previorMonthAmount) {
		this.previorMonthAmount = previorMonthAmount;
	}

	public BigDecimal getKhLuyKe() {
		return khLuyKe;
	}

	public void setKhLuyKe(BigDecimal khLuyKe) {
		this.khLuyKe = khLuyKe;
	}

	public BigDecimal getKhBQNgay() {
		return khBQNgay;
	}

	public void setKhBQNgay(BigDecimal khBQNgay) {
		this.khBQNgay = khBQNgay;
	}

	public BigDecimal getDuThieuLuyKe() {
		return duThieuLuyKe;
	}

	public void setDuThieuLuyKe(BigDecimal duThieuLuyKe) {
		this.duThieuLuyKe = duThieuLuyKe;
	}

	public BigDecimal getPhanTramThucHien() {
		return phanTramThucHien;
	}

	public void setPhanTramThucHien(BigDecimal phanTramThucHien) {
		this.phanTramThucHien = phanTramThucHien;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
}

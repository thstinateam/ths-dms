package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

import ths.dms.core.entities.enumtype.PoVNMStatus;

public class RptPoStatusTrackingVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String productCode;
	
	private String productName;
	
	private BigDecimal price;
	
	private String unit = "T/L";
	
	private Integer convfact;
	
	private Integer poQuantity;
	
	private Integer receivedQuantity;
	
	private Integer returnedQuantity;
	
	private BigDecimal poAmount;

	private BigDecimal returnedAmount;
	
	private BigDecimal receivedAmount;
	
	private String poAutoNumber;
	
	private Date poAutoDate;
	
	private Integer status;
	
	private String shopName = "VNM-Vinamilk";
	
	private Integer type;
	
	public void safeSetNull() throws Exception {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	public Integer getPoQuantity() {
		return poQuantity;
	}

	public void setPoQuantity(Integer poQuantity) {
		this.poQuantity = poQuantity;
	}

	public BigDecimal getPoAmount() {
		return poAmount;
	}

	public void setPoAmount(BigDecimal poAmount) {
		this.poAmount = poAmount;
	}

	public BigDecimal getReturnedAmount() {
		return returnedAmount;
	}

	public void setReturnedAmount(BigDecimal returnedAmount) {
		this.returnedAmount = returnedAmount;
	}

	public Date getPoAutoDate() {
		return poAutoDate;
	}

	public void setPoAutoDate(Date poAutoDate) {
		this.poAutoDate = poAutoDate;
	}


	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Integer getOrderedQuantity() {
		return poQuantity;
	}

	public void setOrderedQuantity(Integer orderedQuantity) {
		this.poQuantity = orderedQuantity;
	}

	public Integer getReceivedQuantity() {
		return receivedQuantity;
	}

	public void setReceivedQuantity(Integer receivedQuantity) {
		this.receivedQuantity = receivedQuantity;
	}

	public String getReturnedQuantity() {
		if (returnedQuantity != null) {
			Integer thung = returnedQuantity/convfact;
			Integer le = returnedQuantity%convfact;
			return thung.toString() + "/" + le.toString();
		}
		return "";
	}

	public void setReturnedQuantity(Integer returnedQuantity) {
		this.returnedQuantity = returnedQuantity;
	}

	public BigDecimal getOrderedTotal() {
		return poAmount;
	}

	public void setOrderedTotal(BigDecimal orderedTotal) {
		this.poAmount = orderedTotal;
	}

	public BigDecimal getReturnedTotal() {
		return returnedAmount;
	}

	public void setReturnedTotal(BigDecimal returnedTotal) {
		this.returnedAmount = returnedTotal;
	}

	public String getPoAutoNumber() {
		return poAutoNumber;
	}

	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}

	public PoVNMStatus getStatus() {
		return PoVNMStatus.parseValue(status);
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public BigDecimal getThucNhan() {
		if (price == null || receivedQuantity == null)
			return BigDecimal.ZERO;
		return price.multiply(new BigDecimal(receivedQuantity));
	}
	public BigDecimal getReceivedAmount() {
		return receivedAmount;
	}
	public void setReceivedAmount(BigDecimal receivedAmount) {
		this.receivedAmount = receivedAmount;
	}
	public String getSlDat() {
		String slDat = "";
		int a = (int)this.poQuantity/this.convfact;
		int b = this.poQuantity % this.convfact;
		slDat = this.unit.equals( "T/L" ) && this.convfact != null && this.convfact != 0 ? a + "/" + b : this.poQuantity + "";
		return slDat;
	}
	public String getSlNhan() {
		String slNhan = "";
		int a = (int)this.receivedQuantity / this.convfact;
		int b = this.receivedQuantity % this.convfact;
		slNhan = this.unit.equals( "T/L" ) && this.convfact != null && this.convfact != 0 ? a + "/" + b : this.receivedQuantity+"";
		return slNhan;
	}
	public String getSlTra() {
		String slTra;
		int a = (int)this.returnedQuantity / this.convfact;
		int b = this.returnedQuantity % this.convfact;
		slTra = this.unit.equals( "T/L" ) && this.convfact != null && this.convfact != 0 ? a + "/" + b : this.returnedQuantity + "";
		return slTra;
	}
}

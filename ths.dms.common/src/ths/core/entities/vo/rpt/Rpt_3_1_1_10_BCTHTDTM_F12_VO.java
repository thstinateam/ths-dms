/*
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Bao cao F12 Xuat nhap ton thiet bi tai NPP
 * 
 * @author trietptm
 * @since Apr 12, 2016
 */
public class Rpt_3_1_1_10_BCTHTDTM_F12_VO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String regionCode;
	private String areaCode;
	private String shopCode;
	private String shopName;
	private String categoryName;
	private String groupName;
	private String brandName;
	private String capacity;
	private BigDecimal openShop;
	private BigDecimal importShop;
	private BigDecimal transferInShop;
	private BigDecimal evictionShop;
	private BigDecimal lostShop;
	private BigDecimal liquidationShop;
	private BigDecimal transferOutShop;
	private BigDecimal closeShop;
	private BigDecimal openCus;
	private BigDecimal importCus;
	private BigDecimal evictionCus;
	private BigDecimal lostCus;
	private BigDecimal liquidationCus;
	private BigDecimal closeCus;
	
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public BigDecimal getOpenShop() {
		return openShop;
	}
	public void setOpenShop(BigDecimal openShop) {
		this.openShop = openShop;
	}
	public BigDecimal getImportShop() {
		return importShop;
	}
	public void setImportShop(BigDecimal importShop) {
		this.importShop = importShop;
	}
	public BigDecimal getTransferInShop() {
		return transferInShop;
	}
	public void setTransferInShop(BigDecimal transferInShop) {
		this.transferInShop = transferInShop;
	}
	public BigDecimal getEvictionShop() {
		return evictionShop;
	}
	public void setEvictionShop(BigDecimal evictionShop) {
		this.evictionShop = evictionShop;
	}
	public BigDecimal getLostShop() {
		return lostShop;
	}
	public void setLostShop(BigDecimal lostShop) {
		this.lostShop = lostShop;
	}
	public BigDecimal getLiquidationShop() {
		return liquidationShop;
	}
	public void setLiquidationShop(BigDecimal liquidationShop) {
		this.liquidationShop = liquidationShop;
	}
	public BigDecimal getTransferOutShop() {
		return transferOutShop;
	}
	public void setTransferOutShop(BigDecimal transferOutShop) {
		this.transferOutShop = transferOutShop;
	}
	public BigDecimal getCloseShop() {
		return closeShop;
	}
	public void setCloseShop(BigDecimal closeShop) {
		this.closeShop = closeShop;
	}
	public BigDecimal getOpenCus() {
		return openCus;
	}
	public void setOpenCus(BigDecimal openCus) {
		this.openCus = openCus;
	}
	public BigDecimal getImportCus() {
		return importCus;
	}
	public void setImportCus(BigDecimal importCus) {
		this.importCus = importCus;
	}
	public BigDecimal getEvictionCus() {
		return evictionCus;
	}
	public void setEvictionCus(BigDecimal evictionCus) {
		this.evictionCus = evictionCus;
	}
	public BigDecimal getLostCus() {
		return lostCus;
	}
	public void setLostCus(BigDecimal lostCus) {
		this.lostCus = lostCus;
	}
	public BigDecimal getLiquidationCus() {
		return liquidationCus;
	}
	public void setLiquidationCus(BigDecimal liquidationCus) {
		this.liquidationCus = liquidationCus;
	}
	public BigDecimal getCloseCus() {
		return closeCus;
	}
	public void setCloseCus(BigDecimal closeCus) {
		this.closeCus = closeCus;
	}
}
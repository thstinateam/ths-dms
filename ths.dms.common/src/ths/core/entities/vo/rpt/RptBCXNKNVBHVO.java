package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RptBCXNKNVBHVO implements Serializable{
	
	private BigDecimal shopId;	
	private String stockTransDate;
	private List<RptBCXNKNVBHStaffVO> lstDataStaffFollowDate;	
	
	public BigDecimal getShopId() {
		return shopId;
	}

	public void setShopId(BigDecimal shopId) {
		this.shopId = shopId;
	}

	public String getStockTransDate() {
		return stockTransDate;
	}

	public void setStockTransDate(String stockTransDate) {
		this.stockTransDate = stockTransDate;
	}	

	public List<RptBCXNKNVBHStaffVO> getLstDataStaffFollowDate() {
		return lstDataStaffFollowDate;
	}

	public void setLstDataStaffFollowDate(
			List<RptBCXNKNVBHStaffVO> lstDataStaffFollowDate) {
		this.lstDataStaffFollowDate = lstDataStaffFollowDate;
	}

	
		
//	private static final long serialVersionUID = 1L;
//	
//	private String staffInfo;
//	
//	private Long stockTransId;
//	
//	private String stockTransCode;
//	
//	private BigDecimal amount;
//	
//	private Date stockTransDate;
//	
//	private List<RptBCXNKNVBHDetailVO> lstDetail;
//
//	public String getStaffInfo() {
//		return staffInfo;
//	}
//
//	public void setStaffInfo(String staffInfo) {
//		this.staffInfo = staffInfo;
//	}
//
//	public Long getStockTransId() {
//		return stockTransId;
//	}
//
//	public void setStockTransId(Long stockTransId) {
//		this.stockTransId = stockTransId;
//	}
//
//	public String getStockTransCode() {
//		return stockTransCode;
//	}
//
//	public void setStockTransCode(String stockTransCode) {
//		this.stockTransCode = stockTransCode;
//	}
//
//	public BigDecimal getAmount() {
//		return amount;
//	}
//
//	public void setAmount(BigDecimal amount) {
//		this.amount = amount;
//	}
//
//	public List<RptBCXNKNVBHDetailVO> getLstDetail() {
//		return lstDetail;
//	}
//
//	public void setLstDetail(List<RptBCXNKNVBHDetailVO> lstDetail) {
//		this.lstDetail = lstDetail;
//	}
//
//	public Date getStockTransDate() {
//		return stockTransDate;
//	}
//
//	public void setStockTransDate(Date stockTransDate) {
//		this.stockTransDate = stockTransDate;
//	}	
}

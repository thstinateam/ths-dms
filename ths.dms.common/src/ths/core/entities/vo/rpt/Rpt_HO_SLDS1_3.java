package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class Rpt_HO_SLDS1_3 implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8921340805150425581L;
	private String mien;
	private String vung;
	private String shopCode;
	private String shopName;
	private String staffCode;
	private String staffName;
	private String catName;
	private BigDecimal quantity;
	private BigDecimal amount;
	private BigDecimal quantityPlan;
	private BigDecimal amountPlan;
	
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getQuantityPlan() {
		return quantityPlan;
	}
	public void setQuantityPlan(BigDecimal quantityPlan) {
		this.quantityPlan = quantityPlan;
	}
	public BigDecimal getAmountPlan() {
		return amountPlan;
	}
	public void setAmountPlan(BigDecimal amountPlan) {
		this.amountPlan = amountPlan;
	}
}

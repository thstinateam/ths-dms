package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptCustomerPayReceivedVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long shopId;
	private String customerCode;
	private String customerName;
	private Date orderDate;
	private String orderNumber;
	private Date payDate;
	private Integer paymentType;
	private String payReceivedNumber;
	private BigDecimal total;
	private BigDecimal remain;
	private String cashierCode;
	private String cashierName;
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Date getPayDate() {
		return payDate;
	}
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	public Integer getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}
	public String getPayReceivedNumber() {
		return payReceivedNumber;
	}
	public void setPayReceivedNumber(String payReceivedNumber) {
		this.payReceivedNumber = payReceivedNumber;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getRemain() {
		return remain;
	}
	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}
	public String getCashierCode() {
		return cashierCode;
	}
	public void setCashierCode(String cashierCode) {
		this.cashierCode = cashierCode;
	}
	public String getCashierName() {
		return cashierName;
	}
	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}
	
	
}

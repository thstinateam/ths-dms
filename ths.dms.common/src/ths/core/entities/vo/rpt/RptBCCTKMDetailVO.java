package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptBCCTKMDetailVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long shopId;
	private String shopName;
	private String shopAddress;
	
	private String promotionProgramCode;
	private String promotionProgramDecription;
	private Integer totalQuantitySale = 0;
	private Integer totalQuantityFree = 0;
	private List<RptBCCTKM_RecordDetailVO> lstBCCTKM_RecordDetailVO = new ArrayList<RptBCCTKM_RecordDetailVO>();
	
	
	public Long getShopId() {
		return shopId;
	}
	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}
	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}
	public String getPromotionProgramDecription() {
		return promotionProgramDecription;
	}
	public void setPromotionProgramDecription(String promotionProgramDecription) {
		this.promotionProgramDecription = promotionProgramDecription;
	}
	public Integer getTotalQuantitySale() {
		return totalQuantitySale;
	}
	public void setTotalQuantitySale(Integer totalQuantitySale) {
		this.totalQuantitySale = totalQuantitySale;
	}
	public Integer getTotalQuantityFree() {
		return totalQuantityFree;
	}
	public void setTotalQuantityFree(Integer totalQuantityFree) {
		this.totalQuantityFree = totalQuantityFree;
	}
	public List<RptBCCTKM_RecordDetailVO> getLstBCCTKM_RecordDetailVO() {
		return lstBCCTKM_RecordDetailVO;
	}
	public void setLstBCCTKM_RecordDetailVO(
			List<RptBCCTKM_RecordDetailVO> lstBCCTKM_RecordDetailVO) {
		this.lstBCCTKM_RecordDetailVO = lstBCCTKM_RecordDetailVO;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class ColumnSaleMoneyAndBooleanVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String columnName;
	private BigDecimal columnValue;
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public BigDecimal getColumnValue() {
		return columnValue;
	}
	public void setColumnValue(BigDecimal columnValue) {
		this.columnValue = columnValue;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptStockTransInMoveVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private BigDecimal price;
	private String productCode;
	private String productName;
	private Integer convfact;
	private String uom2;
	private Integer quantity;
	private BigDecimal toMoney;
	private BigDecimal totalQuantity;
	private String strQuantity;
	
	public BigDecimal getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(BigDecimal totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		if(productCode!=null && productCode!=""){
			this.productCode = productCode;
		}else{
			this.productCode = "";
		}
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		if(productName!=null && productName!=""){
			this.productName = productName;
		}else{
			this.productName = "";
		}
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public String getUom2() {
		return uom2;
	}
	public void setUom2(String uom2) {
		this.uom2 = uom2;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getToMoney() {
		return toMoney;
	}
	public void setToMoney(BigDecimal toMoney) {
		this.toMoney = toMoney;
	}
	public String getStrQuantity() {
		return strQuantity;
	}
	public void setStrQuantity(String strQuantity) {
		this.strQuantity = strQuantity;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class RptPayDetailDisplayProgr1ParentVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<RptPayDetailDisplayProgr1VO> lstRptPayDetailDisplayProgr1VO = new ArrayList<RptPayDetailDisplayProgr1VO>();

	//private DisplayProgram displayProgram;

	/**
	 * @return the lstRptPayDetailDisplayProgr1VO
	 */
	public List<RptPayDetailDisplayProgr1VO> getLstRptPayDetailDisplayProgr1VO() {
		return lstRptPayDetailDisplayProgr1VO;
	}

	/**
	 * @param lstRptPayDetailDisplayProgr1VO the lstRptPayDetailDisplayProgr1VO to set
	 */
	public void setLstRptPayDetailDisplayProgr1VO(
			List<RptPayDetailDisplayProgr1VO> lstRptPayDetailDisplayProgr1VO) {
		this.lstRptPayDetailDisplayProgr1VO = lstRptPayDetailDisplayProgr1VO;
	}

	/**
	 * @return the displayProgram
	 */
//	public DisplayProgram getDisplayProgram() {
//		return displayProgram;
//	}
//
//	/**
//	 * @param displayProgram the displayProgram to set
//	 */
//	public void setDisplayProgram(DisplayProgram displayProgram) {
//		this.displayProgram = displayProgram;
//	}

}

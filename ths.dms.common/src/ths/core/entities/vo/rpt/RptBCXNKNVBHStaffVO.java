package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RptBCXNKNVBHStaffVO implements Serializable{
		
	private BigDecimal staffId;
	private String staffCode;
	private String staffName;	
	
	private String stockTransCode;
	
	//private BigDecimal sumAmount;
	
	private List<RptBCXNKNVBHDetailVO> lstDataProductFollowStaff;
	
	public BigDecimal getStaffId() {
		return staffId;
	}

	public void setStaffId(BigDecimal staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStockTransCode() {
		return stockTransCode;
	}

	public void setStockTransCode(String stockTransCode) {
		this.stockTransCode = stockTransCode;
	}

//	public BigDecimal getSumAmount() {
//		return sumAmount;
//	}
//
//	public void setSumAmount(BigDecimal sumAmount) {
//		this.sumAmount = sumAmount;
//	}

	public List<RptBCXNKNVBHDetailVO> getLstDataProductFollowStaff() {
		return lstDataProductFollowStaff;
	}

	public void setLstDataProductFollowStaff(
			List<RptBCXNKNVBHDetailVO> lstDataProductFollowStaff) {
		this.lstDataProductFollowStaff = lstDataProductFollowStaff;
	}	
		
	
//	private static final long serialVersionUID = 1L;
//	
//	private String staffInfo;
//	
//	private Long stockTransId;
//	
//	private String stockTransCode;
//	
//	private BigDecimal amount;
//	
//	private Date stockTransDate;
//	
//	private List<RptBCXNKNVBHDetailVO> lstDetail;
//
//	public String getStaffInfo() {
//		return staffInfo;
//	}
//
//	public void setStaffInfo(String staffInfo) {
//		this.staffInfo = staffInfo;
//	}
//
//	public Long getStockTransId() {
//		return stockTransId;
//	}
//
//	public void setStockTransId(Long stockTransId) {
//		this.stockTransId = stockTransId;
//	}
//
//	public String getStockTransCode() {
//		return stockTransCode;
//	}
//
//	public void setStockTransCode(String stockTransCode) {
//		this.stockTransCode = stockTransCode;
//	}
//
//	public BigDecimal getAmount() {
//		return amount;
//	}
//
//	public void setAmount(BigDecimal amount) {
//		this.amount = amount;
//	}
//
//	public List<RptBCXNKNVBHDetailVO> getLstDetail() {
//		return lstDetail;
//	}
//
//	public void setLstDetail(List<RptBCXNKNVBHDetailVO> lstDetail) {
//		this.lstDetail = lstDetail;
//	}
//
//	public Date getStockTransDate() {
//		return stockTransDate;
//	}
//
//	public void setStockTransDate(Date stockTransDate) {
//		this.stockTransDate = stockTransDate;
//	}	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptPoVnmVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * so chung tu
	 */
	private String poCoNumber;
	
	private Date poVnmDate;
	
	private String shopName = "Vinamilk";
	
	/**
	 * ma so thue
	 */
	private String taxNumber;
	
	private String description = "Hàng tổng hợp";
	
	private BigDecimal total;
	
	private BigDecimal vatAmount;

	public String getPoCoNumber() {
		return poCoNumber;
	}

	public void setPoCoNumber(String poCoNumber) {
		this.poCoNumber = poCoNumber;
	}

	public Date getPoVnmDate() {
		return poVnmDate;
	}

	public void setPoVnmDate(Date poVnmDate) {
		this.poVnmDate = poVnmDate;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}
}

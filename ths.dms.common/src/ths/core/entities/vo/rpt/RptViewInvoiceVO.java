package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptViewInvoiceVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Date orderDate;
	private String cpnyName;
	private String cpnyAddr;
	private String cpnyBankAccount;
	private String deliveryName;
	private String selName;
	private String orderNumber;
	private String custName;
	private String custTaxNumber;
	private String customerCode;
	private String customerAddr;
	private Integer custPayment;
	private String custDelAddr;
	private String custBankAccount;
	private String custBankName;
	private BigDecimal discount;
	private Float vat;
	private BigDecimal amount;
	private BigDecimal taxAmount;
	private String staffCode;
	private String staffName;
	private String invoiceNumber;
	
	//id san pham
	private Long productId;
	//gia
	private Float price;
	//gia chua vat
	private Float priceNotVat;
	//so luong
	private Integer quantity;
	//ma san pham
	private String productCode;
	//ten san pham
	private String productName;
	//don vi tinh
	private String uom1;
	//don vi tinh 2
	private String uom2;
	//convfact
	private Integer convfact;
	//thanh tien
	private BigDecimal toMoney;
	private BigDecimal totalAmount;
	private Integer isFreeItem;
	
	private int typeRow;	
	
	public String getCpnyName() {
		return cpnyName;
	}
	public void setCpnyName(String cpnyName) {
		this.cpnyName = cpnyName;
	}
	public String getCpnyAddr() {
		return cpnyAddr;
	}
	public void setCpnyAddr(String cpnyAddr) {
		this.cpnyAddr = cpnyAddr;
	}
	public String getCpnyBankAccount() {
		return cpnyBankAccount;
	}
	public void setCpnyBankAccount(String cpnyBankAccount) {
		this.cpnyBankAccount = cpnyBankAccount;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public String getSelName() {
		return selName;
	}
	public void setSelName(String selName) {
		this.selName = selName;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustTaxNumber() {
		return custTaxNumber;
	}
	public void setCustTaxNumber(String custTaxNumber) {
		this.custTaxNumber = custTaxNumber;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerAddr() {
		return customerAddr;
	}
	public void setCustomerAddr(String customerAddr) {
		this.customerAddr = customerAddr;
	}
	public Integer getCustPayment() {
		return custPayment;
	}
	public void setCustPayment(Integer custPayment) {
		this.custPayment = custPayment;
	}
	public String getCustDelAddr() {
		return custDelAddr;
	}
	public void setCustDelAddr(String custDelAddr) {
		this.custDelAddr = custDelAddr;
	}
	public String getCustBankAccount() {
		return custBankAccount;
	}
	public void setCustBankAccount(String custBankAccount) {
		this.custBankAccount = custBankAccount;
	}
	public String getCustBankName() {
		return custBankName;
	}
	public void setCustBankName(String custBankName) {
		this.custBankName = custBankName;
	}
	public Float getVat() {
		return vat;
	}
	public void setVat(Float vat) {
		this.vat = vat;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Float getPriceNotVat() {
		return priceNotVat;
	}
	public void setPriceNotVat(Float priceNotVat) {
		this.priceNotVat = priceNotVat;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getProductCode() {
		return productCode == null?"":productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getUom1() {
		return uom1;
	}
	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}
	public BigDecimal getToMoney() {
		return toMoney;
	}
	public void setToMoney(BigDecimal toMoney) {
		this.toMoney = toMoney;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	/**
	 * @return the orderDate
	 */
	public Date getOrderDate() {
		return orderDate;
	}
	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	/**
	 * @return the isFreeItem
	 */
	public Integer getIsFreeItem() {
		return isFreeItem;
	}
	/**
	 * @param isFreeItem the isFreeItem to set
	 */
	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}
	/**
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}
	/**
	 * @param staffCode the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	/**
	 * @return the staffName
	 */
	public String getStaffName() {
		return staffName;
	}
	/**
	 * @param staffName the staffName to set
	 */
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	/**
	 * @param invoiceNumber the invoiceNumber to set
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public int getTypeRow() {
		return typeRow;
	}
	public void setTypeRow(int typeRow) {
		this.typeRow = typeRow;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public String getUom2() {
		return uom2;
	}
	public void setUom2(String uom2) {
		this.uom2 = uom2;
	}	
	
	public String getDescription(){
		String res = "";
		if(convfact!= null && convfact > 0 && quantity != null){
			int numUOM1 = (int)quantity/convfact;
			int numUOM2 = quantity%convfact;
			StringBuilder sbRes = new StringBuilder();
			if(numUOM1 > 0){
				sbRes = sbRes.append(String.valueOf(numUOM1)).append(uom2);
			}
			if(numUOM1 > 0 && numUOM2 > 0){
				sbRes = sbRes.append("-");
			}
			if(numUOM2 > 0){
				sbRes = sbRes.append(numUOM2).append(uom1);
			}
			res = sbRes.toString();
		}
		return res;
	}
}

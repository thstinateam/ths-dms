package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD11_2CTVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String mien;
	private String vung;
	private String gsnpp;
	private Integer alphaKHTG;
	private Integer alphaKHDat;
	private Float alphaPT;
	private Integer alphaDiem;
	private Integer vnmKHTG;
	private Integer vnmKHDat;
	private Float vnmPT;
	private Integer vnmDiem;
	private Integer diemPA;
	
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getGsnpp() {
		return gsnpp;
	}
	public void setGsnpp(String gsnpp) {
		this.gsnpp = gsnpp;
	}
	public Integer getAlphaKHTG() {
		return alphaKHTG;
	}
	public void setAlphaKHTG(BigDecimal alphaKHTG) {
		this.alphaKHTG = alphaKHTG.intValue();
	}
	public Integer getAlphaKHDat() {
		return alphaKHDat;
	}
	public void setAlphaKHDat(BigDecimal alphaKHDat) {
		this.alphaKHDat = alphaKHDat.intValue();
	}
	public Float getAlphaPT() {
		return alphaPT;
	}
	public void setAlphaPT(BigDecimal alphaPT) {
		this.alphaPT = alphaPT.floatValue();
	}
	public Integer getAlphaDiem() {
		return alphaDiem;
	}
	public void setAlphaDiem(BigDecimal alphaDiem) {
		this.alphaDiem = alphaDiem.intValue();
	}
	public Integer getVnmKHTG() {
		return vnmKHTG;
	}
	public void setVnmKHTG(BigDecimal vnmKHTG) {
		this.vnmKHTG = vnmKHTG.intValue();
	}
	public Integer getVnmKHDat() {
		return vnmKHDat;
	}
	public void setVnmKHDat(BigDecimal vnmKHDat) {
		this.vnmKHDat = vnmKHDat.intValue();
	}
	public Float getVnmPT() {
		return vnmPT;
	}
	public void setVnmPT(BigDecimal vnmPT) {
		this.vnmPT = vnmPT.floatValue();
	}
	public Integer getVnmDiem() {
		return vnmDiem;
	}
	public void setVnmDiem(BigDecimal vnmDiem) {
		this.vnmDiem = vnmDiem.intValue();
	}
	public Integer getDiemPA() {
		return diemPA;
	}
	public void setDiemPA(BigDecimal diemPA) {
		this.diemPA = diemPA.intValue();
	}
}

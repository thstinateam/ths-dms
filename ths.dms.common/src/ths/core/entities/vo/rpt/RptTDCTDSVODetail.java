package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptTDCTDSVODetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2295035896091418044L;
	private String productCode;
	private String productName;
	private BigDecimal price;
	private BigDecimal quantityPlan;
	private BigDecimal amountPlan;
	private BigDecimal quantityReal;
	private BigDecimal amountReal;
	private BigDecimal percent;
	private String staffCode;
	private String staffName;
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getQuantityPlan() {
		return quantityPlan;
	}
	public void setQuantityPlan(BigDecimal quantityPlan) {
		this.quantityPlan = quantityPlan;
	}
	public BigDecimal getAmountPlan() {
		return amountPlan;
	}
	public void setAmountPlan(BigDecimal amountPlan) {
		this.amountPlan = amountPlan;
	}
	public BigDecimal getQuantityReal() {
		return quantityReal;
	}
	public void setQuantityReal(BigDecimal quantityReal) {
		this.quantityReal = quantityReal;
	}
	public BigDecimal getAmountReal() {
		return amountReal;
	}
	public void setAmountReal(BigDecimal amountReal) {
		this.amountReal = amountReal;
	}
	public BigDecimal getPercent() {
		return percent;
	}
	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
}

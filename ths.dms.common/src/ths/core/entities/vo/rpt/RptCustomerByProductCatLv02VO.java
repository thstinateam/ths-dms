package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptCustomerByProductCatLv02VO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String productCat;
	private String productCatDes;
	private List<RptCustomerByProductCatDataVO> listData;

	public RptCustomerByProductCatLv02VO() {
		this.listData = new ArrayList<RptCustomerByProductCatDataVO>();
	}

	public String getProductCat() {
		return productCat;
	}

	public void setProductCat(String productCat) {
		this.productCat = productCat;
	}

	public String getProductCatDes() {
		return productCatDes;
	}

	public void setProductCatDes(String productCatDes) {
		this.productCatDes = productCatDes;
	}

	public List<RptCustomerByProductCatDataVO> getListData() {
		return listData;
	}

	public void setListData(List<RptCustomerByProductCatDataVO> listData) {
		this.listData = listData;
	}
}

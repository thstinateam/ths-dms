/*
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.core.entities.vo.rpt;

import java.io.Serializable;

/**
 * Bao cao [TL1.1] Danh sach thiet bi thanh ly
 * 
 * @author Trietptm
 * @since Apr 06, 2016
 */
public class Rpt_3_1_1_3_BCDSTL11_VO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String regionCode;
	private String areaCode;
	private String shopCode;
	private String shopName;
	private String docNumber;
	private String equipmentCode;
	private String equipmentSeri;
	private String groupName;
	private String categoryName;
	private String brandName;
	private String capacity;
	private Integer manufacturingYear;
	private String liquidationDate;
	private String reason;
	private String status;
	
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public String getEquipmentSeri() {
		return equipmentSeri;
	}
	public void setEquipmentSeri(String equipmentSeri) {
		this.equipmentSeri = equipmentSeri;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public Integer getManufacturingYear() {
		return manufacturingYear;
	}
	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}
	public String getLiquidationDate() {
		return liquidationDate;
	}
	public void setLiquidationDate(String liquidationDate) {
		this.liquidationDate = liquidationDate;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

	
	
}
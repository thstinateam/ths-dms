package ths.core.entities.vo.rpt;

import java.io.Serializable;

public class RptAbsentDetailVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String day;//ngay
	private String dayVal;//X:ko bat may
//	private Integer total;
	
//	public Integer getTotal() {
//		return total;
//	}
//	public void setTotal(Integer total) {
//		this.total = total;
//	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getDayVal() {
		return dayVal;
	}
	public void setDayVal(String dayVal) {
		this.dayVal = dayVal;
	}

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;

public class RptCompareBookAndDeliveryProductVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String poAutoNumber;
	private Long productId;
	private Integer bookQuantity;
	private String productCode;
	private String productName;
	private Integer bookConvfact;
	private Integer saleQuantity;
	private Integer saleConvfact;
	/**
	 * @return the poAutoNumber
	 */
	public String getPoAutoNumber() {
		return poAutoNumber;
	}
	/**
	 * @param poAutoNumber the poAutoNumber to set
	 */
	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}
	/**
	 * @return the productId
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	/**
	 * @return the bookQuantity
	 */
	public Integer getBookQuantity() {
		return bookQuantity;
	}
	/**
	 * @param bookQuantity the bookQuantity to set
	 */
	public void setBookQuantity(Integer bookQuantity) {
		this.bookQuantity = bookQuantity;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the bookConvfact
	 */
	public Integer getBookConvfact() {
		return bookConvfact;
	}
	/**
	 * @param bookConvfact the bookConvfact to set
	 */
	public void setBookConvfact(Integer bookConvfact) {
		this.bookConvfact = bookConvfact;
	}
	/**
	 * @return the saleQuantity
	 */
	public Integer getSaleQuantity() {
		return saleQuantity;
	}
	/**
	 * @param saleQuantity the saleQuantity to set
	 */
	public void setSaleQuantity(Integer saleQuantity) {
		this.saleQuantity = saleQuantity;
	}
	/**
	 * @return the saleConvfact
	 */
	public Integer getSaleConvfact() {
		return saleConvfact;
	}
	/**
	 * @param saleConvfact the saleConvfact to set
	 */
	public void setSaleConvfact(Integer saleConvfact) {
		this.saleConvfact = saleConvfact;
	}
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD21VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String maMien;
    private String maVung;
    private String maNpp;
    private String maCT;
    private String tenCT;
    private String maMuc;
    private String maNhom;
    private BigDecimal dsKH;
    private BigDecimal cpKH;
    private BigDecimal pbKH;
    private BigDecimal dsTH;
    private BigDecimal cpTH;
    private BigDecimal pbTH;
    private Integer ptDS;
    private Integer ptCP;
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNpp() {
		return maNpp;
	}
	public void setMaNpp(String maNpp) {
		this.maNpp = maNpp;
	}
	public String getMaCT() {
		return maCT;
	}
	public void setMaCT(String maCT) {
		this.maCT = maCT;
	}
	public String getTenCT() {
		return tenCT;
	}
	public void setTenCT(String tenCT) {
		this.tenCT = tenCT;
	}
	public String getMaMuc() {
		return maMuc;
	}
	public void setMaMuc(String maMuc) {
		this.maMuc = maMuc;
	}
	public String getMaNhom() {
		return maNhom;
	}
	public void setMaNhom(String maNhom) {
		this.maNhom = maNhom;
	}
	public BigDecimal getDsKH() {
		return dsKH;
	}
	public void setDsKH(BigDecimal dsKH) {
		this.dsKH = dsKH;
	}
	public BigDecimal getCpKH() {
		return cpKH;
	}
	public void setCpKH(BigDecimal cpKH) {
		this.cpKH = cpKH;
	}
	public BigDecimal getPbKH() {
		return pbKH;
	}
	public void setPbKH(BigDecimal pbKH) {
		this.pbKH = pbKH;
	}
	public BigDecimal getDsTH() {
		return dsTH;
	}
	public void setDsTH(BigDecimal dsTH) {
		this.dsTH = dsTH;
	}
	public BigDecimal getCpTH() {
		return cpTH;
	}
	public void setCpTH(BigDecimal cpTH) {
		this.cpTH = cpTH;
	}
	public BigDecimal getPbTH() {
		return pbTH;
	}
	public void setPbTH(BigDecimal pbTH) {
		this.pbTH = pbTH;
	}
	public Integer getPtDS() {
		return ptDS;
	}
	public void setPtDS(BigDecimal ptDS) {
		if (ptDS != null) {
			this.ptDS = ptDS.intValue();
		}
	}
	public Integer getPtCP() {
		return ptCP;
	}
	public void setPtCP(BigDecimal ptCP) {
		if (ptCP != null) {
			this.ptCP = ptCP.intValue();
		}
	}
    
}

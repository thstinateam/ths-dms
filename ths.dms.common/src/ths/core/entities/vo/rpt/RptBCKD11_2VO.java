package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RptBCKD11_2VO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer soNgayBanHang;//so ngay ban hang
	private Integer soNgayThucHien;//so ngay thuc hien
	private Float tienDoChuan;//tien do chuan
	private Date ngayBatDau;//ngay bat dau
	private Date ngayKetThuc;//ngay ket thuc
	private List<RptBCKD11_2CTVO> lstDetail = new ArrayList<RptBCKD11_2CTVO>();
	public Integer getSoNgayBanHang() {
		return soNgayBanHang;
	}
	public void setSoNgayBanHang(Integer soNgayBanHang) {
		this.soNgayBanHang = soNgayBanHang;
	}
	public Integer getSoNgayThucHien() {
		return soNgayThucHien;
	}
	public void setSoNgayThucHien(Integer soNgayThucHien) {
		this.soNgayThucHien = soNgayThucHien;
	}
	public Float getTienDoChuan() {
		return tienDoChuan;
	}
	public void setTienDoChuan(Float tienDoChuan) {
		this.tienDoChuan = tienDoChuan;
	}
	public Date getNgayBatDau() {
		return ngayBatDau;
	}
	public void setNgayBatDau(Date ngayBatDau) {
		this.ngayBatDau = ngayBatDau;
	}
	public Date getNgayKetThuc() {
		return ngayKetThuc;
	}
	public void setNgayKetThuc(Date ngayKetThuc) {
		this.ngayKetThuc = ngayKetThuc;
	}
	public List<RptBCKD11_2CTVO> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<RptBCKD11_2CTVO> lstDetail) {
		this.lstDetail = lstDetail;
	}
}

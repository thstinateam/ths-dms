package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptBCKD13VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String maMien;
    private String maVung;
    private String maNPP;
    private String tenNPP;
    private String maKH;
    private String tenKH;
    private String soNha;
    private String duong;
    private String phuongXa;
    private String quanHuyen;
    private String tinhThanh;
    private Integer slTu;
    private Integer slDiemBan;
    private BigDecimal kpsds1ThangTruoc;
    private BigDecimal kpsds2ThangTruoc;
    private BigDecimal kpsds3ThangTruoc;
    private Date rptMonth;
    private BigDecimal amount;
    
    
	public BigDecimal getKpsds1ThangTruoc() {
		return kpsds1ThangTruoc;
	}
	public void setKpsds1ThangTruoc(BigDecimal kpsds1ThangTruoc) {
		this.kpsds1ThangTruoc = kpsds1ThangTruoc;
	}
	public BigDecimal getKpsds2ThangTruoc() {
		return kpsds2ThangTruoc;
	}
	public void setKpsds2ThangTruoc(BigDecimal kpsds2ThangTruoc) {
		this.kpsds2ThangTruoc = kpsds2ThangTruoc;
	}
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getSoNha() {
		return soNha;
	}
	public void setSoNha(String soNha) {
		this.soNha = soNha;
	}
	public String getDuong() {
		return duong;
	}
	public void setDuong(String duong) {
		this.duong = duong;
	}
	public String getPhuongXa() {
		return phuongXa;
	}
	public void setPhuongXa(String phuongXa) {
		this.phuongXa = phuongXa;
	}
	public String getQuanHuyen() {
		return quanHuyen;
	}
	public void setQuanHuyen(String quanHuyen) {
		this.quanHuyen = quanHuyen;
	}
	public String getTinhThanh() {
		return tinhThanh;
	}
	public void setTinhThanh(String tinhThanh) {
		this.tinhThanh = tinhThanh;
	}
	public Integer getSlTu() {
		return slTu;
	}
	public void setSlTu(BigDecimal slTu) {
		if (slTu != null)
			this.slTu = slTu.intValue();
	}
	public Integer getSlDiemBan() {
		return slDiemBan;
	}
	public void setSlDiemBan(BigDecimal slDiemBan) {
		if (slDiemBan != null)
		this.slDiemBan = slDiemBan.intValue();
	}
	public BigDecimal getKpsds3ThangTruoc() {
		return kpsds3ThangTruoc;
	}
	public void setKpsds3ThangTruoc(BigDecimal kpsds3ThangTruoc) {
		this.kpsds3ThangTruoc = kpsds3ThangTruoc;
	}
	public Date getRptMonth() {
		return rptMonth;
	}
	public void setRptMonth(Date rptMonth) {
		this.rptMonth = rptMonth;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
    
    
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

public class RptSaleOrderInDate2VO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String staffInfo;
	
	private List<RptSaleOrderInDate1VO> lstRptSaleOrderInDate1VO;
	

	public String getStaffInfo() {
		return staffInfo;
	}
	public void setStaffInfo(String staffInfo) {
		this.staffInfo = staffInfo;
	}
	public List<RptSaleOrderInDate1VO> getLstRptSaleOrderInDate1VO() {
		return lstRptSaleOrderInDate1VO;
	}
	public void setLstRptSaleOrderInDate1VO(
			List<RptSaleOrderInDate1VO> lstRptSaleOrderInDate1VO) {
		this.lstRptSaleOrderInDate1VO = lstRptSaleOrderInDate1VO;
	}
	
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author hunglm16
 * @since March 08, 2014
 * @description Class Map Report Bao cao doanh so tieu thu cua tung diem le
 */
public class RptDSTTCTDLVO implements Serializable{
	//BAO CAO DAONH SO TIEU THU CUA TUNG DIEM LE
	private static final long serialVersionUID = 1L;
	
	private String staffCode;//Ma NVBH
	private String staffName;//Ten NVBH
	private String shortCode;// Ma KH
	private String customerName;// Ten KH
	private String address;// Dia Chi
	private String customerType;//Loai KH
	private BigDecimal saleDayQuantity;// DS Ngay
	private BigDecimal saleCurrentQuantity;// DS luy Ke
	
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getShortCode() {
		return shortCode;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public BigDecimal getSaleDayQuantity() {
		return saleDayQuantity;
	}
	public void setSaleDayQuantity(BigDecimal saleDayQuantity) {
		this.saleDayQuantity = saleDayQuantity;
	}
	public BigDecimal getSaleCurrentQuantity() {
		return saleCurrentQuantity;
	}
	public void setSaleCurrentQuantity(BigDecimal saleCurrentQuantity) {
		this.saleCurrentQuantity = saleCurrentQuantity;
	}

}

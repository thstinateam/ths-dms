package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptExImStockOfStaffLv01VO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long staffId;
	private String staffCode;
	private String staffName;
	private List<RptExImStockOfStaffDataVO> listData;

	public RptExImStockOfStaffLv01VO() {
		this.listData = new ArrayList<RptExImStockOfStaffDataVO>();
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public List<RptExImStockOfStaffDataVO> getListData() {
		return listData;
	}

	public void setListData(List<RptExImStockOfStaffDataVO> listData) {
		this.listData = listData;
	}
	
	public BigDecimal getTotalExpAmount() {
		BigDecimal value = new BigDecimal(0);

		if (null != listData && listData.size() > 0) {
			for (RptExImStockOfStaffDataVO item : listData) {
				value = value.add(null == item.getExportAmount() ? new BigDecimal(0)
						: item.getExportAmount());
			}
		}

		return value;
	}
	
	public BigDecimal getTotalSellAmount() {
		BigDecimal value = new BigDecimal(0);

		if (null != listData && listData.size() > 0) {
			for (RptExImStockOfStaffDataVO item : listData) {
				value = value.add(null == item.getSellAmount() ? new BigDecimal(0)
						: item.getSellAmount());
			}
		}

		return value;
	}
	
	public BigDecimal getTotalPromoteAmount() {
		BigDecimal value = new BigDecimal(0);

		if (null != listData && listData.size() > 0) {
			for (RptExImStockOfStaffDataVO item : listData) {
				value = value.add(null == item.getPromoteAmount() ? new BigDecimal(0)
						: item.getPromoteAmount());
			}
		}

		return value;
	}
	
	public BigDecimal getTotalStockAmount() {
		BigDecimal value = new BigDecimal(0);

		if (null != listData && listData.size() > 0) {
			for (RptExImStockOfStaffDataVO item : listData) {
				value = value.add(null == item.getStockAmount() ? new BigDecimal(0)
						: item.getStockAmount());
			}
		}

		return value;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class RptOrderDetailProduct3VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String productCode;
	private String productName;
	private BigDecimal price;
	private String orderDate;
	private String staffCode;
	private String staffName;
	private ArrayList<RptOrderDetailProduct4VO> lstRptOrderDetailProduct4VO = new ArrayList<RptOrderDetailProduct4VO>();

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	/**
	 * @return the staffName
	 */
	public String getStaffName() {
		return staffName;
	}

	/**
	 * @param staffName the staffName to set
	 */
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	/**
	 * @return the lstRptOrderDetailProduct1VO
	 */
	public ArrayList<RptOrderDetailProduct4VO> getLstRptOrderDetailProduct4VO() {
		return lstRptOrderDetailProduct4VO;
	}

	/**
	 * @param lstRptOrderDetailProduct4VO the lstRptOrderDetailProduct1VO to set
	 */
	public void setLstRptOrderDetailProduct4VO(
			ArrayList<RptOrderDetailProduct4VO> lstRptOrderDetailProduct4VO) {
		this.lstRptOrderDetailProduct4VO = lstRptOrderDetailProduct4VO;
	}


}

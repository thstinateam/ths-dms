package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class RptPDTHVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String shopName;
	private String shopPhone;
	private String shopTaxNum;
	private String shopAddress;
	private Boolean isHasInvoice;
	private String invoiceNumber;
	private Date orderDate;
	private String customerName;
	private String customerCode;
	private String customerPhone;
	private String customerMobiphone;
	private String carNumber;
	private String customerShortCode;
	private String customerAddress;
	private String staffCode;
	private String staffName;
	private String deliveryCode;
	private String deliveryName;
	private BigDecimal total;
	private List<RptSaleOrderDetailVO> lstSODetail;
	
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopPhone() {
		return shopPhone;
	}
	public void setShopPhone(String shopPhone) {
		this.shopPhone = shopPhone;
	}
	public String getShopTaxNum() {
		return shopTaxNum;
	}
	public void setShopTaxNum(String shopTaxNum) {
		this.shopTaxNum = shopTaxNum;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public Boolean getIsHasInvoice() {
		return isHasInvoice;
	}
	public void setIsHasInvoice(Boolean isHasInvoice) {
		this.isHasInvoice = isHasInvoice;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerShortCode() {
		return customerShortCode;
	}
	public void setCustomerShortCode(String customerShortCode) {
		this.customerShortCode = customerShortCode;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getCarNumber() {
		return carNumber;
	}
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public List<RptSaleOrderDetailVO> getLstSODetail() {
		return lstSODetail;
	}
	public void setLstSODetail(List<RptSaleOrderDetailVO> lstSODetail) {
		this.lstSODetail = lstSODetail;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getCustomerMobiphone() {
		return customerMobiphone;
	}
	public void setCustomerMobiphone(String customerMobiphone) {
		this.customerMobiphone = customerMobiphone;
	}
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptPTHCKHProductVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal soId;
	private BigDecimal sodId;
	private String orderDate;
	private String orderNumber;
	private String rootOrderNumber;
	private BigDecimal amount;
	private BigDecimal deliveryId;
	private String staffCode;
	private String deliveryCode;
	private String staffName;
	private String deliveryName;
	private BigDecimal quantity;
	private BigDecimal detailAmount;//thanh tien
	private BigDecimal discountAmount;//chiet khau tien
	private BigDecimal priceValue;//don gia
	private String productCode;//ma san pham
	private String productName;//ten san pham
	private String shortCode;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String phone;
	private String mobiphone;
	private BigDecimal adjust;
	private BigDecimal discountOrder;
	private BigDecimal totalPay;
	private BigDecimal numLe;
	private BigDecimal numThung;
	
	public BigDecimal getSoId() {
		return soId;
	}
	public void setSoId(BigDecimal soId) {
		this.soId = soId;
	}
	public BigDecimal getSodId() {
		return sodId;
	}
	public void setSodId(BigDecimal sodId) {
		this.sodId = sodId;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getRootOrderNumber() {
		return rootOrderNumber;
	}
	public void setRootOrderNumber(String rootOrderNumber) {
		this.rootOrderNumber = rootOrderNumber;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(BigDecimal deliveryId) {
		this.deliveryId = deliveryId;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getDetailAmount() {
		return detailAmount;
	}
	public void setDetailAmount(BigDecimal detailAmount) {
		this.detailAmount = detailAmount;
	}
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}
	public BigDecimal getPriceValue() {
		return priceValue;
	}
	public void setPriceValue(BigDecimal priceValue) {
		this.priceValue = priceValue;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobiphone() {
		return mobiphone;
	}
	public void setMobiphone(String mobiphone) {
		this.mobiphone = mobiphone;
	}
	public BigDecimal getAdjust() {
		return adjust;
	}
	public void setAdjust(BigDecimal adjust) {
		this.adjust = adjust;
	}
	public BigDecimal getDiscountOrder() {
		return discountOrder;
	}
	public void setDiscountOrder(BigDecimal discountOrder) {
		this.discountOrder = discountOrder;
	}
	public BigDecimal getTotalPay() {
		return totalPay;
	}
	public void setTotalPay(BigDecimal totalPay) {
		this.totalPay = totalPay;
	}
	public BigDecimal getNumLe() {
		return numLe;
	}
	public void setNumLe(BigDecimal numLe) {
		this.numLe = numLe;
	}
	public BigDecimal getNumThung() {
		return numThung;
	}
	public void setNumThung(BigDecimal numThung) {
		this.numThung = numThung;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

public class RptParentSaleOrderOfDeliveryCustomVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String deliveryCode;
	private String deliveryName;
	ArrayList<RptSaleOrderOfDeliveryCustomVO> listCustomVO = new ArrayList<RptSaleOrderOfDeliveryCustomVO>();

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public ArrayList<RptSaleOrderOfDeliveryCustomVO> getListCustomVO() {
		return listCustomVO;
	}

	public void setListCustomVO(
			ArrayList<RptSaleOrderOfDeliveryCustomVO> listCustomVO) {
		this.listCustomVO = listCustomVO;
	}
	

}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptPGHLotVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String productCode;
	
	private String productName;
	
	private String lot;
	
	private Integer convfact;
	
	private Integer quantity = 0;
	
	private BigDecimal price;
	
	private BigDecimal packagePrice;
	
	private BigDecimal amount = new BigDecimal(0);
	
	private String programInfo;
	
	private Integer thung;
	
	private Integer le;	
	
	
	public void setThung(Integer thung) {
		this.thung = thung;
	}

	public void setLe(Integer le) {
		this.le = le;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getThung() {
		return this.getQuantity() / this.getConvfact();
	}

	public Integer getLe() {
		return this.getQuantity() % this.getConvfact();
	}

	public String getProgramInfo() {
		return programInfo;
	}

	public void setProgramInfo(String programInfo) {
		this.programInfo = programInfo;
	}

	public String getLot() {
		return lot;
	}
	
	public void setLot(String lot) {
		this.lot = lot;
	}

	public BigDecimal getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(BigDecimal packagePrice) {
		this.packagePrice = packagePrice;
	}	
}
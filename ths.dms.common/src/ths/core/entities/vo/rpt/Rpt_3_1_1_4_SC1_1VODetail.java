/*
 * Copyright 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class Rpt_3_1_1_4_SC1_1VODetail implements Serializable {

	/**
	 * Bao cao 3.1.1.4 Bao cao phieu sua chua chua thuc hien
	 * 
	 * @author Datpv4
	 * @since May 06,2015
	 */
	private static final long serialVersionUID = 1L;
	String mien;
	String vung;
	String maphieu;
	String ngaytao;
	String manpp;
	String tennpp;
	String gsnpp;//giam sat nha phan phoi
	String tengsnpp;// ten giam sat nha phan phoi
	String makh;
	String tenkh;
	String nhomtu;
	String loaitu;
	String hieutu;
	String dungtich;
	Integer namsx;
	String mataisan;
	String serial;
	String ngaysudung;//Ngày bắt đầu đưa vào sử dụng Tủ
	String hangmucsc;//hang muc sua chua
	long chiphidenghi;
	Integer suachualanthu;
	BigDecimal materialTotalAmount; //Tong tien vat tu
	
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getMaphieu() {
		return maphieu;
	}
	public void setMaphieu(String maphieu) {
		this.maphieu = maphieu;
	}
	public String getNgaytao() {
		return ngaytao;
	}
	public void setNgaytao(String ngaytao) {
		this.ngaytao = ngaytao;
	}
	public String getManpp() {
		return manpp;
	}
	public void setManpp(String manpp) {
		this.manpp = manpp;
	}
	public String getTennpp() {
		return tennpp;
	}
	public void setTennpp(String tennpp) {
		this.tennpp = tennpp;
	}
	public String getGsnpp() {
		return gsnpp;
	}
	public void setGsnpp(String gsnpp) {
		this.gsnpp = gsnpp;
	}
	public String getMakh() {
		return makh;
	}
	public void setMakh(String makh) {
		this.makh = makh;
	}
	public String getTenkh() {
		return tenkh;
	}
	public void setTenkh(String tenkh) {
		this.tenkh = tenkh;
	}
	public String getLoaitu() {
		return loaitu;
	}
	public void setLoaitu(String loaitu) {
		this.loaitu = loaitu;
	}
	public String getHieutu() {
		return hieutu;
	}
	public void setHieutu(String hieutu) {
		this.hieutu = hieutu;
	}
	public String getDungtich() {
		return dungtich;
	}
	public void setDungtich(String dungtich) {
		this.dungtich = dungtich;
	}
	public Integer getNamsx() {
		return namsx;
	}
	public void setNamsx(Integer namsx) {
		this.namsx = namsx;
	}
	public String getMataisan() {
		return mataisan;
	}
	public void setMataisan(String mataisan) {
		this.mataisan = mataisan;
	}
	public String getNgaysudung() {
		return ngaysudung;
	}
	public void setNgaysudung(String ngaysudung) {
		this.ngaysudung = ngaysudung;
	}
	public String getHangmucsc() {
		return hangmucsc;
	}
	public void setHangmucsc(String hangmucsc) {
		this.hangmucsc = hangmucsc;
	}
	
	public long getChiphidenghi() {
		return chiphidenghi;
	}
	public void setChiphidenghi(long chiphidenghi) {
		this.chiphidenghi = chiphidenghi;
	}
	public Integer getSuachualanthu() {
		return suachualanthu;
	}
	public void setSuachualanthu(Integer suachualanthu) {
		this.suachualanthu = suachualanthu;
	}
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public BigDecimal getMaterialTotalAmount() {
		return materialTotalAmount;
	}
	public void setMaterialTotalAmount(BigDecimal materialTotalAmount) {
		this.materialTotalAmount = materialTotalAmount;
	}
	public String getTengsnpp() {
		return tengsnpp;
	}
	public void setTengsnpp(String tengsnpp) {
		this.tengsnpp = tengsnpp;
	}
	public String getNhomtu() {
		return nhomtu;
	}
	public void setNhomtu(String nhomtu) {
		this.nhomtu = nhomtu;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}

}

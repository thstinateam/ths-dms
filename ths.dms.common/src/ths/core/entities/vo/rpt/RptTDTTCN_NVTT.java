package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * vo: bao cao theo doi thanh toan cong no theo NVTT
 * 
 * @author lacnv1
 * @since Apr 08, 2014
 */
public class RptTDTTCN_NVTT implements Serializable {

	private static final long serialVersionUID = 1L;

	private String maKH;
	private String tenKH;
	private String maNV;
	private String tenNV;
	private String ngayHD;
	private String soHD;
	private String loaiNo;
	private String ngayTT;
	private String loaiTT;
	private String soCTTT;
	private BigDecimal tienHD;
	private BigDecimal tienTT;
	private BigDecimal tienCK;
	private BigDecimal tienConNo;
	private BigDecimal lanTT;

	public String getMaKH() {
		return maKH;
	}

	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	
	public String getTenKH() {
		return tenKH;
	}

	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}

	public String getMaNV() {
		return maNV;
	}

	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}

	public String getTenNV() {
		return tenNV;
	}

	public void setTenNV(String tenNV) {
		this.tenNV = tenNV;
	}

	public String getNgayHD() {
		return ngayHD;
	}

	public void setNgayHD(String ngayHD) {
		this.ngayHD = ngayHD;
	}

	public String getSoHD() {
		return soHD;
	}

	public void setSoHD(String soHD) {
		this.soHD = soHD;
	}

	public String getLoaiNo() {
		return loaiNo;
	}

	public void setLoaiNo(String loaiNo) {
		this.loaiNo = loaiNo;
	}

	public String getNgayTT() {
		return ngayTT;
	}

	public void setNgayTT(String ngayTT) {
		this.ngayTT = ngayTT;
	}

	public String getLoaiTT() {
		return loaiTT;
	}

	public void setLoaiTT(String loaiTT) {
		this.loaiTT = loaiTT;
	}

	public String getSoCTTT() {
		return soCTTT;
	}

	public void setSoCTTT(String soCTTT) {
		this.soCTTT = soCTTT;
	}

	public BigDecimal getTienHD() {
		return tienHD;
	}

	public void setTienHD(BigDecimal tienHD) {
		this.tienHD = tienHD;
	}

	public BigDecimal getTienTT() {
		return tienTT;
	}

	public void setTienTT(BigDecimal tienTT) {
		this.tienTT = tienTT;
	}
	
	public BigDecimal getTienCK() {
		return tienCK;
	}

	public void setTienCK(BigDecimal tienCK) {
		this.tienCK = tienCK;
	}

	public BigDecimal getTienConNo() {
		return tienConNo;
	}

	public void setTienConNo(BigDecimal tienConNo) {
		this.tienConNo = tienConNo;
	}

	public BigDecimal getLanTT() {
		return lanTT;
	}

	public void setLanTT(BigDecimal lanTT) {
		this.lanTT = lanTT;
	}
}
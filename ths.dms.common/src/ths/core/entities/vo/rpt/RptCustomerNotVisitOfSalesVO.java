package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RptCustomerNotVisitOfSalesVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String areaCode;
	private String locationCode;
	private String shopCode;
	private String shopName;

	private String staffOwnerName;
	private String staffCode;
	private String staffName;

	private Integer visitDate;
	private Date visitDay;
	//private String strVisitDay;
	private String customerCode;
	private String customerName;
	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	private String customerAddress;
	private Integer number;

	public RptCustomerNotVisitOfSalesVO() {
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		if(areaCode!=null && areaCode!=""){
			this.areaCode = areaCode;
		}else{
			this.areaCode = "";
		}
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		if(locationCode!=null && locationCode!=""){
			this.locationCode = locationCode;
		}else{
			this.locationCode = "";
		}
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		if(shopCode!=null && shopCode!=""){
			this.shopCode = shopCode;
		}else{
			this.shopCode = "";
		}
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		if(shopName!=null && shopName!=""){
			this.shopName = shopName;
		}else{
			this.shopName = "";
		}
	}

	public String getStaffOwnerName() {
		return staffOwnerName;
	}

	public void setStaffOwnerName(String staffOwnerName) {
		if(staffOwnerName!=null && staffOwnerName!=""){
			this.staffOwnerName = staffOwnerName;
		}else{
			this.staffOwnerName = "";
		}
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		if(staffCode!=null && staffCode!=""){
			this.staffCode = staffCode;
		}else{
			this.staffCode = "";
		}
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		if(staffName!=null && staffName!=""){
			this.staffName = staffName;
		}else{
			this.staffName = "";
		}
	}

	public Integer getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(Integer visitDate) {
		this.visitDate = visitDate;
	}

	public Date getVisitDay() {
		return visitDay;
	}

	public void setVisitDay(Date visitDay) {
		this.visitDay = visitDay;
		
		if (visitDay != null) {
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			//this.strVisitDay = format.format(this.visitDay);
		}
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		if(customerCode!=null && customerCode!=""){
			this.customerCode = customerCode;
		}else{
			this.customerCode = "";
		}
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		if(customerName!=null && customerName!=""){
			this.customerName = customerName;
		}else{
			this.customerName = "";
		}
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		if(customerAddress!=null && customerAddress!=""){
			this.customerAddress = customerAddress;
		}else{
			this.customerAddress = "";
		}
	}

}
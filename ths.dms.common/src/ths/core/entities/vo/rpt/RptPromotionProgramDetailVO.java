package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptPromotionProgramDetailVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String promotionProgramCode;
	private String promotionProgramName;
	private String staffCode;
	private String staffName;
	private String productCode;
	private String productName;
	private Integer quantity;
	private BigDecimal freeProductAmount;
	public String getPromotionProgramCode() {
		return promotionProgramCode;
	}
	public void setPromotionProgramCode(String promotionProgramCode) {
		this.promotionProgramCode = promotionProgramCode;
	}
	public String getPromotionProgramName() {
		return promotionProgramName;
	}
	public void setPromotionProgramName(String promotionProgramName) {
		this.promotionProgramName = promotionProgramName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getFreeProductAmount() {
		return freeProductAmount;
	}
	public void setFreeProductAmount(BigDecimal freeProductAmount) {
		this.freeProductAmount = freeProductAmount;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;

public class RptInvVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SaleOrder saleOrder;
	private List<SaleOrderDetail> lstSaleOrderDetail;

	/**
	 * @return the saleOrder
	 */
	public SaleOrder getSaleOrder() {
		return saleOrder;
	}

	/**
	 * @param saleOrder
	 *            the saleOrder to set
	 */
	public void setSaleOrder(SaleOrder saleOrder) {
		this.saleOrder = saleOrder;
	}

	/**
	 * @return the lstSaleOrderDetail
	 */
	public List<SaleOrderDetail> getLstSaleOrderDetail() {
		return lstSaleOrderDetail;
	}

	/**
	 * @param lstSaleOrderDetail
	 *            the lstSaleOrderDetail to set
	 */
	public void setLstSaleOrderDetail(List<SaleOrderDetail> lstSaleOrderDetail) {
		this.lstSaleOrderDetail = lstSaleOrderDetail;
	}

}

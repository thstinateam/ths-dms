package ths.core.entities.vo.rpt;

import java.io.Serializable;

public class RptBCKD10_2_DLCPPTSkusDNVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String mien;
	private String vung;
    private String maNPP;
    private String tenNPP;
    private String maKH; 
    private String tenKH;
    private String diaChiKH; 
    private Integer phanTramThucHien;
    
    public Integer getPhanTramThucHien() {
		return phanTramThucHien;
	}
	public void setPhanTramThucHien(Integer phanTramThucHien) {
		this.phanTramThucHien = phanTramThucHien;
	}
	private Integer luyKeThucHien;
    private Integer luyKeMucTieu;
    private Integer isTong; 
	public Integer getIsTong() {
		return isTong;
	}
	public void setIsTong(Integer isTong) {
		this.isTong = isTong;
	}
	private Long g;//GROUPING_ID
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getDiaChiKH() {
		return diaChiKH;
	}
	public void setDiaChiKH(String diaChiKH) {
		this.diaChiKH = diaChiKH;
	}
	public Integer getLuyKeThucHien() {
		return luyKeThucHien;
	}
	public void setLuyKeThucHien(Integer luyKeThucHien) {
		this.luyKeThucHien = luyKeThucHien;
	}
	public Integer getLuyKeMucTieu() {
		return luyKeMucTieu;
	}
	public void setLuyKeMucTieu(Integer luyKeMucTieu) {
		this.luyKeMucTieu = luyKeMucTieu;
	}
	public Long getG() {
		return g;
	}
	public void setG(Long g) {
		this.g = g;
	}
}

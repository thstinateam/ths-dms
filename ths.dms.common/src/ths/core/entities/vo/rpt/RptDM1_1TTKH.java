package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * vo bao cao DM1.1 - thong tin khach hang
 * 
 * @author lacnv1
 * @since Mar 20, 2014
 */
public class RptDM1_1TTKH implements Serializable {

	private static final long serialVersionUID = 1L;

	private String mien;
	private String vung;
	private String maNPP;
	private String tenNPP;
	private String maKH;
	private String tenKH;
	private String loaiKH;
	private String soNha;
	private String duong;
	private String dienThoai;
	private String diDong;
	private String maPhuongXa;
	private String tenPhuongXa;
	private String maQuanHuyen;
	private String tenQuanHuyen;
	private String maTinhThanh;
	private String tenTinhThanh;
	private String doTrungThanh;
	private String viTri;
	private String trangThai;
	private String ngayTao;
	private String maSoThue;
	private String soTaiKhoan;
	private BigDecimal hanNo;
	private String mucTB;
	private String mucDS;
	
	public String getMien() {
		return mien;
	}
	
	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getTenNPP() {
		return tenNPP;
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}

	public String getMaKH() {
		return maKH;
	}

	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}

	public String getTenKH() {
		return tenKH;
	}

	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}

	public String getLoaiKH() {
		return loaiKH;
	}

	public void setLoaiKH(String loaiKH) {
		this.loaiKH = loaiKH;
	}

	public String getSoNha() {
		return soNha;
	}

	public void setSoNha(String soNha) {
		this.soNha = soNha;
	}

	public String getDuong() {
		return duong;
	}

	public void setDuong(String duong) {
		this.duong = duong;
	}

	public String getDienThoai() {
		return dienThoai;
	}

	public void setDienThoai(String dienThoai) {
		this.dienThoai = dienThoai;
	}

	public String getDiDong() {
		return diDong;
	}

	public void setDiDong(String diDong) {
		this.diDong = diDong;
	}

	public String getMaPhuongXa() {
		return maPhuongXa;
	}

	public void setMaPhuongXa(String maPhuongXa) {
		this.maPhuongXa = maPhuongXa;
	}

	public String getMaQuanHuyen() {
		return maQuanHuyen;
	}

	public void setMaQuanHuyen(String maQuanHuyen) {
		this.maQuanHuyen = maQuanHuyen;
	}

	public String getTenQuanHuyen() {
		return tenQuanHuyen;
	}

	public void setTenQuanHuyen(String tenQuanHuyen) {
		this.tenQuanHuyen = tenQuanHuyen;
	}

	public String getMaTinhThanh() {
		return maTinhThanh;
	}

	public void setMaTinhThanh(String maTinhThanh) {
		this.maTinhThanh = maTinhThanh;
	}

	public String getTenTinhThanh() {
		return tenTinhThanh;
	}

	public void setTenTinhThanh(String tenTinhThanh) {
		this.tenTinhThanh = tenTinhThanh;
	}

	public String getDoTrungThanh() {
		return doTrungThanh;
	}

	public void setDoTrungThanh(String doTrungThanh) {
		this.doTrungThanh = doTrungThanh;
	}

	public String getViTri() {
		return viTri;
	}

	public void setViTri(String viTri) {
		this.viTri = viTri;
	}

	public String getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}

	public String getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(String ngayTao) {
		this.ngayTao = ngayTao;
	}

	public String getMaSoThue() {
		return maSoThue;
	}

	public void setMaSoThue(String maSoThue) {
		this.maSoThue = maSoThue;
	}

	public String getSoTaiKhoan() {
		return soTaiKhoan;
	}

	public void setSoTaiKhoan(String soTaiKhoan) {
		this.soTaiKhoan = soTaiKhoan;
	}

	public BigDecimal getHanNo() {
		return hanNo;
	}

	public void setHanNo(BigDecimal hanNo) {
		this.hanNo = hanNo;
	}

	public String getMucTB() {
		return mucTB;
	}

	public void setMucTB(String mucTB) {
		this.mucTB = mucTB;
	}

	public String getMucDS() {
		return mucDS;
	}

	public void setMucDS(String mucDS) {
		this.mucDS = mucDS;
	}

	public String getTenPhuongXa() {
		return tenPhuongXa;
	}

	public void setTenPhuongXa(String tenPhuongXa) {
		this.tenPhuongXa = tenPhuongXa;
	}
}
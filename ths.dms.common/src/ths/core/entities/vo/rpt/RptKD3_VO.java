package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * @author hunglm16
 * @since February 24, 2014
 * @description KD3 - Doanh so ban hang cua NVBH theo nhom
 * */
public class RptKD3_VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String mien;
	private String vung;
	private String tbhv;
	private String maNPP;
	private String gsNPP;
	private String maNVBH;
	private String tenNVBH;
	private String nhom;
	private BigDecimal khtt;
	private Integer soKHThangNay;
	private BigDecimal dsthThangNay;
	private Integer soKHThangTruoc;
	private BigDecimal dsthThangTruoc;
	private Integer isTong;
	
	
	public Integer getIsTong() {
		return isTong;
	}
	public void setIsTong(Integer isTong) {
		this.isTong = isTong;
	}
	public Integer getSoKHThangNay() {
		return soKHThangNay;
	}
	public void setSoKHThangNay(Integer soKHThangNay) {
		this.soKHThangNay = soKHThangNay;
	}
	public BigDecimal getDsthThangNay() {
		return dsthThangNay;
	}
	public void setDsthThangNay(BigDecimal dsthThangNay) {
		this.dsthThangNay = dsthThangNay;
	}
	public Integer getSoKHThangTruoc() {
		return soKHThangTruoc;
	}
	public void setSoKHThangTruoc(Integer soKHThangTruoc) {
		this.soKHThangTruoc = soKHThangTruoc;
	}
	public BigDecimal getDsthThangTruoc() {
		return dsthThangTruoc;
	}
	public void setDsthThangTruoc(BigDecimal dsthThangTruoc) {
		this.dsthThangTruoc = dsthThangTruoc;
	}
	
	public String getNhom() {
		return nhom;
	}
	public void setNhom(String nhom) {
		this.nhom = nhom;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getTbhv() {
		return tbhv;
	}
	public void setTbhv(String tbhv) {
		this.tbhv = tbhv;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	
	public String getGsNPP() {
		return gsNPP;
	}
	public void setGsNPP(String gsNPP) {
		this.gsNPP = gsNPP;
	}
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	
	public BigDecimal getKhtt() {
		return khtt;
	}
	public void setKhtt(BigDecimal khtt) {
		this.khtt = khtt;
	}
}

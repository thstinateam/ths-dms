package ths.core.entities.vo.rpt;

import java.io.Serializable;

public class RptEqImExVO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String equipGroup;
	private String equipType;
	private String label;
	private String volumn;
	private int open;
	private int close;
	private int impoxt;
	private int liquidation;
	private int eviction;
	private int lost;
	private int openCus;
	private int closeCus;
	private int impoxtCus;
	private int liquidationCus;
	private int evictionCus;
	private int lostCus;

	public String getEquipGroup() {
		return equipGroup;
	}

	public void setEquipGroup(String equipGroup) {
		this.equipGroup = equipGroup;
	}

	public String getEquipType() {
		return equipType;
	}

	public void setEquipType(String equipType) {
		this.equipType = equipType;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getVolumn() {
		return volumn;
	}

	public void setVolumn(String volumn) {
		this.volumn = volumn;
	}

	public int getOpen() {
		return open;
	}

	public void setOpen(int open) {
		this.open = open;
	}

	public int getClose() {
		return close;
	}

	public void setClose(int close) {
		this.close = close;
	}

	public int getImpoxt() {
		return impoxt;
	}

	public void setImpoxt(int impoxt) {
		this.impoxt = impoxt;
	}

	public int getLiquidation() {
		return liquidation;
	}

	public void setLiquidation(int liquidation) {
		this.liquidation = liquidation;
	}

	public int getEviction() {
		return eviction;
	}

	public void setEviction(int eviction) {
		this.eviction = eviction;
	}

	public int getLost() {
		return lost;
	}

	public void setLost(int lost) {
		this.lost = lost;
	}

	public int getOpenCus() {
		return openCus;
	}

	public void setOpenCus(int openCus) {
		this.openCus = openCus;
	}

	public int getCloseCus() {
		return closeCus;
	}

	public void setCloseCus(int closeCus) {
		this.closeCus = closeCus;
	}

	public int getImpoxtCus() {
		return impoxtCus;
	}

	public void setImpoxtCus(int impoxtCus) {
		this.impoxtCus = impoxtCus;
	}

	public int getLiquidationCus() {
		return liquidationCus;
	}

	public void setLiquidationCus(int liquidationCus) {
		this.liquidationCus = liquidationCus;
	}

	public int getEvictionCus() {
		return evictionCus;
	}

	public void setEvictionCus(int evictionCus) {
		this.evictionCus = evictionCus;
	}

	public int getLostCus() {
		return lostCus;
	}

	public void setLostCus(int lostCus) {
		this.lostCus = lostCus;
	}

}

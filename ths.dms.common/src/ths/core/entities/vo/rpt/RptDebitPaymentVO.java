package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptDebitPaymentVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Date orderDate;
	private String orderNumber;
	private BigDecimal total;
	private String shortCode;
	private String customerCode;
	private String customerName;
	private Integer debitDate;
	private Date payDate;
	private Integer paymentType;
	private String payRecievedNumber;
	private BigDecimal remain;
	private String cashierCode;
	private String cashierName;
	private String staffCode;
	private String staffName;
	private BigDecimal typeOrder;
	private BigDecimal totalPay;
	private Integer stt;
	private String groupCode;
	private String groupName;
	
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Integer getDebitDate() {
		return debitDate;
	}
	public void setDebitDate(Integer debitDate) {
		this.debitDate = debitDate;
	}
	public Date getPayDate() {
		return payDate;
	}
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	public Integer getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}
	public String getPayRecievedNumber() {
		return payRecievedNumber;
	}
	public void setPayRecievedNumber(String payRecievedNumber) {
		this.payRecievedNumber = payRecievedNumber;
	}
	public BigDecimal getRemain() {
		return remain;
	}
	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}
	public String getCashierCode() {
		return cashierCode;
	}
	public void setCashierCode(String cashierCode) {
		this.cashierCode = cashierCode;
	}
	public String getCashierName() {
		return cashierName;
	}
	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public BigDecimal getTypeOrder() {
		return typeOrder;
	}
	public void setTypeOrder(BigDecimal typeOrder) {
		this.typeOrder = typeOrder;
	}
	public BigDecimal getTotalPay() {
		return totalPay;
	}
	public void setTotalPay(BigDecimal totalPay) {
		this.totalPay = totalPay;
	}
	public Integer getStt() {
		return stt;
	}
	public void setStt(Integer stt) {
		this.stt = stt;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
}

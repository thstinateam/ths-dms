package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * @author hunglm16
 * @since January 21, 2014
 * @description report Ds3: Bao cao chi tiet chi tra hang TB - MAP RaperReport
 * */
public class PROC_3_11_BCCTCTHTB_MAP implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer stt;
	public Integer getStt() {
		return stt;
	}
	public void setStt(Integer stt) {
		this.stt = stt;
	}
	private String shopCode;
	private String shopName;
	private String province;
	private String datePrint;
	private String displayProgramCode;
	private String displayProgramName;
	private String fromDate;
	private String toDate;
	private List<PROC_3_11_BCCTCTHTB> lstDetail = new ArrayList<PROC_3_11_BCCTCTHTB>();
	
	public String getShopCode() {
		if(shopCode==null || shopCode.length()==0){
			return "";
		}
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		if(shopName==null || shopName.length()==0){
			return "";
		}
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getProvince() {
		if(province==null || province.length()==0){
			return "";
		}
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getDatePrint() {
		if(datePrint==null || datePrint.length()==0){
			return "";
		}
		return datePrint;
	}
	public void setDatePrint(String datePrint) {
		this.datePrint = datePrint;
	}
	public String getDisplayProgramCode() {
		if(displayProgramCode==null || displayProgramCode.length()==0){
			return "";
		}
		return displayProgramCode;
	}
	public void setDisplayProgramCode(String displayProgramCode) {
		this.displayProgramCode = displayProgramCode;
	}
	public String getDisplayProgramName() {
		if(displayProgramName==null || displayProgramName.length()==0){
			return "";
		}
		return displayProgramName;
	}
	public void setDisplayProgramName(String displayProgramName) {
		this.displayProgramName = displayProgramName;
	}
	public String getFromDate() {
		if(fromDate==null || fromDate.length()==0){
			return "";
		}
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		if(toDate==null || toDate.length()==0){
			return "";
		}
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public List<PROC_3_11_BCCTCTHTB> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<PROC_3_11_BCCTCTHTB> lstDetail) {
		this.lstDetail = lstDetail;
	}
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class Rpt10_1_2 implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8921340805150425581L;
	private String nvbhId;
	private String maNVBH;
	private String tenNVBH;
	private BigDecimal chiTieuThang;
	private BigDecimal doanhSoThucDatLK; //thuc dat luy ke
	private String tyLe;  // ti le
	private BigDecimal soKHActive; //so kh active
	private BigDecimal soKHPSDS; //so kh khong trung lap phat sinh doanh so
	private BigDecimal soDHTC; //so don hang thanh cong
	private BigDecimal soDHTV; // so don hang tra ve
	private BigDecimal doanhSoThucDat;
	private BigDecimal tongSOSKUDH; //tong so SKU/DH
	private BigDecimal soKHPSDSLuyKe; //so kh khong trung lap phat sinh doanh so luy ke
	private BigDecimal soDHTCLuyKe;	 	
	private BigDecimal soDHTVLuyKe;
	private BigDecimal tongSoSKUDHLuyKe;
	private BigDecimal bqSKUDH;
	
	private BigDecimal tongChiTieuNPP;
	private BigDecimal thucDatTrongKyNPP;
	private BigDecimal thucDatLuyKeNPP;
	private String tyLeDatNPP;
	
	List<Rpt10_1_2Detail> lstDetail;
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public BigDecimal getChiTieuThang() {
		return chiTieuThang;
	}
	public void setChiTieuThang(BigDecimal chiTieuThang) {
		this.chiTieuThang = chiTieuThang;
	}
	public BigDecimal getDoanhSoThucDatLK() {
		return doanhSoThucDatLK;
	}
	public void setDoanhSoThucDatLK(BigDecimal doanhSoThucDatLK) {
		this.doanhSoThucDatLK = doanhSoThucDatLK;
	}
	public String getTyLe() {
		return tyLe;
	}
	public void setTyLe(String tyLe) {
		this.tyLe = tyLe;
	}
	public BigDecimal getSoKHActive() {
		return soKHActive;
	}
	public void setSoKHActive(BigDecimal soKHActive) {
		this.soKHActive = soKHActive;
	}
	public BigDecimal getSoKHPSDS() {
		return soKHPSDS;
	}
	public void setSoKHPSDS(BigDecimal soKHPSDS) {
		this.soKHPSDS = soKHPSDS;
	}
	public BigDecimal getSoDHTC() {
		return soDHTC;
	}
	public void setSoDHTC(BigDecimal soDHTC) {
		this.soDHTC = soDHTC;
	}
	public BigDecimal getSoDHTV() {
		return soDHTV;
	}
	public void setSoDHTV(BigDecimal soDHTV) {
		this.soDHTV = soDHTV;
	}
	public BigDecimal getDoanhSoThucDat() {
		return doanhSoThucDat;
	}
	public void setDoanhSoThucDat(BigDecimal doanhSoThucDat) {
		this.doanhSoThucDat = doanhSoThucDat;
	}
	public BigDecimal getTongSOSKUDH() {
		return tongSOSKUDH;
	}
	public void setTongSOSKUDH(BigDecimal tongSOSKUDH) {
		this.tongSOSKUDH = tongSOSKUDH;
	}
	public BigDecimal getSoKHPSDSLuyKe() {
		return soKHPSDSLuyKe;
	}
	public void setSoKHPSDSLuyKe(BigDecimal soKHPSDSLuyKe) {
		this.soKHPSDSLuyKe = soKHPSDSLuyKe;
	}
	public BigDecimal getSoDHTCLuyKe() {
		return soDHTCLuyKe;
	}
	public void setSoDHTCLuyKe(BigDecimal soDHTCLuyKe) {
		this.soDHTCLuyKe = soDHTCLuyKe;
	}
	public BigDecimal getSoDHTVLuyKe() {
		return soDHTVLuyKe;
	}
	public void setSoDHTVLuyKe(BigDecimal soDHTVLuyKe) {
		this.soDHTVLuyKe = soDHTVLuyKe;
	}
	public BigDecimal getTongSoSKUDHLuyKe() {
		return tongSoSKUDHLuyKe;
	}
	public void setTongSoSKUDHLuyKe(BigDecimal tongSoSKUDHLuyKe) {
		this.tongSoSKUDHLuyKe = tongSoSKUDHLuyKe;
	}
	public BigDecimal getBqSKUDH() {
		return bqSKUDH;
	}
	public void setBqSKUDH(BigDecimal bqSKUDH) {
		this.bqSKUDH = bqSKUDH;
	}
	public List<Rpt10_1_2Detail> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<Rpt10_1_2Detail> lstDetail) {
		this.lstDetail = lstDetail;
	}
	public BigDecimal getTongChiTieuNPP() {
		return tongChiTieuNPP;
	}
	public void setTongChiTieuNPP(BigDecimal tongChiTieuNPP) {
		this.tongChiTieuNPP = tongChiTieuNPP;
	}
	public BigDecimal getThucDatTrongKyNPP() {
		return thucDatTrongKyNPP;
	}
	public void setThucDatTrongKyNPP(BigDecimal thucDatTrongKyNPP) {
		this.thucDatTrongKyNPP = thucDatTrongKyNPP;
	}
	public BigDecimal getThucDatLuyKeNPP() {
		return thucDatLuyKeNPP;
	}
	public void setThucDatLuyKeNPP(BigDecimal thucDatLuyKeNPP) {
		this.thucDatLuyKeNPP = thucDatLuyKeNPP;
	}
	public String getTyLeDatNPP() {
		return tyLeDatNPP;
	}
	public void setTyLeDatNPP(String tyLeDatNPP) {
		this.tyLeDatNPP = tyLeDatNPP;
	}
	public String getNvbhId() {
		return nvbhId;
	}
	public void setNvbhId(String nvbhId) {
		this.nvbhId = nvbhId;
	}
	
	
}

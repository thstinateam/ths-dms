package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author cangnd
 *
 */
public class RptBHTSP_DSNhanVienBH implements  Serializable{
	
	private Integer id_nhan_vien;
	private String ten_nhan_vien;
	private Integer so_luong;
	private Float thanh_tien;
	private List<RptBHTSP_DSNganhVO> danh_sach_nganh;
	

	public RptBHTSP_DSNhanVienBH(List<RptBHTSP_ThongTinTVVO> information)
	{		
				
		thanh_tien = (float)0;
		
		so_luong = 0;
		
		if(information != null && information.size() > 0)
		{
			this.ten_nhan_vien = information.get(0).getStaff_name();
		}
	
		danh_sach_nganh = new ArrayList<RptBHTSP_DSNganhVO>();
		//this.ten_nhan_vien = information.getStaff_name();
		int iCountEntity = information.size();
		List<String> lstCheck = new ArrayList<String>();
		for(int i = 0; i < iCountEntity; i++)
		{
			
			if(lstCheck.contains(String.valueOf(i)))
				continue;
			
			lstCheck.add(String.valueOf(i));
			
			List<RptBHTSP_ThongTinTVVO> groupItem = new ArrayList<RptBHTSP_ThongTinTVVO>();
			
			RptBHTSP_ThongTinTVVO rsItem = information.get(i);
			groupItem.add(rsItem);
			
			if(i == iCountEntity - 1)
			{
				RptBHTSP_DSNganhVO itemNganh = new RptBHTSP_DSNganhVO(groupItem);				
				
				danh_sach_nganh.add(itemNganh);
				
				so_luong += itemNganh.getSo_san_pham();
				thanh_tien += itemNganh.getThanh_tien();
				break;
			}
				
			int id_staff = rsItem.getStaff_id();
			for(int j = i + 1; j < iCountEntity; j++)
			{
				if(lstCheck.contains(String.valueOf(j)))
					continue;
				
				RptBHTSP_ThongTinTVVO rsItem2 = information.get(j);
				if(rsItem2.getProduct_info_code().equals(rsItem.getProduct_info_code()))
				{
					lstCheck.add(String.valueOf(j));					
					groupItem.add(rsItem2);
					
					if(j == iCountEntity - 1)
					{
						RptBHTSP_DSNganhVO itemNganh = new RptBHTSP_DSNganhVO(groupItem);
						danh_sach_nganh.add(itemNganh);
						
						so_luong += itemNganh.getSo_san_pham();
						thanh_tien += itemNganh.getThanh_tien();						
					}
				}
				else
				{
					
					RptBHTSP_DSNganhVO itemNganh = new RptBHTSP_DSNganhVO(groupItem);				
					
					danh_sach_nganh.add(itemNganh);
					
					so_luong += itemNganh.getSo_san_pham();
					thanh_tien += itemNganh.getThanh_tien();
					
					//i = j;
					break;
				}
			}
			

		}
		
		
		int a = 4;
		a--;
		
	}
	
	
	public void setTen_nhan_vien(String ten_nhan_vien) {
		this.ten_nhan_vien = ten_nhan_vien;
	}

	public String getTen_nhan_vien() {
		return ten_nhan_vien;
	}

	public void setSo_luong(Integer so_luong) {
		this.so_luong = so_luong;
	}

	public Integer getSo_luong() {
		return so_luong;
	}



	public void setDanh_sach_nganh(List<RptBHTSP_DSNganhVO> danh_sach_nganh) {
		this.danh_sach_nganh = danh_sach_nganh;
		
		int soLuong= 0;
		Float tongTien = (float)0;
		
		for (RptBHTSP_DSNganhVO nganh_item : danh_sach_nganh) {
			soLuong += nganh_item.getSo_san_pham();
			tongTien += nganh_item.getThanh_tien();
		}
	}

	public List<RptBHTSP_DSNganhVO> getDanh_sach_nganh() {
		return danh_sach_nganh;
	}

	private void setId_nhan_vien(Integer id_nhan_vien) {
		this.id_nhan_vien = id_nhan_vien;
	}


	private Integer getId_nhan_vien() {
		return id_nhan_vien;
	}


	public void setThanh_tien(Float thanh_tien) {
		this.thanh_tien = thanh_tien;
	}


	public Float getThanh_tien() {
		return thanh_tien;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RptBCKD18VO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date fromMonth;
	private Date toMonth;
	private List<RptBCKD18CTVO> lstDetail;
	public List<RptBCKD18CTVO> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<RptBCKD18CTVO> lstDetail) {
		this.lstDetail = lstDetail;
	}
	public  Date getFromMonth() {
		return fromMonth;
	}
	public  void setFromMonth(Date fromMonth) {
		this.fromMonth = fromMonth;
	}
	public Date getToMonth() {
		return toMonth;
	}
	public  void setToMonth(Date toMonth) {
		this.toMonth = toMonth;
	}
	
	
}

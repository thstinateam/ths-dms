package ths.core.entities.vo.rpt;

import java.io.Serializable;

public class RptBHTSP_SanPhamVO implements Serializable{
	private String ma_hang;
	private String ten_hang;
	private Float don_gia;
	private Integer so_luong;
	private Float thanh_tien;
	private String nganh;
	
	public RptBHTSP_SanPhamVO(RptBHTSP_ThongTinTVVO object)
	{
		ma_hang = object.getProduct_code();
		ten_hang = object.getProduct_name();
		setDon_gia(object.getPrice());
		so_luong = object.getQuantity();
		
		setThanh_tien((object.getQuantity() * object.getPrice()));
		
		nganh = object.getProduct_info_code();
	}
	
	public void setTen_hang(String ten_hang) {
		this.ten_hang = ten_hang;
	}
	
	public String getTen_hang() {
		return ten_hang;
	}

	public void setMa_hang(String ma_hang) {
		this.ma_hang = ma_hang;
	}

	public String getMa_hang() {
		return ma_hang;
	}



	public void setSo_luong(Integer so_luong) 
	{
		this.so_luong = so_luong;
		this.thanh_tien = this.so_luong * this.don_gia;
	}

	public Integer getSo_luong() {
		return so_luong;
	}

	public void setDon_gia(Float don_gia) {
		this.don_gia = don_gia;
	}

	public Float getDon_gia() {
		return don_gia;
	}

	public void setThanh_tien(Float thanh_tien) {
		this.thanh_tien = thanh_tien;
	}

	public Float getThanh_tien() {
		return thanh_tien;
	}

	public void setNganh(String nganh) {
		this.nganh = nganh;
	}

	public String getNganh() {
		return nganh;
	}




}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

//doanh so phan phoi mien
public class RptDSBHMienCTVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long mienId;
	private String mien;
	private String nhom;
	private String nhan;
	private String maSP;
	private String tenSP;
	private BigDecimal keHoachTT;//ke hoach tieu thu
	private BigDecimal keHoachLK;//ke hoach luy ke
	private BigDecimal keHoachBQNgay;//ke hoach binh quan ngay
	private BigDecimal thTrongNgay;//thuc hien trong ngay
	private BigDecimal thLuyke;//thuc hien luy ke
	private BigDecimal duThieuLK;//du thieu luy ke
	private BigDecimal tongThangTruoc;//thuc hien luy ke
	private BigDecimal luyKeThangTruoc;//thuc hien luy ke
	private Integer ptThucHien;//% thuc hien
	
	
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public Long getMienId() {
		return mienId;
	}
	public void setMienId(BigDecimal mienId) {
		if (mienId != null) {
			this.mienId = mienId.longValue();
		}
	}
	public String getNhom() {
		return nhom;
	}
	public void setNhom(String nhom) {
		this.nhom = nhom;
	}
	public String getNhan() {
		return nhan;
	}
	public void setNhan(String nhan) {
		this.nhan = nhan;
	}
	public String getMaSP() {
		return maSP;
	}
	public void setMaSP(String maSP) {
		this.maSP = maSP;
	}
	public String getTenSP() {
		return tenSP;
	}
	public void setTenSP(String tenSP) {
		this.tenSP = tenSP;
	}
	public BigDecimal getKeHoachTT() {
		return keHoachTT;
	}
	public void setKeHoachTT(BigDecimal keHoachTT) {
		this.keHoachTT = keHoachTT;
	}
	public BigDecimal getKeHoachLK() {
		return keHoachLK;
	}
	public void setKeHoachLK(BigDecimal keHoachLK) {
		this.keHoachLK = keHoachLK;
	}
	public BigDecimal getKeHoachBQNgay() {
		return keHoachBQNgay;
	}
	public void setKeHoachBQNgay(BigDecimal keHoachBQNgay) {
		this.keHoachBQNgay = keHoachBQNgay;
	}
	
	public BigDecimal getDuThieuLK() {
		return duThieuLK;
	}
	public void setDuThieuLK(BigDecimal duThieuLK) {
		this.duThieuLK = duThieuLK;
	}
	public BigDecimal getTongThangTruoc() {
		return tongThangTruoc;
	}
	public void setTongThangTruoc(BigDecimal tongThangTruoc) {
		this.tongThangTruoc = tongThangTruoc;
	}
	public BigDecimal getLuyKeThangTruoc() {
		return luyKeThangTruoc;
	}
	public void setLuyKeThangTruoc(BigDecimal luyKeThangTruoc) {
		this.luyKeThangTruoc = luyKeThangTruoc;
	}
	public Integer getPtThucHien() {
		return ptThucHien;
	}
	public void setPtThucHien(BigDecimal ptThucHien) {
		if (ptThucHien != null) {
			this.ptThucHien = ptThucHien.intValue();
		}
	}
	public BigDecimal getThTrongNgay() {
		return thTrongNgay;
	}
	public void setThTrongNgay(BigDecimal thTrongNgay) {
		this.thTrongNgay = thTrongNgay;
	}
	public BigDecimal getThLuyke() {
		return thLuyke;
	}
	public void setThLuyke(BigDecimal thLuyke) {
		this.thLuyke = thLuyke;
	}
}

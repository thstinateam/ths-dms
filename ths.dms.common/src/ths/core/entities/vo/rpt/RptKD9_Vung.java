package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RptKD9_Vung implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7553383070496711196L;
	private String mien;
	private String vung;
	private BigDecimal tonDauKy1;
	private BigDecimal totalNhap;
	private BigDecimal totalXuat;
	private BigDecimal cuoiKy1;
	private BigDecimal giaTriCKy1;
	private BigDecimal soLuongBan;
	private BigDecimal giaTriBan;
	private BigDecimal soLuongConLai;
	
	private List<RptKD9_Detail> listDetail;

	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public BigDecimal getTonDauKy1() {
		return tonDauKy1;
	}

	public void setTonDauKy1(BigDecimal tonDauKy1) {
		this.tonDauKy1 = tonDauKy1;
	}

	public BigDecimal getTotalNhap() {
		return totalNhap;
	}

	public void setTotalNhap(BigDecimal totalNhap) {
		this.totalNhap = totalNhap;
	}

	public BigDecimal getTotalXuat() {
		return totalXuat;
	}

	public void setTotalXuat(BigDecimal totalXuat) {
		this.totalXuat = totalXuat;
	}

	public BigDecimal getCuoiKy1() {
		return cuoiKy1;
	}

	public void setCuoiKy1(BigDecimal cuoiKy1) {
		this.cuoiKy1 = cuoiKy1;
	}

	public BigDecimal getGiaTriCKy1() {
		return giaTriCKy1;
	}

	public void setGiaTriCKy1(BigDecimal giaTriCKy1) {
		this.giaTriCKy1 = giaTriCKy1;
	}

	public BigDecimal getSoLuongBan() {
		return soLuongBan;
	}

	public void setSoLuongBan(BigDecimal soLuongBan) {
		this.soLuongBan = soLuongBan;
	}

	public BigDecimal getGiaTriBan() {
		return giaTriBan;
	}

	public void setGiaTriBan(BigDecimal giaTriBan) {
		this.giaTriBan = giaTriBan;
	}

	public BigDecimal getSoLuongConLai() {
		return soLuongConLai;
	}

	public void setSoLuongConLai(BigDecimal soLuongConLai) {
		this.soLuongConLai = soLuongConLai;
	}

	public List<RptKD9_Detail> getListDetail() {
		return listDetail;
	}

	public void setListDetail(List<RptKD9_Detail> listDetail) {
		this.listDetail = listDetail;
		if(null != listDetail && listDetail.size() > 0){
			soLuongConLai = giaTriBan = tonDauKy1 = totalNhap = totalXuat = cuoiKy1 = giaTriCKy1 = soLuongBan = BigDecimal.ZERO;

			for(int i = 0; i<listDetail.size(); i++){
				if(null != listDetail.get(i).getSoLuongConLai()){
					soLuongConLai = soLuongConLai.add(listDetail.get(i).getSoLuongConLai());
				}
				if(null != listDetail.get(i).getGiaTriBan()){
					giaTriBan = giaTriBan.add(listDetail.get(i).getGiaTriBan());
				}
				if(null != listDetail.get(i).getTonDauKy1()){
					tonDauKy1 = tonDauKy1.add(listDetail.get(i).getTonDauKy1());
				}
				if(null != listDetail.get(i).getTotalNhap()){
					totalNhap = totalNhap.add(listDetail.get(i).getTotalNhap());
				}
				if(null != listDetail.get(i).getTotalXuat()){
					totalXuat = totalXuat.add(listDetail.get(i).getTotalXuat());
				}
				if(null != listDetail.get(i).getCuoiKy1()){
					cuoiKy1 = cuoiKy1.add(listDetail.get(i).getCuoiKy1());
				}
				if(null != listDetail.get(i).getGiaTriCKy1()){
					giaTriCKy1 = giaTriCKy1.add(listDetail.get(i).getGiaTriCKy1());
				}
				if(null != listDetail.get(i).getSoLuongBan()){
					soLuongBan = soLuongBan.add(listDetail.get(i).getSoLuongBan());
				}
			}
		}
	}
	
	
}

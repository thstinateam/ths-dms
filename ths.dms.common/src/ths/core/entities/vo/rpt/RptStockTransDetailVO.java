package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;

public class RptStockTransDetailVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String shopCode;
	private String shopName;
	private String address;
	private Date stockTransDate;
	private Integer fromOwnerType;
	private Integer toOwnerType;
	private String productCode;
	private String productName;
	private String lot;
	private Integer quantity;
	/**
	 * @return the shopCode
	 */
	public String getShopCode() {
		return shopCode;
	}
	/**
	 * @param shopCode the shopCode to set
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}
	/**
	 * @param shopName the shopName to set
	 */
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the stockTransDate
	 */
	public Date getStockTransDate() {
		return stockTransDate;
	}
	/**
	 * @param stockTransDate the stockTransDate to set
	 */
	public void setStockTransDate(Date stockTransDate) {
		this.stockTransDate = stockTransDate;
	}
	/**
	 * @return the fromOwnerType
	 */
	public Integer getFromOwnerType() {
		return fromOwnerType;
	}
	/**
	 * @param fromOwnerType the fromOwnerType to set
	 */
	public void setFromOwnerType(Integer fromOwnerType) {
		this.fromOwnerType = fromOwnerType;
	}
	/**
	 * @return the toOwnerType
	 */
	public Integer getToOwnerType() {
		return toOwnerType;
	}
	/**
	 * @param toOwnerType the toOwnerType to set
	 */
	public void setToOwnerType(Integer toOwnerType) {
		this.toOwnerType = toOwnerType;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the lot
	 */
	public String getLot() {
		return lot;
	}
	/**
	 * @param lot the lot to set
	 */
	public void setLot(String lot) {
		this.lot = lot;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
}

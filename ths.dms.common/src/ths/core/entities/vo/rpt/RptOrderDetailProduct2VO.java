package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class RptOrderDetailProduct2VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String productCode;
	private String productName;
	private BigDecimal price;
	private String orderDate;
	private ArrayList<RptOrderDetailProduct3VO> lstRptOrderDetailProduct3VO = new ArrayList<RptOrderDetailProduct3VO>();

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the orderDate
	 */
	public String getOrderDate() {
		return orderDate;
	}

	/**
	 * @param orderDate
	 *            the orderDate to set
	 */
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	/**
	 * @return the lstRptOrderDetailProduct1VO
	 */
	public ArrayList<RptOrderDetailProduct3VO> getLstRptOrderDetailProduct3VO() {
		return lstRptOrderDetailProduct3VO;
	}

	/**
	 * @param lstRptOrderDetailProduct1VO
	 *            the lstRptOrderDetailProduct1VO to set
	 */
	public void setLstRptOrderDetailProduct3VO(
			ArrayList<RptOrderDetailProduct3VO> lstRptOrderDetailProduct3VO) {
		this.lstRptOrderDetailProduct3VO = lstRptOrderDetailProduct3VO;
	}

}

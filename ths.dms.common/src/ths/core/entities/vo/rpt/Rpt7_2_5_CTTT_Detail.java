package ths.core.entities.vo.rpt;

import java.math.BigDecimal;
import java.util.Date;


public class Rpt7_2_5_CTTT_Detail implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 710560671268651768L;
	private String code;
	private String staffCode;
	private Date invoiceDate; //Ngay hoa don
	private String invoiceNumber; //So hoa don
	private String debitType; //Loai no
	private Date payDate;   //Ngay thanh toan
	private String payType;    //Loai thanh toan
	private String payNumber;  //So chung tu thanh toan
	private BigDecimal amount; //tien tren hoa don
	private BigDecimal payAmount; //Tien thanh toan
	private BigDecimal debitAmount; //Tien con no
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getDebitType() {
		return debitType;
	}
	public void setDebitType(String debitType) {
		this.debitType = debitType;
	}
	public Date getPayDate() {
		return payDate;
	}
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getPayNumber() {
		return payNumber;
	}
	public void setPayNumber(String payNumber) {
		this.payNumber = payNumber;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getPayAmount() {
		return payAmount;
	}
	public void setPayAmount(BigDecimal payAmount) {
		this.payAmount = payAmount;
	}
	public BigDecimal getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}
	
	
}

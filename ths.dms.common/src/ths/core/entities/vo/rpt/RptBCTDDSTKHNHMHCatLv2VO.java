package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

public class RptBCTDDSTKHNHMHCatLv2VO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal customerId;
	private String customerCode;
	private String customerName;
	private String shortCode;

	private BigDecimal catId;
	private String catCode;
	private String catName;
	private BigDecimal amountMoney;
	
	private List<RptBCTDDSTKHNHMHSubCatLv3VO> lstDataLv3;	
	
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	
	public BigDecimal getCustomerId() {
		return customerId;
	}

	public void setCustomerId(BigDecimal customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public BigDecimal getCatId() {
		return catId;
	}

	public void setCatId(BigDecimal catId) {
		this.catId = catId;
	}

	public String getCatCode() {
		if(null != catName && !catName.equals("")){
			return catCode + " - " + catName;
		}
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	
	public void setAmountMoney(BigDecimal amountMoney) {
		this.amountMoney = amountMoney;
	}	
	public BigDecimal getAmountMoney() {		
		return amountMoney;		
	}
	
	public List<RptBCTDDSTKHNHMHSubCatLv3VO> getLstDataLv3() {
		return lstDataLv3;
	}

	
	public void setLstDataLv3(List<RptBCTDDSTKHNHMHSubCatLv3VO> lstDataLv3) {
		this.lstDataLv3 = lstDataLv3;
		if(null != lstDataLv3 && lstDataLv3.size() > 0){
			amountMoney = BigDecimal.ZERO;
			for(int i = 0; i<lstDataLv3.size(); i++){
				if(null != lstDataLv3.get(i).getAmountMoney()){
					amountMoney = amountMoney.add(lstDataLv3.get(i).getAmountMoney());
				}				
			}		
		}	
	}
	
	public RptBCTDDSTKHNHMHCatLv2VO() {		
	}
	
	
}

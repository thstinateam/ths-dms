package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class RptActionLogVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String parentSuperShopCode; //mien
	private String superShopCode; //vung
	private String shopCode; //npp
	private String shopName; //ten npp
	private String superName; //ten gsnpp
	private String staffCode; //nvbh
	private String staffName; //ten nvbh
	private Date startTime; //thoi gian bat dau
	private Date endTime; //thoi gian ket thuc
	private Integer totalVisit; //tong so khach hang ghe tham
	private Integer numWrongRouting; //ngoai tuyen
	private Integer numCorrectRouting; //dung tuyen
	private Integer numLess2Minute; // nho hon 5'
	private Integer numBetween2And30Minute; //tu 5-30p
	private Integer numOver30Minute; //tu 30-60p
	private Integer numOver60Minute; //> 60p
	private BigDecimal totalVisitTime; //tong thoi gian ghe tham
	private BigDecimal avgVisitTime;
	
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	
	public String getParentSuperShopCode() {
		return parentSuperShopCode;
	}
	public void setParentSuperShopCode(String parentSuperShopCode) {
		this.parentSuperShopCode = parentSuperShopCode;
	}
	public String getSuperShopCode() {
		return superShopCode;
	}
	public void setSuperShopCode(String superShopCode) {
		this.superShopCode = superShopCode;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getSuperName() {
		return superName;
	}
	public void setSuperName(String superName) {
		this.superName = superName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Integer getTotalVisit() {
		return totalVisit;
	}
	public void setTotalVisit(BigDecimal totalVisit) {
		this.totalVisit = totalVisit.intValue();
	}
	public Integer getNumWrongRouting() {
		return numWrongRouting;
	}
	public void setNumWrongRouting(BigDecimal numWrongRouting) {
		this.numWrongRouting = numWrongRouting.intValue();
	}
	public Integer getNumCorrectRouting() {
		return numCorrectRouting;
	}
	public void setNumCorrectRouting(BigDecimal numCorrectRouting) {
		this.numCorrectRouting = numCorrectRouting.intValue();
	}
	public Integer getNumLess2Minute() {
		return numLess2Minute;
	}
	public void setNumLess2Minute(BigDecimal numLess2Minute) {
		this.numLess2Minute = numLess2Minute.intValue();
	}
	public Integer getNumBetween2And30Minute() {
		return numBetween2And30Minute;
	}
	public void setNumBetween2And30Minute(BigDecimal numBetween2And30Minute) {
		this.numBetween2And30Minute = numBetween2And30Minute.intValue();
	}
	public Integer getNumOver30Minute() {
		return numOver30Minute;
	}
	public void setNumOver30Minute(BigDecimal numOver30Minute) {
		this.numOver30Minute = numOver30Minute.intValue();
	}
	public Integer getNumOver60Minute() {
		return numOver60Minute;
	}
	public void setNumOver60Minute(BigDecimal numOver60Minute) {
		this.numOver60Minute = numOver60Minute.intValue();
	}
	public BigDecimal getTotalVisitTime() {
		return totalVisitTime;
	}
	public void setTotalVisitTime(BigDecimal totalVisitTime) {
		this.totalVisitTime = totalVisitTime;
	}
	public void setAvgVisitTime(BigDecimal avgVisitTime) {
		this.avgVisitTime = avgVisitTime;
	}
	public BigDecimal getAvgVisitTime() {
		int a = (numWrongRouting == null ? 0 : numWrongRouting) + (numCorrectRouting == null ? 0 : numCorrectRouting);
		
		if(a == 0) {
			return BigDecimal.ZERO;
		}
		
		this.avgVisitTime = totalVisitTime.divide(new BigDecimal(a), 2, RoundingMode.HALF_UP);
		return avgVisitTime;
	}
}

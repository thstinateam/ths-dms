package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author hungnm
 *
 */
public class RptBCTCNVO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//no qua han
	private BigDecimal overdueDebt;
	
	//no den han
	private BigDecimal nextDebt;
	
	//no chua den han
	private BigDecimal availableDebt;
	
	private BigDecimal total;
	
	private String customerInfo;
	
	private String staffInfo;
	
	private List<RptBCTCNDetailVO> lstDetail;

	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	
	public BigDecimal getOverdueDebt() {
		return overdueDebt;
	}

	public void setOverdueDebt(BigDecimal overdueDebt) {
		this.overdueDebt = overdueDebt;
	}

	public BigDecimal getNextDebt() {
		return nextDebt;
	}

	public void setNextDebt(BigDecimal nextDebt) {
		this.nextDebt = nextDebt;
	}

	public BigDecimal getAvailableDebt() {
		return availableDebt;
	}

	public void setAvailableDebt(BigDecimal availableDebt) {
		this.availableDebt = availableDebt;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(String customerInfo) {
		this.customerInfo = customerInfo;
	}

	public String getStaffInfo() {
		return staffInfo;
	}

	public void setStaffInfo(String staffInfo) {
		this.staffInfo = staffInfo;
	}

	public List<RptBCTCNDetailVO> getLstDetail() {
		return lstDetail;
	}

	public void setLstDetail(List<RptBCTCNDetailVO> lstDetail) {
		this.lstDetail = lstDetail;
	}
	
}

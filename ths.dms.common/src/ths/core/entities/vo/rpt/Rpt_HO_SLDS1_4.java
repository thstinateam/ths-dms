package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;


/**
 * vo: bao cao SLDS 1.4
 * 
 * @author lacnv1
 * @since Mar 24, 2014
 */
public class Rpt_HO_SLDS1_4 implements Serializable {

	private static final long serialVersionUID = 1L;

	private String mien;
	private String vung;
	private String npp;
	private String tenNPP;
	private String staffCode;
	private String staffName;
	private List<DynamicVO> lstValue;
	
	public String getMien() {
		return mien;
	}
	
	public void setMien(String mien) {
		this.mien = mien;
	}
	
	public String getVung() {
		return vung;
	}
	
	public void setVung(String vung) {
		this.vung = vung;
	}
	
	public String getNpp() {
		return npp;
	}
	
	public void setNpp(String npp) {
		this.npp = npp;
	}
	
	public String getStaffCode() {
		return staffCode;
	}
	
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	
	public String getStaffName() {
		return staffName;
	}
	
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
	public List<DynamicVO> getLstValue() {
		return lstValue;
	}
	
	public void setLstValue(List<DynamicVO> lstValue) {
		this.lstValue = lstValue;
	}

	public String getTenNPP() {
		return tenNPP;
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
}
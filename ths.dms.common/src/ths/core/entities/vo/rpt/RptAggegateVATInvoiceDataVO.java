package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptAggegateVATInvoiceDataVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String staffCode;
	private String invoiceNumber;
	private Date invoiceDate;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String customerTax;
	private Integer SKU;
	private Integer VAT;
	private BigDecimal amount;
	private BigDecimal amountWithTax;
	private Integer status;
	private String orderNumber;

	public RptAggegateVATInvoiceDataVO() {

	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerTax() {
		return customerTax;
	}

	public void setCustomerTax(String customerTax) {
		this.customerTax = customerTax;
	}

	public Integer getSKU() {
		return SKU;
	}

	public void setSKU(Integer sKU) {
		SKU = sKU;
	}

	public Integer getVAT() {
		return VAT;
	}

	public void setVAT(Integer vAT) {
		VAT = vAT;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getAmountWithTax() {
		return amountWithTax;
	}

	public void setAmountWithTax(BigDecimal amountWithTax) {
		this.amountWithTax = amountWithTax;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptBookProductVinamilkVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String poAutoNumber;
	private Date poAutoDate;
	private Integer status;
	private Integer quantity;
	private Float priceValue;
	private String productCode;
	private String productName;
	private String uom1;
	private BigDecimal amount;
	private Float grossWeight;
	/**
	 * @return the poAutoNumber
	 */
	public String getPoAutoNumber() {
		return poAutoNumber;
	}
	/**
	 * @param poAutoNumber the poAutoNumber to set
	 */
	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}
	/**
	 * @return the poAutoDate
	 */
	public Date getPoAutoDate() {
		return poAutoDate;
	}
	/**
	 * @param poAutoDate the poAutoDate to set
	 */
	public void setPoAutoDate(Date poAutoDate) {
		this.poAutoDate = poAutoDate;
	}
	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the priceValue
	 */
	public Float getPriceValue() {
		return priceValue;
	}
	/**
	 * @param priceValue the priceValue to set
	 */
	public void setPriceValue(Float priceValue) {
		this.priceValue = priceValue;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the uom1
	 */
	public String getUom1() {
		return uom1;
	}
	/**
	 * @param uom1 the uom1 to set
	 */
	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}
	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * @return the grossWeight
	 */
	public Float getGrossWeight() {
		return grossWeight;
	}
	/**
	 * @param grossWeight the grossWeight to set
	 */
	public void setGrossWeight(Float grossWeight) {
		this.grossWeight = grossWeight;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptCycleCountDiff1VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cycleCountCode;// 1
	private List<RptCycleCountDiff2VO> lstRptCycleCountDiff2VO = new ArrayList<RptCycleCountDiff2VO>();
	private Integer quantityCounted = 0;// 5
	private Integer quantityBeforeCount = 0;// 6
	private Integer quantityDiff = 0;// 8
	private BigDecimal amountDiff = new BigDecimal(0);// 9
	
	public String getCycleCountCode() {
		return cycleCountCode;
	}

	public void setCycleCountCode(String cycleCountCode) {
		this.cycleCountCode = cycleCountCode;
	}

	public List<RptCycleCountDiff2VO> getLstRptCycleCountDiff2VO() {
		return lstRptCycleCountDiff2VO;
	}

	public void setLstRptCycleCountDiff2VO(
			List<RptCycleCountDiff2VO> lstRptCycleCountDiff2VO) {
		this.lstRptCycleCountDiff2VO = lstRptCycleCountDiff2VO;
	}

	public Integer getQuantityCounted() {
		return quantityCounted;
	}

	public void setQuantityCounted(Integer quantityCounted) {
		this.quantityCounted = quantityCounted;
	}

	public Integer getQuantityBeforeCount() {
		return quantityBeforeCount;
	}

	public void setQuantityBeforeCount(Integer quantityBeforeCount) {
		this.quantityBeforeCount = quantityBeforeCount;
	}

	public Integer getQuantityDiff() {
		return quantityDiff;
	}

	public void setQuantityDiff(Integer quantityDiff) {
		this.quantityDiff = quantityDiff;
	}

	public BigDecimal getAmountDiff() {
		return amountDiff;
	}

	public void setAmountDiff(BigDecimal amountDiff) {
		this.amountDiff = amountDiff;
	}

}

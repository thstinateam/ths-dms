package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

public class RptBCVT5VO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6559446005501423414L;
	private List<String> lstDay;
	private List<RptAbsentVO> lstValue;
	public List<String> getLstDay() {
		return lstDay;
	}
	public void setLstDay(List<String> lstDay) {
		this.lstDay = lstDay;
	}
	public List<RptAbsentVO> getLstValue() {
		return lstValue;
	}
	public void setLstValue(List<RptAbsentVO> lstValue) {
		this.lstValue = lstValue;
	}

}
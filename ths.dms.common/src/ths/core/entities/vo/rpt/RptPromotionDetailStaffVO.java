package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

import ths.dms.core.entities.Shop;

public class RptPromotionDetailStaffVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<RptPromotionDetailStaff1VO> lstRptPromotionDetailStaff1VO = new ArrayList<RptPromotionDetailStaff1VO>();
	private Shop shop;

	/**
	 * @return the lstRptPromotionDetailStaff1VO
	 */
	public ArrayList<RptPromotionDetailStaff1VO> getLstRptPromotionDetailStaff1VO() {
		return lstRptPromotionDetailStaff1VO;
	}

	/**
	 * @param lstRptPromotionDetailStaff1VO
	 *            the lstRptPromotionDetailStaff1VO to set
	 */
	public void setLstRptPromotionDetailStaff1VO(
			ArrayList<RptPromotionDetailStaff1VO> lstRptPromotionDetailStaff1VO) {
		this.lstRptPromotionDetailStaff1VO = lstRptPromotionDetailStaff1VO;
	}

	/**
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * @param shop the shop to set
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}
}

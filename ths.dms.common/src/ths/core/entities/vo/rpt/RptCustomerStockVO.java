package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

import ths.dms.core.entities.Shop;

public class RptCustomerStockVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Shop shop;
	private ArrayList<RptCustomerStock1VO> lstRptCustomerStock1VO = new ArrayList<RptCustomerStock1VO>();

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public ArrayList<RptCustomerStock1VO> getLstRptCustomerStock1VO() {
		return lstRptCustomerStock1VO;
	}

	public void setLstRptCustomerStock1VO(ArrayList<RptCustomerStock1VO> lstRptCustomerStock1VO) {
		this.lstRptCustomerStock1VO = lstRptCustomerStock1VO;
	}

}

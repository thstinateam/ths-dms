package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD6VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mien;
	private String vung;
    private String nhomHang;
    private BigDecimal keHoachNam;
    private BigDecimal keHoachThang; 
    private BigDecimal nhapHangTrongNgay;
    private BigDecimal nhapHangThang;

    private BigDecimal banHangTrongNgay; 
    private BigDecimal banHangThang;
    private BigDecimal luyKeNamTruoc; 
    private BigDecimal luyKeNamNay; 
    private BigDecimal soNgayBan;

    //khong lay tu db
	private Float tyLeTHKHNhapHang; 
	private Float tyLeTHKHBanHang; 
	private BigDecimal doanhSoChenhLech; 
	private BigDecimal chenhLechNgayBan;
	private Float phanTramTangTruong; 

	
	public Float getTyLeTHKHNhapHang() {
		if (this.getKeHoachThang() != null && !this.getKeHoachThang().equals(new BigDecimal(0)))
			return this.getNhapHangThang().divide(this.getKeHoachThang()).floatValue();
		return null;
	}
	public Float getTyLeTHKHBanHang() {
		if (this.getKeHoachThang() != null && !this.getKeHoachThang().equals(new BigDecimal(0)))
			return this.getBanHangThang().divide(this.getKeHoachThang()).floatValue();
		return null;
	}
	public BigDecimal getDoanhSoChenhLech() {
		return this.getNhapHangThang().subtract(this.getBanHangThang());
	}
	public BigDecimal getChenhLechNgayBan() {
		if (!this.getKeHoachThang().equals(new BigDecimal(0)) && !this.getSoNgayBan().equals(new BigDecimal(0)))
			return this.getDoanhSoChenhLech()
				.divide(this.getKeHoachThang()
						.divide(this.getSoNgayBan()));
		return null;
	}
	public Float getPhanTramTangTruong() {
		if (this.getLuyKeNamTruoc().equals(new BigDecimal(0)))
			return null;
		return this.getLuyKeNamNay().divide(this.getLuyKeNamTruoc()).floatValue();
	}
	//------------------------------------------
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getNhomHang() {
		return nhomHang;
	}
	public void setNhomHang(String nhomHang) {
		this.nhomHang = nhomHang;
	}
	public BigDecimal getKeHoachNam() {
		return keHoachNam;
	}
	public void setKeHoachNam(BigDecimal keHoachNam) {
		this.keHoachNam = keHoachNam;
	}
	public BigDecimal getKeHoachThang() {
		return keHoachThang;
	}
	public void setKeHoachThang(BigDecimal keHoachThang) {
		this.keHoachThang = keHoachThang;
	}
	public BigDecimal getNhapHangTrongNgay() {
		return nhapHangTrongNgay;
	}
	public void setNhapHangTrongNgay(BigDecimal nhapHangTrongNgay) {
		this.nhapHangTrongNgay = nhapHangTrongNgay;
	}
	public BigDecimal getNhapHangThang() {
		return nhapHangThang;
	}
	public void setNhapHangThang(BigDecimal nhapHangThang) {
		this.nhapHangThang = nhapHangThang;
	}
	public BigDecimal getBanHangTrongNgay() {
		return banHangTrongNgay;
	}
	public void setBanHangTrongNgay(BigDecimal banHangTrongNgay) {
		this.banHangTrongNgay = banHangTrongNgay;
	}
	public BigDecimal getBanHangThang() {
		return banHangThang;
	}
	public void setBanHangThang(BigDecimal banHangThang) {
		this.banHangThang = banHangThang;
	}
	public BigDecimal getLuyKeNamTruoc() {
		return luyKeNamTruoc;
	}
	public void setLuyKeNamTruoc(BigDecimal luyKeNamTruoc) {
		this.luyKeNamTruoc = luyKeNamTruoc;
	}
	public BigDecimal getLuyKeNamNay() {
		return luyKeNamNay;
	}
	public void setLuyKeNamNay(BigDecimal luyKeNamNay) {
		this.luyKeNamNay = luyKeNamNay;
	}
	public BigDecimal getSoNgayBan() {
		return soNgayBan;
	}
	public void setSoNgayBan(BigDecimal soNgayBan) {
		this.soNgayBan = soNgayBan;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
}

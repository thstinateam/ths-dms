package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

public class RptExSaleOrder1VO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String productCode;
	
	private String productName;
	
	private Integer checkLot;
	
	private BigDecimal price;	
	
	private BigDecimal saleAmount = new BigDecimal(0);
	
	private BigDecimal promoAmount = new BigDecimal(0);
	
	private BigDecimal discountAmount = new BigDecimal(0);
	
	private BigDecimal totalWeight = new BigDecimal(0);
	
	private String promotionName;
	
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
		}
	}
	
	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	private List<RptExSaleOrderVO> lstRptExSaleOrderVO;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getSaleAmount() {
		return saleAmount;
	}

	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}

	public BigDecimal getPromoAmount() {
		return promoAmount;
	}

	public void setPromoAmount(BigDecimal promoAmount) {
		this.promoAmount = promoAmount;
	}

	public List<RptExSaleOrderVO> getLstRptExSaleOrderVO() {
		return lstRptExSaleOrderVO;
	}

	public void setLstRptExSaleOrderVO(List<RptExSaleOrderVO> lstRptExSaleOrderVO) {
		this.lstRptExSaleOrderVO = lstRptExSaleOrderVO;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public BigDecimal getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}
	
}
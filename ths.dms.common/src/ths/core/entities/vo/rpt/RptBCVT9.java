package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCVT9 implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String mien;
	private String vung;
	private String npp;
	private String tenNPP;
	private String maNV;	
	private String tenNV;
	private String maKH;
	private String tenKH;
	private String maCT;
	private String tenCT;
	private Integer soLanChup;
	private Integer soHinhDat;	
	
	
	public void safeSetNull() throws IllegalAccessException  {
		for(Field field:getClass().getDeclaredFields()) {
			if(field.getType().equals(Integer.class) && field.get(this) == null) {
				field.set(this, 0);
			}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
				field.set(this, BigDecimal.ZERO);
			}else if(field.getType().equals(Float.class) && field.get(this) == null) {
				field.set(this, 0f);
			}else if(field.getType().equals(Double.class) && field.get(this) == null) {
				field.set(this, 0d);
			}else if(field.getType().equals(Long.class) && field.get(this) == null) {
				field.set(this, 0l);
			}else if(field.getType().equals(String.class) && field.get(this) == null) {
				field.set(this, "");
			}
		}
	}	
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getNpp() {
		return npp;
	}
	public void setNpp(String npp) {
		this.npp = npp;
	}
	public String getMaNV() {
		return maNV;
	}
	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}
	public String getTenNV() {
		return tenNV;
	}
	public void setTenNV(String tenNV) {
		this.tenNV = tenNV;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getMaCT() {
		return maCT;
	}
	public void setMaCT(String maCT) {
		this.maCT = maCT;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getTenCT() {
		return tenCT;
	}
	public void setTenCT(String tenCT) {
		this.tenCT = tenCT;
	}
	public Integer getSoLanChup() {
		return soLanChup;
	}
	public void setSoLanChup(Integer soLanChup) {
		if(null != soLanChup){
			this.soLanChup = soLanChup;
		} else{
			this.soLanChup = 0;
		}
	}
	public Integer getSoHinhDat() {
		return soHinhDat;
	}
	public void setSoHinhDat(Integer soHinhDat) {
		if(null!=soHinhDat){
			this.soHinhDat = soHinhDat;
		}else{
			this.soHinhDat = 0;
		}
	}

		
}

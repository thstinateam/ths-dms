package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class RptPTHCKHVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String orderNumber;//hoa don so
	private String rootInvoiceNumber;//hoa don goc
	private String customerName;//ten khach hang
	private String customerAddress;//dia chi gh
	private String phone;
	private String orderDate;//ngay hoa don	
	private String staffCode;
	private String deliveryCode;
	private String staffName;
	private String deliveryName;
	private String shortCode;
	private List<RptPTHCKHProductVO> listRptPTHCKHProductVO;//danh sach cac mat hang
	private BigDecimal amount;//tong thanh tien(da co vat)
	private BigDecimal discountAmount;//chiet khau tien
	private BigDecimal discountOrder;
	private BigDecimal totalPay;
	
	
	public String getRootInvoiceNumber() {
		return rootInvoiceNumber;
	}
	public void setRootInvoiceNumber(String rootInvoiceNumber) {
		this.rootInvoiceNumber = rootInvoiceNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getDeliveryCode() {
		return deliveryCode;
	}
	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}
	public BigDecimal getDiscountOrder() {
		return discountOrder;
	}
	public void setDiscountOrder(BigDecimal discountOrder) {
		this.discountOrder = discountOrder;
	}
	public BigDecimal getTotalPay() {
		return totalPay;
	}
	public void setTotalPay(BigDecimal totalPay) {
		this.totalPay = totalPay;
	}
	public List<RptPTHCKHProductVO> getListRptPTHCKHProductVO() {
		return listRptPTHCKHProductVO;
	}
	public void setListRptPTHCKHProductVO(
			List<RptPTHCKHProductVO> listRptPTHCKHProductVO) {
		this.listRptPTHCKHProductVO = listRptPTHCKHProductVO;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptExSaleOrder2VO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
		
	private String totalSaleQuantity;
	
	private String totalPromoQuantity;
	
	private BigDecimal totalSaleAmount;
	
	private BigDecimal totalPromoAmount;
	
	/** Danh sach don hang gop */
	private String lstOrderNumber;
	
	/** Thong tin khac cua don hang */
	private String otherInformation;
	
	/** Gop theo nhan vien giao hang */
	private Long deliveryStaffId;
	
	private String deliveryStaffInfo;
	
	/**
	 * @author sangtn
	 * @since 23/05/2014
	 * @description Gop theo nhan vien ban hang 
	 * @note Bổ sung để làm báo cáo 7.1.7 Phiếu xuất hàng theo NVBH
	 * */
	private Long saleStaffId;
	
	private String saleStaffInfo;
	
	//tong tien phai tra
	private BigDecimal total;
		
	//chiet khau hoa don
	private BigDecimal billDiscount = new BigDecimal(0);
	
	//chiet khau tien
	private BigDecimal moneyDiscount = new BigDecimal(0);
	
	//dieu chinh
	private BigDecimal modifiedValue = new BigDecimal(0);
	
	//gia tri hang khuyen mai
	private BigDecimal promotionValue = new BigDecimal(0);
	
	//tien chua chiet khau
	private BigDecimal amount;
	
	private BigDecimal totalWeight = new BigDecimal(0);
	private String totalString;	
	
	
	/** Danh sach san pham ban */
	private List<RptExSaleOrder1VO> lstSaleOrder = new ArrayList<RptExSaleOrder1VO>();
	
	/** Danh sach san pham khuyen mai */
	private List<RptExSaleOrder1VO> lstSaleOrderPromotion = new ArrayList<RptExSaleOrder1VO>();

	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
		}
	}
	
	public String getDeliveryStaffInfo() {
		return deliveryStaffInfo;
	}

	public void setDeliveryStaffInfo(String deliveryStaffInfo) {
		this.deliveryStaffInfo = deliveryStaffInfo;
	}

	public Long getSaleStaffId() {
		return saleStaffId;
	}

	public void setSaleStaffId(Long saleStaffId) {
		this.saleStaffId = saleStaffId;
	}

	public String getSaleStaffInfo() {
		return saleStaffInfo;
	}

	public void setSaleStaffInfo(String saleStaffInfo) {
		this.saleStaffInfo = saleStaffInfo;
	}

	public String getLstOrderNumber() {
		return lstOrderNumber;
	}

	public void setLstOrderNumber(String lstOrderNumber) {
		this.lstOrderNumber = lstOrderNumber;
	}
	
	public String getTotalSaleQuantity() {
		return totalSaleQuantity;
	}

	public void setTotalSaleQuantity(String totalSaleQuantity) {
		this.totalSaleQuantity = totalSaleQuantity;
	}

	public String getTotalPromoQuantity() {
		return totalPromoQuantity;
	}

	public void setTotalPromoQuantity(String totalPromoQuantity) {
		this.totalPromoQuantity = totalPromoQuantity;
	}

	public Long getDeliveryStaffId() {
		return deliveryStaffId;
	}

	public void setDeliveryStaffId(Long deliveryStaffId) {
		this.deliveryStaffId = deliveryStaffId;
	}

	public BigDecimal getTotalSaleAmount() {
		return totalSaleAmount;
	}

	public void setTotalSaleAmount(BigDecimal totalSaleAmount) {
		this.totalSaleAmount = totalSaleAmount;
	}

	public BigDecimal getTotalPromoAmount() {
		return totalPromoAmount;
	}

	public void setTotalPromoAmount(BigDecimal totalPromoAmount) {
		this.totalPromoAmount = totalPromoAmount;
	}

	public List<RptExSaleOrder1VO> getLstSaleOrder() {
		return lstSaleOrder;
	}

	public void setLstSaleOrder(List<RptExSaleOrder1VO> lstSaleOrder) {
		this.lstSaleOrder = lstSaleOrder;
	}

	public List<RptExSaleOrder1VO> getLstSaleOrderPromotion() {
		return lstSaleOrderPromotion;
	}

	public void setLstSaleOrderPromotion(
			List<RptExSaleOrder1VO> lstSaleOrderPromotion) {
		this.lstSaleOrderPromotion = lstSaleOrderPromotion;
	}

	public String getOtherInformation() {
		return otherInformation;
	}

	public void setOtherInformation(String otherInformation) {
		this.otherInformation = otherInformation;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getBillDiscount() {
		return billDiscount;
	}

	public void setBillDiscount(BigDecimal billDiscount) {
		this.billDiscount = billDiscount;
	}

	public BigDecimal getMoneyDiscount() {
		return moneyDiscount;
	}

	public void setMoneyDiscount(BigDecimal moneyDiscount) {
		this.moneyDiscount = moneyDiscount;
	}

	public BigDecimal getModifiedValue() {
		return modifiedValue;
	}

	public void setModifiedValue(BigDecimal modifiedValue) {
		this.modifiedValue = modifiedValue;
	}

	public BigDecimal getPromotionValue() {
		return promotionValue;
	}

	public void setPromotionValue(BigDecimal promotionValue) {
		this.promotionValue = promotionValue;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getTotalString() {
		return totalString;
	}

	public void setTotalString(String totalString) {
		this.totalString = totalString;
	}

	public BigDecimal getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}	
	
}
package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

import ths.dms.core.entities.Shop;

public class RptPoCfrmFromDvkhVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<RptPoCfrmFromDvkh1VO> lstRptPoCfrmFromDvkh1VO = new ArrayList<RptPoCfrmFromDvkh1VO>();

	private Shop shop;

	private Integer totalQty = 0;

	public ArrayList<RptPoCfrmFromDvkh1VO> getLstRptPoCfrmFromDvkh1VO() {
		return lstRptPoCfrmFromDvkh1VO;
	}

	public void setLstRptPoCfrmFromDvkh1VO(
			ArrayList<RptPoCfrmFromDvkh1VO> lstRptPoCfrmFromDvkh1VO) {
		this.lstRptPoCfrmFromDvkh1VO = lstRptPoCfrmFromDvkh1VO;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Integer getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(Integer totalQty) {
		this.totalQty = totalQty;
	}
}

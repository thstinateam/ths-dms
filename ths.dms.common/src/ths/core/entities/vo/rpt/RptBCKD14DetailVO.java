package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

public class RptBCKD14DetailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long index;
	private String maMien;
	private String maVung; //
	private String tbhv;
	private String maNPP; //	
	private String tenNPP; //
	private BigDecimal diemLeT12; //
	private Date rptMonth; //thang bao cao
	private Float mucTieuBqNhomSKUs; //
    private BigDecimal nhomSKUs; //
    private Float bqNhomSKUs; //
    private Float diemPA; //
    
    public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
    
    
    
	public Long getIndex() {
		return index;
	}



	public void setIndex(Long index) {
		this.index = index;
	}



	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public BigDecimal getDiemLeT12() {
		return diemLeT12;
	}
	public void setDiemLeT12(BigDecimal diemLeT12) {
		this.diemLeT12 = diemLeT12;
	}
	public Date getRptMonth() {
		return rptMonth;
	}
	public void setRptMonth(Date rptMonth) {
		this.rptMonth = rptMonth;
	}
	public Float getMucTieuBqNhomSKUs() {
		return mucTieuBqNhomSKUs;
	}
	public void setMucTieuBqNhomSKUs(BigDecimal mucTieuBqNhomSKUs) {
		if (bqNhomSKUs != null) {
			this.mucTieuBqNhomSKUs = mucTieuBqNhomSKUs.floatValue();
		}
	}
	public BigDecimal getNhomSKUs() {
		return nhomSKUs;
	}
	public void setNhomSKUs(BigDecimal nhomSKUs) {
		this.nhomSKUs = nhomSKUs;
	}
	public Float getBqNhomSKUs() {
		return bqNhomSKUs;
	}
	public void setBqNhomSKUs(BigDecimal bqNhomSKUs) {
		if (bqNhomSKUs != null) {
			this.bqNhomSKUs = bqNhomSKUs.floatValue();
		}
	}
	public Float getDiemPA() {
		return diemPA;
	}
	public void setDiemPA(BigDecimal diemPA) {
		if (diemPA != null) {
			this.diemPA = diemPA.floatValue();
		}
	}
	public String getTbhv() {
		return tbhv;
	}
	public void setTbhv(String tbhv) {
		this.tbhv = tbhv;
	}	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ths.dms.core.common.utils.GroupUtility;

public class RptPromotionDetailDataVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String promotionCode;
	private String promotionName;
	private Date orderDate;
	private Long orderId;
	private Long productId;
	private String productCode;
	private String productName;
	private Integer isFreeItem;
	private String customerCode;
	private String channelName;
	private String promotionType;
	private String staffCode;
	private String staffName;
	private Long productQuantity;
	private BigDecimal productAmount;

	// So luong ban
	private Long saleQuantity;
	// So luong khuyen mai
	private Long freeQuantity;
	// So tien khuyen mai
	private BigDecimal freeAmount;

	// San pham ban
	private String saleProduct;
	// San pham khuyen mai
	private String freeProduct;

	public RptPromotionDetailDataVO() {
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getIsFreeItem() {
		return isFreeItem;
	}

	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public void setProductQuantity(Long productQuantity) {
		this.productQuantity = productQuantity;
	}

	public void setProductAmount(BigDecimal productAmount) {
		this.productAmount = productAmount;
	}

	public Long getSaleQuantity() {
		if (this.isFreeItem == 0) {
			return this.productQuantity;
		} else {
			return null;
		}
	}

	public Long getFreeQuantity() {
		if (this.isFreeItem == 1) {
			return this.productQuantity;
		} else {
			return null;
		}
	}

	public BigDecimal getFreeAmount() {
		if (this.isFreeItem == 1 && null == this.productCode) {
			return this.productAmount;
		} else {
			return null;
		}
	}

	public String getSaleProduct() {
		if (this.isFreeItem == 0) {
			return this.productCode;
		} else {
			return null;
		}
	}

	public String getFreeProduct() {
		if (this.isFreeItem == 1) {
			return this.productCode;
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public static List<RptPromotionDetailLv01VO> groupByPromotion(
			List<RptPromotionDetailDataVO> listData) throws Exception {
		List<RptPromotionDetailLv01VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, "promotionCode");

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptPromotionDetailLv01VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptPromotionDetailLv01VO vo = new RptPromotionDetailLv01VO();

					List<RptPromotionDetailDataVO> listObject = (List<RptPromotionDetailDataVO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setPromotionCode((String) mapEntry.getKey());
						vo.setPromotionName(listObject.get(0)
								.getPromotionName());
						vo.setListData(listObject);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}
}

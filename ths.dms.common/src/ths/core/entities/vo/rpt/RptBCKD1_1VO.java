package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCKD1_1VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String maMien;
    private String maVung;
    private String maNPP;
    private String maSP;
    private Integer soDiemLe;
    private BigDecimal doanhSo;
    private Integer isTong;
	
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	
	public void setMaSP(String maSP) {
		this.maSP = maSP;
	}
	public Integer getSoDiemLe() {
		return soDiemLe;
	}
	public void setSoDiemLe(BigDecimal soDiemLe) {
		this.soDiemLe = Integer.parseInt(soDiemLe.toString());
	}
	public BigDecimal getDoanhSo() {
		return doanhSo;
	}
	public void setDoanhSo(BigDecimal doanhSo) {
		this.doanhSo = doanhSo;
	}
	public Integer getIsTong() {
		return isTong;
	}
	public void setIsTong(BigDecimal isTong) {
		this.isTong = Integer.parseInt(isTong.toString());
	}

	public String getMaMien() {
		return maMien;
	}

	public String getMaVung() {
		return maVung;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public String getMaSP() {
		return maSP;
	}
	
	
}

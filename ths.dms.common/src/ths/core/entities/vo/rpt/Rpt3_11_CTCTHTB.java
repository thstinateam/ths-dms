package ths.core.entities.vo.rpt;

import java.math.BigDecimal;
import java.util.List;


public class Rpt3_11_CTCTHTB implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 710560671268651768L;
	private String programCode; //ma CTTB
	private String programName; //ten CTTB
	private String fromDate;	//tu ngay
	private String toDate;		//den ngay
	private BigDecimal sumQuantity;	//tong so luong
	private BigDecimal sumAmount;	//tong thanh tien
	
	private List<Rpt3_11_CTCTHTB_Detail> listDetail;

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public BigDecimal getSumQuantity() {
		return sumQuantity;
	}

	public void setSumQuantity(BigDecimal sumQuantity) {
		this.sumQuantity = sumQuantity;
	}

	public BigDecimal getSumAmount() {
		return sumAmount;
	}

	public void setSumAmount(BigDecimal sumAmount) {
		this.sumAmount = sumAmount;
	}

	public List<Rpt3_11_CTCTHTB_Detail> getListDetail() {
		return listDetail;
	}

	public void setListDetail(List<Rpt3_11_CTCTHTB_Detail> listDetail) {
		this.listDetail = listDetail;
	}
	
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

import ths.dms.core.entities.Shop;

public class RptOrderDetailProductVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<RptOrderDetailProduct1VO> lstRptOrderDetailProduct1VO = new ArrayList<RptOrderDetailProduct1VO>();
	private Shop shop;

	public ArrayList<RptOrderDetailProduct1VO> getLstRptOrderDetailProduct1VO() {
		return lstRptOrderDetailProduct1VO;
	}

	public void setLstRptOrderDetailProduct1VO(
			ArrayList<RptOrderDetailProduct1VO> lstRptOrderDetailProduct1VO) {
		this.lstRptOrderDetailProduct1VO = lstRptOrderDetailProduct1VO;
	}

	/**
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * @param shop
	 *            the shop to set
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

}

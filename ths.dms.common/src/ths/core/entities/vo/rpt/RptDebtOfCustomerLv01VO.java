package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptDebtOfCustomerLv01VO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String customerCode;
	private String customerName;
	private List<RptDebtOfCustomerDataVO> listData;

	public RptDebtOfCustomerLv01VO() {
		this.listData = new ArrayList<RptDebtOfCustomerDataVO>();
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public List<RptDebtOfCustomerDataVO> getListData() {
		return listData;
	}

	public void setListData(List<RptDebtOfCustomerDataVO> listData) {
		this.listData = listData;
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RptVT5 implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String maMien;     //1 mien
	private String maVung;     //2 vung
	private String maTBHV;     
	private String maNPP;     
	private String tenNPP;     
	private String maNVGS;     
	private String maNVBH;     
	private String tenNVBH;     
	private List<DynamicVO> lstValue = new ArrayList<DynamicVO>();
	
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getMaMien() {
		return maMien;
	}

	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}

	public String getMaVung() {
		return maVung;
	}

	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}

	public String getMaTBHV() {
		return maTBHV;
	}

	public void setMaTBHV(String maTBHV) {
		this.maTBHV = maTBHV;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getTenNPP() {
		return tenNPP;
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}

	public String getMaNVGS() {
		return maNVGS;
	}

	public void setMaNVGS(String maNVGS) {
		this.maNVGS = maNVGS;
	}

	public String getMaNVBH() {
		return maNVBH;
	}

	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}

	public String getTenNVBH() {
		return tenNVBH;
	}

	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}

	public List<DynamicVO> getLstValue() {
		return lstValue;
	}

	public void setLstValue(List<DynamicVO> lstValue) {
		this.lstValue = lstValue;
	}
	
}

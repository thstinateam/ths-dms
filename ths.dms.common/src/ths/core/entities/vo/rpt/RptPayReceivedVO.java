package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.Shop;

public class RptPayReceivedVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Shop shop;
	private List<RptPayReceived1VO> lstRptPayReceived1VO;

	/**
	 * @return the shop
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * @param shop
	 *            the shop to set
	 */
	public void setShop(Shop shop) {
		this.shop = shop;
	}

	/**
	 * @return the lstRptPayReceived1VO
	 */
	public List<RptPayReceived1VO> getLstRptPayReceived1VO() {
		return lstRptPayReceived1VO;
	}

	/**
	 * @param lstRptPayReceived1VO
	 *            the lstRptPayReceived1VO to set
	 */
	public void setLstRptPayReceived1VO(
			List<RptPayReceived1VO> lstRptPayReceived1VO) {
		this.lstRptPayReceived1VO = lstRptPayReceived1VO;
	}

}

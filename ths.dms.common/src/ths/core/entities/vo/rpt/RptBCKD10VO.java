package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCKD10VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer percentAmount; //phan tram thuc hien doanh so
	private String parentSuperShopCode; // ma mien
	private String superShopCode; //ma vung
	private String shopCode; //ma nha phan phoi
	private String shopName; //ma gsnpp
	private BigDecimal amount; //doanh so thuc hien
	private BigDecimal amountPlan; //doanh so ke hoach
    private Integer numCustomer; //so khach hang thuc hien F4
    private Integer numPlanCustomer; //MTF4
    private Integer percentCustomer; //phan tram luy ke phan phoi
    private Integer xephangDS; //xep hang doanh so
    private Integer xephangPP; //xep hang phan phoi
    
    public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
	public Integer getNumCustomer() {
		return numCustomer;
	}
	public void setNumCustomer(BigDecimal numCustomer) {
		this.numCustomer = numCustomer.intValue();
	}
	public BigDecimal getAmountPlan() {
		return amountPlan;
	}
	public void setAmountPlan(BigDecimal amountPlan) {
		this.amountPlan = amountPlan;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getSuperShopCode() {
		return superShopCode;
	}
	public void setSuperShopCode(String superShopCode) {
		this.superShopCode = superShopCode;
	}
	public String getParentSuperShopCode() {
		return parentSuperShopCode;
	}
	public void setParentSuperShopCode(String parentSuperShopCode) {
		this.parentSuperShopCode = parentSuperShopCode;
	}
	public Integer getPercentAmount() {
		return percentAmount;
	}
	public void setPercentAmount(BigDecimal percentAmount) {
		if (percentAmount != null) {
			this.percentAmount = percentAmount.intValue();
		}
	}
	public Integer getNumPlanCustomer() {
		return numPlanCustomer;
	}
	public void setNumPlanCustomer(BigDecimal numPlanCustomer) {
		if (numPlanCustomer != null) {
			this.numPlanCustomer = numPlanCustomer.intValue();
		}
	}
	public Integer getPercentCustomer() {
		return percentCustomer;
	}
	public void setPercentCustomer(BigDecimal percentCustomer) {
		if (percentCustomer != null) {
			this.percentCustomer = percentCustomer.intValue();
		}
	}
	public Integer getXephangDS() {
		return xephangDS;
	}
	public void setXephangDS(BigDecimal xephangDS) {
		if (xephangDS != null) {
			this.xephangDS = xephangDS.intValue();
		}
	}
	public Integer getXephangPP() {
		return xephangPP;
	}
	public void setXephangPP(BigDecimal xephangPP) {
		if (xephangPP != null) {
			this.xephangPP = xephangPP.intValue();
		}
	}
	
}

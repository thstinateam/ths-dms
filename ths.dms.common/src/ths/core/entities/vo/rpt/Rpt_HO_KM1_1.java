package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

public class Rpt_HO_KM1_1 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String maMien;
	private String maVung;
	private String maNPP;
	private String orderNumber;//so don hang 
	private String promotionCode;//maCTKM 
	private String promotionName;//tenCTKM
	private String promotionContent;//noi dung CTKM
	private Date orderDate;//ngay
	private String saleStaffCode;//Ma NVBH
	private String saleStaffName;//Ten NVBH
	private String customerCode;//Ma KH
	private String customerName;//Ten KH
	private String customerAddress;//Dia chi KH
	private String productCode;//Ma SP
	private String productName;//Ten Sp
	private String unitMin;//Don vi tinh
	private BigDecimal quantitySale;//So luong
	private BigDecimal quantityPromotion;//So luong
	private BigDecimal amount;//Gia tri
	private BigDecimal tileChietkhau;
	private BigDecimal sotienChieuKhau;
	private BigDecimal price;
	private String loaiKM;
	
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}

	public String getPromotionContent() {
		return promotionContent;
	}

	public void setPromotionContent(String promotionContent) {
		this.promotionContent = promotionContent;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getSaleStaffCode() {
		return saleStaffCode;
	}

	public void setSaleStaffCode(String saleStaffCode) {
		this.saleStaffCode = saleStaffCode;
	}

	public String getSaleStaffName() {
		return saleStaffName;
	}

	public void setSaleStaffName(String saleStaffName) {
		this.saleStaffName = saleStaffName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUnitMin() {
		return unitMin;
	}

	public void setUnitMin(String unitMin) {
		this.unitMin = unitMin;
	}

	public BigDecimal getQuantitySale() {
		return quantitySale;
	}

	public void setQuantitySale(BigDecimal quantitySale) {
		this.quantitySale = quantitySale;
	}

	public BigDecimal getQuantityPromotion() {
		return quantityPromotion;
	}

	public void setQuantityPromotion(BigDecimal quantityPromotion) {
		this.quantityPromotion = quantityPromotion;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getMaMien() {
		return maMien;
	}

	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}

	public String getMaVung() {
		return maVung;
	}

	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getLoaiKM() {
		return loaiKM;
	}

	public void setLoaiKM(String loaiKM) {
		this.loaiKM = loaiKM;
	}

	public BigDecimal getTileChietkhau() {
		return tileChietkhau;
	}

	public void setTileChietkhau(BigDecimal tileChietkhau) {
		this.tileChietkhau = tileChietkhau;
	}

	public BigDecimal getSotienChieuKhau() {
		return sotienChieuKhau;
	}

	public void setSotienChieuKhau(BigDecimal sotienChieuKhau) {
		this.sotienChieuKhau = sotienChieuKhau;
	}	
}

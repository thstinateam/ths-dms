package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptBCKD19DataVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String maVung;
	private String maTBHV;
	private String tenTBHV;
	private String maNVGS;
	private String tenNVGS;
	private Integer soWWKH;
	private Integer soWWTH;
	private Float diemPA;
	private Integer nho3diem;
	private Date rptDate;
	private String strDate;
	private Float diem;
	private Boolean isInvalidate;
	private Integer who; // 0: NVGS , 1:TBHV
	private String strDiem;

	public String getMaVung() {
		return maVung;
	}

	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}

	public String getMaTBHV() {
		return maTBHV;
	}

	public void setMaTBHV(String maTBHV) {
		this.maTBHV = maTBHV;
	}

	public String getTenTBHV() {
		return tenTBHV;
	}

	public void setTenTBHV(String tenTBHV) {
		this.tenTBHV = tenTBHV;
	}

	public String getMaNVGS() {
		return maNVGS;
	}

	public void setMaNVGS(String maNVGS) {
		this.maNVGS = maNVGS;
	}

	public String getTenNVGS() {
		return tenNVGS;
	}

	public void setTenNVGS(String tenNVGS) {
		this.tenNVGS = tenNVGS;
	}

	public Integer getSoWWKH() {
		return soWWKH;
	}

	public void setSoWWKH(BigDecimal soWWKH) {
		if (null != soWWKH) {
			this.soWWKH = soWWKH.intValue();
		}
	}

	public Integer getSoWWTH() {
		return soWWTH;
	}

	public void setSoWWTH(BigDecimal soWWTH) {
		if (null != soWWTH) {
			this.soWWTH = soWWTH.intValue();
		}
	}

	public Float getDiemPA() {
		return diemPA;
	}

	public void setDiemPA(BigDecimal diemPA) {
		if (null != diemPA) {
			this.diemPA = diemPA.floatValue();
		}
	}

	public Integer getNho3diem() {
		return nho3diem;
	}

	public void setNho3diem(BigDecimal nho3diem) {
		if (null != nho3diem) {
			this.nho3diem = nho3diem.intValue();
		}
	}

	public Date getRptDate() {
		return rptDate;
	}

	public void setRptDate(Date rptDate) {
		this.rptDate = rptDate;
	}

	public Float getDiem() {
		return diem;
	}

	public void setDiem(BigDecimal diem) {
		if (null != diem) {
			this.diem = diem.floatValue();
		}
	}

	public Boolean getIsInvalidate() {
		return isInvalidate;
	}

	public void setIsInvalidate(Boolean isInvalidate) {
		this.isInvalidate = isInvalidate;
	}

	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}

	public String getStrDate() {
		return strDate;
	}

	public void setWho(Integer who) {
		this.who = who;
	}

	public Integer getWho() {
		if (this.tenNVGS ==null) return 1;
		else return 0;
	}

	public void setStrDiem(String strDiem) {
		this.strDiem = strDiem;
	}

	public String getStrDiem() {
		if (this.diem ==null) return null;
		String temp = this.diem.toString();
		if (!temp.equals("")){
			Integer a = Integer.parseInt(temp.substring(temp.indexOf('.')+1, temp.length()));
			if (a==0){
				return temp.substring(0,temp.indexOf('.'));
			}else{
				return temp;
			}
		}
		return null;
	}
}

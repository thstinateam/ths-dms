package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptExSaleOrderVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String productCode;
	
	private String productName;
	
	private Integer checkLot;
	
	private String lot;
	
	private BigDecimal price;
	
	private Integer uom1;
	
	private Integer uom2;
	
	private Integer saleQuantity;
	
	private Integer promoQuantity;
	
	private Integer convfact;
	
	private BigDecimal saleAmount = new BigDecimal(0);
	
	private BigDecimal promoAmount = new BigDecimal(0);
	
	private BigDecimal totalWeight = new BigDecimal(0);
	
	private String deliveryStaffInfo;
	
	private Long deliveryStaffId;
	
	/**
	 * @author sangtn
	 * @since 23/05/2014
	 * @description Gop theo nhan vien ban hang 
	 * @note Bổ sung để làm báo cáo 7.1.7 Phiếu xuất hàng theo NVBH
	 * */
	private String saleStaffInfo;	
	private Long saleStaffId;
	
	private BigDecimal discountAmount;
	
	private String orderNumber;
	
	private String promotionInfor;
	
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
		}
	}
	
	
	public Integer getCheckLot() {
		return checkLot;
	}

	public void setCheckLot(Integer checkLot) {
		this.checkLot = checkLot;
	}

	public String getPromotionInfor() {
		return promotionInfor;
	}

	public void setPromotionInfor(String promotionInfor) {
		this.promotionInfor = promotionInfor;
	}

	public Integer getUom1() {
		return uom1;
	}

	public void setUom1(Integer uom1) {
		this.uom1 = uom1;
	}

	public Integer getUom2() {
		return uom2;
	}

	public void setUom2(Integer uom2) {
		this.uom2 = uom2;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getSaleQuantity() {
		return saleQuantity;
	}

	public void setSaleQuantity(Integer saleQuantity) {
		this.saleQuantity = saleQuantity;
	}

	public Integer getPromoQuantity() {
		return promoQuantity;
	}

	public void setPromoQuantity(Integer promoQuantity) {
		this.promoQuantity = promoQuantity;
	}

	public BigDecimal getSaleAmount() {
		return saleAmount;
	}

	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}

	public BigDecimal getPromoAmount() {
		return promoAmount;
	}

	public void setPromoAmount(BigDecimal promoAmount) {
		this.promoAmount = promoAmount;
	}

	public String getDeliveryStaffInfo() {
		return deliveryStaffInfo;
	}

	public void setDeliveryStaffInfo(String deliveryStaffInfo) {
		this.deliveryStaffInfo = deliveryStaffInfo;
	}

	public String getSaleStaffInfo() {
		return saleStaffInfo;
	}


	public void setSaleStaffInfo(String saleStaffInfo) {
		this.saleStaffInfo = saleStaffInfo;
	}


	public Long getSaleStaffId() {
		return saleStaffId;
	}


	public void setSaleStaffId(Long saleStaffId) {
		this.saleStaffId = saleStaffId;
	}


	public Integer getThungSale() {
		if(saleQuantity == null || convfact == null || convfact == 0) {
			return 0;
		}
		return saleQuantity / convfact;
	}
	
	public Integer getLeSale() {
		if(saleQuantity == null || convfact == null || convfact == 0) {
			return 0;
		}
		return saleQuantity % convfact;
	}
	
	public Integer getThungPromo() {
		if(promoQuantity == null || convfact == null || convfact == 0) {
			return 0;
		}
		return promoQuantity / convfact;
	}
	
	public Integer getLePromo() {
		if(promoQuantity == null || convfact == null || convfact == 0) {
			return 0;
		}
		return promoQuantity % convfact;
	}
	
	public String getThungLePromo() {
		if(promoQuantity == null || convfact == null) {
			return "";
		}
		Integer thung = getThungPromo();
		Integer le = getLePromo();
		return thung.toString() + "/" + le.toString();
	}
	
	public String getThungLeSale() {
		if(saleQuantity == null || convfact == null) {
			return "";
		}
		Integer thung = getThungSale();
		Integer le = getLeSale();
		return thung.toString() + "/" + le.toString();
	}

	public Long getDeliveryStaffId() {
		return deliveryStaffId;
	}

	public void setDeliveryStaffId(Long deliveryStaffId) {
		this.deliveryStaffId = deliveryStaffId;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}


	public BigDecimal getTotalWeight() {
		return totalWeight;
	}


	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}
	
	
}
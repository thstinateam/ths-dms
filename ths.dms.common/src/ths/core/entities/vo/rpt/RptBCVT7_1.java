package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCVT7_1 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String maMien; //mien
	private String maVung; //vung
	private String maNPP; //npp
	private String tenNPP; 
	private String tenTBHV; 
	private String maGSNPP; 
	private String tenGSNPP; 
	private String ngay; 
	private String maKH; 
	private String tenKH; 
	private String diaChi; 
	private String thoiGianBatDau; 
	private String thoiGianKetThuc; 
	
	
	public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}


	public String getMaMien() {
		return maMien;
	}


	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}


	public String getMaVung() {
		return maVung;
	}


	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}


	public String getMaNPP() {
		return maNPP;
	}


	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}


	public String getTenNPP() {
		return tenNPP;
	}


	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}


	public String getTenTBHV() {
		return tenTBHV;
	}


	public void setTenTBHV(String tenTBHV) {
		this.tenTBHV = tenTBHV;
	}


	public String getMaGSNPP() {
		return maGSNPP;
	}


	public void setMaGSNPP(String maGSNPP) {
		this.maGSNPP = maGSNPP;
	}


	public String getTenGSNPP() {
		return tenGSNPP;
	}


	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}


	public String getNgay() {
		return ngay;
	}


	public void setNgay(String ngay) {
		this.ngay = ngay;
	}


	public String getMaKH() {
		return maKH;
	}


	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}


	public String getTenKH() {
		return tenKH;
	}


	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}


	public String getDiaChi() {
		return diaChi;
	}


	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}


	public String getThoiGianBatDau() {
		return thoiGianBatDau;
	}


	public void setThoiGianBatDau(String thoiGianBatDau) {
		this.thoiGianBatDau = thoiGianBatDau;
	}


	public String getThoiGianKetThuc() {
		return thoiGianKetThuc;
	}


	public void setThoiGianKetThuc(String thoiGianKetThuc) {
		this.thoiGianKetThuc = thoiGianKetThuc;
	}
	
}

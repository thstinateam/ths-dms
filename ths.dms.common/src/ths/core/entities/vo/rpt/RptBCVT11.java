package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCVT11 implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String mien;
	private String vung;
	private String npp;
	private String tenNPP;
	private String maNV;	
	private String tenNV;
	private String maKH;
	private String tenKH;
	private String diaChi;
	private String ngay;
	private String maClip;
	private String tenClip;
	private BigDecimal soLanMo;	
	
	
	public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}


	public String getMien() {
		return mien;
	}


	public void setMien(String mien) {
		this.mien = mien;
	}


	public String getVung() {
		return vung;
	}


	public void setVung(String vung) {
		this.vung = vung;
	}


	public String getNpp() {
		return npp;
	}


	public void setNpp(String npp) {
		this.npp = npp;
	}


	public String getTenNPP() {
		return tenNPP;
	}


	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}


	public String getMaNV() {
		return maNV;
	}


	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}


	public String getTenNV() {
		return tenNV;
	}


	public void setTenNV(String tenNV) {
		this.tenNV = tenNV;
	}


	public String getMaKH() {
		return maKH;
	}


	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}


	public String getTenKH() {
		return tenKH;
	}


	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}


	public String getDiaChi() {
		return diaChi;
	}


	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}


	public String getNgay() {
        return ngay;
    }


    public void setNgay(String ngay) {
        this.ngay = ngay;
    }


    public String getMaClip() {
		return maClip;
	}


	public void setMaClip(String maClip) {
		this.maClip = maClip;
	}


	public String getTenClip() {
		return tenClip;
	}


	public void setTenClip(String tenClip) {
		this.tenClip = tenClip;
	}


	public BigDecimal getSoLanMo() {
		return soLanMo;
	}


	public void setSoLanMo(BigDecimal soLanMo) {
		this.soLanMo = soLanMo;
	}	
}

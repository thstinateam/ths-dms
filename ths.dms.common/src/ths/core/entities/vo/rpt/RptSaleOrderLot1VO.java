package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;


public class RptSaleOrderLot1VO  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String productCode;
	
	List<RptSaleOrderLotVO> lstRptSaleOrderLotVO;

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public List<RptSaleOrderLotVO> getLstRptSaleOrderLotVO() {
		return lstRptSaleOrderLotVO;
	}

	public void setLstRptSaleOrderLotVO(List<RptSaleOrderLotVO> lstRptSaleOrderLotVO) {
		this.lstRptSaleOrderLotVO = lstRptSaleOrderLotVO;
	}
	
	
}
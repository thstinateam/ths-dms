package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

//doanh so phan phoi theo nhom hang
public class RptBCKD1_2_Vung implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String parentSuperShopCode;
	private String superShopCode;
	private BigDecimal sumAmountVung;//tong ds
	//private Float sumAmountVung;//tong ds
	private Integer sumCustomerVung;//tong khach hang
	private List<RptKDAllowSubCattVO> listItem;

	public String getParentSuperShopCode() {
		return parentSuperShopCode;
	}

	public void setParentSuperShopCode(String parentSuperShopCode) {
		this.parentSuperShopCode = parentSuperShopCode;
	}

	public List<RptKDAllowSubCattVO> getListItem() {
		return listItem;
	}

	public void setListItem(List<RptKDAllowSubCattVO> listItem) {
		this.listItem = listItem;
	}

	public String getSuperShopCode() {
		return superShopCode;
	}

	public void setSuperShopCode(String superShopCode) {
		this.superShopCode = superShopCode;
	}

	public BigDecimal getSumAmountVung() {
		return sumAmountVung;
	}

	public void setSumAmountVung(BigDecimal sumAmountVung) {
		this.sumAmountVung = sumAmountVung;
	}

	public Integer getSumCustomerVung() {
		return sumCustomerVung;
	}

	public void setSumCustomerVung(Integer sumCustomerVung) {
		this.sumCustomerVung = sumCustomerVung;
	}


}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

public class RptReturnSaleOrderParentVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String orderNumber;
	private List<RptReturnSaleOrderVO> listRptReturnVO;
	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}
	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	/**
	 * @return the listRptReturnVO
	 */
	public List<RptReturnSaleOrderVO> getListRptReturnVO() {
		return listRptReturnVO;
	}
	/**
	 * @param listRptReturnVO the listRptReturnVO to set
	 */
	public void setListRptReturnVO(List<RptReturnSaleOrderVO> listRptReturnVO) {
		this.listRptReturnVO = listRptReturnVO;
	}
	
}

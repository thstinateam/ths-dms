package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptRealTotalSalePlanParentVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String staffCode;
	private String staffName;
	private List<RptRealTotalSalePlanVO> listRealPlan = new ArrayList<RptRealTotalSalePlanVO>();
	/**
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}
	/**
	 * @param staffCode the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	/**
	 * @return the staffName
	 */
	public String getStaffName() {
		return staffName;
	}
	/**
	 * @param staffName the staffName to set
	 */
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	/**
	 * @return the listRealPlan
	 */
	public List<RptRealTotalSalePlanVO> getListRealPlan() {
		return listRealPlan;
	}
	/**
	 * @param listRealPlan the listRealPlan to set
	 */
	public void setListRealPlan(List<RptRealTotalSalePlanVO> listRealPlan) {
		this.listRealPlan = listRealPlan;
	}
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptInputProductVinamilkVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String saleOrderNumber;
	private String poAutoNumber;
	private Date poVnmDate;
	private Date poAutoDate;
	private String poCoNumber;
	private String invoiceNumber;
	private Date importDate;
	private Integer status;
	private Integer quantity;
	private Float priceValue;
	private String productCode;
	private String productName;
	private String uom1;
	private BigDecimal amount;
	/**
	 * @return the saleOrderNumber
	 */
	public String getSaleOrderNumber() {
		return saleOrderNumber;
	}
	/**
	 * @param saleOrderNumber the saleOrderNumber to set
	 */
	public void setSaleOrderNumber(String saleOrderNumber) {
		this.saleOrderNumber = saleOrderNumber;
	}
	/**
	 * @return the poAutoNumber
	 */
	public String getPoAutoNumber() {
		return poAutoNumber;
	}
	/**
	 * @param poAutoNumber the poAutoNumber to set
	 */
	public void setPoAutoNumber(String poAutoNumber) {
		this.poAutoNumber = poAutoNumber;
	}
	/**
	 * @return the poVnmDate
	 */
	public Date getPoVnmDate() {
		return poVnmDate;
	}
	/**
	 * @param poVnmDate the poVnmDate to set
	 */
	public void setPoVnmDate(Date poVnmDate) {
		this.poVnmDate = poVnmDate;
	}
	/**
	 * @return the poAutoDate
	 */
	public Date getPoAutoDate() {
		return poAutoDate;
	}
	/**
	 * @param poAutoDate the poAutoDate to set
	 */
	public void setPoAutoDate(Date poAutoDate) {
		this.poAutoDate = poAutoDate;
	}
	/**
	 * @return the poCoNumber
	 */
	public String getPoCoNumber() {
		return poCoNumber;
	}
	/**
	 * @param poCoNumber the poCoNumber to set
	 */
	public void setPoCoNumber(String poCoNumber) {
		this.poCoNumber = poCoNumber;
	}
	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	/**
	 * @param invoiceNumber the invoiceNumber to set
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	/**
	 * @return the importDate
	 */
	public Date getImportDate() {
		return importDate;
	}
	/**
	 * @param importDate the importDate to set
	 */
	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}
	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the priceValue
	 */
	public Float getPriceValue() {
		return priceValue;
	}
	/**
	 * @param priceValue the priceValue to set
	 */
	public void setPriceValue(Float priceValue) {
		this.priceValue = priceValue;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the uom1
	 */
	public String getUom1() {
		return uom1;
	}
	/**
	 * @param uom1 the uom1 to set
	 */
	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}
	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
}

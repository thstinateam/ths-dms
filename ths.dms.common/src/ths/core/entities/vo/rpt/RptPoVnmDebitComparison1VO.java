package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class RptPoVnmDebitComparison1VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String poVnmDate;// 1
	private BigDecimal total = new BigDecimal(0); // 6
	private ArrayList<RptPoVnmDebitComparison2VO> lstRptPoVnmDebitComparison2VO = new ArrayList<RptPoVnmDebitComparison2VO>();

	public String getPoVnmDate() {
		return poVnmDate;
	}

	public void setPoVnmDate(String poVnmDate) {
		this.poVnmDate = poVnmDate;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public ArrayList<RptPoVnmDebitComparison2VO> getLstRptPoVnmDebitComparison2VO() {
		return lstRptPoVnmDebitComparison2VO;
	}

	public void setLstRptPoVnmDebitComparison2VO(
			ArrayList<RptPoVnmDebitComparison2VO> lstRptPoVnmDebitComparison2VO) {
		this.lstRptPoVnmDebitComparison2VO = lstRptPoVnmDebitComparison2VO;
	}

}

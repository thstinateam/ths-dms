package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RptPoStatusTracking2VO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date poAutoDate;
	
	private List<RptPoStatusTracking1VO> lstRptPoStatusTracking1VO;

	public Date getPoAutoDate() {
		return poAutoDate;
	}

	public void setPoAutoDate(Date poAutoDate) {
		this.poAutoDate = poAutoDate;
	}

	public List<RptPoStatusTracking1VO> getLstRptPoStatusTracking1VO() {
		return lstRptPoStatusTracking1VO;
	}

	public void setLstRptPoStatusTracking1VO(
			List<RptPoStatusTracking1VO> lstRptPoStatusTracking1VO) {
		this.lstRptPoStatusTracking1VO = lstRptPoStatusTracking1VO;
	}
	
	
	
}

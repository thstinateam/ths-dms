package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

public class RptReturnSaleOrderByDeliveryLv01VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long deliveryId;
	private String deliveryCode;
	private String deliveryName;
	private List<RptReturnSaleOrderByDeliveryDataVO> listData;

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}

	public List<RptReturnSaleOrderByDeliveryDataVO> getListData() {
		return listData;
	}

	public void setListData(List<RptReturnSaleOrderByDeliveryDataVO> listData) {
		this.listData = listData;
	}
}
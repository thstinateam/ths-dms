package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptLimitDebitParentVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String shortCode;
	private String customerCode;
	private String customerName;
	private List<RptLimitDebitVO> listLimitDebit = new ArrayList<RptLimitDebitVO>();
	/**
	 * @return the customerCode
	 */
	public String getCustomerCode() {
		return customerCode;
	}
	/**
	 * @param customerCode the customerCode to set
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}
	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	/**
	 * @return the listLimitDebit
	 */
	public List<RptLimitDebitVO> getListLimitDebit() {
		return listLimitDebit;
	}
	/**
	 * @param listLimitDebit the listLimitDebit to set
	 */
	public void setListLimitDebit(List<RptLimitDebitVO> listLimitDebit) {
		this.listLimitDebit = listLimitDebit;
	}
	/**
	 * @return the shortCode
	 */
	public String getShortCode() {
		return shortCode;
	}
	/**
	 * @param shortCode the shortCode to set
	 */
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	
	
}

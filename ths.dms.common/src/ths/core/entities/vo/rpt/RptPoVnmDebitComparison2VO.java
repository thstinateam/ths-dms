package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptPoVnmDebitComparison2VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date poVnmDate;// 1
	private Integer type; // 2
	private Integer status;// 3
	private String poCoNumber; // 4
	private String invoiceNumber; // 5
	private BigDecimal total; // 6

	public Date getPoVnmDate() {
		return poVnmDate;
	}

	public void setPoVnmDate(Date poVnmDate) {
		this.poVnmDate = poVnmDate;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPoCoNumber() {
		return poCoNumber;
	}

	public void setPoCoNumber(String poCoNumber) {
		this.poCoNumber = poCoNumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
}

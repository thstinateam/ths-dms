package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptDMVSVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String maMien;
	private String maVung;
	private String maNPP;
	private String tenNPP;
	private String tenGSNPP;
	private String maNVBH;
	private String tenNVBH;
	private String ngay;
	private String tgBatDau;
	private String maKHS;
	private String tenKHS;
	private String diachiKHS;
	private String tgKetThuc;
	private String maKHC;
	private String tenKHC;
	private String diachiKHC;
	private Integer numS;
	private Integer numC;
	private Integer fromHour;
	private Integer toHour;
	private Integer fromMinute;
	private Integer toMinute;
	private Integer numNVDM;
	private Integer numNVVS;

	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 
	}
	public Integer getNumNVDM() {
		return numNVDM;
	}
	public void setNumNVDM(Integer numNVDM) {
		this.numNVDM = numNVDM;
	}
	public Integer getNumNVVS() {
		return numNVVS;
	}
	public void setNumNVVS(Integer numNVVS) {
		this.numNVVS = numNVVS;
	}
	public Integer getFromHour() {
		return fromHour;
	}

	public void setFromHour(Integer fromHour) {
		this.fromHour = fromHour;
	}

	public Integer getToHour() {
		return toHour;
	}

	public void setToHour(Integer toHour) {
		this.toHour = toHour;
	}

	public Integer getFromMinute() {
		return fromMinute;
	}

	public void setFromMinute(Integer fromMinute) {
		this.fromMinute = fromMinute;
	}

	public Integer getToMinute() {
		return toMinute;
	}

	public void setToMinute(Integer toMinute) {
		this.toMinute = toMinute;
	}
	
	public String getMaMien() {
		return maMien;
	}
	public void setMaMien(String maMien) {
		this.maMien = maMien;
	}
	public String getMaVung() {
		return maVung;
	}
	public void setMaVung(String maVung) {
		this.maVung = maVung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaNVBH() {
		return maNVBH;
	}
	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}
	public String getTenNVBH() {
		return tenNVBH;
	}
	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}
	public String getNgay() {
		return ngay;
	}
	public void setNgay(String ngay) {
		this.ngay = ngay;
	}
	public String getMaKHS() {
		return maKHS;
	}
	public String getTenGSNPP() {
		return tenGSNPP;
	}
	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}
	public void setMaKHS(String maKHS) {
		this.maKHS = maKHS;
	}
	public String getTenKHS() {
		return tenKHS;
	}
	public void setTenKHS(String tenKHS) {
		this.tenKHS = tenKHS;
	}
	public String getDiachiKHS() {
		return diachiKHS;
	}
	public void setDiachiKHS(String diachiKHS) {
		this.diachiKHS = diachiKHS;
	}
	public String getMaKHC() {
		return maKHC;
	}
	public void setMaKHC(String maKHC) {
		this.maKHC = maKHC;
	}
	public String getTenKHC() {
		return tenKHC;
	}
	public void setTenKHC(String tenKHC) {
		this.tenKHC = tenKHC;
	}
	public String getDiachiKHC() {
		return diachiKHC;
	}
	public void setDiachiKHC(String diachiKHC) {
		this.diachiKHC = diachiKHC;
	}
	public Integer getNumS() {
		return numS;
	}
	public void setNumS(BigDecimal numS) {
		this.numS = numS.intValue();
	}
	public Integer getNumC() {
		return numC;
	}
	public void setNumC(BigDecimal numC) {
		this.numC = numC.intValue();
	}
	public String getTgBatDau() {
		return tgBatDau;
	}
	public void setTgBatDau(String tgBatDau) {
		this.tgBatDau = tgBatDau;
	}
	public String getTgKetThuc() {
		return tgKetThuc;
	}
	public void setTgKetThuc(String tgKetThuc) {
		this.tgKetThuc = tgKetThuc;
	}
	
}

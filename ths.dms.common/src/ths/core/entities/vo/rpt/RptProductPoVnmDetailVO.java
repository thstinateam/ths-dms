package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;

import ths.dms.core.entities.Shop;

public class RptProductPoVnmDetailVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<RptProductPoVnmDetail1VO> lstRptProductPoVnmDetail2VO = new ArrayList<RptProductPoVnmDetail1VO>();
	private Shop shop;

	public ArrayList<RptProductPoVnmDetail1VO> getLstRptProductPoVnmDetail2VO() {
		return lstRptProductPoVnmDetail2VO;
	}

	public void setLstRptProductPoVnmDetail2VO(
			ArrayList<RptProductPoVnmDetail1VO> lstRptProductPoVnmDetail2VO) {
		this.lstRptProductPoVnmDetail2VO = lstRptProductPoVnmDetail2VO;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
}

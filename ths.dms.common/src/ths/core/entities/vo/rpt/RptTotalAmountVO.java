package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RptTotalAmountVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date orderDate;
	private String orderNumber;
	private BigDecimal total;
	private BigDecimal discount;
	private Integer isFreeItem;
	private Float priceValue;
	private Integer quantity;
	/**
	 * @return the priceValue
	 */
	public Float getPriceValue() {
		return priceValue;
	}
	/**
	 * @param priceValue the priceValue to set
	 */
	public void setPriceValue(Float priceValue) {
		this.priceValue = priceValue;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	private String staffCode;
	private String staffName;
	private BigDecimal totalPay;
	private BigDecimal remain;
	/**
	 * @return the orderDate
	 */
	public Date getOrderDate() {
		return orderDate;
	}
	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}
	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	/**
	 * @return the discount
	 */
	public BigDecimal getDiscount() {
		return discount;
	}
	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	/**
	 * @return the isFreeItem
	 */
	public Integer getIsFreeItem() {
		return isFreeItem;
	}
	/**
	 * @param isFreeItem the isFreeItem to set
	 */
	public void setIsFreeItem(Integer isFreeItem) {
		this.isFreeItem = isFreeItem;
	}
	/**
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}
	/**
	 * @param staffCode the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	/**
	 * @return the staffName
	 */
	public String getStaffName() {
		return staffName;
	}
	/**
	 * @param staffName the staffName to set
	 */
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	/**
	 * @return the totalPay
	 */
	public BigDecimal getTotalPay() {
		return totalPay;
	}
	/**
	 * @param totalPay the totalPay to set
	 */
	public void setTotalPay(BigDecimal totalPay) {
		this.totalPay = totalPay;
	}
	/**
	 * @return the remain
	 */
	public BigDecimal getRemain() {
		return remain;
	}
	/**
	 * @param remain the remain to set
	 */
	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RptBCKD16VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Date thangBC;
	private List<RptBCKD16DataVO> lstDetail;

	public Date getThangBC() {
		return thangBC;
	}

	public void setThangBC(Date thangBC) {
		this.thangBC = thangBC;
	}

	public List<RptBCKD16DataVO> getLstDetail() {
		return lstDetail;
	}

	public void setLstDetail(List<RptBCKD16DataVO> lstDetail) {
		this.lstDetail = lstDetail;
	}
}

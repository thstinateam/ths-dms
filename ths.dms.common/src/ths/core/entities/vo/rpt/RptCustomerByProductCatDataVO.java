package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ths.dms.core.common.utils.GroupUtility;

public class RptCustomerByProductCatDataVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long customerId;
	private String customerCode;
	private String customerName;
	private String productCode;
	private String productName;
	private String productCat;
	private String productCatDes;
	private Float price;
	private Long quantity;
	private BigDecimal amount;

	public RptCustomerByProductCatDataVO() {

	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCat() {
		return productCat;
	}

	public void setProductCat(String productCat) {
		this.productCat = productCat;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getProductCatDes() {
		return productCatDes;
	}

	public void setProductCatDes(String productCatDes) {
		this.productCatDes = productCatDes;
	}

	@SuppressWarnings("unchecked")
	public static List<RptCustomerByProductCatLv01VO> groupByCusAndCat(
			List<RptCustomerByProductCatDataVO> listData) throws Exception {
		List<RptCustomerByProductCatLv01VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, "customerCode");

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptCustomerByProductCatLv01VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptCustomerByProductCatLv01VO vo = new RptCustomerByProductCatLv01VO();

					List<RptCustomerByProductCatDataVO> listObject = (List<RptCustomerByProductCatDataVO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setCustomerCode(listObject.get(0).getCustomerCode());
						vo.setCustomerName(listObject.get(0).getCustomerName());

						List<RptCustomerByProductCatLv02VO> listLV02 = RptCustomerByProductCatDataVO
								.groupByCat(listObject);

						vo.setListData(listLV02);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}

	@SuppressWarnings("unchecked")
	public static List<RptCustomerByProductCatLv02VO> groupByCat(
			List<RptCustomerByProductCatDataVO> listData) throws Exception {
		List<RptCustomerByProductCatLv02VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, "productCat");

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptCustomerByProductCatLv02VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptCustomerByProductCatLv02VO vo = new RptCustomerByProductCatLv02VO();

					List<RptCustomerByProductCatDataVO> listObject = (List<RptCustomerByProductCatDataVO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setProductCat((String) mapEntry.getKey());
						// vo.setProductCat(listObject.get(0).getProductCat());
						vo.setProductCatDes(listObject.get(0)
								.getProductCatDes());
						vo.setListData(listObject);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptBCNVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1843811279986805448L;
	private String staffName;
	private String staffCode;
	private String staffType;
	private BigDecimal amountDay; 	//DS Ngay
	private BigDecimal amountMonth; //DS Luy Ke
	private BigDecimal planMonth; 	//KH Luy Ke
	private BigDecimal progress; 	//% Theo tien do
	private BigDecimal differenceProgress; 	//Chenh lech voi tien do
	private BigDecimal amountPlanMonth; 	//DS Ke hoach
	private BigDecimal amountSale; 			//Doanh so phai ban ngay sau
	private BigDecimal customerTarget; 		//KH Muc tieu
	private BigDecimal f4Day; 		//F4 trong ngay
	private BigDecimal f4Month; 	//F4 Luy ke
	private BigDecimal f4LastMonth; //F4 thang truoc
	private BigDecimal sumCustomer; //Tong so diem ban
	private BigDecimal notDistributed; //So diem chua phan phoi
	private BigDecimal inTarget; 		//Tablet trong tuyen
	private BigDecimal outTarget; 		//Tablet ngoai tuyen
	private BigDecimal payLastDay; 		//Tra ve hom truoc
	private BigDecimal handOrder; 		//Don tay
	private String confirm; 			//Doanh so
	private String distribution; 		//Phan phoi
	
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public String getStaffType() {
		return staffType;
	}
	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}
	public BigDecimal getAmountDay() {
		return amountDay;
	}
	public void setAmountDay(BigDecimal amountDay) {
		this.amountDay = amountDay;
	}
	public BigDecimal getAmountMonth() {
		return amountMonth;
	}
	public void setAmountMonth(BigDecimal amountMonth) {
		this.amountMonth = amountMonth;
	}
	public BigDecimal getPlanMonth() {
		return planMonth;
	}
	public void setPlanMonth(BigDecimal planMonth) {
		this.planMonth = planMonth;
	}
	public BigDecimal getProgress() {
		return progress;
	}
	public void setProgress(BigDecimal progress) {
		this.progress = progress;
	}
	public BigDecimal getDifferenceProgress() {
		return differenceProgress;
	}
	public void setDifferenceProgress(BigDecimal differenceProgress) {
		this.differenceProgress = differenceProgress;
	}
	public BigDecimal getAmountPlanMonth() {
		return amountPlanMonth;
	}
	public void setAmountPlanMonth(BigDecimal amountPlanMonth) {
		this.amountPlanMonth = amountPlanMonth;
	}
	public BigDecimal getAmountSale() {
		return amountSale;
	}
	public void setAmountSale(BigDecimal amountSale) {
		this.amountSale = amountSale;
	}
	public BigDecimal getCustomerTarget() {
		return customerTarget;
	}
	public void setCustomerTarget(BigDecimal customerTarget) {
		this.customerTarget = customerTarget;
	}
	public BigDecimal getF4Day() {
		return f4Day;
	}
	public void setF4Day(BigDecimal f4Day) {
		this.f4Day = f4Day;
	}
	public BigDecimal getF4Month() {
		return f4Month;
	}
	public void setF4Month(BigDecimal f4Month) {
		this.f4Month = f4Month;
	}
	public BigDecimal getF4LastMonth() {
		return f4LastMonth;
	}
	public void setF4LastMonth(BigDecimal f4LastMonth) {
		this.f4LastMonth = f4LastMonth;
	}
	public BigDecimal getSumCustomer() {
		return sumCustomer;
	}
	public void setSumCustomer(BigDecimal sumCustomer) {
		this.sumCustomer = sumCustomer;
	}
	public BigDecimal getNotDistributed() {
		return notDistributed;
	}
	public void setNotDistributed(BigDecimal notDistributed) {
		this.notDistributed = notDistributed;
	}
	public BigDecimal getInTarget() {
		return inTarget;
	}
	public void setInTarget(BigDecimal inTarget) {
		this.inTarget = inTarget;
	}
	public BigDecimal getOutTarget() {
		return outTarget;
	}
	public void setOutTarget(BigDecimal outTarget) {
		this.outTarget = outTarget;
	}
	public BigDecimal getPayLastDay() {
		return payLastDay;
	}
	public void setPayLastDay(BigDecimal payLastDay) {
		this.payLastDay = payLastDay;
	}

	public BigDecimal getHandOrder() {
		return handOrder;
	}
	public void setHandOrder(BigDecimal handOrder) {
		this.handOrder = handOrder;
	}
	public String getConfirm() {
		return confirm;
	}
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}
	public String getDistribution() {
		return distribution;
	}
	public void setDistribution(String distribution) {
		this.distribution = distribution;
	}
}

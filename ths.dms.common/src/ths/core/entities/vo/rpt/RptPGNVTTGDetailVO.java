package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptPGNVTTGDetailVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String productCode;
	
	private String productName;
	
	private Integer convfact;
	
	private Integer quantity;
	
	private BigDecimal price;
	
	private BigDecimal amount;
	
	private String programInfo;
	
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getThung() {
		return this.getQuantity() / this.getConvfact();
	}

	public Integer getLe() {
		return this.getQuantity() % this.getConvfact();
	}

	public String getProgramInfo() {
		return programInfo;
	}

	public void setProgramInfo(String programInfo) {
		this.programInfo = programInfo;
	}	
}
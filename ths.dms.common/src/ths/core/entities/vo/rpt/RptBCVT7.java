package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptBCVT7 implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mien; //mien
	private String vung; //vung
	private String shopcode; //npp
	private String shopNAME; //ten npp
	private String STAFFCODE; //nvgs
	private String NAME; //ten nvgs
	private BigDecimal sokhkh; //Tong so KH
	private BigDecimal sokhthucghe; //SOKHACHHANGTHUCGHE
	private BigDecimal thoigianlamviec; //KHACHHANGTRONGTUYENKOGHETHAM
	private String ngay;
	private String STARTTIME; //TONGTHOIGIANLAMVIEC   ----10
	private String ENDTIME; //THOIGIANBATDAU
	private BigDecimal solanghethamduoi5phut; // THOIGIANKETTHUC
	private BigDecimal solanghethamduoi10phut; //SOLANGHETHAMDUOI5PHUT
	private BigDecimal solanghethamduoi30phut; //SOLANGHETHAMDUOI30PHUT
	private BigDecimal solanghethamduoi60phut; //SOLANGHETHAMDUOI60PHUT
	private BigDecimal solanghethamhon60phut; //SOLANGHETHAMTREN60PHUT
	private BigDecimal THOIGIANTRUNGBINH ; //TBTHOIGIANGHETHAMKHACHHANG'
	
	public void safeSetNull() {
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}


	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public BigDecimal getSokhkh() {
		return sokhkh;
	}

	public void setSokhkh(BigDecimal sokhkh) {
		this.sokhkh = sokhkh;
	}

	public BigDecimal getSokhthucghe() {
		return sokhthucghe;
	}

	public void setSokhthucghe(BigDecimal sokhthucghe) {
		this.sokhthucghe = sokhthucghe;
	}

	public BigDecimal getThoigianlamviec() {
		return thoigianlamviec;
	}

	public void setThoigianlamviec(BigDecimal thoigianlamviec) {
		this.thoigianlamviec = thoigianlamviec;
	}

	

	public String getSTARTTIME() {
		return STARTTIME;
	}

	public void setSTARTTIME(String sTARTTIME) {
		STARTTIME = sTARTTIME;
	}

	public String getENDTIME() {
		return ENDTIME;
	}

	public void setENDTIME(String eNDTIME) {
		ENDTIME = eNDTIME;
	}

	public BigDecimal getSolanghethamduoi5phut() {
		return solanghethamduoi5phut;
	}

	public void setSolanghethamduoi5phut(BigDecimal solanghethamduoi5phut) {
		this.solanghethamduoi5phut = solanghethamduoi5phut;
	}

	public BigDecimal getSolanghethamduoi10phut() {
		return solanghethamduoi10phut;
	}

	public void setSolanghethamduoi10phut(BigDecimal solanghethamduoi10phut) {
		this.solanghethamduoi10phut = solanghethamduoi10phut;
	}

	public BigDecimal getSolanghethamduoi30phut() {
		return solanghethamduoi30phut;
	}

	public void setSolanghethamduoi30phut(BigDecimal solanghethamduoi30phut) {
		this.solanghethamduoi30phut = solanghethamduoi30phut;
	}

	public BigDecimal getSolanghethamduoi60phut() {
		return solanghethamduoi60phut;
	}

	public void setSolanghethamduoi60phut(BigDecimal solanghethamduoi60phut) {
		this.solanghethamduoi60phut = solanghethamduoi60phut;
	}

	public BigDecimal getSolanghethamhon60phut() {
		return solanghethamhon60phut;
	}

	public void setSolanghethamhon60phut(BigDecimal solanghethamhon60phut) {
		this.solanghethamhon60phut = solanghethamhon60phut;
	}

	public BigDecimal getTHOIGIANTRUNGBINH() {
		return THOIGIANTRUNGBINH;
	}

	public void setTHOIGIANTRUNGBINH(BigDecimal tHOIGIANTRUNGBINH) {
		THOIGIANTRUNGBINH = tHOIGIANTRUNGBINH;
	}

	public String getShopcode() {
		return shopcode;
	}

	public void setShopcode(String shopcode) {
		this.shopcode = shopcode;
	}

	public String getShopNAME() {
		return shopNAME;
	}

	public void setShopNAME(String shopNAME) {
		this.shopNAME = shopNAME;
	}

	public String getSTAFFCODE() {
		return STAFFCODE;
	}

	public void setSTAFFCODE(String sTAFFCODE) {
		STAFFCODE = sTAFFCODE;
	}

	public String getNgay() {
		return ngay;
	}

	public void setNgay(String ngay) {
		this.ngay = ngay;
	}
	
}

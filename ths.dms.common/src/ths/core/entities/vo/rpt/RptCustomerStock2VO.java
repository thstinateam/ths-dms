package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptCustomerStock2VO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String productInfoCode;
	private String productCode;
	private String productName;
	private BigDecimal price;
	private String uom1;
	private String uom2;
	private Integer convfact;
	private Integer quantity;
	private BigDecimal amount;

	public String getProductInfoCode() {
		return productInfoCode;
	}

	public void setProductInfoCode(String productInfoCode) {
		this.productInfoCode = productInfoCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getUom1() {
		return uom1;
	}

	public void setUom1(String uom1) {
		this.uom1 = uom1;
	}

	public String getUom2() {
		return uom2;
	}

	public void setUom2(String uom2) {
		this.uom2 = uom2;
	}

	public Integer getConvfact() {
		return convfact;
	}

	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}

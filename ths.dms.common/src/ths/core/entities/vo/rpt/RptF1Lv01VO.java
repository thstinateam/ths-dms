package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptF1Lv01VO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String productInfoName;
	private List<RptF1DataVO> listData;

	public RptF1Lv01VO() {
		this.listData = new ArrayList<RptF1DataVO>();
	}

	public String getProductInfoName() {
		return productInfoName;
	}

	public void setProductInfoName(String productInfoName) {
		this.productInfoName = productInfoName;
	}

	public List<RptF1DataVO> getListData() {
		return listData;
	}

	public void setListData(List<RptF1DataVO> listData) {
		this.listData = listData;
	}
}

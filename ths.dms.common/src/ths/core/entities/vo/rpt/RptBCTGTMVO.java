package ths.core.entities.vo.rpt;

import java.io.Serializable;

/**
 * @author hungnm
 * 
 */
public class RptBCTGTMVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String mien;
	private String vung;
	private String maNpp;
	private String tenNpp;
	private String tenGsnpp;
	private String maNv;
	private String tenNv;
	private String ngay;
	private String khoangThoiGianTatMay;
	
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getMaNpp() {
		return maNpp;
	}
	public void setMaNpp(String maNpp) {
		this.maNpp = maNpp;
	}
	public String getTenNpp() {
		return tenNpp;
	}
	public void setTenNpp(String tenNpp) {
		this.tenNpp = tenNpp;
	}
	public String getTenGsnpp() {
		return tenGsnpp;
	}
	public void setTenGsnpp(String tenGsnpp) {
		this.tenGsnpp = tenGsnpp;
	}
	public String getMaNv() {
		return maNv;
	}
	public void setMaNv(String maNv) {
		this.maNv = maNv;
	}
	public String getTenNv() {
		return tenNv;
	}
	public void setTenNv(String tenNv) {
		this.tenNv = tenNv;
	}	
	public String getNgay() {
		return ngay;
	}
	public void setNgay(String ngay) {
		this.ngay = ngay;
	}
	public String getKhoangThoiGianTatMay() {
		return khoangThoiGianTatMay;
	}
	public void setKhoangThoiGianTatMay(String khoangThoiGianTatMay) {
		this.khoangThoiGianTatMay = khoangThoiGianTatMay;
	}
}
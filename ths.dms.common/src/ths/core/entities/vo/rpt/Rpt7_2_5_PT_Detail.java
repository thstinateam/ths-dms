package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class Rpt7_2_5_PT_Detail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String shopName;
	private String shopAddress;
	private String cashierCode;
	private String cashierName;
	private String payReceivedNumber;
	private String orderNumber;
	private String strDate;
	private BigDecimal amount;
	private BigDecimal discount;
	private BigDecimal timeOfPay;
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String getCashierCode() {
		return cashierCode;
	}
	public void setCashierCode(String cashierCode) {
		this.cashierCode = cashierCode;
	}
	public String getCashierName() {
		return cashierName;
	}
	public void setCashierName(String cashierName) {
		this.cashierName = cashierName;
	}
	public String getPayReceivedNumber() {
		return payReceivedNumber;
	}
	public void setPayReceivedNumber(String payReceivedNumber) {
		this.payReceivedNumber = payReceivedNumber;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getStrDate() {
		return strDate;
	}
	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getTimeOfPay() {
		return timeOfPay;
	}
	public void setTimeOfPay(BigDecimal timeOfPay) {
		this.timeOfPay = timeOfPay;
	}
	
}

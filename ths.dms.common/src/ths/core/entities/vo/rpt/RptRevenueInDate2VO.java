package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

public class RptRevenueInDate2VO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String staffCode;
	
	private List<RptRevenueInDate1VO> lstRptRevenueInDate1VO;
	
	public String getStaffCode() {
		return staffCode;
	}
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	public List<RptRevenueInDate1VO> getLstRptRevenueInDate1VO() {
		return lstRptRevenueInDate1VO;
	}
	public void setLstRptRevenueInDate1VO(
			List<RptRevenueInDate1VO> lstRptRevenueInDate1VO) {
		this.lstRptRevenueInDate1VO = lstRptRevenueInDate1VO;
	}
	
	
	
}

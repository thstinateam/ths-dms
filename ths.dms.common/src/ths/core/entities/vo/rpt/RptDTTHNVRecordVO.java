package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptDTTHNVRecordVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String staffCode;
	private String staffName;
	private String orderDate;
	private BigDecimal moneyForDay; // tien mat;
	private BigDecimal remainForDay; // tien no;
	private BigDecimal discountForDay; // chiet khau
	private BigDecimal promotionMoneyForDay; // khuyen mai tien
	private BigDecimal promotionStockForDay; // khuyen mai hang
	private BigDecimal revenueForDay; // doanh thu
	private BigDecimal salesForDay; // doanh so

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public BigDecimal getMoneyForDay() {
		return moneyForDay;
	}

	public void setMoneyForDay(BigDecimal moneyForDay) {
		this.moneyForDay = moneyForDay;
	}

	public BigDecimal getRemainForDay() {
		return remainForDay;
	}

	public void setRemainForDay(BigDecimal remainForDay) {
		this.remainForDay = remainForDay;
	}

	public BigDecimal getDiscountForDay() {
		return discountForDay;
	}

	public void setDiscountForDay(BigDecimal discountForDay) {
		this.discountForDay = discountForDay;
	}

	public BigDecimal getPromotionMoneyForDay() {
		return promotionMoneyForDay;
	}

	public void setPromotionMoneyForDay(BigDecimal promotionMoneyForDay) {
		this.promotionMoneyForDay = promotionMoneyForDay;
	}

	public BigDecimal getPromotionStockForDay() {
		return promotionStockForDay;
	}

	public void setPromotionStockForDay(BigDecimal promotionStockForDay) {
		this.promotionStockForDay = promotionStockForDay;
	}

	public BigDecimal getRevenueForDay() {
		return revenueForDay;
	}

	public void setRevenueForDay(BigDecimal revenueForDay) {
		this.revenueForDay = revenueForDay;
	}

	public BigDecimal getSalesForDay() {
		return salesForDay;
	}

	public void setSalesForDay(BigDecimal salesForDay) {
		this.salesForDay = salesForDay;
	}
}

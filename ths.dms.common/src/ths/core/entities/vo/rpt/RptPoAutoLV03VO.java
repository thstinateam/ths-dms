package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ths.dms.core.common.utils.GroupUtility;

public class RptPoAutoLV03VO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String poNumber;
	private Date poAutoDate;
	private String strPoAutoDate;
	private String shopCode;
	private List<RptPoAutoDataVO> listData;

	public RptPoAutoLV03VO() {
		listData = new ArrayList<RptPoAutoDataVO>();
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public List<RptPoAutoDataVO> getListData() {
		return listData;
	}

	public void setListData(List<RptPoAutoDataVO> listData) {
		this.listData = listData;
	}

	public Date getPoAutoDate() {
		return poAutoDate;
	}

	public void setPoAutoDate(Date poAutoDate) {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		this.poAutoDate = poAutoDate;
		this.strPoAutoDate = format.format(poAutoDate);
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getStrPoAutoDate() {
		return strPoAutoDate;
	}

	public void setStrPoAutoDate(String strPoAutoDate) {
		this.strPoAutoDate = strPoAutoDate;
	}

	/**
	 * lay so sku voi dieu kien da order
	 * 
	 * @return so luon sku cua don hang
	 * @author ThuatTQ
	 */
	public Integer getSKU() {
		int sku = 0;

		if (null != listData && listData.size() > 0) {
			RptPoAutoDataVO objectTemp = null;

			for (int i = 0; i < listData.size(); i++) {
				RptPoAutoDataVO vo = listData.get(i);

				if (null == objectTemp) {
					sku++;
					objectTemp = vo;
				} else if (!vo.getProductCode().equals(
						objectTemp.getProductCode())) {
					sku++;
					objectTemp = vo;
				}
			}
		}

		return sku;
	}

	@SuppressWarnings("unchecked")
	public static List<RptPoAutoLV03VO> groupBy(
			List<RptPoAutoDataVO> listData, String fieldName)
			throws Exception {
		List<RptPoAutoLV03VO> listGroup = null;

		try {
			Map<String, List<Object>> mapData = GroupUtility.collectionToHash(
					listData, fieldName);

			if (null != mapData && mapData.size() > 0) {
				listGroup = new ArrayList<RptPoAutoLV03VO>();
				Iterator iterator = mapData.entrySet().iterator();

				while (iterator.hasNext()) {
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					RptPoAutoLV03VO vo = new RptPoAutoLV03VO();

					List<RptPoAutoDataVO> listObject = (List<RptPoAutoDataVO>) mapEntry
							.getValue();

					if (null != listObject && listObject.size() >= 0) {
						vo.setPoNumber((String) mapEntry.getKey());
						vo.setPoAutoDate(listObject.get(0).getPoAutoDate());
						vo.setShopCode(listObject.get(0).getShopCode());
						vo.setListData(listObject);

						listGroup.add(vo);
					}
				}
			}

			return listGroup;
		} catch (Exception ex) {
			throw ex;
		}
	}
}

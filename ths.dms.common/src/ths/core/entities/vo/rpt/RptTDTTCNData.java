package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptTDTTCNData implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String maKH;
	private String tenKH;
	private String maNVTT;
	private String tenNVTT;
	private String maNVGH;
	private String tenNVGH;
	private String maNVBH;
	private String tenNVBH;
	private String idHoaDon;
	private String ngayHD;
	private String soHD;
	private String loaiNo;
	private String ngayTT;
	private String loaiTT;
	private String soCTTT;
	private BigDecimal tienHD;
	private BigDecimal tienTT;
	private BigDecimal tienCK;
	private BigDecimal lanTT;

	public String getMaKH() {
		return maKH;
	}

	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	
	public String getTenKH() {
		return tenKH;
	}

	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}

	public String getNgayHD() {
		return ngayHD;
	}

	public void setNgayHD(String ngayHD) {
		this.ngayHD = ngayHD;
	}

	public String getSoHD() {
		return soHD;
	}

	public void setSoHD(String soHD) {
		this.soHD = soHD;
	}

	public String getLoaiNo() {
		return loaiNo;
	}

	public void setLoaiNo(String loaiNo) {
		this.loaiNo = loaiNo;
	}

	public String getNgayTT() {
		return ngayTT;
	}

	public void setNgayTT(String ngayTT) {
		this.ngayTT = ngayTT;
	}

	public String getLoaiTT() {
		return loaiTT;
	}

	public void setLoaiTT(String loaiTT) {
		this.loaiTT = loaiTT;
	}

	public String getSoCTTT() {
		return soCTTT;
	}

	public void setSoCTTT(String soCTTT) {
		this.soCTTT = soCTTT;
	}

	public BigDecimal getTienHD() {
		return tienHD;
	}

	public void setTienHD(BigDecimal tienHD) {
		this.tienHD = tienHD;
	}

	public BigDecimal getTienTT() {
		return tienTT;
	}

	public void setTienTT(BigDecimal tienTT) {
		this.tienTT = tienTT;
	}
	
	public BigDecimal getTienCK() {
		return tienCK;
	}

	public void setTienCK(BigDecimal tienCK) {
		this.tienCK = tienCK;
	}

	public String getIdHoaDon() {
		return idHoaDon;
	}

	public void setIdHoaDon(String idHoaDon) {
		this.idHoaDon = idHoaDon;
	}

	public String getMaNVTT() {
		return maNVTT;
	}

	public void setMaNVTT(String maNVTT) {
		this.maNVTT = maNVTT;
	}

	public String getTenNVTT() {
		return tenNVTT;
	}

	public void setTenNVTT(String tenNVTT) {
		this.tenNVTT = tenNVTT;
	}

	public String getMaNVBH() {
		return maNVBH;
	}

	public void setMaNVBH(String maNVBH) {
		this.maNVBH = maNVBH;
	}

	public String getTenNVBH() {
		return tenNVBH;
	}

	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}

	public BigDecimal getLanTT() {
		return lanTT;
	}

	public void setLanTT(BigDecimal lanTT) {
		this.lanTT = lanTT;
	}

	public String getMaNVGH() {
		return maNVGH;
	}

	public void setMaNVGH(String maNVGH) {
		this.maNVGH = maNVGH;
	}

	public String getTenNVGH() {
		return tenNVGH;
	}

	public void setTenNVGH(String tenNVGH) {
		this.tenNVGH = tenNVGH;
	}
}
package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.math.BigDecimal;

public class RptXBTNVRecord implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String staffCode;
	public String staffName;
	// ma hang hoa
	private String productCode;

	// gia hang hoa
	private BigDecimal price;

	// so luong xuat
	private BigDecimal exportQuantity;

	// tong tien xuat
	private BigDecimal exportAmount;

	// so luong ban
	private BigDecimal sellQuantity;

	// tong tien ban
	private BigDecimal sellAmount;

	// so luong ban
	private BigDecimal promoteQuantity;

	// tong tien ban
	private BigDecimal promoteAmount;

	// so luong ton
	private BigDecimal stockQuantity;

	// tong tien ton
	private BigDecimal stockAmount;

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getExportQuantity() {
		return exportQuantity;
	}

	public void setExportQuantity(BigDecimal exportQuantity) {
		this.exportQuantity = exportQuantity;
	}

	public BigDecimal getExportAmount() {
		return exportAmount;
	}

	public void setExportAmount(BigDecimal exportAmount) {
		this.exportAmount = exportAmount;
	}

	public BigDecimal getSellQuantity() {
		return sellQuantity;
	}

	public void setSellQuantity(BigDecimal sellQuantity) {
		this.sellQuantity = sellQuantity;
	}

	public BigDecimal getSellAmount() {
		return sellAmount;
	}

	public void setSellAmount(BigDecimal sellAmount) {
		this.sellAmount = sellAmount;
	}

	public BigDecimal getPromoteQuantity() {
		return promoteQuantity;
	}

	public void setPromoteQuantity(BigDecimal promoteQuantity) {
		this.promoteQuantity = promoteQuantity;
	}

	public BigDecimal getPromoteAmount() {
		return promoteAmount;
	}

	public void setPromoteAmount(BigDecimal promoteAmount) {
		this.promoteAmount = promoteAmount;
	}

	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public BigDecimal getStockAmount() {
		return stockAmount;
	}

	public void setStockAmount(BigDecimal stockAmount) {
		this.stockAmount = stockAmount;
	}
}

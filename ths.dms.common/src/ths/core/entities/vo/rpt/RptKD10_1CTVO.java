package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptKD10_1CTVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String mien;//mien
	private String vung;//khu vuc
	private String maNPP;//ma nha phan phoi
	private String tenNPP;//ten nha phan phoi
	private String maKH;//ma khach hang
	private String diaChi;//dia chi
	private BigDecimal dsKH;//doanh so ke hoach
	private BigDecimal dsTH;//doanh so thuc hien
	private Float ptTH;//phan tram thuc hien
	private Integer mucTieuF4;//muc tieu f4
	private Integer thucHienF4;//thuc hien f4
	private Float lkPTTH;// luy ke phan tram thuc hien
	
	public void safeSetNull() throws IllegalArgumentException, IllegalAccessException{
		for(Field field:getClass().getDeclaredFields()) {
			if(field.getType().equals(Integer.class) && field.get(this) == null) {
				field.set(this, 0);
			}
			if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
				field.set(this, BigDecimal.ZERO);
			}
			if(field.getType().equals(String.class) && field.get(this) == null) {
				field.set(this, "");
			}
		}
	}
	public String getMien() {
		return mien;
	}
	public void setMien(String mien) {
		this.mien = mien;
	}
	public String getVung() {
		return vung;
	}
	public void setVung(String vung) {
		this.vung = vung;
	}
	public String getMaNPP() {
		return maNPP;
	}
	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}
	public String getTenNPP() {
		return tenNPP;
	}
	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}
	public String getMaKH() {
		return maKH;
	}
	public void setMaKH(String maKH) {
		this.maKH = maKH;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public BigDecimal getDsKH() {
		return dsKH;
	}
	public void setDsKH(BigDecimal dsKH) {
		this.dsKH = dsKH;
	}
	public BigDecimal getDsTH() {
		return dsTH;
	}
	public void setDsTH(BigDecimal dsTH) {
		this.dsTH = dsTH;
	}
	public Float getPtTH() {
		return ptTH;
	}
	public void setPtTH(BigDecimal ptTH) {
		if (ptTH != null) {
			this.ptTH = ptTH.floatValue();
		}
	}
	public Integer getMucTieuF4() {
		return mucTieuF4;
	}
	public void setMucTieuF4(BigDecimal mucTieuF4) {
		if (mucTieuF4 != null) {
			this.mucTieuF4 = mucTieuF4.intValue();
		}
	}
	public Integer getThucHienF4() {
		return thucHienF4;
	}
	public void setThucHienF4(BigDecimal thucHienF4) {
		if (thucHienF4 != null) {
			this.thucHienF4 = thucHienF4.intValue();
		}
	}
	public Float getLkPTTH() {
		return lkPTTH;
	}
	public void setLkPTTH(BigDecimal lkPTTH) {
		if (lkPTTH != null) {
			this.lkPTTH = lkPTTH.floatValue();
		}
	}
	
}

package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

public class RptBCTDDSTKHNHMHSubCatLv3VO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal customerId;
	private String customerCode;
	private String customerName;
	private String shortCode;

	private BigDecimal catId;
	private String catCode;
	private String catName;
	
	private BigDecimal subCatId;
	private String subCatCode;
	private String subCatName;
	private BigDecimal amountMoney;
	private List<RptBCTDDSTKHNHMHBrandLv4VO> lstDataLv4;
	
	public void safeSetNull() throws Exception{
		try {
			for(Field field:getClass().getDeclaredFields()) {
				if(field.getType().equals(Integer.class) && field.get(this) == null) {
					field.set(this, 0);
				}else if(field.getType().equals(BigDecimal.class) && field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				}else if(field.getType().equals(Float.class) && field.get(this) == null) {
					field.set(this, 0f);
				}else if(field.getType().equals(Double.class) && field.get(this) == null) {
					field.set(this, 0d);
				}else if(field.getType().equals(Long.class) && field.get(this) == null) {
					field.set(this, 0l);
				}else if(field.getType().equals(String.class) && field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			throw e;
		} 		
	}
	
	public BigDecimal getAmountMoney() {
		return amountMoney;
	}

	public void setAmountMoney(BigDecimal amountMoney) {
		this.amountMoney = amountMoney;
	}

	public BigDecimal getCustomerId() {
		return customerId;
	}

	public void setCustomerId(BigDecimal customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public BigDecimal getCatId() {
		return catId;
	}

	public void setCatId(BigDecimal catId) {
		this.catId = catId;
	}

	public String getCatCode() {
		return catCode;
	}

	public void setCatCode(String catCode) {
		this.catCode = catCode;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public BigDecimal getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(BigDecimal subCatId) {
		this.subCatId = subCatId;
	}

	public String getSubCatCode() {
		if(null != subCatName && !subCatName.equals("")){
			return subCatCode + " - " + subCatName;
		}
		return subCatCode;
	}

	public void setSubCatCode(String subCatCode) {		
		this.subCatCode = subCatCode;
	}

	public String getSubCatName() {		
		return subCatName;
	}

	public void setSubCatName(String subCatName) {
		this.subCatName = subCatName;
	}

	public List<RptBCTDDSTKHNHMHBrandLv4VO> getLstDataLv4() {
		return lstDataLv4;
	}

	public void setLstDataLv4(List<RptBCTDDSTKHNHMHBrandLv4VO> lstDataLv4) {
		this.lstDataLv4 = lstDataLv4;
		if(null != lstDataLv4 && lstDataLv4.size() > 0){
			amountMoney = BigDecimal.ZERO;
			for(int i = 0; i<lstDataLv4.size(); i++){
				if(null != lstDataLv4.get(i).getAmountMoney()){
					amountMoney = amountMoney.add(lstDataLv4.get(i).getAmountMoney());
				}				
			}		
		}	
	}

	public RptBCTDDSTKHNHMHSubCatLv3VO() {
		
	}	
}

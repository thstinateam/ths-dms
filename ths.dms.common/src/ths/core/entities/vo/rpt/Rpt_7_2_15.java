package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class Rpt_7_2_15 implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mien;
	private String tenMien;
	private String vung;
	private String tenVung;
	private String maNPP;
	private String tenNPP;
	private String GSNPP;
	private String tenGSNPP;
	private String NVBH;
	private String tenNVBH;
	private String KH;
	private String tenKH;
	private String soHoaDon;
	private String ngayHoaDon;
	private String nganhHang;
	private String sku;
	private BigDecimal datHang;
	private BigDecimal duyet;
	private BigDecimal chenhLech;
	private Integer groupNo;
	
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
		}
	}
	
	public String getMien() {
		return mien;
	}

	public void setMien(String mien) {
		this.mien = mien;
	}

	public String getTenMien() {
		return tenMien;
	}

	public void setTenMien(String tenMien) {
		this.tenMien = tenMien;
	}

	public String getVung() {
		return vung;
	}

	public void setVung(String vung) {
		this.vung = vung;
	}

	public String getTenVung() {
		return tenVung;
	}

	public void setTenVung(String tenVung) {
		this.tenVung = tenVung;
	}

	public String getMaNPP() {
		return maNPP;
	}

	public void setMaNPP(String maNPP) {
		this.maNPP = maNPP;
	}

	public String getTenNPP() {
		return tenNPP;
	}

	public void setTenNPP(String tenNPP) {
		this.tenNPP = tenNPP;
	}

	public String getGSNPP() {
		return GSNPP;
	}

	public void setGSNPP(String gSNPP) {
		GSNPP = gSNPP;
	}

	public String getTenGSNPP() {
		return tenGSNPP;
	}

	public void setTenGSNPP(String tenGSNPP) {
		this.tenGSNPP = tenGSNPP;
	}

	public String getNVBH() {
		return NVBH;
	}

	public void setNVBH(String nVBH) {
		NVBH = nVBH;
	}

	public String getTenNVBH() {
		return tenNVBH;
	}

	public void setTenNVBH(String tenNVBH) {
		this.tenNVBH = tenNVBH;
	}

	public String getKH() {
		return KH;
	}

	public void setKH(String kH) {
		KH = kH;
	}

	public String getTenKH() {
		return tenKH;
	}

	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}

	public String getSoHoaDon() {
		return soHoaDon;
	}

	public void setSoHoaDon(String soHoaDon) {
		this.soHoaDon = soHoaDon;
	}

	public String getNgayHoaDon() {
		return ngayHoaDon;
	}

	public void setNgayHoaDon(String ngayHoaDon) {
		this.ngayHoaDon = ngayHoaDon;
	}

	public String getNganhHang() {
		return nganhHang;
	}

	public void setNganhHang(String nganhHang) {
		this.nganhHang = nganhHang;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public BigDecimal getDatHang() {
		return datHang;
	}

	public void setDatHang(BigDecimal datHang) {
		this.datHang = datHang;
	}

	public BigDecimal getDuyet() {
		return duyet;
	}

	public void setDuyet(BigDecimal duyet) {
		this.duyet = duyet;
	}

	public BigDecimal getChenhLech() {
		return chenhLech;
	}

	public void setChenhLech(BigDecimal chenhLech) {
		this.chenhLech = chenhLech;
	}

	public Integer getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(Integer groupNo) {
		this.groupNo = groupNo;
	}

}
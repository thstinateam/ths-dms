package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptTotalAmountParentVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String staffCode;
	private String staffName;
	private List<RptTotalAmountVO> listTotalAmount = new ArrayList<RptTotalAmountVO>();
	/**
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}
	/**
	 * @param staffCode the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	/**
	 * @return the staffName
	 */
	public String getStaffName() {
		return staffName;
	}
	/**
	 * @param staffName the staffName to set
	 */
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	/**
	 * @return the listTotalAmount
	 */
	public List<RptTotalAmountVO> getListTotalAmount() {
		return listTotalAmount;
	}
	/**
	 * @param listTotalAmount the listTotalAmount to set
	 */
	public void setListTotalAmount(List<RptTotalAmountVO> listTotalAmount) {
		this.listTotalAmount = listTotalAmount;
	}
	
}

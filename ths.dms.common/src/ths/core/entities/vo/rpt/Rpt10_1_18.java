package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.List;

/**
 * VO: bao cao doi tra hang hu hong 10.1.18
 * 
 * @author lacnv1
 * @since Mar 11, 2014
 */
public class Rpt10_1_18 implements Serializable {

	private static final long serialVersionUID = 1L;

	private Double dinhMucChoPhep;
	private Double doanhSoNhom;
	private Double tongCong;
	List<Rpt10_1_18_DHHH> lstDetail;
	public Double getDinhMucChoPhep() {
		return dinhMucChoPhep;
	}
	public void setDinhMucChoPhep(Double dinhMucChoPhep) {
		this.dinhMucChoPhep = dinhMucChoPhep;
	}
	public Double getDoanhSoNhom() {
		return doanhSoNhom;
	}
	public void setDoanhSoNhom(Double doanhSoNhom) {
		this.doanhSoNhom = doanhSoNhom;
	}
	public Double getTongCong() {
		return tongCong;
	}
	public void setTongCong(Double tongCong) {
		this.tongCong = tongCong;
	}
	public List<Rpt10_1_18_DHHH> getLstDetail() {
		return lstDetail;
	}
	public void setLstDetail(List<Rpt10_1_18_DHHH> lstDetail) {
		this.lstDetail = lstDetail;
	}
	
}
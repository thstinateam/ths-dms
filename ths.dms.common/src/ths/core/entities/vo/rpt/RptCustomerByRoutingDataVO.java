package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public class RptCustomerByRoutingDataVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private BigDecimal staffId;
	private String staffCode;
	private String staffName;
	private String staffStatus;

	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String customerDistrict;
	private String customerPrecinct;

	private String seq;
	private String mobiphone;
	private String dateName;
	private String customerType;
	private BigDecimal customerStatus;
	private String displayPosition;
	private String displayName;
	//private BigDecimal interval;
	private BigDecimal week1;
	private BigDecimal week2;
	private BigDecimal week3;
	private BigDecimal week4;
	
	public void safeSetNull() {
		try {
			for (Field field : getClass().getDeclaredFields()) {
				if (field.getType().equals(Integer.class)
						&& field.get(this) == null) {
					field.set(this, 0);
				} else if (field.getType().equals(BigDecimal.class)
						&& field.get(this) == null) {
					field.set(this, BigDecimal.ZERO);
				} else if (field.getType().equals(Float.class)
						&& field.get(this) == null) {
					field.set(this, 0f);
				} else if (field.getType().equals(Double.class)
						&& field.get(this) == null) {
					field.set(this, 0d);
				} else if (field.getType().equals(Long.class)
						&& field.get(this) == null) {
					field.set(this, 0l);
				} else if (field.getType().equals(String.class)
						&& field.get(this) == null) {
					field.set(this, "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	public RptCustomerByRoutingDataVO() {
	}
	
	
	public String getCustomerDistrict() {
		return customerDistrict;
	}


	public void setCustomerDistrict(String customerDistrict) {
		this.customerDistrict = customerDistrict;
	}


	public String getCustomerPrecinct() {
		return customerPrecinct;
	}


	public void setCustomerPrecinct(String customerPrecinct) {
		this.customerPrecinct = customerPrecinct;
	}


	public BigDecimal getStaffId() {
		return staffId;
	}

	public void setStaffId(BigDecimal staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffStatus() {
		return staffStatus.equals("0")?"Không hoạt động":"Hoạt động";
	}

	public void setStaffStatus(String staffStatus) {
		this.staffStatus = staffStatus;
	}
	
	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getMobiphone() {
		return mobiphone;
	}

	public void setMobiphone(String mobiphone) {
		this.mobiphone = mobiphone;
	}

	public String getDateName() {
		return dateName;
	}

	public void setDateName(String dateName) {
		this.dateName = dateName;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public BigDecimal getCustomerStatus() {
		return customerStatus;
	}

	public void setCustomerStatus(BigDecimal customerStatus) {
//		if ("1".equals(customerStatus)) {
//			this.customerStatus = "HĐ";
//		} else {
//			this.customerStatus = "TN";
//		}
		this.customerStatus = customerStatus;
	}

	public String getDisplayPosition() {
		return displayPosition;
	}

	public void setDisplayPosition(String displayPosition) {
		this.displayPosition = displayPosition;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public BigDecimal getWeek1() {
		return week1;
	}


	public void setWeek1(BigDecimal week1) {
		this.week1 = week1;
	}


	public BigDecimal getWeek2() {
		return week2;
	}


	public void setWeek2(BigDecimal week2) {
		this.week2 = week2;
	}


	public BigDecimal getWeek3() {
		return week3;
	}


	public void setWeek3(BigDecimal week3) {
		this.week3 = week3;
	}


	public BigDecimal getWeek4() {
		return week4;
	}


	public void setWeek4(BigDecimal week4) {
		this.week4 = week4;
	}


	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
}
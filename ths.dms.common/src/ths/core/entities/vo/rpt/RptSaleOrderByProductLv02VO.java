package ths.core.entities.vo.rpt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RptSaleOrderByProductLv02VO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String cat;
	private List<RptSaleOrderByProductDataVO> listData;

	public RptSaleOrderByProductLv02VO() {
		this.listData = new ArrayList<RptSaleOrderByProductDataVO>();
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public List<RptSaleOrderByProductDataVO> getListData() {
		return listData;
	}

	public void setListData(List<RptSaleOrderByProductDataVO> listData) {
		this.listData = listData;
	}
}
